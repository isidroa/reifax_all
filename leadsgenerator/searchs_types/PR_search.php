     
<div align="left" class="search_realtor_fondo" id="PR_search_div" style=" display:none;" >
    <table width="620" border="0" cellpadding="0" cellspacing="0" style="font-size:12px; margin:auto;">
        <tr>    	    	
            <td colspan="4" style="color:#FFF; font-weight:bold;">Search By</td>                
            <td colspan="6" style="color:#FFF; font-weight:bold;">Type</td>                
            <td colspan="2" style="color:#FFF; font-weight:bold;">State</td>         
            <td width="130" style="color:#FFF; font-weight:bold;">County</td>      	
        </tr>
        <tr>    	    	
            <td colspan="3">
                <input type="hidden" id="PR_combo_search_types" value="PR" />
                <select name="combo_search_by" id="PR_combo_search_by" style="width:105px;" onchange="doSearchByFilter(this.value);">
                    <option value="LOCATION" <?php if($search_by_type=='LOCATION') echo 'selected="selected"';?>>Location</option>
                    <option value="MAP" <?php if($search_by_type=='MAP') echo 'selected="selected"';?>>Map</option>
                </select>
            </td>
            <td width="5">&nbsp;</td>
            <td colspan="5">
            	<select id="PR_proptype" name="proptype" style="width:120px;">
                	<option value="">Any Type</option>
                    <option value="01">Single Family</option>
                    <option value="04">Condo/Town/Villa</option>
                    <option value="03">Multi Family +10</option>
                    <option value="08">Multi Family -10</option>
                    <option value="11">Commercial</option>
                    <option value="00">Vacant Land</option>
                    <option value="02">Mobile Home</option>
                    <option value="99">Other</option>
              	</select>
            </td>
            <td width="5">&nbsp;</td>
            <td width="127"><select name="state_search" id="PR_state_search" style="width:120px;" onchange="doChangeState(this.value);">
            <?php
                $query='select idstate,state FROM xima.lsstate where is_showed="Y" order by state';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($state_search==$r['idstate'])
                        echo '<option value="'.$r['idstate'].'" selected="selected">'.$r['state'].'</option>';
                    else
                        echo '<option value="'.$r['idstate'].'">'.$r['state'].'</option>';
                }
            ?>
            </select></td>
            <td width="5">&nbsp;</td>
            <td width="130"><select name="county_search" id="PR_county_search" style="width:110px;" onchange="if(search_by_type=='MAP') mapSearch.centerMapCounty(this.value,true); else mapSearch.centerMapCounty(this.value,false); search_county=this.value;">
            <?php
                $query='select idcounty,county FROM xima.lscounty where ximapro="Y" and idstate='.$state_search.'  order by county';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($county_search==$r['idcounty'])
                        echo '<option value="'.$r['idcounty'].'" selected="selected">'.$r['county'].'</option>';
                    else
                        echo '<option value="'.$r['idcounty'].'">'.$r['county'].'</option>';
                }
            ?>
            </select></td>
        </tr>
        <tr>    	    	
            <td colspan="13" style="color:#FFF; font-weight:bold;">
                <span id="PR_tsearch_l">Location</span>
            </td>                                
        </tr>        
        <tr>             	
            <td colspan="13">
                <input type="text" name="search" id="PR_search" size="65" maxlength="2048" style="font-size:17px;" value="Address, City or Zip Code" onfocus="colocarDefault('PR_search',this.value);" onblur="colocarDefault2('PR_search',this.value);">
            </td>                   	
        </tr>        
        <tr>    	    	
            <td colspan="4" id="PR_tprice" style="color:#FFF; font-weight:bold;">Sold Range</td>                
            <td colspan="2" id="PR_tbeds" style="color:#FFF; font-weight:bold;">Beds</td>                
            <td colspan="2" id="PR_tbath" style="color:#FFF; font-weight:bold;">Baths</td>                
            <td colspan="2" id="PR_tsqft" style="color:#FFF; font-weight:bold;">Sqft</td> 
                 
            <td colspan="2" id="PR_tpequity" style="color:#FFF; font-weight:bold;"></td>
                                    
            <td width="130" id="PR_toccupied" style="color:#FFF; font-weight:bold;">&nbsp;</td>
        </tr>        
        <tr>        
            <td width="50" valign="middle">
                <span id="PR_price_dol1" style="color:#FFF;vertical-align:top;">$</span><input name="price_low" type="text" id="PR_price_low" style="width:40px;" onfocus="if(this.value=='min')this.value='';" onblur="if(this.value=='')this.value='min';" value="min" size="6">
  </td>        
            <td width="18" id="PR_price_to" style="text-align:center;color:#FFF;">to</td>        
            <td width="50" valign="middle">
                <span id="PR_price_dol2" style="color:#FFF;vertical-align:top; ">$</span><input id="PR_price_hi" style="width:40px;" type="text" name="price_hi" value="max" size="6" onfocus="if(this.value=='max')this.value='';" onblur="if(this.value=='')this.value='max';">
            </td>        
        <td width="5">&nbsp;</td>        
            <td width="63" valign="middle">
                <select id="PR_bed" name="bed" style=""><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
          <td width="5">&nbsp;</td>        
            <td width="63" valign="middle">
                <select id="PR_bath" name="bath" style=""><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
          <td width="5" valign="middle">&nbsp;</td>        
            <td width="93" valign="middle">
              <select id="PR_sqft" name="sqft" style=""><option value="-1">Any</option><option value="250">250+</option><option value="500">500+</option><option value="1000">1,000+</option><option value="1250">1,250+</option><option value="1500">1,500+</option><option value="1750">1,750+</option><option value="2000">2,000+</option><option value="2250">2,250+</option><option value="2500">2,500+</option><option value="2750">2,750+</option><option value="3000">3,000+</option><option value="3250">3,250+</option><option value="3500">3,500+</option><option value="3750">3,750+</option><option value="4000">4,000+</option><option value="5000">5,000+</option><option value="10000">10,000+</option></select>
            </td>       	
          <td width="5" valign="middle">&nbsp;</td>            	
            <td width="127" valign="middle">
            </td>        
          <td width="5">&nbsp;</td>    	
            <td width="130" style="padding-top:0px;">
                <a href="javascript:void();" class="overviewBotonCss3" onClick="searchForm('PR_search','P');" style="padding: 2px 35px;">Search</a>	
                
            </td>   	
        </tr>
      
 
    </table>    
               
  </div>
  <? //include('html.html');?>
       