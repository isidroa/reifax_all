<div id="grid-example"></div>
<script>
	var selected_dataRG = new Array();
	var AllCheckRG=false;
	var limitRG=50;
	
///////////Cargas de data dinamica///////////////
	var storeRG = new Ext.data.Store({
		url: 'coresearch.php?resultType=advance&systemsearch=<?php echo $_POST['systemsearch'] ?>&groupbylevel=1',
		reader: new Ext.data.JsonReader(),
		remoteSort: true,
		listeners: {
			'load': function(store,data,obj){
				if (AllCheckRG){
					Ext.get(gridRG.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckRG=true;
					gridRG.getSelectionModel().selectAll();
					selected_dataRG=new Array();
				}else{
					AllCheckRG=false;
					var sel = [];
					if(selected_dataRG.length > 0){
						for(val in selected_dataRG){
							var ind = gridRG.getStore().find('pid',selected_dataRG[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridRG.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
	});
///////////FIN Cargas de data dinamica///////////////
    
	var smRG = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataRG.indexOf(record.get('pid'))==-1)
					selected_dataRG.push(record.get('pid'));
				
				if(Ext.fly(gridRG.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckRG=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataRG = selected_dataRG.remove(record.get('pid'));
				AllCheckRG=false;				
			}
		}
	});
		

//-------------------- Creacion del Grid----------------
	var gridRG = new Ext.grid.GridPanel({
		renderTo: 'grid-example',	
		loadMask : true,	  
        store: storeRG,
		columns: [],
		border: false,
		enableColLock: false,
		height: windowHeight-200,  //dimensiones del grid
        width: 'auto',
		sm: smRG,
		listeners: {
			"rowdblclick": function(grid, row, e) {
				var record = this.store.getAt(row);
				var pid = record.get('pid');
				var owner = record.get('owner');
				alert(pid+': '+owner);
			}
		},
		tbar: new Ext.PagingToolbar({
			id: 'pagingRG',
            pageSize: limitRG,
            store: storeRG,
            displayInfo: true,
			displayMsg: 'Total: {2} Properties',
			emptyMsg: "No groups to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 properties per page.',
				text: 50,
				handler: function(){
					limitRG=50;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 properties per page.',
				text: 80,
				handler: function(){
					limitRG=80;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 properties per page.',
				text: 100,
				handler: function(){
					limitRG=100;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			})
			]
        })
	});

 
 	storeRG.on('metachange', function(){
		if(typeof(storeRG.reader.jsonData.columns) === 'object') {
			var columns = [];
			columns.push(smRG);
			Ext.each(storeRG.reader.jsonData.columns, function(column){
				columns.push(column);
			});

			gridRG.getColumnModel().setConfig(columns);
		}
	});

	/////////////Inicializar Grid////////////////////////
	storeRG.load();
	/////////////FIN Inicializar Grid////////////////////



</script>