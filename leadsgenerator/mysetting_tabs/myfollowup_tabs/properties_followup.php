<?php  
	include('../../properties_conexion.php');
	conectar();
	$county = $_POST['county'];
	$pid = $_POST['pid'];
	
	if(isset($_POST['type'])){
		if($_POST['type']=='insert'){
			$query='SELECT count(*) FROM xima.followup WHERE userid='.$_COOKIE['datos_usr']['USERID'].' AND parcelid="'.$pid.'"';
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			if($r[0]>0){
				echo '{success: true, exist: 1, userid:'.$_COOKIE['datos_usr']['USERID'].'}';
			}else{
				conectarPorNameCounty($county);
				$query="SELECT c.address, c.unit, c.city, c.ozip as zip, r.agent, r.mlnumber, c.astatus as status
				FROM psummary c  
				LEFT JOIN mlsresidential r ON(c.parcelid=r.parcelid) 
				WHERE c.parcelid ='".$pid."'";	 
				
				$result=mysql_query($query) or die($query.mysql_error());
				
				$r=mysql_fetch_array($result);
				
				$query='INSERT INTO xima.followup (parcelid,userid,bd,address,unit,city,zip,agent,mlnumber,status) VALUES ("'.$pid.'",'.$_COOKIE['datos_usr']['USERID'].',"'.$county.'","'.$r['address'].'","'.$r['unit'].'","'.$r['city'].'","'.$r['zip'].'","'.$r['agent'].'","'.$r['mlnumber'].'","'.$r['status'].'")';
				mysql_query($query) or die($query.mysql_error());
				
				//Agregar agente to my follow agent si no existe ya.
				$query='INSERT INTO xima.followagent (userid,agent,email,tollfree,phone1,phone2,phone3,fax)
				SELECT f.userid, r.agent, r.agentemail, r.agenttollfree, r.agentph, r.agentph2, r.agentph3, r.agentfax 
				FROM xima.followup f
				INNER JOIN mlsresidential r ON (f.parcelid=r.parcelid)
				LEFT JOIN xima.followagent a ON (r.agent=a.agent AND a.userid=f.userid)
				WHERE f.userid='.$_COOKIE['datos_usr']['USERID'].' AND f.parcelid="'.$pid.'"  and a.agent is null';
				mysql_query($query) or die($query.mysql_error());
				
				//Asignacion del agent a la propiedad.
				$query='INSERT INTO xima.follow_assignment (parcelid,userid,agentid,principal)
				SELECT f.parcelid,f.userid, a.agentid, 1
				FROM xima.followup f
				INNER JOIN flbroward.mlsresidential r ON (f.parcelid=r.parcelid)
				LEFT JOIN xima.followagent a ON (r.agent=a.agent AND a.userid=f.userid)
				WHERE f.userid='.$_COOKIE['datos_usr']['USERID'].' AND f.parcelid="'.$pid.'"';
				mysql_query($query) or die($query.mysql_error());
				
				
				//asiganación de la propiedad en las notificaciones de cada follower.
				$query='SELECT follower_id FROM xima.follower WHERE userid='.$_COOKIE['datos_usr']['USERID'];
				$result=mysql_query($query) or die($query.mysql_error());
				
				$i=0;
				$query='INSERT INTO xima.follow_notification VALUES ';
				while($r=mysql_fetch_array($result)){
					if($i>0) $query.=',';
					$query.='("'.$pid.'",'.$_COOKIE['datos_usr']['USERID'].','.$r[0].',0,1)';
					$i++;
				}
				mysql_query($query) or die($query.mysql_error());
				
				
				echo '{success: true, exist: 0, userid:'.$_COOKIE['datos_usr']['USERID'].'}';
			}
		}elseif($_POST['type']=='delete'){
			$query='DELETE FROM xima.followup_history WHERE userid='.$_COOKIE['datos_usr']['USERID'].' AND parcelid IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup WHERE userid='.$_COOKIE['datos_usr']['USERID'].' AND parcelid IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.follow_notification WHERE userid='.$_COOKIE['datos_usr']['USERID'].' AND parcelid IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup_schedule WHERE userid='.$_COOKIE['datos_usr']['USERID'].' AND parcelid IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='change-status'){
			$query="UPDATE xima.followup SET statusalt='".$_POST['statusalt']."' WHERE parcelid='".$_POST['pid']."' AND userid=".$_COOKIE['datos_usr']['USERID'];
			
			mysql_query($query) or die($query.mysql_error());
			echo '{success: true}';
		}
	}else{
		$userid 	= $_POST['userid'];
		$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
		$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
		$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
		$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
		$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
		$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
		$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
		$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
		$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
		$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
		$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
		$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
								  
		$query="SELECT parcelid as pid, userid, mlnumber, status, bd as county, 
		address, unit, zip, agent, offer, coffer, ndate, ntask, contract, pof, emd, realtorsadem as rademdums, msj, DATEDIFF(NOW(),lasthistorydate) as lasthistorydate, ((offer/lprice)*100) as offerpercent, statusalt
		FROM xima.followup 
		WHERE userid =".$userid." AND type IN ('F','FM')";
		
		//filters
		if(trim($address)!='')
			$query.=' AND address LIKE \'%'.trim($address).'%\'';
			
		if(trim($mlnumber)!='')
			$query.=' AND mlnumber=\''.trim($mlnumber).'\'';
			
		if(trim($agent)!='')
			$query.=' AND agent LIKE \'%'.trim($agent).'%\'';
		
		if($status!='ALL')
			$query.=' AND status=\''.$status.'\'';
			
		if(trim($ndateb)!='' && trim($ndate)!='')
			$query.=' AND ndate BETWEEM \''.$ndate.'\' AND \''.$ndateb.'\'';
		elseif(trim($ndate)!='')
			$query.=' AND ndate=\''.$ndate.'\'';
			
		if(trim($ntask)!='-2'){
			if(trim($ntask)!='-1')
				$query.=' AND ntask='.$ntask;
			else
				$query.=' AND ntask<>0';
		}
			
		if($contract!='-1')
			$query.=' AND contract='.$contract;
		
		if($ademdums!='-1')
			$query.=' AND realtorsadem='.$ademdums;
		
		if($pof!='-1')
			$query.=' AND pof='.$pof;
		
		if($emd!='-1')
			$query.=' AND emd='.$emd;
			
		if($msj!='-1')
			$query.=' AND msj='.$msj;
			
		
		//orders
		if(isset($_POST['sort'])) $query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		
		$result=mysql_query($query) or die($query.mysql_error());
		$i=1;
		$vFilas = array();
		while($r=mysql_fetch_object($result))
			$vFilas[]=$r;
							 
		
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}	
?>