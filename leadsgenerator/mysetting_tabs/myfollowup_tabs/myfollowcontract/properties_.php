<?php
	include("../../../properties_conexion.php");
	conectar();	
	
	if(isset($_POST['followlist'])){
		$userid = $_POST['userid']; 
		
		$query = 'SELECT f.userid, concat(x.name," ",x.surname) name,
				 f.total, f.msj, f.history, f.status1, f.status2, f.status3, f.status4 
				 FROM xima.follower f
				 INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
				 WHERE f.follower_id='.$userid.' AND f.status="Active"';
			
		//orders
		if(isset($_POST['sort'])) $query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		
		$result=mysql_query($query) or die($query.mysql_error());
		$vFilas=array();
		while($r=mysql_fetch_object($result))
			$vFilas[]=$r;			 
		
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
		
	}elseif(isset($_POST['userid'])){ 
		$userid 	= $_POST['userid'];
		$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
		$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
		$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
		$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
		$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
		$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-1';
		$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
		$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
		$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
		$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
		$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
		$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
								  
		$query="SELECT f.parcelid as pid, f.userid, f.mlnumber, IF(f.status='A','Active','Non-Active') as status, f.bd as county, f.address, f.unit, f.zip, f.agent, f.offer, f.coffer, f.ndate, f.ntask, f.contract, f.pof, f.emd, f.realtorsadem as rademdums, n.history, n.msj, DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate 
		FROM xima.followup f
		LEFT JOIN xima.follow_notification n ON (f.parcelid=n.parcelid AND f.userid=n.userid)
		WHERE f.userid =".$userid." AND n.followid=".$_COOKIE['datos_usr']['USERID'];
		
		//filters
		if(trim($address)!='')
			$query.=' AND f.address LIKE \'%'.trim($address).'%\'';
			
		if(trim($mlnumber)!='')
			$query.=' AND f.mlnumber=\''.trim($mlnumber).'\'';
			
		if(trim($agent)!='')
			$query.=' AND f.agent LIKE \'%'.trim($agent).'%\'';
		
		if($status=='A')
			$query.=' AND f.status=\'A\'';
		elseif($status=='NA')
			$query.=' AND f.status<>\'A\'';
			
		if(trim($ndate)!='')
			$query.=' AND ndate=\''.$ndate.'\'';
			
		if(trim($ntask)!='-1')
			$query.=' AND ntask=\''.$ntask.'\'';
			
		if($contract!='-1')
			$query.=' AND f.contract='.$contract;
		
		if($ademdums!='-1')
			$query.=' AND f.realtorsadem='.$ademdums;
		
		if($pof!='-1')
			$query.=' AND f.pof='.$pof;
		
		if($emd!='-1')
			$query.=' AND f.emd='.$emd;
			
		if($msj!='-1')
			$query.=' AND n.msj='.$msj;
			
		if($history!='-1')
			$query.=' AND n.history='.$history;
		
		
		//orders
		if(isset($_POST['sort'])) $query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		
		$result=mysql_query($query) or die($query.mysql_error());
		$vFilas=array();
		while($r=mysql_fetch_object($result))
			$vFilas[]=$r;			 
		
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}
?>