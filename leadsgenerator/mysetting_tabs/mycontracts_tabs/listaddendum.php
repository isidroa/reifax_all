<?php
/**
 * listaddendum.php
 *
 * Return the list of addendums associated to an user ID to show in a extJs Grid.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 14.04.2011
 */

include "../../properties_conexion.php"; 
conectar();

$userid = $_COOKIE['datos_usr']["USERID"];
$aux    = '';
$con    = '';

if ($_GET['cmd'] =='chk' and strlen($_GET['id'])>0) {
	$aux = " AND id=".addslashes($_GET['id']);
	$con = ",content";
}
$query   = "SELECT id,userid,name{$con} FROM xima.contracts_addendum  WHERE userid = $userid $aux ORDER BY id ASC";
$rs      = mysql_query($query);

if ($rs) {
	
	$data = array();

	while ($row = mysql_fetch_array($rs)) {
		
		$data[]         = $row;
		
	}

	$resp = array('success'=>'true','data'=>$data);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	