<style>
#todo_advertising_panel{
	text-align: left;
}
body{
	text-align: left;
}
</style>
<?php 
	include("../../properties_conexion.php");
	conectar();
	
	include ("../../properties_getgridcamptit.php");	
	
	$ancho=950;
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYDoc','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$query='SELECT clientid FROM advertising.client WHERE userid='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	$i=mysql_fetch_array($result);
	$client=$i[0];
	
	include("comboModels.php");
?>
<div id="todo_advertising_panel">
<div id="myadvertising_data_div" style=" margin:auto; width:<?php echo $ancho.'px;';?>">
    <div id="myadvertising_properties"></div>
</div>
</div>
<script>
	var store = new Ext.data.JsonStore({
		totalProperty:'total',
		root		:'results',
		url		:'mysetting_tabs/myadvertising_tabs/operations.php?oper=data&tipo=advertisings',
		fields		:[{name: 'advertisingID', type: 'int'}
						,'clientID'
						,'name'
						,'model'
						,'description',
						,'position',
						,'html'
						,'adverLocation'
						,'type'
						,'limit'
						,'dayLimit'
						,'active'	
					 ]
	});
	var Paginator = new Ext.PagingToolbar({
        pageSize	: 200,
        store		: store,
        displayInfo	: true
	});

	
	/*****************************************************
	 *  Tool Bar del Grind
	 *****************************************************/
	var Toolbar = new Ext.Toolbar({
        items  : ['<b>Advertising GRID</b>','-',
                  {
					text	:'Add Advertising',
					iconCls	:'icon',
					cls	:'x-btn-text-icon',			
					icon	:'img/add.gif',
					id	:'add_button',
					tooltip	:'Click to Add Row',
					handler	: AddWindow 
                  },'-',
				  {
					text	:'Edit Rotations',
					iconCls	:'icon',
					cls	:'x-btn-text-icon',			
					id	:'rotation_button',
					tooltip	:'Click to Edit Rotations',
					handler	: editRotations 
                  }]
    });

	
	/*****************************************************
	 *  Grid
	 *****************************************************/
	var Grid = new Ext.grid.EditorGridPanel({
		store		:store,
                iconCls		:'x-icon-settings',
		icon		:'img/countries.png',
		tbar		:Toolbar, 
		columns		:[new Ext.grid.RowNumberer()
						,{id:'advertisingID',header: "ID",	width: 70, 	sortable: true, align: 'center', dataIndex: 'advertisingID'}
						,{header: 'Description', 		width: 170, sortable: true, align: 'left', dataIndex: 'description'}
						,{header: 'Model',	width: 150, sortable: true, align: 'left', dataIndex: 'model'}
						,{header: 'User Name', 		width: 170, sortable: true, align: 'left', dataIndex: 'name'}
						,{header: 'Position', 		width: 70, sortable: true, align: 'center', dataIndex: 'position', renderer: renderPosition}
						,{header: 'Location', 			width: 70, sortable: true, align: 'center', dataIndex: 'adverLocation'}
						,{header: 'Active', 		width: 70, sortable: true, align: 'center', dataIndex: 'active', renderer: renderActive}
						,{header: 'Type', 		width: 70, sortable: true, align: 'center', dataIndex: 'type', renderer: renderType}
						,{header: "Oper", 			width: 60,  sortable: false, align: 'center', dataIndex: 'advertisingID',renderer: renderTopic}			

					],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height		:470,
		width		: <?php echo ($ancho-10);?>,
		frame		:true,
		loadMask	:true,
		bbar		: Paginator 
	});
	store.load({params:{start:0, limit:200, clientid:<?php echo $client;?>}});
	
	var mypanel = new Ext.form.FormPanel({
        //frame:true,
        bodyStyle:'padding:5px 5px 0; text-align: left;',
        width: <?php echo ($ancho-5);?>,
        items: [{xtype: 'compositefield',
					labelWidth: 5,
					fieldLabel: '<b>Filter By</b>',
					items:[{
						width:50,
						xtype:'label',
						text: 'Position:',
						align: 'center'
					},{
						xtype		:'combo',
						id			:'searchposition',
						name		:'searchposition',
						store		:new Ext.data.SimpleStore({
										fields	:['idposition', 'position'],
										data 	: [['', 'All'],['B', 'Bottom'],['R', 'Right'],['L', 'Left'],['T', 'Top']]
									}),
						editable	: false,
						displayField:'position',
						valueField	:'idposition',
						typeAhead	:true,
						mode		:'local',
						triggerAction: 'all',
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank	:false,
						value		:'',
						width		:100
					},{
						width:50,
						xtype:'label',
						text: 'Location:',
						align: 'center'
					},{
						xtype		:'numberfield',
						id			:'searchlocation',
						name		:'searchlocation',
						width		: 50
					},{
						width:50,
						id:'filter',
						xtype:'button',
						text:'Filter',
						handler: searchAdvertising
					}]},Grid	
				]
	})
	/*****************************************************
	 *  Funciones del Grid (botones)
	 *****************************************************/
	function renderPosition(value, p, record){
		if(value=='B'){
			return 'Bottom';
		}else if(value=='T'){
			return 'Top';
		}else if(value=='R'){
			return 'Right';
		}else if(value=='L'){
			return 'Left';
		}	
	}
	
	function renderActive(value, p, record){
		if(value=='Y'){
			return 'Yes';
		}else if(value=='N'){
			return 'No';
		}
	}
	
	function renderType(value, p, record){
		if(value=='R'){
			return 'Rotary';
		}else if(value=='F'){
			return 'Fixed';
		}
	}
	
	function renderTopic(value, p, record){
		return String.format(
				'<a href="javascript:void(0)" title="Click to edit row." onclick="EditViewWindow(false,{0})"><img src="img/toolbar/update.png" width="15" height="15"></a>&nbsp;'+
				'<a href="javascript:void(0)" title="Click to see detail row." onclick="EditViewWindow(true,{0})"><img src="img/toolbar/search.png" width="15" height="15"></a>&nbsp;'+
				'<a href="javascript:void(0)" title="Click to delete row." onclick="DeleteWindows({0})"><img src="img/delete.gif" width="15" height="15"></a>',value);
	}
	mypanel.render('myadvertising_properties');
	function renderRotations(value, p, record){
		return String.format(
				'<a href="javascript:void(0)" title="Click to see detail row." onclick="EditViewWindow(true,{0})"><img src="img/toolbar/search.png" width="15" height="15"></a>&nbsp;'
				,value);
	}
	function checkRotation(val,p,record){
		var marcado='';
		if(val=='Y'){
			marcado='checked';
		}
		
		var idc=record.data.advertisingID;
		var retorno= String.format('<input type="checkbox" name="cb'+idc+'"  id="cb'+idc+'" onClick="marcaRotar('+idc+')" '+marcado+'>',idc);
		return retorno;
	}
	
	

	/*****************************************************
	 *  Funciones del Form
	 *****************************************************/
	function searchAdvertising(){
		store.load({params:{start:0, limit:200, position: Ext.getCmp('searchposition').getValue(), location: Ext.getCmp('searchlocation').getValue() }});
	}
	
	/*****************************************************
	 * Function New Advertising
	 *****************************************************/
	function AddWindow(){
		var formul = new Ext.FormPanel({
			url			:'mysetting_tabs/myadvertising_tabs/operations.php',
			frame		:true,
			monitorValid: true,
			bodyStyle	:'padding:5px 5px 0',
			items		:[{
							layout		:'form',
							items		:[{
											xtype		:'textfield',
											id			:'description',
											name		:'description',
											fieldLabel	: '<span style="color:#F00">*</span> Description',
											allowBlank	:false,
											width		: 150
										},{
											xtype		:'combo',
											name		:'type',
											id			:'type',
											hiddenName	:'idtype',
											store		:new Ext.data.SimpleStore({
															fields	:['idtype', 'typedesc'],
															data 	: [['F', 'Fixed'],['R', 'Rotary']]
														}),
											editable	: false,
											displayField:'typedesc',
											valueField	:'idtype',
											typeAhead	:true,
											fieldLabel	:'<span style="color:#F00">*</span> Type',
											mode		:'local',
											triggerAction: 'all',
											emptyText	:'Select ...',
											selectOnFocus:true,
											allowBlank	:false,
											value		:'F',
											width		:100
										},{
											xtype		:'combo',
											name		:'position',
											id			:'position',
											hiddenName	:'idposition',
											store		:new Ext.data.SimpleStore({
															fields	:['idposition', 'position'],
															data 	: [['B', 'Bottom'],['R', 'Right'],['L', 'Left'],['T', 'Top']]
														}),
											editable	: false,
											displayField:'position',
											valueField	:'idposition',
											typeAhead	:true,
											fieldLabel	:'<span style="color:#F00">*</span> Position',
											mode		:'local',
											triggerAction: 'all',
											emptyText	:'Select ...',
											selectOnFocus:true,
											allowBlank	:false,
											value		:'B',
											width		:100
										},{
											xtype		:'numberfield',
											id			:'adverLocation',
											name		:'adverLocation',
											fieldLabel	: '<span style="color:#F00">*</span> Location',
											allowBlank	:false,
											width		: 100
										},{
											xtype		:'textarea',
											id			:'html',
											name		:'html',
											fieldLabel	: '<span style="color:#F00">*</span> Html',
											allowBlank	:false,
											width		: 250,
											height		: 80
										},{
											xtype		:'combo',
											id			:'idmodel',
											name		:'idmodel',
											hiddenName	:'cmodel',
											fieldLabel	:'<span style="color:#F00">*</span> Model',
											allowBlank	:false,
											width		:150,
											store		:new Ext.data.SimpleStore({
															fields: ['id', 'modelname'],
															data : Ext.combos_selec.comboModels
														}),
											editable	:false,
											displayField:'modelname',
											valueField	:'id',
											mode		:'local',
											triggerAction:'all',
											emptyText	:'Select..',
											selectOnFocus:true
										},{
											xtype		:'combo',
											name		:'active',
											id			:'active',
											hiddenName	:'idactive',
											store		:new Ext.data.SimpleStore({
															fields	:['idactive', 'active'],
															data 	: [['Y', 'YES'],['N', 'NO']]
														}),
											editable	: false,
											displayField:'active',
											valueField	:'idactive',
											typeAhead	:true,
											fieldLabel	:'<span style="color:#F00">*</span> Active',
											mode		:'local',
											triggerAction: 'all',
											emptyText	:'Select ...',
											selectOnFocus:true,
											allowBlank	:false,
											value		:'N',
											width		:100
										},{
											xtype		:'hidden',
											name		:'tipo',
											hiddenName	:'tipo',
											value		:'advertisings'
										},{
											xtype		:'hidden',
											name		:'oper',
											hiddenName	:'oper',
											value		:'add'
										}]	
						}],
			buttons		:[{
							text		:'Save',
							cls		:'x-btn-text-icon',			
							icon		:'img/save.gif',
							formBind	:true,
							handler 	:function(){
											formul.getForm().submit({
												method		:'POST',
												waitTitle	:'Please wait..',
												waitMsg		:'Sending data...',
												success		:function(){
																store.reload();
																Ext.Msg.alert("Success", "Insertion successfully");
																wind.close();
															},
												failure		:function(form, action){
																obj = Ext.util.JSON.decode(action.response.responseText);
																Ext.Msg.alert("Failure", obj.errors.reason);
															}
											});
						   				}
						},{
							text		:'Cancel',
							cls			:'x-btn-text-icon',			
							icon		:'img/exit.gif',
							handler  	:function(){
											wind.close();
						   				}
						}]	
			
		});
		
		wind = new Ext.Window({
			title		: 'New Advertising',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 400,
			height      : 350,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: formul				
		});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function doAdd()
	
	
	/*****************************************************
	 *  Function Modify Currency
	 *****************************************************/
	EditViewWindow=function (ronly,id){					
		
		Ext.Ajax.request({  
			waitMsg		:'Wait please...',
			url		:'mysetting_tabs/myadvertising_tabs/operations.php', 
			method		:'POST', 
			params		:{ 
							tipo	:"advertisings",
							oper: 'buscar',
							idvalue	: id
						},					
			failure		:function(response,options){
							Ext.MessageBox.alert('Warning','Error user show');
						},
						
			success		:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);						
							if(rest.succes==false){
								Ext.MessageBox.alert('Warning',rest.msg); return;
							}							
							arrtask=rest.results;
							Ext.getCmp('idvalue').setValue(arrtask[0].advertisingID);
							Ext.getCmp('position').setValue(arrtask[0].position);
							Ext.getCmp('description').setValue(arrtask[0].description);
							Ext.getCmp('adverLocation').setValue(arrtask[0].adverLocation);
							Ext.getCmp('active').setValue(arrtask[0].active);
							Ext.getCmp('html').setValue(arrtask[0].html);
							Ext.getCmp('idmodel').setValue(arrtask[0].model);
							Ext.getCmp('panelview').update(arrtask[0].html);
							Ext.getCmp('type').setValue(arrtask[0].type);
						}                                
		});	
		var formul = new Ext.FormPanel({
			url			:'mysetting_tabs/myadvertising_tabs/operations.php',
			frame		:true,
			monitorValid: true,
			bodyStyle	:'padding:5px 5px 0',
			items		:[{						
							layout: 'column',
							items:[{
							columnWidth:.5,
							layout	:'form',
							items	:[{
										readOnly	:true,  
										xtype		:'textfield',
										id			: 'idvalue',
										name		: 'idvalue',
										fieldLabel	: '<span style="color:#F00">*</span> Id Advertising',
										width		: 70
									},{
										readOnly	:ronly,  
										xtype		:'textfield',
										id			:'description',
										name		:'description',
										fieldLabel	: '<span style="color:#F00">*</span> Description',
										allowBlank	:false,
										width		: 150
									},{
										readOnly	:ronly,  
										xtype		:'combo',
										name		:'type',
										id			:'type',
										hiddenName	:'idtype',
										store		:new Ext.data.SimpleStore({
														fields	:['idtype', 'typedesc'],
														data 	: [['F', 'Fixed'],['R', 'Rotary']]
													}),
										editable	: false,
										displayField:'typedesc',
										valueField	:'idtype',
										typeAhead	:true,
										fieldLabel	:'<span style="color:#F00">*</span> Type',
										mode		:'local',
										triggerAction: 'all',
										emptyText	:'Select ...',
										selectOnFocus:true,
										allowBlank	:false,
										value		:'F',
										width		:100
									},{
										readOnly	:ronly,  
										xtype		:'combo',
										name		:'position',
										id			:'position',
										hiddenName	:'idposition',
										store		:new Ext.data.SimpleStore({
														fields	:['idposition', 'position'],
														data 	: [['B', 'Bottom'],['R', 'Right'],['L', 'Left'],['T', 'Top']]
													}),
										editable	: false,
										displayField:'position',
										valueField	:'idposition',
										typeAhead	:true,
										fieldLabel	:'<span style="color:#F00">*</span> Position',
										mode		:'local',
										triggerAction: 'all',
										emptyText	:'Select ...',
										selectOnFocus:true,
										allowBlank	:false,
										value		:'B',
										width		:100
									},{
										readOnly	:ronly,  
										xtype		:'numberfield',
										id			:'adverLocation',
										name		:'adverLocation',
										fieldLabel	: '<span style="color:#F00">*</span> Location',
										allowBlank	:false,
										width		: 100
									},{
										readOnly	:ronly,  
										xtype		:'textarea',
										id			:'html',
										name		:'html',
										fieldLabel	: '<span style="color:#F00">*</span> Html',
										allowBlank	:false,
										width		: 250,
										height		: 80
									},{
										readOnly	:ronly,  
										xtype		:'combo',
										id			:'idmodel',
										name		:'idmodel',
										hiddenName	:'cmodel',
										fieldLabel	:'<span style="color:#F00">*</span> Model',
										allowBlank	:false,
										width		:150,
										store		:new Ext.data.SimpleStore({
														fields: ['id', 'modelname'],
														data : Ext.combos_selec.comboModels
													}),
										editable	:false,
										displayField:'modelname',
										valueField	:'id',
										mode		:'local',
										triggerAction:'all',
										emptyText	:'Select..',
										selectOnFocus:true
									},{
										readOnly	:ronly,  
										xtype		:'combo',
										name		:'active',
										id			:'active',
										hiddenName	:'idactive',
										store		:new Ext.data.SimpleStore({
														fields	:['idactive', 'active'],
														data 	: [['Y', 'YES'],['N', 'NO']]
													}),
										editable	: false,
										displayField:'active',
										valueField	:'idactive',
										typeAhead	:true,
										fieldLabel	:'<span style="color:#F00">*</span> Active',
										mode		:'local',
										triggerAction: 'all',
										emptyText	:'Select ...',
										selectOnFocus:true,
										allowBlank	:false,
										value		:'N',
										width		:100
									},{
										xtype		:'hidden',
										name		:'tipo',
										hiddenName	:'tipo',
										value		:'advertisings'
									},{
										xtype		:'hidden',
										name		:'oper',
										hiddenName	:'oper',
										value		:'update'
									}]
						},{columnWidth:.5,
							layout	:'form',
							items	:[{
										xtype		:'panel',
										id			:'panelview',
										name		:'panelview',
										fieldLabel	: '<span style="color:#F00">*</span> Html View',
										width		: 250,
										height		: 500
									}]
							}]		
						}],
			buttons		:[{
							id		:'butsavedet',
							name	:'butsavedet',
							text	:'Save',
							cls		:'x-btn-text-icon',			
							icon	:'img/save.gif',
							formBind:true,
							handler :function(){
										formul.getForm().submit({
											method		:'POST',
											waitTitle	:'Please wait..',
											waitMsg		:'Sending data...',
											success		:function(){
															store.reload();
															Ext.Msg.alert("Success", "Update succesfully");
															wind.close();
														},
											failure		:function(form, action) {
															obj = Ext.util.JSON.decode(action.response.responseText);
															Ext.Msg.alert("Failure", obj.errors.reason);
														}
										});
						   			}
						},{
							id		:'butcanceldet',
							name	:'butcanceldet',
							text	:'Cancel',
							cls		:'x-btn-text-icon',			
							icon	:'img/exit.gif',
							handler :function(){
										wind.close();
									}
						}]			

		});
		
		wind = new Ext.Window({
			title		: 'Details Advertising',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 750,
			height      : 540,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: formul
		});
				
		wind.show();
		if(ronly==true)	{
			Ext.getCmp('butsavedet').setVisible(false);
			Ext.getCmp('butcanceldet').setText('Accept');
			Ext.getCmp('butcanceldet').setIcon('img/check.png');
		}
		
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}
	
	DeleteWindows=function (selected)
	{
		idgridsel=selected;
		Ext.MessageBox.confirm('Delete', 'Confirm deletion?', deleteRow);
	}
	
	/*****************************************************
	 *  Function Delete Advertising
	 *****************************************************/
	function deleteRow(btn){
		if(btn=='no')
			return;
		Ext.Ajax.request({
			waitTitle	:'Please wait..',
          	waitMsg		:'Sending data...',
			url			:'mysetting_tabs/myadvertising_tabs/operations.php', 
			method		:'POST', 
			params		:{
							tipo: "advertisings",
							oper:'delete',	
							ID: idgridsel,
							key: "advertisingid"
						},
			success		:function(response,options){
							store.reload();
							var d = Ext.util.JSON.decode(response.responseText);
							Ext.MessageBox.alert('Success',d.errors.reason);
						},
			failure		:function(response,options){
							var d = Ext.util.JSON.decode(response.responseText);
							Ext.Msg.alert('Failure', d.errors.reason);
						}
		});		
	}
	/*****************************************************
	 *  Function Edit Rotations
	 *****************************************************/
	function editRotations(){
		var name='';
		var idclient;
		var position;
		var location;
		var formul = new Ext.FormPanel({
			url			:'mysetting_tabs/myadvertising_tabs/operations.php',
			frame		:true,
			bodyStyle	:'padding:5px 5px 0',
			items		:[{layout		:'form',
							items		:[{
								xtype		:'combo',
								name		:'sposition',
								id			:'sposition',
								hiddenName	:'idposition',
								store		:new Ext.data.SimpleStore({
												fields	:['idposition', 'position'],
												data 	: [['B', 'Bottom'],['R', 'Right'],['L', 'Left'],['T', 'Top']]
											}),
								editable	: false,
								displayField:'position',
								valueField	:'idposition',
								typeAhead	:true,
								fieldLabel	:'<span style="color:#F00">*</span> Position',
								mode		:'local',
								triggerAction: 'all',
								emptyText	:'Select ...',
								selectOnFocus:true,
								allowBlank	:false,
								value		:'B',
								width		:100
							},{
								xtype		:'numberfield',
								id			:'sadverLocation',
								name		:'sadverLocation',
								fieldLabel	: '<span style="color:#F00">*</span> Location',
								allowBlank	:false,
								width		: 100
							},{
								xtype		:'hidden',
								name		:'tipo',
								hiddenName	:'tipo',
								value		:'searchrotations'
							},{
								xtype		:'hidden',
								name		:'oper',
								hiddenName	:'oper',
								value		:'data'
							}]
						}],
			buttons: [{
				text: 'Search',
				handler: function(){
							windowRotations.show();
							position = formul.getForm().getValues().idposition;
							location = formul.getForm().getValues().sadverLocation;
							storeRotations.load({params:{start:0, limit:200,  sadverLocation: location, idposition: position}});
							storeOrders.load({params:{start:0, limit:200,  sadverLocation: location, idposition: position}});
							GridRotations.setTitle('Block '+formul.getForm().getValues().idposition+formul.getForm().getValues().sadverLocation);
							windowSelectUser.close();
						}
			},{
				text: 'Reset',
				handler: function(){
						formul.getForm().reset();
					}
			}]			
		});				
							
		windowSelectUser = new Ext.Window({
			title		: 'Select Ubication',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 250,
			height      : 200,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: formul
		});
		var storeRotations = new Ext.data.JsonStore({
			totalProperty:'total',
			root		:'results',
			url		:'mysetting_tabs/myadvertising_tabs/operations.php?oper=data&tipo=searchorders',
			fields		:[{name: 'advertisingID', type: 'int'}
							,'clientID'
							,'name'
							,'model'
							,'description',
							,'block'
							,'orderQ'
							,'active'
						 ]
		});
		var Paging = new Ext.PagingToolbar({
			pageSize	: 200,
			store		: storeRotations,
			displayInfo	: true,
			items  : ['<b></b>'+name,'-',
					  ]
		});
		var storeOrders = new Ext.data.JsonStore({
			totalProperty:'total',
			root		:'results',
			url		:'mysetting_tabs/myadvertising_tabs/operations.php?oper=data&tipo=searchrotations',
			fields		:['orderQ'
						 ]
		});
		var GridRotations = new Ext.grid.EditorGridPanel({
			id:"GridRotations",
			store		:storeRotations,
			title: 'Rotations',
			iconCls		:'x-icon-settings',
			tbar: Paging,
			enableDragDrop: true,
			columns		:[	{header: 'Order', 	width: 70, sortable: true, align: 'center', dataIndex: 'orderQ', editor: new Ext.form.ComboBox({
               typeAhead: true,
               triggerAction: 'all',
               store: storeOrders,
			   mode: 'local',
			   triggerAction: 'all',
			   editable: false,
			   selectOnFocus:true,
			   displayField:'orderQ',
			   valueField: 'orderQ',
               listClass: 'x-combo-list-small'
            })}
							,{id:'advertisingID',header: "ID",	width: 50, 	sortable: true, align: 'center', dataIndex: 'advertisingID'}
							,{header: 'Description', 		width: 150, sortable: true, align: 'left', dataIndex: 'description'}
							,{header: 'Model',	width: 150, sortable: true, align: 'left', dataIndex: 'model'}
							,{header: 'Blocks', 		width: 70, sortable: true, align: 'center', dataIndex: 'block'}
							,{header: 'Active', 		width: 70, sortable: true, align: 'center', dataIndex: 'active', renderer: checkRotation}
							,{header: "Oper", 			width: 60,  sortable: false, align: 'center', dataIndex: 'advertisingID',renderer: renderRotations}			

						],
			clicksToEdit:2,
			sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
			height		:500,
			width		:700,
			frame		:true,
			loadMask	:true
		});
		windowSelectUser.show();
		windowSelectUser.addListener("beforeshow",function(wind){
			wind.close();
		});
		windowRotations = new Ext.Window({
			title		: 'Grid Rotations',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 700,
			height      : 500,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: GridRotations
		});
		GridRotations.addListener('afteredit', doEdit);
		function doEdit(oGrid_Event) {
			var fieldValue = oGrid_Event.value;
			var ID =	oGrid_Event.record.data.userid;
			campo=oGrid_Event.field;
			info=fieldValue;
			type='text';
			//alert("clientid="+idclient+"&position="+position+"&location="+location+"&adverid"+oGrid_Event.record.data.advertisingID+"&orderNuevo="+fieldValue+"&tipo=modificaorder");
			if(fieldValue=='BEGIN')
			{Ext.MessageBox.alert('Warning','Error select BEGIN');store.reload();return;}
			var ajax=nuevoAjax();
			ajax.open("POST", "mysetting_tabs/myadvertising_tabs/operations.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("oper=update&position="+position+"&location="+location+"&keyID="+oGrid_Event.record.data.advertisingID+"&value="+fieldValue+"&tipo=modificaorder");
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					storeRotations.commitChanges();
					storeRotations.reload();
				}
			}
		};
	}
	function nuevoAjax()
	{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
	}
	function marcaRotar(idcont){
		var marca='N';
		if(document.getElementById("cb"+idcont).checked==true)marca='Y';
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'mysetting_tabs/myadvertising_tabs/operations.php', 
				method: 'POST',
				params: {
					tipo: "marcarotation",
					oper:'update',	
					key: 'advertisingid',
					keyID: idcont,
					field: 'active',
					value: marca
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("GridRotations").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("GridRotations").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		);  
					
	}
</script>