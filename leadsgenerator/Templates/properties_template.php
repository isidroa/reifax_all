<?php 
$_SERVERXIMA="http://leads.reifax.com/";
//if(isset($_GET['webuser'])){$r=true;}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES CON MAPA
	function tagHeadHeader($mapa=true)
	{?> 
    	<title>REIFAX | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
         <meta content='IE=8' http-equiv='X-UA-Compatible'/>
<?php
		if(isset($_COOKIE['datos_usr']['cleancache']) && $_COOKIE['datos_usr']['cleancache']=='Y')
		{
?>		
		<meta http-equiv="cache-control" content="no-cache"> 
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<?php
		}
?> 
        <meta http-equiv="Content-Language" content="EN-US"> 
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/img/toolbar/home.png">
		
        <?php if($mapa){?>
        <!--<script charset="UTF-8" type="text/javascript" src="<?php echo $link;?>://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>-->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4&libraries=drawing"></script>
        <?php }?>
		<!-- ExtJS -->
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
        
        <!-- JQuery -->
        <script type="text/javascript" language="javascript" src="http://www.reifax.com/includes/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="http://www.reifax.com/includes/jquery.carouFredSel-4.3.3-packed.js"></script>
        
        <!-- htmlEditor -->
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.Word.js"></script>
      	
        <!-- data view plugins -->
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/DataView-more.js"></script>
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/data-view.css"/>
        
        <!-- fileuploadinput -->
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/FileUploadField.js"></script>
        
        <!-- geolocalización -->
     	<!--<script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/geo.js"></script>
        
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/fileuploadfield.css"/>
    	<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/includes/css/layout2.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/includes/css/properties_css.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/includes/css/advertising_css.css" />
<?php 	//handlingError();
	}
	
	function topLoginHeader($advertising=true, $aboutus=true, $login=true, $register=true, $usa=true, $contactus=true, $livesupport=true, $trainning=true,$tickets=true)
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		
		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
			$result=mysql_query($que) or die($que.mysql_error());
			$r=mysql_fetch_array($result);
			$htmls=$r['master_htmls'];?>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
	                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
    	  	</table>
           	<p>&nbsp;</p>
                               
		<? }else{   ?>        
			<div align="center" style=" <?php if (!$realtor && !$investor && !$webmaster){?>height:80px;<? }else{?>height:160px;<? }?> width:100%; position:relative; padding-top:10px;">
            	
                <div style="float:left;">
                    <a href="<?php echo $_SERVERXIMA;?>/properties_search.php" target="_self">
                    <?php if ($realtor){ 
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Realtor Web">
                    <?php } elseif ($investor){ 
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Investor Web">
                    <?php } elseif ($webmaster){ 
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Webmaster Web">
					<?php }else{?> 
                    	<img src="http://www.reifax.com/img/leadsgenerator.png" width="240" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                    <?php } ?>
                    </a> 
                </div>
                
              <div align="left" class="reifaxMenu">
                	<ul>
                        <?php if($register){?>
                        <!--<li id="top_m_reg">
                        	<a href="#" onclick="PlansAndPricing(); return false;">
                            	<img src="http://www.reifax.com/img/menu/shopcart.png" />
                                <br />
                                Plans / 
                                <br />
                                Sign Up
                           	</a>
                        </li>-->
                        <?php }?>
                        
                        <?php if($login){?>
                        <li id="top_m_login">
                            <a href="#" onClick="login_win.show(); return false;">
                                <img src="http://www.reifax.com/img/menu/login.png" />
                                <br />
                                Log
                                <br />
                                In
                            </a>
                        </li>
                        
                        <li id="top_m_user" style="display:none;">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF; display:none;" title="My Account">
                            </a>
                            
                        	<a href="#" onClick="session_release(); return false;" title="Log Out">
                            	<img src="http://www.reifax.com/img/menu/logout.png" />
                                <br />
                                Log
                                <br />
                                Out
                            </a>
                        </li>
                        <?php }?>
                        
                        
                        
                        <?php if($aboutus){?>
                        <!--<li>
                            <a href="#" onclick="AboutUs(); return false;">
                            	<img src="http://www.reifax.com/img/menu/about.png" />
                                <br />
                            	About
                                <br />
                                Us
                           	</a>
                        </li>-->
                        <?php }?>
						
                        <?php if($contactus){?>
                        <li>
                            <a href="http://www.reifax.com/settings/myProducts.php">
                            	<img src="http://www.reifax.com/img/menu/about.png" />
                                <br />
                            	My
                                <br />
                                Products
                           	</a>
                        </li>
                        <?php }?>
                        
                        <?php if($usa){?>
                        <li>
                        	<a href="http://www.reifax.com/FLORIDA/state.php" target="_blank">
                            	<img src="http://www.reifax.com/img/menu/properties.png" />
                                <br />
                            	USA
                            </a>
                        </li>
                        <?php }?>
                        
                        <?php if($trainning){?>
                        <!--<li id="top_m_training" >
                        	<a href="properties_training.php" target="_blank">
                            	<img src="http://www.reifax.com/img/menu/trainning.png" />
                                <br />
                            	Training
                            </a>
                        </li>-->
                        <?php }?>
                        
                        <?php if($tickets){?>
                        <li id="top_m_training" >
                        	<a href="http://www.reifax.com/settings/tickets.php" target="_blank">
                            	<img id="iconTicket" src="http://www.reifax.com/img/menu/ticket.png" />
                                <br />
                            	Tickets
                            </a>
                        </li>
                        <script>
						setInterval(function (){
						$.ajax({
							url:'check_ticket.php',
							dataType: 'JSON',
							success:function (res){
								if(res.msg){
										$('#iconTicket').attr('src',"http://www.reifax.com/img/menu/ticketRed.png");
								}
								else{
										$('#iconTicket').attr('src',"http://www.reifax.com/img/menu/ticket.png");
								}
							}
						})}, 60000);
						</script>
                        <?php }?>
                        
                        <?php if($livesupport){?>
                        <li>
                            <a href="#">
                            	<img src="http://www.reifax.com/img/menu/support_on.png" />
                                <br />
                            	Live
                                <br />
                                Support
                           	</a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
				<?php if($livesupport){?>
                <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
                <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:5px; right:8px;  margin: -2px 0px 0px 0px; "></div><div id="sdSlBh"></div> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <noscript><div style="display:inline"><a href="<?php echo $link;?>://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
                <!-- END ProvideSupport.com Graphics Chat Button Code -->
                <?php }?>
            </div>
            
            <?php if($advertising){?>
            <!--
            <div class="image_carousel" id="image_carousel">
                <div id="carrouselTop">
                    <img src="http://www.reifax.com/img/advertising/lead generator.jpg" width="660" height="300" />
                    <!--<img src="http://www.reifax.com/img/advertising/mobile app.jpg" width="660" height="300"  />
                    <img src="http://www.reifax.com/img/advertising/buyer list.jpg" width="660" height="300"  />
                    <!--<img src="http://www.reifax.com/img/advertising/contract generator.jpg" width="660" height="300"  />
                    <img src="http://www.reifax.com/img/advertising/Home Deals Finder.jpg" width="660" height="300"  />
                </div>
                <!--<div class="clearfix"></div>
                <div class="pagination" id="carrouselTop_pag"></div>
            </div>

            
            <script type="text/javascript" language="javascript">
				$(function() {
					//	Basic carousel
					$('#carrouselTop').carouFredSel({
						items: 1,
						auto: {
							play: true,
							pauseDuration: 6000
						},
						scroll: {
							items: 1,
							duration: 600,
							pauseOnHover: true
						},
						pagination : {
							container	: "#carrouselTop_pag",
							duration	: 600
						}
					});
				});
			</script>-->
            <?php }?>
<?php 
 } }

	
	 //infromacion para el google-analytics
	function googleanalytics()
	{
?>
        <script type="text/javascript">		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-3129734-3']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
<?php 
	}
?>			