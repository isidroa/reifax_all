<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Change Password</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Change Password - Here you can change your password</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext mySettings">
					<h2 style="color:#000; font-weight:bold;">Change Password</h2>
                    <form id="changePassword" action="../resources/php/funcionesMySettings.php">
                        <table>
                            <tr>
                            	<td>
                                	<label class="required">Old Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" name="txtOldPassword">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">New Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" id="password" name="txtPassword" maxlength="20">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 5 characters</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                     <div class="centertext">
                                	<a href="#" id="processForm" class="buttonblue bigButton">Save Change</a>
                     </div>
                                </td>
                           <td></td>
                            </tr>
                     </table>
                        <!--
                        campos hidden
                        //-->
                        <input type="hidden" value="updatePass" name="option">
                     </form>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('.numberPhone input').unbind('blur');
	$('#changePassword').bind('submit',funciones.proccessUpdate)
	$('#processForm').bind('click',function (){
		$('#changePassword').trigger('submit');
		})
	correcto=true;
});

</script>