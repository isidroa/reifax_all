<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	$sql='select surname, name ,email from ximausrs where userid='.$_COOKIE['datos_usr']['USERID'];
	$query=mysql_query($sql);
	$datos_usr=mysql_fetch_array($query);	$fechahoy="NOW()";
	$query="SELECT	x.NAME,x.SURNAME,x.EMAIL,$fechahoy hoy,
			DATE_FORMAT($fechahoy, '%d') diahoy,
			DATE_FORMAT($fechahoy, '%m') meshoy,
			DATE_FORMAT($fechahoy, '%Y') aniohoy,
			DATE_FORMAT(x1.fechacobro, '%d') diausuario,
			DATE_FORMAT(x1.fechacobro, '%m') mesusuario,
			DATE_FORMAT(x1.fechacobro, '%Y') aniousuario 
			FROM xima.ximausrs x
			INNER JOIN xima.usr_cobros x1 ON x1.userid=x.userid
			WHERE x1.userid=".$_COOKIE['datos_usr']['USERID']." order by x1.fechacobro DESC";

	$res=mysql_query($query) or die($query.mysql_error());
	$row= mysql_fetch_array($res, MYSQL_ASSOC);

	if($row['meshoy']==12)
	{
		if($row['diahoy']>=$row['diausuario'])
			$fecha=($row['aniohoy']+1)."-01-".$row['diausuario'];
		else
			$fecha=$row['aniohoy']."-".$row['meshoy']."-".$row['diausuario'];
	}
	else
	{
		if($row['diahoy']>=$row['diausuario'])
			$fecha=$row['aniohoy']."-".($row['meshoy']+1)."-".$row['diausuario'];
		else
			$fecha=$row['aniohoy']."-".$row['meshoy']."-".$row['diausuario'];
	}

    $fecha1=date('Y-m-d');
    $fecha2=$fecha;
	//arregla las fechas invalidas
	$timestamp=strtotime($fecha);
	$paydatecur=$Months[date('m',$timestamp)-1].' '.date('d Y',$timestamp);
	
	
	$query = "SELECT p.preciofreeze
				FROM xima.usr_cobros u
				INNER JOIN `usr_productobase` p on p.idproductobase=u.idproductobase
				where userid=".$_COOKIE['datos_usr']['USERID'];
	$result = mysql_query($query) or die(mysql_error());
	$row=mysql_fetch_array($result, MYSQL_ASSOC);
	$freezeamt=$row["preciofreeze"];
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Freeze Account</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Freeze Account - Here you can freeze your account</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext mySettings">
					<h2 style="color:#000; font-weight:bold;">Freeze your Account</h2>
                    <div class="legentSettings">
                    <p>
                    	This service allows you to keep your current product subscription price as long as you pay for the freeze service.
                    </p>
                    <p>
                    	Your subscription is paid until  <?php echo $paydatecur; ?>.
                    </p>
                    <p>
                    	Best date to freeze your subscription is 48 hours prior to the above mentioned date. If you wish to freeze it now, please continue.
                     </p>
                     <p>
                     Freeze monthly rate= $15
                     </p>
                    </div>
                    <form id="freezarAccount" action="../resources/php/funcionesMySettings.php">
                    
                        <table>
                            <tr>
                            	<td>
                                	<label class="">Full name</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo $datos_usr['name'].' '.$datos_usr['surname'] ?>" readonly >
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Email address</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo $datos_usr['email'] ?>" readonly>
                                </td>
                            </tr>
                            <tr>
                        	<tr>
                            	<td>
                                </td>
                            	<td><div class="clear"></div>
                                	<label class="blacktext">We will reply to your Email, confirming the freezing of your account.</label><br>
                                	<label class="blacktext">Please let us know the reasons you want to freeze your product.</label>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                            	<td>
                                	<textarea name="msg_cancel" style="width:579px"></textarea>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                     <div class="centertext">
                                	<a href="#" id="processForm" class="buttonblue bigButton">Freeze Account</a>
                     </div>
                                </td>
                            </tr>
                     </table>
                        <!--
                        campos hidden
                        //-->
                        <input type="hidden" value="FreezeAccount" name="option">
                     </form>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('#freezarAccount').bind('submit',funciones.proccessUpdate).bind('callback',function (){
		window.location='unfreezeAcount.php';
		});
	$('#processForm').bind('click',function (){$('#freezarAccount').trigger('submit')});
});

</script>