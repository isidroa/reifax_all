<?php 
	require_once('../resources/php/properties_conexion.php');
	require_once('../resources/template/template.php');
	conectar('xima');
	$query=mysql_query('select NAME,SURNAME,STATE,CITY,ADDRESS,HOMETELEPHONE,MOBILETELEPHONE,EMAIL,pseudonimo from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Affiliate Commission</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Affiliate Commission - Here you can check how much you've earned every month</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div >
					<!-- <h2 style="color:#000; font-weight:bold;">Personal Data</h2> -->
                    <table>
						<tr>
							<td>
								<div id="gridPanelAff" style="padding-top:2px; width: 800px; "></div>
							</td>
						</tr>
                    </table>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>

</body>

</html>
<script language="javascript">
 menuClick('menu-my-partner');
</script>
<script language="javascript">
Ext.BLANK_IMAGE_URL='../resources/js/extjs3/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: '../resources/php/funcionesMySettings.php',
		baseParams  :{start:0, limit:50, option:'affMarketing',paffmarkmonth:<?php echo date('m');?>,
			paffmarkyear:<?php echo date('Y');?>,userid:<?php echo $_COOKIE['datos_usr']['USERID'];?>},
		fields: [
			{name: 'Userid', type: 'int'}
			,'Name'
			,'surname'
			,'Operation' 
			,'Date'
			,'payamount'
			,'Amount'
			,'Aff_Userid'
			,'Aff_Name'
			,'Aff_status'
			,'Posi'
		]
	});
///////////FIN Cargas de data dinamica///////////////

//////////////////Manejo de Eventos//////////////////////
	function searchAffMarketing(){
		store.setBaseParam('paffmarkmonth',Ext.getCmp("affmonth").getValue());
		store.setBaseParam('paffmarkyear',Ext.getCmp("affyear").getValue());
		store.load();
		obtenerTotal();
	}
	function obtenerTotal(){
		var arrayRapido = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
		var totales = store.getRange(0,store.getCount());
		var i,yearc,monthc;
		var acum=0;
		var fecha = new Date();
		var year=Ext.getCmp("affyear").getValue();
		var month=Ext.getCmp("affmonth").getValue();
		for(i=0;i<store.getCount();i++)
			acum = acum + parseFloat(totales[i].data.Amount);
		var cad='';
		if(year=='')
		{monthc=arrayRapido[fecha.getMonth()-1]; yearc=fecha.getFullYear();}
		else
		{monthc=arrayRapido[month-1]; yearc=year;}
		cad=monthc+' '+yearc;
		Ext.getCmp('total').setText('Total commission earned on the month of '+cad+': $ '+(acum).toFixed(2));	
	}
	
	
//////////////////FIN Manejo de Eventos//////////////////////

////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.Toolbar({
		//width: 700,
		height: 30,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			//labelWidth: 120,
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;<b>Date</b>',
			items:[{
				xtype:'combo',
				width:100,
				id:'affmonth',
				name:'affmonth',
				store: new Ext.data.SimpleStore({
					fields: ['id','text'],
					data : [['01','January'],['02','February'],['03','March'],['04','April'],['05','May'],['06','June'],['07','July'],['08','August'],['09','September'],['10','October'],['11','November'],['12','December']]
				}),
				editable: false,
				valueField: 'id',
				displayField: 'text',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: <?php echo date('m');?>
			},{
				xtype:'combo',
				width:60,
				id:'affyear',
				name:'affyear',
				store: new Ext.data.SimpleStore({
					fields: ['id','text'],
					data : [<?php for($y=date('Y');$y>=2007;$y--){ if($y<date('Y'))echo ","; echo "['$y','$y']";}?>]
				}),
				editable: false,
				valueField: 'id',
				displayField: 'text',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: <?php echo date('Y');?>
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchAffMarketing
			},{
				width:150,
				xtype:'label',
				id: 'empty',
				text: ' '
			},{
				width:400,
				xtype:'label',
				id: 'total',
				text: 'Total commission earned on the month of <?php echo date('F Y');?>: $0.00',
		        //margins: '5 0 0 50',
				align: 'right'
			}]
		}]
	});
	
////////////////FIN barra de pagineo//////////////////////

///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		//renderTo: 'gridPanelAff',
        //title: '<?php echo $_COOKIE['datos_usr']['NAME']." ".$_COOKIE['datos_usr']['SURNAME']." (".$_COOKIE['datos_usr']['USERID'].")";?>',
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			// new Ext.grid.RowNumberer()
			{header: " ", width:50, align: 'center', sortable: true, dataIndex: 'Posi'}
			,{header: "Date", width: 80, align: 'left', sortable: true, dataIndex: 'Date'}
			,{header: "Description", width: 260, align: 'left', sortable: true, dataIndex: 'Operation'}			
			//,{header: 'User ID', width: 60, sortable: true, align: 'center', dataIndex: 'Aff_Userid'}
			,{header: 'Customer Name', width: 180, sortable: true, align: 'left', dataIndex: 'Aff_Name'}
			,{header: 'Customer Payment ', width: 120, sortable: true, align: 'right', dataIndex: 'payamount',
				renderer: function(v, params, record){
					if(record.data.payamount!='')
						return Ext.util.Format.usMoney(record.data.payamount);
				}}
			,{header: 'Customer Status', width: 100, sortable: true, align: 'left', dataIndex: 'Aff_status'}
			,{header: 'Comm. Amount', width: 100, sortable: true, align: 'right', dataIndex: 'Amount',
				renderer: function(v, params, record){
					if(record.data.Amount!='')
						return Ext.util.Format.usMoney(record.data.Amount);
				}}
		],			
		height:430,
		width: 950,
		frame:false,
		loadMask:true,
		border: false,
		tbar: panelpag 
	});
	var mypanel = new Ext.Panel({
		renderTo: 'gridPanelAff',
        frame:true,
		height:500,
		width: 975,		
        title: '<?php echo $_COOKIE['datos_usr']['NAME']." ".$_COOKIE['datos_usr']['SURNAME']." (".$_COOKIE['datos_usr']['USERID'].")";?>',
        bodyStyle:'padding:5px 5px 0',
        items: [ grid ]
	});
		
/////////////////FIN Grid////////////////////////////

/////////////Inicializar Grid////////////////////////
	store.addListener('load', obtenerTotal);
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
</script>

