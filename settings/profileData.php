<?php 
	require_once('../resources/php/properties_conexion.php');
	require_once('../resources/template/template.php');
	conectar('xima');
	$query=mysql_query('SELECT * FROM profile where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Profile Data</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Profile Data - Here you can change your profile information</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext">
					<h2 style="color:#000; font-weight:bold;">Profile Data</h2>
                    <div class="profileData">
                        <div>
                            <div>
                                <img title="profimg" src="/<?php echo $data['profimg'] ?>">
                            </div>
                            <div class="centertext">
                                <label>
                                    Profile Picture
                                </label>
                                    <div class="upload uploadprofile">
                                        <input name="profile" type="file">
                                    </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <img title="companyimg" src="/<?php echo $data['companyimg'] ?>">
                            </div>
                            <div class="centertext">
                                <label>
                                    Company Logo
                                </label>
                                <div class="upload uploadprofile">
                                	<input name="companyimg" type="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <form action="../resources/php/funcionesMySettings.php" id="updateProfileData">
                    <table>
                        	<tr>
                            	<td>
                                	<label>Profile Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo $data['profname'] ?>" type="text" id="profname" name="profname">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Profile Email</label>
                                </td>
                                <td>
                                	<input  type="text" value="<?php echo $data['profemail'] ?>" id="profemail" name="profemail">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<label>Profile Phone</label>
                                </td>
                                <td>
                                	<input  type="text" value="<?php echo $data['profphone'] ?>" id="txtPhone" name="txtPhone">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<label>Profile Fax</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo $data['proffax'] ?>" id="txtMobil" name="txtMobil">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr><tr>
                            	<td>
                                	<label>User Company</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo $data['companyname'] ?>" id="txtCompanyName" name="txtCompanyName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<?php /*?><tr>
                            	<td>
                                	<label>About Us</label>
                                </td>
                                <td>
                                	<textarea name="aboutus"><?php echo $data['aboutus'] ?>
                                    </textarea>
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Contact Us</label>
                                </td>
                                <td>
                                	<textarea name="contactus"><?php echo $data['contactus'] ?>
                                    </textarea>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr><?php */?>
                            <tr>
                            <td></td>
                            	<td>
                     <div class="centertext">
                                	<a href="#" id="formProcessing" class="buttonblue bigButton">Save Changes</a>
                     </div>
                                </td>
                           <td></td>
                            </tr>
                     </table>
                     <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="option"  value="saveDataPro">
                       </form>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif">  
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 email=true;
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$("textarea").cleditor();

 	/***
	validaciones de campos requeridos
	************/
	$('#formProcessing').bind('click',function (){
		var correcto=true;
		$('.fieldRequired').each(function (){
				$(this).trigger('blur')
			})
		if(correcto && email)
		{
			$('.MsglockScreen').fadeIn(200);
			$('.lockScreen').fadeTo(200,0.5);
			$('#updateProfileData').submit();
		}
		else
		{
			//$(this).removeClass('buttongreen').html('Required fields are empty').addClass('buttonred')
			return false; 
		} 
		})
	$('#updatePersonalData').bind('submit',function ()
	{
		$("#txtPhone").attr('value',$('.numberPhone:eq(0) input:eq(0)').val()+'-'+$('.numberPhone:eq(0) input:eq(1)').val().substring(-1,3)+'-'+$('.numberPhone:eq(0) input:eq(1)').val().substring(3)+'-'+$('.numberPhone:eq(0) input:eq(2)').val());
		$("#txtMobil").attr('value',$('.numberPhone:eq(1) input:eq(0)').val()+'-'+$('.numberPhone:eq(1) input:eq(1)').val().substring(-1,3)+'-'+$('.numberPhone:eq(1) input:eq(1)').val().substring(3)+'-'+$('.numberPhone:eq(1) input:eq(2)').val());
		return false;
	})
	$('#updateProfileData').bind('submit',funciones.proccessUpdate);
	$('.upload > input').each(function (){
		$(this).bind('change',function (){
		$(this).upload('../resources/php/funcionesMySettings.php', 'option=changePicture', function(res) {
			$('.profileData img').each(function (){
				$(this).attr('src','../resources/php/funcionesMySettings.php?reloadImg=true&content='+$(this).attr('title')+'&nocache='+Math.random());
				})
            }, 'json');
		})
	})
});
</script>