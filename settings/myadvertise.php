<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	function advertises()
	{
	conectar('xima');
	$sql='select u.idadv, c.County, s.State, u.position, a.descripcion, u.urladvertise enlace, u.urlimg imagen
from lsstate s, lscounty c, adv_status a, usr_advertising u 
where c.idcounty=u.idcounty AND  s.idstate= u.idstate AND u.idstatus=a.idadv_status AND u.userid='.$_COOKIE['datos_usr']['USERID'];
	$query=mysql_query($sql);
	$posiciones=array(
	1 => '1st Position',
	2 => '2nd Position',
	3 => '3rd Position',
	4 => '4th Position',
	5 => '5th Position',
	6 => '6th Position'
	);
		while($data=mysql_fetch_array($query))
		{
			echo '
            <div class="adminAdvertise">
                	<div class="descripcion image" id="img'.$data['idadv'].'" style="background-image:url('.$data['imagen'].')">
                    	<a href="#" class="buttonblue changeImg">Change Image</a>
                    </div>
                    <div class="descripcion">
                    	<table>
                        	<tr>
                            	<td>
                                <label>State:</label>
                                </td>
                                <td>
                                '.$data['State'].'
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                <label>County:</label>
                                </td>
                                <td>
                                '.$data['County'].'
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                <label>Position:</label>
                                </td>
                                <td>
                                '.$posiciones[$data['position']].'
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                <label>Status:</label>
                                </td>
                                <td>
                                '.$data['descripcion'].'
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                <label>Url Advertising:</label>
                                </td>
                                <td><span id="url'.$data['idadv'].'">
                                '.$data['enlace'].'</span>
                                <a href="#'.$data['idadv'].'" class="buttonblue changeUrl">Change Url</a>
                                </td>
                            </tr>
                        </table>
						<div class="NewImage">
							<div class="upload uploadAdvertise">
                                	<input name="img" id="upload'.$data['idadv'].'" type="file">
                            </div>
								<label>Ó</label>
							<div>
							<input id="urlimg'.$data['idadv'].'" class="inputUrl" value="Enter url"> <a href="#'.$data['idadv'].'" class="buttongreen proccessImg">Go.</a>
							</div>
								
						</div>
                    </div>
             </div>
			 <div class="clear"></div>';
		}
	}
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">My Advertising</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">My Advertising - Here you can review your Ad settings</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center">
                <h2>Advertising</h2>
                <div class="legentSettings">
                <p>The information and image displayed below are related to your current or past Ad purchase. If your ad hasn't expired yet (it is still Active), you may change the image and the url. Please be advised that any change you make will take up to 24 business hours to be posted on our webpage.</p>
                <div class="clear"></div>
                </div>
                <?php advertises(); ?>
             <div class="clear"></div>
         </div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-my-advertise');
 
function testImage(URL) {
	var tester=new Image();
	tester.src=URL;
	return tester.height != 0
}
 
 $(document).ready(function (){
	 
	$('.upload > input').bind('change',function (){
		$(this).upload('../resources/php/funcionesMySettings.php', 'option=changeAdvertise&id='+$(this).attr('id').replace('upload',''), function(respuesta) {
							if(respuesta.success)
							{
								console.debug(respuesta.id);
								console.debug(respuesta.url);
								console.debug($('#img'+respuesta.id));
								$('#img'+respuesta.id).css('background-image','url('+respuesta.url+')');
								$('#img'+respuesta.id).next().children('.NewImage').fadeOut(200);
							}
            }, 'json');
		})
	
	 $('.changeUrl').bind('click', function (e){
			e.preventDefault();
			var url=prompt('Enter the url to advertise');
			if(url && url!=null)
			{
				$.ajax({
					url		:'../resources/php/funcionesMySettings.php',
					data 	:'option=updateUrlAdvertise&id='+$(this).attr('href').replace('#','')+'&url='+url,
					type	:'POST',
					dataType:'json',
					success	:function (respuesta)
						{
							console.debug(respuesta);
							if(respuesta.success)
							{
								console.debug(respuesta.id);
								console.debug(respuesta.url);
								$('#url'+respuesta.id).html(respuesta.url);
							}
						}
					})
			}
		})
	$('.changeImg').bind('click', function (e){
		 e.preventDefault();
		$(this).parent().next().find('.NewImage').fadeIn('200');
		})
		
	$('.inputUrl').each(function (){
			$(this).bind('focus',function (){
				$(this).attr('value',($(this).val()=='Enter url')?'':$(this).val());
			}).bind('blur',function (){
				$(this).attr('value',($(this).val()=='')?'Enter url':$(this).val());
			})
		})
	
	$('.proccessImg').bind('click',function (e){
		e.preventDefault();
		var url=$(this).parent().find('input').val();
		if(url!='Enter url')
		{			
			if(testImage(url))
			{
				console.debug('correcta');
				$.ajax({
					url		:'../resources/php/funcionesMySettings.php',
					data 	:'option=changeAdvertise&id='+$(this).attr('href').replace('#','')+'&url='+url,
					type	:'POST',
					dataType:'json',
					success	:function (respuesta)
						{
							if(respuesta.success)
							{
								$('#img'+respuesta.id).css('background-image','url('+respuesta.url+')');
								$('#img'+respuesta.id).next().children('.NewImage').fadeOut(200);
							}else{
								$('#img'+respuesta.id).next().children('.NewImage').fadeOut(200);
							}
						}
					})
			}
			else{
				alert('ERROR - The URL you entered is not a valid image.');
			}
		}
		})

});

</script>