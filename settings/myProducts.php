<?php
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	//conectar('xima');
	$conReiFax=conectar();
	// verifica si el usuario se encuantra frezzeado
	$sql='select idstatus from ximausrs where userid='.$_COOKIE['datos_usr']['USERID'];
	$query=$conReiFax->query($sql);
	$resul=$query->fetch_array();
	$status=$resul['idstatus'];
	if($status==6)
	{
		echo "<script>document.location='unfreezeAcount.php';</script>";
	}
	// verifica los prductos activos para el usuario
	$sql='select p.idproducto  from usr_productobase p, usr_cobros c where c.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.idproductobase=c.idproductobase AND idstatus=5;';
	$resul=$conReiFax->query($sql);
	$inactiveProduct=array();
	while ($status=$resul->fetch_array())
	{
		$inactiveProduct[$status['idproducto']]=true;
	}
	if(isset($inactiveProduct[1]) || isset($inactiveProduct[2]))
	{
		echo "<script>document.location='homeinactive.php';</script>";
	}
	//funcion que se encarga de mostrar los productos disponibles para el usuario
	function printProduc($name, $img, $url, $idproducto=0, $default=0,$inactive,$btnDefault=true)
		{
			if(!$inactive)
			{
			echo '
				<div class="panel">
           	 		<div class="center panelMyProducts">
						<div style="float:left">
							<a href="'.$url.'">
								<img src="'.$img.'">
							</a>
						</div>
							<h2>
							'.$name.'
							</h2>';

				//if(in_array($_COOKIE['datos_usr']['USERID'],array(4,5,73,2482,3213,3840,2342,4399,4345,4280,3811,3175,3013))){
					$urlTablet = str_replace('.reifax.com','m.reifax.com',$url);
					echo '<a href="'.$urlTablet.'" class="buttonblue bigbutton" style="right: 298px;">
							Tablet Version
						</a>';
				//}

					echo '<a href="'.$url.'" class="buttongreen bigbutton" style="'.(($btnDefault)?'':'right: 30px;').'">
								Access Your Product
							</a>';
				if($btnDefault){
					if($default==0){
						echo '
									<a href="javascript:void(0)" rel="'.$idproducto.'|1" class="buttonblue bigbutton buttonDefault">
										Make Default
									</a>';
					}else{
						echo '
									<a href="javascript:void(0)" rel="'.$idproducto.'|0" class="buttonred bigbutton buttonDefault">
										Cancel Default
									</a>';
					}
				}
				echo '
						<div class="clear">&nbsp;</div>
						</div>
					</div>
				<div class="clear">&nbsp;</div>
				';
			}
		}
	function panelMyProducts()
	{

		//conectar('xima');
		$conReiFax=conectar();
		$sql="SELECT homefinder, buyerspro, leadsgenerator, residential, platinum, professional, professional_esp FROM permission where userid=".$_COOKIE['datos_usr']['USERID'].";";
		$respuesta=$conReiFax->query($sql);
		if($respuesta->num_rows>0)
		{
			$sql="SELECT idproducto FROM usr_producto_default where userid=".$_COOKIE['datos_usr']['USERID']." AND `default`=1";
			$result=$conReiFax->query($sql) or die($sql.' '.$conReiFax->error);
			$default=0;
			if($result->num_rows>0){
				$r=$result->fetch_array();
				$default=$r[0];
			}

			///Sistema pro especial///////////
			$sql="SELECT idproductobase FROM usr_cobros where userid=".$_COOKIE['datos_usr']['USERID']." AND idproductobase=19";
			$result=$conReiFax->query($sql) or die($sql.' '.$conReiFax->error);
			$systempro=false;
			if($result->num_rows>0)
				$systempro=true;
			//////////////////////////////////

			$status=$respuesta->fetch_assoc();
			if($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1)
			{
				if($_COOKIE['datos_usr']['USERID']==2482){
					printProduc('REIFax USA', '', 'http://usa.reifax.local/properties_search.php',0,14,($default==14 ? 1:0));
				}
				//ERROR en la Funcion printProduc les falta un parametro a cada uno
				if($status['professional']==1 || $status['professional_esp']==1)
				{
					if($systempro){
						printProduc('REIFax Platinum', '/img/Platinum.png ', 'http://pro.reifax.com/properties_search.php',14,($default==14 ? 1:0));
					}else{
						printProduc('REIFax Professional', '/img/Professional.png ', 'http://professional.reifax.com',0,3,($default==3 ? 1:0));
					}
				}
				else if($status['platinum']==1)
				{
					printProduc('REIFax Platinum', '/img/Platinum.png ', 'http://platinum.reifax.com',2,($default==2 ? 1:0));
				}
				if(!$systempro){
					printProduc('Home Deal Finder', '/img/homefinder.png', 'http://dealfinder.reifax.com',0,7,($default==7 ? 1:0));
				}
				printProduc('Buyer`s Pro', '/img/buyerspro.png', 'http://buyerspro.reifax.com',0,6,($default==6 ? 1:0));
				printProduc('Leads Generator', '/img/leadsgenerator.png ','http://leads.reifax.com',0,8,($default==8 ? 1:0));

			}
			else
			{
				if($status['residential']==1)
				{
					printProduc('REIFax Residential', '/img/Residential.png ', 'http://residential.reifax.com',1,($default==1 ? 1:0),$inactiveProduct[3]);
				}
				($status['homefinder'])? printProduc('Home Deal Finder', '/img/homefinder.png', 'http://dealfinder.reifax.com',7,($default==7 ? 1:0),$inactiveProduct[7]):null;
				($status['buyerspro'])? printProduc('Buyer`s Pro', '/img/buyerspro.png', 'http://buyerspro.reifax.com',6,($default==6 ? 1:0),$inactiveProduct[6]):null;
				($status['leadsgenerator'])? printProduc('Leads Generator', '/img/leadsgenerator.png ','http://leads.reifax.com',8,($default==8 ? 1:0),$inactiveProduct[8]):null;
			}
		}
			if($_COOKIE['datos_usr']['idusertype']==52){
					printProduc('REIFax Short Sale', '/img/logo-reifax-short-sale.png ', 'http://residential.reifax.com',1,0,NULL,false);
			}
	}
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>

    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt;
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt;
        	<span class="fuchsiatext">My Products</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">
            	     <span class="title bold bluetext">My Products - Please click on the product you want to use</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
        <?php
			panelMyProducts($inactiveProduct);
			if(in_array($_COOKIE['datos_usr']['USERID'],array(4,5,2482,3213))){
				echo '
					<div class="panel">
						<div class="center panelMyProducts">
							<div style="float:left">
								<a href="//test.reifax.com/properties_search.php">
									<img src="">
								</a>
							</div>
								<h2>
								Test Mobile
								</h2>
								<a href="//test.reifax.com/main2.php" class="buttongreen bigbutton" style="right: 30px;">
									Access Your Product
								</a>
							<div class="clear">&nbsp;</div>
							</div>
						</div>
					<div class="clear">&nbsp;</div>
				';
			}
		?>

        <!--End ext container -->

    </div>
	    <div class="clear">&nbsp;</div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script type="text/javascript">
	menuClick('menu-my-products');
	$('.buttonDefault').bind('click',function (){
		$.ajax({
			type	:'POST',
			url		:'../resources/php/makeDefaultProducts.php',
			data	: "idproducto="+$(this).attr('rel').split('|')[0]+"&makeDefault="+$(this).attr('rel').split('|')[1],
			dataType:'json',
			success	: function (){window.location.reload()}
		})
	});
</script>
