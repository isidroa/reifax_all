<?php 
	require_once('../resources/php/properties_conexion.php');
	require_once('../resources/template/template.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: <span class="greentext">
           	<span class="greentext underline">Rei</span><span class="bluetext underline">Fax Home</span></a>&gt; 
        <span class="fuchsiatext">Settings</span>
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
        </div>
        
        <!-- Center Content-->
        <div class="content">
            <!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(resources/banners/contract%20generator.jpg) no-repeat;">
                                        <div class="advertisingFeatureBlock">
                                            <h2 class="bluetext">Contract Generator</h2>
                                            <p class="bluetext">
                                                We will automatically fill any <span class="greentext">Real Estate Contract</span> or Form, including addendums and public information required.
                                            </p>
                                            <p class="bluetext">
                                                A <span class="greentext">PDF document</span> will be generated in less than ten seconds.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/banners/buyer%20list.jpg) no-repeat;">
                                        <div class="advertisingFeatureBlock">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/banners/mobile%20app.jpg) no-repeat;">
                                        <div class="advertisingFeatureBlock">
                                            &nbsp;
                                        </div>
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div>
            <!-- 3 Columns Content-->
            <div class="columns three">
                <div class="col">
                    <div class="panel">
                        <div class="center">  
                            <h4 class="title">RESOURCES</h4>
                            <ul>
                                <li><a href="#" title="ReiFax Real Estate Articles">Real Estate Articles</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Videos">Real Estate Videos</a></li>  
                                <li><a href="#" title="ReiFax Investing Ebooks &amp; Audios">Investing Ebooks &amp; Audios</a></li>  
                                <li><a href="#" title="ReiFax Investing Glossary">Investing Glossary</a></li>  
                                <li><a href="#" title="ReiFax Business Tools">Business Tools</a></li>
                                <li><a href="#" title="ReiFax Tax Appraisal Districts">Tax Appraisal Districts</a></li>
                                <li><a href="#" title="ReiFax Foreclosure Laws">Foreclosure Laws</a></li>
                                <li><a href="#" title="ReiFax Real Estate Clubs">Real Estate Clubs</a></li>
                                <li><a href="#" title="ReiFax Success Stories">Success Stories</a></li>
                                <li><a href="#" title="ReiFax Preferred Partners">Preferred Partners</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col">
                    <div class="panel">
                        <div class="center">  
                            <h4 class="title">ONLINE CATALOG</h4>
                            <ul>
                                <li><a href="#" title="ReiFax Real Estate Courses">Real Estate Courses</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Ebooks">Real Estate Ebooks</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Books">Real Estate Books</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Audios">Real Estate Audios</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Seminars">Real Estate Seminars</a></li>  
                                <li><a href="#" title="ReiFax Real Estate Games">Real Estate Games</a></li>  
                                <li><a href="#" title="ReiFax Investor Mentoring Program">Investor Mentoring Program</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col last">
                    <div class="panel">
                        <div class="center">  
                            <h4 class="title">UPCOMING EVENTS</h4>
                            <div id="events">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">&nbsp;</div> 
    </div>
  
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-home');
</script>