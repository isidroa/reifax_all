<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	require_once '../backoffice/php/mcripty.php';
	conectar('xima');
	$query=mysql_query('select cardholdername,cardname,cardnumber,expirationyear,expirationmonth,csvnumber,billingaddress,billingcity,billingstate,billingzip from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
	$cardtype=array( ''=>'Select...','MasterCard' => 'Master Card' , 'AmericanExpress' => 'American Express','Visa' => 'Visa','Discover' => 'Discover');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Credit Card</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Credit Card Information - Here you can change the credit card information</span>
                     <div class="note">(<img src="https://www.reifax.com/resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext mySettings">
					<h2>Credit Card Information</h2>
                    <form id="creditCard" action="../resources/php/funcionesMySettings.php">
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo $data['cardholdername']?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                    	<?php foreach ($cardtype AS $key => $value)
										{
											echo ($key==$data['cardname'])?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo  mcrypt_Fe7a0a89bd($data['cardnumber'] ) ?>" style="display:none" class="fieldRequired" id="cardNumber" type="text" name="cardnumber">
                                	<input value="<?php echo  mcrypt_Fe7a0a89bd($data['cardnumber'] ) ?>" class="fieldRequired" id="securecardnumber" type="password" name="securecardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$data['expirationmonth'])?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$data['expirationyear'])?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo  mcrypt_Fe7a0a89bd($data['csvnumber'] ) ?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> 
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                       </table>
                       	<div class="clear"></div>
						<h2>Billing Address</h2>
                       <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input value="<?php echo $data['billingaddress']?>" class="fieldRequired" type="text" name="Address">
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">City</label>
                                </td>
                                <td>
                                	<input value="<?php echo $data['billingcity']?>" class="fieldRequired" type="text" name="City">
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">State</label>
                                </td>
                                <td>
                                <select name="state">
								<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            while($aux=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($aux['name']==$data['billingstate'])?'<option selected value="'.$aux['name'].'">'.$aux['name'].'</option>':'<option value="'.$aux['name'].'">'.$aux['name'].'</option>';
											}
                                     ?>	
                                     </select>
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">ZIP Code</label>
                                </td>
                                <td>
                                	<input value="<?php echo $data['billingzip']?>" class="fieldRequired" type="text" name="zip">
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                     <div class="centertext">
                                	<a href="#" id="next" class="buttonblue bigButton">Save Change</a>
                     </div>
                                </td>
                           <td></td>
                            </tr>
                        </table>
                        <!--
                        campos hidden
                        //-->
                        <input type="hidden" value="updateCreditCard" name="option">
                     </form>
                     </div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="https://www.reifax.com/resources/img/ac.gif"> 
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 $(document).ready(function (){
	 /***/
	 $('#securecardnumber').bind('focus',function (){
		 $(this).css('display','none');
		 $('#cardNumber').show().focus();
		 }) 
	$('#cardNumber').bind('blur',function (){
		 $(this).css('display','none');
		 $('#securecardnumber').show().attr('value',$(this).val());
	})
	/*inicio de validaciones*/
	validationRegister();
	$('.numberPhone input').unbind('blur');
	$('#creditCard').bind('submit',funciones.proccessUpdate)
	$('#next').bind('click',function (){$('#creditCard').trigger('submit')});
	correcto=true;
});
</script>