<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	if(!isset($_COOKIE['datos_usr']['USERID']))
		echo "<script>document.location='../index.php';</script>";
	
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>
<style>
.panelMyProducts .buttongreen, .panelMyProducts .buttonblue {
    right: 30px;
}
</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">My Account</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">My Account - Please click on the option you want to use</span>
            	</div>
        	</div>
        </div>
		<!--Text container-->
		<div class="panel">
			<div class="center panelMyProducts">
				<div style="float:left">
					<a href="active.php"><img src="/img/activateaccount.png"></a>
				</div>
				<h2> Activate Account </h2>
				<a class="buttongreen bigbutton" href="active.php">Go activate account </a>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
		<div class="clear"> </div>
		
		<div class="panel">
			<div class="center panelMyProducts">
				<div style="float:left">
					<a href="cancelAccount.php"><img src="/img/cancelaccount.png"></a>
				</div>
				<h2> Cancel Account </h2>
				<a class="buttonblue  bigbutton buttonDefault" href="cancelAccount.php">Go cancel account </a>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
		<div class="clear"> </div>
		
		<div class="panel">
			<div class="center panelMyProducts">
				<div style="float:left">
					<a href="freezeAccount.php"><img src="/img/freezzeaccount.png"></a>
				</div>
				<h2> Freeze Account </h2>
				<a class="buttonblue  bigbutton buttonDefault" href="freezeAccount.php">Go freeze account </a>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
        <div class="clear"> </div>
		
		<!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script type="text/javascript">
	menuClick('menu-my-products');
	$('.buttonDefault').bind('click',function (){
		$.ajax({
			type	:'POST', 
			url		:'../resources/php/makeDefaultProducts.php',
			data	: "idproducto="+$(this).attr('rel').split('|')[0]+"&makeDefault="+$(this).attr('rel').split('|')[1],
			dataType:'json',
			success	: function (){window.location.reload()}
		})
	});
</script>