<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	$sql='SELECT * FROM xima.procode where userid='.$_COOKIE['datos_usr']['USERID'];
	$res=mysql_query($sql) or die ($sql.mysql_error());
	if(mysql_num_rows($res)>0){
		$tem=mysql_fetch_assoc($res);
		$procodeExp=$tem['procode'];
	}
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<style type="text/css">
.register h2{
	padding-top:10px;
	padding-bottom:0px;	
}
.register h4{    
	font-size: 14px;
    margin-left: 10px;
    padding: 3px;
}
</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Banners</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Banners - Choose the banners that best fit your affiliate marketing business</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext mySettings">
					<h2 style="color:#000; font-weight:bold;">
                    	Banner 1 
                    </h2>
                   		<h4>
                    		300x250 pixeles
                    	</h4>
                   
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img border="0" src="http://www.reifax.com/resources/img/banners/Ad1.png"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad1.png"/></a></div></textarea>
                    	
                    </div>
                    <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 2</h2>
                   		<h4>
                    		300x250 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img border="0" src="http://www.reifax.com/resources/img/banners/Ad1_1.png"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad1_1.png"/></a></div></textarea>
                    	
                    </div>
                    <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 3</h2>
                   		<h4>
                    		728x90 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img border="0" src="http://www.reifax.com/resources/img/banners/Ad2.png"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad2.png"/></a></div></textarea>
                    	
                    </div>
                    <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 4</h2>
                   		<h4>
                    		120x600 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img border="0" src="http://www.reifax.com/resources/img/banners/Ad3.png"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad3.png"/></a></div></textarea>
                    	
                    </div>
                    <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 5</h2>
                   		<h4>
                    		160x600 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img src="http://www.reifax.com/resources/img/banners/Ad4.png" border="0"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad4.png"/></a></div></textarea>
                    	
                    </div> <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 6</h2>
                   		<h4>
                    		125x125 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img src="http://www.reifax.com/resources/img/banners/Ad5.png" border="0"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad5.png"/></a></div></textarea>
                    	
                    </div> <div class="clear"></div>
                    <h2 style="color:#000; font-weight:bold;">Banner 7</h2>
                   		<h4>
                    		468x60 pixeles
                    	</h4>
                    <div align="center">
                    	<a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank">
                                <img src="http://www.reifax.com/resources/img/banners/Ad6.png" border="0"/>
                        </a>
                    </div>
                    <div align="center" class="centertext">
                    <p>
                    Copy/Paste this Code in the Body section of the HTML file you want to display the image
                    </p>
                    <textarea readonly="true" style="width:610px;"><div><a href="http://www.reifax.com/index.php?procode=<?php echo ($procodeExp)?$procodeExp:$_COOKIE['datos_usr']['USERID'] ?>" target="_blank"><img border="0" src="http://www.reifax.com/resources/img/banners/Ad6.png"/></a></div></textarea>
                    	
                    </div>
               <div class="clear"></div>
                    
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-my-partner');
 $(document).ready(function (){
	/*inicio de validaciones*/
	
	$('textarea').bind('focus',function (){
		$(this).select();
	}).bind('mouseup',function(e){
        e.preventDefault();
    })
});

</script>