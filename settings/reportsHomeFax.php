<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	// verifica si el usuario se encuantra frezzeado
	$sql='select idstatus from ximausrs where userid='.$_COOKIE['datos_usr']['USERID'];
	$query=mysql_query($sql);
	$resul=mysql_fetch_array($query);
	$status=$resul['idstatus'];
	if($status==6)
	{
		echo "<script>document.location='unfreezeAcount.php';</script>";
	}
	// verifica los prductos activos para el usuario
	$sql='select p.idproducto  from usr_productobase p, usr_cobros c where c.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.idproductobase=c.idproductobase AND idstatus=5;';
	$resul=mysql_query($sql);
	while ($status=mysql_fetch_array($resul))
	{
		$inactiveProduct[$status['idproducto']]=true;
	}
	if($inactiveProduct[1] || $inactiveProduct[2])
	{
		echo "<script>document.location='active.php';</script>";
	}
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3();?>
<style type="text/css">
.x-panel-body{
	border-color:#BBBBBB !important;
}
</style>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">My Reports</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">My Reports</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <div class="panel">
           <div class="center" id="gridReport">
           </div>
        </div>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script type="text/javascript">
	menuClick('menu-my-documents');
	var Record = new Ext.data.Record.create([ 
		{name: 'file', type: 'string'},  
		{name: 'county', type: 'string'},  
		{name: 'type', type: 'string'},  
		{name: 'date', type: 'date', dateFormat: 'Y-m-d'},
		{name: 'address', type: 'string'},
		{name: 'active', type: 'bool'},
		{name: 'url', type: 'string'}
	]);  
	var gridReader = new Ext.data.JsonReader({  
		root: 'data', 
   		totalProperty: 'total', 
		id: 'cad_id'},
		Record 
		);
	var dataProxy = new Ext.data.HttpProxy({  
		url: '../resources/php/funcionesMysettings.php',   // Servicio web  
		method: 'POST'                          // Método de envío  
	});  
	var dataStore = new Ext.data.Store({  
		id: 'reports',  
		proxy: dataProxy,  
		baseParams: {  
			option: "listReport",
			report: "all"
		},  
		reader: gridReader  
	});
var columnMode = new Ext.grid.ColumnModel(  
    [
       	new Ext.grid.RowNumberer(),
	{  
        header: 'Type Report',  
		sortable: true, 
        dataIndex: 'type',  
        width: 150  
    },{  
        header: 'Address',
		sortable: true, 
        dataIndex: 'address',  
        width: 340  
    },{  
        header: 'County',  
        dataIndex: 'county', 
		sortable: true,  
        width: 200  
    },{  
        header: 'Date',  
        dataIndex: 'date', 
		sortable: true,  
        width: 90,  
        renderer: Ext.util.Format.dateRenderer('Y/m/d')  
    },{  
        header: 'Status',  
        dataIndex: 'active',  
		sortable: true, 
        width: 80,  
        renderer: function (value, cell)
		{
			var str = '';  
			if (value) {    // Evalua si el valor campo  
				str='active';
			}  
			else {        // En caso el estado sea false  
				str='inactive';
			}  
			return str;   
		}  
    },{   
        dataIndex: 'url',  
        width: 30,  
        renderer: function (value, cell)
		{
			return '<a href="'+value+'" target="_blank" class="botonDownloadGrid"></a>'; 
		}  
    }
	]  
);
var reports = new Ext.grid.GridPanel({  
    id: 'list_report',
    store: dataStore,  
    cm: columnMode,
        viewConfig: {
            forceFit:true
        },
    enableColLock:false,
	layout: 'fit',
    height           : 425,
	applyTo:'gridReport',
    selModel: new Ext.grid.RowSelectionModel({singleSelect:false})  
});
dataStore.load();
</script>