<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	// verifica si el usuario se encuantra frezzeado
	$sql='select idstatus from ximausrs where userid='.$_COOKIE['datos_usr']['USERID'];
	$query=mysql_query($sql);
	$resul=mysql_fetch_array($query);
	$status=$resul['idstatus'];
	if($status==6)
	{
		echo "<script>document.location='unfreezeAcount.php';</script>";
	}
	// verifica los prductos activos para el usuario
	$sql='select p.idproducto  from usr_productobase p, usr_cobros c where c.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.idproductobase=c.idproductobase AND idstatus=5;';
	$resul=mysql_query($sql);
	while ($status=mysql_fetch_array($resul))
	{
		$inactiveProduct[$status['idproducto']]=true;
	}
	if($inactiveProduct[1] || $inactiveProduct[2])
	{
		echo "<script>document.location='active.php';</script>";
	}
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3();?>
<script type="text/javascript" src="/resources/js/extjs3.2/ux/FileUploadField.js"></script>
<link href="/resources/js/extjs3.2/ux/fileuploadfield.css" rel="stylesheet" type="text/css" media="screen">
<style type="text/css">
.x-panel-body{
	border-color:#BBBBBB !important;
}
	.msgUser
	{
		position:relative;
		padding-bottom:12px;
		border-bottom:solid 1px #567DA9;
	    width: 450px;
	}
	.msgUser .title
	{
		font-family:Arial, Helvetica, sans-serif;
		font-size:14px;
		font-weight:bold;
		color:#4E6D85;
		text-decoration:underline;
		margin: 3px 4px;
	}
	.msgUser .msgContent
	{
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;
		font-weight:bold;
		color:#000;
		text-decoration:none;
		margin: 3px 8px;
	}
	.msgUser .dateMsg
	{
		position: absolute;
		right: 10px;
		bottom:-2px;
	}
	.imgUser img {
		float:left;
		background:#CCF8FD;
		border:solid 1px #9ED5FC;
		margin-right:20px;
		padding:5px;
		width:110px;
		height:110px;
	}
	.imgUser img:nth-child
	{
		margin-right:0px;
	}
	.imgUser img:hover {
		background:#6CB9DD;
		border:solid 1px #128AB8;
	}
	.red-row{
		background:#FE5656;
	}
	.red-row:hover{
		background:#F14B4B;
	}
</style>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">My tickets</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">My tickets - tickets</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <div class="panel">
           <div class="center" id="gridReport">
           </div>
        </div>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    <?php ReiFaxFooter();?>

</div>
</body>

</html>
<script type="text/javascript">
Ext.onReady(function ()
{
	menuClick('menu-my-documents');
	var Record = new Ext.data.Record.create([ 
			{name: 'idcs', type: 'int'},
			{name: 'useridclient', type: 'int'}, 
			{name: 'cliente', type: 'string'},    
			{name: 'useridprogrammer', type: 'int'}, 
			{name: 'programador', type: 'string'},    
			{name: 'useridcustomer', type: 'int'},
			{name: 'mobile', type: 'string'},
			{name: 'id_product', type: 'int'},
			{name: 'productName', type: 'string'},
			{name: 'email', type: 'string'},
			{name: 'phone', type: 'string'},
			{name: 'customer', type: 'string'},
			{name: 'browser', type: 'string'},
			{name: 'os', type: 'string'},
			{name: 'state' , type: 'string'},
			{name: 'idcounty' , type: 'string'},
			{name: 'menutab' , type: 'string'},
			{name: 'submenutab' , type: 'string'},
			{name: 'protype' , type: 'string'},
			{name: 'errortype' , type: 'string'},
			{name: 'search' , type: 'string'},
			{name: 'errordescription' , type: 'string'},
			{name: 'status' , type: 'string'},
			{name: 'datel', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'dated', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'datef', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'datec', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'errortype2', type: 'string'},
			{name: 'soldescription' , type: 'string'},
			{name: 'msg_cliente' , type: 'int'},
			{name: 'msg_system' , type: 'int'},
			{name: 'ticket' , type: 'string'},
			{name: 'source' , type: 'string'}
	]);  
	var gridReader = new Ext.data.JsonReader({  
		root: 'data', 
   		totalProperty: 'total', 
		id: 'cad_id'},
		Record 
		);
	var dataProxy = new Ext.data.HttpProxy({  
		url: '../resources/php/funcionesMyTickets.php',   // Servicio web  
		method: 'POST'                          // Método de envío  
	});  
	var dataStore = new Ext.data.Store({  
		id: 'reports',  
		proxy: dataProxy,  
		baseParams: {  
			option: "listTickets"
		},  
		reader: gridReader  
	});
	setInterval(function (){
		dataStore.reload();
	},2000);
var columnMode = new Ext.grid.ColumnModel(  
    [
       	new Ext.grid.RowNumberer(),
	{  
        header: 'Ticket #',  
        dataIndex: 'ticket', 
		sortable: true,  
        width: 80  
    },{  
        header: 'Product',  
		sortable: true, 
        dataIndex: 'productName',  
        width: 100  
    },{   
        header: 'Error', 
        dataIndex: 'errordescription',  
        width: 250
    },{   
        header: 'Date', 
        dataIndex: 'datel',  
        width: 100,  
        renderer: Ext.util.Format.dateRenderer('Y/m/d'),
		
    },{  
        header: 'Status',  
		sortable: true, 
        dataIndex: 'status',  
        width: 80  
    }
	]  
);
var reports = new Ext.grid.GridPanel({  
    id: 'gridTickets',
    store: dataStore,  
    cm: columnMode,
        viewConfig: {
            forceFit:true
        },
    enableColLock:false,
	layout: 'fit',
	view: new Ext.grid.GridView({ 
				forceFit: true, 
				getRowClass : function (row, index) { 
						var cls = ''; 
						var col = row.data; 
						if(col.msg_system!='')
						{
							cls = 'red-row'
						}
						
						 return cls; 
					  } 
		   }),
    height : 425,
	/*
	bbar:new Ext.Toolbar({
		width: '100%',
		items: [
				'->', // same as {xtype: 'tbfill'}, // Ext.Toolbar.Fill
				
			]
		}),*/
	tbar:new Ext.Toolbar({
		width: '100%',
		items: [{
					xtype: 'button', // default for Toolbars, same as 'tbbutton'
					text: 'New Ticket',
					scale :'large',
					iconCls :'x-icon-add-large',
					handler: function (){
							var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=800, top=85, left=140";
							window.open('../register/recordTicket.php',"",opciones);
							//window.open(,'New Ticket');
						}
				},'|',{
								xtype	: 'combo',
								id:'comboStatus',
								fieldLabel:'Status'
								,displayField:'name'
								,value :''
								,name:'status'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['','All'],
										['not in ("Close")', 'Active'],
										['in ("Close")', 'Inactive']
									]
								})
								,width: 70
								,triggerAction:'all'
								,mode:'local'
							},
				{
					xtype: 'button', // default for Toolbars, same as 'tbbutton'
					text: 'Filter',
					iconCls: 'x-icon-filter',
					handler: function (){
							
						dataStore.reload({params:{
								status:Ext.getCmp('comboStatus').getValue()
							}});
					}
				},'|',{
					xtype: 'button', // default for Toolbars, same as 'tbbutton'
					text: 'Help!',
					scale :'large',
					iconCls :'x-icon-help-large',
					handler: function (){
							var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=800, top=85, left=140";
							window.open('../resources/pdf/Tickets-Help.pdf',"",opciones);
						}
				}
			]
		}),
	applyTo:'gridReport',
    selModel: new Ext.grid.RowSelectionModel({singleSelect:false})  ,
			listeners: {
			//	'rowcontextmenu' : monitorig.menuText,
				'rowdblclick' : showDeatils
			}
});

function showDeatils (grid, rowIndex, columnIndex, e)
{
	var record = grid.getStore().getAt(rowIndex);
		
		
		var storeMsg = new Ext.data.JsonStore({ 
			id : 'store'+record.get('idcs'),
			proxy :  new Ext.data.HttpProxy({  
					url: '../resources/php/funcionesMyTickets.php',   // Servicio web  
					method: 'POST'                          // Método de envío  
			}),  
			baseParams: {
				option	:'listarMsg',
				id	: record.get('idcs')
			},
			root: 'data',
			fields: [
				{ name: 'user', type: 'string' },
				{ name: 'msg', type: 'string' },
				{ name: 'dateMsg', type: 'date', dateFormat: 'Y-m-d H:i:s'}
			]
		});

		
		storeMsg.load();
		
		var templeteMsg=new Ext.XTemplate(
			'<tpl for=".">',
				'<div class="msgUser"> <p class="title">{user} Says:</p>',
				'<p class="msgContent">{msg}</p>',
				'<div class="dateMsg">{dateMsg}</div></div>',
			'</tpl>',
			'<div class="x-clear"></div>'
		);
		var Msg = new Ext.DataView({
			id:'view'+record.get('idcs'),
			autoScroll: true,
			store: storeMsg,
			border :false,
			tpl: templeteMsg,
			autoHeight: false,
			height: 242,
			multiSelect: false,
			overClass: 'x-view-over',
			itemSelector: 'div.thumb-wrap',
			emptyText: 'No Msg to display'
			
		});		
		var storeImg = new Ext.data.JsonStore({ 
			id : 'storeImg'+record.get('idcs'),
			proxy :  new Ext.data.HttpProxy({  
					url: '../resources/php/funcionesMyTickets.php',   // Servicio web  
					method: 'POST'                          // Método de envío  
			}),  
			baseParams: {
				option	:'listarImg',
				id	: record.get('idcs')
			},
			root: 'data',
			fields: [
				{ name: 'img', type: 'string' }
			]
		});

		storeImg.load();
		var templeteImg=new Ext.XTemplate(
			'<tpl for=".">',
				'<div class="imgUser">',
				'<img src="{img}">',
				'</div>',
			'</tpl>',
			'<div class="x-clear"></div>'
		);
		//setInterval(function (){storeMsg.reload()},2000);
		var Img = new Ext.DataView({
			id:'viewImg'+record.get('idcs'),
			autoScroll: true,
			store: storeImg,
			tpl: templeteImg,
			autoHeight: false,
			height: 300,
			width:'100%',
			multiSelect: true,
			overClass: 'x-view-over',
			itemSelector: 'div.imgUser',
			emptyText: 'No Images to display'
		});
		
		Img.on('dblclick', function(dview, index, node, e) {
			window.open(dview.getSelectedRecords()[0].data.img);
		});
		
		var form = new Ext.form.FormPanel({
					fileUpload: true,
					frame : true,
					border :false,
					id:'form'+record.get('idcs'),
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'column',
					items : [
						{
							xtype: 'fieldset',
							padding:10,
							style:{
								background : '#FFF',
								font: '14px tahoma,arial,helvetica,sans-serif'
								},
							title: 'Staff',
							layout:'column',
							columnWidth: 1,
							items: [
								{
									layout: 'form',
									columnWidth: .50,
									items:[{
										xtype	:'label',
										html	: '<b>Attend: </b>'+((record.get('customer')=='')?'unassigned':record.get('customer'))
									}]
								}
							]
						},{
							xtype: 'fieldset',
							padding:10,
							style:{
								background : '#FFF',
								font: '13px tahoma,arial,helvetica,sans-serif'
								},
							title: 'Issues Infomation',
							layout:'column',
							columnWidth: 1,
							items: [
								{
									layout: 'form',
									columnWidth: .33,
									items:[{
										xtype	:'label',
										html	: '<b>System Operative: </b>'+record.get('os') 
									}]
								},
								{
									layout: 'form',
									columnWidth: .66,
									items:[
									{
										xtype	:'label',
										html	: '<b>Browser: </b>'+record.get('browser') 
									}]
								},
								{
									layout: 'form',
									columnWidth: .33,
									items:[{
										xtype	:'label',
										html	: '<b>Issues: </b>'+record.get('errortype') 
									}]
								},
								{
									layout: 'form',
									columnWidth: .33,
									items:[{
										xtype	:'label',
										html	: '<b>Issues About: </b>'+record.get('errortype2') 
									}]
								},
								{
									layout: 'form',
									columnWidth: 1,
									items:[{
										xtype	:'label',
										html	: '<b>Issues Description: </b>'+record.get('errordescription') 
									}]
								}]
						}/*,{
							xtype: 'fieldset',
							padding:10,
							style:{
								background : '#FFF',
								font: '13px tahoma,arial,helvetica,sans-serif'
								},
							title: 'Solution',
							layout:'column',
							columnWidth: 1,
							items: [
								{
									xtype	:'label',
									html	: (record.get('soldescription')=='')?'In process':'<b>Details of the solution: </b>'+record.get('soldescription')
								}]
						}*/,{
							xtype: 'fieldset',
							padding:10,
							style:{
								background : '#FFF',
								font: '12px tahoma,arial,helvetica,sans-serif'
								},
							title: 'Communication',
							layout:'column',
							columnWidth: 0.6,
							items: [Msg,
							((record.get('status')=='Close')? {} :
							new Ext.Panel({
									frame : true,
									layout: 'form',
									scope	:this,
									width	:'100%',
									border :false,
									items:
									[
										{
											xtype	: 'textarea',
											height	: 30,
											width	: 450,
											hideLabel	: true,
											id	: 'msg'+record.get('idcs'),
											emptyText	: 'New Msg',
											name	:'Msg'
										}
									],
									buttons: 
									[
									{
										text : 'Send Msg',
										scope	:this,
										handler: function ()
										{
										var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
										myMask.show();
										var form=Ext.getCmp('msg'+record.get('idcs')).getValue();
										Ext.Ajax.request({
											url: '../resources/php/funcionesMyTickets.php',
											scope : this,
											params :{
												option : 'newMsg',
												Msg : form,
												id	:record.get('idcs')
											},
											success	: function (a)
												{
													var result=Ext.util.JSON.decode(a.responseText);
													myMask.hide();
													Ext.getCmp('view'+result.id).getStore().reload();
													Ext.getCmp('gridTickets').getStore().reload();
													$('#botonTickets').removeClass('buttonred').addClass('buttonblue');
												}
											})
										}
									}
								]
							})
							)]
						},{
							xtype: 'fieldset',
							padding:10,
							style:{
								background : '#FFF',
								font: '12px tahoma,arial,helvetica,sans-serif'
								},
							bbar:[
							{
									xtype		: 'fileuploadfield',
									buttonOnly	: true,
									name		: record.get('idcs'),
									buttonText	: '',
									buttonCfg	: {
										iconCls: 'x-icon-add'
									},
									scope	:this,
									listeners: {
										'fileselected': function(fb, v){
											var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
											myMask.show();
											var fd = new FormData();
											fd.append("fileToUpload",fb.fileInput.dom.files[0]);
											fd.append("ticket", fb.name);
											fd.append("option", 'addImages');
										
											$.ajax({
											   url: "../resources/php/funcionesMyTickets.php",
											   type: "POST",
											   data: fd,
											   processData: false,
											   contentType: false,
											   success: function(response) {
												   storeImg.load();
												   myMask.hide();
											   },
											   error: function(jqXHR, textStatus, errorMessage) {
											   }
											});
											
										}
									}
							
							}
							],
							title: 'Attached',
							layout:'column',
							columnWidth: 0.4,
							items: [Img]
						}
					]
		});
						
		
		
		win= new Ext.Window({
					layout: 'fit',
					id:'win'+record.get('idcs'), 
					title: 'Ticket #'+record.get('ticket'), 
					closable: true,
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 870,
					height: 600,
					items	:[
						form
					]
				});
		win.show();



}

dataStore.load();
});
</script>