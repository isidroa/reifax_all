<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	include("../properties_getprecio.php");
	$query=mysql_query('select cardholdername,cardname,cardnumber,expirationyear,expirationmonth,csvnumber,billingaddress,billingcountry,billingcity,billingstate,billingzip from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../index.php">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Unfreeze Acount</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Unfreeze Acount - Here you can unfreeze your acount</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext mySettings">
					<table width="405px" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
						<tr align="left" >
							<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">Your account is currently on Freeze </td>                
						</tr>
						<tr align="left" >
							<td colspan="2" style="color:#000; font-weight:bold; ">
							In order to reactivate your account please click on "Unfreeze Account" and once you have clicked it you<br>
							will be billed $<?php echo number_format(getpriceuser($_COOKIE['datos_usr']['USERID'],0,'yes'),2,'.',',')?>
							which is the your product price also you will be changing your billing date to <?php echo date('d')?> of each month or year.
							
							</td>                
						</tr>
						<tr align="left" >
							<td colspan="2" style=" font-weight:bold; font-size:14px">To cancel or suspend your account click on the button "Cancel Account"</td>                
						</tr>
					</table>
					<!--<h2 style="color:#000; font-weight:bold;">Unfreeze Acount</h2>-->
                    <form id="unfreezeAcount" action="../resources/php/funcionesMySettings.php">
                       <div class="centertext">
						<br>
                       		<a href="#" id="unfreezeProcess" class="buttonblue">Unfreeze Acount</a>
                       		<a href="cancelAccount.php" id="unfreezeProcess" class="buttonred">Cancel Acount</a>
 						<br>
                      </div>
                        <input type="hidden" value="activeuserinactive" name="option">
                        <input type="hidden" value="true" name="unfreeze">
                     </form>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 $(document).ready(function (){
	 
	 $('#securecardnumber').bind('focus',function (){
		 $(this).css('display','none');
		 $('#cardNumber').show().focus();
		 }) 
	$('#cardNumber').bind('blur',function (){
		 $(this).css('display','none');
		 $('#securecardnumber').show().attr('value',$(this).val());
	})
	 
	 
	/*inicio de validaciones*/
	//validationRegister();
	$('#unfreezeAcount').bind('submit',funciones.proccessUpdate).bind('callback',function (){
		window.location='myProducts.php';
		});
	$('#unfreezeProcess').bind('click',function (){$('#unfreezeAcount').trigger('submit')});
});

</script>