<?php 
	require_once('C:/inetpub/wwwroot/resources/php/properties_conexion.php');
	conectar('xima');
	require_once('C:/inetpub/wwwroot/resources/template/template.php');
	
	$query=mysql_query('select NAME,SURNAME,STATE,CITY,ADDRESS,HOMETELEPHONE,MOBILETELEPHONE,EMAIL,pseudonimo,questionuser,answeruser from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Personal Data</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Personal Data - Here you can change your personal information</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext">
					<h2 style="color:#000; font-weight:bold;">Personal Data</h2>
                    <form action="../resources/php/funcionesMySettings.php" id="updatePersonalData">
                    <table>
                            <tr>
                            	<td>
                                	<label class="">Account Executive:</label>
                                </td>
                                <td>
                                	<label><?php echo $_COOKIE['datos_usr']['USERID']; ?></label>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo $data['NAME'] ?>" type="text" id="firstName" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo $data['SURNAME'] ?>" type="text" id="lastName" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo $data['pseudonimo'] ?>" id="nickName" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                            <?php
									securityQuestion($data['questionuser'],$data['answeruser']);
							?>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                    <input value="<?php echo $data['ADDRESS'] ?>" type="text" class="fieldRequired" name="txtAddress">
                                </td>
                                <td>
                                	<span class="errorRegister" id=""></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">State</label>
                                </td>
                                <td>
                                	<select name="State"  id="State" >
										<?php
                                            $resultado=mysql_query('SELECT * FROM state;');
                                            while($aux=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($aux['name']==$data['STATE'])?'<option value="'.$aux['name'].'" selected>'.$aux['name'].'</option>':'<option value="'.$aux['name'].'">'.$aux['name'].'</option>';
                                            }
                                        ?></select>
                                </td>
                                <td>
                                	<span class="noteRegister"></span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">City</label>
                                </td>
                                <td>
                                    <input value="<?php echo $data['CITY'] ?>" type="text" class="fieldRequired" name="txtCity">
                                </td>
                                <td>
                                	<span class="errorRegister" id=""></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
                                    <!--
									<input type="text" value="<?php echo formantPhone($data['HOMETELEPHONE'])->cod; ?>" class="short" name="txtPhoneCod"> <label>-</label> 
                                    <input class="fieldRequired" value="<?php echo formantPhone($data['HOMETELEPHONE'])->num; ?>"  type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo formantPhone($data['HOMETELEPHONE'])->ext; ?>" class="short" name="txtPhoneExt">
									-->
                                     <input class="fieldRequired" value="<?php echo $data['HOMETELEPHONE']; ?>"  type="text" name="txtPhoneNumber" id="txtPhoneNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                               </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
                                    <!--
									<input type="text" value="<?php echo formantPhone($data['MOBILETELEPHONE'])->cod; ?>" class="short" name="MobileNumberCod"> <label>-</label> 
                                    <input type="text" value="<?php echo formantPhone($data['MOBILETELEPHONE'])->num; ?>" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo formantPhone($data['MOBILETELEPHONE'])->ext; ?>" class="short"  name="MobileNumberExt">
                                    -->
									<input type="text" value="<?php echo $data['MOBILETELEPHONE']; ?>" name="MobileNumber" id="MobileNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                               </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo $data['EMAIL'] ?>" type="text" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote"></span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                     <div class="centertext">
                                	<a href="#" id="formProcessing" class="buttonblue bigButton">Save Changes</a>
                     </div>
                                </td>
                           <td></td>
                            </tr>
                     </table>
                     <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="option"  value="saveDataPer">
                     <input readonly type="hidden" name="txtPhone" id="txtPhone"  value="">
                     <input readonly type="hidden" name="txtMobil" id="txtMobil"  value="">
                     </form>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif">  
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-account');
 email=true;
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();

 	/***
	validaciones de campos requeridos
	************/
	$('#formProcessing').bind('click',function (){
		var correcto=true;
		$('.fieldRequired').each(function (){
				$(this).trigger('blur');
			})
		if(correcto && email)
		{
			$('#updatePersonalData').submit();
		}
		else
		{
			//$(this).removeClass('buttongreen').html('Required fields are empty').addClass('buttonred')
			return false; 
		} 
		})
	$('#updatePersonalData').bind('submit',function ()
	{
		//$("#txtPhone").attr('value',$('.numberPhone:eq(0) input:eq(0)').val()+'-'+$('.numberPhone:eq(0) input:eq(1)').val().substring(-1,3)+'-'+$('.numberPhone:eq(0) input:eq(1)').val().substring(3)+'-'+$('.numberPhone:eq(0) input:eq(2)').val());
		//$("#txtMobil").attr('value',$('.numberPhone:eq(1) input:eq(0)').val()+'-'+$('.numberPhone:eq(1) input:eq(1)').val().substring(-1,3)+'-'+$('.numberPhone:eq(1) input:eq(1)').val().substring(3)+'-'+$('.numberPhone:eq(1) input:eq(2)').val());
		$("#txtPhone").attr('value',$('#txtPhoneNumber').val());
		$("#txtMobil").attr('value',$('#MobileNumber').val());
		return false;
	})
	$('#updatePersonalData').bind('submit',funciones.proccessUpdate);
	
});
</script>