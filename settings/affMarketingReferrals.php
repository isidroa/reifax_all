<?php 
	require_once('../resources/php/properties_conexion.php');
	require_once('../resources/template/template.php');
	conectar('xima');
	$query=mysql_query('select NAME,SURNAME,STATE,CITY,ADDRESS,HOMETELEPHONE,MOBILETELEPHONE,EMAIL,pseudonimo from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
	$data=mysql_fetch_array($query);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#">
           	<span class="bluetext underline">My Settings</span></a> &gt; 
        	<span class="fuchsiatext">Affiliate Referrals</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Affiliate Referrals - Here you can review your affiliate referrals</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div >
					<!-- <h2 style="color:#000; font-weight:bold;">Personal Data</h2> -->
                    <table>
						<tr>
							<td>
								<div id="gridPanelAff" style="padding-top:2px; width: 800px; "></div>
							</td>
						</tr>
                    </table>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>

</body>

</html>
<script language="javascript">
 menuClick('menu-my-partner');
</script>
<script language="javascript">
Ext.BLANK_IMAGE_URL='../resources/js/extjs3/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: '../resources/php/funcionesMySettings.php',
		baseParams  :{
					start:0, limit:50, option:'affMarketingReferrals',pafflevel:'1'
					,userid:'<?php echo $_COOKIE['datos_usr']['USERID'] ?>'
					,name:'<?php echo $_COOKIE['datos_usr']['NAME'] ?>'
					,surname:'<?php echo $_COOKIE['datos_usr']['SURNAME'] ?>'},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status' 
			,'email'
			,'hometelephone'
			,'mobiletelephone'
			,'posi'
			,'father'
		]
	});
///////////FIN Cargas de data dinamica///////////////

//////////////////Manejo de Eventos//////////////////////
	function searchAffMarketing(){
		store.setBaseParam('pafflevel',Ext.getCmp("afflevel").getValue());
		store.load();
	}
	
//////////////////FIN Manejo de Eventos//////////////////////

////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.form.FormPanel({
		//width: 700,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			//labelWidth: 120,
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;<b>Level</b>',
			items:[{
				xtype:'combo',
				width:150,
				id:'afflevel',
				name:'afflevel',
				store: new Ext.data.SimpleStore({
					fields: ['id','text'],
					data : [
						['1','Affiliate Level 1'],['2','Affiliate Level 2']
					]
				}),
				editable: false,
				valueField: 'id',
				displayField: 'text',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: 1,
				listeners: {
					'select': searchAffMarketing
				}
			},{
				width:150,
				xtype:'label',
				id: 'empty',
				text: ' '
			},{
				width:150,
				xtype:'label',
				id: 'empty',
				text: ' '
			}]
		}]
	});
	
////////////////FIN barra de pagineo//////////////////////

///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			// new Ext.grid.RowNumberer()
			{header: " ", width:50, align: 'center', sortable: true, dataIndex: 'posi'}
			,{header: 'Name', width: 130, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: 'LastName', width: 130, sortable: true, align: 'left', dataIndex: 'surname'}
			,{header: 'Status', width: 80, sortable: true, align: 'left', dataIndex: 'status'}
			,{header: 'Email', width: 200, sortable: true, align: 'left', dataIndex: 'email'}
			,{header: 'Phone 1', width: 90, sortable: true, align: 'left', dataIndex: 'hometelephone'}
			,{header: 'Phone 2', width: 90, sortable: true, align: 'left', dataIndex: 'mobiletelephone'}
			,{header: 'Referrer', width: 150, sortable: true, align: 'left', dataIndex: 'father'}
			
			/*,{header: "Date", width: 80, align: 'left', sortable: true, dataIndex: 'Date'}
			,{header: "Description", width: 260, align: 'left', sortable: true, dataIndex: 'Operation'}			
			//,{header: 'User ID', width: 60, sortable: true, align: 'center', dataIndex: 'Aff_Userid'}
			,{header: 'Customer Payment ', width: 120, sortable: true, align: 'right', dataIndex: 'payamount',
				renderer: function(v, params, record){
					if(record.data.payamount!='')
						return Ext.util.Format.usMoney(record.data.payamount);
				}}
			,{header: 'Comm. Amount', width: 100, sortable: true, align: 'right', dataIndex: 'Amount',
				renderer: function(v, params, record){
					if(record.data.Amount!='')
						return Ext.util.Format.usMoney(record.data.Amount);
				}}*/
		],			
		height:430,
		width: 950,
		frame:false,
		loadMask:true,
		border: false,
		tbar: panelpag 
	});
	var mypanel = new Ext.Panel({
		renderTo: 'gridPanelAff',
        frame:true,
		height:500,
		width: 975,		
        title: '<?php echo $_COOKIE['datos_usr']['NAME']." ".$_COOKIE['datos_usr']['SURNAME']." (".$_COOKIE['datos_usr']['USERID'].")";?>',
        bodyStyle:'padding:5px 5px 0',
        items: [ grid ]
	});
		
/////////////////FIN Grid////////////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
</script>

