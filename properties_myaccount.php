<?php include("templates/properties_template.php");?>
<html>
<head>
<?php tagHeadHeader2()?>
<script type="text/javascript" src="includes/properties_generator.js"></script>
<script type="text/javascript" src="includes/properties_myaccount.js"></script>
<script>
function expandir(fila,imagen)
{
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="img/up.png";
		document.getElementById(imagen).alt="Ocultar";
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="img/down.png";
		document.getElementById(imagen).alt="Mostrar";
	}

}
</script>
</head>
<body >

<div align="center">
	<div id="body_central">
        <div id="layout_center" align="center">
			<?php 	
				if($_COOKIE['datos_usr']['status']<>'freeze' && $_COOKIE['datos_usr']['status']<>'freezeinac' && $_COOKIE['datos_usr']['status']<>'inactive')
					topLoginHeader();
				else 
					topLoginHeader2();
			?>
            <div align="center" style="width:97%; margin:auto;">
                <div id="tabs"></div>
           	</div>
        </div>
        
        <div id="layout_top"><div id="T1" style="height:10px; vertical-align:middle;"></div></div>
        
        <div id="layout_bottom"><div id="B1" style="height:110px; vertical-align:middle;"></div></div>
        
        <div id="layout_left"></div>
        
        <div id="layout_right"></div>
	</div>
</div>
<?php googleanalytics()?>
<script>
<?php 	if(isset($_COOKIE['datos_usr']['USERID'])){ 
			echo "var user_loged=true;"; 
			echo "var user_name_menu='".$_COOKIE['datos_usr']['NAME'].' '.$_COOKIE['datos_usr']['SURNAME']."';";
			echo "var useridlogin='".$_COOKIE['datos_usr']['USERID']."';";
		}else{ 
			echo "var user_loged=false;";
			echo "var user_name_menu='';";
			echo "var useridlogin='';";
		}
?>
var tabs=viewport=null; 
Ext.QuickTips.init();

var system_width=document.body.offsetWidth;
if(system_width<1000)
	system_width=660;
if(system_width>1000)
	system_width=980;

Ext.onReady(function(){

	
	var tb = new Ext.Toolbar({
		id:'menu_search',
		width: 660,
		items: [
<?php
	if($_COOKIE['datos_usr']['status']<>'freeze' && $_COOKIE['datos_usr']['status']<>'freezeinac' && $_COOKIE['datos_usr']['status']<>'inactive')
	{ 
?>
			{
				text: ' Personal Settings ',
				enableToggle: true,
				pressed: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/personalsettings.php'});
				}
			},{
				text: ' Banners ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/banners.php'});
				}
/*			},{
				text: ' History ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: '../../../../../myaccount_tabs/history.php'});
				}
			},{
				text: ' Counties ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: '../../../../../myaccount_tabs/counties.php'});
				}
*/
			},{
				text: ' Cancel Account ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/cancelaccount.php'});
				}
			},{
				text: ' Freeze Account ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/freezeaccount.php'});
				}
			}
<?php 
	}
	else//ESTA FREZZE EL USUARIO
	{
		if($_COOKIE['datos_usr']['status']=='freeze' || $_COOKIE['datos_usr']['status']=='freezeinac')
		{ 
?>	
			{
				text: ' Unfreeze Account ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/unfreezeaccount.php'});
				}
			}
<?php
		}
		if($_COOKIE['datos_usr']['status']=='inactive')
		{ 
?>	
			{
				text: ' Account ',
				enableToggle: true,
				toggleGroup: 'searchtoogle',
				handler: function(){
					var tab=tabs.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({url: 'myaccount_tabs/activeuserinactive.php'});
				}
			}
<?php
		}
		
	}
?>			
		],
		autoShow: true
	});

	tabs = new Ext.TabPanel({
        renderTo: 'tabs',
        activeTab: 0,
		height: 2350,
		width: 660,
        plain:true,
		enableTabScroll:false,
        defaults:{	autoScroll: false
				},
        items:[
<?php
	if($_COOKIE['datos_usr']['status']<>'freeze' && $_COOKIE['datos_usr']['status']<>'freezeinac' && $_COOKIE['datos_usr']['status']<>'inactive')
	{ 
?>
			  {
				title: ' My Account ',
				id: 'myaccountTab',
				tbar: tb,
				width: 660,
				autoLoad: {url: 'myaccount_tabs/personalsettings.php', scripts: true}
			  }  
<?php 
	}
	else//ESTA FREZZE EL USUARIO
	{
		if($_COOKIE['datos_usr']['status']=='freeze' || $_COOKIE['datos_usr']['status']=='freezeinac')
		{ 
?>	
			  {
				title: ' Account Activation ',
				id: 'myaccountTab',
				tbar: tb,
				width: 660,
				autoLoad: {url: 'myaccount_tabs/unfreezeaccount.php', scripts: true}
			  }  
<?php
		}
		if($_COOKIE['datos_usr']['status']=='inactive')
		{
?>		
			  {
				title: ' Account Activation ',
				id: 'myaccountTab',
				tbar: tb,
				width: 660,
				autoLoad: {url: 'myaccount_tabs/activeuserinactive.php', scripts: true}
			  }  
<?php
		}
	}
?>			
        ]
    });
	
	//alert(document.width+' '+document.body.offsetWidth);
	var ancho=document.body.offsetWidth;
	if(ancho>1260) ancho=1260;
	
	 viewport = new Ext.Panel({
		  renderTo: 'body_central',
		  id: 'viewport',
		  layout:'border',
		  monitorResize: true,
		  hideBorders: true,
		  width: ancho,
		  height: 2350,
		  items:[
			{
				region: 'south',
				id: 'south_panel',
				layout: 'hbox',
				height: 120,
				minSize: 120,
				maxSize: 120,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_bottom',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'east',
				id: 'east_panel',
				layout: 'vbox',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_right',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'west',
				id: 'west_panel',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_left',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'center',
				id: 'center_panel',
				layout: 'hbox',
				width: 'auto',
				layoutConfig:{align:'middle'},
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_center'
			}
		  ]
	});
	
	if(document.body.offsetWidth < 1200){
		 var west = Ext.getCmp('west_panel');
		 west.collapse();
	}
	
	if(user_loged) login_menu(user_name_menu,useridlogin); else logout_menu();
});
</script>
</body>
</html>