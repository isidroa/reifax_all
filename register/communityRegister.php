<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	header('Location: ../store/');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Community Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Community Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext">
					<h2 style="color:#000; font-weight:bold;">Create an Account</h2>
                    <form action="newUserCommunity.php" id="comunityRegister">
                    <table>
                            <tr>
                            	<td>
                                	<label class="required">I am a (an):</label>
                                </td>
                                <td>
                                	<select  class="fieldRequired" id="usertype" name="usertype">
                                    <option></option>
									<?php
                                        conectar('xima');
                                            $sql="SELECT * FROM usertype where `show`=1 order by orden;";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo '<option value="'.$data['idusertype'].'">'.$data['usertype'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="fieldLicense" style="display:none">
                            	<td>
                                	<label class="required">License #</label>
                                </td>
                                <td>
                                	<input type="text"id="txtBroker" name="txtBroker">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="questionCompany" style="display:none">
                            	<td colspan="2">
                                	<label>Is your company registered width us?</label>
                                	<input type="radio" name="companyRegister" value="yes" class="radioButton"> <label>Yes</label>
                                    <input type="radio" name="companyRegister" value="no" class="radioButton" checked> <label> No</label>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="divOfficeNumber" style="display:none">
                            	<td>
                                	<label class="required">Office License #</label>
                                </td>
                                <td>
                                	<input type="text" id="Office" name="txtOffice">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" id="password" name="txtPassword">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 8 characters and contain a letter and a number</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
                                    <input type="text" name="txtPhoneCod" class="short"> <label>-</label> 
                                    <input class="fieldRequired" type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input type="text" name="txtPhoneExt" class="short">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
                                    <input type="text" name="MobileNumberCod" class="short"> <label>-</label> 
                                    <input type="text" name="MobileNumber" > <label>Ext</label>
                                    <input type="text" name="MobileNumberExt" class="short">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                                <input type="checkbox" class="radioButton" id="iAgree" name="iAgree"><label class="errorRegister">You need to accept the Terms and Conditions</label> <label class="noteRegister">I Agree to the <a href="#" class=" bluetext underline">Terms of Use</a></label>
                                </td>
                             <td></td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                                <input type="checkbox" class="radioButton" name="signNew"> <label>Sign me Up for <span class="greentext">REI</span><span class="bluetext">Fax</span> Newsletters</label>
                                <div class="clear"></div>
                     <div class="centertext">
                                	<a href="#" id="formProcessing" class="buttonblue bigButton">Join Us</a>
                     </div>
                                </td>
                           <td></td>
                            </tr>
                     </table>
                     <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="txtPhone" id="txtPhone"  value="">
                     <input readonly type="hidden" name="txtMobil" id="txtMobil"  value="">
                     </form>
                     </div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Registration processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-support');
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('#usertype').trigger('change');

 	/***
	validaciones de campos requeridos
	************/
	$('#formProcessing').bind('click',function (){
		var correcto=true;
		$('.fieldRequired').each(function (){
				$(this).trigger('blur')
			})
		if(!$('#iAgree').is(':checked'))
		{
			$('#iAgree').parent().find('.noteRegister').fadeOut('slow',function (){
				$(this).parent().find('.errorRegister').fadeIn('slow',function (){
					$(this).delay(2000).parent().find('.errorRegister').fadeOut('slow',function (){
						$(this).parent().find('.noteRegister').fadeIn('slow');
						})
					})
				})
			correcto=false;
		}
		if(correcto)
		{
			$('#comunityRegister').submit();
		}
		else
		{
			//$(this).removeClass('buttongreen').html('Required fields are empty').addClass('buttonred')
			return correcto; 
		} 
		})
	$('#comunityRegister').bind('submit',function ()
	{
		$("#txtPhone").attr('value',$('.numberPhone:eq(0) input:eq(0)').val()+$('.numberPhone:eq(0) input:eq(1)').val()+$('.numberPhone:eq(0) input:eq(2)').val());
		$("#txtMobil").attr('value',$('.numberPhone:eq(1) input:eq(0)').val()+$('.numberPhone:eq(1) input:eq(1)').val()+$('.numberPhone:eq(1) input:eq(2)').val());
		$.ajax({
			 type	: 'POST',
            url		: $(this).attr('action'),
			dataType: 'json',
            data	: $(this).serialize(),
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					$('.MsglockScreen').html('<a href="../company/welcomeCommunity.php" class="buttonblue cerrarVentana">Ok...</a><div class="clear"></div>'+respuesta.mensaje);
					setTimeout(function (){
						document.location.href="../company/welcomeCommunity.php"}
						,6000);
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html('<a href="#" class="buttonred cerrarVentana">Return</a><div class="clear"></div>'+respuesta.mensaje);
					$('.cerrarVentana').bind('click',function (){
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('<img src="../resources/img/ac.gif">  Registration processing...');
						})
				}
			}
		});
		return false;
	})
	
});
</script>