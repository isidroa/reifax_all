<?php
class Fecha {
  var $fecha;
  function Fecha($anio=0, $mes=0, $dia=0) 
  {
       if($anio==0) $anio = Date("Y");
       if($mes==0) $mes = Date("m");
       if($dia==0) $dia = Date("d");
       $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
  }
  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
  {
       $array_date = explode("-", $this->fecha);
       $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
  }
 
  function getFecha() { return $this->fecha; }
}

//ultimo dia del mes actual
function UltimoDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}
//primer dia del mes actual
function PrimerDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

function isNumeric($n){
return ( $n == strval(intval($n)) )? true : false;
}
 
session_start();
require_once('../resources/php/properties_conexion.php');
require_once '../backoffice/php/mcripty.php';
conectar('xima');
$_SESSION['mail']=strtolower($_POST['txtEmail']);
$_SESSION['procode'];
$_SESSION['Name']=$_POST['txtFirstName'];
$_SESSION['LastName']=$_POST['txtLastName'];
$HAYUSERID=$_POST["USERID"];
$HAYIDCOMPANY=$_POST["idcompany"];

	//echo "<pre>";print_r($_POST);echo "</pre>";

	//Customer Information
	$idusertype=$_POST['usertype'];
	$Realtor=($idusertype==8)?substr($_POST["txtBroker"],0,10):'';
	$Broker= ($idusertype==3)?substr($_POST["txtBroker"],0,10):'';
	$Office=substr($_POST['txtOffice'],0,45);
	$Name=$_POST['txtFirstName'];
	$LastName=$_POST['txtLastName'];
	$nickname = $_POST['nickname'];	
	$Email='rp_'.strtolower(str_replace('%40','@',$_POST['txtEmail']));
	$Passregister=$_POST['txtPassword'];
	$questionuser=$_POST['questionuser'];
	$questionuser=str_replace("'","\'",$questionuser)	;
	$answeruser=$_POST['answeruser'];
	$Phone=$_POST['txtPhone'];
	$Mobil=$_POST['txtMobil'];
	
	//Billing Address
	$sameAdd=$_POST["sameAdd"]." ".$_POST["sameAdd2"];
	$sameCity=$_POST["sameCity"];
	$sameState=$_POST["sStateB"];
	$sameZip=$_POST["sameZip"];

	//Mailing Address
	$Country='US';		
	$Address=$_POST['txtAddress1']." ".$_POST["txtAddress2"];		
	$City=$_POST['txtCity'];			
	$State=$_POST['sState'];	
	$Zip=$_POST['txtZip'];				
	
	//Credit Card Information
	$holder=$_POST["holder"];
	$cardtype=$_POST["cardtype"]; 		
	$cardnumber=$_POST["cardnumber"];	
	$cardnumber=mcrypt_Ff969c5b9e($cardnumber);	
	$exdate1=$_POST["exdate1"];		
	$exdate2=$_POST["exdate2"];		
	$csv=$_POST["csv"];	
	if($csv=='')	$csv=0;
	$csv=mcrypt_Ff969c5b9e($csv);	
	

	//Aceptacion/Denegacion del contrato de registro
	$contract="Y";
	
	//Buscando el Executive
	$procode= strtoupper($_POST["prom"]);
	$Executive='';
	if(strlen($procode)>0 && $procode<>'' && $procode<>NULL)//SI HAY ALGO EN PROMOTION CODE
	{
		$query="SELECT userid from xima.ximausrs where userid = '$procode' or LCASE(pseudonimo)='$procode'";
		$rest=mysql_query($query) or die(mysql_error());
		$r=mysql_fetch_array($rest);
		$count=mysql_num_rows($rest);
		if($count>0)
			$Executive=$r[0];
		else
		{
			$query="SELECT userid from xima.procode where LCASE(procode)='$procode'";
			$rest=mysql_query($query) or die(mysql_error());
			$r=mysql_fetch_array($rest);
			$Executive=strtoupper($r[0]);
		}
		//echo "-->".$Executive."|";
	}

	//tomando el id del status para la nueva modalidad de cobranza y productos, usando la tabla xima.status 
	$status="active";
	$montoapagar=$_POST['montoapagar'];
	if(isset($_POST['freeactive']) && $_POST['freeactive']<>'' && $_POST['freeactive']==0)//usuarios free
	{$montoapagar=0;$status="free";}
	
	$freedays=0;
	$montoporcentaje=0;
	if(isset($_POST['freeactive']) && $_POST['freeactive']<>'' && $_POST['freeactive']==2)//usuarios trial
	{
		$status="trial";
		$montoapagar=$_POST['montopagartrial'];
		$freedays=$_POST['freedays'];
	}
	if($status=="trial" )$contract='N';	
	
	$sql_pc = "SELECT idstatus FROM xima.status WHERE status='$status'";
	$res_pc = mysql_query($sql_pc) or die ($sql_pc.mysql_error());
	$row5=mysql_fetch_array($res_pc, MYSQL_ASSOC);
	$idstatus=$row5['idstatus'];
	
	if($HAYUSERID=='' && strlen($HAYUSERID)==0)// si no viene el userid,hago las inserciones normales de nuevo user
	{	
		//INSERT SQL
		$sql="insert into ximausrs 	(pass, name, surname, country, state, city, address, zipcode, hometelephone, mobiletelephone, email, 
									affiliationdate, licensen, brokern, officenum, executive,  procode,pseudonimo, idstatus,idusertype,questionuser,answeruser) 
									values('$Passregister', '$Name', '$LastName', '$Country', '$State', '$City', '$Address', '$Zip', '$Phone', '$Mobil', '$Email',
									NOW(), '$Realtor', '$Broker', '$Office', '$Executive', '$procode', '$nickname','$idstatus',5,'$questionuser','$answeruser')";
	//echo $sql;return;
	
		$res=mysql_query($sql) or die($sql.mysql_error());
	}
	 $text="\r\n\r\n \r\n  ***************************************************************************************************************************\r\n ".$sql."|\r\n ".http_build_query($_POST)."\r\n";
	 $f1=fopen("e:\xima\AuditRegister.txt","a+");
	 fputs($f1,$text);
	 fclose($f1);		
	if($res || ($HAYUSERID<>'' && strlen($HAYUSERID)>0))
	{
		$selectID="select ximausrs.userid, ximausrs.email from ximausrs where email='$Email'";
		$resultID=mysql_query($selectID);	
		$rowID=mysql_fetch_array($resultID, MYSQL_ASSOC);
		$ID=$rowID["userid"];
		
		if($HAYUSERID=='' && strlen($HAYUSERID)==0)// si no viene el userid,hago las inserciones normales de nuevo user
		{	
			//--------- Registrando los usuarios en las tablas auxiliares 
			$query="INSERT INTO xima.profile (userid) VALUES ($ID);";
			mysql_query($query) or die($query.mysql_error());
			$query="Insert into xima.userterms (userid,accept)VALUES($ID,'$contract')";
			mysql_query($query) or die($query.mysql_error());
			$query="Insert into xima.usercondition (userid)VALUES($ID)";
			mysql_query($query) or die($query.mysql_error());
			$query="Insert into xima.usersession (userid)VALUES($ID)";
			mysql_query($query) or die($query.mysql_error());
			$query="Insert into xima.usernotes (userid)VALUES($ID)";
			mysql_query($query) or die($query.mysql_error());
			$queryIns="INSERT INTO xima.custom_user(userid)VALUES($ID)";
			mysql_query($queryIns) or die($queryIns.mysql_error());
			if(strlen($cardnumber)>10)
			{
				$query="INSERT INTO xima.creditcard (cardname,cardholdername,cardnumber,expirationyear,expirationmonth,
						csvnumber,billingaddress,billingcity,billingstate,billingzip,userid)
						VALUES ('$cardtype','$holder','$cardnumber','$exdate2','$exdate1','$csv','$sameAdd', 
						'$sameCity', '$sameState', '$sameZip',$ID)";	
				mysql_query($query) or die($query.mysql_error());
			}
		}	
		else
		{
			$query="UPDATE xima.creditcard 
					SET cardname='$cardtype',
						cardholdername='$holder',
						cardnumber='$cardnumber',
						expirationyear='$exdate2',
						expirationmonth='$exdate1',
						csvnumber='$csv',
						billingaddress='$sameAdd',
						billingcity='$sameCity',
						billingstate='$sameState',
						billingzip='$sameZip'
					WHERE userid=$ID";	
			mysql_query($query) or die($query.mysql_error());
			
			$query="DELETE FROM xima.permission WHERE userid=$ID";
			mysql_query($query) or die($query.mysql_error());
		}
		$queryIns="INSERT IGNORE INTO xima.usr_registertype(userid,idrtyp,insertdate)VALUES($ID,5,NOW())";//Customer Register
		mysql_query($queryIns) or die($queryIns.mysql_error());
			

		$setstate=$_POST['cbGrupoState'];
		$setgroup=$_POST['cbGrupoCounty'];
		//--------- TRANSFORMANDO EL SETSTATE para la nueva modalidad de cobranza y productos, Y LLEVARLO A LA NUEVA TABLA xima.userstate
		/*
		if($setstate=='ALL')
		{
			$query="SELECT l.`abrState` FROM xima.lsstate l WHERE l.`is_showed`='Y'";
			$result=mysql_query($query) or die($query.mysql_error());					
			$cad='';
			$p=0;
			while($row=mysql_fetch_array($result, MYSQL_ASSOC))
			{
				if($p>0)$cad.=',';
				$cad.=$row["abrState"];
				$p++;
			}
			$setstate=$cad;
		}
		$arrstate=explode(',',$setstate);
		foreach($arrstate as $val)
		{
			$allcou='0';
			if($setgroup=='ALL')$allcou='1';
			
			$query="INSERT INTO xima.userstate (`userid`, `idstate`, `all`,defstate)VALUES($ID,$val,$allcou,1)";
			mysql_query($query) or die($query.mysql_error());	
		}
		*/
		//-------------- AVERIGUANDO LA FRECUENDIA Y EL TIPO DE PRODUCTO para la nueva modalidad de cobranza y productos
		unset($idproducto,$idfrecuencia,$precioproducto);
		$platinum=$professional=$homefinder=$buyerspro=$leadsgenerator=$residential=$professional=$webreal=$investorweb=$adverrealtorweb=0;
		
		$producto=$_POST['cbGrupoProducts'];
		$frecuencia=$_POST['cbGrupoFrecuency'];
		/*
		switch($producto)
		{
			case 1://'Residential Pro'
				$residential=1;$homefinder=1; 
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1);
				$idfrecuencia=array($frecuencia);
			break;		
			case 2://'Platinum'
				$platinum=1;$residential=1;$homefinder=1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2);
				$idfrecuencia=array($frecuencia,$frecuencia);
			break;		
			case 3://'Professional'
				$professional=1;$platinum=1;$residential=1;$homefinder =1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2,3);
				$idfrecuencia=array($frecuencia,$frecuencia,$frecuencia);
			break;	
			case 6://'Buyers Pro'
				$buyerspro=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 4://'Webs'
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 7://'Home Finder'
				$homefinder=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 8://'Leads Generator'
				$leadsgenerator=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;
			
		}*/
		$idproducto=(12);
		//------------ Registrando los productos del cliente y frecuencia de los mismos para la nueva modalidad de cobranza y productos
		$query='INSERT INTO xima.usr_cobros (userid,idproductobase,fechacobro) VALUES ('.$ID.',(SELECT idproductobase FROM xima.usr_productobase WHERE activo=1 AND idproducto=11),NOW())';
		$query2='INSERT INTO xima.f_frecuency (userid,idfrecuency,idproducto) VALUES ('.$ID.',1,11)';
		
		/*foreach($idproducto as $k => $val)
		{
			if($k==0){
				$query='INSERT INTO xima.usr_cobros (userid,idproductobase,fechacobro) VALUES ';
				$query2='INSERT INTO xima.f_frecuency (userid,idfrecuency,idproducto) VALUES ';
			}else{
				$query.=',';
				$query2.=',';
			}
			$query.='('.$ID.',(SELECT idproductobase FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val.'),NOW())';
			$query2.='('.$ID.',1,'.$val.')';
			
			$queryp = 'SELECT precio FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val;
			$resultp = mysql_query($queryp) OR die($queryp.mysql_error());
			$rp = mysql_fetch_array($resultp);
			$precioproducto[]=$rp[0];
		}*/
		mysql_query($query) OR die($query.mysql_error());
		mysql_query($query2) OR die($query2.mysql_error());

		//--------- LLENADO DE LAS TABLAS permission 
		$query="INSERT INTO xima.permission (homefinder,buyerspro,leadsgenerator,residential,platinum,professional,professional_esp,realtorweb,investorweb, masterweb,advertisingweb,userid)
				VALUES ($homefinder,$buyerspro,$leadsgenerator,$residential,$platinum,$professional,0,$webreal,$investorweb,0,$adverrealtorweb,$ID);";
		mysql_query($query) or die($query.mysql_error());
		
		//------------------------------------------------------------------
		//Registrando los descuentos de los productos para la nueva modalidad de cobranza y productos
		unset($iddescuento,$estados);
		foreach($idproducto as $k => $val)
		{
			if($Executive<>'')
			{
				$query = "SELECT f.`descuento`
							FROM xima.f_countyprecio f
							INNER JOIN xima.usr_productobase u ON (f.`idproductobase`=u.`idproductobase`)
							WHERE u.`activo`=1 and u.`idproducto`=$val and f.`idstate`=$setstate and $wherecurcty
							LIMIT 1";
				$result = mysql_query($query) or die(mysql_error());
				$row=mysql_fetch_array($result, MYSQL_ASSOC);
				$descuento=$row["descuento"];
			}
			else
				$descuento=1;

			$query='INSERT INTO xima.f_discount (userid,idproducto,discount) VALUES ('.$ID.','.$val.','.$descuento.')';
			mysql_query($query) OR die($query.mysql_error());
		}		

		if($HAYUSERID=='' && strlen($HAYUSERID)==0)// si no viene el userid,hago las inserciones normales de nuevo user
		{	
			//CREANDO INSTANCIAS PARA EL AFFILIATE MARKETING 		
			//SMITH 17-07-20008
			//----------------------------------------------------------------------------------
			$hora=date('H:i:s');
			$query="Insert into xima.cobro_porcentajes (userid,saldo,pctgu,pctgc,mesu,pctgr,pctgr2lc,pctgr2lv,freedays)
					VALUES($ID,$montoporcentaje,50,0,2,20,10,100,$freedays)";
			$res=mysql_query($query) or die($query.mysql_error());//INSERTANDO EN SALDOPORCENTAJE
		
			$queryIns="INSERT INTO xima.cobro_estadocuenta (fechatrans,idoper,userid,monto)
						VALUES('".PrimerDiaActualMes()." 01:00:00',2,$ID,0)";
			mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO SALDO INICIAL AL HISTORICO
		}
		$login='true';//variable para controlar el logueo desde el ajax
		if($status=="active" || ($status=="trial" && $montoapagar>0 ))//si es activo hay que cobrarle o trial con monto de cobro
		{
			$cobrador='registerNormal';
			$idcobra=$ID;
			if($_POST["bkou"]<>'')
			{	$cobrador='registerBackOffice';$idcobra=$_POST["bkou"];	}
			
			$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`)
					VALUES ($ID,NOW(),".($montoapagar*-1).",4)";
			mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

			if(strtoupper($cardtype)=='VISA' || strtoupper($cardtype)=='MASTERCARD' || strtoupper($cardtype)=='DISCOVER')
			{ $cobrador='AUTHORIZE '.$cobrador; include("authorize_cobro.php");}
			else
			{ $cobrador='PAYPAL '.$cobrador; include("paypal_cobro.php");}
			if(strtoupper($paypal)<>'FAILURE')//paso el cobro del usuario
			{
				$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,paypalid,`usercobrador`,`cobradordesc`)
						VALUES(NOW(),5,$ID,".$montoapagar.",$paypalid,$idcobra,'$cobrador')";
				mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO EN EL HISTORICO
				
				$_SESSION['pidComprado']=$_SESSION['pid'];
				header ("Location: ../reports/homeFax.php");
 			}	
			else
			{	
				//echo $longMessage; 
				$selectID="delete from xima.ximausrs where userid=$ID";
				$resultID=mysql_query($selectID) or die($selectID.mysql_error());

				echo json_encode(
					array(  
					"producto"	=> $producto,
					"success"	=> false,
					"mensaje"	=> $longMessage 
				));	
				return;
			}			
		}

	}
	mysql_close();	
?>