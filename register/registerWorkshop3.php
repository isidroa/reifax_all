<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	require_once('../backoffice/php/mcripty.php');
   	conectar('xima');
	session_start();
	
	
		/***************
	*	PRODUCTOS, FREQUENCIA, FECHA DEL PAGO del usuario
	***************/
	$sql='SELECT p.homefinder, p.buyerspro, p.leadsgenerator, p.residential, p.platinum, p.professional, p.professional_esp,f. idfrecuency frecuency ,c.fechacobro
		FROM permission p,f_frecuency f ,usr_cobros c
		where p.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.userid=f.userid AND p.userid=c.userid limit 1';
	$respuesta=mysql_query($sql);
	if(mysql_num_rows($respuesta)>0)
	{
		$status=mysql_fetch_assoc($respuesta);
		if(($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1) && $status['frecuency']==2)
		{
			header ("Location: registerWorkshopFree.php");
		}
	}
		
	$sql='SELECT * FROM workshop WHERE status=1 LIMIT 1;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$data=mysql_fetch_assoc($res);
	$sql='SELECT * FROM xima.usr_productobase where idproducto=13 AND activo=1 LIMIT 1	;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$cost=mysql_fetch_assoc($res);
	

	
	if(!$_COOKIE['customerRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosUser=mysql_fetch_array($query);
		$query=mysql_query('select * from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosBanc=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['customerRegister'])
			return $_COOKIE['customerRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
$idworkshop=$data['idworkshop']; 
	?>

<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>
<style type="text/css">
	#ProductInformation li{
		list-style:none;
	}
	.titleDetailsProduct {
		margin-top: 10px;
	}
	.contentDetailsProduct {
		height: auto !important;
		margin-top: 10px;
		width: 400px !important;
	}
</style>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="/xima3/websiteindex.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../workshop/" class="returnResul"><span class="bluetext underline">WorkShop</span></a> &gt; 
        	<span class="fuchsiatext">WorkShop Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">WorkShop Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
		<?php if( $data['close'] ==1) {
			?>
	    	<div class="reportingframework">
	        	<div class="panel">
	           	 	<div class="center centertext">  
	            	     <span class="fuchsiatext"><strong>Thanks for contacting us regarding the Workshop. Unfortunately it is already full. </strong></span>
	            	</div>
	        	</div>
	        </div>
			<?
			}
		?>
        
       <!--Text container-->
       <form id="customerRegisterForm" class="formPrincipal"  method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                        
                       <h2>Workshop Information</h2>
                          <div id="ProductInformation">
                           <div class="clear"></div>
                               <div class="bold">
                                        Details of your workshop selection:
                                   </div>
                               <div class="clear"></div>                               
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>What:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $data['title'] ?></div>
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>When:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $data['when'] ?> at <?php echo $data['hour'] ?></div>
                                   
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Where:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct"><?php echo $data['address'] ?><br> <?php echo $data['city'] ?>, <?php echo $data['zipcode'] ?><br><?php echo $data['phone'] ?></span>
                                     <a href="<?php echo $data['linkMap'] ?>" target="_blank">Map & Directions</a></div> 
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Price:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct price">$<?php echo number_format($cost['precio'],2) ?></div>
                                  <div class="clear"></div>
                           <div class="clear"></div>
                           </div>

                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <?php if( $data['close'] !=1) {?>
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2 id="custdiv">Customer Information</h2> 
                        <?php if(!$_COOKIE['datos_usr']){ ?>
                        
                        <div class="clear"></div>
                        <div class="addressPanel">	
                        <table>
                        <tbody><tr>
                        	<td></td>
                            	<td>
                				<div class="centertext" style="width:260px;">
        		            		Already have an account?
        			            </div>
		                    </td>
                    	</tr>
                    </tbody></table>
                       <table>
                        	
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input type="hidden" value="/register/registerWorkshop3.php" name="returnTo">
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="loginEmail2" name="loginEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote"></span>
                                	<span class="errorRegister" id="emailwfNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="loginPass2" name="loginPass">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote"></span>
                                    <span id="passwordwfNoteError" class="errorRegister"></span>
                                </td>
                            </tr> 
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<a class="buttonblue bigButton processForm" id="logInInner" href="#">Log In</a>
                                </td>
                            </tr>   
						</table>
                </div><div class="addressPanel">	
                        <table>
                        <tbody><tr>
                        	<td></td>
                            	<td>
                				<div class="centertext" style="width:260px;">
        		            		New Account
        			            </div>
		                    </td>
                    	</tr>
                    </tbody></table>
                        
                       <table>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstNamewf" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="firstNamewf">
                                </td>
                                <td>
                                    <span id="firstNamewfNoteError" class="errorRegister"></span>									
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastNamewf" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="lastNamewf">
                                </td>
                                <td>
                                    <span id="lastNamewfNoteError" class="errorRegister"></span>									
                               </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="emailwf" name="emailwf">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote"></span>
                                	<span class="errorRegister" id="emailwfNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="passwordwf" name="passwordwf">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote"></span>
                                    <span id="passwordwfNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
							<tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <input value="<?php echo ($aux->cod)?$aux->cod:''?>" style="width: 30px;" class="short" type="text" name="txtPhoneCodwf" id="txtPhoneCodwf" maxlength='3'> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" style="width: 65px;"  class="fieldRequired" type="text" name="txtPhoneNumberwf" id="txtPhoneNumberwf" maxlength='7'> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" type="text" name="txtPhoneExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="txtPhoneNumberwfNoteError"></span>
                                </td>
                            </tr>         
						</table>
                </div>
                <?php } else{?>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstNamewf" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="firstNamewf">
                                </td>
                                <td>
                                    <span id="firstNamewfNoteError" class="errorRegister"></span>									
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastNamewf" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="lastNamewf">
                                </td>
                                <td>
                                    <span id="lastNamewfNoteError" class="errorRegister"></span>									
                               </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="emailwf" name="emailwf">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote"></span>
                                	<span class="errorRegister" id="emailwfNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="passwordwf" name="passwordwf">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote"></span>
                                    <span id="passwordwfNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
							<tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <input value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" type="text" name="txtPhoneCodwf" id="txtPhoneCodwf" maxlength='3'> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" name="txtPhoneNumberwf" id="txtPhoneNumberwf" maxlength='7'> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" type="text" name="txtPhoneExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="txtPhoneNumberwfNoteError"></span>
                                </td>
                            </tr>         
						</table> <?php }?>
            <div class="clear"></div>
                </div> 
        	</div>
            
            <div class="clear"></div>
            <div class="panel">
            	<div class="center register">
                        <div class="clear"></div>
                        <div class="centertext">
                        	<!--  Nevertheless, keep in touch, as we  are planning to do another next week. 
                        	<span class="fuchsiatext"><strong><br>Thanks for contacting us regarding the Workshop. Unfortunately it is full already. <br></strong></span>-->
                        	
										<a class="buttongreen bigButton processForm" id="formProcessing" href="#custdiv">Register Now</a>
										
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
        	<?php }?>
			<!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
                     <input readonly type="hidden" name="montoapagar" id="montoapagar"  value="<?php echo $cost['precio'] ?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
                    <input type="hidden" name="cbGrupoProducts" value="<?php echo $cost['idproductobase'] ?>">
                    <input type="hidden" id="workshop" name="workshop" value="<?php echo $idworkshop ?>">
                    <input type="hidden" name="source" value="RegisterWorkshop">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>

</div>


</html>
<script language="javascript">
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){			
	/*inicio de validaciones*/
	/*$('#loginbutton2').bind('click',function (e){
		$('#loginForm2').trigger('submit');
	})*/
	//validationRegister();
	//toConfimationPage($('#formProcessing'),$('#customerRegisterForm'));


			$('#logInInner').bind('click',function (e){
				e.preventDefault()
				$('#customerRegisterForm').attr('action','/resources/php/properties_validacion.php').trigger('submit');
			});

	
});


$('.processForm').bind('click',function ()
{
	if($('#firstNamewf').val()=='')
	{
		$('#firstNamewf').addClass('ErrorField');
		$('#firstNamewfNoteError').html("Field required").show();	
		return;
	}
	else
	{
		$('#firstNamewf').removeClass('ErrorField');
		$('#firstNamewfNoteError').hide();
	}
	if($('#lastNamewf').val()=='')
	{
		$('#lastNamewf').addClass('ErrorField');
		$('#lastNamewfNoteError').html("Field required").show();
		return;		
	}
	else
	{
		$('#lastNamewf').removeClass('ErrorField');
		$('#lastNamewfNoteError').hide();
	}
	if($('#emailwf').val()=='')
	{
		$('#emailwf').addClass('ErrorField');
		$('#emailwfNoteError').html("Field required").show();
		return;		
	}
	else
	{
		if($('#emailwf').val().indexOf('@', 0) == -1 || $('#emailwf').val().indexOf('.', 0) == -1)
		{
			$('#emailwf').addClass('ErrorField');
			$('#emailwfNoteError').html("Email invalid").show();
			return;		
		}

		$('#emailwf').removeClass('ErrorField');
		$('#emailwfNoteError').hide();
	}
	if($('#passwordwf').val()=='')
	{
		$('#passwordwf').addClass('ErrorField');
		$('#passwordwfNoteError').html("Field required").show();
		return;		
	}
	else
	{
		$('#passwordwf').removeClass('ErrorField');
		$('#passwordwfNoteError').hide();
	}
	if($('#txtPhoneCodwf').val()=='')
	{
		$('#txtPhoneCodwf').addClass('ErrorField');
		$('#txtPhoneNumberwfNoteError').html("Field required").show();
		return;		
	}
	else
	{
		$('#txtPhoneCodwf').removeClass('ErrorField');
		$('#txtPhoneNumberwfNoteError').hide();
	}
	if($('#txtPhoneNumberwf').val()=='')
	{
		$('#txtPhoneNumberwf').addClass('ErrorField');
		$('#txtPhoneNumberwfNoteError').html("Field required").show();
		return;		
	}
	else
	
	{
		$('#txtPhoneNumberwf').removeClass('ErrorField');
		$('#txtPhoneNumberwfNoteError').hide();
	}

	$('#txtPhone').val($('#txtPhoneCodwf').val()+$('#txtPhoneNumberwf').val())
	
		
//alert('registra');return;
	$('.lockScreen').fadeTo(100,0.5);
	$('.MsglockScreen').fadeIn(100);
	<?php 
		if( $data['close'] ==1)
		{
	?>
			$('.MsglockScreen').html('<h2>The workshop is full</h2><div class="clear"></div><a href="registerworkshop2.php" class="buttonblue cerrarVentana">OK</a>');
			return;	
	<?php 
		}
	?>
		$.ajax({
            type	: 'POST',
            url		: "newuserworkshopfree.php",
			dataType: 'json',
            data	: $('#customerRegisterForm').serialize(),
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					$('.MsglockScreen').html('<h2>You have been successfully registered</h2><div class="clear"></div><a href="/index.php" class="buttonblue cerrarVentana">Continue...</a>');
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html(respuesta.mensaje+'<div class="clear"></div><a href="#" class="buttonred cerrarVentana">Return</a>');
					$('.cerrarVentana').bind('click',function (e){
						e.preventDefault();
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('Registration processing...');
						})
				}
			}
		})
		
		
		return false;
})

/*highlight menu*/
 menuClick('menu-store');
 
 /* tooltips */
 tooltips();
</script>