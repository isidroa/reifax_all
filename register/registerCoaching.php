<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	require_once('../backoffice/php/mcripty.php');
conectar('xima');
	$query=mysql_query('select * from permission where userid='.$_COOKIE['datos_usr']['USERID']);
	$datosProduct=mysql_fetch_array($query);
	if(!$_COOKIE['customerRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosUser=mysql_fetch_array($query);
		$query=mysql_query('select * from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosBanc=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['customerRegister'])
			return $_COOKIE['customerRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
?>	
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>
<style type="text/css">
	#ProductInformation li{
		list-style:none;
	}
	#ventanalogueo{
		background: none repeat scroll 0 0 #FFFFFF;
		border-radius: 10px 10px 10px 10px;
		box-shadow: 0 0 20px #000000;
		color: #005C83;
		display: none;
		font-size: 14px;
		height: auto;
		left: 50%;
		margin-left: -325px;
		margin-top: -80px;
		padding: 20px;
		position: fixed;
		text-align: center;
		top: 50%;
		width: 650px;
		z-index: 11;
	}
	#ventanalogueo h2{
		font-size:18px;
		border-bottom: solid 1px #647368;
		padding-bottom:2px;
		margin-bottom:4px;
	}
	#ventanalogueo > p{
		margin-bottom:20px;
	}
	#ventanalogueo .paneloption{
		float:left;
		width:49%;
	}
	#ventanalogueo .last{
		border-left:solid 1px #647368;
	}
	#ventanalogueo .paneloption table td{
		padding: 4px 0;
	}
</style>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/xima3/websiteindex.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Coaching Request</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Coaching Request - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" action="registerCoachingConfimation.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Coaching Request</h2>
                          <div id="ProductInformation">
                           <p class="bold">
                           		This form will allow you to request a private training session for any of the products you are subscribed. After request form is filled out, please allow two business days to get contacted by our professional team to schedule the private training session date and time.
                           </p>
                          
                           <div class="clear"></div>
                               <div class="bold">
                                        Details of your Coaching Product Selection:
                                   </div>
                               <div class="clear"></div>                               
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Product:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"></div>
                                   
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Number of hours:
                                   </div>
                                   <div id="selectedNumberHours" class="contentDetailsProduct selectedProduct"></div> 
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Price:
                                   </div>
                                   <div id="selectedPrice" class="contentDetailsProduct selectedProduct price"></div>
                                  <div class="clear"></div>
                            <div>
                           		If you wish to change your selection, you may do it below:
                           </div>
                           <div class="clear"></div>
                           <div>
                           		<div class="centertext selections">
                                	<input type="hidden" value="<?php echo printValue ('executive')?>" name="executive" id="executive">
                                	<label class="required">Product:</label><br>
                                    <select name="cbGrupoProducts" style="width:140px;" id="product">
                                    <?php
										if($datosProduct['residential']==1)
											echo '<option value="1">REIFax Residential</option>';
										if($datosProduct['platinum']==1)
											echo '<option value="2">REIFax Platinum</option>';
										if($datosProduct['professional_esp']==1 || $datosProduct['professional']==1)
											echo '<option value="3">REIFax Professional</option>';
										if($datosProduct['homefinder']==1)
											echo '<option value="7">Home Deals Finder</option>';
										if($datosProduct['buyerspro']==1)
											echo '<option value="6">Buyer&acute;s Pro</option>';
										if($datosProduct['leadsgenerator']==1)
											echo '<option value="8">Leads Generator</option>';
									?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoProductsText" name="cbGrupoProductsText">
									<input type="hidden" id="montoapagar" name="montoapagar" value="">
                                </div>
                           		<div class="centertext selections">
                                	<input type="hidden" value="<?php echo printValue ('selectedNumberHours')?>" name="cbNumberHoursText" id="cbNumberHoursText">
                                	<label class="required">Number of hours:</label><br>
                                    <select name="cbNumberHours" style="width:140px;" id="NnmHours">
                                    	<option value="6">1 Hour</option>
                                    	<option value="8">2 Hours</option>
                                    	<option value="9">3 Hours</option>
                                    	<option value="7">4 Hours</option>
                                    </select>
                                </div> 
                                <div class="clear"></div>                    		
                                <div class="centertext selections" style="width: 600px; text-align:justify">
                                	<label style="padding-left:20px;">Preferred coaching time:</label><br>
                                    <textarea  style="width: 600px;" name="coachingTime"></textarea>
                                </div>
                          </div>
                           </div>

                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
    	            <h2>Billing Address</h2>
	                <p>Your billing address must match the address on your credit card statement</p>
                    <div class="clear"></div>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd',$datosBanc,'billingaddress')?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd2')?>" type="text" name="sameAdd2">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameCity',$datosBanc,'billingcity')?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td>
                                    <select class="fieldRequired" id="sStateB" name="sStateB">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['customerRegister']['sStateB'])?$_COOKIE['customerRegister']['sStateB']:$datosBanc['billingstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
                                        ?>				
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr class="hide" id="SpecifyStateB">
                                <td>
                                    <label class="required">Specify State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('otherState',$datosUser,'otherState')?>" name="otherStateB">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue ('sameZip',$datosBanc,'billingzip')?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('holder',$datosBanc,'cardholdername')?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                        <?php 
                                        $var=($_COOKIE['customerRegister']['cardtype'])?$_COOKIE['customerRegister']['cardtype']:$datosBanc['cardname'];
										foreach ($cardtype AS $key => $value)
										{
											echo ($key==$var)?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';
										} 
										?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cardtypeText')?>" id="cardtypeText" name="cardtypeText">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('cardnumber',$datosBanc,'cardnumber'): mcrypt_Fe7a0a89bd(printValue ('cardnumber',$datosBanc,'cardnumber'))?>" class="fieldRequired" id="cardNumber2" type="text" name="cardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate1'])?$_COOKIE['customerRegister']['exdate1']:$datosBanc['expirationmonth'];
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$var)?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate2'])?$_COOKIE['customerRegister']['exdate2']:$datosBanc['expirationyear'];
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$var)?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('csv',$datosBanc,'cardnumber'):mcrypt_Fe7a0a89bd(printValue ('csv',$datosBanc,'csvnumber'))?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div style="z-index:2; top:-190px;" class="notification">
                                    	<img src="../resources/img/CVV.dib" style="width:100%">
                                	</div>
                                </a>)
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" id="formProcessing" href="#">Continue to Confirmation Page<div id="errorForm" class="errorForm">Required fields are empty<div></div></div></a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
            <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
                     <input type="hidden" id="source" name="source" value="RegisterReports">
         </form>
        <!--End ext container -->
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div id="ventanalogueo">
<div>
	<h2>
    	Request a private training session
    </h2>
    <div class="clear">
    </div>
    <div class="paneloption">
       <form id="loginForm2" method="post" action="/resources/php/properties_validacion.php">
        	<table>
            	<tr>
                    <td>
                		<label title="Login Email Submit" class="bluetext bold">E-mail</label>
                    </td>
                    <td>
                        <input type="email" value="<?php echo($DataRemember[0])?$DataRemember[0]:''; ?>" name="loginEmail" class="fieldtext" id="loginEmail">
                   	</td>
           		</tr>
 				<tr>
            		<td>
               			<label title="Login Password Submit" class="bluetext bold">Password</label>
              		</td>
                    <td>
						<input type="password" value="<?php echo($DataRemember[1])?$DataRemember[1]:''; ?>" name="loginPass"  class="fieldtext" id="loginPass">
					</td>
				</tr>
                <tr>
                	<td>
                    </td>
                    <td>
                    	<div style="margin-left:20px;">
                        	<input style="float:left" type="checkbox" checked="checked" class="radioButton" name="remember"> <label style="margin-top:2px; float:left; margin-left:2px; font-size:11px;">Remember</label>
                        </div>
                    </td>
                </tr>
                <tr>
                	<td>
                    </td>
                    <td>
                    	<div style="margin-left:20px;">
                       		<a href="#" style="float:left;"  id="loginbutton2" class="buttonblue">Log In</a>
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="returnTo" value="../../register/registerCoaching.php">
        </form>
    	
    </div>
    <div class="centertext paneloption last">
        <h3>
            Not registered?
        </h3>
        
		<a href="../store/index.php" class="buttonblue bigButton">Sign Up Now</a>    	
    </div>
</div>
</div>

</html>
<script language="javascript">
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){
	
	$('#loginbutton2').bind('click',function (e){
		e.preventDefault()
		$('#loginForm2').trigger('submit');
	});
	if(!$.cookie('login'))
	{
		$('#ventanalogueo').fadeIn('slow');
		$('.lockScreen').fadeIn('fast');
	}
	$('#product,#NnmHours').bind('change',function (){
		$('#cbGrupoProductsText').val($('#product option:selected').html());
		$('#selectedProduct').html($('#product option:selected').html());
		$('#cbNumberHoursText').val($('#NnmHours option:selected').html());
		$('#selectedNumberHours').html($('#NnmHours option:selected').html());
		/*
		if($('#NnmHours').val()==1)
		{
				$('#selectedPrice').html('$77');
				$('#montoapagar').val('77');
		}
		else if($('#NnmHours').val()==4)
		{
				$('#selectedPrice').html('$249');
				$('#montoapagar').val('77');
		}*/
		$.ajax({
		url		:'../resources/php/properties.php',
		type	: 'get',
		dataType:'json',
		data	:'priceCoaching=true&frecuencia='+$('#NnmHours').val(),
		success:function (resul){
			$('#selectedPrice').html('$'+Math.round((parseFloat(resul.price)*100))/100);
			$('#montoapagar').attr('value',Math.round((parseFloat(resul.price)*100))/100);
			}
		});
		
		/*
		$('.totalValue').html('');
		$('input[name=cbFrecuenciaText]').attr('value',$('select[name=cbFrecuencia] option:selected').html());
		switch($(this).val())
		{
			case (1):
				$('#cbDuration').attr('val',30);
			break;
			case (4):
				$('#cbDuration').attr('val',90);
			break;
			case (5):
				$('#cbDuration').attr('val',365)
			break;
		}
		$.ajax({
		url		:'../resources/php/properties.php',
		type	: 'get',
		dataType:'json',
		data	:'priceReport=true&frecuencia='+$(this).val(),
		success:function (resul){
			$('.price').html('$'+Math.round((parseFloat(resul.price)*100))/100);
			$('#montoapagar').attr('value',Math.round((parseFloat(resul.price)*100))/100);
			}
		});*/
	});
	
$('#sStateB').bind('change',function(){
	if($(this).val()=='Other'){
		$('#SpecifyStateB').show();
	}
	else{
		$('#SpecifyStateB').hide();
	}
}).trigger('change');
	$('#product').trigger('change');
			
	/*inicio de validaciones*/
	validationRegister();
	toConfimationPage($('#formProcessing'),$('#customerRegisterForm'));
});

/*highlight menu*/
 menuClick('menu-store');
 
 /* tooltips */
 tooltips();
</script>