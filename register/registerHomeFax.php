<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	require_once '../backoffice/php/mcripty.php';
   	conectar('xima');
	session_start();
	$_SESSION['property']=$_GET['property'];
	$_SESSION['county']=$_GET['county'];
	$_SESSION['pid']=$_GET['pid'];
	$_SESSION['pagoReport']=false;
	
	if(!$_COOKIE['customerRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosUser=mysql_fetch_array($query);
		$query=mysql_query('select * from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosBanc=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['customerRegister'])
			return $_COOKIE['customerRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>
<style type="text/css">
	#ProductInformation li{
		list-style:none;
	}
</style>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/xima3/websiteindex.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#" class="returnResul"><span class="bluetext underline">Result</span></a> &gt; 
        	<span class="fuchsiatext">HomeFax Order</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/training/overviewTraining.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/advertise.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/become.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">HomeFax Order - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" action="registerHomeFaxConfimation.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Product Information</h2>
                          <div id="ProductInformation">
                           <p class="bold">
                           By buying the Home Fax Report for the property specified below, you will get the following information:
                           </p>
                           <div style="float:left;width: 250px;">
                               <ul>
                                    <li>
                                        <div class="cuote"></div>Public Records
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Active Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Non Active Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Distressed Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Rental Comparables (if any)
                                    </li>
                               </ul>
                           </div>   
                           <div style="float:left;width: 250px;">  
                           <ul>
                           		<li>
                                	<div class="cuote"></div>Owner Information
                                </li>
                                <li>
                                	<div class="cuote"></div>Mortgage Information (if any)
                                </li>
                                <li>
                                	<div class="cuote"></div>Foreclosure Information (if any)
                                </li>
                           </ul>
                           </div>
                           <div class="clear"></div>
                               <div class="bold">
                                        Details of your Property selection:
                                   </div>
                               <div class="clear"></div>                               
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Property:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $_SESSION['property'] ?></div>
                                   
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>County:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct"><?php echo $_SESSION['county'] ?></div> 
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Price:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct price"></div>
                                  <div class="clear"></div>
                            <div>
                           		If you wish to change your selection, you may do it below:
                           </div>
                           <div class="clear"></div>
                           <div>
                           		<div class="centertext selections">
                                	<input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" name="executive" id="executive">
                                	<label class="required">Frecuency:</label><br>
                                    <select name="cbFrecuencia" style="width:140px;" id="product">
                                    	<option value="1">Monthly</option>
                                    	<!-- <option value="4">Quarterly</option> -->
                                    	<option value="5">Annually</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbFrecuenciaText" name="cbFrecuenciaText">
									<input type="hidden" id="montoapagar" name="montoapagar" value="">
									<input type="hidden" id="cbDuration" name="cbDuration" value="30">
                                </div>
                          </div>
                           </div>

                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2> 
                        <?php echo ($_COOKIE['datos_usr'])? '': '<p>Already have an account? <a href="#" id="fastLogin" class="greentext">Log In...</a></p>'; ?>                       
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" value="<?php echo printValue ('nickname',$datosUser,'pseudonimo')?>" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail2',$datosUser,'EMAIL')?>" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="password" name="txtPassword">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 8 characters and contain a letter and a number</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword2',$datosUser,'PASS')?>" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <?php
								if(!$_COOKIE['datos_usr'])
								{
									securityQuestion(printValue ('questionuser'), printValue ('answeruser'));
								}
							?>
                        	<tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <input value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" type="text" name="txtPhoneCod"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" type="text" name="txtPhoneExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('MobileNumberCod');
										$aux->num=printValue ('MobileNumber');
										$aux->ext=printValue ('MobileNumberExt');
									}
									else
										$aux=($datosUser['MOBILETELEPHONE'])?formantPhone($datosUser['MOBILETELEPHONE']):'';
									?>
                                    <input type="text" value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" name="MobileNumberCod"> <label>-</label> 
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" name="MobileNumberExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
    	            <h2>Billing Address</h2>
	                <p>Your billing address must match the address on your credit card statement</p>
                    <div class="clear"></div>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd',$datosBanc,'billingaddress')?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd2')?>" type="text" name="sameAdd2">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameCity',$datosBanc,'billingcity')?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td>
                                    <select class="fieldRequired" id="sStateB" name="sStateB">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['customerRegister']['sStateB'])?$_COOKIE['customerRegister']['sStateB']:$datosBanc['billingstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
                                        ?>				
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue ('sameZip',$datosBanc,'billingzip')?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('holder',$datosBanc,'cardholdername')?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                        <?php 
                                        $var=($_COOKIE['customerRegister']['cardtype'])?$_COOKIE['customerRegister']['cardtype']:$datosBanc['cardname'];
										foreach ($cardtype AS $key => $value)
										{
											echo ($key==$var)?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';
										} 
										?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cardtypeText')?>" id="cardtypeText" name="cardtypeText">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('cardnumber',$datosBanc,'cardnumber'): mcrypt_Fe7a0a89bd(printValue ('cardnumber',$datosBanc,'cardnumber'))?>" class="fieldRequired" id="cardNumber2" type="text" name="cardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate1'])?$_COOKIE['customerRegister']['exdate1']:$datosBanc['expirationmonth'];
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$var)?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate2'])?$_COOKIE['customerRegister']['exdate2']:$datosBanc['expirationyear'];
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$var)?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('csv',$datosBanc,'cardnumber'):mcrypt_Fe7a0a89bd(printValue ('csv',$datosBanc,'csvnumber'))?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div style="z-index:2; top:-190px;" class="notification">
                                    	<img src="../resources/img/CVV.dib" style="width:100%">
                                	</div>
                                </a>)
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" id="formProcessing" href="#">Continue to Confirmation Page<div id="errorForm" class="errorForm">Required fields are empty<div></div></div></a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
            <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>

</html>
<script language="javascript">
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){
	
	$('select[name=cbFrecuencia]').bind('change',function (){
		$('.totalValue').html('');
		$('input[name=cbFrecuenciaText]').attr('value',$('select[name=cbFrecuencia] option:selected').html());
		switch($(this).val())
		{
			case (1):
				$('#cbDuration').attr('val',30);
			break;
			case (4):
				$('#cbDuration').attr('val',90);
			break;
			case (5):
				$('#cbDuration').attr('val',365)
			break;
		}
		$.ajax({
		url		:'../resources/php/properties.php',
		type	: 'get',
		dataType:'json',
		data	:'priceReport=true&frecuencia='+$(this).val(),
		success:function (resul){
			$('.price').html('$'+Math.round((parseFloat(resul.price)*100))/100);
			$('#montoapagar').attr('value',Math.round((parseFloat(resul.price)*100))/100);
			}
		});
	}).trigger('change');
			
	$('.returnResul').attr('href',$.cookie('urlResult'));
	/*inicio de validaciones*/
	validationRegister();
	toConfimationPage("#formProcessing",'#formProcessing');
	$('#formProcessing').bind('click',function (){
		$('.fieldRequired').each(function (){
				$(this).trigger('blur')
			})
		if(correcto && pass && repass && email && remail && nick && credicard)
		{
			$('#customerRegisterForm').submit();
		}
		else
		{
			$('#errorForm').stop(true,true).css({opacity:0,display:'block'}).animate({
					top			:'-65px',
					opacity		:1
					},400).delay(3000).animate({
						top		:'-55px'
						},200,function (){
							$(this).animate({
								top		:'-100px',
								opacity	:0,
								},400,function (){
									$(this).css({display:'none'});
									});
							});
			return false;
		}
		})
});

/*highlight menu*/
 menuClick('menu-store');
 
 /* tooltips */
 tooltips();
</script>