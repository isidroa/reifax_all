<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	$aux=mysql_query('SELECT * FROM `xima`.`xima_system_var` where userid='.$_COOKIE['datos_usr']['USERID']);
	$backupData=mysql_fetch_assoc($aux);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>
<style type="text/css">
	.radioClienteSoft
	{
		float:left;
		width:110px;
	}
	#softCliente
	{
		display:none;
	}
#galeriaFooter{
	height: 100px;
}
#galeriaFooter td{
	width:260px;
}
#galeriaFooter div a{
	background:#F00;
	color:#FFF;
	padding:2px 3px;
	font-weight:bold;
	font-size:10px;
	position:absolute;
	right:0px;
	top:0px;
}
#galeriaFooter td > div  {
	position:relative;
	border:solid 1px #6B7166;
	background:#CACBC9;
	padding:4px 4px 10px 4px;
	margin-right:24px;
	height:85px !important;
	width:102px !important;
	-moz-box-shadow: 0 1px 3px #AAAAAA;
	-webkit-box-shadow: 0 1px 3px #AAAAAA;
	float:left;
}
#galeriaFooter td > div:last-child  {
	margin-right:0px;
}
#galeriaFooter div img
{
	height:85px;
	display:block;
	width:102px;
	background:url(../resources/img/ac.gif) no-repeat center;
}
#galeriaFooter .upload
{
	position:absolute;
	bottom:0px;
}
</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Create a Ticket</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Create a Ticket - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext">
					<h2 style="color:#000; font-weight:bold;">Create a Ticket</h2>
                    <form action="#" id="registerTicket">
                    <table>
                            <tr>
                            	<td>
                                	<label class="required">Topic/Subject:</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="issues" name="issues">
                                    	<option value="">Select an option</option>
                                    	<option value="Billing/ Credit Card">Billing/ Credit Card</option>
                                    	<option value="Cancellation">Cancellation</option>
                                    	<option value="Downgrading Account">Downgrading Account</option>
                                    	<option value="Upgrading Account">Upgrading Account</option>
                                    	<option value="Technical/ System Issue">Technical/ System Issue</option>
                                    	<option value="REIFax Training">REIFax Training</option>
                                    	<option value="REIFax Promotion Inquiry">REIFax Promotion Inquiry</option>
                                    	<option value="General Question">General Question</option>
                                    	<option value="Suggestions">Suggestions</option>
                                    	<option value="Other">Other</option>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Issues Description</label>
                                </td>
                                <td>
                                	<textarea class="fieldRequired" name="issuesDescri"></textarea>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                     </table>
                     <div id="softCliente" class="divErrorSystem" >
                     <table>
                        	<tr>
                            	<td>
                                	<label class="required">System Operative</label>
                                </td>
                                <td>
                                    <div class="clear"></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Windows" name="os"><label>Windows</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Mac" name="os"><label>Mac</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Linux" name="os"><label>Linux</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Android" name="os"><label>Android</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Other" name="os"><label>Other</label></div>
                                    <div class="clear"></div>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Browser</label>
                                </td>
                                <td>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton" value="IE6" name="browser"><label>IE6</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="IE7" name="browser"><label>IE7</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="IE8" name="browser"><label>IE8</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="IE9" name="browser"><label>IE9</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Firefox" name="browser"><label>Firefox</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Chrome" name="browser"><label>Chrome</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Safari" name="browser"><label>Safari</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Opera" name="browser"><label>Opera</label></div>
                                	<div class="radioClienteSoft"><input type="radio" class="radioButton"  value="Other" name="browser"><label>Other</label></div>
                                </td>
                                <td>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Product:</label>
                                </td>
                                <td>
                                    <select name="product" id="product">
                                                <option value="1">REIFax Residential</option>
                                                <option value="2">REIFax Platinum</option>
                                                <option value="3">REIFax Professional</option>
                                            	<option value="7">Home Deals Finder</option>
                                                <option value="6">Buyer&acute;s Pro</option>
                                                <option value="8">Leads Generator</option>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<label class="required">Data:</label>
                                </td>
                                <td>
                                	<select name="data">
                                    	<option selected="selected" value="Public Records">Public Records</option>
                                        <option value="For Sale">For Sale</option>
                                        <option value="For Rent">For Rent</option>
                                        <option value="Foreclosures">Foreclosures</option>
                                        <option value="By Owner">By Owner</option>
                                        <option value="By Owner Rent">By Owner Rent</option>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Seccion:</label>
                                </td>
                                <td>
                                	<select name="seccion">
                                        <option value="Search">Search</option>
                                        <option value="Results">Results</option>
                                        <option value="Comparables">Comparables</option>
                                        <option value="Comparables Actives">Comparables Actives</option>
                                        <option value="Comparables Rental">Comparables Rental</option>
                                        <option value="Statistics">Statistics</option>
                                        <option value="My Short Sale">My Short Sale</option>
                                        <option value="My Short Docs">My Short Docs</option>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Property Type:</label>
                                </td>
                                <td>
                                	<select name="propertyType">
                                        <option value="Any Type">Any Type</option>
                                        <option value="Single Family">Single Family</option>
                                        <option value="Condo/Town/Villa">Condo/Town/Villa</option>
                                        <option value="Multi Family +10">Multi Family +10</option>
                                        <option value="Multi Family -10">Multi Family -10</option>
                                        <option value="Commercial">Commercial</option>
                                        <option value="Vacant Land">Vacant Land</option>
                                        <option value="Mobile Home">Mobile Home</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Stade Searched:</label>
                                </td>
                                <td>
                                	<select id="state" name="state">
                                    <?php
                                    	 $sql="SELECT * FROM lsstate where is_showed='Y';";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo '<option value="'.$data['abrState'].'|'.$data['IdState'].'">'.$data['State'].'</option>';
                                            }
									?>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">County Searched:</label>
                                </td>
                                <td>
                                	<select id="county" name="county"> 
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Search:</label>
                                </td>
                                <td>
                                	<textarea class="" name="search"><?php echo $backupData['search']; ?></textarea>
                                </td>
                                <td>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                      </table>
                      </div>
                     </form>
                     <div id="galeriaFooter" class="divErrorSystem">
                         <table >
                             <tr>
                                 <td>
                                 	<label class="">Upload Image:</label>
                                 </td>
                                 <td>
                                    <div>
                                        <img src="http://www.eez.csic.es/files/images/no_image.gif">
                                        <a href="#image1">X</a>
                                        <div class="upload uploadprofile">
                                            <input name="image1" type="file">
                                       </div>
                                   </div>
                                    <div>
                                        <img src="http://www.eez.csic.es/files/images/no_image.gif">
                                        <a href="#image2">X</a>
                                        <div class="upload uploadprofile">
                                            <input name="image2" type="file">
                                       </div>
                                   </div>
                                 </td>
                                 <td>
                                 </td>
                               </tr>
                           </table>
                     </div>
                     <div class="clear"></div>
                     <div class="centertext">
                     	<a href="#" id="formProcessing" class="buttongreen">Create Ticket</a>
                     </div>
                     <div class="clear"></div>
                     </div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Registration processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-support');
 var uploadImg=false;
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('#usertype').trigger('change');
	$('#issues').bind('change', function () {
		var val=$(this).val();
		if(val=='Technical/ System Issue')
		{
			$('.divErrorSystem').show('fast');
			$('#softCliente input, #softCliente textarea ,#softCliente select').removeClass('fieldRequired');
		}
		else if($('#softCliente').is(':visible'))
		{
			$('.divErrorSystem').hide('fast');
			$('#softCliente input, #softCliente textarea ,#softCliente select').removeClass('fieldRequired');
		}
	}).trigger('change');
	
	/******************************************
	*** Pre-llenado de los campos
	*******************************************/
	
	if ($.browser.webkit) {
		$('input:radio[value=Chrome]').attr('checked','checked');		// esto es para el navegador
	}else if ($.browser.msie){
   		if ($.browser.version==6){
			$('input:radio[value=IE6]').attr('checked','checked');
   		}
		else if($.browser.version==7){
			$('input:radio[value=IE7]').attr('checked','checked');
   		}
		else if($.browser.version==8){
			$('input:radio[value=IE8]').attr('checked','checked');
   		}
		else if($.browser.version==9){
			$('input:radio[value=IE9]').attr('checked','checked');
   		}
		else
		{
		$('input:radio[value=Other]').attr('checked','checked');
		}
	}else if ($.browser.opera) {
		$('input:radio[value=Opera]').attr('checked','checked');
	}else if ($.browser.mozilla) {
		$('input:radio[value=Firefox]').attr('checked','checked');
	}
	else
	{
		$('input:radio[value=Other][name=browser]').attr('checked','checked');
	}
	
	///////////////////////////////////////////// 
	var navInfo = window.navigator.appVersion.toLowerCase();
	if(navInfo.indexOf('win') != -1)			// esto es para el sistema operativo
	{
		$('input:radio[value=Windows]').attr('checked','checked');
	}
	else if(navInfo.indexOf('linux') != -1)
	{
		$('input:radio[value=Linux]').attr('checked','checked');
	}
	else if(navInfo.indexOf('mac') != -1)
	{
		$('input:radio[value=Mac]').attr('checked','checked');
	}
	else if(navInfo.indexOf('mac') != -1)
	{
		$('input:radio[value=Mac]').attr('checked','checked');
	}
	/****************************************
	****	FUNCION DE CARGA DE IMG
	*******************************************/
	$('#galeriaFooter input').each(function ()
	{
		$(this).bind('change',function (){
			uploadImg=true;
			$(this).parent().parent().find('img').attr('old',$(this).parent().parent().find('img').attr('src')).attr('src','');
			$(this).upload('../resources/php/funcionesMyTickets.php', 'option=loadImg&picture='+$(this).attr('name'), function(res) {
				if(res.success)
				{
					uploadImg=false;
					$(this).parent().parent().find('img').attr('src',res.img)
				}
				else
				{
					alert('Sorry, the image could not be loaded, please try again');
					$(this).parent().parent().find('img').attr('src',$(this).parent().parent().find('img').attr('old'));
				}
			}, 'json');
		})
	});
 	/***
	validaciones de campos requeridos
	************/
	$('#formProcessing').bind('click',function (){
		$('.fieldRequired').each(function (){
				$(this).trigger('blur')
		});
		if($('.ErrorField').length<1)
		{
			if(uploadImg){
				if(confirm('Sorry, all images have not been uploaded yet. If you continue any missing image will not be taken into account')){
					$('#registerTicket').submit();
				}
			}
			else
				$('#registerTicket').submit();
		}
		else
		{
			return false; 
		} 
	})
		
 	/***
	validaciones de campos requeridos
	************/
	$('#registerTicket').bind('submit', function (e){
		e.preventDefault();
		$.ajax({
			url:'../resources/php/funcionesMyTickets.php',
			type:'POST',
			data:'option=createTicket&'+$(this).serialize(),
			dataType:'json',
			success: function (result)
			{
				$('.lockScreen').fadeIn('fast');
				if(result.success)
				{
						$('.MsglockScreen').html('Ticket has been successfully created under the number # '+result.ticket+' . To check the status please go to My Settings and click on My Tickets tab. <div class="clear"></div><a href="#" class="buttonblue cerrarVentana">Ok...</a>').fadeIn('fast');
								$('.cerrarVentana').bind('click',function (){
									window.close();
									})
				}
				else
				{
					$('.MsglockScreen').css({
									'font-size': '16px',
									 color		:'#C60303'
								}).html('Sorry, an error occurred when creating the ticket. Please try again.<div class="clear"></div><a href="#" class="buttonred cerrarVentana">Return</a>').fadeIn('fast');
								$('.cerrarVentana').bind('click',function (){
									$('.lockScreen,.MsglockScreen').fadeOut(200,function (){
											$('.MsglockScreen').css({
												 'font-size': '26px',
												 color		:'#005C83'
											}).html('<img src="../resources/img/ac.gif">  Processing...');
										});
									})
				}
			}
		})
	})
	$('#state').bind('change',function ()
	{
	$.ajax({
		type	:'POST',
		url		:'../resources/php/properties.php',
		data	: "ejecutar=countylist&state="+$('#state').val().split('|')[1],
		dataType:'json',
		success	:function (resul)
			{
				$('#county').html('');
				$(resul).each(function (index,data){
					$('#county').append('<option value="'+data.id+'">'+data.county+'</option>');
				});
			}
		});
	}).trigger('change');
	
});
</script>