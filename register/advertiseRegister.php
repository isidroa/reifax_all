<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
   	conectar('xima');
	if(!$_COOKIE['advertiseRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs u LEFT JOIN usr_companyinformation c ON c.userid=u.userid where u.userid='.$_COOKIE['datos_usr']['USERID'].' limit 1');
		$datosUser=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['advertiseRegister'])
			return $_COOKIE['advertiseRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
	$datosUser['expirationdate']=explode('/',$datosUser['expirationdate']);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="http://www.reifax.com/company/advertise.php"><span class="bluetext underline">Advertise</span></a> &gt; 
        	<span class="fuchsiatext">Advertiser Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                <a href="#1" class="yourAdHere advertise1"></a>
                <a href="#2" class="yourAdHere advertise2"></a>
                <a href="#3" class="yourAdHere advertise3"></a>
                <a href="#4" class="yourAdHere advertise4"></a>
                <a href="#5" class="yourAdHere advertise5"></a>
                <a href="#6" class="yourAdHere advertise6"></a>
                <div></div>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Advertiser Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->                    
       <form id="advertiseRegisterForm" action="advertiseRegisterConfirmation.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Advertising Information</h2>
                           <div class="BoxTextLoudGray bold" style="display:none;visibility:hidden;">
                                Optional - Promotion Code
                           </div>
                           <div class="BoxTextSoftGray" style="display:none;visibility:hidden;">
                                <!--<span>If you have a Promotion Code, enter it in the box to the right </span>-->
                                <input type="hidden" class="shortBox mediumMarginLef promotionCode"  value="<?php echo ($_COOKIE['procode'])?$_COOKIE['procode']:printValue ('prom')?>" name="prom" id="promotionCode" >
                           </div>
                           <div id="ProductInformation">
                               <input type="hidden" id="inputTotalValue" value="<?php echo printValue ('price')?>" name="price">
                           		<div class="bold">
                                    Details of best Ad position available: Total $<span class="totalValue"></span>
                               </div>    
                           <div class="clear"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>State:
                               </div>
                               <div id="selectedState" class="contentDetailsProduct selectedState"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>County:
                               </div>
                               <div  id="selectedCounties" class="contentDetailsProduct selectedCounties"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Ad Position:
                               </div>
                               <div style="width:190px;" id="selectedProduct" class="contentDetailsProduct"><span class="selectedProduct"></span><span class="redtext bold" style="display:none" id="positionNotAvailable">, Not Available</span></div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Frequency:
                               </div>
                               <div id="selectedFrequency" class="contentDetailsProduct selectedFrequency"></div>
                               
                           
                               
                           <div class="clear"></div>
                           <div>
                           		The best ad position currently available at home page right column for <span id="ordering" class="bold"><span class="selectedState"></span>/<span class="selectedCounties"></span> is the  <span class="bestProduct"></span> </span>. The total amount you are paying today is  $<span class="totalValue"></span>. If you wish to make a different selection, you may do it below.
                           </div>
                           <div class="clear"></div>
                           <div class="clear"></div>
                           <div>                           		
                           <div class="centertext selections">
                                	<label class="required">State:</label><br>
                                    <select name="cbGrupoState" class="shortBox"  id="State" >
										<?php
                                            $sql="SELECT * FROM lsstate where is_showed='Y';";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                            }
                                        ?></select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoStateText" name="cbGrupoStateText">
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">County:</label><br>
                                    <select name="cbGrupoCounty" id="County" >
                                    	<option>Loading...</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoCountyText" name="cbGrupoCountyText">
                                </div>
                           		<div class="centertext selections">
                                	<input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" name="executive" id="executive">
                                	<label class="required">Ad Position:</label><br>
                                    <select name="cbGrupoProducts" style="width:140px;" id="product">
                                    	<option value="1">1st Position</option>
                                    	<option value="2">2nd Position</option>
                                    	<option value="3">3rd Position</option>
                                    	<option value="4">4th Position</option>
                                    	<option value="5">5th Position</option>
                                    	<option value="6">6th Position</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoProductsText" name="cbGrupoProductsText">
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">Frequency:</label><br>
                                    <select name="cbGrupoFrecuency" id="frencuency" class="shortBox">
                                   		<option value="1">Monthly</option>
                                    	<option value="3">Quarterly</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoFrecuencyText" name="cbGrupoFrecuencyText">
                                </div>

                           </div>
                           </div>
                           
                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2>
                        <?php echo ($_COOKIE['datos_usr'])? '': '<p>Already have an account? <a href="#" id="fastLogin" class="greentext">Log In...</a></p>'; ?>
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">I am a (an):</label>
                                </td>
                                <td>
                                	<select  class="fieldRequired" id="usertype" name="usertype">
                                    <option></option>
									<?php
                                            $sql="SELECT * FROM usertype where `show`=1 order by orden;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['advertiseRegister']['usertype'])?$_COOKIE['advertiseRegister']['usertype']:$datosUser['idusertype'];
											while($data=mysql_fetch_assoc($resultado))
											{
												echo ($var==$data['idusertype'])?'<option value="'.$data['idusertype'].'" selected>'.$data['usertype'].'</option>':'<option value="'.$data['idusertype'].'">'.$data['usertype'].'</option>';
											}
                                        ?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('sStateBText')?>" id="usertypeText" name="usertypeText">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="fieldLicense" style="display:none">
                            	<td>
                                	<label class="required">License #</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo printValue ('txtBroker',$datosUser,'LicenseN')?>" id="txtBroker" name="txtBroker">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="questionCompany" style="display:none">
                            	<td colspan="2">
                                	<label>Is your company registered width us?</label>
                                	<input type="radio" name="companyRegister" value="yes" class="radioButton"> <label>Yes</label>
                                    <input type="radio" name="companyRegister" value="no" class="radioButton" checked> <label> No</label>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="divOfficeNumber" style="display:none">
                            	<td>
                                	<label class="required">Office License #</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo printValue ('txtOffice',$datosUser,'BrokerN')?>" id="Office" name="txtOffice">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" value="<?php echo printValue ('nickname',$datosUser,'pseudonimo')?>" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail2',$datosUser,'EMAIL')?>" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php 
									$pass=printValue ('txtPassword',$datosUser,'PASS');
									echo (strlen($pass)>7 && $datosUser)?$pass.'" readonly="true': $pass;?>" id="password" name="txtPassword" maxlength="20">
								</td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 5 characters</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php 
									$pass=printValue ('txtPassword2',$datosUser,'PASS');
									echo (strlen($pass)>7 && $datosUser)?$pass.'" readonly="true': $pass;?>" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <?php
								if(!$_COOKIE['datos_usr'])
								{
									securityQuestion(printValue ('questionuser'), printValue ('answeruser'));
								}
							?>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['advertiseRegister'])
									{
										/*
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
										*/
										$aux->num=printValue ('txtPhoneNumber');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <!--
									<input type="text" value="<?php echo ($aux->cod)?$aux->cod:''?>" name="txtPhoneCod" class="short"> <label>-</label> 
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" name="txtPhoneNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo ($aux->ext)?$aux->ext:''?>" name="txtPhoneExt" class="short">
									-->
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" id="txtPhoneNumber" name="txtPhoneNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)" > 
                                </td>
                                <td>
                                	<!-- <span class="noteRegister">No spaces, dashes or punctuation</span>-->
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['advertiseRegister'])
									{
										/*
										$aux->cod=printValue ('MobileNumberCod');
										$aux->num=printValue ('MobileNumber');
										$aux->ext=printValue ('MobileNumberExt');
										*/
										$aux->num=printValue ('MobileNumber');
									}
									else
										$aux=($datosUser['MOBILETELEPHONE'])?formantPhone($datosUser['MOBILETELEPHONE']):'';
									?>
                                    <!--
									<input type="text" value="<?php echo ($aux->cod)?$aux->cod:''?>" name="MobileNumberCod" class="short"> <label>-</label> 
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo ($aux->ext)?$aux->ext:''?>" name="MobileNumberExt" class="short">
									-->
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" id="MobileNumber" name="MobileNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)" > 
                                </td>
                                <td>
                                	<!-- <span class="noteRegister">No spaces, dashes or punctuation</span>-->
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
              <!-- Panel business details -->
            	<div class="panel">
            		<div class="center register">
                        <h2>Company Information</h2>
                        <table><tr>
                                <td>
                                    <label class="required">Title in Organization</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" value="<?php echo printValue  ('Organizationtitle',$datosUser,'orgtitlein')?>" type="text" name="Organizationtitle">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Organization Name</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" value="<?php echo printValue  ('Organizationtitle',$datosUser,'orgname') ?>" type="text" name="OrganizationName">
                                </td>
                                <td>
                                    <span class="noteRegister" id="passwordNote">Your Name and Last Name if an Individual</span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Address</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" value="<?php echo printValue  ('OrganizationAddress',$datosUser,'orgaddress') ?>" type="text" name="OrganizationAddress">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                </td>
                                <td>
                                    <input class="" type="text" value="<?php echo printValue  ('OrganizationAddress2') ?>" name="OrganizationAddress2">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" value="<?php echo printValue  ('Organizationcity',$datosUser,'orgcity')?>" type="text" name="Organizationcity">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>                            
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td>
                                    <select class="fieldRequired" id="OrganizationState" name="OrganizationsState">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            $var=($_COOKIE['advertiseRegister']['OrganizationsState'])?$_COOKIE['advertiseRegister']['OrganizationsState']:$datosUser['orgstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
									?>				
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" value="<?php echo printValue  ('OrganizationzipCode',$datosUser,'orgzipcode')?>" type="text" name="OrganizationzipCode">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="required">Organization Phone</label>
                                </td>
                                <td class="numberPhone">
								<?php 
									if($_COOKIE['advertiseRegister'])
									{
										/*
										$aux->cod=printValue ('OrganizationNumberCod');
										$aux->num=printValue ('OrganizationNumber');
										$aux->ext=printValue ('OrganizationNumberExt');
										*/
										$aux->num=printValue ('OrganizationNumber');
									}
									else
										$aux=($datosUser['orgphone'])?formantPhone($datosUser['orgphone']):'';
									?>
									<!--
                                    <input value="<?php echo ($aux->cod)?$aux->cod:''?>"  type="text" name="OrganizationNumberCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>"  type="text" name="OrganizationNumberExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationNumber" id="OrganizationNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister" id="OrganizationNumberError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Organization Fax</label>
                                </td>
                                <td class="numberPhone">
                                <?php 
									if($_COOKIE['advertiseRegister'])
									{
										$aux->cod=printValue ('OrganizationFaxCod');
										$aux->num=printValue ('OrganizationFax');
										$aux->ext=printValue ('OrganizationFaxExt');
									}
									else
										$aux=($datosUser['orgfax'])?formantPhone($datosUser['orgfax']):'';
									?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>"  type="text" name="OrganizationFaxCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationFax"> <label>Ext</label> 
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>"  type="text" name="OrganizationFaxExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationFax" id="OrganizationFax" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister" id="OrganizationFaxError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label >Tax ID Number</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue ('OrganizationTaxId')?>" type="text" name="OrganizationTaxId">
                                </td>
                                <td>
	                                <span class="noteRegister">Your Social Security Number if an Individual</span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        
                        <div class="clear"></div>
                    </div>
                </div>
        <!--Panel end business details -->
        <div class="clear"></div>
            <div class="panel">
            	<div class="center register">
                <div class="addressPanel">	
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:260px;" class="centertext">
        		            		<h2>Billing Address</h2>Your billing address must match the 
                    		   		 address on your credit card statement
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue  ('sameAdd',$datosUser,'billingaddress')?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd2')?>" type="text" name="sameAdd2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue ('sameCity',$datosUser,'billingcity')?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <select class="fieldRequired" id="sStateB" name="sStateB">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            $var=($_COOKIE['advertiseRegister']['sStateB'])?$_COOKIE['advertiseRegister']['sStateB']:$datosUser['billingstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
                                        ?>				
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('sameZip',$datosUser,'billingzip')?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                            </tr>
                        </table>
                </div>
                
                
                <div class="addressPanel">
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:250px;" class="centertext">
        		            		<h2>Mailing Address</h2>
                                    	<input type="checkbox" style="margin-top:-5px;"  class="radioButton" id="copyAddress">
                                        Check if mailing address is the same as billing address
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table style="margin-top:-3px;">
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue ('txtAddress1',$datosUser,'ADDRESS')?>" class="fieldRequired" type="text" name="txtAddress1">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('txtAddress2')?>" type="text" name="txtAddress2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('txtCity',$datosUser,'CITY')?>" type="text" class="fieldRequired" name="txtCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                           <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>                                   
                                <select class="fieldRequired" id="sState" name="sState">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            $var=($_COOKIE['advertiseRegister']['sState'])?$_COOKIE['advertiseRegister']['sState']:$datosUser['STATE'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
									?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('txtZip',$datosUser,'zipcode')?>" class="fieldRequired" type="text" name="txtZip">
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                         <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('holder',$datosUser,'cardholdername')?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                        <?php 
                                        $var=($_COOKIE['advertiseRegister']['cardtype'])?$_COOKIE['advertiseRegister']['cardtype']:$datosUser['cardtype'];
										foreach ($cardtype AS $key => $value)
										{
											echo ($key==$var)?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';
										} 
										?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cardtypeText')?>" id="cardtypeText" name="cardtypeText">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('cardnumber',$datosUser,'cardnumber')?>" class="fieldRequired" id="cardNumber" type="text" name="cardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['advertiseRegister']['exdate1'])?$_COOKIE['advertiseRegister']['exdate1']:$datosUser['expirationdate'][0];
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$var)?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['advertiseRegister']['exdate2'])?$_COOKIE['advertiseRegister']['exdate2']:$datosUser['expirationdate'][1];
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$var)?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('csv',$datosUser,'csvnumber')?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div style="z-index:2; top:-190px;" class="notification">
                                    	<img src="../resources/img/CVV.dib" style="width:100%">
                                	</div>
                                </a>)
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" id="formProcessing" href="#">Continue to Confirmation Page<div id="errorForm" class="errorForm">Required fields are empty<div></div></div></a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
             <!--
             Filed hidden
             //-->
             <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
             <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
             <input readonly type="hidden" name="txtPhone" id="txtPhone"  value="">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>

</html>
<script language="javascript">
	var cbGrupoProducts="<?php echo printValue ('cbGrupoProducts')?>";
	var cbGrupoFrecuency="<?php echo printValue ('cbGrupoFrecuency')?>";
	var cbGrupoState="<?php echo printValue ('cbGrupoState')?>";
	var cbGrupoCounty="<?php echo printValue ('cbGrupoCounty')?>";
	var cargaInicial=true;
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){


	/**********************************
	*	FUNCIONES PARA LOS ICONOS PUBLICITARIOS
	****************************/
	$('.yourAdHere').each(function (){
		$(this).bind('click',function (){
			$('#product').attr('value',$(this).attr('href').replace('#',''))
			CustomerText();
		})
	})
	/*inicio de validaciones*/
	validationRegister();
	
		$("#customerRegisterForm").bind('submit',function (){
		$('#cbGrupoProductsText').attr('value',$('#product option:selected').html());
		$('input[name=price]').attr('value',$('.totalValue').html());
		$('#cbGrupoFrecuencyText').attr('value',$('#frencuency option:selected').html());
		$('#cbGrupoStateText').attr('value',$('#State option:selected').html());
		$('#cbGrupoCountyText').attr('value',$('#County option:selected').html());
		$('#usertypeText').attr('value',$('#usertype option:selected').html());
		$('#cardtypeText').attr('value',$('#cardtype option:selected').html());
	});
	CustomerLoadCouty();
	toConfimationPage('#formProcessing','#advertiseRegisterForm');
});
function CustomerAsignar()
{
	if(cbGrupoProducts)
	{
		$('#product').attr('value',cbGrupoProducts);
		$('#frencuency').attr('value',cbGrupoFrecuency);
		$('#State').attr('value',cbGrupoState);
		$('#County').attr('value',cbGrupoCounty);
		$('#usertype').trigger('change')
		CustomerText();
	}
	else if (cargaInicial)
	{
		$('#product').attr('value', (getUrlVars()['gp'])?getUrlVars()['gp']:1);
		$('#frencuency').attr('value',(getUrlVars()['gf'])?getUrlVars()['gf']:1);
		$('#State').attr('value',(getUrlVars()['gs'])?getUrlVars()['gs']:1);
		IniCounty.init($('#County'),CustomerText);
		$('#usertype').trigger('change')
	}
	cbGrupoProducts=null;
	cargaInicial=null;
}
function CustomerText()
{
	$('.AdactiveRegister').removeClass('AdactiveRegister');
	$('.yourAdHere[href=#'+$('#product').val()+']').addClass('AdactiveRegister');
	($('.yourAdHere[href=#'+$('#product').val()+']').is('.AddNotAvailebleRegister'))?$('#positionNotAvailable').fadeIn():$('#positionNotAvailable').fadeOut();
	$('.selectedProduct').html($('#product option:selected').html());
	$('.selectedFrequency').html($('#frencuency option:selected').html());
	$('.selectedState').html($('#State option:selected').html());
	$('.selectedCounties').html($('#County option:selected').html());
	$('.totalValue').html('');
	$('#inputTotalValue').attr('value','');
	$.ajax({
		url		:'../resources/php/properties.php',
		type	: 'get',
		dataType:'json',
		data	:'precioAdvertise=true&id='+$('#product').val()+'&county='+$('#County').val()+'&frencuency='+$('#frencuency').val()+'&promotionCode='+$('#promotionCode').val()+'&stade='+$('#State').val(),
		success:function (resul){
			$('.totalValue').html(resul.price);
			$('#inputTotalValue').attr('value',resul.price);
			$('#executive').attr('value',resul.userdescuento);
			}
		});	
}

/*Carga Condados*/
function CustomerLoadCouty()
{
$.ajax({
	type	:'POST',
	url		:'../resources/php/properties.php',
	data	: "ejecutar=countylist&state="+$('#State').val(),
	dataType:'json',
	success	:proccessCustomerLoadCouty
	});
};
function proccessCustomerLoadCouty (resul)
{
	$('#County').html('');
	$(resul).each(function (index,data){
		$('#County').append('<option value="'+data.id+'">'+data.county+'</option>');
	});
	CustomerLoadAvailable();
}
/*Carga disponibilidad*/
function CustomerLoadAvailable()
{
	CustomerAsignar();
		$.ajax({
			type	:'POST',
			url		:'../resources/php/properties.php',
			data	: "ejecutar=advertiseAvailable&state="+$('#State').val()+'&county='+$('#County').val(),
			dataType:'json',
			success	:function (resul)
				{
					var aux=true;
					$('.yourAdHere').removeClass('AddNotAvailebleRegister AdactiveRegister AddAvailebleRegister').each(function (index){
						$(this).addClass((resul[index+1])?'AddNotAvailebleRegister':'AddAvailebleRegister');
						if( !resul[index+1] && aux)
						{
							$('.bestProduct').html($('#product option[value='+(index+1)+']').html());
							aux=false;
						}
					})
					$('.yourAdHere[href=#'+$('#product').val()+']').addClass('AdactiveRegister');
				}
			});
};
$('#promotionCode').bind('blur',CustomerText);
$('#product, #frencuency, #State, #County').bind('change',CustomerText);
$('#State').bind('change',CustomerLoadCouty);
$('#County').bind('change',CustomerLoadAvailable);


/*
	Funciones para el check box
*/

$("#copyAddress").change(function(){
	if($(this).is(':checked'))
	{
		$("input[name=txtAddress1]").attr('value',$("input[name=sameAdd]").val());
		$("input[name=txtAddress2]").attr('value',$("input[name=sameAdd2]").val());
		$("input[name=txtZip]").attr('value',$("input[name=sameZip]").val());
		$("input[name=txtCity]").attr('value',$("input[name=sameCity]").val());
		$("#sState").attr('value',$("#sStateB").val());		
		$('.addressPanel .fieldRequired').trigger('blur');
	}
	else
	{
		$("input[name=txtAddress1]").attr('value','');
		$("input[name=txtAddress2]").attr('value','');
		$("input[name=txtZip]").attr('value','');
		$("input[name=txtCity]").attr('value','');
		$("#sState option").removeAttr('selected');
	}
});
/*
	tamaños del bloque publicitario
*/

 if ($.browser.webkit) {
	 $('select[name="exdate2"], select[name="exdate1"]').css('width','75px');
  }

/*highlight menu*/
 menuClick('menu-advertise');
 /* tooltips */
 tooltips();
</script>