<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	foreach($_POST AS $key => $value)
	{
		setcookie('advertiseRegister['.$key.']',$value,0,'/');
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="http://www.reifax.com/store/index.php"><span class="bluetext underline">Store</span></a> &gt; 
        <a href="advertiseRegister.php"><span class="bluetext underline">Advertise Register</span></a> &gt; 
        	<span class="fuchsiatext">Confirm</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Advertiser Register Confirmation - Please review and complete your order</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" class="formPrincipal" action="newUserAdvertise.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Adrvertising Information</h2>
                        <div style="text-align:right;" class="BoxTextSoftGray" style="display:none;visibility:hidden;">
                                <!--<span>Promotion Code </span>-->
                                <input readonly type="hidden"  value="<?php echo $_POST['prom']?>" class="shortBox promotionCode" name="prom" id="promotionCode">
                        </div>
                           <div id="ProductInformation">
                           <div class="bold">
                                    Details of your product selection: Total $<span class="totalValue"><?php echo $_POST['price']?></span>
									<input type="hidden" id="montoapagar" name="montoapagar" value="<?php echo $_POST['price']?>">
                               </div>
                           <div class="clear"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>State:
                               </div>
                               <div id="selectedState" class="contentDetailsProduct selectedState"><?php echo $_POST['cbGrupoStateText']?></div>
                               <input value="<?php echo $_POST['cbGrupoState']?>" type="hidden" name="cbGrupoState">
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>County:
                               </div>
                               <div  id="selectedCounties" class="contentDetailsProduct selectedCounties"><?php echo $_POST['cbGrupoCountyText']?></div>
                               <input value="<?php echo $_POST['cbGrupoCounty']?>" type="hidden" name="cbGrupoCounty">
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Ad Position:
                               </div>
                               <div id="selectedProduct" class="contentDetailsProduct selectedProduct"><?php echo $_POST['cbGrupoProductsText']?></div>
                               <input value="<?php echo $_POST['cbGrupoProducts']?>" type="hidden" name="cbGrupoProducts">
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Frequency:
                               </div>
                               <div id="selectedFrequency" class="contentDetailsProduct selectedFrequency"><?php echo $_POST['cbGrupoFrecuencyText']?></div>
                               <input value="<?php echo $_POST['cbGrupoFrecuency']?>" type="hidden" name="cbGrupoFrecuency">
                               
                               
                           <div class="clear"></div>
                           </div>
                           
                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2>
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="">I am a (an):</label>
                                </td>
                                <td>
                                	<input readonly type="text" value="<?php echo $_POST['usertypeText']?>" id="usertype" name="usertype">
                               <input value="<?php echo $_POST['usertype']?>" type="hidden" name="usertype">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr style="display:<?php echo ($_POST['txtBroker'])? '':'none'; ?>">
                            	<td>
                                	<label class="">License #</label>
                                </td>
                                <td>
                                	<input readonly type="text" value="<?php echo $_POST['txtBroker']?>" id="txtBroker" name="txtBroker">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr style="display:<?php echo ($_POST['txtOffice'])? '':'none'; ?>">
                            	<td>
                                	<label class="">Office License #</label>
                                </td>
                                <td>
                                	<input readonly type="text" value="<?php echo $_POST['txtOffice']?>" id="Office" name="txtOffice">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">First Name</label>
                                </td>
                                <td>
                                	<input  type="text" id="firstName" value="<?php echo $_POST['txtFirstName']?>" name="txtFirstName">
                                </td>
                                <td>                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Last Name</label>
                                </td>
                                <td>
                                	<input  type="text" id="lastName" value="<?php echo $_POST['txtLastName']?>" name="txtLastName">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input readonly type="text" id="nickname" value="<?php echo $_POST['nickname']?>" name="nickname">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">E-mail Address</label>
                                </td>
                                <td>
                                	<input  type="text" value="<?php echo $_POST['txtEmail']?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Password</label>
                                </td>
                                <td>
                                	<input  type="text" value="<?php echo $_POST['txtPassword']?>" id="password" name="txtPassword">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <?php
								if(!$_COOKIE['datos_usr'])
								{ ?>
                        	<tr>
                            	<td>
                                	<label class="">Security Question</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo $_POST['questionuser']?>" name="questionuser">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Your answer</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo $_POST['answeruser']?>" name="answeruser">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <label class="">Phone Number</label>
                                </td>
                                <td>
                                	<input readonly type="text" name="txtPhone" value="<?php echo $_POST['txtPhoneCod'].$_POST['txtPhoneNumber'].$_POST['txtPhoneExt']?>">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td>
                                	<input readonly type="text" name="txtMobil" value="<?php echo $_POST['MobileNumberCod'].$_POST['MobileNumber'].$_POST['MobileNumberExt']?>">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
              <!-- Panel business details -->
            	<div class="panel">
            		<div class="center register">
                        <h2>Company Information</h2>
                         <table><tr>
                                <td>
                                    <label class="">Title in Organization</label>
                                </td>
                                <td>
                                    <input  value="<?php echo $_POST['Organizationtitle']?>"  type="text" name="Organizationtitle">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">Organization Name</label>
                                </td>
                                <td>
                                    <input   value="<?php echo $_POST['OrganizationName']?>" type="text" name="OrganizationName">
                                </td>
                                <td>
                                    <span class="noteRegister" id="passwordNote">Your Name and Last Name if an Individual</span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">Address</label>
                                </td>
                                <td>
                                    <input  value="<?php echo $_POST['OrganizationAddress']?>" type="text" name="OrganizationAddress">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                </td>
                                <td>
                                    <input  value="<?php echo $_POST['OrganizationAddress2']?>" type="text" name="OrganizationAddress2">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">City</label>
                                </td>
                                <td>
                                    <input   value="<?php echo $_POST['Organizationcity']?>" type="text" name="Organizationcity">
                                </td>
                                <td>
                                </td>
                            </tr>                            
                            <tr>
                                <td>
                                    <label class="">State</label>
                                </td>
                                <td>
                                    <input   value="<?php echo $_POST['OrganizationsState']?>"  name="OrganizationsState">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">ZIP Code</label>
                                </td>
                                <td>
                                    <input   value="<?php echo $_POST['OrganizationzipCode']?>"  type="text" name="OrganizationzipCode">
                                </td>
                                <td>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="">Organization Phone</label>
                                </td>
                                <td>
                                    <input  type="text"  value="<?php echo $_POST['OrganizationNumberCod'].$_POST['OrganizationNumber'].$_POST['OrganizationNumberExt']?>" name="OrganizationPhoneNumber">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">Organization Fax</label>
                                </td>
                                <td>
                                    <input   value="<?php echo $_POST['OrganizationFaxCod'].$_POST['OrganizationFax'].$_POST['OrganizationFaxExt']?>"  type="text" name="OrganizationFaxNumber">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">Tax ID Number</label>
                                </td>
                                <td>
                                    <input  type="text"  value="<?php echo $_POST['OrganizationTaxId']?>" name="OrganizationTaxId">
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        
                        <div class="clear"></div>
                    </div>
                </div>
        <!--Panel end business details -->
            
            <div class="clear"></div>
            <div class="panel">
            	<div class="center register">
                <div class="addressPanel">	
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:250px;" class="centertext">
        		            		<h2>Billing Address</h2>
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input readonly value="<?php echo $_POST['sameAdd']?>"  type="text" name="sameAdd">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['sameAdd2']?>" type="text" name="sameAdd2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input readonly value="<?php echo $_POST['sameCity']?>"  type="text" name="sameCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                               		<input readonly value="<?php echo $_POST['sStateB']?>" type="text"  name="sStateB">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input readonly value="<?php echo $_POST['sameZip']?>"  type="text" name="sameZip">
                                </td>
                            </tr>
                        </table>
                </div>
                
                
                <div class="addressPanel">
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:250px;" class="centertext">
        		            		<h2>Mailing Address</h2>
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table style="margin-top:-3px;">
                        	<tr>
                            	<td>
                                	<label class="">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input readonly value="<?php echo $_POST['txtAddress1']?>"  type="text" name="txtAddress1">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['txtAddress2']?>" type="text" name="txtAddress2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input readonly value="<?php echo $_POST['txtCity']?>" type="text"  name="txtCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div> 
                                <input readonly value="<?php echo $_POST['sState']?>" type="text"  name="sState">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input readonly value="<?php echo $_POST['txtZip']?>"  type="text" name="txtZip">
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="">Full Name on Card</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['holder']?>"  type="text" name="holder">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Type of Credit Card</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['cardtypeText']?>"  type="text">
                                    <input value="<?php echo $_POST['cardtype']?>"  type="hidden" name="cardtype">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Credit Card Number</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['cardnumber']?>"  id="cardNumber" type="text" name="cardnumber">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><input readonly value="<?php echo $_POST['exdate1']?>"  type="text" name="exdate1">
                                    <label>&nbsp;&nbsp;Year&nbsp;</label>
                                	<input readonly value="<?php echo $_POST['exdate2']?>"  type="text" name="exdate2">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['csv']?>" type="text" name="csv" class="field shortBox">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
             <div class="panel">
            	<div class="center register">
                        <h2>Order Authorization </h2>
                        <div class="clear"></div>
                        <p style="padding:0px 15px; text-align:justify;">
                        By clicking the "<span class="bold">Complete Order</span>" button, I authorize to charge the credit card indicated in this
form according to the <a href="../resources/pdf/REIFax_Advertiser_Terms_and_Conditions.pdf" target="_blank" class="bluetext underline">Advertiser Terms and Conditions</a>. I certify that I am an authorized user of this credit
card. Also, by clicking the "<span class="bold">Complete Order</span>" button, I hereby agree to REIFax
 <a href="../resources/pdf/REIFax_Advertiser_Terms_and_Conditions.pdf" target="_blank" class="bluetext underline">Advertiser Terms and Condictions</a> and <a href="../resources/pdf/REIFax_Privacy_Policy.pdf" target="_blank" class="bluetext underline">Privacy Policy.</a>.</p>
                        <div class="clear"></div>
                        <div class="centertext"> 
                        	<a class="buttongreen bigButton" href="advertiseRegister.php">Back to Edit</a>
                        	<a class="buttonblue bigButton processForm" id="formProcessing" href="#">Complete Order</a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
            <!---
            Hidden input
            
             //-->

                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo  $_POST['idcompany']?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo  $_POST['USERID'] ?>">        
              </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>
</html>
<script language="javascript"> 
$('.processForm').bind('click',function ()
{
	$('.formPrincipal').trigger('submit');
	return false;

})	
$('.formPrincipal').bind('submit',function (){
	$('.lockScreen').fadeTo(100,0.5);
	$('.MsglockScreen').fadeIn(100);
		$.ajax({
            type	: 'POST',
            url		: $(this).attr('action'),
            data	: $(this).serialize(),
			dataType: 'json',
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					$('.MsglockScreen').html('<a href="http://www.reifax.com/company/welcomeConsumer.php" class="buttonblue cerrarVentana">Ok...</a><div class="clear"></div>'+respuesta.mensaje);
					setTimeout(function (){
						document.location.href="http://www.reifax.com/company/welcomeConsumer.php"}
						,6000);
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html('<a href="#" class="buttonred cerrarVentana">Return</a><div class="clear"></div>'+respuesta.mensaje);
					$('.cerrarVentana').bind('click',function (){
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('<img src="../resources/img/ac.gif">  Registration processing...');
						})
				}
			}
			})
	return false;
})
 /* tooltips */
 tooltips();
</script>