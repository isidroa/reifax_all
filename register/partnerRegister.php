<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
   	conectar('xima');
	if($_COOKIE['datos_usr'])
	{
		$sql='select * from ximausrs u LEFT JOIN usr_companyinformation c ON c.userid=u.userid where u.userid='.$_COOKIE['datos_usr']['USERID'].' limit 1';
		$query=mysql_query($sql);
		$dataUser=mysql_fetch_array($query);
	}
	function printValue($field, $array)
	{
		return ($array[$field])?$array[$field]:'';
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="http://www.reifax.com/company/aboutUs.php"><span class="bluetext underline">Company</span></a> &gt; 
        <a href="http://www.reifax.com/company/become.php"><span class="bluetext underline">Become a Partner</span></a> &gt; 
        	<span class="fuchsiatext">Partner Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Partner Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form action="newUserPartner.php" id="partnerRegister">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <h2>Create an Account</h2>
                        <div class="BoxTextLoudGray bold">
                                Optional - Affiliate Partner Promotion Code
                           </div>
                           <div class="BoxTextSoftGray">
                                <div style="width:470px; float:left"><span>If you are being promoted by another Partner, enter his/her Account Executive Code in the box to the right</span></div>
                                <div style="float:left">
                                <input type="text" class="shortBox mediumMarginLef promotionCode" value="<?php echo ($_COOKIE['procode'])?$_COOKIE['procode']:printValue ('prom')?>"  name="prom" id="promotionCode"></div>
                                <div class="clearEmpty"></div>
                           </div>
                           <div class="clear"></div>
                        <?php echo ($dataUser)? '': '<p>Already have an account? <a href="#" id="fastLogin" class="greentext">Log In...</a></p>'; ?>
                        <table>
                           <tr>
                            	<td>
                                	<label class="required">I am a (an):</label>
                                </td>
                                <td>
                                	<select  class="fieldRequired" id="usertype" name="usertype">
                                    <option></option>
									<?php
                                        conectar('xima');
                                            $sql="SELECT * FROM usertype where `show`=1 order by orden;";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
												echo (printValue('idusertype', $dataUser)==$data['idusertype'])?'<option selected value="'.$data['idusertype'].'">'.$data['usertype'].'</option>':'<option value="'.$data['idusertype'].'">'.$data['usertype'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="fieldLicense" style="display:none">
                            	<td>
                                	<label class="required">License #</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue('LicenseN', $dataUser)?>" type="text"id="txtBroker" name="txtBroker">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="questionCompany" style="display:none">
                            	<td colspan="2">
                                	<label>Is your company registered width us?</label>
                                	<input type="radio" name="companyRegister" value="yes" class="radioButton"> <label>Yes</label>
                                    <input type="radio" name="companyRegister" value="no" class="radioButton" checked> <label> No</label>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="divOfficeNumber" style="display:none">
                            	<td>
                                	<label class="required">Office License #</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue('BrokerN', $dataUser)?>" type="text" id="Office" name="txtOffice">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('NAME', $dataUser)?>" type="text" id="firstName" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('SURNAME', $dataUser)?>" type="text" id="lastName" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" value="<?php echo printValue('pseudonimo', $dataUser)?>" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('EMAIL', $dataUser).'"'; echo ($dataUser['EMAIL'])?'readonly="readonly"': '' ?> type="text" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('EMAIL', $dataUser).'"'; echo ($dataUser['EMAIL'])?'readonly="readonly"': ''?> type="text" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('PASS', $dataUser).'"'; echo ($dataUser['PASS'])?'readonly': ''?> type="password" id="password" name="txtPassword" maxlength="20">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 5 characters </span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" value="<?php echo printValue('PASS', $dataUser).'"'; echo ($dataUser['PASS'])?'readonly': ''?>  type="password" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
                                	<?php $aux=($dataUser['HOMETELEPHONE'])?formantPhone($dataUser['HOMETELEPHONE']):''; ?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>" type="text" name="txtPhoneCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" type="text" name="txtPhoneExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" id="txtPhoneNumber" name="txtPhoneNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
                                	<?php $aux=($dataUser['MOBILETELEPHONE'])?formantPhone($dataUser['MOBILETELEPHONE']):''; ?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>" type="text" name="MobileNumberCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" type="text" name="MobileNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" type="text" name="MobileNumberExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" type="text" name="MobileNumber" id="MobileNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                        
                        <div class="clear"></div>
                    </div>
                </div>
             <div class="clear"></div>
             <!-- end of the panel of personal information -->
             <!-- Panel business details -->
            	<div class="panel">
            		<div class="center register">
                        <h2>Company Information</h2>
                        <table><tr>
                                <td>
                                    <label class="required">Title in Organization</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" type="text" value="<?php echo printValue('orgtitlein', $dataUser)?>"  name="Organizationtitle">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Organization Name</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" type="text" value="<?php echo printValue('orgname', $dataUser)?>"  name="OrganizationName">
                                </td>
                                <td>
                                    <span class="noteRegister" id="passwordNote">Your Name and Last Name if an Individual</span>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Address</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" type="text" value="<?php echo printValue('orgaddress', $dataUser)?>"  name="OrganizationAddress">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                </td>
                                <td>
                                    <input type="text"  name="OrganizationAddress2">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                    <input class="fieldRequired" type="text" value="<?php echo printValue('orgcity', $dataUser)?>"  name="Organizationcity">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>                            
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td>
                                    <select class="fieldRequired" id="sState" name="OrganizationsState">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo (printValue('orgstate', $dataUser)==$data['name'])?'<option selected value="'.$data['name'].'">'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
											}
									?>				
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label  class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue('orgzipcode', $dataUser)?>"  class="fieldRequired" type="text" name="OrganizationzipCode">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="required">Organization Phone</label>
                                </td>
                                <td class="numberPhone">
                                	<?php $aux=($dataUser['orgphone'])?formantPhone($dataUser['orgphone']):''; ?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>"  type="text" name="OrganizationNumberCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>"  type="text" name="OrganizationNumberExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationNumber" id="OrganizationNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister" id="OrganizationNumberError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Organization Fax</label>
                                </td>
                                <td class="numberPhone">
                                	<?php $aux=($dataUser['orgfax'])?formantPhone($dataUser['orgfax']):''; ?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>"  type="text" name="OrganizationFaxCod" class="short"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationFax"> <label>Ext</label> 
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>"  type="text" name="OrganizationFaxExt" class="short">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>"  class="fieldRequired" type="text" name="OrganizationFax" id="OrganizationFax" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister" id="OrganizationFaxError"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label  class="required">Tax ID Number</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue('orgtaxidnumber', $dataUser)?>"  class="fieldRequired" type="text" name="OrganizationTaxID">
                                </td>
                                <td>
	                                Your Social Security Number if an Individual
                                </td>
                            </tr>
                        </table>
                        
                        <div class="clear"></div>
                    </div>
                </div>
        <!--Panel end business details -->
        <div class="clear"></div>
        <!-- Panel Website details -->
            	<div class="panel">
            		<div class="center register">
                        <h2>Promotional Methods Information</h2>
                        <table>
                            <tr>
                                <td>
                                    <label>Web Site Name</label>
                                </td>
                                <td>
                                    <input type="text" name="webSitName">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Web Site Url</label>
                                </td>
                                <td>
                                    <input type="text" name="SiteUrl">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Please provide a brief description of your Site</label>
                                </td>
                                <td>
                                    <textarea name="descriptionSite"></textarea>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Monthly unique visitors</label>
                                </td>
                                <td>
                                    <select class="right" name="monthly">
                                    	<option value="1-50,000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000</option>
                                    	<option value="50,001-100,000">&nbsp;&nbsp;&nbsp;50,001&nbsp;&nbsp;-<span style=" display:block; width:300px;">100,000</span></option>
                                    	<option value="100,001-250,000">&nbsp;&nbsp;100,001&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;250,000</option>
                                    	<option value="250,001-500,000">&nbsp;&nbsp;250,001&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;500,000</option>
                                    	<option value="500,001-750,000">&nbsp;&nbsp;500,001&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;750,000</option>
                                    	<option value="750,001-1,000,000">&nbsp;&nbsp;750,000&nbsp;&nbsp;- 1,000,000</option>
                                    	<option value="1,000,000-MORE">1,000,000  -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp+</option>
                                    </select>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Promotional Methods (Select all you use)</label>
                                </td>
                                <td class="borderLeft">
                                   	<div> <input type="checkbox" class="radioButton" name="webSite"><label>Web Site/Content</label></div>
                                    <div><input type="checkbox" class="radioButton"  name="Banners"><label>Banners</label></div>
                                    <div><input type="checkbox"  class="radioButton" name="EmailMarketing"><label>Email Marketing</label></div>
                                    <div><input type="checkbox" class="radioButton"  name="SearchEngineMarketing"><label>Search Engine Marketing</label></div>
                                    <div><input type="checkbox" class="radioButton"  name="SocialNetworking"><label>Social Networking</label></div>
                                    <div><input type="checkbox" class="radioButton"  name="Other"><label>Other</label></div>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        
                        <div class="clear"></div>
                    </div>
                </div>
	    		<div class="clear">&nbsp;</div>
                <div class="panel">
                	<div class="center register">
                <h2>Terms of Service</h2>
                <p>
                By clicking the "Sign Me Up Now" button below, I acknowledge, represent and warrant that I have the authority to act on behalf of the Company (if a company) entering into the <a href="../resources/pdf/REIFax_Affiliate_Partner_Terms_and_Conditions.pdf" target="_blank" class="bluetext underline">Affiliate Partner Terms and Conditions</a>, and I acknowledge, represent and warrant that all information is true, complete, and accurate, and I will keep all such information current with REIFax.</p><p>

By clicking the "Sign Me Up Now" button below, I confirm I have read, understand, acknowledge and accept the <a href="../resources/pdf/REIFax_Affiliate_Partner_Terms_and_Conditions.pdf" target="_blank" class="bluetext underline">Affiliate Partner Terms and Conditions</a>, and I agree to be bound by the terms of the REIFax Affiliate Partner Service Agreement.
</p><p>
By clicking the "Sign Me Up Now" button below, I confirm that I have read, understand, acknowledge, and agree to the <a href="../resources/pdf/REIFax_Privacy_Policy.pdf" target="_blank" class="bluetext underline">Privacy Policy.</a>
</p><p>
By clicking the "Sign Me Up Now" button below, I acknowledge and warrant that I am at least 18 years of age.

                </p>
                	<div class="centertext">
                		<a href="#" id="processForm" class="buttonblue bigButton">Sign Me Up Now</a>
                   </div>
	    		<div class="clear">&nbsp;</div>
            	</div>
        	</div>
                     <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp', $dataUser)?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
                    <input type="hidden" id="source" name="source" value="RegisterPartner">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
<img src="../resources/img/ac.gif">  Registration processing...
</div>

</html>
<script language="javascript">
 menuClick('menu-company');
 $('#learnNotificationSSL').bind('mouseenter',function (){
	 $('#NotificationSSL').fadeIn(200);
	 }).parent().parent().bind('mouseleave',function (){$('#NotificationSSL').fadeOut(200);})
	 
$(document).ready(function (){
	 validationRegister();
	
	 
	
	$('#usertype').trigger('change');
	/***
	validaciones de campos requeridos
	************/
	$('#processForm').bind('click',function (){
		$('.fieldRequired').each(function (){
				$(this).trigger('blur')
			})
		if($('.ErrorField').length<1)
				{
				$.ajax({
					type:'POST',
					url	: "../resources/php/datarecord.php",
					data	:'action=record&statusRegister=Success&'+$('#partnerRegister').serialize(),
					success	:function (result)
							{		
								$('#partnerRegister').submit();
							}
				});
				}
				else
				{
				$.ajax({
					type:'POST',
					url	: "../resources/php/datarecord.php",
					data	:'action=record&statusRegister=Failure&'+$('#partnerRegister').serialize()
				});
					$('#errorForm').stop(true,true).css({opacity:0,display:'block'}).animate({
								top			:'-65px',
								opacity		:1
							},400).delay(3000).animate({
								top		:'-55px'
							},200,function (){
								$(this).animate({
									top		:'-100px',
									opacity	:0
								},400,function (){
									$(this).css({display:'none'});
								});
					});
					return false;
				}	 
		})
	$('#partnerRegister').bind('submit',function ()
	{
		$('.MsglockScreen').fadeIn(200);
		$('.lockScreen').fadeTo(200,0.5);
		/*$("#txtPhone").attr('value',$('.numberPhone:eq(0) input:eq(0)').val()+$('.numberPhone:eq(0) input:eq(1)').val()+$('.numberPhone:eq(0) input:eq(2)').val());
		$("#txtMobil").attr('value',$('.numberPhone:eq(1) input:eq(0)').val()+$('.numberPhone:eq(1) input:eq(1)').val()+$('.numberPhone:eq(1) input:eq(2)').val());
		$("#txtPhoneOrg").attr('value',$('.numberPhone:eq(2) input:eq(0)').val()+$('.numberPhone:eq(2) input:eq(1)').val()+$('.numberPhone:eq(2) input:eq(2)').val());
		$("#txtFaxOrg").attr('value',$('.numberPhone:eq(3) input:eq(0)').val()+$('.numberPhone:eq(3) input:eq(1)').val()+$('.numberPhone:eq(3) input:eq(2)').val());*/
		$.ajax({
			 type	: 'POST',
            url		: $(this).attr('action'),
			dataType: 'json',
            data	: $(this).serialize(),
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					$('.MsglockScreen').html(respuesta.mensaje+'<div class="clear"></div><a href="../company/welcomePartner.php?proCode='+respuesta.userid+'" class="buttonblue cerrarVentana">Ok...</a>');
					setTimeout(function (){
						document.location.href="../company/welcomePartner.php?proCode="+respuesta.userid}
						,6000);
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html(respuesta.mensaje+'<div class="clear"></div><a href="#" class="buttonred cerrarVentana">Ok..</a>');
					$('.cerrarVentana').bind('click',function (){
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('<img src="../resources/img/ac.gif">  Registration processing...');
						})
				}
			}
		});
		return false;
	}) 

}) 
</script>