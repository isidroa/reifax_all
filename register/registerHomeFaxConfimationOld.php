<?php 
	session_start();
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	foreach($_POST AS $key => $value)
	{
		setcookie('customerRegister['.$key.']',$value,0,'/');
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>
<style type="text/css">
	#ProductInformation li{
		list-style:none;
	}
</style>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#" class="returnResul"><span class="bluetext underline">Result</span></a> &gt; 
        <a href="registerHomeFax.php"><span class="bluetext underline">HomeFax Order</span></a> &gt; 
        	<span class="fuchsiatext">Confirm</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
   </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">HomeFax Order Confirmation- Please review and complete your order</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" class="formPrincipal" action="newUserReport.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Product Information</h2>
                           <div id="ProductInformation">
                           <p class="bold" style="padding-left:0px">
                           By buying the Home Fax Report for the property specified below, you will get the following information:
                           </p>
                           <div style="float:left;width: 250px;">
                               <ul>
                                    <li>
                                        <div class="cuote"></div>Public Records
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Active Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Non Active Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Distressed Comparables
                                    </li>
                                    <li>
                                        <div class="cuote"></div>Rental Comparables (if any)
                                    </li>
                               </ul>
                           </div>   
                           <div style="float:left;width: 250px;">  
                           <ul>
                           		<li>
                                	<div class="cuote"></div>Owner Information
                                </li>
                                <li>
                                	<div class="cuote"></div>Mortgage Information (if any)
                                </li>
                                <li>
                                	<div class="cuote"></div>Foreclosure Information (if any)
                                </li>
                           </ul>
                           </div>
                           <div class="clear"></div>
                           <div class="bold">
                                    Details of your Property selection:
									<input type="hidden" id="montoapagar" name="montoapagar" value="15">
                               </div>
                           <div class="clear"></div>
                              
                           <div class="titleDetailsProduct">
                               <div class="cuote"></div>Property:
                               </div>
                               <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $_SESSION['property'] ?></div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>County:
                               </div>
                               <div id="selectedProduct" class="contentDetailsProduct selectedProduct"><?php echo $_SESSION['county'] ?></div>
                               
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Price:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct">$9.97</div>
                           </div>
                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2>
                        <div class="clear"></div>
                        <table>
                           	<tr>
                            	<td>
                                	<label class="">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" value="<?php echo $_POST['txtFirstName']?>" name="txtFirstName">
                                </td>
                                <td>                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" value="<?php echo $_POST['txtLastName']?>" name="txtLastName">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo $_POST['txtEmail']?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">Phone Number</label>
                                </td>
                                <td>
                                	<input readonly type="text" name="txtPhone" value="<?php echo $_POST['txtPhoneCod'].$_POST['txtPhoneNumber'].$_POST['txtPhoneExt']?>">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td>
                                	<input readonly type="text" name="txtMobil" value="<?php echo $_POST['MobileNumberCod'].$_POST['MobileNumber'].$_POST['MobileNumberExt']?>">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
            		<h2>Billing Address</h2>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input readonly value="<?php echo $_POST['sameAdd']?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['sameAdd2']?>" type="text" name="sameAdd2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input readonly value="<?php echo $_POST['sameCity']?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                               		<input readonly value="<?php echo $_POST['sStateB']?>" type="text" class="fieldRequired" name="sStateB">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input readonly value="<?php echo $_POST['sameZip']?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                            </tr>
                        </table>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="">Full Name on Card</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['holder']?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Type of Credit Card</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['cardtypeText']?>" class="fieldRequired" type="text">
                                    <input value="<?php echo $_POST['cardtype']?>" class="fieldRequired" type="hidden" name="cardtype">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Credit Card Number</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['cardnumber']?>" class="fieldRequired" id="cardNumber" type="text" name="cardnumber">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><input readonly value="<?php echo $_POST['exdate1']?>" class="fieldRequired" type="text" name="exdate1">
                                    <label>&nbsp;&nbsp;Year&nbsp;</label>
                                	<input readonly value="<?php echo $_POST['exdate2']?>" class="fieldRequired" type="text" name="exdate2">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input readonly value="<?php echo $_POST['csv']?>" type="text" name="csv" class="fieldRequired shortBox">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
             <div class="panel">
            	<div class="center register">
                        <h2>Order Authorization </h2>
                        <div class="clear"></div>
                        <p style="padding:0px 15px; text-align:justify;">
                        
                        By clicking the "<span class="bold">Complete Order</span>" button, I authorize to charge the credit card indicated in this form. 
                        I certify that I am an authorized user of this credit card. Also, by clicking the "<span class="bold">Complete Order</span>" button, I hereby certify that I have read the <a class="bluetext underline" href="../resources/pdf/Product_Disclaimer.pdf" target="_blank">Product Disclaimer.</a>                  
                        
</p>
                        <div class="clear"></div>
                        <div class="centertext"> 
                        	<a class="buttongreen bigButton" id="backEdit" href="#">Back to Edit</a>
                        	<a class="buttonblue bigButton processForm" id="formProcessing" href="#">Complete Order</a>
                        </div>
                        <div class="clear">
                        </div>
            <!--
                     Filed hidden
                     //-->
                    <input type="hidden" id="bkou" name="bkou" size="5" value="<?php echo $_GET['bkou'] ?>" />
                    <input type="hidden" id="hdAccept" name="hdAccept" value="<?php echo $rbko; ?>">
                    <input type="hidden" id="freeactive" name="freeactive" value="<?php echo $freeactive; ?>">
                    <input type="hidden" id="rbko" name="rbko" value="<?php echo $_POST['rbko']; ?>">
                    <input type="hidden" id="freedays" name="freedays" value="<?php echo $freedays ?>">
                    <input type="hidden" id="ui" name="ui" value="<?php echo $ui; ?>">
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo  $_POST['idcompany']?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo  $_POST['USERID'] ?>">
                </div>
        	</div>
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
Registration processing...
</div>
</html>
<script language="javascript"> 
$('.formPrincipal').bind('submit',function (){
	$('.returnResul').attr('href',$.cookie('urlResult'));
	
	$('.lockScreen').fadeTo(100,0.5);
	$('.MsglockScreen').fadeIn(100);
		$.ajax({
            type	: 'POST',
            url		: $(this).attr('action'),
			dataType: 'json',
            data	: $(this).serialize(),
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					$('.MsglockScreen').html(respuesta.mensaje+'<div class="clear"></div><a href="'+respuesta.url+'" target="_blank" class="buttonblue cerrarVentana">Download</a>');
					$('.cerrarVentana').bind('click',function (e){
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('Registration processing...');
					});
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html(respuesta.mensaje+'<div class="clear"></div><a href="#" class="buttonred cerrarVentana">Return</a>');
					$('.cerrarVentana').bind('click',function (e){
						e.preventDefault();
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('Registration processing...');
						})
				}
			}
		})
	return false;
});
$('.processForm').bind('click',function ()
{
	$('.formPrincipal').trigger('submit');
	return false;
})
$('#backEdit').bind('click',function (e){
	e.preventDefault()
	history.go(-1);
})
 /* tooltips */
 tooltips();
</script>