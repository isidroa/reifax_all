<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	require_once('../backoffice/php/mcripty.php');
   	conectar('xima');
	session_start();
	
	
		/***************
	*	PRODUCTOS, FREQUENCIA, FECHA DEL PAGO del usuario
	***************/
	$sql='SELECT p.homefinder, p.buyerspro, p.leadsgenerator, p.residential, p.platinum, p.professional, p.professional_esp,f. idfrecuency frecuency ,c.fechacobro
		FROM permission p,f_frecuency f ,usr_cobros c
		where p.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.userid=f.userid AND p.userid=c.userid limit 1';
	$respuesta=mysql_query($sql);
	if(mysql_num_rows($respuesta)>0)
	{
		$status=mysql_fetch_assoc($respuesta);
		if(($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1) && $status['frecuency']==2)
		{
			header ("Location: registerWorkshopFree.php");
		}
	}
		
	$sql='SELECT * FROM workshop WHERE status=1 LIMIT 1;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$data=mysql_fetch_assoc($res);
	$sql='SELECT * FROM xima.usr_productobase where idproducto=13 AND activo=1 LIMIT 1	;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$cost=mysql_fetch_assoc($res);
	

	
	if(!$_COOKIE['customerRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosUser=mysql_fetch_array($query);
		$query=mysql_query('select * from creditcard where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosBanc=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['customerRegister'])
			return $_COOKIE['customerRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>
<style type="text/css">
	#ProductInformation li{
		list-style:none;
	}
	.titleDetailsProduct {
		margin-top: 10px;
	}
	.contentDetailsProduct {
		height: auto !important;
		margin-top: 10px;
		width: 400px !important;
	}
</style>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/xima3/websiteindex.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../workshop/" class="returnResul"><span class="bluetext underline">WorkShop</span></a> &gt; 
        	<span class="fuchsiatext">WorkShop Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">WorkShop Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" action="registerWorkshopConfimation.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Workshop Information</h2>
                          <div id="ProductInformation">
                           <div class="clear"></div>
                               <div class="bold">
                                        Details of your workshop selection:
                                   </div>
                               <div class="clear"></div>                               
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>What:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $data['title'] ?></div>
                               <div class="titleDetailsProduct">
                                        <div class="cuote"></div>When:
                                   </div>
                                   <div id="selectedProduct" style="width:500px" class="contentDetailsProduct selectedProduct"><?php echo $data['when'] ?> at <?php echo $data['hour'] ?></div>
                                   
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Where:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct"><?php echo $data['address'] ?><br> <?php echo $data['city'] ?>, <?php echo $data['zipcode'] ?><br><?php echo $data['phone'] ?></span>
                                     <a href="<?php echo $data['linkMap'] ?>" target="_blank">Map & Directions</a></div> 
                                   <div class="titleDetailsProduct">
                                        <div class="cuote"></div>Price:
                                   </div>
                                   <div id="selectedProduct" class="contentDetailsProduct selectedProduct price">$<?php echo number_format($cost['precio'],2) ?></div>
                                  <div class="clear"></div>
                           <div class="clear"></div>
                           </div>

                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2> 
                        <?php echo ($_COOKIE['datos_usr'])? '': '<p>Already have an account? <a href="#" id="fastLogin" class="greentext">Log In...</a></p>'; ?>                       
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" value="<?php echo printValue ('nickname',$datosUser,'pseudonimo')?>" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail2',$datosUser,'EMAIL')?>" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="password" name="txtPassword">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 8 characters and contain a letter and a number</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword2',$datosUser,'PASS')?>" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <input value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" type="text" name="txtPhoneCod"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" type="text" name="txtPhoneExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										$aux->cod=printValue ('MobileNumberCod');
										$aux->num=printValue ('MobileNumber');
										$aux->ext=printValue ('MobileNumberExt');
									}
									else
										$aux=($datosUser['MOBILETELEPHONE'])?formantPhone($datosUser['MOBILETELEPHONE']):'';
									?>
                                    <input type="text" value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" name="MobileNumberCod"> <label>-</label> 
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" name="MobileNumberExt">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
    	            <h2>Billing Address</h2>
	                <p>Your billing address must match the address on your credit card statement</p>
                    <div class="clear"></div>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd',$datosBanc,'billingaddress')?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd2')?>" type="text" name="sameAdd2">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameCity',$datosBanc,'billingcity')?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td>
                                    <select class="fieldRequired" id="sStateB" name="sStateB">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['customerRegister']['sStateB'])?$_COOKIE['customerRegister']['sStateB']:$datosBanc['billingstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
                                        ?>				
                                    </select>
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input value="<?php echo printValue ('sameZip',$datosBanc,'billingzip')?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('holder',$datosBanc,'cardholdername')?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                        <?php 
                                        $var=($_COOKIE['customerRegister']['cardtype'])?$_COOKIE['customerRegister']['cardtype']:$datosBanc['cardname'];
										foreach ($cardtype AS $key => $value)
										{
											echo ($key==$var)?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';

										} 
										?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cardtypeText')?>" id="cardtypeText" name="cardtypeText">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('cardnumber',$datosBanc,'cardnumber'): mcrypt_Fe7a0a89bd(printValue ('cardnumber',$datosBanc,'cardnumber'))?>" class="fieldRequired" id="cardNumber2" type="text" name="cardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate1'])?$_COOKIE['customerRegister']['exdate1']:$datosBanc['expirationmonth'];
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$var)?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate2'])?$_COOKIE['customerRegister']['exdate2']:$datosBanc['expirationyear'];
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$var)?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo ($_COOKIE['customerRegister'])?printValue ('csv',$datosBanc,'cardnumber'):mcrypt_Fe7a0a89bd(printValue ('csv',$datosBanc,'csvnumber'))?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div style="z-index:2; top:-190px;" class="notification">
                                    	<img src="../resources/img/CVV.dib" style="width:100%">
                                	</div>
                                </a>)
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" id="formProcessing" href="#">Continue to Confirmation Page<div id="errorForm" class="errorForm">Required fields are empty<div></div></div></a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
            <!--
                     Filed hidden
                     //-->
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
                     <input readonly type="hidden" name="montoapagar" id="montoapagar"  value="<?php echo $cost['precio'] ?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
                    <input type="hidden" name="cbGrupoProducts" value="<?php echo $cost['idproductobase'] ?>">
                    <input type="hidden" name="source" value="RegisterWorkshop">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>

<div id="ventanalogueo">
<div>
	<h2>
    	Log In
    </h2>
    <div class="clear">
    </div>
    <div class="paneloption">
       <form id="loginForm2" method="post" action="/resources/php/properties_validacion.php">
        	<table>
            	<tr>
                    <td>
                		<label title="Login Email Submit" class="bluetext bold">E-mail</label>
                    </td>
                    <td>
                        <input type="email" value="<?php echo($DataRemember[0])?$DataRemember[0]:''; ?>" name="loginEmail" class="fieldtext" id="loginEmail">
                   	</td>
           		</tr>
 				<tr>
            		<td>
               			<label title="Login Password Submit" class="bluetext bold">Password</label>
              		</td>
                    <td>
						<input type="password" value="<?php echo($DataRemember[1])?$DataRemember[1]:''; ?>" name="loginPass"  class="fieldtext" id="loginPass">
					</td>
				</tr>
                <tr>
                	<td>
                    </td>
                    <td>
                    	<div style="margin-left:20px;">
                        	<input style="float:left" type="checkbox" checked="checked" class="radioButton" name="remember"> <label style="margin-top:2px; float:left; margin-left:2px; font-size:11px;">Remember</label>
                        </div>
                    </td>
                </tr>
                <tr>
                	<td>
                    </td>
                    <td>
                    	<div style="margin-left:20px;">
                       		<a href="#" style="float:left;"  id="loginbutton2" class="buttonblue">Log In</a>
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="returnTo" value="../../register/registerWorkshop.php">
        </form>
    	
    </div>
</div>
</div>


</html>
<script language="javascript">
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){			
	/*inicio de validaciones*/
	$('#loginbutton2').bind('click',function (e){
		$('#loginForm2').trigger('submit');
	})
	validationRegister();
	toConfimationPage($('#formProcessing'),$('#customerRegisterForm'));
});

/*highlight menu*/
 menuClick('menu-store');
 
 /* tooltips */
 tooltips();
</script>