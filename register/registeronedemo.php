<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Learn with the help from our experts','Real Estate, florida, reifax, florida real estate, Training overview, Learn how to use our products, reifax');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Register - Please fill out the form below to order Your Free<br>One on One Demo Now</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext">
					<h2 style="color:#000; font-weight:bold;">Create an Account</h2>
                    <form action="newuseroodemo.php" id="comunityRegister">
						<table>
								<tr>
									<td>
										<label class="required">First Name</label>
									</td>
									<td>
										<input class="fieldRequired" type="text" id="firstName" name="txtFirstName">
									</td>
									<td>
										<span class="errorRegister"></span>
									</td> 
								</tr>
								<tr>
									<td>
										<label class="required">Last Name</label>
									</td>
									<td>
										<input class="fieldRequired" type="text" id="lastName" name="txtLastName">
									</td>
									<td>
										<span class="errorRegister"></span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="required">E-mail Address</label>
									</td>
									<td>
										<input class="fieldRequired " type="text" id="email" name="txtEmail" alt=''/>
										
									</td>
									<td>
										<span class="noteRegister" id="emailNote"></span>
										<span class="errorRegister" id="emailNoteError"></span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="required">Phone Number</label>
									</td>
									<td class="numberPhone">
										<input class="fieldRequired" type="text" id="txtPhoneNumber" name="txtPhoneNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
									</td>
									<td>
										<span class="errorRegister phone" id="numberPhoneError"></span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="required">Demo Date</label>
									</td>
									<td>
										<input class="fieldRequired" type="text" id="demodate" name="demodate" maxlength="15" >
									</td>
									<td>
										<span class="errorRegister"></span>
									</td>
								</tr>
								<tr>
									<td>
										<label class="required">Demo Time</label>
									</td>
									<td>
										<input class="fieldRequired" type="text" id="demotime" name="demotime" maxlength="10" >
									</td>
									<td>
										<span class="errorRegister"></span>
									</td>
								</tr>
								<tr>
									<td colspan='3'>
										<label class="required">To be able to schedule all of the demo dates and time successfully we need a few days in advance. Please allow up to 48 hours to be contacted by our support staff to confirm the date of your demo. As such, please choose an estimated date after 48 hours so we can schedule your coaching in a timely manner.</label>
									</td>
								</tr>
							   <tr>
								<td></td>
									<td>
										<div class="clear"></div>
										<div class="clear">&nbsp;</div>
										<div class="clear">&nbsp;</div>
										<div class="clear">&nbsp;</div>
										<div class="clear">&nbsp;</div>
										<div class="centertext">
											<a href="#" id="formProcessing" class="buttonblue bigButton">Sign in</a>
										</div>
									</td>
							   <td></td>
								</tr>
						 </table>
						 <!--
						 Filed hidden
						 //-->
						 <input readonly type="hidden" name="txtPhone" id="txtPhone"  value="">
						 <input readonly type="hidden" name="txtMobil" id="txtMobil"  value="">
                     </form>
					<div class="clear">&nbsp;</div>
					<div class="clear">&nbsp;</div>
					<div class="clear">&nbsp;</div>
					<div class="clear">&nbsp;</div>
					<div class="clear">&nbsp;</div>
				</div>
		</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Registration processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-company');
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('#usertype').trigger('change');

 	/***
	validaciones de campos requeridos
	************/
	$('#formProcessing').bind('click',function (){
		//var correcto=true;
		$('.fieldRequired').each(function (){
			$(this).trigger('blur')
		});
		if(correcto)
		{
			$('#comunityRegister').submit();
		}
		else
		{
			//$(this).removeClass('buttongreen').html('Required fields are empty').addClass('buttonred')
			return correcto; 
		} 
		})
	$('#comunityRegister').bind('submit',function ()
	{
		$("#txtPhone").attr('value',$('.numberPhone:eq(0) input:eq(0)').val());
		
		$.ajax({
			 type	: 'POST',
            url		: $(this).attr('action'),
			dataType: 'json',
            data	: $(this).serialize(),
			success	: function (respuesta)
			{
				if(respuesta.success)
				{
					alert(respuesta.mensaje);
					document.location.href="http://www.reifax.com";
				}
				else
				{
					$('.MsglockScreen').css({
						'font-size': '16px',
						 color		:'#C60303'
					}).html('<a href="#" class="buttonred cerrarVentana">Return</a><div class="clear"></div>'+respuesta.mensaje);
					$('.cerrarVentana').bind('click',function (){
						$('.lockScreen,.MsglockScreen').fadeOut(100);
						$('.MsglockScreen').css({
							 'font-size': '26px',
							 color		:'#005C83',
						}).html('<img src="../resources/img/ac.gif">  Registration processing...');
						})
				}
			}
		});
		return false;
	})
	
});
</script>