<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>
<style type="text/css">
#trPassword,#transwer, #trquestion,#trsupport{
	display:none;
}
</style>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Forgot Password</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright" >
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>

    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Forgot Password - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs informationaltext" style="min-height: 200px;">
					<h2 style="color:#000; font-weight:bold;">Forgot Password</h2>
                    <form action="newUserCommunity.php" id="comunityRegister">
                    <table>
                        	<tr id="trEmail">
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" name="txtEmail">
                                 </td>
                                <td>
                                    <a href="#" id="go" class="buttongreen">Go</a>
                                </td>
                            </tr>
                            <tr id="trquestion">
                                <td>
                                </td>
                                <td>
                                    <label id="questionuser"></label>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="transwer">
                            	<td>
                                	<label class="required">Your answer</label>
                                </td>
                                <td>
                                	<input class="" type="text" value="" name="answeruser">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="trsupport">
                            	<td>
                                </td>
                                <td>
                                	<label>For your enhanced security and privacy, we have created a secret security question. Please call our customer service for help. Thank you</label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            </table>
                            <div class="clear"></div>
                            <table id="trPassword">
                        	<tr>
                            	<td>
                                	<label class="required">New Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="" id="password" name="txtPassword">
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 8 characters and contain a letter and a number</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" id="re-password" name="txtPassword2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                            	<td>
                                 <div class="centertext">
                                                <a href="#" id="next" class="buttonblue bigButton">Next</a>
                                 </div>
                                </td>
                           <td></td>
                            </tr>
                     </table>
                     </form>
                     </div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>
<div class="lockScreen"></div>
<div class="MsglockScreen bold">
<img src="../resources/img/ac.gif"> 
Processing...
</div>
</body>

</html>
<script language="javascript">
 menuClick('menu-support');
 $(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	$('.numberPhone input').unbind('blur');
	$('#next').bind('click',funciones.proccess)
	$('#go').bind('click',funciones.findUser);
	$('input[name=txtEmail]').bind('focus',funciones.initAll);
	correcto=true;
});
var funciones={
	initAll:function (){
			$('#go').fadeIn(200);
			$('#trPassword,#trquestion,#transwer,#trsupport').hide();
		},
	findUser:function ()
		{
			if($('input[name=txtEmail]').val()=='')
				return false;
			$('#go').fadeOut(200);
			$.ajax({
				type	:'POST',
				data 	:'option=findData&email='+$('input[name=txtEmail]').val(),
				dataType:'json',
				url		:'../resources/php/funcionesForgotPassword.php',
				success	: function (respuesta)
						{
							if(respuesta.success)
							{
								switch(respuesta.estado){
									case (1):
										$('#questionuser').html(respuesta.questionuser);
										$('#transwer input').addClass('fieldRequired');
										$('#trPassword,#trquestion,#transwer').show();
									break;
									case (8):
										$('#trsupport').show();
									break;
									default:
										$('#trPassword').show();
										
									}
							}
						}
				
				})
	},
	proccess:function ()
	{
		$('.fieldRequired').each(function (){
			$(this).trigger('blur');
			})
		if(correcto)
		{
			$('.lockScreen').fadeTo(100,0.5);
			$('.MsglockScreen').fadeIn();
			$.ajax({
				type	:'POST',
				data 	:'option=proccess&email='+$('input[name=txtEmail]').val()+'&answeruser='+$('input[name=answeruser]').val()+'&pass='+$('input[name=txtPassword]').val(),
				dataType:'json',
				url		:'../resources/php/funcionesForgotPassword.php',
				success	: function (respuesta)
							{
								if(respuesta.success)
								{
									if(respuesta.estado==4){
										$('.MsglockScreen').html('successfully processed request');
									}
									else{
										$('.MsglockScreen').html('Email or password is incorrect..!!');
									}
									$('.lockScreen, .MsglockScreen').delay(3000).fadeOut(250,function (){$('.MsglockScreen').html('<img src="../resources/img/ac.gif"> Processing...');});
									
								}
							}
					})
			}
		}
	}
</script>