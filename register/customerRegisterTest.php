<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
   	conectar('xima');
	if(!$_COOKIE['customerRegister'] && $_COOKIE['datos_usr'])
	{
		$query=mysql_query('select * from ximausrs where userid='.$_COOKIE['datos_usr']['USERID']);
		$datosUser=mysql_fetch_array($query);
	}
	function printValue ($field,$datosUser,$fieldbd)
	{
		if($_COOKIE['customerRegister'])
			return $_COOKIE['customerRegister'][$field];
		else if($datosUser)
			return $datosUser[$fieldbd];
		else
			return '';
	}
	$datosUser['expirationdate']=explode('/',$datosUser['expirationdate']);
	//codigo adicionado por smith 21/11/2011 cuando se viene de backoffice	
	if(isset($_GET['bkou']) && strlen($_GET['bkou'])>0)
	{
		if($_COOKIE[customerRegister])
		{
			//prinf_r($_COOKIE['customerRegister']);
				foreach($_COOKIE[customerRegister] AS $key =>$value)
				{
					setcookie("customerRegister[$key]",false,time()-3600,'/');
				}
			unset($_COOKIE['customerRegister']);
		}
	}
	//if(isset($_GET['procode']) && strlen($_GET['procode'])>0)$_COOKIE['procode']=$_GET['procode'];
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadHttps();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderHttps();?> 
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: 
        <a href="http://www.reifax.com/index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="http://www.reifax.com/store/index.php"><span class="bluetext underline">Store</span></a> &gt; 
        	<span class="fuchsiatext">Customer Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div id="sidebarright" class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="https://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Customer Register - Please fill out the form below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form id="customerRegisterForm" action="customerRegisterConfimationTest.php" method="post">
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register productInformationDetails">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Product Information</h2>
                           <div class="BoxTextLoudGray bold">
                                Optional - Promotion Code
                           </div>
                           <div class="BoxTextSoftGray">
                                <span>If you have a Promotion Code, enter it in the box to the right </span>
                                <input type="text" class="shortBox mediumMarginLef promotionCode" value="<?php echo ($_COOKIE['procode'])?$_COOKIE['procode']:printValue ('prom')?>"  name="prom" id="promotionCode">&nbsp;<a href="#" class="buttongreen">Go.</a>
                           </div>
                           <div id="ProductInformation">
                               <input type="hidden" id="inputTotalValue" value="<?php echo printValue ('price')?>" name="price">
                               <input type="hidden" id="valueDiscount" value="<?php echo printValue ('valueDiscount')?>" name="valueDiscount">
                           		<div class="bold">
                                	<div>
                                    	Details of your product selection: <span id="priceNormal" style=" padding-left:23px; padding-right:8px;">Total</span>$<span class="totalValue"></span>
                                    </div>
                                    <div id="DiscountEspecial">
                                    	<span class="discuntespecial">
                                        <table>
                                        <tr>
                                        	<td style="width:260px;">
                                        		Promotional Discount - First Month Only
                                            </td>
                                        	<td class="blacktext">
                                        		$<span class="valueDiscount"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td style="width:260px;">
                                        		You pay now only
                                            </td>
                                        	<td class="blacktext">
                                        		$<span class="valuePayNow"></span>
                                            </td>
                                        </tr>
                                        </table>
                                    </div>
                               </div>
                           <div class="clear"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Product:
                               </div>
                               <div id="selectedProduct" class="contentDetailsProduct selectedProduct"></div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Frequency:
                               </div>
                               <div id="selectedFrequency" class="contentDetailsProduct selectedFrequency"></div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>State:
                               </div>
                               <div id="selectedState" class="contentDetailsProduct selectedState"></div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>County:
                               </div>
                               <div  id="selectedCounties" class="contentDetailsProduct selectedCounties"></div>
                           <div class="clear"></div>
                           <div>
                           		You are ordering the <span id="ordering" class="bold"><span class="selectedState"></span>/<span class="selectedCounties"></span>/<span class="selectedFrequency"> </span> <span class="selectedProduct">REIfax Residential</span> </span>. The  total amount you are paying today is $<span class="totalValue valuePayNow"></span>. Your account will automatically renew at $<span class="totalValue"></span>/<span class="selectedFrequency">month</span> for as long as you remain a member. You can easily upgrade, downgrade, or cancel your account at any time.
                           </div>
                           <div class="clear"></div>
                           <div>
                           		If you wish to change your selection, you may do it below:
                           </div>
                           <div class="clear"></div>
                           <div>
                           		<div class="centertext selections">
                                	<input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" name="executive" id="executive">
                                	<label class="required">Product:</label><br>
                                    <select name="cbGrupoProducts" style="width:140px;" id="product">
                                    	<option value="1">REIFax Residential</option>
                                    	<option value="2">REIFax Platinum</option>
                                    	<option value="3">REIFax Professional</option>
                                    	<option value="7">Home Deals Finder</option> 
                                    	<option value="6">Buyer&acute;s Pro</option>
                                    	<option value="8">Leads Generator</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoProductsText" name="cbGrupoProductsText">
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">Frequency:</label><br>
                                    <select name="cbGrupoFrecuency" id="frencuency" class="shortBox">
                                   		<option value="1">Monthly</option>
                                    	<option value="2">Annual</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoFrecuencyText" name="cbGrupoFrecuencyText">
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">State:</label><br>
                                    <select name="cbGrupoState" class="shortBox"  id="State" >
										<?php
                                            $sql="SELECT * FROM lsstate where is_showed='Y';";
                                            $resultado=mysql_query($sql);
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                            }
                                        ?></select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoStateText" name="cbGrupoStateText">
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">County:</label><br>
                                    <select name="cbGrupoCounty" id="County" >
                                    	<option>Loading...</option>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cbGrupoProductsText')?>" id="cbGrupoCountyText" name="cbGrupoCountyText">
                                </div>
                           </div>
                           </div>
                           
                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register productInformationDetails">
                        <h2>Customer Information</h2>
                        <?php echo ($_COOKIE['datos_usr'])? '': '<p>Already have an account? <a href="#" id="fastLogin" class="greentext">Log In...</a></p>'; ?>
                        
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">I am a (an):</label>
                                </td>
                                <td>
                                	<select  class="fieldRequired" id="usertype" name="usertype">
                                    <option></option>
									<?php
                                            $sql="SELECT * FROM usertype where `show`=1 order by orden;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['customerRegister']['usertype'])?$_COOKIE['customerRegister']['usertype']:$datosUser['idusertype'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['idusertype'])?'<option value="'.$data['idusertype'].'" selected>'.$data['usertype'].'</option>':'<option value="'.$data['idusertype'].'">'.$data['usertype'].'</option>';
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('sStateBText')?>" id="usertypeText" name="usertypeText">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="fieldLicense" style="display:none">
                            	<td>
                                	<label class="required">License #</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo printValue ('txtBroker',$datosUser,'LicenseN')?>" id="txtBroker" name="txtBroker">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr id="questionCompany" style="display:none">
                            	<td colspan="2">
                                	<label>Is your company registered width us?</label>
                                	<input type="radio" name="companyRegister" value="yes" class="radioButton"> <label>Yes</label>
                                    <input type="radio" name="companyRegister" value="no" class="radioButton" checked> <label> No</label>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr id="divOfficeNumber" style="display:none">
                            	<td>
                                	<label class="required">Office License #</label>
                                </td>
                                <td>
                                	<input type="text" value="<?php echo printValue ('txtOffice',$datosUser,'BrokerN')?>" id="Office" name="txtOffice">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="firstName" value="<?php echo printValue ('txtFirstName',$datosUser,'NAME')?>" name="txtFirstName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" id="lastName" value="<?php echo printValue ('txtLastName',$datosUser,'SURNAME')?>" name="txtLastName">
                                </td>
                                <td>
                                	<span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label>Nickname</label>
                                </td>
                                <td>
                                	<input type="text" id="nickName" value="<?php echo printValue ('nickname',$datosUser,'pseudonimo')?>" name="nickname">
                                </td>
                                <td>
                                	<span class="errorRegister" id="nickNameNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail',$datosUser,'EMAIL')?>" id="email" name="txtEmail">
                                </td>
                                <td>
                                	<span class="noteRegister" id="emailNote">Your email will be your Username</span>
                                	<span class="errorRegister" id="emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="text" value="<?php echo printValue ('txtEmail2',$datosUser,'EMAIL')?>" id="Re-email" name="txtEmail2">
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-emailNoteError"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword',$datosUser,'PASS')?>" id="password" name="txtPassword" maxlength="20" >
                                </td>
                                <td>
                                	<span class="noteRegister" id="passwordNote">Must be at least 5 characters</span>
                                    <span id="passwordNoteError" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input class="fieldRequired" type="password" value="<?php echo printValue ('txtPassword2',$datosUser,'PASS')?>" id="re-password" name="txtPassword2" maxlength="20" >
                                </td>
                                <td>
                                	<span class="errorRegister" id="Re-passwordNoteError"></span>
                                </td>
                            </tr>
                            <?php
								if(!$_COOKIE['datos_usr'])
								{
									securityQuestion(printValue ('questionuser'), printValue ('answeruser'));
								}
							?>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										/*
										$aux->cod=printValue ('txtPhoneCod');
										$aux->num=printValue ('txtPhoneNumber');
										$aux->ext=printValue ('txtPhoneExt');
										*/
										$aux->num=printValue ('txtPhoneNumber');
									}
									else
										$aux=($datosUser['HOMETELEPHONE'])?formantPhone($datosUser['HOMETELEPHONE']):'';
									?>
                                    <!--
									<input value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" type="text" name="txtPhoneCod"> <label>-</label> 
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" name="txtPhoneNumber"> <label>Ext</label>
                                    <input value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" type="text" name="txtPhoneExt">
									-->
                                    <input value="<?php echo ($aux->num)?$aux->num:''?>" class="fieldRequired" type="text" id="txtPhoneNumber" name="txtPhoneNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)"> 
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
									<?php 
									if($_COOKIE['customerRegister'])
									{
										/*
										$aux->cod=printValue ('MobileNumberCod');
										$aux->num=printValue ('MobileNumber');
										$aux->ext=printValue ('MobileNumberExt');
										*/
										$aux->num=printValue ('MobileNumber');
									}
									else
										$aux=($datosUser['MOBILETELEPHONE'])?formantPhone($datosUser['MOBILETELEPHONE']):'';
									?>
                                    <!--
									<input type="text" value="<?php echo ($aux->cod)?$aux->cod:''?>" class="short" name="MobileNumberCod"> <label>-</label> 
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" value="<?php echo ($aux->ext)?$aux->ext:''?>" class="short" name="MobileNumberExt">
									-->
                                    <input type="text" value="<?php echo ($aux->num)?$aux->num:''?>" name="MobileNumber" id="MobileNumber" maxlength="12" onKeyDown="return phonesvalidation(this,'-','0',true)">
                                </td>
                                <td>
                                	<span class="errorRegister phone" id="numberPhoneError2"></span>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="addressPanel">	
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:260px;" class="centertext">
        		            		<h2>Billing Address</h2>Your billing address must match the 
                    		   		 address on your credit card statement
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue ('sameAdd',$datosUser,'billingaddress')?>" class="fieldRequired" type="text" name="sameAdd">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('sameAdd2')?>" type="text" name="sameAdd2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue ('sameCity',$datosUser,'billingcity')?>" class="fieldRequired" type="text" name="sameCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <select class="fieldRequired" id="sStateB" name="sStateB">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
											$var=($_COOKIE['customerRegister']['sStateB'])?$_COOKIE['customerRegister']['sStateB']:$datosUser['billingstate'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
                                        ?>				
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('sameZip',$datosUser,'billingzip')?>" class="fieldRequired" type="text" name="sameZip">
                                </td>
                            </tr>
                        </table>
                </div>
                
                
                <div class="addressPanel">
                        <table>
                        <tr>
                        	<td></td>
                            	<td>
                				<div style="width:250px;" class="centertext">
        		            		<h2>Mailing Address</h2>
                                    	<input type="checkbox" style="margin-top:-5px;"  class="radioButton" id="copyAddress">
                                        Check if mailing address is the same as billing address
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table style="margin-top:-3px;">
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                	<input value="<?php echo printValue ('txtAddress1',$datosUser,'ADDRESS')?>" class="fieldRequired" type="text" name="txtAddress1">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('txtAddress2')?>" type="text" name="txtAddress2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('txtCity',$datosUser,'CITY')?>" type="text" class="fieldRequired" name="txtCity">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">State</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>                                   
                                <select class="fieldRequired" id="sState" name="sState">
									<option value="">-Select A State-</option>
									<?php
                                            $sql="SELECT * FROM state;";
                                            $resultado=mysql_query($sql);
                                            $var=($_COOKIE['customerRegister']['sState'])?$_COOKIE['customerRegister']['sState']:$datosUser['STATE'];
                                            while($data=mysql_fetch_assoc($resultado))
                                            {
                                                echo ($var==$data['name'])?'<option value="'.$data['name'].'" selected>'.$data['name'].'</option>':'<option value="'.$data['name'].'">'.$data['name'].'</option>';
                                            }
									?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td><div class="centertext errorRegister"></div>
                                    <input value="<?php echo printValue ('txtZip',$datosUser,'zipcode')?>" class="fieldRequired" type="text" name="txtZip">
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="CreditCardlogo">
                </div>
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('holder',$datosUser,'cardholdername')?>" class="fieldRequired" type="text" name="holder">
                                </td>
                                <td>
                                	<span class="noteRegister">As it appears on the credit card</span>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select class="fieldRequired" id="cardtype" name="cardtype">
                                        <?php 
                                        $var=($_COOKIE['customerRegister']['cardtype'])?$_COOKIE['customerRegister']['cardtype']:$datosUser['cardtype'];
										foreach ($cardtype AS $key => $value)
										{
											echo ($key==$var)?'<option value="'.$key.'" selected>'.$value.'</option>':'<option value="'.$key.'">'.$value.'</option>';
										} 
										?>
                                    </select>
                                    <input type="hidden" value="<?php echo printValue ('cardtypeText')?>" id="cardtypeText" name="cardtypeText">
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('cardnumber',$datosUser,'cardnumber')?>" class="fieldRequired" id="cardNumber" type="text" name="cardnumber">
                                </td>
                                <td>
                                	<span id="noteCardNumber" class="noteRegister">No spaces, dashes or punctuation</span>
                                    <span id="errorCardNumber" class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expiration Date</label>
                                </td>
                                <td>
                                	<label>Month&nbsp;</label><select  class="fieldRequired" id="exdate1" name="exdate1">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate1'])?$_COOKIE['customerRegister']['exdate1']:$datosUser['expirationdate'][0];
										for ($i=1;$i<13;$i++)
										{
											$aux=($i>9)?$i:'0'.$i;
											echo ($aux==$var)?'<option value="'.$aux.'" selected>'.$aux.'</option>':'<option value="'.$aux.'">'.$aux.'</option>';
										} 
										?>
                                    </select>
                                    <label>&nbsp;&nbsp;Year&nbsp;</label><select  class="fieldRequired" id="exdate2" name="exdate2">
                                    	<option value="">Select...</option>
                                    	<?php 
                                        $var=($_COOKIE['customerRegister']['exdate2'])?$_COOKIE['customerRegister']['exdate2']:$datosUser['expirationdate'][1];
										for ($i=date(Y);$i<(date(Y)+10);$i++)
										{
											echo ($i==$var)?'<option value="'.$i.'" selected>'.$i.'</option>':'<option value="'.$i.'">'.$i.'</option>';
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CVV</label>
                                </td>
                                <td>
                                	<input value="<?php echo printValue ('csv',$datosUser,'csvnumber')?>" type="password" name="csv" id="csv" class="fieldRequired shortBox"> (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div style="z-index:2; top:-190px;" class="notification">
                                    	<img src="../resources/img/CVV.dib" style="width:100%">
                                	</div>
                                </a>)
                                </td>
                                <td>
                                    <span class="errorRegister"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" id="formProcessing" href="#">Continue to Confirmation Page<div id="errorForm" class="errorForm">Required fields are empty<div></div></div></a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
            <!--
                     Filed hidden
                     //-->
                    <input type="hidden" id="bkou" name="bkou" size="5" value="<?php echo $_GET['bkou'] ?>" />
                    <input type="hidden" id="hdAccept" name="hdAccept" value="<?php  echo $_GET['rbko']  ?>">
                    <input type="hidden" id="freeactive" name="freeactive" value="<?php echo  $_GET['freeactive'] ; ?>">
                    <input type="hidden" id="rbko" name="rbko" value="<?php echo $_GET['rbko']; ?>">
                    <input type="hidden" id="freedays" name="freedays" value="<?php  echo $_GET['freedays']; ?>">
                    <input type="hidden" id="ui" name="ui" value="<?php echo $ui; ?>">
                    <input type="hidden" id="source" name="source" value="RegisterCustomer">
					 
                     <input readonly type="hidden" name="idcompany" id="idcompany"  value="<?php echo printValue('idcomp',$datosUser,'idcomp')?>">
                     <input readonly type="hidden" name="USERID" id="USERID"  value="<?php echo $_COOKIE['datos_usr']['USERID']?>">
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Registration processing...
</div>

</html>
<script language="javascript">
	var cbGrupoProducts="<?php echo printValue ('cbGrupoProducts')?>";
	var cbGrupoFrecuency="<?php echo printValue ('cbGrupoFrecuency')?>";
	var cbGrupoState="<?php echo printValue ('cbGrupoState')?>";
	var cbGrupoCounty="<?php echo printValue ('cbGrupoCounty')?>";
	var cargaInicial=true;
	correcto=pass=repass=email=remail=nick=credicard=true;
$(document).ready(function (){
	/*inicio de validaciones*/
	validationRegister();
	Customer.loadCouty();
	toConfimationPage($('#formProcessing'),$('#customerRegisterForm'));

});
var Customer=new Object();
Customer.asignar=function ()
{
	if(cbGrupoProducts)
	{
		$('#product').attr('value',cbGrupoProducts);
		$('#frencuency').attr('value',cbGrupoFrecuency);
		$('#State').attr('value',cbGrupoState);
		$('#County').attr('value',cbGrupoCounty);
		$('#usertype').trigger('change');
		Customer.text(); 
	}
	else if (cargaInicial)
	{
		$('#product').attr('value',(getUrlVars()['gp'])?getUrlVars()['gp']:3);
		$('#frencuency').attr('value',(getUrlVars()['gf'])?getUrlVars()['gf']:1);
		$('#State').attr('value',(getUrlVars()['gs'])?getUrlVars()['gs']:1);
		if(getUrlVars()['gc'])
		{
			if(getUrlVars()['gc']!='ALL')
			{
				IniCounty.init($('#County'),Customer.text);
			}
			else
			{
				$('#County').attr('value','ALL');
				Customer.text();
			}
		}
		else
		{
			$('#County').attr('value','ALL');
			Customer.text();
		}
		$('#usertype').trigger('change');
	}
	
		cbGrupoProducts=null;
		cargaInicial=null;
}
Customer.text=function()
{
	$('.selectedProduct').html($('#product option:selected').html());
	$('.selectedFrequency').html($('#frencuency option:selected').html());
	$('.selectedState').html($('#State option:selected').html());
	$('.selectedCounties').html($('#County option:selected').html());
	$('.totalValue').html('');
	$('#inputTotalValue').attr('value','');
	$.ajax({
		url		:'../resources/php/properties.php',
		type	: 'get',
		dataType:'json',
		data	:'precio=true&id='+$('#product').val()+'&county='+$('#County').val()+'&frencuency='+$('#frencuency').val()+'&promotionCode='+$('#promotionCode').val()+'&stade='+$('#State').val(),
		success:function (resul){
			$('.totalValue').html(resul.price);
			$('#inputTotalValue').attr('value',resul.price);
			$('#executive').attr('value',resul.userdescuento);
			if(resul.descuentoEspecia)
			{
				//se registra el valor del descuento
				$('.valueDiscount').html(resul.precioDescuento);
				$('#valueDiscount').attr('value',resul.precioDescuento);
				//se registra el valor del pago actual
				//$('#inputTotalValue').attr('value',resul.payNow);
				$('.valuePayNow').html(resul.payNow);
				$('#DiscountEspecial').fadeIn('fast').show('fast');
			}
			else
			{
				$('#DiscountEspecial').fadeOut('fast').hide('fast');
			}
			}
		});
	
}
/*Carga Condados*/
Customer.loadCouty=function ()
{
$.ajax({
	type	:'POST',
	url		:'../resources/php/properties.php',
	data	: "ejecutar=countylist&state="+$('#State').val(),
	dataType:'json',
	success	:function (resul)
		{
			$('#County').html('<option value="ALL">All Counties</option>');
			$(resul).each(function (index,data){
				$('#County').append('<option value="'+data.id+'">'+data.county+'</option>');
			});
			Customer.asignar();
		}
	});
};
$('#promotionCode').bind('blur',Customer.text);
$('#product, #frencuency, #State, #County').bind('change',Customer.text);
$('#State').bind('change',Customer.loadCouty);

 
/*
	Funciones para el check box
*/

$("#copyAddress").change(function(){
	if($(this).is(':checked'))
	{
		$("input[name=txtAddress1]").attr('value',$("input[name=sameAdd]").val());
		$("input[name=txtAddress2]").attr('value',$("input[name=sameAdd2]").val());
		$("input[name=txtZip]").attr('value',$("input[name=sameZip]").val());
		$("input[name=txtCity]").attr('value',$("input[name=sameCity]").val());
		$("#sState").attr('value',$("#sStateB").val());		
		$('.addressPanel .fieldRequired').trigger('blur');
	}
	else
	{
		$("input[name=txtAddress1]").attr('value','');
		$("input[name=txtAddress2]").attr('value','');
		$("input[name=txtZip]").attr('value','');
		$("input[name=txtCity]").attr('value','');
		$("#sState option").removeAttr('selected');
	}
});
/*
	tamaños del bloque publicitario
*/

 if ($.browser.webkit) {
	 $('select[name="exdate2"], select[name="exdate1"]').css('width','75px');
  }
  if($.browser.webkit || $.browser.msie){
	 $('#sidebarright').css('height','1805px');
  }

/*highlight menu*/
 menuClick('menu-store');
 
 /* tooltips */
 tooltips();
</script>