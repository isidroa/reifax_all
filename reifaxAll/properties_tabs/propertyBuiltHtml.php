<?php 	
	include('../../properties_conexion.php');
	conectar();
	/*Old Datos By Smith
		PRDEF	538,3,4,43,10,558,26,556,29,541,30,31,41,339,536	Public Records	100
		LISDEF	293,61,71,72,129,76,84,85,96,90,93,95,102,290,106,115,124,126,131	Listing	200
		FCALL	350,594,354,362,361,408,368,572,351,349,565,502	Foreclosure	300
		MORALL	327,301,304,323,317,313,306,325,321,328,303	Mortgage	400
		PRALL	20,21,22,23,24,25,590,591,7,15,2,36,14,19,6,559,566,292,294,16,32,595,9,28,40,33,42,34,17,18,1,39	Public Records	500
		LISALL	120,345,88,80,91,79,346,84,603,89,121,70,593,540,546,341,78,125,127	Listing	600
		BODEF	615,623,624,625,631,634,636,637,638,690,642,643,646,648,650,651	By Owner	700
		BOALL	616,618,618,619,620,622,626,627,628,629,630,632,654,633,635,640,641,644,645,647,649,652,653,655,656,657,658,659	By Owner	800	
	
	WHERE s.`idGrid` in ('PRDEF','LISDEF','FCALL','MORALL','PRALL','LISALL','BODEF','BOALL')*/
	$sql1="SELECT * FROM `searchtemplateadv` s
			WHERE s.`idGrid` in ('PRDEF','LISDEF','FCALL','MORALL','PRALL','LISALL')
			ORDER BY s.orden";
	$res1=mysql_query($sql1) or die($sql1.mysql_error());
	$imain=0;
	$cadresult='';
	$readOnly=isset($_POST['readOnly'])?'true':'false';
	while($rowmain=mysql_fetch_array($res1))
	{
		//$idcts='538,10,26,556,29,541,31,41,339,536,3';//$rowmain['campos'];
		$idcts=$rowmain['campos'];
		$textotitulo=$rowmain['texto'];
		$idGrid=$rowmain['idGrid'];
		$text = isset($_POST['text'])	?	$_POST['text']	:	"tx";
		$combo = isset($_POST['combo'])	?	$_POST['combo']	:	"cb";
		$composite = isset($_POST['composite'])	?	$_POST['composite']	:	"idcompfield";
		switch($idGrid)
		{
			case 'PRDEF':	$idGrid='PR'; break;
			case 'LISDEF':	$idGrid='FS'; break;
			case 'FCALL':	$idGrid='FO'; break;
			case 'MORALL':	$idGrid='MO'; break;
			case 'PRALL':	$idGrid='PRMORE'; break;
			case 'LISALL':	$idGrid='FS'; break;
			//case 'BODEF':	$idGrid='BO'; break;
			//case 'BOALL':	$idGrid='BOMORE'; break;
		}
					
		$sql2	=	"
			SELECT c.`idtc`, c.`Tabla`, c.`Campos`, c.`titulos`, c.`Decimals`, c.`type`, c.`Align`, c.`size`, CONCAT(c.`Desc`,' ','(',IF(c.`ref` IS NULL,' ',c.`ref`),')') as `Desc`, c.`r_size`, c.`px_size`
			FROM camptit c
			WHERE  c.`idtc` IN ($idcts)
			ORDER BY  c.`Tsaorden`";

		$res2=mysql_query($sql2) or die($sql2.mysql_error());
		unset($arrdta);
		while($row=mysql_fetch_array($res2)) $arrdta[]=$row;
		$canttotal=count($arrdta);
		$cant=($canttotal/2);

		if($imain>0)	$cadresult.= "^";		

		$cadresult	.=	"
			[{
				columnWidth	:	.50,
				layout		:	'form',				
				items		:	[";
	
		for($iadvc=0;$iadvc<$cant;$iadvc++){
			$realnumber='';
			switch($arrdta[$iadvc]['type']){
				//Eliminated Second Field because it is not a search and dint haved betwen $secondfield=0 for real, int and date
				case 'string':	$datestore='dataString';	$xtype='textfield';		$valueselect='Start With';	$secondfield=0;	$val=0; break;
				case 'txarea':	$datestore='dataString';	$xtype='textarea';		$valueselect='Start With';	$secondfield=0;	$val=0; break;
				//case 'real':	$datestore='dataIntDate';	$xtype='numberfield';	$valueselect='Equal';		$secondfield=0;	$val=1;	$realnumber=",decimalPrecision : 2, decimalSeparator : '.'"; break;
				case 'real':	$datestore='dataIntDate';	$xtype='realNumber';	$valueselect='Equal';		$secondfield=0;	$val=1;	$realnumber=",maskRe: /[0-9\\.,]/"; break;
				case 'int':		$datestore='dataIntDate';	$xtype='numberfield';	$valueselect='Equal';		$secondfield=0;	$val=2;	break;
				case 'date':	$datestore='dataIntDate';	$xtype='textfield';		$valueselect='Equal';		$secondfield=0;	$val=3;	break;
				case 'boolean':	$datestore='dataBoolean';	$xtype='hidden';		$valueselect='Select';		$secondfield=0;	$val=4;	break;					
			}

			if($iadvc>0)	$cadresult.= ",";		

			$cadresult	.=	"
				{ 
					xtype	:	'compositefield',
					id		:	'$composite".$arrdta[$iadvc]['idtc']."',
					name	:	'$composite".$arrdta[$iadvc]['idtc']."',
					anchor	:	'-5',fieldLabel: '".$arrdta[$iadvc]['titulos']."',
					items	:	[";
			//Apply this condition only for de boolean Type: this show a combo with yes or no values
			if($datestore === 'dataBoolean'){
				$cadresult	.=	"
				{
					width			:	100,
					xtype			:	'combo',
					mode			:	'local',
					readOnly 		:	".$readOnly.",
					triggerAction	:	'all',
					forceSelection	:	true,
					editable		:	false,
					//name			:	'".'cb'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*".$idGrid."',
					name			:	'".$combo.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
					//id				:	'".'cb'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*".$idGrid."',
					id				:	'".$combo.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
					//hiddenName		:	'tx".$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
					hiddenName		:	'tx".$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
					displayField	:	'desc',
					valueField		:	'id',
					store			:	".$datestore.",
					value			:	'".$valueselect."',
					listeners		:	{
						'select'	:	selectBetween,
						render		:	function(c) {
							Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
						}
					}
				}";
			}else{
				if($arrdta[$iadvc]['type']<>'date'){
					if($arrdta[$iadvc]['type']=='txarea'){
						$cadresult	.=	"
						{
							xtype		:	'".$xtype."',
							//name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
							name		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
							//id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."' $realnumber ,
							id			:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."' $realnumber ,
							width		:	450,
							readOnly 	:	".$readOnly.",
							height		:	100,
							listeners		:	{
								render		:	function(c) {
									Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
								}
							}
						}";
					
					}else{
						$listener="";
						if($xtype=="realNumber"){
							$xtype="textfield";
							$listener="
								,change: function(object, newValue, oldValue){
									newValue = Ext.util.Format.usMoney(newValue);
									if(newValue == '\$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
								}
							";
						}
						$cadresult	.=	"
						{
							xtype		:	'".$xtype."',
							//name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
							name		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
							//id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."' $realnumber ,
							id			:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."' $realnumber ,
							readOnly	:	".$readOnly.",
							autoCreate	:	{
								tag			:	'input', 
								autocomplete:	'off', 
								size		:	'".$arrdta[$iadvc]['size']."'
							},
							listeners		:	{
								render		:	function(c) {
									Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
								}
								$listener
							}
						}";
				
						if($secondfield==1){
							$cadresult	.=	"
								,{
									xtype		:	'".$xtype."',
									name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
									id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."' $realnumber ,
									readOnly	:	".$readOnly.",
									autoCreate	: 	{
										tag			:	'input', 
										autocomplete:	'off', 
										size		:	'".$arrdta[$iadvc]['size']."'
									}
								}";//
							}
					}
				}else{
					$cadresult	.=	"
						{
							xtype	:	'datefield',
							//name	:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
							name	:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
							//id		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
							id		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
							width	:	93,
							readOnly	:	".$readOnly.",
							//format	:	'Ymd',
							format	:	'm/d/Y',
							listeners		:	{
								render		:	function(c) {
									Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
							}
						}
						}";
						/*Jesus: I separed this Code because i dont need a second datefield, it is utiliced on the search
						",{
							xtype	:	'datefield',
							name	:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
							id		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
							format	:	'Ymd'
						}";*/
				}
			}

				$cadresult	.=	"
					]
				}";
	
	}//for left
		$cadresult	.=
				"	]//items left column 
				},{
					columnWidth: .50,
					layout:'form',				
					items: [";
	
					$iadvc2=0;
					for($iadvc=$iadvc;$iadvc<$canttotal;$iadvc++)
					{
						$realnumber='';
						switch($arrdta[$iadvc]['type']){
							//Eliminated Second Field because it is not a search and dint haved betwen $secondfield=0 for real, int and date
							case 'string':	$datestore='dataString';	$xtype='textfield';		$valueselect='Start With';	$secondfield=0;	$val=0; break;
							//case 'real':	$datestore='dataIntDate';	$xtype='numberfield';	$valueselect='Equal';		$secondfield=0;	$val=1;	$realnumber=",decimalPrecision : 2, decimalSeparator : '.'"; break;
							case 'real':	$datestore='dataIntDate';	$xtype='realNumber';	$valueselect='Equal';		$secondfield=0;	$val=1;	$realnumber=",maskRe: /[0-9\\.,]/"; break;
							case 'int':		$datestore='dataIntDate';	$xtype='numberfield';	$valueselect='Equal';		$secondfield=0;	$val=2;	break;
							case 'date':	$datestore='dataIntDate';	$xtype='textfield';		$valueselect='Equal';		$secondfield=0;	$val=3;	break;
							case 'boolean':	$datestore='dataBoolean';	$xtype='hidden';		$valueselect='Select';		$secondfield=0;	$val=4;	break;
						}
						
		if($iadvc2>0)	$cadresult	.=	",";		
		$cadresult	.=	"
			{ 
				xtype		:	'compositefield',
				id			:	'$composite".$arrdta[$iadvc]['idtc']."',
				name		:	'$composite".$arrdta[$iadvc]['idtc']."',
				anchor		:	'-5',
				fieldLabel	:	'".$arrdta[$iadvc]['titulos']."',
				tabTip		:	'".$arrdta[$iadvc]['Desc']."',
				items		:	[";
		//Apply this condition only for de boolean Type: this show a combo with yes or no values
		if($datestore === 'dataBoolean'){
			$cadresult	.=	"
			{
				width			:	100,
				xtype			:	'combo',
				mode			:	'local',
				triggerAction	:	'all',
				readOnly		:	".$readOnly.",
				forceSelection	:	true,
				editable		:	false,
				//name			:	'".'cb'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*".$idGrid."',
				name			:	'".$combo.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
				//id				:	'".'cb'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*".$idGrid."',
				id				:	'".$combo.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
				//hiddenName		:	'tx".$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
				hiddenName		:	'tx".$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
				displayField	:	'desc',
				valueField		:	'id',
				store			:	".$datestore.",
				//value			:	'".$valueselect."',
				emptyText		:	'Select ...',
				listeners		:	{
					'select'	:	selectBetween,
					render		:	function(c) {
						Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
					}
				}
			}";
		}else{
			if($arrdta[$iadvc]['type']<>'date')											 
			{
				if($arrdta[$iadvc]['type']=='txarea'){
					$cadresult	.=	"
					{
						xtype		:	'".$xtype."',
						//name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
						name		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
						//id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."' $realnumber ,
						id			:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."' $realnumber ,
						width		:	450,
						readOnly	:	".$readOnly.",
						height		:	100,
						listeners		:	{
							render		:	function(c) {
								Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
							}
						}
					}";
				
				}else{			
					$listener="";
					if($xtype=="realNumber"){
						$xtype="textfield";
						$listener="
							,change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '\$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						";
					}
					$cadresult	.=	"
						{
							xtype		:	'".$xtype."',
							//name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
							name		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
							//id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."' $realnumber ,
							readOnly	:	".$readOnly.",
							id			:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."' $realnumber ,
							autoCreate	:	{
								tag			:	'input', 
								autocomplete:	'off', 
								size		:	'".$arrdta[$iadvc]['size']."'
							},
							listeners		:	{
								render		:	function(c) {
									Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
								}
								$listener
							}
						}";
					if($secondfield==1){
						$cadresult	.=	"
						,{
							xtype		:	'".$xtype."',
							name		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
							readOnly	:	".$readOnly.",
							id			:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."' $realnumber ,
							autoCreate	:	{
								tag			:	'input', 
								autocomplete:	'off', 
								size		:	'".$arrdta[$iadvc]['size']."'}}";//
					}
				}
			}else{
				$cadresult	.=	"
					{
						xtype	:	'datefield',
						//name	:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
						name	:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
						//id		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."',
						id		:	'".$text.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos']."',
						width	:	93,
						readOnly	:	".$readOnly.",
						//format	:	'Ymd',
						format	:	'm/d/Y',
						listeners		:	{
							render		:	function(c) {
								Ext.QuickTips.register({target: c,text: '".$arrdta[$iadvc]['Desc']."'});
							}
						}
					}";
					/*Jesus: I separed this Code because i dont need a second datefield, it is utiliced on the search
					",{
						xtype	:	'datefield',
						name	:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
						id		:	'".'tx'.$arrdta[$iadvc]['Tabla'].'*'.$arrdta[$iadvc]['Campos'].'*'.$arrdta[$iadvc]['idtc']."*other".$idGrid."',
						format	:	'Ymd'
					}";*/
			}
		}
		$cadresult.= "
				]
			}";
		$iadvc2++;		
	}//for rigth
	$cadresult	.=	"
			]//items rigth column 
		}]//end items columns	
		";
			
		$imain++;
	}//while de todos los templates

	echo $cadresult;

 ?>