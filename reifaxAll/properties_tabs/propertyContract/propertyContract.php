<?php
	$aux = json_decode(stripslashes($_POST["data"]));
?>
<div id="propertyContract-CT"></div>
<script>
	var aux = '<?php echo stripslashes($_POST["data"]);?>';
	aux = Ext.decode(aux);
	PcDataForm = aux.data;
	aux = null;

	/**
	 * VType for Currency format
	 */

	Ext.apply(Ext.form.VTypes, {
		moneda : function(v) {
			return /^[\d]*(.[\d]{2})?$/.test(v);
		},
		monedaText: 'Wrong format in the amount.(Example: 1500.00)',
		monedaMask: /^[\d\.]/i
	});


	/**
	 * Stores for the combo options
	 */

	//Pc = propertyContract.php
	var PcStoreContract = new Ext.data.JsonStore({
		url:		'/custom_contract/includes/php/functions.php',
		method:		'post',
		totalProperty:	'total',
		root:		'results',
		baseParams:{
			idfunction:	"contratos"
		},
		fields: [
			{name:'idCustom', type:'int'},
			{name:'name', type:'string'},
			{name:'type', type:'string'}
		],
		autoLoad: true
	});
	
	PcStoreContract.on('load',function(ds,records,o){
			/////////////////////////////////Ejecutamos el Select del Combo de los Contratos////////////////////////////////
		if(records.length>0){
			Ext.getCmp('ctype').setValue(records[0].data.idCustom);
			Ext.getCmp('ctype').fireEvent('select',Ext.getCmp('ctype'),Ext.getCmp('ctype').store.getAt(Ext.getCmp('ctype').selectedIndex+1));
		}
		//////////////Le Incremente 1 al indice para que no tomara el espacio en blanco desde un principio//////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	});

	/**
	 *
	 * Panel Definitions
	 *
	 */

	// -- Addendum Panel (START)

	// Helper function to show windows for toolbar options
	function showWin(ttle,cmp) {

		win = new Ext.Window({
			title       : ttle,
			minimzable  : false,
			maximizable : false,
			resizable   : false,
			dragable    : false,
			modal       : true,
			id          : 'auxWin',
			hideMode    : 'offsets',
			layout      : 'fit',
			autoDestroy : true,
			autoWidth   : true,
			autoHeight  : true,
			items       : [ cmp ]
		});

		win.show();

	}

	// Add and save and addendum to the DB
	function agregar() {

		addendform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					allowBlank    : false
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						addendform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.msg);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});

		showWin('Add Addendum',addendform);

	}

	// Edit and save and addendum to the DB
	function modificar(sel,name,content) {

		var modifyform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					value         : name,
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					value         : content,
					allowBlank    : false
				},{
					xtype         : 'hidden',
					name          : 'id',
					value         : sel
				},{
					xtype         : 'hidden',
					name          : 'cmd',
					value         : 'mod'
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						modifyform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.mensaje);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});

		showWin('Edit Addendum',modifyform);

	}

	// START OPTIONS FOR THE GRID

	// Reader of the data
	var addReader = new Ext.data.JsonReader(
		{
			successProperty : 'success',
			root            : 'data',
			id              : 'id'
		}, Ext.data.Record.create([
			{name : 'id', type: 'int'},
			{name : 'user', type: 'int'},
			{name : 'name', type: 'string'},
			{name : 'active', type: 'string'}
		])
	);

	// Selection model for the checkbox column
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false
	});

	// columns of the grid
	var addColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			{  header: 'Name', dataIndex: 'name' },
			selmode
		]
	});

	// Toolbar definition.
	var toolbar = new Ext.Toolbar({
		items : [
			{
				text    : ' Add ',
				handler : agregar
			},{
				text    : ' Edit ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					if (selId.length > 0) {

						Ext.Ajax.request({
							url     : 'mysetting_tabs/mycontracts_tabs/listaddendum.php?cmd=chk&id='+selId[0].id,
							success : function(r,a) {
								var resp   = Ext.decode(r.responseText);
								modificar(selId[0].id, resp.data[0].name, resp.data[0].content);
							}
						});

					} else
						Ext.MessageBox.alert('','Please select the row to edit.');

				}
			},{
				text    : ' Delete ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					var ckEliminar = function(btn) {

						if (btn == 'yes') {

							Ext.Ajax.request({
								url     : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php?cmd=del&id='+selId[0].id,
								success : function(r) {
									Ext.MessageBox.alert('',r.responseText);
									Ext.getCmp('addGrid').store.reload();
								}
							});
						}
					}

					if (selId.length > 0)
						Ext.MessageBox.confirm('Confirm','Are you sure?',ckEliminar);
					else
						Ext.MessageBox.alert('','Please select the row to delete.');

				}
			}
		]
	});

	// END OPTIONS FOR THE GRID

	// Definition of the addendum grid
	var gridAdem = new Ext.grid.GridPanel({
		id              : 'addGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : true,
			url         : 'mysetting_tabs/mycontracts_tabs/listaddendum.php',
			reader      : addReader
		}),
		sm               : selmode,
		cm               : addColumn,
		height           : 150,
		frame            : true,
		tbar             : toolbar,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	
	//Definition of the panel with all the options and the grid
	var addend  = new Ext.Panel({
		xtype         : 'panel',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddend',
		title         : 'Addendum Options',
		width         : 435,
		labelWidth    : 150,
		items         : [ gridAdem ]

	});
	
	// -- Addendum Panel (END)
	
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false,
	});
	
	var addOnColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			{
				header: 'Name', 
				dataIndex: 'name' 
			},
			selmode,
			{
				header: 'Options', 
				width:60, 
				renderer: function(val,id,r){ 
					return '<input type="button" value="Update" onClick="modifyaddons_ini(\''+r.json.id+'\',\''+r.json.place+'\');">'; 
				}, 
				dataIndex: 'somefieldofyourstore'
			}
		]
	});
	
	// Definition of the addon grid
	var gridAdon = new Ext.grid.GridPanel({
		id              : 'addonGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : false,
			reader      : addReader,
			url         : 'mysetting_tabs/mycontracts_tabs/listaddons.php',
			listeners   : {
				'load'   : function () {										
					var recs = [];
					Ext.getCmp('addonGrid').getStore().each(function(rec){
						if(rec.data.active == 'true'){
							recs.push(rec);
						}
					});
					selmode.selectRecords(recs);		
				}
			}
		}),
		sm               : selmode,
		cm               : addOnColumn,
		height           : 130,
		frame            : true,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	

	//Definition of the panel with all the options and the grid
	var addond  = new Ext.Panel({
		xtype         : 'panel',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddon',
		title         : 'Additional Documents Options',
		width         : 435,
		labelWidth    : 150,
		items         : [ gridAdon ]

	});
	
	

	// -- Addon Documents Panel (END)

	// -- Additional Info Panel

    var addinfo = new Ext.Panel({
        xtype         : 'panel',
        border        : true,
        bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
        //hidden        : true,
        layout        : 'form',
        id            : 'formOpt',
        title         : 'Additional Info',
		//collapsible   : true,
		collapsed     : false,
        width         : 435,
        labelWidth    : 150,
        items         : [
            {
				xtype         : 'panel',
				border        : false,
				height        : 26,
				layout        : 'table',
				items         : [
					{
					    xtype         : 'label',
						text          : 'Offer price',
						width         : 155,
	                    cls           : 'x-form-item'
					},{
					    xtype         : 'textfield',
						id            : 'offer',
						name          : 'offer',
						style         : 'text-align:right',
						allowBlank    : true,
						vtype         : 'moneda'
					},{
						xtype         : 'button',
						id 			  : 'buttonoffer',
						hidden		  : true,	
						icon          : 'http://www.reifax.com/img/ximaicon/man3.png',
						handler       : function() {

							var factor  = 100.00;
							var roundTo = 500;
							
							var iOffer = new Ext.FormPanel({
								title         : 'Investor Offer',
								width         : 350,
								waitMsgTarget : 'Waiting...',
								labelWidth    : 75,
								defaults      : {width: 230},
								items: [
									{
										xtype         : 'numberfield',
										name          : 'factor',
										fieldLabel    : 'Offer Factor',
										value         : factor,
										maxValue      : 100,
										minValue      : 1
									},{
										xtype         : 'combo',
										mode          : 'local',
										fieldLabel    : 'Round To',
										triggerAction : 'all',
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['10','10`s'],
												['100','100`s'],
												['500','500`s'],
												['1000','1000`s']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'roundToName',
										value         : 500,
										hiddenName    : 'roundTo',
										hiddenValue   : 500,
										allowBlank    : false
									},{ 
										// Combo added by alexbariv - 20.04.2011
										xtype         : 'combo',
										mode          : 'local',
										fieldLabel    : 'Type',
										triggerAction : 'all',
										displayField  : 'texto',
										valueField    : 'id',
										name          : 'lowMedian',
										hiddenName    : 'lowMedian',
										value         : 1,
										allowBlank    : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['id', 'texto'],
											data      : [
												['1','Low'],
												['2','Median']
											]
										})
									}
								],							
								buttons : [
									{
										text    : 'Get Value',
										handler : function() {
											loading_win.show();
											
											var parcelids_cco='';
											if(smComp){
												if(!AllCheckComp){
													var comparables = smComp.getSelections();
													if(comparables.length > 0){
														parcelids_cco='\''+comparables[0].get('pid')+'\'';
														for(i=1; i<comparables.length; i++){
															parcelids_cco+=',\''+comparables[i].get('pid')+'\'';
														}
													}
												}	
											}
											
											var parcelids_cca='';
											if(smActive){
												if(!AllCheckActive){
													var comparables = smActive.getSelections();
													if(comparables.length > 0){
														parcelids_cca='\''+comparables[0].get('pid')+'\'';
														for(i=1; i<comparables.length; i++){
															parcelids_cca+=',\''+comparables[i].get('pid')+'\'';
														}
													}
												}	
											}
											
											var values = iOffer.getForm().getValues();
											factor     = values.factor;
											lowMedian  = values.lowMedian;
											roundTo    = values.roundTo;

											
											Ext.Ajax.request({
												url     : 'overview_contract/getInvestorOffer.php?db='+county+'&pid='+pid,
												method	: 'POST', 
												timeout : 600000,
												params	: { 
													lowMedian 		: lowMedian,
													factor    		: factor,
													roundTo   		: roundTo,
													parcelids_cco 	: parcelids_cco,
													parcelids_cca 	: parcelids_cca 
												},
												success : function(r) {
													loading_win.hide();
													iOfferWin.close();
													Ext.getCmp('offer').setValue(r.responseText);
													
												}
											});

										}
										
									},{
										text    : 'Cancel',
										handler : function() {
											iOffer.getForm().reset();
											iOfferWin.close();
										}
									}
								]
							});
							 
							var iOfferWin = new Ext.Window({
								layout      : 'fit',
								width       : 350,
								height      : 250,
								modal	 	: true,
								plain       : true,
								items		: iOffer
							});
							iOfferWin.show();
						}
					}
				]                                   
            },{
                xtype		:	'textfield',
                id			:	'deposit',
                name		:	'deposit',
                fieldLabel	:	'Initial Deposit',
                style		:	'text-align:right',
                allowBlank	:	true,
                vtype		:	'moneda'
            },{
                xtype		:	'textfield',
                id			:	'inspection',
                name		:	'inspection',
                fieldLabel	:	'Inspection Days',
                style		:	'text-align:right',
                emptyText	:	15,
                allowBlank	:	true
            },{
                xtype		:	'datefield',
                id			:	'dateAcc',
                name		:	'dateAcc',
                width		:	100,
                fieldLabel	:	'Acceptance date',
				value		:	PcDataForm.dateAccept,
                allowBlank	:	true
            },{
                xtype		:	'datefield',
                id			:	'dateClo',
                name		:	'dateClo',
                width		:	100,
                fieldLabel	:	'Closing date',
				value		:	PcDataForm.dateClose,
                allowBlank	:	true
            }
        ]
    });




	// -- Change buyer

	var buyer  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formBuy',
		title         : 'Change Buyer Info',
		width         : 435,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'buyername',
				name          : 'buyername',
				fieldLabel    : 'Buyer Name',
				allowBlank    : true
			}
		]
	});



	// -- Change seller

	var seller  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formSel',
		title         : 'Change Seller Info',
		width         : 435,
		labelWidth    : 150,
		items         : [
			{
				xtype		:	'textfield',
				id			:	'sellname',
				name		:	'sellname',
				fieldLabel	:	'Seller Name',
				value		:	PcDataForm.sellname,
				allowBlank	:	true
			}
		]
	});

	// -- Change agent
	/*
	function getAgents(btn){
		if(btn=='cancel'){
			return;
		}
		loading_win.show();
		var pidssend = pid;
		var useridspider = useridlogin;
		Ext.Ajax.request({  
			waitMsg: 'Checking...',
			url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
			method: 'POST',
			timeout :120000, 
			params: { 
				userid:useridspider,
				pids: pidssend,
				county: county
			},
					
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Operation Failure');
			},
			success:function(response,options){								
				var respresiden = Ext.decode(response.responseText);
				if(respresiden.mlsresidential==true || respresiden.num==0 || respresiden.num=='0'){
					Ext.Ajax.request({  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_getcontacts.php?updatecontact='+btn, 
						method: 'POST',
						timeout :120000, 
						params: { 
							pids: '\''+pidssend+'\''
							,'agent':respresiden.agent
							,'agentph':respresiden.agentph
							,'agentcell':respresiden.agentcell
							,'agentfax':respresiden.agentfax
							,'agentemail':respresiden.agentemail
							,'agenttollfree':respresiden.agenttollfree
							,'office':respresiden.office
							,'officeph':respresiden.officeph
							,'officefax':respresiden.officefax
							,'officeemail':respresiden.officeemail
							,'officetollfree':respresiden.officetollfree										
							,'runspider':respresiden.runspider										
							,'mlsresidential':respresiden.mlsresidential										
						},										
						failure:function(response1,options1){
							loading_win.hide();
							var output = '';
							for (prop in response1) {
							  output += prop + ': ' + response1[prop]+'; ';
							}
							Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
						},
						success:function(response,options){
							Ext.Ajax.request({
								url     : 'overview_tabs/getinfo.php?db='+county+'&pid='+pid,
								method  : 'POST',
								waitMsg : 'Getting Info',
								success : function(r) {
									var resp   = Ext.decode(r.responseText)
									Ext.getCmp('listingagent').setValue(resp.data.Agent);
									Ext.getCmp('listingbroker').setValue(resp.data.OfficeName);
									Ext.getCmp('rname').setValue(resp.data.Agent);
									Ext.getCmp('remail').setValue(resp.data.AgentEmail);
									loading_win.hide();
								},
								failure:function(response1,options1){
									loading_win.hide();
									var output = '';
									for (prop in response1) {
									  output += prop + ': ' + response1[prop]+'; ';
									}
									Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
								}
							});
							
						}
					});										
				}
				else{							
					var client = new XMLHttpRequest();
					client.onreadystatechange = function () {
						if (this.readyState == 4 && this.status == 200) {
							var resp = Ext.decode(this.responseText);
							
							Ext.Ajax.request({  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_getcontacts.php?updatecontact='+btn, 
								method: 'POST',
								timeout :120000, 
								params: { 
									pids: '\''+pidssend+'\''
									,'agent':resp.agent
									,'agentph':resp.agentph
									,'agentcell':resp.agentcell
									,'agentfax':resp.agentfax
									,'agentemail':resp.agentemail
									,'agenttollfree':resp.agenttollfree
									,'office':resp.office
									,'officeph':resp.officeph
									,'officefax':resp.officefax
									,'officeemail':resp.officeemail
									,'officetollfree':resp.officetollfree										
									,'runspider':resp.runspider										
									,'mlsresidential':resp.mlsresidential										
								},
								
								failure:function(response2,options2){
									loading_win.hide();
									var output = '';
									for (prop in response2) {
									  output += prop + ': ' + response2[prop]+'; ';
									}
									Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
								},
								success:function(response,options){
									Ext.Ajax.request({
										url     : 'overview_tabs/getinfo.php?db='+county+'&pid='+pid,
										method  : 'POST',
										waitMsg : 'Getting Info',
										success : function(r) {
											var resp   = Ext.decode(r.responseText)
											Ext.getCmp('listingagent').setValue(resp.data.Agent);
											Ext.getCmp('listingbroker').setValue(resp.data.OfficeName);
											Ext.getCmp('rname').setValue(resp.data.Agent);
											Ext.getCmp('remail').setValue(resp.data.AgentEmail);
											loading_win.hide();
										},
										failure:function(response1,options1){
											loading_win.hide();
											var output = '';
											for (prop in response1) {
											  output += prop + ': ' + response1[prop]+'; ';
											}
											Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
										}
									});
								}
							});										
						}
					};
					client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+useridspider+'&p='+pidssend);
					client.send();									
				}//else if(resp.inresidential==true)
			}
		});	
	}
	*/
	function getAgents(){
		loading_win.show();
		Ext.MessageBox.show({
			title:    'Contacts',
			msg:      'Overwrite existing contact?',
			buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
			fn: function(btn){
				if(btn=='cancel'){
					return;
				}
				var urlContact='mysetting_tabs/myfollowup_tabs/properties_getcontacts.php?updatecontact='+btn+'&blockproperties=no&overview=yes&db='+county+'&pid='+pid;
				Ext.Ajax.request({  
					waitMsg: 'Checking...',
					url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
					method: 'POST',
					timeout :120000, 
					params: { 
						userid:useridlogin,
						pids: pid,
						county: county,
						overview: 'yes'
					},
							
					failure:function(response,options){
						progressbar_win.hide();
						Ext.MessageBox.alert('Warning','Operation Failure');
					},
					success:function(response,options){								
						var respresiden = Ext.decode(response.responseText);

						if(respresiden.mlsresidential==true || respresiden.num==0 || respresiden.num=='0')
						{
							Ext.Ajax.request({  
								waitMsg: 'Checking...',
								url: urlContact, 
								method: 'POST',
								timeout :120000, 
								params: { 
									pids: '\''+pid+'\''
									,'agent':respresiden.agent
									,'agentph':respresiden.agentph
									,'agentcell':respresiden.agentcell
									,'agentfax':respresiden.agentfax
									,'agentemail':respresiden.agentemail
									,'agenttollfree':respresiden.agenttollfree
									,'office':respresiden.office
									,'officeph':respresiden.officeph
									,'officefax':respresiden.officefax
									,'officeemail':respresiden.officeemail
									,'officetollfree':respresiden.officetollfree										
									,'runspider':respresiden.runspider										
									,'mlsresidential':respresiden.mlsresidential
									,'userid':useridlogin
								},										
								failure:function(response1,options1){
									loading_win.hide();
									var output = '';
									for (prop in response1) {
									  output += prop + ': ' + response1[prop]+'; ';
									}
									Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
								},
								success:function(response,options){
									Ext.Ajax.request({
										url     : 'overview_tabs/getinfo.php?db='+county+'&pid='+pid,
										method  : 'POST',
										waitMsg : 'Getting Info',
										success : function(r) {
											var resp   = Ext.decode(r.responseText)
											Ext.getCmp('listingagent').setValue(resp.data.Agent);
											Ext.getCmp('listingbroker').setValue(resp.data.OfficeName);
											Ext.getCmp('rname').setValue(resp.data.Agent);
											Ext.getCmp('remail').setValue(resp.data.AgentEmail);
											loading_win.hide();
										},
										failure:function(response1,options1){
											loading_win.hide();
											var output = '';
											for (prop in response1) {
											  output += prop + ': ' + response1[prop]+'; ';
											}
											Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
										}
									});
									
								}
							});										
						}
						else
						{							
							var client = new XMLHttpRequest();
							client.onreadystatechange = function () {
								if (this.readyState == 4 && this.status == 200) {
									loading_win.hide();
									var resp = Ext.decode(this.responseText);
									
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url: urlContact, 
										method: 'POST',
										timeout :120000, 
										params: { 
											pids: '\''+pid+'\''
											,'agent':resp.agent
											,'agentph':resp.agentph
											,'agentcell':resp.agentcell
											,'agentfax':resp.agentfax
											,'agentemail':resp.agentemail
											,'agenttollfree':resp.agenttollfree
											,'office':resp.office
											,'officeph':resp.officeph
											,'officefax':resp.officefax
											,'officeemail':resp.officeemail
											,'officetollfree':resp.officetollfree										
											,'runspider':resp.runspider										
											,'mlsresidential':resp.mlsresidential
											,'userid':useridlogin										
										},
										
										failure:function(response2,options2){
											loading_win.hide();
											var output = '';
											for (prop in response2) {
											  output += prop + ': ' + response2[prop]+'; ';
											}
											Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
										},
										success:function(response,options){
											Ext.Ajax.request({
												url     : 'overview_tabs/getinfo.php?db='+county+'&pid='+pid,
												method  : 'POST',
												waitMsg : 'Getting Info',
												success : function(r) {
													var resp   = Ext.decode(r.responseText)
													Ext.getCmp('listingagent').setValue(resp.data.Agent);
													Ext.getCmp('listingbroker').setValue(resp.data.OfficeName);
													Ext.getCmp('rname').setValue(resp.data.Agent);
													Ext.getCmp('remail').setValue(resp.data.AgentEmail);
													loading_win.hide();
												},
												failure:function(response1,options1){
													loading_win.hide();
													var output = '';
													for (prop in response1) {
													  output += prop + ': ' + response1[prop]+'; ';
													}
													Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
												}
											});
											
										}
									});										
								}
							};
							client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+useridlogin+'&p='+pid+'&c='+county+'&overview=yes');
							client.send();									
						}//else if(resp.inresidential==true)
					}
				});		
			}
		});	
	}
	
	var agentsChange  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAgentChange',
		title         : 'Change Agents Info',
		width         : 450,
		labelWidth    : 150,
		items         : [
			{
				xtype	:	'panel',
				border	:	false,
				height	:	26,
				layout	:	'table',
				items	:	[
					{
					    xtype	:	'label',
						text	:	'Seller\'s Agent',
						width	:	155,
	                    cls		:	'x-form-item'
					},{
					    xtype		:	'textfield',
						id			:	'listingagent',
						name		:	'listingagent',
						value		:	PcDataForm.Agent,
						allowBlank	:	true
					},{
						xtype	:	'button',
						text	:	'Get Agent',
						handler	:	function() {
							getAgents();
						}
					}
				]                                   
            },{
				xtype		:	'textfield',
				id			:	'remail',
				name		:	'remail',
				fieldLabel	:	'Agent Email',
				value		:	PcDataForm.AgentEmail,
				allowBlank	:	true
			},{
				xtype		:	'textfield',
				id			:	'listingbroker',
				name		:	'listingbroker',
				fieldLabel	:	'Seller\'s Broker',
				value		:	PcDataForm.OfficeName,
				allowBlank	:	true
			},{
				xtype		:	'textfield',
				id			:	'buyeragent',
				name		:	'buyeragent',
				fieldLabel	:	'Buyer\'s Agent',
				allowBlank	:	true
			},{
				xtype		:	'textfield',
				id			:	'buyerbroker',
				name		:	'buyerbroker',
				fieldLabel	:	'Buyer\'s Broker',
				allowBlank	:	true
			}
		]
	});
	
/*
* Para Jesus Esto ya no Sirve
*  var checks  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		bodyStyle     : 'padding : 5px;',
		id            : 'formChecks', 
		width         : 430,
		items         : [
			{
				
				xtype   : 'checkboxgroup',
				columns : 3,
				items   : [
					{
						xtype         : 'checkbox',
						id            : 'scrow',
						name          : 'scrow',
						boxLabel      : 'Escrow Letter',
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);}
					}
				]
			}
		]
	});
*/						
	var checksDocuments  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		id            : 'formChecksDocuments',
		width         : 450,
		title		  : 'Documents to Show in the Follow Up System',
		//hidden		  : true,	
		//labelWidth	  :	0,
		layout        : 'form',
		items         : [
		{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 2 },
			items        : [
				{
					layout: 'form',
					labelWidth	: 100,
					items:[
						{
							xtype:'checkbox',
							fieldLabel: 'Proof of Funds', 
							name: 'pof'
						}
					]
				},{
					layout: 'form',
					labelWidth	: 140,
					items:[
						{
							xtype:'checkbox',
							fieldLabel: 'Earnest money Deposit', 
							name: 'emd'
						}
					]
				},{
					layout: 'form',
					labelWidth	: 100,
					items:[
						{										
							xtype:'checkbox',
							fieldLabel: 'Addendums', 
							name: 'rademdums'
						}
					]
				}
			]
		}]
	});
	
	var panelMailSetting=new Ext.FormPanel({
								title      : 'Please Configure your Mail Settings',
								border     : true,
								bodyStyle  : 'padding: 10px; text-align:left;',
								frame      : true,
								fileUpload : true,
								method     : 'POST',
								items      : [
									{
										html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
										cls         : 'x-form-item',
										style       : {
											marginBottom : '10px'
										}
									},{
										xtype      : 'textfield',
										fieldLabel : 'Server Address',
										name       : 'server',
										width      : '200'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
									},{
										xtype      : 'textfield',
										fieldLabel : 'Port Number',
										name       : 'port',
										width      : '200',
										value      : '25'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
									},{
										xtype      : 'textfield',
										fieldLabel : 'Server Username',
										name       : 'username',
										width      : '200'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
									},{
										xtype      : 'textfield',
										inputType  : 'password',
										fieldLabel : 'Server Password',
										name       : 'password',
										width      : '200'
									}
								],
								buttonAlign :'center',
								buttons     : [{
									text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
									handler : function(){
										if (panelMailSetting.getForm().isValid()) {
											panelMailSetting.getForm().submit({
												url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
												waitMsg : 'Saving...',
												success : function(f, a){ 
													var resp = a.result;
													alert(resp.mensaje);
													if(resp.savesett=='yes'){
														winMailSett.hide();
														genFinalContract(0);
													}
														
												}
											});
										}
									}
								}]
							});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	// -- Email Delivery panel
	
	function button_generate_and_send(){
		var check_email = Ext.getCmp('sendmail').getValue();
		var check_fax = Ext.getCmp('sendfax').getValue();
		var check_to_me = Ext.getCmp('sendme').getValue();
		if(check_email == true || check_fax == true || check_to_me == true)
			Ext.getCmp('sendBut').show();
		else
			Ext.getCmp('sendBut').hide();							
	}
	
	var email  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formDel',
		title         : 'Email Delivery',
		width         : 450,
		//labelWidth    : 150,
		items         : [
			{
				xtype        : 'panel',
				layout       : 'table',
				layoutConfig : { columns : 2 },
				items        : [
				{
					layout: 'form',
					labelWidth	: 140,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendmail',
						name          : 'sendmail',
						fieldLabel    : 'Send Contract By Email',
						//checked		  : true,
						handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
							}
						}
					}]
				},{
					layout: 'form',
					labelWidth	: 130,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendfax',
						name          : 'sendfax',
						fieldLabel    : 'Send Contract By Fax',
						handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
							}
						}
					}]
				},{
					layout: 'form',
					labelWidth	: 140,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendme',
						name          : 'sendme',
						fieldLabel    : 'Send Copy to me',
						handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
							}
						}
					}]
				},{
					layout: 'form',
					labelWidth	: 130,
					items:[
						{										
							xtype         : 'checkbox',
							id            : 'scrow',
							name          : 'scrow',
							fieldLabel    : 'Escrow Letter',
							onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
							onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);}
						}
					]
				},{
					layout: 'form',
					labelWidth	: 60,
					items:[
					{
						xtype         : 'combo',
						name          : 'ccontracttemplate',
						id            : 'ccontracttemplate',
						hiddenName    : 'contracttemplate',
						fieldLabel    : 'Template',
						typeAhead     : true,
						autoSelect    : true,
						hidden		  : true,
						mode          : 'local',  
						store         : new Ext.data.JsonStore({
							root:'results',
							totalProperty:'total',
							autoLoad: true, 
							baseParams: {
								type: 'emailtemplates'
							},
							fields:[
								{name:'id', type:'int'},
								{name:'name', type:'string'},
								{name:'default', type:'int'}
							],
							url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
							listeners     : {
								'load'  : function(store, records) {
									var sel=records[0].get('id');
									//Ext.getCmp('ccontracttemplate').setValue(sel); 
									for(i=1;i<records.length;i++){
										if(records[i].get('default')==1)
											sel=records[i].get('id');
									}
									Ext.getCmp('ccontracttemplate').setValue(sel); 
								}
							}
						}),
						triggerAction : 'all', 
						editable      : false,
						selectOnFocus : true,
						allowBlank    : false,
						displayField  : 'name',
						valueField    : 'id',
						value		  : 0,
						width         : 170
					}]
				},{
					xtype	:	'hidden',
					value	:	PcDataForm.Agent,
					id		:	'rname',
					name	:	'rname'
				}]
			}
		]

	});



	// -- genFinalContract
	//     Function to generate the contract


	function genFinalContract(gen) {
		loading_win.show();

		selId = Ext.getCmp('addGrid').selModel.selections.items;
		var temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddeum').setValue(temp);

        selId = Ext.getCmp('addonGrid').selModel.selections.items;
		temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddonG').setValue(temp);
		if (gen == 1){
			Ext.getCmp('sendbyemail').setValue('no');
		}
		if (gen == 0){
			Ext.getCmp('sendbyemail').setValue('yes');
		}
		generate.getForm().url='overview_contract/generatecontract.php';
		generate.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				loading_win.hide();
				var Digital=new Date()
				var hours=Digital.getHours()
				var minutes=Digital.getMinutes()
				var seconds=Digital.getSeconds()
				var url = 'http://www.reifax.com/'+action.result.pdf;
				//var url = 'http://www.reifax.com/custom_contract/view/web/viewer.html?url=/'+action.result.pdf;
				if (gen == 1){
					//window.open('http://docs.google.com/gview?url='+url+'&embedded=true'); //Agregado por Smith
					window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
					//window.open(url); //Agregado por Jesus
				}
				if (gen == 0){
					Ext.MessageBox.alert('', 'Email sent');	
				}
				if (gen == 2){
					window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
				}
			},
			failure : function(form, action) {
				loading_win.hide();
				Ext.MessageBox.alert('Warning', action.result.msg);
			}
		});
	}

	function genFinalContract1(gen) {
		loading_win.show();

		selId = Ext.getCmp('addGrid').selModel.selections.items;
		var temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddeum').setValue(temp);

        selId = Ext.getCmp('addonGrid').selModel.selections.items;
		temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddonG').setValue(temp);
		if (gen == 1){
			Ext.getCmp('sendbyemail').setValue('no');
		}
		if( gen == 0){
			Ext.getCmp('sendbyemail').setValue('yes');
		}
		generate.getForm().url='custom_contract/pepe.php';
/*							var form = generate.getForm();
		var el = form.getEl().dom;
		 
		var target = document.createAttribute("target");

		target.nodeValue = "_blank";
		el.action = "custom_contract/pepe.php"; 
		el.setAttributeNode(target);
		el.submit();*/
		
		generate.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				loading_win.hide();
				var Digital=new Date()
				var hours=Digital.getHours()
				var minutes=Digital.getMinutes()
				var seconds=Digital.getSeconds()
				var url = 'http://www.reifax.com/'+action.result.pdf;
				//var url = 'http://www.reifax.com/custom_contract/view/web/viewer.html?url=/'+action.result.pdf;
				if (gen == 1){
					//window.open(url);
					//window.open('http://docs.google.com/gview?url='+url+'&embedded=true'); //Agregado por Smith
					window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
					//window.open(url); //Agregado por Jesus
				}
				if (gen == 0){
					Ext.MessageBox.alert('', 'Email sent');	
				}
				if (gen == 2){
					window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
				}
			},
			failure : function(form, action) {
				loading_win.hide();
				Ext.MessageBox.alert('Warning', action.result.msg);
			}
		});
	}

	// -- Main panel
	//     Include generate options, Additional info
	//     and email delivery

	function modifyContract(typecontract,placecontract){
		if(typecontract=="addon"){
			//var idcontract = Ext.getCmp('addon_id'+placecontract).getValue();
		}else{
			var idcontract = Ext.getCmp('ctype').getValue();			
		}
		
		if(idcontract && idcontract!=0)
		{
			//Ext.MessageBox.alert('',idcontract);
			new Ext.util.Cookies.set("idmodifycontract", idcontract);
			new Ext.util.Cookies.set("typemodifycontract", typecontract);
			if(typecontract=="addon"){
				new Ext.util.Cookies.set("placemodifycontract", placecontract);
			}

			if(document.getElementById('idmodifycontract')){
				var tab = tabs.getItem('idmodifycontract');
				tabs.remove(tab);
			}

			Ext.getCmp('tabs-pt').add({
				//xtype: 'component', 
				title: 'Modify Contract', 
				closable 	: true,
				autoScroll	: true,
				id: 'idmodifycontract', 
				autoLoad:	{url:'/custom_contract/manager.php',scripts:true}
/*									autoEl: { 
						tag: 'iframe', 
						width: '100%', 
						height: '100%', 
						focusOnLoad: true, 
						frameborder: 0, 
						src: '../../custom_contract/index.php'
				}*/
			}).show();
/*				Ext.Ajax.request({
				url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php?cmd=del&id='+idcontract,
				waitMsg : 'Deleting...',
				success : function(r) {
					Ext.MessageBox.alert('',r.responseText);
					var tab     = tabs3.getActiveTab();
					var updater = tab.getUpdater();
					updater.update({
						url   : 'mysetting_tabs/mycontracts_tabs/mycontracts1.php',
						cache : false
					});					
				}
			});*/
		}

	}
	
	var emailConfig = '0';
	generate = new Ext.FormPanel({
		url           	:	'overview_contract/generatecontract.php',
		frame         	:	true,
		monitorValid  	:	true,
		id            	:	'GenMainPanel',
		renderTo		:	"propertyContract-CT",
		//title         	:	'Select Contract.',
		labelWidth    	:	130,
		waitMsgTarget 	:	'Generating...',
		layout		  	:	'column',
		defaults		:	{
			columnWidth	:	'.5',
			bodyStyle	:  	'paddingLeft:80px'
		},
		items:[
			{
				layout: 'form',
				autoHeight: true,
				items : [
					{
						xtype         	:	'combo',
						name          	:	'ctype',
						id            	:	'ctype',
						hiddenName    	:	'type',
						fieldLabel    	:	'Contract',
						typeAhead     	:	true,
						store         	:	PcStoreContract,
						triggerAction 	:	'all', 
						mode          	:	'local',
						editable      	:	false,
						autoLoad		:	true,
						selectOnFocus 	:	true,
						//allowBlank    	:	false,
						displayField  	:	'name',
						valueField    	:	'idCustom',
						//value		  	:	defaultcontract,	//OJO EVALE
						width         	:	260,
						listeners     	:	{
							'select'  	:	function(combo, record, index) {
								var tp = Ext.getCmp('ctype').getValue();
								Ext.getCmp('addonGrid').store.load({params: {type : tp}});
							}
						}
					},{
						xtype    : 'button',
						id		 : 'buttonmoddifycontract2',
						hidden	 : true,
						cls		 : 'x-btn-text-icon',
						icon	 : '/img/update.png',
						iconAlign: 'left',
						text     : 'Modify contract',
						scale:		'medium',
						style    : {
							marginRight : 10,
							marginLeft  : 150
						},
						handler  : function (){
							var navegador=navigator.userAgent;
							if(navegador.indexOf('MSIE') != -1) {
								if(parseInt(BrowserDetect.version)<9){
									Ext.Msg.show({
									   title      : '',
									   //msg        : 'This option works only with Firefox and Chrome. Internet Explorer users please download and use any of the mentioned browsers.',
									   msg		  : 'This option only works with IE9 or above.',
									   width      : 400,
									   buttons    : Ext.MessageBox.OK,
									   icon       : Ext.MessageBox.INFO
									})
								}else{
									modifyContract("contract","0");
								}
							}else{
								modifyContract("contract","0");
							}
						}					
						//handler  : deleteContract
					},{
						xtype	:	'hidden',
						name	:	'county'
						//value	:	county	//OJO EVALUE
					},{
						xtype	:	'hidden',
						name	:	'pid',
						//value	:	pid
					},{
						xtype	:	'hidden',
						value	:	PcDataForm.MLNumber,
						name	:	'mlnaux',
						id		:	'mlnaux'
					},{
						xtype	:	'hidden',
						name	:	'fieldAddeum',
						id		:	'fieldAddeum'
					},{
						xtype	:	'hidden',
						name	:	'fieldAddonG',
						id		:	'fieldAddonG'
					},{
						xtype	:	'hidden',
						name	:	'addr',
						id		:	'addr'
					},{
						xtype	:	'hidden',
						name	:	'sendbyemail',
						id		:	'sendbyemail'
					},
					agentsChange,
					email,
					checksDocuments
				]
		},{
			layout: 'form',
			autoHeight: true,
			items : [
				buyer,
				seller,
				addinfo,
				addend,
				addond
			]
		}], 		
		style       : "text-align:left",
		formBind    : true,
		buttonAlign : 'center',
		buttons     : [
			{
				text    : 'Generate and Send',
				handler : function() {
                    var sendmail = Ext.getCmp('sendmail').getValue();
					var sendme = Ext.getCmp('sendme').getValue();
					var deposit = Ext.getCmp('deposit').getValue();
					var offer = Ext.getCmp('offer').getValue();
					if(deposit==0 || deposit=='' || offer==0 || offer==''){
						Ext.Msg.alert('', 'Offer price and deposit cannot be empty');
						return false;
					}
					if(!sendme && !sendmail){
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php',
						method  : 'POST',
						params: { module:'mailsettings',
								  action:''	
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							var resp   = Ext.decode(r.responseText)
							if(resp.mail=='true'){
								Ext.getCmp('ctype').getStore().each(function(rec){
									if(rec.data.id == Ext.getCmp('ctype').getValue()){
										if(rec.data.type=="N"){
											genFinalContract1(0);
										}else{
											genFinalContract(0);
										}
									}
								});										
							}else{
								winMailSett.show();
							}
						}
					});
					return;
					//genFinalContract(0);
				},
				id      : 'sendBut',
				hidden  : true
			},
/*								{
				text    : 'View',
				handler : function() {
					if(navigator.appName=='Microsoft Internet Explorer'){
						Ext.Msg.alert('', 'Regret to inform that due to incompatibility from IE, the contract will not be shown.');
					}
					genFinalContract(1);
				},
				hidden  : false
			},*/
			{
				text    : 'View',
				handler : function() {
					/*if(navigator.appName=='Microsoft Internet Explorer'){
						Ext.Msg.alert('', 'Regret to inform that due to incompatibility from IE, the contract will not be shown.');
					}*/
/*										var navegador=navigator.userAgent;
					if(navegador.indexOf('MSIE') != -1) {
						Ext.Msg.show({
						   title      : '',
						   msg        : 'This option is compatible with Firefox and Chrome. Internet Explorer users must download the contract instead and see the donwloaded file.',
						   width      : 400,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.INFO
						})
					}else{*/
						Ext.getCmp('ctype').getStore().each(function(rec){
							if(rec.data.id == Ext.getCmp('ctype').getValue()){
								if(rec.data.type=="N"){
									genFinalContract1(1);
								}else{
									genFinalContract(1);
								}
							}
						});
					//}
				},
				hidden  : false
			},{
				text    : 'Download',
				handler : function() {
					Ext.getCmp('ctype').getStore().each(function(rec){
						if(rec.data.id == Ext.getCmp('ctype').getValue()){
							if(rec.data.type=="N"){
								genFinalContract1(2);
							}else{
								genFinalContract(2);
							}
						}
					});										
				},
				hidden  : false
			},{
				text    : 'Close',
				handler : function(){
					//win.close();
					var tab = tabs.getItem('generate_contractPT');
					tabs.remove(tab);										
				}
			}
		],
		listeners:{
			afterrender:function(){				
			}
		}
	});
	/**
	 *
	 * Window Definition
	 *
	 */
	/*var message = "<div style=\"padding:10px 0 10px 30px\">In order to be able to edit the ";
	message += "contracts you must have Abobe Reader Pro.<br>";
	message += "You can also do it with the following  free ";
	message += "softwares FoxitReader at ";
	message += "<a href='http://www.foxitsoftware.com/pdf/reader/' target='_new'>";
	message += "http://www.foxitsoftware.com/pdf/reader/</a> ";
	message += "and PDF-XChange viewer at ";
	message += "<a href='http://www.tracker-software.com/product/pdf-xchange-viewer' target='_new'>";
	message += "http://www.tracker-software.com/product/pdf-xchange-viewer</a></div>";
	
	var dismessage="<div style=\"padding:10px 0 10px 30px\">WARNING<br/>";
	dismessage+="The following Document Generator feature of REIFAX.com (herein after known as REIFAX) is for the sole purpose of modifying existing documents that the user uploads to the system.";
	dismessage+="No documents will be supplied by REIFAX .";
	dismessage+="The user MUST HAVE legal authority to upload, modify, change, fill-in and use any document he intends to modify and submit to another buyer or seller.";
	dismessage+="Legal authority means that if a copyrighted document is uploaded, the user MUST HAVE written permission of the person or entity who drafted the document, or have paid an authorized re-seller for the use of that document, i.e. FAR/BAR forms.";
	dismessage+="By uploading any document, the user hereby agrees to these terms and conditions for using the REIFAX Document Generator.";
	dismessage+="Violation of these terms and conditions could result in the user violating copyright laws.";
	dismessage+="The only information that will be filled in by REIFAX, on any form, if applicable is information that is available in the public record.";
	dismessage+="Any and all other information will have to be supplied by the user.</div>";
	var disclaimer = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		items       : [
			{ xtype: "panel", html: dismessage }
		],
		buttons : [{
				text     : 'Accept',
				handler  : function(){
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php',
						method  : 'POST',
						params: { module:'acceptcontract',
								  action:'accept'	
						},
						waitMsg : 'Wait...',
						success : function(r) {
							var resp   = Ext.decode(r.responseText)
							if(resp.msg=='true'){
								disclaimer.close();
								Ext.Ajax.request({
									url     : 'overview_contract/verifications.php',
									method  : 'POST',
									params: { module:'mailsettings',
											  action:''	
									},
									waitMsg : 'Getting Info',
									success : function(r) {
										var resp   = Ext.decode(r.responseText)
										if(resp.mail=='true'){
											Ext.Ajax.request({
												url     : 'overview_tabs/getinfo.php?db='+county+'&pid='+pid,
												method  : 'POST',
												waitMsg : 'Getting Info',
												success : function(r) {
						
													var resp   = Ext.decode(r.responseText)
													Ext.getCmp('dateAcc').setValue(resp.data.dateAccept);
													Ext.getCmp('dateClo').setValue(resp.data.dateClose);
													Ext.getCmp('mlnaux').setValue(resp.data.MLNumber);
													Ext.getCmp('sellname').setValue(resp.data.sellname);
													
													emailConfig = resp.data.mailconf;
													if (resp.data.platinum == '1')
														Ext.getCmp('ccontracttemplate').show();
													if (resp.data.professional_esp == '1')
														Ext.getCmp('buttonoffer').show();
													
													Ext.getCmp('scrow').show();
													
													if(resp.data.userid=='2223' || resp.data.userid=='2607'){
														Ext.getCmp('listingagent').setValue(resp.data.Agent);
														Ext.getCmp('buyeragent').setValue(resp.data.Agent);
													}else{
														Ext.getCmp('listingagent').setValue(resp.data.Agent);
														Ext.getCmp('listingbroker').setValue(resp.data.OfficeName);
														Ext.getCmp('rname').setValue(resp.data.Agent);
														Ext.getCmp('remail').setValue(resp.data.AgentEmail);
													}
													var tp = Ext.getCmp('ctype').getValue();
													Ext.getCmp('addonGrid').store.load({params: {type : tp}});
						
												}
											});
											win.show();
										}else{
											Ext.Msg.alert('', 'To be able to send contracts you must first set your email within Reifax.<br> Please go to -> '+resp.settings+' and input your email settings.');
										}
									}
								});
							}
						}
					});
					disclaimer.close();
				}
			},{
				text     : 'Decline',
				handler  : function(){
					disclaimer.close();
				}
			}]
	});
	var win = new Ext.Window({
		autoWidth   : true,
		autoHeight  : true,
		//modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				generate.getForm().reset();
			}
		},
		items       : [
			generate,
			{ xtype: "panel", html: message }
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				win.close();
			}
		}]
	});*/
</script>