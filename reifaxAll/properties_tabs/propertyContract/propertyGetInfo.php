<?php
/**
* propertyGetInfo.php
*
* Save the addons of a document.
* 
* @autor   Jesus Marquez <jesusdaredevil@gmail.com>
* @version 29.08.2012
*/

require('../../properties_conexion.php');
conectar();

$pid    = $_GET['pid'];
$userid = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

$query   = "SELECT * FROM mlsresidential WHERE parcelid='$pid'";
$rs      = mysql_query($query);

if ($rs) {
	
	if(mysql_affected_rows()>0){
		$row  = mysql_fetch_assoc($rs);
		
		$row['userid']=$userid;
		$row['dateAccept'] = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")));
		$row['dateClose']  = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")));

		$query	=	"SELECT platinum FROM permission WHERE userid='$userid'";
		$rs		=	mysql_query($query);
		if(mysql_affected_rows()>0)
			$rpinv	=	mysql_fetch_assoc($rs);
		else
			$rpinv	=	Array();
		
		$row['platinum']	=	$rpinv[0];
		
		$query	=	"SELECT professional_esp FROM permission WHERE userid='$userid'";
		$rs		=	mysql_query($query);
		if(mysql_affected_rows()>0)
			$rpinv	=	mysql_fetch_assoc($rs);
		else
			$rpinv	=	Array();
		
		$row['professional_esp']   = $rpinv[0];
		
		$query             = "SELECT owner from psummary where parcelid='$pid'";
		$rs                = mysql_query($query);
		if(mysql_affected_rows()>0)
			$rpinv	=	mysql_fetch_assoc($rs);
		else
			$rpinv	=	Array();
		
		$row['sellname']   = $rpinv['owner'];

		$query              = "SELECT count(*) as total FROM contracts_mailsettings WHERE userid = $userid";
		$rs                 = mysql_query($query);
		if(mysql_affected_rows()>0)
			$mailInfo	=	mysql_fetch_assoc($rs);
		else
			$mailInfo	=	Array();

	    $row['mailconf']    = 0;
		if ($mailInfo['total']>0)
			$row['mailconf'] = 1;
		
		$query="select bd, offer, address, mlnumber, a.* from followup f
			LEFT JOIN followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$pid."')";
		$result=mysql_query($query) or die($query.mysql_error());
		
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_assoc($result);
			$remail     = $r['email'];
			
			if($remail==''){
				$remail = $r['email2'];
			}
			if($remail!=''){
				$row['Agent']=$r['agent'];
				$row['AgentEmail']=$remail;
				$row['OfficeName']=$r['company'];
			}
		}
		
		$resp = array('success'=>'true','data'=>$row);	
	}else{
		$resp = array('success'=>'true','data'=>Array());	
	}
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

if ($error != "") 
	$resp = array('success'=>'true','mensaje'=>$error);

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	