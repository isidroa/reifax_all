<div align="center" class="fondo_realtor_result_tab">
	<div align="center" style="width:100%; margin:auto; margin-top:0px;">
		<div id="mapImport" style="width:99%; height:450px; border: 1px solid #4E9494; position:relative; float:center; margin-right:0px;"></div>
		<div id="mapa_search_latlong2"></div>
		<div id="control_mapa_div1"></div>
		<div></div>
	</div>
</div>
<script>
    var SpaceNeedleImport = new VELatLong(mapsLatLonImport.latitude,mapsLatLonImport.longitude);
	var mapImport = new XimaMap('mapImport','mapa_search_latlong2','control_mapa_div2','_pan2','_draw2','_poly2','_clear2','_maxmin2');
    mapImport.map = new VEMap('mapImport');
    mapImport.map.LoadMap(SpaceNeedleImport, 15, VEMapStyle.BirdseyeHybrid);
    var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedleImport);
    pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' > <img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
    mapImport.map.AddShape(pin);
</script>