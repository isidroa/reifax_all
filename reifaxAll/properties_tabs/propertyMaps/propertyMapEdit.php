<div align="center" class="fondo_realtor_result_tab">
	<div align="center" style="width:100%; margin:auto; margin-top:0px;">
		<div id="mapEdit" style="width:99%; height:450px; border: 1px solid #4E9494; position:relative; float:center; margin-right:0px;"></div>
		<div id="mapa_search_latlong1"></div>
		<div id="control_mapa_div1"></div>
		<div></div>
	</div>
</div>
<script>
    var SpaceNeedleEdit = new VELatLong(mapsLatLonEdit.latitude,mapsLatLonEdit.longitude);
	var mapEdit = new XimaMap('mapEdit','mapa_search_latlong1','control_mapa_div1','_pan1','_draw1','_poly1','_clear1','_maxmin1');
    mapEdit.map = new VEMap('mapEdit');
    mapEdit.map.LoadMap(SpaceNeedleEdit, 15, VEMapStyle.BirdseyeHybrid);
    var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedleEdit);
    pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' > <img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
    mapEdit.map.AddShape(pin);
</script>