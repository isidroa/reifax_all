	<style>
		#galleryImport { float: left; width: 65%; /*min-height: 12em;*/ } * html #galleryImport { /*height: 12em;*/ } /* IE6 */
		.galleryImport.custom-state-active { background: #eee; }
		.galleryImport li { float: left; width: 96px; height: 120px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
		.galleryImport li h5 { margin: 0 0 0.4em; cursor: move; }
		.galleryImport li a { float: right; }
		.galleryImport li div { height:16px; width:100%; position: relative; display:none; }
		.galleryImport li a.icon-zoom-in { float: left; }
		.galleryImport li a.icon-check { left: 50%; margin-left: -6px; position: absolute; }
		.galleryImport li a.icon-trash { float: right; }
		.galleryImport li img { width: 100%; cursor: move; }

		#trashImport { float: right; width: 32%; min-height: 18em; padding: 1%;} * html #trashImport { height: 18em; } /* IE6 */
		#trashImport h4 { line-height: 16px; margin: 0 0 0.4em; }
		#trashImport h4 .ui-icon { float: left; }
		#trashImport .galleryImport h5 { display: none; }
	</style>
	
	<input type="file" id="file-Image-Import"/>
	
	<div align="left" id="principal-Image-Import" style="background-color:#FFF;border-color:#FFF">
		<br clear="all" />
		<div align="center" style=" background-color:#FFF; margin:auto; width:950px;">
	  		<div id="list-Images-Import"></div>
		</div>
		<br clear="all" />
	</div>
	<script>
	/******************
	*	To Clear Always Images Cache
	******************/
	var aux=	new Date();
	var tempCacheImagesImport	=	(aux.getHours()).toString() + (aux.getMinutes()).toString() + (aux.getSeconds()).toString();
	/*******END*******/
	
	var totalImagesImport,imagesToRegister,defaultImageImport;
	$("#file-Image-Import").hide();
	
	function imageDefaultImport(){
		if($( "a.icon-check[default=true]" ).length){
			var aux = $("#galleryImport a.icon-check[default=true]").length == 0	?	""	:	$("#galleryImport a.icon-check[default=true]").attr("href")	;
			if(aux != ""){
				aux = aux.split("temp/");
				return aux[1];
			}else{
				return "";
			}
		}
	}
	
	function CreateArrayImagesImport(){
		imagesToRegister = Array();
		$.each($("#galleryImport img"), function(i, ite){
			var aux = $(this).attr("src").split("thumb/");
			imagesToRegister.push(aux[1]);
		});
		return imagesToRegister;
	}
		
	var toolbarImageImport	=	new	Ext.Toolbar({
		renderTo	:	'list-Images-Import',
		items		:[
			new Ext.Button({
				tooltip: 'Upload New Image',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-plus"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					$("#file-Image-Import").trigger('click');
				}
			}),
			new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'FollowupContact'} }
					});
					win.show();
				}
			}),
			new Ext.Button({
				tooltip: 'Test Temp By JEsus',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					imageDefaultImport();
					//var a = $("#galleryImport a.icon-check[default=true]").length == 0	?	""	:	$("#galleryImport a.icon-check[default=true]").attr("href")	;
					//Ext.getCmp('displayImageImport').autoEl.src='/propertiesImages/2482/120/2.jpg';	//Nunca lo Probe si Funcionaba
					//Ext.getCmp('displayImageImport').getEl().dom.src='/propertiesImages/2482/120/2.jpg';
				}
			})
		]
	});


	$(document).ready(function (){
		//Variable Receibed with namespace Ext from propertyResult.php
		var imagesFinal		=	typeof imagesImport.Import == "undefined"	?	Array() :	imagesImport.Import;
		if(typeof imagesImport.Import.temp != "undefined")
			totalImagesImport	=	imagesImport.Import.temp.length;
		else
			totalImagesImport	=	0;
		
		var $galleryImport = $( "#galleryImport" ),
				$trashImport = $( "#trashImport" );
		// image deletion function  
		var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='icon-refresh'></a>";
		
		function deleteImage( $item ) {
			$item.fadeOut(function() {
				var $list = $( "ul", $trashImport ).length ?
					$( "ul", $trashImport ) :
					$( "<ul class='galleryImport ui-helper-reset'/>" ).appendTo( $trashImport );

				$item.find( "a.icon-trash" ).remove();
				$item.find( "a.icon-check" ).hide();
				$item.children("div").append( recycle_icon );
				$item.appendTo( $list ).fadeIn(function() {
					$item
						.animate({ width: "48px" ,height: "52px"})
						.find( "img" )
							.animate({ height: "36px" });
				});
				
				if($item.find( "a.icon-check[default=true]" ).length){
					if($("#galleryImport li:first").length)
						$("#galleryImport li:first a:eq(1)").trigger('click');
					else{
						//Ext.getCmp('displayImageImport').getEl().dom.src="";
						$("#"+mapsLatLonImport.idRender).html("");
						$("#"+mapsLatLonImport.idRender).attr('map','true');
						var map2 = new XimaMap(mapsLatLonImport.idRender,'mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
						map2.map = new VEMap(mapsLatLonImport.idRender);
						map2.map.LoadMap(SpaceNeedle, 15);
						map2.map.HideDashboard();
						var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
						pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
							"<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
						map2.map.AddShape(pinS);
					}
				}
			});
		}

		// image recycle function
		var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>";
		function recycleImage( $item ) {
			$item.fadeOut(function() {
				$item
					.find( "a.icon-check" )
						.show()
					.end()
					.find( "a.icon-refresh" )
						.remove()
					.end()
					.css( "width", "96px")
					.css( "height", "120px")
					.children("div").append( trash_icon )
					.end()
					.find( "img" )
						.css( "height", "72px" )
					.end()
					.appendTo( $galleryImport )
					.fadeIn();
			});
		}

		// image preview function, demonstrating the ui.dialog used as a modal window
		function viewLargerImage( $link ) {
			var src = $link.attr( "href" ),
				title = $link.siblings( "img" ).attr( "alt" ),
				$modal = $( "img[src$='" + src + "']" );
			
			if ( $modal.length ) {
				$modal.dialog( "open" );
			} else {
				var img = $( "<img alt='Loading... Please Wait.' width='384' height='288' style='display: none; padding: 8px;' />" )
					.attr( "src", src ).appendTo( "body" );
				setTimeout(function() {
					img.dialog({
						title: "Show By RealtyTask",
						width: 400,
						modal: true
					});
				}, 1 );
			}
		}
		
		function changeImage($link,$item){
			if($("#"+mapsLatLonImport.idRender+"[map=true]").length){
				$("#"+mapsLatLonImport.idRender).html("");
				$("#"+mapsLatLonImport.idRender).removeClass("MSVE_MapContainer");
				$("#"+mapsLatLonImport.idRender).removeAttr("style");
				$("#"+mapsLatLonImport.idRender).removeAttr("map");
				$("#"+mapsLatLonImport.idRender).attr("style","position:relative;height:100%;");
				var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonImport.idRender );
				//$("#"+mapsLatLon.idRender).append("<img/>");
			}
			var src = $link.attr( "href" );
			$("#"+mapsLatLonImport.idRender+" img").attr("src",src);
			//Ext.getCmp('displayImageImport').getEl().dom.src=src;
			
			$('#galleryImport a[default=true]').show(200);
			$('#galleryImport a[default=true]').removeAttr('default');
			
			$item.find( "a.icon-check" ).attr('default', 'true');
			$item.find( "a.icon-check" ).hide(200);
			
		}
		
		$galleryImport.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-trash').length){
				deleteImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
			if($(e.target).closest('a.icon-check').length){
				changeImage($(e.target),$(e.target).closest('li'));
			}
		});
		
		$trashImport.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-refresh').length){
				recycleImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
		});

		function initGallery(){
				
			// let the galleryImport items be draggable
			$( "li", $galleryImport ).draggable({
				cancel: "a", // clicking an icon won't initiate dragging
				revert: "invalid", // when not dropped, the item will revert back to its initial position
				containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", // stick to demo-frame if present
				helper: "clone",
				cursor: "move"
			});
	
			// let the trash be droppable, accepting the galleryImport items
			$trashImport.droppable({
				accept: "#galleryImport > li",
				activeClass: "ui-state-highlight",
				drop: function( event, ui ) {
					deleteImage( ui.draggable );
				}
			});
	
			// let the galleryImport be droppable as well, accepting items from the trash
			$galleryImport.droppable({
				accept: "#trashImport li",
				activeClass: "custom-state-active",
				drop: function( event, ui ) {
				}
			});
			
			$("#galleryImport li").on("mouseenter",function(){
				$(this).find('div').stop(false,true).slideDown('fast');
			}).on("mouseleave",function(){
				$(this).find('div').slideUp('fast');
			});
		}

		if(typeof imagesFinal.temp != "undefined"){
			$.each(imagesFinal.temp, function(i, ite){
				var templete=$(" " 
					+"<li class='ui-widget-content ui-corner-tr'>"
					+	"<h5 class='ui-widget-header'>Image</h5>"
					+	"<img src='"+ imagesFinal.thumb[i] +"?" + tempCacheImagesImport + "'  alt='Loading, Please wait ..!' width='96' height='72' //>"
					+	"<div>"
					+	"<a href='" + ite +"?" + tempCacheImagesImport + "' title='View larger image' class='icon-zoom-in'></a>"
					+	"<a href='" + ite +"?" + tempCacheImagesImport + "' title='Set as Default' class='icon-check'></a>"
					+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
					+	"</div>"
					+"</li>"
				+ " ");
				
				$("#galleryImport").append(templete);
				initGallery();
			});
			
			var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonImport.idRender );
					
			if($("#galleryImport li:first").length)
				$("#galleryImport li:first a:eq(1)").trigger('click');
			else{
				//Ext.getCmp('displayImageImport').getEl().dom.src="";
				$("#"+mapsLatLonImport.idRender+" img").attr("src","");
			}
			
		}
		
		$('#file-Image-Import').on("change",function(){
			var filename	=	$('#file-Image-Import').val().replace(/C:\\fakepath\\/i, '');	//Extract the file name with her extention
			var filetype	=	filename.substr(filename.lastIndexOf('.') + 1);	//Extract the extention for the file
			var extentions	=	Array('jpg','png','jpeg','bmp');
			if(in_array(filetype.toLowerCase(),extentions)){
				if(window.FormData){
					var formData	=	new FormData();
					formData.append('file', $('#file-Image-Import')[0].files[0]);
					formData.append('id_function', "uploadImport");
					formData.append('count', totalImagesImport);
					$.ajax({
						url: "/properties_tabs/propertyImages/propertyImagesCore.php",
						type: "POST",
						dataType: 'json',
						data: formData,
						processData: false,
						contentType: false,
						success: function (res){
							if(res.result){
								totalImagesImport	=	res.indexx;
								var templete=$(" " 
									+"<li class='ui-widget-content ui-corner-tr'>"
									+	"<h5 class='ui-widget-header'>Image</h5>"
									+	"<img src='"+ res.newThumb +"?" + tempCacheImagesImport + "'  alt='Loading, Please wait ..!' width='96' height='72' //>"
									+	"<div>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesImport + "' title='View larger image' class='icon-zoom-in'></a>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesImport + "' title='Set as Default' class='icon-check'></a>"
									+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
									+	"</div>"
									+"</li>"
								+ " ");
								
								$("#galleryImport").append(templete);
								initGallery();
								CreateArrayImagesImport();
							}
						}
					});
				}else{
					console.debug("No se puede hacer un toche");
				}
			}else{
				Ext.MessageBox.alert('Warning','<span>Sorry you may only upload images that are in .bmp, .jpg, .jpeg and .png format.<br> Please try again</span>');
			}
		});
	});
	
	</script>
	
	<div class="demo ui-widget ui-helper-clearfix">

		<ul id="galleryImport" class="galleryImport ui-helper-reset ui-helper-clearfix"></ul>
	
		<div id="trashImport" class="ui-widget-content ui-state-default">
			<h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Trash</span> Drag the unwanted images here</h4><!--Drag the unwanted images here-->
		</div>
	</div>