	<style>
		#galleryEdit { float: left; width: 65%; /*min-height: 12em;*/ } * html #galleryEdit { /*height: 12em;*/ } /* IE6 */
		.galleryEdit.custom-state-active { background: #eee; }
		.galleryEdit li { float: left; width: 96px; height: 120px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
		.galleryEdit li h5 { margin: 0 0 0.4em; cursor: move; }
		.galleryEdit li a { float: right; }
		.galleryEdit li div { height:16px; width:100%; position: relative; display:none; }
		.galleryEdit li a.icon-zoom-in { float: left; }
		.galleryEdit li a.icon-check { left: 50%; margin-left: -6px; position: absolute; }
		.galleryEdit li a.icon-trash { float: right; }
		.galleryEdit li img { width: 100%; cursor: move; }

		#trashEdit { float: right; width: 32%; min-height: 18em; padding: 1%;} * html #trashEdit { height: 18em; } /* IE6 */
		#trashEdit h4 { line-height: 16px; margin: 0 0 0.4em; }
		#trashEdit h4 .ui-icon { float: left; }
		#trashEdit .galleryEdit h5 { display: none; }
	</style>
	
	<input type="file" id="file-Image-Edit"/>
	
	<div align="left" id="principal-Image-Edit" style="background-color:#FFF;border-color:#FFF">
		<br clear="all" />
		<div align="center" style=" background-color:#FFF; margin:auto; width:950px;">
	  		<div id="list-Images-Edit"></div>
		</div>
		<br clear="all" />
	</div>
	<script>
	/******************
	*	To Clear Always Images Cache
	******************/
	var aux=	new Date();
	var tempCacheImagesEdit	=	(aux.getHours()).toString() + (aux.getMinutes()).toString() + (aux.getSeconds()).toString();
	/*******END*******/
	
	var totalImagesEdit,defaultImageEdit;
	$("#file-Image-Edit").hide();
	
	function imageDefaultEdit(){
		if($( "a.icon-check[default=true]" ).length){
			var aux = $("#galleryEdit a.icon-check[default=true]").length == 0	?	""	:	$("#galleryEdit a.icon-check[default=true]").attr("href")	;
			if(aux != ""){
				aux = aux.split("/");
				return aux[(aux.length)-1];
			}else{
				return "";
			}
		}
	}
	
	function CreateArrayImagesEdit(){
		var imagesToRegister	=	new Object();
		var old					=	new Object();
		var news				=	new Object();
		
		$.each($("#galleryEdit img[news='0']"), function(i, ite){
			var aux = $(this).attr("src").split("thumb/");
			old[i]	=	aux[1];
		});
		
		$.each($("#galleryEdit img[news='1']"), function(i, ite){
			var aux = $(this).attr("src").split("thumb/");
			news[i]	=	aux[1];
		});
		imagesToRegister.old	=	old;
		imagesToRegister.news	=	news;
		return imagesToRegister;
	}
			
	var toolbarImageEdit	=	new	Ext.Toolbar({
		renderTo	:	'list-Images-Edit',
		items		:[
			new Ext.Button({
				tooltip: 'Upload New Image',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-plus"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					$("#file-Image-Edit").trigger('click');
				}
			}),
			new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'FollowupContact'} }
					});
					win.show();
				}
			})/*,
			new Ext.Button({
				tooltip: 'Test Temp By JEsus',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					var a = $("#galleryEdit a.icon-check[default=true]").length == 0	?	""	:	$("#galleryEdit a.icon-check[default=true]").attr("href")	;
					//Ext.getCmp('displayImageImport').autoEl.src='/propertiesImages/2482/120/2.jpg';	//Nunca lo Probe si Funcionaba
					//Ext.getCmp('displayImageImport').getEl().dom.src='/propertiesImages/2482/120/2.jpg';
				}
			})*/
		]
	});


	$(document).ready(function (){
		//Variable Receibed with namespace Ext from propertyResult.php
		var imagesFinalEdit		=	typeof imagesEdit.Import == "undefined"	?	Array() :	imagesEdit.Import;
		if(typeof imagesEdit.Import.image != "undefined")
			totalImagesEdit	=	imagesEdit.Import.image.length;
		else
			totalImagesEdit	=	0;
		
		var $galleryEdit = $( "#galleryEdit" ),
				$trashEdit = $( "#trashEdit" );
		// image deletion function  
		var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='icon-refresh'></a>";
		
		function deleteImage( $item ) {
			$item.fadeOut(function() {
				var $list = $( "ul", $trashEdit ).length ?
					$( "ul", $trashEdit ) :
					$( "<ul class='galleryEdit ui-helper-reset'/>" ).appendTo( $trashEdit );

				$item.find( "a.icon-trash" ).remove();
				$item.find( "a.icon-check" ).hide();
				$item.children("div").append( recycle_icon );
				$item.appendTo( $list ).fadeIn(function() {
					$item
						.animate({ width: "48px" ,height: "52px"})
						.find( "img" )
							.animate({ height: "36px" });
				});
				
				if($item.find( "a.icon-check[default=true]" ).length){
					if($("#galleryEdit li:first").length)
						$("#galleryEdit li:first a:eq(1)").trigger('click');
					else{
						//Ext.getCmp('displayImageImport').getEl().dom.src="";
						$("#"+mapsLatLonEdit.idRender).html("");
						$("#"+mapsLatLonEdit.idRender).attr('map','true');
						
						var	map1 = new XimaMap(mapsLatLonEdit.idRender,'mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
						map1.map = new VEMap(mapsLatLonEdit.idRender);
						map1.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
						map1.map.HideDashboard();
						var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
						pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
							"<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
						map1.map.AddShape(pin);
					}
				}
			});
		}

		// image recycle function
		var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>";
		function recycleImage( $item ) {
			$item.fadeOut(function() {
				$item
					.find( "a.icon-check" )
						.show()
					.end()
					.find( "a.icon-refresh" )
						.remove()
					.end()
					.css( "width", "96px")
					.css( "height", "120px")
					.children("div").append( trash_icon )
					.end()
					.find( "img" )
						.css( "height", "72px" )
					.end()
					.appendTo( $galleryEdit )
					.fadeIn();
			});
		}

		// image preview function, demonstrating the ui.dialog used as a modal window
		function viewLargerImage( $link ) {
			var src = $link.attr( "href" ),
				title = $link.siblings( "img" ).attr( "alt" ),
				$modal = $( "img[src$='" + src + "']" );

			if ( $modal.length ) {
				$modal.dialog( "open" );
			} else {
				var img = $( "<img alt='Loading... Please Wait.' width='384' height='288' style='display: none; padding: 8px;' />" )
					.attr( "src", src ).appendTo( "body" );

				setTimeout(function() {
					img.dialog({
						title: "Show By RealtyTask",
						width: 400,
						modal: true
					});
					console.debug($(img).parent());
					$(img).parent().css({
						top			:	"50%",
						marginTop	:	"-200px",
						position	:	"fixed"
					});
					console.debug($(img).parent());
				}, 1 );
			}

			//console.debug($("div[role=dialog]"));
		}
		
		function changeImageEdit($link,$item){
			if($("#"+mapsLatLonEdit.idRender+"[map=true]").length){
				$("#"+mapsLatLonEdit.idRender).html("");
				$("#"+mapsLatLonEdit.idRender).removeClass("MSVE_MapContainer");
				$("#"+mapsLatLonEdit.idRender).removeAttr("style");
				$("#"+mapsLatLonEdit.idRender).removeAttr("map");
				$("#"+mapsLatLonEdit.idRender).attr("style","position:relative;height:100%;");
				var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonEdit.idRender );
				//$("#"+mapsLatLonEdit.idRender).append("<img/>");
			}
			var src = $link.attr( "href" );
			$("#"+mapsLatLonEdit.idRender+" img").attr("src",src);
			//Ext.getCmp('displayImageImport').getEl().dom.src=src;
			
			$('#galleryEdit a[default=true]').show(200);
			$('#galleryEdit a[default=true]').removeAttr('default');
			
			$item.find( "a.icon-check" ).attr('default', 'true');
			$item.find( "a.icon-check" ).hide(200);
			
		}
		
		$galleryEdit.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-trash').length){
				deleteImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
			if($(e.target).closest('a.icon-check').length){
				changeImageEdit($(e.target),$(e.target).closest('li'));
			}
		});
		
		$trashEdit.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-refresh').length){
				recycleImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
		});

		function initGalleryEdit(){
				
			// let the galleryEdit items be draggable
			$( "li", $galleryEdit ).draggable({
				cancel: "a", // clicking an icon won't initiate dragging
				revert: "invalid", // when not dropped, the item will revert back to its initial position
				containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", // stick to demo-frame if present
				helper: "clone",
				cursor: "move"
			});
	
			// let the trash be droppable, accepting the galleryEdit items
			$trashEdit.droppable({
				accept: "#galleryEdit > li",
				activeClass: "ui-state-highlight",
				drop: function( event, ui ) {
					deleteImage( ui.draggable );
				}
			});
	
			// let the galleryEdit be droppable as well, accepting items from the trash
			$galleryEdit.droppable({
				accept: "#trashEdit li",
				activeClass: "custom-state-active",
				drop: function( event, ui ) {
				}
			});
			
			$("#galleryEdit li").on("mouseenter",function(){
				$(this).find('div').stop(false,true).slideDown('fast');
			}).on("mouseleave",function(){
				$(this).find('div').slideUp('fast');
			});
		}

		//console.debug(imagesFinalEdit);
		if(typeof imagesFinalEdit.image != "undefined"){
			$.each(imagesFinalEdit.image, function(i, ite){
				var templete=$(" " 
					+"<li class='ui-widget-content ui-corner-tr'>"
					+	"<h5 class='ui-widget-header'>Image</h5>"
					+	"<img src='"+ imagesFinalEdit.thumb[i] +"?" + tempCacheImagesEdit + "' news='0' alt='Loading, Please wait ..!' width='96' height='72' //>"
					+	"<div>"
					+	"<a href='" + ite +"?" + tempCacheImagesEdit + "' title='View larger image' class='icon-zoom-in'></a>"
					+	"<a href='" + ite +"?" + tempCacheImagesEdit + "' title='Set as Default' class='icon-check'></a>"
					+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
					+	"</div>"
					+"</li>"
				+ " ");
				
				$("#galleryEdit").append(templete);
				initGalleryEdit();
			});
			
			var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonEdit.idRender );
			
			if(typeof imagesEdit.Import.default != "undefined"){
				if(imagesEdit.Import.default == 0){
					if($("#galleryEdit li:first").length){
						$("#galleryEdit li:first a:eq(1)").trigger('click');
					}else{
						$("#"+mapsLatLonEdit.idRender+" img").attr("src","");
					}
				}else{
					$("#galleryEdit a[href$='"+imagesEdit.Import.default+"?" + tempCacheImagesEdit + "']:eq(1)").trigger('click');
				}
			}else{
				$("#"+mapsLatLonEdit.idRender+" img").attr("src","");
			}
		}
		
		$('#file-Image-Edit').on("change",function(){
			var filename	=	$('#file-Image-Edit').val().replace(/C:\\fakepath\\/i, '');	//Extract the file name with her extention
			var filetype	=	filename.substr(filename.lastIndexOf('.') + 1);	//Extract the extention for the file
			var extentions	=	Array('jpg','png','jpeg','bmp');
			if(in_array(filetype.toLowerCase(),extentions)){
				if(window.FormData){
					var formData	=	new FormData();
					formData.append('file', $('#file-Image-Edit')[0].files[0]);
					formData.append('id_function', "uploadEdit");
					formData.append('count', totalImagesEdit);
					$.ajax({
						url: "/properties_tabs/propertyImages/propertyImagesCore.php",
						type: "POST",
						dataType: 'json',
						data: formData,
						processData: false,
						contentType: false,
						success: function (res){
							if(res.result){
								totalImagesEdit	=	res.indexx;
								var templete=$(" " 
									+"<li class='ui-widget-content ui-corner-tr'>"
									+	"<h5 class='ui-widget-header'>Image</h5>"
									+	"<img src='"+ res.newThumb +"?" + tempCacheImagesEdit + "' news='1' alt='Loading, Please wait ..!' width='96' height='72' //>"
									+	"<div>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesEdit + "' title='View larger image' class='icon-zoom-in'></a>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesEdit + "' title='Set as Default' class='icon-check'></a>"
									+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
									+	"</div>"
									+"</li>"
								+ " ");
								
								$("#galleryEdit").append(templete);
								initGalleryEdit();
							}
						}
					});
				}else{
					console.debug("No se puede hacer");
				}
			}else{
				Ext.MessageBox.alert('Warning','<span>Sorry you may only upload images that are in .bmp, .jpg, .jpeg and .png format.<br> Please try again</span>');
			}
		});
	});
	
	</script>
	
	<div class="demo ui-widget ui-helper-clearfix">

		<ul id="galleryEdit" class="galleryEdit ui-helper-reset ui-helper-clearfix"></ul>
	
		<div id="trashEdit" class="ui-widget-content ui-state-default">
			<h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Trash</span> Drag the unwanted images here</h4><!--Drag the unwanted images here-->
		</div>
	</div>
	</div>