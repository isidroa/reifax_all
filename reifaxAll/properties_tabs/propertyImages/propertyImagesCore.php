<?php
	require('../../properties_conexion.php');
	require('../propertiesFunctions.php');
	conectar();

	$userid	=	$_COOKIE['datos_usr']['USERID'];
	$function = $_POST['id_function'];

	switch($function){
		case 	"uploadImport":
			require_once("class.resizeImage.php");
			$index	=	$_POST['count']+1;
			$type	=	str_replace("image/","",$_FILES["file"]["type"]);
			if(!@move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/temp/$index.$type"))
				die(json_encode(array("result" => false, "msg" => "No se Pudo Copiar el Archivo")));
				
			//Resize Original Image
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/temp/$index.$type");
			$image->resize(400,300);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/temp/$index.$type");
			//Create Thumb
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/temp/$index.$type");
			$image->resize(96,72);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/thumb/$index.$type");
			
			echo json_encode(
					array(
						"result"	=>	true, 
						"newTemp"	=>	"/propertiesImages/".$userid."/temp/".$index.".".$type, 
						"newThumb"	=>	"/propertiesImages/".$userid."/thumb/".$index.".".$type, 
						"indexx"	=>	$index
					)
				);
				
			break;
		case	"uploadEdit":
			require_once("class.resizeImage.php");
			$index	=	$_POST['count']+1;
			$type	=	str_replace("image/","",$_FILES["file"]["type"]);

			if(!@move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/edit/$index.$type"))
				die(json_encode(array("result" => false, "msg" => "No se Pudo Copiar el Archivo")));
				
			//Resize Original Image
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/edit/$index.$type");
			$image->resize(400,300);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/edit/$index.$type");
			//Create Thumb
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/edit/$index.$type");
			$image->resize(96,72);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/edithumb/$index.$type");
			
			echo json_encode(
					array(
						"result"	=>	true, 
						"newTemp"	=>	"/propertiesImages/".$userid."/edit/".$index.".".$type, 
						"newThumb"	=>	"/propertiesImages/".$userid."/edithumb/".$index.".".$type, 
						"indexx"	=>	$index
					)
				);
				
			break;
		case	"uploadAdd":
			require_once("class.resizeImage.php");
			$index	=	$_POST['count']+1;
			$type	=	str_replace("image/","",$_FILES["file"]["type"]);
			if(!is_dir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/"))
				mkdir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/");
			
			if(!is_dir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/"))
				mkdir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/");

			if(!is_dir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/addthumb/"))
				mkdir($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/addthumb/");

			if(!@move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/$index.$type"))
				die(json_encode(array("result" => false, "msg" => "No se Pudo Copiar el Archivo")));
				
			//Resize Original Image
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/$index.$type");
			$image->resize(400,300);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/$index.$type");
			//Create Thumb
			$image = new SimpleImage();
			$image->load($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/add/$index.$type");
			$image->resize(96,72);
			$image->save($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/addthumb/$index.$type");
			
			echo json_encode(
					array(
						"result"	=>	true, 
						"newTemp"	=>	"/propertiesImages/".$userid."/add/".$index.".".$type, 
						"newThumb"	=>	"/propertiesImages/".$userid."/addthumb/".$index.".".$type, 
						"indexx"	=>	$index
					)
				);
				
			break;
	}
?>