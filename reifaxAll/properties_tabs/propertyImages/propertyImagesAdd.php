	<style>
		#galleryAdd { float: left; width: 65%; /*min-height: 12em;*/ } * html #galleryAdd { /*height: 12em;*/ } /* IE6 */
		.galleryAdd.custom-state-active { background: #eee; }
		.galleryAdd li { float: left; width: 96px; height: 120px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
		.galleryAdd li h5 { margin: 0 0 0.4em; cursor: move; }
		.galleryAdd li a { float: right; }
		.galleryAdd li div { height:16px; width:100%; position: relative; display:none; }
		.galleryAdd li a.icon-zoom-in { float: left; }
		.galleryAdd li a.icon-check { left: 50%; margin-left: -6px; position: absolute; }
		.galleryAdd li a.icon-trash { float: right; }
		.galleryAdd li img { width: 100%; cursor: move; }

		#trashAdd { float: right; width: 32%; min-height: 18em; padding: 1%;} * html #trashAdd { height: 18em; } /* IE6 */
		#trashAdd h4 { line-height: 16px; margin: 0 0 0.4em; }
		#trashAdd h4 .ui-icon { float: left; }
		#trashAdd .galleryAdd h5 { display: none; }
	</style>
	
	<input type="file" id="file-Image-Add"/>
	
	<div align="left" id="principal-Image-Add" style="background-color:#FFF;border-color:#FFF">
		<br clear="all" />
		<div align="center" style=" background-color:#FFF; margin:auto; width:950px;">
	  		<div id="list-Images-Add"></div>
		</div>
		<br clear="all" />
	</div>
	<script>
	/******************
	*	To Clear Always Images Cache
	******************/
	var aux=	new Date();
	var tempCacheImagesAdd	=	(aux.getHours()).toString() + (aux.getMinutes()).toString() + (aux.getSeconds()).toString();
	/*******END*******/
	
	var totalImagesAdd,defaultImageAdd;
	$("#file-Image-Add").hide();
	
	function imageDefaultAdd(){
		if($( "a.icon-check[default=true]" ).length){
			var aux = $("#galleryAdd a.icon-check[default=true]").length == 0	?	""	:	$("#galleryAdd a.icon-check[default=true]").attr("href")	;
			if(aux != ""){
				aux = aux.split("/");
				return aux[(aux.length)-1];
			}else{
				return "";
			}
		}
	}
	
	function CreateArrayImagesAdd(){
		var imagesToRegister	=	new Object();
		var old					=	new Object();
		var news				=	new Object();
		
		$.each($("#galleryAdd img[news='0']"), function(i, ite){
			var aux = $(this).attr("src").split("thumb/");
			old[i]	=	aux[1];
		});
		
		$.each($("#galleryAdd img[news='1']"), function(i, ite){
			var aux = $(this).attr("src").split("thumb/");
			news[i]	=	aux[1];
		});
		imagesToRegister.old	=	old;
		imagesToRegister.news	=	news;
		return imagesToRegister;
	}
			
	var toolbarImageAdd	=	new	Ext.Toolbar({
		renderTo	:	'list-Images-Add',
		items		:[
			new Ext.Button({
				tooltip: 'Upload New Image',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-plus"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					$("#file-Image-Add").trigger('click');
				}
			}),
			new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'FollowupContact'} }
					});
					win.show();
				}
			})/*,
			new Ext.Button({
				tooltip: 'Test Temp By JEsus',
				cls:'x-btn-text',
				iconAlign: 'left',
				text: '<i class="icon-question-sign"></i>',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.realtytask.com/img/toolbar/videohelp.png',
				handler: function(){
					var a = $("#galleryAdd a.icon-check[default=true]").length == 0	?	""	:	$("#galleryAdd a.icon-check[default=true]").attr("href")	;
					//Ext.getCmp('displayImageImport').autoEl.src='/propertiesImages/2482/120/2.jpg';	//Nunca lo Probe si Funcionaba
					//Ext.getCmp('displayImageImport').getEl().dom.src='/propertiesImages/2482/120/2.jpg';
				}
			})*/
		]
	});


	$(document).ready(function (){
		//Variable Receibed with namespace Ext from propertyResult.php
		//var imagesFinalAdd		=	typeof imagesAdd.Import === "undefined"	?	Array() :	imagesAdd.Import;
		//if(typeof imagesAdd.Import.image != "undefined")
		//	totalImagesAdd	=	imagesAdd.Import.image.length;
		//else
			totalImagesAdd	=	0;
		
		var $galleryAdd = $( "#galleryAdd" ),
				$trashAdd = $( "#trashAdd" );
		// image deletion function  
		var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='icon-refresh'></a>";
		
		function deleteImage( $item ) {
			$item.fadeOut(function() {
				var $list = $( "ul", $trashAdd ).length ?
					$( "ul", $trashAdd ) :
					$( "<ul class='galleryAdd ui-helper-reset'/>" ).appendTo( $trashAdd );

				$item.find( "a.icon-trash" ).remove();
				$item.find( "a.icon-check" ).hide();
				$item.children("div").append( recycle_icon );
				$item.appendTo( $list ).fadeIn(function() {
					$item
						.animate({ width: "48px" ,height: "52px"})
						.find( "img" )
							.animate({ height: "36px" });
				});
				
				if($item.find( "a.icon-check[default=true]" ).length){
					if($("#galleryAdd li:first").length)
						$("#galleryAdd li:first a:eq(1)").trigger('click');
					else{
						//Ext.getCmp('displayImageImport').getEl().dom.src="";
						$("#"+mapsLatLonAdd.idRender).html("");
						$("#"+mapsLatLonAdd.idRender).attr('map','true');
						
						var	map1 = new XimaMap(mapsLatLonAdd.idRender,'mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
						map1.map = new VEMap(mapsLatLonAdd.idRender);
						map1.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
						map1.map.HideDashboard();
						var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
						pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
							"<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
						map1.map.AddShape(pin);
					}
				}
			});
		}

		// image recycle function
		var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>";
		function recycleImage( $item ) {
			$item.fadeOut(function() {
				$item
					.find( "a.icon-check" )
						.show()
					.end()
					.find( "a.icon-refresh" )
						.remove()
					.end()
					.css( "width", "96px")
					.css( "height", "120px")
					.children("div").append( trash_icon )
					.end()
					.find( "img" )
						.css( "height", "72px" )
					.end()
					.appendTo( $galleryAdd )
					.fadeIn();
			});
		}

		// image preview function, demonstrating the ui.dialog used as a modal window
		function viewLargerImage( $link ) {
			var src = $link.attr( "href" ),
				title = $link.siblings( "img" ).attr( "alt" ),
				$modal = $( "img[src$='" + src + "']" );

			if ( $modal.length ) {
				$modal.dialog( "open" );
			} else {
				var img = $( "<img alt='Loading... Please Wait.' width='384' height='288' style='display: none; padding: 8px;' />" )
					.attr( "src", src ).appendTo( "body" );

				setTimeout(function() {
					img.dialog({
						title: "Show By RealtyTask",
						width: 400,
						modal: true
					});
					console.debug($(img).parent());
					$(img).parent().css({
						top			:	"50%",
						marginTop	:	"-200px",
						position	:	"fixed"
					});
					console.debug($(img).parent());
				}, 1 );
			}

			//console.debug($("div[role=dialog]"));
		}
		
		function changeImageAdd($link,$item){
			if($("#"+mapsLatLonAdd.idRender+"[map=true]").length){
				$("#"+mapsLatLonAdd.idRender).html("");
				$("#"+mapsLatLonAdd.idRender).removeClass("MSVE_MapContainer");
				$("#"+mapsLatLonAdd.idRender).removeAttr("style");
				$("#"+mapsLatLonAdd.idRender).removeAttr("map");
				$("#"+mapsLatLonAdd.idRender).attr("style","position:relative;height:100%;");
				var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonAdd.idRender );
				//$("#"+mapsLatLonAdd.idRender).append("<img/>");
			}
			var src = $link.attr( "href" );
			$("#"+mapsLatLonAdd.idRender+" img").attr("src",src);
			//Ext.getCmp('displayImageImport').getEl().dom.src=src;
			
			$('#galleryAdd a[default=true]').show(200);
			$('#galleryAdd a[default=true]').removeAttr('default');
			
			$item.find( "a.icon-check" ).attr('default', 'true');
			$item.find( "a.icon-check" ).hide(200);
			
		}
		
		$galleryAdd.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-trash').length){
				deleteImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
			if($(e.target).closest('a.icon-check').length){
				changeImageAdd($(e.target),$(e.target).closest('li'));
			}
		});
		
		$trashAdd.on('click',function (e){
			e.preventDefault();
			if($(e.target).closest('a.icon-refresh').length){
				recycleImage($(e.target).closest('li'));
			}
			if($(e.target).closest('a.icon-zoom-in').length){
				viewLargerImage($(e.target));
			}
		});

		function initgalleryAdd(){
				
			// let the galleryAdd items be draggable
			$( "li", $galleryAdd ).draggable({
				cancel: "a", // clicking an icon won't initiate dragging
				revert: "invalid", // when not dropped, the item will revert back to its initial position
				containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", // stick to demo-frame if present
				helper: "clone",
				cursor: "move"
			});
	
			// let the trash be droppable, accepting the galleryAdd items
			$trashAdd.droppable({
				accept: "#galleryAdd > li",
				activeClass: "ui-state-highlight",
				drop: function( event, ui ) {
					deleteImage( ui.draggable );
				}
			});
	
			// let the galleryAdd be droppable as well, accepting items from the trash
			$galleryAdd.droppable({
				accept: "#trashAdd li",
				activeClass: "custom-state-active",
				drop: function( event, ui ) {
				}
			});
			
			$("#galleryAdd li").on("mouseenter",function(){
				$(this).find('div').stop(false,true).slideDown('fast');
			}).on("mouseleave",function(){
				$(this).find('div').slideUp('fast');
			});
		}

		//console.debug(imagesFinalAdd);
		/*if(typeof imagesFinalAdd.image != "undefined"){
			$.each(imagesFinalAdd.image, function(i, ite){
				var templete=$(" " 
					+"<li class='ui-widget-content ui-corner-tr'>"
					+	"<h5 class='ui-widget-header'>Image</h5>"
					+	"<img src='"+ imagesFinalAdd.thumb[i] +"?" + tempCacheImagesAdd + "' news='0' alt='Loading, Please wait ..!' width='96' height='72' //>"
					+	"<div>"
					+	"<a href='" + ite +"?" + tempCacheImagesAdd + "' title='View larger image' class='icon-zoom-in'></a>"
					+	"<a href='" + ite +"?" + tempCacheImagesAdd + "' title='Set as Default' class='icon-check'></a>"
					+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
					+	"</div>"
					+"</li>"
				+ " ");
				
				$("#galleryAdd").append(templete);
				initgalleryAdd();
			});
			
			var img = $( "<img/>" )
					.attr( "style", "height:100%;" ).appendTo( "#"+mapsLatLonAdd.idRender );
			
			if(typeof imagesAdd.Import.default != "undefined"){
				if(imagesAdd.Import.default == 0){
					if($("#galleryAdd li:first").length){
						$("#galleryAdd li:first a:eq(1)").trigger('click');
					}else{
						$("#"+mapsLatLonAdd.idRender+" img").attr("src","");
					}
				}else{
					$("#galleryAdd a[href$='"+imagesAdd.Import.default+"?" + tempCacheImagesAdd + "']:eq(1)").trigger('click');
				}
			}else{
				$("#"+mapsLatLonAdd.idRender+" img").attr("src","");
			}
		}*/
		
		$('#file-Image-Add').on("change",function(){
			var filename	=	$('#file-Image-Add').val().replace(/C:\\fakepath\\/i, '');	//Extract the file name with her extention
			var filetype	=	filename.substr(filename.lastIndexOf('.') + 1);	//Extract the extention for the file
			var extentions	=	Array('jpg','png','jpeg','bmp');
			if(in_array(filetype.toLowerCase(),extentions)){
				if(window.FormData){
					var formData	=	new FormData();
					formData.append('file', $('#file-Image-Add')[0].files[0]);
					formData.append('id_function', "uploadAdd");
					formData.append('count', totalImagesAdd);
					$.ajax({
						url: "/properties_tabs/propertyImages/propertyImagesCore.php",
						type: "POST",
						dataType: 'json',
						data: formData,
						processData: false,
						contentType: false,
						success: function (res){
							if(res.result){
								totalImagesAdd	=	res.indexx;
								var templete=$(" " 
									+"<li class='ui-widget-content ui-corner-tr'>"
									+	"<h5 class='ui-widget-header'>Image</h5>"
									+	"<img src='"+ res.newThumb +"?" + tempCacheImagesAdd + "' news='1' alt='Loading, Please wait ..!' width='96' height='72' //>"
									+	"<div>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesAdd + "' title='View larger image' class='icon-zoom-in'></a>"
									+	"<a href='" + res.newTemp +"?" + tempCacheImagesAdd + "' title='Set as Default' class='icon-check'></a>"
									+	"<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='icon-trash'></a>"
									+	"</div>"
									+"</li>"
								+ " ");
								
								$("#galleryAdd").append(templete);
								initgalleryAdd();
							}
						}
					});
				}else{
					console.debug("No se puede hacer");
				}
			}else{
				Ext.MessageBox.alert('Warning','<span>Sorry you may only upload images that are in .bmp, .jpg, .jpeg and .png format.<br> Please try again</span>');
			}
		});
	});
	
	</script>
	
	<div class="demo ui-widget ui-helper-clearfix">

		<ul id="galleryAdd" class="galleryAdd ui-helper-reset ui-helper-clearfix"></ul>
	
		<div id="trashAdd" class="ui-widget-content ui-state-default">
			<h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Trash</span> Drag the unwanted images here</h4><!--Drag the unwanted images here-->
		</div>
	</div>
	</div>