<?php 	
	if(@session_start() == false){session_destroy();session_start();}
	include('../../properties_conexion.php');
	conectar();
	//print_r($_SESSION);
	$userid=$_COOKIE['datos_usr']['USERID'];

	$state_search = $_POST['state_search'];
	$county_search = $_POST['county_search'];
	
	if($_GET['id_followup']){
		$sql='SELECT * FROM realtytask.followup where idfollowup='.$_GET['id_followup'].' limit 1';
		$res=mysql_query($sql);
		$data=mysql_fetch_assoc($res);
		$property=$data['parcelid'];
	}
	else{
		$property=$_GET['parcelid'];
	}
	
	$sql='SELECT idlisting from listing where idfollowup='.$_GET['id_followup'].' LIMIT 1';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$idlisting=mysql_fetch_assoc($res);
	
	
	$sql='SELECT * FROM properties p join state s on p.state=s.initial WHERE parcelid='.$property.' LIMIT 1';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$data=mysql_fetch_assoc($res);

?>
<style type="text/css">
.container-overview{
	margin:0 auto;
	margin-top:10px;
	width:948px;
}
.overview-Property{   
}
.overview-Property td{
}
.overview-Property h1 {
    color: #000000;
    font-size: 2.3em;
}
.overview-Property p {
    font-size: 1.3em;
}
.overview-Property h1 span{
	color:#777;
	font-weight:100;
	font-size:0.4em;
}

.overview-Property ul li:first-child{
	padding-right: 5px;
    width: 70px;
}
.clear{
	clear:both;
}
</style>

<div align="left">
	<div >
        <!--<div id="mapSearchAdv" style="display:none; width:99%;height:320;border: medium solid #b8dae3;position:relative; margin-bottom:5px;"></div>-->
		<div id="listing-s_counterO_divButtonsPT" style=" float:left; margin:5px;">
		</div>
        <div class="border-overview-Property"  style=" float:left">
			<table class="overview-Property">
                <tr>
                    <td> <div class="body-list">
                        <h1><?php echo $data['address'].(($data['unit'])?' <span>Unit '.$data['unit'].'</span>':'') ?> <span class="segunda-linea"><?php echo $data['city'].', '.$data['name'].' '.$data['zip'] ?></span></h1>        </div></td><td></td></tr></table>
		</div>
         <div class="clear"></div>
		<div id="listing-s_OffertBE_advSearchd" style="padding-top:2px;"></div>
		<div id="view_Coffer<?php echo $_GET['id_followup'] ?>"></div>
	</div>
</div>
<script language="javascript">	

	Ext.ns("resultET");
	countListingDocsAdd[<?php echo $_GET['id_followup']; ?>]={
		contract	:	0,
		addendum	:	0,
		other		:	0
	};
	var reloadListingsDocsCoffer	=	0;
	function setFormFieldTooltip(component) {
		if (component.getXType() == 'textfield') {
	        var label = Ext.get('x-form-el-' + component.id).child('input');
	        Ext.QuickTips.register({
				target		:	label,
				text		:	component.tooltipText,
				dismissDelay:	15000,
				title		:	''
	        });
		}
	};
	
	var dataBoolean	=	new Ext.data.SimpleStore({
		fields: ['id','desc'],
		data  : [['Y','Yes'],['N','No']]
	});

	var propStatus	=	new Ext.data.SimpleStore({
		fields	:	['id','desc'],
		data	:	[
			['AA','FOR SALE'],
			['BB','FOR RENT'],
			['CC','NON-ACTIVE'],
			['CS','SOLD']
		]
	});

	function selectBetween(combo,record,index)
	{
		var arfield=combo.getId().replace("cb","tx");
		arfield=arfield.split('*');
		var txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3];
		//alert(arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3]+"-->"+txfield);
		
		if(record.get('id')=='Between')
		{
			if(Ext.getCmp(txfield))Ext.getCmp(txfield).setValue('');
			if(Ext.getCmp(txfield) && !Ext.getCmp(txfield).isVisible())
			{
				Ext.getCmp(txfield).setVisible(true);
				arrinpbetween.push(txfield);				
			}
		}
		else
		{
			if(Ext.getCmp(txfield) && Ext.getCmp(txfield).isVisible())
				Ext.getCmp(txfield).setVisible(false);
		}
	}
	
	function hideBetween(where)
	{
		var arfield;
		var txfield;
		var elementos = document.forms[1].elements.length;
		for(i=0; i<elementos; i++)
		{
			if(findstring(document.forms[1].elements[i].id) && Ext.getCmp(document.forms[1].elements[i].id).isVisible())
			{
				if(where=='ALL')
					Ext.getCmp(document.forms[1].elements[i].id).setVisible(false);
				else
				{
					arfield=document.forms[1].elements[i].id.split('*');
					txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+where;
					//alert(txfield)
					if(Ext.getCmp(txfield) )Ext.getCmp(txfield).setVisible(false);
				}
			}
		}
	}

	function findstring(cadena)
	{
		pat = /other/
		if(pat.test(cadena))
			return true
		return false
	}	

	function expandcollapse(section)
	{
		Ext.getCmp('PR').collapse();
		Ext.getCmp('FS').collapse();
		Ext.getCmp('FO').collapse();
		Ext.getCmp('MO').collapse();
		Ext.getCmp(section).expand();
	}
	
	function selectcounty(combo,record,index)
	{
		if(record.data.havemortgage=='N'){Ext.getCmp('MO').disable();Ext.getCmp('MO').setVisible(false);}
		if(record.data.havemortgage=='Y'){Ext.getCmp('MO').enable();Ext.getCmp('MO').setVisible(true);}
		
		mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
		search_county=Ext.getCmp('ncounty').getValue();
	}
	
	var proptype=new Ext.data.SimpleStore({
		fields: ['valuec', 'textc'],
		data : [
			['01','Single Family'],['04','Condo/Town/Villa'],['03','Multi Family +10'],['08','Multi Family -10'],
			['11','Commercial'],['00','Vacant Land'],['02','Mobile Home'],['99','Other']
		]
	})
	
	var toolbarmyoffers=new Ext.Toolbar({
		renderTo: 'listing-s_counterO_divButtonsPT',
		items: [{
			tooltip: 'View Property',
			cls:'x-btn-text',
			iconAlign: 'left',
			width: 36,
			height: 36,
			scale: 'large',
			iconCls	:'icon-contracts-view',
			handler		:	function(){
				loading_win.show();
				Ext.Ajax.request({
					waitMsg	:	'Checking...',
					url		:	'properties_tabs/propertyList/propertyCore.php', 
					method	:	'POST',
					timeout	:	600000,
					params	:{ 
						type		:	'showSingle',
						propertyIds	:	<?php echo $property ?>
					},
					failure:	function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var optionals = Ext.decode(response.responseText);
						if(optionals.success){
							resultET.result	=	optionals.data;
							if(document.getElementById('view-pt-ofer')){
								var tabs = Ext.getCmp('tabsFollowId');
								var tab = tabs.getItem('view-pt-ofer');
								tabs.remove(tab);
							}
								
							Ext.getCmp('tabsFollowId').add({
								title:		'View Property', 
								closable:	true,
								autoScroll:	true,
								id:			'view-pt-ofer', 
								autoLoad:	{url:'/properties_tabs/overview/viewPropertyS.php',scripts:true}
							}).show();														
						}else{
							Ext.Msg.show({
								title	:	"Warning", 
								buttons	:	Ext.Msg.OK,
								minWidth:	100,
								msg		:	optionals.msg,
								icon	:	Ext.MessageBox.ERROR
							});

						}							
					}
				});
			}
		},{
			tooltip: 'Submit Offer',
			cls:'x-btn-text',
			iconAlign: 'left',
			width: 36,
			height: 36,
			scale: 'large',
			iconCls	:'icon-offerconfirm',
			handler		:	function(){
					Ext.getCmp('counOff_formBrokersF').getForm().submit({
						url:'/propertyoffer/funciones.php',
						params:{
							action	: 'counter-confirm',
							offerprice	:	Ext.getCmp('counOff_listing-n_followupoffer_offerprice').getValue(),
							idfollowup: <?php echo $_GET['id_followup'] ?>
						},
						success: function (a,b){
							var listing=Ext.util.JSON.decode(b.response.responseText);
							console.debug(listing);
							
							var forms=Ext.getCmp('view_Coffer<?php echo $_GET['id_followup'] ?>-pt').findByType('form'); 
							loading_win.show();
							$.each(forms,function (k){
								console.debug(k);
								console.debug(this);
								var form=this.getForm();
								if(form.submit){
									data=new Object();
									dataCheck=this.findByType('checkbox');
									console.debug(dataCheck);
									$.each(dataCheck,function(){
										if(!this.getValue()){
												console.debug(this.getName());
												data[this.getName()]='0';
										}
									})
									data['idlisting']=listing.listing;
									if(1+parseInt(k)==forms.length){
										form.submit({
											url	:'/properties_tabs/edit/saveData.php',
											params	:data,
											success	:function (){
												loading_win.hide();
											}
										});
									}
									else{
										form.submit({
											url	:'/properties_tabs/edit/saveData.php',
											params	:data
										});
									}
								}
								else{
									console.info('fail save: '+forms[k].id);
								}
							});
						}
			});
			}
		}]
	});
	
	var formularios=SysRt.templates.buying({
		subj		:'counOff_',
		linkListing	:false,
		edit		:true,
		titltePanel	:false,
		compacto	:true,
		id_followup	:	<?php echo $_GET['id_followup'];?>,
		listing		:	<?php echo $idlisting['idlisting'];?>,
		formDocs	:	true,
		docsConfig	:	{
			edit			:	true,
			externalLink	:	false,
			externalModify	:	false,
			externalModifyId:	'tabsFollowSellingId',
			docsTitle		:	"Contract Forms",
			usedOnlyFor		:	'contract',
			textOr			:	true,
			comboDocuments	:	true,
			buttonDelete	:	true,
			buttonModify	:	true,
			buttonAddNew	:	true,
			buttonDownAll	:	false,
			buttonDownEach	:	false,
			buttonView		:	true,
			buttonUpload	:	false
			/*edit				:	false,	//Old Counter Offer
			docsTitle			:	"Offer Documents",
			requiredDocsTitle	:	"Offer Required Documents",
			requiredDocs		:	true,
			requiredType		:	'onlyShow',
			buttonCopyAll		:	false,
			buttonCopyEach		:	false,
			externalLink		:	true,
			externalModify		:	false,
			buttonDelete		:	false,
			buttonModify		:	false,
			buttonAddNew		:	false,
			textOr				:	false,
			comboDocuments		:	false,
			buttonDownAll		:	false,
			buttonDownEach		:	false,
			buttonView			:	true*/
		},
		form		:{
			buyerFull	:	true,
			espas		:	true,
			assing		:	true,
			homw		:	true,
			repai		:	true,
			appar		:	true,
			closing		:	true,
			perpor		:	true,
			emd			:	true,
			title		: 	true,
			finan		:	true,
			survey		:	true,
			inspe		:	true,
			addter		:	true,
			offerace	:	true,
			other		:	true
		}
	});
							
	/*var formulariosOther=SysRt.templates.buying({	//Old Counter Offer work With TempTable
		subj		:	'ListingCounterOffer',
		linkListing	:	false,
		id_followup	:	<?php //echo $_GET['id_followup'];?>/*,
		edit		:	true,
		titltePanel	:	false,
		listing		:	<?php //echo $idlisting['idlisting'];?>/*,
		forms		:	{
		},
		formDocs	:	true,
		docsConfig	:	{
			edit			:	true,
			requiredDocs	:	false,
			buttonCopyAll	:	false,
			buttonCopyEach	:	false,
			externalLink	:	false,
			externalModify	:	true,
			buttonDelete	:	true,
			buttonModify	:	true,
			buttonAddNew	:	true,
			textOr			:	true,
			comboDocuments	:	true,
			buttonDownAll	:	false,
			buttonDownEach	:	false,
			buttonView		:	true,
			workTemporary	:	true
		}
	});*/
	formCounterOfferAddendum=SysRt.templates.buying({
		subj		:	'counterOfferAddendum',
		linkListing	:	false,
		id_followup	:	<?php echo $_GET['id_followup'];?>,
		edit		:	false,
		titltePanel	:	false,
		listing		:	<?php echo $idlisting['idlisting'];?>,
		forms		:	{},
		formDocs	:	true,
		docsConfig	:	{
			edit			:	true,
			externalLink	:	false,
			externalModify	:	false,
			externalModifyId:	'tabsFollowSellingId',
			docsTitle		:	"Addendum Forms",
			usedOnlyFor		:	'addendum',
			textOr			:	true,
			comboDocuments	:	true,
			buttonDelete	:	true,
			buttonModify	:	true,
			buttonAddNew	:	true,
			buttonDownAll	:	false,
			buttonDownEach	:	false,
			buttonView		:	true,
			buttonUpload	:	false
		}
	});
	formCounterOfferAdditional=SysRt.templates.buying({
		subj		:	'counterOfferAditional',
		linkListing	:	false,
		id_followup	:	<?php echo $_GET['id_followup'];?>,
		edit		:	false,
		titltePanel	:	false,
		listing		:	<?php echo $idlisting['idlisting'];?>,
		forms		:	{},
		formDocs	:	true,
		docsConfig	:	{
			edit			:	true,
			externalLink	:	false,
			externalModify	:	false,
			docsTitle		:	"Additional Documents",
			usedOnlyFor		:	'other',
			textOr			:	true,
			comboDocuments	:	true,
			buttonDelete	:	true,
			buttonModify	:	true,
			buttonAddNew	:	true,
			buttonDownAll	:	false,
			buttonDownEach	:	false,
			buttonView		:	true,
			buttonUpload	:	false
		}
	});
	var listing_listing_panelsIMP = new Ext.TabPanel({
		id				:	'view_Coffer<?php echo $_GET['id_followup'] ?>-pt',
		renderTo		:	'view_Coffer<?php echo $_GET['id_followup'] ?>',
		deferredRender	:	 false,
   		activeTab		:	0,
		autoWidth		:	true,
		height			:	system_height - ($("#view_Coffer<?php echo $_GET['id_followup'] ?>").offset().top - grid_height),
		plain			:	true,
		enableTabScroll	:	true,
		defaults		:	{  
			autoScroll	:	true
		},
		items:[
			{
				title	:	'offer information',
				items	:	formularios.formBrokersF,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formBrokersF',<?php echo $idlisting['idlisting'] ?>,'counOff_',<?php echo $property?> );
			   		}
				}
			},
			{
				title	:	'Personal Property',
				items	:	formularios.formulcontentPerPro,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentPerPro',<?php echo $idlisting['idlisting'] ?>,'counOff_');
			   		}
				}
			},
			{
				title	:	'EMD',
				items	:	formularios.formulcontentEMD,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentEMD',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Financing',
				items	:	formularios.formulcontentFinaci,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentFinaci',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Closing',
				items	:	formularios.formulcontentCLOSING,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentCLOSING',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Appraisal',
				items	:	formularios.formulcontentAPPARIA,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentAPPARIA',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Inspections',
				items	:	formularios.formulPsummaryINSPE,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulPsummaryINSPE',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Title',
				items	:	formularios.formulcontentTITLE,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentTITLE',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Survey',
				items	:	formularios.formulcontentSURVEY,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentSURVEY',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Other',
				items	:	formularios.formulcontentOTHER,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formulcontentOTHER',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			},
			{
				title	:	'Additional Terms',
				items	:	formularios.formAddTerm,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formAddTerm',<?php echo $idlisting['idlisting'] ?>);
					}
				}
			},
			{
				title	:	'Offer and Acceptance',
				items	:	formularios.formOfferAccep,
				listeners:{
				   afterrender: function(){
						SysRt.templates.initData('counOff_formOfferAccep',<?php echo $idlisting['idlisting'] ?>);
					}
				}
			},
			{
				title	:	'Docs',
				items	:	[
					//formularios.formulcontentDOCS,
					formCounterOfferAddendum.formulcontentDOCS
					//formCounterOfferAdditional.formulcontentDOCS
					],
				listeners:{
				   afterrender: function(){
						//SysRt.templates.initData('formulcontentDOCS',<?php echo $idlisting['idlisting'] ?>);
			   		}
				}
			}
		],
		listeners:{
			afterrender: function(tab){
			  tab.setActiveTab(0);
			  tab.doLayout();
			},
			tabchange:	function(tabPanel,panel){
				if(panel.title	==	"Docs"){
					if(reloadListingsDocsCoffer == 0){
						tabChangeDocs(formularios.opt);
						tabChangeDocs(formCounterOfferAddendum.opt);
						tabChangeDocs(formCounterOfferAdditional.opt);
						reloadListingsDocsCoffer	=	1;
					}
				}
			}
		}
	});
</script>