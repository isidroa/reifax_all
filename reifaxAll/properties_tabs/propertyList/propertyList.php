<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 8px 1px;
}
</style>
<div align="left" id="principal-list-pt" style="background-color:#FFF;border-color:#FFF">
	<div style=" background-color:#FFF; margin:0px 0px 0px 0px;">
  		<div id="list-filters-pt"></div>
        <div id="list-pt" style="margin-top:5px;"></div> 
	</div>
</div>
<script>
	Ext.ns("resultET");
	var limitproperties = 50;
	var selectedGridPropertyList = new Array();
	var AllCheckPropertyList = false;
	
	var filterfield='agent';
	var filterdirection='ASC';
	
	var filterPropeprtyListStatus 	=	'';
	var filterPropeprtyListYrBuilt 	=	'';
	var filterPropeprtyListBaths 	=	'';
	var filterPropeprtyListCounty 	=	'';
	var filterPropeprtyListAddress 	=	'';
	var filterPropeprtyListBeds 	=	'';
	var filterPropeprtyListType 	=	'';
	
	var storeProperty = new Ext.data.JsonStore({
        url: 'properties_tabs/propertyList/propertyCore.php',
		fields: [
           	{name: 'parcelid', type: 'int'},
           	{name: 'addressfull'},
			//{name: 'status'},
			{name: 'county'},
			{name: 'CCodeD'},
			{name: 'lsqft'},
			{name: 'bheated'},
			{name: 'beds'},
			{name: 'bath', type: 'int'},
			{name: 'yrbuilt'}
        ],
		root			:	'records',
		totalProperty	:	'total',
		baseParams: {
			'userid'	:	<?php echo $userid;?>
		},
		remoteSort		:	true,
		sortInfo		:	{
			field		:	'parcelid',
			direction	:	'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners		:	{
			'beforeload': function(store,obj){
				//storePropertyAll.load();
				AllCheckPropertyList=false;
				selectedGridPropertyList=new Array();
				smProperties.deselectRange(0,limitproperties);
				//obj.params.status =	filterPropeprtyListStatus;
				obj.params.types	=	filterPropeprtyListType;
				obj.params.address	=	filterPropeprtyListAddress;
				obj.params.county	=	filterPropeprtyListCounty;
				obj.params.beds		=	filterPropeprtyListBeds;
				obj.params.baths	=	filterPropeprtyListBaths;
				obj.params.yrbuilt	=	filterPropeprtyListYrBuilt;
			},
			'load' : function (store,data,obj){
				if (AllCheckPropertyList){
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckPropertyList=true;
					gridmyfollowagent.getSelectionModel().selectAll();
					selectedGridPropertyList=new Array();
				}else{
					AllCheckPropertyList=false;
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selectedGridPropertyList.length > 0){
						for(val in selectedGridPropertyList){
							var ind = gridmyfollowagent.getStore().find('agentid',selectedGridPropertyList[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowagent.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storePropertyAll = new Ext.data.JsonStore({
        url			:	'properties_tabs/propertyList/propertyCore.php',
		fields		:	[
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root		:	'records',
		totalProperty:	'total',
		baseParams	:	{
			'userid'	:	<?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				//obj.params.status	=	filterPropeprtyListStatus;
				obj.params.types	=	filterPropeprtyListType;
				obj.params.address	=	filterPropeprtyListAddress;
				obj.params.county	=	filterPropeprtyListCounty;
				obj.params.beds		=	filterPropeprtyListBeds;
				obj.params.baths	=	filterPropeprtyListBaths;
				obj.params.yrbuilt	=	filterPropeprtyListYrBuilt;
			}
		}
    });
	var smProperties = new Ext.grid.CheckboxSelectionModel({
		checkOnly	:	true, 
		width		:	25,
		listeners	:	{
			"rowselect": function(selectionModel,index,record){
				if(selectedGridPropertyList.indexOf(record.get('parcelid'))==-1)
					selectedGridPropertyList.push(record.get('parcelid'));
					
				if(Ext.fly(gridmyfollowagent.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckPropertyList=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selectedGridPropertyList = selectedGridPropertyList.remove(record.get('parcelid'));
				AllCheckPropertyList=false;
				Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarFilterList	=	new	Ext.Toolbar({
		renderTo	:	'list-filters-pt',
		items		:[
			new	Ext.Button({
				tooltip: 'Delete',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-delete',
				handler		:	function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Properties to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg	:	'Checking...',
						url		:	'properties_tabs/propertyList/propertyCore.php', 
						method	:	'POST',
						timeout	:	600000,
						params	:{ 
							type	:	'delete',
							propertyIds:	propertyIds
						},
						failure:	function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var optionals = Ext.decode(response.responseText);
							if(optionals.success){
								storeProperty.load({params:{start:0, limit:limitproperties}});
								Ext.Msg.show({
									title	:	"Property Result", 
									buttons	:	Ext.Msg.OK,
									minWidth:	150,
									msg		:	optionals.msg,
									icon	:	Ext.MessageBox.INFO
								});
							}else{
								Ext.Msg.show({
									title	:	"Warning", 
									buttons	:	Ext.Msg.OK,
									minWidth:	100,
									msg		:	optionals.msg,
									icon	:	Ext.MessageBox.ERROR
								});
							}
						},
						error:function(){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','Sorry. Please, Try again.');
						}
					});
				}
			}),new Ext.Button({
				tooltip: 'Print',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-print',
				handler: function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to print.'); return false;
					}
					/*if(AllCheckPropertyList==true){
						selectedGridPropertyList=new Array();
						var totales = storePropertyAll.getRange(0,storePropertyAll.getCount());
						
						for(i=0;i<storePropertyAll.getCount();i++){
							if(selectedGridPropertyList.indexOf(totales[i].data.parcelid)==-1)
								selectedGridPropertyList.push(totales[i].data.parcelid);
						}
					
					}*/
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i];
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 7,
							sort: filterfield,
							dir: filterdirection,
							propertyIds: propertyIds
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.realtytask.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export to Excel',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls	:'icon-export',
				handler: function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to export.'); return false;
					}
					/*if(AllCheckPropertyList==true){
						selectedGridPropertyList=new Array();
						var totales = storePropertyAll.getRange(0,storePropertyAll.getCount());
						
						for(i=0;i<storePropertyAll.getCount();i++){
							if(selectedGridPropertyList.indexOf(totales[i].data.parcelid)==-1)
								selectedGridPropertyList.push(totales[i].data.parcelid);
						}
					
					}*/
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i];
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 7,
							sort: filterfield,
							dir: filterdirection,
							propertyIds: propertyIds
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.realtytask.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Add Filter',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls	:'icon-filter-plus',
				handler: function(){
					//filterPropeprtyListStatus 	=	'';
					filterPropeprtyListYrBuilt 	=	'';
					filterPropeprtyListBaths 	=	'';
					filterPropeprtyListCounty 	=	'';
					filterPropeprtyListAddress 	=	'';
					filterPropeprtyListBeds 	=	'';
					filterPropeprtyListType 	=	'';
					
					var filterProppertiesListPT = new Ext.FormPanel({
						url:'properties_tabs/propertyList/propertyCore.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						//collapsible: true,
						collapsed: false,
						title: 'Filters',
						//renderTo: 'list-filters-pt',
						id: 'filterProppertiesListPT',
						name: 'filterProppertiesListPT',
						items:[{
							xtype: 'fieldset',
							//title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							items: [/*{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype			:	'combo',
									mode			:	'local',
									fieldLabel		:	'Status',
									triggerAction	:	'all',
									width			:	150,
									store			:	propertiesStatus,
									displayField	:	'desc',
									valueField		:	'id',
									name			:	'ffilterPropeprtyListStatus',
									//value			:	'ALL',
									hiddenName		:	'filterPropeprtyListStatus',
									//hiddenValue		:	'ALL',
									emptyText		:	'SELECT',
									//allowBlank	:	false,
									listeners		:	{
										beforequery	:	function(qe){
											//delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListStatus=newvalue;
										}
									}
								}]
							},*/{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'filterPropeprtyListAddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListAddress	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftypeagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Beds',
									name		  : 'filterPropeprtyListBeds',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListBeds=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fweb',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'County',
									name		  : 'filterPropeprtyListCounty',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListCounty	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fphone',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Baths',
									name		  : 'filterPropeprtyListBaths',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListBaths	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Year Built',
									name		  : 'filterPropeprtyListYrBuilt',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListYrBuilt	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcompany',
								items	: [{
									xtype			:	'combo',
									mode			:	'local',
									fieldLabel		:	'Type',
									triggerAction	:	'all',
									width			:	150,
									store			:	propertiesType,	//Are in globalFunctions.js
									displayField	:	'desc',
									valueField		:	'id',
									name			:	'ffilterPropeprtyListType',
									//value         :	'00',
									hiddenName		:	'filterPropeprtyListType',
									emptyText		:	'SELECT',
									//hiddenValue   :	'00',
									allowBlank		: 	true,
									listeners		: 	{
										beforequery	:	function(qe){
											//delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyListType=newvalue;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.realtytask.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storeProperty.load({params:{start:0, limit:limitproperties}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.realtytask.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									//filterPropeprtyListStatus 	=	'';
									filterPropeprtyListYrBuilt 	=	'';
									filterPropeprtyListBaths 	=	'';
									filterPropeprtyListCounty 	=	'';
									filterPropeprtyListAddress 	=	'';
									filterPropeprtyListBeds 	=	'';
									filterPropeprtyListType 	=	'';
									Ext.getCmp('filterProppertiesListPT').getForm().reset();
									
									storeProperty.load({params:{start:0, limit:limitproperties}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 285,
								modal	 	: true,  
								plain       : true,
								items		: filterProppertiesListPT,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filter',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-filter-negative',
				handler: function(){
					//filterPropeprtyListStatus 	=	'';
					filterPropeprtyListYrBuilt 	=	'';
					filterPropeprtyListBaths 	=	'';
					filterPropeprtyListCounty 	=	'';
					filterPropeprtyListAddress 	=	'';
					filterPropeprtyListBeds 	=	'';
					filterPropeprtyListType 	=	'';
					
					storeProperty.load({params:{start:0, limit:limitproperties}});
				}
			}),/*new Ext.Button({
				tooltip: 'Send Contract',
				cls:'x-btn-text',
				//iconAlign: 'left',
				//text: '<i class="icon-ban-circle"></i>',
				text: '  ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.realtytask.com/img/toolbar/guarantee.png',
				handler: function(){
					if(selectedGridPropertyList.length	==	0){
						Ext.Msg.alert('Warning', 'You must previously select(check) one Property.'); return false;
					}
					if(selectedGridPropertyList.length	>	1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one Property.'); return false;
					}
					
					//Verificar que haya aceptado el disclaimer
					Ext.Ajax.request({
						url		:	'overview_contract/verifications.php',
						method	:	'POST',
						params	:	{	
							module	:	'acceptcontract',
							action	:	'verificate'	
						},
						waitMsg :	'Wait...',
						success :	function(r) {
							var resp	=	Ext.decode(r.responseText)
							if(resp.msg	==	'true'){
								Ext.Ajax.request({
									url		:	'overview_contract/verifications.php',
									method  :	'POST',
									params	:	{
										module	:	'mailsettings',
										action	:	''	
									},
									waitMsg :	'Getting Info',
									success :	function(r) {

										var tempSelections	=	smProperties.getSelections();
										var parcelid		=	tempSelections[0].data.parcelid;

										var resp	=	Ext.decode(r.responseText)
										if(resp.mail	==	'true'){
											Ext.Ajax.request({
												url     : 'properties_tabs/propertyContract/propertyGetInfo.php?pid='+parcelid,
												method  : 'POST',
												waitMsg : 'Getting Info',
												success : function(r) {

													var resp	=	r.responseText;
													if(document.getElementById('generate_contractPT')){
														var tab = tabs.getItem('generate_contractPT');
														tabs.remove(tab);
													}

													//////////CREO EL TAB PARA LOS CONTRATOS//////////
													Ext.getCmp('tabs-pt').add({
														//xtype: 'component', 
														title: 'Contract Manager', 
														id:'generate_contractPT',
														closable 	: true,
														autoScroll	: false,
														autoLoad:	{
															url		:	'properties_tabs/propertyContract/propertyContract.php', 
															scripts	:	true,
															params:{
																data:	resp
															},
															timeout	:	10800
														}
													}).show();						
												}
											});

										}else{
											Ext.Msg.alert('', 'To be able to send contracts you must first set your email within Reifax.<br> Please go to -> '+resp.settings+' and input your email settings.'); 
										}
									}
								});
							}else{
								disclaimer.show();
							}
						}
					});
					/*if(AllCheckPropertyList==true){
						selectedGridPropertyList=new Array();
						var totales = storePropertyAll.getRange(0,storePropertyAll.getCount());
						
						for(i=0;i<storePropertyAll.getCount();i++){
							if(selectedGridPropertyList.indexOf(totales[i].data.agentid)==-1)
								selectedGridPropertyList.push(totales[i].data.agentid);
						}
					
					}
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i];*/
				/*}
			}),*/{
				xtype: 'tbspacer', 
				width: 30
			},new Ext.Button({
				tooltip: 'Edit/View',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls	:'icon-view',
				handler		:	function(){
					var sel_properties	=	smProperties.getSelections();
					if(sel_properties.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Property to be edited.'); return false;
					}else if(sel_properties.length > 1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one Property to be edited.'); return false;
					}
					loading_win.show();
					var property	=	sel_properties[0];
					Ext.Ajax.request({
						waitMsg	:	'Checking...',
						url		:	'properties_tabs/propertyList/propertyCore.php', 
						method	:	'POST',
						timeout	:	600000,
						params	:{ 
							type		:	'showEdit',
							propertyIds	:	property.get('parcelid')
						},
						failure:	function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var optionals = Ext.decode(response.responseText);
							if(optionals.success){
								resultET.result	=	optionals.data;
								if(document.getElementById('edit-pt')){
									var tabs = Ext.getCmp('tabs-pt');
									var tab = tabs.getItem('edit-pt');
									tabs.remove(tab);
								}
									
								Ext.getCmp('tabs-pt').add({
									title:		'Edit', 
									closable:	true,
									autoScroll:	true,
									id:			'edit-pt', 
									autoLoad:	{url:'/properties_tabs/propertyEdit/propertyEdit.php',scripts:true}
								}).show();														
							}else{
								Ext.Msg.show({
									title	:	"Warning", 
									buttons	:	Ext.Msg.OK,
									minWidth:	100,
									msg		:	optionals.msg,
									icon	:	Ext.MessageBox.ERROR
								});

							}							
						}
					});
				}
			}),new	Ext.Button({
				tooltip: 'Follow-up',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-follow-up',
				handler		:	function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Properties to be follow.'); return false;
					}
					
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i]; 

					Ext.MessageBox.show({
						title:    'Follow Up',
						msg:      'Do you Want To add the properties to?',
						buttons: {yes: 'Listing', no: 'Offers',cancel: 'Cancel'},
						fn: function(btn) {
							var typeFollow='B';
							if(btn=='cancel'){
								return false;
							}else if(btn=='yes'){
								typeFollow='S';
							}else if(btn=='no'){
								typeFollow='B';
							}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Follow Up...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
								method: 'POST', 
								timeout :600000,   
								params: {
									type: 'multi-insert',
									pids: propertyIds,
									userid	:	<?php echo $userid;?>,
									typeFollow: typeFollow
								},
								
								failure:function(response,options){
									Ext.MessageBox.alert('Warning','ERROR');
									loading_win.hide();
								},
								success:function(response,options){
									loading_win.hide();
									var optionals = Ext.decode(response.responseText);
									if(optionals.success){
										storeProperty.load({params:{start:0, limit:limitproperties}});
										Ext.Msg.show({
											title	:	"Follow Up", 
											buttons	:	Ext.Msg.OK,
											minWidth:	150,
											msg		:	optionals.msg,
											icon	:	Ext.MessageBox.INFO
										});
									}else{
										Ext.Msg.show({
											title	:	"Warning", 
											buttons	:	Ext.Msg.OK,
											minWidth:	100,
											msg		:	optionals.msg,
											icon	:	Ext.MessageBox.ERROR
										});
									}
								}                                
							});
						}
					});
				}
			}),new	Ext.Button({
				tooltip: 'Add to Mailing Campaign',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-sendmail-campaign',
				handler		:	function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Properties to be mailing.'); return false;
					}
					
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i]; 

					var simple = new Ext.FormPanel({
						frame:true,
						title: 'Mailing Campaign',
						width: 400,
						waitMsgTarget : 'Wait...',
						items: [{
									xtype     : 'textfield',
									name      : 'campaign',
									id		  : 'campaign',
									allowBlank: false,
									fieldLabel: 'Name Campaign',
									value     : '',
									width: 200
								}],
						buttons: [{
								text: 'Accept',
								handler: function(){
									if(simple.getForm().isValid()){
										loading_win.show();
										Ext.Ajax.request({  
											waitMsg: 'Mailing Campaings...',
											url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
											method: 'POST', 
											timeout :600000,
											params: {
												type:'verify',
												pids: propertyIds,
												userid	:	<?php echo $userid;?>,
												campaign: Ext.getCmp('campaign').getValue()
											},
											
											failure:function(response,options){
												Ext.MessageBox.alert('Warning','ERROR');
												loading_win.hide();
											},
											success:function(response,options){
												var respuesta = Ext.util.JSON.decode(response.responseText);
												if(respuesta.exist==2){
													loading_win.hide();
													Ext.MessageBox.alert('Warning','Campaign name already exists');
												}else if(respuesta.exist==1){
													loading_win.hide();
													Ext.MessageBox.confirm('Mailing', 'Some properties already in another campaign, change?', saveCampaign);
												}else{
													saveCampaign('yes');
												}
											}
										});
										function saveCampaign(btn){
											if(btn=='no'){
												win.close();
												return;
											}else{
												loading_win.show();
												Ext.Ajax.request({  
													waitMsg: 'Mailing Campaings...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
													method: 'POST', 
													timeout :600000,
													params: {
														type: 'multi-insert',
														pids: propertyIds,
														userid	:	<?php echo $userid;?>,
														campaign: Ext.getCmp('campaign').getValue()
													},
													
													failure:function(response,options){
														Ext.MessageBox.alert('Warning','ERROR');
														loading_win.hide();
														win.close();
													},
													success:function(response,options){
														loading_win.hide();
														win.close();
														var optionals = Ext.decode(response.responseText);
														if(optionals.success){
															storeProperty.load({params:{start:0, limit:limitproperties}});
															Ext.Msg.show({
																title	:	"Mailing Campaign", 
																buttons	:	Ext.Msg.OK,
																minWidth:	150,
																msg		:	optionals.msg,
																icon	:	Ext.MessageBox.INFO
															});
														}else{
															Ext.Msg.show({
																title	:	"Warning", 
																buttons	:	Ext.Msg.OK,
																minWidth:	100,
																msg		:	optionals.msg,
																icon	:	Ext.MessageBox.ERROR
															});
														}
													}                                
												 });
											}
										}
									}
								}
							},{
								text: 'Cancel',
								handler  : function(){
										win.close();
									}
							}]
						});
						var win = new Ext.Window({
							layout      : 'fit',
							width       : 400,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'hide',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									win.close();
								}
							}]
						});
						win.show();
				}
			}),new Ext.Button({
				tooltip: 'Block Property/Agent',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-block',
				handler: function(){
					if(selectedGridPropertyList.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to block.'); return false;
					}
					if(AllCheckPropertyList==true){
						selectedGridPropertyList=new Array();
						var totales = storePropertyAll.getRange(0,storePropertyAll.getCount());
						
						for(i=0;i<storePropertyAll.getCount();i++){
							if(selectedGridPropertyList.indexOf(totales[i].data.agentid)==-1)
								selectedGridPropertyList.push(totales[i].data.agentid);
						}
					
					}
					var propertyIds=selectedGridPropertyList[0];
					for(i=1; i<selectedGridPropertyList.length; i++)
						propertyIds+=','+selectedGridPropertyList[i];
					
					if(selectedGridPropertyList.length>1)
						var msgBlock	=	"Block properties?";
					else
						var msgBlock	=	"Block property?";
					
					Ext.MessageBox.show({
					    title:    'Properties',
					    msg:      msgBlock,
					    buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
					    fn: function(btn){
								if(btn=='cancel' || btn=='no'){
									return;
								}
								loading_win.show();
					
								Ext.Ajax.request( 
								{  
									waitMsg: 'Checking...',
									url: 'properties_tabs/propertyList/propertyCore.php', 
									method: 'POST',
									timeout :600000,
									params: { 
										type: 'block',
										propertyIds: propertyIds,
										blockproperties: btn
									},
									
									failure:function(response,options){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','ERROR');
									},
									
									success:function(response,options){
										var resp   = Ext.decode(response.responseText);
										loading_win.hide();
										
										Ext.Msg.alert("Properties", selectedGridPropertyList.length+' Properties added to Block.<br>'+resp.properties+' Properties blocked');
										storeProperty.load({params:{start:0, limit:limitproperties}});
										if(typeof storePropertyBlock !== "undefined")
											storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
										/*if(storemyblockagent){
											storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
										}*/ 
									}                                
								});
							}
					});
					
				}
			}),new Ext.Button({
				tooltip: 'Property Analisys',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-analisys',
				hidden: !addon5,
				handler		:	function(){
					var sel_properties	=	smProperties.getSelections();
					if(sel_properties.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Property to view comparables.'); return false;
					}else if(sel_properties.length > 1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one Property to view comparables.'); return false;
					}
					loading_win.show();
					var property	=	sel_properties[0];
					Ext.Ajax.request({
						waitMsg	:	'Checking...',
						url		:	'analisys_tabs/get_comparables.php', 
						method	:	'POST',
						timeout	:	600000,
						params	:{ 
							type		:	'get-comparables',
							propertyIds	:	property.get('parcelid'),
							propertyAddress: property.get('addressfull'),
							getNew: 'no'
						},
						failure:	function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var optionals = Ext.decode(response.responseText);
							if(optionals.msg==''){
								if(document.getElementById('comparables-ptP')){
									var tabs = Ext.getCmp('tabs-pt');
									var tab = tabs.getItem('comparables-ptP');
									tabs.remove(tab);
								}
								Ext.getCmp('tabs-pt').add({
									title:		'Property Analisys', 
									closable:	true,
									id:			'comparables-ptP', 
									autoLoad:	{
										url:'analisys_tabs/tabs_comparables.php',
										scripts:true,
										params:{
											parcelid: property.get('parcelid'),
											address: property.get('addressfull'),
											tabRender: 'P'
										}
									}
								}).show();
							}else{
								Ext.MessageBox.alert('',optionals.msg);
							}
						}
					});
				}
			}),{
				xtype: 'tbspacer', 
				width: 60
			},new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-help',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'FollowupContact'} }
					});
					win.show();
				}
			})	
		]
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var num = colIndex-3;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = 'H';
		else if(tyvalue == 1){ 
			classvalue = 'O';
		}else if(tyvalue == 2){ 
			classvalue = 'C';
		}else if(tyvalue == 3){ 
			classvalue = 'HF';
		}else if(tyvalue == 4){ 
			classvalue = 'TF';
		}else if(tyvalue == 5){ 
			classvalue = 'OTF';
		}else if(tyvalue == 6){ 
			classvalue = 'OF';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function emailRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-1;
		var type = 'typeemail'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function webRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-9;
		var type = 'typeurl'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function addressRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-12;
		var type = 'typeaddress'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(H)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}	
	var gridmyfollowagent = new Ext.grid.GridPanel({
		renderTo	:	'list-pt',
		cls			:	'grid_comparables',
		width		:	system_width_panel,
		height		:	system_height - ($("#list-pt").offset().top - grid_height),
		store		:	storeProperty,
		stripeRows	:	true,
		sm			:	smProperties, 
		columns		:	[
			smProperties,
			//{header: 'Status', width: 110, sortable: true, tooltip: 'Property Status', dataIndex: 'status'/*, renderer: emailRender*/},
			//{header: 'Block', width: 40, sortable: true, tooltip: 'Property Blocked', dataIndex: 'ccoded', renderer: emailRender},
			{header: 'Address', width: 320, sortable: true, tooltip: 'Property Address,', dataIndex: 'addressfull'},
			{header: 'County', width: 100, sortable: true, tooltip: 'Property county,', dataIndex: 'county'},
			{header: 'Type', width: 110, sortable: true, tooltip: 'Property Type,', dataIndex: 'CCodeD'},
			{header: 'G. Area', width: 60, sortable: true, tooltip: 'Property Gross Area', dataIndex: 'lsqft', renderer: emailRender},
			{header: 'L. Area', width: 60, sortable: true, tooltip: 'Property Living Area', dataIndex: 'bheated', renderer: emailRender},
			{header: 'Beds', width: 50, sortable: true, tooltip: 'Property Bedthrooms', dataIndex: 'beds'/*, renderer: phoneRender*/},
			{header: 'Baths', width: 50, sortable: true, tooltip: 'Property Bathrooms', dataIndex: 'bath'/*, renderer: phoneRender*/},
			{header: 'Built', width: 55, sortable: true, tooltip: 'Property Year Built', dataIndex: 'yrbuilt', renderer: phoneRender}
		],
		listeners	:	{
			'sortchange':	function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowdblclick' : function (grid, rowIndex, columnIndex){
				var record = grid.getStore().getAt(rowIndex);
				var parcelid=record.get('parcelid');
				properties_overview(parcelid, null,'tabs-pt')
			}
		},
		tbar	:	new Ext.PagingToolbar({
			id			:	'pagingPropertiesListPT',
            pageSize	:	limitproperties,
            store		:	storeProperty,
            displayInfo	:	true,
			displayMsg	:	'{2} Properties',
			emptyMsg	:	"No Propperties to display",
			items		:	[
				'Show:',
				new Ext.Button({
					tooltip		:	'Click to show 50 Properties per page.',
					text: '&nbsp;50&nbsp;',
					handler		:	function(){
						limitproperties=50;
						Ext.getCmp('pagingPropertiesListPT').pageSize = limitproperties;
						Ext.getCmp('pagingPropertiesListPT').doLoad(0);
					},
					enableToggle:	true,
					pressed		:	true,
					toggleGroup	:	'show_res_group'
				}),
				'-',
				new Ext.Button({
					tooltip		:	'Click to show 80 Properties per page.',
					text: '&nbsp;80&nbsp;',
					handler		:	function(){
						limitproperties=80;
						Ext.getCmp('pagingPropertiesListPT').pageSize = limitproperties;
						Ext.getCmp('pagingPropertiesListPT').doLoad(0);
					},
					enableToggle:	true,
					toggleGroup	:	'show_res_group'
				}),
				'-',
				new Ext.Button({
					tooltip		:	'Click to show 100 Properties per page.',
					text: '&nbsp;100&nbsp;',
					handler		:	function(){
						limitproperties=100;
						Ext.getCmp('pagingPropertiesListPT').pageSize = limitproperties;
						Ext.getCmp('pagingPropertiesListPT').doLoad(0);
					},
					enableToggle:	true,
					toggleGroup	:	'show_res_group'
				})
			]
        })
	});
	
	storeProperty.load({params:{start:0, limit:limitproperties}});

	if(document.getElementById('tabs')){
		if(document.getElementById('principal-list-pt').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('principal-list-pt').offsetHeight+100);
			//tabs2.setHeight(tabs.getHeight());
			//tabs3.setHeight(tabs.getHeight());
			//viewport.setHeight(tabs.getHeight());
		}
	}
</script>