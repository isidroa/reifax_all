<?php
include ('../../properties_conexion.php');
conectar();

$report=$_GET['report']?true:false;
if($report){
	echo '<!DOCTYPE HTML>
	<html>
	<head>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	</head>
	<body>';
}
?>
<style type="text/css">
.container-overview{
	margin:0 auto;
	margin-top:10px;
	width:948px;
}
.overview-Property{   
	padding: 6px;
	border:solid 4px #F1F1F1;
	background:#FFF;
	width:100%;
	height:auto;
}
.overview-Property td{
	width:50%;
}
.overview-Property ul{
	list-style:none;
	padding:0;
	margin: 4px 0 0 4px;
}
.overview-Property h1 {
    color: #000000;
    font-size: 2em;
}
.overview-Property h1 span{
	color:#777;
}

.overview-Property ul li:first-child{
	padding-right: 5px;
    width: 70px;
}
.overview-Property .container-buttons{
	text-align:center;
	height:40px;
	margin-top:10px;
}
.overview-Property .container-buttons a{
	background:#FFF;
	padding:12px 8px 12px 36px;
	border: solid 1px #B5B8C8;
	border-radius:3px;
	color: #1D6AAA;
	font-weight:bold;
	font-size:13px;
	font-family:arial,tahoma,verdana,helvetica;
	text-decoration:none;
}
.overview-Property .container-buttons a:hover{
-moz-box-shadow: 0px 0px 3px #ccc;
-webkit-box-shadow: 0px 0px 3px #ccc;
box-shadow: 0px 0px 3px #ccc;
	
}
.overview-Property .container-buttons .close{
	background: no-repeat 11px 10px url("/img/cancel.png");

}.tab-bar{
	margin-top:10px;
}
.tab-bar .tbas ul{
	list-style:none;
	padding:0;
}
.tab-bar .tbas ul li{
	float:left;
	margin-left:3px;
}
.tab-bar .tbas ul li a{
	display:block;
	color:#298AD1;
	font-size:15px;
	padding:4px 15px;
	background-color: #EEEEEE;
	border-radius: 6px 6px 0 0;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif;
	
}
.tab-bar .tbas ul li .active{
	color:#FFF;
	font-weight:bold;
	background: -moz-linear-gradient(center top , #2293BD 25%, #005C83) repeat scroll 0 0 transparent;
	
}
.tab-bar .divider{
	clear:both;
	background:#EAEAEA;
	width:100%;
	height:2px;
	border:1px solid #D0D0D0;
}

.tab-bar .contariner-tabs{
	height:auto;	
	background:#F1F1F1;
	width:100%;
	border:1px solid #D0D0D0;
}
.tab-bar .container-body{    
	margin: 5px;
    padding: 8px;
	background:#FFF;
}

.tab-bar .container-body .body-list, .overview-Property .body-list{
	margin-top:4px;
	border:1px solid #D0D0D0;
	padding: 2px 4px;
	font-family:Tahoma, Geneva, sans-serif;
	font-size:12px;
	position:relative;
}
.tab-bar .container-body .body-list{
	margin-bottom:15px;
	display:none;
}
.tab-bar .container-body .active{
	display:block;
}
.tab-bar .container-body .body-list td{
	padding:3px;
	padding-top:5px;
}
.tab-bar .container-body .body-list .td-content{
	width:200px;
}
.tab-bar .container-body .body-list .td-title{
	width:170px;
}
.tab-bar .container-body .body-list .td-content{
	width:200px;
}
.tab-bar .container-body .body-list .title-panel, .overview-Property .body-list .title-panel{
	position:absolute;
	background:#FFF;
	width:auto;
	height:auto;
	top:-10px;
	padding:1px 6px;
    color: #15428B;
	font-family:tahoma,arial,helvetica,sans-serif;
	font-size:11px;
	font-weight:bold;
}
.clear{
	clear:both;
}
.border-overview-Property{
	border: 1px solid #D0D0D0;
}
</style>
<?php
if($_GET['id_followup']){
	$sql='SELECT * FROM realtytask.followup where idfollowup='.$_GET['id_followup'].' limit 1';
	$res=mysql_query($sql);
	$data=mysql_fetch_assoc($res);
	$property=$data['parcelid'];
}
else{
	$property=$_GET['parcelid'];
}

$sql='SELECT * FROM properties p join state s on p.state=s.initial WHERE parcelid='.$property.' LIMIT 1';
$res=mysql_query($sql) or die ($sql.mysql_error());
$data=mysql_fetch_assoc($res);

$sql='SELECT parcelid FROM psummary WHERE parcelid='.$property.' LIMIT 1';
$queryPsumary=mysql_query($sql) or die ($sql.mysql_error());
$psummary=mysql_num_rows($queryPsumary);

$sql='SELECT parcelid FROM mlsresidential WHERE parcelid='.$property.' LIMIT 1';
$res=mysql_query($sql) or die ($sql.mysql_error());
$mlsresidential=mysql_num_rows($res);

$sql='SELECT parcelid FROM pendes WHERE parcelid='.$property.' LIMIT 1';
$res=mysql_query($sql) or die ($sql.mysql_error());
$pendes=mysql_num_rows($res);

$sql='SELECT parcelid FROM rental WHERE parcelid='.$property.' LIMIT 1';
$res=mysql_query($sql) or die ($sql.mysql_error());
$rental=mysql_num_rows($res);

$sql='SELECT parcelid FROM mortgage WHERE parcelid='.$property.' LIMIT 1';
$res=mysql_query($sql) or die ($sql.mysql_error());
$mortgage=mysql_num_rows($res);

$status=array('A'=>'FOR SALE',
			'BB'=>'FOR RENT',
			'CC'=>'NON-ACTIVE',
			'CS'=>'SOLD');


$descript=array('address'	=> 'Address',
				'unit'		=> 'Unit',
				'city' 		=> 'City',
				'zip'		=> 'Zip',
				'county'	=> 'County',
				'name'		=> 'State',
				'CCodeD'	=> 'Type',
				);
			
echo '
<div class="container-overview">
<div class="border-overview-Property">
<table class="overview-Property">
<tr>
<td> <div class="body-list">
	<h1>'.$data['address'].(($data['unit'])?' <span>Unit '.$data['unit'].'</span>':' ').'</h1>
	<P>'.$data['city'].', '.$data['name'].' '.$data['zip'].'</P>
	<P>'.$data['county'].', '.$data['CCodeD'].'</P>
	<P>'.$status[$data['status']].'</P>
	<div class="clear"></div>
	<div class="container-buttons">
		<a href="#close" class="close">Close</a>
	</div>
	<div class="clear"></div>
	</div>
</td>
<td>
</td>
</tr>
</table>
</div>
<div class="tab-bar" id="tab-bar">
	<div class="tbas">
		<ul><li><a href="#tab-PRDEF,#tab-PRALL" class="active">Public Records</a></li>
		';
			if($mlsresidential)
				echo '<li><a href="#tab-LISDEF">For Sale</a></li>';
			if($rental)
				echo '<li><a href="#tab-LISDEF">For Rent</a></li>';
			if($pendes)
				echo '<li><a href="#tab-FCALL">Foreclosure</a></li>';
			if($mortgage)
				echo '<li><a href="#tab-MORALL">Mortgage</a></li>';
		echo ' 
		</ul>
	</div>
	<div class="divider">
	</div>
	<div class="contariner-tabs">
		<div class="container-body">';
			$sql1="SELECT * FROM `searchtemplateadv` s
				WHERE s.`idGrid` in ('PRDEF','LISDEF','FCALL','MORALL','PRALL')
			ORDER BY s.orden";
			$res1=mysql_query($sql1) or die($sql1.mysql_error());
			$imain=0;
			$cadresult='';
			while($rowmain=mysql_fetch_array($res1))
			{
				
				$campos='';
				$idcts=$rowmain['campos'];
				$id=$rowmain['idGrid'];
				$textotitulo=$rowmain['texto'];
				$sql2	=	"
					SELECT c.`idtc`, c.`Tabla`, c.`Campos`, c.`titulos`,  c.`Titulos_a`, c.`Decimals`, c.`type`, c.`Align`, c.`size`, c.`Desc`, c.`r_size`, c.`px_size`
					FROM camptit c
					WHERE  c.`idtc` IN ($idcts)
					ORDER BY  c.`Tsaorden`";
				$res2=mysql_query($sql2) or die($sql2.mysql_error());
				unset($arrdta);
				while($row=mysql_fetch_array($res2)){
					$campos.=$campos==''? $row['Campos']: ','.$row['Campos'];
					$arrdta[]=$row;
				}
				$canttotal=count($arrdta);
				$cant=($canttotal/2);
				$sql= 'SELECT '.$campos.' FROM '.$arrdta[0]['Tabla'].' WHERE parcelid='.$property.' LIMIT 1';
				$query=mysql_query($sql) or die ($sql.mysql_error());
				$data=mysql_fetch_array($query);
					echo '<div id="tab-'.$id.'" class="body-list">';
					if($id=='PRALL')
						echo '<div class="title-panel">Owner information</div>';
					echo '
				<table><tr VALIGN=TOP><td><table>';
				for($i=0; $i<$cant;$i++){
					if($arrdta[$i]['type']=='real' || $arrdta[$i]['type']=='int')
						$data[$i]=number_format($data[$i],2);
					if( $arrdta[$i]['type']=='int')
						$data[$i]=number_format($data[$i]);
					if( $arrdta[$i]['type']=='date'  && $data[$i]!=''){
						$aux=substr($data[$i],-4,-2).'/';
						$aux.=substr($data[$i],-2).'/';
						$aux.=substr($data[$i],-8,-4);
						$data[$i]=$aux;
					}
					echo '<tr><td class="td-title">
						'.$arrdta[$i]['Titulos_a'].':
					</td><td class="td-content">
						'.$data[$i].'
					</td></tr>';
				}
				echo '</table></td><td><table>';
				for($i=($canttotal % 2==0)?$cant:$cant+1; $i<$cant*2;$i++){
					if($arrdta[$i]['type']=='real')
						$data[$i]=number_format($data[$i],2);
					if( $arrdta[$i]['type']=='int')
						$data[$i]=number_format($data[$i]);
					if( $arrdta[$i]['type']=='date' && $data[$i]!=''){
						$aux=substr($data[$i],-4,-2).'/';
						$aux.=substr($data[$i],-2).'/';
						$aux.=substr($data[$i],-8,-4);
						$data[$i]=$aux;
					}
					echo '<tr><td class="td-title">
						'.$arrdta[$i]['Titulos_a'].':
					</td><td class="td-content">
						'.$data[$i].'
					</td></tr>';
				}
				echo '</table></td></tr></table></div>';
				
			}
	echo '
		<div>
	</div>
</div>
</div>
';
if($report){
	echo '</body></html>';
}
?>


<script>
	$(document).ready(function (){
		var tab=$("#tab-bar");
		$(".tbas a", tab).bind('click',function (e){
			e.preventDefault();
			$('.body-list', tab).hide();
			$(".tbas a", tab).removeClass('active');
			$(this).addClass('active');	
			$($(this).attr('href'), tab).show();	
		});
		$($(".tbas .active", tab).attr('href'), tab).show();
		
		$('.overview-Property .container-buttons .close').bind('click',function (){
			console.debug(Ext.getCmp('<?php echo $_GET['tab']?>'));
			console.debug(Ext.getCmp('<?php echo $_GET['id']?>'));
			Ext.getCmp('<?php echo $_GET['tab']?>').remove(Ext.getCmp('<?php echo $_GET['id']?>'));
		});
		$('.container-overview').css('width',parseInt(system_width)-20);
	});
	if(loading_win)
		loading_win.hide()
</script>