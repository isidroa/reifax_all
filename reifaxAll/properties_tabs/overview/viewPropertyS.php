<?php 	
	include('../../properties_conexion.php');
	if(@session_start() == false){session_destroy();session_start();}
	conectar();
	//print_r($_SESSION);
	$userid=$_COOKIE['datos_usr']['USERID'];

	$state_search = $_POST['state_search'];
	$county_search = $_POST['county_search'];

?>
<div align="left" style="height:100%">
	<div id="body_central2" style="height:100%">
		<!--<div id="editButtonsPT" style="display:none;position: absolute; right: 30px; z-index: 2; top: 40px;">-->
		<div id="editButtonsPT" style="display:none; position: absolute; left: 180px; z-index: 2; top: 145px;">
			<table cellspacing="20">
				<tr>
					<td id="bt-edit-list"></td>
					<td id="bt-edit-cancel-list"></td>
				</tr>
			</table>
		</div>
		<div id="advView" style="padding-top:2px;"></div>
		<div id="panelsView"></div>
	</div>
</div>
<script language="javascript">
	
	var reloadMapViewPt = 0;
	Ext.ns("imagesView");
	Ext.ns("mapsLatLonView");
		
	function selectBetween(combo,record,index)
	{
		var arfield=combo.getId().replace("cb","tx");
		arfield=arfield.split('*');
		var txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3];
		
		if(record.get('id')=='Between')
		{
			if(Ext.getCmp(txfield))Ext.getCmp(txfield).setValue('');
			if(Ext.getCmp(txfield) && !Ext.getCmp(txfield).isVisible())
			{
				Ext.getCmp(txfield).setVisible(true);
				arrinpbetween.push(txfield);				
			}
		}
		else
		{
			if(Ext.getCmp(txfield) && Ext.getCmp(txfield).isVisible())
				Ext.getCmp(txfield).setVisible(false);
		}
	}
	
	var dataBoolean	=	new Ext.data.SimpleStore({
		fields: ['id','desc'],
		data  : [['Y','Yes'],['N','No']]
	});
	
	function removeSaleView(id){
		Ext.getCmp("tableRowSalesView"+id).hide();
		Ext.getCmp("edsales*price"+id).setValue(0);
		Ext.getCmp("edsales*priceSqft"+id).setValue(0);
		Ext.getCmp("edsales*date"+id).setValue("");
		Ext.getCmp("edsales*book"+id).setValue("");
		Ext.getCmp("edsales*page"+id).setValue("");
	}
	
	var countSalesHistoryPTED=0;
	function addSaleHistoryPTED(){
		var atte = {
			id			:	'tableRowSalesView'+countSalesHistoryPTED,
			layout		:	'table',
			defaults	:	{	bodyStyle:'padding:5px'},
			layoutConfig:	{	columns: 6},
			items		:	[
				{	
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'datefield',
						id			:	'edsales*date'+countSalesHistoryPTED,
						fieldLabel	:	'Date',
								readOnly	:true,
								name		:	'edsales*date'+countSalesHistoryPTED,
						format		:	'm/d/Y',
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Date'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'edsales*price'+countSalesHistoryPTED,
						fieldLabel	:	'Price',
								readOnly	:true,
								name		:	'edsales*price'+countSalesHistoryPTED,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Price'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	80,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'edsales*priceSqft'+countSalesHistoryPTED,
						fieldLabel	:	'Price Sqft',
								readOnly	:true,
								name		:	'edsales*priceSqft'+countSalesHistoryPTED,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale $/Sqft'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'edsales*book'+countSalesHistoryPTED,
						fieldLabel	:	'Book',
								readOnly	:true,
								name		:	'edsales*book'+countSalesHistoryPTED,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Book'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'edsales*page'+countSalesHistoryPTED,
						fieldLabel	:	'Page',
								readOnly	:true,
								name		:	'edsales*page'+countSalesHistoryPTED,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Page'});
							}
						}
					},{
						xtype	:	'hidden',
						id		:	'edsales*idsales'+countSalesHistoryPTED,
								readOnly	:true,
								name		:	'edsales*idsales'+countSalesHistoryPTED
					}]
				},{
					xtype	:	'button',
					id		:	'edsales*button'+countSalesHistoryPTED,
					cls		:	'x-btn-text',
					idSales	:	countSalesHistoryPTED,
					tooltip	:	'Delete this row',
					text	:	'<i class="icon-minus"></i>',
					listeners	:	{
						click	:	function(button){
							removeSaleView(button.idSales);
						}
					}
				}
			]
		}
		countSalesHistoryPTED=countSalesHistoryPTED+1;
		return atte;
	}
	
	loading_win.show();
	var PR_ACT=0;	var LS_ACT=0;	var FC_ACT=0;	var MG_ACT=0;
	Ext.Ajax.request( 
	{  
		waitMsg	:	'Processing...',
		url		:	'properties_tabs/propertyImport/propertyImportBuilt.php', 
		method	:	'POST',
		timeout :	600000,
		params	:	{
			text		:	"ed",
			combo		:	"de",
			readOnly	:   true,
			composite	:	"eTcomposite"
		},
		failure	:	function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success	:	function(response,options){
			var aRes=response.responseText.split("^");
			
			var publicrecordView=Ext.decode(aRes[0]);
			var listingView=Ext.decode(aRes[1]);
			var foreclosureView=Ext.decode(aRes[2]);
			var mortgageView=Ext.decode(aRes[3]);
			var publicrecordmoreView=Ext.decode(aRes[4]);
			var listingmoreView=Ext.decode(aRes[5]);
			//var byowner=Ext.decode(aRes[6]);
			//var byownermore=Ext.decode(aRes[7]);
			
			var displayImageView = new Ext.BoxComponent({ id:'displayImageMapView', style: 'position:relative; height:100%;'});
			
			mapsLatLonView.idRender	=	"displayImageMapView";
			
			formulViewPt = new Ext.FormPanel({
				url			:	'properties_tabs/propertyView/propertyViewed.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				renderTo	:	'advView',
				id			:	'formulViewPt',
								name		:	'formulViewPt',
				items		:	[{
					layout		:	'table',
					bodyStyle	:	"padding:10px",
					layoutConfig:	{
		                columns	:	3
		            },
					items		:	[{
						xtype	:	'fieldset',
						title	:	'Property',
						width	:	600,
						height	:	200,
						layout	:	'column',
						items	:	[{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Address',
								id			:	'edproperty*address',
								readOnly	:true,
								name		:	'edproperty*address'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'City',
								id			:	'edproperty*city',
								readOnly	:true,
								name		:	'edproperty*city'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'County',
								readOnly	:true,
								name		:	'edproperty*county',
								id			:	'edproperty*county'
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								readOnly	:true,
								name		:	'edproperty*xcode',
								id				:	'edproperty*xcode',
								hiddenName		:	'hdproperty*xcode',
								store			:	propertiesType,				
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								typeAhead		:	true,
								fieldLabel		:	'Type',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	125
								
							}/*,{
								xtype		:	'textfield',
								fieldLabel	:	'Subd Name',
								id			:	'edproperty*sbdname',
								readOnly	:true,
								name		:	'edproperty*sbdname'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Year Built',
								id			:	'edproperty*yrbuilt',
								readOnly	:true,
								name		:	'edproperty*yrbuilt',
								width		:	50
							}*/,{
								xtype		:	'hidden',
								id			:	'edproperty*addressfull',
								readOnly	:true,
								name		:	'edproperty*addressfull'
							},{
								xtype		:	'hidden',
								id			:	'edproperty*latitude',
								readOnly	:true,
								name		:	'edproperty*latitude'
							},{
								xtype		:	'hidden',
								id			:	'edproperty*longitude',
								readOnly	:true,
								name		:	'edproperty*longitude'
							},{
								xtype		:	'hidden',
								id			:	'edproperty*parcel',
								readOnly	:true,
								name		:	'edproperty*parcel'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Unit',
								id			:	'edproperty*unit',
								readOnly	:true,
								name		:	'edproperty*unit',
								width		:	50
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Zip',
								id			:	'edproperty*zip',
								readOnly	:true,
								name		:	'edproperty*zip',
								width		:	50
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								readOnly	:true,
								name		:	'edproperty*state',
								id				:	'edproperty*state',
								hiddenName		:	'hdproperty*state',
								store			:	new Ext.data.JsonStore ({
									fields	:	['initial', 'name'],
									url		:	'../../resources/Json/states.json',
									autoLoad:	true
								}),				
								editable		:	false,
								displayField	:	'name',
								valueField		:	'initial',
								typeAhead		:	true,
								fieldLabel		:	'State',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155,
								listeners		:	{

								}
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								readOnly	:true,
								name		:	'edproperty*status',
								id				:	'edproperty*status',
								hiddenName		:	'hdproperty*status',
								store			:	propertiesStatus,
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								typeAhead		:	true,
								hidden			:	true,
								hideLabel		:	true,
								fieldLabel		:	'Status',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155
							}]
						}],
						buttonAlign	:	'center',
						buttons		:	[]
					},{
						xtype	:	'fieldset',
						title	:	'Property',
						width	:	600,
						height	:	200,
						//layout	:	'column',
						items	:	[displayImageView]
					}]//end items fieldset
				}],//end items fieldset
				listeners: {
					afterrender : function(){
						//expandcollapse('PR');
						//Ext.getCmp('PRMORE').expand();
						/*if(resultET.result.residential){
							if(resultET.result.residential.length !== 0)
								Ext.getCmp('FS').expand();
						}*/
					}
				}
			});
			
			formulPsummary = new Ext.FormPanel({
				url			:	'properties_tabs/propertyView/propertyViewed.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulPsummary',
								name		:	'formulPsummary',
				items		:	[{
					xtype		:	'fieldset',
					//title		:	'Public Records',
					collapsible	:	false,
					collapsed	:	false,
					id			:	'PR',
								name		:	'PR',
					items		:	{
						layout	:	'column',
						items	: 	publicrecordView
					}
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Sales History</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'SL',
								name		:	'SL',
					items		:	[{
						xtype	:	'panel',
						id		:	'SLEDSALES'/*,
						items	:	[{
						}]*/
					},{
						//layout	:	'column'
						items	:	[{
							xtype	:	'button',
							cls		:	'x-btn-text',
							text	:	'<i class="icon-plus"></i>',
							tooltip	:	'Add more Sales',
							listeners	:	{
								click	:	function(button){
									var aux = addSaleHistoryPTED();
									Ext.getCmp('SLEDSALES').add(aux);
									Ext.getCmp('SLEDSALES').doLayout();
									Ext.getCmp('salesTotalED').setValue(countSalesHistoryPTED);
								}
							}
						},{
							xtype	:	'hidden',
								name		:	'salesTotalED',
							id		:	'salesTotalED'
						}]
					}]
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Owner Information</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'PRMORE',
								name		:	'PRMORE',
					items		:	{
						layout	:	'column',
						items	:	publicrecordmoreView
					}
				}],
				listeners:{
					afterrender:	function(){
						//console.debug(this);
						setTimeout(function(){
							$(Ext.get('edpsummary*folio').dom).on('keyup',function (){
								//Ext.getCmp('edmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('edpendes*folio').setValue($(this).val());
								Ext.getCmp('edmortgage*folio').setValue($(this).val());
							});
							/*$(Ext.get('edmlsresidential*folio').dom).on('keyup',function (){
								Ext.getCmp('edpsummary*folio').setValue($(this).val());
								Ext.getCmp('edpendes*folio').setValue($(this).val());
								Ext.getCmp('edmortgage*folio').setValue($(this).val());
							});*/
							$(Ext.get('edpendes*folio').dom).on('keyup',function (){
								//Ext.getCmp('edmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('edpsummary*folio').setValue($(this).val());
								Ext.getCmp('edmortgage*folio').setValue($(this).val());
							});
							$(Ext.get('edmortgage*folio').dom).on('keyup',function (){
								//Ext.getCmp('edmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('edpendes*folio').setValue($(this).val());
								Ext.getCmp('edpsummary*folio').setValue($(this).val());
							});
						},500);
						//console.debug(Ext.getCmp('edpsummary*folio'));
						/*Ext.getCmp('edpsummary*folio').onBlur(event, target){
							console.debug("ASASASASs");
						};*/
					}
				}
			});

			/*formulResidential = new Ext.FormPanel({
				url			:	'properties_tabs/propertyView/propertyViewed.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulResidential',
								readOnly	:true,
								name		:	'formulResidential',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'For Sale/For Rent',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FS',
								readOnly	:true,
								name		:	'FS',
					items		:	{
						layout	:	'column',
						items	: 	listingView
					}
				}
			});*/
			
			formulForeclosed = new Ext.FormPanel({
				url			:	'properties_tabs/propertyView/propertyViewed.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulForeclosed',
								name		:	'formulForeclosed',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Foreclosure',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FO',
								name		:	'FO',
					items		:	{
						layout	:	'column',
						items	:	foreclosureView
					}
				}
			});

			formulMortgage = new Ext.FormPanel({
				url			:	'properties_tabs/propertyView/propertyViewed.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulMortgage',
								name		:	'formulMortgage',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Mortgage',
					collapsible :	false,
					collapsed	:	false,
					id			:	'MO',
								name		:	'MO',
					items		:	{
						layout	:	'column',
						items	:	mortgageView
					}
				}
			});
			
			var panelsViewMaps	=	new Ext.TabPanel({
				id				:	'panelsViewMaps-pt',
				plain			:	true,
				height			:	800,
				//activeTab		:	0,
				enableTabScroll	:	true,
				defaults		:	{  
					autoScroll	:	false
				},
				items:[{
					title	:	'Bird View',
					autoLoad:	{
						url		:	'/properties_tabs/propertyMaps/propertyMapView.php', 
						scripts	:	true,
						timeout	:	10800
					}
				},{
					title	:	'Street View',
					autoLoad:	{
						url		:	'/properties_tabs/propertyMaps/propertyStreetView.php', 
						scripts	:	true,
						timeout	:	10800
					}
				}]
			});
			
			var panelsView = new Ext.TabPanel({
				id				:	'panelsView-pt',
				renderTo		:	'panelsView',
		   		//activeTab		:	0,
				autoWidth		:	true,
				deferredRender	:	false,
				height			:	system_height - ($("#panelsView").offset().top - tablevel_height),
				plain			:	true,
				enableTabScroll	:	true,
				defaults		:	{  
					autoScroll	:	true
				},
				items:[
					{
						title	:	'Public Records', 
						id		:	'PR-tab-PT',
						items	:	formulPsummary,
						listeners:{
							afterrender:	function(){
								//Ext.getCmp('panelsView-pt').setHeight(1000);
							}
						}
					},{
						title		:	'County Site',
						id			:	'CS-tab-PT',
						autoScroll	:	false,
						html		:	'<iframe src="'+ resultET.result.aditionals.countyUrl +'" width="99%" height="99%"></iframe>'
					},{
						title		:	'Listing Site',
						id			:	'LIS-tab-PT',
						autoScroll	:	false,
						html		:	'<iframe src="'+ RtBase64.decode(resultET.result.aditionals.urlMore) +'" width="99%" height="99%"></iframe>'
					},/*{
						title	:	'For Sale / For Rent',
						id		:	'LS-tab-PT',
						items	:	formulResidential
					},*/{
						title	:	'Foreclosure',
						id		:	'FC-tab-PT',
						items	:	formulForeclosed
					},{
						title	: 	'Mortgage',
						id		:	'MT-tab-PT',
						items	:	formulMortgage
					},/*{
						title	: 	'Images',
						id		:	'IMG-tab-PT',
						autoLoad:	{
							url		:	'/properties_tabs/propertyImages/propertyImagesView.php', 
							scripts	:	true,
							timeout	:	10800
						}
					},*/{
						title	: 	'Maps',
						id		:	'MAP-tab-PT',
						items	:	[panelsViewMaps]
					}
				],
				listeners:{
					afterrender:	function(tab){
						tab.setActiveTab(0);
						tab.doLayout();
						//Extract the latitude and longitude for propertyMaps.php with namespace mapsLatLon
						$.each(resultET.result.property,function(i, ite){
							if(i=='edproperty*latitude')
								mapsLatLonView.latitude	=	ite;
							if(i=='edproperty*longitude')
								mapsLatLonView.longitude=	ite;
						});
						$.each(resultET.result, function(i, ite) {
							if(i != "sales" && i != "images" && i != "aditionals"){
								$.each(ite, function(j, itej) {
									switch(j){
										case "edproperty*state":
											Ext.getCmp(j).setValue(itej);
											break;
										default:
											Ext.getCmp(j).setValue(itej);
									}
								});
							}else if( i == "sales"){
								if(ite.total>0){
									Ext.getCmp('salesTotalED').setValue(ite.total);
									var j;
									for(j=0;j<ite.total;j=j+1){
										var aux = addSaleHistoryPTED();
										Ext.getCmp('SLEDSALES').add(aux);
										Ext.getCmp('SLEDSALES').doLayout();
									}
									$.each(ite.data, function(j, itej) {
										Ext.getCmp(j).setValue(itej);
									});
								}else{
									Ext.getCmp('salesTotalED').setValue(1);
									var aux = addSaleHistoryPTED();
									Ext.getCmp('SLEDSALES').add(aux);
									Ext.getCmp('SLEDSALES').doLayout();
								}
							}else if(i == "images"){
								imagesView.Import = ite;
								//This Variables is pased through Ext.namespace for propertyImagesView.php, it will be executed where that php document ready 
								
								SpaceNeedle=new VELatLong(mapsLatLonView.latitude,mapsLatLonView.longitude);
								//if(imagesView.Import.length <= 0){
									$("#"+mapsLatLonView.idRender).attr('map','true');
									var	map1 = new XimaMap(mapsLatLonView.idRender,'mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
									map1.map = new VEMap(mapsLatLonView.idRender);
									map1.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
									map1.map.HideDashboard();
									var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
									pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
										"<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
									map1.map.AddShape(pin); 
								//}
							}else if(i == "aditionals"){
								if(ite.countyUrl == "" || typeof ite.countyUrl == "undefined"){
									var tabPanel = Ext.getCmp('panelsView-pt');
									var tabToHide = Ext.getCmp('CS-tab-PT');
									tabPanel.hideTabStripItem(tabToHide);
								}
								if(ite.urlMore == "" || typeof ite.urlMore == "undefined"){
									var tabPanel = Ext.getCmp('panelsView-pt');
									var tabToHide = Ext.getCmp('LIS-tab-PT');
									tabPanel.hideTabStripItem(tabToHide);
								}	
							}
						});
						
						/*****************
						*	To Update Description on ComboBox State
						*****************/
						var store = Ext.getCmp('edproperty*state').getStore();
						store.on("load", function() {
						   Ext.getCmp('edproperty*state').setValue(Ext.getCmp('edproperty*state').getValue());
						});
						store.load();
						$("#editButtonsPT").show(1000);
					},
					tabchange: function(tabPanel,panel){
						//This only will be used once time, this reload once the tab MAPS. By Jesus
						if(panel.title	==	"Maps"){
							if(reloadMapViewPt == 0){
								Ext.getCmp('panelsViewMaps-pt').setActiveTab(0);
								/*var tab     = Ext.getCmp('panelsView-pt').getActiveTab();
								var updater = tab.getUpdater();
								updater.update({
									url		:	'/properties_tabs/propertyMaps/propertyMapView.php',
									cache	:	false
								});*/
								reloadMapViewPt =	1;
							}
						}
					}
				}
			});
			loading_win.hide();
		}
		
	});

</script>