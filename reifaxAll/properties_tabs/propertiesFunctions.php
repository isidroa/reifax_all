<?php
	/***********************************************
	*	Obtains Campos that are Type Real or Integer
	***********************************************/
	function obtainCampsRealInt($arrayTypes,$table,$typeCamp){
		$c=0;$campos="";
		foreach($arrayTypes as $value){
			if($c<>0)
				$campos	.=	",";
			$query	=	"SELECT campos FROM `searchtemplateadv` s WHERE s.`idGrid` = '$value'";
			$result	=	mysql_query($query);
			$result	=	mysql_fetch_assoc($result);
			$campos	.=	$result['campos'];
			$c++;
		}
	
		$query	=	"SELECT Campos as campos
					FROM camptit  
					WHERE idtc IN ($campos) AND tabla ='$table'
					AND TRIM(type) IN ($typeCamp)";
		$result	=	mysql_query($query);
		$campos=Array();
		while($row	=	mysql_fetch_assoc($result)){
			$campos[]	=	$row['campos'];
		}
		return $campos;
	}

	/*******************************************
	*	Obtain the Real Status For Any Property
	*******************************************/
	function statusReal($val,$parcel){
		$Array = Array(
					'AA' => 'FOR SALE',	
					'BB' => 'FOR RENT',	
					'DD' => 'FOR SALE BY OWNER',	
					'CC' => 'NON-ACTIVE',	
					'CS' => 'SOLD', 
					'FF' => 'FORECLOSURE', 
					'GG' => 'PENDING', 
					'EE' => 'FOR SALE BANK OWNED'
				);
		if($val=="A"){
			$query = "SELECT COUNT(*) FROM mlsresidential WHERE parcelid='$parcel'";
			mysql_query($query);
			if(mysql_affected_rows() > 0)
				$val="AA";
			else
				$val="BB";
		}
		return $Array[$val];
	}

	/*******************************************
	*	Obtain the Real CcodeD For Any Property
	*******************************************/
	function ccoded($val){
		$Array = Array(
				'01' => 'Single Family',		'04' => 'Condo/Town/Villa',		'03' => 'Multi Family +10',
				'08' => 'Multi Family -10',		'11' => 'Commercial',			'00' => 'Vacant Land',
				'02' => 'Mobile Home',			'99' => 'Other');
		return $Array[$val];
	}
	
	/*******************************************
	*	Convert Date (Ymd) to (m/d/Y)
	*******************************************/
	function formatDate($date){
		return date("m/d/Y",strtotime($date));
	}
	
	/*******************************************
	*	Convert Date (m/d/Y) to (Ymd)
	*******************************************/
	function formatDateImport($date){
		return date("Ymd",strtotime($date));
	}
	
	/*******************************************
	*	Delete Directories or Files
	*******************************************/
	function deleteDirOrFiles($dirname){
		if ( is_dir( $dirname ) ) {
			$dir_handle = opendir( $dirname );
		}
		if ( !$dir_handle ) {
			return false;
		}
		while ( $file = readdir( $dir_handle ) ) {
			if ( $file != '.' && $file != '..' ) {
				if ( !is_dir( $dirname.'/'.$file ) ) {
					unlink( $dirname.'/'.$file );
				} else {
					deleteDirOrFiles( $dirname.'/'.$file );
				}
			}
		}
		closedir( $dir_handle );
		rmdir( $dirname );
		return true;
	}

?>