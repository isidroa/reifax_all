<?php
	require('../../properties_conexion.php');
	require('../propertiesFunctions.php');
	conectar();
	$Array = Array();
	//print_r($_POST);
	$userid	=	$_COOKIE['datos_usr']['USERID'];
	foreach($_POST as $key => $value){
		$aux = explode("*",$key);
		$Array[substr($aux[0],2)][$aux[1]] = $value;
	}
	if(isset($_POST['unicID'])){
		$temp			=	explode("|",$_POST['unicID']);
		$parcelId		=	$temp[0];
		$statusOriginal	=	$temp[1];
	}
		
	//print_r($Array);
	foreach($Array as $key => $record){
		switch($key){
			case	"property":
				$cP=0;$camps="";
				foreach($record as $camp => $value){
					//Obtain the parcelid and avoid that it enter on the properties table.
					if($camp<>"parcel"){
						if($camp=="status"){
							$statusOriginal = $value;
							///Change the Values AA and BB for A
							if($value == "AA" or $value == "BB")
								$value="A";
						}
						if($cP<>0)
							$camps	.=	",";
						$camps	.=	"`$camp` = '$value'";
						$cP++;
					}else
						$parcelId	=	$value;
				}
				//Correct the addressFull
				foreach($record as $camp => $value){
					switch($camp){
						case "address":
							if(trim($value)<>""){
								$aux1	=	"$value, ";
								$aux	=	"$value ";
							}
							break;
						case "unit":
							if(trim($value)<>"")
								$aux1	=	"$aux UNIT $value, ";
							break;
						case "city":
							if(trim($value)<>"")
								$aux2	=	"$value, ";
							break;
						case "state":
							if(trim($value)<>"")
								$aux3	=	"$value ";
							break;
						case "zip":
							if(trim($value)<>"")
								$aux4	=	"$value";
							break;
					}
				}
				$aux = $aux1.$aux2.$aux3.$aux4;
				if(trim($aux) <> "")
					$camps	.=	", `addressfull` = '$aux'";
					
				$query	=	"UPDATE properties SET $camps WHERE parcelid='$parcelId'";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-1")));
				break;
			case	"psummary":
				$cP=0;$camps="";
				$campsRealInt	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'real','int'");
				$campsReal	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'date'");
				foreach($record as $camp => $value){
					if($cP<>0)
						$camps	.=	",";
					$value	=	trim($value);
					if(empty($value)){
						if(in_array($camp,$campsRealInt))
							$camps	.=	"`$camp` = 0";
						else
							$camps	.=	"`$camp` = '$value'";
					}else{
						if(in_array($camp,$campsDate))	//Update date camps to Ymd
							$camps	.=	"`$camp` = '".date("Ymd",strtotime($value))."'";
						elseif(in_array($camp,$campsReal))	//Update real's camps erase , by nothing
							$camps	.=	"`$camp` = '".str_replace(",","",$value)."'";
						else
							$camps	.=	"`$camp` = '$value'";
					}
					$cP++;
				}
				$query	=	"UPDATE psummary SET $camps WHERE parcelid='$parcelId'";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => mysql_error()."Error ET-2")));
				
				if($statusOriginal	!=	"BB"){	//Update the Parcelid in Rental O Residential
					$query	=	"UPDATE mlsresidential SET folio=".$_POST['edpsummary*folio']." WHERE parcelid='$parcelId'";
					if(!empty($_POST['edpsummary*folio']))
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => $query." -> ".mysql_error()."Error ET-2.1")));
				}else{
					$query	=	"UPDATE rental SET folio=".$_POST['edpsummary*folio']." WHERE parcelid='$parcelId'";
					if(!empty($_POST['edpsummary*folio']))
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => $query." -> ".mysql_error()."Error ET-2.2")));
				}
				//Process to Sales History
				for($i=0;$i<$_POST['salesTotalED'];$i++){
					if(	!empty($Array['sales']["date$i"]) OR $Array['sales']["price$i"]>0 OR $Array['sales']["priceSqft$i"]>0 OR !empty($Array['sales']["book$i"]) OR 
						!empty($Array['sales']["page$i"])){
						
						if(empty($Array['sales']["idsales$i"])){
							$cP=0;$camps="";$values="";
							foreach($Array['sales'] as $camp => $value){
								if(ereg_replace("[A-Za-z]", "", $camp) == $i){
									if(ereg_replace("[0-9]", "", $camp) == "date")	//Update date camps to Ymd
										$value	=	formatDateImport($value);
									elseif(ereg_replace("[0-9]", "", $camp) == "price" OR ereg_replace("[0-9]", "", $camp) == "priceSqft")	//Update real's camps erase , by nothing
										$value	=	str_replace(",","",$value);
										
									if($cP<>0){
										$camps	.=	",";	$values	.=	",";}

									$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."`";
									$values	.=	"'$value'";
									$cP++;
								}
							}
							$query	=	"INSERT INTO sales (`parcelid`,`recdate`,$camps) VALUES ('$parcelId','NOW()',$values)";
							mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-3 $query")));
						}else{
							$cP=0;$camps="";
							foreach($Array['sales'] as $camp => $value){
								if(ereg_replace("[A-Za-z]", "", $camp) == $i){
									if($cP<>0)
										$camps	.=	",";
									$value	=	trim($value);
									if(empty($value)){
										if(ereg_replace("[0-9]", "", $camp) == "price" OR ereg_replace("[0-9]", "", $camp) == "priceSqft")	//Update real's camps empty by 0
											$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."` = 0";
										else
											$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."` = '$value'";
									}else{
										if(ereg_replace("[0-9]", "", $camp) == "date")	//Update date camps to Ymd
											$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."` = '".date("Ymd",strtotime($value))."'";
										elseif(ereg_replace("[0-9]", "", $camp) == "price" OR ereg_replace("[0-9]", "", $camp) == "priceSqft")	//Update real's camps erase , by nothing
											$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."` = '".str_replace(",","",$value)."'";
										else
											$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."` = '$value'";
									}
									$cP++;
								}
							}
							$query	=	"UPDATE sales SET $camps WHERE parcelid='$parcelId' AND idsales='".$Array['sales']["idsales$i"]."'";
							mysql_query($query) or die(json_encode(Array("success" => false, "msg" => mysql_error()."Error ET-4")));							
						}
					}else{
						$query = "DELETE FROM sales WHERE idsales = '".$Array['sales']["idsales$i"]."'";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-5 $query")));
					}
				}
				break;
			/*case	"mlsresidential":
				//If is AA, Are mlsresidential else BB it's rental'
				$campsRealInt	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'real','int'");
				$campsReal	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'date'");
				if($statusOriginal	!=	"BB"){	//It's MLSRESIDENTIAL
					$query	=	"SELECT idmlsresidential FROM mlsresidential WHERE parcelid='$parcelId'";
					$result=mysql_query($query);
					
					$cP=0;$camps="";$values="";
					//Verify if it's new or not
					if(mysql_num_rows($result) > 0){	//It's for Update
						foreach($record as $camp => $value){
							if($cP<>0)
								$camps	.=	",";
							$value	=	trim($value);
							if(empty($value)){
								if(in_array($camp,$campsRealInt))
									$camps	.=	"`$camp` = 0";
								else
									$camps	.=	"`$camp` = '$value'";
							}else{
								if(in_array($camp,$campsDate))	//Update date camps to Ymd
									$camps	.=	"`$camp` = '".date("Ymd",strtotime($value))."'";
								elseif(in_array($camp,$campsReal))	//Update real's camps erase , by nothing
									$camps	.=	"`$camp` = '".str_replace(",","",$value)."'";
								else
									$camps	.=	"`$camp` = '$value'";
							}
							$cP++;
						}
						//print_r($record);
						$query	=	"UPDATE mlsresidential SET $camps WHERE parcelid='$parcelId'";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-6 $query")));
					}else{	//It's New
						foreach($record as $camp => $value){
							$value	=	trim($value);
							if(!empty($value)){
								if($cP<>0){
									$camps	.=	",";	$values	.=	",";
								}
								if(in_array($camp,$campsDate)){	//Update date camps to Ymd
									$camps	.=	"`$camp`";
									$values	.=	"'".date("Ymd",strtotime($value))."'";
								}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
									$camps	.=	"`$camp`";
									$values	.=	"'".str_replace(",","",$value)."'";
								}else{
									$camps	.=	"`$camp`";
									$values	.=	"'$value'";
								}
								$cP++;
							}
						}
						$query	=	"INSERT INTO mlsresidential (`parcelid`,$camps) VALUES ('$parcelId',$values)";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-7")));
					}
				}else{	//It's RENTAL
					$query	=	"SELECT COUNT(*) FROM rental WHERE parcelid='$parcelId'";
					mysql_query($query);
					
					$cP=0;$camps="";$values="";
					//Verify if it's new or not
					if(mysql_affected_rows() > 0){	//It's for Update
						foreach($record as $camp => $value){
							if($camp == "remark")	//change name because in Mlsresidential Table its Remark bat en Rental Table its Remark1
								$camp =	"remark1";
								
							if($cP<>0)
								$camps	.=	",";
							$value	=	trim($value);
							if(empty($value)){
								if(in_array($camp,$campsRealInt))
									$camps	.=	"`$camp` = 0";
								else
									$camps	.=	"`$camp` = '$value'";
							}else{
								if(in_array($camp,$campsDate))	//Update date camps to Ymd
									$camps	.=	"`$camp` = '".date("Ymd",strtotime($value))."'";
								elseif(in_array($camp,$campsReal))	//Update real's camps erase , by nothing
									$camps	.=	"`$camp` = '".str_replace(",","",$value)."'";
								else
									$camps	.=	"`$camp` = '$value'";
							}
							$cP++;
						}
						$query	=	"UPDATE rental SET $camps WHERE parcelid='$parcelId'";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-8 - $query - ".mysql_error())));
					}else{	//It's New
						foreach($record as $camp => $value){
							if($camp == "remark")	//change name because in Mlsresidential Table its Remark bat en Rental Table its Remark1
								$camp =	"remark1";
								
							$value	=	trim($value);
							if(!empty($value)){
								if($cP<>0){
									$camps	.=	",";	$values	.=	",";
								}
								if(in_array($camp,$campsDate)){	//Update date camps to Ymd
									$camps	.=	"`$camp`";
									$values	.=	"'".date("Ymd",strtotime($value))."'";
								}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
									$camps	.=	"`$camp`";
									$values	.=	"'".str_replace(",","",$value)."'";
								}else{
									$camps	.=	"`$camp`";
									$values	.=	"'$value'";
								}
								$cP++;
							}
						}
						$query	=	"INSERT INTO rental (`parcelid`,$camps) VALUES ('$parcelId',$values)";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-9")));
					}
				}
				break;*/
			case	"pendes":
				$campsRealInt	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'real','int'");
				$campsReal	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'date'");
				$query	=	"SELECT * FROM pendes WHERE parcelid='$parcelId'";
				mysql_query($query);
				
				$cP=0;$camps="";$values="";
				//Verify if it's new or not
				if(mysql_affected_rows() > 0){	//It's for Update
					foreach($record as $camp => $value){
						if($cP<>0)
							$camps	.=	",";
						$value	=	trim($value);
						if(empty($value)){
							if(in_array($camp,$campsRealInt))
								$camps	.=	"`$camp` = 0";
							else
								$camps	.=	"`$camp` = '$value'";
						}else{
							if(in_array($camp,$campsDate))	//Update date camps to Ymd
								$camps	.=	"`$camp` = '".date("Ymd",strtotime($value))."'";
							elseif(in_array($camp,$campsReal))	//Update real's camps erase , by nothing
								$camps	.=	"`$camp` = '".str_replace(",","",$value)."'";
							else
								$camps	.=	"`$camp` = '$value'";
						}
						$cP++;
					}
					//print_r($record);
					$query	=	"UPDATE pendes SET $camps WHERE parcelid='$parcelId'";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-10 $query")));
				}else{	//It's New
					foreach($record as $camp => $value){
						$value	=	trim($value);
						if(!empty($value)){
							if($cP<>0){
								$camps	.=	",";	$values	.=	",";
							}
							if(in_array($camp,$campsDate)){	//Update date camps to Ymd
								$camps	.=	"`$camp`";
								$values	.=	"'".date("Ymd",strtotime($value))."'";
							}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
									$camps	.=	"`$camp`";
									$values	.=	"'".str_replace(",","",$value)."'";
							}else{
								$camps	.=	"`$camp`";
								$values	.=	"'$value'";
							}
							$cP++;
						}
					}
					$separador	=	empty($camps)	?	""	:	",";
					
					$query	=	"INSERT INTO pendes (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-11 $query - ".mysql_error())));
				}
				break;
			case	"mortgage":
				$campsRealInt	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'real','int'");
				$campsReal	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'date'");
				$query	=	"SELECT * FROM mortgage WHERE parcelid='$parcelId'";
				mysql_query($query);
				
				$cP=0;$camps="";$values="";
				//Verify if it's new or not
				if(mysql_affected_rows() > 0){	//It's for Update
					foreach($record as $camp => $value){
						if($cP<>0)
							$camps	.=	",";
						$value	=	trim($value);
						if(empty($value)){
							if(in_array($camp,$campsRealInt))
								$camps	.=	"`$camp` = 0";
							else
								$camps	.=	"`$camp` = '$value'";
						}else{
							if(in_array($camp,$campsDate))	//Update date camps to Ymd
								$camps	.=	"`$camp` = '".date("Ymd",strtotime($value))."'";
							elseif(in_array($camp,$campsReal))	//Update real's camps erase , by nothing
								$camps	.=	"`$camp` = '".str_replace(",","",$value)."'";
							else
								$camps	.=	"`$camp` = '$value'";
						}
						$cP++;
					}
					//print_r($record);
					$query	=	"UPDATE mortgage SET $camps WHERE parcelid='$parcelId'";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-12 $query")));
				}else{	//It's New
					foreach($record as $camp => $value){
						$value	=	trim($value);
						if(!empty($value)){
							if($cP<>0){
								$camps	.=	",";	$values	.=	",";
							}
							if(in_array($camp,$campsDate)){	//Update date camps to Ymd
								$camps	.=	"`$camp`";
								$values	.=	"'".date("Ymd",strtotime($value))."'";
							}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
									$camps	.=	"`$camp`";
									$values	.=	"'".str_replace(",","",$value)."'";
							}else{
								$camps	.=	"`$camp`";
								$values	.=	"'$value'";
							}
							$cP++;
						}
					}
					$separador	=	empty($camps)	?	""	:	",";
					
					$query	=	"INSERT INTO mortgage (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-13 $query - ".mysql_error())));
				}
				break;
			/*case	"images":
				foreach($record as $values){
					foreach($values as $typeImage => $images){
						switch($typeImage){
							case	"old":	//Old Images
								$arrayTemp = Array();
								$c=0;	$queryIndex=""; $newIndex=100001;
								foreach($images as $index => $value){
									$separador	=	$c>0	?	","	:	"";
									
									$aux	=	explode("?",$value);
									$value	=	$aux['0'];
									$query	=	"SELECT idimagenes FROM imagenes WHERE parcelid='$parcelId' AND image LIKE '%$value'";
									$result	=	mysql_query($query) or die(mysql_error());
									
									if(mysql_num_rows($result)	>	0){
										$result		=	mysql_fetch_assoc($result);
										$queryIndex .=	$separador."'".$result['idimagenes']."'";
										$c++;
									}
									$arrayTemp[$newIndex] = $value;
									$newIndex++;
								}
								//Delete Imagenes fisicaly not Selected
								$query	=	"SELECT image,thumb FROM imagenes WHERE parcelid='$parcelId' AND idimagenes NOT IN ($queryIndex)";
								$result	=	mysql_query($query) or die(mysql_error());
								while($row	=	mysql_fetch_assoc($result)){
									@unlink(getenv("DOCUMENT_ROOT").$row['image']);
									@unlink(getenv("DOCUMENT_ROOT").$row['thumb']);
								}
								//Delete Imagener From database not Selected
								$query	=	"DELETE FROM imagenes WHERE parcelid='$parcelId' AND idimagenes NOT IN ($queryIndex)";
								mysql_query($query) or die(mysql_error());
								
								//Update temporal names for files
								foreach($arrayTemp as $index => $value){
									rename(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/$value",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/$index");
									rename(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/thumb/$value",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/thumb/$index");
								}
								
								//update new reals names with new numeration for each file
								$c=1;	$finalImages = Array();
								foreach($arrayTemp as $index => $value){
									$aux	=	explode(".",$value); //Extract the Extencion of file
									rename(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/$index",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/$c.$aux[1]");
									rename(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/thumb/$index",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/thumb/$c.$aux[1]");
									$finalImages[$value]	=	$c.".".$aux['1'];
									$c++;
								}
								
								foreach($finalImages as $index => $value){
									//echo "<br>".
									$query	=	"UPDATE imagenes 
													SET 
														image = '/propertiesImages/$userid/$parcelId/$value',
														thumb = '/propertiesImages/$userid/$parcelId/thumb/$value'
													WHERE parcelid='$parcelId' AND image LIKE '%$index'
												";
									mysql_query($query) or die(mysql_error());
								}
								
								break;	//End Case Old Images
							case	"news":	//News Images
								$query		=	"SELECT COUNT(*) AS total FROM imagenes WHERE parcelid='$parcelId'";
								$result		=	mysql_query($query) or die(mysql_error());
								$result		=	mysql_fetch_assoc($result);
								$totalImages=	$result['total'];
								
								$c = 0;	$query = "";
								foreach($images as $index => $value){
									$aux			=	explode("?",$value);
									$value			=	$aux['0'];	//Real Old name Image
									$aux			=	explode(".",$value); //Extract the Extencion of file with Cache Variable	Explample:	10.jpg?93545	Result:	jpg?93545
									$aux			=	explode("?",$aux['1']);//Extract the Real Extencion Deleting cache Variable	Explample:	jpg?93545		Result:	jpg
									$totalImages++;
									$newNameImage	=	$totalImages.".".$aux['0'];
									copy(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/edit/$value",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/$newNameImage");
									copy(getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/edithumb/$value",getenv("DOCUMENT_ROOT")."/propertiesImages/$userid/$parcelId/thumb/$newNameImage");
									
									if($c==0)
										$separador	=	"";
									else
										$separador	=	",";
										
									$query	.=	$separador."(0,'$parcelId','0','/propertiesImages/$userid/$parcelId/$newNameImage','/propertiesImages/$userid/$parcelId/thumb/$newNameImage',NOW())";
									$c++;
									$arrayNew[$value]	=	$newNameImage;
								}
								$query	=	"INSERT INTO imagenes VALUES $query";
								if($c>0)	//If there are New Images Procced to includes
									mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-482 - $query - ".mysql_error())));

								break;	//End Case News Images
						}
					}
				}
				/**********************
				*	Update Default Image
				**********************//*
				$query	=	"UPDATE imagenes SET `default`='0' WHERE parcelid='$parcelId'";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-489 - $query - ".mysql_error())));
				
				$aux			=	explode("?",$_POST['imageDefault']);
				$imageDefault	=	$aux['0'];
				
				$c = 0;
				if(count($arrayNew)>0){
					foreach($arrayNew as $index => $value){
						if($index	==	$imageDefault){
							$query	=	"UPDATE imagenes SET `default`='1' WHERE parcelid='$parcelId' AND image LIKE '%$value'";
							mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-505 - $query - ".mysql_error())));
							$c++;
							break;
						}
					}
				}
				if($c	==	0){
					foreach($finalImages as $index => $value){
						if($index	==	$imageDefault){
							$query	=	"UPDATE imagenes SET `default`='1' WHERE parcelid='$parcelId' AND image LIKE '%$value'";
							mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-518 - $query - ".mysql_error())));
							break;
						}
					}
				}
				
				echo "<pre>";
				print_r($finalImages);
				echo "</pre>";
				
				echo $queryIndex;
				echo "<br>JESUS FIN";
				
				break;	 //End Imagenes Case*/
		}
	}
	//echo json_encode(Array("success" => true, "msg" => "Property Edited Successfully", "id" => $parcelId."|".$statusOriginal, "optional" => $query));
	echo json_encode(Array("success" => true, "msg" => "Property Edited Successfully", "id" => $parcelId."|".$statusOriginal));
	//print_r($Array);
?>