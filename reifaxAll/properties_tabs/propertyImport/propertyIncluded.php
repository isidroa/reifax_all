<?php
	require('../propertiesFunctions.php');
	if(!isset($_POST['idMultiple'])){
		require('../../properties_conexion.php');
		conectar();
	}
	
	if(isset($_POST['foliofirstvalid'])){
		if(!empty($_POST['foliofirstvalid'])){
			$query	=	"SELECT * FROM properties a 
							INNER JOIN psummary b on a.parcelid=b.parcelid
							WHERE a.userid='".$_COOKIE['datos_usr']['USERID']."' AND b.folio='".$_POST['foliofirstvalid']."'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)>0)
				die(json_encode(Array("success" => true, "msg" => "Ready Exists")));
		}
	}
	
	$Array = Array();
	foreach($_POST as $key => $value){
		$value = trim($value);
		if(!empty($value)){
			$aux = explode("*",$key);
			$Array[substr($aux[0],2)][$aux[1]] = $value;
		}
	}
	if(isset($_POST['unicID'])){
		$temp			=	explode("|",$_POST['unicID']);
		$parcelId		=	$temp[0];
		$statusOriginal	=	$temp[1];
	}
	$userid	=	$_COOKIE['datos_usr']['USERID'];
	
	foreach($Array as $key => $record){
		switch($key){
			case "property":
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if($camp=="status"){
						$statusOriginal = $value;
						///Change the Values AA and BB for A
						if($value == "AA" or $value == "BB")
							$value="A";
					}
					if($camp=="xcode")
						$ccodedOriginal = ccoded($value);
					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				$query	=	"INSERT INTO properties (`userid`,`ccoded`,`regdate`,$camps) VALUES ('$userid','$ccodedOriginal',NOW(),$values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-1 - $query - ".mysql_error())));
				$parcelId = mysql_insert_id();
				break;
			case "psummary": //in Psummary it's procesed the sales history too
				$campsReal	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'date'");
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if(in_array($camp,$campsDate,true))	//Update date camps to Ymd
						$value	=	formatDateImport($value);
					elseif(in_array($camp,$campsReal,true))	//Update real's camps erase , by nothing
						$value	=	str_replace(",","",$value);
					
					if($camp == "legal")	//Delete all ( ' ) in string
						$value	=	str_replace("'","",$value);	

					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				
				$query	=	"INSERT INTO psummary (`parcelid`,$camps) VALUES ('$parcelId',$values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-2 - $query - ".mysql_error())));

				//Process to Sales History
				for($i=0;$i<$_POST['salesTotal'];$i++){
					if(	!empty($Array['sales']["date$i"]) OR $Array['sales']["price$i"]>0 OR $Array['sales']["priceSqft$i"]>0 OR !empty($Array['sales']["book$i"]) OR 
						!empty($Array['sales']["page$i"])){

						$cP=0;$camps="";$values="";
						foreach($Array['sales'] as $camp => $value){
							if(ereg_replace("[A-Za-z]", "", $camp) == $i){
								if(ereg_replace("[0-9]", "", $camp) == "date")	//Update date camps to Ymd
									$value	=	formatDateImport($value);
								elseif(ereg_replace("[0-9]", "", $camp) == "price" OR ereg_replace("[0-9]", "", $camp) == "priceSqft")	//Update real's camps erase , by nothing
									$value	=	str_replace(",","",$value);
									
								if($cP<>0){
									$camps	.=	",";	$values	.=	",";}

								$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."`";
								$values	.=	"'$value'";
								$cP++;
							}
						}
						$query	=	"INSERT INTO sales (`parcelid`,`recdate`,$camps) VALUES ('$parcelId',NOW(),$values)";
						mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-21 - $query - ".mysql_error())));
					}
				}
				break;
			case "mlsresidential":
			
				if($statusOriginal != "BB")
					$table	=	"mlsresidential";
				else
					$table	=	"rental";
					
				$campsReal	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'date'");
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if(in_array($camp,$campsDate,true))	//Update date camps to Ymd
						$value	=	formatDateImport($value);
					elseif(in_array($camp,$campsReal,true))	//Update real's camps erase , by nothing
						$value	=	str_replace(",","",$value);

					if($table == "rental"){
						if($camp == "remark")
							$camp	=	"remark1";
					}
					if($camp == "remark" or $camp == "remark1")	//Delete all ( ' ) in string
						$value	=	str_replace("'","",$value);	
							
					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO $table (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-3 - $query - ".mysql_error())));
				break;
			case "pendes":
				$campsReal	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'date'");
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if(in_array($camp,$campsDate,true))	//Update date camps to Ymd
						$value	=	formatDateImport($value);
					elseif(in_array($camp,$campsReal,true))	//Update real's camps erase , by nothing
						$value	=	str_replace(",","",$value);

					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO pendes (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-4 - $query - ".mysql_error())));
				break;
			case "mortgage":
				$campsReal	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'real'");
				$campsDate	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'date'");
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if(in_array($camp,$campsDate,true))	//Update date camps to Ymd
						$value	=	formatDateImport($value);
					elseif(in_array($camp,$campsReal,true))	//Update real's camps erase , by nothing
						$value	=	str_replace(",","",$value);

					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO mortgage (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-5 - $query - ".mysql_error())));
				break;
			case "images":	//Images with Links
			
				$query="";$c=0;
				foreach($_POST['txUrls'] as $index => $value){
					if(!empty($value)){
						$separador	=	$c>0	?	","	:	"";
						switch($index){
							case "countyUrl":	$index	=	"county";	break;
							case "urlDetail":	$index	=	"zillow";	break;
							case "urlMore"	:	$index	=	"realtor";	break;
							case "urlFree"	:	$index	=	"trulia";	break;
						}
						$query	.=	$separador."('$parcelId','$index','$value',NOW())";
						$c++;
					}
				}
				
				if(!empty($query))
					mysql_query("INSERT INTO propertyLinks VALUES ".$query) or die(json_encode(Array("success" => false, "msg" => "Error PT-51 - $query - ".mysql_error())));
					
				//Images Directopry
				$dirTemp	=	$_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/$parcelId/";
				if(!is_dir($dirTemp))
					mkdir($dirTemp);
				//Thumbails Directory
				$dirThumb	=	$dirTemp."thumb/";
				if(!is_dir($dirThumb))
					mkdir($dirThumb);
					
				$query="";$c=0;
				foreach($_POST["tximages"] as $index => $item){
					if(isset($_POST['idMultiple'])){
						$oldName=	$item;
						$aux	=	explode("-",$item);
						$item	=	$aux['1'];
					}else{
						$aux	=	explode("?",$item);
						$oldName=	$item	=	$aux['0'];	//Real Old name Image
					}
					if($c==0)
						$separador	=	"";
					else
						$separador	=	",";
					
					if($_POST['imageDefault'] == $item)
						$campDefault = "1";
					else
						$campDefault = "0";
						
					copy($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/temp/$oldName",$dirTemp.$item);
					copy($_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$userid/thumb/$oldName",$dirThumb.$item);
					$query	.=	$separador."(0,'$parcelId','$campDefault','/propertiesImages/$userid/$parcelId/$item','/propertiesImages/$userid/$parcelId/thumb/$item',NOW())";
					$c++;
				}
				$query	=	"INSERT INTO imagenes VALUES $query";
				if($c>0)
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-6 - $query - ".mysql_error())));
				break;
		}
	}
	echo json_encode(Array("success" => true, "msg" => "Property Registered Successfully", "id" => $parcelId."|".$statusOriginal));
	//print_r($Array);
?>