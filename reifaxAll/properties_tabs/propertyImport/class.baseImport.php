<?php

class import extends managerDownload{
	//variables globales
	var $fatalError=false;
	var $CUrlsRquest=array();
	var $requestIndex=0;
	var $htmlRquestCurl=array();
		
	/******************************
	*
	*	Get the html code that I will explore mobile
	*
	*******************************/
	function codeHtmlmobile($search,$constructUrl=true){
		$curl = curl_init($search);
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Opera/9.60 (J2ME/MIDP; Opera Mini/4.2.14912/812; U; ru) Presto/2.4.15');
					
		//We assign the data to the variable $html
		
		$html	=	curl_exec($curl);
		$info	=	curl_getinfo($curl);
		//$info['http_code'] ='';
		$info	=	json_encode($info);
		$sql="insert into recordHTML_spaider (html,url,infoT,date_record) VALUES ('".mysql_real_escape_string($html)."','$url','".mysql_real_escape_string($info)."',NOW())";
		mysql_query($sql) or die ($sql.mysql_error()) ;
		
		curl_close($curl);
		
		//Close the Operation cUrl
		//curl_close($curl);
		
		return $html;
	}

	
	/******************************
	*
	*	Create thumbnail from any image
	*
	*******************************/
	function createThumbs( $pathToImages, $pathToThumbs, $thumbWidth ) {
		// open the directory
		$dir = opendir( $pathToImages );

		// loop through it, looking for any/all JPG files:
		while (false !== ($fname = readdir( $dir ))) {
			if(!empty($this->idMultiple)){
				$aux	=	explode("-",$fname);
				if($aux[0]	!=	($this->idMultiple))
					$flag	=	false;
				else
					$flag	=	true;
			}else
				$flag	=	true;
			
			if($flag){
				// parse path for the extension
				$info	=	pathinfo($pathToImages . $fname);
				// continue only if this is a JPEG image
				if ( strtolower($info['extension']) == 'jpg' ){
					//echo "Creating thumbnail for {$fname} <br />";

					// load image and get image size
					$img	=	imagecreatefromjpeg( "{$pathToImages}{$fname}" );
					$width	=	imagesx( $img );
					$height	=	imagesy( $img );

					// calculate thumbnail size
					$new_width	=	$thumbWidth;
					$new_height	=	floor( $height * ( $thumbWidth / $width ) );

					// create a new tempopary image
					$tmp_img	=	imagecreatetruecolor( $new_width, $new_height );

					// copy and resize old image into new image 
					imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

					// save thumbnail into a file
					imagejpeg( $tmp_img, "{$pathToThumbs}".strtolower($fname)."" );
				}
			}
		}
		// close the directory
		closedir( $dir );
	}

	/******************************
	*
	*	Save Fisicaly Images on RealtyTask
	*
	*******************************/
	function saveImage($url,$path) {
		$c = curl_init();
		curl_setopt($c,CURLOPT_URL,$url);
		curl_setopt($c,CURLOPT_HEADER,0);
		curl_setopt($c,CURLOPT_RETURNTRANSFER,true);
		$s = curl_exec($c);
		curl_close($c);
		$f = fopen($path, 'wb');
		$z = fwrite($f,$s);
		if ($z != false) return true;
		return false;
	}
	
	/******************************
	*
	*	Create image tem
	*
	*******************************/
	function saveImgTem($image,$prefix=''){
		$dirImages=$this->verifyPathImg(false);
		if(!empty($image)){
			$c++;
			$image=preg_replace('/:\d.*/','',$image);
			$aux=explode(".",$image);
			$position=count($aux)-1;
			$this->saveImage($prefix.$image,$dirImages."temp/$c.$aux[$position]");
			$this->images['temp'] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."temp/$c.$aux[$position]");
			$this->images['thumb'] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."thumb/$c.$aux[$position]");
		}
		$this->createThumbs($dirImages."temp/",$dirImages."thumb/",96);
	}
	
	/******************************
	*
	*	function responsible for verifying directory structure for the images of a property
	*
	*******************************/
	function verifyPathImg($user=TRUE){
		
		$parcelid=$this->parcelid;
		$type=($user)?'user':'property';
		$dirImages = $_SERVER["DOCUMENT_ROOT"]."/propertiesImages/$type/";
		
		//Create User Directory for properties images
		if(!is_dir($dirImages))
			mkdir($dirImages);
		$dirImages = $dirImages."$parcelid/";
		
		//Create User Directory for properties images
		if(!is_dir($dirImages))
			mkdir($dirImages);
		
		//Create User temp Directory for properties images
		if(!is_dir($dirImages."temp/"))
			mkdir($dirImages."temp/");
		
		//Create User thumb Directory for properties images
		if(!is_dir($dirImages."thumb/"))
			mkdir($dirImages."thumb/");
		
		//Create User Edit Directory for new Upload properties images
		if(!is_dir($dirImages."edit/"))
			mkdir($dirImages."edit/");
		
		//Create User EdiThumb Directory for new Upload properties images
		if(!is_dir($dirImages."edithumb/"))
			mkdir($dirImages."edithumb/");
			
		return $dirImages;
	}
	//registro de errores de las clases
	
	function recordError($log,$line,$type=2){
		/*
		$sql="INSERT INTO `log_sphider`
				(`log`,`id_server`,`peticion`,`date`,`line`,`status`,`type`)
					VALUES 
				(
					'{$log}',
					{$this->idServer},
					{$this->lastRequest},
					NOW(),
					{$line},
					1,
					$type
				)";
		//mysql_query($sql) or die ($sql.mysql_error());
		if($type==1){
			$this->fatalError=true;
		}
		 * 
		 */
	}
	
	
	//Configurar latitude y longitude y formatear a 6 decimales.
	function setLatLng($str){
		if(strpos($str,'.')===false){
			return preg_replace('/\d{6}$/','.'.substr($str,-6),$str);
		}else{
			$aux = explode('.',$str);
			return ($aux[0]).str_pad(($aux[1]),6,"0",STR_PAD_RIGHT); 
		}
	}	
	
}

?>