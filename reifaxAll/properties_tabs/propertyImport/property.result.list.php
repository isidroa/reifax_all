<style>
	.resultList{
		clear:both;
		border:solid 1px #D0D0D0;
		background:#F7F7F7;
		cursor:pointer;
		margin:0px;
		position:relative;
		width:600px;
		float:right;
		-webkit-box-shadow: 0 0 10px 0px #878787;
		-moz-box-shadow: 0 0 10px 0px #878787;
		box-shadow: 0 0 10px 0px #878787;
	}
	.resultList:hover,.resultList.active{
		border:solid 1px #376A95;
		background:#fDfDfD;
	}
	.resultList .numberProperty{
		float: left;
	    padding: 70px 0 0 6px;
	    text-align: right;
	    width: 36px;
	}
	.resultList .content{
		float:left;
		width:100%;
		font-size: 13px;
	}
	.resultList .details{
		float:left;
	}
	.resultList .contentBtn{
		box-shadow:none;
		width:auto;
		height:auto;
		position:relative;
		border:none;
	}
	.contentBtn2 li{
		float:left;
		margin-left:10px;
	}
	.contentBtn4{
		float:right;
	}
	.contentBtn4 a{
		margin:3px 0 3px 0;
	}
	
	.contentBtn1 li{
		margin-bottom:3px;
	}
	.contentBtn a{
		display:block;
		padding:5px;
		padding-right:10px;
		background-size:23px !important;
		border-radius:2px 0 0 2px;
	}
	.contentBtn a:hover{
		background-color:#f3f3f3 !important;
		background-size:23px !important;
	}
	.resultList .content img{
		 background: none repeat scroll 0 0 #FFFFFF;
		 border: 1px solid #D0D0D0;
		 float: left;
		 margin: 0 10px 0 15px;
		 width: 125px;
	}
	.resultList:hover .content img,.resultList.active .content img{
		background:#F7F7F7;
	}
	.resultList .clear{
		clear:both;
	}
	.resultList .content table{
		margin-left:15px;
	}
	.resultList h3{
		color:#376A95;
		font-size: 16px;
		padding-top:6px;
		margin-bottom:5px;
		height:23px;
		border-bottom: solid 2px #376A95;
	}
	.resultList h3 strong{
		float:right;
	}
	.resultList h3 input{
		float:left;
	}
	.resultList h3 span{
		margin-left:20px;
		float:left;
	}
	.resultList:hover h3,.resultList.active h3{
		background:#376A95;
		color:#FFF;
	}
	.resultList .details tr {
	  display: block;
	  margin-bottom: 8px;
	}
	.resultList td{
		font-size: 13px;
		padding:3px;
	}
	.resultList td.title{
		width:96px;
		font-weight: bold;
	}
	.resultList .details tr td:first-child {
		width:60px;
	}
	.resultList td.descr{
		width:110px;
	}
	#toolbarresulSearch{
		position:absolute;
		top:0px;
		width:100%;
		height:40px;
		border-bottom:solid 2px #D0D0D0;
		z-index:2;
		background:#F4F4F4;
	}
	#containerResult{
		height:100%;
		width:100%;
		overflow:hidden;
	}
	#containerResult > div:first-child{
		padding-top:40px;
		float:left;
	}
	#containerResult > div:first-child > div{
		float:left;
		overflow:auto;
		background:url(/resources/img/backgroundListSearch.png);
	}
	.sombraTolbar{
		-webkit-box-shadow: 0 1px 15px #777;
		-moz-box-shadow: 0 1px 15px  #777;
		box-shadow: 0 1px 15px  #777;
	}
	#toolbarresulSearch .indicadores{
		color: #36475A;
		font-weight: bold;
		padding: 8px;
		position: absolute;
		right: 10px;
	}
	#toolbarresulSearch .paginationSearch {
		margin-top: 2px;
		padding-top: 2px;
	}
	.titlePagin{
		margin-top:4px;
	}
	#toolbarresulSearch .paginationSearch a{
		background-clip: padding-box;
		background-color: #FFFFFF;
		background-image: -moz-linear-gradient(center bottom , #EEEEEE 0%, white 50%);
		border: 1px solid #AAAAAA;
		border-radius: 4px 4px 4px 4px;
		display: inline-block;
		font-size: 15px;
		margin: 0 3px;
		padding: 2px;
		text-align: center;
		width: 85px;
	}
	#toolbarresulSearch .paginationSearch a:hover{
		background: #f8f8f8; /* Old browsers */
		background: -moz-linear-gradient(top,  #f8f8f8 10%, #ffffff 38%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(10%,#f8f8f8), color-stop(38%,#ffffff)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #f8f8f8 10%,#ffffff 38%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #f8f8f8 10%,#ffffff 38%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #f8f8f8 10%,#ffffff 38%); /* IE10+ */
		background: linear-gradient(to bottom,  #f8f8f8 10%,#ffffff 38%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8f8f8', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
		border: 1px solid #666;
	}
	#toolbarresulSearch .paginationSearch a strong{
		background: none repeat scroll 0 0 #FFFFFF;
		color: #414141;
		font-weight: bold;
		padding: 2px 5px;
		position:relative;
	}
	#toolbarresulSearch .paginationSearch .inactive:hover{
		box-shadow:none;
		background:#CFCFCF;
	}
	#toolbarresulSearch .paginationSearch .active{
		background:#376A95;
		color:#FFF;
		box-shadow:none;
		cursor:text;
	}
	#toolbarresulSearch .paginationSearch > div{
		position:relative;
		width:350px;
	}
	.btn-actions{
		float:left;
	}
	.btn-actions .check{
		border: 1px solid #AAAAAA;
		background:#FFF;
		color: #FFFFFF;
		display: block;
		float: left;
		font-weight: bold;
		height: 14px;
		margin: 12px 8px 4px;
		position: relative;
		width: 14px;
	}
	.btn-actions .check span{
		display: block;
		height: 22px;
		margin: -3px;
		width: 22px;
	}
	.btn-actions .check:hover{
		border: 1px solid #777;
	}
	.btn-actions .btn{
		background-size: 25px 25px !important;
		border-radius: 3px 3px 3px 3px;
		float: left;
		height: 27px;
		margin: 5px 6px;
		width: 27px;
	}/*
	.btn-actions .btn:hover{
		border: 1px solid #777;
		background-color:#FFF !important;
		box-shadow:0 0 1px #777777;
		margin: 4px 5px;
	}*/
	.check-some{
		background:url(https://ssl.gstatic.com/ui/v1/menu/checkmark-partial.png);
	}
	.check-all{
		background:url(https://ssl.gstatic.com/ui/v1/menu/checkmark.png);
	}
	.btn-actions .chzn-container{
		margin-top:5px;
	}
	.btn-actions .chzn-results{
		max-height:240px;
	}
	.mapContainer{
		position:absolute;
		background:#333;
		top:40px;
		left:0px;
		right:620px;
		bottom:0px;
	}
	.mapContainerGrid{
		right:0px;
		bottom:50%;
		display:none;
	}
	.mapContainerGrid.show{
		display:block;
	}
	.page > table{
		width:98%;
		margin:0 1%;
	}
	.gridContainerResult .page > table thead td{
		background:#005C83;
		color:#FFF;
		border-left:solid 1px #fafafa;
	}
	.gridContainerResult .page > table td{
		background:#fafafa;
		color:#005C83;
		padding:4px 5px;
	}
	.gridContainerResult .page > table tbody tr:hover td{
		background:#E1EEF2;
		cursor:pointer;
		font-weight:bold;
	}
	.gridContainerResult{
		float:none;
		position:relative;
	}
	.conMap{
		padding-top:0px !important;
		top:50% !important;
	}
</style>
<div id="toolbarresulSearch">
	<div class="btn-actions">
    	<a href="#" class="check" data-action="select">
        	<span></span>
        </a>
    	<a href="#" class="btn icon-map" style="display:none" title="Show/ Hide Map" data-action="map">
        	<span></span>
        </a>
    	<a href="#" class="btn icon-listing" title="Add To Listings"  data-action="listing">
        	<span></span>
        </a>
    	<a href="#" class="btn icon-offert" title="Add To Offers"  data-action="offert">
        	<span></span>
        </a>
    	<a href="#" title="Change View" class="btn icon-view-grid" data-action="changeOverview">
        	<span></span>
        </a>
        <select title="Sort By..." data-action="sort" style="width:110px; margin-top:6px;">
        	<option value="feacture">Sort By...</option>
        	<option value="newest">Newest</option>
        	<option value="price">Price (Lo-Hi)</option>
        	<option value="price1">Price (Hi-Lo)</option>
        	<option value="address">Address</option>
        	<option value="beds">Beds</option>
        	<option value="baths">Baths</option>
        	<option value="sqft">Sqft</option>
        	<option value="PropertyType">Property type</option>
        </select>
    </div>
    <div class="count indicadores">
    </div>
    <center class="paginationSearch">
        <div class="titlePagin">
            <a class="btnBack" href="#">Previous</a>
        	<span>Page 1</span>
            <a class="btnNext" href="#">Next</a>
        </div>
    </center>
</div>
<div id="containerResult">
	<div>
    	<div class="clear"></div>
    </div>
    <div class="mapContainer" id="SysRt.globalVar.result.map">
    	
    </div>
</div>

<script>
	var templatePgn='<div class="page"></div>';
	var templatePgnGrid='<div class="page"><table>'+
							'<thead><tr>'+
								'<td class=""></td>'+
								'<td class="number">Nº</td>'+
								'<td style="width:45%" class="address">Address</td>'+
								'<td class="price">Price</td>'+
								'<td style="width:150px" class="type">Type</td>'+
								'<td class="beds">Beds</td>'+
								'<td class="baths">Baths</td>'+
								'<td class="sqft">Sqft</td>'+
								'<td  style="width:80px" class="status">Status</td>'+
							'</tr></thead>'+
						'</table></div>';
	var template ='<div class="resultList">'+
					//'<div class="numberPropertyResult"></div>'+
					'<h3><input type="checkbox"><span data-container="address"></span>'+
					' <strong class="numberPropertyResult"></strong></h3>'+
					'<div class="content"><img>'+
					'<div class="details">'+
						'<table>'+
							'<tr>'+
								'<td class="title">Price:</td>'+
								'<td class="price descr"></td>'+
								'<td class="title">Propery Type:</td>'+
								'<td class="type descr"></td>'+
							'</tr><tr>'+
								'<td class="title">Sqft:</td>'+
								'<td class="sqft descr"></td>'+
								'<td class="title">Status:</td>'+
								'<td class="status descr"></td>'+
							'</tr><tr>'+
								'<td class="title">Beds</td>'+
								'<td class="beds descr"></td>'+
								'<td class="title">Baths:</td>'+
								'<td class="baths descr"></td>'+
							'</tr>'+
						'</table>'+	
					'</div>'+
					'</div>'+
					'<div class="clear"></div>'+
				'</div>';
	
	var templateGrid='<tr>'+
						'<td class=""><input type="checkbox"></td>'+
						'<td class="numberPropertyResult"></td>'+
						'<td class="address" data-container="address"></td>'+
						'<td class="price"></td>'+
						'<td class="type"></td>'+
						'<td class="beds"></td>'+ 
						'<td class="baths"></td>'+
						'<td class="sqft"></td>'+
						'<td class="status"></td>'+
					'</tr>';
	
	SysRt.globalVar.result.properties.length;
	SysRt.globalVar.result.layoutActive='view';
	SysRt.globalVar.result.currentRemote=1;
	SysRt.globalVar.result.currentLocate=1;
	SysRt.globalVar.result.perPage=(SysRt.globalVar.result.properties.length<15)?SysRt.globalVar.result.properties.length:15;
	SysRt.globalVar.result.perPageRemote=SysRt.globalVar.result.properties.length;
	SysRt.globalVar.result.page=SysRt.globalVar.result.numberResult/SysRt.globalVar.result.perPage;
	SysRt.globalVar.result.ResultApro=SysRt.globalVar.result.numberResult;
	SysRt.globalVar.result.ResultAproShow=(SysRt.globalVar.result.ResultApro%5==0 || SysRt.globalVar.result.ResultApro < SysRt.globalVar.result.perPage)?
								SysRt.globalVar.result.ResultApro :
								(parseInt(SysRt.globalVar.result.ResultApro) + (5-(SysRt.globalVar.result.ResultApro%5)));
	
	
	SysRt.globalVar.result.ancho=$('#containerResult').width();
	SysRt.globalVar.result.alto=$('#containerResult').height();
	
	//	DEFINICIONES DE LOS ESTADOS
	
	var statusPro={
		ForSale 	: 'For Sale',
		'House For Sale'	:'For Sale',
		'Condo For Sale'	:'For Sale',
		'For Sale' 	: 'For Sale',
		ForRent		: 'For Rent',
		'For Rent'	: 'For Rent',
		'Sold'		: 'Sold',
		RecentlySold	: 'Sold',
		Zestimate	: 'Foreclosed',
		Other		: 'Other'
	}
	
	//	DEFINICIONES DE LOS TIPOS
	var typePro={
		Condo 			: 'Condo/Apartament',
		Apartment		: 'Condo/Apartament',
		Townhouse		: 'Condo/Apartament',
		Townhome		: 'Condo/Apartament',
		'Apartment/condo/townhouse'		: 'Condo/Apartament',
		'Apt/Condo/Twnhm'				: 'Condo/Apartament',
		'Single Family'			: 'Single Family',
		'Single-family'			: 'Single Family',
		'Single-Family Home'	: 'Single Family',
		'Multi family'			: 'Multy Family',
		'Multi-family'			: 'Multy Family',
		'Multi-Family Home'		: 'Multy Family',
		'Mobile/manufactured'	: 'Mobile',
		Lost			: 'Lost/Land',
		Land			: 'Lost/Land',
		'Lot/Land'		: 'Lost/Land'
		
	}
	
	
	/**********************
	
		definicion de funciones para desplazamiento
	
	*********************/
	
	$('.btnBack').on('click',function (e){
		e.preventDefault();
		if(SysRt.globalVar.result.currentLocate>1){
			var newPage=SysRt.globalVar.result.currentLocate-1;
			moveToPage(newPage);
		}
	});
	$('.btnNext').on('click',function (e){
		e.preventDefault();
		console.info('next');
		var newPage=SysRt.globalVar.result.currentLocate+1;
		
		if($('#containerResult [data-page='+(newPage-1)+']').length){
			console.info('hay pagina');
			moveToPage(newPage);
		}
		else{
			if(((SysRt.globalVar.result.currentRemote+1)*SysRt.globalVar.result.perPageRemote)>SysRt.globalVar.result.numberResult){
				if(((SysRt.globalVar.result.numberResult/SysRt.globalVar.result.perPageRemote)+1)>newPage){
					makePage(newPage);
					moveToPage(newPage);
					return false;
				}
				else{
					return false;
				}
			}
			console.info('construir pagina');
			makePage(newPage);
			moveToPage(newPage);
		}
	});
	/**********************
	
		definicion de funciones para desplazamiento
	
	*********************/
	function makePage(page){
		page--;
		if(SysRt.globalVar.result.layoutActive=='view'){
			var container=$(templatePgn).attr('data-page',page);
		}
		else{
			var container=$(templatePgnGrid).attr('data-page',page);
		}
		while(((1+page)*SysRt.globalVar.result.perPage)>SysRt.globalVar.result.properties.length && SysRt.globalVar.result.currentRemote <=SysRt.globalVar.result.page){
			getNewPage();
		}
		var indece=page*SysRt.globalVar.result.perPage;
		for(k=(indece);k<((1+page)*SysRt.globalVar.result.perPage);k++){
			
			if(!SysRt.globalVar.result.properties[k])
				break;
			e=SysRt.globalVar.result.properties[k];
			if(SysRt.globalVar.result.layoutActive=='view'){
				var tem=$(template);
			}
			else{
				var tem=$(templateGrid);
			}
			$.each(e,function (i,el){
					e[i]=(el== null)?'-':el;
			});
			
			// load data to template
			$('[data-container=address]',tem).html(e.address);
			var img=new Image();
			img.src=e.img.thumb;
			img.dataId='imgResul_'+k
			$('img',tem).attr('data-image','imgResul_'+k);
			img.onload=function (){
				$('img[data-image='+this.dataId+']').attr('src',this.src);
			};
			img.onerror=function (){
				$('img[data-image='+this.dataId+']').attr('src','http://www.mnit.ac.in/new/PortalProfile/images/faculty/noimage.jpg');
			};
			
			$('.price',tem).html(((typeof e.price == "undefined" || e.price == '')?'--':'$'+Ext.util.Format.number(e.price,'0,000')));
			
			
			$('.beds',tem).html((typeof e.beds == "undefined" || e.beds == '')?'--':e.beds);
			$('.baths',tem).html((typeof e.baths == "undefined" || e.baths == '')?'--':e.baths);
			
			
			if(typeof e.sqft === "undefined"){
					$('.sqft',tem).html('--');
			}
			else{
				var auxInt=(typeof e.sqft == 'number')?e.sqft.toString():e.sqft;
				$('.sqft',tem).html(Ext.util.Format.number(auxInt,'0,000'));
			}
			
			
			if(typeof e.lot === "undefined"){
				$('.lot',tem).html('--');
			}
			else{
				var auxInt=(typeof e.sqft == 'number')?e.lot.toString():e.sqft;
				
				$('.lot',tem).html(Ext.util.Format.number(auxInt,'0,000'));
			}
			$('.marke',tem).html(
				(typeof e.marke == "undefined" || $.trim(e.marke) == '')?'--':e.marke);
			
			$('.built',tem).html(
				(typeof e.yb == "undefined" || $.trim(e.yb) == '')?'--':e.yb);
			
			//definiendo status y verificando la existenia del mismo
			if(statusPro[$.trim(e.status)]== null)
				console.log('status no definido: '+e.status)
			$('.status',tem).html((statusPro[$.trim(e.status)] != null)?statusPro[$.trim(e.status)]:'Other');
				
			//definiendo los tipos y verificando la existenia del mismo
			if(typePro[$.trim(e.type)]== null)
				console.log('type no definido: '+$.trim(e.type))
			$('.type',tem).html((typePro[$.trim(e.type)] != null)?typePro[$.trim(e.type)]:'Other');
			
			
			// defining event of the buttons
			 			
			$(tem).data('numberProperty',(k)).on('click',{
					location	:	e.href,
					id			:	e.id,
					idlocal		:	e.idlocal,
					address		:	e.address,
					location	:	e.location,
					urlDirect	:	
						(SysRt.globalVar.result.properties.length==1)
							?SysRt.globalVar.result.urlDirect:
							false
				},showOverview).hover(
				function (e){
					var pin=SysRt.globalVar.result.map.getPushpin($(this).data('numberProperty'));
					if(pin)
						pinMouseOver(pin);
				},function (e){
					var pin=SysRt.globalVar.result.map.getPushpin($(this).data('numberProperty'));
					if(pin)
						infopinMouseOut(pin.getInfobox());
			});
			
			//defining counter
			$('.numberPropertyResult',tem).html(k+1);
			
			//correction assigning input onclick event
			$('input',tem).on('click',function (e){
				e.stopPropagation();
				refreshCheckPage();
			}).val(e.idlocal);
			
			
			
			if(SysRt.globalVar.result.layoutActive=='view'){
				container.append(tem);
				container.height(SysRt.globalVar.result.alto-40).width(SysRt.globalVar.result.ancho).scroll(function(e) {
					if(e.target.scrollTop>0){
						$('#toolbarresulSearch').addClass('sombraTolbar');
					}
					else{
						$('#toolbarresulSearch').removeClass('sombraTolbar');
					}
				});
			}
			else{
				console.debug(tem);
				container.find('table').append(tem);
				container.height(SysRt.globalVar.result.alto-40).width(SysRt.globalVar.result.ancho)
			}
		};
		container.append("<div class='clear'></div>");
		container.appendTo('#containerResult > div:first-child');
		$('#containerResult > div:first-child  > .clear').remove();
		
		var containerResult=$('#containerResult > div:first-child');
		
		var ancho = SysRt.globalVar.result.ancho;
		var pages=$('> div ',containerResult);
		console.debug(pages.length);
		containerResult.width((pages.length) * ancho);
		containerResult.append("<div class='clear' style='float:none; width:100%'></div>");
	}
	function filterProperties(properties){
		/*var works=SysRt.globalVar.result.q.split(' ');
		var expresion='';
		var expresionF='';
		
		$.each(works, function(i, e) {
			expresion+=expresion==''?e:'|'+e;
		});
		for(i=0;i<works.length;i++){
			expresionF+='.*('+expresion+')';
		}
		console.log(SysRt.globalVar.result.q);
		var expresionRegular=	new RegExp(expresionF+'.*','i');
		if(SysRt.globalVar.result.t!=0){
			var typeExpre={
				house			: 'single',
				apartment_condo	: 'condo|Apartment',
				duplex			: 'multi',
				mobile			: 'manufactured',
				land			: 'lots|land'
			};
			
			var type=new RegExp(typeExpre[SysRt.globalVar.result.t],'ig');
		}
		*/
		var res=new Array();
		var expresionRegularExcluyente=	new RegExp('undisclosed|disclosed','ig');
		$.each(properties,function (i,e){
			//var control1=expresionRegular.test(e.address);
			var control2=!expresionRegularExcluyente.test(e.address);
			if(control2){
				/*if(type){
					console.log('filter by type ', type)
					if(type.test(e.type))*/
						res.push(e);
				/*}
				else{
					res.push(e);
				}*/
			}
			
		});
		return res;
	}
	function getNewPage(){
		loading_win.show();
		SysRt.globalVar.result.currentRemote++;
		$.ajax({
			url		:'/properties_tabs/propertyImport/proterty.result.getPage.php',
			type	:'POST',
			async	:false,
			data	:{
				page: SysRt.globalVar.result.currentRemote,
				nextPage:SysRt.globalVar.result.nextPage
			},
			dataType:"json",
			success: function(res){
				if(res.success){
					loading_win.hide();
					//var data = filterProperties(res.data.properties);
					var data = res.data.properties;
					$.each(data,function (i,e){
						SysRt.globalVar.result.properties.push(e);
					});
					SysRt.globalVar.result.nextPage=res.data.nextPage;
				}
				else{
				}
			}
		});	
		return true;
	}
	function moveToPage(page){
		$('#toolbarresulSearch .titlePagin span').html('Page '+page);
		$('#containerResult [data-page='+(page)+']').scrollTop(0);
		SysRt.globalVar.result.currentLocate=page;
		page--;
		
		var toNumber=((SysRt.globalVar.result.perPage*(page+1))>SysRt.globalVar.result.ResultApro)?
						SysRt.globalVar.result.ResultApro:
						(SysRt.globalVar.result.perPage*(page+1));
		$('#toolbarresulSearch .indicadores').html('Results '+
														((SysRt.globalVar.result.perPage*page)+1)+' to '+
														(toNumber)+' of about '+
														(SysRt.globalVar.result.ResultAproShow)+' ');
		var pos=SysRt.globalVar.result.ancho*(page);
		
		$('#containerResult > div:first-child').delay(200).animate({
			marginLeft	:	'-'+(pos)+'px'
		},700);
		
		refreshCheckPage();
		loadMapResult(page);
	}
	
	function showOverview(obj){
		if($(obj.target).closest('input').length){
			return false;
		}
		loading_win.show();
		tabs.remove(Ext.getCmp('overviewSearch'));
		$.ajax({
			url		: '/properties_tabs/propertyImport/property.result.loadDataProperty.php',
			type	: 'POST',
		    timeout:800000,
			data	:{
				id		: obj.data.id,
				location: obj.data.location,
				address	: obj.data.address,
				idlocal	: obj.data.idlocal,
				urlDirect: obj.data.urlDirect
			},
			dataType:"json",
			success	: function(res){
				SysRt.globalVar.OS=res.data;
				
				tabs.remove(Ext.getCmp('overviewSearch'));
				tabs.setActiveTab(tabs.add({
						iconCls	: 'i-treemain',
						id		: 'overviewSearch',
						title	: "Overview",
						closable	:true,
						autoLoad	: {
							url	: '/properties_tabs/propertyImport/property.result.overview.php', 
							params	: {width: system_width}, 
							scripts	: true,
							timeout	: 10800
						}		
					}
				));
				loading_win.hide();
			},
			error	:function (){
				loading_win.hide();
			}
			
		});
	}
	function refreshCheckPage(){
		var total=$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input');
		var check=$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input:checked').length;
		var btn=$('#toolbarresulSearch [data-action=select] span');
		if(total.length==check){
			btn.removeClass('check-some').addClass('check-all');
		}
		else if (check>0){
			btn.removeClass('check-all').addClass('check-some');
		}
		else{
			btn.removeClass('check-all').removeClass('check-some');
		}
		total.each(function(index, element) {
            if($(element).is(':checked')){
				$(element).closest('.resultList').addClass('active');
			}
			else{
				$(element).closest('.resultList').removeClass('active');
			}
        });
	}
	function sorftResult(){
		var value=$(this).val();
		loading_win.show();
		
		SysRt.globalVar.result.currentRemote++;
		$.ajax({
			url		:'/properties_tabs/propertyImport/proterty.result.getPage.php',
			type	:'POST',
			async	:false,
			data	:{
				page	: SysRt.globalVar.result.currentRemote,
				type	: 'order',
				order	: value,
				page	: 1,
				nextPage:SysRt.globalVar.result.nextPage
			},
			dataType:"json",
			success: function(res){
				if(res.success){
					loading_win.hide();
					var data = res.data.properties;
					SysRt.globalVar.result.properties=[];
					
					SysRt.globalVar.result.currentRemote=1;
					SysRt.globalVar.result.currentLocate=1;
					$('#containerResult > div:first-child').empty();
					$.each(data,function (i,e){
						SysRt.globalVar.result.properties.push(e);
					});
					SysRt.globalVar.result.nextPage=res.data.nextPage;
					makePage(1);
				}
				else{
				}
			}
		});	
		return true;
	}
	function propertiesToList(Obj){
        Ext.MessageBox.show({
            title: 'Import Properties',
            progressText: '0% completed',
            width:400,
            progress:true,
            closable:false
        });
		var PRimported=0;
		var PRimportedError=0;
		var PRimport=$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input:checked');
		if(!PRimport.length){
			Ext.MessageBox.show({
				title: 'Alert',
				width:300,
				msg: 'Sorry, No properties selected. please select one or more properties',
				buttons: Ext.MessageBox.OK
			});
		}
		PRimport.each(function(index, element) {
			$.ajax({
				url		:'properties_tabs/propertyImport/propertyImportSO.php',
				type	:'POST',
				data	:{
					idlocal	: $(element).val(),
					type	: Obj.data.list
				},
				dataType:"json",
				success: function(data){
					PRimported++;
					if((PRimported+PRimportedError)==PRimport.length){
						Ext.MessageBox.hide();
						Ext.MessageBox.show({
							title: 'Success',
							width:300,
							msg: '<p>' + PRimported +' Properties Imported</p><p>' + PRimportedError +' Properties No Imported</p>',
							buttons: Ext.MessageBox.OK
						});
					}
					else{
						var i = (PRimported+PRimportedError)/PRimport.length;
						Ext.MessageBox.updateProgress(i, Math.round(100*i)+'% completed');
					}
				},
				error: function(){
					PRimportedError++;
					if((PRimported+PRimportedError)==PRimport.length){
						Ext.MessageBox.hide();
						Ext.MessageBox.show({
							title: 'Success',
							width:300,
							msg: '<p>' + PRimported +' Properties Imported</p><p>' + PRimportedError +' Properties No Imported</p>',
							buttons: Ext.MessageBox.OK
						});
					}
					else{
						var i = (PRimported+PRimportedError)/PRimport.length;
						Ext.MessageBox.updateProgress(i, Math.round(100*i)+'% completed');
					}
				}
			});
        });
	}
	function changeView(){
		if(SysRt.globalVar.result.layoutActive=='view'){
			SysRt.globalVar.result.layoutActive='grid';
			$(this).removeClass('icon-view-grid').addClass('icon-view-cudritos')
			$('#toolbarresulSearch [data-action=map]').show('fast');
			$('#containerResult .mapContainer').addClass('mapContainerGrid');
			$('#containerResult > div:first-child').addClass('gridContainerResult');
		}
		else{
			SysRt.globalVar.result.layoutActive='view';
			$(this).removeClass('icon-view-cudritos').addClass('icon-view-grid')
			$('#toolbarresulSearch [data-action=map]').hide('fast');
			$('#containerResult .mapContainer').removeClass('mapContainerGrid');
			$('#containerResult > div:first-child').removeClass('gridContainerResult');
		}
		var pag=parseInt(SysRt.globalVar.result.properties.length/SysRt.globalVar.result.perPage)+1;
		$('#containerResult > div:first-child').empty();
		for(i=1;i<pag;i++){
			console.debug(i);
			makePage(i);
		}
		moveToPage(SysRt.globalVar.result.currentLocate);
	}
	function loadMapResult (page){
		SysRt.globalVar.result.map.borrarTodoMap();
		SysRt.globalVar.result.map.map.setOptions({
			width	: $('#containerResult .mapContainer').width(),
			height	: $('#containerResult .mapContainer').height()
		});
		var arrLatLong=new Array();
		for(i=page*SysRt.globalVar.result.perPage;i<((page*SysRt.globalVar.result.perPage)+SysRt.globalVar.result.perPage)-1;i++){
			SysRt.globalVar.result.map.addPushpinInfobox(
				  i,
				  SysRt.globalVar.result.properties[i].latitude,
				  SysRt.globalVar.result.properties[i].longitude,
				  'http://www.realtytask.com/img/houses/verdeb.png',
				  SysRt.globalVar.result.properties[i].address,
				  SysRt.globalVar.result.properties[i].sqft,
				  SysRt.globalVar.result.properties[i].sqft,
				  SysRt.globalVar.result.properties[i].beds+'/'+SysRt.globalVar.result.properties[i].baths,
				  SysRt.globalVar.result.properties[i].price,
				  SysRt.globalVar.result.properties[i].status,
				  '',
				  ''
			 );
			arrLatLong.push(new Microsoft.Maps.Location(
				SysRt.globalVar.result.properties[i].latitude,
				SysRt.globalVar.result.properties[i].longitude));
		}
		SysRt.globalVar.result.map.map.setView({
		  bounds: Microsoft.Maps.LocationRect.fromLocations(arrLatLong)
		});
	}
	
	$('#toolbarresulSearch [data-action=select]').on('click',function(e){
		e.preventDefault();
		var btn=$(this).find('span');
		if(btn.hasClass('check-all')){
			$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input').removeAttr('checked');
		}
		else if(btn.hasClass('check-some')){
			$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input').attr('checked','checked');
		}
		else{
			$('#containerResult [data-page='+(SysRt.globalVar.result.currentLocate-1)+'] input').attr('checked','checked');
		}
		refreshCheckPage();
	});
	
	
	$('#toolbarresulSearch [data-action=sort]').data("placeholder","Order By...").chosen({disable_search :true,disable_search_threshold: 30 }).change(sorftResult).css('marginTop','5px');
	
	$('#toolbarresulSearch [data-action=listing]').on('click',{ list: "S" },propertiesToList);
	
	$('#toolbarresulSearch [data-action=offert]').on('click',{ list: "B" },propertiesToList);
	
	$('#toolbarresulSearch [data-action=changeOverview]').on('click',changeView);
	
	$('#toolbarresulSearch [data-action=map]').on('click',function (){
		var containerMap=$('.mapContainerGrid');
		if(!containerMap.is(':visible')){
			containerMap.addClass('show');
			$('.gridContainerResult').addClass('conMap');
			$('#containerResult .page').each(function(index, element) {
				var height=$(element).height();
				console.debug(height/2,height);
                $(element).height(height/2);
            });
		}
		else{
			containerMap.removeClass('show');
			$('.gridContainerResult').removeClass('conMap');
			$('#containerResult .page').each(function(index, element) {
				var height=$(element).height();
				console.debug(height*2,height);
                $(element).height(height*2);
            });
		}
		
	});
	
	
	
	/*
	$('#toolbarresulSearch .indicadores').html('Results 1 to '+
		((SysRt.globalVar.result.ResultApro>SysRt.globalVar.result.perPage)?
			SysRt.globalVar.result.perPage:
			SysRt.globalVar.result.properties.length)
			+' of about '+
			(SysRt.globalVar.result.ResultAproShow)
		);*/
		
	SysRt.globalVar.result.map=new XimaMap(
		'SysRt.globalVar.result.map',
		'SysRt.globalVar.result.map_search_latlong',
		'SysRt.globalVar.result.map_control_mapa_div',
		'SysRt.globalVar.result.map_pan',
		'SysRt.globalVar.result.map_draw',
		'SysRt.globalVar.result.map_poly',
		'SysRt.globalVar.result.map_clear',
		'SysRt.globalVar.result.map_maxmin',
		'SysRt.globalVar.result.map_circle');
	SysRt.globalVar.result.map._IniMAP(
		SysRt.globalVar.result.properties[0].latitude,
		SysRt.globalVar.result.properties[0].longitude
	);
	
		
	if(SysRt.globalVar.result.ResultApro<=SysRt.globalVar.result.perPage){
		$('.paginationSearch').hide();
	}
	$("[title]").tipTip();
	
	if(!SysRt.globalVar.result.isAddress)
		SysRt.globalVar.result.properties=filterProperties(SysRt.globalVar.result.properties);
	makePage(1);
	moveToPage(1);
	
	
</script>