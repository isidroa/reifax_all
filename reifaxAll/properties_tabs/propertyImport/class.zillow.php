<?php
/*************************
Classes Designed To Manage The Zillow Spider

Author: Jesus Marquez

modified by isidro 
*************************/
//include_once("class.realtor.php");
class Zillow extends import {
	
	//Variables
	var $countyProxy     =  'newReifax';   
	var $aditional		=	array();
	var $images			=	array();
	var $address		=	"";
	var $idServer		=	"";
	var $county			=	"";
	var $price			=	"";
	var $status			=	"";
	var $street			=	"";
	var $city			=	"";
	var $state			=	"";
	var $zip			=	"";
	var $userid			=	"";
	var $unit			=	"";
	var	$htmlDetail		=	"";
	var $latitude		=	"";
	var $longitude		=	"";
	var $remark			=	"";
	var $listingDate	=	"";
	var $entryDate		=	"";
	var $salesHistoryData=	"";
	var $idMultiple		=	"";
	var $urlCounty		=	"";
	var $urlDetail		=	"";
	var $factorLat		=0.016;
	var	$factorMin		=0.016;
    var $conMaster      =   NULL;
    var $conReifax      =   NULL;
    var $managerParseo  =   NULL;
	
	/******************************
	*
	*	Constructor Function
	*
	*******************************/
	
	public function Zillow($options=array()){
        $this->idServer=$options['id'];
        $this->managerParseo=$options['managerParseo'];
        $this->conReifax=$options['conReifax'];
        $this->conMaster=$options['conMaster'];
		return true;
	}
	
	
	/******************************
	*
	*	Principal Search Process. Return the First Result
	*
	*******************************/
	function getPrincipalResult($options=array()){

		$options=array_merge(
			array(
				'urlDirect'=>'',
				'address'
			),
			$options
		);
		$options	=	(object)$options;
		
		if(empty($options->urlDirect)){
			$options->urlDirect	=	$this->constructUrl($options->address);
		}
        $this->stratSession(array('CURLOPT_FOLLOWLOCATION' => true, 'isMobile' => true, 'CURLOPT_TIMEOUT' =>250));
        $this->html	=	$this->getHtml(array('url'    =>  $options->urlDirect, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
		
		/*
		$xpathFirst		=	$this->managerParseo->getXpathHtml($this->html,false);
		
		$FirstResult=	$this->managerParseo->queryDom($xpathFirst,"//div[@class='selected-listing']//article[contains(@class, 'property-listing')]");
		
		
		
		foreach($FirstResult as $link){
			$this->zid	=	$link->getAttribute( 'id' );
			$this->zid	=	preg_replace('/\D/','',$this->zid);
		}
		$Url	=	'http://www.zillow.com/homedetails/'.$this->zid.'_zpid/';
		
		$html	=	$this->getHtml($Url);
		$this->htmlProperty=$html;
		
		$xpathFirst	=	$this->managerParseo->getXpathHtml($html,'/<!--{(.*)}-->/');
		$FirstResult=	$this->managerParseo->queryDom($xpathFirst,"//div[@id='pinfo-block']//div[@class='adr']");

		/*
		foreach($FirstResult as $link){
			echo $link->getAttribute( 'id' )."<br>";
			$nodes	=	$link->childNodes;
			foreach($nodes as $node) {
				foreach($node->attributes as $attrName => $attrNode) {
					if($attrName == 'href')
						$Url = "http://www.zillow.com".$attrNode->nodeValue;
					if($attrName == 'title')
						$this->address = $attrNode->nodeValue;
				}
			}
		}
		

		/*
		$TypeResult	=	$this->managerParseo->queryDom($xpathFirst,"//dt[contains(@class, 'prop-value')]//span[2]");

		foreach ( $TypeResult as $link ){
			$this->status	=	str_replace(":","",$link->nodeValue);
		}

		$PriceResult=	$this->managerParseo->queryDom($xpathFirst,"//dt[contains(@class, 'prop-value')]//span[3]");
		
		foreach ( $PriceResult as $link ){
			$this->price	=	str_replace(",","",str_replace("$","",$link->nodeValue));
		}

			
		if(trim($this->zid) == "" or empty($this->zid))
			return json_encode(array('url' => false, 'option' => 'Z'));
		else

			return json_encode(
					array(
						'url' 	=> base64_encode($Url), 
						'option' 	=> 'Z',
						'id'		=>	$this->zid,
						'aditional' 	=> array(
							'address' 	=> $this->address, 
							'status' 	=> $this->status, 
							'price' 	=> $this->price
						)
					)
			);
		 * */
		 
		return json_encode(array('url' => true, 'option' => 'T'));
	}
	/******************************
	*
	*	Principal Search Process. Return the First Result
	*
	*******************************/
	function getPrincipalResultMod($address){
		$url='http://www.zillow.com/homedetails/'.$id.'_zpid/?isJson=true';	
		$html	=	$this->codeHtml($url,false);
		$xpathFirst	=	$this->managerParseo->getXpathHtml($html,'/<!--{(.*)}-->/');
		$FirstResult=	$this->managerParseo->queryDom($xpathFirst,"//div[@id='pinfo-block']//div[@class='adr']");

		foreach($FirstResult as $link){
			//echo $link->getAttribute( 'id' )."<br>";
			$nodes	=	$link->childNodes;
			foreach($nodes as $node) {
				foreach($node->attributes as $attrName => $attrNode) {
					if($attrName == 'href')
						$Url = "http://www.zillow.com".$attrNode->nodeValue;
					if($attrName == 'title')
						$this->address = $attrNode->nodeValue;
				}
			}
		}
		
		
		$TypeResult	=	$this->managerParseo->queryDom($xpathFirst,"//div[@id='pinfo-block']//span[@class='type']");

		foreach ( $TypeResult as $link ){
			$this->status	=	str_replace(":","",$link->nodeValue);
		}

		$PriceResult=	$this->managerParseo->queryDom($xpathFirst,"//div[@id='pinfo-block']//span[@class='price']");
		
		foreach ( $PriceResult as $link ){
			$this->price	=	str_replace(",","",str_replace("$","",$link->nodeValue));
		}
		
		if(trim($Url) == "" or $this->address == "" or empty($Url))
			return json_encode(array('url' => false, 'option' => 'Z'));
		else
			return json_encode(array('url' => base64_encode($Url), 'option' => 'Z', 'aditional' => array('address' => $this->address, 'status' => $this->status, 'price' => $this->price)));
	}
	
	/******************************
	*
	*	beginning shit for isidro
	*
	*******************************/
	
	function getListResult($address,$options){
		$Url	=	"";
		$urlOptions='/';
		
		if($_POST['propertyType']){
			$urlOptions.=$_POST['propertyType']."_type/";
		}
		if($_POST['propertyStatus']){
			$urlOptions.=$_POST['propertyStatus']."/";
			switch($_POST['propertyStatus']){
				case('for_sale'):
					$urlOptions.='fsba,fsbo_lt/';
				break;
				case('for_rent'):
					
				break;
				case('make_me_move'):
					$urlOptions.='pmf,pf_pt/';
				break;
			}
		}
		if($_POST['beds']){
			$urlOptions.=$_POST['beds']."-_beds/";
		}
		if($_POST['baths']){
			$urlOptions.=$_POST['baths']."-_baths/";
		}
		if($_POST['min_price']!='' || $_POST['max_price']!='' ){
			$urlOptions.=$_POST['min_price']."-".$_POST['max_price']."_price/";
		}
		if($_POST['min_SF']!='' || $_POST['max_SF']!='' ){
			$urlOptions.=$_POST['min_SF']."-".$_POST['max_SF']."_size/";
		}
		if($_POST['min_year']!='' || $_POST['max_year']!='' ){
			$urlOptions.=$_POST['min_year']."-".$_POST['max_year']."_built/";
		}
		$this->url='http://www.zillow.com/homes/'.str_replace(' ','-',$address).$urlOptions;
		$html	=	$this->codeHtml($this->url,false);
		
		
		$xpathFirst	=	$this->managerParseo->getXpathHtml($html,false);
		$dataResult	=	$this->managerParseo->queryDom($xpathFirst,"//div[@class='selected-listing'][0]");
		foreach ( $dataResult as $link ){
		   $temp_dom  = new DOMDocument();
		   $temp_dom->appendChild($temp_dom->importNode($link,true));
		   $aux =$temp_dom->saveHtml();
		   
		   //print_r($aux);
		   //die();
		   
		   $aux = str_replace('<div id="resurrection-page-state" class="template hide"><!--','', $aux);
		   $aux = '['.(str_replace('--></div>','', $aux)).']';
		   $aux = (str_replace('\\','', $aux));
		   $aux = strip_tags($aux);
		   $this->dataResult=json_decode($aux);
		}
		$FirstResult=	$this->managerParseo->queryDom($xpathFirst,"//article");
		$resultProperty=array();
		foreach ($FirstResult as $link){
		   $temp_dom  = new DOMDocument();
		   $temp_dom->appendChild($temp_dom->importNode($link,true));
		   
		   
		   
		   $property	=	$this->managerParseo->getXpathHtml($temp_dom->saveHtml(),false);
		   
		   $address			=	$this->managerParseo->queryDom($property,"//header[@class='property-header']");
		   foreach ( $address as $link ){
				$this->address	=	$link->nodeValue;
		   }
		   
		   $img			=	$this->managerParseo->queryDom($property,"//figure");
		   foreach ( $img as $link ){
				$this->img	=	$link->getAttribute( 'data-photourl' );
		   }
		   
		   
			$TypeResult	=	$this->managerParseo->queryDom($property,"//dt[contains(@class, 'type')]/strong");
	
			foreach ( $TypeResult as $link ){
				$this->status	=	str_replace(":","",$link->nodeValue);
			}
		   
		   
		   $price			=	$this->managerParseo->queryDom($property,"//dd[@class='price']/strong");
		   foreach ( $price as $link ){
				$this->price	=	$link->nodeValue;
		   }
		   
		   $beds			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Beds')]//following-sibling::dd[1]");
		   foreach ( $beds as $link ){
				$this->beds	=	$link->nodeValue;
		   }
		   
		   $baths			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Baths')]//following-sibling::dd[1]");
		   foreach ( $baths as $link ){
				$this->baths	=	$link->nodeValue;
		   }
		   
		   $sqft			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Sqft')]//following-sibling::dd[1]");
		   foreach ( $sqft as $link ){
				$this->sqft	=	$link->nodeValue;
		   }
		   
		   $lot			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Lot')]//following-sibling::dd[1]");
		   foreach ( $sqft as $link ){
				$this->lot	=	$link->nodeValue;
		   }
		   
		   $built			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Built')]//following-sibling::dd[1]");
		   foreach ( $built as $link ){
				$this->built	=	$link->nodeValue;
		   }
		   
		   $marke			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Days on Zillow')]//following-sibling::dd[1]");
		   foreach ( $marke as $link ){
				$this->marke	=	$link->nodeValue;
		   }
		   
		   $type			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Property type')]//following-sibling::dd[1]");
		   foreach ( $type as $link ){
				$this->type	=	$link->nodeValue;
		   }
		   
		   array_push($resultProperty,array(
					'address'	=> $this->address,
					'img'		=> $this->img,
					'price'		=> $this->price,
					'beds'		=> $this->beds,
					'baths'		=> $this->baths,
					'sqft'		=> $this->sqft,
					'lot'		=> $this->lot,
					'yb'		=> $this->built,
					'marke'		=> $this->marke,
					'type'		=> $this->type,
					'status'	=> $this->status
		   ));
		 }
		// return $resultProperty;
		return array(
			'numberResult' 	=> $this->dataResult,
			'properties'	=> $resultProperty,
			'isAddress'		=> true
			
		);
		
	}
	/******************************
	*
	*	beginning for isidro
	*
	*******************************/
	
	function getListResultMobile($address=NULL,$options=NULL,$url=NULL){
		if($url==NULL){
			$intoOptions=$options;
			$Url	=	"";
			$urlOptions='/';
			$propertyType=array(
				'0'		=>	'11111',
				'house' => '10000',
				'apartment_condo' => '01000',
				'duplex' => '00100',
				'mobile' => '00010',
				'land' => '00001'
			);
			$status='101011';
			$lt='11110';
			
			if($_POST['propertyStatus']){
				switch($_POST['propertyStatus']){
					case('for_sale'):
						$urlOptions.='fsba_lt/0_mmm/';
						$status='100000';
						$lt='10000';
					break;
					case('for_rent'):
						$status='000010';
						$lt='11000';
						
					break;
					case('make_me_move_1'):
						$_POST['propertyStatus']='make_me_move';
						$urlOptions.='pmf_pt/';
						$status='000001';
						$lt='10000';
						$pmf=1;
					break;
					case('make_me_move_2'):
						$_POST['propertyStatus']='make_me_move';
						$urlOptions.='pf_pt/';
						$status='000001';
						$lt='10000';
						$pf=1;
					break;
					case('recently_sold'):
						$status='001000';
						$lt='11000';
					break;
				}
				$Url.=$_POST['propertyStatus']."/";
			}
			if($options['propertyType']){
				$urlOptions.=$_POST['propertyType']."_type/";
			}
			if($options['beds']){
				$urlOptions.=$_POST['beds']."-_beds/";
			}
			if($options['baths']){
				$urlOptions.=$options['baths']."-_baths/";
			}
			if($options['min_price']!='' || $options['max_price']!='' ){
				$urlOptions.=$_POST['min_price']."-".$_POST['max_price']."_price/";
			}
			if($options['min_SF']!='' || $options['max_SF']!='' ){
				$urlOptions.=$options['min_SF']."-".$options['max_SF']."_size/";
			}
			if($_POST['min_year']!='' || $_POST['max_year']!='' ){
				$urlOptions.=$options['min_year']."-".$options['max_year']."_built/";
			}
			$this->url='http://www.zillow.com/homes/'.$Url.str_replace(' ','-',$address).$urlOptions;
			
		}
		else{
			$returnComparable=true;
			$this->url=$url;
		}
		$html	=	$this->codeHtml($this->url,false);
		$html=preg_replace("/:\n+/",":",$html);

		/*preg_match_all("/-\d+\.\d+, \d+\.\d+, -\d+\.\d+, \d+\.\d+/i", $html, $coordenadas);
		$coordenadas=str_replace(".","",$coordenadas[0][0]);
		$coordenadas=preg_replace("/\s/","",$coordenadas);
		$coordenadas=explode(',',$coordenadas);*/
		
		
		if(preg_match_all("/{ \"pageStateString\".*}/", $html, $filters)==false){
			$this->recordError('fallo al encontrar el objeto pageStateString',__LINE__,1);
			return false;
		}
		
		$filters=str_replace("\\","",$filters[0][0]);
		
		
		
		$filters=preg_replace("/, \"sortControl\":.*\"/","",$filters);
		
		
		$filters=preg_replace("/, \"disambig\":.*\"/","",$filters);
		
		
		$urlFilters=json_decode($filters);
		
		
		/******************************************
		*
		*	SI SE ENCUENTRA SE VERIFICAN LAS VARIABLES A USAR
		*
		*********************************************/
		foreach(array('mapState','regionSelectionObject','filterState','binCounts') as $i => $k){
			if(!property_exists($filters, $k)){
				$this->recordError("fallo al encontrar $k dentro del json de pageStateString ",__LINE__,2);
			}
		}
		
		$coordenadas[3]=$urlFilters->mapState->boundingRect->ne->lat;
		$coordenadas[2]=$urlFilters->mapState->boundingRect->ne->lon;
		$coordenadas[1]=$urlFilters->mapState->boundingRect->sw->lat;
		$coordenadas[0]=$urlFilters->mapState->boundingRect->sw->lon;
		$zoom = $urlFilters->mapState->zoomLevel;
		
		foreach($coordenadas as $i => $v){
			$coordenadas[$i]=$this->setLatLng($v);
		}
		
		$options=NULL;
		$options=$urlFilters->filterState;
		$options->rid	=$urlFilters->regionSelectionObject->regionId;
		$options->rt	=$urlFilters->regionSelectionObject->regionType;
		$options->rect  = $coordenadas[0].','.$coordenadas[1].','.$coordenadas[2].','.$coordenadas[3];
		$options->zoom	= $zoom;
		
		$this->totalresult=$urlFilters->binCounts->totalResultCount;
		
		$filters=json_encode($urlFilters);
		
		
		
		
		setcookie("searchFilter", $filters, 0, "/");
		
		$data=$this->getListJsonStandar($options,1,$returnComparable);
		
		$respuesta['url1']			=$this->url;
		$respuesta['url12']			=$data['url'];
		$respuesta['current']		=$data['current'];
		$respuesta['numberResult']	=$data['numberResult'];
		$respuesta['pages']			=$data['pages'];
		$respuesta['filters']		=$filters;
		$respuesta['properties']	=$data['properties'];
		
		return $respuesta;
		
		if($respuesta['numberResult']){
			return $respuesta;
		}
		else{
			return $this->getListResult($address,$intoOptions);
		}
		
		
	}
	
	function getListJson($options=NULL,$page=NULL,$returnComparable=FALSE){
		global $keys;
		
		$urlQuery='';
		foreach($options as $index => $value){
			$urlQuery.=($urlQuery=='')?$index.'='.$value:'&'.$index.'='.$value;
		}
		$url='http://www.zillow.com/search/GetMobileResults.htm?'.$urlQuery.'&p='.$page;
		
		$html=file_get_contents($url);
		
		$statusVar=array(
			'ForSale' 	=> 'AA',
			'ForRent'		=> 'BB',
			'RecentlySold'	=> 'CS',
			'Zestimate'	=> 'CC',
			'Other'		=> 'CC'
		);
		$typePro=array(
			'Condo' 		=> 'Condo/Apartament',
			'Apartment'		=> 'Condo/Apartament',
			'Townhouse'		=> 'Condo/Apartament',
			'Single Family'	=> 'Single Family',
			'Multi Family'	=> 'multy Family',
			'Lost'			=> 'Lost/Land',
			'Land'			=> 'Lost/Land',
		);
		$typeProC=array(
			'Condo' 		=> '04',
			'Apartment'		=> '04',
			'Townhouse'		=> '04',
			'Cooperative'	=> '04',
			'Single Family'	=> '01',
			'Multi Family'	=> '08',
			'Lost'			=> '00',
			'Land'			=> '00'
		);
		//
		$values=$valuesresi=$valuesren=''; 
		$resultProperty=array();
		
		foreach ($data['results'] as $index => $val){
			
			$aux= encrypt($val['href'],$keys['zillow']);
			$this->price=explode(' ',trim($this->price));
			$this->price=preg_replace("/\D/",'', $this->price[0]);
			$address=$val['ad']['st'].' '.$val['ad']['ut'].', '.$val['ad']['cy'].' '.$val['ad']['zc'];
			$this->parcelid=md5($address);
			$this->saveImgTem($val['il']);
			array_push($resultProperty,array(
						//'urlSearch'	=> $val['hs'][],
						'address'	=> $address,
						'idlocal'	=> $this->parcelid,
						'img'		=> $this->images,
						'price'		=> $this->price,
						'beds'		=> $val['be'],
						'baths'		=> $this->baths,
						'sqft'		=> $this->sqft,
						'lot'		=> $this->sqft,
						'yb'		=> $this->built,
						'marke'		=> $val['do'],
						'href'		=> $aux,
						'type'		=> $val['ht'],
						'id'		=> $val['id'],
						'status'	=> $val['fgmt'],
						'log'		=> $this->longitude,
						'lat'		=> $this->latitude,
						'imgC'		=> $val['pc']
			   ));
			   
				switch($val['ht']){
					case	"Single Family":		$typePT	=	"01";	break;
					case	"Townhouse":			$typePT	=	"04";	break;
					case	"Condo":				$typePT	=	"04";	break;
					case	"Cooperative":			$typePT	=	"04";	break;
					case	"Multi Family":			$typePT	=	"08";	break;
					case	"Vacant Land":			$typePT	=	"00";	break;
					case	"Mobile / Manufactured":$typePT	=	"02";	break;
					default:						$typePT	=	"99";	break;
				}
			   
			   $values.=($values==''?'':',')." (
				MD5('{$address}'),
				'{$address}',
				'{$val['ad']['st']}',
				'{$val['ad']['ut']}',
				'{$val['ad']['cy']}',
				'{$val['ad']['zc']}',
				'{$val['ad']['et']}',
				'{$statusVar[$val['fgmt']]}',
				'{$typePro[$val['ht']]}',
				'{$typePT}',
				'{$this->latitude}',
				'{$this->longitude}',
				NOW(),
				'S'
				)";
				$this->sqft=str_replace(',','',$this->sqft);
				$this->price=str_replace(',','',$this->price);
				$this->price=str_replace('$','',$this->price);
				$this->sqft=$this->sqft==''?'0':$this->sqft;
				$this->built=$this->built==''?'0':$this->built;
				$this->price=$this->price==''?'0':$this->price;
				$val['be']=$val['be']==''?'0':$val['be'];
				if(preg_match('/\w/',$this->price)){
					$this->price='0';
				}
				
			   $valuesps.=($valuesps==''?'':',')." (
				MD5('{$address}'),
				'{$val['be']}',
				'{$this->baths}',
				{$this->sqft},
				'{$this->built}'
				)";
				
			if($val['fgmt']=='ForSale'){
			   $valuesresi.=($valuesresi==''?'':',')." (
				MD5('{$address}'),
				'{$val['be']}',
				'{$this->baths}',
				'{$val['ad']['cy']}',
				'{$this->sqft}',
				'{$this->price}',
				'{$typePT}',
				'{$this->built}',
				'{$val['ad']['zc']}'
				)";
			}
				
			if($val['fgmt']=='ForRent'){
			   $valuesren.=($valuesren==''?'':',')." (
				MD5('{$address}'),
				'{$address}',
				'{$this->baths}',
				'{$val['be']}',
				'{$val['ad']['cy']}',
				'{$this->latitude}',
				'{$this->longitude}',
				'{$this->price}',
				'{$this->sqft}',
				'{$val['ad']['et']}',
				'{$val['ad']['st']}',
				'{$this->built}',
				'{$val['ad']['zc']}',
				'{$val['ad']['ut']}'
				)";
			}
		 }
		 if(count($resultProperty)>0){
			 $sql="	INSERT IGNORE  INTO `temp_properties`
						(`parcelid`, `addressfull`, `address`, `unit`, `city`, `zip`, `state`, `status`, `CCodeD`, `xcode`, `latitude`, `longitude`, `regdate`,`statusData`)
					VALUES
						$values
			;";
			mysql_query($sql) or die($sql.mysql_error());
			$sql="INSERT IGNORE INTO temp_psummary  (`parcelid`, `beds`, `bath`, `lsqft`, `yrbuilt`)
					VALUES
						$valuesps";
			mysql_query($sql) or die($sql.mysql_error());
			if($valuesresi!=''){
				$sql="INSERT IGNORE INTO `temp_mlsresidential`
						(`parcelid`, `Bath`, `Beds`, `City`, `lotsize`, `Lprice`, `Xcode`, `Yrbuilt`, `Zip`)
					VALUES
						$valuesresi
					";
				mysql_query($sql) or die($sql.mysql_error());
			}
			if($valuesren!=''){
				$sql="INSERT IGNORE INTO `temp_rental`
					(`parcelid`, `Address`, `Bath`, `Beds`, `City`, `Latitude`, `Longitude`, `Lprice`, `Lsqft`, `State`,  `Street`, `Yrbuilt`, `Zip`, `Unit`)
					VALUES
						$valuesren
					";
				mysql_query($sql) or die($sql.mysql_error());
			}			
		 }
		return array(
			'url' 	=> $url,
			'numberResult' 	=> $data['total'],
			'current' 		=> $data['pagination']['currentPage'],
			'pages' 		=> $data['pagination']['totalPages'],
			'isAddress'		=> true,
			'idTru'			=> false,
			'properties'	=> $resultProperty
		);
		
	}
	
	
	
	function getListJsonStandar($options=NULL,$page=NULL,$returnComparable=FALSE){
		global $keys;
		
		$statusVar=array(
			'For Sale' 	=> 'AA',
			'For Rent'		=> 'BB',
			'RecentlySold'	=> 'CS',
			'Zestimate'	=> 'CC',
			'Other'		=> 'CC'
		);
		$typePro=array(
			'Condo' 		=> 'Condo/Apartament',
			'Apartment'		=> 'Condo/Apartament',
			'Townhouse'		=> 'Condo/Apartament',
			'Single Family'	=> 'Single Family',
			'Multi Family'	=> 'multy Family',
			'Lost'			=> 'Lost/Land',
			'Land'			=> 'Lost/Land'
		);
		$typeProC=array(
			'Condo' 		=> '04',
			'Apartment'		=> '04',
			'Townhouse'		=> '04',
			'Cooperative'	=> '04',
			'Single Family'	=> '01',
			'Multi Family'	=> '08',
			'Lost'			=> '00',
			'Land'			=> '00'
		);
		$values=$valuesresi=$valuesren=$urlQuery='';
		
		foreach($options as $index => $value){
			$urlQuery.=($urlQuery=='')?$index.'='.$value:'&'.$index.'='.$value;
		}

		$resultProperty=array();
		$urlBase='http://www.zillow.com/search/GetResults.htm?'.$urlQuery.'&search=maplist';
		
		$this->curl_multi();
		
		
		do {
			
			$url = $urlBase.'&p='.$page;
			
			$this->addRequestCurl($url);
			
			//$html=$this->codeHtml($url);
			 $page++;
		} while($returnComparable && $page<=$data['list']['numPages']);
		$this->exectCurl();
		
		
		foreach($this->htmlRquestCurl as $k => $v){
			$data=json_decode($v,true);
			//print_r($data);
			
			/* extrating form html */
			//print_r($data['list']['listHTML']);
			
			
			$xpathFirst	=	$this->managerParseo->getXpathHtml($data['list']['listHTML'],false);
			
			$FirstResult=	$this->managerParseo->queryDom($xpathFirst,"//article");
			
			foreach ($FirstResult as $link){
			   $temp_dom  = new DOMDocument();
			   $temp_dom->appendChild($temp_dom->importNode($link,true));
			   
			   
			   /*extract lat*/
			   if($this->latitude = $this->setLatLng($link->getAttribute( 'latitude' ))){
					$this->recordError("fallo al encontrar latitude",__LINE__,2);
			   }
			   
			   
			   
			   /*extract log*/
			   if($this->longitude = $this->setLatLng($link->getAttribute( 'longitude' ))){
					$this->recordError("fallo al encontrar longitude",__LINE__,2);
			   }
			   
			   
			   
			   /* zid */
			   if($this->zid = preg_replace('/\D/','',$link->getAttribute( 'id' ))){
					$this->recordError("fallo al encontrar id",__LINE__,2);
			   }
			   
			   $htmlProperty=$temp_dom->saveHtml();
			   
			   
			   if(preg_match_all('/\[.*\]/',$htmlProperty,$dataArray)){
					$this->recordError("fallo al encontrar el array contenedor del precio, imagen, baños, cuartos, sqft",__LINE__,2);
			   }
			   
			  // print_r($dataArray);
			   $dataArray=json_decode(str_replace('\\','/',$dataArray[0][0]),true);
			   /*extract price*/
			   
			   $this->price= preg_replace('/\D/','',$dataArray[0]);
			   /*extract image*/
			   
			   $this->saveImgTem($dataArray[1]);
			   
			   /*extract baths*/
			   $this->baths=preg_replace('/\D/','',$dataArray[2]);
			   
			   /*extract beds*/
			   $this->beds=explode('.',$dataArray[3]);
			   
			   /*extract beds*/
			   $this->beds= preg_replace('/\D/','',$this->beds[0]);
			   
			   /*extract sqft*/
				$this->sqft=preg_replace('/\D/','',$dataArray[4]);
			   
			   
			   /*************
			   
			   extraccion del html
			   
			   ************/
			   $property	=	$this->managerParseo->getXpathHtml($htmlProperty,false);
			   
				$address			=	$this->managerParseo->queryDom($property,"//a");
				if($address->length){
				   foreach ( $address as $link ){
					   if($link->getAttribute('title')){
						$this->address	=	str_replace('Real Estate','',$link->getAttribute('title'));
						$this->address	=	trim($this->address);
						$this->parcelid	= 	md5($this->address);
					   }
				   }
				}
				else{
					$this->recordError("fallo al encontrar la direccion",__LINE__,1);
				}
				
				$TypeResult	=	$this->managerParseo->queryDom($property,"//dt[contains(@class, 'type')]/strong");
				if($TypeResult->length){
					foreach ( $TypeResult as $link ){
						$this->status	=	str_replace(":","",$link->nodeValue);
					}
				}
				else{
					$this->recordError("fallo al encontrar el status de la propiedad",__LINE__,2);
				}
			   
			  
				$built			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Built')]//following-sibling::dd[1]");
				if($built->length){
				   foreach ( $built as $link ){
						$this->built	=	$link->nodeValue;
				   }
				}
				else{
					$this->recordError("fallo al encontrar el año de contruccion de la propiedad",__LINE__,2);
				}
				
				$marke			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Days on Zillow')]//following-sibling::dd[1]");
				if($marke->length){
				   foreach ( $marke as $link ){
						$this->marke	=	$link->nodeValue;
				   }
				}
				else{
					$this->recordError("fallo al encontrar los dias en el mercado",__LINE__,2);
				}
				
				$type			=	$this->managerParseo->queryDom($property,"//dt[contains(.,'Property type')]//following-sibling::dd[1]");
				if($type->length){
				   foreach ( $type as $link ){
						$this->type	=	trim($link->nodeValue);
				   }
				}
				else{
					$this->recordError("fallo al encontrar el typo de propiedad",__LINE__,2);
				}
				
				// fn del sphider
				$this->parcelid=md5($this->address);
				
				array_push($resultProperty,array(
					'idlocal'	=> $this->parcelid,
					'address'	=> $this->address,
					'img'		=> $this->images,
					'price'		=> $this->price,
					'beds'		=> $this->beds,
					'baths'		=> $this->baths,
					'sqft'		=> $this->sqft,
					'lot'		=> $this->sqft,
					'yb'		=> $this->built,
					'marke'		=> $this->marke,
					'type'		=> $this->type,
					'status'	=> $this->status,
					'latitude'  => $this->latitude,
					'longitude' => $this->longitude
				));
			 
				 /*+++
				 
				 save data temporal
				 
				 **********************/
				 
				switch($this->type){
					case	"Single Family":		$typePT	=	"01";	break;
					case	"Townhouse":			$typePT	=	"04";	break;
					case	"Condo":				$typePT	=	"04";	break;
					case	"Cooperative":			$typePT	=	"04";	break;
					case	"Multi Family":			$typePT	=	"08";	break;
					case	"Vacant Land":			$typePT	=	"00";	break;
					case	"Mobile / Manufactured":$typePT	=	"02";	break;
					default:						$typePT	=	"99";	break;
				}
				 
				   
				   $values.=($values==''?'':',')." (
					'{$this->parcelid}',
					'{$this->address}',
					'',
					'',
					'',
					'',
					'',
					'{$statusVar[$this->status]}',
					'{$typePro[$this->type]}',
					'{$typePT}',
					'{$this->latitude}',
					'{$this->longitude}',
					NOW(),
					'S',
					{$this->zid}
					)";
					$this->sqft=str_replace(',','',$this->sqft);
					$this->sqft=$this->sqft==''?'0':$this->sqft;
					$this->built=$this->built==''?'0':$this->built;
					$this->price=$this->price==''?'0':$this->price;
					$this->beds=$this->beds==''?'0':$this->beds;
					if(preg_match('/\w/',$this->price)){
						$this->price='0';
					}
					
				   $valuesps.=($valuesps==''?'':',')." (
					'{$this->parcelid}',
					'{$this->beds}',
					'{$this->baths}',
					{$this->sqft},
					'{$this->built}'
					)";
					
				if(in_array($this->status,array('For Sale','House For Sale','Condo For Sale'))){
				   $valuesresi.=($valuesresi==''?'':',')." (
					'{$this->parcelid}',
					'{$this->beds}',
					'{$this->baths}',
					'',
					'{$this->sqft}',
					'{$this->price}',
					'{$typePT}',
					'{$this->built}',
					''
					)";
				}
					
				if(in_array($this->status,array('ForRent','For Rent'))){
				   $valuesren.=($valuesren==''?'':',')." (
					'{$this->parcelid}',
					'{$address}',
					'{$this->baths}',
					'{$this->beds}',
					'{$val['ad']['cy']}',
					'{$this->latitude}',
					'{$this->longitude}',
					'{$this->price}',
					'{$this->sqft}',
					'{$val['ad']['et']}',
					'{$val['ad']['st']}',
					'{$this->built}',
					'{$val['ad']['zc']}',
					'{$val['ad']['ut']}'
					)";
				}
			 
			 }
			 /*+++
			 
			 end save
			 
			 **********************/
		}
			 
		
		 if(count($resultProperty)>0){
			 $sql="	INSERT IGNORE  INTO `temp_properties`
						(`parcelid`, `addressfull`, `address`, `unit`, `city`, `zip`, `state`, `status`, `CCodeD`, `xcode`, `latitude`, `longitude`, `regdate`,`statusData`,`zid`)
					VALUES
						$values
			;";
			mysql_query($sql) or die($sql.mysql_error());
			$sql="INSERT IGNORE INTO temp_psummary  (`parcelid`, `beds`, `bath`, `lsqft`, `yrbuilt`)
					VALUES
						$valuesps";
			mysql_query($sql) or die($sql.mysql_error());
			if($valuesresi!=''){
				$sql="INSERT IGNORE INTO `temp_mlsresidential`
						(`parcelid`, `Bath`, `Beds`, `City`, `lotsize`, `Lprice`, `Xcode`, `Yrbuilt`, `Zip`)
					VALUES
						$valuesresi
					";
				mysql_query($sql) or die($sql.mysql_error());
			}
			if($valuesren!=''){
				$sql="INSERT IGNORE INTO `temp_rental`
					(`parcelid`, `Address`, `Bath`, `Beds`, `City`, `Latitude`, `Longitude`, `Lprice`, `Lsqft`, `State`,  `Street`, `Yrbuilt`, `Zip`, `Unit`)
					VALUES
						$valuesren
					";
				mysql_query($sql) or die($sql.mysql_error());
			}			
		 }
		return array(
			'url' 	=> $url,
			'numberResult' 	=> $this->totalresult,
			'current' 		=> $data['pagination']['currentPage'],
			'pages' 		=> $data['pagination']['totalPages'],
			'properties'	=> $resultProperty
		);
	}
	
	
	function getPropertyJson($id){
		$url='http://www.zillow.com/homedetails/'.$id.'_zpid/?isJson=true';		
		$html=$this->codeHtmlmobile($url,false);
		$data=json_decode($html,true);
		$this->images=$data['header']['photos'];
		return $data;
	}
	
	/******************************
	*
	*	ending shit for isidro
	*
	*******************************/
	
	/******************************
	*
	*	Second Process To Search and Extract the Url Detail
	*
	*******************************/
	function getUrlDetail($url,$parcelid=NULL,$zid){
		
		$url="http://www.zillow.com/homedetails/{$zid}_zpid/";
		
		$this->urlDetail	=	base64_encode($url);
		
		$html				=	$this->codeHtml($url,false);
		

		
		
		$this->parcelid=$parcelid;

		////Obtain de URL for Price History
		$this->priceHistory($html);
		
		


		//REPLAY THIS CODE FOR EXCEPTION
		//We get the code block comments from Zillow that seem this json format

		preg_match_all('/\n\[(.*)]/',$html,$addr);
		
		foreach($addr[0] as $value){
			$value	=	trim(str_replace("\"","",str_replace("'","",str_replace("\n","",str_replace("]","",str_replace("[","",$value))))));
			if(!empty($value))
				$aux[]	=	explode(",",$value);
		}
		unset($tempValues);
		foreach($aux	as $row){
			if(trim($row[2])	==	"City")
				$tempValues->city		=	trim($row[3]);
			if(trim($row[2])	==	"Address")
				$tempValues->street		=	trim($row[3]);
			if(trim($row[2])	==	"County")
				$tempValues->county		=	trim($row[3]);
			if(trim($row[2])	==	"State")
				$tempValues->state	=	trim($row[3]);
			if(trim($row[2])	==	"Zip")
				$tempValues->zip		=	trim($row[3]);
		}
		$contentTable	=	$this->managerParseo->getXpathHtml($this->htmlProperty,false);
		/**********
		*	GET IMAGES
		*****************************/
		$img		=	$this->managerParseo->queryDom($contentTable,"//div[@id='home-image-preview']//img");	
		$this->images=array();
		foreach ( $img as $link ){
			$urlImg=$link->getAttribute( 'src' );
			if($urlImg==''){
				$urlImg=$link->getAttribute( 'href' );
			}
			array_push($this->images,array('url' =>$urlImg));
		}
		$this->urlImages($parcelid);
		///// Obtain the Status
		
		$results		=	$this->managerParseo->queryDom($contentTable,"//div[contains(@class, 'prop-value')]//span[2]");	
		if($results->length	>	0){
			$this->status	=	str_replace(':','',$results->item(0)->nodeValue);
		}
		///// Obtain the Street Address
		$results		=	$this->managerParseo->queryDom($contentTable,"//h1[@class='prop-addr']");	
		if($results->length	>	0){
			$this->street	=	explode(',',str_replace(':','',$results->item(0)->nodeValue));
			$this->address	=	$results->item(0)->nodeValue;
			$this->street	=	$this->street[0];
		}
		/*
		else{
			$results		=	$this->managerParseo->queryDom($contentTable,"//h1[@property='prop-addr']");
			if($results->length	>	0)
				$this->street	=	$results->item(0)->getAttribute('content');
			else
				$this->street	=	$tempValues->street;
		}*/
		
		
		if(!preg_match_all('/\(\d{3}\)\s\d{3}-\d{4}/',$html,$addr)){
			$this->recordError("fallo al encontrar los nuemeros telefonicos",__LINE__,2);
		}
		else{
			$this->agentPhone=$addr[0][0];
		}
		
		/**************
		**
			STATUS AND PRICE
		*
		**********************/
		if(!preg_match_all('/\w*\s*\w*:?\s*<\/\w*><\w*[^>]*>\$\d*,?\d*<\/\w*>/',$html,$addr)){
			$this->recordError("fallo al encontrar el status y el precio",__LINE__,2);
		}
		else{
			
			preg_match_all('/<\w*[^>]*>/',$addr[0][0],$tem);
			$tem=(str_replace('/','',$tem[0][0])).$addr[0][0];
			preg_match_all('/<\w*[^>]*>(.*?)<\/\w*>/',$tem,$tem);
			$this->aditional['status']=preg_replace('/\W/','',$tem[1][0]);
			$this->aditional['price']=preg_replace('/\D/','',$tem[1][1]);
		}
		
		
		/**********
		days on marked
		**************/
		if(!preg_match_all('/Days on Zillow<\/\w*><\w*[^>]*>(.*?)<\/\w*>/',$html,$addr)){
			$this->recordError("fallo al encontrar los dias en el mercado",__LINE__,2);
		}
		else{
			$this->dom	=	preg_replace("/\D/",'',$addr[1][0]);
			$this->dom	=	intval($this->dom);
			if($this->dom){
				$fecha = date(DATE_RFC822,time ());
				$nuevafecha = strtotime ( '-'.$this->dom.' day') ;
				$this->listingDate = date ( 'm/d/Y' , $nuevafecha );
			}
		}
		///// Obtain the Remark
		$results		=	$this->managerParseo->queryDom($contentTable,"//div[@class='prop-mod']//div");
		//echo "<pre>".str_replace('…More Less',"",(string)$results->item(0)->nodeValue)."</pre>";
		$aux	=	trim(utf8_encode($results->item(0)->nodeValue));
		if(strpos($aux,"â¦MoreÂ") !== false)
			$aux	=	substr($aux,0,strlen($aux)-18);
		//echo "<pre>".str_replace("â¦MoreÂ Less","",utf8_encode($results->item(0)->nodeValue))."</pre>";
		//preg_match_all('/.*"(.*)"/',$results->item(0)->nodeValue,$aux);
		$this->remark	=	str_replace("View Virtual Tour (opens new tab)","",$aux);
		
		///// Obtain the City
		$results		=	$this->managerParseo->queryDom($contentTable,"//li[@id='region-city']");
		if($results->length	>	0)
			$this->city	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->managerParseo->queryDom($contentTable,"//meta[@property='og:locality']");
			if($results->length	>	0)
				$this->city		=	$results->item(0)->getAttribute('content');
			else
				$this->city		=	$tempValues->city;
		}
		
		///// Obtain the State
		$results		=	$this->managerParseo->queryDom($contentTable,"//li[@id='region-state']");
		if($results->length	>	0)
			$this->state	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->managerParseo->queryDom($contentTable,"//meta[@property='og:region']");
			if($results->length	>	0)
				$this->state	=	$results->item(0)->getAttribute('content');
			else
				$this->state	=	$tempValues->state;
		}

		///// Obtain the Zip
		$results	=	$this->managerParseo->queryDom($contentTable,"//li[@class='region-zipcode']");
		if($results->length	>	0)
			$this->zip	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->managerParseo->queryDom($contentTable,"//meta[@property='og:postal-code']");
			if($results->length	>	0)
				$this->zip		=	$results->item(0)->getAttribute('content');
			else
				$this->zip		=	$tempValues->zip;
		}
		
		///// Obtain the Unit
		$results	=	$this->managerParseo->queryDom($contentTable,"//div[@class='extended-address']/div[@class='second-line']");
		if($results->length	>	0)
			$this->unit	=	$results->item(0)->nodeValue;
		else{
			$results	=	$this->managerParseo->queryDom($contentTable,"//span[@class='prop-addr-unit']");
			$this->unit	=	trim($results->item(0)->nodeValue);
		}

		///// Obtain the Latitude
		$results		=	$this->managerParseo->queryDom($contentTable,"//meta[@itemprop='latitude']");
		if($results->length	>	0)
			$this->latitude	=	$results->item(0)->getAttribute('content');
		else					
			$this->latitude	=	$this->LatLong($html,'latitude');
		
		///// Obtain the Longitude
		$results		=	$this->managerParseo->queryDom($contentTable,"//meta[@itemprop='longitude']");
		if($results->length	>	0)
			$this->longitude=	$results->item(0)->getAttribute('content');		
		else
			$this->longitude=	$this->LatLong($html,'longitude');
		


		
		preg_match_all('/{.*home-facts-comparison.*}/',$html,$addr);
		$tem=json_decode($addr[0][0]);
		$urlMorFacts	="http://www.zillow.com".$tem->cfg->blocks[0]->url;
		return $urlMorFacts;
	}
	
	/******************************
	*
	*	Obtain Detail from a Url. (Principal Search)
	*
	*******************************/
	function getDetailResult($html){
		$data=json_decode($html);
		echo '<br>';
		print_r($data);die();
		
		$html	=	str_replace("\\","",$html);
		$html	=	str_replace("th>","td>",$html);
		$html	=	str_replace("</td>","|</td>",$html);
		
		
		$contentTable	=	$this->managerParseo->getXpathHtml($html,false);
		$results		=	$this->managerParseo->queryDom($contentTable,"//div[@id='county-link']//a");
		if(!is_null($results->item(0)))
			$this->urlCounty=	$this->obtainCountyURL($results->item(0)->getAttribute('href'));
		
		$results	=	$this->managerParseo->queryDom($contentTable,"//tr[count(td)=2]");

		$detail	=	array();
		
		foreach ($results as $link){
			$temp_dom 	=	new DOMDocument();
			$temp_dom->appendChild($temp_dom->importNode($link,true));
			$detail[]	=	explode("|",trim(strip_tags(str_replace(":","",str_replace("&nbsp;"," ",$temp_dom->saveHtml())))));
			unset($temp_dom);
			//$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));	//OLD METHOD
		}
		//var_dump($detail);
		$results	=	$this->managerParseo->queryDom($contentTable,"//span[@class='locality']");
//		echo $results->getAttribute('class');
		foreach ($results as $link)
		{
				//echo $link->getAttribute('class');
			$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));
		}
		/*
		echo '<pre>';
		print_r($detail);
		echo '</pre>';die();*/
		return json_encode(array("result" => true, "data" => $this->map($detail)));
	}
	
	/******************************
	*
	*	Obtain Detail from a Url. (Principal Search)
	*
	*******************************/
	function getDetailResultWithoutAjax(){		
		//We get the code block comments from Zillow that seem this json format
		$html	=	$this->codeHtml($url,false);	
		preg_match_all('/\n\[(.*)]/',$this->htmlDetail,$addr);
		foreach($addr[0] as $value){
			$value	=	trim(str_replace("\"","",str_replace("'","",str_replace("\n","",str_replace("]","",str_replace("[","",$value))))));
			if(!empty($value))
				$aux[]	=	explode(",",$value);
		}
		foreach($aux	as $row){
			//echo "<pre>";print_r($row[2]);echo "</pre>";
			if(trim($row[2])	==	"City")
				$this->city		=	trim($row[3]);
			if(trim($row[2])	==	"Address")
				$this->street	=	trim($row[3]);
			if(trim($row[2])	==	"County")
				$this->county	=	trim($row[3]);
			if(trim($row[2])	==	"State")
				$this->state	=	trim($row[3]);
			if(trim($row[2])	==	"Zip")
				$this->zip		=	trim($row[3]);
		}
		//echo "<pre>";print_r($aux);echo "</pre>";

		//replacing all slashes from \ that interfere with the code
		$contentTable	=	str_replace('\\','',$addr[1][0]);
		$contentTable	=	str_replace('<td class="null">',"<td>|",$contentTable);
		$contentTable	=	$this->managerParseo->getXpathHtml($contentTable,false);
		
		$results		=	$this->managerParseo->queryDom($contentTable,"//div[@id='county-link']//a");
		if(!is_null($results->item(0)))
			$this->urlCounty=	$this->obtainCountyURL($results->item(0)->getAttribute('href'));
		
		$results	=	$this->managerParseo->queryDom($contentTable,"//tr[count(td)=2]");

		$detail	=	array();
		
		foreach ($results as $link)
		{
			$link->nodeValue;
			$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));
		}
		
		$results	=	$this->managerParseo->queryDom($contentTable,"//span[@class='locality']");
//		echo $results->getAttribute('class');
		foreach ($results as $link)
		{
				//echo $link->getAttribute('class');
			$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));
		}

		return json_encode(array("result" => true, "data" => $this->map($detail)));
	}
	
	

	/******************************
	*
	*	Get the Address to Search
	*
	*******************************/
	function constructUrl($search,$details=FALSE){
	
		$search		=	preg_replace("/\s{1,}/","-",$search);
		$search		=	str_replace(array("#"), "", $search);
		$principal	=	"http://www.zillow.com/".(($details)?'homedetails':'homes')."/".$search."_rb/";

		return $principal;
	}
	
	/******************************
	*
	*	Clear Any folder on the System
	*
	*******************************/
	function clearDirectory($carpeta){
		foreach(glob($carpeta."*") as $archivos_carpeta){
			if(!is_dir($archivos_carpeta)) unlink($archivos_carpeta);
		}
	}
	
	/******************************
	*
	*	Excecute any Query over Xpath
	*
	*******************************/
	function queryDom($xpath,$query){

		//We make the XPath Query
		$arrayToExplode	=	$xpath->query($query);
		
		return $arrayToExplode;
	}
	
	/******************************
	*
	*	Obtains the Sales History from some Properti on Zillow
	*
	*******************************/
	private function salesHistory($tableWithSales){
		$tempSales = array();$tempSalesCount = 0;
		foreach($tableWithSales as $field){
			if(trim($field[1])=="Sold"){
				$tempSales['data'][]	=	array(
												"txsales*date$tempSalesCount"		=>	$field[0], 
												"txsales*price$tempSalesCount"		=>	number_format(str_replace(",","",str_replace("$","",$field[2])),2,".",","), 
												"txsales*priceSqft$tempSalesCount"	=>	number_format(str_replace(",","",str_replace("$","",$field[4])),2,".",",")
											);
				$tempSalesCount++;
			}
		}
		//if $tempSalesCount is equal to 0, it's because there are not data and need send a empty array
		if($tempSalesCount	==	0)
			$tempSales['data'][]	=	array();

		$tempSales['total'] = $tempSalesCount;
		return	$tempSales;
	}

	/******************************
	*
	*	Obtains the Price History from Zillow
	*
	*******************************/
	private function priceHistory($html){
		
		preg_match_all('/asyncLoader.load((.*));/',$html,$addr);
		$json = array();
		foreach($addr[1] as $value)
				$json[] = json_decode(str_replace("(","",str_replace(")","",str_replace(");","",str_replace("asyncLoader.load(","",$value)))),true);
		
		//var_dump($json);
		$c=0;$priceHistory="";
		foreach($json as $key1 => $value1){
			//foreach($value1 as $key => $value){
				if($value1["phaseType"]	==	"scroll" and $value1["divId"]	==	"home-transactions"){
					//if($key=="ajaxURL"){
						//if($c==1)
						//if($c==2)
							//$priceHistory = "http://www.zillow.com".$value;
							$priceHistory = "http://www.zillow.com".$value1["ajaxURL"];
						$c++;
					//}
				}
			//}
		}
		//echo $priceHistory;
		if(!empty($priceHistory)){
			$html			=	$this->codeHtml($priceHistory,false);
			$html			=	str_replace("\\","",$html);
			$html			=	str_replace("</td>","|</td>",$html);
			$contentTable	=	$this->managerParseo->getXpathHtml($html,false);
			$results		=	$this->managerParseo->queryDom($contentTable,"//tr[count(td)=7]");
			
			$detail			=	array();
			foreach ( $results as $link )
			{
				$detail[] = explode("|",str_replace(":","",$link->nodeValue));
			}
			//var_dump($detail);
			$this->salesHistoryData	=	$this->salesHistory($detail);
			
			$detail = json_encode($detail);
			$detail = json_decode($detail,true);

			$ldate=""; $entryDate="";
			///Extract the Listing Date
			foreach($detail as $rows){
					//echo "<BR>|".$rows[1]."| ---- ".$rows[0];
					if(trim($rows[1]) == "Listed for sale"){
						$ldate=$rows[0];
						break;
					}
			}
			
			///Extract the Entry Date
			$c=0;
			foreach($detail as $rows){
					//echo "<BR>|".$rows[1]."| ---- ".$rows[0];
					if($c==0){
						$entryDate=$rows[0];
						break;
					}
			}
			if(empty($ldate))
				$ldate=$entryDate;
			
			$this->listingDate	=	empty($ldate)		?	""	:	date("Y/m/d",strtotime($ldate));
			$this->entryDate	=	empty($entryDate)	?	""	:	date("Y/m/d",strtotime($entryDate));
		}
	}

	
	

	/******************************
	*
	*	Extract Url Images from Zillow
	*
	*******************************/
		private function urlImages($xpath){
		$this->userid = $_COOKIE['datos_usr']['USERID'];
		$imagenes = '';
		$c=0;$ArrayImages=array();
		foreach ( $this->images as $img )
		{
			$ArrayImages[] = $img['url'];
			$c++;
		}

		$c=0;$ArrayImagesFinal=array();
		$parcelid=$this->parcelid;

		$dirImages=$this->verifyPathImg(false);
	
		
		if(!empty($this->idMultiple))	//Control for Multiple Import, saving all images in one folder for this case see propertyIncludedMultiple.php
			$identifyMultiple	=	$this->idMultiple."-";
		else{
			$identifyMultiple	=	"";
			//if not are multiple import need to clear this folders....
			//Clear all temp directories for new images on a new search
			$this->clearDirectory($dirImages."temp/");
			$this->clearDirectory($dirImages."thumb/");
		}
		foreach($ArrayImages as $index => $image){
			if(!empty($image)){
				$c++;
				$aux=explode(".",$image);
				$position=count($aux)-1;
				$this->saveImage($image,$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$imageG=str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$imageT=str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."thumb/$identifyMultiple$c.$aux[$position]");
				$this->images['temp'][] = $imageG;
				$this->images['thumb'][] = $imageT;
				//if(!empty($parcelid)){
					$sql="
						INSERT INTO `temp_imagenes` ( `parcelid`, `image`, `thumb`, `dateReg`)
						VALUES
						(
							'$parcelid',
							'$imageG',
							'$imageT',
							NOW()
						);
					";
					mysql_query($sql) or die ($sql.mysql_error());
				//}
				
			}
		}
		$this->createThumbs($dirImages."temp/",$dirImages."thumb/",96);
	}
	/******************************
	*
	*	Extract the Latitude and Longitude Fron Case Zillow
	*
	*******************************/
	private function LatLong($html,$type){
		//preg_match_all('/.*asyncLoader.load((.*));/',$html,$addr);	//Old Method
		preg_match_all('/asyncLoader.load((.*));/U',$html,$addr);
		$json	=	array();
		foreach($addr[1] as $value)
				$json[] = json_decode(str_replace("(","",str_replace(")","",str_replace(");","",str_replace("asyncLoader.load(","",$value)))),true);
		
		foreach($json as $row){
			foreach($row as $index => $value){
				if($index	==	$type)
					return	$value;
			}
		}
			
		return	"";
	}
	
	/******************************
	*
	*	Mapea the data to the correct fields
	*
	*******************************/
	private function map($data){
		/**************************
		*	Order Data Recibed
		**************************/
		$standarData = array(
			'parcel #' => 'parcel', 'beds' => 'beds', 'baths' => 'baths', 'mls #' => 'mls', 'sqft' => 'sqft', 'lot' => 'lot', 'unit' => 'unit', 'type' => 'type', 'pool' => 'pool', 
			'waterfront' => 'waterfront', 'year built' => 'ybuilt', 'legal description' => 'legald', 'per floor sqft' => 'perfsqft', 'lot depth' => 'lotd', 'lot width' => 'lotw', 
			'last sold' => 'lsold', 'last remodel year' => 'lremodely', 'unit cnt' => 'unit', 'total rooms' => 'trooms', '# stories' => 'stories', 'school district' => 'schoold',
			'county' => 'county','county name' => 'county', 'on zillow' => 'dom', 'cooling' => 'ac'
		);


		/**************************
		*	Extract Data for Public Record
		**************************/
		$publicR = array();
		foreach($data as $row){
			$c=0;	//With this variable, I control to obtain the values ??of either the Combined or the Public Record. 1 is Combined, 2 is Public Record
			foreach($row as $field){
				if($c==0){	//Get the firs position to obtain the field name{
					$nameField=$standarData[strtolower(trim($field))];
				}
				if($c==2){	//Get the second position to obtain the first value for this field
					if(!empty($nameField) and trim($field) != "--" and !empty($field))
						$publicR[$nameField]=trim($field);
				}
				$c++;
			}
		}
		//echo "<pre>";print_r($publicR);echo "</pre>";
		/**************************
		*	Extract Data for Combined Result
		**************************/		
		$residential = array();
		foreach($data as $row){
			$c=0;	//With this variable, I control to obtain the values ??of either the Combined or the Public Record. 1 is Combined, 2 is Public Record
			foreach($row as $field){
				if($c==0)	//Get the firs position to obtain the field name
					$nameField=$standarData[strtolower(trim($field))];

				if($c==1){	//Get the first position to obtain the first value for this field
					if(!empty($nameField) and trim($field) != "--" and !empty($field))
						$residential[$nameField]=trim($field);
				}
				$c++;
			}
		}
		//echo "<pre>";print_r($residential);echo "</pre>";die();
		/**************************
		*	Standarize status camp
		**************************/
		
		switch(trim($this->aditional['status'])){
			case	"NotforSale":	$status	=	"CC";	break;
			case	"MakemeMove":	$status	=	"CC";	break;
			case	"ForSale":		$status	=	"AA";	break;
			case	"ForRent":		$status	=	"BB";	break;
			case	"Sold":			$status	=	"CS";	break;
			case	"ByOwner":		$status	=	"DD";	break;
			case	"BankOwned":	$status	=	"EE";	break;
			case	"Foreclosure":	$status	=	"FF";	break;
			case	"Pending":		$status	=	"GG";	break;
			default:				$status	=	"CC";	break;
		}
		/**************************
		*	Standarize status type
		**************************/
		if(empty($publicR['type']) or $publicR['type'] == "--")
			$type = $residential['type'];
		else
			$type = $publicR['type'];

		switch($type){
			case	"Single Family":		$typePT	=	"01";	break;
			case	"Townhouse":			$typePT	=	"04";	break;
			case	"Condo":				$typePT	=	"04";	break;
			case	"Cooperative":			$typePT	=	"04";	break;
			case	"Multi Family":			$typePT	=	"08";	break;
			case	"Vacant Land":			$typePT	=	"00";	break;
			case	"Mobile / Manufactured":$typePT	=	"02";	break;
			default:						$typePT	=	"99";	break;
		}
		/**************************
		*	Data for Proterty Table
		**************************/
		unset($propertyArray);
		$status=="CC" ? $propertyArray=$residential : $propertyArray=$publicR;
		if(empty($this->county))
			$county	=	empty($publicR['county'])	?	trim($residential['county'])	:	trim($publicR['county']);
		else
			$county	=	$this->county;
		$propertyFields = array(
			'txproperty*address' => $this->street,
			'txproperty*addressfull' => $this->aditional['address']." ".$this->zip,
			'txproperty*zip' => $this->zip,
			'txproperty*city' => $this->city,
			'txproperty*unit' => trim(str_replace("UNIT","",$this->unit)),
			//'txproperty*county' => trim($propertyArray['county']),	// OLD, Next line New
			'txproperty*county' => $county,
			//'txproperty*yrbuilt' => $propertyArray['ybuilt'],
			'cbproperty*xcode' => $typePT,
			'cbproperty*state' => $this->state,
			'txproperty*latitude' => $this->latitude,
			'txproperty*longitude' => $this->longitude,
			'cbproperty*status' => $status
		);

		$status=="CC" ? $psummaryArray=$residential : $psummaryArray=$publicR;
		/**************************
		*	Data for Psummary Table
		**************************/
		////*Aditional Process For Camp Sale Price And Sale Date*///
		if(trim($publicR['lsold'])== "" or trim($publicR['lsold']) == "--")
			$psummaryLsold = explode("for",$residential['lsold']);
		else{
			if(trim($residential['lsold'])!= "" and trim($residential['lsold']) != "--")
				$psummaryLsold = explode("for",$publicR['lsold']);
		}
		if(count($psummaryLsold)>1)
			$psummarySaleDate = $this->saleDate($psummaryLsold[0]);
		////*Aditional Process For Camp Lot*///
		$psummaryTsqft=explode("sq ft",$psummaryArray['lot']);
			$residentialTsqft=explode("sq ft",$residential['lot']);
		$psummaryFields = array(
			'txpsummary*beds' 		=> empty($psummaryArray['beds']) 			?	$residential['beds']	:	$psummaryArray['beds'],
			'txpsummary*bath'		=> empty($psummaryArray['baths'])			?	$residential['baths']	:	$psummaryArray['baths'],
			'txpsummary*lsqft'		=> trim(str_replace(",","",$psummaryArray['sqft']))==""	?	str_replace(",","",trim($residentialTsqft[0]))	:	str_replace(",","",$psummaryArray['sqft']),
			'txpsummary*bheated'	=> str_replace(",","",$psummaryArray['sqft']),
			'txpsummary*folio'		=> $psummaryArray['parcel'],
			'txpsummary*yrbuilt'	=> empty($psummaryArray['ybuilt'])			?	$residential['ybuilt']	:	$psummaryArray['ybuilt'],
			'cbpsummary*pool'		=> $psummaryArray['pool']	=== "Yes"		?	"Y"						:	"N",
			'cbpsummary*ac'			=> !empty($psummaryArray['ac'])				?	"Y"						:	!empty($residential['ac'])	?	"Y"	:	"N",
			'txpsummary*saledate'	=> $psummarySaleDate,
			'txpsummary*saleprice'	=> number_format(str_replace("$","",str_replace(",","",trim($psummaryLsold[1]))),2,".",","),
			'txpsummary*stories'	=> $psummaryArray['stories'],
			'txpsummary*legal'		=> $psummaryArray['legald'],
			'txpsummary*tsqft'		=> str_replace(",","",trim($psummaryTsqft[0])),
			'cbpsummary*waterf'		=> $psummaryArray['waterfront']	=== "Yes"	?	"Y"						:	"N"
		);
		/**************************
		*	Data for Residential Table
		**************************/
		if($status==="AA" or $status==="BB" or $status==="DD" or $status==="EE" or $status==="FF" or $status==="GG"){
			$residentialTsqft=explode("sq ft",$residential['lot']);
			$residentialFields	=	array(
				'txmlsresidential*bath'			=>	$residential['baths'],
				'txmlsresidential*beds'			=>	$residential['beds'],
				'txmlsresidential*folio'		=>	$residential['parcel'],
				'txmlsresidential*yrbuilt'		=>	$residential['ybuilt'],
				'txmlsresidential*dom'			=>	$this->dom,
				'txmlsresidential*apxtotsqft'	=>	str_replace(",","",trim($residentialTsqft[0])),
				'txmlsresidential*lsqft'		=>	str_replace(",","",trim($residentialTsqft[0])),
				'txmlsresidential*ldate'		=>	$this->listingDate,
				'txmlsresidential*mlnumber'		=>	$residential['mls'],
				'txmlsresidential*lprice'		=>	number_format($this->aditional['price'],2,".",","),
				'txmlsresidential*remark'		=>	$this->remark,
				'cbmlsresidential*waterf'		=>	$residential['waterfront']	=== "Yes"	?	"Y"	:	"N",
				'cbmlsresidential*pool'			=>	$residential['pool']	=== "Yes"	?	"Y"	:	"N",
				'txmlsresidential*entrydate'	=>	$this->entryDate
			);
		}else{
			$residentialFields	=	array(
				'txmlsresidential*folio'		=>	$residential['parcel']
			);
		}

		$agentFields=array(
			'txagent*agent'			=>	$this->agentName,
			'txagent*phone'			=>	$this->agentPhone,
			'txagent*broker'			=>	$this->broker,
			'txagent*phoneBroker'		=>	$this->agentph2,
			'txagent*avatar'			=>	$this->agentImg
		);
		/**************************
		*	Data for Sales Table
		**************************/
		//$this->salesHistory;
		
		/**************************
		*	Data for Url Images
		**************************/
		//$this->images;
		
		$foreclosure = array(
			'txpendes*folio'	=>	$psummaryArray['parcel']
		);
		
		$mortgage = array(
			'txmortgage*folio'	=>	$psummaryArray['parcel']
		);
		
		$allData = array(
			'property'		=>	$propertyFields,
			'psummary'		=>	$psummaryFields,
			'residential'	=>	$residentialFields, 
			'sales'			=>	$this->salesHistoryData, 
			'foreclosure'	=>	$foreclosure, 
			'agent'			=>	$agentFields,
			'mortgage'		=>	$mortgage,
			'images'		=>	$this->images,
			'aditionals'	=>	array(
									"countyUrl" =>	$this->urlCounty,
									"urlDetail"	=>	$this->urlDetail
								)
		);
		if(!empty($residential['mls'])){
			$AuxRealtor	=	new Realtor();
			$c=0;
			do{
				$jsonResultRealtor	=	json_decode($AuxRealtor->ObtainUrlDetail($residential['mls']));	//Convert json String toObject Json
				$c++;
			}while($jsonResultRealtor->url == false and $c<=5);
			$allData['aditionals']['urlMore']	=	$jsonResultRealtor->url;
		}

		return $allData;
		
		
		//Add fields standar
		$newData['street']		=	$this->street;
		$newData['latitude']	=	$this->latitude;
		$newData['longitude']	=	$this->longitude;
	}
	
	private function saleDate($date){
		$date	=	trim($date);
		if(empty($date))
			return "";
		$aux = explode(" ",trim($date));
		$meses = array("Jan" => '01',"Feb" => "02","Mar" => "03","Apr" => "04","May" => "05","Jun" => "06","Jul" => "07","Aug" => "08","Sep" => "09","Oct" => "10","Nov" => "11","Dic" => "12");
		return $aux[1]."/".$meses[$aux[0]]."/"."01";
	}
	
	/******************************
	*
	*	Convert the Zillow url to the real Url
	*
	*******************************/
	private function obtainCountyURL( $url ) {
		$res = array();
		$options = array( 
			CURLOPT_RETURNTRANSFER => true,     // return web page 
			CURLOPT_HEADER         => false,    // do not return headers 
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects 
			CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1", // who am i 
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect 
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect 
			CURLOPT_TIMEOUT        => 120,      // timeout on response 
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects 
		); 
		$ch      = curl_init( $url ); 
		curl_setopt_array( $ch, $options ); 
		$content = curl_exec( $ch ); 
		$err     = curl_errno( $ch ); 
		$errmsg  = curl_error( $ch ); 
		$header  = curl_getinfo( $ch ); 
		curl_close( $ch ); 

		//$res['content'] = $content;     
		//$res['url'] = $header['url'];
		//return $res; 
		return $header['url'];
	}
	
	/******************************
	*
	*	Second Process To Search and Extract the Url of comparables
	*
	*******************************/
	private function saveHtmlCode($html){
		$fd = fopen ("html.txt", "w"); 
		fwrite ($fd, $html);
		fclose($fd);
	}

	/******************************
	*
	*	Second Process To Search and Extract the Url of comparables
	*
	*******************************/
	function getUrlComparables($url,$latitude,$longitude){
		$latMin=$latitude-($this->factorLat);
		$latMax=$latitude+($this->factorLat);
		$lonMin=$longitude-($this->factorMin);
		$lonMax=$longitude+($this->factorMin);
		$rect=$this->setLatLng($lonMin).','.$this->setLatLng($latMin).','.$this->setLatLng($lonMax).','.$this->setLatLng($latMax);
		$array=explode('/',$url);
		$zpid=str_replace('_zpid','',$array[count($array)-2]);
		
		
		$urlComps2="http://www.zillow.com/homes/comps/".$zpid."_zpid/";
		$urlComps1="http://www.zillow.com/search/GetCompsResults.htm?zpid=$zpid&mm=undefined&rect=$rect&sort=default&desc=true";

		//$urlCompsAct="http://www.zillow.com/homes/for_sale/FL/fsba_lt/days_sort/".$latMax.",".$lonMax.",".$latMin.",".$lonMin."_rect/15_zm/";
		$urlCompsAct="http://www.zillow.com/homes/for_sale/fsba_lt/0_pnd/days_sort/".$latMax.",".$lonMax.",".$latMin.",".$lonMin."_rect/15_zm/0_mmm/";
		
		
		
		return array('urlComp'=>array('url1'=>$urlComps2, 'url2'=>$urlComps1),'urlAct'=>$urlCompsAct);
	}
	
	
	/******************************
	*
	*	Third Process To Search and Extract comparables
	*
	*******************************/
	function getComparables($url){
		$url1 = $url['url1'];
		$url2 = $url['url2'];
		
		
		$r = $this->codeHtml($url2);	
		
		$json = $r;
		$comparables=json_decode($json,true);
		
		$html			=	$comparables['html'];
		$html			=	str_replace("\\","",$html);
		$html			=	str_replace("</td>","|</td>",$html);
		$contentTable	=	$this->managerParseo->getXpathHtml($html,false);
		$results		=	$this->managerParseo->queryDom($contentTable,"//tr[count(td)=10]");
		
		$arrayComp			=	array();
		foreach ( $results as $link )
		{
			$arrayComp[] = explode("|",str_replace(":","",$link->nodeValue));
		}

		$arrayReturn=array();
		$status='CC';
		$j=0;
		for($i=0;$i<count($arrayComp);$i++){
			if($this->exclusionaryProperty($arrayComp[$i][0])){
				$arrayReturn[$j]['status']		=	$status;
				$arrayReturn[$j]['address']		=	$arrayComp[$i][0];
				$arrayReturn[$j]['distance']	=	$arrayComp[$i][9];
				
				$arrayReturn[$j]['saledate']	=	$this->saleComparable($arrayComp[$i][2]);
				$arrayReturn[$j]['saleprice']	=	$this->replaceString($arrayComp[$i][1],array('$',','));
				$arrayReturn[$j]['lprice']		=	0;
				$arrayReturn[$j]['dom']			=	0;
				
				$arrayReturn[$j]['bath']		=	$arrayComp[$i][4]!='--' ? $arrayComp[$i][4] : 0;
				$arrayReturn[$j]['bed']			=	$arrayComp[$i][3]!='--' ? $arrayComp[$i][3] : 0;
				$arrayReturn[$j]['lsqft']		=	$arrayComp[$i][5]!='--' ? $this->replaceString($arrayComp[$i][5],array(',')) : 0;
				$arrayReturn[$j]['tsqft']		=	$arrayComp[$i][6]!='--' ? $this->replaceString($arrayComp[$i][6],array(',')) : 0;
				$arrayReturn[$j]['yearbuilt']	=	$arrayComp[$i][7]!='--' ? $arrayComp[$i][7] : 0; 
				$arrayReturn[$j]['pricesqft']	=	$arrayComp[$i][8]!='--' ? $this->replaceString($arrayComp[$i][8],array('$',',')) : 0;
				$arrayReturn[$j]['latitude']	=	$this->setLatLng($comparables['mapResults'][$i][1],true);
				$arrayReturn[$j]['longitude']	=	$this->setLatLng($comparables['mapResults'][$i][2],true);
				$arrayReturn[$j]['propertyType']	='';
				$j++;
			}
		}
		return $arrayReturn; 
	}
	
	function getComparablesAct($url){

		$arrayReturn=array();			
		$data=$this->getListResultMobile(NULL,NULL,$url);
		
		$typePro=array(
			'Condo' 		=> 'Condo/Apartament',
			'Apartment'		=> 'Condo/Apartament',
			'Townhouse'		=> 'Condo/Apartament',
			'Single Family'	=> 'Single Family',
			'Multi Family'	=> 'Multy Family',
			'Lost'			=> 'Lost/Land',
			'Land'			=> 'Lost/Land',
			'Mobile / Manufactured'	=> 'Mobile / Manufactured'
		);

		foreach ( $data['properties'] as $k => $v ){
			if($this->exclusionaryProperty($v['address'])){
				array_push($arrayReturn,array(
					'status'	=>	'A',
					'address'	=>  $v['address'],
					'latitude'	=>	$v['latitude'],
					'longitude'	=>	$v['longitude'],
					'lprice'	=>	$v['price'],
					'saledate' 	=>	'',
					'saleprice'	=>	0,
					'bath'		=>	$v['baths'],
					'lsqft'		=>	$v['sqft'],
					'tsqft'		=>	$v['lot'],
					'bed'		=>	$v['beds'],
					'yearbuilt'	=>  $v['yb'],
					'pricesqft'	=>	($v['sqft']>0 && $v['price']>0) ? ($v['price']/$v['sqft']) : 0,
					'dom'		=>  $v['marke'],
					'propertyType' => $typePro[trim($v['type'])]=='' ? trim($v['type']) : $typePro[trim($v['type'])]
				));
			}
		}
		
		//die();
		/*
		$html	=	$this->codeHtml($url,false);
		//echo $html;
		$contentTable	=	$this->managerParseo->getXpathHtml($html,false);
		$results = $this->managerParseo->queryDom($contentTable,"//ul[@id='search-results']//li[@class='search-result clearfix rollable featured-listing']");
		$j=0;

		foreach ( $results as $link ){
			$arrayReturn[$j]['status'] = $status;
			$arrayReturn[$j]['latitude']	=	$this->setLatLng($link->getAttribute('latitude'),true);
			$arrayReturn[$j]['longitude']	=	$this->setLatLng($link->getAttribute('longitude'),true);
			//Extract address
			$href = $link->getElementsByTagName('a');
			foreach($href as $div){
				//echo '<br>'.$div->getAttribute('class');
				if($div->getAttribute('class')=='hdp-link'){
					$arrayReturn[$j]['address']=$div->nodeValue;
				}
			}
			$href = $link->getElementsByTagName('li');
			foreach($href as $li){
				//echo '<br>'.$li->getAttribute('class');
				if($li->getAttribute('class')=='type-forSale'){
					$price = $link->getElementsByTagName('span');
					foreach($price as $span){
						if($span->getAttribute('class')=='price'){
							$arrayReturn[$j]['saledate']	=	'';
							$arrayReturn[$j]['saleprice']	=	0;
							$arrayReturn[$j]['lprice']=$this->replaceString($span->nodeValue,array('$',','));
						}
					}
				}
				if($li->getAttribute('class')=='prop-cola'){
					//$imagesInformation=extractData('"LST_Media"',$htmlText,"],");
					$arrayReturn[$j]['bath']		=	$this->extractData('Baths:',$li->nodeValue,"Sqft")!='--' ? $this->extractData('Baths:',$li->nodeValue,"Sqft") : 0;
					$arrayReturn[$j]['bed']			=	$this->extractData('Beds:',$li->nodeValue,"Baths")!='--' ? $this->extractData('Beds:',$li->nodeValue,"Baths") : 0;
					$arrayReturn[$j]['lsqft']		=	$this->extractData('Sqft:',$li->nodeValue,"Lot")!='--' ? $this->replaceString($this->extractData('Sqft:',$li->nodeValue,"Lot"),array('$',',')) : 0;
					$arrayReturn[$j]['tsqft']		=	$this->extractData('Lot:',$li->nodeValue,"<br>")!='-' ? $this->replaceString($this->extractData('Lot:',$li->nodeValue,"<br>"),array('$',',')) : 0;
					
					$arrayReturn[$j]['pricesqft']	=	0;
				}
				if($li->getAttribute('class')=='prop-colb'){
					$arrayReturn[$j]['dom']			=	$this->extractData('Days on Zillow:',$li->nodeValue,"Built")!='--' ? $this->extractData('Days on Zillow:',$li->nodeValue,"Built") : 0;
					$built=$this->extractData('Built:',$li->nodeValue," ");
					$built=substr($built,0,4);
					$arrayReturn[$j]['yearbuilt']	=	substr($built,0,2)!='--' ? $built : 0;
				}
			}
			//break;
			$arrayReturn[$j]['distance'] = 0;
			$j++;
		}*/
		return $arrayReturn;
	}
	
	private function replaceString($string,$replaces){
		$returnString = $string;
		foreach($replaces as $replace){
			$returnString = str_replace($replace,'',$returnString);
		}
		return $returnString;
	}
	private function saleComparable($date){
		$date	=	trim(str_replace(',','',$date));
		if(empty($date))
			return "";
		$aux = explode("/",trim($date));
		return $aux[2].$aux[1].$aux[0];
	}
	private function extractData($find,$text,$finddelimiter){
		$pos = strpos($text, $find);
		$data = substr($text,$pos+strlen($find)); 
		$pos = strpos($data, $finddelimiter);
		$data = substr($data,1,$pos-1); 
		//$data = str_replace('"',"",$data);
		//echo '<br>'.$data;
		return trim($data);
	}
	
	private function exclusionaryProperty($p){
		$patron = "/undisclosed/i";  
		if (preg_match($patron, $p)) {
			return false;
		}else{
			return true;
		}
	}
}
?>