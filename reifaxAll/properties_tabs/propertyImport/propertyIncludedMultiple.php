<?php

require('../../properties_conexion.php');
require('class.zillow.php');
conectar();
	
$Zillow =new Zillow();

if($_POST['type']=="A"){

	/*******************
	*	Correct Address
	*******************/
	$exeptions = Array("#","\t");
	foreach($exeptions as $value)
		$_POST['address']	=	str_replace($value,"",$_POST['address']);

	$c=0;
	do{
		$jsonResult = $Zillow->getPrincipalResult($_POST['address']); //For Sale
		$result = json_decode($jsonResult);	//Convert json String toObject Json
		$c++;
	}while($result->url == false and $c<=5);

	if(!$result->url){	//If not found onle show false and break
		require("class.trulia.php");
		$Trulia=	new Trulia();

		$jsonResultTrulia	=	$Trulia->getPrincipalResult($_POST['address']);
		
		$jsonResultTrulia	=	json_decode($jsonResultTrulia);	//Convert json String toObject Json
		
		if(!$jsonResultTrulia->url){
			echo json_encode(Array('url' => false, 'option' => 'T'));
			die();
		}else{
			$c=0;
			do{
				$jsonResultZillow = $Zillow->getPrincipalResult($Trulia->addressFull);
				$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
				$c++;
			}while($jsonResultZillow->url == false and $c<=5);

			if(!$jsonResultZillow->url){
				$Trulia->idMultiple	=	$_POST['idMultiple']+1;	//IdMultiplpe it is the position of array in order to search, for control all images in one folder
				$jsonResultTrulia	=	$Trulia->getDetailResult($Trulia->html);
				echo processDataAndImport(json_decode($jsonResultTrulia));
				die();
			}else
				$result	=	$jsonResultZillow;
		}
	}
	
	$Zillow->idMultiple	= $_POST['idMultiple']+1;	//IdMultiplpe it is the position of array in order to search, for control all images in one folder
	/************************
	*	Conver Object to Array
	************************/
	$postParameter = Array();
	if(count($result->aditional)>0){
		foreach($result->aditional as $index => $value){
			$postParameter[$index]	=	$value;
		}
	}
	/******END******/

	$Zillow->aditional	=	$postParameter;	//Send Additional Parameters

	$urlDetail = $Zillow->getUrlDetail(base64_decode($result->url));

	$details = $Zillow->getDetailResult($urlDetail);

	$details = json_decode($details);
	
	if(!$details->result)	//Not Found Details
		echo json_encode(Array("url" => false));
	else{	//Found Details
		
		echo processDataAndImport($details);
		/*
		unset($_POST['address']);
		unset($_POST['type']);
		
		foreach($details->data as $type => $arrayValues){
			if($type == "property" or $type == "psummary" or $type == "residential" or $type == "foreclosure" or $type == "mortgage"){
				$details = json_encode($arrayValues);
				$details = json_decode($details,true);
				
				foreach($details as $index => $value){
					if($index	==	"txmlsresidential*dom")
						$value = preg_replace("/[a-zA-Z]/","",$value);
					$_POST[$index]	=	$value;
				}
			}elseif($type == "sales"){
				$details = json_encode($arrayValues->data);
				$details = json_decode($details,true);
				$c=0;
				if(count($details)>0){
					foreach($details as $salesValues){
						$c++;
						foreach($salesValues as $index => $value){
							$_POST[$index]	=	$value;
						}
					}
				}
				$_POST['salesTotal']	=	$c;
			}elseif($type == "images"){
				$details = json_encode($arrayValues->temp);
				$details = json_decode($details,true);
				foreach($details as $index => $value){
					$aux	=	explode("/",$value);
					$value	=	$aux[count($aux)-1];
					$_POST['tximages'][]	=	$value;
				}
			}elseif($type == "aditionals"){
				$details = json_encode($arrayValues);
				$details = json_decode($details,true);
				foreach($details as $index => $value){
					if(!empty($value))
						$_POST['txUrls'][$index]	=	$value;
				}
			}
		}
		//print_r($_POST['tximages']);
		//die();
		include("propertyIncluded.php");*/
	}

}else{

	//require('class.realtor.php');
	
	$Realtor = new Realtor();

	$urlDetail = $Realtor->ObtainUrlDetail(trim($_POST['address']));	//Status Sold
	$resultR = json_decode($urlDetail);	//Convert json String toObject Json
	
	if(!$resultR->url)	//If not found onle show false and break
		echo json_encode(Array("url" => false));
	else{	//else continued extrain details
		
		$propertyDetails = json_decode($Realtor->getDetailResult(base64_decode($resultR->url)));
		
		$property	=	$propertyDetails->data;

		//New data for Mlsresidential from Realtor
		$newMlsResidential	=	$propertyDetails->data->data;

		$c=0;
		do{
			$jsonResultZillow	= $Zillow->getPrincipalResult($property->street." ".$property->city." ".$property->state." ".$property->zip);
			$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
			$c++;
		}while($jsonResultZillow->url == false and $c<=5);

		if(!$jsonResultZillow->url){
			require("class.trulia.php");
			$Trulia=	new Trulia();

			$jsonResultTrulia	=	$Trulia->getPrincipalResult($property->street." ".$property->city." ".$property->state." ".$property->zip);
			
			$jsonResultTrulia	=	json_decode($jsonResultTrulia);	//Convert json String toObject Json
			
			if(!$jsonResultTrulia->url){
				echo json_encode(Array('url' => false, 'option' => 'T'));
				die();
			}else{
				$c=0;
				do{
					$jsonResultZillow = $Zillow->getPrincipalResult($Trulia->addressFull);
					$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
					$c++;
				}while($jsonResultZillow->url == false and $c<=5);
				
				if(!$jsonResultZillow->url){
					$Trulia->idMultiple	=	$_POST['idMultiple']+1;	//IdMultiplpe it is the position of array in order to search, for control all images in one folder
					$jsonResultTrulia	=	$Trulia->getDetailResult($Trulia->html);
					echo processDataAndImport(json_decode($jsonResultTrulia));
					die();
				}
			}
		}

		$Zillow->idMultiple	=	$_POST['idMultiple']+1;	//IdMultiplpe it is the position of array in order to search, for control all images in one folder
		/************************
		*	Conver Object to Array
		************************/
		$postParameter = Array();
		foreach($jsonResultZillow->aditional as $index => $value){
			$postParameter[$index]	=	$value;
		}
		/******END******/

		$Zillow->aditional	=	$postParameter;	//Send Additional Parameters

		$urlDetailZillow = $Zillow->getUrlDetail(base64_decode($jsonResultZillow->url));

		$detailsZillow = $Zillow->getDetailResult($urlDetailZillow);

		$detailsZillow = json_decode($detailsZillow);

		if(!$detailsZillow->result)	//Not Found Details
			echo json_encode(Array("url" => false));
		else{	//Found Details
			
			/****Assing Data From Realtor****/
			$detailsZillow->data->residential->{"txmlsresidential*beds"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->beds));
			$detailsZillow->data->residential->{"txmlsresidential*bath"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->baths));
			$detailsZillow->data->residential->{"txmlsresidential*yrbuilt"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->ybuilt));
			$detailsZillow->data->aditionals->urlMore	=	$resultR->url;
			if(!empty($Trulia->urlFree))
				$detailsZillow->data->aditionals->urlFree	=	$Trulia->urlFree;

			if(!$detailsZillow->result)	//Not Found Details
				echo json_encode(Array("url" => false));
			else{	//Found Details
				
				echo processDataAndImport($detailsZillow);
				/*unset($_POST['address']);
				unset($_POST['type']);
				
				foreach($detailsZillow->data as $type => $arrayValues){
					if($type == "property" or $type == "psummary" or $type == "residential" or $type == "foreclosure" or $type == "mortgage"){
						$detailsZillow = json_encode($arrayValues);
						$detailsZillow = json_decode($detailsZillow,true);
						
						foreach($detailsZillow as $index => $value){
							if($index	==	"txmlsresidential*dom")
								$value = preg_replace("/[a-zA-Z]/","",$value);
							$_POST[$index]	=	$value;
						}
					}elseif($type == "sales"){
						$detailsZillow = json_encode($arrayValues->data);
						$detailsZillow = json_decode($detailsZillow,true);
						$c=0;
						if(count($detailsZillow)>0){
							foreach($detailsZillow as $salesValues){
								$c++;
								foreach($salesValues as $index => $value){
									$_POST[$index]	=	$value;
								}
							}
						}
						$_POST['salesTotal']	=	$c;
					}elseif($type == "images"){
						$detailsZillow = json_encode($arrayValues->temp);
						$detailsZillow = json_decode($detailsZillow,true);
						foreach($detailsZillow as $index => $value){
							$aux	=	explode("/",$value);
							$value	=	$aux[count($aux)-1];
							$_POST['tximages'][]	=	$value;
						}
					}elseif($type == "aditionals"){
						$detailsZillow = json_encode($arrayValues);
						$detailsZillow = json_decode($detailsZillow,true);
						foreach($detailsZillow as $index => $value){
							if(!empty($value))
								$_POST['txUrls'][$index]	=	$value;
						}
					}
				}
				//print_r($_POST['tximages']);
				//die();
				include("propertyIncluded.php");*/
			}
		}
	}
}

function processDataAndImport($dataDetails){
	unset($_POST['address']);
	unset($_POST['type']);
	
	foreach($dataDetails->data as $type => $arrayValues){
		if($type == "property" or $type == "psummary" or $type == "residential" or $type == "foreclosure" or $type == "mortgage"){
			$dataDetails = json_encode($arrayValues);
			$dataDetails = json_decode($dataDetails,true);
			
			foreach($dataDetails as $index => $value){
				if($index	==	"txmlsresidential*dom")
					$value = preg_replace("/[a-zA-Z]/","",$value);
				$_POST[$index]	=	$value;
			}
		}elseif($type == "sales"){
			$dataDetails = json_encode($arrayValues->data);
			$dataDetails = json_decode($dataDetails,true);
			$c=0;
			if(count($dataDetails)>0){
				foreach($dataDetails as $salesValues){
					$c++;
					foreach($salesValues as $index => $value){
						$_POST[$index]	=	$value;
					}
				}
			}
			$_POST['salesTotal']	=	$c;
		}elseif($type == "images"){
			$dataDetails = json_encode($arrayValues->temp);
			$dataDetails = json_decode($dataDetails,true);
			if(count($dataDetails)>0){
				foreach($dataDetails as $index => $value){
					$aux	=	explode("/",$value);
					$value	=	$aux[count($aux)-1];
					$_POST['tximages'][]	=	$value;
				}
			}else
				$_POST['tximages']	=	Array();
		}elseif($type == "aditionals"){
			$dataDetails = json_encode($arrayValues);
			$dataDetails = json_decode($dataDetails,true);
			foreach($dataDetails as $index => $value){
				if(!empty($value))
					$_POST['txUrls'][$index]	=	$value;
			}
		}
	}
	if(!empty($_POST['txpsummary*folio'])){
		$query	=	"SELECT * FROM properties a 
						INNER JOIN psummary b on a.parcelid=b.parcelid
						WHERE a.userid='".$_COOKIE['datos_usr']['USERID']."' AND b.folio='".$_POST['txpsummary*folio']."'";
		$result = mysql_query($query);
		if(mysql_num_rows($result)>0)
			die(json_encode(Array("success" => true, "msg" => "Ready Exists")));
	}
	include("propertyIncluded.php");
}
?>