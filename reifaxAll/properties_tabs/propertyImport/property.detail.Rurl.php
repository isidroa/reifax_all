<?php
/**************************************************************
*
*	Primero se Busca en Realtor, si lo consigue, se busca en Zillow, si en Zillow no Existe, se Busca en Trulia, si se Consigue en Trulia, se Vuelve a Buscar en Zillow con la direccion
*	y si en Zillow no es encontrado con la nueva direccion, entonces ahi si toca buscarlo definitivamente por Trulia
*
**************************************************************/
require('../../properties_conexion.php');
conectar();

require('class.realtor.php');
require("class.zillow.php");
	
$Realtor=	new Realtor();
$Zillow	=	new Zillow();

$propertyDetails = json_decode($Realtor->getDetailResult(base64_decode($_POST['detail'])));

$property	=	$propertyDetails->data;

//New data for Mlsresidential from Realtor
$newMlsResidential	=	$propertyDetails->data->data;

$c=0;
do{
	$jsonResultZillow = $Zillow->getPrincipalResult($property->street." ".$property->city." ".$property->state." ".$property->zip);
	$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
	$c++;
}while($jsonResultZillow->url == false and $c<=5);

if(!$jsonResultZillow->url){
	require("class.trulia.php");
	$Trulia=	new Trulia();

	$jsonResultTrulia	=	$Trulia->getPrincipalResult($property->street." ".$property->city." ".$property->state." ".$property->zip);
	
	$jsonResultTrulia	=	json_decode($jsonResultTrulia);	//Convert json String toObject Json
	
	if(!$jsonResultTrulia->url){
		echo json_encode(Array('url' => false, 'option' => 'T'));
		die();
	}else{
		$c=0;
		do{
			$jsonResultZillow = $Zillow->getPrincipalResult($Trulia->addressFull);
			$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
			$c++;
		}while($jsonResultZillow->url == false and $c<=5);
		
		if(!$jsonResultZillow->url){
			echo $jsonResultTrulia	=	$Trulia->getDetailResult($Trulia->html);
			die();
		}
	}
}
	
	/************************
	*	Conver Object to Array
	************************/
	$postParameter = Array();
	foreach($jsonResultZillow->aditional as $index => $value){
		$postParameter[$index]	=	$value;
	}
	/******END******/

	$Zillow->aditional	=	$postParameter;	//Send Additional Parameters

	$urlDetailZillow = $Zillow->getUrlDetail(base64_decode($jsonResultZillow->url));

	$detailsZillow = $Zillow->getDetailResult($urlDetailZillow);

	$detailsZillow = json_decode($detailsZillow);

	/****Assing Data From Realtor****/
	$detailsZillow->data->residential->{"txmlsresidential*beds"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->beds));
	$detailsZillow->data->residential->{"txmlsresidential*bath"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->baths));
	$detailsZillow->data->residential->{"txmlsresidential*yrbuilt"}	=	trim(ereg_replace("[A-Za-z]", "", $newMlsResidential->ybuilt));
	$detailsZillow->data->aditionals->urlMore	=	$_POST['detail'];

	$detailsZillow = json_encode($detailsZillow);

	echo $detailsZillow;

?>