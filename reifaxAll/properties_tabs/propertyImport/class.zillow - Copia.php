<?php
/*************************
Classes Designed To Manage The Zillow Spider

Author: Jesus Marquez
*************************/
//include_once("class.realtor.php");
class Zillow {
	
	//Variables
	var $aditional		=	Array();
	var $images			=	Array();
	var $address		=	"";
	var $price			=	"";
	var $status			=	"";
	var $street			=	"";
	var $city			=	"";
	var $state			=	"";
	var $zip			=	"";
	var $userid			=	"";
	var $unit			=	"";
	var $latitude		=	"";
	var $longitude		=	"";
	var $remark			=	"";
	var $listingDate	=	"";
	var $entryDate		=	"";
	var $salesHistory	=	"";
	var $idMultiple		=	"";
	var $urlCounty		=	"";
	var $urlDetail		=	"";

	/******************************
	*
	*	Constructor Function
	*
	*******************************/
	function Zillow(){
		return true;
	}
	
	/******************************
	*
	*	Principal Search Process. Return the First Result
	*
	*******************************/
	function getPrincipalResult($address){
		
		$Url	=	"";
		$html	=	$this->codeHtml($address);
		$xpathFirst	=	$this->getXpathHtml($html,'/<!--{(.*)}-->/');
		$FirstResult=	$this->queryDom($xpathFirst,"//div[@id='pinfo-block']//div[@class='adr']");

		foreach($FirstResult as $link){
			//echo $link->getAttribute( 'id' )."<br>";
			$nodes	=	$link->childNodes;
			foreach($nodes as $node) {
				foreach($node->attributes as $attrName => $attrNode) {
					if($attrName == 'href')
						$Url = "http://www.zillow.com".$attrNode->nodeValue;
					if($attrName == 'title')
						$this->address = $attrNode->nodeValue;
				}
			}
		}
		
		$TypeResult	=	$this->queryDom($xpathFirst,"//div[@id='pinfo-block']//span[@class='type']");

		foreach ( $TypeResult as $link ){
			$this->status	=	str_replace(":","",$link->nodeValue);
		}

		$PriceResult=	$this->queryDom($xpathFirst,"//div[@id='pinfo-block']//span[@class='price']");
		
		foreach ( $PriceResult as $link ){
			$this->price	=	str_replace(",","",str_replace("$","",$link->nodeValue));
		}

		if($Url == "" or $this->address == "")
			return json_encode(Array('url' => false, 'option' => 'Z'));
		else
			return json_encode(Array('url' => base64_encode($Url), 'option' => 'Z', 'aditional' => Array('address' => $this->address, 'status' => $this->status, 'price' => $this->price)));
	}
	
	/******************************
	*
	*	Second Process To Search and Extract the Url Detail
	*
	*******************************/
	function getUrlDetail($url){
		$this->urlDetail	=	base64_encode($url);
		$html				=	$this->codeHtml($url,false);
		
		////Obtain de URL for Price History
		$this->priceHistory($html);
		
		$contentTable	=	$this->getXpathHtml($html,false);

		////Obtain de URL for Images
		$this->urlImages($contentTable);
		
		///// Obtain the Street Address
		$results		=	$this->queryDom($contentTable,"//div[@class='street-address']");	
		if($results->length	>	0)
			$this->street	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->queryDom($contentTable,"//meta[@property='og:street-address']");
			if($results->length	>	0)
				$this->street	=	$results->item(0)->getAttribute('content');
		}
		
		///// Obtain the Remark
		$results		=	$this->queryDom($contentTable,"//div[@id='home-description']");
		//echo "<pre>".str_replace('…More Less',"",(string)$results->item(0)->nodeValue)."</pre>";
		$aux	=	trim(utf8_encode($results->item(0)->nodeValue));
		if(strpos($aux,"â¦MoreÂ") !== false)
			$aux	=	substr($aux,0,strlen($aux)-18);
		//echo "<pre>".str_replace("â¦MoreÂ Less","",utf8_encode($results->item(0)->nodeValue))."</pre>";
		//preg_match_all('/.*"(.*)"/',$results->item(0)->nodeValue,$aux);
		$this->remark	=	str_replace("View Virtual Tour (opens new tab)","",$aux);
		
		///// Obtain the City
		$results		=	$this->queryDom($contentTable,"//span[@class='locality']");
		if($results->length	>	0)
			$this->city	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->queryDom($contentTable,"//meta[@property='og:locality']");
			if($results->length	>	0)
				$this->city		=	$results->item(0)->getAttribute('content');
		}
		
		///// Obtain the State
		$results		=	$this->queryDom($contentTable,"//span[@class='region']");
		if($results->length	>	0)
			$this->state	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->queryDom($contentTable,"//meta[@property='og:region']");
			if($results->length	>	0)
				$this->state	=	$results->item(0)->getAttribute('content');
		}

		///// Obtain the Zip
		$results	=	$this->queryDom($contentTable,"//span[@class='postal-code']");
		if($results->length	>	0)
			$this->zip	=	$results->item(0)->nodeValue;
		else{
			$results		=	$this->queryDom($contentTable,"//meta[@property='og:postal-code']");
			if($results->length	>	0)
				$this->zip		=	$results->item(0)->getAttribute('content');
		}
		
		///// Obtain the Unit
		$results	=	$this->queryDom($contentTable,"//div[@class='extended-address']/div[@class='second-line']");
		$this->unit	=	$results->item(0)->nodeValue;

		///// Obtain the Latitude
		$results		=	$this->queryDom($contentTable,"//meta[@property='og:latitude']");
		if($results->length	>	0)
			$this->latitude	=	$results->item(0)->getAttribute('content');
		else					
			$this->latitude	=	$this->LatLong($html,'latitude');
		
		///// Obtain the Longitude
		$results		=	$this->queryDom($contentTable,"//meta[@property='og:longitude']");
		if($results->length	>	0)
			$this->longitude=	$results->item(0)->getAttribute('content');		
		else
			$this->longitude=	$this->LatLong($html,'longitude');
		
		//To Verify Code obtain from spider
		/*$fd = fopen ("html.txt", "w"); 
		fwrite ($fd, $html);
		fclose($fd);
		echo $html;
		die();*/
		preg_match_all('/ZILLOW.LightboxManager.addComponent((.*));/',$html,$addr);
		foreach($addr[1] as $index => $value)
			$resultTemp[]	=	json_decode(str_replace(")","",str_replace("(","",$value)));

		foreach($resultTemp as $index => $row){
			if($row->boundingBox == "#home-facts-comparison")
				$urlMorFacts	=	"http://www.zillow.com".$row->cfg->blocks[0]->url;
		}
		
		return $urlMorFacts;
	}
	
	/******************************
	*
	*	Obtain Detail from a Url. (Principal Search)
	*
	*******************************/
	function getDetailResult($url){
		$html	=	$this->codeHtml($url,false);			
		$html	=	str_replace("\\","",$html);
		$html	=	str_replace("<td>","<td>|",$html);
		
		$contentTable	=	$this->getXpathHtml($html,false);
		$results		=	$this->queryDom($contentTable,"//div[@id='county-link']//a");
		if(!is_null($results->item(0)))
			$this->urlCounty=	$this->obtainCountyURL($results->item(0)->getAttribute('href'));
		
		$results	=	$this->queryDom($contentTable,"//tr[count(td)=2]");

		$detail	=	Array();
		
		foreach ($results as $link)
		{
			$link->nodeValue;
			$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));
		}
		
		$results	=	$this->queryDom($contentTable,"//span[@class='locality']");
//		echo $results->getAttribute('class');
		foreach ($results as $link)
		{
				//echo $link->getAttribute('class');
			$detail[]	=	explode("|",str_replace(":","",$link->nodeValue));
		}

		return json_encode(Array("result" => true, "data" => $this->map($detail)));
	}
	
	/******************************
	*
	*	Get the html code that I will explore
	*
	*******************************/
	function codeHtml($search,$constructUrl=true){
		$curl = curl_init(($constructUrl) ? $this->constructUrl($search) : $search);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1');
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

		//We assign the data to the variable $html
		$html	=	curl_exec($curl);
		
		//Close the Operation cUrl
		//curl_close($curl);
		
		return $html;
	}

	/******************************
	*
	*	Get the Address to Search
	*
	*******************************/
	function constructUrl($search){
	
		$search		=	str_replace("  "," ",$search);
		$search		=	str_replace("   "," ",$search);
		$search		=	str_replace("    "," ",$search);
		$search		=	str_replace("     "," ",$search);
		$search		=	str_replace("      "," ",$search);
		$search		=	str_replace("       "," ",$search);
		$search		=	str_replace("        "," ",$search);
		$search		=	str_replace("         "," ",$search);
		$search		=	str_replace("          "," ",$search);
		$search		=	str_replace(" ","-",$search);
		$principal	=	"http://www.zillow.com/homes/".$search."_rb/";
		
		return $principal;
	}
	
	/******************************
	*
	*	Get the url of level 1 and the exact address.
	*
	*******************************/
	function getXpathHtml($htmlDom,$pregMatch){

		if($pregMatch <> false){
			//We get the code block comments from Zillow that seem this json format
			preg_match_all($pregMatch,$htmlDom,$addr);
			
			//replacing all slashes from \ that interfere with the code
			$htmlDom	=	str_replace('\\','',$addr[1][0]);

		}
		
		$oldSetting	=	libxml_use_internal_errors( true );
		libxml_clear_errors();

		//New DOMDocument
		$dom	=	new DOMDocument();

		//We load it with data from cUrl ($ html)
		@$dom->loadHTML($htmlDom);

		//Uses XPath to Obtain the Object DOM previously obtained
		$xpath = new DOMXPath( $dom );

		libxml_clear_errors();
		libxml_use_internal_errors( $oldSetting );
		libxml_use_internal_errors( $oldSetting );

		return $xpath;		
	}
	
	/******************************
	*
	*	Clear Any folder on the System
	*
	*******************************/
	function clearDirectory($carpeta){
		foreach(glob($carpeta."*") as $archivos_carpeta){
			if(!is_dir($archivos_carpeta)) unlink($archivos_carpeta);
		}
	}
	
	/******************************
	*
	*	Excecute any Query over Xpath
	*
	*******************************/
	function queryDom($xpath,$query){

		//We make the XPath Query
		$arrayToExplode	=	$xpath->query($query);
		
		return $arrayToExplode;
	}
	
	/******************************
	*
	*	Obtains the Sales History from some Properti on Zillow
	*
	*******************************/
	private function salesHistory($tableWithSales){
		$tempSales = Array();$tempSalesCount = 0;
		foreach($tableWithSales as $field){
			if(trim($field[1])=="Sold"){
				$tempSales['data'][]	=	Array(
												"txsales*date$tempSalesCount"		=>	$field[0], 
												"txsales*price$tempSalesCount"		=>	number_format(str_replace(",","",str_replace("$","",$field[2])),2,".",","), 
												"txsales*priceSqft$tempSalesCount"	=>	number_format(str_replace(",","",str_replace("$","",$field[4])),2,".",",")
											);
				$tempSalesCount++;
			}
		}
		//if $tempSalesCount is equal to 0, it's because there are not data and need send a empty array
		if($tempSalesCount	==	0)
			$tempSales['data'][]	=	Array();

		$tempSales['total'] = $tempSalesCount;
		return	$tempSales;
	}

	/******************************
	*
	*	Obtains the Price History from Zillow
	*
	*******************************/
	private function priceHistory($html){
		preg_match_all('/.*asyncLoader.load((.*));/',$html,$addr);
		
		$json = Array();
		foreach($addr[1] as $value)
				$json[] = json_decode(str_replace("(","",str_replace(")","",str_replace(");","",str_replace("asyncLoader.load(","",$value)))),true);
		
		//print_r($json);
		$c=0;
		foreach($json as $key1 => $value1){
			//foreach($value1 as $key => $value){
				if($value1["phaseType"]	==	"scroll"){
					//if($key=="ajaxURL"){
						//if($c==1)
						if($c==2)
							//$priceHistory = "http://www.zillow.com".$value;
							$priceHistory = "http://www.zillow.com".$value1["ajaxURL"];							
						$c++;
					//}
				}
			//}
		}
		if(!empty($priceHistory)){
			$html			=	$this->codeHtml($priceHistory,false);
			$html			=	str_replace("\\","",$html);
			$html			=	str_replace("</td>","|</td>",$html);
			$contentTable	=	$this->getXpathHtml($html,false);
			$results		=	$this->queryDom($contentTable,"//tr[count(td)=7]");
			
			$detail			=	Array();
			foreach ( $results as $link )
			{
				$detail[] = explode("|",str_replace(":","",$link->nodeValue));
			}
			
			$this->salesHistory	=	$this->salesHistory($detail);
			
			$detail = json_encode($detail);
			$detail = json_decode($detail,true);

			$ldate=""; $entryDate="";
			///Extract the Listing Date
			foreach($detail as $rows){
					//echo "<BR>|".$rows[1]."| ---- ".$rows[0];
					if(trim($rows[1]) == "Listed for sale"){
						$ldate=$rows[0];
						break;
					}
			}
			
			///Extract the Entry Date
			$c=0;
			foreach($detail as $rows){
					//echo "<BR>|".$rows[1]."| ---- ".$rows[0];
					if($c==0){
						$entryDate=$rows[0];
						break;
					}
			}
			if(empty($ldate))
				$ldate=$entryDate;
			
			$this->listingDate	=	empty($ldate)		?	""	:	date("m/d/Y",strtotime($ldate));
			$this->entryDate	=	empty($entryDate)	?	""	:	date("m/d/Y",strtotime($entryDate));
		}
	}

	/******************************
	*
	*	Create thumbnail from any image
	*
	*******************************/
	private function createThumbs( $pathToImages, $pathToThumbs, $thumbWidth ) {
		// open the directory
		$dir = opendir( $pathToImages );

		// loop through it, looking for any/all JPG files:
		while (false !== ($fname = readdir( $dir ))) {
			if(!empty($this->idMultiple)){
				$aux	=	explode("-",$fname);
				if($aux[0]	!=	($this->idMultiple))
					$flag	=	false;
				else
					$flag	=	true;
			}else
				$flag	=	true;
			
			if($flag){
				// parse path for the extension
				$info	=	pathinfo($pathToImages . $fname);
				// continue only if this is a JPEG image
				if ( strtolower($info['extension']) == 'jpg' ){
					//echo "Creating thumbnail for {$fname} <br />";

					// load image and get image size
					$img	=	imagecreatefromjpeg( "{$pathToImages}{$fname}" );
					$width	=	imagesx( $img );
					$height	=	imagesy( $img );

					// calculate thumbnail size
					$new_width	=	$thumbWidth;
					$new_height	=	floor( $height * ( $thumbWidth / $width ) );

					// create a new tempopary image
					$tmp_img	=	imagecreatetruecolor( $new_width, $new_height );

					// copy and resize old image into new image 
					imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

					// save thumbnail into a file
					imagejpeg( $tmp_img, "{$pathToThumbs}".strtolower($fname)."" );
				}
			}
		}
		// close the directory
		closedir( $dir );
	}

	/******************************
	*
	*	Save Fisicaly Images on RealtyTask
	*
	*******************************/
	private function saveImage($url,$path) {
		$c = curl_init();
		curl_setopt($c,CURLOPT_URL,$url);
		curl_setopt($c,CURLOPT_HEADER,0);
		curl_setopt($c,CURLOPT_RETURNTRANSFER,true);
		$s = curl_exec($c);
		curl_close($c);
		$f = fopen($path, 'wb');
		$z = fwrite($f,$s);
		if ($z != false) 
			return true;
		return false;
	}

	/******************************
	*
	*	Extract Url Images from Zillow
	*
	*******************************/
	private function urlImages($xpath){
		$this->userid = $_COOKIE['datos_usr']['USERID'];
		$imagenes = $xpath->query( "//div[@id='photo-view-container']//div[@id='home-image-preview']//div[@id='hip-content']//img");
		$c=0;$ArrayImages=Array();
		foreach ( $imagenes as $link )
		{
			if($c==0)
				$ArrayImages[] = $link->getAttribute( 'src' );
			$aux = $link->getAttribute( 'href' );
			if(!empty($aux))
				$ArrayImages[] = $link->getAttribute( 'href' );
			$c++;
		}
		
		$c=0;$ArrayImagesFinal=Array();
		$dirImages = $_SERVER["DOCUMENT_ROOT"]."/propertiesImages/".$this->userid."/";
		
		//Create User Directory for properties images
		if(!is_dir($dirImages))
			mkdir($dirImages);
		
		//Create User temp Directory for properties images
		if(!is_dir($dirImages."temp/"))
			mkdir($dirImages."temp/");
		
		//Create User thumb Directory for properties images
		if(!is_dir($dirImages."thumb/"))
			mkdir($dirImages."thumb/");
		
		//Create User Edit Directory for new Upload properties images
		if(!is_dir($dirImages."edit/"))
			mkdir($dirImages."edit/");
		
		//Create User EdiThumb Directory for new Upload properties images
		if(!is_dir($dirImages."edithumb/"))
			mkdir($dirImages."edithumb/");
		
		if(!empty($this->idMultiple))	//Control for Multiple Import, saving all images in one folder for this case see propertyIncludedMultiple.php
			$identifyMultiple	=	$this->idMultiple."-";
		else{
			$identifyMultiple	=	"";
			//if not are multiple import need to clear this folders....
			//Clear all temp directories for new images on a new search
			$this->clearDirectory($dirImages."temp/");
			$this->clearDirectory($dirImages."thumb/");
		}
		foreach($ArrayImages as $index => $image){
			if(!empty($image)){
				$c++;
				$aux=explode(".",$image);
				$position=count($aux)-1;
				$this->saveImage($image,$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$this->images['temp'][] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$this->images['thumb'][] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."thumb/$identifyMultiple$c.$aux[$position]");
			}
		}
		$this->createThumbs($dirImages."temp/",$dirImages."thumb/",96);
	}
	/******************************
	*
	*	Extract the Latitude and Longitude Fron Case Zillow
	*
	*******************************/
	private function LatLong($html,$type){
		preg_match_all('/.*asyncLoader.load((.*));/',$html,$addr);
		
		$json = Array();
		foreach($addr[1] as $value)
				$json[] = json_decode(str_replace("(","",str_replace(")","",str_replace(");","",str_replace("asyncLoader.load(","",$value)))),true);
			
		return	$json[1][$type];
	}
	
	/******************************
	*
	*	Mapea the data to the correct fields
	*
	*******************************/
	private function map($data){
		/**************************
		*	Order Data Recibed
		**************************/
		$standarData = Array(
			'parcel #' => 'parcel', 'beds' => 'beds', 'baths' => 'baths', 'mls #' => 'mls', 'sqft' => 'sqft', 'lot' => 'lot', 'unit' => 'unit', 'type' => 'type', 'pool' => 'pool', 
			'waterfront' => 'waterfront', 'year built' => 'ybuilt', 'legal description' => 'legald', 'per floor sqft' => 'perfsqft', 'lot depth' => 'lotd', 'lot width' => 'lotw', 
			'last sold' => 'lsold', 'last remodel year' => 'lremodely', 'unit cnt' => 'unit', 'total rooms' => 'trooms', '# stories' => 'stories', 'school district' => 'schoold',
			'county' => 'county', 'on zillow' => 'dom', 'cooling' => 'ac'
		);

		/**************************
		*	Extract Data for Public Record
		**************************/
		$publicR = Array();
		foreach($data as $row){
			$c=0;	//With this variable, I control to obtain the values ??of either the Combined or the Public Record. 1 is Combined, 2 is Public Record
			foreach($row as $field){
				if($c==0)	//Get the firs position to obtain the field name
					$nameField=$standarData[strtolower(trim($field))];

				if($c==2){	//Get the second position to obtain the first value for this field
					if(!empty($nameField) and ($field <> "--" and $field <> ""))
						$publicR[$nameField]=trim($field);
				}
				$c++;
			}
		}

		/**************************
		*	Extract Data for Combined Result
		**************************/		
		$residential = Array();
		foreach($data as $row){
			$c=0;	//With this variable, I control to obtain the values ??of either the Combined or the Public Record. 1 is Combined, 2 is Public Record
			foreach($row as $field){
				if($c==0)	//Get the firs position to obtain the field name
					$nameField=$standarData[strtolower(trim($field))];

				if($c==1){	//Get the first position to obtain the first value for this field
					if(!empty($nameField) and ($field <> "--" and $field <> ""))
						$residential[$nameField]=trim($field);
				}
				$c++;
			}
		}

		/**************************
		*	Standarize status camp
		**************************/
		switch(trim($this->aditional['status'])){
			case	"Not for Sale":	$status	=	"CC";	break;
			case	"Make me Move":	$status	=	"CC";	break;
			case	"For Sale":		$status	=	"AA";	break;
			case	"For Rent":		$status	=	"BB";	break;
			case	"Sold":			$status	=	"CS";	break;
			case	"By Owner":		$status	=	"DD";	break;
			case	"Bank Owned":	$status	=	"EE";	break;
			case	"Foreclosure":	$status	=	"FF";	break;
			case	"Pending":		$status	=	"GG";	break;
			default:				$status	=	"CC";	break;
		}
		/**************************
		*	Standarize status type
		**************************/
		if(empty($publicR['type']) or $publicR['type'] == "--")
			$type = $residential['type'];
		else
			$type = $publicR['type'];

		switch($type){
			case	"Single Family":		$typePT	=	"01";	break;
			case	"Townhouse":			$typePT	=	"04";	break;
			case	"Condo":				$typePT	=	"04";	break;
			case	"Cooperative":			$typePT	=	"04";	break;
			case	"Multi Family":			$typePT	=	"08";	break;
			case	"Vacant Land":			$typePT	=	"00";	break;
			case	"Mobile / Manufactured":$typePT	=	"02";	break;
			default:						$typePT	=	"99";	break;
		}

		/**************************
		*	Data for Proterty Table
		**************************/
		unset($propertyArray);
		$status=="CC" ? $propertyArray=$residential : $propertyArray=$publicR;
		$propertyFields = Array(
			'txproperty*address' => $this->street,
			'txproperty*addressfull' => $this->aditional['address']." ".$this->zip,
			'txproperty*zip' => $this->zip,
			'txproperty*city' => $this->city,
			'txproperty*unit' => trim(str_replace("UNIT","",$this->unit)),
			//'txproperty*county' => trim($propertyArray['county']),	// OLD, Next line New
			'txproperty*county' => empty($psummaryArray['county'])	?	trim($residential['county'])	:	trim($psummaryArray['county']),
			//'txproperty*yrbuilt' => $propertyArray['ybuilt'],
			'cbproperty*xcode' => $typePT,
			'cbproperty*state' => $this->state,
			'txproperty*latitude' => $this->latitude,
			'txproperty*longitude' => $this->longitude,
			'cbproperty*status' => $status
		);

		$status=="CC" ? $psummaryArray=$residential : $psummaryArray=$publicR;
		/**************************
		*	Data for Psummary Table
		**************************/
		////*Aditional Process For Camp Sale Price And Sale Date*///
		if(trim($publicR['lsold'])== "" or trim($publicR['lsold']) == "--")
			$psummaryLsold = explode("for",$residential['lsold']);
		else
			$psummaryLsold = explode("for",$publicR['lsold']);
		$psummarySaleDate = $this->saleDate($psummaryLsold[0]);
		////*Aditional Process For Camp Lot*///
		$psummaryTsqft=explode("sq ft",$psummaryArray['lot']);
			$residentialTsqft=explode("sq ft",$residential['lot']);
		$psummaryFields = Array(
			'txpsummary*beds' 		=> empty($psummaryArray['beds']) 			?	$residential['beds']	:	$psummaryArray['beds'],
			'txpsummary*bath'		=> empty($psummaryArray['baths'])			?	$residential['baths']	:	$psummaryArray['baths'],
			'txpsummary*lsqft'		=> trim(str_replace(",","",$psummaryArray['sqft']))==""	?	str_replace(",","",trim($residentialTsqft[0]))	:	str_replace(",","",$psummaryArray['sqft']),
			'txpsummary*bheated'	=> str_replace(",","",$psummaryArray['sqft']),
			'txpsummary*folio'		=> $psummaryArray['parcel'],
			'txpsummary*yrbuilt'	=> empty($psummaryArray['ybuilt'])			?	$residential['ybuilt']	:	$psummaryArray['ybuilt'],
			'cbpsummary*pool'		=> $psummaryArray['pool']	=== "Yes"		?	"Y"						:	"N",
			'cbpsummary*ac'			=> !empty($psummaryArray['ac'])				?	"Y"						:	!empty($residential['ac'])	?	"Y"	:	"N",
			'txpsummary*saledate'	=> $psummarySaleDate,
			'txpsummary*saleprice'	=> number_format(str_replace("$","",str_replace(",","",trim($psummaryLsold[1]))),2,".",","),
			'txpsummary*stories'	=> $psummaryArray['stories'],
			'txpsummary*legal'		=> $psummaryArray['legald'],
			'txpsummary*tsqft'		=> str_replace(",","",trim($psummaryTsqft[0])),
			'cbpsummary*waterf'		=> $psummaryArray['waterfront']	=== "Yes"	?	"Y"						:	"N"
		);
		/**************************
		*	Data for Residential Table
		**************************/
		if($status==="AA" or $status==="BB" or $status==="DD" or $status==="EE" or $status==="FF" or $status==="GG"){
			$residentialTsqft=explode("sq ft",$residential['lot']);
			$residentialFields	=	Array(
				'txmlsresidential*bath'			=>	$residential['baths'],
				'txmlsresidential*beds'			=>	$residential['beds'],
				'txmlsresidential*folio'		=>	$residential['parcel'],
				'txmlsresidential*yrbuilt'		=>	$residential['ybuilt'],
				'txmlsresidential*dom'			=>	$residential['dom'],
				'txmlsresidential*apxtotsqft'	=>	str_replace(",","",trim($residentialTsqft[0])),
				'txmlsresidential*lsqft'		=>	str_replace(",","",trim($residentialTsqft[0])),
				'txmlsresidential*ldate'		=>	$this->listingDate,
				'txmlsresidential*mlnumber'		=>	$residential['mls'],
				'txmlsresidential*lprice'		=>	number_format($this->aditional['price'],2,".",","),
				'txmlsresidential*remark'		=>	$this->remark,
				'cbmlsresidential*waterf'		=>	$residential['waterfront']	=== "Yes"	?	"Y"	:	"N",
				'cbmlsresidential*pool'			=>	$residential['pool']	=== "Yes"	?	"Y"	:	"N",
				'txmlsresidential*entrydate'	=>	$this->entryDate
			);
		}else{
			$residentialFields	=	Array(
				'txmlsresidential*folio'		=>	$residential['parcel']
			);
		}

		/**************************
		*	Data for Sales Table
		**************************/
		//$this->salesHistory;
		
		/**************************
		*	Data for Url Images
		**************************/
		//$this->images;
		
		$foreclosure = Array(
			'txpendes*folio'	=>	$psummaryArray['parcel']
		);
		
		$mortgage = Array(
			'txmortgage*folio'	=>	$psummaryArray['parcel']
		);
		
		$allData = Array(
			'property'		=>	$propertyFields,
			'psummary'		=>	$psummaryFields,
			'residential'	=>	$residentialFields, 
			'sales'			=>	$this->salesHistory, 
			'foreclosure'	=>	$foreclosure, 
			'mortgage'		=>	$mortgage,
			'images'		=>	$this->images,
			'aditionals'	=>	Array(
									"countyUrl" =>	$this->urlCounty,
									"urlDetail"	=>	$this->urlDetail
								)
		);
		
		if(!empty($residential['mls'])){
			$AuxRealtor	=	new Realtor();
			$c=0;
			do{
				$jsonResultRealtor	=	json_decode($AuxRealtor->ObtainUrlDetail($residential['mls']));	//Convert json String toObject Json
				$c++;
			}while($jsonResultRealtor->url == false and $c<=5);
			$allData['aditionals']['urlMore']	=	$jsonResultRealtor->url;
		}

		return $allData;
		
		
		//Add fields standar
		$newData['street']		=	$this->street;
		$newData['latitude']	=	$this->latitude;
		$newData['longitude']	=	$this->longitude;
	}
	
	private function saleDate($date){
		$date	=	trim($date);
		if(empty($date))
			return "";
		$aux = explode(" ",trim($date));
		$meses = Array("Jan" => '01',"Feb" => "02","Mar" => "03","Apr" => "04","May" => "05","Jun" => "06","Jul" => "07","Aug" => "08","Sep" => "09","Oct" => "10","Nov" => "11","Dic" => "12");
		return $aux[1].$meses[$aux[0]]."01";
	}
	
	/******************************
	*
	*	Convert the Zillow url to the real Url
	*
	*******************************/
	private function obtainCountyURL( $url ) {
	    $res = array();
	    $options = array( 
	        CURLOPT_RETURNTRANSFER => true,     // return web page 
	        CURLOPT_HEADER         => false,    // do not return headers 
	        CURLOPT_FOLLOWLOCATION => true,     // follow redirects 
	        CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1", // who am i 
	        CURLOPT_AUTOREFERER    => true,     // set referer on redirect 
	        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect 
	        CURLOPT_TIMEOUT        => 120,      // timeout on response 
	        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects 
	    ); 
	    $ch      = curl_init( $url ); 
	    curl_setopt_array( $ch, $options ); 
	    $content = curl_exec( $ch ); 
	    $err     = curl_errno( $ch ); 
	    $errmsg  = curl_error( $ch ); 
	    $header  = curl_getinfo( $ch ); 
	    curl_close( $ch ); 

	    //$res['content'] = $content;     
	    //$res['url'] = $header['url'];
	    //return $res; 
	    return $header['url'];
	}
	
	/******************************
	*
	*	Second Process To Search and Extract the Url of comparables
	*
	*******************************/
	function getUrlComparables($url,$latitude,$longitude){
		$factorLat=0.0330;
		$factorMin=0.0729;
		$latMin=$latitude-($factorLat);
		$latMax=$latitude+($factorLat);
		$lonMin=$longitude-($factorMin);
		$lonMax=$longitude+($factorMin);
		$rect=str_replace('.','',"$lonMin,$latMin,$lonMax,$latMax");
		$array=explode('/',$url);
		$zpid=str_replace('_zpid','',$array[count($array)-2]);
		$urlComps="http://www.zillow.com/search/GetCompsResults.htm?zpid=$zpid&mm=undefined&rect=$rect&sort=default&desc=true";
		
		$html	=	$this->codeHtml($url,false);
		
		$contentTable	=	$this->getXpathHtml($html,false);
		
		//echo $html;
		$results = $this->queryDom($contentTable,"//div[@class='for-sale comps generic-box yui3-widget-stdmod']//li[@class='carrot']//a");
		$urlCompsAct='';
		foreach ( $results as $link ){
			$urlCompsAct = "http://www.zillow.com".$link->getAttribute( 'href' );
			break;
		}
		if($urlCompsAct!=''){
			$urlCompsAct.="#/homes/for_sale/fsba_lt/days_sort/$latMax,$lonMax,$latMin,$lonMin".'_rect/15_zm/';
		}
		
		return array('urlComp'=>$urlComps,'urlAct'=>$urlCompsAct);
	}
	
	/******************************
	*
	*	Third Process To Search and Extract comparables
	*
	*******************************/
	function getComparables($url){
		$json = $this->codeHtml($url,false);
		$comparables=json_decode($json,true);
		
		//print_r($comparables);
		$html			=	$comparables['html'];
		$html			=	str_replace("\\","",$html);
		$html			=	str_replace("</td>","|</td>",$html);
		$contentTable	=	$this->getXpathHtml($html,false);
		$results		=	$this->queryDom($contentTable,"//tr[count(td)=10]");
		
		$arrayComp			=	Array();
		foreach ( $results as $link )
		{
			$arrayComp[] = explode("|",str_replace(":","",$link->nodeValue));
		}
		//print_r($arrayComp);
		
		$arrayReturn=Array();
		$status='CC';
		$j=0;
		for($i=0;$i<count($arrayComp);$i++){
			$arrayReturn[$j]['status']		=	$status;
			$arrayReturn[$j]['address']		=	$arrayComp[$i][0];
			$arrayReturn[$j]['distance']	=	$arrayComp[$i][9];
			
			$arrayReturn[$j]['saledate']	=	$this->saleComparable($arrayComp[$i][2]);
			$arrayReturn[$j]['saleprice']	=	$this->replaceString($arrayComp[$i][1],array('$',','));
			$arrayReturn[$j]['lprice']		=	0;
			$arrayReturn[$j]['dom']			=	0;
			
			$arrayReturn[$j]['bath']		=	$arrayComp[$i][4]!='--' ? $arrayComp[$i][4] : 0;
			$arrayReturn[$j]['bed']			=	$arrayComp[$i][3]!='--' ? $arrayComp[$i][3] : 0;
			$arrayReturn[$j]['lsqft']		=	$arrayComp[$i][5]!='--' ? $this->replaceString($arrayComp[$i][5],array(',')) : 0;
			$arrayReturn[$j]['tsqft']		=	$arrayComp[$i][6]!='--' ? $this->replaceString($arrayComp[$i][6],array(',')) : 0;
			$arrayReturn[$j]['yearbuilt']	=	$arrayComp[$i][7]!='--' ? $arrayComp[$i][7] : 0; 
			$arrayReturn[$j]['pricesqft']	=	$arrayComp[$i][8]!='--' ? $this->replaceString($arrayComp[$i][8],array('$',',')) : 0;
			$arrayReturn[$j]['latitude']	=	$this->convertPosition($comparables['mapResults'][$i][1]);
			$arrayReturn[$j]['longitude']	=	$this->convertPosition($comparables['mapResults'][$i][2]);
			$j++;
		}
		return $arrayReturn;  
	}
	
	function getComparablesAct($url){
		$arrayReturn=Array();
		$status='A';
		$html	=	$this->codeHtml($url,false);
		//echo $html;
		$contentTable	=	$this->getXpathHtml($html,false);
		
		$results = $this->queryDom($contentTable,"//ul[@id='search-results']//li[@class='search-result clearfix rollable featured-listing']");
		$j=0;
		foreach ( $results as $link ){
			$arrayReturn[$j]['status'] = $status;
			$arrayReturn[$j]['latitude']	=	$this->convertPosition($link->getAttribute('latitude'));
			$arrayReturn[$j]['longitude']	=	$this->convertPosition($link->getAttribute('longitude'));
			//Extract address
			$href = $link->getElementsByTagName('a');
			foreach($href as $div){
				//echo '<br>'.$div->getAttribute('class');
				if($div->getAttribute('class')=='hdp-link'){
					$arrayReturn[$j]['address']=$div->nodeValue;
				}
			}
			$href = $link->getElementsByTagName('li');
			foreach($href as $li){
				//echo '<br>'.$li->getAttribute('class');
				if($li->getAttribute('class')=='type-forSale'){
					$price = $link->getElementsByTagName('span');
					foreach($price as $span){
						if($span->getAttribute('class')=='price'){
							$arrayReturn[$j]['saledate']	=	'';
							$arrayReturn[$j]['saleprice']	=	0;
							$arrayReturn[$j]['lprice']=$this->replaceString($span->nodeValue,array('$',','));
						}
					}
				}
				if($li->getAttribute('class')=='prop-cola'){
					//$imagesInformation=extractData('"LST_Media"',$htmlText,"],");
					$arrayReturn[$j]['bath']		=	$this->extractData('Baths:',$li->nodeValue,"Sqft")!='--' ? $this->extractData('Baths:',$li->nodeValue,"Sqft") : 0;
					$arrayReturn[$j]['bed']			=	$this->extractData('Beds:',$li->nodeValue,"Baths")!='--' ? $this->extractData('Beds:',$li->nodeValue,"Baths") : 0;
					$arrayReturn[$j]['lsqft']		=	$this->extractData('Sqft:',$li->nodeValue,"Lot")!='--' ? $this->replaceString($this->extractData('Sqft:',$li->nodeValue,"Lot"),array('$',',')) : 0;
					$arrayReturn[$j]['tsqft']		=	$this->extractData('Lot:',$li->nodeValue,"<br>")!='-' ? $this->replaceString($this->extractData('Lot:',$li->nodeValue,"<br>"),array('$',',')) : 0;
					
					$arrayReturn[$j]['pricesqft']	=	0;
				}
				if($li->getAttribute('class')=='prop-colb'){
					$arrayReturn[$j]['dom']			=	$this->extractData('Days on Zillow:',$li->nodeValue,"Built")!='--' ? $this->extractData('Days on Zillow:',$li->nodeValue,"Built") : 0;
					$built=$this->extractData('Built:',$li->nodeValue," ");
					$built=substr($built,0,4);
					$arrayReturn[$j]['yearbuilt']	=	substr($built,0,2)!='--' ? $built : 0;
				}
			}
			//break;
			$arrayReturn[$j]['distance'] = 0;
			$j++;
		}
		return $arrayReturn;
	}
	
	private function replaceString($string,$replaces){
		$returnString = $string;
		foreach($replaces as $replace){
			$returnString = str_replace($replace,'',$returnString);
		}
		return $returnString;
	}
	private function saleComparable($date){
		$date	=	trim(str_replace(',','',$date));
		if(empty($date))
			return "";
		$aux = explode("/",trim($date));
		return $aux[2].$aux[1].$aux[0];
	}
	private function convertPosition($num){
		$num=trim($num);
		if($num[0]=='-'){
			return substr($num,0,3).'.'.substr($num,3);
		}else{
			return substr($num,0,2).'.'.substr($num,2);
		}
	}
	private function extractData($find,$text,$finddelimiter){
		$pos = strpos($text, $find);
		$data = substr($text,$pos+strlen($find)); 
		$pos = strpos($data, $finddelimiter);
		$data = substr($data,1,$pos-1); 
		//$data = str_replace('"',"",$data);
		//echo '<br>'.$data;
		return trim($data);
	}
}
?>