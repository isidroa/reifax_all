<?php

require('../../properties_conexion.php');
require('../../includes/globalFunction.php');
require('class.baseImport.php');
conectar();

/*******************
*	Correct Address
*******************/
//	$_POST['address']	=	str_replace($value,"",$_POST['address']);


if($_POST['nextPage']!=''){
	require('class.trulia.php');
	$Trulia = new Trulia(1);
	
	$_POST['nextPage']=decrypt($_POST['nextPage'],$keys['trulia']);
	if($_POST['order']){
		$sort=array(
			'feacture' 	=> 	'',
			'newest' 	=> 	'/date;d_sort/',
			'price'		=> 	'/price;a_sort/',
			'price1'	=> 	'/price;d_sort/',
			'address'	=> 	'/address;d_sort/',
			'beds' 		=> 	'/beds;d_sort/',
			'baths' 	=> 	'/baths;d_sort/',
			'sqft' 		=> 	'/sqft;d_sort/',
			'PropertyType' 	=>	'/type;d_sort/'
		);
		$_POST['nextPage']=preg_replace('/\d+_p/','',$_POST['nextPage']);
		$_POST['nextPage']=preg_replace('/\w+;\w_sort\//','',$_POST['nextPage']);
		$_POST['nextPage'].=$sort[$_POST['order']];
	}
	else{
		$_POST['nextPage']=preg_replace('/\d+_p/',$_POST['page'].'_p',$_POST['nextPage']);
	}
	
	$jsonResult = $Trulia->getListResultMobile(NULL,$_POST); //For Sale
	$jsonResult['nextPage']=encrypt($jsonResult['nextPage'],$keys['trulia']);
}
else{
	require('class.zillow.php');
	$Zillow =new Zillow();
	$c=0;
	$filter=json_decode($_COOKIE['searchFilter'],true);
	
	
	$options->rect	=$filter['rect'];
	$options->ht	=$filter['ht'];
	$options->ba	=$filter['baths_min']?$filter['baths_min']:'';
	$options->bd	=$filter['beds_min']?$filter['beds_min']:'';
	$options->lt	=$filter['lt'];
	$options->status=$filter['status'];
	$options->pmf	=$filter['pmf'];
	$options->pf	=$filter['pf'];
	$options->bd	=$filter['beds_min']?$filter['beds_min']:'';
	$options->sf	=($filter['lot_min']?$filter['lot_min']:'').','.($filter['lot_max']?$filter['lot_max']:'');
	$options->pr	=($filter['price_min']?$filter['price_min']:'').','.($filter['price_min']?$filter['price_min']:'');
	$options->yr	=($filter['built_min']?$filter['filter']:'').','.($filter['built_min']?$filter['built_min']:'');
	
	do{
		$jsonResult = $Zillow->getListJsonStandar($options,$_POST['page']);
		$c++;
	}while(!$jsonResult['numberResult'] && $c<5);
}
echo json_encode(array('success' => true, 'data' => $jsonResult));
?>