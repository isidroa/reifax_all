<style>
#overviewSearch >div >div{
	background:url(/resources/img/backgroundListSearch.png);
}

#containerOverview .content{
	float: none;
	margin: 10px auto;
	min-width:625px;
	max-width:1024px;
	width: 85%;
}

#containerOverview .header,#containerOverview .contact-agent{
	float:left;
	padding:8px;
	width: 100%;
	height: 270px;
	border: solid 1px #ccc;
	background:#FFF;
}
#containerOverview .contact-agent{
	height: 84px;margin-top: 8px;
}
#containerOverview .contact-agent div{
	float:left;
}
.content-headerDetails{
	float:left;
	margin-right:18px;
	width:40%;
}
#containerOverview .contact-agent div:first-child{
	width:50px;
	text-align:center;
}
#containerOverview .contact-agent div img{
	padding:4px;
	border:#090;
	height:50px;
}
#containerOverview .contact-agent div{
	font-size:12px;
	font-weight:400;
}

#containerOverview .container-img{
	float:left;
	margin-left:10px;
	width: 0;
	height: 396px;
	border: solid 1px #ccc;
	background:#FFF;
}
#containerOverview .container table,#containerOverview  .container h3{
	padding:8px;
}

#containerOverview table{
	width:100%;
}
#containerOverview h3{
	background: none repeat scroll 0 0 #FFFFFF;
	color: #8AB420;
	font-family: Arial,Helvetica,Tahoma,Verdana,sans-serif;
	font-weight: bold;
	padding: 0;
	text-transform: capitalize;
	font-size: 20px;
	border-bottom:#8AB420 solid 2px;
	padding:8px;
}
#containerOverview .header h3 p{
	font-size: 14px;
	color: #005C83;
	padding:0;
}
#containerOverview .header tr td:first-child{
	float: left;
	font-weight: bold;
	height: auto;
	margin-left: 5px;
	padding: 6px 0;
	width: 130px;
	font-size:13px;
}
#containerOverview .container{  
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
	float: left;
	height: auto;
	margin: 10px 0 0;
	width: 100%;
}
#containerOverview td{
	font-size:13px;
}
#containerOverview .container .title{
	width:130px;
	font-weight:bold;
}
#containerOverview .container .describe{
	width:200px;
}
#containerOverview .container:last-child{
	margin-bottom:20px;
}
#containerOverview .showcase{
	margin-top:5px;
}
.showcase-content-container{
	padding-top:0px;
}
.btn-property{
	background: none repeat scroll 0 0 #F2F2F2;
	border: 1px solid #333333;
	border-radius: 2px 2px 2px 2px;
	display: block;
	float: left;
	height: 16px;
	margin-left: 4px;
	padding: 5px;
	width: 16px;
}
.btn-property i{
	background-size:23px auto !important;
}
#containerOverview .container .thead{
	width:auto;
}
.contact-agent h5{
	padding:0px;
	margin:2px 0px 0px 8px;
}
#containerOverview .menu{
	top:20px;
	margin-left: -40px;
	position: relative;
	width: 25px;
	position:absolute;
}
#containerOverview .menu .btn{  
	float: left;
 	height: 32px;
  	margin: 1px 1px 13px;
 	width: 32px;
}
#containerOverview .menu .btn:hover{
	padding: 0;
}
.container-map{
	width:100%;
	height:300px;
	margin-bottom:20px !important;
}
/***********
stye for galery
***********************/
.showcase-content > img {
  height: 100% !important;
}
.showcase-thumbnail-container {
  background-color: rgba(0, 0, 0, 0.3);
  bottom: 0;
  position: absolute;
}
.showcase-thumbnail-button-backward .showcase-thumbnail-horizontal, .showcase-thumbnail-button-forward .showcase-thumbnail-horizontal {
  margin-bottom: 25px;
  margin-top: 25px;
}
</style>

<div id="containerOverview">
	<div class="content">    
    	<article class="container container-map" style="display:none">
            <div id="SysRt.globalVar.OS.map">
            </div>
	   	</article>

    	<div class="content-headerDetails">
        	<div class="menu">
                <a href="#" class="btn icon-map" title="Show/ Hide Map" data-id="map">
                    <span></span>
                </a>
                <a href="#" data-id="analyze" class="btn icon-analisys" title="Analisys Property">
                    <span></span>
                </a>
                <a href="#" data-id="listing" class="btn icon-listing" title="Add To Listings">
                    <span></span>
                </a>
                <a href="#" data-id="offert" class="btn icon-offert" title="Add To Offers">
                    <span></span>
                </a>
            </div>
            <article class="header" >
            	
                <div>
                    <h3>
                    </h3>
                    <table>
                    </table>
                    
                </div>
            </article>
            <article class="contact-agent">
                <div>
                	<img data-content="agent-img" />
                </div>
                <div>
                    <h5 class="agent">
                    	<p>Agent: <span data-content="Agent"></span></p>
                    	<p data-content="AgentPh"></p>
                    </h5>
                    <h5 class="broker">
                    	<p data-content="Broker"></p>
                    	<p data-content="AgentPh2"></p>
                    </h5>
                 </div>
            </article>
        </div>  
		<article class="container-img">
			<div class="showcase">
			</div>
		</article>
        <!--
       <article class="container container-map" style="display:none">
			<h3>Map</h3>
            <div id="SysRt.globalVar.OS.map">
            </div>
	   </article>//-->
		<article class="container">
			<h3>Property Details</h3>
			<table>
				<tr>
					<td class="title">Listing Date:</td>
					<td data-content="dateL" class="describe"></td>
					<td class="title">Listing Price $:</td>
					<td data-content="price" class="describe"></td>
					<td class="title">Days on Market:</td>
					<td data-content="dom" class="describe"></td>
				</tr>
				<tr>
					<td class="title">Living Area:</td>
					<td data-content="larea" class="describe"></td>
					<td class="title">Gross Area:</td>
					<td data-content="garea" class="describe"></td>
					<td class="title">Stories:</td
					><td data-content="stories" class="describe"></td>
				</tr>
				<tr>
					<td class="title">Half Bathrooms:</td>
					<td data-content="bathrooms" class="describe"></td>
					<td class="title">Waterfront:</td>
					<td data-content="waterf" class="describe"></td>
					<td class="title">Pool:</td>
					<td data-content="pool" class="describe"></td>
				</tr>
				<tr>
					<td class="title">Folio #:</td>
					<td data-content="folio" class="describe"></td>
				</tr>
			</table>
	   </article>
       <article class="container" data-container="sales" style="display:none">
			<h3>Sales</h3>
			<table>
				<tr>
					<td class="title thead">Date</td>
					<td class="title thead">Price $</td>
					<td class="title thead">Price/Sqft</td>
				</tr>
			</table>
	   </article>
		<article class="container">
			<h3>Property Description</h3>
				<div style="float:left; width:160px; margin-left:8px; font-weight:bold;"><label>Remarks:</label></div>
				<div style="float: left;padding: 0 8px 8px; text-align: justify;" data-content="remarks">
				</div>
                <div class="clear">
                </div>
				<div style="float:left; width:160px; margin-left:8px; font-weight:bold;"><label>Legal Description:</label></div>
				<div style="float: left;padding: 0 8px 8px; text-align: justify;" data-content="remarks1">
				</div>
	   </article>
	</div>
</div>
<script>
	var property=SysRt.globalVar.OS;
	var templateDetailsHeader=	'<tr>'+
									'<td></td><td></td>'+
								'</tr>';
	var templateDetails='<article>'+
							'<h3></h3>'+
							'<table></table>'+
						'</article>';
	var templateDetailsBody='<tr>'+
								'<td class="title"></td><td class="describe"></td>'+
								'<td class="title"></td><td class="describe"></td>'+
								'<td class="title"></td><td class="describe"></td>'+
								'<td class="title"></td><td class="describe"></td>'+
							'</tr>';
	var templateSales='<tr>'+
								'<td class="describe" data-content="date"></td>'+
								'<td class="describe" data-content="price"></td>'+
								'<td class="describe" data-content="ps"></td>'+
							'</tr>';
	var templateImg='<div class="showcase-slide">'+
						'<div class="showcase-content">'+
							'<img alt="Loading" />'+
						'</div>'+
						'<div class="showcase-thumbnail">'+
							'<img alt="Loading" width="96px" height="64px"/>'+
							'<div class="showcase-thumbnail-caption"></div>'+
							'<div class="showcase-thumbnail-cover"></div>'+
						'</div>'+
					'</div>';
	//loading data
	$('#containerOverview .header h3').append(property.property.address+' '+property.property.unit+'<p>'+property.property.city+' '+property.property.zip+'</p>');
	var dataToHeader={
			'Status:'		: 'status',
			'ML#:'			: 'MLNumber',
			'Property Type:': 'CCodeD',
			'Year Built:'	: 'YrBuilt',
			'Bedrooms:'		: 'Beds',
			'Bathrooms:'	: 'Bath',
			'Lot Size:'		: 'TSqft'
		}
	if(property.property.status=='AA'){
		var otherData={
			'folio'		:'mlsresidential.Folio',
			'larea'		:'mlsresidential.Lsqft',
			'garea'		:'mlsresidential.Lsqft',
			'price'		:'mlsresidential.Lprice',
			'dateL'		:'mlsresidential.Ldate',
			'waterf'	:'mlsresidential.WaterF',
			'pool'		:'mlsresidential.Pool',
			'remarks'	:'mlsresidential.Remark',
			'remarks1'	:'mlsresidential.Remark1',
			'stories'	:'property.Stories',
			'Bathrooms'	:'mlsresidential.Bath',
			'dom'		:'mlsresidential.Dom',
			'MLNumber'	:'mlsresidential.MLNumber',
			'Agent'		:'agent.agent',
			'AgentPh'	:'agent.phone',
			'Broker'	:'agent.broker',
			'AgentPh2'	:'agent.phoneBroker',			
			'Bedrooms'	:'mlsresidential.Beds'
		};
		if(property.mlsresidential.Remark){
			property.mlsresidential.Remark=property.mlsresidential.Remark.replace(/\\/g,"");
		}
		property.property.MLNumber=property.mlsresidential.MLNumber;
		if(property.mlsresidential.Remark1){
			property.mlsresidential.Remark1=property.mlsresidential.Remark1.replace(/\\/g,"");
		}
	}
	else if(property.property.status=='CS' || property.property.status=='For Sale'){
		var otherData={
			'folio'		:'mlsresidential.Folio',
			'larea'		:'mlsresidential.Lsqft',
			'garea'		:'mlsresidential.Lsqft',
			'price'		:'mlsresidential.Lprice',
			'dateL'		:'mlsresidential.Ldate',
			'waterf'	:'mlsresidential.WaterF',
			'pool'		:'mlsresidential.Pool',
			'remarks'	:'mlsresidential.Remark',
			'stories'	:'property.Stories',
			'Bathrooms'	:'mlsresidential.Bath',
			'Bedrooms'	:'mlsresidential.Beds'
		};
	}
	else{
		var otherData={
			'folio'		:'property.Folio',
			'larea'		:'property.Lsqft',
			'garea'		:'property.Lsqft',
			'price'		:'property.SalePrice',
			'waterf'	:'property.WaterF',
			'pool'		:'property.Pool',
			'remarks'	:'mlsresidential.Remark',
			'stories'	:'property.Stories'
		};
	}
	var xcode={
		'04':'Condo/Apartament',
		'01':'Single Family',
		'08':'Multi Family',
		'00':'Lost/ Land'
	}
	var statusProperty={
		'AA':'For Sale',
		'BB':'For Rent',
		'CS':'Sold',
		'CC':'Other'
	}
	property.property.status=statusProperty[property.property.status];
	property.property.xcode=xcode[property.property.xcode];
	
	var dataNumber=['larea','garea','price'];
	var dataBool=['pool','waterf'];
	
	$.each(dataToHeader,function (i,e){
		var tem=$(templateDetailsHeader);
		$('td:first-child',tem).html(i);
		var data=property.property;
		$.each(e.split('.'),function (i,e){
			data=data[e]
		})
		$('td:last-child',tem).html(data);
		tem.appendTo('#containerOverview .header table');
	});
	var propertyDetails=$(templateDetails);
	
	$('a[data-id=map]').on('click',{la:property.property.latitude,lo:property.property.longitude},showMap);
	
	$('a[data-id=listing]').on('click',{
		idlocal	:	property.property.parcelid,
		list: "S" 
	},propertiesToListSingle);
	
	$('a[data-id=offert]').on('click',{ 
		idlocal	:	property.property.parcelid,
		list: "B" 
	},propertiesToListSingle);
	
	$('a[data-id=analyze]').on('click',{
		address	:	property.property.addressfull,
		idlocal	:	property.property.parcelid,
		lat		:	property.property.latitude,
		log		:	property.property.longitude
	},SysRt.util.analyze_property);
	
	
	$.each(otherData,function (i,e){
		var data=property;
		$.each(e.split('.'),function (i,e){
			data=data[e]
		})
		if((dataNumber.indexOf(i)>-1)){
			$('[data-content='+i+']').html((parseInt(data)==0 || parseInt(data)=='NaN')?'--':Ext.util.Format.number(data,'0,000'));
		}
		else if((dataBool.indexOf(i)>-1)){
			$('[data-content='+i+']').html(data=='N'?'No':'Yes');
		}
		else{
			$('[data-content='+i+']').html($.trim(data)==''?'--':data);
		}
	});
	if(property.mlsresidential.Ldate){
	}
	$('[data-content=dateL]').html(property.property.dateL);
	$('[data-content=bathrooms]').html((property.property.Bath.split('.')[1]>0)?1:0);
	
	var dateL=$.trim($('[data-content=dateL]').html());
	dateL=dateL.substring(6,8)+'/'+dateL.substring(4,6)+'/'+
				dateL.substring(0,4);
				
	if(property.sales.length){
		$.each(property.sales,function (index,val){
			var tem=$(templateSales);
			val.date=val.date.substring(6,8)+'/'+val.date.substring(4,6)+'/'+
				val.date.substring(0,4);
			$('[data-content=date]',tem).html(val.date);
			$('[data-content=price]',tem).html('$'+Ext.util.Format.number(val.price,'0,000'));
			
			if(typeof property.mlsresidential.Lsqft == 'string'){
				property.mlsresidential.Lsqft=parseInt(property.mlsresidential.Lsqft.replace(/\D/,''));
				var pSf=parseFloat(val.price/property.mlsresidential.Lsqft);
				if(typeof pSf === 'number' && pSf!='infinity'){
					$('[data-content=ps]',tem).html('$'+Ext.util.Format.number(pSf,'0,000'));
				}
				else{
					$('[data-content=ps]',tem).html('--');
				}
			}
			else{
				$('[data-content=ps]',tem).html('--');
			}
			tem.appendTo('[data-container=sales] table');
		});
		$('[data-container=sales]').slideDown('fast');
	}
	if(dateL=='//'){
		$('[data-content=dateL]').html('--');
	}
	else{
		$('[data-content=dateL]').html(dateL);
	}
	if(property.agent.avatar){
		$('[data-content=agent-img]').attr('src',property.agent.avatar);
	}
	else{
		$('[data-content=agent-img]').remove();
	}
	function showGalery(){
		$('#containerOverview .showcase').empty()
		$.each(SysRt.globalVar.OS.images,function (i,e){
			var tem=$(templateImg);
			$('img',tem).attr('src',e.image).attr('title',i);
			tem.appendTo('#containerOverview .showcase');
		});
		var widthImgContent=$('#containerOverview .content').width()-($('#containerOverview .content-headerDetails').outerWidth()+31);
		console.log(widthImgContent);
		$('#containerOverview .container-img').width(widthImgContent);
		
		$("#containerOverview .showcase").awShowcase({
			content_width:			widthImgContent,
			content_height:			385,
			fit_to_parent:			false,
			auto:					false,
			interval:				3000,
			continuous:				false,
			loading:				true,
			tooltip_width:			200,
			tooltip_icon_width:		32,
			tooltip_icon_height:	32,
			tooltip_offsetx:		18,
			tooltip_offsety:		0,
			arrows:					false,
			buttons:				false,
			btn_numbers:			false,
			keybord_keys:			true,
			mousetrace:				false, 
			pauseonover:			true,
			stoponclick:			true,
			transition:				'hslide', /* hslide/vslide/fade */
			transition_delay:		300,
			transition_speed:		500,
			show_caption:			'onhover', /* onload/onhover/show */
			thumbnails:				true,
			thumbnails_position:	'outside-last', /* outside-last/outside-first/inside-last/inside-first */
			thumbnails_direction:	'horizontal',
			thumbnails_slidex:		0,
			dynamic_height:			false, 
			speed_change:			false,
			viewline:				false
		});
	}
	Ext.getCmp('overviewSearch').on( 'resize',function (){
		showGalery();
	}); 
	setTimeout(showGalery,200);
	
	
	
	/***
	*
		funciones
	**********************/
		function showMap(obj){
			var containerMap=$('.container-map');
			if(containerMap.is(':visible')){
				containerMap.slideUp('fast');
			}
			else{
				if(!SysRt.globalVar.OS.map){
					SysRt.globalVar.OS.map=new XimaMap(
						'SysRt.globalVar.OS.map',
						'SysRt.globalVar.OS.map_search_latlong',
						'SysRt.globalVar.OS.map_control_mapa_div',
						'SysRt.globalVar.OS.map_pan',
						'SysRt.globalVar.OS.map_draw',
						'SysRt.globalVar.OS.map_poly',
						'SysRt.globalVar.OS.map_clear',
						'SysRt.globalVar.OS.map_maxmin',
						'SysRt.globalVar.OS.map_circle');
					SysRt.globalVar.OS.map._IniMAP(
						obj.data.la,
						obj.data.lo,
						'birdseye'
					);
					SysRt.globalVar.OS.map.map.setOptions({
						width	: $('#containerOverview article.container:visible').width(),
						height	: $('#SysRt.globalVar.OS.map').height()
					});
						SysRt.globalVar.OS.map.addPushpinInfobox(
							  1,
							  obj.data.la,
							  obj.data.lo,
							  'http://www.realtytask.com/img/houses/verdeb.png',
							  SysRt.globalVar.OS.property.addressfull ,
							  SysRt.globalVar.OS.property.Lsqft,
							  SysRt.globalVar.OS.property.Lsqft,
							  SysRt.globalVar.OS.property.Beds+'/'+SysRt.globalVar.OS.property.Bath,
							  SysRt.globalVar.OS.property.Lsqft,
							  SysRt.globalVar.OS.property.Lsqft,
							  '',
							  ''
						 );
					SysRt.globalVar.OS.map.map.setView({
					  bounds: Microsoft.Maps.LocationRect.fromLocations([new Microsoft.Maps.Location(obj.data.la,obj.data.lo)])
					});	
				}
				$('.container-map').slideDown('fast');
			}
		}
	
	function propertiesToListSingle(Obj){
        Ext.MessageBox.show({
            title: 'Import Propertie',
            progressText: '0% completed',
            width:400,
            progress:true,
            closable:false
        });
		var PRimported=0;
		var PRimport=1;
		$.ajax({
			url		:'properties_tabs/propertyImport/propertyImportSO.php',
			type	:'POST',
			data	:{
				idlocal	: Obj.data.idlocal,
				type	: Obj.data.list
			},
			dataType:"json",
			success: function(data){
				PRimported++;
				if(PRimported==PRimport){
					Ext.MessageBox.hide();
					Ext.MessageBox.show({
						title: 'Result',
						width:300,
						msg: 'Success: ' + PRimported +' Properties Imported',
						buttons: Ext.MessageBox.OK
					});
				}
				else{
					var i = (PRimported)/PRimport;
					Ext.MessageBox.updateProgress(i, Math.round(100*i)+'% completed');
				}
			},
			error: function(){
				PRimported++;
			}
		});
	}
	$("[title]").tipTip();
	
	$('h3',propertyDetails).html('Property Details');
	
	
</script>