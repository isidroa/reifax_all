<?php 	
	include('../../properties_conexion.php');
	if(@session_start() == false){session_destroy();session_start();}
	conectar();
	//print_r($_SESSION);
	$userid=$_COOKIE['datos_usr']['USERID'];

	$state_search = $_POST['state_search'];
	$county_search = $_POST['county_search'];

?>
<div align="left" style="height:100%">
	<div id="body_central2" style="height:100%">
        <!--<div id="mapSearchAdv" style="display:none; width:99%;height:320;border: medium solid #b8dae3;position:relative; margin-bottom:5px;"></div>-->
		<div id="divButtonsPT" style="display:none; position: absolute; left: 180px; z-index: 2; top: 145px;">
			<table cellspacing="20">
				<tr>
					<td id="bt-cancel-list"></td>
					<td id="bt-import-list"></td>
				</tr>
			</table>
		</div>
		<div id="advSearchd" style="padding-top:2px;"></div>
		<div id="panelsIMP"></div>
	</div>
</div>
<script language="javascript">
	var imagesToRegister;
	function CreateArrayImagesImport(){
		imagesToRegister = Array();
		if(typeof resultPT.result.images.temp != "undefined"){
			$.each(resultPT.result.images.thumb, function(i, ite){
				var aux = ite.split("thumb/");
				imagesToRegister.push(aux[1]);
			});
		}
		return imagesToRegister;
	}
	function imageDefaultImport(){
		if(typeof resultPT.result.images.temp != "undefined"){
			var aux = resultPT.result.images.thumb[0].split("thumb/");
			return aux[1];
		}else{
			return "";
		}
	}
	var reloadMapImpPt = 0;
	Ext.ns("imagesImport");
	Ext.ns("mapsLatLonImport");
	function setFormFieldTooltip(component) {
		if (component.getXType() == 'textfield') {
		        var label = Ext.get('x-form-el-' + component.id).child('input');
		        Ext.QuickTips.register({
					target		:	label,
					text		:	component.tooltipText,
					dismissDelay:	15000,
					title		:	''
		        });
		}
	};
	
	var dataBoolean	=	new Ext.data.SimpleStore({
		fields: ['id','desc'],
		data  : [['Y','Yes'],['N','No']]
	});

	function selectBetween(combo,record,index)
	{
		var arfield=combo.getId().replace("cb","tx");
		arfield=arfield.split('*');
		var txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3];
//alert(arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3]+"-->"+txfield);
		
		if(record.get('id')=='Between')
		{
			if(Ext.getCmp(txfield))Ext.getCmp(txfield).setValue('');
			if(Ext.getCmp(txfield) && !Ext.getCmp(txfield).isVisible())
			{
				Ext.getCmp(txfield).setVisible(true);
				arrinpbetween.push(txfield);				
			}
		}
		else
		{
			if(Ext.getCmp(txfield) && Ext.getCmp(txfield).isVisible())
				Ext.getCmp(txfield).setVisible(false);
		}
	}
	
	function hideBetween(where)
	{
		var arfield;
		var txfield;
		var elementos = document.forms[1].elements.length;
		for(i=0; i<elementos; i++)
		{
			if(findstring(document.forms[1].elements[i].id) && Ext.getCmp(document.forms[1].elements[i].id).isVisible())
			{
				if(where=='ALL')
					Ext.getCmp(document.forms[1].elements[i].id).setVisible(false);
				else
				{
					arfield=document.forms[1].elements[i].id.split('*');
					txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+where;
					//alert(txfield)
					if(Ext.getCmp(txfield) )Ext.getCmp(txfield).setVisible(false);
				}
			}
		}
	}

	function findstring(cadena)
	{
		pat = /other/
		if(pat.test(cadena))
			return true
		return false
	}	

	function expandcollapse(section)
	{
		Ext.getCmp('PR').collapse();
		Ext.getCmp('FS').collapse();
		Ext.getCmp('FO').collapse();
		Ext.getCmp('MO').collapse();
		Ext.getCmp(section).expand();
	}
	
	function selectcounty(combo,record,index)
	{
		if(record.data.havemortgage=='N'){Ext.getCmp('MO').disable();Ext.getCmp('MO').setVisible(false);}
		if(record.data.havemortgage=='Y'){Ext.getCmp('MO').enable();Ext.getCmp('MO').setVisible(true);}
		
		mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
		search_county=Ext.getCmp('ncounty').getValue();
	}
	
	function removeSaleImport(id){
		Ext.getCmp("tableRowSalesImport"+id).hide();
		Ext.getCmp("txsales*price"+id).setValue(0);
		Ext.getCmp("txsales*priceSqft"+id).setValue(0);
		Ext.getCmp("txsales*date"+id).setValue("");
		Ext.getCmp("txsales*book"+id).setValue("");
		Ext.getCmp("txsales*page"+id).setValue("");
	}
	var countSalesHistoryPT=0;
	function addSaleHistoryPT(){
		var att = {
			id			:	'tableRowSalesImport'+countSalesHistoryPT,
			layout		:	'table',
			defaults	:	{	bodyStyle:'padding:5px'},
			layoutConfig:	{	columns: 6},
			items		:	[
				{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'datefield',
						id			:	'txsales*date'+countSalesHistoryPT,
						fieldLabel	:	'Date',
						name		:	'txsales*date'+countSalesHistoryPT,
						format		:	'm/d/Y',
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Date'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'txsales*price'+countSalesHistoryPT,
						fieldLabel	:	'Price',
						name		:	'txsales*price'+countSalesHistoryPT,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Price'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	80,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'txsales*priceSqft'+countSalesHistoryPT,
						fieldLabel	:	'Price Sqft',
						name		:	'txsales*priceSqft'+countSalesHistoryPT,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale $/Sqft'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'txsales*book'+countSalesHistoryPT,
						fieldLabel	:	'Book',
						name		:	'txsales*book'+countSalesHistoryPT,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Book'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'txsales*page'+countSalesHistoryPT,
						fieldLabel	:	'Page',
						name		:	'txsales*page'+countSalesHistoryPT,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Page'});
							}
						}
					}]
				},{
					xtype	:	'button',
					id		:	'txsales*button'+countSalesHistoryPT,
					cls		:	'x-btn-text',
					idSales	:	countSalesHistoryPT,
					tooltip	:	'Delete this row',
					text	:	'<i class="icon-minus"></i>',
					listeners	:	{
						click	:	function(button){
							removeSaleImport(button.idSales);
						}
					}
				}
			]
		};	
		countSalesHistoryPT=countSalesHistoryPT+1;
		return att;
	}
	loading_win.show();
	Ext.Ajax.request( 
	{  
		waitMsg: 'Processing...',
		url: 'properties_tabs/propertyImport/propertyImportBuilt.php', 
		method: 'POST',
		timeout :600000,
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			//loading_win.show();
			var aRes=response.responseText.split("^");
			
			var publicrecordIMP=Ext.decode(aRes[0]);
			var listingIMP=Ext.decode(aRes[1]);
			var foreclosureIMP=Ext.decode(aRes[2]);
			var mortgageIMP=Ext.decode(aRes[3]);
			var publicrecordmoreIMP=Ext.decode(aRes[4]);
			/*var listingmoreIMP=Ext.decode(aRes[5]);
			var byownerIMP=Ext.decode(aRes[6]);
			var byownermoreIMP=Ext.decode(aRes[7]);*/
			
			//var displayImageImport = new Ext.BoxComponent({ id:'displayImageImport', autoEl: {tag: 'img', height: 200, src: ''}});
			var displayImageImport = new Ext.BoxComponent({ id:'displayImageMapImport', style: 'position:relative; height:100%;'});
			
			mapsLatLonImport.idRender	=	"displayImageMapImport";
			
			formulsearchadv = new Ext.FormPanel({
				url			:	'properties_tabs/propertyImport/propertyIncluded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				renderTo	:	'advSearchd',
				id			:	'formulsearchadv',
				name		:	'formulsearchadv',
				items		:	[{
					layout		:	'table',
					bodyStyle	:	"padding:10px",
					layoutConfig:	{
		                columns	:	3
		            },
					items		:	[{
						xtype	:	'fieldset',
						title	:	'Property',
						width	:	600,
						height	:	200,
						layout	:	'column',
						items	:	[{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Address',
								id			:	'txproperty*address',
								name		:	'txproperty*address'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'City',
								id			:	'txproperty*city',
								name		:	'txproperty*city'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'County',
								name		:	'txproperty*county',
								id			:	'txproperty*county'
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'cbproperty*xcode',
								id				:	'cbproperty*xcode',
								hiddenName		:	'hdproperty*xcode',
								store			:	propertiesType,				
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								typeAhead		:	true,
								fieldLabel		:	'Type',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	125
								
							}/*,{
								xtype		:	'textfield',
								fieldLabel	:	'Subd Name',
								id			:	'edproperty*sbdname',
								name		:	'edproperty*sbdname'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Year Built',
								id			:	'edproperty*yrbuilt',
								name		:	'edproperty*yrbuilt',
								width		:	50
							}*/,{
								xtype		:	'hidden',
								id			:	'txproperty*addressfull',
								name		:	'txproperty*addressfull'
							},{
								xtype		:	'hidden',
								id			:	'txproperty*latitude',
								name		:	'txproperty*latitude'
							},{
								xtype		:	'hidden',
								id			:	'txproperty*longitude',
								name		:	'txproperty*longitude'
							},{
								xtype		:	'hidden',
								id			:	'txproperty*parcel',
								name		:	'txproperty*parcel'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Unit',
								id			:	'txproperty*unit',
								name		:	'txproperty*unit',
								width		:	50
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Zip',
								id			:	'txproperty*zip',
								name		:	'txproperty*zip',
								width		:	50
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'cbproperty*state',
								id				:	'cbproperty*state',
								store			:	new Ext.data.JsonStore ({
									fields	:	['initial', 'name'],
									url		:	'../../resources/Json/states.json',
									autoLoad:	true
								}),				
								hiddenName		:	'hdproperty*state',
								editable		:	false,
								displayField	:	'name',
								valueField		:	'initial',
								typeAhead		:	true,
								fieldLabel		:	'State',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155,
								listeners		:	{

								}
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'cbproperty*status',
								id				:	'cbproperty*status',
								hiddenName		:	'hdproperty*status',
								store			:	propertiesStatus,
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								typeAhead		:	true,
								hidden			:	true,
								hideLabel		:	true,
								fieldLabel		:	'Status',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155
							}]
						}],
						buttonAlign:'center',
						buttons:[{
							text	:	'Import',
							width	:	100,
							handler: function() {
								RtWinSaving.show();
								/*********************************
								*	Save Changes for Property
								*********************************/
								formulsearchadv.getForm().submit({
									params:{
										foliofirstvalid:	Ext.getCmp('txpsummary*folio').getValue(),
									},
									success: function(formulsearchadv, action){
										//console.debug(action);
										if(action.result.success){
											console.debug(action);
											if(action.result.msg	==	"Ready Exists"){
												Ext.MessageBox.alert('Warning','Property already in the System');
											}else{
												/*********************************
												*	Save Changes for Psummary
												*********************************/
												formulPsummaryIMP.getForm().submit({
													params:{
															unicID:action.result.id
														},
													success: function(formulPsummaryIMP, action){
														//console.debug(action);
														if(action.result.success){						
															/*********************************
															*	Save Changes for Residential
															*********************************/
															formulResidentialIMP.getForm().submit({
																params:{
																	unicID:action.result.id
																},
																success: function(formulResidentialIMP, action){
																	//console.debug(action);
																	if(action.result.success){						
																		/*********************************
																		*	Save Changes for Foreclosed
																		*********************************/
																		formulForeclosedIMP.getForm().submit({
																			params:{
																				unicID:action.result.id
																			},
																			success: function(formulForeclosedIMP, action){
																				//console.debug(action);
																				if(action.result.success){						
																					/*********************************
																					*	Save Changes for Mortgage
																					*********************************/
																					formulMortgageIMP.getForm().submit({
																						params:{
																							unicID:action.result.id
																						},
																						success: function(formulMortgageIMP, action){
																							//console.debug(action);
																							if(action.result.success){
																								$.ajax({
																									type	:	"POST",
																									async	:	false,
																									url		:	"/properties_tabs/propertyImport/propertyIncluded.php",
																									data	:	{
																										unicID		:	action.result.id,
																										tximages	:	CreateArrayImagesImport(),
																										imageDefault:	imageDefaultImport(),
																										txUrls		:	resultPT.result.aditionals
																									},
																									dataType:	'json',
																									success:function(action){
																										RtWinSaving.hide();
																										if(action.success){
																											if(typeof storeProperty != "undefined")
																												storeProperty.load({params:{start:0, limit:limitproperties}});
																											//Ext.getCmp('bt-cancel-pt').fireEvent('click',Ext.getCmp('bt-cancel-pt'));
																											var tabs = Ext.getCmp('tabs-import-pt');
																											var tab = tabs.getItem('result-pt');
																											tabs.remove(tab);
																										}
																									}
																								});
																							}else{
																								alert(action.result.msg);
																								RtWinSaving.hide();
																							}
																						}
																					});
																				}
																			}
																		});
																	}
																}
															});
														}else{
															RtWinSaving.hide();
															Ext.MessageBox.alert('Warning',action.result.msg);
														}
													}
												});
											}
										}else{
											RtWinSaving.hide();
											Ext.MessageBox.alert('Warning',action.result.msg);
										}
				                    }
								});
							},
							id		:	'bt-import-pt',
							//icon	:	"http://www.realtytask.com/img/add-property.png",
							icon	:	'/img/toolbar/NEW/properties_add_focus.png',
							scale	:	"large"
						},{
							text	:	'Cancel',
							width	:	100,
							handler: function() {
								//console.debug(imageDefaultImport());
								var tabs = Ext.getCmp('tabs-import-pt');
								var tab = tabs.getItem('result-pt');
								tabs.remove(tab);
							},
							id		:	'bt-cancel-pt',
							//icon	:	"http://www.realtytask.com/img/cancel-property.png",
							cls		:	'x-btn-text',
							iconCls	:	"icon-del-property",
							scale	:	"large"
						}]
					},{
						xtype	:	'fieldset',
						title	:	'Map',
						width	:	600,
						height	:	200,
						//layout	:	'column',
						items	:	[displayImageImport]
					}]//end items fieldset
				}],
				listeners: {
					afterrender : function(){
						//expandcollapse('PR');
						//Ext.getCmp('PRMORE').expand();
						/*if(resultPT.result.residential){
							if(resultPT.result.residential.length !== 0)
								Ext.getCmp('FS').expand();
						}*/
					}
				}
			});
			
			formulPsummaryIMP = new Ext.FormPanel({
				url			:	'properties_tabs/propertyImport/propertyIncluded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulPsummaryIMP',
				name		:	'formulPsummaryIMP',
				items		:	[{
					xtype		:	'fieldset',
					//title		:	'Public Records',
					collapsible	:	false,
					collapsed	:	false,
					id			:	'PRIMP',
					name		:	'PR',
					items		:	{
						layout	:	'column',
						items	: 	publicrecordIMP
					}
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Sales History</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'SLIMP',
					name		:	'SLIMP',
					items		:	[{
						xtype	:	'panel',
						id		:	'SLIMPSALES'/*,
						items	:	[{
						}]*/
					},{
						//layout	:	'column'
						items	:	[{
							xtype	:	'button',
							cls		:	'x-btn-text',
							text	:	'<i class="icon-plus"></i>',
							tooltip	:	'Add more Sales',
							listeners	:	{
								click	:	function(button){
									var aux = addSaleHistoryPT();
									Ext.getCmp('SLIMPSALES').add(aux);
									Ext.getCmp('SLIMPSALES').doLayout();
									Ext.getCmp('salesTotal').setValue(countSalesHistoryPT);
								}
							}
						},{
							xtype	:	'hidden',
							name	:	'salesTotal',
							id		:	'salesTotal'
						}]
					}]
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Owner Information</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'PRMOREIMP',
					name		:	'PRMOREIMP',
					items		:	{
						layout	:	'column',
						items	:	publicrecordmoreIMP
					}
				}],
				listeners:{
					afterrender:	function(){
						//console.debug(this);
						setTimeout(function(){
							$(Ext.get('txpsummary*folio').dom).on('keyup',function (){
								Ext.getCmp('txmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('txpendes*folio').setValue($(this).val());
								Ext.getCmp('txmortgage*folio').setValue($(this).val());
							});
							/*$(Ext.get('txmlsresidential*folio').dom).on('keyup',function (){
								Ext.getCmp('txpsummary*folio').setValue($(this).val());
								Ext.getCmp('txpendes*folio').setValue($(this).val());
								Ext.getCmp('txmortgage*folio').setValue($(this).val());
							});*/
							$(Ext.get('txpendes*folio').dom).on('keyup',function (){
								Ext.getCmp('txmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('txpsummary*folio').setValue($(this).val());
								Ext.getCmp('txmortgage*folio').setValue($(this).val());
							});
							$(Ext.get('txmortgage*folio').dom).on('keyup',function (){
								Ext.getCmp('txmlsresidential*folio').setValue($(this).val());
								Ext.getCmp('txpendes*folio').setValue($(this).val());
								Ext.getCmp('txpsummary*folio').setValue($(this).val());
							});
						},500);
						//console.debug(Ext.getCmp('edpsummary*folio'));
						/*Ext.getCmp('edpsummary*folio').onBlur(event, target){
							console.debug("ASASASASs");
						};*/
					}
				}
			});

			formulResidentialIMP = new Ext.FormPanel({
				url			:	'properties_tabs/propertyImport/propertyIncluded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulResidentialIMP',
				name		:	'formulResidentialIMP',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'For Sale/For Rent',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FSIMP',
					name		:	'FSIMP',
					items		:	{
						layout	:	'column',
						items	: 	listingIMP
					}
				}
			});
			
			formulForeclosedIMP = new Ext.FormPanel({
				url			:	'properties_tabs/propertyImport/propertyIncluded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulForeclosedIMP',
				name		:	'formulForeclosedIMP',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Foreclosure',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FOIMP',
					name		:	'FOIMP',
					items		:	{
						layout	:	'column',
						items	:	foreclosureIMP
					}
				}
			});

			formulMortgageIMP = new Ext.FormPanel({
				url			:	'properties_tabs/propertyImport/propertyIncluded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulMortgageIMP',
				name		:	'formulMortgageIMP',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Mortgage',
					collapsible :	false,
					collapsed	:	false,
					id			:	'MOIMP',
					name		:	'MOIMP',
					items		:	{
						layout	:	'column',
						items	:	mortgageIMP
					}
				}
			});
			
			var panelsImportMaps=	new Ext.TabPanel({
				id				:	'panelsImportMaps-pt',
				plain			:	true,
				height			:	800,
				//activeTab		:	0,
				enableTabScroll	:	true,
				defaults		:	{  
					autoScroll	:	false
				},
				items:[{
					title	:	'Bird View',
					autoLoad:	{
						url		:	'/properties_tabs/propertyMaps/propertyMapImport.php', 
						scripts	:	true,
						timeout	:	10800
					}
				},{
					title	:	'Street View',
					autoLoad:	{
						url		:	'/properties_tabs/propertyMaps/propertyStreetImport.php', 
						scripts	:	true,
						timeout	:	10800
					}
				}]
			});
			
			var panelsIMP = new Ext.TabPanel({
				id				:	'panelsIMP-pt',
				renderTo		:	'panelsIMP',
				deferredRender	:	 false,
		   		activeTab		:	0,
				autoWidth		:	true,
				height			:	system_height - ($("#panelsIMP").offset().top - 30),
				plain			:	true,
				enableTabScroll	:	true,
				defaults		:	{  
					autoScroll	:	true
				},
				items:[
					{
						title		:	'Public Records', 
						id			:	'PRIMP-tab-PT',
						items		:	formulPsummaryIMP,
						listeners	:	{
							afterrender:	function(){
								//Ext.getCmp('panelsIMP-pt').setHeight(1000);
							}
						}
					},{
						title		:	'County Site',
						id			:	'CSIMP-tab-PT',
						autoScroll	:	false,
						html		:	'<iframe src="'+ resultPT.result.aditionals.countyUrl +'" width="99%" height="99%"></iframe>'
					},{
						title		:	'Listing Site',
						id			:	'LISIMP-tab-PT',
						autoScroll	:	false,
						html		:	'<iframe src="'+ RtBase64.decode(resultPT.result.aditionals.urlMore) +'" width="99%" height="99%"></iframe>'
					},{
						title	:	'For Sale / For Rent',
						id		:	'LSIMP-tab-PT',
						items	:	formulResidentialIMP
					},{
						title	:	'Foreclosure',
						id		:	'FCIMP-tab-PT',
						items	:	formulForeclosedIMP
					},{
						title	: 	'Mortgage',
						id		:	'MTIMP-tab-PT',
						items	:	formulMortgageIMP
					}/*,{
						title	: 	'Images',
						id		:	'IMGIMP-tab-PT',
						autoLoad:	{
							url		:	'/properties_tabs/propertyImages/propertyImagesImport.php', 
							scripts	:	true,
							timeout	:	10800
						}
					}*/,{
						title	: 	'Maps',
						id		:	'MAPIMP-tab-PT',
						items	:	[panelsImportMaps]
					}
				],
				listeners:{
					afterrender:	function(tab){
						tab.setActiveTab(0);
						tab.doLayout();
						//Extract the latitude and longitude for propertyMaps.php with namespace mapsLatLon
						$.each(resultPT.result.property,function(i, ite){
							if(i=='txproperty*latitude')
								mapsLatLonImport.latitude	=	ite;
							if(i=='txproperty*longitude')
								mapsLatLonImport.longitude=	ite;
						});
						$.each(resultPT.result, function(i, ite) {
							if(i != "sales" && i != "images" && i != "aditionals"){
								$.each(ite, function(j, itej) {
									switch(j){
										case "cbproperty*state":
											Ext.getCmp(j).setValue(itej);
											break;
										default:
											Ext.getCmp(j).setValue(itej);
									}
									//console.debug(j);
								});
							}else if( i == "sales"){
								if(ite.total>0){
									Ext.getCmp('salesTotal').setValue(ite.total);
									$.each(ite.data, function(j, itej) {
										var aux = addSaleHistoryPT();
										Ext.getCmp('SLIMPSALES').add(aux);
										Ext.getCmp('SLIMPSALES').doLayout();
									});
									$.each(ite.data, function(j, itej) {
										$.each(itej, function(k, itek) {
											Ext.getCmp(k).setValue(itek);
										});
									});
								}else{
									Ext.getCmp('salesTotal').setValue(1);
									var aux = addSaleHistoryPT();
									Ext.getCmp('SLIMPSALES').add(aux);
									Ext.getCmp('SLIMPSALES').doLayout();
								}
							}else if(i == "images"){
								imagesImport.Import = ite;
								//This Variables is pased through Ext.namespace for propertyImagesImport.php, it will be executed where that php document ready 
								
								SpaceNeedle=new VELatLong(mapsLatLonImport.latitude,mapsLatLonImport.longitude);
								//if(imagesImport.Import.length <= 0){
									$("#"+mapsLatLonImport.idRender).attr('map','true');
									var	displayImageMapImport = new XimaMap(mapsLatLonImport.idRender,'mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
									displayImageMapImport.map = new VEMap(mapsLatLonImport.idRender);
									displayImageMapImport.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
									displayImageMapImport.map.HideDashboard();
									var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
									pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
										"<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
									displayImageMapImport.map.AddShape(pin); 
								//}
							}else if(i == "aditionals"){
								if(ite.countyUrl == "" || typeof ite.countyUrl == "undefined"){
									var tabPanel = Ext.getCmp('panelsIMP-pt');
									var tabToHide = Ext.getCmp('CSIMP-tab-PT');
									tabPanel.hideTabStripItem(tabToHide);
								}
								if(ite.urlMore == "" || typeof ite.urlMore == "undefined"){
									var tabPanel = Ext.getCmp('panelsIMP-pt');
									var tabToHide = Ext.getCmp('LISIMP-tab-PT');
									tabPanel.hideTabStripItem(tabToHide);
								}
							}
						});
						
						/*****************
						*	To Update Description on ComboBox State
						*****************/
						var store = Ext.getCmp('cbproperty*state').getStore();
						store.on("load", function() {
						   Ext.getCmp('cbproperty*state').setValue(Ext.getCmp('cbproperty*state').getValue());
						});
						store.load();
						/*if(resultPT.result.property){
							if(resultPT.result.property.length !== 0){
						}*/					
						$("#divButtonsPT").show(1000);
						
						//Hide Panels To Dont Use
						var tabPanel = Ext.getCmp('panelsIMP-pt');
						var tabToHide = Ext.getCmp('LSIMP-tab-PT');
						tabPanel.hideTabStripItem(tabToHide);
					},
					tabchange: function(tabPanel,panel){
						//This only will be used once time, this reload once the tab MAPS. By Jesus
						if(panel.title	==	"Maps"){
							if(reloadMapImpPt == 0){
								Ext.getCmp('panelsImportMaps-pt').setActiveTab(0);
								/*var tab     = Ext.getCmp('panelsIMP-pt').getActiveTab();
								var updater = tab.getUpdater();
								updater.update({
									url		:	'/properties_tabs/propertyMaps/propertyMapImport.php',
									cache	:	false
								});*/
								reloadMapImpPt =	1;
							}
						}
					}
				}
			});
			
			loading_win.hide();
			/*Ext.getCmp('PRMORE').expand();
			//Load Data in Form	
			var record = resultPT.result;
			$.each(record, function(i, ite) {
				$.each(ite, function(j, itej) {
					Ext.getCmp(j).setValue(itej);
					//console.debug(j);
				});
			});*/
		}
		
	});

</script>