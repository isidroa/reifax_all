<?php

require('../../properties_conexion.php');
conectar();

/*******************
*	Correct Address
*******************/
$exeptions = Array("#","\t","\n");
foreach($exeptions as $value)
	$_POST['address']	=	str_replace($value,"",$_POST['address']);
	
if((trim($_POST['listingNumber'])!=="" and trim($_POST['address'])!=="") or (trim($_POST['address'])!=="" and trim($_POST['listingNumber'])==="")){

	require('class.zillow.php');
	
	$Zillow =new Zillow();

	$c=0;
	do{
		$jsonResult = $Zillow->getPrincipalResult($_POST['address']." ".$_POST['select-states']); //For Sale
		$result = json_decode($jsonResult);
		$c++;
	}while($result->url == false and $c<=5);
	
	if(!$result->url){
		require("class.trulia.php");
		
		$Trulia	=	new Trulia();
		
		$jsonResultTrulia	=	$Trulia->getPrincipalResult($_POST['address']." ".$_POST['select-states']);
		
		$jsonResultTrulia	=	json_decode($jsonResultTrulia);	//Convert json String toObject Json
			
		if(!$jsonResultTrulia->url){
			echo json_encode(Array('url' => false, 'option' => 'T'));
			die();
		}else{
			$c=0;
			do{
				$jsonResultZillow	=	$Zillow->getPrincipalResult($Trulia->addressFull);
				$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
				$c++;
			}while($jsonResultZillow->url === false and $c<=5);
			
			if(!$jsonResultZillow->url or empty($jsonResultZillow->url)){
				$jsonResultTrulia	=	$Trulia->getDetailResult($Trulia->html);
				$jsonResultTrulia	=	json_decode($jsonResultTrulia);
				$jsonResultTrulia->option	=	"T";	//Add This Option
				
				echo json_encode($jsonResultTrulia);
				die();
			}else
				$jsonResult	=	json_encode($jsonResultZillow);
		}
	}
	
	echo $jsonResult;

}else{

	require('class.realtor.php');
	
	$Realtor = new Realtor();
	
	echo $urlDetail = $Realtor->ObtainUrlDetail(trim($_POST['listingNumber']));	//Status Sold
	
	//$result = json_decode($urlDetail);

	//echo $propertyDetails = $Realtor->getDetailResult($result->url);

}

?>