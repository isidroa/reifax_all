<style type="text/css">
.result-Multiple-Import-Container{
	margin:0 auto;
	margin-top:0px;
	width:948px;
}
.result-Multiple-Import{   
	padding: 6px;
	border:solid 6px #F1F1F1;
	background:#FFF;
	width:100%;
	height:auto;
}
.result-Multiple-Import td{
	width:50%;
}
.result-Multiple-Import ul{
	list-style:none;
	padding:0;
	margin: 4px 0 0 4px;
}
.result-Multiple-Import h1 {
    color: #000000;
    font-size: 2em;
}
.result-Multiple-Import h1 span{
	color:#777;
}

.result-Multiple-Import .container-buttons-multiImport{
	text-align:center;
	height:20px;
	margin-top:0px;
	width:173px;
}
.result-Multiple-Import .container-buttons-multiImport a{
	background:#FFF;
	padding:4px 19px 0px 31px;
	border: solid 1px #B5B8C8;
	border-radius:3px;
	color: #1D6AAA;
	font-weight:bold;
	font-size:13px;
	font-family:arial,tahoma,verdana,helvetica;
	text-decoration:none;
}
.result-Multiple-Import .container-buttons-multiImport a:hover{
-moz-box-shadow: 0px 0px 3px #ccc;
-webkit-box-shadow: 0px 0px 3px #ccc;
box-shadow: 0px 0px 3px #ccc;
	
}
.result-Multiple-Import .container-buttons-multiImport .close{
	background: no-repeat 5px 3px url("/img/cancel.png");

}.tab-bar{
	margin-top:10px;
}
.tab-bar .tbas ul{
	list-style:none;
	padding:0;
}
.tab-bar .tbas ul li{
	float:left;
	margin-left:3px;
}
.tab-bar .tbas ul li a{
	display:block;
	color:#298AD1;
	font-size:15px;
	padding:4px 15px;
	background-color: #EEEEEE;
	border-radius: 6px 6px 0 0;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif;
	
}
.tab-bar .tbas ul li .active{
	color:#FFF;
	font-weight:bold;
	background: -moz-linear-gradient(center top , #2293BD 25%, #005C83) repeat scroll 0 0 transparent;
	
}
.tab-bar .divider{
	clear:both;
	background:#EAEAEA;
	width:100%;
	height:2px;
	border:1px solid #D0D0D0;
}

.tab-bar .contariner-tabs{
	height:auto;	
	background:#F1F1F1;
	width:100%;
	border:1px solid #D0D0D0;
}
.tab-bar .container-body{    
	margin: 5px;
    padding: 8px;
	background:#FFF;
}

.tab-bar .container-body .body-list-import-Multiple, .result-Multiple-Import .body-list-import-Multiple{
	margin-top:4px;
	border:1px solid #D0D0D0;
	padding: 2px 4px;
	font-family:Tahoma, Geneva, sans-serif;
	font-size:12px;
	position:relative;
}
.tab-bar .container-body .body-list-import-Multiple{
	margin-bottom:15px;
	display:none;
}
.tab-bar .container-body .active{
	display:block;
}
.tab-bar .container-body .body-list-import-Multiple td{
	padding:3px;
	padding-top:5px;
}
.tab-bar .container-body .body-list-import-Multiple .td-content{
	width:200px;
}
.tab-bar .container-body .body-list-import-Multiple .td-title{
	width:170px;
}
.tab-bar .container-body .body-list-import-Multiple .td-content{
	width:200px;
}
.tab-bar .container-body .body-list-import-Multiple .title-panel, .result-Multiple-Import .body-list-import-Multiple .title-panel{
	position:absolute;
	background:#FFF;
	width:auto;
	height:auto;
	top:-10px;
	padding:1px 6px;
    color: #15428B;
	font-family:tahoma,arial,helvetica,sans-serif;
	font-size:11px;
	font-weight:bold;
}
.clear{
	clear:both;
}
.border-Multiple-Import-Container{
	border: 1px solid #D0D0D0;
}
</style>

<style>
.info {
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: url("img/propertiesTabs/info.png") no-repeat scroll 10px 0px #D8ECF5;
    border-color: #9AC9DF;
    border-style: solid;
    border-width: 1px 1px 1px 4px;
    color: #528DA9;
	margin-top:-3px;
	/*width:46px;*/
}

.saving {
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: url("img/propertiesTabs/notice.png") no-repeat scroll 10px 1px #E3EBC6;
    border-color: #C2D288;
    border-style: solid;
    border-width: 1px 1px 1px 4px;
    color: #8fa442;
	margin-top:-3px;
}

.error {
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: url("img/propertiesTabs/error.png") no-repeat scroll 10px 2px #F9E5E6;
    border-color: #F7C7C9;
    border-style: solid;
    border-width: 1px 1px 1px 4px;
    color: #B3696C;
	margin-top:-3px;
	width:46px;
}

.error, .info {
    display: none;
    padding: 3px 10px 6px 44px;
	position: absolute;
}

.saving {
    /*display: none;*/
    padding: 3px 10px 6px 44px;
	position: absolute;
}

.uno{
	background:white;
	color: black;
}
.dos{
	background:green;
	color: #FFF;
}
.tres{
	background:red;
	color: #FFF;
}
.lineaInterna, .todo{
	list-style:none;
}
.campo1{
	float: left;
    height: 20px;
    padding: 4px;
    width: 350px;
}

.campo2{
	float: left;
    height: 20px;
    padding: 4px;
    width: 180px;
}

.campo3{
	float: left;
    height: 20px;
    padding: 4px;
    width: 100px;
}

.campo{
	float: left;
    height: 20px;
    padding: 4px;
    width: 200px;
}

.linea{
	clear:both;
}

#response, #wait{
	width:650px;
	margin-left:35%;
}

.linea:hover .campo1{
	background: #58D3F7;
	color: #FFF;
}

.linea:hover .campo{
	background: #58D3F7;
	color: #FFF;
}
</style>

<style type="text/css">
.red{background-color: FF6060;}
.green{background-color: 95FF8B;}
.orange{background-color: FCC82B;}
</style>

<script>
	function initMultipleImportIndividual(address,i){
		$("a[data="+i+"]").hide(100);
		address	=	MultipleImport.type	==	"A"	?	address + " " + MultipleImport.state	:	address;
		var mensajes = "mensajes"+i;
		var save = "saving"+i;
		$("#"+mensajes).hide();
		$("#"+save).show(100);
		$.ajax({
			url		: 'properties_tabs/propertyImport/propertyIncludedMultiple.php',
			type	: 'POST',
			dataType: 'json',
			data	: {
				address		:	address,
				type		:	MultipleImport.type,
				idMultiple	:	i
			},
			success	: function (result){
				if(result.url == true || result.success == true){
					var mensajes = "mensajes"+i;
					var save = "saving"+i;
					$("#"+save).hide();
					document.getElementById(mensajes).className="";
					$("#"+mensajes).addClass("info");
					$("#"+mensajes).html('Success.');
					$("#"+mensajes).show(200);						
				}else if(!result.url){
					var mensajes = "mensajes"+i;
					var save = "saving"+i;
					$("#"+save).hide();
					document.getElementById(mensajes).className="";
					$("#"+mensajes).addClass("error");
					$("#"+mensajes).html('Error.');
					$("#"+mensajes).show(200);
				}else if(!result.success){
					var mensajes = "mensajes"+i;
					var save = "saving"+i;
					$("#"+save).hide();
					document.getElementById(mensajes).className="";
					$("#"+mensajes).addClass("error");
					$("#"+mensajes).html('Error.');
					$("#"+mensajes).show(200);
				}
			},
			error: function(){
				var mensajes = "mensajes"+i;
				var save = "saving"+i;
				$("#"+save).hide();
				document.getElementById(mensajes).className="";
				$("#"+mensajes).addClass("error");
				$("#"+mensajes).html('Error.');
				$("#"+mensajes).show(200);
				/*setTimeout(function(){
					$("#"+control).hide(500);
				}, 20000);*/
			}
		});
	}
	function initMultipleImport(){
		/*$.each(MultipleImport.propertiesToImpport, function(i,ite){
			var address	=	MultipleImport.type	==	"A"	?	ite + " " + MultipleImport.state	:	ite;
		});*/
		initMultipleImportAjax(0);
	}
	function initMultipleImportAjax(i){
		$.ajax({
			url		:	'properties_tabs/propertyImport/propertyIncludedMultiple.php',
			type	:	'POST',
			//async	:	false,
			dataType:	'json',
			data	:	{
				address		:	MultipleImport.type	==	"A"	?	MultipleImport.propertiesToImpport[i] + " " + MultipleImport.state	:	MultipleImport.propertiesToImpport[i],
				type		:	MultipleImport.type,
				idMultiple	:	i
			},
			success:	function (result){
				if(result.url == true || result.success == true){
					if(typeof result.success != "undefined"){
						if(result.msg	==	"Ready Exists"){
							var mensajes = "mensajes"+i;
							var save = "saving"+i;
							$("#"+save).hide();
							document.getElementById(mensajes).className="";
							$("#"+mensajes).addClass("info");
							$("#"+mensajes).html('Property already in the System');
							$("#"+mensajes).show(200);						
						}else{
							var mensajes = "mensajes"+i;
							var save = "saving"+i;
							$("#"+save).hide();
							document.getElementById(mensajes).className="";
							$("#"+mensajes).addClass("info");
							$("#"+mensajes).html('Success.');
							$("#"+mensajes).show(200);						
						}
					}else{
						var mensajes = "mensajes"+i;
						var save = "saving"+i;
						$("#"+save).hide();
						document.getElementById(mensajes).className="";
						$("#"+mensajes).addClass("info");
						$("#"+mensajes).html('Success.');
						$("#"+mensajes).show(200);						
					}
				}else if(!result.url) {
					var mensajes = "mensajes"+i;
					var save = "saving"+i;
					$("#"+save).hide();
					document.getElementById(mensajes).className="";
					$("#"+mensajes).addClass("error");
					$("#"+mensajes).html('Error.');
					$("#"+mensajes).show(200);
					$("a[data="+i+"]").show(200);
				}else if(result.success) {
					var mensajes = "mensajes"+i;
					var save = "saving"+i;
					$("#"+save).hide();
					document.getElementById(mensajes).className="";
					$("#"+mensajes).addClass("error");
					$("#"+mensajes).html('Error.');
					$("#"+mensajes).show(200);
					$("a[data="+i+"]").show(200);
				}
			},
			error:	function(){
				var mensajes = "mensajes"+i;
				var save = "saving"+i;
				$("#"+save).hide();
				document.getElementById(mensajes).className="";
				$("#"+mensajes).addClass("error");
				$("#"+mensajes).html('Error.');
				$("#"+mensajes).show(200);
				$("a[data="+i+"]").show(200);
			},
			complete:	function(){
				//console.debug((i+1) + " <--> "+ MultipleImport.propertiesToImpport.length);
				if((i+1)<MultipleImport.propertiesToImpport.length)
					initMultipleImportAjax(i+1);
			}
		});
	}
	$(document).ready(function (){
		var options = "<div style='margin: 0px 9%;'><h2 align='center'>Result for Each</h2><br><ul class='todo'>";

		$.each(MultipleImport.propertiesToImpport, function(i,ite){
			//console.debug(ite);
			options +="<li class='linea'><ul class='lineaInterna'><li class='campo1'>"+ite+"</li>";
			options +="</li data-mensaje='"+i+"'></li>";
			options +="<li class='campo2'><p id='saving"+i+"' class='saving'>Searching. &nbsp;&nbsp;&nbsp;&nbsp;<img src='img/propertiesTabs/saving.gif'></p><p id='mensajes"+i+"' class='info'><strong>Info</strong>- Your message!</p></li>";
			options +="<li class='campo3'><div class='container-buttons-multiImport'><a data='"+i+"' class='close' href='javascript:void(0)'>Try one time more</a></div></li>";
			options +="</ul></li>";
		});

		options += "</ul><div class='clear'></div></div>";
		$(".body-list-import-Multiple").html(options);
		
		$("a[class=close]").on("click",function(event){
			initMultipleImportIndividual(MultipleImport.propertiesToImpport[$(this).attr('data')],$(this).attr('data'));
		});
		$("a[class=close]").hide();
		initMultipleImport();
	});
	
</script>

<div class="result-Multiple-Import-Container">
	<div class="border-Multiple-Import-Container">
		<table class="result-Multiple-Import">
			<tr>
				<td>
					<div class="body-list-import-Multiple">
						
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>