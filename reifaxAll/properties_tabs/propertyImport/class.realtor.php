<?php
/*************************
Classes Designed To Manage The Realtor Spider

Author: Jesus Marquez

modified by isidro 
*************************/
class Realtor {
	

	//Variables
	var $address	=	"";
	var $price		=	"";
	var $status		=	"";
	var $street		=	"";
	var $city		=	"";
	var $county		=	"";
	var $state		=	"";
	var $zip		=	"";
	
	/******************************
	*
	*	Constructor Function
	*
	*******************************/
	function Realtor(){
		return true;
	}
	
	function ObtainUrlDetail($mlsNumber){
		$url = "http://www.realtor.com/realestateandhomes-search?mlslid=".$mlsNumber;

		$html = $this->codeHtml($url);
		
		$xpathFirst = $this->getXpathHtml($html,false);
		
		//$FirstResult = $this->queryDom($xpathFirst,"//a[@class='primaryAction']");	//Old
		$FirstResult = $this->queryDom($xpathFirst,"//ul[@class='listing-summary']//a");
		
		foreach ( $FirstResult as $link ){
			//$Url = "http://www.realtor.com".$link->getAttribute( 'data' );	//Old
			$Url = "http://www.realtor.com".$link->getAttribute( 'href' );
			$this->address	=	$link->nodeValue;
			break;
		}
		if($Url==="")
			return json_encode(Array('url' => false,'option' => 'R'));
		else
			return json_encode(Array('url' => base64_encode($Url),'option' => 'R'));
	}
	
	/******************************
	*
	*	Obtain Detail from a Url. (Principal Search)
	*
	*******************************/
	function getDetailResult($url){

		$html = $this->codeHtml($url,false);
		
		
		
		$html = str_replace("\\","",$html);
		$html = str_replace("<td>","<td>|",$html);
		$html = str_replace("<th>","<th>*",$html);
		
		$contentTable = $this->getXpathHtml($html,false);
		
		$results = $this->queryDom($contentTable,"//div[@id='LeadFormOpenHouse']//li[@class='address']");
		$this->street	=	trim($results->item(0)->nodeValue);
		
		$results = $this->queryDom($contentTable,"//div[@id='LeadFormOpenHouse']//li[@class='address2']");
		$aux			=	explode(",",$results->item(0)->nodeValue);
		$this->city		=	trim($aux['0']);
		$this->zip		=	trim(ereg_replace("[A-Za-z]", "", $aux['1']));
		$this->state	=	trim(ereg_replace("[0-9]", "", $aux['1']));
		
		$results = $this->queryDom($contentTable,"//div[@id='PropertyDetails']//div[@class='propertyData']//li");
		foreach($results as $link){
			$aux	=	explode(":",$link->nodeValue);
			if(strtolower(trim($aux['0']))	==	"county")
				$this->county	=	trim($aux['1']);
		}
		
		$results = $this->queryDom($contentTable,"//div[@id='LeadFormOpenHouse']//li[@class='price']");
		$this->price	=	str_replace(",","",str_replace("$","",$results->item(0)->nodeValue));
		
		$results = $this->queryDom($contentTable,"//tr[count(td)=2]");

		$detail = Array();
		foreach ( $results as $link )
		{
			 $first_array = explode("*",str_replace(",","",str_replace("$","",str_replace("/","",str_replace(":","",$link->nodeValue)))));
			 foreach($first_array as $uno){
				if($uno <> "")
					$detail[] = explode("|",$uno);
			 }
			 
		}
		return json_encode(Array("result" => true, "data" => $this->map($detail)));
	}

	/******************************
	*
	*	Get the html code that I will explore
	*
	*******************************/
	function codeHtml($url){
	
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10');

		//We assign the data to the variable $html
		$html = curl_exec($curl);
		
		//Close the Operation cUrl
		curl_close($curl);
		
		return $html;
	}

	/******************************
	*
	*	Get the url of level 1 and the exact address.
	*
	*******************************/
	function getXpathHtml($htmlDom,$pregMatch){

		if($pregMatch <> false){
			//We get the code block comments from Zillow that seem this json format
			preg_match_all($pregMatch,$htmlDom,$addr);
			
			//replacing all slashes from \ that interfere with the code
			$htmlDom = str_replace('\\','',$addr[1][0]);

		}
		
		$oldSetting = libxml_use_internal_errors( true );
		libxml_clear_errors();

		//New DOMDocument
		$dom = new DOMDocument();

		//We load it with data from cUrl ($ html)
		@$dom->loadHTML($htmlDom);

		//Uses XPath to Obtain the Object DOM previously obtained
		$xpath = new DOMXPath( $dom );

		libxml_clear_errors();
		libxml_use_internal_errors( $oldSetting );
		libxml_use_internal_errors( $oldSetting );

		return $xpath;		
	}

	/******************************
	*
	*	Excecute any Query over Xpath
	*
	*******************************/
	function queryDom($xpath,$query){

		//We make the XPath Query
		$arrayToExplode = $xpath->query($query);
		
		return $arrayToExplode;
	}	

	/******************************
	*
	*	Mapea the data to the correct fields
	*
	*******************************/
	private function map($data){
		
		$standarData = Array(
			'beds' => 'beds', 'baths' => 'baths', 'mls' => 'mls', 'house size' => 'sqft', 'lot size' => 'lot', 'price' => 'price', 'property type' => 'type', 
			'year built' => 'ybuilt', 'pricesqft' => 'perfsqft', 'lot depth' => 'lotd', 'lot width' => 'lotw', 'stories' => 'stories', 
			'last sold' => 'lsold', 'last remodel year' => 'lremodely', 'unit cnt' => 'unit', 'total rooms' => 'trooms', 'school district' => 'schoold'
		);

		$newData = Array();

		foreach($data as $row){
			$c=0;	//With this variable, I control to obtain the values
			foreach($row as $field){
				if($c==0)	//Get the firs position to obtain the field name
					$nameField=$standarData[strtolower(trim($field))];

				if($c==1){	//Get the first position to obtain the first value for this field
					if(!empty($nameField))
						$newData[$nameField]=trim($field);
				}
				$c++;
			}
		}
		
		/*****************************
		*	All Data For Residential From Realtor
		*****************************/

		$ResidentialExport	=	Array(
			'txmlsresidential*bath'			=>	$newData['baths'],
			'txmlsresidential*beds'			=>	$newData['beds'],
			'txmlsresidential*yrbuilt'		=>	$newData['ybuilt'],
			'txmlsresidential*mlnumber'		=>	$newData['mls']
		);

		return Array("street" => $this->street, "county" => $this->county, "city" => $this->city, "county" => $this->county, "state" => $this->state, "zip" => $this->zip, "data" => $newData);
	}
}
?>