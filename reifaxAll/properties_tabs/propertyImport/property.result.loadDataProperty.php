	<?php
	
	require('../../properties_conexion.php');
	require('../../includes/globalFunction.php');
	conectar();
	
	/*******************
	*	Correct Address
	*******************/
	$parceilid=$_POST['idlocal'];
	$_POST['address']=preg_replace('/#/',' ',$_POST['address']);
	$sql = 'SELECT * FROM temp_properties WHERE parcelid="'.$_POST['idlocal'].'" AND statusData="F" AND DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= regdate limit 1';
	$result=mysql_query($sql);
	if(!mysql_num_rows($result)){
		$sql = 'SELECT * FROM temp_properties WHERE parcelid="'.$_POST['idlocal'].'" limit 1';
		$result=mysql_query($sql);
		$dataTabla=mysql_fetch_assoc($result);
		require('class.baseImport.php');
		require('class.zillow.php');
		require('class.realtor.php');
		require('class.trulia.php');
		
		$Trulia = new Trulia(1);
		$Zillow = new Zillow(2);
		
		/**************************
		*
			for zillow
		*
		*
		*****************************/
		if($dataTabla['zid']){
			
			//echo 'zillow';
			
			$property = $Zillow->getPropertyJson($dataTabla['zid']);
			//print_r($property);
			$parceilid=$_POST['idlocal'];
			$urlAjax = $Zillow->getUrlDetail($urlProperty,$parceilid,$dataTabla['zid']);
			$details = json_decode($Zillow->getDetailResult($urlAjax));
			$caseSave='Z1';
		}
		else{
			$c=0;
				/**
					busqueda de zillow
				**************** /
				$data=json_decode($Zillow->getPrincipalResult($_POST['address']),true);
				/**
					busqueda de trulia
				**************** /
				$url=base64_decode($data['url']);
				if($url!=''){
					$urlAjax = $Zillow->getUrlDetail($url,$parceilid);
					$detailsZ = json_decode($Zillow->getDetailResult($urlAjax));
				}
				else{
				}*/
				$_POST['urlDirect']=$_POST['urlDirect']!='false'?decrypt($_POST['urlDirect'],$keys['trulia']):false;

				
				$jsonResultZillow = $Trulia->getPrincipalResult(NULL,$dataTabla['tid'],$parceilid,$_POST['urlDirect']);
				$details	=	json_decode($Trulia->getDetailResult($Trulia->html));
				//$details=combineData($detailsZ,$detailsT);
		}
		
		
		// regiter data on tables temporales 
		if($caseSave=='Z1'){
			$address=	$_POST['address'];
			$sql="UPDATE `temp_properties` SET 
				`addressfull`='{$address}', 
				`address`='{$property ['header']['property']['ad']['st']}', 
				`unit`='{$property ['header']['property']['ad']['ut']}', 
				`city`='{$property ['header']['property']['ad']['cy']}',
				`zip`='{$property ['header']['property']['ad']['zc']}',
				`state`='{$property ['header']['property']['ad']['et']}',
				`status`='{$property ['header']['property']['hs']['st']}',
				`latitude`='{$property ['header']['property']['ad']['lo']}',
				`longitude`='{$property ['header']['property']['ad']['la']}',
				`regdate`=NOW(),
				`statusData`='F'
				WHERE parcelid= '{$_POST['idlocal']}';
			";
			$tables=array(
				'psummary'		=> 'temp_psummary',
				'residential'	=> 'temp_mlsresidential',
				'mortgage'		=> 'temp_mortgage'
			);
			mysql_query($sql) or die ($sql.mysql_error());
		}
		else{
			$sql="UPDATE `temp_properties` SET 
				`regdate`=NOW(),
				`statusData`='F'
				WHERE parcelid= '{$_POST['idlocal']}';
			";
			mysql_query($sql) or die ($sql.mysql_error());
				
			$tables=array(
				'psummary'		=> 'temp_psummary',
				'property'		=> 'temp_properties',
				'residential'	=> 'temp_mlsresidential',
				'mortgage'		=> 'temp_mortgage',
				'agent'			=> 'temp_agent'
			);
		}
		$repalceDateRT=array('saledate','ldate','entrydate');
		$dataFloat=array('saleprice','lprice');
		$dataInteger=array('stories','yrbuilt','tsqft','bath','beds','bheated','lsqft','dom');
		foreach ($details->data as $k => $v){
			if($k=='sales'){
				$value='';
				if(($v->total)>0)
				{
					$v->data=json_decode(json_encode($v->data), true);
					$i=0;
					foreach($v->data as $k2){
						$date=explode('/',$k2['txsales*date'.$i]);
						$price=str_replace(',','',$k2['txsales*price'.$i]);
						
						$priceSqft=str_replace(',','',$k2['txsales*priceSqft'.$i]);
						$priceSqft=(trim($priceSqft)=='' || $priceSqft==NULL)?0:$priceSqft;
						if(trim($price!='')){
							$value.=(($value==''?'':', '))."('{$_POST['idlocal']}', '{$date[2]}{$date[1]}{$date[0]}' ,{$price}, {$priceSqft})";
						}
						$i++;
					};
					$sql="INSERT  INTO temp_sales (parcelid, `date`,`price`,`priceSqft`) VALUES $value";
					$querySales=$sql;
					mysql_query($sql) or die ($sql.mysql_error());
				}
			}
			if($k!='images' && $k!='sales' && is_object($v)){
				$campos='';
				foreach ($v as $k2 => $v2){
					$aux=explode('*',$k2);
					if (preg_match("/tx.*/", $aux[0])) {
						//
						if(in_array($aux[1],$repalceDateRT)){
							$value=trim($v2);
							$value=explode('/',$value);
							$value=$value[2].$value[0].$value[1];
						}
						else if(in_array($aux[1],$dataFloat)){
							$value=str_replace(',','',$v2);
							$value=str_replace('$','',$value);
						}
						else if(in_array($aux[1],$dataInteger)){
							$value=str_replace(',','',$v2);
							$value=str_replace('$','',$value);
							$value=preg_replace("/\D/",'',$value);
						}
						else{
							$value=mysql_escape_string($v2) ;
						}
						if(in_array($aux[1],$dataInteger) && $value!=''){
							$campos.=',`'.$aux[1].'`=( case when abs(`'.$aux[1].'`) = 0 OR `'.$aux[1].'` is null then '.($value==''?'0':"$value").' else `'.$aux[1].'` end )
							';
						}
						elseif(in_array($aux[1],$dataFloat) && $value!=''){
							$campos.=',`'.$aux[1].'`=( case when abs(`'.$aux[1].'`) = 0 OR `'.$aux[1].'` is null then '.($value==''?'0':"$value").' else `'.$aux[1].'` end )
							';
						}
						elseif($value!=''){
							$campos.=',`'.$aux[1].'`=( case when TRIM(`'.$aux[1].'`) = "" OR `'.$aux[1].'` is null then "'.(mysql_real_escape_string($value)).'" else `'.$aux[1].'` end )
							';
						}
					}
					elseif(preg_match("/cb.*/", $aux[0])){
							$campos.=',`'.$aux[1].'`=( case when TRIM(`'.$aux[1].'`) = "N" OR `'.$aux[1].'` is null then "'.($v2).'" else `'.$aux[1].'` end )
							';
					}
				}
				if($tables[$k]){
					$sql="UPDATE {$tables[$k]} SET `parcelid`='".$parceilid."' $campos WHERE parcelid='".$parceilid."'";
					//print_r($sql);
					mysql_query($sql) or die ($sql.mysql_error());
					
				}
			}
		}
	}
	/*************************************************
	
	*************************************************/
	//}
	$images=array();
	$sales=array();
	
	$sql = 'SELECT ps.*,pr.* FROM temp_properties pr left JOIN temp_psummary ps ON pr.parcelid=ps.parcelid  WHERE pr.parcelid="'.$parceilid.'" AND statusData="F" limit 1';
	$result=mysql_query($sql);
	$property=mysql_fetch_assoc($result);
	
	
	$sql = 'SELECT * FROM temp_sales WHERE parcelid="'.$parceilid.'"';
	$result=mysql_query($sql) or die ($sql.mysql_error());
	while($data=mysql_fetch_assoc($result)){
		array_push($sales,$data);
	}
	
	$sql = 'SELECT * FROM temp_rental WHERE parcelid="'.$parceilid.'" limit 1';
	$result=mysql_query($sql) or die ($sql.mysql_error());
	$rental=mysql_fetch_assoc($result);
	
	$sql = 'SELECT * FROM temp_imagenes WHERE parcelid="'.$parceilid.'"';
	$result=mysql_query($sql) or die ($sql.mysql_error());
	while($data=mysql_fetch_assoc($result)){
		array_push($images,$data);
	}
	
	
	$sql = 'SELECT * FROM temp_mlsresidential WHERE parcelid="'.$parceilid.'" limit 1';
	$result=mysql_query($sql) or die ($sql.mysql_error());
	$mlsresidential=mysql_fetch_assoc($result);
	
	$sql = 'SELECT * FROM temp_agent WHERE parcelid="'.$parceilid.'" limit 1';
	$result=mysql_query($sql) or die ($sql.mysql_error());
	$temp_agent=mysql_fetch_assoc($result);
	
	
	
	echo json_encode(array('success' => true, 'data' => array(
		'property' 	=> $property,
		'rental' 	=> $rental,
		'images' 	=> $images,
		'sales'		=> $sales,
		'agent'		=> $temp_agent,
		'dataProperty'	=> $details->data,
		'salesSql'		=> $querySales,
		'mlsresidential' 	=> $mlsresidential,
		'case'		=> $caseSave,
		'trulia' => $detailsT,
		'Combine' => $details
	)));
?>