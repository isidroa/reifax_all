<?php
/*************************
Classes Designed To Manage The Trulia Spider

	
	modify by jesus & isidro 

*************************/
class Trulia extends import {

	//Variables
	var $countyProxy     =  'newReifax';   
	var $images			=	array();
	var $latitude		=	"";
	var $idServer		=	"";
	var $longitude		=	"";
	var $remark			=	"";
	var $address		=	"";
	var $subDName		=	"";
	var $price			=	"";
	var $status			=	"";
	var $city			=	"";
	var $county			=	"";
	var $state			=	"";
	var $zip			=	"";
	var $salesHistory	=	"";
	var $urlFree		=	"";
	var $listingDate	=	"";
	var $entryDate		=	"";
	var $addressFull	=	"";
	var $urlCounty		=	"";
	var $html			=	"";
	var $idMultiple		=	"";
    var $conMaster      =   NULL;
    var $conReifax      =   NULL;
    var $managerParseo  =   NULL;
	
	/******************************
	*
	*	Constructor Function
	*
	*******************************/
	public function Trulia($options=array()){
        $this->idServer=$options['id'];
        $this->managerParseo=$options['managerParseo'];
        $this->conReifax=$options['conReifax'];
        $this->conMaster=$options['conMaster'];
		return true;
	}
	
	/******************************
	*
	*	Principal Search Process. Return the First Result
	*
	*******************************/
	public function getPrincipalResult($options= array()){

		$options=array_merge(
			array(
				'urlDirect'=>'',
				'address'
			),
			$options
		);
		$options	=	(object)$options;
        echo '<br> 55 trulia '.date('Y M d H:i:s');
		
		$this->parcelid=$parcelid;
		if($options->urlDirect){
			$Url	=	$options->urlDirect;
		}
		elseif($id==NULL){
			$Url	=	$this->obtainCountyURL("http://www.trulia.com/validate.php?tst=h&display=for+sale&search=".urlencode($options->address));
		}
		else{
			$Url	=	"http://www.trulia.com/property/{$options->id}";
		}
		if(strpos($Url,"error") or strpos($Url,"failed"))
			return json_encode(array('url' => false, 'option' => 'T'));
		else{
			echo $urlDirect;
			$this->urlFree	=	base64_encode($Url);
            echo '<br> 70 trulia '.date('Y M d H:i:s');
            $this->stratSession(array('CURLOPT_FOLLOWLOCATION' => true));
            echo '<br> 72 trulia '.date('Y M d H:i:s');
            $this->html	=	$this->getHtml(array('url'    =>  $Url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
            echo '<br> 74 trulia '.date('Y M d H:i:s');
            $this->closeSession();
            echo '<br> 76 trulia '.date('Y M d H:i:s');
			$xpathFirst		=	$this->managerParseo->getXpathHtml($this->html,false);
			$results			=	$this->managerParseo->queryDom($xpathFirst,"//h1[@class='address fn summary street-address location']");
            echo '<br> 79 trulia '.date('Y M d H:i:s');
			if($results->length > 0){
				$this->addressFull	=	$results->item(0)->nodeValue;
            }
			else{
				$results			=	$this->managerParseo->queryDom($xpathFirst,"//h1[@class='property_description']");
				foreach ($results as $index => $link){
					$this->addressFull	=	$this->managerParseo->clearTrash($this->managerParseo->getHtmlDom($link));
				}
				$aux="";
				foreach(explode("\n",$this->addressFull) as $value){
					$aux	.=	" ".trim($value);
				}
				$this->addressFull	=	trim($aux);
			}
            //echo '<br> 94 trulia '.date('Y M d H:i:s');
			
			return json_encode(array('url' => true, 'option' => 'T'));
		}
	}
	/***************************************
	*
	*	edit by isidro 
	*		add futncion add data
	*
	********************************************/
	
		
	public function getPropertyJson($url,$html=NULL){
		if($url!=NULL){
			$url='http://www.trulia.com'.$url;	
			$html=$this->getHtml(array('url'    =>  $url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
			$data=$this->getDetailResult($html);
			
		}
		else{
			preg_match_all('/trulia.propertyData.set\(.*\);/',$html,$addr);
			preg_match_all('/\{.*\}/',$addr[0][0],$addr);
			$data=json_decode(str_replace(');','',$addr[0][0]),true);
			//$this->images=$data['photos'];
		}
		return $data;
	}
	
	/***************************************
	*
	*	edit by isidro 
	*		add futncion search on mobile system
	*
	********************************************/
	
	public function getListResultMobile ($address, $option=array()){
		if($option['nextPage']){
			$this->currentPage++;
			$this->url=str_replace('page='.($this->currentPage-1), 'page='.$this->currentPage, $this->url);
		}
		else{
			$this->currentPage=1;
			$urlOptions='path='.urlencode($address).'&page_size=15&page=1';
			if($_POST['propertyStatus']!=''){
				switch($_POST['propertyStatus']){
					case('for_sale'):
						$urlOptions.='&display=for+sale&lt=resale,new_homes';
					break;
					case('for_rent'):
						$urlOptions.='&display=for+rent';
					break;
					case('recently_sold'):
						$urlOptions.='&display=sold';
					break;
				}
			}
			else{
				//$urlOptions.='&display=for+sale';
			}
			if($_POST['propertyType']!=''){
				switch($_POST['propertyType']){
					case('house'):
						$urlOptions.='&type=single-family';
					break;
					case('apartment_condo'):
						$urlOptions.='&type=apartment%2Ccondo%2Ctownhouse';
					break;
					case('duplex'):
						$urlOptions.='&type=multifamily';
					break;
					case('mobile'):
						$urlOptions.='&type=mobile';
					break;
					case('land'):
						$urlOptions.='&type=lot';
					break;
				}
			}
			else{
				$urlOptions.='&type=any';
			}
			if($_POST['beds']){
				$urlOptions.='&min_num_beds='.$_POST['beds'];
			}
			if($_POST['baths']){
				$urlOptions.='&min_num_baths='.$_POST['baths'];
			}
			if($_POST['min_price']!='' || $_POST['max_price']!='' ){
				$urlOptions.='&min_price='.$_POST['min_price'];
				$urlOptions.='&max_price='.$_POST['max_price'];
			}
			if($_POST['min_SF']!='' || $_POST['max_SF']!='' ){
				$urlOptions.='&min_size='.$_POST['min_SF'];
			}
			//$this->url='http://www.trulia.com/validate.php?tst=h'.$urlOptions;
       		$this->url='http://m.trulia.com/api/default/-/url/resolver?'.$urlOptions;
		}
        
		$this->stratSession(array('CURLOPT_FOLLOWLOCATION' => true, 'CURLOPT_TIMEOUT' =>5000000, 'isMobile' => true));
		$html	=	$this->getHtml(array('url'    =>  $this->url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
		// echo '</textarea>';
        $this->closeSession();
		
		if(strpos($this->lastUrlRequest,'advanced/error')){
			die('no have result');
		}
		$resultProperty=array();
		$dataFull=json_decode($html,true);
		
		if(!isset($dataFull['data'])){
			echo "\n reintentado peticion \n{$html} \n"; 
			$interacciones=0;
			do{
				$salir=true;
				sleep(10);
				$this->stratSession(array('CURLOPT_FOLLOWLOCATION' => true, 'CURLOPT_TIMEOUT' =>5000000, 'isMobile' => true));
				$html	=	$this->getHtml(array('url'    =>  $this->url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
	        	$this->closeSession();
				$dataFull=json_decode($html,true);
				if (!isset($dataFull['data'])) {
					$interacciones++;
					if ($interacciones>5) {
						$salir=false;
					}
					else{
						echo "\n intento # {$interacciones} de descarga de la pagina {$this->currentPage} \nUrl: {$this->url}\n{$html}\n";
					}				
				} else {
					$salir=false;
				} 
			}while($salir);
		}
		//$xpathFirst	=	$this->managerParseo->getXpathHtml($html,false);
		
		/******************************************
		*
		*	BUSCANDO EL JSON DE LA PAGINA DE TRULIA PARA VARIAS PROPIEDADES 
		*
		********************************************
		$fun=preg_match_all('/ data: \[\{.*\}\]/',$html,$addr);
		if($fun){
			$addr=json_decode(str_replace('data: ','',$addr[0][0]),true);
			//$addr=json_decode(str_replace('','',$addr),true);
			
			$currentPage	=	  $this->managerParseo->queryDom($xpathFirst,"//span[contains(@class, 'srpPagination_page')]");
			foreach ( $currentPage as $link ){
				$this->currentPage	=	preg_replace("/\D/",'',$link->nodeValue);
			}
			$totalresult	=	  $this->managerParseo->queryDom($xpathFirst,"//span[contains(@class, 'srpTitleResults')]");
			if($totalresult->length){
				foreach ( $totalresult as $link ){
					$this->totalresult	=	preg_replace("/\D/",'',$link->nodeValue);
				}
			}
			else{
				$this->recordError('fallo al encontrar totalresult',__LINE__,1);
				return false;
			}
			if($this->totalresult>14){
				$nextPage			=	$this->managerParseo->queryDom($xpathFirst,"//link[@rel='next']");
				if($nextPage->length){
					foreach ( $nextPage as $link ){
						
						$this->nextPage	=	$link->getAttribute( 'href' );
						echo '<h1>'.$this->nextPage.'</h1>';
					}
				}
				elseif($this->currentPage*15<$this->totalresult){
					$this->recordError('fallo al encontrar nextPage',__LINE__,1);
					return false;
				}	
			}			
			/******************************************
			*
			*	SI SE ENCUENTRA SE VERIFICAN LAS VARIABLES A USAR
			*
			********************************************* /
			foreach(array('addressForDisplay','zipCode','formatted_price','picPath','id','numBedrooms','numBathrooms','status','longitude','addressForDisplay','latitude') as $i => $k){
				if(!array_key_exists($k,$addr[0])){
					$this->recordError("fallo al encontrar $k dentro del json de trulia.propertyData.set",__LINE__,2);
				}
			}
			
			/******************************************
			*
			*	CONSTRUYO EL ARRAY A PROCESAR
			*
			*********************************************/
			foreach($dataFull['data'] as $k => $val){

			//		$this->parcelid=md5($val['addressForDisplay'].' '.$val['zipCode']);
           // echo '<br> '.__LINE__;
					//$this->saveImgTem($val['picPath']);
		/*			$sqft			=  $this->managerParseo->queryDom($xpathFirst,"//li[@data-property-id='".$val['id']."']//div[contains(., 'sqft')]");
                    
           // echo '<br> '.__LINE__;
					if($sqft->length){
						foreach ( $sqft as $link ){
							$this->sqft	=	explode(' ',trim($link->nodeValue));
							$this->sqft	=	preg_replace("/\D/",'',$this->sqft[0]);
						}
					}
					else{
						//$this->recordError('fallo al encontrar sqft',__LINE__,2);
					}
                    
           // echo '<br> '.__LINE__;
					$val['price']=trim(($val['price'])?$val['price']:$val['formatted_price']);
					$val['price']=explode(' ',$val['price']);
					$val['price']=preg_replace("/\D/",'',$val['price']);
					
            //echo '<br> '.__LINE__;
				*/	
				 
					array_push($resultProperty,array(
								'address'	=> $val['address']['display'],
								'county'	=> $val['address']['components']['county'],
								'stateCode'	=> $val['address']['components']['stateCode'],
								'json'		=> json_encode($val),/*,
								'json'		=> $this->images,
								'price'		=> $val['price'][0],*/
								'tid'		=> $val['metadata']['id'],/*
								'county'		=> $val['county'],
								'baths'		=> $val['numBathrooms'],
								'sqft'		=> $this->sqft,
								'lot'		=> $this->sqft,
								'yb'		=> $val['yb'],
								'marke'		=> $val['do'],
								'zc'		=> $val['zipCode'],
								'cy'		=> $val['city'],
								'co'		=> $val['county'],
								'type'		=> $val['typeDisplay'],
								'state'		=> $option['select-states'],
								'id'		=> $val['id'],
								'status'	=> $val['status'],
								'longitude'		=> $val['longitude'],
								'latitude'		=> $val['latitude'],*/
								'pdpURL'		=> $val['pdpURL']/*,
								'imgC'		=> $val['pc']//,
								//'location'	=> encrypt($val['pdpURL'],$keys['trulia'])
					   */));
            //echo '<br> '.__LINE__;
			}
			return array(
				'numberResult'	=> $this->totalresult,
				'url'			=> $this->url,
				'properties'	=> $resultProperty,
				'urlDirect'		=> $this->lastUrlRequest,
				'isAddress'		=> true,
				'idTru'			=> true,
				'nextPage'		=> $this->nextPage
			);
				
		}
		/******************************************
		*
		*	BUSCANDO EL JSON DE LA PAGINA DE TRULIA PARA UNA PROPIEDAD
		*
		********************************************* /
		elseif(preg_match_all('/trulia.propertyData.set\(.*\);/',$html,$addr)){
			$addr='['.(str_replace('trulia.propertyData.set(','',$addr[0][0])).']';
			$addr=json_decode(str_replace(');','',$addr),true);
			$addr[0]['picPath']='http://thumbs.trulia-cdn.com/pictures/thumbs_4/'.$addr[0]['photos'][0];
			$this->sqft=$addr[0]['sqft'];
			$this->nextPage	= 0;
			$this->totalresult	= 1;
			
			$this->parcelid=md5($addr[0]['addressForDisplay'].' '.$addr[0]['zipCode']);
			
			$propertyFull=$this->getDetailResult($html);
			
			//print_r($addr);
			//print_r(json_decode($propertyFull)); die();
			array_push($resultProperty,array(
						'address'	=> $addr[0]['addressForDisplay'].' '.$addr[0]['zipCode'],
						'idlocal'	=> $this->parcelid,
						'img'		=> $this->images,
						'price'		=> $val['price'][0],
						'tid'		=> $addr[0]['id'],
						'beds'		=> $addr[0]['numBedrooms'],
						'baths'		=> $addr[0]['numBathrooms'],
						'sqft'		=> $this->sqft,
						'lot'		=> $this->sqft,
						'yb'		=> $addr[0]['yb'],
						'marke'		=> $addr[0]['do'],
						'zc'		=> $addr[0]['zipCode'],
						'cy'		=> $addr[0]['city'],
						'co'		=> $addr[0]['county'],
						'type'		=> $addr[0]['typeDisplay'],
						'state'		=> $option['select-states'],
						'id'		=> $addr[0]['id'],
						'status'	=> $addr[0]['status'],
						'longitude'		=> $addr[0]['longitude'],
						'latitude'		=> $addr[0]['latitude'],
						'imgC'		=> $addr[0]['pc']//,
						//'location'	=> encrypt($addr[0]['pdpURL'],$keys['trulia'])
			   ));
			
			
		}
		/******************************************
		*
		*	SI NO ENCUENTRO NADA REPORTO EL ERROR
		*
		********************************************* /
		else{
			$this->recordError('fallo al encontrar el json trulia.propertyData.set',__LINE__,1);
			return false;
		}
		
		
		
		//$this->saveDataTemporal($resultProperty);
		
		if(isset($propertyFull)){
			$this->urlImages(NULL,$this->parcelid,true);
		}
			
		return array(
			'numberResult'	=> $this->totalresult,
			'url'			=> $this->url,
			'properties'	=> $resultProperty,
			'urlDirect'		=> $this->lastUrlRequest,
			'isAddress'		=> true,
			'idTru'			=> true,
			'nextPage'		=> $this->nextPage
		);
		 */
	
	
	public function saveDataTemporal($data=array(), $type='S'){
		/******************************************
		*
		*	GUARDADO EN LAS TABLAS TEMPORALES
		*
		*********************************************/
			$statusVar=array(
				'ForSale' 	=> 'AA',
				'For Sale' 	=> 'AA',
				'ForRent'		=> 'BB',
				'For Rent'		=> 'BB',
				'RecentlySold'	=> 'CS',
				'Zestimate'	=> 'CC',
				'Other'		=> 'CC'
			);
			$typePro=array(
			
				'Condo' 		=> 'Condo/Apartament',
				'Apartment'		=> 'Condo/Apartament',
				'Townhouse'		=> 'Condo/Apartament',
				'Townhome'		=> 'Condo/Apartament',
				'Apartment/condo/townhouse'		=> 'Condo/Apartament',
				'Apt/Condo/Twnhm'				=> 'Condo/Apartament',
				'Single Family'			=> 'Single Family',
				'Single-family'	 		=> 'Single Family',
				'Single-Family Home'	=> 'Single Family',
				'Multi family'			=> 'Multy Family',
				'Multi-Family Home'		=> 'Multy Family',
				'Lost'			=> 'Lost/Land',
				'Land'			=> 'Lost/Land',
				'Condo' 		=> 'Condo/Apartament',
				'Apartment'		=> 'Condo/Apartament',
				'Townhouse'		=> 'Condo/Apartament',
				'Single Family'	=> 'Single Family',
				'Multi Family'	=> 'multy Family',
				'Lost'			=> 'Lost/Land',
				'Land'			=> 'Lost/Land'
			);
			$typeProC=array(
				'Condo' 		=> '04',
				'Apartment'		=> '04',
				'Townhouse'		=> '04',
				'Cooperative'	=> '04',
				'Single Family'	=> '01',
				'Multi Family'	=> '08',
				'Lost'			=> '00',
				'Land'			=> '00' 
			);
			$values=$valuesresi=$valuesI=$valuesren=''; 
			foreach ($data as $index => $val){
					switch($typePro[$val['type']]){
						case	"Single Family":		$typePT	=	"01";	break;
						case	"Townhouse":			$typePT	=	"04";	break;
						case	"Condo":				$typePT	=	"04";	break;
						case	"Cooperative":			$typePT	=	"04";	break;
						case	"Multi Family":			$typePT	=	"08";	break;
						case	"Vacant Land":			$typePT	=	"00";	break;
						case	"Mobile / Manufactured":$typePT	=	"02";	break;
						default:						$typePT	=	"99";	break;
					}
				   
				   $values=" (
					'{$val['property']['txproperty*addressfull']}',
					'{$val['property']['txproperty*address']}',
					'{$val['property']['txproperty*unit']}',
					'{$val['property']['txproperty*city']}',
					'{$val['property']['txproperty*zip']}',
					'{$val['property']['cbproperty*state']}',
					'{$val['property']['cbproperty*status']}',
					'{$val['property']['txproperty*county']}',
					'{$val['property']['txproperty*xcoded']}',
					'{$val['property']['txproperty*xcode']}',
					'{$val['property']['txproperty*latitude']}',
					'{$val['property']['txproperty*longitude']}',
					NOW(),
					'{$val['property']['txproperty*propertyId']}'
					)";
					
				
					$sql="	INSERT  INTO `reifaxcounty`.`properties`
								(`addressfull`, `address`, `unit`, `city`, `zip`, `state`, `status`,`county`, `xcoded`, `xcode`, `latitude`, `longitude`,`regdate`,`idTrulia`)
							VALUES
								$values
					;";
					$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
					$val['idlocal']=$this->conReifax->insert_id;
					$val['agent']['txagent*broker']=$this->conReifax->real_escape_string($val['agent']['txagent*broker']);
					$val['agent']['txagent*agent']=$this->conReifax->real_escape_string($val['agent']['txagent*agent']);
					$valuesagen.=($valuesagen==''?'':',')." (
						{$val['idlocal']},
						'{$val['agent']['txagent*agent']}',
						'{$val['agent']['txagent*phone']}',
						'{$val['agent']['txagent*broker']}',
						'{$val['agent']['txagent*phoneBroker']}' )";
					
					
					$featurePS=implode('][',$val['feature']);
				   	$valuesps.=($valuesps==''?'':',')." (
						{$val['idlocal']},
						{$val['psummary']['txpsummary*beds']},
						{$val['psummary']['txpsummary*bath']},
						{$val['psummary']['txpsummary*lsqft']},
						{$val['psummary']['txpsummary*yrbuilt']},
						'{$val['psummary']['txpsummary*saledate']}',
						{$val['psummary']['txpsummary*saleprice']},
						'{$featurePS}'
					)";
					
					
					if($val['fgmt']=='ForSale' || $val['status']=='For Sale'){
						$featurePR=implode('][',$val['featurePR']);
					   $valuesresi.=($valuesresi==''?'':',')." (
						{$val['idlocal']},
						'{$val['residential']['txmlsresidential*beds']}',
						'{$val['residential']['txmlsresidential*bath']}',
						'{$val['residential']['txmlsresidential*dom']}',
						'{$val['residential']['txmlsresidential*lsqft']}',
						'{$val['residential']['txmlsresidential*lsqft']}',
						'{$val['residential']['txmlsresidential*yrbuilt']}',
						'{$val['residential']['txmlsresidential*mlnumber']}',
						'{$val['residential']['txmlsresidential*ldate']}',
						'{$val['residential']['txmlsresidential*lprice']}',
						'{$val['residential']['txmlsresidential*remark']}',
						'{$featurePR}'
						)";
					}
						
					if($val['fgmt']=='ForRent'){
					  $featureRen=implode('][',$val['featureRen']);
					   $valuesresi.=($valuesresi==''?'':',')." (
						'{$val['idlocal']}',
						'{$val['residential']['txmlsresidential*beds']}',
						'{$val['residential']['txmlsresidential*bath']}',
						'{$val['residential']['txmlsresidential*dom']}',
						'{$val['residential']['txmlsresidential*lsqft']}',
						'{$val['residential']['txmlsresidential*lsqft']}',
						'{$val['residential']['txmlsresidential*yrbuilt']}',
						'{$val['residential']['txmlsresidential*mlnumber']}',
						'{$val['residential']['txmlsresidential*ldate']}',
						'{$val['residential']['txmlsresidential*lprice']}',
						'{$val['residential']['txmlsresidential*remark']}',
						'{$featureRen}'
					)";
				}
			 }
			 if(count($data)>0){
				
				 $sql="	INSERT INTO `reifaxcounty`.`agent`
							(`idreifax`,`agent`,`phone`,`broker`,`phoneBroker`)
						VALUES
							$valuesagen;
				;";
				$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
				
				$sql="INSERT  INTO `reifaxcounty`.`psummary`  (`idreifax`, `beds`, `bath`, `lsqft`, `yrbuilt`, `saledate`, `saleprice`, `features`)
						VALUES
							$valuesps";
				$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
				
				if($valuesresi!=''){
					$sql="INSERT  INTO `reifaxcounty`.`mlsresidential`
							(`idreifax`, `beds`, `bath`, `dom`, `lsqft`, `tsqft`, `yrbuilt`,`mlnumber`, `ldate`, `lprice`, `remark`, `features`)
						VALUES
							$valuesresi
						";
					$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
				}
				if($valuesren!=''){
					$sql="INSERT  INTO `reifaxcounty`.`rental`
						(`idreifax`, `Address`, `Bath`, `Beds`, `City`, `Latitude`, `Longitude`, `Lprice`, `Lsqft`, `State`,  `Street`, `Yrbuilt`, `Zip`, `Unit`)
						VALUES
							$valuesren
						";
					$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
				}
				if(count($val['sales']['data'])>0){
					$valuesales='';
					foreach ($val['sales']['data'] as $key => $value) {
						$temSal=explode('/', $value['txsales*date'.$key]);
						$temSal=$temSal[2].$temSal[1].$temSal[0];
						$temSalP=$value['txsales*price'.$key];
						$temSalB='';
						$temSalPa='';

					   	$valuesales.=($valuesales==''?'':',')." (
							{$val['idlocal']},
							'{$temSal}',
							'{$temSalP}',
							'{$temSalB}',
							'{$temSalPa}',
							NOW()
						)";
					}
					$sql="INSERT  INTO `reifaxcounty`.`sales`
						(`idreifax`, `date`, `price`, `book`, `page`, `recdate`)
						VALUES
							$valuesales
						";
					$this->conReifax->query($sql) or die($sql.$this->conReifax->error);
				}
			 }
	}
	
	
	/******************************
	*
	*	Obtain Detail from a Html. (Principal Search)
	*
	*******************************/
	public function getDetailResult($html){
	    echo '<br> inicio del parseo';
	    echo date('Y M d H:i:s');
		$xpathFirst		=	$this->managerParseo->getXpathHtml($html,false);
		//$this->getPropertyJson(NULL,$html);
		//$this->urlImages(NULL,$this->parcelid,true);
		echo '<br> 560 -)'.date('Y M d H:i:s');
		$dataJson=$this->getPropertyJson(NULL,$html);
		if(preg_match_all('/trulia\.propertyData\.set\(.*)\);/',$html,$addr)){
			$this->recordError("fallo al encontrar el json pdp_location_data dentro del overview",__LINE__,2);
		}

		$jsonObject		=	json_decode($addr);
		$this->addressfull	=	$dataJson['addressForDisplay'];
		$this->addressfull	=	$dataJson['addressForDisplay'];
		$tempA=explode(',',$this->addressfull);
		$this->address=$tempA[0];
		$this->zip	=	$dataJson['zipCode'];
		$this->propertyId	=	$dataJson['id'];
		$this->city	=	$dataJson['city'];
		$this->unit	=	$dataJson['apartmentNumber'];
		$this->county	=	$dataJson['county'];
		$this->state	=	$dataJson['stateCode'];
		$this->latitude	=	$dataJson['latitude'];
		$this->longitude	=	$dataJson['longitude'];

	
		if(preg_match_all('/typeDisplay":"([\w\-\s]*)"/',$html,$addr)){
			$this->recordError("fallo al encontrar el json typeDisplay dentro del overview",__LINE__,2);
		}
		$this->typePr	=	$addr[1][0];
		
		/**************************
		*	Order Data Recibed
		**************************/
		$standarData = array(
			'parcel #' => 'parcel', 'bedrooms' => 'beds', 'bathrooms' => 'baths', 'mls/id' => 'mls', 'size' => 'sqft', 'lot' => 'lot', 'unit' => 'unit', 'type' => 'type', 'pool' => 'pool', 
			'waterfront' => 'waterfront', 'year built' => 'ybuilt', 'legal description' => 'legald', 'price/sqft' => 'perfsqft', 'lot depth' => 'lotd', 'lot width' => 'lotw', 
			'last sold' => 'lsold', 'last remodel year' => 'lremodely', 'unit cnt' => 'unit', 'total rooms' => 'trooms', '# stories' => 'stories', 'school district' => 'schoold',
			'county' => 'county', 'on zillow' => 'dom', 'cooling' => 'ac', 'price' => 'price', 'property type' => 'type'
		);
		
		/*******************
		*	extrain data remark
		************************/
		

		$results			=	$this->managerParseo->queryDom($xpathFirst,"//span[@itemprop='description']");
		if($results->length){
            foreach ($results as $index => $link){
                $this->remark  =   str_replace('Show more','',$this->managerParseo->clearTrash($this->managerParseo->getHtmlDom($link),'/[^a-zA-Z0-9,-\s]/'));
            }
        }
		else
			$this->recordError("fallo al encontrar el remark de la propiedad",__LINE__,2);
		

		/*******************
		*	extrain data agent
		************************/
		
		
		if(!preg_match_all('/Listing Agent: .*/',$html,$addr)){
			$this->recordError("fallo al encontrar el nombre del agente",__LINE__,2);
		}
		else{
			$this->agentName=trim(str_replace('Listing Agent:','',strip_tags($addr[0][0])));
		}
		
		$agentImg	=	$this->managerParseo->queryDom($xpathFirst,"//div[contains(@class,'contact_module')]//div[contains(@class,'mediaBody')]//img");
		foreach ( $agentImg as $link ){
				$this->agentImg=$link->getAttribute('src');
				
	   	}
		if(!$this->agentImg){
			$this->agentImg='/contacts_tabs/img/default.jpg';
		}
		

        /*********
         * 
         *  extrain features
         *
         *  */
        $this->feature=array();
        $Feature    =   $this->managerParseo->queryDom($xpathFirst,"//div[contains(.,'Features') and contains(@class,'mtl')]//ul[contains(@class,'listBulleted ')]/li");
        
        foreach ($Feature as $index => $link){
            array_push($this->feature,$this->managerParseo->clearTrash($this->managerParseo->getHtmlDom($link,array()),'/[^a-zA-Z0-9,-\s]/'));
        }

        /*********
         * 
         *  extrain public record
         *
         *  */
        $this->pr=array();
        $Feature    =   $this->managerParseo->queryDom($xpathFirst,"//div[contains(.,'Public Records') and contains(@class,'mtl')]//ul[contains(@class,'listBulleted ')]/li");
        foreach ($Feature as $index => $link){
            array_push($this->pr,$this->managerParseo->clearTrash($this->managerParseo->getHtmlDom($link,array()),'/[^a-zA-Z0-9,-\s]/'));
        }

        /*********
         * 
         *  extrain Listing Info 
         *
         *  */
        $this->lis=array();
        $Feature    =   $this->managerParseo->queryDom($xpathFirst,"//div[contains(.,'Listing Info') and contains(@class,'mtl')]//ul[contains(@class,'listBulleted ')]/li");
        foreach ($Feature as $index => $link){
            array_push($this->lis,$this->managerParseo->clearTrash($this->managerParseo->getHtmlDom($link,array()),'/[^a-zA-Z0-9,-\s]/'));
        }


		/*******************
		*	extrain data Broker
		************************/
		
		if(!preg_match_all('/Broker: .*/',$html,$addr)){
			$this->recordError("fallo al encontrar el nombre del broker",__LINE__,2);
		}
		else{
			$this->broker=trim(str_replace('Broker:','',strip_tags($addr[0][0])));
			if(preg_match('/href/',$this->broker,$addr)){
				$this->broker=explode(' ',$this->broker);
				$this->broker=str_replace('"','',$this->broker[0]);
			}
		}

		/*************************************
			extra phone information 
		
		*/
		
		if(!preg_match_all('/\(\d{3}\)\s\d{3}-\d{4}/',$html,$addr)){
			$this->recordError("fallo al encontrar los nuemoers telefonicos",__LINE__,2);
		}
		else{
			$this->agentPhone=$addr[0][0];
			if($addr[0][0]!=$addr[0][count($addr[0])-1])
				$this->agentph2=$addr[0][count($addr[0])-1];
		}
		
        echo '<br> 671 -)'.date('Y M d H:i:s');
		
		//Only use this code for obtain the price
		$results			=	$this->managerParseo->queryDom($xpathFirst,"//tr[contains(.,'Public records')]");
		$temp_dom = new DOMDocument();
		foreach($results as $index => $node){ 
			if($index==0){
				$temp_dom->appendChild($temp_dom->importNode($node,true));
			}
		}
		$htmlHistory=$temp_dom->saveHTML();
		preg_match_all('/price">(.*)</',$htmlHistory,$addr);
		$this->price	=	(!empty($addr[1][0])?str_replace(',', '.', $addr[1][0]):0);
		//END Only use this code for obtain the price
		
		
        echo '<br> 687 -)'.date('Y M d H:i:s');
		$this->priceHistory($xpathFirst); 
        echo '<br> 689 -)'.date('Y M d H:i:s');
		/*
		if(trim($this->listingDate)==''){
			$this->listingDate =$this->salesHistory['data'][0]['txsales*date0'];
		}*/
		
		if(!preg_match_all('/\d+\+? Days on Trulia/',$html,$addr)){
			$this->recordError("fallo al encontrar los dias en el mercado",__LINE__,2);
		}
		else{
			$Residential['dom']	=	preg_replace("/\D/",'',$addr[0][0]);
			$Residential['dom']=intval($Residential['dom']);
			if($Residential['dom']){
				$fecha = date(DATE_RFC822,time ());
				$nuevafecha = strtotime ( '-'.$Residential['dom'].' day') ;
				$this->listingDate = date ( 'm/d/Y' , $nuevafecha );
			}
		}
        echo '<br> 707 -)'.date('Y M d H:i:s');
		
		foreach($results as $link){
			foreach($link->childNodes as $index => $node) {
				if($index == 0){
					$indexName	=	$node->nodeValue;
				}
				if($index == 2){
					$nodesChild	=	$node->childNodes;
					foreach($nodesChild as $subNode) {
						$subNode->nodeValue;
						$newIndex		=	$standarData[strtolower(trim(str_replace(":","",$indexName)))];
						switch($newIndex){
							case	"type":
								switch(trim($subNode->nodeValue)){
									case	"Single-Family Home"	:	$subNode->nodeValue	=	"Single Family";	break;
								}
								break;
						}
						if(!empty($newIndex))
							$Residential[$newIndex]	=	trim($subNode->nodeValue);
						break;
					}
				}
			}
		}
        echo '<br> 733 -)'.date('Y M d H:i:s');
		
		
		$Residential['pool'] = "";$Residential['waterfront'] = "";$Residential['lot']=0;$Residential['sqft']=0;$Residential['ybuilt']="";$Residential['baths']='0';$Residential['beds']='0';
		$results		=	$this->managerParseo->queryDom($xpathFirst,"//ul[contains(@class,'listBulleted ')]/li");
		$arraySphider=array();
		foreach($results as $link){
			array_push($arraySphider,$link->nodeValue);
			if(strpos($link->nodeValue,":")){
				$aux	=	explode(":",$link->nodeValue);
				if(strtolower(trim($aux[0]))=="status")
					$this->status	=	trim($aux[1]);
				if(strtolower(trim($aux[0]))=="price")
					$this->price	=	preg_replace('/\D/','',$aux[1]);
				if(preg_match("/pool/i",$aux[0])){
					$Residential['pool']		=	"Y";
				}
				if(preg_match("/waterfront/i",$aux[0])){
					$Residential['waterfront']	=	"Y";
				}
				if(preg_match("/MLS/i",$aux[0])){
					$Residential['mls']	=	trim($aux[1]);
				}
				if(preg_match("/subdivision/i",$aux[0])){
					$Residential['sdname']		=	trim($aux[1]);
				}
				if(preg_match("/lot size/i",$aux[0])){
					$Residential['lot']		=	preg_replace('/\D/','',$aux[1]);
				}
				if(preg_match("/Sqft/i",$aux[0])){
					$PublicRecord['sqft']		=	preg_replace('/\D/','',$aux[1]);
				}
			}else{
				if(preg_match("/Price/i",$link->nodeValue)){
					$this->price				=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Bedroom/i",$link->nodeValue)){
					$Residential['beds']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Bathroom/i",$link->nodeValue)){
					$Residential['baths']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Built in/i",$link->nodeValue)){
					$Residential['ybuilt']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/pool/i",$link->nodeValue)){
					$Residential['pool']		=	"Y";
				}
				if(preg_match("/Sqft/i",$link->nodeValue)){
					$PublicRecord['sqft']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/waterfront/i",$link->nodeValue)){
					$Residential['waterfront']	=	"Y";
				}
				
			}
		}
        echo '<br> 790 -)'.date('Y M d H:i:s');


		$PublicRecord['pool'] = "";$PublicRecord['waterfront'] = "";$PublicRecord['ybuilt']="";$PublicRecord['baths']='0';$PublicRecord['beds']='0';$PublicRecord['sdname']='';
		$results		=	$this->managerParseo->queryDom($xpathFirst,"//div[@class='mtl']//ul//ul//li");
		
		foreach($results as $link){
						
			if(strpos($link->nodeValue,":")){
				$aux	=	explode(":",$link->nodeValue);
				if(strtolower(trim($aux[0]))=="status")
					$this->status	=	trim($aux[1]);
				if(strtolower(trim($aux[0]))=="price")
					$this->price	=	preg_replace('/\D/','',$aux[1]);
				if(preg_match("/pool/i",$aux[0])){
					$Residential['pool']		=	"Y";
				}
				$aux	=	explode(":",$link->nodeValue);
				if(preg_match("/subdivision/i",$aux[0]))
					$PublicRecord['sdname']		=	trim($aux[1]);
				if(preg_match("/lot size/i",$aux[0])){
					if(preg_match("/acres/i",$aux[1])){
						$PublicRecord['lot']	=	preg_replace('/[a-zA-Z]/','',$aux[1]);
						$PublicRecord['lot']	=	$PublicRecord['lot']*43560;
					}
					else{
						$PublicRecord['lot']	=	preg_replace('/\D/','',$aux[1]);
					}
					
				}
				if(preg_match("/pool/i",$aux[0])){
					$PublicRecord['pool']		=	"Y";
				}
				if(preg_match("/waterfront/i",$aux[0])){
					$PublicRecord['waterfront']	=	"Y";
				}
				if(preg_match("/A\/C/i",$aux[0])){
					$PublicRecord['ac']	=	"Y";
				}
				if(preg_match("/MLS/i",$aux[0])){
					$Residential['MLS']	=	$aux[1];
				}
				if(strtolower($aux[0])=="stories")
					$PublicRecord['stories']		=	preg_replace('/[a-zA-Z]/','',trim($aux[1]));
			}else{
				switch(trim($link->nodeValue)){
					case "Single Family Residential":	$PublicRecord['type']	=	"Single Family";	break;
				}
				if(preg_match("/Bedrooms/i",$link->nodeValue)){
					$PublicRecord['beds']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Bathrooms/i",$link->nodeValue)){
					$PublicRecord['baths']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Built in/i",$link->nodeValue)){
					$PublicRecord['ybuilt']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/Sqft/i",$link->nodeValue)){
					$PublicRecord['sqft']		=	preg_replace('/\D/','',$link->nodeValue);
				}
				if(preg_match("/A\/C/i",$link->nodeValue)){
					$PublicRecord['ac']	=	"Y";
				}
				if(preg_match("/pool/i",$link->nodeValue)){
					$PublicRecord['pool']		=	"Y";
				}
				if(preg_match("/waterfront/i",$link->nodeValue)){
					$PublicRecord['waterfront']	=	"Y";
				}
				if(preg_match("/lot size/i",$link->nodeValue)){
					if(preg_match("/acres/i",$link->nodeValue)){
						$PublicRecord['lot']	=	preg_replace('/[a-zA-Z]/','',$aux[1]);
						$PublicRecord['lot']	=	$PublicRecord['lot']*43560;
					}
					else{
						$PublicRecord['lot']	=	preg_replace('/\D/','',$aux[1]);
					}
					
				}
			}
		}
        echo '<br> 871 -)'.date('Y M d H:i:s');
		
		
		$temresidencial		=    $this->managerParseo->combineData($Residential,$PublicRecord);
        echo '<br> 874 -)'.date('Y M d H:i:s');
		$temPublicRecord	=    $this->managerParseo->combineData($PublicRecord,$Residential);
        echo '<br> 876 -)'.date('Y M d H:i:s');
		
		
        echo '<br> fin del parseo';
        echo date('Y M d H:i:s');
		return json_encode(array("result" => true, "data" => $this->map($temresidencial,$temPublicRecord),'sphider'=>$arraySphider));
	}
	
	/******************************
	*
	*	Obtains the Price History from Trulia
	*
	*******************************/
	private function priceHistory($xpathFirst){
		$results		=	$this->managerParseo->queryDom($xpathFirst,"//tr[contains(.,'Public records')]");
		echo '<br> linea:'.__LINE__;
		$temp_dom = new DOMDocument();
		foreach($results as $n) {
			$temp_dom->appendChild($temp_dom->importNode($n,true));
		}
		echo '<br> linea:'.__LINE__;
		$html			=	str_replace("</td>","|</td>",$temp_dom->saveHTML());
		echo '<br> linea:'.__LINE__;
		$contentTable	=	$this->managerParseo->getXpathHtml($html,false);
		echo '<br> linea:'.__LINE__;
		$results		=	$this->managerParseo->queryDom($contentTable,"//tr[count(td)=5]");
		echo '<br> linea:'.__LINE__;
		
		$detail			=	array();
		foreach ( $results as $link )
		{
			$detail[] = explode("|",str_replace(":","",$link->nodeValue));
		}
		echo '<br> linea:'.__LINE__;
		$this->salesHistory	=	$this->priceHistoryExtra($detail);
		echo '<br> linea:'.__LINE__;
		
		$detail = json_encode($detail);
		$detail = json_decode($detail,true);
		echo '<br> linea:'.__LINE__;

		$ldate=""; $entryDate="";
		///Extract the Listing Date
		foreach($detail as $rows){
				if(trim($rows[1]) == "Listed for sale"){
					$ldate=$rows[0];
					break;
				}
		}
		echo '<br> linea:'.__LINE__;

		///Extract the Entry Date
		$c=0;
		foreach($detail as $rows){
				if($c==0){
					$entryDate=$rows[0];
					break;
				}
		}
		if(empty($ldate))
			$ldate=$entryDate;
		echo '<br> linea:'.__LINE__;
		
		$this->entryDate	=	empty($entryDate)	?	""	:	date("m/d/Y",strtotime($entryDate));

	}
	
	/******************************
	*
	*	Extract Url Images from Trulia
	*
	*******************************/
	public function urlImages($xpath,$parcelid,$internal=FALSE){
		$ArrayImages=array();
		if($internal){
			foreach ( $this->images as $img => $val )
			{
				array_push($ArrayImages,'http://thumbs.trulia-cdn.com/pictures/thumbs_4/'.(preg_replace('/:\d.*/','',$val)));
				$c++;
			}
		}
		else{
			$imagenes = $xpath->query( "//div[@id='image_player_thumbnails']//li");
			$ArrayImages=array();
			foreach ( $imagenes as $link )
			{
				$nodes	=	$link->childNodes;
				foreach($nodes as $child){
					$aux = $child->getAttribute( 'data-big-src' );
					if(!empty($aux))
						$ArrayImages[] = $child->getAttribute( 'data-big-src' );
				}
			}
			$c=0;$ArrayImagesFinal=array();
		}
		$this->userid = $_COOKIE['datos_usr']['USERID'];
		
		
		$dirImages=$this->verifyPathImg();
		
		if(!empty($this->idMultiple))	//Control for Multiple Import, saving all images in one folder for this case see propertyIncludedMultiple.php
			$identifyMultiple	=	$this->idMultiple."-";
		else{
			$identifyMultiple	=	"";
			//if not are multiple import need to clear this folders....
			//Clear all temp directories for new images on a new search
			$this->clearDirectory($dirImages."temp/");
			$this->clearDirectory($dirImages."thumb/");
		}
		
		foreach($ArrayImages as $index => $image){
						
			if(!empty($image)){
				$c++;
				$aux=explode(".",$image);
				$position=count($aux)-1;
				$this->saveImage($image,$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$imageG=$this->images['temp'][] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."temp/$identifyMultiple$c.$aux[$position]");
				$imageT=$this->images['thumb'][] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dirImages."thumb/$identifyMultiple$c.$aux[$position]");	$sql="
				INSERT INTO `temp_imagenes` ( `parcelid`, `image`, `thumb`, `dateReg`)
					VALUES
					(
						'$parcelid',
						'$imageG',
						'$imageT',
						NOW()
					);
				";
				mysql_query($sql) or die ($sql.mysql_error());
			}
		}
		$this->createThumbs($dirImages."temp/",$dirImages."thumb/",96);
	}
	/******************************
	*
	*	Obtains the Sales History from some Properti on Trulia
	*
	*******************************/
	private function priceHistoryExtra($tableWithSales){
		$tempSales = array();$tempSalesCount = 0;
		foreach($tableWithSales as $field){
			if(preg_match("/Sold/i",$field[1])){
				$tempSales['data'][]	=	array(
												"txsales*date$tempSalesCount"		=>	$field[0], 
												"txsales*price$tempSalesCount"		=>	number_format(str_replace(",","",str_replace("$","",$field[2])),2,".",","), 
												"txsales*priceSqft$tempSalesCount"	=>	number_format(str_replace(",","",str_replace("$","",$field[4])),2,".",",")
											);
				$tempSalesCount++;
			}
		}
		//if $tempSalesCount is equal to 0, it's because there are not data and need send a empty array
		if($tempSalesCount	==	0)
			$tempSales['data'][]	=	array();

		$tempSales['total'] = $tempSalesCount;
		return	$tempSales;
	}
	
	/******************************
	*
	*	Mapea the data to the correct fields
	*
	*******************************/
	private function map($residential,$publicR){
		
		/**************************
		*	Standarize status camp
		**************************/
		switch(trim($this->status)){
			case	"Not for Sale":	
				$status	=	"CC";
				break;
			case	"Make me Move":
				$status	=	"CC";
				break;
			case	"For Sale":	
				$status	=	"AA";
				break;
			case	"For Rent":
				$status	=	"BB";
				break;
			case	"Sold":
				$status	=	"CS";
				break;
			case	"By Owner":	
				$status	=	"DD";
				break;
			case	"Bank Owned":	
				$status	=	"EE";
				break;
		}
		/**************************
		*	Standarize status type
		**************************/
		if(empty($publicR['type']) or $publicR['type'] == "--")
			$type = $residential['type'];
		else
			$type = $publicR['type'];

		switch($this->typePr){
			case	"Single Family":		$typePT	=	"01";	break;
			case	"Condo":				$typePT	=	"04";	break;
			case	"Cooperative":			$typePT	=	"04";	break;
			case	"Multi Family":			$typePT	=	"08";	break;
			case	"Vacant Land":			$typePT	=	"00";	break;
			case	"Mobile / Manufactured":$typePT	=	"02";	break;
			default	:$typePT	=	"99"; 
		}

		/**************************
		*	Data for Proterty Table
		**************************/
		unset($propertyArray);
		//$status=="CC" ? $propertyArray=$residential : $propertyArray=$publicR;
		
		$propertyFields = array(
			'txproperty*address' => $this->address,
			'txproperty*addressfull' => $this->addressfull,
			'txproperty*zip' => $this->zip,
			'txproperty*propertyId' => $this->propertyId,
			'txproperty*city' => $this->city,
			'txproperty*unit' => trim(str_replace("UNIT","",$this->unit)),
			//'txproperty*county' => trim($propertyArray['county']),	// OLD, Next line New
			//'txproperty*county' => empty($psummaryArray['county'])	?	trim($residential['county'])	:	trim($psummaryArray['county']),
			'txproperty*county' => $this->county,
			/*//'txproperty*yrbuilt' => $propertyArray['ybuilt'],*/
			'txproperty*xcode' => $typePT,
			'txproperty*xcoded' => $this->typePr,
			'cbproperty*state' => $this->state,
			'txproperty*latitude' => $this->latitude,
			'txproperty*longitude' => $this->longitude,
			'cbproperty*status' => $status
		);
		$status=="CS" ? $psummaryArray=$publicR	: $psummaryArray=$residential;
		/**************************
		*	Data for Psummary Table
		**************************/
		////*Aditional Process For Camp Sale Price And Sale Date*///
		if(trim($publicR['lsold'])== "" or trim($publicR['lsold']) == "--")
			$psummaryLsold = explode("for",$residential['lsold']);
		else
			$psummaryLsold = explode("for",$publicR['lsold']);
		$psummarySaleDate = $this->saleDate($psummaryLsold[0]);
		$psummaryArray['ybuilt']=empty($psummaryArray['ybuilt'])?'NULL':$psummaryArray['ybuilt'];
		////*Aditional Process For Camp Lot*///
		$psummaryFields = array(
			'txpsummary*beds' 		=> $psummaryArray['beds'],
			'txpsummary*bath'		=> $psummaryArray['baths'],
			'txpsummary*lsqft'		=> $psummaryArray['sqft'], 
			'txpsummary*bheated'	=> $psummaryArray['sqft'],
			'txpsummary*folio'		=> $psummaryArray['parcel'],
			'txpsummary*yrbuilt'	=> $psummaryArray['ybuilt'],
			'cbpsummary*pool'		=> !empty($psummaryArray['pool'])	?	"Y"	:	"N",
			'cbpsummary*ac'			=> !empty($psummaryArray['ac'])	?	"Y"	:	"N",
			'txpsummary*saledate'	=> $psummarySaleDate,
			'txpsummary*saleprice'	=> $this->price, 
			'txpsummary*stories'	=> $psummaryArray['stories'],
			'txpsummary*legal'		=> $psummaryArray['legald'],
			'txpsummary*tsqft'		=> $psummaryArray['lot'],
			'cbpsummary*waterf'		=> !empty($psummaryArray['waterfront'])	?	"Y"	:	"N"
		);
		/**************************
		*	Data for Residential Table
		**************************/
		if($status==="AA" or $status==="BB" or $status==="DD" or $status==="EE"){
			$residentialFields	=	array(
				'txmlsresidential*bath'			=>	$residential['baths'],
				'txmlsresidential*beds'			=>	$residential['beds'],
				'txmlsresidential*folio'		=>	$residential['parcel'],
				'txmlsresidential*yrbuilt'		=>	$residential['ybuilt'],
				'txmlsresidential*dom'			=>	$residential['dom'],
				'txmlsresidential*apxtotsqft'	=>	$residential['lot'],
				'txmlsresidential*lsqft'		=>	$residential['sqft'],
				'txmlsresidential*ldate'		=>	$this->listingDate,
				'txmlsresidential*mlnumber'		=>	$residential['MLS'],
				'txmlsresidential*lprice'		=>	$this->price,
				'txmlsresidential*remark'		=>	$this->remark,
				'cbmlsresidential*waterf'		=>	!empty($residential['waterfront'])	?	"Y"	:	"N",
				'cbmlsresidential*pool'			=>	!empty($residential['pool'])	?	"Y"	:	"N",
				'txmlsresidential*entrydate'	=>	$this->entryDate
			);
		}else{
			$residentialFields	=	array(
				'txmlsresidential*folio'		=>	$residential['parcel']
			);
		}
		/**************************
		*	Data for Sales Table
		**************************/
		
		$agentFields=array(
			'txagent*agent'			=>	$this->agentName,
			'txagent*phone'			=>	$this->agentPhone,
			'txagent*broker'			=>	$this->broker,
			'txagent*phoneBroker'		=>	$this->agentph2,
			'txagent*avatar'			=>	$this->agentImg
		);
		
		/**************************
		*	Data for Url Images
		************************** /
		//$this->images;
		
		$foreclosure = array(
			'txpendes*folio'	=>	$psummaryArray['parcel']
		);
		
		$mortgage = array(
			'txmortgage*folio'	=>	$psummaryArray['parcel']
		);
		*/
		$allData = array(
			'property'		=>	$propertyFields,
            'feature'       =>  $this->feature,
            'featurePR'     =>  $this->pr,
            'featureRen'     =>  $this->lis,
			'psummary'		=>	$psummaryFields,
			'agent'			=>	$agentFields,
			'residential'	=>	$residentialFields, 
			'sales'			=>	$this->salesHistory, 
		//	'foreclosure'	=>	$foreclosure, 
			'mortgage'		=>	$mortgage,
		//	'images'		=>	$this->images ,
		/*	'aditionals'	=>	array(
									"countyUrl" =>	$this->urlCounty,
									"urlFree"	=>	$this->urlFree
								)*/
		);
		
		
		return $allData;
		
		
		//Add fields standar
		$newData['street']		=	$this->street;
		$newData['latitude']	=	$this->latitude;
		$newData['longitude']	=	$this->longitude;
	}
	
	private function saleDate($date){
		$date	=	trim($date);
		if(empty($date))
			return "";
		$aux = explode(" ",trim($date));
		$meses = array("Jan" => '01',"Feb" => "02","Mar" => "03","Apr" => "04","May" => "05","Jun" => "06","Jul" => "07","Aug" => "08","Sep" => "09","Oct" => "10","Nov" => "11","Dic" => "12");
		return $aux[1].$meses[$aux[0]]."01";
	}
	
	/******************************
	*
	*	Convert the url to the real Url
	*
	*******************************/
	private function obtainCountyURL( $url ) {
	    $res = array();
	    $options = array( 
	        CURLOPT_RETURNTRANSFER => true,     // return web page 
	        CURLOPT_HEADER         => false,    // do not return headers 
	        CURLOPT_FOLLOWLOCATION => true,     // follow redirects 
	        CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1", // who am i 
	        CURLOPT_AUTOREFERER    => true,     // set referer on redirect 
	        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect 
	        CURLOPT_TIMEOUT        => 120,      // timeout on response 
	        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects 
	    ); 
	    $ch      = curl_init( $url ); 
	    curl_setopt_array( $ch, $options ); 
	    $content = curl_exec( $ch ); 
	    $err     = curl_errno( $ch ); 
	    $errmsg  = curl_error( $ch ); 
	    $header  = curl_getinfo( $ch ); 
	    curl_close( $ch ); 

	    //$res['content'] = $content;     
	    //$res['url'] = $header['url'];
	    //return $res; 
	    return $header['url'];
	}
	
	function ObtainUrlComparables($address){
		$url="http://www.trulia.com/validate.php?tst=h&display=for+sale&search=".urlencode($address);
		$url.="&min_num_beds=&min_num_baths=&min_price=&max_price=&min_size=&filter_pets=&srl=&type=";
		//echo $url;
		$html = $this->getHtml(array('url'    =>  $url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
		
		//echo $html;
		$xpathFirst = $this->managerParseo->queryDom($html,false);
		
		$firstResult = $this->managerParseo->queryDom($xpathFirst,"//div[@id='pdp_comps_table_all_comps_cell']//a[@id='pdp_comps_table_all_comps_link']");
		$urlComps='';
		foreach ( $firstResult as $link ){
			$urlComps = "http://www.trulia.com".$link->getAttribute( 'href' );
			break;
			//echo '<br>'.$Url;
		}
		if($urlComps==""){
			$firstResult = $this->managerParseo->queryDom($xpathFirst,"//div[@id='homes_you_might_like_module']//div[@class='bottom']//a");
			$urlComps='';
			foreach ( $firstResult as $link ){
				$urlComps = "http://www.trulia.com".$link->getAttribute( 'href' );
				break;
				//echo '<br>'.$urlComps;
			}
			if($urlComps=="")
				return "";
			else
				return $urlComps; 
		}else
			return $urlComps;
	}
	
	/******************************
	*
	*	Obtain Detail from a Url. (Principal Search)
	*
	*******************************/
	public function getComparables($url){

		$html = $this->getHtml(array('url'    =>  $url, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
		
		//echo $html;
		$html = str_replace("\\","",$html);
		$html = str_replace("</td>","</td>|",$html);
		$html = str_replace("</th>","</th>*",$html);
		
		$contentTable = $this->managerParseo->queryDom($html,false);
		
		$results = $this->managerParseo->queryDom($contentTable,"//tbody");
		
		preg_match_all('/var\s+(\w+)\s*=\s*(["\']?)(.*?)\2;/i', $html, $matches); 
		
		//print_r($matches);
		$vars=array(); 
		foreach($matches[0] as $result){
			$result=trim(str_replace('var','',$result));
			$result=trim(str_replace(';','',$result));
			$parts=explode('=',$result);
			
			$vars[trim($parts[0])]=$this->replaceString(trim($parts[1]),array("'"));
		}
		//print_r($vars);
		
		$urlComps='http://www.trulia.com/q_property_comps.php?json&pid='.$vars['_MAIN_PROPERTY_ID'].'&type='.$vars['_MAIN_PROPERTY_TYPE'].'&state='.$vars['_MAIN_PROPERTY_STATE'].'&zipCode='.$vars['_MAIN_PROPERTY_ZIP'];
		
		//echo $urlComps;
		
		$json = $this->getHtml(array('url'    =>  $urlComps, 'conMaster' => $this->conMaster, 'condado' => $this->countyProxy));
		
		$comparables=json_decode($json,true);
		
		//print_r($respuesta['sold']);
		
		$dataRtmaster = array();
		
		$dataRtmaster = array_merge($dataRtmaster,$this->createArrayComparables($comparables['sold'],'CC'));
		$dataRtmaster = array_merge($dataRtmaster,$this->createArrayComparables($comparables['for_sale'],'A'));
		
		//print_r($dataRtmaster);
		
		return $dataRtmaster;
	}

	/******************************
	*
	*	Clear Any folder on the System
	*
	*******************************/
	public function clearDirectory($carpeta){
		foreach(glob($carpeta."*") as $archivos_carpeta){
			if(!is_dir($archivos_carpeta)) unlink($archivos_carpeta);
		}
	}

	private function createArrayComparables($arrayComp,$status){
		$arrayReturn=array();
		//print_r($arrayComp);
		
		$typePro=array(
			'Condo' 		=> 'Condo/Apartament',
			'Apartment'		=> 'Condo/Apartament',
			'Townhouse'		=> 'Condo/Apartament',
			'Townhome'		=> 'Condo/Apartament',
			'Apartment/condo/townhouse'		=> 'Condo/Apartament',
			'Apt/Condo/Twnhm'				=> 'Condo/Apartament',
			'Single Family'			=> 'Single Family',
			'Single-family'			=> 'Single Family',
			'Single-Family Home'	=> 'Single Family',
			'Multi family'			=> 'Multy Family',
			'Multi-family'			=> 'Multy Family',
			'Multi-Family Home'		=> 'Multy Family',
			'Mobile/manufactured'	=> 'Mobile',
			'Lost'			=> 'Lost/Land',
			'Land'			=> 'Lost/Land',
			'Condo' 		=> 'Condo/Apartament',
			'Apartment'		=> 'Condo/Apartament',
			'Townhouse'		=> 'Condo/Apartament',
			'Single Family'	=> 'Single Family',
			'Multi Family'	=> 'multy Family',
			'Lost'			=> 'Lost/Land',
			'Land'			=> 'Lost/Land'
		);
			
		$j=0;
		for($i=0;$i<count($arrayComp);$i++){
			$arrayReturn[$j]['status']=$status;
			$arrayReturn[$j]['address']=$this->getAddress($arrayComp[$i]['address']);
			$arrayReturn[$j]['distance']=$this->replaceString($arrayComp[$i]['distance'],array('mi'));
			
			$arrayReturn[$j]['saledate']=$this->saleComparable($arrayComp[$i]['date']);
			if($status=='CC'){
				$arrayReturn[$j]['saleprice']=$this->replaceString($arrayComp[$i]['price'],array('$',','));
				$arrayReturn[$j]['lprice']=0;
				$arrayReturn[$j]['dom']=0;
			}else if($status=='A'){
				$arrayReturn[$j]['lprice']=$this->replaceString($arrayComp[$i]['price'],array('$',','));
				$arrayReturn[$j]['dom']=$this->replaceString($arrayComp[$i]['days'],array('$',','));;
				$arrayReturn[$j]['saleprice']=0;
				$arrayReturn[$j]['propertyType']= $typePro[trim($arrayComp[$i]['property_type'])]=='' ? trim($arrayComp[$i]['property_type']) : $typePro[trim($arrayComp[$i]['property_type'])];
			}
			
			$arrayReturn[$j]['bath']=($arrayComp[$i]['baths']!='' ? $arrayComp[$i]['baths'] : 0);
			$arrayReturn[$j]['bed']=($arrayComp[$i]['beds']!='' ? $arrayComp[$i]['beds'] : 0);
			$arrayReturn[$j]['lsqft']=($arrayComp[$i]['sqft']!='' ? $this->replaceString($arrayComp[$i]['sqft'],array('sqft',',')) : 0);
			$arrayReturn[$j]['tsqft']=($arrayComp[$i]['lot_size']!='' ? $this->replaceString($arrayComp[$i]['lot_size'],array('sqft',',')) : 0);
			$arrayReturn[$j]['yearbuilt']=($arrayComp[$i]['year_built']!='' ? $arrayComp[$i]['year_built'] : 0); 
			$arrayReturn[$j]['pricesqft']=($arrayComp[$i]['ppsqft']!='' ? $this->replaceString($arrayComp[$i]['ppsqft'],array('$',',')) : 0);
			$arrayReturn[$j]['latitude']=($arrayComp[$i]['lat']!='' ? $arrayComp[$i]['lat'] : 0);
			$arrayReturn[$j]['longitude']=($arrayComp[$i]['lng']!='' ? $arrayComp[$i]['lng'] : 0);
			$j++;
		}
		return $arrayReturn;
	}
	
	private function saleComparable($date){
		$date	=	trim(str_replace(',','',$date));
		if(empty($date))
			return "";
		$aux = explode(" ",trim($date));
		$meses = array("Jan" => '01',"Feb" => "02","Mar" => "03","Apr" => "04","May" => "05","Jun" => "06","Jul" => "07","Aug" => "08","Sep" => "09","Oct" => "10","Nov" => "11","Dec" => "12");
		$day=$aux[1];
		if(strlen($day)==1 || $day<10){
			$day='0'.$day;
		}
		return $aux[2].$meses[$aux[0]].$day;
	}
	
	private function replaceString($string,$replaces){
		$returnString = $string;
		foreach($replaces as $replace){
			$returnString = str_replace($replace,'',$returnString);
		}
		return $returnString;
	}
	
	private function getAddress($stringAddress){
		$find='>';
		$finddelimiter='</';
		$pos = strpos($stringAddress, $find);
		$address = substr($stringAddress,$pos+strlen($find)); 
		$pos = strpos($address, $finddelimiter);
		$address = substr($address,0,$pos); 
		//$data = str_replace('"',"",$data);
		//echo '<br>'.$data;
		return trim($address);
	}
	function diferenciaEntreFechas($fecha_principal, $fecha_secundaria, $obtener = 'DIAS', $redondear = false){
		$f0 = strtotime($fecha_principal);
		$f1 = strtotime($fecha_secundaria);
		if ($f0 && $f1) { $tmp = $f1; $f1 = $f0; $f0 = $tmp; }
		$resultado = ($f0 - $f1);
		switch ($obtener) {
		   default: break;
		   case "MINUTOS"   :   $resultado = $resultado / 60;   break;
		   case "HORAS"     :   $resultado = $resultado / 60 / 60;   break;
		   case "DIAS"      :   $resultado = $resultado / 60 / 60 / 24;   break;
		   case "SEMANAS"   :   $resultado = $resultado / 60 / 60 / 24 / 7;   break;
		}
		if($redondear) $resultado = round($resultado);
		return $resultado;
	}
}
?>