<?php	
	include('../../properties_conexion.php');
	conectar();
?>
<script>
	Ext.ns("resultPT");
	Ext.onReady(function(){							
		var button = new Ext.Button({
			text	:	'Search',
			width	:	100,
			handler: function() {
			
				if($("#form-list-pt input[name=address]").val() != "" && $("#form-list-pt select[name=select-states]").val() == "00" && $("#form-list-pt select[name=listingNumber]").val() != ""){
					if($("#form-list-pt select[name=select-states]").val() == "00"){
						alert("Seleccione");
						return false;
					}
					alert("Inserte Address");
					return false;
				}
				if($("#form-list-pt select[name=listingNumber]").val() == ""){
					alert("Ingrese un Address or LKisting Number ");
					return false;
				}
				loading_win.show();
				$.ajax({
					url		: 'properties_tabs/propertyImport/property.search.url.php',
					type	: 'POST',
					dataType: 'json',
					data	: $('#form-list-pt').serialize(),
					success	: function (result){
						//loading_win.show();
						if(result.url || result.result == true){
							if(result.option === 'Z'){
								$.ajax({
									url		: 'properties_tabs/propertyImport/property.detail.Zurl.php',
									type	: 'POST',
									dataType: 'json',
									data	: {
										detail:result.url,
										aditional:result.aditional
									},
									success	: function (json){
										if(json.result){
											resultPT.result	=	json.data;
											if(document.getElementById('result-pt')){
												var tabs = Ext.getCmp('tabs-import-pt');
												var tab = tabs.getItem('result-pt');
												tabs.remove(tab);
											}
												
											Ext.getCmp('tabs-import-pt').add({
												//xtype:	'component', 
												title:		'Result', 
												closable:	true,
												autoScroll:	true,
												id:			'result-pt', 
												autoLoad:	{url:'/properties_tabs/propertyImport/propertyImportResult.php',scripts:true}
											}).show();
										
											loading_win.hide();
										}else{
											loading_win.hide();
										}
									},
									error: function(){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','Sorry. Please, Try again.');
									}
								});
							}else if(result.option === 'R'){
								$.ajax({
									url		: 'properties_tabs/propertyImport/property.detail.Rurl.php',
									type	: 'POST',
									dataType: 'json',
									data	: {detail:result.url},
									success	: function (json){
										if(json.result){
											resultPT.result	=	json.data;
											if(document.getElementById('result-pt')){
												var tabs = Ext.getCmp('tabs-import-pt');
												var tab = tabs.getItem('result-pt');
												tabs.remove(tab);
											}
												
											Ext.getCmp('tabs-import-pt').add({
												//xtype:	'component', 
												title:		'Result', 
												closable:	true,
												autoScroll:	true,
												id:			'result-pt', 
												autoLoad:	{url:'/properties_tabs/propertyImport/propertyImportResult.php',scripts:true}
											}).show();
										
											loading_win.hide();
										}else{
											loading_win.hide();
										}
									},
									error: function(){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','Sorry. Please, Try again.');
									}
								});
							}else if(result.option === 'T'){
								resultPT.result	=	result.data;
								if(document.getElementById('result-pt')){
									var tabs = Ext.getCmp('tabs-import-pt');
									var tab = tabs.getItem('result-pt');
									tabs.remove(tab);
								}
									
								Ext.getCmp('tabs-import-pt').add({
									//xtype:	'component', 
									title:		'Result', 
									closable:	true,
									autoScroll:	true,
									id:			'result-pt', 
									autoLoad:	{url:'/properties_tabs/propertyImport/propertyImportResult.php',scripts:true}
								}).show();
							
								loading_win.hide();
							}
						}else{
							loading_win.hide();
							if(result.option==='Z')
								alert("Can't Find This Address");
							if(result.option==='R')
								alert("Can't Find by Listing Number");
						}
					},
					error: function(){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','Can\'t Find This Address');
					}
				});
			},
			id		:	'submit_button',
			cls		:	'x-btn-text',
			iconCls	:	'global-search',
			//icon	:	 "http://www.realtytask.com/img/toolbar/search.png",
			scale	:	"large",
			renderTo:	'bt-search-list'
		});
		var fondo = formulEditPt = new Ext.FormPanel({
				url			:	'properties_tabs/propertyEdit/propertyEdited.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				renderTo	:	'fondoE',
				//id			:	'formulEditPt',
				//name		:	'formulEditPt',
				items		:	[{
					xtype	:	'fieldset',
					//title	:	'Property',
					//width	:	system_width-40,
					height	:	260,
					layout	:	'column',
				}]
			})
	});
</script>
<div id="fondoE"></div>
<div align="center" style="position: relative; top: -250px;"> <!--class="search_realtor_fondo"-->
	<form id="form-list-pt">
	    <table border="0" cellpadding="0" cellspacing="0"  style="font-size:12px; margin:50px auto auto;">
	        <tr class="search_realtor_titulo">    	    	
	            <td>Address</td>                
	            <td>&nbsp;&nbsp;&nbsp;State</td>                              
	            <td>&nbsp;</td>                              
	        </tr>
	        <tr>    	    	
	            <td>
					<div style="width:550px;padding:5px;border:2px solid #005C83;border-radius:10px; text-align:center;">
						<input type="text" style="border:none;width:550px;font-size: 21px;height: 25px;line-height: 1.308em;" placeholder="Address City Optional Zip" name="address">
					</div>
				</td>
	            <td width="230" align="center">
					<div style="width:200px;padding:5px;border:2px solid #005C83;border-radius:10px; text-align:center;">
						<select name="select-states" id="select-states" style="border:none;width:200px;font-size: 21px;height: 25px;line-height: 1.308em;">
						<script>
							$.ajax({
								url		: '/resources/Json/states.json',
								dataType: 'json',
								success	:function (res){
									$("#form-list-pt select[name=select-states]").append("<option value='00'>Select a State</option>");
									$(res).each(function (index){
										$("#form-list-pt select[name=select-states]").append("<option value='"+res[index].initial+"'>"+res[index].name+"</option>");
									});
								}
							});
						</script>
		            	</select>
					</div>
				</td>
				<td>
					<div id="bt-search-list"></div>
				</td>
	        </tr>
			<!--<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr class="search_realtor_titulo">
				<td colspan="3">Or By Listing Number</td>
			</tr>
			<tr>
				<td colspan="3">
					<div style="width:200px;padding:5px;border:2px solid #005C83;border-radius:10px; text-align:center;">
						<input type="text" style="border:none;width:200px;font-size: 21px;height: 25px;line-height: 1.308em;" placeholder="Listing Number" name="listingNumber">
					</div>
				</td>
			</tr>-->
	    </table>
	</form>
</div>