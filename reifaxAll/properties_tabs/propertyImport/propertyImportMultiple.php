<?php	
	include('../../properties_conexion.php');
	conectar();
?>
<style>
textarea:focus::-webkit-input-placeholder{
    color:#fff;
}?
</style>
<script>
	Ext.ns("MultipleImport");
	$(document).ready(function (){
		/*$("textarea").attr('placeholder','Example By Address:                                                First Address City, Optional Zip                                  Second Address City, Optional Zip                                 Thirth Address City, Optional Zip            ----------                                                       Example By Listing Number:                                     0123456ACB                                                     0123456ACB                                                     0123456ACB');
		$("textarea").focus(function() {
		    $(this).attr('placeholder','');
		});
		$("textarea").focusout(function() {
			$(this).attr('placeholder','Example By Address:                                                First Address City, Optional Zip                                  Second Address City, Optional Zip                                 Thirth Address City, Optional Zip            ----------                                                       Example By Listing Number:                                     0123456ACB                                                     0123456ACB                                                     0123456ACB');
		});*/
		$("textarea").attr('placeholder','Example By Address:                                                First Address City, Optional Zip                                  Second Address City, Optional Zip                                 Thirth Address City, Optional Zip            ');
		$("textarea").focus(function() {
		    $(this).attr('placeholder','');
		});
		$("textarea").focusout(function() {
			$(this).attr('placeholder','Example By Address:                                                First Address City, Optional Zip                                  Second Address City, Optional Zip                                 Thirth Address City, Optional Zip            ');
		});
	});
	//Ext.ns("resultPT");
	Ext.onReady(function(){
			
		var buttonMultiplePT = new Ext.Button({
			text	:	'Import',
			width	:	100,
			handler: function() {
			
				if($("#form-listMultiple-pt textarea[name=mensaje]").val() == ""){
					alert("Insert Address");
					return false;
				}
				if($("#form-listMultiple-pt select[name=select-states-multiple]").val() == "00"){
					alert("Select a State");
					return false;
				}
				
				var aux = $("#form-listMultiple-pt textarea[name=mensaje]").val();
				MultipleImport.type	=	$("#form-listMultiple-pt select[name=select-type-multiple]").val();
				MultipleImport.state=	$("#form-listMultiple-pt select[name=select-states-multiple]").val();
				MultipleImport.propertiesToImpport	=	aux.split('\n');
				MultipleImport.propertiesToImpport.clean("");	//Clear all Empty Values inside Array
				
				if(document.getElementById('multiple-result-pt')){
					var tabs = Ext.getCmp('tabs-import-pt');
					var tab = tabs.getItem('multiple-result-pt');
					tabs.remove(tab);
				}
					
				Ext.getCmp('tabs-import-pt').add({
					//xtype:	'component', 
					title:		'Multiple Result', 
					closable:	true,
					autoScroll:	true,
					id:			'multiple-result-pt', 
					autoLoad:	{url:'/properties_tabs/propertyImport/propertyImportMultipleResult.php',scripts:true}
				}).show();
			},
			id		:	'submit_button',
			//icon	:	 "http://www.realtytask.com/img/toolbar/search.png",
			cls		:	'x-btn-text',
			iconCls	:	'global-search',
			scale	:	"large",
			renderTo:	'bt-searchMultiple-list'
		});
		
		var buttonclearMultiplePT = new Ext.Button({
			text	:	'Clear List',
			width	:	100,
			handler: function() {
			
				$("#form-listMultiple-pt textarea[name=mensaje]").val("");
				$("textarea").trigger("focusout");
			},
			//icon	:	"http://www.realtytask.com/img/toolbar/search.png",
			cls		:	'x-btn-text',
			iconCls	:	'icon-clear',
			scale	:	"large",
			renderTo:	'bt-clearMultiple-list'
		});
		var fondo = formulEditPt = new Ext.FormPanel({
				//url			:	'properties_tabs/propertyEdit/propertyEdited.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				renderTo	:	'fondoMultiplePT',
				//id			:	'formulEditPt',
				//name		:	'formulEditPt',
				items		:	[{
					xtype	:	'fieldset',
					//title	:	'Property',
					//width	:	600,
					height	:	420,
					layout	:	'column',
				}]
			})
	});
</script>
<div id="fondoMultiplePT"></div>
<div align="center" style="position: relative; top: -470px;"> <!--class="search_realtor_fondo"-->
	<form id="form-listMultiple-pt">
	    <table border="0" cellpadding="0" cellspacing="0"  style="font-size:12px; margin:50px auto auto;">
	        <tr class="search_realtor_titulo">    	    	
	            <td>State</td>                
	            <td>&nbsp;</td>
	            <td><div style="display:none;">Type</div></td>                              
	        </tr>
	        <tr>    	    	
	            <td>
					<div style="width:200px;padding:5px;border:2px solid #005C83;border-radius:10px; text-align:center;">
						<select name="select-states-multiple" id="select-states-multiple" style="border:none;width:200px;font-size: 21px;height: 25px;line-height: 1.308em;">
						<script>
							$.ajax({
								url		: '../../resources/Json/states.json',
								dataType: 'json',
								success	:function (res){
									$("#form-listMultiple-pt select[name=select-states-multiple]").append("<option value='00'>Select a State</option>");
									$(res).each(function (index){
										$("#form-listMultiple-pt select[name=select-states-multiple]").append("<option value='"+res[index].initial+"'>"+res[index].name+"</option>");
									});
								}
							});
						</script>
		            	</select>
					</div>
				</td>
	            <td width="530" align="center" style="vertical-align: bottom;"><p style="color:#005C83">Each Property or Listing Number in a different line.</p></td>
				<td>
					<div style="display:none;width:200px;padding:5px;border:2px solid #005C83;border-radius:10px; text-align:center;">
						<select name="select-type-multiple" id="select-type-multiple" style="border:none;width:200px;font-size: 21px;height: 25px;line-height: 1.308em;">
							<option value="A">By Address</option>
							<!--<option value="B">By Listing Number</option>-->
		            	</select>
					</div>
				</td>
	        </tr>
			<tr>
				<td colspan="3" align="center">
					<textarea name="mensaje" cols="70" rows="15"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<table cellpadding="2">
						<tr>
							<td>
								<div id="bt-searchMultiple-list"></div>
							</td>
							<td>
								<div id="bt-clearMultiple-list"></div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
	    </table>
	</form>
</div>