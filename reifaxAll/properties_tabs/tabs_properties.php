<?php
	include('../properties_conexion.php');
	conectar();
	
	$que="SELECT * FROM ximausrs WHERE userid=".$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($que) or die($que.mysql_error());
	$USERCURRENT=mysql_fetch_array($result);
?>
<div align="left" style="height:100%">
    <div id="body-pt" style="height:100%">
        <div id="principal-pt" style="padding-top:2px;"></div>
    </div>
</div>
<script>
	var ancho=640;
	<?php if($USERCURRENT['idstatus']!=8 && $USERCURRENT['idstatus']!=9){ ?>
		if(user_loged) ancho=system_width; 
	<?php } ?>
	var properties = new Ext.TabPanel({
		id				:	'tabs-pt',
		renderTo		:	'principal-pt',
   		activeTab		:	0,
		autoWidth		:	true,
		height			:	system_height - ($("#principal-pt").offset().top - tablevel_height),
		plain			:	true,
		enableTabScroll	:	true,
		defaults		:	{  
			autoScroll	:	false
		},
		items:[
			{
				title	:	'List', 
				id	 	:	'listtab-pt',
				autoLoad:	{
					url		:	'properties_tabs/propertyList/propertyList.php', 
					scripts	:	true,
					timeout	:	10800
				}
			},{
				title	:	'Import',
				id	 	:	'importtab-pt',
				autoLoad:	{
					url		:	'properties_tabs/propertyImport/propertyImport.php', 
					scripts	:	true,
					timeout	:	10800
				}
			},{
				title	:	'Blocked',
				id	 	:	'blockedtab-pt',
				autoLoad:	{
					url		:	'properties_tabs/propertyBlock/propertyBlock.php', 
					scripts	:	true,
					timeout	:	10800
				}
			}
		],
		listeners: {
			'tabchange': function(tabpanel,tab){
				if(tab){
					if(tab.id!='comparables-pt'){
						if(document.getElementById('comparable_mymap_control_mapa_div'))
							document.getElementById('comparable_mymap_control_mapa_div').style.display='none';
						if(document.getElementById('comparableact_mymap_control_mapa_div'))
							document.getElementById('comparableact_mymap_control_mapa_div').style.display='none';
					}else{
						if(document.getElementById('comparable_mymap_control_mapa_div'))
							document.getElementById('comparable_mymap_control_mapa_div').style.display='';
						if(document.getElementById('comparableact_mymap_control_mapa_div'))
							document.getElementById('comparableact_mymap_control_mapa_div').style.display='';
						
						if(document.getElementById('tabs-comp')){
							if(comparablesTabs.getActiveTab().id=='comparables-comp'){
								if(document.getElementById('comparable_mymap_control_mapa_div'))
									document.getElementById('comparable_mymap_control_mapa_div').style.display='';
								if(document.getElementById('comparableact_mymap_control_mapa_div'))
									document.getElementById('comparableact_mymap_control_mapa_div').style.display='none';
							}else{
								if(document.getElementById('comparable_mymap_control_mapa_div'))
									document.getElementById('comparable_mymap_control_mapa_div').style.display='none';
								if(document.getElementById('comparableact_mymap_control_mapa_div'))
									document.getElementById('comparableact_mymap_control_mapa_div').style.display='';
							}
						}
					}
				}
			}
		}
	});
</script>
