<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 8px 1px; 
}
</style>
<div align="left" id="principal-block-pt" style="background-color:#FFF;border-color:#FFF">
	<div align="center" style=" background-color:#FFF; margin:auto; width:auto;">
  		<div id="block-filters-pt"></div>
        <div id="block-pt" align="left" style="margin-top:5px;"></div> 
	</div>
</div>
<script>
	var limitpropertiesBlock = 50;
	var selectedGridPropertyBlock = new Array();
	var AllCheckPropertyBlock = false;
	
	var filterfield='agent';
	var filterdirection='ASC';
	
	//var filterPropeprtyBlockStatus 	=	'';
	var filterPropeprtyBlockYrBuilt =	'';
	var filterPropeprtyBlockBaths 	=	'';
	var filterPropeprtyBlockCounty 	=	'';
	var filterPropeprtyBlockAddress =	'';
	var filterPropeprtyBlockBeds 	=	'';
	var filterPropeprtyBlockType 	=	'';
	
	var storePropertyBlock = new Ext.data.JsonStore({
        url: 'properties_tabs/propertyBlock/propertyCore.php',
		fields: [
           	{name: 'parcelid', type: 'int'},
           	{name: 'addressfull'},
			//{name: 'status'},
			{name: 'county'},
			{name: 'CCodeD'},
			{name: 'lsqft'},
			{name: 'bheated'},
			{name: 'beds'},
			{name: 'bath', type: 'int'},
			{name: 'yrbuilt'}
        ],
		root			:	'records',
		totalProperty	:	'total',
		baseParams: {
			'userid'	:	<?php echo $userid;?>
		},
		remoteSort		:	true,
		sortInfo		:	{
			field		:	'parcelid',
			direction	:	'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners		:	{
			'beforeload': function(store,obj){
				//storePropertyAll.load();
				AllCheckPropertyBlock=false;
				selectedGridPropertyBlock=new Array();
				smPropertiesBlock.deselectRange(0,limitpropertiesBlock);
				//obj.params.status	=	filterPropeprtyBlockStatus;
				obj.params.types	=	filterPropeprtyBlockType;
				obj.params.address	=	filterPropeprtyBlockAddress;
				obj.params.county	=	filterPropeprtyBlockCounty;
				obj.params.beds		=	filterPropeprtyBlockBeds;
				obj.params.baths	=	filterPropeprtyBlockBaths;
				obj.params.yrbuilt	=	filterPropeprtyBlockYrBuilt;
			},
			'load' : function (store,data,obj){
				if (AllCheckPropertyBlock){
					Ext.get(gridPropertyBlock.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckPropertyBlock=true;
					gridPropertyBlock.getSelectionModel().selectAll();
					selectedGridPropertyBlock=new Array();
				}else{
					AllCheckPropertyBlock=false;
					Ext.get(gridPropertyBlock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selectedGridPropertyBlock.length > 0){
						for(val in selectedGridPropertyBlock){
							var ind = gridPropertyBlock.getStore().find('agentid',selectedGridPropertyBlock[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridPropertyBlock.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storePropertyAll = new Ext.data.JsonStore({
        url			:	'properties_tabs/propertyList/propertyCore.php',
		fields		:	[
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root		:	'records',
		totalProperty:	'total',
		baseParams	:	{
			'userid'	:	<?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				//obj.params.status	=	filterPropeprtyBlockStatus;
				obj.params.types	=	filterPropeprtyBlockType;
				obj.params.address	=	filterPropeprtyBlockAddress;
				obj.params.county	=	filterPropeprtyBlockCounty;
				obj.params.beds		=	filterPropeprtyBlockBeds;
				obj.params.baths	=	filterPropeprtyBlockBaths;
				obj.params.yrbuilt	=	filterPropeprtyBlockYrBuilt;
			}
		}
    });
	var smPropertiesBlock = new Ext.grid.CheckboxSelectionModel({
		checkOnly	:	true, 
		width		:	25,
		listeners	:	{
			"rowselect": function(selectionModel,index,record){
				console.debug(selectionModel);
				console.debug(index);
				console.debug(record);
				if(selectedGridPropertyBlock.indexOf(record.get('parcelid'))==-1)
					selectedGridPropertyBlock.push(record.get('parcelid'));
					
				if(Ext.fly(gridPropertyBlock.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckPropertyBlock=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selectedGridPropertyBlock = selectedGridPropertyBlock.remove(record.get('parcelid'));
				AllCheckPropertyBlock=false;
				Ext.get(gridPropertyBlock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarFilterListBlock	=	new	Ext.Toolbar({
		renderTo	:	'block-filters-pt',
		items		:[
			new	Ext.Button({
				tooltip: 'Delete',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-delete',
				handler		:	function(){
					if(selectedGridPropertyBlock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Properties to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var propertyIds=selectedGridPropertyBlock[0];
					for(i=1; i<selectedGridPropertyBlock.length; i++)
						propertyIds+=','+selectedGridPropertyBlock[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg	:	'Checking...',
						url		:	'properties_tabs/propertyList/propertyCore.php', 
						method	:	'POST',
						timeout	:	600000,
						params	:{ 
							type	:	'delete',
							propertyIds:	propertyIds
						},
						failure:	function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var optionals = Ext.decode(response.responseText);
							if(optionals.success){
								storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
								Ext.Msg.show({
									title	:	"Property Result", 
									buttons	:	Ext.Msg.OK,
									minWidth:	150,
									msg		:	optionals.msg,
									icon	:	Ext.MessageBox.INFO
								});
							}else{
								Ext.Msg.show({
									title	:	"Warning", 
									buttons	:	Ext.Msg.OK,
									minWidth:	100,
									msg		:	optionals.msg,
									icon	:	Ext.MessageBox.ERROR
								});
							}
						}                                
					});
				}
			}),new	Ext.Button({
				tooltip: 'Print',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-print',
				handler		:	function(){
					if(selectedGridPropertyBlock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to print.'); return false;
					}
					
					loading_win.show();
					
					var propertyIds=selectedGridPropertyBlock[0];
					for(i=1; i<selectedGridPropertyBlock.length; i++)
						propertyIds+=','+selectedGridPropertyBlock[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 8,
							sort: filterfield,
							dir: filterdirection,
							propertyIds: propertyIds
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.realtytask.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new	Ext.Button({
				tooltip: 'Export to Excel',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-export',
				handler		:	function(){
					if(selectedGridPropertyBlock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to export.'); return false;
					}
					
					loading_win.show();
					
					var propertyIds=selectedGridPropertyBlock[0];
					for(i=1; i<selectedGridPropertyBlock.length; i++)
						propertyIds+=','+selectedGridPropertyBlock[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 8,
							sort: filterfield,
							dir: filterdirection,
							propertyIds: propertyIds
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.realtytask.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Add Filter',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls	:'icon-filter-plus',
				handler: function(){
					//filterPropeprtyBlockStatus 	=	'';
					filterPropeprtyBlockYrBuilt =	'';
					filterPropeprtyBlockBaths 	=	'';
					filterPropeprtyBlockCounty 	=	'';
					filterPropeprtyBlockAddress =	'';
					filterPropeprtyBlockBeds 	=	'';
					filterPropeprtyBlockType 	=	'';
					
					var filterProppertiesBlockPT = new Ext.FormPanel({
						url:'properties_tabs/propertyBlock/propertyCore.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						//collapsible: true,
						collapsed: false,
						title: 'Filters',
						//renderTo: 'block-filters-pt',
						id: 'filterProppertiesBlockPT',
						name: 'filterProppertiesBlockPT',
						items:[{
							xtype: 'fieldset',
							//title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							items: [/*{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype			:	'combo',
									mode			:	'local',
									fieldLabel		:	'Status',
									triggerAction	:	'all',
									width			:	150,
									store			:	propertiesStatus,
									displayField	:	'desc',
									valueField		:	'id',
									name			:	'ffilterPropeprtyBlockStatus',
									//value			:	'ALL',
									hiddenName		:	'filterPropeprtyBlockStatus',
									//hiddenValue		:	'ALL',
									emptyText		:	'SELECT',
									//allowBlank	:	false,
									listeners		:	{
										beforequery	:	function(qe){
											//delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockStatus=newvalue;
										}
									}
								}]
							},*/{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'filterPropeprtyBlockAddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockAddress	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftypeagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Beds',
									name		  : 'filterPropeprtyBlockBeds',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockBeds=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fweb',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'County',
									name		  : 'filterPropeprtyBlockCounty',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockCounty	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fphone',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Baths',
									name		  : 'filterPropeprtyBlockBaths',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockBaths	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Year Built',
									name		  : 'filterPropeprtyBlockYrBuilt',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockYrBuilt	=	newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcompany',
								items	: [{
									xtype			:	'combo',
									mode			:	'local',
									fieldLabel		:	'Type',
									triggerAction	:	'all',
									width			:	150,
									store			:	propertiesType,	//Are in globalFunctions.js
									displayField	:	'desc',
									valueField		:	'id',
									name			:	'ffilterPropeprtyBlockType',
									//value         :	'00',
									hiddenName		:	'filterPropeprtyBlockType',
									emptyText		:	'SELECT',
									//hiddenValue   :	'00',
									allowBlank		: 	true,
									listeners		: 	{
										beforequery	:	function(qe){
											//delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filterPropeprtyBlockType=newvalue;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.realtytask.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.realtytask.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									//filterPropeprtyBlockStatus 	=	'';
									filterPropeprtyBlockYrBuilt =	'';
									filterPropeprtyBlockBaths 	=	'';
									filterPropeprtyBlockCounty 	=	'';
									filterPropeprtyBlockAddress =	'';
									filterPropeprtyBlockBeds 	=	'';
									filterPropeprtyBlockType 	=	'';
									Ext.getCmp('filterProppertiesBlockPT').getForm().reset();
									
									storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 285,
								modal	 	: true,  
								plain       : true,
								items		: filterProppertiesBlockPT,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filter',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-filter-negative',
				handler: function(){
					//filterPropeprtyBlockStatus 	=	'';
					filterPropeprtyBlockYrBuilt =	'';
					filterPropeprtyBlockBaths 	=	'';
					filterPropeprtyBlockCounty 	=	'';
					filterPropeprtyBlockAddress =	'';
					filterPropeprtyBlockBeds 	=	'';
					filterPropeprtyBlockType 	=	'';
					
					storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
				}
			}),{
				xtype: 'tbspacer', 
				width: 30
			},new Ext.Button({
				tooltip: 'Unblock Property',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-unblock',
				handler: function(){
					//console.debug(smPropertiesBlock.getSelections());
					//console.debug(smPropertiesBlock.getSelections().length);
					
					if(selectedGridPropertyBlock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to block.'); return false;
					}
					if(AllCheckPropertyBlock==true){
						selectedGridPropertyBlock=new Array();
						var totales = storePropertyAll.getRange(0,storePropertyAll.getCount());
						
						for(i=0;i<storePropertyAll.getCount();i++){
							if(selectedGridPropertyBlock.indexOf(totales[i].data.agentid)==-1)
								selectedGridPropertyBlock.push(totales[i].data.agentid);
						}
					
					}
					var propertyIds=selectedGridPropertyBlock[0];
					for(i=1; i<selectedGridPropertyBlock.length; i++)
						propertyIds+=','+selectedGridPropertyBlock[i];
					
					if(selectedGridPropertyBlock.length>1)
						var msgUnblock	=	"Unblock properties?";
					else
						var msgUnblock	=	"Unblock property?";
						
					Ext.MessageBox.show({
					    title:    'Properties',
					    msg:      msgUnblock,
					    buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
					    fn: function(btn){
								if(btn=='cancel'){
									return;
								}
								loading_win.show();
					
								Ext.Ajax.request( 
								{  
									waitMsg: 'Checking...',
									url: 'properties_tabs/propertyBlock/propertyCore.php', 
									method: 'POST',
									timeout :600000,
									params: { 
										type: 'unblock',
										propertyIds: propertyIds,
										blockproperties: btn
									},
									
									failure:function(response,options){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','ERROR');
									},
									
									success:function(response,options){
										var resp   = Ext.decode(response.responseText);
										loading_win.hide();
										
										Ext.Msg.alert("Properties", selectedGridPropertyBlock.length+' Properties add to Unblock.<br>'+resp.properties+' Properties Unblocked');
										storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});
										if(typeof storeProperty !== "undefined")
											storeProperty.load({params:{start:0, limit:limitproperties}});
										/*if(storemyblockagent){
											storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
										}*/ 
									}                                
								});
							}
					});					
				}
			}),{
				xtype: 'tbspacer', 
				width: 60
			},new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text',
				iconAlign: 'left',
				width: 36,
				height: 36,
				scale: 'large',
				iconCls:'icon-help',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'FollowupContact'} }
					});
					win.show();
				}
			})	
		]
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var num = colIndex-3;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = 'H';
		else if(tyvalue == 1){ 
			classvalue = 'O';
		}else if(tyvalue == 2){ 
			classvalue = 'C';
		}else if(tyvalue == 3){ 
			classvalue = 'HF';
		}else if(tyvalue == 4){ 
			classvalue = 'TF';
		}else if(tyvalue == 5){ 
			classvalue = 'OTF';
		}else if(tyvalue == 6){ 
			classvalue = 'OF';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function emailRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-1;
		var type = 'typeemail'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function webRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-9;
		var type = 'typeurl'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function addressRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-12;
		var type = 'typeaddress'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(H)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}	
	var gridPropertyBlock = new Ext.grid.GridPanel({
		renderTo	:	'block-pt',
		cls			:	'grid_comparables',
		width		:	system_width_panel,
		height		:	system_height - ($("#block-pt").offset().top - grid_height),
		store		:	storePropertyBlock,
		stripeRows	:	true,
		sm			:	smPropertiesBlock, 
		columns		:	[
			smPropertiesBlock,
			//{header: 'Status', width: 100, sortable: true, tooltip: 'Property Status', dataIndex: 'status'/*, renderer: emailRender*/},
			//{header: 'Block', width: 40, sortable: true, tooltip: 'Property Blocked', dataIndex: 'ccoded', renderer: emailRender},
			{header: 'Property Address', width: 350, sortable: true, tooltip: 'Property Address,', dataIndex: 'addressfull'},
			{header: 'County', width: 120, sortable: true, tooltip: 'Property county,', dataIndex: 'county'},
			{header: 'Type', width: 100, sortable: true, tooltip: 'Property Type,', dataIndex: 'CCodeD'},
			{header: 'G. Area', width: 70, sortable: true, tooltip: 'Property Gross Area', dataIndex: 'lsqft', renderer: emailRender},
			{header: 'L. Area', width: 70, sortable: true, tooltip: 'Property Living Area', dataIndex: 'bheated', renderer: emailRender},
			{header: 'Beds', width: 50, sortable: true, tooltip: 'Property Bedthrooms', dataIndex: 'beds'/*, renderer: phoneRender*/},
			{header: 'Baths', width: 50, sortable: true, tooltip: 'Property Bathrooms', dataIndex: 'bath'/*, renderer: phoneRender*/},
			{header: 'Built', width: 70, sortable: true, tooltip: 'Property Year Built', dataIndex: 'yrbuilt', renderer: phoneRender}
		],
		listeners	:	{
			'sortchange':	function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			}
		},
		tbar	:	new Ext.PagingToolbar({
			id			:	'pagingPropertiesBlock',
            pageSize	:	limitpropertiesBlock,
            store		:	storePropertyBlock,
            displayInfo	:	true,
			displayMsg	:	'{2} Properties Blocked',
			emptyMsg	:	"No Properties Block.",
			items		:	[
				'Show:',
				new Ext.Button({
					tooltip		:	'Click to show 50 Properties per page.',
					text: '&nbsp;50&nbsp;',
					handler		:	function(){
						limitpropertiesBlock=50;
						Ext.getCmp('pagingPropertiesBlock').pageSize = limitpropertiesBlock;
						Ext.getCmp('pagingPropertiesBlock').doLoad(0);
					},
					enableToggle:	true,
					pressed		:	true,
					toggleGroup	:	'show_res_group'
				}),
				'-',
				new Ext.Button({
					tooltip		:	'Click to show 80 Properties per page.',
					text: '&nbsp;80&nbsp;',
					handler		:	function(){
						limitpropertiesBlock=80;
						Ext.getCmp('pagingPropertiesBlock').pageSize = limitpropertiesBlock;
						Ext.getCmp('pagingPropertiesBlock').doLoad(0);
					},
					enableToggle:	true,
					toggleGroup	:	'show_res_group'
				}),
				'-',
				new Ext.Button({
					tooltip		:	'Click to show 100 Properties per page.',
					text		:	100,
					handler		:	function(){
						limitpropertiesBlock=100;
						Ext.getCmp('pagingPropertiesBlock').pageSize = limitpropertiesBlock;
						Ext.getCmp('pagingPropertiesBlock').doLoad(0);
					},
					enableToggle:	true,
					toggleGroup	:	'show_res_group'
				})
			]
        })
	});
	
	storePropertyBlock.load({params:{start:0, limit:limitpropertiesBlock}});

	if(document.getElementById('tabs')){
		if(document.getElementById('principal-block-pt').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('principal-block-pt').offsetHeight+100);
			//tabs2.setHeight(tabs.getHeight());
			//tabs3.setHeight(tabs.getHeight());
			//viewport.setHeight(tabs.getHeight());
		}
	}
</script>