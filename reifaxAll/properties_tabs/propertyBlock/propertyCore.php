<?php  
	require('../../properties_conexion.php');
	require('../propertiesFunctions.php');
	conectar();
	
	if(isset($_POST['type'])){
		if($_POST['type']=='insert'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$agent 		= addslashes($_POST['agent']);
			$email 		= $_POST['email'];
			$tollfree 	= $_POST['tollfree'];
			$phone1 	= $_POST['phone1'];
			$typeph1 	= $_POST['typeph1'];
			$phone2 	= $_POST['phone2'];
			$typeph2 	= $_POST['typeph2'];
			$phone3 	= $_POST['phone3'];
			$typeph3 	= $_POST['typeph3'];
			$typeph4 	= $_POST['typeph4'];
			$typeph5 	= $_POST['typeph5'];
			$typeph6 	= $_POST['typeph6'];
			$fax 		= $_POST['fax'];
			$phone6 	= $_POST['phone6'];
			$urlsend	= $_POST['urlsend'];
			$typeemail1	= $_POST['typeemail1'];
			$typeemail2	= $_POST['typeemail2'];
			$email2		= $_POST['email2'];
			$urlsend2	= $_POST['urlsend2'];
			$typeurl1	= $_POST['typeurl1'];
			$typeurl2	= $_POST['typeurl2'];
			$company	= addslashes($_POST['company']);
			$address1	= $_POST['address1'];
			$address2	= $_POST['address2'];
			$typeaddress1	= $_POST['typeaddress1'];
			$typeaddress2	= $_POST['typeaddress2'];
			$agenttype = $_POST['agenttype'];
			if($agenttype=='Agent')
				$agenttype=1;
			$query	= 'SELECT agent FROM followagent WHERE userid='.$userid.' AND agent="'.$agent.'"';
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)==0){
				$query	= 'INSERT INTO followagent (userid,agent,email,tollfree,phone1,typeph1,phone2,typeph2,phone3,typeph3,fax,urlsend,
							typeph4,typeph5,typeph6,phone6, typeemail1, typeemail2, email2, typeurl1, urlsend2, typeurl2, address1, 
							typeaddress1, address2, typeaddress2, company, agenttype) 
				VALUES ('.$userid.',"'.$agent.'","'.$email.'","'.$tollfree.'",
				"'.$phone1.'",'.$typeph1.',"'.$phone2.'",'.$typeph2.',"'.$phone3.'",'.$typeph3.',"'.$fax.'","'.$urlsend.'",'.$typeph4.','.$typeph5.','.$typeph6.',"'.$phone6.'",'.$typeemail1.','.$typeemail2.',"'.$email2.'",'.$typeurl1.',"'.$urlsend2.'",'.$typeurl2.',"'.$address1.'",'.$typeaddress1.',"'.$address2.'",'.$typeaddress2.',"'.$company.'",'.$agenttype.')'; 
				mysql_query($query) or die($query.mysql_error());
				$idagent=mysql_insert_id();
				
				
				//Assignment agent to Emails.
				$query='SELECT fe.idmail FROM follow_emails fe 
				LEFT JOIN follow_emails_assigment fea ON (fe.idmail=fea.idmail AND fe.userid=fea.userid)
				WHERE fe.userid='.$userid.' 
				AND (fe.from_msg="'.$email.'" OR fe.from_msg="'.$email2.'")
				AND fea.agentid IS NULL';
				$result = mysql_query($query) or die($query.mysql_error());
				while($r=mysql_fetch_array($result)){
					$query='INSERT INTO follow_emails_assigment (idmail,userid,agentid)
					VALUES ('.$r['idmail'].','.$userid.','.$idagent.')';
					mysql_query($query) or die($query.mysql_error());
				}
				
				echo '{success: true, msg: "New Follow Agent.", agentid: '.$idagent.'}';
			}else
				echo '{success: true, msg: "Agent Exist."}';

		}elseif($_POST['type']=='update'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$agentid	= $_POST['agentid'];
			$agent 		= addslashes($_POST['agent']);
			$email 		= $_POST['email'];
			$tollfree 	= $_POST['tollfree'];
			$phone1 	= $_POST['phone1'];
			$typeph1 	= $_POST['typeph1'];
			$phone2 	= $_POST['phone2'];
			$typeph2 	= $_POST['typeph2'];
			$phone3 	= $_POST['phone3'];
			$typeph3 	= $_POST['typeph3'];
			$typeph4 	= $_POST['typeph4'];
			$typeph5 	= $_POST['typeph5'];
			$typeph6 	= $_POST['typeph6'];
			$fax 		= $_POST['fax'];
			$phone6 	= $_POST['phone6'];
			$urlsend	= $_POST['urlsend'];
			$typeemail1	= $_POST['typeemail1'];
			$typeemail2	= $_POST['typeemail2'];
			$email2	= $_POST['email2'];
			$urlsend2	= $_POST['urlsend2'];
			$typeurl1	= $_POST['typeurl1'];
			$typeurl2	= $_POST['typeurl2'];
			$company	= addslashes($_POST['company']);
			$address1	= $_POST['address1'];
			$address2	= $_POST['address2'];
			$typeaddress1	= $_POST['typeaddress1'];
			$typeaddress2	= $_POST['typeaddress2'];
			$agenttype = $_POST['agenttype'];
			
			$query="UPDATE followagent SET email='$email', tollfree='$tollfree', 
			phone1='$phone1', typeph1=$typeph1, phone2='$phone2', typeph2=$typeph2, phone3='$phone3', typeph3=$typeph3, fax='$fax',
			urlsend='$urlsend',  typeph4=$typeph4,  typeph5=$typeph5,  typeph6=$typeph6,  phone6='$phone6', email2='$email2',
			typeemail1=$typeemail1, typeemail2=$typeemail2, typeurl1=$typeurl1, urlsend2='$urlsend2', typeurl2=$typeurl2, company='$company',
			address1='$address1', address2='$address2', typeaddress1=$typeaddress1, typeaddress2=$typeaddress2, agenttype=$agenttype 
			WHERE userid=".$userid." AND agentid=$agentid";
			mysql_query($query) or die($query.mysql_error());
			
			//Assignment agent to Emails.
			$query='SELECT fe.idmail FROM follow_emails fe 
			LEFT JOIN follow_emails_assigment fea ON (fe.idmail=fea.idmail AND fe.userid=fea.userid)
			WHERE fe.userid='.$userid.' 
			AND (fe.from_msg="'.$email.'" OR fe.from_msg="'.$email2.'")
			AND fea.agentid IS NULL';
			$result = mysql_query($query) or die($query.mysql_error());
			while($r=mysql_fetch_array($result)){
				$query='INSERT INTO follow_emails_assigment (idmail,userid,agentid)
				VALUES ('.$r['idmail'].','.$userid.','.$agentid.')';
				mysql_query($query) or die($query.mysql_error());
			}

			echo '{success: true}';

		}elseif($_POST['type']=='delete'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$query='DELETE FROM properties
			WHERE userid='.$userid.' AND parcelid IN ('.$_POST['propertyIds'].')';
			mysql_query($query) or die("{success: false, msg : 'Error PT-Core-125'}");
			$msg = mysql_affected_rows() > 1 ? "Properties Delete" : "Property Delete";
			echo json_encode(Array(success => true, msg => $msg));

		}elseif($_POST['type']=='showEdit'){
			$userid =	isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$parcel =	$_POST['propertyIds'];
			$query	=	"SELECT address, unit, city, zip, county, state, xcode, status, latitude, longitude 
						FROM properties 
						WHERE parcelid='$parcel' AND userid='$userid'";
			$result	=	mysql_query($query) or die("{success: false, msg : 'Error PT-Core-132'}");
			$aux 	=	Array();$data 	=	Array();
			while($row	=	mysql_fetch_assoc($result)){
				$aux[]	=	$row;
			}
			foreach($aux as $akey => $avalue){
				foreach($avalue as $key => $value){
					if($value !== null and $value !== ""){
						if($key=="status"){
							if($value=="A"){
								$query	=	"SELECT COUNT(*) FROM mlsresidential WHERE parcelid='$parcel' ";
								mysql_query($query);
								if(mysql_affected_rows() > 0)
									$value="AA";
								else
									$value="BB";
							}
							$realStatus=$value;
						}
						$data['property']["edproperty*".$key]=$value;
					}
				}
			}
			$data['property']["edproperty*parcel"]	=	$parcel;
			
			/************************
			*	////PSUMMARY DATA////
			************************/
			$query	=	"SELECT campos FROM `searchtemplateadv` s WHERE s.`idGrid` in ('PRDEF')";
			$result	=	mysql_query($query);
			$result	=	mysql_fetch_assoc($result);
			$campos	=	$result['campos'];
			
			$query	=	"SELECT campos FROM `searchtemplateadv` s WHERE s.`idGrid` in ('PRALL')";
			$result	=	mysql_query($query);
			$result	=	mysql_fetch_assoc($result);
			$campos	.=	",".$result['campos'];
			
			$query	=	"SELECT Campos as campos
						FROM camptit  
						WHERE idtc IN ($campos) AND tabla ='psummary'
						ORDER BY  `Tsaorden`";
			$result	=	mysql_query($query);
			$c=0;$campos="";
			while($row	=	mysql_fetch_assoc($result)){
				if($c<>0)
					$campos	.= ",";
				$campos	.=	$row['campos'];
				$c++;
			}
			
			$query	=	"SELECT $campos FROM psummary WHERE parcelid=$parcel";
			$result	=	mysql_query($query);
			
			unset($aux);
			while($row	=	mysql_fetch_assoc($result))
				$aux[]	=	$row;
			
			foreach($aux as $akey => $avalue){
				foreach($avalue as $key => $value){
					if($value !== null and $value !== ""){
						if($key=='pool' or $key=='waterf' or $key=='ac')
							$data['psummary']["depsummary*".$key]=$value;
						else
							$data['psummary']["edpsummary*".$key]=$value;
					}
				}
			}
			
			/************************
			*	////MLSRESIDENTIAL OR RENTAL DATA////
			************************/
			$query	=	"SELECT campos FROM `searchtemplateadv` s WHERE s.`idGrid` in ('LISDEF')";
			$result	=	mysql_query($query);
			$result	=	mysql_fetch_assoc($result);
			$campos	=	$result['campos'];
						
			$query	=	"SELECT Campos as campos
						FROM camptit  
						WHERE idtc IN ($campos) AND tabla ='mlsresidential'
						ORDER BY  `Tsaorden`";
			$result	=	mysql_query($query);
			$c=0;$campos="";
			while($row	=	mysql_fetch_assoc($result)){
				if($c<>0)
					$campos	.= ",";
				$campos	.=	$row['campos'];
				$c++;
			}

			$table 	=	$realStatus == "BB" ? "rental" : "mlsresidential" ;
			$query	=	"SELECT $campos FROM $table WHERE parcelid=$parcel";
			$result	=	mysql_query($query);
			
			//If Found Records Charge rows
			if(mysql_affected_rows() > 0){
				unset($aux);
				while($row	=	mysql_fetch_assoc($result)){
					$aux[]	=	$row;
				}
				
				foreach($aux as $akey => $avalue){
					foreach($avalue as $key => $value){
						if($value !== null and $value !== ""){
							if($key=='pool' or $key=='waterf')
								$data['residential']["demlsresidential*".$key]=$value;
							else
								$data['residential']["edmlsresidential*".$key]=$value;
						}
					}
				}
			}
			
			echo json_encode(Array(success => true, data => $data));

		}elseif($_POST['type']=='assignment'){
			$userid	= $_POST['userid'];
			$pid	= $_POST['pid'];
			
			$query	= "SELECT a.parcelid,a.principal,ag.*,t.name as agentype
			FROM `realtytask`.`follow_assignment` a
			INNER JOIN followagent ag ON (a.agentid=ag.agentid)
			inner join followagent_type t on ag.agenttype=t.idtype 
			WHERE ag.userid=$userid AND a.parcelid='$pid' 
			ORDER BY a.principal DESC";
			
			//orders
			if(isset($_POST['sort'])) $query.=', '.$_POST['sort'].' '.$_POST['dir'];
			
			$result=mysql_query($query) or die($query.mysql_error());
			$vFilas = array();
			while($r=mysql_fetch_object($result))
				$vFilas[]=$r;
			
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';

		}elseif($_POST['type']=='assignment-add'){
			$userid	= $_POST['userid'];
			$pid	= $_POST['pid'];
			$agentid= $_POST['agentid'];
			
			$query="SELECT * FROM `realtytask`.`follow_assignment` WHERE parcelid='$pid' AND userid=$userid AND agentid=$agentid";
			$result=mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)==0){
				$query	= "INSERT INTO `realtytask`.`follow_assignment` (parcelid,userid,agentid,principal)
				VALUES ('$pid',$userid,$agentid,0)";
				
				mysql_query($query) or die($query.mysql_error());
				
				$query="SELECT * FROM `realtytask`.`follow_assignment` a
						inner join followagent f on a.agentid=f.agentid WHERE parcelid='$pid' AND a.userid=$userid";
				$result=mysql_query($query) or die($query.mysql_error());
				
				if(mysql_num_rows($result)==1){
					$r=mysql_fetch_array($result);
					$query	= "UPDATE followup SET agent='".$r['agent']."'
					WHERE userid=$userid AND parcelid='$pid'";
					mysql_query($query) or die($query.mysql_error());
					
					$query	= "UPDATE follow_assignment 
					SET principal=1
					WHERE userid=$userid AND parcelid='$pid' AND agentid=$agentid";
					mysql_query($query) or die($query.mysql_error());
				}
				
				echo '{success: true, existe: 0}';
			}else{
				echo '{success: true, existe: 1}';
			}
			
			
		}elseif($_POST['type']=='assignment-del'){
			$userid	= $_POST['userid'];
			$pid	= $_POST['pid'];
			$agentid= $_POST['agentid'];
			
			$query="DELETE FROM `realtytask`.`follow_assignment` WHERE parcelid='$pid' AND userid=$userid AND agentid=$agentid";
			mysql_query($query) or die($query.mysql_error());
			
			$query="SELECT * FROM `realtytask`.`follow_assignment` a
						inner join followagent f on a.agentid=f.agentid WHERE parcelid='$pid' AND a.userid=$userid";
			$result=mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)==0){
				$r=mysql_fetch_array($result);
				$query	= "UPDATE followup SET agent=''
				WHERE userid=$userid AND parcelid='$pid'";
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo '{success: true}';
		}elseif($_POST['type']=='assignment-principal'){
			$agentname 	= addslashes($_POST['agentname']);
			$userid		= $_POST['userid'];
			$pid		= $_POST['pid'];
			
			$query	= "UPDATE followup SET agent='$agentname'
			WHERE userid=$userid AND parcelid='$pid'";
			mysql_query($query) or die($query.mysql_error());
			
			$query	= "UPDATE follow_assignment 
			SET principal=0
			WHERE userid=$userid AND parcelid='$pid'";
			mysql_query($query) or die($query.mysql_error());
			
			$query	= "UPDATE follow_assignment a, followagent ag
			SET a.principal=1
			WHERE a.userid=$userid AND a.parcelid='$pid' 
			AND a.agentid=ag.agentid AND ag.agent='$agentname'";
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='getfollowagent'){
			$userid		= $_POST['userid'];
			$pid		= $_POST['pid'];
			
			$query="SELECT a.*, t.name as agentype
					FROM followup f inner join followagent a on (f.userid=a.userid and f.agent=a.agent)
					inner join followagent_type t on a.agenttype=t.idtype 
					WHERE f.userid =$userid AND parcelid='$pid'";
			$result=mysql_query($query) or die($query.mysql_error());
			
			if ($result) {
				$row  = mysql_fetch_array($result);
				$total=mysql_num_rows($result);
				$resp = array('success'=>'true','total'=>$total,'data'=>$row);	
			}else
				$resp = array('success'=>'true', 'total'=> 0, 'mensaje'=>mysql_error());
			
			echo json_encode($resp);
		}elseif($_POST['type']=='agenttype'){
			$userid		= $_POST['userid'];
			$pid		= $_POST['pid'];
			
			$query="SELECT * FROM followagent_type f;";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			
			if(isset($_POST['selectall'])){
				$row['idtype']='ALL';
				$row['name']='ALL';
				$data [] = $row;
			}
			//$data [] = $row;
			while ($row=mysql_fetch_array($result)){
				$data [] = $row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='contracts'){
			$userid		= $_POST['userid'];
			$pid		= $_POST['pid'];
			
			$query="SELECT * FROM contracts_custom f where userid=$userid";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			
			while ($row=mysql_fetch_array($result)){
				$data [] = $row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='emailtemplates'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$query="SELECT id, name, `default` FROM templates WHERE userid = $userid and template_type=1";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			
			$row['id']=0;
			$row['name']='Standard';
			$row['default']=0;
			$data [] = $row;
			
			while ($row=mysql_fetch_array($result)){
				$data [] = $row;
			}
			
			$row['id']=-1;
			$row['name']='New Mail';
			$row['default']=0;
			$data [] = $row;
			
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
			
		}elseif($_POST['type']=='docstemplates'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$template_type=$_POST['template_type'];
			$query="SELECT id, name, `default` FROM templates WHERE userid = $userid and template_type=$template_type";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			if($template_type==3 || $template_type==4){
				$row['id']=0;
				$row['name']='New';
				$row['default']=0;
				$data [] = $row;
			}
			while ($row=mysql_fetch_array($result)){
				$data [] = $row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
			
		}elseif($_POST['type']=='loadtemplates'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$template_type=$_POST['template_type'];
			$query="SELECT id, name, `default` FROM templates WHERE userid = $userid and template_type=$template_type";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			while ($row=mysql_fetch_array($result)){
				$data [] = $row;
			}
			$row['id']=0;
			$row['name']='New Template';
			$row['default']=0;
			$data [] = $row;
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
			
		}elseif($_POST['type']=='template-variables'){
			$arrayNames=Array();
			
			$query     = "SELECT campos, titulos, `desc` FROM camptit WHERE variable=1"; 
			$rscr      = mysql_query($query) or die($query.mysql_error());
			$i=0;
			$checked='';
			while ($rowcr = mysql_fetch_array($rscr)) {
				$valor=	$rowcr['desc'];
				if($valor==''){
					$valor=	$rowcr['titulos'];
				}
				$arrayNames[$valor]=$valor;
			}
			
			asort($arrayNames);
			$data = array();
			$row['id']='';
			$row['name']='Select';
			$data [] = $row;
			foreach($arrayNames as $id => $name){
				//$combo     .= "['{%{$id}%}','{$name}'],";
				$row=array();
				$row['id']='{%'.$id.'%}';
				$row['name']=$name;
				$data[]=$row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='unblock'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$query = "UPDATE properties SET
						block='' WHERE parcelid in ($_POST[propertyIds]) AND userid='$userid'";
			mysql_query($query) or die("{success: false, msg : 'Error PT-Core-134s'}");
			$properties	=	mysql_affected_rows();
			echo json_encode(Array(success => true, "properties" => $properties));
		}elseif($_POST['type']=='ContactEmailsList'){
			$userid	= $_POST['userid'];
			
			$query="select agent,email
			FROM(
				(select concat(agent,' [',trim(email),']') as agent,trim(email) as email,userid 
				FROM followagent
				WHERE userid=$userid AND email is not null and length(email)>2)
			UNION
				(select concat(agent,' [',trim(email2),']') as agent,trim(email2) as email,userid 
				FROM followagent
				WHERE userid=$userid AND email2 is not null and length(email2)>2)
			UNION
				(select concat(agent,' [',trim(email3),']') as agent,trim(email3) as email,userid 
				FROM followagent
				WHERE userid=$userid AND email3 is not null and length(email3)>2)
			UNION
				(select concat(agent,' [',trim(email4),']') as agent,trim(email4) as email,userid 
				FROM followagent
				WHERE userid=$userid AND email4 is not null and length(email4)>2)
			UNION
				(select concat(agent,' [',trim(email5),']') as agent,trim(email5) as email,userid 
				FROM followagent
				WHERE userid=$userid AND email5 is not null and length(email5)>2)
			ORDER BY agent) as contact";
			
			//filters
			if(isset($_POST['query']) && strlen($_POST['query'])>0)
				$query.=" WHERE agent LIKE '".$_POST['query']."%' 
				OR agent LIKE '% ".$_POST['query']."%' 
				OR agent LIKE '%[".$_POST['query']."%'";

				
			$result=mysql_query($query) or die($query.mysql_error());
			$vFilas = array();
			while($r=mysql_fetch_object($result))
				$vFilas[]=$r;
				
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
		}
	}else{
		$userid	= $_COOKIE['datos_usr']['USERID'];
		
		//$status 	=	isset($_POST['status'])	?	$_POST['status']	:	'';
		$type	 	=	isset($_POST['types'])	?	$_POST['types']		:	'';
		$address 	=	isset($_POST['address'])?	$_POST['address']	:	'';
		$county		=	isset($_POST['county'])	?	$_POST['county']	:	'';
		$beds		=	isset($_POST['beds'])	?	$_POST['beds']		:	'';
		$baths		=	isset($_POST['baths'])	?	$_POST['baths']		:	'';
		$yrbuilt	=	isset($_POST['yrbuilt'])?	$_POST['yrbuilt']	:	'';
		
		$query	= "SELECT a.*,b.bath,b.beds,b.lsqft,b.bheated,b.yrbuilt FROM properties a 
					INNER JOIN psummary b ON a.parcelid=b.parcelid
					where a.block='1' AND a.userid=".$userid;
		
		//filters
		if(!empty($address))
			$query	.=	" AND addressfull LIKE '%".trim($address)."%'";

		//if(!empty($status))
		//	$query.=" AND a.status LIKE '%".trim($status)."%'";
		
		if(!empty($type))
			$query.=" AND a.xcode LIKE '%".trim($type)."%'";

		if(!empty($county))
			$query.=" AND LOWER(county) LIKE '%".strtolower(trim($county))."%'";

		if(!empty($beds))
			$query	.=	" AND b.beds LIKE '%".trim($beds)."%'";
		
		if(!empty($baths))
			$query	.=	" AND b.bath LIKE '%".trim($baths)."%'";
		
		if(!empty($yrbuilt))
			$query	.=	" AND b.yrbuilt LIKE '%".trim($yrbuilt)."%'";
			
		//orders
		if(isset($_POST['sort'])) 
			$query	.=	" ORDER BY $_POST[sort] $_POST[dir]";
		//else $query.=' ORDER BY agent';
		
		//echo $query;
		$result=mysql_query($query) or die($query.mysql_error());
		$i=1;
		$vFilas = array();
		while($r=mysql_fetch_object($result)){
			$r->status	=	statusReal($r->status,$r->parcelid);
			$vFilas[]	=	$r;
		}

		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}	
	
?>