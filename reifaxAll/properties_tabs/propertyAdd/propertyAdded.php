<?php
	require('../../properties_conexion.php');
	require('../propertiesFunctions.php');
	include('../propertyImport/class.zillow.php');
	include('../propertyImport/class.realtor.php');
	conectar();
	
	
	$Realtor	=	new	Realtor();
	$Zillow		=	new Zillow();
	
	$c=0;
	do{
		$jsonResult = $Zillow->getPrincipalResult(
			$_POST['adproperty*address']." ".
			$_POST['adproperty*unit']." ".
			$_POST['adproperty*city']." ".
			$_POST['adproperty*state']." ".
			$_POST['adproperty*zip']
		); //For Sale
		$result = json_decode($jsonResult);
		$c++;
	}while($result->url == false and $c<=5);

	///In Case to Search in Trulia
	/*if(!$result->url){
		require("../propertyImport/class.trulia.php");
		
		$Trulia	=	new Trulia();
		
		$jsonResultTrulia	=	$Trulia->getPrincipalResult($_POST['address']." ".$_POST['select-states']);
		
		$jsonResultTrulia	=	json_decode($jsonResultTrulia);	//Convert json String toObject Json
			
		if(!$jsonResultTrulia->url){
			echo json_encode(Array('url' => false, 'option' => 'T'));
			die();
		}else{
			$c=0;
			do{
				$jsonResultZillow	=	$Zillow->getPrincipalResult($Trulia->addressFull);
				$jsonResultZillow	=	json_decode($jsonResultZillow);	//Convert json String toObject Json
				$c++;
			}while($jsonResultZillow->url === false and $c<=5);
			
			if(!$jsonResultZillow->url or empty($jsonResultZillow->url)){
				$jsonResultTrulia	=	$Trulia->getDetailResult($Trulia->html);
				$jsonResultTrulia	=	json_decode($jsonResultTrulia);
				$jsonResultTrulia->option	=	"T";	//Add This Option
				
				echo json_encode($jsonResultTrulia);
				die();
			}else
				$jsonResult	=	json_encode($jsonResultZillow);
		}
	}*/
	
	if($result->url	!= false){
		
		/************************
		*	Conver Object to Array
		************************/
		$postParameter = Array();
		if(count($result->aditional)>0){
			foreach($result->aditional as $index => $value){
				$postParameter[$index]	=	$value;
			}
		}
		/******END******/

		$Zillow->aditional	=	$postParameter;	//Send Additional Parameters

		$urlDetail	=	$Zillow->getUrlDetail(base64_decode($result->url));

		$details	=	json_decode($Zillow->getDetailResult($urlDetail));
		$_POST['adproperty*latitude']	=	empty($_POST['adproperty*latitude'])	?	$details->data->property->{"txproperty*latitude"}	:	$_POST['adproperty*latitude'];
		$_POST['adproperty*longitude']	=	$details->data->property->{"txproperty*longitude"};
		$_POST['hdproperty*status']		=	$details->data->property->{"cbproperty*status"};
		$a	=	empty($_POST['adproperty*address'])	?	""	:	$_POST['adproperty*address'];
		$b	=	empty($_POST['adproperty*city'])	?	""	:	", ".$_POST['adproperty*city'];
		$c	=	", ".$_POST['hdproperty*state'];
		$d	=	empty($_POST['adproperty*zip'])		?	""	:	$_POST['adproperty*zip'];
		$_POST['adproperty*addressfull']=	"$a$b$c$d";
	}
	/*************************************************************************************************/
	$Array = Array();
	//print_r($_POST);
	$userid	=	$_COOKIE['datos_usr']['USERID'];
	foreach($_POST as $key => $value){
		$aux = explode("*",$key);
		$Array[substr($aux[0],2)][$aux[1]] = $value;
	}
	if(isset($_POST['unicID'])){
		$temp			=	explode("|",$_POST['unicID']);
		$parcelId		=	$temp[0];
		$statusOriginal	=	$temp[1];
	}
		
	//print_r($Array);
	foreach($Array as $key => $record){
		switch($key){
			case	"property":
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					if($camp=="status"){
						$statusOriginal = $value;
						///Change the Values AA and BB for A
						if($value == "AA" or $value == "BB")
							$value="A";
					}
					if($camp=="xcode")
						$ccodedOriginal = ccoded($value);
					if($cP<>0){
						$camps	.=	",";	$values	.=	",";
					}
					$camps	.=	"`$camp`";
					$values	.=	"'$value'";
					$cP++;
				}
				$query	=	"INSERT INTO properties (`userid`,`ccoded`,`regdate`,$camps) VALUES ('$userid','$ccodedOriginal',NOW(),$values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error PT-1 - $query - ".mysql_error())));
				$parcelId = mysql_insert_id();
				
				$query	=	"";$c=0;
				foreach($details->data->aditionals as $index => $value){
					if(!empty($value)){
						$separador	=	$c>0	?	","	:	"";
						switch($index){
							case "countyUrl":	$index	=	"county";	break;
							case "urlDetail":	$index	=	"zillow";	break;
							case "urlMore"	:	$index	=	"realtor";	break;
							case "urlFree"	:	$index	=	"trulia";	break;
						}
						$query	.=	$separador."('$parcelId','$index','$value',NOW())";
						$c++;
					}
				}
				
				if(!empty($query))
					mysql_query("INSERT INTO propertyLinks VALUES ".$query) or die(json_encode(Array("success" => false, "msg" => "Error PT-51 - $query - ".mysql_error())));
					
				break;
			case	"psummary":
				$campsRealInt	=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'real','int'");
				$campsReal		=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'real'");
				$campsDate		=	obtainCampsRealInt(Array('PRDEF','PRALL'),'psummary',"'date'");
				//It's New
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					$value	=	trim($value);
					if(!empty($value)){
						if($cP<>0){
							$camps	.=	",";	$values	.=	",";
						}
						if(in_array($camp,$campsDate)){	//Update date camps to Ymd
							$camps	.=	"`$camp`";
							$values	.=	"'".date("Ymd",strtotime($value))."'";
						}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
								$camps	.=	"`$camp`";
								$values	.=	"'".str_replace(",","",$value)."'";
						}else{
							$camps	.=	"`$camp`";
							$values	.=	"'$value'";
						}
						$cP++;
					}
				}
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO psummary (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-101 $query - ".mysql_error())));
				
				//Process to Sales History
				for($i=0;$i<$_POST['salesTotalED'];$i++){
					$cP=0;$camps="";$values="";
					foreach($Array['sales'] as $camp => $value){
						if(ereg_replace("[A-Za-z]", "", $camp) == $i){
							if(ereg_replace("[0-9]", "", $camp) == "date")	//Update date camps to Ymd
								$value	=	formatDateImport($value);
							elseif(ereg_replace("[0-9]", "", $camp) == "price" OR ereg_replace("[0-9]", "", $camp) == "priceSqft")	//Update real's camps erase , by nothing
								$value	=	str_replace(",","",$value);
								
							if($cP<>0){
								$camps	.=	",";	$values	.=	",";}

							$camps	.=	"`".ereg_replace("[0-9]", "", $camp)."`";
							$values	.=	"'$value'";
							$cP++;
						}
					}
					$query	=	"INSERT INTO sales (`parcelid`,`recdate`,$camps) VALUES ('$parcelId','NOW()',$values)";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-3 $query")));
				}
				//Process to MLSRESIDENTIAL OR RENTAL
				if(!empty($_POST['adpsummary*folio'])){
					$camps	=	",`folio`";
					$values	=	",'".$_POST['adpsummary*folio']."'";
				}else{
					$camps	=	"";
					$values	=	"";
				}
				if($statusOriginal	!=	"BB"){	//It's MLSRESIDENTIAL
					$query	=	"INSERT INTO mlsresidential (`parcelid`$camps) VALUES ('$parcelId'$values)";
				}else{
					$query	=	"INSERT INTO rental (`parcelid`$camps) VALUES ('$parcelId'$values)";
				}
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-7")));
				break;
			/*case	"mlsresidential":
				//If is AA, Are mlsresidential else BB it's rental'
				$campsRealInt	=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'real','int'");
				$campsReal		=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'real'");
				$campsDate		=	obtainCampsRealInt(Array('LISDEF'),'mlsresidential',"'date'");
				if($statusOriginal	!=	"BB"){	//It's MLSRESIDENTIAL
					//It's New
					$cP=0;$camps="";$values="";
					foreach($record as $camp => $value){
						$value	=	trim($value);
						if(!empty($value)){
							if($cP<>0){
								$camps	.=	",";	$values	.=	",";
							}
							if(in_array($camp,$campsDate)){	//Update date camps to Ymd
								$camps	.=	"`$camp`";
								$values	.=	"'".date("Ymd",strtotime($value))."'";
							}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
								$camps	.=	"`$camp`";
								$values	.=	"'".str_replace(",","",$value)."'";
							}else{
								$camps	.=	"`$camp`";
								$values	.=	"'$value'";
							}
							$cP++;
						}
					}
					$query	=	"INSERT INTO mlsresidential (`parcelid`,$camps) VALUES ('$parcelId',$values)";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-7")));
				}else{	//It's RENTAL
					//It's New
					$cP=0;$camps="";$values="";
					foreach($record as $camp => $value){
						if($camp == "remark")	//change name because in Mlsresidential Table its Remark bat en Rental Table its Remark1
							$camp =	"remark1";
							
						$value	=	trim($value);
						if(!empty($value)){
							if($cP<>0){
								$camps	.=	",";	$values	.=	",";
							}
							if(in_array($camp,$campsDate)){	//Update date camps to Ymd
								$camps	.=	"`$camp`";
								$values	.=	"'".date("Ymd",strtotime($value))."'";
							}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
								$camps	.=	"`$camp`";
								$values	.=	"'".str_replace(",","",$value)."'";
							}else{
								$camps	.=	"`$camp`";
								$values	.=	"'$value'";
							}
							$cP++;
						}
					}
					$query	=	"INSERT INTO rental (`parcelid`,$camps) VALUES ('$parcelId',$values)";
					mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-9")));
				}
				break;*/
			case	"pendes":
				$campsRealInt	=	obtainCampsRealInt(Array('FCALL'),'pendes',"'real','int'");
				$campsReal		=	obtainCampsRealInt(Array('FCALL'),'pendes',"'real'");
				$campsDate		=	obtainCampsRealInt(Array('FCALL'),'pendes',"'date'");
				//It's New
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					$value	=	trim($value);
					if(!empty($value)){
						if($cP<>0){
							$camps	.=	",";	$values	.=	",";
						}
						if(in_array($camp,$campsDate)){	//Update date camps to Ymd
							$camps	.=	"`$camp`";
							$values	.=	"'".date("Ymd",strtotime($value))."'";
						}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
								$camps	.=	"`$camp`";
								$values	.=	"'".str_replace(",","",$value)."'";
						}else{
							$camps	.=	"`$camp`";
							$values	.=	"'$value'";
						}
						$cP++;
					}
				}
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO pendes (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-11 $query - ".mysql_error())));
				break;
			case	"mortgage":
				$campsRealInt	=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'real','int'");
				$campsReal		=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'real'");
				$campsDate		=	obtainCampsRealInt(Array('MORALL'),'mortgage',"'date'");
				//It's New
				$cP=0;$camps="";$values="";
				foreach($record as $camp => $value){
					$value	=	trim($value);
					if(!empty($value)){
						if($cP<>0){
							$camps	.=	",";	$values	.=	",";
						}
						if(in_array($camp,$campsDate)){	//Update date camps to Ymd
							$camps	.=	"`$camp`";
							$values	.=	"'".date("Ymd",strtotime($value))."'";
						}elseif(in_array($camp,$campsReal)){	//Update real's camps erase , by nothing
								$camps	.=	"`$camp`";
								$values	.=	"'".str_replace(",","",$value)."'";
						}else{
							$camps	.=	"`$camp`";
							$values	.=	"'$value'";
						}
						$cP++;
					}
				}
				$separador	=	empty($camps)	?	""	:	",";
				
				$query	=	"INSERT INTO mortgage (`parcelid` $separador $camps) VALUES ('$parcelId' $separador $values)";
				mysql_query($query) or die(json_encode(Array("success" => false, "msg" => "Error ET-13 $query - ".mysql_error())));
				break;
		}
	}
	//echo json_encode(Array("success" => true, "msg" => "Property Edited Successfully", "id" => $parcelId."|".$statusOriginal, "optional" => $query));
	echo json_encode(Array("success" => true, "msg" => "Property Edited Successfully", "id" => $parcelId."|".$statusOriginal));
	//print_r($Array);
?>