<?php 	
	include('../../properties_conexion.php');
	if(@session_start() == false){session_destroy();session_start();}
	conectar();
	//print_r($_SESSION);
	$userid=$_COOKIE['datos_usr']['USERID'];

	$state_search = $_POST['state_search'];
	$county_search = $_POST['county_search'];

?>
<div align="left" style="height:100%">
	<div id="body_central3121" style="height:100%">
		<!--<div id="addButtonsPT" style="display:none;position: absolute; right: 30px; z-index: 2; top: 40px;">-->
		<!--<div id="addButtonsPT" style="display:none; position: absolute; left: 180px; z-index: 2; top: 145px;">
			<table cellspacing="20">
				<tr>
					<td id="bt-add-list"></td>
					<td id="bt-add-cancel-list"></td>
				</tr>
			</table>
		</div>-->
		<div id="advAdd" style="padding-top:2px;"></div>
		<div id="panelsAdd"></div>
	</div>
</div>
<script language="javascript">
	
	var reloadMapEditPt = 0;
	Ext.ns("imagesEdit");
	Ext.ns("mapsLatLonEdit");

	function selectBetween(combo,record,index)
	{
		var arfield=combo.getId().replace("cb","tx");
		arfield=arfield.split('*');
		var txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3];
		
		if(record.get('id')=='Between')
		{
			if(Ext.getCmp(txfield))Ext.getCmp(txfield).setValue('');
			if(Ext.getCmp(txfield) && !Ext.getCmp(txfield).isVisible())
			{
				Ext.getCmp(txfield).setVisible(true);
				arrinpbetween.push(txfield);				
			}
		}
		else
		{
			if(Ext.getCmp(txfield) && Ext.getCmp(txfield).isVisible())
				Ext.getCmp(txfield).setVisible(false);
		}
	}
	
	var dataBoolean	=	new Ext.data.SimpleStore({
		fields: ['id','desc'],
		data  : [['Y','Yes'],['N','No']]
	});
	
	function removeSaleEdit(id){
		Ext.getCmp("tableRowSalesEdit"+id).hide();
		Ext.getCmp("adsales*price"+id).setValue(0);
		Ext.getCmp("adsales*priceSqft"+id).setValue(0);
		Ext.getCmp("adsales*date"+id).setValue("");
		Ext.getCmp("adsales*book"+id).setValue("");
		Ext.getCmp("adsales*page"+id).setValue("");
	}
	
	var countSalesHistoryPTED=0;
	function addSaleHistoryPTED(){
		var atte = {
			id			:	'tableRowSalesEdit'+countSalesHistoryPTED,
			layout		:	'table',
			defaults	:	{	bodyStyle:'padding:5px'},
			layoutConfig:	{	columns: 6},
			items		:	[
				{	
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'datefield',
						id			:	'adsales*date'+countSalesHistoryPTED,
						fieldLabel	:	'Date',
						name		:	'adsales*date'+countSalesHistoryPTED,
						format		:	'm/d/Y',
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Date'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'adsales*price'+countSalesHistoryPTED,
						fieldLabel	:	'Price',
						name		:	'adsales*price'+countSalesHistoryPTED,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Price'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	80,
					items		:	[{			
						xtype		:	'textfield',
						maskRe		:	/[0-9\.,]/,
						id			:	'adsales*priceSqft'+countSalesHistoryPTED,
						fieldLabel	:	'Price Sqft',
						name		:	'adsales*priceSqft'+countSalesHistoryPTED,
						width		:	100,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale $/Sqft'});
							},
							change: function(object, newValue, oldValue){
								newValue = Ext.util.Format.usMoney(newValue);
								if(newValue == '$NaN.00') object.setValue(oldValue.replace('$','')); else object.setValue(newValue.replace('$',''));
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'adsales*book'+countSalesHistoryPTED,
						fieldLabel	:	'Book',
						name		:	'adsales*book'+countSalesHistoryPTED,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Book'});
							}
						}
					}]
				},{
					layout		:	'form',
					labelWidth	:	50,
					items		:	[{			
						xtype		:	'textfield',
						id			:	'adsales*page'+countSalesHistoryPTED,
						fieldLabel	:	'Page',
						name		:	'adsales*page'+countSalesHistoryPTED,
						width		:	50,
						listeners	:	{
							render	:	function(c) {
								Ext.QuickTips.register({target: c,text: 'Sale Page'});
							}
						}
					},{
						xtype	:	'hidden',
						id		:	'adsales*idsales'+countSalesHistoryPTED,
						name	:	'adsales*idsales'+countSalesHistoryPTED
					}]
				},{
					xtype	:	'button',
					id		:	'adsales*button'+countSalesHistoryPTED,
					cls		:	'x-btn-text',
					idSales	:	countSalesHistoryPTED,
					tooltip	:	'Delete this row',
					text	:	'<i class="icon-minus"></i>',
					listeners	:	{
						click	:	function(button){
							removeSaleEdit(button.idSales);
						}
					}
				}
			]
		}
		countSalesHistoryPTED=countSalesHistoryPTED+1;
		return atte;
	}
	
	loading_win.show();
	var PR_ACT=0;	var LS_ACT=0;	var FC_ACT=0;	var MG_ACT=0;
	Ext.Ajax.request( 
	{  
		waitMsg	:	'Processing...',
		url		:	'properties_tabs/propertyImport/propertyImportBuilt.php', 
		method	:	'POST',
		timeout :	600000,
		params	:	{
			text		:	"ad",
			combo		:	"da",
			composite	:	"aTcomposite"
		},
		failure	:	function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success	:	function(response,options){
			var aRes=response.responseText.split("^");
			
			var publicrecordEdit=Ext.decode(aRes[0]);
			var listingEdit=Ext.decode(aRes[1]);
			var foreclosureEdit=Ext.decode(aRes[2]);
			var mortgageEdit=Ext.decode(aRes[3]);
			var publicrecordmoreEdit=Ext.decode(aRes[4]);
			var listingmoreEdit=Ext.decode(aRes[5]);
			//var byowner=Ext.decode(aRes[6]);
			//var byownermore=Ext.decode(aRes[7]);
			
			var displayImageAdd = new Ext.BoxComponent({ id:'displayImageMapAdd', style: 'position:relative; height:100%;'});
			
			mapsLatLonEdit.idRender	=	"displayImageMapAdd";
			
			formulAddPt = new Ext.FormPanel({
				url			:	'properties_tabs/propertyAdd/propertyAdded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				renderTo	:	'advAdd',
				id			:	'formulAddPt',
				name		:	'formulAddPt',
				items		:	[{
					layout		:	'table',
					bodyStyle	:	"padding:10px",
					layoutConfig:	{
		                columns	:	3
		            },
					items		:	[{
						xtype	:	'fieldset',
						title	:	'Property',
						width	:	600,
						height	:	220,
						layout	:	'column',
						items	:	[{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Address',
								allowBlank	:	false,
								id			:	'adproperty*address',
								name		:	'adproperty*address'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'City',
								allowBlank	:	false,
								id			:	'adproperty*city',
								name		:	'adproperty*city'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'County',
								name		:	'adproperty*county',
								id			:	'adproperty*county'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Latitude',
								id			:	'adproperty*latitude',
								name		:	'adproperty*latitude'
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'adproperty*xcode',
								id				:	'adproperty*xcode',
								hiddenName		:	'hdproperty*xcode',
								store			:	propertiesType,				
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								typeAhead		:	true,
								fieldLabel		:	'Type',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	125
								
							}/*,{
								xtype		:	'textfield',
								fieldLabel	:	'Subd Name',
								id			:	'adproperty*sbdname',
								name		:	'adproperty*sbdname'
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Year Built',
								id			:	'adproperty*yrbuilt',
								name		:	'adproperty*yrbuilt',
								width		:	50
							},{
								xtype		:	'hidden',
								id			:	'adproperty*addressfull',
								name		:	'adproperty*addressfull'
							},{
								xtype		:	'hidden',
								id			:	'adproperty*parcel',
								name		:	'adproperty*parcel'
							}*/]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype		:	'textfield',
								fieldLabel	:	'Unit',
								id			:	'adproperty*unit',
								name		:	'adproperty*unit',
								width		:	50
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Zip',
								allowBlank	:	false,
								id			:	'adproperty*zip',
								name		:	'adproperty*zip',
								width		:	50
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'adproperty*state',
								id				:	'adproperty*state',
								hiddenName		:	'hdproperty*state',
								store			:	new Ext.data.JsonStore ({
									fields	:	['initial', 'name'],
									url		:	'../../resources/Json/states.json',
									autoLoad:	true
								}),				
								editable		:	false,
								displayField	:	'name',
								valueField		:	'initial',
								typeAhead		:	true,
								fieldLabel		:	'State',
								allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155,
								listeners		:	{

								}
							},{
								xtype			:	'combo',
								labelWidth		:	120,
								name			:	'adproperty*status',
								id				:	'adproperty*status',
								hiddenName		:	'hdproperty*status',
								store			:	propertiesStatus,
								editable		:	false,
								displayField	:	'desc',
								valueField		:	'id',
								value			:	'CC',
								hidden			:	true,
								hideLabel		:	true,
								typeAhead		:	true,
								fieldLabel		:	'Status',
								//allowBlank		:	false,
								mode			:	'local',
								triggerAction	:	'all',
								emptyText		:	'Select ...',
								selectOnFocus	:	true,
								autoSelect		:	true,
								width			:	155
							},{
								xtype		:	'textfield',
								fieldLabel	:	'Longitude',
								id			:	'adproperty*longitude',
								name		:	'adproperty*longitude'
							},{
								xtype		:	'spacer',
								height		:	20
							}]
						}],
						buttonAlign:'center',
						buttons:[{
							xtype		:	'button',
							tooltip		:	'Add Property',
							//icon		:	'/img/add-property.png',
							icon		:	'/img/toolbar/NEW/properties_add_focus.png',
							cls			:	'x-btn-text',
							//iconCls		:	'icon-delete',
							text		:	'Add',
							height		:	40,
							scale		:	'large',
							width		:	90,
							listeners	:	{
								click	:	function(){
									/*********************************
									*	Save Changes for Property
									*********************************/
									formulAddPt.getForm().submit({
										success: function(formulAddPt, action){
											//console.debug(action);
											RtWinSaving.show();
											if(action.result.success){
												/*********************************
												*	Save Changes for Psummary
												*********************************/
												formulPsummaryAdd.getForm().submit({
													params:{
															unicID:action.result.id
														},
													success: function(formulPsummaryAdd, action){
														//console.debug(action);
														if(action.result.success){						
															/*********************************
															*	Save Changes for Residential
															*********************************/
															formulResidentialAdd.getForm().submit({
																params:{
																	unicID:action.result.id
																},
																success: function(formulResidentialAdd, action){
																	//console.debug(action);
																	if(action.result.success){						
																		/*********************************
																		*	Save Changes for Foreclosed
																		*********************************/
																		formulForeclosedAdd.getForm().submit({
																			params:{
																				unicID:action.result.id
																			},
																			success: function(formulForeclosedAdd, action){
																				//console.debug(action);
																				if(action.result.success){						
																					/*********************************
																					*	Save Changes for Mortgage
																					*********************************/
																					formulMortgageAdd.getForm().submit({
																						params:{
																							unicID:action.result.id
																						},
																						success: function(formulMortgageAdd, action){
																							RtWinSaving.hide();
																							//console.debug(action);
																							if(action.result.success){
																								/*$.ajax({
																									type	:	"POST",
																									async	:	false,
																									url		:	"/properties_tabs/propertyAdd/propertyAdded.php",
																									data	:	{
																										unicID		:	action.result.id,
																										tximages	:	CreateArrayImagesEdit(),
																										imageDefault:	imageDefaultEdit()
																									},
																									dataType:	'json',
																									success:function(action){
																										if(action.success){*/
																											storeProperty.load({params:{start:0, limit:limitproperties}});
																											//Ext.getCmp('bt-cancel-pt').fireEvent('click',Ext.getCmp('bt-cancel-pt'));
																											var tabsAux = Ext.getCmp('tabs');
																											var tabAux = tabsAux.getItem('id-tproadd');
																											tabsAux.remove(tabAux);
																										/*}
																									}
																								});*/
																							}else{
																								RtWinSaving.hide();
																								alert(action.result.msg);
																							}
																						}
																					});
																				}
																			}
																		});
																	}
																}
															});
														}
													}
												});
											}
					                    }
									});
								}
							}
						},{
							xtype		:	'button',
							tooltip		:	'Cancel Add Property',
							//icon		:	'/img/cancel-property.png',
							cls			:	'x-btn-text',
							iconCls		:	"icon-del-property",
							scale		:	"large",
							text		:	'Cancel',
							height		:	40,
							scale		:	'large',
							width		:	90,
							handler		:	function(){
								var tabsAux = Ext.getCmp('tabs');
								var tabAux = tabsAux.getItem('id-tproadd');
								tabsAux.remove(tabAux);
							}
						}]
					}/*,{
						xtype	:	'fieldset',
						title	:	'Property',
						width	:	600,
						height	:	206,
						//layout	:	'column',
						items	:	[displayImageAdd]
					}*/]//end items fieldset
				}],//end items fieldset
				listeners: {
					afterrender : function(){
						//expandcollapse('PR');
						//Ext.getCmp('PRMORE').expand();
						/*if(resultET.result.residential){
							if(resultET.result.residential.length !== 0)
								Ext.getCmp('FS').expand();
						}*/
					}
				}
			});
			
			formulPsummaryAdd = new Ext.FormPanel({
				url			:	'properties_tabs/propertyAdd/propertyAdded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulPsummaryAdd',
				name		:	'formulPsummaryAdd',
				items		:	[{
					xtype		:	'fieldset',
					//title		:	'Public Records',
					collapsible	:	false,
					collapsed	:	false,
					id			:	'PR',
					name		:	'PR',
					items		:	{
						layout	:	'column',
						items	: 	publicrecordEdit
					}
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Sales History</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'SL',
					name		:	'SL',
					items		:	[{
						xtype	:	'panel',
						id		:	'SLADDSALES'/*,
						items	:	[{
						}]*/
					},{
						//layout	:	'column'
						items	:	[{
							xtype	:	'button',
							cls		:	'x-btn-text',
							text	:	'<i class="icon-plus"></i>',
							tooltip	:	'Add more Sales',
							listeners	:	{
								click	:	function(button){
									var aux = addSaleHistoryPTED();
									Ext.getCmp('SLADDSALES').add(aux);
									Ext.getCmp('SLADDSALES').doLayout();
									Ext.getCmp('addSalesTotalED').setValue(countSalesHistoryPTED);
								}
							}
						},{
							xtype	:	'hidden',
							name	:	'addSalesTotalED',
							id		:	'addSalesTotalED'
						}]
					}]
				},{
					xtype		:	'fieldset',
					title		:	'<h2>Owner Information</h2>',
					collapsible :	false,
					collapsed	:	false,
					id			:	'PRMOREADD',
					name		:	'PRMOREADD',
					items		:	{
						layout	:	'column',
						items	:	publicrecordmoreEdit
					}
				}],
				listeners:{
					afterrender:	function(){
						//console.debug(this);
						setTimeout(function(){
							$(Ext.get('adpsummary*folio').dom).on('keyup',function (){
								//Ext.getCmp('admlsresidential*folio').setValue($(this).val());
								Ext.getCmp('adpendes*folio').setValue($(this).val());
								Ext.getCmp('admortgage*folio').setValue($(this).val());
							});
							/*$(Ext.get('admlsresidential*folio').dom).on('keyup',function (){
								Ext.getCmp('adpsummary*folio').setValue($(this).val());
								Ext.getCmp('adpendes*folio').setValue($(this).val());
								Ext.getCmp('admortgage*folio').setValue($(this).val());
							});*/
							$(Ext.get('adpendes*folio').dom).on('keyup',function (){
								//Ext.getCmp('admlsresidential*folio').setValue($(this).val());
								Ext.getCmp('adpsummary*folio').setValue($(this).val());
								Ext.getCmp('admortgage*folio').setValue($(this).val());
							});
							$(Ext.get('admortgage*folio').dom).on('keyup',function (){
								//Ext.getCmp('admlsresidential*folio').setValue($(this).val());
								Ext.getCmp('adpendes*folio').setValue($(this).val());
								Ext.getCmp('adpsummary*folio').setValue($(this).val());
							});
						},500);
						//console.debug(Ext.getCmp('edpsummary*folio'));
						/*Ext.getCmp('edpsummary*folio').onBlur(event, target){
							console.debug("ASASASASs");
						};*/
					}
				}
			});

			/*formulResidentialAdd = new Ext.FormPanel({
				url			:	'properties_tabs/propertyAdd/propertyAdded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulResidentialAdd',
				name		:	'formulResidentialAdd',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'For Sale/For Rent',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FSADD',
					name		:	'FSADD',
					items		:	{
						layout	:	'column',
						items	: 	listingEdit
					}
				}
			});*/
			
			formulForeclosedAdd = new Ext.FormPanel({
				url			:	'properties_tabs/propertyAdd/propertyAdded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulForeclosedAdd',
				name		:	'formulForeclosedAdd',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Foreclosure',
					collapsible :	false,
					collapsed	:	false,
					id			:	'FOADD',
					name		:	'FOADD',
					items		:	{
						layout	:	'column',
						items	:	foreclosureEdit
					}
				}
			});

			formulMortgageAdd = new Ext.FormPanel({
				url			:	'properties_tabs/propertyAdd/propertyAdded.php',
				frame		:	true,
				bodyStyle	:	'padding:5px 5px 0;	text-align:left;	background-color: white;',
				id			:	'formulMortgageAdd',
				name		:	'formulMortgageAdd',
				items		:	{
					xtype		:	'fieldset',
					//title		:	'Mortgage',
					collapsible :	false,
					collapsed	:	false,
					id			:	'MOADD',
					name		:	'MOADD',
					items		:	{
						layout	:	'column',
						items	:	mortgageEdit
					}
				}
			});
			
			var panelsAdd = new Ext.TabPanel({
				id				:	'panelsAdd-pt',
				renderTo		:	'panelsAdd',
		   		//activeTab		:	0,
				autoWidth		:	true,
				deferredRender	:	false,
				height			:	system_height - ($("#panelsAdd").offset().top - tablevel_height),
				plain			:	true,
				enableTabScroll	:	true,
				defaults		:	{  
					autoScroll	:	true
				},
				items:[
					{
						title	:	'Public Records', 
						id		:	'PR-tab-add-PT',
						items	:	formulPsummaryAdd,
						listeners:{
							afterrender:	function(){
								//Ext.getCmp('panelsAdd-pt').setHeight(1000);
							}
						}
					}/*,{
						title	:	'For Sale / For Rent',
						id		:	'LS-tab-add-PT',
						items	:	formulResidentialAdd
					}*/,{
						title	:	'Foreclosure',
						id		:	'FC-tab-add-PT',
						items	:	formulForeclosedAdd
					},{
						title	: 	'Mortgage',
						id		:	'MT-tab-add-PT',
						items	:	formulMortgageAdd
					}/*,{
						title	: 	'Images',
						id		:	'IMG-tab-add-PT',
						autoLoad:	{
							url		:	'/properties_tabs/propertyImages/propertyImagesAdd.php', 
							scripts	:	true,
							timeout	:	10800
						}
					}*/
				],
				listeners:{
					afterrender:	function(tab){
						tab.setActiveTab(0);
						tab.doLayout();
						var aux = addSaleHistoryPTED();
						Ext.getCmp('SLADDSALES').add(aux);
						Ext.getCmp('SLADDSALES').doLayout();
						Ext.getCmp('addSalesTotalED').setValue(countSalesHistoryPTED);
						$("#addButtonsPT").show(1000);
						
						//Hide Panels To Dont Use
						/*var tabPanel = Ext.getCmp('panelsAdd-pt');
						var tabToHide = Ext.getCmp('LS-tab-add-PT');
						tabPanel.hideTabStripItem(tabToHide);*/
					},
					tabchange: function(tabPanel,panel){
						
					}
				}
			});
			loading_win.hide();
		}
		
	});

</script>