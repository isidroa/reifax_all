<?php
include('../../properties_conexion.php');
conectar();

switch($_POST['action']){
	case ('record'):
		if($_POST['idlisting']==0){
			$q="INSERT INTO followup (parcelid,userid,type,followdate) VALUES 
					(999999999,{$_COOKIE['datos_usr']['USERID']},'{$_POST['type']}',NOW())";
			mysql_query($q) or die($q.mysql_error());
			
			$idfollowup=mysql_insert_id();
			
			$q="INSERT INTO `followup_template`
				(`id_followup`,
					`date`,
					`name`)
					VALUES
				({$idfollowup},NOW(),'{$_POST['title']}');";
				
			mysql_query($q) or die($q.mysql_error());
		
			$q="INSERT INTO listing (idfollowup, idlstatus, bath, beds, folio, larea,  
									lot, pool, waterf, yrbuilt, propertype, bedrooms, bathrooms,
									garea, lotsize, waterfront,stories)  
				select $idfollowup, 1, m.bath, m.beds, m.folio, m.lsqft, m.tsqft, if(m.pool='N',0,1), m.waterf, m.yrbuilt, m.ccoded, 
				m.beds, m.bath, m.lsqft, m.tsqft, if(m.waterf='N',0,1), m.stories  
				from properties p
				left join psummary m on p.parcelid=m.parcelid
				where p.parcelid=999999999";
			mysql_query($q) or die($q.mysql_error());
			$idlisting=mysql_insert_id();
			
			/*************
			
				save docs new template
			*/
			foreach ($_POST['documents'] as $k => $v){
				$query	=	"
					INSERT INTO listingdocs	
						(idlisting,{$v['campIdName']})	
					VALUES	
						({$idlisting},{$v['idDocument']})";
				mysql_query($query) or die(json_encode(Array('success'=>'false','message'=>$query."--".mysql_error())));
			}
			
			echo json_encode(array(
				'success'		=>	true,
				'idlisting'		=>	$idlisting,
				'idfollowup'	=> 	$idfollowup
			));
		}
		else{
			$q="UPDATE followup_template SET
				`name`='{$_POST['title']}' where id_followup={$_POST['idfollowup']}";
			mysql_query($q) or die($q.mysql_error());
			
			echo json_encode(array(
				'success'		=>	true
			));
		}
	break;
	case ('delete'):
		$sql="delete from followup where idfollowup in (select id_followup FROM realtytask.followup_template where id in ({$_POST['ids']}));";
			$res=mysql_query($sql) or die(json_encode(
				array(
					'success'	=> false,
					'error'		=> mysql_error(),
					'sql'		=> $sql
				)
			));
			echo json_encode(array(
				'success'		=>	true
			));
		
	break;
	case ('list'):
		$sql="select t.* from followup_template t join followup f on t.id_followup=f.idfollowup where f.userid={$_COOKIE['datos_usr']['USERID']} AND f.type='{$_POST['type']}'";
		$res=mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		$templates=array();
		while($data=mysql_fetch_assoc($res)){
			array_push($templates,$data);
		}
		echo json_encode(
			array(
				'success'	=> true,
				'templates' => $templates
			)
		);
	break;
	case ('applyTemplate'):
		$sql="select l.idlisting from followup_template t 
				join listing l on l.idfollowup=t.id_followup
			where t.id={$_POST['template']}
			";
		$res=mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		$template=mysql_fetch_assoc($res);
		
		$sql="SELECT GROUP_CONCAT(l.idlisting,' ') originales FROM realtytask.followup f join listing l on l.idfollowup=f.idfollowup WHERE f.parcelid in ({$_POST['current']}) GROUP BY userid;";
		$res=mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		$idListing=mysql_fetch_assoc($res);
		$newInsert='('.(str_replace(',','),(',$idListing['originales'])).')';
		$newInsertCSV='('.(str_replace(',','),(',$idListing['originales'])).')';
		
		
		$sql="UPDATE `listing` l
				left join `listing` t on
					t.idlisting={$template['idlisting']}
				set
					l.`idlstatus` = t.`idlstatus`,
					l.`Tcomission` = t.`Tcomission`,
					l.`LBcomission` = t.`LBcomission`,
					l.`SBcomission` = t.`SBcomission`,
					l.`AMcompany` = t.`AMcompany`,
					l.`Tcomissionpor` = t.`Tcomissionpor`,
					l.`LBcomissionpor` = t.`LBcomissionpor`,
					l.`SBcomissionpor` = t.`SBcomissionpor`
				WHERE l.`idlisting` in ({$idListing['originales']});";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		
		$sql="delete from listingaccepta where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingaccepta (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingaccepta` l
				left join `listingaccepta` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`datea` = t.datea,
					l.`hour` = t.hour,
					l.`buyer` = t.buyer
				WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingbroker where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingbroker (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingbroker` l
				left join `listingbroker` t on
					t.idlisting={$template['idlisting']}
				SET
				l.`name1` = t.name1,
					l.`name2` = t.name2,
					l.`firm1` = t.firm1,
					l.`firm2` = t.firm2,
					l.`license1` = t.license1,
					l.`license2` = t.license2,
					l.`fee1` = t.fee1,
					l.`fee2` = t.fee2,
					l.`state1` = t.state1,
					l.`porce1` = t.porce1,
					l.`state2` = t.state2,
					l.`buyer` = t.buyer,
					l.`porce2` = t.porce2
				WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingclosing where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingclosing (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingclosing` l
				left join `listingclosing` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`closing` = t.closing,
					l.`datec` = t.datec,
					l.`dolar` = t.dolar,
					l.`porce` = t.porce
				WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingemd where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingemd (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingemd` l
			left join `listingemd` t on
				t.idlisting={$template['idlisting']}
			
			SET
				l.`agentcontactname` = t.agentcontactname,
					l.`agentcompanyname` = t.agentcompanyname,
					l.`initdeposite` = t.initdeposite,
					l.`adddeposite` = t.adddeposite,
					l.`days1` = t.days1,
					l.`days2` = t.days2,
					l.`initdolar` = t.initdolar,
					l.`initpor` = t.initpor,
					l.`adddolar` = t.adddolar,
					l.`addpor` = t.addpor,
					l.`type` = t.type,
					l.`totalFin` = t.totalFin,
					l.`otherE` = t.otherE,
					l.`balanClo` = t.balanClo,
					l.`address1` = t.address1,
					l.`city1` = t.city1,
					l.`zip1` = t.zip1,
					l.`state1` = t.state1,
					l.`country1` = t.country1
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingfinanci where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingfinanci (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingfinanci` l
				left join `listingfinanci` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`cash` = t.cash,
					l.`conventional` = t.conventional,
					l.`fha` = t.fha,
					l.`mortgageassu` = t.mortgageassu,
					l.`va` = t.va,
					l.`moneynote` = t.moneynote,
					l.`other` = t.other,
					l.`specify` = t.specify,
					l.`days1` = t.days1,
					l.`days2` = t.days2,
					l.`method` = t.method,
					l.`qualified` = t.qualified
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listinghoa where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listinghoa (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listinghoa` l
				left join `listinghoa` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`hoa1` = t.hoa1,
					l.`hoa1approval` = t.hoa1approval,
					l.`hoa1refusal` = t.hoa1refusal,
					l.`hoa1contact` = t.hoa1contact,
					l.`hoa1name` = t.hoa1name,
					l.`company1contact` = t.company1contact,
					l.`company1company` = t.company1company,
					l.`feeshoa1` = t.feeshoa1,
					l.`feesdue1` = t.feesdue1,
					l.`assessmenthoa1` = t.assessmenthoa1,
					l.`assessmenthoa1due` = t.assessmenthoa1due,
					l.`parkingspaces11` = t.parkingspaces11,
					l.`parkingspaces12` = t.parkingspaces12,
					l.`garage11` = t.garage11,
					l.`garage12` = t.garage12,
					l.`mailbox11` = t.mailbox11,
					l.`mailbox12` = t.mailbox12,
					l.`otherareas1` = t.otherareas1,
					l.`hoa2` = t.hoa2,
					l.`hoa2approval` = t.hoa2approval,
					l.`hoa2refusal` = t.hoa2refusal,
					l.`hoa2contact` = t.hoa2contact,
					l.`hoa2name` = t.hoa2name,
					l.`company2contact` = t.company2contact,
					l.`company2company` = t.company2company,
					l.`feeshoa2` = t.feeshoa2,
					l.`feesdue2` = t.feesdue2,
					l.`assessmenthoa21` = t.assessmenthoa21,
					l.`assessmenthoa2due` = t.assessmenthoa2due,
					l.`parkingspaces21` = t.parkingspaces21,
					l.`parkingspaces22` = t.parkingspaces22,
					l.`garage21` = t.garage21,
					l.`garage22` = t.garage22,
					l.`mailbox21` = t.mailbox21,
					l.`mailbox22` = t.mailbox22,
					l.`otherareas2` = t.otherareas2,
					l.`doc1` = t.doc1,
					l.`doc2` = t.doc2,
					l.`datedoc1` = t.datedoc1,
					l.`datedoc2` = t.datedoc2
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingfinanci where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingfinanci (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingfinanci` l
				left join `listingfinanci` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`cash` = t.cash,
					l.`conventional` = t.conventional,
					l.`fha` = t.fha,
					l.`mortgageassu` = t.mortgageassu,
					l.`va` = t.va,
					l.`moneynote` = t.moneynote,
					l.`other` = t.other,
					l.`specify` = t.specify,
					l.`days1` = t.days1,
					l.`days2` = t.days2,
					l.`method` = t.method,
					l.`qualified` = t.qualified
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listinginspectios where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listinginspectios (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listinginspectios` l
				left join `listinginspectios` t on
					t.idlisting={$template['idlisting']}
				SET
				l.`days` = t.days,
					l.`homewarra` = t.homewarra,
					l.`exceed` = t.exceed,
					l.`repair1` = t.repair1,
					l.`valuerepair1` = t.valuerepair1,
					l.`porrepair1` = t.porrepair1,
					l.`repair2` = t.repair2,
					l.`valuerepair2` = t.valuerepair2,
					l.`porrepair2` = t.porrepair2,
					l.`repair3` = t.repair3,
					l.`valuerepair3` = t.valuerepair3,
					l.`porrepair3` = t.porrepair3
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingother where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingother (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingother` l
				left join `listingother` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`assignability` = t.assignability,
					l.`assessments` = t.assessments
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingperpor where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingperpor (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingperpor` l
				left join `listingperpor` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`aboveground` = t.aboveground,
					l.`ceiling` = t.ceiling,
					l.`dishwasher` = t.dishwasher,
					l.`draperies` = t.draperies,
					l.`dryer` = t.dryer,
					l.`freezer` = t.freezer,
					l.`garagedoor` = t.garagedoor,
					l.`garbagedisposal` = t.garbagedisposal,
					l.`generator` = t.generator,
					l.`hotubwitheater` = t.hotubwitheater,
					l.`intercom` = t.intercom,
					l.`lightfixtures` = t.lightfixtures,
					l.`poolbarrier` = t.poolbarrier,
					l.`poolequipment` = t.poolequipment,
					l.`poolheater` = t.poolheater,
					l.`range` = t.range,
					l.`refrigerator` = t.refrigerator,
					l.`rods` = t.rods,
					l.`satellite` = t.satellite,
					l.`securitygate` = t.securitygate,
					l.`securitysystem` = t.securitysystem,
					l.`smokedetector` = t.smokedetector,
					l.`spa` = t.spa,
					l.`standaloneice` = t.standaloneice,
					l.`storageshed` = t.storageshed,
					l.`stormpanels` = t.stormpanels,
					l.`stormshutters` = t.stormshutters,
					l.`tvantenna` = t.tvantenna,
					l.`wallac` = t.wallac,
					l.`washer` = t.washer,
					l.`watersoftener` = t.watersoftener,
					l.`window` = t.window,
					l.`other` = t.other,
					l.`otherContent` = t.otherContent
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingrental where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingrental (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingrental` l
				left join `listingrental` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`first` = t.first,
					l.`firstDate` = t.firstDate,
					l.`last` = t.last,
					l.`lastDate` = t.lastDate,
					l.`specify` = t.specify,
					l.`otherVal` = t.otherVal,
					l.`otherDate` = t.otherDate,
					l.`security` = t.security,
					l.`securityDate` = t.securityDate
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingseller where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingseller (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingseller` l
				left join `listingseller` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`name1` = t.name1,
					l.`address1` = t.address1,
					l.`city1` = t.city1,
					l.`state1` = t.state1,
					l.`country1` = t.country1,
					l.`zip1` = t.zip1,
					l.`borrower` = t.borrower,
					l.`type1` = t.type1,
					l.`name2` = t.name2,
					l.`address2` = t.address2,
					l.`city2` = t.city2,
					l.`state2` = t.state2,
					l.`country2` = t.country2,
					l.`zip2` = t.zip2,
					l.`cborrower` = t.cborrower,
					l.`type2` = t.type2,
					l.`agent_seller1` = t.agent_seller1,
					l.`agent_seller2` = t.agent_seller2
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingterm where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingterm (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingterm` l
				left join `listingterm` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`terms` = t.terms
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		///////////////////////////////
		
		///////////////////////////////
		$sql="delete from listingtitle where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		$sql="INSERT INTO listingtitle (idlisting) VALUES {$newInsert}";
		mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
		
		$sql="UPDATE `listingtitle` l
				left join `listingtitle` t on
					t.idlisting={$template['idlisting']}
				
				SET
				l.`insurance` = t.insurance,
					l.`days1` = t.days1,
					l.`days2` = t.days2,
					l.`chargers` = t.chargers,
					l.`pay` = t.pay,
					l.`survey` = t.survey,
					l.`surveyDays` = t.surveyDays
			WHERE l.`idlisting` in ({$idListing['originales']})";
		mysql_query($sql) or die(json_encode(
			array(
				'success'	=> false,
				'error'		=> mysql_error(),
				'sql'		=> $sql
			)
		));
		
		/*******************
		*
		*	copia documentos
		*
		*************************************/
		
		$idsListings=explode(',',$newInsertCSV);
		$sql="delete from listingdocs where idlisting in ({$idListing['originales']})";
		mysql_query($sql);
		
		$sql="SELECT * FROM listingdocs where idlisting={$template['idlisting']};";
		$result=mysql_query($sql);
		while($docs=mysql_fetch_assoc($result)){
			foreach($idsListings as $k => $currentListing){
				if($docs['idAddons']!=''){
					$sql="	INSERT INTO contracts_addonscustom (
								userid,
								idOriginal,
								place,
								type,
								document,
								addon_act,
								addon_name,
								state
							) 
							SELECT 
								userid,
								idOriginal,
								place,
								type,
								document,
								addon_act,
								addon_name,
								state
							FROM contracts_addonscustom WHERE idAddons={$docs['idAddons']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$idAddons=mysql_insert_id();
					
					$sql="	INSERT INTO contracts_campos_addons (
								camp_userid,
								camp_contract_id,
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							) 
							SELECT 
								camp_userid,
								{$idAddons},
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							FROM contracts_campos_addons WHERE camp_contract_id={$docs['idAddons']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$sql="	INSERT INTO listingdocs (
								idListing,
								idAddons,
								checks,
								otherId,
								otherText,
								status
							) 
							SELECT 
								{$currentListing},
								{$idAddons},
								checks,
								otherId,
								otherText,
								status
							FROM listingdocs WHERE idDoc={$docs['idDoc']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
				}
				elseif ($docs['idCustom']!=''){
					
					$sql="	INSERT INTO contracts_custom (
								idOriginal,
								name,
								filename,
								tpl1_name,
								tpl1_addr,
								tpl1_addr2,
								tpl1_addr3,
								tplactive,
								userid,
								tpl2_name,
								tpl2_addr,
								tpl2_addr2,
								tpl2_addr3,
								date_upload,
								signature,
								type
							) 
							SELECT 
								idOriginal,
								name,
								filename,
								tpl1_name,
								tpl1_addr,
								tpl1_addr2,
								tpl1_addr3,
								tplactive,
								userid,
								tpl2_name,
								tpl2_addr,
								tpl2_addr2,
								tpl2_addr3,
								date_upload,
								signature,
								type
							FROM contracts_custom WHERE idCustom={$docs['idCustom']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$idCustom=mysql_insert_id();
					
					$sql="	INSERT INTO contracts_campos_addons (
								camp_userid,
								camp_contract_id,
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							) 
							SELECT 
								camp_userid,
								{$idCustom},
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							FROM contracts_campos_addons WHERE camp_contract_id={$docs['idCustom']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$sql="	INSERT INTO listingdocs (
								idListing,
								idCustom,
								checks,
								otherId,
								otherText,
								status
							) 
							SELECT 
								{$currentListing},
								{$idCustom},
								checks,
								otherId,
								otherText,
								status
							FROM listingdocs WHERE idDoc={$docs['idDoc']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
				}
				else{
					
					$sql="	INSERT INTO contracts_othercustom (
								userid,
								idOriginal,
								place,
								type,
								document,
								other_act,
								other_name,
								state
							) 
							SELECT 
								userid,
								idOriginal,
								place,
								type,
								document,
								other_act,
								other_name,
								state
							FROM contracts_othercustom WHERE idOther={$docs['idOther']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$idOther=mysql_insert_id();
					
					$sql="	INSERT INTO contracts_campos_addons (
								camp_userid,
								camp_contract_id,
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							) 
							SELECT 
								camp_userid,
								{$idOther},
								camp_page,
								camp_type,
								camp_idtc,
								camp_text,
								camp_posx,
								camp_posy
							FROM contracts_campos_addons WHERE camp_contract_id={$docs['idOther']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
					$sql="	INSERT INTO listingdocs (
								idListing,
								idOther,
								checks,
								otherId,
								otherText,
								status
							) 
							SELECT 
								{$currentListing},
								{$idOther},
								checks,
								otherId,
								otherText,
								status
							FROM listingdocs WHERE idDoc={$docs['idDoc']}
						";
					mysql_query($sql) or die (json_encode(array('success'=> false, 'sql' => $sql, 'error' => mysql_error())));
				}
			}
		}
		
		
		
		
		echo json_encode(array(
			'success'	=> true
		));
	break;

}
?>