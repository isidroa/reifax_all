<?php 
$url_youtube='http://www.youtube.com/embed/';
$source = $_REQUEST['source'];
?>
<link rel="stylesheet" type="text/css" href="../resources/css/cssvideos.css" />
<script type="text/javascript" src="../includes/jquery.js"></script>
<style>
body {
	font: 100%/1.4 Arial, Helvetica, sans-serif;
	color: #000;
	margin:auto;
}

.cuote {
	background-color: #D90461;
	float: left;
	height: 5px;
	margin-right: 5px;
	margin-top: 3px;
	width: 5px;
}
a{
	text-decoration:none;
}

.videoV .list a {
	color: #4A4C59;
	display: block;
	font-size: 14px;
	height: 40px;
	line-height: normal !important;
	padding-left: 10px;
	width:100% !important;
}
.videoV .list a .cuote {
	margin-right: 5px;
	margin-top: 5px;
} 
.videoV .list {
    height: 320px !important;
    overflow-y: auto;
	overflow-x:hidden;
	width:480px !important;
}
.containervideos {
    background: none repeat scroll 0 0 #FFFFFF;
    margin: 0 auto;
    position: relative;
}
</style>
<div class="containervideos">
  	<div id="contentPrincipalVideos">
    		<div class="panelVideos">
           	 	<div class="center videoV"> 
            	<div class="title">
                	<div>Select a video below to start!</div>
                </div>
                <div></div>
                <div id="list" class="list">
                <ul>
				<?php
				if($source=='BasicSearch')
				{
					$firstVideo='vQj1MXYj3vk';
				?>
                	<li class="parV">
                    	<a href="#" rel="vQj1MXYj3vk">
                        	<div class="cuote"></div>Basic Search 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="IFAaMnOjflo">
                        	<div class="cuote"></div>Save Searches
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="mxYjxucpQE4">
                        	<div class="cuote"></div>Mobile Homes 
                        </a>
                    </li>
					
				<?php
				}//fin if($source=='BasicSearch')
				
				if($source=='AdvancedSearch')
				{
					$firstVideo='FYuLFzw03ew';
				?>
                	<li class="parV">
                    	<a href="#" rel="FYuLFzw03ew">
                        	<div class="cuote"></div>Advanced Search 
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="IFAaMnOjflo">
                        	<div class="cuote"></div>Save Searches
                        </a>
                    </li>
				<?php
				}//fin if($source=='AdvancedSearch')
				?>

				<?php
				if($source=='BasicResult')
				{
					$firstVideo='YCOzsWxKEi4';
				?>
                	<li class="parV">
                    	<a href="#" rel="YCOzsWxKEi4">
                        	<div class="cuote"></div>Results 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="7hx7nKCJQxM">
                        	<div class="cuote"></div>Follow up Several Properties
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="UjSzKE54XAs">
                        	<div class="cuote"></div>Follow Up a Property
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="rp7cFD-s9sM">
                        	<div class="cuote"></div>Mailing Campaign
                        </a>
                    </li>
					
				<?php
				}//fin if($source=='BasicResult')
				?>
				
				<?php
				if($source=='AdvancedResult')
				{
					$firstVideo='YCOzsWxKEi4';
				?>
                	<li class="parV">
                    	<a href="#" rel="YCOzsWxKEi4">
                        	<div class="cuote"></div>Results 
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="TNlsdLAOLc8">
                        	<div class="cuote"></div>Templates 
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="7hx7nKCJQxM">
                        	<div class="cuote"></div>Follow up Several Properties
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="UjSzKE54XAs">
                        	<div class="cuote"></div>Follow Up a Property
                        </a>
                    </li>	
                    <li class="parV">
                    	<a href="#" rel="rp7cFD-s9sM">
                        	<div class="cuote"></div>Mailing Campaign
                        </a>
                    </li>				
				<?php
				}//fin if($source=='AdvancedResult')
				?>
				
				<?php
				if($source=='PropertiesOverview')
				{
					$firstVideo='GcNGJFh7SK0';
				?>
                    <li class="parV">
                    	<a href="#" rel="GcNGJFh7SK0">
                        	<div class="cuote"></div>Overview Training 
                        </a>
                    </li>
                	<li class="imparV">
                    	<a href="#" rel="kQsFxBwMQtE">
                        	<div class="cuote"></div>Property Analysis Report 
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="lwredU6KcIQ">
                        	<div class="cuote"></div>Discount Report 
                        </a>
                    </li>
                	<li class="imparV">
                    	<a href="#" rel="XaYT5z6OLdw">
                        	<div class="cuote"></div>X-Ray Report 
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="I1NxNv3ZfcA">
                        	<div class="cuote"></div>BPO Report 
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="6At-awymQ0Y">
                        	<div class="cuote"></div>Follow Up Selling
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="UjSzKE54XAs">
                        	<div class="cuote"></div>Follow Up a Property
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="YJsq9bwt7Ws">
                        	<div class="cuote"></div>How to send a contract from the overview tab?
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="o0QM4tRDhkE">
                        	<div class="cuote"></div>Offer Price Using Freddy
                        </a>
                    </li>
				<?php
				}//fin if($source=='PropertiesOverview')
				?>
				
 				<?php
				if($source=='BuyingFollowup')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="injxfLMDETI">
                        	<div class="cuote"></div>Calculating Offer Price
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="7hx7nKCJQxM">
                        	<div class="cuote"></div>Follow up Several Properties
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="UjSzKE54XAs">
                        	<div class="cuote"></div>Follow Up a Property
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="o0QM4tRDhkE">
                        	<div class="cuote"></div>Offer Price Using Freddy
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>	
                    <li class="parV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="imparV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="rhyc3z2Hy_I">
                        	<div class="cuote"></div>Schedule Task
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='BuyingFollowup')
				?>
				
 				<?php
				if($source=='BuyingPendingTask')
				{
					$firstVideo='K_zdoA3wQYQ';
				?>
                    <li class="parV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="injxfLMDETI">
                        	<div class="cuote"></div>Calculating Offer Price
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="o0QM4tRDhkE">
                        	<div class="cuote"></div>Offer Price Using Freddy
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="parV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="parV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>			
                    <li class="parV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='BuyingPendingTask')
				?>
				
  				<?php
				if($source=='BuyingPendingContracts')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="injxfLMDETI">
                        	<div class="cuote"></div>Calculating Offer Price
                        </a>
                    </li>   
					<li class="imparV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="o0QM4tRDhkE">
                        	<div class="cuote"></div>Offer Price Using Freddy
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="imparV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>									
                    <li class="imparV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='BuyingPendingContracts')
				?>
				
				
 				<?php
				if($source=='SellingFollowup')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
					<li class="parV">
                    	<a href="#" rel="6At-awymQ0Y">
                        	<div class="cuote"></div>Follow Up Selling
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="7hx7nKCJQxM">
                        	<div class="cuote"></div>Follow up Several Properties
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="imparV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="rhyc3z2Hy_I">
                        	<div class="cuote"></div>Schedule Task
                        </a>
                    </li>			
                    <li class="parV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='SellingFollowup')
				?>
				
 				<?php
				if($source=='SellingPendingTask')
				{
					$firstVideo='K_zdoA3wQYQ';
				?>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
 					<li class="imparV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
                   <li class="parV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="imparV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>				
                    <li class="imparV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='SellingPendingTask')
				?>
				
  				<?php
				if($source=='SellingPendingContracts')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
  					<li class="imparV">
                    	<a href="#" rel="6PeZ7Noil4Q">
                        	<div class="cuote"></div>Getting Contact Info
                        </a>
                    </li>
                   <li class="parV">
                    	<a href="#" rel="1RaiAeqF2rs">
                        	<div class="cuote"></div>Overview Button
                        </a>
                    </li>	
                    <li class="imparV">
                    	<a href="#" rel="GzwcTNAbf-I">
                        	<div class="cuote"></div>Send a Fax
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GG1DXhtQOgQ">
                        	<div class="cuote"></div>Send Contracts
                        </a>
                    </li>		
                    <li class="imparV">
                    	<a href="#" rel="Z3NX-heJ64g">
                        	<div class="cuote"></div>Send Emails
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="GEhwAlMH5cE">
                        	<div class="cuote"></div>Send SMS
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='SellingPendingContracts')
				?>
				
  				<?php
				if($source=='FollowupContact')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
				<?php
				}//fin if($source=='FollowupContact')
				?>
				
  				<?php
				if($source=='BlockProperties')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
				<?php
				}//fin if($source=='BlockProperties')
				?>
				
  				<?php
				if($source=='BlockContacts')
				{
					$firstVideo='SRUA6bzppLk';
				?>
                    <li class="parV">
                    	<a href="#" rel="SRUA6bzppLk">
                        	<div class="cuote"></div>Blocking Function 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="K_zdoA3wQYQ">
                        	<div class="cuote"></div>Delete, Print and Export
                        </a>
                    </li>   
					<li class="parV">
                    	<a href="#" rel="9i6qZJ9SSyg">
                        	<div class="cuote"></div>Filter Properties
                        </a>
                    </li>
				<?php
				}//fin if($source=='BlockContacts')
				?>
				
 				<?php
				if($source=='TemplateEmail')
				{
					$firstVideo='TNlsdLAOLc8';
				?>
                    <li class="parV">
                    	<a href="#" rel="TNlsdLAOLc8">
                        	<div class="cuote"></div>Create Templates 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="5YvaVhXgoyg">
                        	<div class="cuote"></div>How to set up your mail templates?
                        </a>
                    </li>
				<?php
				}//fin if($source=='TemplateEmail')
				?>
				
 				<?php
				if($source=='TemplateSms')
				{
					$firstVideo='TNlsdLAOLc8';
				?>
                    <li class="parV">
                    	<a href="#" rel="TNlsdLAOLc8">
                        	<div class="cuote"></div>Create Templates 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="EUW5bbXSpDw">
                        	<div class="cuote"></div>How to set up an sms template?
                        </a>
                    </li>
				<?php
				}//fin if($source=='TemplateSms')
				?>
				
 				<?php
				if($source=='TemplateDocuments')
				{
					$firstVideo='TNlsdLAOLc8';
				?>
                    <li class="parV">
                    	<a href="#" rel="TNlsdLAOLc8">
                        	<div class="cuote"></div>Create Templates 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="bU7ZLQ7C7GA">
                        	<div class="cuote"></div>How to set up a mail merge template?
                        </a>
                    </li>
				<?php
				}//fin if($source=='TemplateDocuments')
				?>
				
 				<?php
				if($source=='TemplateFax')
				{
					$firstVideo='TNlsdLAOLc8';
				?>
                    <li class="parV">
                    	<a href="#" rel="TNlsdLAOLc8">
                        	<div class="cuote"></div>Create Templates 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="_Zf8q-gw8pE">
                        	<div class="cuote"></div>How to set up a fax template?
                        </a>
                    </li>
				<?php
				}//fin if($source=='TemplateFax')
				?>
				
 				<?php
				if($source=='Mycontracts')
				{
					$firstVideo='roEtfXjFNMw';
				?>
                    <li class="parV">
                    	<a href="#" rel="roEtfXjFNMw">
                        	<div class="cuote"></div>Creating Contracts
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="AuN14VV9MCA">
                        	<div class="cuote"></div>How to add an escrow letter?
                        </a>
                    </li>
                     <li class="parV">
                    	<a href="#" rel="TcJnYEn-xrc">
                        	<div class="cuote"></div>How to upload a contract?
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="rHeqgNw6bGE">
                        	<div class="cuote"></div>How to modify a contract? 
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="2oKCUohNWxQ">
                        	<div class="cuote"></div>How to add the buyer name? 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="wAOmuRoISPM">
                        	<div class="cuote"></div>How to modify an additional document? 
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="gTcV2e9GRbg">
                        	<div class="cuote"></div>How to delete a contract? 
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="RomJnHz9OTo">
                        	<div class="cuote"></div>How to delete an additional document? 
                        </a>
                    </li>
				<?php
				}//fin if($source=='Mycontracts')
				?>
				
 				<?php
				if($source=='FollowMyemail')
				{
					$firstVideo='9iGrmVH7smU';
				?>
                     <li class="parV">
                    	<a href="#" rel="9iGrmVH7smU">
                        	<div class="cuote"></div>How to Fetch emails
                        </a>
                    </li>
				<?php
				}//fin if($source=='FollowMyemail')
				?>
				
 				<?php
				if($source=='ImportData')
				{
					$firstVideo='6aZeEQ_ZLNk';
				?>
                    <li class="parV">
                    	<a href="#" rel="6aZeEQ_ZLNk">
                        	<div class="cuote"></div>Import Properties
                        </a>
                    </li>
				<?php
				}//fin if($source=='ImportData')
				?>
				
 				<?php
				if($source=='Mailingcampaing')
				{
					$firstVideo='rp7cFD-s9sM';
				?>                    
                    <li class="parV">
                    	<a href="#" rel="rp7cFD-s9sM">
                        	<div class="cuote"></div>Mailing Campaign
                        </a>
                    </li>
				<?php
				}//fin if($source=='Mailingcampaing')
				?>
				
 				<?php
				if($source=='MailSetting')
				{
					$firstVideo='8NOguR4Zz-4';
				?>                  
                    <li class="parV">
                    	<a href="#" rel="8NOguR4Zz-4">
                        	<div class="cuote"></div>Setup Follow Up
                        </a>
                    </li>
                     <li class="imparV">
                    	<a href="#" rel="RchrOY28sug">
                        	<div class="cuote"></div>How to set up your email?
                        </a>
                    </li>
                    <li class="parV">
                    	<a href="#" rel="aOHJsNTdl5Q">
                        	<div class="cuote"></div>How to set up your google voice number?
                        </a>
                    </li>
                    <li class="imparV">
                    	<a href="#" rel="sjPSdUJ6A8Q">
                        	<div class="cuote"></div>How to set up your gmail account?
                        </a>
                    </li>
				<?php
				}//fin if($source=='MailSetting')
				?>				
				
 				<?php
				if($source=='PhoneSetting')
				{
					$firstVideo='8NOguR4Zz-4';
				?>                  
                    <li class="parV">
                    	<a href="#" rel="8NOguR4Zz-4">
                        	<div class="cuote"></div>Setup Follow Up
                        </a>
                    </li>
					<li class="imparV">
                    	<a href="#" rel="feY_uBQREFw">
                        	<div class="cuote"></div>Make Calls
                        </a>
                    </li>
				<?php
				}//fin if($source=='PhoneSetting')
				?>
				
				</ul>
                </div>
                </div>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>

</div>
<script>
	
	$('.list a').bind('click', function ()
		{
			$('.videoV .active').removeClass('active');
			$(this).addClass('active');
			window.open('http://www.youtube.com/embed/'+$(this).attr('rel')+'?rel=0','Video Help','width=720,height=500');
			 winVideoHelp.destroy();
			winVideoHelp.close();
			//$('#player').attr('src','<?php echo $url_youtube; ?>'+$(this).attr('rel')+'<?php echo '?rel=0&origin=http://www.reifax.com/';?>');
			//flowplayer("player", "http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf");
		}
	)
			//flowplayer("player", "http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf");
	
</script>