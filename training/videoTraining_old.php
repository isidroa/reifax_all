<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	$video=$_GET['video'];
	$arrayVideos=array(
						array('ruta'=>'','nombre'=>''),
						array('ruta'=>'http://66.232.103.138/Videos/REIfax%20Definitions.flv','nombre'=>'REIFax Definitions'),
				  		array('ruta'=>'http://66.232.103.138/Videos/Basic%20Training.flv','nombre'=>'Basic Training'),
						array('ruta'=>'http://66.232.103.138/Videos/Save%20Searches.flv','nombre'=>'Save Searches'),
						array('ruta'=>'http://66.232.103.138/Videos/AdvancedSearch.flv','nombre'=>'Advanced Search'),
						array('ruta'=>'http://66.232.103.138/Videos/MobileHomes.flv','nombre'=>'Mobile Homes'),
						array('ruta'=>'http://66.232.103.138/Videos/Results.flv','nombre'=>'Results'),
						array('ruta'=>'http://66.232.103.138/Videos/Overview%20Training.flv','nombre'=>'Overview Training'),
						array('ruta'=>'http://66.232.103.138/Videos/PropertyAnalisysReport.flv','nombre'=>'Property Analysis Report'),
						array('ruta'=>'http://66.232.103.138/Videos/Discount%20Report.flv','nombre'=>'Discount Report'),
						array('ruta'=>'http://66.232.103.138/Videos/X-ray.flv','nombre'=>'X-Ray Report'),
						array('ruta'=>'http://66.232.103.138/Videos/Templates.flv','nombre'=>'Templates'),
						array('ruta'=>'http://66.232.103.138/Videos/BPO%20Report.flv','nombre'=>'BPO Report'),
						array('ruta'=>'http://66.232.103.138/Videos/Blocking%20Function.mp4','nombre'=>'Blocking Function'),
						array('ruta'=>'http://66.232.103.138/Videos/Calculating%20Offer%20Price.mp4','nombre'=>'Calculating Offer Price'),
						array('ruta'=>'http://66.232.103.138/Videos/Create%20Templates.mp4','nombre'=>'Create Templates'),
						array('ruta'=>'http://66.232.103.138/Videos/Creating%20Contracts.mp4','nombre'=>'Creating Contracts'),
						array('ruta'=>'http://66.232.103.138/Videos/Delete%20Print%20and%20Export.mp4','nombre'=>'Delete, Print and Export'),
						array('ruta'=>'http://66.232.103.138/Videos/Filter%20Properties.mp4','nombre'=>'Filter Properties'),
						array('ruta'=>'http://66.232.103.138/Videos/Follow%20Up%20Selling.mp4','nombre'=>'Follow Up Selling'),
						array('ruta'=>'http://66.232.103.138/Videos/Follow%20up%20Several%20Properties.mp4','nombre'=>'Follow up Several Properties'),
						array('ruta'=>'http://66.232.103.138/Videos/Follow%20Up%20Une%20Property.mp4','nombre'=>'Follow Up a Property'),
						array('ruta'=>'http://66.232.103.138/Videos/Getting%20Contact%20Info.mp4','nombre'=>'Getting Contact Info'),
						array('ruta'=>'http://66.232.103.138/Videos/How%20se%20Fetch%20emails.mp4','nombre'=>'How to Fetch emails'),
						array('ruta'=>'http://66.232.103.138/Videos/Import%20Properties.mp4','nombre'=>'Import Properties'),
						array('ruta'=>'http://66.232.103.138/Videos/Mailing%20Campaing.mp4','nombre'=>'Mailing Campaign'),
						array('ruta'=>'http://66.232.103.138/Videos/Offer%20Price%20Using%20Freddy.mp4','nombre'=>'Offer Price Using Freddy'),
						array('ruta'=>'http://66.232.103.138/Videos/Open%20a%20google%20email.mp4','nombre'=>'Open a google email'),
						array('ruta'=>'http://66.232.103.138/Videos/Open%20Google%20Voice%20account.mp4','nombre'=>'Open Google Voice account'),
						array('ruta'=>'http://66.232.103.138/Videos/Overbiew%20Button.mp4','nombre'=>'Overview Button'),
						array('ruta'=>'http://66.232.103.138/Videos/Send%20a%20Fax.mp4','nombre'=>'Send a Fax'),
						array('ruta'=>'http://66.232.103.138/Videos/Send%20Contracts.mp4','nombre'=>'Send Contracts'),
						array('ruta'=>'http://66.232.103.138/Videos/Send%20Emails.mp4','nombre'=>'Send Emails'),
						array('ruta'=>'http://66.232.103.138/Videos/Send%20SMS.mp4','nombre'=>'Send SMS'),
						array('ruta'=>'http://66.232.103.138/Videos/HowToSetUpTheDialerSystemAndMakeCalls.mp4','nombre'=>'Make Calls'),
						array('ruta'=>'http://66.232.103.138/Videos/Setup%20Follow%20Up.mp4','nombre'=>'Setup Follow Up'),
						array('ruta'=>'http://66.232.103.138/Videos/Shedule%20Task.mp4','nombre'=>'Schedule Task')
				  );
	
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
       <a href="overviewTraining.php"><span class="bluetext underline">Training</span></a> &gt; 
        	<span class="fuchsiatext">Training Videos</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Training Videos - Learn how to use our products at your own pace</span>
            	</div>
        	</div>
        </div>
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>Select a training video below on the left column to get started!</div>
                </div>
                <div></div>
                <div id="list" class="list">
                    <ul>
                        <?php
                            for($j=1;$j<count($arrayVideos);$j++){
							    $class= ($j%2==0) ? 'par':'impar';
								?>
                                    <li class="<?php echo $class; ?>">
                                        <a id="<?php echo 'video'.$j; ?>" href="#" rel="<?php echo $arrayVideos[$j]['ruta']; ?>">
                                            <div class="cuote"></div><?php echo $arrayVideos[$j]['nombre']; ?>
                                        </a>
                                    </li>
                                <?php
                            }
                        ?>
                    </ul>
                </div>
                <div class="marcoVideo">
                <a  
                     href="#"
                     style="display:block;width:720px;height:500px"  
                     id="player"> 
                </a> 
                </div>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	$('.list a').bind('click', function ()
		{
			$('.video .active').removeClass('active');
			$(this).addClass('active');
			$('#player').attr('href',$(this).attr('rel'));
			flowplayer("player", "http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf");
		}
	)
	menuClick('menu-training');
	$('#<?php echo 'video'.$video?>').click();
</script>