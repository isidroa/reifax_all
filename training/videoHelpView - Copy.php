<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
  	<div id="contentPrincipal">
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>Select a video below on the left column to get started!</div>
                </div>
                <div></div>
                <div id="list" class="list">
                <ul>
				<?php
				if($_POST['source']=='BasicSearch')
				{
					$firstVideo='http://66.232.103.138/Videos/Basic%20Training.flv';
				?>
                	<li class="par">
                    	<a href="#" rel="http://66.232.103.138/Videos/Basic%20Training.flv">
                        	<div class="cuote"></div>Basic Search 
                        </a>
                    </li>
                    <li class="impar">
                    	<a href="#" rel="http://66.232.103.138/Videos/Save%20Searches.flv">
                        	<div class="cuote"></div>Save Searches
                        </a>
                    </li>
				<?php
				}//fin if($_POST['source']=='BasicSearch')
				
				if($_POST['source']=='AdvancedSearch')
				{
					$firstVideo='http://66.232.103.138/Videos/AdvancedSearch.flv';
				?>
                	<li class="par">
                    	<a href="#" rel="http://66.232.103.138/Videos/AdvancedSearch.flv">
                        	<div class="cuote"></div>Advanced Search 
                        </a>
                    </li>
					<li class="impar">
                    	<a href="#" rel="http://66.232.103.138/Videos/Save%20Searches.flv">
                        	<div class="cuote"></div>Save Searches
                        </a>
                    </li>
				<?php
				}//fin if($_POST['source']=='AdvancedSearch')
				?>
                </ul>
                </div>
                <div class="marcoVideo">
                <!--
				<a  
                     href="#"
                     style="display:block;width:720px;height:500px"  
                     id="player"> 
                </a> 
				-->
					<a id="player" style="display:block;width:720px;height:500px" href="<?php echo $firstVideo ?>">
						<object width="100%" height="100%" type="application/x-shockwave-flash" data="http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf" id="player_api">
							<param value="true" name="allowfullscreen">
							<param value="always" name="allowscriptaccess">
							<param value="high" name="quality">
							<param value="false" name="cachebusting">
							<param value="#000000" name="bgcolor">
							<param value='config={	"playerId":"player","clip":{"url":"<?php echo $firstVideo ?>"},"playlist":[{"url":"<?php echo $firstVideo ?>"}]}' name="flashvars">
						</object>
					</a>					 
                </div>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>

</div>

</body>

</html>
<script language="javascript">
	$('.list a').bind('click', function ()
		{
			$('.video .active').removeClass('active');
			$(this).addClass('active');
			$('#player').attr('href',$(this).attr('rel'));
			flowplayer("player", "http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf");
		}
	)
	
</script>