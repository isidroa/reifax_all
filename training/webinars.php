<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	function conversor_segundos($seg_ini) {
		$horas = floor($seg_ini/3600);
		$minutos = floor(($seg_ini-($horas*3600))/60);
		$minutos =strlen($minutos)==1?'0'.$minutos:$minutos;
		$segundos = $seg_ini-($horas*3600)-($minutos*60);
		$segundos =strlen($segundos)==1?'0'.$segundos:$segundos;
		return $horas.':'.$minutos.':'.$segundos;
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'These recorded Webinars will get you started in the right direction','Real estate, reifax, florida, florida real estate, webinars');?>
<style type="text/css">

.video .list {
	width:100%;
}
.list a{
	position:relative;
	height: 50px !important;
	padding:0 !important;
	width: 100% !important;
	border-bottom:solid 1px #777;
}
.list div{
	float:left;
}

.list .texto{
	line-height: 14px;
	margin-top:14px;
	position:relative;
}
.list strong{
	bottom:-8px;
	right:2px;
	position: absolute;
}
.list img{
	width:70px;
	padding:2px;
	background:#f2f2f2;
	color:aaa;
	margin:2px;
	margin-top:10px;
}
.title h6{
	font-weight: 100;
    line-height: 8px;
    margin-top: 0;
	padding:0;
}
.video .title{
	height:40px;
}

</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Webinars</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Webinars - These recorded Webinars will get you started in the right direction</span>
            	</div>
        	</div>
        </div>
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>
                    	Select a webinars below on the left column to get started! 
                        <!--
                        <h6 id="issuesVideo">
                        	Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue.
                        </h6>
                        //-->
                    </div>
                </div>
                <div></div>
                <div id="list" class="list">
                <ul>
                <?php
					$sql="SELECT * FROM `webinar` order by id desc limit 60;";
					$result=mysql_query($sql);
					while($tem = mysql_fetch_assoc($result)){
						$time=conversor_segundos($tem['length']);
						echo "
							<li >
								<a href='#'  rel='{$tem['idYoutube']}'>
		                        	<div class='cuote'></div>
									<div class='texto'>
										{$tem['name']}
									</div>
									<strong>$time</strong>
								</a> 
							</li>
						";
					} 
				?>
                </ul>
                </div>
                <!--
                <div class="marcoVideo">
                <a  
                     href="#"
                     style="display:block;width:720px;height:500px"  
                     id="player"> 
                </a> 
                </div>
                //-->
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	var userAgent = navigator.userAgent.toLowerCase();
	jQuery.browser = {
		version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
		chrome: /chrome/.test( userAgent ),
		safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),
		opera: /opera/.test( userAgent ),
		msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
		mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
	};
		
	$(document).ready(function(e) {
		$('.list li:even').addClass('par');
		$('.list li:odd').addClass('impar');
		/*
        $('.list li > a').each(function (index){
			var aux=$($.templaBoton);
			var type=$(this).attr('data-type');
			var aux2=$(this).attr('rel');
			aux2=aux2.split('/');
			var down=aux2[aux2.length-1];
			delete aux2[aux2.length-1];
			down= aux2.join('/')+'download.php?file='+down;
			if(type=='s'){
				$('.download',aux).remove();
				$('.view',aux).attr('rel',$(this).attr('rel')).attr('data-formato',$(this).attr('data-formato'));
			}
			else if(type=='d'){
				$('.download',aux).attr('href',down);
				$('.view',aux).remove();
			}
			else{
				$('.download',aux).attr('href',down);
				$('.view',aux).attr('rel',$(this).attr('rel')).attr('data-formato',$(this).attr('data-formato'));
			}
			$(this).append(aux);
		}).bind('mouseenter',function (){
			$(this).stop(false,true).animate({height:'70px'},'fast');
			$(this).stop(false,true).find('.actions').fadeIn('fast');
		}).bind('mouseleave',function (){
			$(this).find('.actions').hide();
			$(this).animate({height:'40px'},'fast')
		});
		//Ejecutamos las condiciones si el fichero existe o no.
		jQuery.each(jQuery.browser, function(i, val) {
			if($.browser.mozilla){
				$('#issuesVideo').html('Trouble to watch the video? Please install the \'plugin\' for your browser from <a href="http://www.interoperabilitybridges.com/windows-media-player-firefox-plugin-download" target="_blank" class="underline">here</a> and the codec fron <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a>; to solve the problem');
			}
			else if($.browser.opera){
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
			else if($.browser.safari){
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
			else if($.browser.chrome){
				$('#issuesVideo').html('Trouble to watch the video? Please install the \'plugin\' for your browser from <a href="http://www.interoperabilitybridges.com/wmp-extension-for-chrome" target="_blank" class="underline">here</a> and the codec fron <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a>; to solve the problem ');
			}
			else{
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
		});
		*/
		
		
		$('.list a').bind('click', function (e)
			{
				/*
				e.preventDefault();
				$('.marcoVideo').empty();
				$('.video .active').removeClass('active');
				$(this).addClass('active');
				var tem=$($.templateFlv);
				$(tem).attr('src',$(this).attr('rel'));
				$('.marcoVideo').html(tem);
				*/
				
				window.open('http://www.youtube.com/embed/'+$(this).attr('rel')+'?rel=0',$('.texto',this).html(),'width=720,height=500');
			}
		)
    });
	
	$.templaBoton=['<div class="actions">',
			'<a href="#" title="Download webinar" class="download" target="_blank"></a>',
			'<a href="#" title="See webinar" class="view"></a>',
			'<div>'
		].join('');
	$.templateWmv=['<object id="MediaPlayer" width=320 height=286 classid="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" standby="Loading Windows Media Player components..." type="application/x-oleobject" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112">',
		
		'<param name="filename" value="">',
		'<param name="Showcontrols" value="True">',
		'<param name="autoStart" value="True">',
		'<param name="wmode" value="transparent">',
		
		'<embed type="application/x-mplayer2" src="http://tudominio.com/tuvideo.wmv" name="MediaPlayer" autoStart="True" wmode="transparent" width="720" height="500" ></embed>',
		
		'</object>'].join('');
	$.templateFlv=['<iframe width="100%" height="100%" src="#" frameborder="0" allowfullscreen></iframe>' ].join('');
	menuClick('menu-training');
</script>