<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	function conversor_segundos($seg_ini) {
		$horas = floor($seg_ini/3600);
		$minutos = floor(($seg_ini-($horas*3600))/60);
		$minutos =strlen($minutos)==1?'0'.$minutos:$minutos;
		$segundos = $seg_ini-($horas*3600)-($minutos*60);
		$segundos =strlen($segundos)==1?'0'.$segundos:$segundos;
		return $horas.':'.$minutos.':'.$segundos;
	}
	$video=$_GET['video'];
	/*
	$arrayVideos=array(
						array('ruta'=>'','nombre'=>''),
						array('ruta'=>'XcoMHGFgQR4','nombre'=>'REIFax Definitions'),
				  		array('ruta'=>'vQj1MXYj3vk','nombre'=>'Basic Training'),
						array('ruta'=>'IFAaMnOjflo','nombre'=>'Save Searches'),
						array('ruta'=>'FYuLFzw03ew','nombre'=>'Advanced Search'),
						array('ruta'=>'mxYjxucpQE4','nombre'=>'Mobile Homes'),
						array('ruta'=>'YCOzsWxKEi4','nombre'=>'Results'),
						array('ruta'=>'GcNGJFh7SK0','nombre'=>'Overview Training'),
						array('ruta'=>'kQsFxBwMQtE','nombre'=>'Property Analysis Report'),
						array('ruta'=>'lwredU6KcIQ','nombre'=>'Discount Report'),
						array('ruta'=>'XaYT5z6OLdw','nombre'=>'X-Ray Report'),
						array('ruta'=>'TNlsdLAOLc8','nombre'=>'Templates'),
						array('ruta'=>'I1NxNv3ZfcA','nombre'=>'BPO Report'),
						array('ruta'=>'SRUA6bzppLk','nombre'=>'Blocking Function'),
						array('ruta'=>'injxfLMDETI','nombre'=>'Calculating Offer Price'),
						array('ruta'=>'leQVvA6Dwr4','nombre'=>'Create Templates'),
						array('ruta'=>'roEtfXjFNMw','nombre'=>'Creating Contracts'),
						array('ruta'=>'K_zdoA3wQYQ','nombre'=>'Delete, Print and Export'),
						array('ruta'=>'9i6qZJ9SSyg','nombre'=>'Filter Properties'),
						array('ruta'=>'6At-awymQ0Y','nombre'=>'Follow Up Selling'),
						array('ruta'=>'7hx7nKCJQxM','nombre'=>'Follow up Several Properties'),
						array('ruta'=>'UjSzKE54XAs','nombre'=>'Follow Up a Property'),
						array('ruta'=>'6PeZ7Noil4Q','nombre'=>'Getting Contact Info'),
						array('ruta'=>'9iGrmVH7smU','nombre'=>'How to Fetch emails'),
						array('ruta'=>'6aZeEQ_ZLNk','nombre'=>'Import Properties'),
						array('ruta'=>'rp7cFD-s9sM','nombre'=>'Mailing Campaign'),
						array('ruta'=>'o0QM4tRDhkE','nombre'=>'Offer Price Using Freddy'),
						array('ruta'=>'m9DzgXnbPQQ','nombre'=>'Open a google email'),
						array('ruta'=>'ajg6JZnXRog','nombre'=>'Open Google Voice account'),
						array('ruta'=>'1RaiAeqF2rs','nombre'=>'Overview Button'),
						array('ruta'=>'GzwcTNAbf-I','nombre'=>'Send a Fax'),
						array('ruta'=>'GG1DXhtQOgQ','nombre'=>'Send Contracts'),
						array('ruta'=>'Z3NX-heJ64g','nombre'=>'Send Emails'),
						array('ruta'=>'GEhwAlMH5cE','nombre'=>'Send SMS'),
						array('ruta'=>'feY_uBQREFw','nombre'=>'Make Calls'),
						array('ruta'=>'8NOguR4Zz-4','nombre'=>'Setup Follow Up'),
						array('ruta'=>'rhyc3z2Hy_I','nombre'=>'Schedule Task'),
						array('ruta'=>'AuN14VV9MCA','nombre'=>'How to add an escrow letter'),
						array('ruta'=>'TcJnYEn-xrc','nombre'=>'How to upload a contract'),
						array('ruta'=>'RchrOY28sug','nombre'=>'How to set up your email'),
						array('ruta'=>'aOHJsNTdl5Q','nombre'=>'How to set up your google voice number'),
						array('ruta'=>'sjPSdUJ6A8Q','nombre'=>'How to set up your gmail account'),
						array('ruta'=>'5YvaVhXgoyg','nombre'=>'How to set up your mail templates'),
						array('ruta'=>'bU7ZLQ7C7GA','nombre'=>'How to set up a mail merge template'),
						array('ruta'=>'EUW5bbXSpDw','nombre'=>'How to set up an sms template'),
						array('ruta'=>'_Zf8q-gw8pE','nombre'=>'How to set up a fax template'),
						array('ruta'=>'YJsq9bwt7Ws','nombre'=>'How to send a contract from the overview tab'),
						array('ruta'=>'rHeqgNw6bGE','nombre'=>'How to modify a contract'),
						array('ruta'=>'2oKCUohNWxQ','nombre'=>'How to add the buyer name'),
						array('ruta'=>'gTcV2e9GRbg','nombre'=>'How to delete a contract'),
						array('ruta'=>'xGG4awB7a_c','nombre'=>'How to upload an additional document?'),
						array('ruta'=>'wAOmuRoISPM','nombre'=>'How to modify an additional document'),
						array('ruta'=>'RomJnHz9OTo','nombre'=>'How to delete an additional document'),
						array('ruta'=>'wiT994u03Vc','nombre'=>'How to edit signatures and initials?'),
						array('ruta'=>'rybl-9MJ6g0','nombre'=>'How to delete signatures and initials?'),
						array('ruta'=>'NNqlDvOCH2g','nombre'=>'How to convert your pdf signatures and initials to jpeg?')
						
				  );
				  */
	
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Learn how to use our products at your own pace','Real estate, training videos, products, reifax');?>
<style type="text/css">

.video .list {
	width:100%;
}
.list a{
	position:relative;
	height: 50px !important;
	padding:0 !important;
	width: 100% !important;
	border-bottom:solid 1px #777;
}
.list div{
	float:left;
}

.list .texto{
	line-height: 14px;
	margin-top:14px;
	position:relative;
}
.list strong{
	bottom:-8px;
	right:2px;
	position: absolute;
}
.list img{
	width:70px;
	padding:2px;
	background:#f2f2f2;
	color:aaa;
	margin:2px;
	margin-top:10px;
}
.title h6{
	font-weight: 100;
    line-height: 8px;
    margin-top: 0;
	padding:0;
}
.video .title{
	height:40px;
}

</style>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
       
        	<span class="fuchsiatext">Training Videos</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Training Videos - Learn how to use our products at your own pace</span>
            	</div>
        	</div>
        </div>
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>Select a training video below to get started!</div>
                </div>
                <div></div>
                <div id="list" class="list">
                    <ul>
                        <?php
                        $sql="SELECT v.* FROM xima.videos_training v 
								left join  xima.training_assignment_video m on v.id=m.id_video
								where v.status=5
								group by v.id order by id_modulo, pos;";
						$result=mysql_query($sql);
						while($tem = mysql_fetch_assoc($result)){
							$time=conversor_segundos($tem['length']);
							echo "
								<li >
									<a href='#'  rel='{$tem['idYoutube']}'>
										<div class='cuote'></div>
										<div class='texto'>
											{$tem['name']}
										</div>
										<strong>$time</strong>
									</a>
								</li>
							";
						} 
                          /*  for($j=1;$j<count($arrayVideos);$j++){
							    $class= ($j%2==0) ? 'par':'impar';
								?>
                                    <li class="<?php echo $class; ?>">
                                        <a id="<?php echo 'video'.$j; ?>" href="javascript:void(0)" rel="<?php echo $arrayVideos[$j]['ruta']; ?>" style="width: 550px;">
                                            <div class="cuote"></div><?php echo $arrayVideos[$j]['nombre']; ?>
                                        </a>
                                    </li>
                                <?php
                            }
							*/
                        ?>
                    </ul>
                </div>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	$('.list a').bind('click', function (e)
		{
			/*
			e.preventDefault();
			$('.marcoVideo').empty();
			$('.video .active').removeClass('active');
			$(this).addClass('active');
			var tem=$($.templateFlv);
			$(tem).attr('src',$(this).attr('rel'));
			$('.marcoVideo').html(tem);
			*/
			
			window.open('http://www.youtube.com/embed/'+$(this).attr('rel')+'?rel=0',$('.texto',this).html(),'width=720,height=500');
		}
	)
	menuClick('menu-training');
	$('#<?php echo 'video'.$video?>').click();
</script>