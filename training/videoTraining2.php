<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	$video=$_GET['video'];
	$arrayVideos=array(
						array('ruta'=>'','nombre'=>''),
						array('ruta'=>'http://www.youtube.com/embed/XcoMHGFgQR4?rel=0','nombre'=>'REIFax Definitions'),
				  		array('ruta'=>'http://www.youtube.com/embed/vQj1MXYj3vk?rel=0','nombre'=>'Basic Training'),
						array('ruta'=>'http://www.youtube.com/embed/IFAaMnOjflo?rel=0','nombre'=>'Save Searches'),
						array('ruta'=>'http://www.youtube.com/embed/FYuLFzw03ew?rel=0','nombre'=>'Advanced Search'),
						array('ruta'=>'http://www.youtube.com/embed/mxYjxucpQE4?rel=0','nombre'=>'Mobile Homes'),
						array('ruta'=>'http://www.youtube.com/embed/YCOzsWxKEi4?rel=0','nombre'=>'Results'),
						array('ruta'=>'http://www.youtube.com/embed/GcNGJFh7SK0?rel=0','nombre'=>'Overview Training'),
						array('ruta'=>'http://www.youtube.com/embed/kQsFxBwMQtE?rel=0','nombre'=>'Property Analysis Report'),
						array('ruta'=>'http://www.youtube.com/embed/lwredU6KcIQ?rel=0','nombre'=>'Discount Report'),
						array('ruta'=>'http://www.youtube.com/embed/XaYT5z6OLdw?rel=0','nombre'=>'X-Ray Report'),
						array('ruta'=>'http://www.youtube.com/embed/TNlsdLAOLc8?rel=0','nombre'=>'Templates'),
						array('ruta'=>'http://www.youtube.com/embed/I1NxNv3ZfcA?rel=0','nombre'=>'BPO Report'),
						array('ruta'=>'http://www.youtube.com/embed/SRUA6bzppLk?rel=0','nombre'=>'Blocking Function'),
						array('ruta'=>'http://www.youtube.com/embed/injxfLMDETI?rel=0','nombre'=>'Calculating Offer Price'),
						array('ruta'=>'http://www.youtube.com/embed/leQVvA6Dwr4?rel=0','nombre'=>'Create Templates'),
						array('ruta'=>'http://www.youtube.com/embed/roEtfXjFNMw?rel=0','nombre'=>'Creating Contracts'),
						array('ruta'=>'http://www.youtube.com/embed/K_zdoA3wQYQ?rel=0','nombre'=>'Delete, Print and Export'),
						array('ruta'=>'http://www.youtube.com/embed/9i6qZJ9SSyg?rel=0','nombre'=>'Filter Properties'),
						array('ruta'=>'http://www.youtube.com/embed/6At-awymQ0Y?rel=0','nombre'=>'Follow Up Selling'),
						array('ruta'=>'http://www.youtube.com/embed/7hx7nKCJQxM?rel=0','nombre'=>'Follow up Several Properties'),
						array('ruta'=>'http://www.youtube.com/embed/UjSzKE54XAs?rel=0','nombre'=>'Follow Up a Property'),
						array('ruta'=>'http://www.youtube.com/embed/6PeZ7Noil4Q?rel=0','nombre'=>'Getting Contact Info'),
						array('ruta'=>'http://www.youtube.com/embed/9iGrmVH7smU?rel=0','nombre'=>'How to Fetch emails'),
						array('ruta'=>'http://www.youtube.com/embed/6aZeEQ_ZLNk?rel=0','nombre'=>'Import Properties'),
						array('ruta'=>'http://www.youtube.com/embed/rp7cFD-s9sM?rel=0','nombre'=>'Mailing Campaign'),
						array('ruta'=>'http://www.youtube.com/embed/UHWIi2adROU?rel=0','nombre'=>'Offer Price Using Freddy'),
						array('ruta'=>'http://www.youtube.com/embed/m9DzgXnbPQQ?rel=0','nombre'=>'Open a google email'),
						array('ruta'=>'http://www.youtube.com/embed/ajg6JZnXRog?rel=0','nombre'=>'Open Google Voice account'),
						array('ruta'=>'http://www.youtube.com/embed/1RaiAeqF2rs?rel=0','nombre'=>'Overview Button'),
						array('ruta'=>'http://www.youtube.com/embed/GzwcTNAbf-I?rel=0','nombre'=>'Send a Fax'),
						array('ruta'=>'http://www.youtube.com/embed/GG1DXhtQOgQ?rel=0','nombre'=>'Send Contracts'),
						array('ruta'=>'http://www.youtube.com/embed/Z3NX-heJ64g?rel=0','nombre'=>'Send Emails'),
						array('ruta'=>'http://www.youtube.com/embed/GEhwAlMH5cE?rel=0','nombre'=>'Send SMS'),
						array('ruta'=>'http://www.youtube.com/embed/feY_uBQREFw?rel=0','nombre'=>'Make Calls'),
						array('ruta'=>'http://www.youtube.com/embed/8NOguR4Zz-4?rel=0','nombre'=>'Setup Follow Up'),
						array('ruta'=>'http://www.youtube.com/embed/rhyc3z2Hy_I?rel=0','nombre'=>'Schedule Task')
				  );
	
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
       <a href="overviewTraining.php"><span class="bluetext underline">Training</span></a> &gt; 
        	<span class="fuchsiatext">Training Videos</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Training Videos - Learn how to use our products at your own pace</span>
            	</div>
        	</div>
        </div>
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>Select a training video below on the left column to get started!</div>
                </div>
                <div></div>
                <div id="list" class="list">
                    <ul>
                        <?php
                            for($j=1;$j<count($arrayVideos);$j++){
							    $class= ($j%2==0) ? 'par':'impar';
								?>
                                    <li class="<?php echo $class; ?>">
                                        <a id="<?php echo 'video'.$j; ?>" href="#" rel="<?php echo $arrayVideos[$j]['ruta']; ?>">
                                            <div class="cuote"></div><?php echo $arrayVideos[$j]['nombre']; ?>
                                        </a>
                                    </li>
                                <?php
                            }
                        ?>
                    </ul>
                </div>
                <iframe class="marcoVideo" frameborder="0">

                </iframe>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	$('.list a').bind('click', function ()
		{
			$('.video .active').removeClass('active');
			$(this).addClass('active');
			window.open($(this).attr('rel'),'Reifax Training','width=720,height=500');
			//$('.marcoVideo').attr('src',$(this).attr('rel'));
			//$('#player').attr('href',$(this).attr('rel'));
			//flowplayer("player", "http://www.reifax.com/resources/swf/flowplayer-3.2.7.swf");
		}
	)
	menuClick('menu-training');
	$('#<?php echo 'video'.$video?>').click();
</script>