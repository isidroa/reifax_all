<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>
<style type="text/css">
.list .texto{
	white-space:nowrap;
	overflow:hidden;    
	line-height: 40px;
	text-overflow: ellipsis;
}
.list li:hover .texto {
	text-overflow:inherit;
	overflow:visible;  
	line-height: 20px;
	white-space:normal;
}
.title h6{
	font-weight: 100;
    line-height: 8px;
    margin-top: 0;
	padding:0;
}
.video .title{
	height:60px;
}
</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
       <a href="overviewTraining.php"><span class="bluetext underline">Training</span></a> &gt; 
        	<span class="fuchsiatext">Webinars</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Webinars - These recorded Webinars will get you started in the right direction</span>
            	</div>
        	</div>
        </div>
    		<div class="panel">
           	 	<div class="center video"> 
            	<div class="title">
                	<div>
                    	Select a webinars below on the left column to get started!
                        <h6 id="issuesVideo">
                        	Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue.
                        </h6>
                    </div>
                </div>
                <div></div>
                <div id="list" class="list">
                <ul>
                <?php
					$sql="";
				?>
				<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-09-06.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-09-06 New Features In My Emails</div>
						</a>
                    </li>
				<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/SKYPECALLINGREIFAX.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">SKYPE CALLING IN REIFAX</div>
						</a>
                    </li>

				<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-08-23.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-08-23 Sending Contracts With the Follow up </div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-08-16.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-08-16 Comparables </div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-08-09.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-08-09 follow up emails</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-08-02.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-08-02 follow up history</div>
						</a>
                    </li>	
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-07-12.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-07-12 Changing Account Information</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-06-21.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-06-21 Reifax Products Review</div>
						</a>
                    </li>
					<li >
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-06-14.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-06-14 X-Ray/Discount Report</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-06-07.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-06-07 Set Up Follow Up</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-05-24.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-05-24 Mailing Campign/Mail Merge</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-05-17.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-05-17 Templates</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-05-10.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-05-10 leads generator/Save search</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-05-03.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-05-03 follow up</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-04-19.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-04-19 follow up system</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-04-05.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-04-05 overview</div>
						</a>
                    </li> 
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-03-22.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-03-22 basicad/vanced result</div>
						</a>
                    </li> 
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-03-08.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-03-08 Basic and Advanced Search</div>
						</a>
                    </li> 
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-03-01 contratos - templates.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-03-01 contracts - templates</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-02-22 Tasks - Tickets.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-02-22 Tasks - Tickets</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-02-08 Reports.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-02-08 Reports</div>
						</a>
                    </li>	
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-02-01 contracts.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-02-01 contracts</div>
						</a>
                    </li>				
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-01-25 comparables.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-01-25 comparables</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-01-18 cashbuyersfollow up.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-01-18 cashbuyersfollow up</div>
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2013-01-11  searchresult.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2013-01-11  searchresult</div>
						</a>
                    </li>				
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2012-12-14.wmv">
                        	<div class="cuote"></div>
                            <div class="texto">2012-12-14 Result advanced templates</div>
						</a>
                    </li>
					
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2012-12-07.wmv">
                        	<div class="cuote"></div>2012-12-07 Intelligent Market
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-11-16escrowlette.wmv">
                        	<div class="cuote"></div>2012-11-16 Escrow letter
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' data-type='d' rel="http://66.232.103.138/videos/webinars/2012-11-02 search by map.wmv">
                        	<div class="cuote"></div>2012-11-02 search by map
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2012-10-26.wmv">
                        	<div class="cuote"></div>2012-10-26 follow list
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv'  rel="http://66.232.103.138/videos/webinars/2012-10-19.wmv">
                        	<div class="cuote"></div>2012-10-19 cash buyer
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' data-type='d' rel="http://66.232.103.138/videos/webinars/2012-10-05.wmv">
                        	<div class="cuote"></div>2012-10-05
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' data-type='d' rel="http://66.232.103.138/videos/webinars/2012-09-14Setup.wmv">
                        	<div class="cuote"></div>2012-09-14 Setup
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-08-31.wmv">
                        	<div class="cuote"></div>2012-08-31 Contracts Maths
						</a>
                    </li>
					<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-08-17.wmv">
                        	<div class="cuote"></div>2012-08-17 Intelligent Market
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012 08 10 Leverage reports.mp4">
                        	<div class="cuote"></div>2012-08-10 Leverage reports
                        </a>
                    </li> 
                	<li >
                    	<a href="#" data-type='d'  data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-08-03Search.wmv">
                        	<div class="cuote"></div>2012-08-03 Search
                        </a>
                    </li> 
                	<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-07-20.wmv">
                        	<div class="cuote"></div>2012-07-20
                        </a>
                    </li> 
                	<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-07-13Searches.wmv">
                        	<div class="cuote"></div>2012-07-13 Searches
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-07-06.flv">
                        	<div class="cuote"></div>2012-07-06 mailing campaign
                        </a>
                    </li> 
                	<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-06-29.wmv">
                        	<div class="cuote"></div>2012-06-29
                        </a>
                    </li> 
                	<li >
                    	<a href="#" data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-06-22.wmv">
                        	<div class="cuote"></div>2012-06-22
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-05-31.flv">
                        	<div class="cuote"></div>2012-05-31
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-05-18.flv">
                        	<div class="cuote"></div>2012-05-18
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-05-11.flv">
                        	<div class="cuote"></div>2012-05-11
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-05-01.flv">
                        	<div class="cuote"></div>2012-05-01
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-04-20.flv">
                        	<div class="cuote"></div>2012-04-20
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-04-13.flv">
                        	<div class="cuote"></div>2012-04-13
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-04-05.flv">
                        	<div class="cuote"></div>2012-04-05
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-03-30.flv">
                        	<div class="cuote"></div>2012-03-30
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-03-23.flv">
                        	<div class="cuote"></div>2012-03-23
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-03-16.flv">
                        	<div class="cuote"></div>2012-03-16
                        </a>
                    </li> 
                	<li >
                    	<a href="#"  data-type='d'  data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-03-09.wmv">
                        	<div class="cuote"></div>2012-03-09
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-03-02.flv">
                        	<div class="cuote"></div>2012-03-02
                        </a>
                    </li> 
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-02-24.flv">
                        	<div class="cuote"></div>2012-02-24
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-02-09.flv">
                        	<div class="cuote"></div>2012-02-09
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-02-03.flv">
                        	<div class="cuote"></div>2012-02-03
                        </a>
                    </li>
                	<li >
                    	<a href="#" data-type='d'  data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-01-27.wmv">
                        	<div class="cuote"></div>2012-01-27
                        </a>
                    </li>
                	<li >
                    	<a href="#" data-type='d'  data-formato='wmv' rel="http://66.232.103.138/videos/webinars/2012-01-20.wmv">
                        	<div class="cuote"></div>2012-01-20
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2012-01-06.flv">
                        	<div class="cuote"></div>2012-01-06
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-12-22.flv">
                        	<div class="cuote"></div>2011-12-22
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-12-16.flv">
                        	<div class="cuote"></div>2011-12-16
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-12-02.flv">
                        	<div class="cuote"></div>2011-12-02 
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-11-18.flv">
                        	<div class="cuote"></div>2011-11-18 
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-11-11.flv">
                        	<div class="cuote"></div>2011-11-11
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-11-04.flv">
                        	<div class="cuote"></div>2011-11-04 
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-10-28.flv">
                        	<div class="cuote"></div>2011-10-28 
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-10-21.flv">
                        	<div class="cuote"></div>2011-10-21
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-10-14.flv">
                        	<div class="cuote"></div>2011-10-14 
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-10-07.flv">
                        	<div class="cuote"></div>2011-10-07
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-09-30.flv">
                        	<div class="cuote"></div>2011-09-30 
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-09-23.flv">
                        	<div class="cuote"></div>2011-09-23 
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-09-16.flv">
                        	<div class="cuote"></div>2011-09-16 
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-09-09.flv">
                        	<div class="cuote"></div>2011-09-09 
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-09-02.flv">
                        	<div class="cuote"></div>2011-09-02
                        </a>
                    </li>
                	<li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-08-26.flv">
                        	<div class="cuote"></div>2011-08-26
                        </a>
                    </li>
                    <li >
                    	<a href="#" rel="http://66.232.103.138/videos/webinars/2011-08-19.flv">
                        	<div class="cuote"></div>2011-08-19 
                        </a>
                    </li>
                </ul>
                </div>
                <div class="marcoVideo">
                <a  
                     href="#"
                     style="display:block;width:720px;height:500px"  
                     id="player"> 
                </a> 
                </div>
                <div class="clearEmpty"></div>
            	</div>
        	</div>
	</div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	var userAgent = navigator.userAgent.toLowerCase();
	jQuery.browser = {
		version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
		chrome: /chrome/.test( userAgent ),
		safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),
		opera: /opera/.test( userAgent ),
		msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
		mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
	};
		
	$(document).ready(function(e) {
		$('.list li:even').addClass('par');
		$('.list li:odd').addClass('impar');
        $('.list li > a').each(function (index){
			var aux=$($.templaBoton);
			var type=$(this).attr('data-type');
			var aux2=$(this).attr('rel');
			aux2=aux2.split('/');
			var down=aux2[aux2.length-1];
			delete aux2[aux2.length-1];
			down= aux2.join('/')+'download.php?file='+down;
			if(type=='s'){
				$('.download',aux).remove();
				$('.view',aux).attr('rel',$(this).attr('rel')).attr('data-formato',$(this).attr('data-formato'));
			}
			else if(type=='d'){
				$('.download',aux).attr('href',down);
				$('.view',aux).remove();
			}
			else{
				$('.download',aux).attr('href',down);
				$('.view',aux).attr('rel',$(this).attr('rel')).attr('data-formato',$(this).attr('data-formato'));
			}
			$(this).append(aux);
		}).bind('mouseenter',function (){
			$(this).stop(false,true).animate({height:'70px'},'fast');
			$(this).stop(false,true).find('.actions').fadeIn('fast');
		}).bind('mouseleave',function (){
			$(this).find('.actions').hide();
			$(this).animate({height:'40px'},'fast')
		});
		
		//Ejecutamos las condiciones si el fichero existe o no.
		jQuery.each(jQuery.browser, function(i, val) {
			if($.browser.mozilla){
				$('#issuesVideo').html('Trouble to watch the video? Please install the \'plugin\' for your browser from <a href="http://www.interoperabilitybridges.com/windows-media-player-firefox-plugin-download" target="_blank" class="underline">here</a> and the codec fron <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a>; to solve the problem');
			}
			else if($.browser.opera){
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
			else if($.browser.safari){
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
			else if($.browser.chrome){
				$('#issuesVideo').html('Trouble to watch the video? Please install the \'plugin\' for your browser from <a href="http://www.interoperabilitybridges.com/wmp-extension-for-chrome" target="_blank" class="underline">here</a> and the codec fron <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a>; to solve the problem ');
			}
			else{
				$('#issuesVideo').html('Problems watching the webinars? please download and install the codec from <a href="https://www1.gotomeeting.com/codec?Portal=gotomeeting.com" target="_blank" class="underline">here</a> to solve the issue');
			}
		});
		
		$('.list .view').bind('click', function (e)
			{
				e.preventDefault();
				$('.marcoVideo').html('');
				$('.video .active').removeClass('active');
				$(this).parent().parent().addClass('active');
				/*$('embed,param[name=filename]').attr('src',$(this).attr('rel'));*/
				if($(this).attr('data-formato')=='wmv'){
					var tem=$($.templateWmv);
					$('param[name=filname]',tem).attr('value',$(this).attr('rel'));
					$('embed',tem).attr('src',$(this).attr('rel'));
					tem.appendTo('.marcoVideo');
				}
				else{
					var tem=$($.templateFlv);
					$(tem).attr('href',$(this).attr('rel'));
					tem.appendTo('.marcoVideo');
					flowplayer("player", "/resources/swf/flowplayer-3.2.7.swf");
				}
			}
		)
    });
	
	$.templaBoton=['<div class="actions">',
			'<a href="#" title="Download webinar" class="download" target="_blank"></a>',
			'<a href="#" title="See webinar" class="view"></a>',
			'<div>'
		].join('');
	$.templateWmv=['<object id="MediaPlayer" width=320 height=286 classid="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" standby="Loading Windows Media Player components..." type="application/x-oleobject" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112">',
		
		'<param name="filename" value="">',
		'<param name="Showcontrols" value="True">',
		'<param name="autoStart" value="True">',
		'<param name="wmode" value="transparent">',
		
		'<embed type="application/x-mplayer2" src="http://tudominio.com/tuvideo.wmv" name="MediaPlayer" autoStart="True" wmode="transparent" width="720" height="500" ></embed>',
		
		'</object>'].join('');
	$.templateFlv=['<a  ',
                     'href="#"',
                     'style="display:block;width:720px;height:500px" ',
                     'id="player"> ',
                '</a>' ].join('');
	menuClick('menu-training');
</script>