<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Learn with the help from our experts','Real Estate, florida, reifax, florida real estate, Training overview, Learn how to use our products, reifax');?>

<body>

<div	 class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Training</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="https://www.reifax.com/register/registerCoaching.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D52.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Training Overview - Learn with the help from our experts</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs">
                	<h4>REIFax Live Workshop and Training</h4>
                        <div>
                         <p>We offer frequent live training sessions with the flexibility to meet the needs of both beginners and experienced users.</p>
                        <p>Training is available at our locations through our open enrollment sessions. Our next live training sessions are listed below.</p>
                        <p>
                        Join us in the FREE LIVE Intensive hands-on training coming soon, where you will get the opportunity to learn everything you need to know about all the powerful features included in our new product REIFax Professional, and how to effectively use them to generate Powerful Marketing Campaigns</p>
                        <p>
                        	The real money in real estate investing is not in making lots of offers or finding deals but it is in following up on those offers or deals. In this Live Training you will learn:
                        </p>
                        <ul><li>
                        	<div class="cuote"></div>How to effectively and easily follow up on every offer you made.</li><li>
                        	<div class="cuote"></div>How to effectively manage all your daily tasks.</li><li>
                        	<div class="cuote"></div>How to deliver rich, engaging and effective email campaigns.</li><li>
                        	<div class="cuote"></div>How to easily manage, store and find contact information, such as names, addresses and telephone numbers of your contacts and leads.</span></li><li>
                        	<div class="cuote"></div>How to manage all parties involved in a short sale transaction and generate short sale packages in less than 10 seconds automatically</span></li><li>
                        	<div class="cuote"></div>and so much more...</li><li>
                        </ul>

                        <p>We will have Questions and Answers Sessions for each topic we cover, so come prepared with your questions. Bring any deals you are working on right now, and we may be able to thoroughly analyze them as part of the class.</p>
						<p>Our commitment to you is to give you all the information you need, so that when you leave the room, you will be an expert using REIFax Professional, which will help you succeed in your Real Estate Business.</p>
                        <!--
						<p>Below are the details about the Event:</p>
						<p>When: Wednesday, November 30th, 2011, 10 am - 12:30 pm, (Doors open at 9:45 am).</p>
						<p>Where: Total Wine & More at North Miami (Meeting Room)</p>
						<p>Address: 14750 Biscayne Boulevard, North Miami, FL 33181</p>
						<p>Ticket: <span class="bold">Free </span> (breakfast is included).</p>
                        <div class="centertext">
						<p class="bold">
                        <span class="redtext">EARLY BIRD SPECIAL</span><br>
                        Register before March 23rd<br>
                        and pay <span style="background:#FF0">Only $57.00</span> per person<br>

                        </p>
						<p class="bold">
                        <span class="redtext">Very Limited Space!!!<br>
                        Only 40 Seats Available</span>
                        <br>
                        <span style="background:#FF0">(This workshop will Sell-Out Fast - Don't Wait)</span><br>

                        </p>
                        </div>
                        //-->
						<p>For registration and more information please call Customer Service at <span class="bold">(888) 349-5368</span></p>
	                	<h4>REIFax Webinars</h4>
                        <p>We conduct a Webinar training every Friday at 10:00 am Eastern Time. If you wish to receive webinar reminder emails
                        </p><p>We record all Friday Webinar trainings, so you can access the Webinar Trainings Archive 24/7 at   <a href="http://www.reifax.com/training/webinars.php" class="bluetext">http://www.reifax.com/training/webinars.php</a>
                        </p>
                        <h4>REIFax One-On-One Coaching Program</h4>
                        <p>Now you can learn how to effectively use REIFax products with Private Coaching Sessions for only $77 per hour or $249 for 4 hours.</p>
                        <p>
                        </p>
                        <p>To subscribe for a coaching <a href="https://www.reifax.com/register/registerCoaching.php" class="bluetext">"click here"</a> and more information please call Customer Service at <span class="bold">(888) 349-5368</span></p>

     					</div> 
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-training');
</script>