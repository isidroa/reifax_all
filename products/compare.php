<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/conexion.php');
	require_once('../resources/php/properties_conexion.php');
	if($_COOKIE['datos_usr'] && !$_COOKIE['suspended'])
	{
		$sql="SELECT homefinder, buyerspro, leadsgenerator, residential, platinum, professional, professional_esp FROM permission where userid=".$_COOKIE['datos_usr']['USERID'].";";
		$respuesta=$conex->query($sql);
		$status=$respuesta->fetch_assoc();
	}
	if($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1)
	{
		if($status['professional']==1 || $status['professional_esp']==1)
		{
			$boton[3]='<a href="http://professional.reifax.com" class="buttongreen">Access</a><div></div>';
			$boton[1]=$boton[2]='';
		}
		else if($status['platinum']==1)
		{
			$boton[3]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[2]='<a href="http://platinum.reifax.com" style="margin-left:-30%" class="buttongreen">Access</a><div></div>';
			$boton[1]='';
		}
			$boton[7]='<a href=" http://dealfinder.reifax.com" class="buttongreen">Access</a>';
			$boton[6]='<a href="http://buyerspro.reifax.com" class="access buttongreen">Access</a><div></div>';
			$boton[8]='<a href="http://leads.reifax.com" class="access buttongreen">Access</a><div></div>';
	}
	else {
		if($status['residential']==1)
		{
			$boton[3]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[2]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[1]='<a href="http://residential.reifax.com" class="buttongreen">Access</a><div></div>';
			$boton[1]='';
		}
		else
		{
			$boton[3]='<a href="https://www.reifax.com/register/customerRegister.php?gp=3" class="buttongreen">Buy Now</a><div></div>';
			$boton[2]='<a href="https://www.reifax.com/register/customerRegister.php?gp=2" class="buttongreen">Buy Now</a><div></div>';
			$boton[1]='<a href="https://www.reifax.com/register/customerRegister.php?gp=1" class="buttongreen">Buy Now</a><div></div>';
		}
			$boton[7]=($status['homefinder']==1)?
				'<a href=" http://dealfinder.reifax.com" class="buttongreen">Access</a>':
				'<a href="https://www.reifax.com/register/customerRegister.php?gp=7" class="buttongreen">Buy Now</a><div></div>';
			$boton[6]=($status['buyerspro']==1)?
				'<a href="http://buyerspro.reifax.com" class="buttongreen">Access</a><div></div>':
				'<a href="https://www.reifax.com/register/customerRegister.php?gp=6" class="buttongreen">Buy Now</a><div></div>';
			$boton[8]=($status['leadsgenerator']==1)?
				'<a href="http://home.reifax.com" class="buttongreen">Access</a><div></div>':
				'<a href="https://www.reifax.com/register/customerRegister.php?gp=8" class="buttongreen">Buy Now</a><div></div>';
	}
	function printNames($products,$boton)
	{
		foreach($products as $indice)
			{
				$style = intval($indice['id'])==3 ? 'style="background-color:#005c83; color:#FFF;"' : '';
				echo '<th '.$style.' class="details"><div>'.$indice['name']./*$boton[$indice['id']].*/'</div></th>';
			}
	}
	$sql='SELECT idproducto, name_compare FROM xima.usr_producto where idproducto in (1,2,3,6,7,8) ORDER BY orden_compare;';
	$tem=$conex->query($sql);
	$products=array();
	while($tem2=$tem->fetch_assoc())
	{
		$products[]=array(id =>$tem2['idproducto'],name =>$tem2['name_compare']);
	}
	//print_r($products);
	$tem=$tem2=0;
	$sql='SELECT idusr_features,idproducto,activo FROM usr_producto_features;';
	$tem=$conex->query($sql);
	while($tem2=$tem->fetch_assoc())
	{
		$features[$tem2['idusr_features']][$tem2['idproducto']]=$tem2['activo'];
	}
	
	$sql='SELECT p.idproducto id,cp.precio,cp.countCounty
			FROM xima.usr_producto p 
			INNER JOIN xima.usr_productobase pb ON (p.idproducto=pb.idproducto)
			INNER JOIN xima.f_countyprecio cp ON (pb.idproductobase=cp.idproductobase)
			WHERE pb.activo=1 
			AND cp.idstate=1
			ORDER BY p.orden,cp.countCounty';
	$query=$conex->query($sql);
	$price=array();
	while($resul=$query->fetch_assoc())
	{
		if($resul['countCounty']==1)
			$price[$resul['id']]['one']=$resul['precio'];
		else
			$price[$resul['id']]['all']=$resul['precio'];	
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Find the right product to help you grow your business','Real Estate, Florida, Reifax, florida real estate, task manager, public record search, contact manager, market intelligence, leads generator, contract generator,  properties search, search for rent, follow-up system, residential property search, rental comparables, property analysis report, cash buyer leads, discount report, life estates, buyer leads statistics, customizable comparables, properties by potential equity, lis pendens, short sale manager, property information search, foreclosure listings, short sales, foreclosure properties');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Pricing & Product Comparison</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Pricing & Product Comparison</span>
            	</div>
        	</div>
        </div>
    <table class="tableCompare">
    	<tr>
        	<th class="feature">
            	Plan Type
            </th>
            <?php
				printNames($products,$boton);
			?>
        </tr>
        <tr>
        	<td colspan="<?php echo count($products) +1;?>" style="height:8px; padding:0"></td>
        </tr>
        <tr class="par">
        	<td style="padding-left:10px;" class="bold">One County (Monthly Plan)</td>
            <?php 
				foreach($products as $indice){
					$style = intval($indice['id'])==3 ? 'style="background-color:#9cc55f; color:#FFF;"' : '';
					$priceProduct = $price[$indice['id']]['one'];
					if(intval($indice['id'])==2) $priceProduct += $price[1]['one'];
					if(intval($indice['id'])==3) $priceProduct += $price[1]['one'] + $price[2]['one'];
					
					echo '<td '.$style.' class="bold">$'.number_format($priceProduct,0).'<input value="gp='.$indice['id'].'&gf=1&gs=1&gc=1" type="radio" name="'.$indice['id'].'" id="residential-one-couty"></td>';
				}
			?>
        </tr>
        <tr class="impar">
        	<td style="padding-left:10px;" class="bold">One County (Annual Plan - 2 months free)</td>
            <?php 
				foreach($products as $indice){
					$style = intval($indice['id'])==3 ? 'style="background-color:#9cc55f; color:#FFF;"' : '';
					$priceProduct = $price[$indice['id']]['one'];
					if(intval($indice['id'])==2) $priceProduct += $price[1]['one'];
					if(intval($indice['id'])==3) $priceProduct += $price[1]['one'] + $price[2]['one'];
					
					echo '<td '.$style.' class="bold">$'.number_format($priceProduct*10,0).'<input value="gp='.$indice['id'].'&gf=2&gs=1&gc=1" type="radio" name="'.$indice['id'].'" id="residential-one-couty"></td>';
				}
			?>
        </tr>
        <tr>
        	<td colspan="<?php echo count($products) +1;?>" style="height:8px; padding:0"></td>
        </tr>
        <tr class="par">
        	<td style="padding-left:10px;" class="bold">All Counties (Monthly Plan)</td>
            <?php 
				foreach($products as $indice){
					$style = intval($indice['id'])==3 ? 'style="background-color:#9cc55f; color:#FFF;"' : '';
					$priceProduct = $price[$indice['id']]['all'];
					if(intval($indice['id'])==2) $priceProduct += $price[1]['all'];
					if(intval($indice['id'])==3) $priceProduct += $price[1]['all'] + $price[2]['all'];
					
					echo '<td '.$style.' class="bold">$'.number_format($priceProduct,0).'<input value="gp='.$indice['id'].'&gf=1&gs=1&gc=ALL" type="radio" name="'.$indice['id'].'" id="residential-one-couty"></td>';
				}
			?>
        </tr>
        <tr class="impar">
        	<td style="padding-left:10px;" class="bold">All Counties (Annual Plan - 2 months free)</td>
            <?php 
				foreach($products as $indice){
					$style = intval($indice['id'])==3 ? 'style="background-color:#9cc55f; color:#FFF;"' : '';
					$priceProduct = $price[$indice['id']]['all'];
					if(intval($indice['id'])==2) $priceProduct += $price[1]['all'];
					if(intval($indice['id'])==3) $priceProduct += $price[1]['all'] + $price[2]['all'];
					
					echo '<td '.$style.' class="bold">$'.number_format($priceProduct*10,0).'<input value="gp='.$indice['id'].'&gf=2&gs=1&gc=ALL" type="radio" name="'.$indice['id'].'" id="residential-one-couty"></td>';
				}
			?>
        </tr>
        <tr>
        	<td colspan="<?php echo count($products) +1;?>" style="height:8px; padding:0"></td>
        </tr>
        <tr class="par" id="signupbuttons">
        	<td></td>
            <?php 
				foreach($products as $indice){
					$style = intval($indice['id'])==3 ? 'buttonblue' : 'buttongreen';
					echo '<td style="text-align: center;"><a href="https://www.reifax.com/register/customerRegister.php?gp='.$indice['id'].'&gf=1&gs=1&gc=ALL" rel="'.$indice['id'].'" class="'.$style.'">Sign Up</a></td>';
				}
			?>
        </tr>
    </table>
    <div class="clear"></div>
    <table>
  	<?php
	$sql='SELECT f.idusr_features, f.feature, g.title FROM usr_features f, usr_groupfeatures g where f.idusr_groupfeatures=g.idusr_groupfeatures order by g.orden, f.orden;';
	$groups=$conex->query($sql);
	while($group=$groups->fetch_assoc())
	{
		if(($featureGroup!=$group['title']) || ($featureGroup==''))
		{	
			$featureGroup=$group['title'];
			echo '</table><div class="featureGroup bold">'.$featureGroup.'</div><table class="tableCompare">';
			$i=0;
		}
		echo ($i%2==0)?'<tr class="par"><td class="feature"> 	<div class="cuote">&nbsp;</div>'.$group['feature'].'</td>':'<tr class="impar"><td class="feature">	<div class="cuote">&nbsp;</div>'.$group['feature'].'</td>';
		foreach($products as $indice)
		{
			 echo ($features[$group['idusr_features']][$indice['id']])? '<td class="details active"></td>' : '<td class="details inactive"></td>';
		}
		$i++;
	}
	?>
    </tr></table>
    </div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	<?php if(isset($_GET['pricing'])){?>
	menuClick('menu-store');
	<?php }else{?>
	menuClick('menu-products');
	<?php }?>
	$('input[type="radio"]').change(function (){
		$("#signupbuttons a").each(function (index){
			var complemento= $("input[name="+$(this).attr('rel')+"]:checked").val();
				
			$(this).attr('href',$(this).attr('href').split('?')[0]+'?'+complemento);
		});
	}).attr('checked', false);
</script>