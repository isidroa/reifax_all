<?php 
	require_once('../../resources/template/template.php');
	require_once('../../resources/php/properties_conexion.php');
	conectar('xima');
	require_once('../../resources/php/buttonsProdutcs.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Powerful tool to find properties homes for sale by potential equity','Real Estate, Florida, reifax, florida real estate,  properties in foreclosure, foreclosure listings, short sales, foreclosure properties, properties  By Potential Equity, Mapable Results,  Residential Property Search, search properties with equity, homes for sale by potential equity, Home deal finder, Mortgage Search,  Public Record Search','Home Deal Finder is a powerful tool that enables you to search for residential properties for sale by Potential Equity in just minutes. ');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:        <a href="../../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../compare.php"><span class="bluetext underline">Products</span></a> &gt; <span class="fuchsiatext">Home Deal Finder</span>
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                   <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
        </div>
        
        <!-- Center Content-->
        <div class="content">
            <!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(../../resources/img/banners/C3.png) no-repeat;">
                                    <!--
                                        <div class="advertisingFeatureBlock">
                                            <h2 class="bluetext"><span class="greentext">Home</span> Deal Finder</h2>
                                            <p class="bluetext">
                                                Powerful tool to find properties homes for sale by potential equity   
                                            </p>
                                        </div>
                                        //-->
                                        <div class="button left">
                                            <?php echo $boton[7]; ?>
                                       	</div>
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext"><span class="greentext">Home</span> Deal Finder - Powerful tool to find properties homes for sale by potential equity  </span>
            	</div>
        	</div>
        </div>
            <!-- Submenu Of Products -->
            <nav id="subMenu-products">
            	<ul>
                	<li>
                    	<a href="#"><span>Overview</span></a><div></div>
                    </li>
                    <li>
                    	<a href="#"><span>What&acute;s New</span></a><div></div>
                    </li>
                    <li class="active">
                    	<a href="#"><span>Sample &amp; Demos</span></a><div></div>
                    </li>
                    <li class="last">
                    	<a href="#"><span>Testimonials</span></a><div></div>
                    </li>
                </ul>
            </nav>
            <!-- Product details pane -->
            <div id="contentProductDetails" class="panel">
            	<div class="center"> 
                	<div id="contentOverview">
                    <p class="maintitle bold bluetext"><span class="greentext">Home</span> Deal Finder Overview</p>
                        <p>
                        Home Deal Finder is a powerful tool that enables you to search for residential properties for sale by Potential Equity, exclusive REIFax feature, which gives you the power to find properties at the lowest price on market, in just minutes.
                        </p>
                    <p class="title bold bluetext">Top Features Included in <span class="greentext">Home</span> Deal Finder</p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Type of Property Searches Allowed:</span> Residential properties.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Potential Equity:</span> With this tool you can easily search for residential properties currently on the market with 10%, 20%, 30% or more Potential Equity in just minutes. Home Deal Finder calculates the potential equity by comparing property market value (market value is determined by calculating the median price of the properties within 0.5 miles, with +/- 10% square feet, sold in the last 12 months) with the asking price. The formula is:
                            <div class="centertext">
                            <p class="bold"><br>Potential Equity = (Market Value – Listing Price) / Market Value * 100%</p>
                            </div>
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">GPS Mobile Search:</span> This search capability was designed to help you find residential property deals with a specific search criteria while in a neighborhood, using the GPS capabilities to locate leads. All properties matching your search criteria will be displayed on a map in the smart device. This feature works with Blackberry, Apple and Android phones.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Map:</span> With this tool you can easily search for properties by simply drawing the shape or the area you wish to search in the map. Properties inside that area will be included in the results.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search Manager:</span> This feature enables you to save searches for future use, so when you'll need to make the same search again you won't have to select the same search criteria.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Mapable Results:</span> This feature enables you to see your search results in a map.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Print:</span> This feature enables you to print your search results.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Export Data to Excel &amp; PDF:</span> This feature enables you to easily export your search results to an Excel or PDF document.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Mobile Application for Smart Phones:</span> This search capability was designed to help you find and analyze residential property deals on-the-spot, while in a neighborhood. Take your walking-for-dollars business to the level next with our state-of-the-art Mobile Application. This feature works with Blackberry, Apple and Android phones.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Compatibility with Internet Browsers:</span> Home Deal Finder is compatible with all internet browsers.
                        </li>
                    </ul>
                    <p>
                    	For more information about Home Deal Finder and other products, please click on <a class="bluetext underline" href="http://www.reifax.com/products/compare.php">compare products.</a>
                    </p>
                    <div class="clear"></div>
                    
                    </div>
                	<div id="contentNew"></div>
                	<div id="contentSample"></div>
                	<div id="contentTestimonials"></div>
            	</div>
            </div>
            
        <div class="clear">&nbsp;</div> 
    </div>
  
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-products');
</script>