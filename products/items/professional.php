<?php 
	require_once('../../resources/template/template.php');
	require_once('../../resources/php/properties_conexion.php');
	conectar('xima');
	require_once('../../resources/php/buttonsProdutcs.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'The most powerful tool to find and manage real estate deals','Real Estate, Florida, reifax, florida real estate, properties in foreclosure, foreclosure listings, short sales, foreclosure properties, commercial properties, vacant land, Mortgage Search, Public Record Search, Search for Rent, Shadow Inventory Calculator, search by map homes for sale, Mailing Campaign Manager, Short Sale Manager, Search By Map, Contact Manager, Properties Search, Follow-Up System','Our top-of-the-line product is packed with features for real estate investors, agents, short sale specialists and many other real estate professionals. It\'s the most advanced and complete REIFax product.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:        <a href="../../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../compare.php"><span class="bluetext underline">Products</span></a> &gt; <span class="fuchsiatext">REIFax Professional</span>
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                   <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
        </div>
        
        <!-- Center Content-->
        <div class="content">
            <!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(../../resources/img/products/P3.png) no-repeat;">
                                    <!--
                                        <div class="advertisingFeatureBlock">
                                            <h2 class="bluetext"><span class="greentext">REI</span>Fax Professional</h2>
                                            <p class="bluetext">
                                                The most powerful tool to find and manage real estate deals 
                                            </p>
                                        </div>
                                        //-->
                                        <div class="button right">
                                            <?php echo $boton[3]; ?>
                                       	</div>
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext"><span class="greentext">REI</span>Fax Professional - The most powerful tool to find and manage real estate deals </span>
            	</div>
        	</div>
        </div>
            <!-- Submenu Of Products -->
            <nav id="subMenu-products">
            	<ul>
                	<li>
                    	<a href="#"><span>Overview</span></a><div></div>
                    </li>
                    <li>
                    	<a href="#"><span>What&acute;s New</span></a><div></div>
                    </li>
                    <li class="active">
                    	<a href="#"><span>Sample &amp; Demos</span></a><div></div>
                    </li>
                    <li class="last">
                    	<a href="#"><span>Testimonials</span></a><div></div>
                    </li>
                </ul>
            </nav>
            <!-- Product details pane -->
            <div id="contentProductDetails" class="panel">
            	<div class="center"> 
                	<div id="contentOverview">
                    	<p class="maintitle bold bluetext"><span class="greentext">REI</span>Fax Professional Overview</p>
                        <p>
                        Our top-of-the-line product is packed with features for real estate investors, agents, short sale specialists and many other real estate professionals. It's the most advanced and complete REIFax product. It was designed for those Real Estate Professionals who need the best tool to search for real estate opportunities in single family homes, condos/townhomes/villas, multi-family with 10 + or - units, commercial properties, vacant land, mobile homes and other properties.
                        </p>
                        <p>
                        REIFax Professional comes with the best built-in sorting capabilities to get <span class="bold">real</span> selling and buying leads and the best reports to help you take your real estate investing business to the next level.
                        </p>
                    	<p class="title bold bluetext">Top Features Included in <span class="greentext">REI</span>Fax Professional</p>
                        <ul>
                        	<li><div class="cuote"></div><span class="bold">Type of Property Searches Allowed:</span> All type of properties.</li>
                        	<li><div class="cuote"></div><span class="bold">REIFax Residential and Platinum Features:</span> REIFax Professional includes all features built-in in REIFax Residential and REIFax Platinum.</li>
                        	<li><div class="cuote"></div><span class="bold">GPS Mobile Search:</span> This search capability was designed to help you find property deals with a specific search criteria while in a neighborhood, using the GPS capabilities to locate leads. All properties matching your search criteria will be displayed on a map in the smart device. This feature works with Blackberry, Apple and Android phones.</li>
                        	<li><div class="cuote"></div><span class="bold">Search By Map:</span> With this tool you can easily search for properties by drawing the shape or the area you wish to search in the map. Only the properties inside that area will be included in the results.</li>
                        	<li><div class="cuote"></div><span class="bold">Search Manager:</span>This feature enables you to save searches for future use, so when you'll need to make the same search again you won't have to select the same search criteria
.</li>
                        	<li><div class="cuote"></div><span class="bold">Mobile Application for Smart Phones:</span> This search capability was designed to help you find and analyze property deals on-the-spot, while in a neighborhood. Take your walking-for-dollars business to the level next with our state-of-the-art Mobile Application. This feature works with Blackberry, Apple and Android phones.</li>
                        	<li><div class="cuote"></div><span class="bold">Contact Manager:</span> This features enables you to easily manage store and find contact information, such as names, addresses and telephone numbers of your contacts and leads.</li>
                        	<li><div class="cuote"></div><span class="bold">Task Manager:</span> This feature enables you to manage all your daily tasks. You can set due dates on your tasks and get overview over what needs to get done today, tomorrow or next week.</li>
                        	<li><div class="cuote"></div><span class="bold">Mailing Campaign Manager:</span> This feature enables you to deliver rich, engaging and effective email campaigns. Mailing Campaign Manager has the necessary features to help you meet all your email campaign needs.</li>
                        	<li><div class="cuote"></div><span class="bold">Short Sale Manager:</span>This feature enables you to manage all parties involved in a short sale transaction and generate short sale packages in less than 10 seconds automatically.</li>
                        </ul>
                        <p>
                        For more information and other features included in REIFax Professional, please click on <a class="bluetext underline" href="http://www.reifax.com/products/compare.php">compare products.</a>
                        </p>
                    <div class="clear"></div>
                    
                    </div>
                	<div id="contentNew"></div>
                	<div id="contentSample"></div>
                	<div id="contentTestimonials"></div>
            	</div>
            </div>
            
        <div class="clear">&nbsp;</div> 
    </div>
  
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-products');
</script>