<?php require_once('../../resources/template/template.php');
	require_once('../../resources/php/properties_conexion.php');
	conectar('xima');
	require_once('../../resources/php/buttonsProdutcs.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Find the best selling and buying leads in just seconds','Real Estate, Florida, properties in foreclosure, foreclosure listings, short sales, foreclosure properties, commercial properties in florida, vacant land, Mortgage Search,public records search florida, Search for Rent, house for rent in florida, shadow inventory calculator, Life Estates, Probates, find properties for selling in florida, Rental Comparables, Contract Generator, Properties Search, Search By Life Estates, Buyer Leads Statistics','REIFax Platinum was designed for those Real Estate Professionals who need a powerful tool to help them search for any type of properties with the sorting capabilities needed to get the best selling and buying leads and the best reports to amaze their customers and get the deals close fast.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:        <a href="../../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../compare.php"><span class="bluetext underline">Products</span></a> &gt; <span class="fuchsiatext">REIFax Platinum</span>
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                   <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
        </div>
        
        <!-- Center Content-->
        <div class="content">
            <!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(../../resources/img/products/P2.png) no-repeat;">
                                    <!--
                                        <div class="advertisingFeatureBlock">
                                            <h2 class="bluetext"><span class="greentext">REI</span>Fax Platinum</h2>
                                            <p class="bluetext">
                                                Find the best selling and buying leads in just minutes 
                                            </p>
                                        </div>//-->
                                        <div class="button right">
                                            <?php echo $boton[2]; ?>
                                       	</div>
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext"><span class="greentext">REI</span>Fax Platinum - Find the best selling and buying leads in just minutes</span>
            	</div>
        	</div>
        </div>
            <!-- Submenu Of Products -->
            <nav id="subMenu-products">
            	<ul>
                	<li>
                    	<a href="#"><span>Overview</span></a><div></div>
                    </li>
                    <li>
                    	<a href="#"><span>What&acute;s New</span></a><div></div>
                    </li>
                    <li class="active">
                    	<a href="#"><span>Sample &amp; Demos</span></a><div></div>
                    </li>
                    <li class="last">
                    	<a href="#"><span>Testimonials</span></a><div></div>
                    </li>
                </ul>
            </nav>
            <!-- Product details pane -->
            <div id="contentProductDetails" class="panel">
            	<div class="center"> 
                	<div id="contentOverview">
                    <p class="maintitle bold bluetext"><span class="greentext">REI</span>Fax Platinum</p>
                    <p>
                    	REIFax Platinum was designed for those Real Estate Professionals who need a powerful tool to help them search for any type of properties with the sorting capabilities needed to get the best selling and buying leads and the best reports to amaze their customers and get the deals close fast. With REIFax Platinum you may search for single family homes, condos/townhomes/villas, multi-family with +/- 10 units, commercial properties, vacant land, mobile homes and other properties.
                    </p>
                    <p class="title bold bluetext">
                    	Top Features Included  in <span class="greentext">Rei</span>Fax Platinum
                    </p>
                    <ul>
                    
                    	<li>
                        	<div class="cuote"></div><span class="bold">Property Type Searches Allowed:</span> All type of properties.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">REIFax Residential Features:</span> REIFax Platinum includes all features built-in in REIFax Residential.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Mortgage Search (where available):</span> This feature enables you to search property mortgage information recorded in public records.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Public Record Search:</span> This feature enables you to search property public record information.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search for Rent:</span> This feature enables you to search for residential properties for rent.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Shadow Inventory Calculator:</span> This feature enables you to search for foreclosed properties held by lenders (Real Estate Owned - REO).
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Life Estates and Probates:</span> This features enables you to search for any property by life estates and probates.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Market Intelligence:</span> This feature enables you to find cash buyers in any area that buy the type of properties you sell. You get all the contact information of cash buyers, property holders, out-of-state and international buyers and property holders.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Rental Comparables:</span> This feature enables you to obtain rental comparables of a subject property.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Contract Generator:</span> This feature enables you to automatically fill in any real estate contract or form, including addendums and public information required. A PDF document will be generated in less than ten seconds.
                        </li>
                    </ul>
                    <p>
                    	For other features included in REIFax Platinum, please click on <a class="bluetext underline" href="http://www.reifax.com/products/compare.php">compare products.</a>
                    </p>
                    <div class="clear"></div>
                    </div>
                	<div id="contentNew"></div>
                	<div id="contentSample"></div>
                	<div id="contentTestimonials"></div>
            	</div>
            </div>
            
        <div class="clear">&nbsp;</div> 
    </div>
  
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-products');
</script>