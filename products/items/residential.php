<?php 
	require_once('../../resources/template/template.php');
	require_once('../../resources/php/properties_conexion.php');
	conectar('xima');
	require_once('../../resources/php/buttonsProdutcs.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Find the best residential property deal in just seconds','Real Estate, Florida, reifax, florida real estate, find properties with equity in florida, Lis Pendens in florida,  pre foreclosed properties sold in florida, Property Analysis Report,  properties active for sale in florida,  equity residential properties in florida,Real Estate, Florida, Reifax, properties by potential equity, lis pendens listings florida , Distressed Comparables, Property Analysis Report, X-Ray Report,  Residential Property Search, Search by Lis pendens in florida, Customizable Comparables, properties in foreclosure, foreclosure listings, short sales, foreclosure properties','REIFax Residential is the most powerful tool that enables you to search for residential properties by Potential Equity in just minutes.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="../compare.php"><span class="bluetext underline">Products</span></a> &gt; 
        <span class="fuchsiatext">REIFax Residential</span>
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                  <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
        </div>
        
        <!-- Center Content-->
        <div class="content">
            <!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(../../resources/img/products/P1.png) no-repeat;">
                                        <!-- 
                                        <div class="advertisingFeatureBlock">
                                            <h2 class="bluetext"><span class="greentext">REI</span>Fax Residential</h2>
                                            <p class="bluetext">
                                                Find the best residential property deal in just minutes 
                                            </p>
                                        </div>//-->
                                        <div class="button left">
                                            <?php echo $boton[1]; ?>
                                       	</div>
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext"><span class="greentext">REI</span>Fax Residential - Find the best residential property deal in just minutes </span>
            	</div>
        	</div>
        </div>
            <!-- Submenu Of Products -->
            <nav id="subMenu-products">
            	<ul>
                	<li>
                    	<a href="#"><span>Overview</span></a><div></div>
                    </li>
                    <li>
                    	<a href="#"><span>What&acute;s New</span></a><div></div>
                    </li>
                    <li class="active">
                    	<a href="#"><span>Sample &amp; Demos</span></a><div></div>
                    </li>
                    <li class="last">
                    	<a href="#"><span>Testimonials</span></a><div></div>
                    </li>
                </ul>
            </nav>
            <!-- Product details pane -->
            <div id="contentProductDetails" class="panel">
            	<div class="center"> 
                	<div id="contentOverview">
                    	<p class="maintitle bold bluetext"><span class="greentext">REI</span>Fax Residential Overview</p>
                        <p>
                        REIFax Residential is the most powerful tool that enables you to search for residential properties by Potential Equity, exclusive REIFax feature, which gives you the power to find properties at the lowest price on market, in just minutes.                        </p>
                        <p>
                        REIFax Residential was specifically designed for those Real Estate Professionals who need a powerful tool to help them search for residential properties with the sorting capabilities needed to get the best selling and buying leads and the best reports to amaze their customers and get the deals close fast.                        </p>
                        <p class="title bold bluetext">Top Features Included in <span class="greentext">REI</span>Fax Residential</p>
                        <ul>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Type of Property Searches Allowed:</span> Residential properties.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Potential Equity:</span> With this tool you can easily search for properties currently on the market with 10%, 20%, 30% or more Potential Equity in just minutes. REIFax calculates the potential equity by comparing property market value (market value is determined by calculating the median price of the properties within 0.5 miles, with +/- 10% square feet, sold in the last 12 months) with the asking price. The formula is:
                            <div class="centertext">
                            <p class="bold">
                            <br>
                            	Potential Equity = (Market Value – Listing Price) / Market Value * 100%
                            </p>
                            </div>
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Lis Pendens:</span> This feature enables you to search for pre-foreclosed or foreclosed properties. This search capability is specifically designed to help you find clients that are ready and willing to sell their property, mainly in a short sale process.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Search By Debt-to-Equity:</span> With this tool you can easily search for upside down properties with 10%, 20%, 30% or more negative equity in just minutes. REIFax calculates the debt-to-equity by comparing property market value (market value is determined by calculating the median price of the properties within 0.5 miles, with +/- 10% square feet, sold in the last 12 months) with the property total debt recorded on public records. The formula is.
                            <div class="centertext">
                            <p class="bold">
                            <br>
                            	Debt-to-Equity = (Market Value – Total Debt) / Market Value * 100%
                            </p>
                            </div>
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Distressed Comparables:</span> This feature enables you to compare subject property with distressed comparable properties.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Property Analysis Report:</span> This feature provides you with a complete property analysis report which includes property details, customizable comparables, comparables actives (comparables with properties for sale), distressed properties comparables and rental comparables.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Discount Report:</span> This feature enables you to obtain a report showing relevant information about the subject property area as pre-foreclosed properties sold, average days of short sale process, average amount of discount, average percentage of discount, foreclosed properties sold, average days of sale after file date, average days of sale after judgment date (auction), average amount of discount and average percentage of discount.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">X-Ray Report:</span> One of the most useful reports on REIFax, this feature enables you to obtain a report showing all the relevant information and statistics from a neighborhood, as distressed properties, pre-foreclosed properties, foreclosed properties, upside down properties - not pre-foreclosed and not foreclosed, properties active for sale, owner-occupied properties, non owner-occupied properties, properties sold in the last 6 months, months of inventory, average days on market, average sold price in the last 3, 6, 9 and 12 months and median sold price in the last 3, 6, 9 and 12 months.
                        </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">Broker's Price Opinion Report (BPO):</span> This feature enables you to obtain at a glance a report showing two comparables reports, the comparable properties report and the comparable active properties report. Both reports show the subject property compared with 3 comparable properties and the information needed for a broker's price opinion on a specific property.
                        </li>
                        </ul>
                        <p>
                        	For other features included in REIFax Residential, please click on <a class="bluetext underline" href="http://www.reifax.com/products/compare.php">compare products.</a>
                        </p>
                    <div class="clear"></div>
                    </div>
                	<div id="contentNew"></div>
                	<div id="contentSample"></div>
                	<div id="contentTestimonials"></div>
            	</div>
            </div>
            
        <div class="clear">&nbsp;</div> 
    </div>
  
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-products');
</script>