<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="/index.php">
           	<span class="greentext underline">Rei</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="http://www.reifax.com/store/index.php"><span class="bluetext underline">Store</span></a> &gt; 
        	<span class="fuchsiatext">Customer Register</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright" style="height:1608px;">
		<div class="panel">
            <div class="center">  
                &nbsp;
            </div>
        </div>
    </div>
    <!-- Center Content-->
    
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Customer Register - Please Fill Out Fields Below</span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <form>
       		<!--Personal information panel-->
            <div class="panel">
                    <div class="center register">
                        <!--notification SSL-->
                        <div class="notification">
                            <p>This page is secure. <a href="#" id="learnNotificationSSL" class="bluetext underline">Learn more</a></p>
                            <div id="NotificationSSL">
                                This site uses Secure Sockets Layer (SSL) encryption for all customer data and has been authenticated by Go Daddy Secure Certification Authority<br><br>
                                Any information you provide will remain safe, secure, and confidential<br><br>
                                Review our <a class="bluetext underline" href="#">privacy policy</a> to learn how we protect your personal information
                            </div>
                        </div>
                        <!--end notification SSL-->
                       <h2>Product Information</h2>
                           <div class="BoxTextLoudGray bold">
                                Optional - Promotion Code (<a href="#" class="bluetext whatsThis">What&acute;s this?
                                	<div class="notification">
                                			It's the Code that will identify you as a ReiFax Affiliate Partner. It will automatically appear on the Promotion Code field when a customer clicks on one of our affiliate link on your site or in an email (which contains embedded the Promotion Code). Customer may also type it at registration.
                                	</div>
                                </a>)
                           </div>
                           <div class="BoxTextSoftGray">
                                <span>If you have a Promotion Code, enter it in the box to the right </span>
                                <input type="text" class="shortBox mediumMarginLef" id="promotionCode" class="promotionCode">
                           </div>
                           <div id="ProductInformation">
                           		<div class="bold">
                                    Details of your product selection: Total $<span class="totalValue">149</span>
                               </div>
                           <div class="clear"></div>
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Product:
                               </div>
                               <div id="selectedProduct" class="contentDetailsProduct">Reifax Residential</div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Frequency:
                               </div>
                               <div id="selectedFrequency" class="contentDetailsProduct">Monthly</div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>State:
                               </div>
                               <div id="selectedState" class="contentDetailsProduct">Florida</div>
                               
                               <div class="titleDetailsProduct">
                                    <div class="cuote"></div>Couty:
                               </div>
                               <div  id="selectedCounties" class="contentDetailsProduct">All Counties</div>
                           <div class="clear"></div>
                           <div>
                           		You are ordering the <span id="ordering" class="bold">Florida/All Counties Reifax Residential Monthly Plan</span>. The  total amount you are paying today is $<span class="totalValue">149</span>. Your accoun will automaticallu renew at $<span class="totalValue">149</span>/<span id="frequency">month</span> for as long as you remain a member. you can easily upgrade, downgrade, or cancel your account at any time.
                           </div>
                           <div>
                           		If youwish to change your selection, you may do it below:
                           </div>
                           <div class="clear"></div>
                           <div>
                           		<div class="centertext selections">
                                	<label class="required">Product:</label><br>
                                    <select class="shortBox">
                                    	<option>ReiFax Residential</option>
                                    	<option>ReiFax Platinum</option>
                                    	<option>ReiFax Professional</option>
                                    	<option>Buyer&acute;s Pro</option>
                                    	<option>Home Deals Finder</option>
                                    	<option>Leads Generator</option>
                                    </select>
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">Frequency:</label><br>
                                    <select class="shortBox">
                                   		<option>monthly</option>
                                    	<option>Annually</option>
                                    </select>
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">State:</label><br>
                                    <select class="shortBox"></select>
                                </div>
                           		<div class="centertext selections">
                                	<label class="required">County:</label><br>
                                    <select class="shortBox"></select>
                                </div>
                           </div>
                           </div>
                           
                           <div class="clear"></div>
                       </div>
        	</div>
         
          	<div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                        <h2>Customer information</h2>
                        <p>Already have an account? <a class="greentext" href="#">Log In...</a></p> 
                        <p>I am a (an):</p>
                        <div> 
                        	<div class="titleCodLicense"><input type="radio" name="type" value="LicenseEstateSales" class="radioButton"> <span>Real Estate Sales Associate</span></div>
                        	<div class="CodLicense" id="LicenseEstateSales"><span class="required">License # </span><input class="shortBox" type="text"></div>
                         </div>
                        <div id="registerCompany">
                        	<div class="titleCodLicense" style="margin-left:70px; height:50px; width:260px;">
                            		<span>Is your company registered width us?</span>
                                    <div style=" width:80px; padding:5px">
                                    	<input type="radio" name="companyRegister" class="radioButton"> Yes
                                    	<input type="radio" name="companyRegister" class="radioButton" checked> No
                                    </div>
                            </div>
                        	<div class="CodLicense" style="height:50px;" id="divOfficeNumber"><span class="required">Office # </span><input class="shortBox" type="text"></div>
                        </div>
                         <div>
                           <div class="titleCodLicense"><input type="radio" name="type" value="LicenseEstateBroker" class="radioButton"> <span>Real Estate Broker</span></div>
                        	<div class="CodLicense" id="LicenseEstateBroker"><span class="required">License # </span><input class="shortBox" type="text"></div>
                        </div>
                        <div id="registerCompany2">
                        	<div class="titleCodLicense" style="margin-left:70px; height:50px; width:260px;">
                            		<span>Is your company registered width us?</span>
                                    <div style=" width:80px; padding:5px">
                                    	<input type="radio" name="companyRegister2" class="radioButton"> Yes
                                    	<input type="radio" name="companyRegister2" class="radioButton" checked> No
                                    </div>
                            </div>
                        	<div class="CodLicense" style="height:50px;" id="divOfficeNumber2"><span class="required">Office # </span><input class="shortBox" type="text"></div>
                        </div>
                        
                         <div>
                           <div class="titleCodLicense"><input type="radio" name="type" class="radioButton"> <span>Real Estate Investor</span></div>
                        	
                        </div>
                        <div class="clear"></div>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">E-mail Address</label>
                                </td>
                                <td>
                                	<input type="text" name="email">
                                </td>
                                <td>
                                	Your email will be your Username
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type E-mail Address</label>
                                </td>
                                <td>
                                	<input type="text" name="Re-email">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">First Name</label>
                                </td>
                                <td>
                                	<input type="text" name="firstName">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Last Name</label>
                                </td>
                                <td>
                                	<input type="text" name="lastName">
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Password</label>
                                </td>
                                <td>
                                	<input type="password" name="password">
                                </td>
                                <td>
                                	Must be at least 8 characters and contain a letter a number
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Re-type Password</label>
                                </td>
                                <td>
                                	<input type="password" name="re-password">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Phone Number</label>
                                </td>
                                <td class="numberPhone">
                                    <input type="text" name="PhoneNumberCod"> <label>-</label> 
                                    <input type="text" name="PhoneNumber"> <label>Ext</label>
                                    <input type="text" name="PhoneNumberExt">
                                </td>
                                <td>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>Mobile Number</label>
                                </td>
                                <td class="numberPhone">
                                    <input type="text" name="MobileNumberCod"> <label>-</label> 
                                    <input type="text" name="MobileNumber"> <label>Ext</label>
                                    <input type="text" name="MobileNumberExt">
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                </div>
        	</div>
            
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                <div class="addressPanel">	
                        <table>
                        	<tr>
                            	<td colspan="2">
                				<div class="centertext">
        		            		<h2>Billing Addres</h2>
		                        	<div>Your billing address must match the 
                    		   		 address on your credit card statement</div>
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full name</label>
                                </td>
                                <td>
                                	<input type="text" name="fullNameBilling">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input type="text" name="addressBilling">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input type="text" name="addressBilling2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                    <select name="city"></select>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input type="text" name="zipCode">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Country</label>
                                </td>
                                <td>
                                    <select name="coutry"></select>
                                </td>
                            </tr>
                        </table>
                </div>
                
                
                <div class="addressPanel">
                        <table>
                        	<tr>
                            	<td colspan="2">
                				<div class="centertext">
        		            		<h2>Mailing Addres</h2>
                                    <div>
                                    	<input type="checkbox"  class="radioButton" id="copyAddress">
                                        Check if mailing address is the same as billing address
                                    </div>
        			            </div>
		                    </td>
                    	</tr>
                    </table>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full name</label>
                                </td>
                                <td>
                                	<input type="text" name="fullNameBilling">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Address</label>
                                </td>
                                <td>
                                	<input type="text" name="addressBilling">
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                </td>
                                <td>
                                	<input type="text" name="addressBilling2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">City</label>
                                </td>
                                <td>
                                    <select name="city"></select>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">ZIP Code</label>
                                </td>
                                <td>
                                    <input type="text" name="zipCode">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Country</label>
                                </td>
                                <td>
                                    <select name="coutry"></select>
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="clear"></div>
                </div>
        	</div>
            <div class="clear"></div>
            
            <div class="panel">
            	<div class="center register">
                        <h2>Credit Card Information</h2>
                        <table>
                        	<tr>
                            	<td>
                                	<label class="required">Full Name on Card</label>
                                </td>
                                <td>
                                	<input type="text" name="nameCard">
                                </td>
                                <td>
                                	As it appears on the credit card
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Type of Credit Card</label>
                                </td>
                                <td>
                                	<select name="typeCard"></select>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Credit Card Number</label>
                                </td>
                                <td>
                                	<input type="text" name="cardNumber">
                                </td>
                                <td>
                                	No spaces, dashes or punctuation
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Expitation Date</label>
                                </td>
                                <td>
                                	Month<select name="monthExpiration">
                                    	<option>01</option>
                                    	<option>02</option>
                                    	<option>03</option>
                                    	<option>04</option>
                                    	<option>05</option>
                                    	<option>06</option>
                                    	<option>07</option>
                                    	<option>08</option>
                                    	<option>09</option>
                                    	<option>10</option>
                                    	<option>11</option>
                                    	<option>12</option>
                                    </select>
                                    Year<select name="yearExpiration ">
                                    	<option>2011</option>
                                    	<option>2012</option>
                                    	<option>2013</option>
                                    	<option>2014</option>
                                    	<option>2015</option>
                                    	<option>2016</option>
                                    	<option>2017</option>
                                    </select>
                                </td>
                                <td>
                                </td>
                            </tr>
                        	<tr>
                            	<td>
                                	<label class="required">Secure Code/CW</label>
                                </td>
                                <td>
                                	<input type="password" class="shortBox" name="password"> (<span class="bluetext">What is this?</span>)
                                </td>
                                <td>
                                	
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="centertext">
                        	<a class="buttongreen bigButton" href="#">Continue to Confirmation Page</a>
                        </div>
                        <div class="clear">
                        </div>
                </div>
        	</div>
         </form>
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-store');
 $('#learnNotificationSSL').bind('mouseenter',function (){
	 $('#NotificationSSL').fadeIn(200);
	 }).parent().parent().bind('mouseleave',function (){$('#NotificationSSL').fadeOut(200);})

$("input[name='companyRegister']").change(function(){
	$("#divOfficeNumber").toggle(200);
});
$("input[name='companyRegister2']").change(function(){
	$("#divOfficeNumber2").toggle(200);
});
$("input[name='type']").change(function(){
	$("#registerCompany,#registerCompany2").fadeOut(200);
	$(".CodLicense").delay(300).fadeOut(200,function (){
		    if ($("input[name='type']:checked").val() == 'LicenseEstateSales')
			{
				$("#LicenseEstateSales").slideDown(200).fadeIn(200);
				$("#registerCompany").delay(200).fadeIn(200);
			}
			else if ($("input[name='type']:checked").val() == 'LicenseEstateBroker')
			{
				$("#LicenseEstateBroker").slideDown(200).fadeIn(200);
				$("#registerCompany2").delay(200).fadeIn(200);
			}
		});

});
$(".whatsThis").bind('mouseenter',function (){$(this).find('div').fadeIn(200);}).bind('mouseout',function (){$(this).find('div').stop(false,true).fadeOut(200);});
</script>