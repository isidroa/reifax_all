<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/conexion.php');
	require_once('../resources/php/properties_conexion.php');
	if($_COOKIE['datos_usr'] && !$_COOKIE['suspended'])
	{
		$sql="SELECT homefinder, buyerspro, leadsgenerator, residential, platinum, professional, professional_esp FROM permission where userid=".$_COOKIE['datos_usr']['USERID'].";";
		$respuesta=$conex->query($sql);
		$status=$respuesta->fetch_assoc();
	}
	if($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1)
	{
		if($status['professional']==1 || $status['professional_esp']==1)
		{
			$boton[3]='<a href="http://professional.reifax.com" class="buttongreen">Access</a><div></div>';
			$boton[1]=$boton[2]='';
		}
		else if($status['platinum']==1)
		{
			$boton[3]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[2]='<a href="http://platinum.reifax.com" style="margin-left:-30%" class="buttongreen">Access</a><div></div>';
			$boton[1]='';
		}
			$boton[7]='<a href=" http://dealfinder.reifax.com" class="buttongreen">Access</a>';
			$boton[6]='<a href="http://buyerspro.reifax.com" class="access buttongreen">Access</a><div></div>';
			$boton[8]='<a href="http://leads.reifax.com" class="access buttongreen">Access</a><div></div>';
	}
	else {
		if($status['residential']==1)
		{
			$boton[3]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[2]='<a href="../settings/upgradeAccountTem.php" class="buttongreen">Upgrade</a><div></div>';
			$boton[1]='<a href="http://residential.reifax.com" class="buttongreen">Access</a><div></div>';
			$boton[1]='';
		}
		else
		{
			$boton[3]='<a href="http://www.reifax.com/register/customerRegister.php?gp=3" class="buttongreen">Buy Now</a><div></div>';
			$boton[2]='<a href="http://www.reifax.com/register/customerRegister.php?gp=2" class="buttongreen">Buy Now</a><div></div>';
			$boton[1]='<a href="http://www.reifax.com/register/customerRegister.php?gp=1" class="buttongreen">Buy Now</a><div></div>';
		}
			$boton[7]=($status['homefinder']==1)?
				'<a href=" http://dealfinder.reifax.com" class="buttongreen">Access</a>':
				'<a href="http://www.reifax.com/register/customerRegister.php?gp=7" class="buttongreen">Buy Now</a><div></div>';
			$boton[6]=($status['buyerspro']==1)?
				'<a href="http://buyerspro.reifax.com" class="buttongreen">Access</a><div></div>':
				'<a href="http://www.reifax.com/register/customerRegister.php?gp=6" class="buttongreen">Buy Now</a><div></div>';
			$boton[8]=($status['leadsgenerator']==1)?
				'<a href="http://home.reifax.com" class="buttongreen">Access</a><div></div>':
				'<a href="http://www.reifax.com/register/customerRegister.php?gp=8" class="buttongreen">Buy Now</a><div></div>';
	}
	function printNames($products,$boton)
	{
		foreach($products as $indice)
			{
				if($indice['id']=='6')
				{
					$indice['name']=str_replace(' ','<br>',$indice['name']);
				}
				if($indice['id']=='2')
				{
					$indice['name']=str_replace(' ','<br>',$indice['name']);
				}
				echo '<th class="details"><div>'.$indice['name'].$boton[$indice['id']].'</div></th>';
			}
	}
	$sql='SELECT idproducto, name FROM xima.usr_producto where idproducto in (1,2,3,6,7,8) ORDER BY orden_compare;';
	$tem=$conex->query($sql);
	$products=array();
	while($tem2=$tem->fetch_assoc())
	{
		$products[]=array(id =>$tem2['idproducto'],name =>$tem2['name']);

	}
	//print_r($products);
	$tem=$tem2=0;
	$sql='SELECT idusr_features,idproducto,activo FROM usr_producto_features;';
	$tem=$conex->query($sql);
	while($tem2=$tem->fetch_assoc())
	{
		$features[$tem2['idusr_features']][$tem2['idproducto']]=$tem2['activo'];
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="index.php"><span class="bluetext underline">Products</span></a> &gt; 
        	<span class="fuchsiatext">Product Comparison</span>
    </div>
  	<div id="contentPrincipal">
        <div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Product Comparison - Here you can compare features among our products</span>
            	</div>
        	</div>
        </div>
    <table class="tableCompare">
    	<tr>
        	<th class="feature">
            	Feature
            </th>
            <?php
				printNames($products,$boton);
			?>
        </tr>
    </table>
    <div class="clear"></div>
    <div class="clear"></div>
    <div class="clear"></div>
    <div class="clear"></div>
    <table>
  	<?php
	$sql='SELECT f.idusr_features, f.feature, g.title FROM usr_features f, usr_groupfeatures g where f.idusr_groupfeatures=g.idusr_groupfeatures order by g.orden, f.orden;';
	$groups=$conex->query($sql);
	while($group=$groups->fetch_assoc())
	{
		if(($featureGroup!=$group['title']) || ($featureGroup==''))
		{	
			$featureGroup=$group['title'];
			echo '</table><div class="featureGroup bold">'.$featureGroup.'</div><table class="tableCompare">';
			$i=0;
		}
		echo ($i%2==0)?'<tr class="par"><td class="feature"> 	<div class="cuote">&nbsp;</div>'.$group['feature'].'</td>':'<tr class="impar"><td class="feature">	<div class="cuote">&nbsp;</div>'.$group['feature'].'</td>';
		foreach($products as $indice)
		{
			 echo ($features[$group['idusr_features']][$indice['id']])? '<td class="details active"></td>' : '<td class="details inactive"></td>';
		}
		$i++;
	}
	?>
    </tr></table>
    </div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
	menuClick('menu-products');
</script>