<?php
# Coded By Jijo Last Update Date [Jan/19/06] (http://www.phpclasses.org/browse/package/2964.html)
# Updated 2008-12-18 by Dustin Davis (http://nerdydork.com)
	# Utilized $savedirpath parameter
	# Added delete_emails parameter
# Updated 2011-09-02 by Wathek Bellah LOUED (http://wathek.org)
	# Changing the way to retrieve the UID to make it works with gmail
	# Changing the encoding of the messages of type 0 and 1
	# Adding a message when it completes
	
class ReadAttachment
{
	function getdecodevalue($message,$coding) {
		switch($coding) {
			case 0:
			case 1:
//				$message = imap_8bit($message);
				$message = imap_base64($message);
				break;
			case 2:
				$message = imap_binary($message);
				break;
			case 3:
			case 5:
				$message = imap_base64($message);
				break;
			case 4:
				$message = imap_qprint($message);
				break;
		}
		return $message;
	}

	function getdata($host,$login,$password,$savedirpath,$delete_emails=false) {
		$nbattach = 0;
		// make sure save path has trailing slash (/)
		$savedirpath = str_replace('\\', '/', $savedirpath);
		if (substr($savedirpath, strlen($savedirpath) - 1) != '/') {
			$savedirpath .= '/';
		}
		
		$mbox = imap_open ($host, $login, $password) or die("can't connect: " . imap_last_error());
		$message = array();
		$message["attachment"]["type"][0] = "text";
		$message["attachment"]["type"][1] = "multipart";
		$message["attachment"]["type"][2] = "message";
		$message["attachment"]["type"][3] = "application";
		$message["attachment"]["type"][4] = "audio";
		$message["attachment"]["type"][5] = "image";
		$message["attachment"]["type"][6] = "video";
		$message["attachment"]["type"][7] = "other";
		
		$ids = imap_search($mbox, 'ALL', SE_UID);
		$cant=count($ids);
		echo "Total Mails:: $cant<br>";
		
		$cant=5;
		for ($jk = 0; $jk < $cant; $jk++) {
			$overview = imap_fetch_overview($mbox,$ids[$jk] , FT_UID);
			echo '<hr/><p>'.$jk.'.- Tema: '.$overview[0]->subject.'<br/>';
			echo 'uid: '.$overview[0]->uid.'<br/>';
			echo 'msgno: '.$overview[0]->msgno.'<br/>';
			echo 'De: '.$overview[0]->from.'<br/>';
			echo 'to: '.$overview[0]->to.'<br/>';
			echo 'date: '.$overview[0]->date.'<br/>';
			echo 'message_id: '.$overview[0]->message_id.'<br/>';
			echo 'references: '.$overview[0]->references.'<br/>';
			echo 'in_reply_to: '.$overview[0]->in_reply_to.'<br/>';
			echo 'size: '.$overview[0]->size.'<br/>';
			echo 'recent: '.$overview[0]->recent.'<br/>';
			echo 'flagged: '.$overview[0]->flagged.'<br/>';
			echo 'answered: '.$overview[0]->answered.'<br/>';
			echo 'deleted: '.$overview[0]->deleted.'<br/>';
			echo 'seen: '.$overview[0]->seen.'<br/>';
			echo 'draft: '.$overview[0]->draft.'<br/>';
			echo '</p>';    
			$mid=$overview[0]->msgno;
			$uid=$overview[0]->uid;
			
			$structure = imap_fetchstructure($mbox, $ids[$jk] , FT_UID);
			if (!isset($structure->parts)) continue;

			
			$parts = $structure->parts;
			$fpos=2;
			for($i = 1; $i < count($parts); $i++) {
				$message["pid"][$i] = ($i);
				$part = $parts[$i];

				if(isset($part->disposition) && $part->disposition == "ATTACHMENT") {
					$message["type"][$i] = $message["attachment"]["type"][$part->type] . "/" . strtolower($part->subtype);
					$message["subtype"][$i] = strtolower($part->subtype);
					$ext=$part->subtype;
					$params = $part->dparameters;
					$filename=$uid."_".$part->dparameters[0]->value;
					
					$mege = imap_fetchbody($mbox,$jk+1,$fpos);  
					$fp=fopen($savedirpath.$filename, 'w');
					$data=$this->getdecodevalue($mege,$part->type);	
					fwrite($fp,$data);
					fclose($fp);
					$nbattach++;
					$fpos+=1;
				}
			}
			if ($delete_emails) {
				// imap_delete tags a message for deletion
				imap_delete($mbox,$jk+1);
			}
		}
		// imap_expunge deletes all tagged messages
		if ($delete_emails) {
			imap_expunge($mbox);
		}
		imap_close($mbox);
		return ("Completed ($nbattach attachment(s) downloaded into $savedirpath)");
	}
}

$correo= new ReadAttachment();
//'pop.gmail.com'  993   pop3 cn seguridad
$host = '{imap.gmail.com:993/imap/ssl}INBOX';
$login = 'xelasmi@gmail.com';
$password = 'alesmi8312';
$savedirpath='../files';

$correo->getdata($host,$login,$password,$savedirpath);
?>
