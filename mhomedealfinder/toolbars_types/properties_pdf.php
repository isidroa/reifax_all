<?php  
	define('FPDF_FONTPATH','../FPDF/font/');
	$realtor=$_POST['userweb']=="false" ? false:true;	
	$realtorid= $_POST['realtorid'];
	if($realtor){ 
		$imp=100; $usr= $realtorid;
	}else{
		$imp=1000; $usr= $_COOKIE['datos_usr']['USERID'];
	}
	$_SERVERXIMA="http://www.reifax.com/";
	
	if(isset($_POST['groupbylevel'])) $_GET['groupbylevel']=$_POST['groupbylevel'];
	$_GET['resultType']='basic';
	$_GET['systemsearch']=isset($_POST['systemsearch']) ? $_POST['systemsearch'] : (isset($_GET['systemsearch']) ? $_GET['systemsearch']:'basic');
	
	if(isset($_POST['resultby'])) $_GET['resultby']=$_POST['resultby'];
	if(isset($_POST['filter_buyer_owns'])) $_GET['filter_buyer_owns']=$_POST['filter_buyer_owns'];
	if(isset($_POST['search_filter_groupby'])) $_GET['search_filter_groupby']=$_POST['search_filter_groupby'];
	if(isset($_POST['search_filter_groupby_type'])) $_GET['search_filter_groupby_type']=$_POST['search_filter_groupby_type'];
	
	$_POST['ResultTemplate']=-1;
	include('../coresearch.php');

	if(!isset($_POST['groupbylevel']) || (isset($_POST['groupbylevel']) && $_POST['groupbylevel']!=1)){
		if(!isset($_POST['parcelids_res']) || (isset($_POST['parcelids_res']) && $_POST['parcelids_res']=='')){
			$rest_total=array();
			$query=substr($query,0,strpos($query,'LIMIT'));
			$query=substr($query,strpos($query,'FROM'),strlen($query));
			$query='SELECT p.parcelid '.$query.'LIMIT '.$imp;//s.parcelid ------ p.parcelid
	
			$result=mysql_query($query) or die($query.mysql_error());
		
			while($r=mysql_fetch_array($result))
				$rest_total[]=$r['parcelid'];
			//$rest_total=array_slice($list_parcelid,0,$imp);
		}else{
			$rest_total=explode(',',$_POST['parcelids_res']);
		}
	}else{
		$query=substr($query,0,strpos($query,'LIMIT'));
		$query=substr($query,strpos($query,'FROM'),strlen($query));
		$query.='LIMIT '.$imp;
		
		$queryGroup=$query;
		$rest_total=array();
		if(isset($_POST['parcelids_res']) && $_POST['parcelids_res']!='')
			$rest_total=explode(',',$_POST['parcelids_res']);
	}
	
	include('../FPDF/mysql_table.php');
	include("../FPDF/limpiar.php");
	
	limpiardirpdf2('../FPDF/generated/');	//Se borran los pdf viejos
	
	class PDF extends PDF_MySQL_Table
	{
		function Header()
		{
			global $datos_usr;
			//Title
			$this->SetFont('Arial','',10);
			$this->Cell(0,6,'Rei Property Fax',0,1,'L');
			switch($datos_usr['search_type']){
			case 'FS': 	$this->Cell(0,6,'For Sale Report',0,1,'C');	break;
			case 'PR': 	$this->Cell(0,6,'P.Records Report',0,1,'C');	break;
			case 'FR': 	$this->Cell(0,6,'For Rent Report',0,1,'C');	break;
			case 'FO':	$this->Cell(0,6,'Foreclosure Report',0,1,'C');	break;
			}
			$this->Ln(2);
			//Ensure table header is output
			parent::Header();
		}
	}
	
	$query='select search_type from xima.xima_system_var WHERE userid='.$usr;
	$result=mysql_query($query) or die($query.mysql_error());
	$datos_usr=mysql_fetch_array($result);
	
	$pdf=new PDF('L');		//Landscape -- hoja horizontal
	$pdf->Open();
	$pdf->AddPage();
	
	include("../properties_getgridcamptit.php");

	if(!isset($_POST['template_res']) || (isset($_POST['template_res']) && ($_POST['template_res']=='Default' || $_POST['template_res']==-1)))
		$id = getArray($datos_usr['search_type'],'result');
	else
		$id = getArray($_POST['template_res'],'template');
		
	$ArIDCT = array(61,124,131,96,603,72,71,106,126,129,95,559,294,293,290);
	
	if(isset($_POST['groupbylevel']) && $_POST['groupbylevel']==1){
		if($resultby=='ownername') $id  = array(20,21,22,25,24,23);
		if($resultby=='owneraddress') $id  = array(21,22,25,24,23,20);
		if($resultby=='agentname') $id = array(62,66,67,604); 
		if($resultby=='lender') $id  = array(323);
		if($resultby=='plaintiff') $id  = array(502);
	}
	if (isset($_GET['groupbylevel']) && $_GET['groupbylevel']==2){
		$id = array(538,41,536,19,6,4,3,541,339,43,296,30,29);
	}

	//print_r( $id);
	$ArTab=getCamptitTipo($id, "Tabla",'defa');	
	$ArCamp=getCamptitTipo($id, "Campos",'defa');
	$ArTit=getCamptitTipo($id, "Titulos",'defa');
	$ArSize=getCamptitTipo($id, "r_size",'defa');
	
	if(!isset($_POST['groupbylevel']) || (isset($_POST['groupbylevel']) && $_POST['groupbylevel']!=1)){
		$select='psummary.parcelid as pid';
		$joint = array('psummary');
		for($i=0; $i< count($ArCamp); $i++){	//Formacion de Select
			$select.=",";
			
			if(array_search($ArTab[$i],$joint)===false)
				$joint[]=$ArTab[$i];
			
			$select.=$ArTab[$i].".".$ArCamp[$i]."\r";
		}
		
		
		$xSql='SELECT '.$select.' FROM ';
		
		foreach($joint as $k => $val){
			if($k==0){
				$xSql.="$val ";
			}else
				$xSql.="LEFT JOIN $val ON (".$joint[0].".parcelid=$val.parcelid) ";
		}
		
		$xSql.='WHERE '.$joint[0].'.parcelid IN (\''.implode("','",$rest_total).'\')';
	}

	
	if(isset($_POST['groupbylevel']) && $_POST['groupbylevel']==1){
		$select='p.parcelid, count(*) as count';
		for($i=0; $i< count($ArCamp); $i++){	//Formacion de Select
			$select.=', ';
			$select.=$ArTab[$i].".".$ArCamp[$i]."\r";
		}
		
		$xSql='SELECT '.$select.$queryGroup;
		$pdf->AddCol('count',20,'Quantity','C');
	}
	
	//Formacion de Tabla
	for($i=0; $i< count($ArCamp); $i++){
		$pdf->AddCol($ArCamp[$i],$ArSize[$i],$ArTit[$i],'C');
	}

	$prop=array('HeaderColor'=>array(135,206,250),
            'color1'=>array(255,255,255),
            'color2'=>array(255,255,255),
            'padding'=>2,
			'align'=>'C');
		
	$pdf->Table($xSql,$prop,$rest_total);		

	$file='FPDF/generated/'.strtotime("now").'.pdf';
	$pdf->Output('C:/inetpub/wwwroot/'.$file, 'F');
	echo "{success:true, pdf:'$file'}";
?>