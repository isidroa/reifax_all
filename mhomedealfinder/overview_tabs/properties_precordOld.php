<?php
	$db_data=$_GET['db'];
	$pid=$_GET['pid'];
	$realtor=$_POST['userweb']=="false" ? false:true;	
	if(isset($_GET['overview_comp'])) $overview_comp=true;
	
	if(!isset($par_no_conexion)){
		include("../properties_conexion.php");
		conectarPorBD($db_data);
	
		$sql_camptit="Select Tabla,Campos,Titulos,`Desc` FROM xima.camptit;";	
		$res = mysql_query($sql_camptit) or die(mysql_error());
		$Camptit_array=array();
		
		while($row=mysql_fetch_array($res, MYSQL_ASSOC))
			$Camptit_array[]= array($row["Tabla"],$row["Campos"],$row["Titulos"],$row["Desc"]);
		
		function camptit($campo,$tabla,$tipo='Titulos'){
			global $Camptit_array;
			foreach($Camptit_array as $k => $val){
				if(strtolower($val[0])==strtolower($tabla) && strtolower($val[1])==strtolower($campo)){
					if($tipo=='Titulos')
						return $val[2];
					else
						return $val[3];
				}
			}
		}
		
		function format_fecha($fecha){
			if($fecha==NULL || empty($fecha) || strlen($fecha)!=8)
				return '';
			else
				return substr($fecha,4,2).'/'.substr($fecha,6,2).'/'.substr($fecha,0,4);
		}
	}
	
	$sql_comparado="Select 
	status,lprice
	FROM mlsresidential 
	Where parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$myrow= mysql_fetch_array($res);
	
	$status_pro=$myrow['status'];
	$lprice_pro=$myrow['lprice'];	

	$sql_comparado="Select 
	latitude,longitude
	FROM latlong 
	Where parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$myrow= mysql_fetch_array($res);
	
	$latitude=$myrow['latitude'];
	$longitude=$myrow['longitude'];
///////////////////PUBLIC RECORDS///////////////////////////////////
	$sql_comparado="Select 
	p.address,p.unit,p.zip,p.city,p.sdname,p.folio,p.ccode,p.stories,
	p.lsqft,p.ac,p.yrbuilt,p.bheated,p.beds,p.pool,p.units,p.bath,p.waterf,p.buildingv,p.sfp,p.homstead,
	p.landv,p.taxablev,p.saledate,p.saleprice,p.owner,p.owner_a,p.owner_s,p.owner_c,p.owner_p,p.owner_z,
	p.phonename,p.phonenumber1,p.phonenumber2,p.rng,p.sec,p.twn,p.legal,p.ccoded,p.lunit_type,p.lunits,p.tsqft,
	m.closingdt,m.mlnumber
	FROM psummary p LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
	Where p.parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$myrow= mysql_fetch_array($res);
	
	$loged=false;
	$block=true;
	if(isset($_COOKIE['datos_usr']['USERID'])){
		$loged=true;
		if ($realtor){ $loged=false; }
		
		$query='select block_county,block_commercial,block_realtorweb from xima.xima_system_var WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		$block=($r['block_county']=='Y' || $r['block_commercial']=='Y' || $r['block_realtorweb']=='Y') ? true : false;
		$commercial=$r['block_commercial']=='Y' ? 2 : 1;
		$realtorweb=$r['block_realtorweb']=='Y' ? true : false;
		
		if($realtorweb){
			$query='select adverrealtorweb from xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']['USERID'];
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			if($r[0]==0){ $loged=false; } 
		}
	}
	
	$map="psummary_mymap";
	$map2="psummary_mymapS";
	if(isset($par_no_conexion)){
		$map="Rpsummary_mymap";
		$map2="Rpsummary_mymapS";
	}
	if(isset($overview_comp)){
		$map="Cpsummary_mymap";
		$map2="Cpsummary_mymapS";
	}
?>		

<div align="center" id="psummary_data_div"  class="fondo_realtor_result_tab">
<div align="center" style="width:100%; margin:auto; margin-top:0px;">
<div id="<?php echo $map; ?>" style="width:48%;height:240px; border:1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;"></div><br clear="all" />
	
    <h1 align="left" class="overtab_realtor_titulo">PUBLIC RECORDS</h1>
<?php if($status_pro=='A'){?>
   <div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
        For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
    </div>
<?php }?>
<br clear="all" />
<table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit('address','psummary','desc'); ?>"><span style="font-weight:bold;"><?php echo camptit('address','psummary'); ?>:</span></td>
        <td><?php echo $myrow['address']; ?></td>
        <td title="<?php echo camptit("unit","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("unit","psummary"); ?>:</span></td>
        <td><?php echo $myrow['unit']; ?></td>
   	</tr>
    <tr align="left">
    	<td  title="<?php echo camptit("city","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("city","psummary"); ?>:</span></td>
        <td><?php echo $myrow['city']; ?></td>
        <td title="<?php echo camptit("sdname","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("sdname","psummary"); ?>:</span></td>
        <td><?php echo $myrow['sdname']; ?></td>
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("zip","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("zip","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['zip']; ?></td>
        <td title="<?php echo camptit("folio","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("folio","psummary"); ?>:</span></td>
        <td><?php echo $myrow['folio']; ?></td>
    </tr>
    <tr align="left">
    	<td title="<?php echo camptit("ac","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("ac","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['ac']; ?></td>
    	<td title="<?php echo camptit("stories","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("stories","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['stories']; ?></td>
    </tr>
    <tr style="background-color:#b5cedd;" align="left">
        <td title="<?php echo camptit("yrbuilt","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['yrbuilt']; ?></td>
        <td title="<?php echo camptit("tsqft","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("tsqft","psummary"); ?>:</span></td>
        <td><?php echo $myrow['tsqft']; ?></td>
   	</tr>
    <tr align="left">
    	<td title="<?php echo camptit("beds","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("beds","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['beds']; ?></td>
        <td title="<?php echo camptit("pool","mlsresidential",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("pool","mlsresidential"); ?>:</span></td>
        <td><?php echo $myrow['pool']; ?></td>
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("bath","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("bath","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['bath']; ?></td>
        <td title="<?php echo camptit("waterf","mlsresidential",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("waterf","mlsresidential"); ?>:</span></td>
        <td><?php echo $myrow['waterf']; ?></td>
   	</tr>
    <tr align="left">
    	<td title="<?php echo camptit("lsqft","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("lsqft","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['lsqft']; ?></td>
        <td title="<?php echo camptit("bheated","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("bheated","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['bheated']; ?></td>
    </tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("units","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("units","psummary"); ?>:</span></td>
        <td><?php echo $myrow['units']; ?></td>
        <td title="<?php echo camptit("ccoded","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("ccoded","psummary"); ?>:</span></td>
        <td><?php echo $myrow['ccoded']; ?></td>
    </tr>
    <tr align="left">
    	<td title="<?php echo camptit("buildingv","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("buildingv","psummary") ; ?>:</span></td>
        <td><?php echo number_format($myrow['buildingv'],2,'.',','); ?></td>
        <td title="<?php echo camptit("sfp","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("sfp","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['sfp']; ?></td>
        
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("landv","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("landv","psummary") ; ?>:</span></td>
        <td><?php echo number_format($myrow['landv'],2,'.',','); ?></td>
        <td title="<?php echo camptit("taxablev","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("taxablev","psummary") ; ?>:</span></td>
        <td><?php echo number_format($myrow['taxablev'],2,'.',','); ?></td>
   	</tr>
    <tr align="left">
    	<td valign=top title="<?php echo camptit("legal","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("legal","psummary"); ?>:</span></td>
        <td colspan=3><?php echo $myrow['legal']; ?></td>
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("saledate","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("saledate","psummary"); ?>:</span></td>
        <td><?php echo format_fecha($myrow['saledate']); ?></td>
       	<td title="<?php echo camptit("saleprice","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("saleprice","psummary"); ?>:</span></td>
        <td><?php echo number_format($myrow['saleprice'],2,'.',','); ?></td>
   	</tr>
</table>

<?php
	////////////////////////Sales////////////////////////////////
		$sql_comparado="SELECT  
		parcelid,date,type,price,book,page,grantor
		FROM Sales WHERE parcelid='$pid' 
		ORDER BY date DESC;";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$i=1;
		$sales=mysql_num_rows($res);
		while($myrow2= mysql_fetch_array($res)){
			if($i==1){
?>
<br />
<h1 align="left" class="overtab_realtor_titulo">HISTORY SALES</h1><br clear="all" />
<table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
<?php 		}?>
	<tr style="background-color:#b5cedd;" align="left">
    	<td colspan=6 align="center"><span style="font-weight:bold;">SALES <?php echo $i;?></span></td>
   	</tr>
    <tr align="left">
    	<td title="<?php echo camptit("date","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("date","sales");?>:</span></td>
        <td><?php echo format_fecha($myrow2['date']);?></td>
        <td title="<?php echo camptit("price","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("price","sales");?>:</span></td>
        <td class="mortcellpadright"><?php echo number_format($myrow2['price'],2,'.',',');?></td>
    </tr>
    <tr align="left">
    	<td title="<?php echo camptit("book","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("book","sales");?>:</span></td>
        <td><?php echo $myrow2['book'];?></td>
        <td title="<?php echo camptit("page","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("page","sales");?>:</span></td>
        <td><?php echo $myrow2['page'];?></td>
    </tr>
<?php 		if($i==$sales){?>
</table>
<br />
<?php if(!$realtor && $loged && !$block){?>
<h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">OWNER INFORMATION</h1><br clear="all" />
<table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
	<tr align="left">
    	<td title="<?php echo camptit("owner","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner","psummary") ; ?>:</span></td>
        <td colspan="3"><?php echo $myrow['owner']; ?></td>
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("owner_a","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_a","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['owner_a']; ?></td>
        <td title="<?php echo camptit("owner_c","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_c","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['owner_c']; ?></td>
    </tr>
    <tr align="left">
    	<td title="<?php echo camptit("owner_z","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_z","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['owner_z']; ?></td>
        <td title="<?php echo camptit("owner_s","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_s","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['owner_s']; ?></td>
   	</tr>
    <tr style="background-color:#b5cedd;" align="left">
    	<td title="<?php echo camptit("owner_p","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_p","psummary") ; ?>:</span></td>
        <td><?php if(strtolower($myrow['owner_p'])!='null') echo $myrow['owner_p']; ?></td>
        <td></td>
        <td></td>
   	</tr>
    <tr align="left">
    	<td title="<?php echo camptit("phonenumber1","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("phonenumber1","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['phonenumber1']; ?></td>
        <td title="<?php echo camptit("phonenumber2","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("phonenumber2","psummary") ; ?>:</span></td>
        <td><?php echo $myrow['phonenumber2']; ?></td>
   	</tr>    
</table>
<?php }
			}
			$i++;
		}
?>
</div>
</div>
<script>
	SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
	if(mapOverview == null)	mapOverview = new XimaMap('mapOverview','region','4points','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
	mapOverview.map = new VEMap('<?php echo $map; ?>');
	mapOverview.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
	mapOverview.map.HideDashboard();
	var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
	pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
		"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
	mapOverview.map.AddShape(pin); 
				
	if(mapOverviewS == null) mapOverviewS = new XimaMap('mapOverviewS','region','4points','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
	mapOverviewS.map = new VEMap('<?php echo $map2; ?>');
	mapOverviewS.map.LoadMap(SpaceNeedle, 15);
	mapOverviewS.map.HideDashboard();
	var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
	pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
		"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
	mapOverviewS.map.AddShape(pinS);
	
	
	

</script>