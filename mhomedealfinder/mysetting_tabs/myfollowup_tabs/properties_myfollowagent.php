<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 1px; 
}
</style>
<div align="left" id="todo_myfollowagent_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowagent_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowagent_filters"></div><br />
        <div id="myfollowagent_properties" align="left"></div> 
	</div>
</div>
<script>
	var limitmyfollowagent = 50;
	var selected_datamyfollowagent = new Array();
	var AllCheckmyfollowagent = false;
	
	var filterfield='agent';
	var filterdirection='ASC';
	
	var storemyfollowagent = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
		fields: [
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'urlsend'}
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'load' : function (store,data,obj){
				if (AllCheckmyfollowagent){
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowagent=true;
					gridmyfollowagent.getSelectionModel().selectAll();
					selected_datamyfollowagent=new Array();
				}else{
					AllCheckmyfollowagent=false;
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowagent.length > 0){
						for(val in selected_datamyfollowagent){
							var ind = gridmyfollowagent.getStore().find('agentid',selected_datamyfollowagent[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowagent.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var smmyfollowagent = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowagent.indexOf(record.get('agentid'))==-1)
					selected_datamyfollowagent.push(record.get('agentid'));
				
				if(Ext.fly(gridmyfollowagent.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowagent=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowagent = selected_datamyfollowagent.remove(record.get('agentid'));
				AllCheckmyfollowagent=false;
				Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-3;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = '';
		
		if(tyvalue == 0) 
			classvalue = 'home';
		else{ 
			if(tyvalue == 1) 
				classvalue = 'office';
			else 
				classvalue = 'cell';
		}
		if(value.length == 0) classvalue = '';
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		return '<div class="'+classvalue+'">'+value+'</div>';
	}
		
	var gridmyfollowagent = new Ext.grid.GridPanel({
		renderTo: 'myfollowagent_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowagent,
		stripeRows: true,
		sm: smmyfollowagent, 
		columns: [
			smmyfollowagent,
			{header: 'Agent', width: 150, sortable: true, tooltip: 'Agent name,', dataIndex: 'agent'}
			,{header: 'Email', width: 150, sortable: true, tooltip: 'Agent Email.', dataIndex: 'email'}
			,{header: 'Toll Free', width: 100, sortable: true, tooltip: 'Phone to Toll Free.', dataIndex: 'tollfree'}
			,{header: 'Phone 1', width: 100, sortable: true, tooltip: 'Phone 1.', dataIndex: 'phone1', renderer: phoneRender}
			,{header: 'Phone 2', width: 100, sortable: true, tooltip: 'Phone 2.', dataIndex: 'phone2', renderer: phoneRender}
			,{header: 'Phone 3', width: 100, sortable: true, tooltip: 'Phone 3.', dataIndex: 'phone3', renderer: phoneRender}
			,{header: 'Fax', width: 100, sortable: true, tooltip: 'Fax.', dataIndex: 'fax'}
			,{header: 'Send Page', width: 100, sortable: true, tooltip: 'Url of page to send documents.', dataIndex: 'urlsend'}			 
		],
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			}
		},
				
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowagent',
            pageSize: limitmyfollowagent,
            store: storemyfollowagent,
            displayInfo: true,
			displayMsg: 'Total: {2} Agents.',
			emptyMsg: "No Agents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 agents per page.',
				text: 50,
				handler: function(){
					limitmyfollowagent=50;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 agents per page.',
				text: 80,
				handler: function(){
					limitmyfollowagent=80;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 agents per page.',
				text: 100,
				handler: function(){
					limitmyfollowagent=100;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to delete selected agents.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					if(selected_datamyfollowagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the agents to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var agentids=selected_datamyfollowagent[0];
					for(i=1; i<selected_datamyfollowagent.length; i++)
						agentids+=','+selected_datamyfollowagent[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
							Ext.Msg.alert("Follow Agent", 'Agent delete.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to add a new agent.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						title: 'Follow Agent',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 230},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'agent',
									fieldLabel: 'Agent',
									allowBlank: false
								},{
									xtype     : 'textfield',
									name      : 'email',
									fieldLabel: 'Email',
									vtype	  : 'email'
								},{
									xtype     : 'textfield',
									name      : 'tollfree',
									fieldLabel: 'Toll Free'
								},{
									xtype     : 'textfield',
									name      : 'urlsend',
									fieldLabel: 'Send Page'
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname1',
										value         : '0',
										hiddenName    : 'typeph1',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone1',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname2',
										value         : '0',
										hiddenName    : 'typeph2',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone2',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 3',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname3',
										value         : '0',
										hiddenName    : 'typeph3',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone3',
										width	  : 165
									}]
								},{
									xtype     : 'textfield',
									name      : 'fax',
									fieldLabel: 'Fax'
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'insert'
								}],
						
						buttons: [{
								text: 'Insert',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Agent", action.result.msg);
											storemyfollowagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", "ERROR");
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 340,
						height      : 350,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			}),new Ext.Button({
				tooltip: 'Click to edit selected agent.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/refresh.gif',
				handler: function(){
					var agents = smmyfollowagent.getSelections();
					if(agents.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the agent to be edited.'); return false;
					}else if(agents.length > 1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one agent to be edited.'); return false;
					}
					
					var agent = agents[0];
					
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						title: 'Follow Agent',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 230},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'agent',
									fieldLabel: 'Agent',
									allowBlank: false,
									value	  : agent.get('agent')
								},{
									xtype     : 'textfield',
									name      : 'email',
									fieldLabel: 'Email',
									value	  : agent.get('email')
								},{
									xtype     : 'textfield',
									name      : 'tollfree',
									fieldLabel: 'Toll Free',
									value	  : agent.get('tollfree')
								},{
									xtype     : 'textfield',
									name      : 'urlsend',
									fieldLabel: 'Send Page',
									value	  : agent.get('urlsend')
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname1',
										value         : agent.get('typeph1'),
										hiddenName    : 'typeph1',
										hiddenValue   : agent.get('typeph1'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone1',
										width	  : 165,
										value	  : agent.get('phone1')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname2',
										value         : agent.get('typeph2'),
										hiddenName    : 'typeph2',
										hiddenValue   : agent.get('typeph2'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone2',
										width	  : 165,
										value	  : agent.get('phone2')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 3',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 60,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname3',
										value         : agent.get('typeph3'),
										hiddenName    : 'typeph3',
										hiddenValue   : agent.get('typeph3'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone3',
										width	  : 165,
										value	  : agent.get('phone3')
									}]
								},{
									xtype     : 'textfield',
									name      : 'fax',
									fieldLabel: 'Fax',
									value	  : agent.get('fax')
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'update'
								},{
									xtype     : 'hidden',
									name      : 'agentid',
									value     : agent.get('agentid')
								}],
						
						buttons: [{
								text: 'Update',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Agent", 'Updated Agent.');
											storemyfollowagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
												Ext.Msg.alert('Error',
													'Status:'+action.response.status+': '+
													action.response.statusText);
											}
											if (action.failureType === Ext.form.Action.SERVER_INVALID){
												// server responded with success = false
												Ext.Msg.alert('Invalid', action.result.errormsg);
											}
											if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
												Ext.Msg.alert('Error',
													'Please check de red market field, before update agent.');
											}
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 340,
						height      : 350,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			}),new Ext.Button({
				tooltip: 'Click to print agents.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to export Excel.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			})]
        })
	});
	
	storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowagent_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowagent_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>