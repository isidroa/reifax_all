<?php
	include("../properties_conexion.php");
	conectar();
	
	include ("../properties_getgridcamptit.php");	
	
	$ancho=950;
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYDoc','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$query='SELECT * FROM xima.saveddoc WHERE usr='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	
	while($r=mysql_fetch_object($result))
		$vFilas[]=$r;
		
	//print_r($vFilas);
	
?>
<div align="center" id="todo_mydocument_panel">
<br clear="all" />
<div id="mydocument_data_div" align="center" style=" margin:auto; width:<?php echo $ancho.'px;';?>">
    <div id="mydocument_properties"></div>
<?php 
	if(count($vFilas)==0){ 
		echo "There are no documents to show"; } ?>
</div>
</div>
<?php if(count($vFilas)!=0) { ?>
<script>
	function urldoc(value, metaData, record, rowIndex){
		return '<a href="'+value+'" target="_blank"><img src="http://www.reifax.com/img/doc.png"/></a>';
	}

	var arrayDatamydocument = [
		<?php 
			foreach($vFilas as $x => $val){
				if($x>0) echo ',';
				echo "['".($x+1)."','".$val->parcelid."'";
				foreach($hdArray as $k => $v){
					eval("\$xdata=\$val->".$v->name.";");
					echo ",'$xdata'";
				}
				echo ']';
				
			}
		?>
	];
	var storemydocument = new Ext.data.ArrayStore({
        fields: [
           <?php 
		   		echo "'ind','pid'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
		   ?>
        ]
    });
	
	var smmydocument = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	var gridmydocument = new Ext.grid.GridPanel({
		renderTo: 'mydocument_properties',
		cls: 'grid_comparables',
		width: <?php echo ($ancho-5);?>,
		height: <?php echo ((count($vFilas)+2)*24);?>,
		store: storemydocument,
		columns: [
				  smmydocument,
			<?php 
		   		echo "{id: 'ind' , header: 'Ind.', width: 30, sortable: true, tooltip: 'Index Property.', dataIndex: 'ind'},{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='url')
						echo ",{header: '".$val->title."', renderer: urldoc, width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					else
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
				}
		   ?>	  
			
		],
		sm: smmydocument
	});
	
	storemydocument.loadData(arrayDatamydocument);

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_mydocument_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_mydocument_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>
<?php } ?>