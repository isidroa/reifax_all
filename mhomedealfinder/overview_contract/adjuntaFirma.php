<?php
/**
 * adjuntarFirma.php
 *
 * Takes the saved signatures of the user and place the images 
 * in the documents.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 01.04.2011
 */

// Fix to get the correct aspect ratio of a image in a PDF
function PixelToDPI($px) {
	return abs(round((($px/110)*72),0));
}
function adjuntaFirma($type, $userid, $file){
	//echo 'aq '.$type.' '.$userid.' '.$file.' '.getcwd();
	
	//$userid = $_COOKIE['datos_usr']["USERID"];
	//$file   = $_GET['d']; 
	$pdf    = Zend_Pdf::load("{$file}");
	
	$path   = 'C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/signatures/';
	$signs  = null;
	
	// Getting the location of the signatures
	$query  = "SELECT imagen,type FROM xima.contracts_signature  
				WHERE userid = $userid ORDER BY type";
	$rs     = mysql_query($query);
	
	if ($rs) {
		while ($row = mysql_fetch_array($rs)) {
			$signs["{$row['type']}"] = $row[0];
		}
	}
	switch ($type) {
		case 'ContractPurchase' : 
			$count  = count($pdf->pages);
			$k      = 0;
			$inimax = 28;
			$sigmax = 50;
			
			// Place the initials for all the pages
			for ($i = 1; $i<$count ; $i++) {
				
				$k      = $i - 1;
				$page   = $pdf->pages[$k];
				
				if (strlen($signs[3])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,97,47,PixelToDPI($imgwd)+97,PixelToDPI($inimax)+47);
				}
				
				if (strlen($signs[4])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,163,47,PixelToDPI($imgwd)+163,PixelToDPI($inimax)+47);
				}
			
				if (strlen($signs[7])>0) {		
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,435,47,PixelToDPI($imgwd)+435,PixelToDPI($inimax)+47);
				}
	
				if (strlen($signs[8])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,500,47,PixelToDPI($imgwd)+500,PixelToDPI($inimax)+47);
				}
					
			}	
		
			// Place the signatures in the last page	
			$k++;
		
			$page  = $pdf->pages[$k];
	
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,495,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+495);
			}
	
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,459,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+459);
			}
	
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,415,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+415);
			}
	
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);	
				$page->drawImage($image,100,380,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+380);
			}
		break;
		
		case 'ResidentialContract' : 
		
			$count  = count($pdf->pages);
			$k      = 0;
			$sigmax = 50;
	
			// Place the initials for all the pages
			for ($i = 1; $i<$count ; $i++) {
				
				if ($i == 6 or $i == 7)
					$inimax = 16;
				else
					$inimax = 28;
				
				$k     = $i - 1;
				$page  = $pdf->pages[$k];
				
				if (strlen($signs[3])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
					$page->drawImage($image,97,42,PixelToDPI($imgwd)+97,PixelToDPI($inimax)+42);
				}
	
				if (strlen($signs[4])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
					$page->drawImage($image,163,42,PixelToDPI($imgwd)+163,PixelToDPI($inimax)+42);
				}
			
				if (strlen($signs[7])>0) {		
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
					$page->drawImage($image,428,42,PixelToDPI($imgwd)+428,PixelToDPI($inimax)+42);
				}
	
				if (strlen($signs[8])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
					$page->drawImage($image,490,42,PixelToDPI($imgwd)+490,PixelToDPI($inimax)+42);
				}
					
			}	
			
			// Place the signatures in the last page	
			$k++;
			
			$page  = $pdf->pages[$k];
	
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,100,395,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+395);
			}
	
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,100,350,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+350);
			}
	
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,100,305,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+305);
			}
	
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,100,260,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+260);
			}
	
		break;
	
	
		 
		case 'leadBasedPaint' :
	
			$page  = $pdf->pages[0];
			$inimax = 28;
	
			// Place the initials
			if (strlen($signs[3])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,120,610,PixelToDPI($imgwd)+120,PixelToDPI($inimax)+610);
			}
			
			if (strlen($signs[4])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,190,610,PixelToDPI($imgwd)+190,PixelToDPI($inimax)+610);
			}
			
			if (strlen($signs[7])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,390,610,PixelToDPI($imgwd)+390,PixelToDPI($inimax)+610);
			}
			
			if (strlen($signs[8])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);		
				$page->drawImage($image,465,610,PixelToDPI($imgwd)+465,PixelToDPI($inimax)+610);
			}
	
	
			// Place the buyers initials pars
	
			$inimax = 17;
			
			if (strlen($signs[3])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,28,333,PixelToDPI($imgwd)+28,PixelToDPI($inimax)+333);
			}
				
			if (strlen($signs[4])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,55,333,PixelToDPI($imgwd)+55,PixelToDPI($inimax)+333);
			}
				
			if (strlen($signs[3])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,28,315,PixelToDPI($imgwd)+28,PixelToDPI($inimax)+315);
			}
				
			if (strlen($signs[4])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,55,315,PixelToDPI($imgwd)+55,PixelToDPI($inimax)+315);
			}
				
			if (strlen($signs[3])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,28,297,PixelToDPI($imgwd)+28,PixelToDPI($inimax)+297);
			}
				
			if (strlen($signs[4])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,55,297,PixelToDPI($imgwd)+55,PixelToDPI($inimax)+297);
			}
			
			// ---
				
			if (strlen($signs[3])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,28,230,PixelToDPI($imgwd)+28,PixelToDPI($inimax)+230);
			}
			
			if (strlen($signs[4])>0) {		
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,55,230,PixelToDPI($imgwd)+55,PixelToDPI($inimax)+230);
			}
			
			// Place the signatures
	
			$inimax = 24;
	
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,80,165,PixelToDPI($imgwd)+80,PixelToDPI($inimax)+165);
			}
		
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,80,143,PixelToDPI($imgwd)+80,PixelToDPI($inimax)+143);
			}
		
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,348,165,PixelToDPI($imgwd)+348,PixelToDPI($inimax)+165);
			}
		
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
				$page->drawImage($image,348,143,PixelToDPI($imgwd)+348,PixelToDPI($inimax)+143);
			}
	
		break;
	
		
		case 'TX_unimproved_property' :
		
			$count  = count($pdf->pages);
			$k      = 0;
			$inimax = 28;
			$sigmax = 50;
			 
			for ($i = 1; $i<$count-1 ; $i++) {
				
				$k      = $i - 1;
				$page   = $pdf->pages[$k];
				
				if (strlen($signs[3])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,212,21,PixelToDPI($imgwd)+212,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[4])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,250,21,PixelToDPI($imgwd)+250,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[7])>0) {		
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,345,21,PixelToDPI($imgwd)+345,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[8])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,395,21,PixelToDPI($imgwd)+395,PixelToDPI($inimax)+21);
				}
					
			}	
			
			// Place the signatures in the last page	
			$k++;
			
			$page  = $pdf->pages[$k];
			
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,275,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+275);
			}
			
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,220,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+220);
			}
			
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,330,275,PixelToDPI($imgwd)+330,PixelToDPI($sigmax)+275);
			}
			
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);	
				$page->drawImage($image,330,220,PixelToDPI($imgwd)+330,PixelToDPI($sigmax)+220);
			}
	
		break;
		
		
	
		case 'TX_residential_condominium' :
		
			$count  = count($pdf->pages);
			$k      = 0;
			$inimax   = 17;
			$sigmax   = 35;
					
			// Place the initials for all the pages
			for ($i = 1; $i<$count ; $i++) {
				
				$k      = $i - 1;
				$page   = $pdf->pages[$k];
				
				
				if (strlen($signs[3])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,212,22,PixelToDPI($imgwd)+212,PixelToDPI($inimax)+22);
				}
			
				if (strlen($signs[4])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,250,22,PixelToDPI($imgwd)+250,PixelToDPI($inimax)+22);
				}
			
				if (strlen($signs[7])>0) {		
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,345,22,PixelToDPI($imgwd)+345,PixelToDPI($inimax)+22);
				}
			
				if (strlen($signs[8])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,395,22,PixelToDPI($imgwd)+395,PixelToDPI($inimax)+22);
				}
					
			}	
			
			// Place the signatures in the last page	
			
			$page  = $pdf->pages[$k];
			
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,110,130,PixelToDPI($imgwd)+110,PixelToDPI($sigmax)+130);
			}
			
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,110,100,PixelToDPI($imgwd)+110,PixelToDPI($sigmax)+100);
			}
			
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,350,130,PixelToDPI($imgwd)+350,PixelToDPI($sigmax)+130);
			}
			
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);	
				$page->drawImage($image,350,100,PixelToDPI($imgwd)+350,PixelToDPI($sigmax)+100);
			}
	
	
		break;
		
		
		case 'TX_one_to_four_residential' :
		
			$count  = count($pdf->pages);
			$k      = 0;
			$inimax   = 18;
			$sigmax   = 38;
			
			for ($i = 1; $i<$count-1 ; $i++) {
				
				$k      = $i - 1;
				$page   = $pdf->pages[$k];
				
				$inimax = $i == 1 ? 14 : 18;			
				
				if (strlen($signs[3])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[3]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,212,21,PixelToDPI($imgwd)+212,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[4])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[4]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,250,21,PixelToDPI($imgwd)+250,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[7])>0) {		
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[7]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,345,21,PixelToDPI($imgwd)+345,PixelToDPI($inimax)+21);
				}
			
				if (strlen($signs[8])>0) {
					$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[8]}");
					$imgwd = round(($image->getPixelWidth()*$inimax)/$image->getPixelHeight(),0);
					$page->drawImage($image,395,21,PixelToDPI($imgwd)+395,PixelToDPI($inimax)+21);
				}
					
			}	
			
			// Place the signatures in the last page	
			$k++;
			
			$page  = $pdf->pages[$k];
			
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,110,145,PixelToDPI($imgwd)+110,PixelToDPI($sigmax)+145);
			}
			
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,110,110,PixelToDPI($imgwd)+110,PixelToDPI($sigmax)+110);
			}
			
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,350,145,PixelToDPI($imgwd)+350,PixelToDPI($sigmax)+145);
			}
			
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);	
				$page->drawImage($image,350,110,PixelToDPI($imgwd)+350,PixelToDPI($sigmax)+110);
			}		
	
		break;
	
		
		case 'TX_lead_based_paint' :
		
			$count  = count($pdf->pages);
			$k      = 0;
			$sigmax   = 30;
					
			$page  = $pdf->pages[0];
			
			if (strlen($signs[1])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[1]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,191,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+191);
			}
			
			if (strlen($signs[2])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[2]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,100,160,PixelToDPI($imgwd)+100,PixelToDPI($sigmax)+160);
			}
			
			if (strlen($signs[5])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[5]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);
				$page->drawImage($image,390,191,PixelToDPI($imgwd)+390,PixelToDPI($sigmax)+191);
			}
			
			if (strlen($signs[6])>0) {
				$image = Zend_Pdf_Image::imageWithPath("{$path}{$signs[6]}");
				$imgwd = round(($image->getPixelWidth()*$sigmax)/$image->getPixelHeight(),0);	
				$page->drawImage($image,390,160,PixelToDPI($imgwd)+390,PixelToDPI($sigmax)+160);
			} 
		break;
	}
	//unlink($file);
	$pdf->save($file); 
}

?>