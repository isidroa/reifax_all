<?php
	include("../properties_conexion.php");
	include("../FPDF/limpiar.php");
	require("../mailer/class.phpmailer.php");
	limpiardirpdf2('../FPDF/generated/');	//Se borran los pdf viejos
    
	$type=$_POST['type'];
	$price=$_POST['pricedefault'];
	$county =  $_POST['county'];
	$parcelid = $_POST['pid'];
	$userid = $_COOKIE['datos_usr']['USERID'];
	$arrayCamp=Array('txtseller','txtbuyer','txtaddress','txtcounty','txtlegal1','txtlegal2','txtprice','txtlistingsales','txtbuyeraddress1','txtselleraddress1','txtpage','txtdatebuyer1','txtdatebuyer2','txtcollectedfunds');
	conectarPorBd('fl'.strtolower($county));
	$valores=Array();
	$balancevalue=0;
	$query="SELECT * FROM xima.contracts_default c where contract='".$type."' and `defaultoption`='Y' and userid=".$userid;
	$result=mysql_query($query) or die($query.mysql_error());
	$total=mysql_num_rows($result);
	if($total>0){
		
		$r=mysql_fetch_array($result);
		$valores[1]=$r['name'];
		$valores[8]=$r['address'];
		$usatemplate=$r['template'];
		$activotemplate=$r['activetemplate'];
		if($usatemplate=='Y' && $activotemplate=='Y'){
			$id=$r['id'];
			$q="select min(id) as minid from xima.contracts_default c where contract='".$type."' and userid=".$userid;
			$res=mysql_query($q) or die($q.mysql_error());
			$ro=mysql_fetch_array($res);
			$minid=$ro['minid'];
			if($id==$minid){
				$pdfusar='1';
			}else{
				$pdfusar='2';
			}
			$borrartemplate='Y';
		}else{
			$pdfusar='default';
			$borrartemplate='N';
		}
		//echo $pdfusar;
	}else{
		//Obtener valores del  usuario
		$query="SELECT x.`HOMETELEPHONE`, x.`ADDRESS`, x.`STATE`, x.`CITY`, x.`NAME`, x.`SURNAME` from xima.ximausrs x where 	userid=".$userid;
		$result=mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		$valores[1]=$r['NAME'].' '.$r['SURNAME'];
		$valores[8]=$r['ADDRESS'];
		$pdfusar='default';
		$borrartemplate='N';
	}
	
	//Obtener valores public record
	$query="SELECT owner, address, ozip, city, owner_a, phonenumber1 from psummary where parcelid='".$parcelid."'";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$valores[0]=$r['owner'];
	$valores[3]=$county;
	$valores[9]=$r['owner_a'];
	$address=$r['address'];
	$city=$r['city'];
	$zip=$r['ozip'];
	$valores[4]="Folio Number: ".$parcelid."  Legal Description as Shown in Public Records.";
	//Obtener datos mlsresidential
	$query="SELECT lprice, agent, address, city, zip, officefax from mlsresidential where parcelid='".$parcelid."'";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	if(strlen($price)==0){
		$valores[6]=$r['lprice'];
		$balancevalue=$balancevalue+$r['lprice'];
	}else{
		$valores[6]=$price;
		$balancevalue=$balancevalue+$price;
	}
	$fax=$r['officefax'];
	$fax=str_replace('(','',$fax);
	$fax=str_replace(')','',$fax);
	$fax=str_replace(' ','',$fax);
	$fax=str_replace('-','',$fax);
	$fax=$fax.'@rcfax.com';
	$valores[7]=$r['agent'];
	if($address==''){
		$address=$r['address'];
	}
	if($city==''){
		$city=$r['city'];
	}
	if($zip==''){
		$zip=$r['zip'];
	}
	$valores[2]=$address.', '.$city.', '.$zip;
	$valores[10]='1';
	
	$fecha=date('m-d-Y');
	$valores[11]=$fecha;
	$valores[12]=$fecha;
	
	$contratos=Array('ContractPurchase','leadBasedPaint','ResidentialContract');
	$camposContratos=Array('contract','lead','residential');
	
	$ruta= getCwd(); //break;
	$s=0;
	if($pdfusar!='default'){
		//Tomar plantilla de cliente
		$templatePdf=$contratos[$type].$userid.$pdfusar;
		//$nombre='C:/inetpub/wwwroot/Xima3/mysetting_tabs/mycontracts_tabs/template_upload/campos.pdf.fields';
		copy('C:/inetpub/wwwroot/Xima3/mysetting_tabs/mycontracts_tabs/template_upload/'.$templatePdf.'.pdf',$ruta.'/'.$templatePdf.'.pdf');
		$nombre=$ruta.'/campos.pdf.fields';
		if(file_exists($nombre)){
			unlink($nombre);
		}
		passthru( $ruta.'/pdftk/pdftk '.$templatePdf.'.pdf dump_data_fields >> '.$nombre );
		$fields=$nombre;
	}else{
		$fields=$camposContratos[$type].'.pdf.fields';
		$templatePdf=$contratos[$type];
		
	}
	//echo $userid.' '.$fields.' '.$templatePdf;
	//Recorre Form fields para obtener array de campos a llenar
	$campos=Array();
	$valoresPredeterminados=Array();
	$field_arr= load_field_data($fields);
	foreach( $field_arr as $field ) { // itera en los campos
		$campos[]=$field['FieldName:'];
		$valoresPredeterminados[]=$field['FieldValue:'];
		if($field['FieldName:']=='txtdeposit' || $field['FieldName:']=='txtaditional' || $field['FieldName:']=='txtotherprice'){
			$balancevalue=$balancevalue-$field['FieldValue:'];
		}
	}
	$valores[13]=$balancevalue;
	require_once( 'forge_fdf.php' );

	$fdf_data_strings= array();
	$fdf_data_names= array();
	
	//Manda valores a la plantilla pdf
	foreach( $campos as $key => $value ) {
	   // translate tildes back to periods
	   //$fdf_data_strings[ strtr(str_replace('_',' ',$key), '~', '.') ]= $value;
	   $encontrado=true;
	   $count=0;
	   while($encontrado && $count<count($arrayCamp)){
			if($value==$arrayCamp[$count]){
				$encontrado=false;
				$fdf_data_strings[ strtr($arrayCamp[$count], '~', '.') ]= $valores[$count];
			}
			$count++;
	   }
	   if($encontrado){
	   		$fdf_data_strings[ strtr($value, '~', '.') ]= $valoresPredeterminados[$key];
	   }
	}
	// ignore these in this example
	$fields_hidden= array();
	$fields_readonly= array();
	
	$fdf= forge_fdf( '',
							$fdf_data_strings,
							$fdf_data_names,
							$fields_hidden,
							$fields_readonly );
	//echo $fdf; //break;
	$fdf_fn= tempnam( '.', 'fdf' );
	$fp= fopen( $fdf_fn, 'w' );
	if( $fp ) {
	   fwrite( $fp, $fdf );
	   fclose( $fp );
		
	   
	   $file='FPDF/generated/'.$contratos[$type].$parcelid.'.pdf';
	   passthru(   $ruta.'/pdftk/pdftk '.$templatePdf.'.pdf fill_form '. $fdf_fn.
				   ' output C:/inetpub/wwwroot/'.$file );
	   $archivo='C:/inetpub/wwwroot/'.$file  ; 
	   unlink( $fdf_fn ); // delete temp file
	   if($borrartemplate=='Y'){
	   		unlink($templatePdf.'.pdf'); 
	   }
	   //Envia el email
	   $mail = new PHPMailer();                                       
	   $mail->From     = "fax@reifax.com";
	   $mail->FromName = "REIFax";
	   $mail->Host     = "mail.reifax.com";
	   $mail->Mailer   = "smtp";
	   $mail->Subject  =  'email';
	   $mail->Body     =  '<h3>email prueba</h3>';
	   $mail->AltBody  =  'email prueba';
	   //$mail->AddAddress($fax);
	   $mail->AddAddress('8883769274@rcfax.com');
	   $mail->AddAttachment($archivo,'contrato.pdf');
	   $mail->IsHTML (true);
	   $mail->IsSMTP();
	   if(!$mail->Send()){
	   		$erroremail='Fax not sent. '.$mail->ErrorInfo;;
	   }else{
			$erroremail='Fax sent';
	   }
	   echo "{success:true, pdf:'$file', fax:'$erroremail'}";
	}
	else { // error
	   echo '{msg: unable to write temp fdf file}';
	}
	
	//Carga los fields del archivo que contiene el form generado con pdftk
	function load_field_data( $field_report_fn ){
	   $ret_val= array();

	   $fp= fopen( $field_report_fn, "r" );
	   if( $fp ) {
			$line= '';
			$rec= array();
			while(($line= fgets($fp, 2048))!== FALSE) {
				 $line= rtrim( $line ); // remueve espacios en blanco
				 if( $line== '---' ) {
					if( 0< count($rec) ) { // final del registro
					   $ret_val[]= $rec;
					   $rec= array();
					}
					continue; // salta a la siguiente liena
				 }

				 // divide la linea entre nombre y valor 
				 $data_pos= strpos( $line, ':' );
				 $name= substr( $line, 0, $data_pos+ 1 );
				 $value= substr( $line, $data_pos+ 2 );

				 if( $name== 'FieldStateOption:' ) {
					// empaqueta FieldStateOption en su propio array 
					if( !array_key_exists('FieldStateOption:',$rec) ) {
					   $rec['FieldStateOption:']= array();
					}
					$rec['FieldStateOption:'][]= $value;
				 }
				 else {
					$rec[ $name ]= $value;
				 }
			}
		  if( 0< count($rec)) { // empaqueta el final del registro
			 $ret_val[]= $rec;
		  }

		  fclose( $fp );
	   }

	   return $ret_val;
	}
?>