  
  <? 
  
  $parcelid=$_GET['pid'];
    $county=$_GET['county'];
	
  function eliminarDir($carpeta){ 
  
  foreach(glob($carpeta."/*") as $archivos_carpeta) 
  { //echo $archivos_carpeta; 
  if(is_dir($archivos_carpeta)) eliminarDir($archivos_carpeta); 
  else unlink($archivos_carpeta); } 
  
  rmdir($carpeta); 
  }

  //// mantenimiento de carpetas temporales
  $userid=$_COOKIE['datos_usr']['USERID'];
//  
//  if (!is_dir('img/pic'.$userid.'/')) {
//	   mkdir('img/pic'.$userid.'/',0777);
//       }
//	
//	 if (!is_dir('img/thumbs'.$userid.'/')) {
//	    mkdir('img/thumbs'.$userid.'/',0777);
//	}
	
  if (is_dir('img/pic'.$userid.'/')) {
	  $dir='img/pic'.$userid.'/';
	  eliminarDir($dir);
	   mkdir('img/pic'.$userid.'/',0777);
      // rmdir('img/pic'.$userid.'/');
	   }else{
    mkdir('img/pic'.$userid.'/',0777);
	}
	
	 if (is_dir('img/thumbs'.$userid.'/')) {
	  $dir='img/thumbs'.$userid.'/';
	  eliminarDir($dir);
	   mkdir('img/thumbs'.$userid.'/',0777);
       //rmdir('img/thumbs'.$userid.'/');
	    }else{
    mkdir('img/thumbs'.$userid.'/',0777);
	}
 ?>


    <script type="text/javascript">
	
	
    Ext.onReady(function(){
        var store = new Ext.data.JsonStore({
            //url: 'get-images.php',
            proxy: new Ext.data.HttpProxy({
                url: 'byowner/get-images.php', method: 'POST'
            }),
            root: 'images',
            fields: [
                'name', 'url',
                { name: 'size', type: 'float' },
                { name: 'lastmod', type: 'date', dateFormat: 'timestamp' },
                'thumb_url'
            ]
        });
        store.load();

        var tpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="thumb-wrap" id="{name}">',
                '<div class="thumb"><img src="byowner/{thumb_url}" title="{name}"></div>',
                '<span class="x-editable">{shortName}</span></div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        );

        var tplDetail = new Ext.XTemplate(
            '<div class="details">',
				'<tpl for=".">',
					'<img src="byowner/{thumb_url}"><div class="details-info">',
					'<b>Image Name:</b>',
					'<span>{name}</span>',
					'<b>Size:</b>',
					'<span>{sizeString}</span>',
					'<b>Last Modified:</b>',
					'<span>{dateString}</span>',
                    '<span><a href="{url}" target="_blank">view original</a></span></div>',
				'</tpl>',
			'</div>'
        );

        var tbar = new Ext.Toolbar({
            style: 'border:1px solid #99BBE8;'
        });
        
        tbar.add('->', {
            text: 'Delete',
            icon: 'byowner/img/delete.png',
            handler: function() {
                var records = datav.getSelectedRecords();
                if (records.length != 0) {
                    var imgName = '';
                    for (var i = 0; i < records.length; i++) {
                        imgName = imgName + records[i].data.name + ';';
                    }
                    Ext.Ajax.request({
                       url: 'byowner/delete.php',
                       method: 'post',
                       params: { images: imgName},
                       success: function() {
                           store.load();
                       }
                    });
                }
            }
        });

        var datav = new Ext.DataView({
            autoScroll: true, store: store, tpl: tpl,
            autoHeight: true, height: 300, multiSelect: true,
            overClass: 'x-view-over', itemSelector: 'div.thumb-wrap',
            emptyText: 'No images to display',
            style: 'border:1px solid #99BBE8; border-top-width: 0',

//            plugins: [
//                new Ext.DataView.DragSelector(),
//            ],
/*
            prepareData: function(data){
                data.shortName = Ext.util.Format.ellipsis(data.name, 15);
                data.sizeString = Ext.util.Format.fileSize(data.size);
                data.dateString = data.lastmod.format("m/d/Y g:i a");
                return data;
            },
*/
            listeners: {

                selectionchange: {
                    fn: function(dv,nodes){
                        var l = nodes.length;
                        var s = l != 1 ? 's' : '';
                        panelLeft.setTitle('Pictures Select ('+l+' image'+s+' selected)');
                    }
                },

                click: {
                    fn: function() {
                        var selNode = datav.getSelectedRecords();
                        tplDetail.overwrite(panelRightBottom.body, selNode[0].data);
                    }
                }
            }
        })

        var panelLeft = new Ext.Panel({
            id: 'images-view',
            frame: true,
            width: 960,
            height: 200,
            autoHeight: true,
            layout: 'auto',
            title: 'Gallery (0 images selected)',
            items: [tbar,datav]
        });
        panelLeft.render('centro');
		
       var pid = '<? echo $parcelid; ?>';
	   
        var panelRightTop = new Ext.FormPanel({
            title: 'Upload Images',
            width: 960,
            renderTo: 'arriba',
            buttonAlign: 'center',
            labelWidth: 50,
            fileUpload: true,
            frame: true,
            items: [{
				layout:'column',
				items:[{
                        columnWidth:.3,
                        layout: 'form',
                        items: [{
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 1',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						 }, {
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 2',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						}]
                  }, {
					  
					   columnWidth:.3,
                       layout: 'form',
                        items: [{
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 3',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						 }, {
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 4',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						}]
				  }, {
					  
					   columnWidth:.3,
                       layout: 'form',
                        items: [{
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 5',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						 }, {
						xtype: 'fileuploadfield',
						emptyText: '',
						fieldLabel: 'Image 6',
						buttonText: 'Select a File',
						width: 200,
						name: 'img[]'
						}]
                   }]
            }],
            buttons: [{
				
                text: 'Upload',
                handler: function() {
                    panelRightTop.getForm().submit({
                        url: 'byowner/upload.php?pid='+pid,
                        waitMsg: 'Uploading ....',
                        success: function(form, o) {
                            obj = Ext.util.JSON.decode(o.response.responseText);
                            if (obj.failed == '0' && obj.uploaded != '0') {
                                Ext.Msg.alert('Success', 'All files uploaded');
                            } else if (obj.uploaded == '0') {
                                Ext.Msg.alert('Success', 'Nothing Uploaded');
                            } else {
                                Ext.Msg.alert('Success',
                                    obj.uploaded + ' files uploaded <br/>' +
                                    obj.failed + ' files failed to upload');
                            }
                            //alert(obj.type);
						    panelRightTop.getForm().reset();
                            store.load();
                        }
                    });
                }
            }, {
                text: 'Reset',
                handler: function() {
                    panelRightTop.getForm().reset();
                }
            }]
        });

        var panelRightBottom = new Ext.Panel({
            frame: true,
            width: 960,
            height: 200,
            id: 'panelDetail',
            renderTo: 'abajo',
			hidden:true,
            tpl: tplDetail   //plantilla detalle
        });
    });
    </script>
   
    <style type="text/css">
        body {
            padding: 20px;
            margin: 0 auto;
        }
        #container {
            padding: 10px;
            background: #e3e3e3;
            border: 1px solid #d3d3d3;
            margin: 0 auto;
            width: 100%;
        }
        #centro {
            float: left;
        }
        #right {
            float: right;
        }
        #right-bottom {
            margin-top: 10px;
        }
        .clear {
            clear: both;
        }
        .details {
            padding: 10px;
            font-family: "Arial";
            font-size: 11px;
        }
        .details-info {
            margin-top: 10px;
        }
        .details-info span {
            display: block;
        }
        .details-info span a {
            color: #0066cc;
        }
        .details-info span a:hover {
            text-decoration: none;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body>
  <table width="100%" border="0" cellspacing="2" cellpadding="2" >
  <tr>
    <td width="69%"><span class="ti">Pictures the property</span></td>
    <td width="31%" align="center"><img src="img/apply.png" alt="Apply Changes" onClick="confirmar2('<? echo $county;?>');" /></td>
  </tr>
</table>
      <div id="container">
          <div id="arriba"></div>
          <div id="centro"></div>
          <div id="abajo"></div>
          <div class="clear"></div>
      </div>
  </body>

