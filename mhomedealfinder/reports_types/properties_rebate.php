<?php
$_GET['resultType']='basic';
$_GET['systemsearch']=isset($_POST['systemsearch']) ? $_POST['systemsearch'] : (isset($_GET['systemsearch']) ? $_GET['systemsearch']:'basic');
$_POST['ResultTemplate']=-1;
include('../coresearch.php');

$rest_total=array();
$query=substr($query,0,strpos($query,'LIMIT'));
$query=substr($query,strpos($query,'FROM'),strlen($query));
$query='SELECT p.parcelid '.$query;

$result=mysql_query($query) or die($query.mysql_error());

while($r=mysql_fetch_array($result))
	$rest_total[]=$r['parcelid'];

$_SERVERXIMA="http://www.reifax.com/";

$typeDiscount = $_GET['typeTab']=='foreclosures' ? 'P' : 'F'; 

?>
<div id="report_content">
<?php 
		
		echo '<h1 align="center" >DISCOUNT REPORT</h1>';
		$latlong=0;
		$rebatequery=' p.parcelid IN (\''.implode("','",$rest_total).'\')';
		

		function porcentaje($val){
			return number_format($val, 2, '.', '');
		}
		
		$SPS=$CPS=$SFS=$CFS=0;
		$SPSAD=$CPSAD=$SPSAR1=$SPSAR2=$CPSAR1=$CPSAR2=0.00;
		$SFSADF=$CFSADF=$SFSADJ=$CFSADJ=$SFSAR1=$SFSAR2=$CFSAR1=$CFSAR2=0.00;

		//Pre-Foreclosed
		if($typeDiscount=='P'){
			$query="SELECT count(*) FROM marketvalue m INNER JOIN psummary p ON (m.parcelid=p.parcelid) LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid) 
			WHERE m.pendes='P' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SPS=$r[0];
			
			$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.file_date,'%Y%m%d')))/count(*) 
					FROM pendes pe
					INNER JOIN marketvalue m ON (pe.parcelid=m.parcelid)
					INNER JOIN psummary p ON (pe.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (pe.parcelid=ll.parcelid)
					WHERE m.pendes='P' and m.sold='S' and length(pe.file_date)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SPSAD=$r[0];
			
			$query="SELECT sum(m.totalpenmort)/count(*)
					FROM marketvalue m 
					INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid)
					WHERE m.pendes='P' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SPSAR1=$r[0];
	
			$query="SELECT sum(p.saleprice)/count(*)
					FROM psummary p
					INNER JOIN marketvalue m ON (p.parcelid=m.parcelid) 
					LEFT JOIN latlong ll ON (p.parcelid=ll.parcelid)
					WHERE m.pendes='P' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SPSAR2=$r[0];
		}
		
		//Foreclosed
		if($typeDiscount=='F'){
			$query="SELECT count(*) FROM marketvalue m 
					INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid)
					WHERE m.pendes='F' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SFS=$r[0];

			$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.file_date,'%Y%m%d')))/count(*)
					FROM pendes pe
					INNER JOIN marketvalue m ON (pe.parcelid=m.parcelid)
					INNER JOIN psummary p ON (pe.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (pe.parcelid=ll.parcelid)
					WHERE m.pendes='F' and m.sold='S' and length(pe.file_date)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SFSADF=$r[0];
			
			$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.judgedate,'%Y%m%d')))/count(*)
					FROM pendes pe
					INNER JOIN marketvalue m ON (pe.parcelid=m.parcelid)
					INNER JOIN psummary p ON (pe.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (pe.parcelid=ll.parcelid)
					WHERE m.pendes='F' and m.sold='S' and length(pe.judgedate)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SFSADJ=$r[0];
			
			$query="SELECT sum(m.totalpenmort)/count(*)
					FROM marketvalue m
					INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
					LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid)
					WHERE m.pendes='F' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SFSAR1=$r[0];
			
			$query="SELECT sum(p.saleprice)/count(*)
					FROM psummary p
					INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
					LEFT JOIN latlong ll ON (p.parcelid=ll.parcelid)
					WHERE m.pendes='F' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			$SFSAR2=$r[0];
		}
?>	
    <div style='font-family:Trebuchet MS,Arial,Helvetica,sans-serif;'>
        <table align='center' cellpadding=0 cellspacing=0 style='font-family:Trebuchet MS,Arial,Helvetica,sans-serif; font-size:140%; width: 450px;'>
        	<tr class=mortcelltitulo style='font-weight:bold'>
            	<td colspan=2 align=center>Subject Area</td>
           	</tr>
            <tr class=mortcelltitulo style='font-weight:bold'>
            	<td align=center></td>
                <td align=center style='width:90'><strong>Quantity</strong></td>
           	</tr>
            <?php if($typeDiscount=='P'){?>
            <tr>
            	<td >Pre-Foreclosed Sold</td>
                <td align=right><?php echo $SPS;?></td>
           	</tr>
            <tr>
            	<td >Average Days of Shortsale</td>
                <td align=right><?php echo porcentaje($SPSAD);?></td>
           	</tr>
            <tr>
            	<td >Average Amount of Discount</td>
                <td align=right><?php echo porcentaje($SPSAR1-$SPSAR2);?></td>
           	</tr>
            <tr>
            	<td >Average Percentage of Discount</td>
                <td align=right><?php if($SPSAR1>0) echo porcentaje(100-(($SPSAR2/$SPSAR1)*100)).'%'; else echo '0.00%';?></td>
           	</tr>
            <tr class=mortcelltitulo style='height:2px'>
            	<td colspan=3 align=right>&nbsp;</td>
           	</tr>
            <?php }if($typeDiscount=='F'){?>
            <tr>
            	<td >Foreclosed Sold</td>
                <td align=right><?php echo $SFS;?></td>
           	</tr>
            <tr>
            	<td >Average Days of Sale After File Date</td>
                <td align=right><?php echo porcentaje($SFSADF);?></td>
           	</tr>
            <tr>
            	<td >Average Days of Sale After Judgement Date (Auction)</td>
                <td align=right><?php echo porcentaje($SFSADJ);?></td>
           	</tr>
            <tr>
            	<td >Average Amount of Discount</td>
                <td align=right><?php echo porcentaje($SFSAR1-$SFSAR2);?></td>
           	</tr>
            <tr>
            	<td >Average Percentage of Discount</td>
                <td align=right><?php if($SFSAR1>0) echo porcentaje(100-(($SFSAR2/$SFSAR1)*100)).'%'; else echo '0.00%';?></td>
           	</tr>
            <tr class=mortcelltitulo style='height:2px'>
            	<td colspan=3 align=right>&nbsp;</td>
           	</tr>
            <?php }?>
        </table>
    </div>
<br>
	<div id="grid-discount<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?>"></div>
</div>
<script>

<?php
	if($typeDiscount=='P'){
		$query="SELECT p.address,(SELECT mtg_lender FROM mortgage WHERE parcelid=m.parcelid limit 1) mtg_lender,m.totalpenmort,p.saleprice 
		FROM marketvalue m 
		INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
		LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid) 
		WHERE m.pendes='P' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	}else{
		$query="SELECT p.address,(SELECT mtg_lender FROM mortgage WHERE parcelid=m.parcelid limit 1) mtg_lender,m.totalpenmort,p.saleprice 
		FROM marketvalue m 
		INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
		LEFT JOIN latlong ll ON (m.parcelid=ll.parcelid) 
		WHERE m.pendes='F' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	}	
	$result=mysql_query($query) or die($query.mysql_error());
		
	$data='[';
	while($r=mysql_fetch_array($result)){
		if($data!='[') $data.=',';
		$data.= "['".$r['address']."','".$r['mtg_lender']."',".$r['totalpenmort'].",".$r['saleprice'].",".($r['totalpenmort']-$r['saleprice'])."]";
	}
	$data.=']';
?>

var dataRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?> = <?php echo $data;?>;

var storeRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?> = new Ext.data.ArrayStore({
	fields: [
		{name: 'address'},
		{name: 'lender'},
		{name: 'deuda', type: 'float'},
		{name: 'venta', type: 'float'},
		{name: 'diff', type: 'float'}
	]
});


var gridRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?> = new Ext.grid.GridPanel({
	renderTo: 'grid-discount<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?>',
	width: 'auto',
	height: 400,
	store: storeRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?>,
	border: false,
	columns: [
		{
			header: 'Address',
			width: 150,
			dataIndex: 'address',
			sortable : true
		},
		{
			header: 'Lender',
			width: 150,
			dataIndex: 'lender',
			sortable : true
		},
		{
			header: 'Know Debt',
			width: 100,
			dataIndex: 'deuda',
			renderer: 'usMoney',
			sortable : true
		},
		{
			header: 'Sale Price',
			width: 100,
			dataIndex: 'venta',
			renderer: 'usMoney',
			sortable : true
		},
		{
			header: 'Difference',
			width: 100,
			dataIndex: 'diff',
			renderer: 'usMoney',
			sortable : true
		}
	],
	enableColLock: false
});

storeRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?>.loadData(dataRebate<?php echo '_'.$_GET['typeTab'].$_GET['subTypeTab'];?>);

if(document.getElementById('tabs')){
	if(document.getElementById('report_content').offsetHeight > tabs.getHeight()){
		tabs.setHeight(document.getElementById('report_content').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content').offsetHeight+110);
	}
}
</script>