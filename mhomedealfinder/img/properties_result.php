<?php  
session_start();
$_SESSION["SERVER"]='www.ximausa.com';
$_SESSION["FOLDER"];
$_SERVERXIMA="http://www.ximausa.com/";

// funcion que devuelve un array con los elementos de una hora
	function parteHora( $hora )
    {    
    	$horaSplit = explode(":", $hora);
		if( count($horaSplit) < 3 )
        {
        	$horaSplit[2] = 0;
		}
		return $horaSplit;
	}

    // funcion que devuelve la resta de dos horas en formato string
    function RestaHoras( $time1, $time2 )
    {
        list($hour1, $min1, $sec1) = parteHora($time1);
        list($hour2, $min2, $sec2) = parteHora($time2);
        return date('H:i:s', mktime( $hour1 - $hour2, $min1 - $min2, $sec1 - $sec2));
    }  	

	function tiempo()
	{
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		return $mtime;
	}
	
	function url_exists($url)
	{
		$url_info = parse_url($url);
		
		if (! isset($url_info['host']))
			return false;
		
		$port = (isset($url_info['post'])?$url_info['port']:80);
		
		if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr)) 
			return false;
		
		$uri = @$url_info['path'] . '?' . @$url_info['query'];
		
		$http = "HEAD $uri HTTP/1.1\r\n";
		$http .= "Host: {$url_info['host']}\r\n";
		$http .= "Connection: close\r\n\r\n";
		
		@fwrite($hwnd, $http);
		
		$response = fgets($hwnd);
		$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";
		
		fclose($hwnd);
		
		if (preg_match($response_match, $response, $matches)) {
			//print_r($matches);
			if ($matches[1] == 404)
				return false;
			else if ($matches[1] == 200)
				return true;
			else
				return false;
			 
		} else {
			return false;
		}
	}
	
	$TIMEINIHORA=date("H:i:s");
	$TIMETOTALINI=tiempo();
	
	if(isset($_POST['search'])) $_SESSION['search']=$_POST['search'];
	else $_POST['search']=$_SESSION['search'];
	
	include('coresearch.php');
	$TIMETOTALFIN=tiempo();
	$TIMETOTALSEG=$TIMETOTALFIN-$TIMETOTALINI;
	
	
	$max_page_pagin=15;
	$num_page_pagin=ceil($num_rows_all/$num_rows);
	if($max_page_pagin>$num_page_pagin) $max_page_pagin=$num_page_pagin;
	$star_for=floor($num_limit_page/$max_page_pagin)*$max_page_pagin;
	$end_for=$star_for+$max_page_pagin;
	
	$star_page=($num_limit_page*$limit_cant_reg)+1;
	
	
	
	$query='SELECT p.parcelid,p.beds,p.bath,if(p.lsqft>0,p.lsqft,p.bheated) sqft,p.ccoded,p.ccode,concat(i.url,i.url2,if(i.letra="Y",i.mlnumber,substring(i.mlnumber,2)),".",i.tipo) imagen, if(length(i.url)>0,"Y","N") tieneImagen 
			FROM psummary p 
			LEFT JOIN imagenes i ON (p.parcelid=i.parcelid) 
			WHERE p.parcelid IN ("'.implode('","',$list_parcelid).'")';
	//echo $query;
	$list_reg_content=array();
	$list_reg_aux=array();
	$i=0;
	foreach($list_county as $k => $val){
		mysql_select_db('fl'.str_replace(' ','',$val));
		
		$rest=mysql_query($query) or die($query.mysql_query());
		
		while($r=mysql_fetch_array($rest,MYSQL_ASSOC)){
			$list_reg_content[]=$r;
			
			if($r['tieneImagen']=='Y' && !url_exists($r['imagen'])) $list_reg_content[$i]['tieneImagen']='N'; 
			
			$key=array_search($r['parcelid'],$list_parcelid);
			
			if($r['ccode']=='01') $module=array(module=>'PS_SF');
			else $module=array(module=>'PS_CONDOS');
			
			$list_reg_aux[]=array_merge($list_reg[$key],$list_reg_content[$i],$module);
			$i++;
		}
	}
	
	$list_reg=$list_reg_aux;
	//print_r($list_reg);
?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Language" content="EN-US"> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="robots" content="all">
<meta name="rating" content="General">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="2 days">
<meta name="category" content="Real Estate Search">
<meta name="author" content="Francisco Mago">
<meta name="reply-to" content="info@ximausa.com">
<meta name="copyright" content="XIMA, LLC - 2007">
<meta name="expires" content="never">

<script type="text/javascript" src="<?php echo $_SERVERXIMA.'MANT/LIB/ext/adapter/ext/ext-base.js';?>"></script>
<script type="text/javascript" src="<?php echo $_SERVERXIMA.'MANT/LIB/ext/ext-all.js';?>"></script>
<script type="text/javascript" src="<?php echo $_SERVERXIMA.'includes/properties_generator.js';?>"></script>
<script type="text/javascript" src="<?php echo $_SERVERXIMA.'includes/advertising.js';?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $_SERVERXIMA.'MANT/LIB/ext/resources/css/ext-all.css';?>" />
<link href="<?php echo $_SERVERXIMA.'includes/css/layout2.css';?>" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SERVERXIMA.'includes/css/properties_css.css';?>" rel="stylesheet" type="text/css" />

<style type="text/css">
	.box_result {
		background-image: url(<?php echo $_SERVERXIMA."/";?>img/bkgd_result.jpg); 
		width: 550px; 
		height: 175px; 
		margin-bottom: 2px; 
		border: 1px solid #FFF; 
		border-top: 1px solid #b8dae3;
	}
	.box_result_hover {
		background-image: url(<?php echo $_SERVERXIMA."/";?>img/bkgd_result_over.jpg); 
		width: 550px; 
		height: 175px; 
		margin-bottom: 2px; 
		border: 1px solid #b8dae3; 
		border-top: 1px solid #b8dae3;
	}
	.paginationstyle{
		margin-top:5px;
		margin-bottom:10px;
	}
	.paginationstyle a{
		padding: 2px 5px 2px 5px;
		margin-right: 2px;
		border: 1px solid #ddd;
		text-align:center;
		text-decoration: none; 
		color: #88AF3F;
		background-color: #FFFFFF;
	}
	.paginationstyle a:hover {
		border:1px solid #85BD1E;
		color: #638425;
		background-color: #F1FFD6;
	}
	.paginationstyle span.current{
		padding: 2px 5px 2px 5px;
		margin-right: 2px;
		border: 1px solid #B2E05D;
		font-weight: bold;
		background-color: #B2E05D;
		color: #FFF;
	}
	.paginationstyle span.disableds{
		padding: 2px 5px 2px 5px;
		margin-right: 2px;
		border: 1px solid #f3f3f3;
		color: #ccc;
	}
</style>
</head>

<body onLoad="loadAdvertising(<?php if(isset($_SESSION['datos_usr']) && $_SESSION['datos_usr']['EMAIL']!='guest'){ echo 'true,'; echo '\''.$_SESSION['datos_usr']['NAME'].'\','; echo '\''.$_SESSION['datos_usr']['SURNAME'].'\''; }else{ echo 'false,\'\',\'\'';}?>);">
<div align="center">
	<div id="body_central">
        <div id="layout_center" align="center">
            <div align="center" style="width:660px; margin:auto;">
                <div id="tabs">
                	<?php 
							echo '<div style=" font-size:12px;"><span style="font-weight:bold;">Properties Found:</span> '.$num_rows_all." (".$TIMETOTALSEG." seconds)</div>";
							
							echo '<div class="paginationstyle">';
                            	if($num_limit_page>0) echo '<a href="properties_result.php?page='.($num_limit_page-1).'">�Previous</a>'; 
								else echo '<span class="disableds">�Previous</span>';
								
								for($i=$star_for; $i<$end_for; $i++){
									if($i==$num_limit_page) echo '<span class="current">'.($i+1).'</span>';
									else echo '<a href="properties_result.php?page='.($i).'">'.($i+1).'</a>';
								}
								
                                if($num_limit_page!=($num_page_pagin-1)) echo '<a href="properties_result.php?page='.($num_limit_page+1).'">Next�</a>'; 
								else echo '<span class="disableds">Next�</span>';
                           	echo '</div>';
							
							echo '<table border="0" cellpadding="0" cellspacing="10" style=" width:570px;">';
							foreach($list_reg as $k => $val){?>
								<tr id="box_result_<?php echo $k;?>" class="box_result" align="center" onMouseOver="this.className='box_result_hover'; document.getElementById('view_result_<?php echo $k;?>').style.display='';" onMouseOut="this.className='box_result';document.getElementById('view_result_<?php echo $k;?>').style.display='none';">
										<td style="vertical-align: top;">
											<div id="title_result" style=" height:30px; padding-left: 10px; padding-top: 3px; font-size: 16px; float:left;"><a href="<?php echo $val['url'];?>" style="font-weight:normal; margin-left:5px; text-decoration:none; color: #0066CC;" ><?php echo $val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].', '.$val['county'];?></a></div>
											<div style=" float:right; font-weight:bold; font-size:14px; margin-right:30px; margin-top:10px;"><?php echo ($star_page+$k);?></div>
											<div id="content_result_<?php echo $k;?>" style=" height:115px; padding-left: 10px; clear:both; text-align:left;" >
                                                <div style="float:left; border: medium solid #b8dae3; margin-right:20px; padding-top:2px; padding-left:2px;">
                                                	<img src="<?php if($val['tieneImagen']=='Y') echo $val['imagen']; else echo $_SERVERXIMA.'img/nophotocasa.jpg'; ?>" height="105px">
                                                </div>                                               
                                                <table border="0" cellpadding="0" cellspacing="1px" style="font-size:14px;">
                                                	<tr>
                                                		<td style="font-weight:bold;"><?php echo $val['beds'];?> Beds / <?php echo str_replace('.0','',$val['bath']);?> Baths</td>
                                                   	</tr>
                                                    <tr>
                                                		<td style="font-weight:bold;"><?php echo $val['sqft'];?> Sqft</td>
                                                   	</tr>
                                                    <tr>
                                                		<td style="font-weight:bold;"><?php echo $val['ccoded'];?></td>
                                                   	</tr>
                                                </table>
                                            </div>
											<div id="view_result_<?php echo $k;?>" style=" padding-left: 10px; display: none; text-align:left; vertical-align:middle;">
                                            	<a href="<?php echo 'properties_reports.php?type=distress&module='.$val['module'].'&db='.'fl'.str_replace(' ','',$val['county']).'&pid='.$val['parcelid'];?>" target="_blank" style="text-decoration:none; color:#000; font-weight:bold; font-size:10px;"><img src="<?php echo $_SERVERXIMA.'img/ximaicon/distress.png';?>" height="15px;" align="absmiddle"> Distress Report</a>
                                                <a href="<?php echo 'properties_reports.php?type=par&module='.$val['module'].'&db='.'fl'.str_replace(' ','',$val['county']).'&pid='.$val['parcelid'];?>"  target="_blank" style="text-decoration:none; color:#000; font-weight:bold; font-size:10px;"><img src="<?php echo $_SERVERXIMA.'img/ximaicon/ssale.png';?>" height="15px;" align="absmiddle"> Property Analisys</a>
                                                <a href="<?php echo 'properties_reports.php?type=bpo&module='.$val['module'].'&db='.'fl'.str_replace(' ','',$val['county']).'&pid='.$val['parcelid'];?>"  target="_blank" style="text-decoration:none; color:#000; font-weight:bold; font-size:10px;"><img src="<?php echo $_SERVERXIMA.'img/ximaicon/bpo.png';?>" height="15px;" align="absmiddle"> BPO Report</a>
                                                <a href="<?php echo 'properties_reports.php?type=xray&module='.$val['module'].'&db='.'fl'.str_replace(' ','',$val['county']).'&pid='.$val['parcelid'];?>"  target="_blank" style="text-decoration:none; color:#000; font-weight:bold; font-size:10px;"><img src="<?php echo $_SERVERXIMA.'img/ximaicon/exray.png';?>" height="15px;" align="absmiddle"> XRay Report</a>
                                                <!--<div style="float:right; padding-right:10px;"><a href="<?php echo $val['url'];?>" style="text-decoration:none; color:#000; font-weight:bold; font-size:12px;">view details �</a></div>-->
                                           	</div>
										</td>
									</tr>  
							<?php }
							echo '</table>';
					?>
                </div>
           	</div>
        </div>
        
        <div id="layout_top"><div id="T1" style="height:110px; vertical-align:middle;"></div></div>
        
        <div id="layout_bottom"><div id="B1" style="height:110px; vertical-align:middle;"></div></div>
        
        <div id="layout_left"></div>
        
        <div id="layout_right"></div>
	</div>
</div>


<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7654830-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<script>
var tabs=viewport=null;
Ext.onReady(function(){

	/*tabs = new Ext.TabPanel({
        renderTo: 'tabs',
        activeTab: 0,
        width:650,
		height: 1200,
        plain:true,
		enableTabScroll:false,
        defaults:{	autoScroll: false
				},
        items:[
			  {
				title: ' Result ',
        		html: ''
			  }
        ]
    });*/
	
	
	//alert(document.width+' '+document.body.offsetWidth);
	var ancho=document.body.offsetWidth;
	if(ancho>1260) ancho=1260;
	
	 viewport = new Ext.Panel({
		  renderTo: 'body_central',
		  id: 'viewport',
		  layout:'border',
		  monitorResize: true,
		  hideBorders: true,
		  width: ancho,
		  height: 2200,
		  items:[
			{
				region: 'north',
				id: 'north_panel',
				layout: 'hbox',
				height: 120,
				minSize: 120,
				maxSize: 120,
				collapsible: false,
				contentEl: 'layout_top'
			},
			{
				region: 'south',
				id: 'south_panel',
				layout: 'hbox',
				height: 120,
				minSize: 120,
				maxSize: 120,
				collapsible: false,
				contentEl: 'layout_bottom'
			},
			{
				region: 'east',
				id: 'east_panel',
				layout: 'vbox',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapsible: true,
				collapseMode: 'mini',
				contentEl: 'layout_right',
				listeners: {beforeexpand: function(){return false;}}
			},
			{
				region: 'west',
				id: 'west_panel',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapsible: true,
				collapseMode: 'mini',
				contentEl: 'layout_left',
				listeners: {beforeexpand: function(){return false;}}
			},
			{
				region: 'center',
				id: 'center_panel',
				layout: 'hbox',
				layoutConfig:{align:'middle'},
				collapsible: false,
				contentEl: 'layout_center'
			}
		  ]
	});
	
	if(document.body.offsetWidth < 1200){
		 var west = Ext.getCmp('west_panel');
		 west.collapse();
	}
});
</script>
</body>
</html>