//************************ [ Maria Oliveira]
//FUNCION LLAMADA EN onblur EN LA PAGINA myaccount.php EN EL CAMPO email
//COMPRUEBA QUE EL EMAIL NO EXISTA EN LA BASE DE DATOS
	
function MostrarError(action, value1, value2){
//Para EMAIL:  value1 = valor del input de Email actual del usuario
//Para PASSWORD:  value1 = valor del input del Email del usuario ;  value2= valor del password actual del usuario para ese email
var parameters='';
var message;
	switch(action){
		case 'email': case 'checkEmail': 
			parameters+="action="+action+"&value1="+value1; 
			message=document.getElementById("errorEmail");
		break;
		
		case 'password': case 'pass_email':
			parameters+="action="+action+"&value1="+value1+"&value2="+value2; 
			if(action=='pass_email') message=document.getElementById("errorEmail"); else message=document.getElementById("errorPass");
		break;		
		
		case 'checkNick':
			parameters+="action="+action+"&value1="+value1; 
			message=document.getElementById("errorNick");
		break;
	}
	
	if(value1!=""){
		ajax=objetoAjax();
		ajax.open("POST", "includes/errormyaccount.php",true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send(parameters);
		
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				var resultado=ajax.responseText;
				
				if(resultado!="" ){
					document.getElementById("submit").disabled=true; 
				
				}else if(resultado==""){
					document.getElementById("submit").disabled=false;
				}
				message.innerHTML=resultado;
			}
		}
	}//Fin de If
	else{
		message.innerHTML='';
	}
}


