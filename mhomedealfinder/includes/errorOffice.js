//FUNCION LLAMADA EN onblur EN LA PAGINA register.php EN EL CAMPO Officenia cuando es Realtor
//COMPRUEBA QUE LA Oficina DE REALTOR EXISTA EN LA BASE DE DATOS

function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(E){
				xmlhttp = false;
			}
		}
		if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
			xmlthttp = new XMLHttpRequest();
		}
		return xmlhttp;
}
	
function checkOfficeNumber(id){
	var error=document.getElementById("msgONumber");
	var office= document.getElementById(''+id+'').value;
	var msg;
	
	msg="Please, enter a valid Office Number";
	
	//if(office==""){
		//alert(msg)
		//return false;
	//}else
	if(office!='' && document.getElementById("rRealtor").checked==true && document.getElementById("officey").checked==true){
		ajax=objetoAjax();
		ajax.open("POST", "includes/errorOffice.php",true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("office="+office);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				var resultado=ajax.responseText;
				if(resultado!="") 
					error.style.visibility="visible"; 
				else 
				{
					error.style.visibility="hidden";
					//MostrarProductos();
				}
				if(resultado!="")	document.getElementById("btnsubmit").disabled=true; 
			else if(resultado=="") document.getElementById("btnsubmit").disabled=false;
			
				error.innerHTML=resultado;
			}
		}
	}
	else{
		error.innerHTML=''
		error.style.visibility="hidden";
	}
}
