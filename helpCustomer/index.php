<?php
require '../properties_conexion.php';
	conectar();

function getChild($id){
	$sql='SELECT * FROM training_modulo WHERE father='.$id;
	$res=mysql_query($sql);
	$menu=array();
	while($data=mysql_fetch_assoc($res)){
		if($data['is_tree']==1){
			$childs=getChild($data['id']);
			$respuesta=array( 'text' => $data['name'], 'expanded' => true, 'children' => $childs);
		}
		else{
			$respuesta=array( 'text' => $data['name'], 'leaf' => true);
		}
		array_push($menu, $respuesta);
	}
	return $menu;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Help!</title>
<link href="/mant/lib/Ext4.2/resources/css/ext-all-gray-debug.css" type="text/css" rel="stylesheet">
<style type="text/css">
.resaltado{
	background:#D9E3CA !important;
	font-weight:bold !important;
}
.thumb-wrap{
	background: none repeat scroll 0 0 #F2F2F2;
	border: 1px solid #a0a0a0;
	color: #3C3C3C;
	float: left;
	height: 120px;
	margin-left: 10px;
	position: relative;
	width: 150px;
}
.thumb-wrap .title{
	position:absolute;
	bottom:0px;
	height:50px;
	width:100%;
	background:#FFF;
	text-align:center;
	border-top: 1px solid #a0a0a0;
}
.thumb-wrap:hover,.thumb-wrap.active{
	background: none repeat scroll 0 0 #E0E0E0;
	border: 1px solid #444444;
}
.thumb-wrap:hover .title,.thumb-wrap.active .title{
	background: none repeat scroll 0 0 #444444;
	color: #FFFFFF;
	font-weight: bold;
}
.x-icon-movie {background:url(/backoffice/images/icon-video.png) 0 0 no-repeat !important;}
</style>
</head>

<body>
	<span id="loading-msg">Cargando imagenes y estilos...</span>
    <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading Core API...';</script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="/mant/lib/Ext4.2/ext-all.js"></script>
    <script type="text/javascript" src="/mant/lib/Ext4.2/ext-theme-gray.js"></script>
    <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading interface components...';</script>
    <script>
	var typeHelp=2;
	var menuHelp= <?php
		
		$sql='SELECT * FROM training_modulo WHERE father IS NULL';
		$res=mysql_query($sql) or die (mysql_error());
		$tree=array();
		while($data=mysql_fetch_assoc($res)){
			$childs=getChild($data['id']);
			$tem=array( 'text' => $data['name'], 'expanded' => true, 'children' => $childs);
			array_push($tree,$tem);
		}
		echo json_encode($tree);
	?>;
	</script>
	<script type="text/javascript" src="/resources/js/help.js"></script>
    <script type="text/javascript">
		document.getElementById('loading-msg').innerHTML = 'Initializing...';
    </script>
</body>
</html>
