<?php
	function sanitize($string = '', $is_filename = FALSE){
		// Replace all weird characters with dashes
		$string = preg_replace('/[^\w\-'. ($is_filename ? '~_\.' : ''). ']+/u', '-', $string);
		$string = str_replace('&','-',$string);
		// Only allow one dash separator at a time (and make string lowercase)
		return mb_strtolower(preg_replace('/--+/u', '-', $string), 'UTF-8');
	}
	
	$url_county 	= $_GET['county'];
	$url_city 		= sanitize($_GET['city']);
	$url_zip 		= sanitize($_GET['zip']);
	$url_address 	= sanitize($_GET['address']);
	
	$url_city = $url_city=='NOCITY' ? 'NO-CITY' : $url_city;
	$url_zip = $url_zip=='NOZIP' ? 'NO-ZIP' : $url_zip;
	$url_address = $url_address=='NOADDRESS' ? 'NO-ADDRESS' : $url_address;
	
	$county = trim(str_replace('-',' ',$url_county)); 
	$city = trim(str_replace('-',' ',$url_city)); 
	$zip = trim(str_replace('-',' ',$url_zip));
	$address = trim(str_replace('-',' ',$url_address));
	$pid = $_GET['pid'];
	
	if(strpos($pid,'.php')!==false){
		$php = explode('.',$pid);
		$add = explode('-',$php[0]);
		
		$pid = $add[(count($add)-1)];
	}
	
	require_once($_SERVER['DOCUMENT_ROOT']."/resources/template/template.php");
	require_once($_SERVER['DOCUMENT_ROOT'].'/resources/php/funcionesOverview.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	
	$db=conectarPorNameCounty($county);
	$sql_comparado="Select 
	p.state, p.address,p.city,p.zip,p.xcoded, m.remark, m.agent, m.officeemail, 
	p.xcode,p.beds,p.bath,p.sqft,p.price,ps.waterf,ps.pool,p.unit, 
	if(m.status is not null, m.status, ps.astatus) status, 
	l.latitude,l.longitude,ma.marketvalue,ma.debttv,p.pendes 
	FROM properties_php p
	LEFT JOIN psummary ps ON (p.parcelid=ps.parcelid)
	LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
	LEFT JOIN marketvalue ma ON (p.parcelid=ma.parcelid)
	LEFT JOIN latlong l ON (p.parcelid=l.parcelid)
	Where p.parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$r= mysql_fetch_array($res);
	
	$currentMOD='PS_';
	$state=$r['state'];
	$unit=$r['unit'];
	$xcoded=$r['xcoded'];
	$marketvalue=$r['marketvalue'];
	$address = $r['address'];
	
	$xcode=$r['xcode'];
	$beds=$r['beds'];
	$bath=$r['bath'];
	$sqft=$r['sqft'];
	$price=$r['price'];
	$pendesStatus=$r['pendes'];
	$debttv=$r['debttv'];
	$pendesTxt = ($pendesStatus=='N' ? 'Not Forclosures' : ($pendesStatus=='P' ? 'Pre-foreclosed' : 'Foreclosures'));
	$status = $r['status'];
	$statusTxt = ($status == 'A' ? 'For Sale / For Rent ' : ($status == 'CC' ? 'By Owner ' : ($status == 'CS' ? 'Closed Sale ' : 'Not For Sale / Not For Rent ')));
	$statusTxt .= ($pendesStatus=='N' ? '' : ($pendesStatus=='P' ? 'Pre-foreclosed' : 'Foreclosures'));
	
	
	
	$sql1="SELECT MORTGAGE.parcelid FROM MORTGAGE WHERE MORTGAGE.PARCELID='$pid'";
	$res1 = mysql_query($sql1) or die(mysql_error());
	$mortgage =mysql_num_rows($res1);
	
	$sql2="select pendes.parcelid from pendes where pendes.parcelid='$pid' and (pof='F' or pof='P')";
	$res2 = mysql_query($sql2) or die(mysql_error());
	$pendestab =mysql_num_rows($res2);	
	
?>
<!DOCTYPE HTML>
<html>
    <!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3(1,($address.', '.$city.', FL, '.$zip.' - REIFax.com'),('Home value and property details for '.$address.', '.$city.', FL, '.$zip.', '.$county.', '.$xcoded.', '.$pendesTxt.' - '.$sqft.' sq ft, '.$beds.' bedrooms, '.$bath.' bathrooms.'),($address.', '.$city.', FL, '.$zip.', '.$county.', '.$xcoded.', '.$pendesTxt.', real estate, real estate search, home value, property details.'));?>
<style>
	input[name=locationSearch]{
		width:400px;
	}
#tabDetailsOverview li {
  float: left;
  font-size: 1.1em;
  font-weight: 400;
  padding: 1%;
  width: 48%;
}
</style>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderSearch();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
        
        <a href="http://rosafranciamago.com/property-search" target="_blank">
            <img width="300" src="http://www.reifax.com/resources/img/advertise/publicidadproperties.jpg">
        </a>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Medium Rectangle -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:300px;height:250px"
             data-ad-client="ca-pub-3795268126364893"
             data-ad-slot="3282546543"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
            <img src="http://www.reifax.com/resources/img/advertise/D3.png">
        </a>
        <a href="http://www.reifax.com/company/contactUs.php">
            <img src="http://www.reifax.com/resources/img/advertise/D6.png">
        </a>
    </div>
    <!-- Center Content-->
    <div class="content" style="min-height:500px;">
        <div class="panel detailsTitle">
        	<div class="center">
              	<div id="map">
                 	
                </div>
                <div class="describe">
        	        <div class="title">
                    	 <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>
                    </div>
              		<div class="relevante">
						<?php echo $sqft.' sqft / '.$beds.' beds / '.$bath.' baths / '.$xcoded;?>    
                    </div>
                    <div class="price">
                    	<span>
                    		<?php 
                        	if($xcoded =='Single Family' || $xcoded=='Condo/Town/Villa'){
								if(round($marketvalue)>0)
									echo '$'.number_format(round($marketvalue),0,'.',',');
							}
							?>
                        </span>
                    </div>          
                </div>
                <div class="clear">
                </div>
            </div>
      	</div>
        <!-- inicio del overview//-->
        <div class="overviewDetails" style="width:670px; overflow:hidden;">
        	<div>
                 <ul class="css-tabs small" id="detailsOverview">
                    <li>
                        <a class="current" id="overviewDetailInit" href="#tabDetailsOverview">Details</a>
                    </li>
                    <li>
                        <a href="#tabComparablesOverview" rel="views/comparableDetails.php?xcode=+<?php echo $xcode ?>">Comparables</a>
                    </li>
                    <li>
                        <a class="" href="#tabComparablesAcOverview" rel="">Similar Properties For Sale</a>
                    </li>
                    <li>
                        <a class="" href="#tabDistressOverview" rel="">Foreclosures</a>
                    </li>
                    <li>
                        <a class="" href="#tabComparablesRenOverview" rel="">Rental Comparables</a>
                    </li>
                    <?php
							/*
						if($mortgage>0)
						{
							echo '
							<li>
								<a class="" href="#tabMortgageOverview" rel="">Mortgage</a>
							</li>';
						}
						if($pendes>0)
						{
							echo '
							<li>
								<a class="" href="#tabForeclosureOverview" rel="">Foreclosure</a>
							</li>
							';
						}*/
					?>
                 </ul>
             </div>
            <div class="panes mediunTabs">
            	<div id="tabDetailsOverview">
                <?php
				
					$db=conectarPorNameCounty($county);
					
					$sql_camptit="Select Tabla,Campos,Titulos,`Desc`,titulos_a FROM xima.camptit;";	
					$res = mysql_query($sql_camptit) or die(mysql_error());
					$Camptit_array=array();
					
					while($row=mysql_fetch_array($res, MYSQL_ASSOC))
						$Camptit_array[]= array($row["Tabla"],$row["Campos"],$row["Titulos"],$row["Desc"],$row["titulos_a"]);
					
					$sql_comparado="Select 
					status,lprice
					FROM mlsresidential 
					Where parcelid='$pid';";	
					$res = mysql_query($sql_comparado) or die(mysql_error());
					$myrow= mysql_fetch_array($res);
					
					$status_pro=$myrow['status'];
					$lprice_pro=$myrow['lprice'];	
				
					$sql_comparado="Select 
					latitude,longitude
					FROM latlong 
					Where parcelid='$pid';";	
					$res = mysql_query($sql_comparado) or die(mysql_error());
					$myrow= mysql_fetch_array($res);
					
					$latitude=$myrow['latitude'];
					$longitude=$myrow['longitude'];
					
					$map="psummary_mymap";
					$map2="psummary_mymapS";
					if(isset($par_no_conexion)){
						$map="Rpsummary_mymap";
						$map2="Rpsummary_mymapS";
					}
					if(isset($overview_comp)){
						$map="Cpsummary_mymap";
						$map2="Cpsummary_mymapS";
					}
					
					overviewDetails($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$realtor,$loged,$block);
?>
                
                
                </div>
            	<div id="tabComparablesOverview">
                </div>
            	<div id="tabComparablesAcOverview">
                </div>
            	<div id="tabDistressOverview">
                </div>
            	<div id="tabComparablesRenOverview">
                </div>
                <?php
							/*
						if($mortgage>0)
						{
							echo '
							<div id="tabMortgageOverview" class="restricted">
							</div>';
						}
						if($pendes>0)
						{
							echo '
							<div id="tabForeclosureOverview" class="restricted">
							</div>
							';
							
						}*/
					?>
            </div>
        </div>
       <!--Text container-->
        <div id="mapResult" style="display:none;width:100%;height:320;border: medium solid #b8dae3;position:relative;margin-bottom:5px;"></div>
        <input type="hidden" name="result_mapa_search_latlong" id="result_mapa_search_latlong" value="-1" />
				
        
    </div>
	    <div class="clear" style="margin-top:10px;">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script src="/resources/js/chosen.jquery.min.js"></script>
<script src="/includes/properties_draw.js"></script>

<script language="javascript">
	var dirPro="<?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>";
	var countyBuy="<?php echo $_COOKIE['county'] ?>";
	var parcelIdBuy="<?php echo $_COOKIE['parselid'] ?>";
	var user_web=false;
	
	$(document).ready(function (){
		$('#typeSerach').chosen({disable_search_threshold: 15});
		
		loadCounty();
		$('#buttonSearch').bind('click',initSearch);
		
		$('.returnResul').attr('href',$.cookie('urlResult'));
		loadPanel($('#detailsOverview'));
		var	map = new XimaMap('map','map_mapa_search_latlong','_control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_clear');
		map._IniMAPOverview(new Microsoft.Maps.Location(<?php echo $r['latitude'].','.$r['longitude'];?>),'birdseye');
		
			$('#tabComparablesOverview').load('/overview/views/comparableDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
			$('#tabComparablesAcOverview').load('/overview/views/comparableAcDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
			$('#tabDistressOverview').load('/overview/views/distressDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
			$('#tabComparablesRenOverview').load('/overview/views/comparableRenDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
			$('.restricted').load('/overview/views/restricted.php');
		/*$('#tabDetailsOverview').load('/overview/views/overviewDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>');
		$('#tabComparablesOverview').load('/overview/views/comparableDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
		//$('#tabComparablesAcOverview').load('/overview/views/comparableAcDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
		$('.restricted').load('/overview/views/restricted.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>');*/
		
		
		
	});
	
function initSearch(e)
{
	e.preventDefault();
	$('body').fadeOut(200);
	var parametrosEnviar='';
	var parametros=[
		{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
		{campo: 'county',	valor : ($('#countySearch').val())?$('#countySearch').val():''},
		{campo: 'tsearch', 	valor : 'location'},
		{campo: 'proptype', 	valor : ''},
		{campo: 'price_low', 	valor : ''},
		{campo: 'price_hi', 	valor : ''},
		{campo: 'bed', 	valor : -1},
		{campo: 'bath', 	valor : -1},
		{campo: 'sqft', 	valor : -1},
		{campo: 'pequity', 	valor : -1},
		{campo: 'pendes', 	valor : -1},
		{campo: 'search_mapa', 	valor : -1},
		{campo: 'search_type', 	valor : ($('#typeSerach').val())?$('#typeSerach').val():''},
		{campo: 'search_mapa', 	valor : '-1'},
		{campo: 'occupied', 	valor : -1}
	]
	$(parametros).each(function (index){
		parametrosEnviar+=this.campo+'='+this.valor+'&';
		});
	$.ajax({
		url		:'/properties_coresearch.php',
		type	:'POST',
		data	:parametrosEnviar,
		success	:function (resul){
			window.location='/result/index.php';
			}
	});
}
	
function loadCounty(){
	$.ajax({
		type	:'POST',
		url		:'/resources/php/properties.php',
		data	: "ejecutar=countylist&state=1",
		dataType:'json',
		success	:function (resul){
			$('.County,#SearchCounty,#countySearch').html('').append('<option value="">Select a county</option>');
			$(resul).each(function (index,data){
				$('.County,#SearchCounty,#countySearch').append('<option value="'+data.id+'">'+data.county+'</option>');
			});
			IniCounty.init($('#countySearch'),null);
			$('#countySearch').chosen();
		}
	})
};
</script>