<?php
	$tiempo_inicio = microtime(true);
	require_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");

	$url_county = $_GET['county'];
	$county = trim(str_replace('-',' ',$url_county));
	
	function sanitize($string = '', $is_filename = FALSE){
		// Replace all weird characters with dashes
		$string = preg_replace('/[^\w\-'. ($is_filename ? '~_\.' : ''). ']+/u', '-', $string);
		$string = str_replace('&','-',$string);
		// Only allow one dash separator at a time (and make string lowercase)
		return mb_strtolower(preg_replace('/--+/u', '-', $string), 'UTF-8');
	}
	
	//Search County
	conectarPorNameCounty($county);
		
	$query = 'SELECT p.* 
	FROM properties_php p 
	WHERE length(address) > 3 
	AND abs(address) != 0 
	ORDER BY p.address ASC';
	
	$file = fopen('sitemap'.$url_county.'.xml',"w");
			
	fwrite($file, '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
');
	fwrite($file, '<url>
	<loc>http://www.reifax.com/FLORIDA/'.strtoupper($url_county).'/</loc>
	<lastmod>2014-06-10</lastmod>
	<changefreq>weekly</changefreq>
	<priority>0.8</priority>
</url>
');

	$i=1;
	$result = mysql_query($query) or die($query.mysql_error());
	while($val=mysql_fetch_assoc($result)){		
		if($i % 40000 == 0){
			$j = $i/40000;
			fwrite($file, '</urlset>');
			fclose($file);
			
			$file = fopen('sitemap'.$url_county.$j.'.xml',"w");
			
			fwrite($file, '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
');
		}
		
		$url = '<url>
	<loc>http://www.reifax.com/FLORIDA/'.strtoupper($url_county).'/'.sanitize(trim(str_replace(' ','-',$val['city']))).'/'.sanitize(trim(str_replace(' ','-',$val['zip']))).'/'.sanitize(rtrim(trim(str_replace(' ','-',$val['address'])),"-")).'/'.trim(str_replace(' ','-',$val['parcelid'])).'/</loc>
	<lastmod>2014-06-10</lastmod>
	<changefreq>weekly</changefreq>
	<priority>1.0</priority>
</url>
';
		
		fwrite($file, $url);
		if($i % 1000 == 0) echo $i.' - '.$url.'<br><br>';
		$i++;
	}
	
	fwrite($file, '</urlset>');
	fclose($file);
	$tiempo_fin = microtime(true);
	echo "<br>Tiempo de ejecuci&oacute;n redondeado: " . round($tiempo_fin - $tiempo_inicio, 4);
?>