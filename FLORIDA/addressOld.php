<?php 
	$url_county = $_GET['county']; 
	$url_city = $_GET['city']; 
	$url_zip = $_GET['zip'];
	$url_address = $_GET['address'];
	
	$url_city = $url_city=='NOCITY' ? 'NO-CITY' : $url_city;
	$url_zip = $url_zip=='NOZIP' ? 'NO-ZIP' : $url_zip;
	$url_address = $url_address=='NOADDRESS' ? 'NO-ADDRESS' : $url_address;
	
	$county = trim(str_replace('-',' ',$url_county)); 
	$city = trim(str_replace('-',' ',$url_city)); 
	$zip = trim(str_replace('-',' ',$url_zip));
	$address = trim(str_replace('-',' ',$url_address));
	$html = explode('.',$address);
	if($html[1]=='html'){ 
		header('Location: http://www.reifax.com/FLORIDA/'.$url_county.'/'.$url_city.'/'.$url_zip.'/');
		return true;
	}
	
	require_once('../resources/template/template.php');
	include('../properties_conexion.php');
?>

<!DOCTYPE HTML>
<html>
    <!-- Head of ReiFax Website -->
    <?php ReiFaxHead(0,'FLORIDA '.$county.' '.$city.' '.$zip.' '.$address,'FLORIDA '.$county.' '.$city.' '.$zip.' '.$address);?>
<body>
	<div class="container">
        <!-- Header of ReiFax Website -->
        <?php ReiFaxHeader();?>
        
        <!--ReiFax "You are here" Text  -->
        <div class="bluetext" id="heretext">
            <div class="cuote big">&nbsp;</div>
            You are here:
            <a href="http://www.reifax.com/">
                <span class="bluetext underline">ReiFax Home</span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county;?>/">
                <span class="bluetext underline"><?php echo $county;?></span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county.'//'.$url_city;?>/">
                <span class="bluetext underline"><?php echo $city;?></span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county.'//'.$url_city.'//'.$url_zip;?>/">
                <span class="bluetext underline"><?php echo $zip;?></span></a> &gt; 
                
            <span class="fuchsiatext"><?php echo $address;?></span>
        </div>
        
        <div id="contentPrincipal">
            <!-- Advertising Right Block-->
            <div class="sidebarright">
                            <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                                <img src="http://www.reifax.com/resources/img/advertise/D3.png">
                            </a>
                            <a href="http://www.reifax.com/company/contactUs.php">
                                <img src="http://www.reifax.com/resources/img/advertise/D6.png">
                            </a>
            </div>
            
            <div class="content" style="min-height:500px;">
				<?php
                conectarPorNameCounty(strtolower($county));
                
                $query='SELECT address,unit,parcelid 
                FROM properties_php 
                WHERE if(city is null or length(city)=0,"NO CITY",city)="'.$city.'" 
                AND if(zip is null or length(zip)=0,"NO ZIP",zip)="'.$zip.'" 
                AND if(address is null or length(address)=0,"NO ADDRESS",address)="'.$address.'" 
                ORDER BY address,unit;';
                $result=mysql_query($query) or die($query.mysql_error());
                
                while($r=mysql_fetch_array($result)){
                    $address = trim(str_replace('-',' ',$address));
                    $unit = strlen(trim($r['unit']))==0 ? '' : ' '.trim($r['unit']);
                    $pid = $r['parcelid'];
                    
                
                    echo "<a href='http://www.reifax.com/FLORIDA/".$url_county."/".$url_city."/".$url_zip."/".$url_address."/".$pid."/'>".$address.' '.$unit.' - '.$pid."</a><br>";
                
                }?>
          	</div>
        </div>
    </div>               
</body>
</html>