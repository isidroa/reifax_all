<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/resources/template/template.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");

	$url_county = $_GET['county'];
	$county = trim(str_replace('-',' ',$url_county)); 
	$num_rows_all = isset($_GET['num_rows_all']) ? intval($_GET['num_rows_all']) : 0;
	
	function sanitize($string = '', $is_filename = FALSE){
		// Replace all weird characters with dashes
		$string = preg_replace('/[^\w\-'. ($is_filename ? '~_\.' : ''). ']+/u', '-', $string);
		$string = str_replace('&','-',$string);
		// Only allow one dash separator at a time (and make string lowercase)
		return mb_strtolower(preg_replace('/--+/u', '-', $string), 'UTF-8');
	}
	
	//Search County
	conectarPorNameCounty($county);
	if($num_rows_all == 0){
		$query = 'SELECT count(*) 
		FROM properties_php p 
		WHERE length(p.address) > 3 
		AND abs(address) != 0';
		$result = mysql_query($query) or die($query.mysql_error());
		$r = mysql_fetch_array($result);
		
		$num_rows_all = $r[0];
	}
	
	$query = 'SELECT p.* 
	FROM properties_php p 
	WHERE length(p.address) > 3 AND abs(address) != 0';
	
	//Variables for Ordering
	$order 	= isset($_GET['order']) ? str_replace('-',' ',$_GET['order']) : 'ORDER BY p.address';
	$direc 	= isset($_GET['dir']) ? $_GET['dir'] : 'ASC';
	$query	.=' '.$order.' '.$direc;
	
	//Variables for Paging
	$limit_cant_reg = 10;
	$num_limit_page= isset($_GET['page'])? intval($_GET['page']):0;
	$limit=' LIMIT '.($num_limit_page*$limit_cant_reg).', '.$limit_cant_reg;
	
	$num_page_pagin=$limit_cant_reg>0 ? ceil($num_rows_all/$limit_cant_reg):0;
	$star_page=($num_limit_page * $limit_cant_reg)+1;

	$query.=$limit;
	
	$result = mysql_query($query) or die($query.mysql_error());
	
	$list_reg = array();
	while($r=mysql_fetch_assoc($result)){
		$list_reg[] = $r;
	}
	
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHeadNoExt(1,($county.' County Florida Real Estate - Home values and details - REIFax.com'),($county.' County, Florida Real Estate properties list, including home values and property details.'),($county.' County Florida Real Estate, '.$county.' County public records, '.$county.' County properties, '.$county.' County home values and details, '.$county.' County homes for sale, Reifax'));?>
<style>
	.result2 .buttonblue {
	  display: inline;
	}
	.botonOrdenamiento{
		width:24px;
		height:24px;
		display:block;
		border: 1px solid #AAAAAA;
	}
	.botonOrdenamiento:hover{
		border: 1px solid #666666;
		background-color:#FFF;
		-moz-box-shadow: 1px 1px 2px #666666;
		-webkit-box-shadow: 1px 1px 2px #666666;
	}
	#result_orderby_asc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-asc.gif) center no-repeat;
    	border-radius: 4px 0 0 4px;
	}
	#result_orderby_desc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-desc.gif) center no-repeat;
    	border-radius: 0 4px 4px 0;
	}
	.botonDesplazamiento .buttongreen {
		width: auto;
	}
	.result2 .view{
		max-width: 270px;
	}
</style>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="http://www.reifax.com/" rel="nofollow">
           	<span class="bluetext underline">ReiFax Home</span></a> &gt;  
                
            <span class="fuchsiatext"><?php echo $county;?></span>
    </div>
    <div id="contentPrincipal" style="min-height:1050px;">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Medium Rectangle -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:300px;height:250px"
                         data-ad-client="ca-pub-3795268126364893"
                         data-ad-slot="3282546543"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
                         <span style="font-size:18px" class="title bold bluetext">
                            Properties Found: <?php echo $num_rows_all ?>
                         </span>    
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <?php
			if($num_rows_all==0)
			{
				echo '
					<div class="centertext">
						<div class="clear"></div>
						<h2>Property Result: 0 (Please review your search and try again)</h2>
					</div>
					
				';
			}
			else
			{
				?>
       <div class="result2Option">
            <div class="">
                 <div style="position:absolute; top:1px; left:10px;">
                    <div class="botonDesplazamiento">
                        <span class="bluetext">Order By:</span>
                    </div>
                    <div class="botonDesplazamiento">
                        <select id="orderByTo" style="width:120px; height: 22px;">
                            <option value="ORDER BY p.address" <?php echo $order=='ORDER BY p.address' ? 'selected' : '';?>>Address</option>
                            <option value="ORDER BY p.parcelid" <?php echo $order=='ORDER BY p.parcelid' ? 'selected' : '';?>>Parcel ID</option>
                            <option value="ORDER BY p.sqft" <?php echo $order=='ORDER BY p.sqft' ? 'selected' : '';?>>Gross Area</option>
                            <option value="ORDER BY p.beds" <?php echo $order=='ORDER BY p.beds' ? 'selected' : '';?>>Beds</option>
                            <option value="ORDER BY p.bath" <?php echo $order=='ORDER BY p.bath' ? 'selected' : '';?>>Baths</option>
                            <option value="ORDER BY p.city" <?php echo $order=='ORDER BY p.city' ? 'selected' : '';?>>City</option>
                            <option value="ORDER BY p.zip" <?php echo $order=='ORDER BY p.zip' ? 'selected' : '';?>>Zip Code</option>
                            <option value="ORDER BY p.price" <?php echo $order=='ORDER BY p.price' ? 'selected' : '';?>>Price</option>
                            <option value="ORDER BY p.xcoded" <?php echo $order=='ORDER BY p.xcoded' ? 'selected' : '';?>>Property Types</option>
                        </select>
                    </div>
                    <div class="botonDesplazamiento">
                        <a href="<?php echo '?county='.$url_county.'&page=0&order='.str_replace(' ','-',$order).'&dir=ASC';?>" class="botonOrdenamiento" rel='nofollow' id="result_orderby_asc">&nbsp;
                        </a>
                    </div>
                    <div class="botonDesplazamiento">
                        <a href="<?php echo '?county='.$url_county.'&page=0&order='.str_replace(' ','-',$order).'&dir=DESC';?>" class="botonOrdenamiento" rel='nofollow' id="result_orderby_desc">&nbsp;
                        </a>
                    </div>
                </div>
                 <div style="position:absolute; top:1px; right:10px;">
                      <?php
                     if($num_limit_page>0)
                     {
                         echo '
                         <div class="botonDesplazamiento">
                            <!--<a href="?county='.$url_county.'&page=0&order='.str_replace(' ','-',$order).'&dir='.$direc.'" class="buttongreen">|&lt; First</a>-->
                            <a href="?county='.$url_county.'&page='.($num_limit_page-1) .'&order='.str_replace(' ','-',$order).'&dir='.$direc.'" class="buttongreen flatButton">&lt; Previous</a>
                        </div>';
                     }
                     ?>
                     <div class="botonDesplazamiento">
                           Page <b><?php echo $num_limit_page+1;?></b> of <?php echo $num_page_pagin;?>
                           <!--<select id="changePageTo" style="width:110px; height: 22px;">
                             <?php
                                /*for($j=0;$j<$num_page_pagin;$j++)
                                {
                                    echo ($num_limit_page==$j)?'<option value="?county='.$url_county.'&page='. $j .'&order='.str_replace(' ','-',$order).'&dir='.$direc.'" selected>Page '.($j+1).'</option>':'<option value="?county='.$url_county.'&page='. $j .'&order='.str_replace(' ','-',$order).'&dir='.$direc.'">Page '.($j+1).'</option>';
                                }*/
                            ?>
                             </select>-->
                     </div>
                     <?php
                     if($num_limit_page!=($num_page_pagin-1))
                     {
                        echo'
                         <div class="botonDesplazamiento">
                            <a href="?county='.$url_county.'&page='.($num_limit_page+1)  .'&order='.str_replace(' ','-',$order).'&dir='.$direc.'" class="buttongreen flatButton">Next &gt;</a>
                            <!--<a href="?county='.$url_county.'&page='.($num_page_pagin-1)  .'&order='.str_replace(' ','-',$order).'&dir='.$direc.'" class="buttongreen">Last &gt;|</a>-->
                         </div>';
                     }
                     ?>
                 </div>
            </div>
        </div>
       
       
       
       	<?php 
			}
			$i=($num_limit_page*10)+1;
			foreach($list_reg as $key => $val)
			{
				$url_city 		= strlen(sanitize(trim(str_replace(' ','-',$val['city']))))>0 ? sanitize(trim(str_replace(' ','-',$val['city']))) : 'NO-CITY';
				$url_zip 		= strlen(sanitize(trim(str_replace(' ','-',$val['zip']))))>0 ? sanitize(trim(str_replace(' ','-',$val['zip']))) : 'NO-ZIP';
				$url_address 	= strlen(sanitize(trim(str_replace(' ','-',$val['address']))))>0 ? sanitize(trim(str_replace(' ','-',$val['address']))) : 'NO-ADDRESS';
				
				$val['url'] = 'http://www.reifax.com/'.trim(str_replace(' ','-',$val['state'])).'/'.$url_county.'/'.$url_city.'/'.$url_zip.'/'.$url_address.'/'.trim(str_replace(' ','-',$val['parcelid'])).'/';
				
				echo '
				<div class="result2">
					<div class="title"> <!--<span class="price">$'.number_format($val['price'],0,'.',',').'</span> -->'.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].' <span class="Num">'.$i.'</span></div>
					<div class="view">
						<div id="map_result_'.$key.'" class="img" style="width:135px; height:135px; overflow:hidden; position:relative;">
						</div>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>City:</td>
								<td class="">'.$val['city'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Folio Number:</td>
								<td class="">'.$val['parcelid'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Beds/Baths:</td>
								<td class="">'.$val['beds'].'/'.$val['bath'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Foreclosure Status:</td>
								<td class="">'.($val['pendes']=='N' ? 'Not Forclosures' : ($val['pendes']=='P' ? 'Pre-Foreclosed' : 'Foreclosed')).'</td>
							</tr>
						</table>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>County:</td>
								<td class="">'.$val['county'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Type:</td>
								<td class="">'.substr($val['xcoded'],0,25).'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Gross Area:</td>
								<td class="">'.number_format($val['sqft'],0,'.',',').'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>ReiFax Value:</td>
								<td class="">$'.number_format($val['marketvalue'],2,'.',',').'</td>
							</tr>
						</table>
					</div>
						<a href="'.$val['url'].'" rel="'.$val['parcelid'].'|'.$val['county'].'" class="buttonblue">View Details</a>
					<div class="clearEmpty"></div>
				</div>
					<div class="clearEmpty"></div>
				';
			$i++;
			}
		?>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script src="/includes/properties_draw.js"></script>
<script language="javascript">
<?php if(count($list_reg)>0){?>

		<?php foreach($list_reg as $k => $val){	?>
			
			var	map_result_<?php echo $k;?> = new XimaMap('map_result_<?php echo $k;?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_circle');
			map_result_<?php echo $k;?>._IniMAPResult(<?php echo $val['latitude'].','.$val['longitude'];?>);
			
			getCasita(<?php echo '"'.$val['status'].'","'.$val['pendes'].'","'.$val['sold'].'"';?>);
			
			
			map_result_<?php echo $k;?>.addPushpin(
				<?php echo $val['latitude'].','.$val['longitude'];?>,
				'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
				'<?php echo ($star_page+$k);?>'
			);			
			
		<?php }?>
		
<?php }?>
	$(document).ready(function (){
		$('#orderByTo').bind('change',ordenar);		
	});
	
function ordenar()
{
	var order=$('#orderByTo').val();
	var dir=($(this).attr('rel'))?$(this).attr('rel'):'ASC';
	
	window.location='?county=<?php echo $url_county;?>&page=0&order='+order.replace(/(\s)/gi,'-')+'&dir='+dir;
}
</script>