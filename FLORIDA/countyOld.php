<?php 
	$url_county = $_GET['county']; 
	
	$county = trim(str_replace('-',' ',$url_county)); 
	require_once('../resources/template/template.php');
	include('../properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>
    <!-- Head of ReiFax Website -->
    <?php ReiFaxHead(0,'FLORIDA '.$county,'FLORIDA '.$county);?>
<body>
	<div class="container">
        <!-- Header of ReiFax Website -->
        <?php ReiFaxHeader();?>
        
        <!--ReiFax "You are here" Text  -->
        <div class="bluetext" id="heretext">
            <div class="cuote big">&nbsp;</div>
            You are here:
            <a href="http://www.reifax.com/">
                <span class="bluetext underline">ReiFax Home</span></a> &gt;  
                
            <span class="fuchsiatext"><?php echo $county;?></span>
        </div>
        
        <div id="contentPrincipal">
            <!-- Advertising Right Block-->
            <div class="sidebarright">
                            <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                                <img src="http://www.reifax.com/resources/img/advertise/D3.png">
                            </a>
                            <a href="http://www.reifax.com/company/contactUs.php">
                                <img src="http://www.reifax.com/resources/img/advertise/D6.png">
                            </a>
            </div>
            
            <div class="content" style="min-height:500px;">
				<?php
				conectarPorNameCounty(strtolower($county));
                $query='SELECT distinct if(city is null,"",city) city
                FROM properties_php 
                ORDER BY city;';
                $result=mysql_query($query) or die($query.mysql_error());
                
                while($r=mysql_fetch_array($result)){
                    $city = trim($r['city']);
                    $urlCity = strtoupper(str_replace(' ','-',$city));
                    
                    if(strlen($city)==0){
                        $city='NO CITY';
                        $urlCity='NO-CITY';
                    }
                
                    echo "<a href='http://www.reifax.com/FLORIDA/".$url_county."/".$urlCity."/'>".$city."</a><br>";
                
                }?>
         	</div>
        </div>
    </div>
</body>
</html>