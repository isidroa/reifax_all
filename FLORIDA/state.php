<?php
	require_once('../resources/template/template.php');
	include('../properties_conexion.php');
	conectar();
?>
<!DOCTYPE HTML>
<html>
    <!-- Head of ReiFax Website -->
    <?php ReiFaxHead(0,'FLORIDA','FLORIDA');?>
<body>
	<div class="container">
        <!-- Header of ReiFax Website -->
        <?php ReiFaxHeader();?>
        
        <!--ReiFax "You are here" Text  -->
        <div class="bluetext" id="heretext">
            <div class="cuote big">&nbsp;</div>
            You are here:
            <a href="http://www.reifax.com/">
                <span class="bluetext underline">ReiFax Home</span></a> &gt; 
            
            <span class="fuchsiatext">FLORIDA</span>
        </div>
        
        <div id="contentPrincipal">
            <!-- Advertising Right Block-->
            <div class="sidebarright">
                            <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                                <img src="http://www.reifax.com/resources/img/advertise/D3.png">
                            </a>
                            <a href="http://www.reifax.com/company/contactUs.php">
                                <img src="http://www.reifax.com/resources/img/advertise/D6.png">
                            </a>
            </div>
            
            <div class="content" style="min-height:500px;">
				<?php
                $query='SELECT County FROM xima.lscounty 
                WHERE IdState=1 and State="FL" and is_showed=1
                ORDER BY County';
                $result=mysql_query($query) or die($query.mysql_error());
                
                while($r=mysql_fetch_array($result)){
                    $county = trim($r['County']);
                    $urlCounty = strtoupper(str_replace(' ','-',$county));
                    
                    echo "<a href='http://www.reifax.com/FLORIDA/".$urlCounty."/'>".$county."</a><br>";
                
                }?>
            </div>
        </div>
    </div>
</body>
</html>