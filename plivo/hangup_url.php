<?php
	require 'plivo.php';

    $reason = isset($_POST['reason']) ? $_POST['reason'] : 'rejected';
    $schedule = isset($_POST['schedule']) ? $_POST['schedule'] : 0;

    $response = new Response();
	
	$params = array(
		'reason' => $reason,
		'schedule' => $schedule
	);
	$response->addHangup($params);
	
	header("Content-Type: text/xml");
    echo($response->toXML());
?>