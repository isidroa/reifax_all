<?php
	require 'plivo.php';

    $dst = $_POST['To'];
    $src = isset($_GET['plivoNum']) ? $_GET['plivoNum'] : $_POST['From'];

    $response = new Response();
    if($dst) {
        $dial_params = array();
        if($src) $dial_params['callerId'] = $src;

        $dial = $response->addDial($dial_params);
        if(substr($dst, 0,4) == "sip:") {
            $dial->addUser($dst);
        } else {
        $dial->addNumber($dst);
        }
    } else {
        $response->addHangup();
    }

    header("Content-Type: text/xml");
    echo($response->toXML());
?>