<?php
/*
** KLM function library
** By Eriestuff.blogspot.com
*/

// getUploadedFileData
// checks if a file is uploaded through a html form and returns the tmp filename
// attributes: 	string file upload input field name
//				boolean switch to suppress error outputs (default to false)
// returns: 	array of file data equal to the $_FILES array of php
// outputs:		error msg's on errors, if error outputting is not suppressed
function getUploadedFileData($strInputName, $suppress_errors=FALSE){
	if(isset($_FILES) && isset($_FILES[$strInputName]) && $_FILES[$strInputName]['size']>0){
		if(is_uploaded_file($_FILES[$strInputName]['tmp_name'])){
			return $_FILES[$strInputName];
		}elseif(!$suppress_errors){
			// ERROR HANDLING
			switch($_FILES['uploadedfile']['error']){
				case UPLOAD_ERR_OK: echo "There is no error, the file uploaded with success.";break;
				case UPLOAD_ERR_INI_SIZE: echo "The uploaded file exceeds the upload_max_filesize directive in php.ini.";break;
				case UPLOAD_ERR_FORM_SIZE: echo "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";break;
				case UPLOAD_ERR_PARTIAL: echo "The uploaded file was only partially uploaded.";break;
				case UPLOAD_ERR_NO_FILE: echo "No file was uploaded.";break;
				case UPLOAD_ERR_NO_TMP_DIR: echo "Missing a temporary folder.";break;
				case UPLOAD_ERR_CANT_WRITE: echo "Failed to write file to disk.";break;
				case UPLOAD_ERR_EXTENSION: echo "File upload stopped by extension.";break;
			}
		}
	}
	return NULL;
}

// Return the distance between two points on the Globe
// This calculates the length of the Great Circle between the 2 points
function dist_mean_radius($lat1, $lng1, $lat2, $lng2){
	$r = 6372797;	// mean radius of Earth in meters
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lng1 *= $pi80;
	$lat2 *= $pi80;
	$lng2 *= $pi80;
	$dlat = $lat2 - $lat1;
	$dlng = $lng2 - $lng1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$distance = $r * $c;
	
	return $distance; // in meters
}

/*
 * Calculate geodesic distance (in m) between two points specified by latitude/longitude (in numeric degrees)
 * using Vincenty inverse formula for ellipsoids
 * The function below is translated from the javascript version 
 * provided by http://www.movable-type.co.uk/scripts/latlong-vincenty.html
 */
function dist_vincenty($lat1, $lon1, $lat2, $lon2) {
	$a = 6378137;
	$b = 6356752.3142;
	$f = 1/298.257223563;  // WGS-84 ellipsiod
	$L = deg2rad($lon2-$lon1);
	$U1 = atan((1-$f) * tan(deg2rad($lat1)));
	$U2 = atan((1-$f) * tan(deg2rad($lat2)));
	$sinU1 = sin($U1);
	$cosU1 = cos($U1);
	$sinU2 = sin($U2);
	$cosU2 = cos($U2);
  
	$lambda = $L;
	$lambdaP = 2*M_PI;
	$iterLimit = 20;
	while (abs($lambda-$lambdaP) > 1e-12 && --$iterLimit>0) {
		$sinLambda = sin($lambda);
		$cosLambda = cos($lambda);
		$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + 
			($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
		if ($sinSigma==0) return 0;  // co-incident points
		$cosSigma = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
		$sigma = atan2($sinSigma, $cosSigma);
		$sinAlpha = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
		$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
		$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
		if (is_numeric($cos2SigmaM)) $cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (�6)
		$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
		$lambdaP = $lambda;
		$lambda = $L + (1-$C) * $f * $sinAlpha *
			($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
	}
	if ($iterLimit==0){ return NULL; }	// formula failed to converge

	$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
	$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
	$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
	$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)-
		$B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	$s = $b*$A*($sigma-$deltaSigma);
	
	$s = round($s,3); // round to 1mm precision
	return $s;
}

class kml{
	var $str_url;
	var $str_xml;
	var $arr_xml;
	var $str_document_name;
	var $str_filename;
	var $arr_placemarks;
		
	## CONSTRUCTUR ##################################################################################
	function kml($str_url=NULL){
		if(is_string($str_url)){
			$this->load_url($str_url);
		}
	}
	## METHODS ######################################################################################
	// get methods
	function get_url(){return $this->str_url;}
	function get_original_xml(){return $this->str_xml;}
	function & get_xml_array(){return $this->arr_xml;}
	function get_document_name(){return $this->str_document_name;}
	function get_filename(){return $this->str_filename;}
	function & get_placemarks(){return $this->arr_placemarks;}

	// set methods
	function set_url($str_url){$this->str_url = $str_url;}
	function set_document_name($str_document_name){$this->str_document_name=$str_document_name;}
	function set_filename($str_filename){$this->str_filename=$str_filename;}

	// input en output methods
	function load_url($str_url=NULL){
		// sync var with argument
		$this->str_url = (isset($str_url) && !empty($str_url))?$str_url:$this->str_url;
		// get KMLdata from url and unserialize it
		$this->str_xml = $this->XML_fetch_url($this->str_url);
		$this->arr_xml = $this->XML_unserialize($this->str_xml);
		$this->KML_coordinates2arrays($this->arr_xml);
		$this->arr_placemarks = & $this->KML_get_placemarks($this->arr_xml);
		// set document name
		$this->str_document_name = $this->arr_xml['kml']['Document']['name'];
		// get filename from url
		$tmp_filename = basename($this->str_url);
		// check filename, use document name if it isnt valid
		if(!preg_match('!\.[0-9a-z]+$!', $tmp_filename)){
			$tmp_filename = preg_replace('/[^0-9a-z\.\_\-]/i','_',$this->str_document_name.'.kml');
		}
		// set filename
		$this->str_filename = $tmp_filename;
	}
	function output_kml($str_filename=NULL){
		// sync var with argument
		$this->str_filename = (isset($str_filename) && !empty($str_filename))?$str_filename:$this->str_filename;
		// set default filename if none isset
		$this->str_filename = (isset($this->str_filename) && !empty($this->str_filename))?$this->str_filename:'output.kml';
		// serialize kml data
		$this->KML_coordinates2strings($this->arr_xml);
		$this->str_xml = $this->XML_serialize($this->arr_xml);
		// wrap descriptions in <![CDATA[ ]]> wrappers
		if(preg_match_all('�<description>([\s\S]*?)<\/description>�',$this->str_xml,$matches)){
		    for($i=0;$i<count($matches[1]);$i++){
		        $this->str_xml = str_replace($matches[0][$i],'<description><![CDATA['.$matches[1][$i].']]></description>',$this->str_xml);
		    }
		}
		// output kml
		header('Content-Type: application/vnd.google-earth.kml+xml');    
		header('Content-Disposition: attachment; filename="'.$this->str_filename.'"');
		echo($this->str_xml . "\n");
		exit;
	}

	## HELPER FUNCTIONS #############################################################################
	private function XML_fetch_url($url){
		$url = html_entity_decode($url);	// return &amp; into & (transformed by form submit) 
		$urlIsRemote = (file_exists($url))?FALSE:TRUE;
		if($urlIsRemote){
			// check url and get host/path parts
			// retuns [source,protocol,host,path(incl query)]
			// NB this regex might need some work to accept all possible valid urls
			$strRegEx =  "/^(.*):\/\/([0-9a-z_!~*'().&=+$%-]+:?[0-9]{0,4})?(.*)?$/i";
			if (!preg_match($strRegEx,$url,$arrMatches)) die('Error: url seems invalid: '.$url);
			$host = $arrMatches[2];
			$path = $arrMatches[3];
			// fetch the source kml file
			$result = '';
			$fp = @fsockopen($host,80);
			if ($fp) {
				fputs($fp, "GET $path HTTP/1.0\n");
				fputs($fp, "Host: $host\n");
				fputs($fp, "Connection: close\n\n");
				while (!feof($fp))
					$result .= fgets($fp,128);
				fclose($fp);
			}
			$xml = $result;
		}else{
			// open uploaded file
			$xml = file_get_contents($url);
		}
		// check if url returned a xml file
		if(strpos($xml,"<?xml ")===FALSE || strpos($xml,"<kml ")===FALSE){
			// no xml header, no kml root element
			$xml = '';
		}else{
			// clean the file transfer header stuff from the result
			$xml = substr($xml, strpos($xml,"<?xml "));
		}
		return $xml;
	}
	private function & KML_get_placemarks(&$data, &$arr_parent_info = array()){
	    $return_array = array();
		if(gettype($data)=='array'){
		    foreach($data as $key => & $value) {
				// clean out the intermediate nodes and just saves the coordinates (array)
				// stuff the value in the return array;
		   	    if ($key === 'Polygon' && isset($value['outerBoundaryIs']['LinearRing']['coordinates'])) {
	        	    $value = array('coordinates'=> & $value['outerBoundaryIs']['LinearRing']['coordinates']);
	        	    $return_array[] = & array_merge($arr_parent_info,array('type'=> &$key),$value);
		   	    }elseif ($key === 'Point' && isset($value['coordinates'][0])) {
	        	    $value = array('coordinates'=> & $value['coordinates'][0]);
	        	    $return_array[] = & array_merge($arr_parent_info,array('type'=> &$key),$value);
		   	    }elseif ($key === 'LineString' && isset($value['coordinates'])) {
	        	    $value = & $value['coordinates'];
	        	    $return_array[] = & array_merge($arr_parent_info,array('type'=> &$key),$value);
				}else{
					if($key === 'name'){
						// save the name of this node to use as parent name for the next nodes
						$key = (!isset($arr_parent_info['document_name']))?'document_name':$key;
						$arr_parent_info =& array_merge($arr_parent_info, array($key=>&$value));
					}
					// recursively call this function to get deeper nodes, send parent info array with
	    	        $return_array=& array_merge($return_array,$this->KML_get_placemarks($value,$arr_parent_info));
	        	}
	    	}
		}
		return $return_array;
	}

	private function KML_coordinates2arrays(&$data){
		if(gettype($data)=='array'){
		    foreach($data as $key => & $value) {
	    	    if ($key === 'coordinates') {
					$vertices = explode("\n",$value);
					foreach($vertices as $key => & $vertex){
						$vertex = explode(',',$vertex);
						if(count($vertex)<2){		// coords need minimum of 2 values, the possible third being altitude
							array_splice($vertices,$key,1); //remove vertex from vertrices
						}else{
							foreach($vertex as & $coord){
								$coord = trim($coord);
							}
						}
					}
					// turn coordinate string into array of vertices
					$value = $vertices;
		        } else {
					// recursively call to search one level deeper
	    	        $this->KML_coordinates2arrays($value);
	        	}
	    	}
		}
	}
	private function KML_coordinates2strings(&$data, $level=0){
		if(gettype($data)=='array'){
		    foreach($data as $key => & $value) {
	    	    if ($key === 'coordinates') {
					if(gettype($value[0])=='string'){
						// add one layer of arrays, so points have the same format as polylines and polygons
						// this is needed to be able to convert them to strings with the same algorithm below
						$vertices = array(&$value);
					}else{
						$vertices =& $value;
					}
					foreach($vertices as $key => & $vertex){
						$vertex = array_slice($vertex, 0, 3);	// trim to 3 elements (x,y,z)
						$vertex = implode(',',$vertex);			//
					}
					if(count($vertices)>1){
						// for polylines and polygons (multiple coordinates)
						// turn array into string and add linefeeds and tabs for layout
						$vertices = "\n".str_repeat("\t",$level).implode("\n".str_repeat("\t",$level),$vertices)."\n".str_repeat("\t", $level-1);
					}else{
						// flatten array to string
						$vertices = $vertices[0];
					}
					// turn coordinate string into array of vertices
					$value = $vertices;
		        } else {
					// recursively call to search one level deeper
	    	        $this->KML_coordinates2Strings($value,$level+1);
	        	}
	    	}
		}
	}

	###################################################################################
	#
	# XML Library, by Keith Devens, version 1.2b
	# http://keithdevens.com/software/phpxml
	#
	# This code is Open Source, released under terms similar to the Artistic License.
	# Read the license at http://keithdevens.com/software/license
	#
	###################################################################################

	###################################################################################
	# XML_unserialize: takes raw XML as a parameter (a string)
	# and returns an equivalent PHP data structure
	###################################################################################
	function & XML_unserialize(&$xml){
		$xml_parser = &new XML();
		$data = $xml_parser->parse($xml);
		$xml_parser->destruct();
		return $data;
	}
	###################################################################################
	# XML_serialize: serializes any PHP data structure into XML
	# Takes one parameter: the data to serialize. Must be an array.
	###################################################################################
	function XML_serialize($data, $level = 0, $prior_key = NULL){
		$return = '';
		if($level == 0){ $return .= '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n"; }
		while(list($key, $value) = each($data))
			if(!strpos($key, ' attr')) #if it's not an attribute
				#we don't treat attributes by themselves, so for an empty element
				# that has attributes you still need to set the element to NULL
				if(is_array($value) and array_key_exists(0, $value)){
					$return .= $this->XML_serialize($value, $level, $key);
				}else{
					$tag = $prior_key ? $prior_key : $key;
					$return .= str_repeat("\t", $level).'<'.$tag;
					if(array_key_exists("$key attr", $data)){ #if there's an attribute for this element
						while(list($attr_name, $attr_value) = each($data["$key attr"]))
							$return .= ' '.$attr_name.'="'.htmlspecialchars($attr_value).'"';
						reset($data["$key attr"]);
					}
					if(is_null($value)) $return .= " />\n";
					elseif(!is_array($value)) $return .= '>'.htmlspecialchars($value)."</$tag>\n";
					else $return .= ">\n".$this->XML_serialize($value, $level+1).str_repeat("\t", $level)."</$tag>\n";
				}
		reset($data);
		return $return;
	}
	
// END KML class
}
###################################################################################
# XML class: utility class to be used with PHP's XML handling functions
###################################################################################
class XML{
	var $parser;   #a reference to the XML parser
	var $document; #the entire XML structure built up so far
	var $parent;   #a pointer to the current parent - the parent will be an array
	var $stack;    #a stack of the most recent parent at each nesting level
	var $last_opened_tag; #keeps track of the last tag opened.

	function XML(){
 		$this->parser = xml_parser_create();
		xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, false);
		xml_set_object($this->parser, $this);
		xml_set_element_handler($this->parser, 'open','close');
		xml_set_character_data_handler($this->parser, 'data');
	}
	function destruct(){ xml_parser_free($this->parser); }
	function parse(&$data){
		$this->document = array();
		$this->stack    = array();
		$this->parent   = &$this->document;
		return xml_parse($this->parser, $data, true) ? $this->document : NULL;
	}
	function open(&$parser, $tag, $attributes){
		$this->data = ''; #stores temporary cdata
		$this->last_opened_tag = $tag;
		if(is_array($this->parent) and array_key_exists($tag,$this->parent)){ #if you've seen this tag before
			if(is_array($this->parent[$tag]) and array_key_exists(0,$this->parent[$tag])){ #if the keys are numeric
				#this is the third or later instance of $tag we've come across
				$key = $this->count_numeric_items($this->parent[$tag]);
			}else{
				#this is the second instance of $tag that we've seen. shift around
				if(array_key_exists("$tag attr",$this->parent)){
					$arr = array('0 attr'=>&$this->parent["$tag attr"], &$this->parent[$tag]);
					unset($this->parent["$tag attr"]);
				}else{
					$arr = array(&$this->parent[$tag]);
				}
				$this->parent[$tag] = &$arr;
				$key = 1;
			}
			$this->parent = &$this->parent[$tag];
		}else{
			$key = $tag;
		}
		if($attributes) $this->parent["$key attr"] = $attributes;
		$this->parent  = &$this->parent[$key];
		$this->stack[] = &$this->parent;
	}
	function data(&$parser, $data){
		if($this->last_opened_tag != NULL) #you don't need to store whitespace in between tags
			$this->data .= $data;
	}
	function close(&$parser, $tag){
		if($this->last_opened_tag == $tag){
			$this->parent = $this->data;
			$this->last_opened_tag = NULL;
		}
		array_pop($this->stack);
		if($this->stack) $this->parent = &$this->stack[count($this->stack)-1];
	}
	function count_numeric_items(&$array){
		return is_array($array) ? count(array_filter(array_keys($array), 'is_numeric')) : 0;
	}
}


?>