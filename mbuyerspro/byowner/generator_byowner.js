
	
	///////// search by owner ////////////////////
	
	
	

function colocarDefault_byowner(id,val){
	if(val=='Address' || val=='Parcelid'){
		document.getElementById(id).value='';
	}
}

function colocarDefault2_byowner(id,val){
	if(val==''){
		if(search_by_type=='LOCATION')
			document.getElementById(id).value='Address';
		if(search_by_type=='PARCELID')
			document.getElementById(id).value='Parcelid';
		}
}
    //cambia el titulo
	function cambiarDefault_byowner(id,val){
	document.getElementById(id).value=val;
	if(val=='Address')
		document.getElementById('search_byowner').innerHTML='Location';
    if(val=='Parcelid')
		document.getElementById('search_byowner').innerHTML='Parcelid';
		
	}
	
	function doSearchByFilter_byowner(value){
	search_by_type_byowner = value;
	
	if(search_by_type_byowner=='LOCATION'){
		cambiarDefault_byowner('search_byowner_t','Address');
		if(search_type=='PR' && document.getElementById('county_search_byowner').value!=-1)
			OcultarMostrar2(true);
	}
	
	if(search_by_type_byowner=='PARCELID'){
		cambiarDefault_byowner('search_byowner_t','Parcelid');
		OcultarMostrar2(false);
	}
}

function OcultarMostrar2(val){
	var equity='Debt Equity';
	if(search_type=='FS') equity='Potential Equity';
	
	if(val==true){
		if(document.getElementById('tproptype2'))document.getElementById('tproptype2').innerHTML='Property Type';
		if(document.getElementById('tpropfore2'))document.getElementById('tpropfore2').innerHTML='Foreclosure Status';
		if(document.getElementById('proptype2'))document.getElementById('proptype2').style.display='';
		if(document.getElementById('pende2s'))document.getElementById('pendes2').style.display='';
		if(document.getElementById('tprice2'))document.getElementById('tprice2').innerHTML='Price Range';
		if(document.getElementById('tbeds2'))document.getElementById('tbeds2').innerHTML='Beds';
		if(document.getElementById('tbath2'))document.getElementById('tbath2').innerHTML='Baths';
		if(document.getElementById('tsqft2'))document.getElementById('tsqft2').innerHTML='Sqft';
		if(document.getElementById('tpequity2')){
			document.getElementById('tpequity2').innerHTML='Debt Equity';
			if(search_type=='FS') document.getElementById('tpequity2').innerHTML='Potential Equity';
		}
		if(document.getElementById('price_dol12'))document.getElementById('price_dol12').style.display='';
		if(document.getElementById('price_to2'))document.getElementById('price_to2').innerHTML='to';
		if(document.getElementById('price_dol22'))document.getElementById('price_dol22').style.display='';
		if(document.getElementById('bed2'))document.getElementById('bed2').style.display='';
		if(document.getElementById('bath2'))document.getElementById('bath2').style.display='';
		if(document.getElementById('sqft2'))document.getElementById('sqft2').style.display='';
		if(document.getElementById('pequity2'))document.getElementById('pequity2').style.display='';
		if(document.getElementById('price_low2'))document.getElementById('price_low2').style.display='';
		if(document.getElementById('price_hi2'))document.getElementById('price_hi2').style.display='';
		if(document.getElementById('tentrydate2'))document.getElementById('tentrydate2').innerHTML='Entry Date';
		if(document.getElementById('entrydate2'))document.getElementById('entrydate2').style.display='';
	}else{
		if(document.getElementById('tproptype2'))document.getElementById('tproptype2').innerHTML='&nbsp;';
		if(document.getElementById('tpropfore2'))document.getElementById('tpropfore2').innerHTML='&nbsp;';
		if(document.getElementById('proptype2'))document.getElementById('proptype2').style.display='none';
		if(document.getElementById('pendes2'))document.getElementById('pendes2').style.display='none';
		if(document.getElementById('tprice2'))document.getElementById('tprice2').innerHTML='&nbsp;';
		if(document.getElementById('tbeds2'))document.getElementById('tbeds2').innerHTML='&nbsp;';
		if(document.getElementById('tbath2'))document.getElementById('tbath2').innerHTML='&nbsp;';
		if(document.getElementById('tsqft2'))document.getElementById('tsqft2').innerHTML='&nbsp;';
		if(document.getElementById('tpequity2'))document.getElementById('tpequity2').innerHTML='&nbsp;';
		if(document.getElementById('price_dol12'))document.getElementById('price_dol12').style.display='none';
		if(document.getElementById('price_to2'))document.getElementById('price_to2').innerHTML='&nbsp;';
		if(document.getElementById('price_dol22'))document.getElementById('price_dol22').style.display='none';
		if(document.getElementById('bed2'))document.getElementById('bed2').style.display='none';
		if(document.getElementById('bath2'))document.getElementById('bath2').style.display='none';
		if(document.getElementById('sqft2'))document.getElementById('sqft2').style.display='none';
		if(document.getElementById('pequity2'))document.getElementById('pequity2').style.display='none';
		if(document.getElementById('price_low2'))document.getElementById('price_low2').style.display='none';
		if(document.getElementById('price_hi2'))document.getElementById('price_hi2').style.display='none';
		if(document.getElementById('tentrydate2'))document.getElementById('tentrydate2').innerHTML='&nbsp;';
		if(document.getElementById('entrydate2'))document.getElementById('entrydate2').style.display='none';
	}
}


function searchForm_byowner(id,tipo){
	var value=document.getElementById(id).value;
	var long=document.getElementById(id).value.length;
	var county=document.getElementById('county_search_byowner').value;
   
	if(long>0 && value!='Address' && value!='Parcelid')
		{GuardarSearch_byowner(tipo);}else{ alert("Enter Address or ParcelId to Search");} 
}	

function GuardarSearch_byowner(tipo){
	
	if(search_by_type_byowner=='LOCATION') 
	var ty='location';	
	if(search_by_type_byowner=='PARCELID') 
		ty='parcelid';
     
	var _search='';
	if(document.getElementById('search_byowner_t'))_search=document.getElementById('search_byowner_t').value;
	var equity=-1;
	var entrydate='';
    var proptype='';
	var price_low='';
	var price_hi='';
	var bed=-1;
	var bath=-1;
	var sqft=-1;
	var pendes=-1;
	var mapa_search_latlong='-1';
	var realtor='-1';
	if(document.getElementById('realtor'))realtor=document.getElementById('realtor').value;
	
		
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_coresearch.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			search: _search,
			county: document.getElementById('county_search_byowner').value,
			tsearch: ty,
			proptype: proptype,
			price_low: price_low,
			price_hi: price_hi,
			bed: bed,
			bath: bath,
			sqft: sqft,
			pequity: equity,
			pendes: pendes,
			search_mapa: mapa_search_latlong,
			entrydate: entrydate,
			search_type: document.getElementById('combo_search_types_byowner').value,
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			systemsearch= 'basic';
			byowner=true;
			createResultbyowner(user_web,realtor_block);
		}                                
	});
}



function createResultbyowner(user_web,realtor_block){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultbyownerTab')){
		var tab = tabbyowner.getItem('resultbyownerTab');
		tabbyowner.remove(tab);
	}
	
	if(document.getElementById('overviewbyownerTab')){
		var tab = tabbyowner.getItem('overviewbyownerTab');
		tabbyowner.remove(tab);
	}
		if(document.getElementById('reviewbyownerTab')){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('photosbyownerTab')){
		var tab = tabbyowner.getItem('photosbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('compartirbyownerTab')){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.remove(tab);  }
	
	if(realtor_block==false || byowner==false) {
	        var icon_result=false;
	}else{ 
	 	var icon_result=true;
		var icon_mylabel=true;
	}
	
	if(user_web==false) {
	        var icon_mylabel=false;
	}else{ 
	 	var icon_mylabel=true;
	}
	

	
	tabbyowner.add({
		title: ' Step Two ',
		id: 'resultbyownerTab',
		autoLoad: {url: 'byowner/properties_result_byowner.php', scripts: true, discardUrl:true, nocache:true, params:{userweb:user_web,realtorid:user_webid,systemsearch:systemsearch}},
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabbyowner.getItem('resultbyownerTab');
		tabbyowner.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function pagingResultbyowner(page){
	window.scrollTo(0,0);
	var tab2 = tabbyowner.getItem('resultbyownerTab');
	tabbyowner.setActiveTab(tab2);
	
	var updaterbyowner = tab2.getUpdater();
	updaterbyowner.update({url:'byowner/properties_result_byowner.php?page='+page, scripts: true, discardUrl:true, nocache:true,
				   params:{userweb:user_web,systemsearch:systemsearch} });
}

function orderByResultbyowner(order,dir){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_order_by_result_byowner.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			order: order+' '+dir
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			pagingResultbyowner(0);	
		}                                
	});
}

function createoverviewbyowner(county,pid,astatus,realtor,compType){
	
	// if(!user_loged || user_block || user_web)
// {    document.location.href="http://www.reifax.com/properties_register.php";      }
//	 
	window.scrollTo(0,0);
		
	if(document.getElementById('overviewbyownerTab')){
		var tab = tabbyowner.getItem('overviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('reviewbyownerTab')){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('photosbyownerTab')){
		var tab = tabbyowner.getItem('photosbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('compartirbyownerTab')){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.remove(tab);  }
			
	var url='byowner/properties_list_byowner.php?county='+county+'&pid='+pid+'&astatus='+astatus;
		 	
	tabbyowner.add({
		title: ' Step Three ',
		id: 'overviewbyownerTab',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, params:{userweb:user_web}},
		closable: true,
		
				}).show();
		
		
	if(Ext.isIE){
		var tab = tabbyowner.getItem('overviewbyownerTab');
		tabbyowner.setActiveTab(tab);
		tab.getEl().repaint();
	}
	
}


function confirmar(idcounty1,pid){	  
   
    var lprice=document.getElementById('lprice').value;
    var zip= document.getElementById('zip').value;
    var city=document.getElementById('city').value;
	var address=document.getElementById('address').value;
	var phone=document.getElementById('owner_ph1').value;
    var owner=document.getElementById('owner').value;
    var email=document.getElementById('owner_email').value;
   
   	var garea=document.getElementById('garea').value;
    var bath= document.getElementById('bathby').value;
    var beds=document.getElementById('beds').value;
	var hbath=document.getElementById('hbath').value;
	var stories= document.getElementById('stories').value;
    var lotsize=document.getElementById('lotsize').value;
	var units=document.getElementById('units').value;
   
   if (garea.length==0) document.getElementById('garea').value=0;
    if (bath.length==0) document.getElementById('bathby').value=0;
	  if (beds.length==0) document.getElementById('beds').value=0;
	    if (hbath.length==0) document.getElementById('hbath').value=0;
		 if (stories.length==0) document.getElementById('stories').value=0;
	  if (lotsize.length==0) document.getElementById('lotsize').value=0;
	    if (units.length==0) document.getElementById('units').value=0;
   
   
    if(lprice.length==0){
		alert("Required field Lprice");return;
	      }else{
			    if(city.length==0){
                alert("Required field City");return;
				} else {
					if(address.length==0){
                    alert("Required field Address");return;
					}else{
						 if(zip.length==0){
                         alert("Required field Zip code");return;
						   }else{
						 if(owner.length==0){
                         alert("Required field Owner name");return;
						   } else{
								 if(email.length==0){
								 alert("Required field Owner email");return;
								   }else{
										 if(phone.length==0){
										 alert("Required field Owner Phone");return;
						   }
						 
					}}}}}}
	
	 if(lprice.length!=0 || city.length!=0 || address.length!=0 || zip.length!=0 )
	{			
			Ext.Msg.show({
		   title:'Save Changes?',
		   msg: 'Would you like to apply your changes?',
		   buttons: Ext.Msg.YESNO,
		   fn: function (btn)
			   {
			   if (btn == 'yes')
			     
				  createphotosbyowner(idcounty1,pid);
				 },
		   animEl: 'elId',
		   icon: Ext.MessageBox.QUESTION
		});
	}

}


function createphotosbyowner(idcounty2,pid2){	

window.scrollTo(0,0);
	
	if(document.getElementById('photosbyownerTab')){
		var tab = tabbyowner.getItem('photosbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('reviewbyownerTab')){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('compartirbyownerTab')){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.remove(tab);  }

		
	var url='byowner/properties_photos_byowner.php?county='+idcounty2+'&pid='+pid2;
	
		 	
	tabbyowner.add({
		title: ' Step Four ',
		id: 'photosbyownerTab',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, 
		params:{
			    idcounty:idcounty2,  
			     property:document.getElementById('property').value,
				 parcelid:document.getElementById('folio').value,
				
				
	       	}},
		closable: true,
		
				}).show();
		
	
	
	if(Ext.isIE){
		var tab = tabbyowner.getItem('photosbyownerTab');
		tabbyowner.setActiveTab(tab);
		tab.getEl().repaint();
	}
}


function confirmar2(idcounty2){
	
		if(document.getElementById('compartirbyownerTab')){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.remove(tab);  }
		
			
	var url='byowner/properties_compartir_byowner.php?county='+idcounty2;
	
		 	
	tabbyowner.add({
		title: ' Step Five ',
		id: 'compartirbyownerTab',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, 
		params:{
			    idcounty:idcounty2,  
			     property:document.getElementById('property').value,
				 parcelid:document.getElementById('folio').value,
				
				
	       	}},
		closable: true,
		
				}).show();
		
	
	
	if(Ext.isIE){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.setActiveTab(tab);
		tab.getEl().repaint();
	}
	
}


function confirmar3(idcounty1){	
  
    var lprice=document.getElementById('lprice').value;
    var zip= document.getElementById('zip').value;
    var city=document.getElementById('city').value;
	var address=document.getElementById('address').value;
	var garea=document.getElementById('garea').value;
    var bath= document.getElementById('bathby').value;
    var beds=document.getElementById('beds').value;
	var hbath=document.getElementById('hbath').value;
	var stories= document.getElementById('stories').value;
    var lotsize=document.getElementById('lotsize').value;
	var units=document.getElementById('units').value;
   
   if (garea.length==0) document.getElementById('garea').value=0;
    if (bath.length==0) document.getElementById('bathby').value=0;
	  if (beds.length==0) document.getElementById('beds').value=0;
	    if (hbath.length==0) document.getElementById('hbath').value=0;
	if (stories.length==0) document.getElementById('stories').value=0;
	  if (lotsize.length==0) document.getElementById('lotsize').value=0;
	    if (units.length==0) document.getElementById('units').value=0;
   
    if(lprice.length==0){
		alert("Required field Lprice");return;
	      }else{
			    if(city.length==0){
                alert("Required field City");return;
				} else {
					if(address.length==0){
                    alert("Required field Address");return;
					}else{
						 if(zip.length==0){
                         alert("Required field Zip code");return;
						   }
						 
					}}}
	
	 if(lprice.length!=0 || city.length!=0 || address.length!=0 || zip.length!=0 )
	{			
		createreviewbyowner(idcounty1);	
		//Ext.Msg.show({
//		   title:'Save Changes?',
//		   msg: 'Would you like to apply your changes?',
//		   buttons: Ext.Msg.YESNO,
//		   fn: function (btn)
//			   {
//			   if (btn == 'yes')
//				  
//				 },
//		   animEl: 'elId',
//		   icon: Ext.MessageBox.QUESTION
//		});
	}

}


function createreviewbyowner(idcounty2){	

window.scrollTo(0,0);
	
	if(document.getElementById('reviewbyownerTab')){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.remove(tab);
	}
	
		
	var url='byowner/properties_review_byowner.php';
		 	
	tabbyowner.add({
		title: ' Step Six ',
		id: 'reviewbyownerTab',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, 
		params:{
			    idcounty:idcounty2,  
			     property:document.getElementById('property').value,
				 parcelid:document.getElementById('folio').value,
				 listdate:document.getElementById('listdate').value,
				 //ximanumber:document.getElementById('ximanumber').value,
				 lprice:document.getElementById('lprice').value,
				 //location
				 state:document.getElementById('state').value,
				 city:document.getElementById('city').value,
				 address :document.getElementById('address').value,
				 sdname:document.getElementById('sdname').value,
				 county:document.getElementById('county').value,
				 unit :document.getElementById('unit').value,
				 zip:document.getElementById('zip').value,
		          //Property Details
		         proptype:document.getElementById('type').value,
				 larea:document.getElementById('larea').value,
				 garea :document.getElementById('garea').value,
				 bath:document.getElementById('bathby').value,
				 beds:document.getElementById('beds').value,
				 hbath :document.getElementById('hbath').value,
				 location :document.getElementById('location').value,
				 constype:document.getElementById('constype').value,
				 wf:document.getElementById('wf').value,
				 built :document.getElementById('built').value,
				 stories:document.getElementById('stories').value,
				 lotsize:document.getElementById('lotsize').value,
				 pool:document.getElementById('pool').value,
				 units:document.getElementById('units').value,
				 //Other details
				 financial:document.getElementById('financial').value,
				 community:document.getElementById('community').value,
				 fencing :document.getElementById('fencing').value,
				 hc:document.getElementById('hc').value,
				 utilities:document.getElementById('utilities').value,
				 parking :document.getElementById('parking').value,
				 roof :document.getElementById('roof').value,
				 flooring:document.getElementById('flooring').value,
				 inclusions:document.getElementById('inclusions').value,
				 remark:document.getElementById('remark').value,
				 // owner
				 owner:document.getElementById('owner').value,
				 owner_email:document.getElementById('owner_email').value,
				 owner_fax:document.getElementById('owner_fax').value,
				 owner_ph1:document.getElementById('owner_ph1').value,
				  owner_web:document.getElementById('owner_web').value,
				 owner_ph2:document.getElementById('owner_ph2').value
				 
		}},
		closable: true,
		
				}).show();
		
	
	
	if(Ext.isIE){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.setActiveTab(tab);
		tab.getEl().repaint();
	}
}


function save_byowner(idcounty3){	
  
	 Ext.Ajax.request({  
								waitMsg: 'Save By Owner...',
								url: 'byowner/save_byowner.php', 
								method: 'POST', 
								params: { 
				idcounty:idcounty3,  	
				 shareinvestor:document.getElementById('shareinvestor').value,
				 sharerealtor:document.getElementById('sharerealtor').value,			
				 property:document.getElementById('property').value,
				 parcelid:document.getElementById('folio').value,
				 listdate:document.getElementById('listdate').value,
				 //ximanumber:document.getElementById('ximanumber').value,
				 lprice:document.getElementById('lprice').value,
				 //location
				 state:document.getElementById('state').value,
				 city:document.getElementById('city').value,
				 address :document.getElementById('address').value,
				 sdname:document.getElementById('sdname').value,
				 county:document.getElementById('county').value,
				 unit :document.getElementById('unit').value,
				 zip:document.getElementById('zip').value,
		          //Property Details
		         proptype:document.getElementById('type').value,
				 larea:document.getElementById('larea').value,
				 garea :document.getElementById('garea').value,
				 bath:document.getElementById('bathby').value,
				 beds:document.getElementById('beds').value,
				 hbath :document.getElementById('hbath').value,
				 location :document.getElementById('location').value,
				 constype:document.getElementById('constype').value,
				 wf:document.getElementById('wf').value,
				 built :document.getElementById('built').value,
				 stories:document.getElementById('stories').value,
				 lotsize:document.getElementById('lotsize').value,
				 pool:document.getElementById('pool').value,
				 units:document.getElementById('units').value,
				 //Other details
				 financial:document.getElementById('financial').value,
				 community:document.getElementById('community').value,
				 fencing :document.getElementById('fencing').value,
				 hc:document.getElementById('hc').value,
				 utilities:document.getElementById('utilities').value,
				 parking :document.getElementById('parking').value,
				 roof :document.getElementById('roof').value,
				 flooring:document.getElementById('flooring').value,
				 inclusions:document.getElementById('inclusions').value,
				 remark:document.getElementById('remark').value,
				 // owner
				 owner:document.getElementById('owner').value,
				 owner_email:document.getElementById('owner_email').value,
				 owner_fax:document.getElementById('owner_fax').value,
				 owner_ph1:document.getElementById('owner_ph1').value,
				  owner_web:document.getElementById('owner_web').value,
				 owner_ph2:document.getElementById('owner_ph2').value},
								
								success:function(response,options)
								{
									if(response.responseText== 'true' || response.responseText==true)
										Ext.Msg.alert('Mensaje', 'Property added successfully to My Listing.');
									else
										Ext.Msg.alert('Mensaje', response.responseText);
								},
								failure:function(response,options)
								{
									Ext.Msg.alert("Failure", response.responseText);
								}
								
																
						   });//ajax         
   
	  if(document.getElementById('resultbyownerTab')){
		var tab = tabbyowner.getItem('resultbyownerTab');
		tabbyowner.remove(tab);
	}
	   if(document.getElementById('overviewbyownerTab')){
		var tab = tabbyowner.getItem('overviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('reviewbyownerTab')){
		var tab = tabbyowner.getItem('reviewbyownerTab');
		tabbyowner.remove(tab);
	}
	if(document.getElementById('photosbyownerTab')){
		var tab = tabbyowner.getItem('photosbyownerTab');
		tabbyowner.remove(tab);
	}
		if(document.getElementById('compartirbyownerTab')){
		var tab = tabbyowner.getItem('compartirbyownerTab');
		tabbyowner.remove(tab);  }
}

function share(idcounty,pid){
	var astatus='A';
	    Ext.Ajax.request({  
								waitMsg: 'Save My Byowner...',
								url: 'mylisting/properties_mybyowner.php?accion=1', 
								method: 'POST', 
								params: { 
								db:idcounty,
								pid:pid,
								astatus:astatus},
								
								success:function(response,options)
								{
									if(response.responseText== 'true' || response.responseText==true)
										Ext.Msg.alert('Mensaje', 'Property added successfully to My Byowner.');
									else
										Ext.Msg.alert('Mensaje', response.responseText);
								},
								failure:function(response,options)
								{
									Ext.Msg.alert("Failure", response.responseText);
								}
								
																
						   });//ajax                 
}	

