<?php 	
	include('../properties_conexion.php');
	conectar();
?>
<div align="left" style="height:100%">
	<div id="body_central2" style="height:100%">
        <div id="mapSearchAdv" style="display:none; width:99%;height:320;border: medium solid #b8dae3;position:relative; margin-bottom:5px;"></div>

		<div id="advSearchd" style="padding-top:2px;"></div>
        <script> 
//            if(document.getElementById('county_search').value!=-1) OcultarMostrar(true);
        </script>        
	</div>
</div>
<script language="javascript">

Ext.namespace('Ext.combos_selec');

Ext.combos_selec.combocbCounty = [
<?php
	$query='select idcounty,county FROM xima.lscounty where ximapro="Y" order by county';
	$res=mysql_query($query)or die(mysql_error());
	$i=0;
//	echo '[\'-1\',\'ALL\']';
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ',';
		echo '[\''.$r['idcounty'].'\',\''.$r['county'].'\']';
		$i++;
	}
?>
];
</script>
<script>

	var dataString = new Ext.data.SimpleStore({
		fields: ['id'],
	 	data  : [['Start With'],['Exact'],['Contains'],['Not Start With'],['Not Exact'],['Not Contains']]
	});
	var dataIntDate=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [['Equal'],['Greater Than'],['Less Than'],['Equal or Less'],['Equal or Greater']]
		//data  : [['Equal'],['Greater Than'],['Less Than'],['Equal or Less'],['Equal or Greater'],['Between']]
	});
	var dataBoolean=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [['Select'],['Yes'],['No']]
	});

	function selectBetween(combo,record,index)
	{
		var arfield=combo.getId().replace("cb","tx");
		arfield=arfield.split('*');
		var txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3];
//alert(arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+arfield[3]+"-->"+txfield);
		
		if(record.get('id')=='Between')
		{
			Ext.getCmp(txfield).setValue('');
			if(!Ext.getCmp(txfield).isVisible())
			{
				Ext.getCmp(txfield).setVisible(true);
			}
		}
		else
		{
			if(Ext.getCmp(txfield).isVisible())
				Ext.getCmp(txfield).setVisible(false);
		}
	}
	
	function hideBetween(where)
	{
		var arfield;
		var txfield;
		var elementos = document.forms[1].elements.length;
		for(i=0; i<elementos; i++)
		{
			if(findstring(document.forms[1].elements[i].id) && Ext.getCmp(document.forms[1].elements[i].id).isVisible())
			{
				if(where=='ALL')
					Ext.getCmp(document.forms[1].elements[i].id).setVisible(false);
				else
				{
					arfield=document.forms[1].elements[i].id.split('*');
					txfield=arfield[0]+"*"+arfield[1]+"*"+arfield[2]+"*other"+where;
					Ext.getCmp(txfield).setVisible(false);
				}
			}
		}
	}

	function findstring(cadena)
	{
		pat = /other/
		if(pat.test(cadena))
			return true
		return false
	}
	
	function expandactive(combo,record,index)
	{
		var val=record.get('valuec');
		if(val=='FR')val='FS';
		if(val=='BOR')val='BO';
		var cprop = Ext.getCmp('nproptype');        
        cprop.clearValue();
		cprop.store.removeAll();
		
		switch (val)
		{
			case 'PR': case 'FO': case 'MO'://public records  foreclosure
				cprop.store.add(new cprop.store.recordType({valuec:'*',textc:'Any Type'}));
				cprop.store.add(new cprop.store.recordType({valuec:'01',textc:'Single Family'}));
				cprop.store.add(new cprop.store.recordType({valuec:'04',textc:'Condo/Town/Villa'}));
				cprop.store.add(new cprop.store.recordType({valuec:'03',textc:'Multi Family +10'}));
				cprop.store.add(new cprop.store.recordType({valuec:'08',textc:'Multi Family -10'}));
				cprop.store.add(new cprop.store.recordType({valuec:'11',textc:'Commercial'}));
				cprop.store.add(new cprop.store.recordType({valuec:'00',textc:'Vacant Land'}));
				cprop.store.add(new cprop.store.recordType({valuec:'02',textc:'Mobile Home'}));
				cprop.store.add(new cprop.store.recordType({valuec:'99',textc:'Other'}));
			break;
			case 'FS':case 'BO':  //listing
				cprop.store.add(new cprop.store.recordType({valuec:'*',textc:'Any Type'}));
				cprop.store.add(new cprop.store.recordType({valuec:'01',textc:'Single Family'}));
				cprop.store.add(new cprop.store.recordType({valuec:'04',textc:'Condo/Town/Villa'}));
				cprop.store.add(new cprop.store.recordType({valuec:'08',textc:'Multi Family'}));
				cprop.store.add(new cprop.store.recordType({valuec:'11',textc:'Commercial'}));
				cprop.store.add(new cprop.store.recordType({valuec:'00',textc:'Vacant Land'}));
				cprop.store.add(new cprop.store.recordType({valuec:'02',textc:'Mobile Home'}));
			break;
		}
		cprop.setValue('*');		
		
		
		var cfore = Ext.getCmp('nforeclosure');        
        cfore.clearValue();
		cfore.store.removeAll();
		if(val=='FO')//foreclosure
		{
				cfore.store.add(new cfore.store.recordType({valuec:'-1',textc:'Any'}));
				cfore.store.add(new cfore.store.recordType({valuec:'P',textc:'Pre-Foreclosed'}));
				cfore.store.add(new cfore.store.recordType({valuec:'F',textc:'Foreclosed'}));
		}
		else//other outpput NOT foreclosure
		{
				cfore.store.add(new cfore.store.recordType({valuec:'-1',textc:'Any'}));
				cfore.store.add(new cfore.store.recordType({valuec:'N',textc:'No'}));
				cfore.store.add(new cfore.store.recordType({valuec:'P',textc:'Pre-Foreclosed'}));
				cfore.store.add(new cfore.store.recordType({valuec:'F',textc:'Foreclosed'}));
		}
		cfore.setValue('-1');		
		
		expandcollapse(val);
	}

	function expandcollapse(section)
	{
		Ext.getCmp('PR').collapse();
		Ext.getCmp('FS').collapse();
		Ext.getCmp('FO').collapse();
		Ext.getCmp('MO').collapse();
		Ext.getCmp('BO').collapse();
		Ext.getCmp(section).expand();
	}
	var proptype=new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : [['*','Any Type'],['01','Single Family'],['04','Condo/Town/Villa'],['03','Multi Family +10'],['08','Multi Family -10'],
										['11','Commercial'],['00','Vacant Land'],['02','Mobile Home'],['99','Other']]
							})
	
	var inpubr=0;var inlis=0;var infor=0;var inmor=0;var inpubrmore=0;var inlismore=0;var inbo=0;var inbomore=0;
	
	var Contador =0;
  	function agregar ()
  	{
    	var textNew = new Ext.form.TextField({
            fieldLabel          : 'Name',
            name                : 'ruleSetName' + Contador,
            id                  : 'ruleSetName' + Contador,
            anchor              : '90%',
            allowBlank          : false,
            grow                : false
        });
        Contador ++;
        Ext.getCmp('idcompfield29').add ( textNew );//el "getCmp" permite obtener el elemento por medio de su ID =GetElement
        Ext.getCmp('idcompfield29').doLayout();
	}
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Processing...',
		url: 'searchs_types/advance_search_built.php', 
		method: 'POST',
		//timeout :600000
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			loading_win.show();
			var aRes=response.responseText.split("^");
			
			var publicrecord=Ext.decode(aRes[0]);
			var listing=Ext.decode(aRes[1]);
			var foreclosure=Ext.decode(aRes[2]);
			var mortgage=Ext.decode(aRes[3]);
			var publicrecordmore=Ext.decode(aRes[4]);
			var listingmore=Ext.decode(aRes[5]);
			var byowner=Ext.decode(aRes[6]);
			var byownermore=Ext.decode(aRes[7]);
			
			//alert(aRes[0]);	alert(aRes[1]);	alert(aRes[2]);	alert(aRes[3]);			
			formulsearchadv = new Ext.FormPanel({
				url:'properties_coresearch.php',
				frame:true,
				bodyStyle:'padding:5px 5px 0;text-align:left;',
				renderTo: 'advSearchd',
				id: 'formulsearchadv',
				name: 'formulsearchadv',
				items:[{
					xtype: 'fieldset',
					title: 'Search',
					layout:'column',
					items: [{
						layout: 'form',
						columnWidth: .33,
						items:[{
							xtype: 'hidden',
							id: 'mapa_search_latlongAdv',
							name: 'mapa_search_latlongAdv',
							hiddenName: 'mapa_search_latlongAdv',
							value: '-1'
						},{
							xtype: 'combo',
							labelWidth: 120,
							name: 'nproperty',
							id: 'nproperty',
							hiddenName: 'ocproperty',
							store: new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : [['PR','Public Records'],['FS','For Sale'],['FR','For Rent'],['BO','By Owner'],['BOR','By Owner Rent'],['FO','Foreclosures'],['MO','Mortgage']]
							}),				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'Property',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value: 'PR',
							width: 105,
							listeners: {'select': expandactive}	
						},{
							xtype: 'combo',
							labelWidth: 120,
							name: 'ncounty',
							id: 'ncounty',
							hiddenName: 'occounty',
							store: new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : Ext.combos_selec.combocbCounty
							}),				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'County',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value: '1',
							width: 105,
							listeners: {
								select : function(){
									mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
								}
							}
						}]
					},{
						layout: 'form',
						columnWidth: .33,
						items:[{
							xtype: 'combo',
							labelWidth: 120,
							name: 'nproptype',
							id: 'nproptype',
							hiddenName: 'ocproptype',
							store: proptype,				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'Property Type',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value:'*',
							width: 105
							
						},{
							xtype: 'combo',
							labelWidth: 120,
							name: 'nforeclosure',
							id: 'nforeclosure',
							hiddenName: 'ocforeclosure',
							store: new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : [['-1','Any'],['N','No'],['P','Pre-Foreclosed'],['F','Foreclosed']]
							}),				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'Foreclosure',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value: '-1',
							width: 105							
						}]
					},{
						layout: 'form',
						columnWidth: .33,
						items:[{
							xtype: 'combo',
							labelWidth: 120,
							name: 'nstate',
							id: 'nstate',
							hiddenName: 'ocstate',
							store: new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : [['FL','Florida']]
							}),				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'State',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value: 'FL',
							width: 105	
						},{
							xtype: 'combo',
							labelWidth: 120,
							name: 'nownerocc',
							id: 'nownerocc',
							hiddenName: 'ocownerocc',
							store: new Ext.data.SimpleStore({
								fields: ['valuec', 'textc'],
								data : [['*','Select'],['Y','Yes'],['N','No']]
							}),				
							editable: false,
							displayField: 'textc',
							valueField: 'valuec',
							typeAhead: true,
							fieldLabel: 'O. Occupied',
							allowBlank:false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Select ...',
							selectOnFocus:true,
							autoSelect:true,
							value: '*',
							width: 105	
						}]
					}]
				},{
					xtype: 'fieldset',
					title: 'Public Records',
					collapsible : true,
					collapsed : true,
					id:'PR',
					name:'PR',
					listeners: {
						expand: function(){
							if(inpubr==1)return;
							inpubr=1;
							this.removeAll()
							this.add({
								layout:'column',
								items: publicrecord
							},{
								xtype: 'fieldset',
								title: 'More Fields',
								collapsible : true,
								collapsed : true,
								id:'PRMORE',
								name:'PRMORE',
								listeners: {
									expand: function(){
										if(inpubrmore==1)return;
										inpubrmore=1;
										this.removeAll()
										this.add({
											layout:'column',
											items: publicrecordmore
										});
										this.doLayout();
										//hideBetween('PRMORE');
									}
								}
							});
							this.doLayout();
						}
					}
				},{
					xtype: 'fieldset',
					title: 'For Sale/For Rent',
					collapsible : true,
					collapsed : true,
					id:'FS',
					name:'FS',
					listeners: {
						expand: function(){
							if(inlis==1)return;
							inlis=1;
							this.removeAll()
							this.add({
								layout:'column',
								items: listing
							},{
								xtype: 'fieldset',
								title: 'More Fields',
								collapsible : true,
								collapsed : true,
								id:'FSMORE',
								name:'FSMORE',
								listeners: {
									expand: function(){
										if(inlismore==1)return;
										inlismore=1;
										this.removeAll()
										this.add({
											layout:'column',
											items: listingmore
										});
										this.doLayout();
										//hideBetween('FSMORE');
									}
								}
							});
							this.doLayout();
							//hideBetween('FS');
						}
					}
				},{
					xtype: 'fieldset',
					title: 'By Owner/By Owner Rent',
					collapsible : true,
					collapsed : true,
					id:'BO',
					name:'BO',
					listeners: {
						expand: function(){
							if(inbo==1)return;
							inbo=1;
							this.removeAll()
							this.add({
								layout:'column',
								items: byowner
							},{
								xtype: 'fieldset',
								title: 'More Fields',
								collapsible : true,
								collapsed : true,
								id:'BOMORE',
								name:'BOMORE',
								listeners: {
									expand: function(){
										if(inbomore==1)return;
										inbomore=1;
										this.removeAll()
										this.add({
											layout:'column',
											items: byownermore
										});
										this.doLayout();
										//hideBetween('BOMORE');
									}
								}
							});
							this.doLayout();
							//hideBetween('BO');
						}
					}
				},{
					xtype: 'fieldset',
					title: 'Foreclosure',
					collapsible : true,
					collapsed : true,
					id:'FO',
					name:'FO',
					listeners: {
						expand: function(){
							if(infor==1)return;
							infor=1;
							this.removeAll()
							this.add({
								layout:'column',
								items: foreclosure
							});
							this.doLayout();
							//hideBetween('FO');
						}
					}
				},{
					xtype: 'fieldset',
					title: 'Mortgage',
					collapsible : true,
					collapsed : true,
					id:'MO',
					name:'MO',
					listeners: {
						expand: function(){
							if(inmor==1)return;
							inmor=1;
							this.removeAll()
							this.add({
								layout:'column',
								items: mortgage
							});
							this.doLayout();
							//hideBetween('MO');
						}
					}
				}],//end items fieldset
				listeners: {
					afterrender : function(){
						expandcollapse('PR');
						//_IniMAPAdv(); 
						//hideBetween('ALL');
					}
				}
			});
			loading_win.hide();
			
		}                                
	});

</script>
<script> 
	if(mapSearchAdv == null) 
	{
		mapSearchAdv = new XimaMap('mapSearchAdv','regionAdv','4pointsAdv','mapa_search_latlongAdv',
								   'control_mapa_divAdv','_panAdv','_drawAdv','_polyAdv','_clearAdv','_maxminAdv');
	}
	mapSearchAdv._IniMAP(); 
</script>