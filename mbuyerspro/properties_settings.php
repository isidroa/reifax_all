<link href="includes/css/properties_register.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="includes/properties_myaccount.js"></script>
<script type="text/javascript" src="includes/validateNewUser.js"></script>
<script type="text/javascript" src="includes/errorMail.js"></script>
<script type="text/javascript" src="includes/errorLicence.js"></script>
<script type="text/javascript" src="includes/checkexecutive.js"></script>
<script type="text/javascript" src="includes/errorOffice.js"></script>
<script type="text/javascript" src="includes/usersuspend.js"></script>
<script type="text/javascript" src="includes/Products.js"></script>

<!--<script type="text/javascript" src="includes/tooltip.js"></script>
<script type="text/javascript" src="includes/subscription.js"></script>
<script type="text/javascript" src="includes/js/mostrardiv.js"></script>
<script type="text/javascript" src="includes/norightclick.js"></script>
<script type="text/javascript" src="includes/disablekey.js"></script>-->

<?php
include('properties_conexion.php');
conectar();

$que="SELECT * FROM xima.ximausrs x
INNER JOIN xima.permission p ON (x.userid=p.userid) 
WHERE x.userid=".$_COOKIE['datos_usr']['USERID'];
$result=mysql_query($que) or die($que.mysql_error());
$USERCURRENT=mysql_fetch_array($result);
?>


<div align="left" style="height:100%">
    <div id="body_central" style="height:100%">
        <div id="tabs3" style="padding-top:2px;"></div>
    </div>
</div>

<script>
var tabs3=null;
var ancho=640;
if(user_loged) ancho=system_width;

var tb = new Ext.Toolbar({
        items: [
<?php
    if($USERCURRENT['idstatus']!=5 && $USERCURRENT['idstatus']!=6 && $USERCURRENT['idstatus']!=7){
?>
            {
                text: ' Personal Settings ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/personalsettings.php'});
                }
			}
			<?php
			if($USERCURRENT['idstatus']!=2){
			?>
			,{
                text: ' Defaults ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/defaultcounty.php'});
				}
            }/*,{
                text: ' Banners ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/banners.php'});
                }
            },{
                text: ' Cancel Acc. ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/cancelaccount.php'});
                }
            },{
                text: ' Freeze Acc. ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/freezeaccount.php'});
                }
			}*/
			<?php } ?>
<?php
    }
    elseif($USERCURRENT['idstatus']==6 || $USERCURRENT['idstatus']==7)//ESTA FREZZE EL USUARIO
    {
?>
            {
                text: ' Unfreeze Account ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/unfreezeaccount.php'});
                }
            }
<?php
	}else{
?>
            {
                text: ' Account ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/activeuserinactive.php'});
                }
            }
<?php
    }
?>
        ],
        autoShow: true
    });


	tabs3 = new Ext.TabPanel({
		id: 'secundario',
		renderTo: 'tabs3',
		activeTab: 0,
		width: ancho,
		height: tabs.getHeight(),
		plain:true,
		enableTabScroll:true,
		defaults:{  autoScroll: false},
		items:[
			  {
				title: 'My Account',
				id: 'AccountTab3',
				tbar: tb,
				<?php if($USERCURRENT['idstatus']!=5 && $USERCURRENT['idstatus']!=6 && $USERCURRENT['idstatus']!=7){?>
				autoLoad: {url: 'mysetting_tabs/myaccount_tabs/personalsettings.php', scripts: true}
				<?php }elseif($USERCURRENT['idstatus']==6 || $USERCURRENT['idstatus']==7){?>
				autoLoad: {url: 'mysetting_tabs/myaccount_tabs/unfreezeaccount.php', scripts: true}
				<?php }else{?>
				autoLoad: {url: 'mysetting_tabs/myaccount_tabs/activeuserinactive.php', scripts: true}
				<?php }?>
			  }
		]
	});

</script>