<?php 
//print_r($_COOKIE['query_search']);
$_SERVERXIMA="http://www.reifax.com/";
$realtor=$_POST['userweb']=="false" ? false:true;	
$realtorid=$_POST['realtorid'];

?>
<div align="left" style="height:100%">
	<div id="body_central" style="height:100%">
		<div id="tabsSearchs" style="padding-top:2px;"></div>
	</div>
</div>

<script>
var tabsSearchs=null;var formulsearchadv;
var ancho=640;
<?php if($_COOKIE['datos_usr']['status']!='realtorweb'){?>
if(user_loged) ancho=system_width;
<?php }?>

var icon_result=icon_mylabel=false;
var ResultTemplate=-1;
if(realtor_block!=false){
	var icon_result=true;
	var icon_mylabel=true;
}
if(user_web!=false)	var icon_mylabel=true;


tabsSearchs = new Ext.TabPanel({
	renderTo: 'tabsSearchs',
	activeTab: 0,
	width: ancho,
	height: tabs.getHeight(),
	plain:true,
	enableTabScroll:true,
	defaults:{	autoScroll: false},
	items:[
		{
			title: ' Basic Search ',
			id: 'searchTabBasic',
			name: 'searchTabBasic',
			disabled: <?php echo $_POST['tipo_login'];?>,
			autoLoad: {url: 'searchs_types/basic_search.php', scripts:true, params: {userweb:'<?php echo $_POST['userweb'];?>', search_type: '<?php echo $_POST['search_type'];?>', search_by_type: '<?php echo $_POST['search_type'];?>', state_search: '<?php echo $_POST['search_state'];?>', county_search: '<?php echo $_POST['search_county'];?>'}},
			tbar: new Ext.Toolbar({
						cls: 'no-border-search',
						width: 'auto',
						items: [' ',{
							 tooltip: 'Click to Search by Map',
							 iconCls:'icon',
							 iconAlign: 'top',
							 width: 40,
							 icon: 'http://www.reifax.com/img/toolbar/map.png',
							 scale: 'medium',
							 handler: function (){
							 
								 if(document.getElementById("mapSearch").style.display=='none'){
									 doSearchByFilter('MAP');
									 document.getElementById(search_type+'_combo_search_by').value = 'MAP';
								 }else{
									 doSearchByFilter('MAP_OFF');
									 document.getElementById(search_type+'_combo_search_by').value = 'LOCATION';
								 }
							 }
							},{
							 tooltip: 'Click to Xray Report',
							 iconCls:'icon',
							 iconAlign: 'top',
							 width: 40,
							 icon: 'http://www.reifax.com/img/toolbar/exray.png',
							 scale: 'medium',
							 handler: function(){
								 var bd = document.getElementById(search_type+'_county_search').value;
								 var latlong = document.getElementById('mapa_search_latlong').value;
								 var xcode = document.getElementById(search_type+'_proptype').value;
								 if(Ext.isEmpty(xcode)){ 
									Ext.Msg.alert("XRay Report", 'Property Type is requiered to execute the XRay Report.');
									return false;
								 }
								 if(latlong=='-1'){
									 Ext.Msg.alert("XRay Report", 'Map shape/polygon is requiered to execute the XRay Report.');
									 return false;
								 }
															 
								 if(document.getElementById('reportsTab')){
									 var tab = tabs.getItem('reportsTab');
									 tabs.remove(tab);
								 }
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
								tabs.add({
									title: ' Reports ',
									id: 'reportsTab',
									autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
									closable: true,
									tbar: new Ext.Toolbar({
										cls: 'no-border',
										width: 'auto',
										items: [' ',{
											 tooltip: 'Click to Print XRay Report.',
											 iconCls:'icon',
											 iconAlign: 'top',
											 width: 40,
											 icon: 'http://www.reifax.com/img/toolbar/printer.png',
											 scale: 'medium',
											 handler: function(){
												Ext.Ajax.request( 
												{  
													waitMsg: 'Printing...',
													url: 'imprimir/properties_xray_print.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														db: bd,
														proper: xcode,
														type: 'P',
														latlong: latlong
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														
														var results=response.responseText;
														if(Ext.isIE)
															window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
														 else
															window.open(results,'_newtab');
													}                                
												});
											 }
										},{
											 tooltip: 'Click to Save XRay Report.',
											 iconCls:'icon',
											 iconAlign: 'top',
											 width: 40,
											 icon: 'http://www.reifax.com/img/toolbar/save.png',
											 scale: 'medium',
											 handler: function(){
												 var simple = new Ext.FormPanel({
													url: 'imprimir/properties_xray_print.php',
													frame:true,
													title: 'Saved Documents.',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													
													items: [{
																xtype     : 'textfield',
																name      : 'name_save',
																fieldLabel: 'Name',
																value     : '',
																width: 200
															},{
																xtype     : 'hidden',
																name      : 'db',
																value     : bd
															},{
																xtype     : 'hidden',

																name      : 'proper',
																value     : xcode
															},{
																xtype     : 'hidden',
																name      : 'type',
																value     : 'S'
															},{
																xtype     : 'hidden',
																name      : 'latlong',
																value     : latlong
															}],
													
													buttons: [{
															text: 'Save',
															handler: function(){
																loading_win.show();
																simple.getForm().submit({
																	success: function(form, action) {
																		loading_win.hide();
																		win.close();
																		Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																	},
																	failure: function(form, action) {
																		loading_win.hide();
																		Ext.Msg.alert("Failure", action.result.msg);
																	}
																});
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	simple.getForm().reset();
																	win.close();
																}
														}]
													});
												 
												var win = new Ext.Window({
													layout      : 'fit',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: simple,
													closeAction : 'hide',
													buttons: [{
														text     : 'Close',
														handler  : function(){
															win.close();
														}
													}]
												});
												win.show();
											 }
										}]
									})
								}).show();
								 
							 }
							},{
							 tooltip: 'Click to Rebate Report',
							 iconCls:'icon',
							 iconAlign: 'top',
							 width: 40,
							 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
							 scale: 'medium',
							 handler: function(){
								 var bd = document.getElementById(search_type+'_county_search').value;
								 var latlong = document.getElementById('mapa_search_latlong').value;
								 var xcode = document.getElementById(search_type+'_proptype').value;
								 if(Ext.isEmpty(xcode)){ 
									Ext.Msg.alert("Rebate Report", 'Property Type is requiered to execute the Rebate Report.');
									return false;
								 }
								 if(latlong=='-1'){
									 Ext.Msg.alert("Rebate Report", 'Map shape/polygon is requiered to execute the Rebate Report.');
									 return false;
								 }
															 
								 if(document.getElementById('reportsTab')){
									 var tab = tabs.getItem('reportsTab');
									 tabs.remove(tab);
								 }
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
								tabs.add({
									title: ' Reports ',
									id: 'reportsTab',
									autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
									closable: true,
									tbar: new Ext.Toolbar({
										cls: 'no-border',
										width: 'auto',
										items: [' ',{
											 tooltip: 'Click to Print Rebate Report.',
											 iconCls:'icon',
											 iconAlign: 'top',
											 width: 40,
											 icon: 'http://www.reifax.com/img/toolbar/printer.png',
											 scale: 'medium',
											 handler: function(){
												Ext.Ajax.request( 
												{  
													waitMsg: 'Printing...',
													url: 'imprimir/properties_rebate_print.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														db: bd,
														proper: xcode,
														type: 'P',
														latlong: latlong
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														
														var results=response.responseText;
														if(Ext.isIE)
															window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
														 else
															window.open(results,'_newtab');
													}                                
												});
											 }
										},{
											 tooltip: 'Click to Save Rebate Report.',
											 iconCls:'icon',
											 iconAlign: 'top',
											 width: 40,
											 icon: 'http://www.reifax.com/img/toolbar/save.png',
											 scale: 'medium',
											 handler: function(){
												 var simple = new Ext.FormPanel({
													url: 'imprimir/properties_rebate_print.php',
													frame:true,
													title: 'Saved Documents.',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													
													items: [{
																xtype     : 'textfield',
																name      : 'name_save',
																fieldLabel: 'Name',
																value     : '',
																width: 200
															},{
																xtype     : 'hidden',
																name      : 'db',
																value     : bd
															},{
																xtype     : 'hidden',

																name      : 'proper',
																value     : xcode
															},{
																xtype     : 'hidden',
																name      : 'type',
																value     : 'S'
															},{
																xtype     : 'hidden',
																name      : 'latlong',
																value     : latlong
															}],
													
													buttons: [{
															text: 'Save',
															handler: function(){
																loading_win.show();
																simple.getForm().submit({
																	success: function(form, action) {
																		loading_win.hide();
																		win.close();
																		Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																	},
																	failure: function(form, action) {
																		loading_win.hide();
																		Ext.Msg.alert("Failure", action.result.msg);
																	}
																});
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	simple.getForm().reset();
																	win.close();
																}
														}]
													});
												 
												var win = new Ext.Window({
													layout      : 'fit',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: simple,
													closeAction : 'hide',
													buttons: [{
														text     : 'Close',
														handler  : function(){
															win.close();
														}
													}]
												});
												win.show();
											 }
										}]
									})
								}).show();
								 
							 }
							},{
							 tooltip: 'Click to Manage Search',
							 iconCls:'icon',
							 iconAlign: 'top',
							 width: 40,
							 icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
							 scale: 'medium',
							 handler: function(){
								 //alert(user_loged+"||"+user_block+"||"+user_web);
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 ShowSavedSearch();
							 }
							},'->'
							,{
								iconCls:'icon',
								icon: 'http://www.reifax.com/img/toolbar/reset.png',
								scale: 'medium',
								text: 'Reset&nbsp;&nbsp; ',
								handler  : function(){
									document.getElementById('pformul').reset();
								}
							},
							' '
							,{
								iconCls:'icon',
								icon: 'http://www.reifax.com/img/toolbar/search.png',
								scale: 'medium',
								text: 'Search&nbsp;&nbsp; ',
								handler  : function(){
									searchForm(search_type+'_search','xxx');
								}				
							}]
					})
			
		}
		<?php if(isset($_COOKIE['datos_usr']['USERID']) && $_COOKIE['datos_usr']['status']!='realtorweb' && $_COOKIE['datos_usr']['status']!='investorweb'){?> 
		,{
			title: ' Advanced Search ',
			id: 'searchTabAdv',
			name: 'searchTabAdv',
			disabled: <?php echo $_POST['tipo_login'];?>,
			autoLoad: {url: 'searchs_types/advance_search.php', scripts:true, params: {userweb:'<?php echo $_POST['userweb'];?>', search_type: '<?php echo $_POST['search_type'];?>', search_by_type: '<?php echo $_POST['search_type'];?>', state_search: '<?php echo $_POST['search_state'];?>', county_search: '<?php echo $_POST['search_county'];?>'}}, 
			tbar: new Ext.Toolbar({
				cls: 'no-border-search',
				width: 'auto',
				items: [' ',{
					 tooltip: 'Click to Search by Map',
					 iconCls:'icon',
					 iconAlign: 'top',
					 width: 40,
					 icon: 'http://www.reifax.com/img/toolbar/map.png',
					 scale: 'medium',
					 handler: function (){
						
						if(document.getElementById("mapSearchAdv").style.display=='none'){
							mapSearchAdv.control_map();
							mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
							search_by_typeAdv='MAP';
						 }else{
							mapSearchAdv.control_map();
							search_by_typeAdv='MAP_OFF';
						 }
					 }
				},{
					tooltip: 'Click to Xray Report',
					iconCls:'icon',
					iconAlign: 'top',
					width: 40,
					icon: 'http://www.reifax.com/img/toolbar/exray.png',
					scale: 'medium',
					handler: function(){
						var bd = document.getElementById('occounty').value;
						var latlong = document.getElementById('mapa_search_latlongAdv').value;
						var xcode = document.getElementById('ocproptype').value;
						if(Ext.isEmpty(xcode) || xcode=='*'){ 
							Ext.Msg.alert("XRay Report", 'Property Type is requiered to execute the XRay Report.');
							return false;
						}
						if(latlong=='-1'){
							Ext.Msg.alert("XRay Report", 'Map shape/polygon is requiered to execute the XRay Report.');
							return false;
						}
															 
						if(document.getElementById('reportsTab')){
							var tab = tabs.getItem('reportsTab');
							tabs.remove(tab);
						}
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
						tabs.add({
							title: ' Reports ',
							id: 'reportsTab',
							autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
							closable: true,
							tbar: new Ext.Toolbar({
								cls: 'no-border',
								width: 'auto',
								items: [' ',{
									tooltip: 'Click to Print XRay Report.',
									iconCls:'icon',
									iconAlign: 'top',
									width: 40,
									icon: 'http://www.reifax.com/img/toolbar/printer.png',
									scale: 'medium',
									handler: function(){
										Ext.Ajax.request( 
										{  
											waitMsg: 'Printing...',
											url: 'imprimir/properties_xray_print.php', 
											method: 'POST',
											timeout :600000,
											params: { 
												db: bd,
												proper: xcode,
												type: 'P',
												latlong: latlong
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												
												var results=response.responseText;
												if(Ext.isIE)
													window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
												else
													window.open(results,'_newtab');
											}                                
										});
									}
								},{
									 tooltip: 'Click to Save XRay Report.',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/toolbar/save.png',
									 scale: 'medium',
									 handler: function(){
										 var simple = new Ext.FormPanel({
											url: 'imprimir/properties_xray_print.php',
											frame:true,
											title: 'Saved Documents.',
											width: 400,
											waitMsgTarget : 'Saving Documents...',
												
											items: [{
												xtype     : 'textfield',
												name      : 'name_save',
												fieldLabel: 'Name',
												value     : '',
												width: 200
											},{
												xtype     : 'hidden',
												name      : 'db',
												value     : bd
											},{
												xtype     : 'hidden',
												name      : 'proper',
												value     : xcode
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'S'
											},{
												xtype     : 'hidden',
												name      : 'latlong',
												value     : latlong
											}],
												
											buttons: [{
												text: 'Save',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Saved Documents", 'Your document has been save.');
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", action.result.msg);
														}
													});
												}
											},{
												text: 'Cancel',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
													 
										var win = new Ext.Window({
											layout      : 'fit',
											width       : 400,
											height      : 170,
											modal	 	: true,
											plain       : true,
											items		: simple,
											closeAction : 'hide',
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
									 }
								}]
							})
						}).show();
				 	}
				},{
					 tooltip: 'Click to Rebate Report',
					 iconCls:'icon',
					 iconAlign: 'top',
					 width: 40,
					 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
					 scale: 'medium',
					 handler: function(){
						 var bd = document.getElementById('occounty').value;
						 var latlong = document.getElementById('mapa_search_latlongAdv').value;
						 var xcode = document.getElementById('ocproptype').value;
						 if(Ext.isEmpty(xcode) || xcode=='*'){ 
							Ext.Msg.alert("Rebate Report", 'Property Type is requiered to execute the Rebate Report.');
							return false;
						 }
						 if(latlong=='-1'){
							 Ext.Msg.alert("Rebate Report", 'Map shape/polygon is requiered to execute the Rebate Report.');
							 return false;
						 }
														 
						 if(document.getElementById('reportsTab')){
							 var tab = tabs.getItem('reportsTab');
							 tabs.remove(tab);
						 }
						 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
						 
						tabs.add({
							title: ' Reports ',
							id: 'reportsTab',
							autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
							closable: true,
							tbar: new Ext.Toolbar({
								cls: 'no-border',
								width: 'auto',
								items: [' ',{
									 tooltip: 'Click to Print Rebate Report.',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/toolbar/printer.png',
									 scale: 'medium',
									 handler: function(){
										Ext.Ajax.request( 
										{  
											waitMsg: 'Printing...',
											url: 'imprimir/properties_rebate_print.php', 
											method: 'POST',
											timeout :600000,
											params: { 
												db: bd,
												proper: xcode,
												type: 'P',
												latlong: latlong
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												
												var results=response.responseText;
												if(Ext.isIE)
													window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
												 else
													window.open(results,'_newtab');
											}                                
										});
									 }
								},{
									 tooltip: 'Click to Save Rebate Report.',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/toolbar/save.png',
									 scale: 'medium',
									 handler: function(){
										 var simple = new Ext.FormPanel({
											url: 'imprimir/properties_rebate_print.php',
											frame:true,
											title: 'Saved Documents.',
											width: 400,
											waitMsgTarget : 'Saving Documents...',
											
											items: [{
												xtype     : 'textfield',
												name      : 'name_save',
												fieldLabel: 'Name',
												value     : '',
												width: 200
											},{
												xtype     : 'hidden',
												name      : 'db',
												value     : bd
											},{
												xtype     : 'hidden',
												name      : 'proper',
												value     : xcode
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'S'
											},{
												xtype     : 'hidden',
												name      : 'latlong',
												value     : latlong
											}],
												
											buttons: [{
												text: 'Save',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Saved Documents", 'Your document has been save.');
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", action.result.msg);
														}
													});
												}
											},{
												text: 'Cancel',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
												 
										var win = new Ext.Window({
											layout      : 'fit',
											width       : 400,
											height      : 170,
											modal	 	: true,
											plain       : true,
											items		: simple,
											closeAction : 'hide',
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
									 }
								}]
							})
						}).show();
					 }
				},{
					tooltip: 'Click to Manage Saved Parameters',
					iconCls:'icon',
					iconAlign: 'top',
					width: 40,
					icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
					scale: 'medium',
					handler: function(){
						//alert(user_loged+"||"+user_block+"||"+user_web);
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
							 ShowSavedSearchParameter();
					}
				},
				'->'
				,{
					iconCls:'icon',
					//iconAlign: 'top',
					//width: 40,
					icon: 'http://www.reifax.com/img/toolbar/reset.png',
					scale: 'medium',
					text: 'Reset&nbsp;&nbsp; ',
					handler  : function(){
						if(mapSearchAdv!=null)
						{
							mapSearchAdv.control_map();
							search_by_typeAdv='MAP_OFF';
						}
						
						formulsearchadv.getForm().reset();
						formulsearchadv.getForm().submit({
							method: 'POST',
							params: { searchType:'advance'},
							waitTitle: 'Please wait..',
							waitMsg: 'Sending data...',
							success: function() {
								//Ext.Msg.alert("Success", "aqui");
								systemsearch='advance';
							},
							failure: function(form, action) {
								obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.errors.reason);
							}
						});
					}
				},
				' '
				,{
					iconCls:'icon',
					//iconAlign: 'top',
					//width: 40,
					icon: 'http://www.reifax.com/img/toolbar/search.png',
					scale: 'medium',
					text: 'Search&nbsp;&nbsp; ',
					handler  : function(){
						formulsearchadv.getForm().submit({
							method: 'POST',
							params: { searchType:'advance'},
							waitTitle: 'Please wait..',
							waitMsg: 'Sending data...',
							success: function() {
								//Ext.Msg.alert("Success", "aqui");
								systemsearch='advance';
								createResult(user_web,realtor_block);
							},
							failure: function(form, action) {
								obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.errors.reason);
							}
						});
					}				
				}]
			})			
		}
		<?php }?>
	],
	listeners: {
		'tabchange': function(tabpanel,tab){
			if(tab){
				if(tab.id!='searchTabBasic'){
					if(document.getElementById('control_mapa_div'))
						document.getElementById('control_mapa_div').style.display='none';
					if(document.getElementById('control_mapa_divAdv'))
						document.getElementById('control_mapa_divAdv').style.display='';
				}else{
					if(document.getElementById('control_mapa_div'))
						document.getElementById('control_mapa_div').style.display='';
					if(document.getElementById('control_mapa_divAdv'))
						document.getElementById('control_mapa_divAdv').style.display='none';
				}
			}
		}
	}
});
</script>

