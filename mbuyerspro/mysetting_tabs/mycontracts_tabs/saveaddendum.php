<?php
/**
 * saveaddendum.php
 *
 * Save the addendums of a document.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 14.04.2011
 */

include "../../properties_conexion.php"; 
conectar();

// Allowed images
$error   = "";
$userid  = $_COOKIE['datos_usr']["USERID"];

// Delete addons and end execution if the command its recieve
if (strlen($_GET['id']) and $_GET['cmd'] == 'del') {
	
	$id = addslashes($_GET['id']);
	
	$query = "DELETE FROM xima.contracts_addendum
				WHERE userid = $userid AND id = $id";
	@mysql_query($query);	
	
	echo "Addendum deleted";
	die();
	
} else {

	// Check the form fields
	$id      = addslashes($_POST['id']);
	$name    = addslashes($_POST['name']);
	$content = addslashes($_POST['contentt']);

	if (strlen($_POST['id']) and $_POST['cmd'] == 'mod') {
	
		$query = "UPDATE xima.contracts_addendum SET
					name='$name', content='$content'
					WHERE userid=$userid AND id=$id";
	
		if (mysql_query($query))
			$resp = array('success'=>'true','mensaje'=>'Record saved!');
		else
			$resp = array('success'=>'true','mensaje'=>mysql_error());
		
	} else {
		
		$query = "INSERT INTO xima.contracts_addendum (userid, name, content) 
				  VALUES ($userid,'$name','$content')";
	
		if (mysql_query($query))
			$resp = array('success'=>'true','mensaje'=>'Record saved!');
		else
			$resp = array('success'=>'true','mensaje'=>mysql_error());

}

	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);

}
?>	