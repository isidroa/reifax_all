<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	conectar();	
	
	//Elimina duplicados
	$query='SELECT *, count(*) cant
	FROM xima.follow_assignment 
	group by parcelid,userid,agentid
	having count(*)>1
	ORDER BY userid';
	$result = mysql_query($query) or die($query.mysql_error());
	
	$i=0;
	while($r = mysql_fetch_assoc($result)){
		$query = 'DELETE FROM xima.follow_assignment WHERE userid='.$r['userid'].' AND parcelid="'.$r['parcelid'].'" LIMIT '.(intval($r['cant'])-1);
		
		
		if($i % 100 == 0) echo $i.'.- '.$query.'<br>';
		
		mysql_query($query) or die($query.mysql_error());
		$i++;
	}
	
	//Coloca un solo principal cuando hay varios y elimina agentes null cuando ya hay agentes a las propiedades.
	$query='SELECT *
	FROM xima.follow_assignment 
	group by userid,parcelid,principal
	having count(*)>1 and principal=1
	ORDER BY userid,parcelid,agentid';
	$result = mysql_query($query) or die($query.mysql_error());
	
	$i=0;
	while($r = mysql_fetch_assoc($result)){
		$query = 'DELETE FROM xima.follow_assignment WHERE userid='.$r['userid'].' AND parcelid="'.$r['parcelid'].'" AND agentid is null';
		mysql_query($query) or die($query.mysql_error());
		
		$query = 'UPDATE xima.follow_assignment SET principal=0 WHERE userid='.$r['userid'].' AND parcelid="'.$r['parcelid'].'"';
		mysql_query($query) or die($query.mysql_error());
		
		$query = 'UPDATE xima.follow_assignment SET principal=1 WHERE userid='.$r['userid'].' AND parcelid="'.$r['parcelid'].'" 
		order by idassigment DESC limit 1';
		mysql_query($query) or die($query.mysql_error());
		
		if($i % 100 == 0) echo $i.'.- '.$query.'<br>';
		$i++;
	}
?>