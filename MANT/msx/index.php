﻿<?php 	
	class Fecha {
	  var $fecha;
	  function Fecha($anio=0, $mes=0, $dia=0) 
	  {
		   if($anio==0) $anio = Date("Y");
		   if($mes==0) $mes = Date("m");
		   if($dia==0) $dia = Date("d");
		   $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
	  }
	  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
	  {
		   $array_date = explode("-", $this->fecha);
		   $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
	  }
	 
	  function getFecha() { return $this->fecha; }
	}

	// funcion que devuelve un array con los elementos de una hora
	function parteHora( $hora )
    {    
    	$horaSplit = explode(":", $hora);
		if( count($horaSplit) < 3 )
        {
        	$horaSplit[2] = 0;
		}
		return $horaSplit;
	}

    // funcion que devuelve la resta de dos horas en formato string
    function RestaHoras( $time1, $time2 )
    {
        list($hour1, $min1, $sec1) = parteHora($time1);
        list($hour2, $min2, $sec2) = parteHora($time2);
        return date('H:i:s', mktime( $hour1 - $hour2, $min1 - $min2, $sec1 - $sec2));
    }  	

	function tiempo()
	{
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		return $mtime;
	}
	function ordenarPor($vector,$field,$orden)
	{
		foreach($vector as $i=>$valor)
			$ordenar[$i]=doubleval($vector[$i][$field]);
		if($orden=='asc') $orden=SORT_ASC;
		else $orden=SORT_DESC;
		array_multisort($ordenar,$orden,SORT_NUMERIC,$vector);
		return($vector);
	}

	function curl_get_file_contents($URL)
	{
		$c = curl_init();
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $URL);
		$contents = curl_exec($c);
		curl_close($c);
	
		if ($contents) return $contents;
		else return FALSE;
	}	

$TIMETOTALINI=tiempo();	
$TIMETOTALINIHORA=date("d/m/Y H:i:s");	
$TIMEINIHORA=date("H:i:s");	

//////********************************************************************
if(!isset($_POST['numpages']))$_POST['numpages']=20;
$CANTIDADLOTES=30;///CANTIDAD DE LOTES DE $nnum1 (4500) REGISTROS CADA LOTE
//////********************************************************************
$validarSoloNum="if ((event.keyCode < 48 || event.keyCode > 57)) event.returnValue = false;";

include("../administrador/includes/php/conexion.php");
$conex=conectar('xima');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SPIDER IDX</title>
</head>
<body>
<form id="form1" name="form1" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
<h2>SPIDER IDX</h2>
BD: <select name="cbd" id="cbd" >
	<option value="*" selected>-- Select --</option>
    <?php
    	$q='select DISTINCT state,county,bd from xima.lscounty order by county';
        $res=mysql_query($q) or die($q.mysql_error());
        while($r=mysql_fetch_array($res))
		{
			$sel='';
			if($_POST['cbd']==strtolower($r['bd']))$sel=' selected ';
        	echo '<option value="'.strtolower($r['bd']).'" '.$sel.'>'.strtoupper($r['bd']).'</option>';
        }
	?>
    </select>
&nbsp;&nbsp;    
Status: <select name="cbs" id="cbs" >
<?php 			
		$sela='';if($_POST['cbs']=='A')$sela=' selected ';
		$sels='';if($_POST['cbs']=='S')$sels=' selected ';
		$sels='';if($_POST['cbs']=='NANCS')$sels=' selected ';
?>
	<option value="*" selected>-- Select --</option>
	<option value="ALL" >ALL</option>
    <option value="A" <?php echo $sela ?>>A</option>
	<option value="S" <?php echo $sels ?>>CS</option>
	<option value="NANCS" <?php echo $sels ?>>NANCS</option>
    <option value="NOTA" <?php echo $sela ?>>CS - NACS ( NOT A)</option>
    </select>
&nbsp;&nbsp; Pages View: <input type="text" name="numpages" id="numpages" size="5" value="40" onKeypress="<?php  echo $validarSoloNum?>">  
&nbsp;&nbsp; LastUpdateDate &ge; <input  style="text-align:center" type="text" size="12" maxlength="10"  id="txtif" name="txtif" 
value="
<?php 
	//$fecha = new Fecha(date('Y'),date('m'),date('d'));
	//$fecha->SumarFecha(0, 0, -15);// $fecha->SumarFecha(año, mes, dia) 
	//echo $fecha->getFecha();
	echo date('Y-m-d');
?>
"	onKeypress="return validarNumeros(this, event,'fecha')">yyyy-mm-dd

&nbsp;&nbsp; <input type="submit" name="button" id="button" value="Download Data" />
<?php 
if(isset($_POST['cbd']) && $_POST['cbd']<>'*' && isset($_POST['cbs']) && $_POST['cbs']<>'*' && isset($_POST['numpages']) && $_POST['numpages']<>'*')
{
	if(isset($_POST['txtif']) && strlen(($_POST['txtif'])>0))
	{
		$timestamp=strtotime($_POST['txtif']);
		if($timestamp=='' || strlen($timestamp)==0)
			die("INCORRECT DATE");
		$fechab=date('d-M-y',$timestamp);
	}

	$DB=$_POST['cbd'];// Base de datos
	$status=$_POST['cbs'];//;$arrstatus[$icit]// A o S
	if($status=='ALL')
		$arrstatus=array('A','CS','NANCS');
	elseif($status=='NOTA')
		$arrstatus=array('CS','NANCS');
	else
		$arrstatus=array($status);
	
//print_r($arrstatus);return;
		
	$query="SELECT c.citycode,c.city FROM xima.lscity c
			INNER JOIN xima.lscounty y ON (c.idcounty=y.idcounty)
			WHERE y.bd='".$DB."' and length(c.citycode)>0";
	$res=mysql_query($query) or die($query.mysql_error());
	$ic=0;
	$citycode='';
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{
		if($ic>0)$citycode.=",";
		$citycode.="'".$row['citycode']."'";
		$ic++;
	}//while de ciudades..
	
	
 	$query="TRUNCATE TABLE ".$DB.".msxdownload";	mysql_query($query) or die($query.mysql_error());
 	$query="TRUNCATE TABLE ".$DB.".imgmsxdownload";	mysql_query($query) or die($query.mysql_error());

	$primeravez=0;
	if($cantmsx==0  && strlen($rowT['mlslistingid'])==0 && $cantimgmsx==0 && strlen($rowT2['mlslistingid'])==0) $primeravez=1;
	$acumcondado=0;
	$CUENTA=0;
	$codefile=date('Ymd_His');
	for($icit=0;$icit<count($arrstatus);$icit++)//FOR DE STATUS
	{
		$status=$arrstatus[$icit];// 'A','CS','NANCS'
		echo "<br />".$DB." ".$status." ".$fechab." |||".$cantmsx."|".$cantimgmsx."|".$primeravez;
		
	//INICIALIZANDO LAS VARIABLES
		if($status=='A')//NI A NI CS
		{
			//$contreg=2414;
			$status2='A';
			$urlstatus='AND%20L.STATUS_CODE=%27A%27%20';		
		}
		if($status=='CS')//NI A NI CS
		{
		//$contreg=86;
			$status2='S';
			$urlstatus='AND%20L.STATUS_CODE=%27S%27%20';		
		}
		if($status=='NANCS')//NI A NI CS
		{
			//$contreg=246;
			$status2='A';
			$urlstatus='AND%20L.STATUS_CODE%3C%3E%27A%27%20AND%20L.STATUS_CODE%3C%3E%27S%27%20';		
		}
		
//BUSCANDO CUANNTOS REGISTROS HAY POR STATUS
		
		$url='http://webapps2.planetrealtor.com/idx/pkg_IDX.pr_ListOfIDXProperties?';
		$url.='sStateID=FL';
		$url.='&sRealtorID=';
		$url.='&sUserType=F';
		$url.='&sTrackNum=';
		$url.='&sSearchType=';
		$url.='&sPropertyTypeCode='.$status2;
		$url.='&sSQLPreBuiltWhere=';
		if(strlen($fechab)>0)
			$url.='%20L.LAST_UPDATE_DATE%20%3E=%20%27'.$fechab.'%27%20AND%20';
		$url.='L.PROPERTY_STATE_ID=%27FL%27%20';
		$url.=$urlstatus;
		$url.='AND%20L.NODISPLAY_IDX%20IS%20NULL%20';
		$url.='AND%20L.NODISPLAY_OWNER%20IS%20NULL%20';
		$url.='AND%20L.IDX_ENABLED%20IS%20NOT%20NULL%20';
		$url.='AND%20L.CITY_ID%20IN%20('.$citycode.')%20';
		$url.='AND%20L.SALE_PRICE%20%3E=%200%20';
		$url.='AND%20L.SALE_PRICE%20%3C=%2099999999999';
		$url.='&sSQLPreBuiltOrderBy=';
		$url.='&sDisplayPhoto=F';
		$url.='&sSearchSource=X';
		$url.='&sOnlyCities=F';
		$url.='&sRunSearch=F';
		$url.='&sSearchID=';
		$url.='&sClientID=';
		$url.='&sSavedSearchType=';
		$url.='&sLangCode=ENGLISH';
		$url.='&sSubSystemCode=';
		$url.='&nPgNum=1';
		$url.='&nNum=1';
		//echo '<br/>'.$url.'<br/>';//return;

		$source = curl_get_file_contents($url);
			
		if(!$source || $source ==false )//hacer el ciclo de verificar que se dañe 
		{	echo '<br/>ENTRA EN EL SINO';
			do
			{
				echo '<br/>ENTRA EN EL DO WHILE';
				$source = curl_get_file_contents($url);//or die('se ha producido un error');
			}while(!$source || $source ==false );
		}
		$contreg=0;
		$pos=strpos($source,'CLASS=TDBasicCounterText');
		if ($pos !== false)//si hay registros para mostrar 
		{
			$aux=substr($source,$pos+10);
			$pini=strpos($aux,'>')+1;
			$pfin=strpos($aux,'TD>')-4;
			$subaux=substr($aux,$pini,($pfin-$pini)-5);
			$num=explode(' ',$subaux);
			$contreg=$num[0];
		}	

		//echo '<a href="'.$url.'" target="_blank">'.$status." | ".$contreg.'</a>';

//CONSTRUYENDO LOS LINKS Y PAGINANDO EN LOSTES DE 600 REGISTROS POR LINKS..

		$nnum1=600;//600cantidad de registros por lote
		$npg1=$nnum1/15;//600;//cntidad de lotes de 15 registros para llegar a 4500
	
		$codigocorrida=$status2.date('YmdHis');
		$fechacorrida=date('Y-m-d');
		
		$lotes=ceil($contreg/$nnum1);
		for($ITOTAL=1;$ITOTAL<=$lotes;$ITOTAL++)
		{
			$CUENTALINKS++;
			$npg=$npg1+($npg1*($ITOTAL-1));
			$nnum=1+($nnum1*($ITOTAL-1));//[ ".(1+(($ITOTAL-1)*$nnum1)).",".($ITOTAL*$nnum1)." ]
			$text="LINKS: ".$CUENTALINKS.".- Status: ".$status." , contreg: ".$contreg." , npg: ".$npg.", nnum: ".$nnum;
			$link='gethtmlpage.php?npg='.$npg.'&nnum='.$nnum.'&status='.$status.'&status2='.$status2.'&bd='.$DB.'&citycode='.str_replace("'",'',$citycode).'&codefile='.$codefile.
			'&fecha='.$fechab.'&primeravez='.$primeravez.'&codefile='.$codefile.'&totalregistros='.$contreg.'&page='.$CUENTALINKS;
			
			echo '<br/><a href="'.$link.'" target="_blank">'.$text.'</a>';
			if($CUENTALINKS>1)
			{
				$cadlinks.=",";
				$cadparams.=",";
			}
			$cadlinks.="'".$link."'";
			
			$cadparams.="{'npg':'".$npg."','nnum':'".$nnum."','status':'".$status."','status2':'".$status2."','bd':'".$DB."','citycode':'".str_replace("'","",$citycode)."',
						 'codefile':'".$codefile."','fecha':'".$fechab."','primeravez':'".$primeravez."'}";
			
			//include('gethtmlpage.php');
			
		}//FOR DE TODAS LAS PAGINAS 1 A BUSCAR 
	}//for de los status

	echo "<script>var arrlinks=[".$cadlinks."];</script>";
?>
<br /><input name="but" type="button" onclick="abrirlotes()" value="Run All Links" />
<br /><input type="text" name="mostrar" id="mostrar" size="30" readonly="readonly" style="border:none">
<br /><input type="hidden" name="mostrarcant" id="mostrarcant" size="30" readonly="readonly" style="border:none">
<br />
<?php
    //**************************************************************************************************************************************************************************************************************
	$TIMETOTALFIN=tiempo();
	$TIMETOTALSEG=$TIMETOTALFIN-$TIMETOTALINI;
	
			//echo "<br><br><strong>TOTAL REGISTROS: ".$CUENTA."</strong><br>";
	//echo "<br><br><strong>TIEMPO INICIO TOTAL: ".$TIMETOTALINIHORA."</strong><br>";
	//echo "<br><strong>TIEMPO FINAL TOTAL: ".date("d/m/Y H:i:s")."</strong><br>";
	//echo "<br><strong>TIEMPO TOTAL DE EJECUCION DE TODOS LOS PHP's: ".RestaHoras(date("H:i:s"),$TIMEINIHORA)." | SEGUNDOS:".$TIMETOTALSEG."<strong>";
}//if de los post
?>
<br /><br /><br />
<fieldset style="width:800px">
    <legend>Generate CSV</legend>
    <label>MSX DATA: </label>
    <label><input type="checkbox" name="chmsx" id="chmsxnone" onchange="limpiarACS()"/>None</label>
    <label><input type="checkbox" name="chmsx" id="chmsxa" onchange="limpiarNONE()"  checked="checked" />MSX A</label>
    <label><input type="checkbox" name="chmsx" id="chmsxcs" onchange="limpiarNONE()" checked="checked"/>MSX CS</label>
    <label><input type="checkbox" name="chmsx" id="chmsxnancs" onchange="limpiarNONE()" checked="checked"/>MSX NANCS</label>
    <br /><label>MSX IMAGES: </label>
    <label> <input type="radio" name="rbimgx" value="radio" id="chimgy" checked="checked" />Yes</label>
    <label> <input type="radio" name="rbimgx" value="radio" id="chimgn" />No</label>
    <br /><input name="but" type="button" onclick="generarCsv()" value="Generate CSV" />
    <br /><textarea name="mostrarcsv" cols="100" rows="2" readonly="readonly" id="mostrarcsv" style="border:none"></textarea>
</fieldset>

</form>
</body>
</html>
<script>
var LOTE=<?php echo $_POST['numpages']; ?>;
var IVENTANAS=-1;
var REGCANT=0;

//Creacion del ajax
function nuevoAjax()
{ 
	var xmlhttp=false; 
	try 
	{ xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 	}
	catch(e)
	{ 	try	{ xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); } 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
	return xmlhttp; 
}

function abrirlotes()
{
	document.getElementById("mostrarcsv").value="";
	document.getElementById("mostrar").value="";
	document.getElementById("mostrarcant").value="Registros Procesados: "+REGCANT;
	for(i=0;i<LOTE && i<arrlinks.length;i++)
	{
		IVENTANAS++;
		window.open(arrlinks[i],"", "width=385,height=180,status,toolbar=1,scrollbars,location,resize");
	}
}  

function nuevaventana()
{
	IVENTANAS++;
	if(IVENTANAS<=(arrlinks.length-1))
		window.open(arrlinks[IVENTANAS],"", "width=385,height=180,status,toolbar=1,scrollbars,location,resize");
}

function contarlinks()
{
	if(IVENTANAS<=(arrlinks.length-1))
	{
		//REGCANT+=cant;
		document.getElementById("mostrar").value="Links Procesados: "+(IVENTANAS+1)+" de "+arrlinks.length;
//		document.getElementById("mostrarcant").value="Registros Procesados: "+REGCANT;
	}
}

function generarCsv()
{
	var db=document.getElementById("cbd").options[document.getElementById("cbd").selectedIndex].value;
	var none=document.getElementById("chmsxnone").checked;
	var a=document.getElementById("chmsxa").checked;
	var cs=document.getElementById("chmsxcs").checked;
	var nancs=document.getElementById("chmsxnancs").checked;
	var imgy=document.getElementById("chimgy").checked;
	var imgn=document.getElementById("chimgn").checked;
	document.getElementById("mostrarcsv").value="";
	
	if(db.length==0 || db=='*'){alert("Select county/database");return;}
	if(	none==true && imgn==true){alert("Select file to generate");return;}

	var parametros="db="+db+"&none="+none+"&a="+a+"&cs="+cs+"&nancs="+nancs+"&imgy="+imgy+"&imgn="+imgn;
//	alert(parametros);return;
	document.getElementById("mostrarcsv").value="Generating CSV....";
	var ajax=nuevoAjax();
	ajax.open("POST", "generarcsv.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			document.getElementById("mostrarcsv").value=results;
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

	
}

function limpiarACS()
{	document.getElementById("chmsxa").checked=document.getElementById("chmsxcs").checked=document.getElementById("chmsxnancs").checked=false;marcarNONE();}

function limpiarNONE()
{	document.getElementById("chmsxnone").checked=false;marcarNONE();}

function marcarNONE()
{	
	if(	document.getElementById("chmsxnone").checked==false && 
		document.getElementById("chmsxa").checked==false && 
		document.getElementById("chmsxcs").checked==false && 
		document.getElementById("chmsxnancs").checked==false)
		document.getElementById("chmsxnone").checked=true;
}

//onKeypress="return validarNumeros(this, event,'number')"
function validarNumeros(myfield, e,t)
{
	var key,keychar;
	var arrke;
	if (window.event) key = window.event.keyCode;
	else if (e)	key = e.which;
	else return true;
//alert(key+' | '+t);	
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) 	return true;

	// si es fecha y es el guion
	if (t=='fecha' && key==45) 	return true;

	// si es decimal y es el punto
	if (t=='decimal' && key==46) 	
	{
		arrke=myfield.value.split('.') 
		if(arrke.length>=2)return false;
		return true;
	}
	// si es hora y es el dos punto
	if (t=='hora' && key==58) 	
	{
		arrke=myfield.value.split(':') 
		if(arrke.length>=3)return false;
		return true;
	}

	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))  return true;
	else return false;
}
</script>


