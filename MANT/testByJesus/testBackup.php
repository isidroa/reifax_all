<?php
// Example. Zip all .html files in the current directory and save to current directory.
// Make a copy, also to the current dir, for good measure.
$fileDir	=	$_SERVER["DOCUMENT_ROOT"].'/MailAttach/';
$excludes	=	Array(
					
				);
function recurseDir($dir) {
	GLOBAL $zip;GLOBAL $excludes;
	if(is_dir($dir)) {
		if($dh = opendir($dir)){
			while($file = readdir($dh)){
				if($file != '.' && $file != '..'){
					if(!in_array(str_replace($_SERVER["DOCUMENT_ROOT"],"",$dir.$file),$excludes,true)){
						if(is_dir($dir . $file)){
							echo $dir . $file;
							// since it is a directory we recurse it.
							recurseDir($dir . $file . '/');
						}else{
							$zip->addFile(file_get_contents($dir . $file), $dir.$file, filectime($dir . $file));
							//echo "<br>".$dir . $file;   
				 		}
					}
				}
	 		}
		}
 		closedir($dh);         
	}
}
include_once("zipClass/zipClass.php");
$fileTime = date("D, d M Y H:i:s T");

$zip = new Zip();
$zip->setZipFile("ZipExample.zip");

$zip->setComment("Backup $fileDir Zip file.\nCreated on " . date('l jS \of F Y h:i:s A'));
$zip->addFile("By Jesus! XD", "Readme.txt");
$c=0;

recurseDir($fileDir);

$zip->finalize(); // as we are not using getZipData or getZipFile, we need to call finalize ourselves.
//$zip->setZipFile("ZipExample2.zip");
?>
<html>
<head>
<title>MailAttachMent Server Xima</title>
</head>
<body>
<h1>Zip Test</h1>
<p>Zip files saved, length is <?php echo strlen($zip->getZipData()); ?> bytes.</p>
<p>total <?php echo $c;?></p>
</body>
</html>