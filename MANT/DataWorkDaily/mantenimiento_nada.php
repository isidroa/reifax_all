<html>
<head>
<title>Mantenimiento de Tablas Diario</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO DE TABLAS DIARIO</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
			<th width="190" >BD: 
				<select name="cbd">
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
					<option value="FLBROWARD" >FLBROWARD</option>
					<option value="FLDADE"    >FLDADE</option>
					<option value="FLPALMBEACH">FLPALMBEACH</option>
				</select>
               
			</th>
		</tr>

<!-- 1) Tabla PENDES-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titTransSinc','imgTransSinc'); return false;">
    	<img id="imgTransSinc" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2" >PENDES</th>
	<th width="190" ><input type="checkbox" name="ckbTranfSinc"   id="ejecTranfSinc" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSinc, 0)">Ejecutar</th>
</tr>
  <tr  id="titTransSinc" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">

		<!-- 1.1) Pasar los CSV de access1 a la Tabla Pendes-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincBackup','imgTransSincBackup'); return false;">
				<img id="imgTransSincBackup" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Pasar los CSV de access1 a la Tabla Pendes</th>
			<th width="180"></th>
		</tr>
   	    <tr  id="titTransSincBackup" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincBackup=array(
										
							);
				for($i=0;$i<count($titTransSincBackup);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincBackup[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc"  id="<?php echo $titTransSincBackup[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!-- 1.2) Ejecutar los php de Sincronización de Data para Pendes-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincAcc1Msq','imgTransSincAcc1Msq'); return false;">
				<img id="imgTransSincAcc1Msq" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Ejecutar los php de Sincronización de Data para Pendes</th>
		</tr>
   	    <tr  id="titTransSincAcc1Msq" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincAcc1Msq=array(
										
							);
				for($i=0;$i<count($titTransSincAcc1Msq);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincAcc1Msq[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincAcc1Msq[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--  1.3) Pasar los CSV de access1 a la Tabla Pendes)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Pasar los CSV de access1 a la Tabla Pendes</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincSincroniza=array(
											 
							);
				for($i=0;$i<count($titTransSincSincroniza);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincSincroniza[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincSincroniza[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	
  
  <!--  1.4) Creando CSV a transferir de Data Mantenimiento->Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Creando CSV a transferir de Data Mantenimiento->Sistema</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincSincroniza=array(
											 
							);
				for($i=0;$i<count($titTransSincSincroniza);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincSincroniza[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincSincroniza[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	
  
    <!--  1.5) Hacer Backup de las Tablas de Data del Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Hacer Backup de las Tablas de Data del Sistema</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincSincroniza=array(
											 
							);
				for($i=0;$i<count($titTransSincSincroniza);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincSincroniza[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincSincroniza[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	
  
  <!--  1.5) Restore de los CSV de Data Mantenimiento->Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Restore de los CSV de Data Mantenimiento->Sistema</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincSincroniza=array(
											 
							);
				for($i=0;$i<count($titTransSincSincroniza);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincSincroniza[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincSincroniza[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	</table>	
  </td>
  </tr>
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->



<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
