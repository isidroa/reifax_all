function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function checkSeleccion(field, i) 
{
	if (i == 0) //Todos
	{ 
		if (field[0].checked == true) 
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = true;
	   	}
		else
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = false;
		}
	}
	else  // Seleccion de cualquiera
	{  
		if(field[0].id!="nada")
			field[0].checked = false;

		if(field[0].id=="nada")
			field[0].checked = true;
	}
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var field,i;
	field =document.formulario.ckbPendesBackupTablas;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesCSVtoPendes;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesSincPendes;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesCalculos;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesTransMantoSis;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesCSVTransDM;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesValData;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesBackupDS;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbPendesResCSVDM;
	for (i=0; i<field.length;i++)
		field[i].checked=false;

}

function buscarParametros()
{
	var field,i,icombo;
	var ret="",datnew="";
	var ruta,texto;
	var cont=0;
	var swbd=0;
	var cad;
	
	for(icombo=0;icombo<9;icombo++)
	{
		//alert("icombo "+icombo);
		if(icombo==0)
			field=document.formulario.ckbPendesBackupTablas;
		if(icombo==1)
			field=document.formulario.ckbPendesCSVtoPendes;
		if(icombo==2)
			field=document.formulario.ckbPendesSincPendes;
		if(icombo==3)
			field=document.formulario.ckbPendesCalculos;
		if(icombo==4)
			field=document.formulario.ckbPendesTransMantoSis;
		if(icombo==5)
			field=document.formulario.ckbPendesCSVTransDM;
		if(icombo==6)
			field=document.formulario.ckbPendesValData;
		if(icombo==7)
			field=document.formulario.ckbPendesBackupDS;
		if(icombo==8)
			field=document.formulario.ckbPendesResCSVDM;
		
		//alert("icombo: "+icombo+" field: "+field[0].id+" field.lenght: "+field.length);
		
		for (i=0; i<field.length;i++)
		{
			//alert("i= "+i);
			cad=field[i].id.substr(0,6);
			//alert("case: "+cad);
			switch(cad)
			{
				
				case "Backup":
					ruta="backup/Dobackup/";
					if( icombo==0 && (i==1 || i==2 ||i==3) )
						texto="Hacer Backup de las Tablas";
					if( icombo==5 && (i==1 || i==2 ||i==3) )
						texto="Creando CSV a transferir de Data Mantenimiento->Sistema";
					//if(i==14 || i==15 ||i==16)
					if( icombo==7 && (i==1 || i==2 ||i==3) )
						texto="Hacer Backup de las Tablas de Data del Sistema (FL)";
					break;
				case "Transf":
					{
					if(icombo==1)
					{
					ruta="access to csv/";
					texto="Pasar los CSV de access1 a las Tablas";
					}
					else
					{
					ruta="transferMantToSis/";
					texto="Transferir Data de Tabla Mant-->Sis";
					}
					break;
					}
				case "Restor":
					ruta="backup/Dorestore/";
					texto="Restore de los CSV de Data Mantenimiento-->Sistema";
					break;
				case "calcul":
					ruta="calculos/";
					texto="Calculos";
					break;	
					
				case "cantRe":
					ruta="cantidad/";
					texto="Validacion de data";
					break;	
					
				case "Cantid":
					ruta="cantidad/";
					texto="Validacion de data";
					break;	
				
				case "query.":
					ruta="cantidad/";
					texto="Validacion de data";
					break;
				
				case "Porcen":
					ruta="cantidad/";
					texto="Validacion de data";
					break;
					
				default:	
					ruta="dataverificacion/";
					texto="Ejecutar los php de Sincronizacion de Data";
			}

			if(field[i].checked==true && field[i].id!="*")
			{
				cont++;
				//alert("field[i].id: "+field[i].id);
				//if(i==1 || i==2 || i==3 || i==14 || i==15 || i==16)
				if( (icombo==0 || icombo==7 )  && (i==1 || i==2 || i==3 ))
					datnew="datanueva=csvfiles&copypaste=0";
				//if(i==11 || i==12 || i==13 || i==17 || i==18 || i==19)
				if(  (icombo==5 || icombo==8 )  &&  (i==1 || i==2 || i==3) )
					datnew="datanueva=dataserver1&copypaste=0";
				
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				//alert("i= "+i);
			}
			//alert("ruta: "+ruta+" texto: "+texto);
			//alert("ret: "+ret);
		}
		
	}

	//alert(" datnew= "+datnew+" ret= "+ret);
	if(ret=="") return "";
	//return "";
	else return datnew+"&parametros="+ret;
}
function ValidarBD(bd)
{
	var letra=bd.substr(0,1);
	var i;
//SI BD ES DE MANTENIMINENTO, NINGUNO DE 
//ESTOS PHP DEBE ESTAR SELECCIONADO

	if(letra.toUpperCase()=='M') //MANTENIMIENTO
	{
		
		field =document.formulario.ckbPendesBackupDS;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
			{
				//alert("i= "+i);
				return 0;
			}
		field =document.formulario.ckbPendesResCSVDM;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
			{
				//alert("i= "+i);
				return 0;
			}	
	}
	else //SISTEMA
	{

			field=document.formulario.ckbPendesBackupTablas;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	
			
			field=document.formulario.ckbPendesCSVtoPendes;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	
			
			field=document.formulario.ckbPendesSincPendes;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	
			
			field=document.formulario.ckbPendesCalculos;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	

			field=document.formulario.ckbPendesTransMantoSis;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	

			field=document.formulario.ckbPendesCSVTransDM;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	

			field=document.formulario.ckbPendesValData;
			for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*"){return 1;}	

	}
	return 2;
}

function ejecutarPHP()
{	
	var i;
	var bd=document.getElementById("cbd").value;
	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd=="")
		alert("Debes seleccionar Base de Datos");
	else
	{
		
		parametros=buscarParametros();
		if(parametros==null || parametros=="")
			alert("Debes seleccionar al menos una opcion a Ejecutar");
		else
		{
			ret=ValidarBD(bd);
			if(ret!=2)
			{
				if(ret==0)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Sistema.\n Debes seleccionar Base de Datos de Sistema");
				if(ret==1)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Mantenimiento.\n Debes seleccionar Base de Datos de Mantenimiento");
				return;
			}
			//expandir('filResultados','imgfilResultados');
			document.getElementById("divResultados").innerHTML='';
			document.getElementById("imgfilResultados").src="includes/up.png";
			document.getElementById("imgfilResultados").alt="Ocultar Resultados";
			document.getElementById("reloj").style.visibility="visible";
			if(document.getElementById("guardarAccess1").checked==true){
				var guardarAccess = document.getElementById("guardarAccess1").value;
				var fechaAccess = document.getElementById("fechaAccess1").value;
			}
			else
				var guardarAccess = document.getElementById("guardarAccess2").value;
			/*if(document.getElementById("guardarAccess1").checked==true)
				var guardarAccess = document.getElementById("guardarAccess1").value;
			else
				var guardarAccess = document.getElementById("guardarAccess2").value;*/
			//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=true;
			//habilitacion(true);
			//alert("bd="+ bd + "&" + parametros);

			var ajax=nuevoAjax();
			ajax.open("POST", "mantenimiento_respuesta.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("bd="+bd+"&fechaAccess="+fechaAccess+"&guardarAccess="+guardarAccess+"&"+parametros);
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					htmlTag.push(ajax.responseText);
					document.getElementById("divResultados").innerHTML=htmlTag.join('');
					document.getElementById("filResultados").style.display="inline";
					document.getElementById("reloj").style.visibility="hidden";
					//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=false;
					limpiarchecks();
					//habilitacion(false);
				}
			}
		}//Al Menos un ckechboxes seleccionado
	}//IF Base de Datos
}

function checkSeleccionCSV(field){
	if(field[0].checked==true){
		for(i=0; i<field.length; i++)
			field[i].checked = true;
	}else{
		for(i=0; i<field.length; i++)
			field[i].checked = false;
	}
}

function parametrosCSV(field){
	var parametros='';
	for(i=1; i<field.length; i++){
		if(field[i].checked == true)
			parametros+=field[i].value+'*';
	}
	
	alert(parametros);
}

function validarFecha(Cadena){
	
    var Fecha= new String(Cadena) 
	
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length)) 
  

    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
            alert('Fecha Incorrecta. A�o inv�lido')  
        return false  
    }  

    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        alert('Fecha Incorrecta. Mes inv�lido')  
        return false  
    }  

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        alert('Fecha Incorrecta. D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            alert('Fecha Incorrecta. D�a inv�lido')  
            return false  
        }  
    }  
 
  return true      
}