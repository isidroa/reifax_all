<html>
<head>
<title>Mantenimiento de Tablas Diario</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO DE TABLAS DIARIO</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
			<th width="190" >BD: 
				<select name="cbd" id='cbd'>
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
					<option value="MFLLAKE">MFLLAKE</option>
					<option value="MFLORANGE">MFLORANGE</option>
					<option value="FLBROWARD" >FLBROWARD</option>
					<option value="FLDADE"    >FLDADE</option>
					<option value="FLPALMBEACH">FLPALMBEACH</option>
					<option value="FLLAKE">FLLAKE</option>
					<option value="FLORANGE">FLORANGE</option>
				</select><br> SAVE: <input type="radio" name="guardarAccess" id="guardarAccess1" value=1>SI<input name="guardarAccess" type="radio" id="guardarAccess2" value=0 checked>NO
                <br>FECHA: <input name="fechaAccess" type="text" id="fechaAccess1" value="AAAA-MM-DD" size="12" maxlength="10" onFocus="javascript: this.select();" onBlur="javascript: if(document.getElementById('guardarAccess1').checked==true) validarFecha(this.value);">
			</th>
		</tr>

<!-- 1) Tabla PENDES-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('bluePendes','imgbluePendes'); return false;">
    	<img id="imgbluePendes" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="3" >PENDES</th>
	<!-- ********************************************************EJECUTAR*********************************************************-->	
</tr>
  <tr  id="bluePendes" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">



<!-- 0.1) Hacer BackUp de las tablas -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesBackupTablas','imgyellowPendesBackupTablas'); return false;">
				<img id="imgyellowPendesBackupTablas" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Hacer Backup de las Tablas</th> 
			<th width="180"><input type="checkbox" name="ckbPendesBackupTablas"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesBackupTablas, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesBackupTablas" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesBackupTablas=array(
										array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php"),
										array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
										array("tit"=>"MarketValue","value"=>"BackupMysqlToCsvMarketvalue.php"),
							);
				for($i=0;$i<count($whitePendesBackupTablas);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesBackupTablas[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesBackupTablas"  id="<?php echo $whitePendesBackupTablas[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesBackupTablas, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>


		<!-- 1.1) Pasar los CSV de access1 a la Tabla Pendes-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesCSVtoPendes','imgyellowPendesCSVtoPendes'); return false;">
				<img id="imgyellowPendesCSVtoPendes" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Pasar los CSV de access1 a la Tabla Pendes</th>
			<th width="180"><input type="checkbox" name="ckbPendesCSVtoPendes"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesCSVtoPendes, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesCSVtoPendes" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesCSVtoPendes=array(
										array("tit"=>"Pendes","value"=>"TransferAccess1ToCsvPendes.php"),
							);
				for($i=0;$i<count($whitePendesCSVtoPendes);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesCSVtoPendes[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesCSVtoPendes"  id="<?php echo $whitePendesCSVtoPendes[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesCSVtoPendes, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>


	<!-- 1.2) Ejecutar los php de Sincronización de Data para Pendes-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesSincPendes','imgyellowPendesSincPendes'); return false;">
				<img id="imgyellowPendesSincPendes" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Ejecutar los php de Sincronización de Data para Pendes</th>
			<th width="180"><input type="checkbox" name="ckbPendesSincPendes"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesSincPendes, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesSincPendes" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesSincPendes=array(
										array("tit"=>"SincronizaMarketconPen","value"=>"sincronizaMarketconPen.php")
									   ,array("tit"=>"SincronizaMkvconMkpsPsumPendesY","value"=>"sincronizaMkvconMkpsPsumPendesY.php")
									   ,array("tit"=>"Fillsparcel","value"=>"fillsparcel.php")
							);
				for($i=0;$i<count($whitePendesSincPendes);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesSincPendes[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesSincPendes" id="<?php echo $whitePendesSincPendes[$i]["value"] ?>" value="<?php echo ($i+1) ?>" onClick="checkSeleccion(document.formulario.ckbPendesSincPendes, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		
		<!-- 1.3) -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesCalculos','imgyellowPendesCalculos'); return false;">
				<img id="imgyellowPendesCalculos" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Calculos</th>
			<th width="180"><input type="checkbox" name="ckbPendesCalculos"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesCalculos, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesCalculos" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesCalculos=array(
										array("tit"=>"CalculoMarketValuePsummary","value"=>"calculoMarketValuePsummary.php")
							);
				for($i=0;$i<count($whitePendesCalculos);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesCalculos[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesCalculos"  id="<?php echo $whitePendesCalculos[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesCalculos, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		
		<!-- 1.4) -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesTransMantoSis','imgyellowPendesTransMantoSis'); return false;">
				<img id="imgyellowPendesTransMantoSis" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Transferir Data de Tabla Mant-->Sis</th>
			<th width="180"><input type="checkbox" name="ckbPendesTransMantoSis"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesTransMantoSis, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesTransMantoSis" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesTransMantoSis=array(
										 array("tit"=>"Marketvalue","value"=>"TransferMantToSisMarketValue.php"),
										 array("tit"=>"MarketPs","value"=>"TransferMantToSisMarketPs.php")
							);
				for($i=0;$i<count($whitePendesTransMantoSis);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesTransMantoSis[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesTransMantoSis"  id="<?php echo $whitePendesTransMantoSis[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesTransMantoSis, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		<!-- 1.5) Creando CSV a transferir de Data Mantenimiento -> Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesCSVTransDM','imgyellowPendesCSVTransDM'); return false;">
				<img id="imgyellowPendesCSVTransDM" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Creando CSV a transferir de Data Mantenimiento->Sistema</th>
			<th width="180"><input type="checkbox" name="ckbPendesCSVTransDM"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesCSVTransDM, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesCSVTransDM" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesCSVTransDM=array(
										array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php"),
										array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
										array("tit"=>"MarketValue","value"=>"BackupMysqlToCsvMarketvalue.php"),
							);
				for($i=0;$i<count($whitePendesCSVTransDM);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesCSVTransDM[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesCSVTransDM"  id="<?php echo $whitePendesCSVTransDM[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesCSVTransDM, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		
				<!-- 1.6) Validacion de data -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesValData','imgyellowPendesValData'); return false;">
				<img id="imgyellowPendesValData" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455"> Validacion de data</th>
			<th width="180"><input type="checkbox" name="ckbPendesValData"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesValData, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesValData" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesValData=array(
										array("tit"=>"cantRecordMarketPS","value"=>"cantRecordMarketPS.php"),
										array("tit"=>"cantRecordMarketValue","value"=>"cantRecordMarketValue.php"),
										array("tit"=>"cantRecordPendes","value"=>"cantRecordPendes.php"),
										array("tit"=>"query","value"=>"query.php"),
										array("tit"=>"PorcentajeCalculomarketps","value"=>"PorcentajeCalculomarketps.php"),
										array("tit"=>"Cantidades ForeClosure y PreForeclosure","value"=>"CantidadPOF.php"),
										array("tit"=>"Cantidad de Pendes con Mortgage=0","value"=>"CantidadPendesMTGCERO.php"),
										array("tit"=>"Cantidad de DebtTv=0 - ArrPctjMarketValuePS","value"=>"CantidadDebttvCero.php")
							);
				for($i=0;$i<count($whitePendesValData);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesValData[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesValData"  id="<?php echo $whitePendesValData[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesValData, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		
				<!-- 1.7) Hacer Backup de las Tablas de Data del Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesBackupDS','imgyellowPendesBackupDS'); return false;">
				<img id="imgyellowPendesBackupDS" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Hacer Backup de las Tablas de Data del Sistema (FL)</th>
			<th width="180"><input type="checkbox" name="ckbPendesBackupDS"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesBackupDS, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesBackupDS" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesBackupDS=array(
										array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php"),
										array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
										array("tit"=>"MarketValue","value"=>"BackupMysqlToCsvMarketvalue.php"),
							);
				for($i=0;$i<count($whitePendesBackupDS);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesBackupDS[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesBackupDS"  id="<?php echo $whitePendesBackupDS[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesBackupDS, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		
		
		<!-- 1.8)Restore de los CSV de Data Mantenimiento->Sistema -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowPendesResCSVDM','imgyellowPendesResCSVDM'); return false;">
				<img id="imgyellowPendesResCSVDM" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Restore de los CSV de Data Mantenimiento->Sistema (FL)</th>
			<th width="180"><input type="checkbox" name="ckbPendesResCSVDM"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPendesResCSVDM, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="yellowPendesResCSVDM" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitePendesResCSVDM=array(
										array("tit"=>"MarketPs","value"=>"RestoreCsvToMysqlMarketPs.php"),
										array("tit"=>"Pendes","value"=>"RestoreCsvToMysqlPendes.php"),
										array("tit"=>"MarketValue","value"=>"RestoreCsvToMysqlMarketvalue.php")
							);
				for($i=0;$i<count($whitePendesResCSVDM);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitePendesResCSVDM[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbPendesResCSVDM"  id="<?php echo $whitePendesResCSVDM[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbPendesResCSVDM, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>
		

	</table>	
  </td>
  </tr>
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->



<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
