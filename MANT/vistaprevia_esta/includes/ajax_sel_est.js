var resgraf;

function nuevoAjax()
{ 
	var xmlhttp=false; 
	try 
	{ xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 	}
	catch(e)
	{ 	try	{ xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); } 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
	return xmlhttp; 
}

function act_sis()
{
	var year,month;
	year=document.getElementById('year').value;
	month=document.getElementById('month').value;
	
	//alert("funcion actualizar");
	
	var res;
	var ajax=nuevoAjax();
	ajax.open("POST", "actualiza_sistema.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("year="+year+"&month="+month);

	ajax.onreadystatechange=function() 
	{
		if (ajax.readyState==4) 
		{
			alert(ajax.responseText);
			//alert("...aqui el response text!!!!");
			
		}
	}

}

function combo_bd()
{
	var res;
	var ajax=nuevoAjax();
	ajax.open("POST", "combobox_county.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send();
	
	
	ajax.onreadystatechange=function() 
	{
		if (ajax.readyState==4) 
		{
			var resultado=ajax.responseText;
			//alert(resultado);
			if(resultado=="message")
			{
				alert("Sorry, Databases Cannot be displayed");
			}
			else
			{
				res=eval('['+resultado+']');
				var valbd;
				for(var i=0; i<res.length; i++)
				{
					switch(res[i].bd)
					{
						case "flbroward":
							valbd="BROWARD";
							break;
						case "fldade":
							valbd="DADE";
							break;
						case "flpalmbeach":
							valbd="PALMBEACH";
							break;
						default: valbd="NONE"
					}
					//alert("res[i].county: "+res[i].county+" valbd:"+valbd);
					document.getElementById("county").options[i]=new Option(res[i].county,valbd);

				}
			}
			
		}
	}
}

function sel_est(){
	
	var year,month,county,zip,type;
	year=document.getElementById('year').value;
	month=document.getElementById('month').value;
	county=document.getElementById('county').value;
	zip=document.getElementById('zip').value;
	type=document.getElementById('type').value;

	var ajax=nuevoAjax();
	//alert("year="+year+"&month="+month+"&county="+county);
	if(zip=="") // sin zip
	{
		
		if(type=="SingleFamily")
		{
			//alert("year="+year+"&month="+month+"&county="+county);
			ajax.open("POST", "sel_est_man_RE1.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("year="+year+"&month="+month+"&county="+county);
		}
		else
		{
			ajax.open("POST", "sel_est_man_RE2.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("year="+year+"&month="+month+"&county="+county);
		}
	}
	else // con zip
	{
		if(type=="SingleFamily")
		{
			ajax.open("POST", "sel_est_man_RE1z.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("year="+year+"&month="+month+"&county="+county+"&zip="+zip);
		}
		else
		{
			ajax.open("POST", "sel_est_man_RE2z.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("year="+year+"&month="+month+"&county="+county+"&zip="+zip);
		}
		
	}

	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			
			var resultado=ajax.responseText;
			//alert(resultado);
			if(resultado=="message"){
				
				alert("Sorry, There is no statistics for this parameters");
			}
			else
			{

			res=eval('['+resultado+']');
		
			for(var i=0; i<res.length; i++)
			{
				// MES-A�O
				if( res[i].month.substr(0,3)=="Dic" )
				{
					document.getElementById("mes_"+i).innerText=res[i].month.replace("Dic","Dec");
				}
				else
				if( res[i].month.substr(0,3)=="Agu" )
				{
					document.getElementById("mes_"+i).innerText=res[i].month.replace(res[i].month.substr(0,3),"Aug");;
				}
				else	
				{
				document.getElementById("mes_"+i).innerText=res[i].month;
				}
				//MEDIANA
				document.getElementById("ave_"+i).innerText=res[i].med;
				
				//%MEDIANA
				if(i==0)
				{
					document.getElementById("ave1_"+i).innerText='0.00';
				}
				else
				{
					document.getElementById("ave1_"+i).innerText=res[i].diffmed;
				}
				//TOTAL QUANTITY	
				document.getElementById("qt_"+i).innerText=res[i].qtty;

				//% TOTAL QUANTITY
				if(i==0)
				{
					document.getElementById("qt1_"+i).innerText='0.00';
				}
				else
				{
					document.getElementById("qt1_"+i).innerText=res[i].diffqt;
				}
				
				//QUANTITY DE MLS
				document.getElementById("mls_"+i).innerText=res[i].mls;
				
				//QUANTITY COUNTY
				document.getElementById("cou_"+i).innerText=res[i].county;
			}
			
			resgraf=res;	//Esta variable se usa para los parametros que se pasaran a la siguiente funcion para la genracion del graficO
			generarGrafico();
			}//else de si hay resultados
	
		} // fin de if de ajax.readyState
	} //fin de ajax.onreadystatechange
} // fin de la funcion

function generarGrafico()
{
	var param="param=";
	for (i=0;i<resgraf.length;i++)
	{
		if (i>0) param+="^";
		param+='"med"=>'+resgraf[i].med+',"diffmed"=>'+resgraf[i].diffmed+',"qtty"=>'+resgraf[i].qtty+',"diffqt"=>'+resgraf[i].diffqt+',"mls"=>'+resgraf[i].mls+',"county"=>'+resgraf[i].county+'';
		
	}
	
	ajax=nuevoAjax();
	ajax.open("POST", "grafico_pag.php",true);		
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send(param);
	
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4) 
		{
			var retorno=ajax.responseText;
			//alert("llamada a setgrafico");
			setGrafico()
							
		} // fin de if de ajax.readyState
	} //fin de ajax.onreadystatechange
} // fin de la funcion


//**************************************** [ Maria Oliveira ]
//Se llama a esta funcion en el Load o al cambiar cualquier parametro en la seccion del Grafico de la estadistica, como: tipo de Grafico o la Serie, a traves del ONLOAD

function setGrafico()
{
	document.getElementById("reloj").style.visibility="visible";	//reloj de indicacion de ejecucion de un proceso
	var opcion=document.getElementById("opciones").value;
	var grafico=document.getElementById("tipografico").value;

	//alert("funcion setgrafico!!!!");
	
	var mt1=resgraf[0].month;
	var mt2=resgraf[1].month;
	var mt3=resgraf[2].month;
	var mt4=resgraf[3].month;
	var mt5=resgraf[4].month;
	var mt6=resgraf[5].month;
	var mt7=resgraf[6].month;
	var mt8=resgraf[7].month;
	var mt9=resgraf[8].month;
	var mt10=resgraf[9].month;
	var mt11=resgraf[10].month;
	var mt12=resgraf[11].month;
	
	
	//alert("opciones="+opcion+"&tipografico="+grafico+"&mt1="+mt1+"&mt2="+mt2+"&mt3="+mt3+"&mt4="+mt4+"&mt5="+mt5+"&mt6="+mt6+"&mt7="+mt7+"&mt8="+mt8+"&mt9="+mt9+"&mt10="+mt10+"&mt11="+mt11+"&mt12="+mt12);
	
	ajax=nuevoAjax();
	ajax.open("POST", "grafico_maindin.php",true);		//Donde se genera el grafico correspondiente segun las opciones
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("opciones="+opcion+"&tipografico="+grafico+"&mt1="+mt1+"&mt2="+mt2+"&mt3="+mt3+"&mt4="+mt4+"&mt5="+mt5+"&mt6="+mt6+"&mt7="+mt7+"&mt8="+mt8+"&mt9="+mt9+"&mt10="+mt10+"&mt11="+mt11+"&mt12="+mt12);
	
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4) 
		{
			var retorno=ajax.responseText;
			document.getElementById("reloj").style.visibility="hidden";
			document.getElementById("grafico").innerHTML=retorno;
			
		} // fin de if de ajax.readyState
	} //fin de ajax.onreadystatechange
} // fin de la funcion

