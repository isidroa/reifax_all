<?php
//************************************ [ Maria Oliveira ]   --- copiado de Smith
//GENERA EL GRAFICO DE LA ESTADISTICA EN : PAGMENU1.PHP
	session_start();
	if (empty($_POST['opciones']))$opciones='0'; else $opciones=strtolower($_POST['opciones']);
	if (empty($_POST['tipografico']))$tipografico='0'; else $tipografico=strtolower($_POST['tipografico']);

$mt1=$_POST["mt1"];
$mt2=$_POST["mt2"];
$mt3=$_POST["mt3"];
$mt4=$_POST["mt4"];
$mt5=$_POST["mt5"];
$mt6=$_POST["mt6"];
$mt7=$_POST["mt7"];
$mt8=$_POST["mt8"];
$mt9=$_POST["mt9"];
$mt10=$_POST["mt10"];
$mt11=$_POST["mt11"];
$mt12=$_POST["mt12"];


	function limpiardirimagenes()
	{
		$ruta="includes/phplot/generated/";
		$directorio=dir($ruta); 
		$directorio->read();$directorio->read();
		$tiempo=strtotime("now")-1800;//Borra los que estan modificados de hace dos horas
		while ($archivo = $directorio->read()) 
		{ 
			$ext=substr($archivo,strrpos($archivo,'.')+1);
			if(filectime($ruta.$archivo)<$tiempo && $ext=='png')
				unlink($ruta.$archivo);  
			//echo $extension." ".$ruta.$archivo." ".date ("Y-n-d_H:i:s", filemtime($ruta.$archivo))." ".date ("Y-n-d_H:i:s")."<br>";
		} 
		$directorio->close(); 	
	}

?>
<!--<html>
<head>
<title>Graphics</title>
<link href="includes/css/stats.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#EDEEF0">
	<form name="form1" id="form1" method="post" action="<?php //echo $_SERVER['PHP_SELF']; ?>" target="_self">
		<table width="551">
		  <tr>
			<td width="44">Series</td>
			<td width="116">
			  <select name="opciones" id="opciones">
				<option value="0" <?php //if ($opciones=='0') echo "selected"; else echo ""; ?>>Median</option>
				<option value="1" <?php //if ($opciones=='1') echo "selected"; else echo ""; ?>>% Median</option>
				<option value="2" <?php //if ($opciones=='2') echo "selected"; else echo ""; ?>>Quantity</option>
				<option value="3" <?php //if ($opciones=='3') echo "selected"; else echo ""; ?>>% Quantity</option>
				<option value="4" <?php //if ($opciones=='4') echo "selected"; else echo ""; ?>>Quantity Listing</option>
				<option value="5" <?php //if ($opciones=='5') echo "selected"; else echo ""; ?>>Quantity County</option>
			</select>
			</td>
			<td width="57">Graphic</td>
			<td width="138">
			<select name="tipografico" id="tipografico">
			  <option value="0" <?php //if ($tipografico=='0') echo "selected"; else echo ""; ?>>VerticalBar1</option>
			  <option value="1" <?php //if ($tipografico=='1') echo "selected"; else echo ""; ?>>VerticalBar2</option>
			  <option value="2" <?php //if ($tipografico=='2') echo "selected"; else echo ""; ?>>Pie</option>
			  <option value="3" <?php //if ($tipografico=='3') echo "selected"; else echo ""; ?>>Line</option>
			</select></td>
			<td width="172"><input type="submit" name="Submit" class="botonsml" value="Execute"></td>
		  </tr>
		</table>
	</form> -->
<?
	limpiardirimagenes();

	$mon=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$anio=date("Y");
	$mes=date("n")-1;    //Originalmente es 1 --- Se coloca 2 cuando se 'trunca' la estadistica en pagmenu1.php para cuestiones de mantenimiento
	$indmes=0;
	/*
	for($i=$mes;$i<12;$i++) 
		$mestit[$indmes++]=$mon[$i].'-'.($anio-1);
	for($i=0;$i<$mes;$i++)	
		$mestit[$indmes++]=$mon[$i].'-'.($anio);
   */
   $mestit[0]=$mt1;
   $mestit[1]=$mt2;
   $mestit[2]=$mt3;
   $mestit[3]=$mt4;
   $mestit[4]=$mt5;
   $mestit[5]=$mt6;
   $mestit[6]=$mt7;
   $mestit[7]=$mt8;
   $mestit[8]=$mt9;
   $mestit[9]=$mt10;
   $mestit[10]=$mt11;
   $mestit[11]=$mt12;
   
	$param2=$_SESSION["param"];

	$param2=str_replace('\"','"',$param2);
	$arr_param2=explode("^",$param2);
	$param2="";
	for ($i=0;$i<count($arr_param2);$i++)  //for ($i=count($arr_param2)-1;$i>-1;$i--)//
	{
		if ($i>0) $param2.=",";
		$param2.="array(".$arr_param2[$i].")";
	}
	$tmp2='$aresultados = array('.$param2.');';	
	eval($tmp2);
	
	$indice=11;
	$indmes=11;
	
	while (count($aresultados)>$indice && $indice>=0)//$indice<count($aresultados))
	{

		$fecdiv=explode('-',$mestit[$indmes]);
		if($fecdiv[0]=="Dic")
			$mestit[$indmes]="Dec-".$fecdiv[1];
		if($fecdiv[0]=="Agu")
			$mestit[$indmes]="Aug-".$fecdiv[1];
		
		$array_resultados[]=array("mes"=>$mestit[$indmes],
								"med"=>$aresultados[$indice]["med"],
								"diffmed"=>$aresultados[$indice]["diffmed"],
								"qtty"=>$aresultados[$indice]["qtty"],
								"diffqt"=>$aresultados[$indice]["diffqt"],
								"mls"=>$aresultados[$indice]["mls"],
								"county"=>$aresultados[$indice]["county"]
								);
		$indice--;
		$indmes--;
	}
	if($opciones=='0'){$titulo="Median";$indtit="med";}
	if($opciones=='1'){$titulo="% Median";$indtit="diffmed";}
	if($opciones=='2'){$titulo="Quantity";$indtit="qtty";}
	if($opciones=='3'){$titulo="% Total Quantity";$indtit="diffqt";}
	/*if($opciones=='2'){$titulo="Total Quantity";$indtit="qtty";}
	if($opciones=='4'){$titulo="Quantity Listing";$indtit="mls";}
	if($opciones=='5'){$titulo="Quantity County";$indtit="county";}
	*/
	$ind=11;
	$pmin=1000000;
	$pmax=-1000000;
	
	if($opciones=='2'){  //Si el Grafico es para Quantity
		$data=array(array($array_resultados[11]["mes"],$array_resultados[11]["qtty"],$array_resultados[11]["mls"],$array_resultados[11]["county"]),array($array_resultados[10]["mes"],$array_resultados[10]["qtty"],$array_resultados[10]["mls"],$array_resultados[10]["county"]),array($array_resultados[9]["mes"],$array_resultados[9]["qtty"],$array_resultados[9]["mls"],$array_resultados[9]["county"]),array($array_resultados[8]["mes"],$array_resultados[8]["qtty"],$array_resultados[8]["mls"],$array_resultados[8]["county"]),array($array_resultados[7]["mes"],$array_resultados[7]["qtty"],$array_resultados[7]["mls"],$array_resultados[7]["county"]),array($array_resultados[6]["mes"],$array_resultados[6]["qtty"],$array_resultados[6]["mls"],$array_resultados[6]["county"]),array($array_resultados[5]["mes"],$array_resultados[5]["qtty"],$array_resultados[5]["mls"],$array_resultados[5]["county"]),array($array_resultados[4]["mes"],$array_resultados[4]["qtty"],$array_resultados[4]["mls"],$array_resultados[4]["county"]),array($array_resultados[3]["mes"],$array_resultados[3]["qtty"],$array_resultados[3]["mls"],$array_resultados[3]["county"]),array($array_resultados[2]["mes"],$array_resultados[2]["qtty"],$array_resultados[2]["mls"],$array_resultados[2]["county"]),array($array_resultados[1]["mes"],$array_resultados[1]["qtty"],$array_resultados[1]["mls"],$array_resultados[1]["county"]),array($array_resultados[0]["mes"],$array_resultados[0]["qtty"],$array_resultados[0]["mls"],$array_resultados[0]["county"]));
			
	}else{
		while (count($array_resultados)>$ind && $ind>=0)
		{
			if($opciones=='1' || $opciones=='3' )
			{
				if($array_resultados[$ind][$indtit]>$pmax)$pmax=$array_resultados[$ind][$indtit];
				if($array_resultados[$ind][$indtit]<$pmin)$pmin=$array_resultados[$ind][$indtit];
			}
			
			$data[]=array($array_resultados[$ind]['mes'], $array_resultados[$ind][$indtit]);
		
			$ind--;
		}
	}
	
	require_once 'includes/phplot/phplot.php';
	$output_file='includes/phplot/generated/'.strtotime("now").'.png';

	if($opciones=='0' /*|| $opciones=='2' */|| $opciones=='4' || $opciones=='5')//Med 
	{
		if($tipografico==0)$ploti=0;
		if($tipografico==1)$ploti=1;
		if($tipografico==2)$ploti=3;
		if($tipografico==3)$ploti=4;
	}
	if($opciones=='1' || $opciones=='3')//% Med o % Quantity
	{
		if($tipografico==0 || $tipografico==1)$ploti=2;  
		if($tipografico==2)$ploti=3;
		if($tipografico==3)$ploti=5;					
	}
	if($opciones=='2')//Quantity
	{
		if($tipografico==0)$ploti=6;
		if($tipografico==1)$ploti=1;
		if($tipografico==2)$ploti=7;
		if($tipografico==3)$ploti=8;
	}
    

	//0:VerticalBar normal
	//1:VerticalBar Titulos arriba 
	//2:VerticalBar normal para los porcentajes
	//3:Pie 
	//4:Line Median y Quantity
	//5:Line % Median y % Quantity
	
	if($ploti==0)//VerticalBar normal 
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('bars');
		$plot->SetDataType('text-data');
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		$plot->SetLegend(array($titulo));
		$plot->SetXTitle('Months');
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	
	if($ploti==1)//1:VerticalBar Titulos arriba 
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('bars');
		$plot->SetDataType('text-data');
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetPlotAreaWorld(NULL, 0);
		$plot->SetYTickIncrement(100);
		$plot->SetXTitle('Months');
		$plot->SetYDataLabelPos('plotin');
		$plot->SetYTickLabelPos('none');
		$plot->SetYTickPos('none');
		$plot->SetPrecisionY(1);
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==2)//2:VerticalBar normal para los porcentajes
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('bars');
		$plot->SetDataType('text-data');
		$plot->SetDataValues($data);
		$plot->SetPlotAreaWorld(0, $pmin-5, NULL, $pmax+5);
		$plot->SetTitle('County Sales: '.$titulo);
		$plot->SetLegend(array($titulo));
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetXTitle('Months');
		//$plot->SetYTickIncrement(2);
		$plot->SetPrecisionY(4);
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==3)//3:Pie 
	{
		$plot =& new PHPlot(650,300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('pie');
		$plot->SetDataType('text-data-single');
		$plot->SetDataValues($data);
		$plot->SetDataColors(array('red', 'green', 'blue', 'yellow', 'cyan',
								'magenta', 'brown', 'lavender', 'pink',
								'gray', 'orange','tan'));
		$plot->SetTitle('County Sales: '.$titulo);
		foreach ($data as $row)
		  $plot->SetLegend(implode(': ', $row));
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==4)//4:Line Median y Quantity
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		$plot->SetLegend(array($titulo));
		$plot->SetXTitle('Months');
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==5)//5:Line % Median y % Quantity
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		$plot->SetLegend(array($titulo));
		$plot->SetPlotAreaWorld(0, $pmin-5, NULL, $pmax+5);
		$plot->SetXTitle('Months');
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	
	//**************Para gRafico de Quantity
	if($ploti==6)//VerticalBar normal 
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('bars');
		$plot->SetDataType('text-data');
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		# Make a legend for the 3 data sets plotted:
		$plot->SetLegend(array('Total', 'Listing', 'County'));
		$plot->SetXTitle('Months');
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==7)//3:Pie 
	{
		$plot =& new PHPlot(650,300);
		$plot->SetIsInline(true);
		$plot->SetImageBorderType('plain');
		$plot->SetPlotType('pie');
		$plot->SetDataType('text-data-single');
		$plot->SetDataValues($data);
		$plot->SetDataColors(array('red', 'green', 'blue', 'yellow', 'cyan',
								'magenta', 'brown', 'lavender', 'pink',
								'gray', 'orange','tan'));
		$plot->SetTitle('County Sales: '.$titulo);
		foreach ($data as $row)
		  $plot->SetLegend(implode(': ', $row));
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	if($ploti==8)//4:Line Median y Quantity
	{
		$plot =& new PHPlot(600, 300);
		$plot->SetIsInline(true);
		$plot->SetDataValues($data);
		$plot->SetTitle('County Sales: '.$titulo);
		# Make a legend for the 3 data sets plotted:
		$plot->SetLegend(array('Total', 'Listing', 'County'));
		$plot->SetXTitle('Months');
		$plot->SetXTickLabelPos('none');
		$plot->SetXTickPos('none');
		$plot->SetOutputFile($output_file);
		$plot->DrawGraph();
	}
	//*****************************************
	echo '<img alt="'.$titulo.'" src="'.$output_file.'" style="border: 1px solid gray;"/>';
?>
<!--</body>
</html>  -->