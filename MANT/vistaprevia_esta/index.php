<html><!-- InstanceBegin template="/Templates/template1new.dwt" codeOutsideHTMLIsLocked="false" -->
<?php  require('Templates/Template.php'); ?>
		<head>
    
    <?php TemplateHeader(); ?>  
<!-- InstanceBeginEditable name="doctitle" -->
<title>Xima LCC. - Realtor Services </title>
<!-- InstanceEndEditable -->


<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="includes/ajax_sel_est.js"></script>

<link href="includes/css/stats.css" rel="stylesheet" type="text/css">
<!-- InstanceEndEditable -->
		</head>
<body>
	<?php TemplateBody();?>
<!-- InstanceBeginEditable name="contenido" -->
  <div id="reloj" style="background-color:#FFFFE1;position:absolute;top:30%;left:40%;visibility:hidden;"><img src="img/ac.gif">Please 
          wait, loading!...</div><!--Reloj-->
    <h1 align="center" style=" font-size:1.2em; font-weight:bold; margin:1px; padding:0; text-transform:uppercase;">County Sales</h1>
    
	
	<table  class="monthtable">
      <tr>
	  
        <td class="nothing"><span class="textolateralcampo">County</span>
            <label>
            <select name="county" id="county" onChange="sel_est();">
              <!--<option value="BROWARD">Broward</option>-->
             <!-- <option value="DADE">Dade</option>-->
			  <!--<option value="PALMBEACH">Palm Beach</option>-->
            </select>
          </label></td>
        <td class="nothing"><span class="textolateralcampo">Type</span>
            <label>
            <select name="type" id="type" onChange="sel_est();">
              <option value="SingleFamily">S. Family</option>
              <option value="Condominium">Condominium</option>
            </select>
          </label></td>
        <td class="nothing"  ><span class="textolateralcampo">ZIP
            <label> </label>
          </span>
            <label>
            <input name="zip" type="text" id="zip" style="width:60px;">
          </label></td>
      </tr> 
    </table>
	
	
    <table  class="monthtable">
	  <tr id="mesTh">
<th>SOLD</th>
<th id="mes_0"></th>
<th id="mes_1"></th>	
<th id="mes_2"></th>	
<th id="mes_3"></th>	
<th id="mes_4"></th>	
<th id="mes_5"></th>	
<th id="mes_6"></th>	
<th id="mes_7"></th>	
<th id="mes_8"></th>	
<th id="mes_9"></th>	
<th id="mes_10"></th>			
<th id="mes_11"></th>
      </tr>
	  <tr>
      	<th width="10%">Median</th>
		<td id="ave_0"></td> 
        <td id="ave_1"></td>
        <td id="ave_2"></td>
        <td id="ave_3"></td>
        <td id="ave_4"></td>
        <td id="ave_5"></td>
        <td id="ave_6"></td>
        <td id="ave_7"></td>
        <td id="ave_8"></td>
        <td id="ave_9"></td>
        <td id="ave_10"></td>
        <td id="ave_11"></td>
      </tr>
	  <tr>
        <th>% Median</th>
		<td id="ave1_0"></td>
        <td id="ave1_1"></td>
        <td id="ave1_2"></td>
        <td id="ave1_3"></td>
        <td id="ave1_4"></td>
        <td id="ave1_5"></td>
        <td id="ave1_6"></td>
        <td id="ave1_7"></td>
        <td id="ave1_8"></td>
        <td id="ave1_9"></td>
        <td id="ave1_10"></td>
        <td id="ave1_11"></td>
      </tr>
     <tr>
        <th width="10%">Quantity Listing</th>
	    <td id="mls_0"></td> 
        <td id="mls_1"></td>
        <td id="mls_2"></td>
        <td id="mls_3"></td>
        <td id="mls_4"></td>
        <td id="mls_5"></td>
        <td id="mls_6"></td>
        <td id="mls_7"></td>
        <td id="mls_8"></td>
        <td id="mls_9"></td>
        <td id="mls_10"></td>
        <td id="mls_11"></td>
      </tr>
	  <tr>
        <th width="10%">Quantity County</th>
		<td id="cou_0"></td> 
        <td id="cou_1"></td>
        <td id="cou_2"></td>
        <td id="cou_3"></td>
        <td id="cou_4"></td>
        <td id="cou_5"></td>
        <td id="cou_6"></td>
        <td id="cou_7"></td>
        <td id="cou_8"></td>
        <td id="cou_9"></td>
        <td id="cou_10"></td>
        <td id="cou_11"></td>
      </tr>

	  <tr>
        <th >Total Quantity</th>
		<td id="qt_0"></td>
        <td id="qt_1"></td>
        <td id="qt_2"></td>
        <td id="qt_3"></td>
        <td id="qt_4"></td>
        <td id="qt_5"></td>
        <td id="qt_6"></td>
        <td id="qt_7"></td>
        <td id="qt_8"></td>
        <td id="qt_9"></td>
        <td id="qt_10"></td>
        <td id="qt_11"></td>
       </tr>
	 <tr>
       <th >% Total Quantity</th>
		<td id="qt1_0"></td>
        <td id="qt1_1"></td>
        <td id="qt1_2"></td>
        <td id="qt1_3"></td>
        <td id="qt1_4"></td>
        <td id="qt1_5"></td>
        <td id="qt1_6"></td>
        <td id="qt1_7"></td>
        <td id="qt1_8"></td>
        <td id="qt1_9"></td>
        <td id="qt1_10"></td>
        <td id="qt1_11"></td>
      </tr>
	  
	  
    </table>
	
	<table>
	<tr>
	<td><label> YEAR </label></td>
	<td><select name="year" id="year">
	  <option value="0" selected>Seleccione</option>
	  <?php for($iy=2007;$iy<2011;$iy++) 
	  {?>
			  <option value="<?php echo $iy ?>"  <?php if ($year==$iy) echo "selected"; else echo ""; ?>  ><?php echo $iy ?></option>
     <?php } ?> 
			  </select>
			  </td>
     <td><label> MONTH </label></td> 
			  <td><select name="month" id="month">
              <option value="1" <?php if ($month=='1') echo "selected"; else echo ""; ?>>January</option>
              <option value="2" <?php if ($month=='2') echo "selected"; else echo ""; ?>>February</option>
			  <option value="3" <?php if ($month=='3') echo "selected"; else echo ""; ?>>March</option>
			  <option value="4" <?php if ($month=='4') echo "selected"; else echo ""; ?>>April</option>
			  <option value="5" <?php if ($month=='5') echo "selected"; else echo ""; ?>>May</option>
			  <option value="6" <?php if ($month=='6') echo "selected"; else echo ""; ?>>June</option>
			  <option value="7" <?php if ($month=='7') echo "selected"; else echo ""; ?>>July</option>
			  <option value="8" <?php if ($month=='8') echo "selected"; else echo ""; ?>>August</option>
			  <option value="9" <?php if ($month=='9') echo "selected"; else echo ""; ?>>September</option>
			  <option value="10" <?php if ($month=='10') echo "selected"; else echo ""; ?>>October</option>
			  <option value="11" <?php if ($month=='11') echo "selected"; else echo ""; ?>>November</option>
  			  <option value="12" <?php if ($month=='12') echo "selected"; else echo ""; ?>>December</option>
            </select></td> 
    <td><input name="button" type="submit" id="button" value="Accept" class="botonlg" onClick="sel_est();"/></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	
	<td><input name="button" type="submit" id="button2" value="Update" class="botonlg" onClick="act_sis();"/></td>
	</tr>
	</table>
	
	
	<!-- GRAFICO DE LA ESTADISTICA -->
	<form name="form1" id="form1" method="post" action="" target="_self" bgcolor="#EDEEF0">
		<table width="450" align="center">
		  <tr>
			<td width="40">Series</td>
			<td width="274" >
			  <select name="opciones" id="opciones" onChange="setGrafico();">
				<option value="0" <?php if ($opciones=='0') echo "selected"; else echo ""; ?>>Median</option>
				<option value="1" <?php if ($opciones=='1') echo "selected"; else echo ""; ?>>% Median</option>
				<!--<option value="4" <?php //if ($opciones=='4') echo "selected"; else echo ""; ?>>Quantity Mls</option>-->
				<!--<option value="5" <?php //if ($opciones!='5') echo "selected"; else echo ""; ?>>Quantity County</option>-->
				<option value="2" <?php if ($opciones=='2' || $opciones=="") echo "selected"; else echo ""; ?>>All Quantities</option>
				<option value="3" <?php if ($opciones=='3') echo "selected"; else echo ""; ?>>% Total Quantity</option>
				
				
			</select>
			</td>
			<td width="59">Graphic</td>
			<td width="158">
			<select name="tipografico" id="tipografico" onChange="setGrafico();">
			  <option value="0" <?php if ($tipografico=='0') echo "selected"; else echo ""; ?>>VerticalBar1</option>
			  <option value="1" <?php if ($tipografico=='1') echo "selected"; else echo ""; ?>>VerticalBar2</option>
			  <option value="2" <?php if ($tipografico=='2') echo "selected"; else echo ""; ?>>Pie</option>
			  <option value="3" <?php if ($tipografico=='3') echo "selected"; else echo ""; ?>>Line</option>
			</select></td>
			<!--<td width="172"><input type="button" name="Submit" class="botonsml" value="Execute" onClick="setGrafico();"></td>-->
		  </tr>
		</table>
		
		
	</form> 
	<div id="grafico" align="center"></div>
	            
<!-- InstanceEndEditable -->
	<?php TemplatePie(); ?>
<!-- InstanceBeginEditable name="EditRegion5" -->
<script>
if (window.attachEvent) {
	window.attachEvent("onload", combo_bd);
	///===>window.attachEvent("onbeforeunload", releaseSess);
} else {
	window.addEventListener("load", combo_bd, false);
	///===>	window.addEventListener("onbeforeunload", releaseSess, false);
}
//show('Itoolbar');
</script>
<!-- InstanceEndEditable -->
</body>
<script type="text/javascript" src="includes/tooltip.js"></script>
<!-- InstanceEnd --></html>
