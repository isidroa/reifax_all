<div id="Itoolbar" class="visible">
<?php if($_SESSION['browser']=='IE'){ $pad =''; }else{ if($_SESSION['browser']=='Safari'){ $pad='style="padding:5px 0px 8px 0px;"'; }else{ $pad='style="padding:5px 0px 6px 0px;"'; }} ?>
    <!--MAP-->
    <?php if($toolbar_map==0){?>
        <a title="View Map" href="javascript:void(0);" onclick="javascript:control_map('search');" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/map.png" />
        </a>
    <?php }elseif($toolbar_map==1){?>
        <a title="View Map" href="javascript:void(0);" onclick="javascript:showmap('mymapcontroldiv'); gridAjust(mymapcontroldiv.className);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/map.png" />
        </a>
    <?php }else{?>
        <a title="View Map Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/map_disable.png" />
        </a>
    <?php }?>
    <!--GPS-->
    <?php if($toolbar_gps){?>
        <a title="GPS Map" href="javascript:control_map('gps');" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/gps.png" />
        </a>
    <?php }else{?>
        <a title="GPS Map Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/gps_disable.png" />
        </a>
    <?php }?>
    <!--Legend-->
    <?php if($toolbar_legend){?>
        <script>
            var lsImgCss2={'A-F':{'img':'verdel.png','explain':'Active Foreclosure'},
                            'A-P':{'img':'verdel.png','explain':'Active Pre-Foreclosure'},	
                            'A-N':{'img':'verdeb.png','explain':'Active'},
                            'P-F': {'img':'lilal.png','explain':'Active Foreclosure'},
                            'P-P': {'img':'lilal.png','explain':'Active Pre-Foreclosure'},
                            'P-N': {'img':'lilab.png','explain':'Active'},
                            'CC-F': {'img':'cielol.png','explain':'County Foreclosure'},
                            'CC-P': {'img':'cielol.png','explain':'County Pre-Foreclosure'},
                            'CC-N': {'img':'cielo.png','explain':'County Closed'},
                            'CS-P': {'img':'marronl.png','explain':'Closed Sale'},
                            'CS-F': {'img':'marronl.png','explain':'Closed Sale'},
                            'CS-N': {'img':'marronb.png','explain':'Closed Sale'},
                            'PS-P': {'img':'amarillol.png','explain':'Pending Sale Pre-Foreclosure'},
                            'PS-F': {'img':'amarillol.png','explain':'Pending Sale Foreclosure'},
                            'PS-N': {'img':'amarillob.png','explain':'Pending Sale'},
                            'SP-': {'img':'grisdiamante.png','explain':'Unknow'},
                            'T-P': {'img':'vinotl.png','explain':'Temporary off market Pre-Foreclosure'},
                            'T-F': {'img':'vinotl.png','explain':'Temporary off market Foreclosure'},
                            'T-N': {'img':'vinot.png','explain':'Temporary off market'},
                            'R-P': {'img':'azull.png','explain':'Rental Pre-Foreclosure'},
                            'R-F': {'img':'azull.png','explain':'Rental Foreclosure'},
                            'R-N': {'img':'azulb.png','explain':'Rental'},
                            'E-P': {'img':'naranjal.png','explain':'Unknow'},
                            'E-F': {'img':'naranjal.png','explain':'Unknow'},
                            'E-N': {'img':'naranjab.png','explain':'Unknow'},
                            'C-P': {'img':'rojol.png','explain':'Cancelled Pre-Foreclosure'},
                            'C-F': {'img':'rojol.png','explain':'Cancelled Foreclosure'},
                            'C-N': {'img':'rojob.png','explain':'Cancelled'},
                            'Q-N': {'img':'grisb.png','explain':'Terminated'},
                            'Q-P': {'img':'grisl.png','explain':'Terminated Pre-Foreclosure'},
                            'Q-F': {'img':'grisl.png','explain':'Terminated Foreclosure'},
                            'W-N': {'img':'ocre.png','explain':'Withdraw'},
                            'W-F': {'img':'ocrel.png','explain':'Withdraw Foreclosure'},
                            'W-P': {'img':'ocrel.png','explain':'Withdraw Pre-Foreclosure'},
                            'X-N': {'img':'naranjab.png','explain':'Expired'},
                            'X-P': {'img':'naranjal.png','explain':'Expired Pre-Foreclosure'},
                            'X-F': {'img':'naranjal.png','explain':'Expired Foreclosure'},
                            'Subject': {'img':'verdetotal.png','explain':'Subject'},
                            'user_car': {'img':'xxima.png','explain':'User'},
                            'B-N': {'img':'aguam.png','explain':'Backup contract'},
                            'B-P': {'img':'aguaml.png','explain':'Backup contract Pre-Foreclosure'},
                            'B-F': {'img':'aguaml.png','explain':'Backup contract Foreclosure'},
                            'N-N': {'img':'grisb.png','explain':'Non-Active'},
                            'N-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosure'},
                            'N-F': {'img':'grisl.png','explain':'Non-Active Foreclosure'}};
            var indImgCss;
            function LegendToolbar()
            {	
            var legend=document.getElementById("Legendtoolbar");
            legend.style.zIndex='1000';
            var table=new Array();
            var MORE=false;
            
            table.push('<div style="overflow:auto; height:210px; width:260px;"><table style="background-color:#edeef0">'+
                        '  <tr><td colspan=3 align=right><a href=javascript:void(0); onclick=hideddrivetip();> <img border=0 src=img/cancel.png></a></td></tr>'+
                        '  <tr> '+
                        '    <td ><strong>Color</strong></td>'+
                        '    <td ><strong>Status</strong></td>'+
                        '    <td ><strong>Description</strong></td>'+
                        '  </tr> ');
                    
                    table.push('  <tr> '+
                        '    <td align="center"><img src=img/houses/verdetotal.png></td>'+
                        '    <td>S</td>'+	
                        '    <td>Subject</td>'+
                        '  </tr> ');
                    var status=new Array("A","CC","CS","N")
                    for (i=0;i<status.length;i++){
                        getCasita(status[i],'F');
                        table.push('  <tr> '+
                        '    <td align="center"><img src=img/houses/'+lsImgCss2[indImgCss].img+'></td>'+
                        '    <td>'+status[i]+'-F</td>'+	
                        '    <td>'+lsImgCss2[indImgCss].explain+'</td>'+
                        '  </tr> ');
                        
                        getCasita(status[i],'P');
                        table.push('  <tr> '+
                        '    <td align="center"><img src=img/houses/'+lsImgCss2[indImgCss].img+'></td>'+
                        '    <td>'+status[i]+'-P</td>'+	
                        '    <td>'+lsImgCss2[indImgCss].explain+'</td>'+
                        '  </tr> ');
                        
                        getCasita(status[i],'N');
                        table.push('  <tr> '+
                        '    <td align="center"><img src=img/houses/'+lsImgCss2[indImgCss].img+'></td>'+
                        '    <td>'+status[i]+'-N</td>'+	
                        '    <td>'+lsImgCss2[indImgCss].explain+'</td>'+
                        '  </tr> ');
            
                    } //Fin de For
            
                table.push('</table></div>');
                table=table.join('');
                ddrivetip(table,-135,-5,-5,-10,false);
            }
        </script>
        <a id="Legendtoolbar" title="Legend" href="javascript:void(0);" onclick="javascript:LegendToolbar();" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/legend.png" />
        </a>
    <?php }else{?>
        <a title="Legend Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/legend_disable.png" />
        </a>
    <?php }?>
    <!--Printer-->
        <script>
            var wait='';
            function relojON(){
                wait=window.open('Excel/wait.html','_blank','width=50,height=50');
            }
            function relojOFF(){
                wait.close();
            }			
            function selectedChk(){
                var us='';
                var i2=0;
                var tmp='';
                for (var k in multiSel)
                {
                    if (multiSel[k] == true)
                    {
                        if (i2>0) us+=",";
                        us+= k.replace("chk", "")
                        i2++	
                    }
                }
                
                if(us==''){
                    alert('You must check some records in the field \'SEL\' to activate this option.');
                    return false;
                }else{
                    return(us);
                }
            }
            
            function selectedChkE(){
                var us='';
                var i2=0;
                var tmp='';
                
                for (var k in multiSel)
                {
                    if (multiSel[k] == true)
                    {
                        if (i2>0) us+=",";
                        us+= k.replace("chk", "")
                        i2++	
                    }
                }
                
                if(us==''){
                    alert('You must check some records in the field \'SEL\' to activate this option.');
                    return false;
                }else{
                    //return(us);
                    var obj = document.getElementById('mytempla');
                    var curtemplatoolbar = obj.options[obj.selectedIndex].value;
                    var ajax=nuevoAjax();
                    ajax.open("POST", "Excel/result.php",true);
                    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                    ajax.send("t="+curtemplatoolbar+"&s="+GetCookie('sort')+"&n="+GetCookie('idname')+"&parcel="+us);
					relojON();
                    ajax.onreadystatechange=function()
                    {
                        if (ajax.readyState==4) 
                        {
                            //alert(us);
                            relojOFF();
                            eval(ajax.responseText);
                            //alert(nombre);
                            window.open('Excel/d.php?nombre='+nombre,'_blank','width=50,height=50');
                            return(true);
                        } // fin de if de ajax.readyState
                        
                    } //fin de ajax.onreadystatechange
                }
            }

            var multiSel= new Array();
            function selectedChkP(value){
                var us='';
                var i2=0;
                var tmp='';
                //alert(GetCookie("_allSel"))
                /*if(GetCookie("_allSel") == 'true'){
                    us+="_all";
                }
                else{
                */
                    for (var k in multiSel)
                    {
                        if (multiSel[k] == true)
                        {
                            if (i2>0) us+=",";
                            us+= k.replace("chk", "")
                            i2++	
                        }
                    }
                //alert(us)
                if(us==''){
                    alert('You must check some records in the field \'SEL\' to activate this option.');
                    return false;
                }else{
                    var param="parcel="+us;
                    var pag="FPDF/session.php";
                    if(value=='email'){
                        param+="&email=yes";
                        pag="Email/session.php"
                    }
                    
                    var ajax=nuevoAjax();
                    ajax.open("POST", pag ,true);
                    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                    ajax.send(param);
                    ajax.onreadystatechange=function()
                    {
                        if (ajax.readyState==4) 
                        {
                            return(true);
                        } // fin de if de ajax.readyState
                        
                    } //fin de ajax.onreadystatechange
                }
            }
            
                    
            function getCookieVal (offset) {
                var endstr = document.cookie.indexOf (";", offset);
                if (endstr == -1){
                    endstr = document.cookie.length;
                }
                return unescape(document.cookie.substring(offset, endstr));
            }
            
            function GetCookie (name) {
                var arg = name + "=";
                var alen = arg.length;
                var clen = document.cookie.length;
                var i = 0;
                while (i < clen) {
                    var j = i + alen;
                    if (document.cookie.substring(i, j) == arg)
                    return getCookieVal (j);
                    i = document.cookie.indexOf(" ", i) + 1;
                    if (i == 0)
                    break;
                }
                
                return '';
            }
            
            function crearReporteP(value)
            {
            if(value=='email'){	var page='Email/result.php'; var ide='emailtoolbar';}else{ var page='FPDF/result.php'; var ide='imprimirtoolbar';}
            var obj = document.getElementById('mytempla');
            var curtemplatoolbar = obj.options[obj.selectedIndex].value;
            var parcel=selectedChkP();
            if(parcel==false){
                return false;
            }
            else{
                document.getElementById(ide).href=""+page+"?t="+curtemplatoolbar+"&s="+GetCookie('sort')+"&n="+GetCookie('idname');
                //&parcel="+parcel;
                return true;
            }
            }
            
            function crearReporteE()
            {
                var obj = document.getElementById('mytempla');
                var curtemplatoolbar = obj.options[obj.selectedIndex].value;
                var parcel=selectedChk();
                if(parcel==false){
                    return false;
                }
                else{
                    document.getElementById('exceltoolbar').href="Excel/result.php?t="+curtemplatoolbar+"&s="+GetCookie('sort')+"&n="+GetCookie('idname')+"&parcel="+parcel;
                    return true;
                }
            }
			
			function crearReportePS(){
				var sta = document.getElementById('status').value;
				var sta2 = document.getElementById('status2').value;
				document.getElementById('imprimirtoolbar').href="FPDF/statistics.php?<?php echo('id='.$_GET['id']);?>&status="+sta+"&status2="+sta2;
				return true;
			}
			
			function crearReporteES(){
				var sta = document.getElementById('status').value;
				var sta2 = document.getElementById('status2').value;
				document.getElementById('exceltoolbar').href="Excel/statistics.php?id=<?php echo($_GET['id']);?>&status="+sta+"&status2="+sta2;
				
				return true;
			}
			
			
            
            function crearReportePU(value)
            {
            if(value=='email'){	var page='Email/user.php'; var ide='emailtoolbar';}else{ var page='FPDF/user.php'; var ide='imprimirtoolbar';}
            var obj = document.getElementById('mytempla');
            var curtemplatoolbar = obj.options[obj.selectedIndex].value;
            var parcel=selectedChkP();
            if(parcel==false){
                return false;
            }
            else{
                document.getElementById(ide).href=""+page+"?t="+curtemplatoolbar+"&s="+GetCookie('sort')+"&n="+GetCookie('idname');
                //&parcel="+parcel;
                return true;
            }
            
            }
            
            function crearReporteEU()
            {
                var obj = document.getElementById('mytempla');
                var curtemplatoolbar = obj.options[obj.selectedIndex].value;
                var parcel=selectedChk();
                if(parcel==false){
                    return false;
                }
                else{
                    document.getElementById('exceltoolbar').href="Excel/user.php?t="+curtemplatoolbar+"&s="+GetCookie('sort')+"&n="+GetCookie('idname')+"&parcel="+parcel;
                    return true;
                }
            
            }
            
            
            function calc_takenToolbar()
            {
                var parameters;
                //NEW
                //var param1='comparado="id"=>"'+datacompar["id"]+'","type"=>"COMP.SPL","which_mod"=>"MLS_SF"&';
                //var param2="comparables=";
                var ids="taken=";
                var i2=0;
                for (i=0;i<vFilas.length;i++)
                {
                    if (vFilas[i].taken==true)
                        {
                            if (i2>0){
                            //param2+="^";
                            ids+=",";
                            }
                            //param2+='"price_sqft"=>'+(vFilas[i].saleprice/vFilas[i].lsqft)+',"Distance"=>'+vFilas[i].distance;
                            ids+=vFilas[i].id;
                            i2++
                        }
                }
                
                return parameters=ids;  //param1+param2+ids+mv+mm+mp;
            }
            
            function crearReporteP0(value)
            {
                if(value=='email'){	var page='Email/comp.php'; var ide='emailtoolbar';}else{ var page='FPDF/comp.php'; var ide='imprimirtoolbar';}
                document.getElementById(ide).href=""+page+"?t=defa&s="+GetCookie('sort')+"&n="+GetCookie('idname')+"<?php if(isset($_GET['id'])) echo('&id='.$_GET['id']);?>&type="+typeComp;
                if(typeComp=='rental' || typeComp=='distress'){
                    var parcel=selectedChkP();
                    if(parcel==false){
                        return false;
                    }
                    else{
                        //document.getElementById(ide).href+="&"+searchrental;
                        return true;
                    }
                    
                }
                if(typeComp=='active' || typeComp=='comparable'){
                    document.getElementById(ide).href+="&"+calc_takenToolbar()+"&tabla_market=<?php echo($_GET["p"]);?>";	
                return true;
                }
            }
            
            function crearReporteE0()
            {
                
                document.getElementById('exceltoolbar').href="Excel/comp.php?t=defa&s="+GetCookie('sort')+"&n="+GetCookie('idname')+"<?php if(isset($_GET['id'])) echo('&id='.$_GET['id']);?>&type="+typeComp;
                if(typeComp=='distress'){
                    var parcel=selectedChk();
                    if(parcel==false){
                        return false;
                    }
                    else{
                        document.getElementById('exceltoolbar').href+="&parcel="+parcel;
                        return true;
                    }
                }
                if(typeComp=='rental'){
                    var parcel=selectedChk();
                    if(parcel==false){
                        return false;
                    }
                    else{
                        document.getElementById('exceltoolbar').href+="&"+searchrental+"&parcel="+parcel;
                        return true;
                    }
                }
                if(typeComp=='active' || typeComp=='comparable')
                    document.getElementById('exceltoolbar').href+="&"+calc_takenToolbar()+"&tabla_market=<?php echo($_GET["p"]);?>";	
                return true;
            }
            
        </script>
    <?php if($toolbar_print==0){?>
        <a id="imprimirtoolbar" title="Print Report" href="FPDF/result.php?t=defa" onclick="if(!crearReporteP()) return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/printer.png" />
        </a>
    <?php }elseif($toolbar_print==1){?>
        <a id="imprimirtoolbar" title="Print Report" href="FPDF/comp.php?t=defa" onclick="if(!crearReporteP0()) return false;" target="_blank" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/printer.png" />
        </a>
    <?php }elseif($toolbar_print==2){?>
        <a id="imprimirtoolbar" title="Print Report" href="FPDF/user.php?t=defa" onclick="if(!crearReportePU()) return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/printer.png" />
        </a>
    <?php }elseif($toolbar_print==4){?>
        <a id="imprimirtoolbar" title="Print Report" href="FPDF/statistics.php" onclick="if(!crearReportePS()) return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/printer.png" />
        </a>
    <?php }else{?>
        <a title="Print Report Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/printer_disable.png" />
        </a>
    <?php }?>
        <script>
        var typeComp;
            function crearLabel()
            {
                //if(typeComp=='undefined')	typeComp='';
                if(typeComp=='active' || typeComp=='comparable'){
                    document.getElementById('labeltoolbar').href+="?"+calc_takenToolbar()+"&type="+typeComp;	
                    return true;
                }
                else{
                    var parcel=selectedChkP();
                    if(parcel==false){
                        return false;
                    }
                    else{
						if(typeComp == undefined) typeComp ="result";
                        document.getElementById('labeltoolbar').href="FPDF/labels.php?type="+typeComp;
                        return true;
                    }
                }
            }
        </script>
    <!--Labels-->
    <?php if($toolbar_label){?>
        <a id="labeltoolbar" title="Print Labels" href="FPDF/labels.php" onclick="javascript: if(!crearLabel()) return false;" class="toolbarI" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/label.png" />
        </a>
    <?php }else{?>
        <a title="Print Labels Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/label_disable.png" />
        </a>
    <?php }?>
    <!--Email-->
    <?php if($toolbar_email==0){?>
        <a id="emailtoolbar" title="Email" href="EMAIL/result.php?t=defa" onclick="if(!crearReporteP('email')) return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/email.png" />
        </a>
    <?php }elseif($toolbar_email==1){?>
        <a id="emailtoolbar" title="Email" href="EMAIL/comp.php?t=defa" onclick="if(!crearReporteP0('email')) return false;" target="_blank" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/email.png" />
        </a>
    <?php }elseif($toolbar_email==2){?>
        <a id="emailtoolbar" title="Email" href="EMAIL/user.php?t=defa" onclick="if(!crearReportePU('email')) return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/email.png" />
        </a>
    <?php }else{?>
        <a title="Email Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/email_disable.png" />
        </a>
    <?php }?>
    <!--Excel-->
    <?php if($toolbar_excel==0){?>
        <a id="exceltoolbar" title="Excel Report" href="javascript:void(0);" onclick="selectedChkE(); return false;" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/Excel.png" />
        </a>
    <?php }elseif($toolbar_excel==1){?>
        <a id="exceltoolbar" title="Excel Report" href="Excel/comp.php?t=defa" onclick="if(crearReporteE0()){ window.open(this.href, this.target, 'width=300,height=300');} return false;" target="_blank" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/Excel.png" />
        </a>
    <?php }elseif($toolbar_excel==2){?>
        <a id="exceltoolbar" title="Excel Report" href="Excel/user.php?t=defa" onclick="if(crearReporteEU()){ window.open(this.href, this.target, 'width=300,height=300');} return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/Excel.png" />
        </a>
    <?php }elseif($toolbar_excel==4){?>
        <a id="exceltoolbar" title="Excel Report" href="javascript:void(0);" onclick="if(crearReporteES()){ window.open(this.href, this.target, 'width=300,height=300');} return false;" target="_blank" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/Excel.png" />
        </a>
    <?php }else{?>
        <a title="Excel Report Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/excel_disable.png" />
        </a>
    <?php }?>
    <!--Filter-->
    <?php if($toolbar_filter){?>
        <a title="Show Filter" href="javascript:_callFilter();" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/filter.png" />
        </a>
    <?php }else{?>
        <a title="Show Filter Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/filter_disable.png" />
        </a>
    <?php }?>
    <!--Trash-->
    <?php if($toolbar_trash){?>
        <a title="Trash" href="javascript:Xcheked();" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/trash.png" />
        </a>
    <?php }else{?>
        <a title="Trash Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/trash_disable.png" />
        </a>
    <?php }?>
    <!-- Graph-->
    <?php if($toolbar_graph){?>
        <a title="Graph" href="javascript:generarGrafico();" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/graph.png" />
        </a>
    <?php }else{?>
        <a title="Graph Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>> 
        <img align="absmiddle" src="img/toolbar/graph_disable.png" />
        </a>
    <?php }?>
    <!--Users-->
        <script>
            
            
            function user(result){
                var resultado=true;
                var us='';
                var i2=0;
                var tmp='';
                for (var k in multiSel)
                {
                    if (multiSel[k] == true)
                    {
                        if (i2>0) us+=",";
                        us+= k.replace("chk", "")
                        i2++	
                    }
                }
                
                if(i2!=0){
                    if(i2>10){
                         resultado= confirm('Please confirm you want to include all the selected records in your personal file.?');
                    }
                    if(resultado){
                        var ajax=nuevoAjax();
                        ajax.open("POST", "master_user.php",true);
                        ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                        ajax.send("usr_sel="+us+"&_action=1&tm="+new Date().getTime());
                            
                        ajax.onreadystatechange=function()
                        {
                            if (ajax.readyState==4) 
                            {
                                alert('The selected recods have been sucessfully saved in your personal file.');
                            } // fin de if de ajax.readyState
                            

                        } //fin de ajax.onreadystatechang
                    }
                }
            }
            
            function shortSale(){
                if(currentSel==''){
                    alert('You must select a property, clicking on it, for this action to work.');
                    document.getElementById('ssaletoolbar').href = "javascript:void(0);";
                }
                else{
                    var res = window.showModalDialog('templates/popup.html','Short Sale Report','scroll:no;center:yes;dialogWidth:310px;dialogHeight:100px');
                    if(res==0){
                        document.getElementById('ssaletoolbar').href = "sscomp.php?pid="+currentSel;
                    }
                    if(res==1){
                        document.getElementById('ssaletoolbar').href = "ss_print.php?pid="+currentSel+'&mode=normal';
                    }
                    
                }
            }
        </script>
    <?php if($toolbar_user){?>
        <a title="User" href="javascript:user(selectedChk());" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/user.png" />
        </a>
    <?php }else{?>
        <a title="User Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/user_disable.png" />
        </a>
    <?php }?>
    <!--Short Sale-->
    <?php if($toolbar_ssale){?>
        <a id="ssaletoolbar" title="Property Analysis Report" href="javascript:void(0);" onclick="javascript:shortSale();" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/ssale.png" />
        </a>
    <?php }else{?>
        <a title="Property Analysis Report Disable" href="javascript:void(0);" class="toolbarI" <?php echo($pad);?>>
        <img align="absmiddle" src="img/toolbar/ssale_disable.png" />
        </a>
    <?php }?>
</div>