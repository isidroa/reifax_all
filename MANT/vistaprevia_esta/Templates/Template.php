<?php 
	//Version 6 Virtual Earth
	$src_map_link='http://dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6';
	
	function TemplateHeader(){?>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta http-equiv="imagetoolbar" content="no">
		<meta name="robots" content="all" />
		<meta name="title" content="XIMA Inc. - Realtor Services" />
		<meta name="description" content="XIMA is a Real Estate listing search web-based system" />
		<meta name="keywords" content="listings, pre-foreclosure, foreclosure,  real estate, short sales, corporate own, bargain, xima, gps, map, search, broward, dade, palm beach, miami, florida, properties, mls, condos, single family, search, mortgage, comparables, active" />
		<meta name="category" content="Real Estate Search" />
		<meta name="author" content="Francisco Mago" />
		<meta name="reply-to" content="info@ximausa.com" />
		<meta name="copyright" content="XIMA, LLC - 2007" />
		<meta name="rating" content="General" />
		<meta name="generator" content="XIMA, LLC" />  
		<link rel="shortcut icon" type="image/ico" href="http://www.ximausa.com/favicon.ico">
		<link href="includes/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="includes/css/searchstyles.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="includes/js/mostrardiv.js"></script>
		<script type="text/javascript" src="includes/botones.js"></script> 
	<?php }
	
	function TemplateHeader2(){?>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
		<link rel="shortcut icon" type="image/ico" href="http://www.ximausa.com/favicon.ico">
		<link href="includes/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="includes/css/searchstyles.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="includes/js/mostrardiv.js"></script>
        <script type="text/javascript" src="includes/botones.js"></script>
	<?php }
	
	function TemplateBody( $toolbar_map = 0, $toolbar_gps = false, $toolbar_legend = false, $toolbar_print = false,  $toolbar_label = false,  $toolbar_email = false, $toolbar_excel = false, $toolbar_filter = false, $toolbar_trash = false, $toolbar_graph = false, $toolbar_user = false, $toolbar_ssale = false){ ?>

<div id="maincontainer">
  <div id="header">
  	
    <div id="menu">
    <?php if(intval($_SESSION['datos_usr']['PRIVILEGE'])!=99){?>
  	  <ul id="menuppal" class="visible">
  	    <li><a href="pssfsearch.php">P. Records</a></li>
        <li><a href="pensfsearch.php">Foreclosures</a></li> 
        <li><a href="mlsfsearch.php">Listings</a></li>
        <li><a class="logoff" href="index.php"> <img src="img/logoffonada.gif" alt="Logoff" title="Logoff"> </a></li>
      </ul>
      <ul id="mls" class="invisible">
  	    <li id="mlssfamily" class="activo"><a href="mlsfsearch.php">S. Family</a></li>
		<li id="mlscondos" class=""><a href="mlsconsearch.php">Condos</a></li>
        <li><a href="pagmenu1.php" >&lt;&lt; Main Menu</a></li>
        <li id="mlstitle">Listings</li>
        <li><a class="logoff" href="index.php"> <img src="img/logoffonada.gif" alt="Logoff" title="Logoff"> </a></li>        
      </ul>
      
      <ul id="pendes1" class="invisible">
  	    <li id="pensfamily" class="activo"><a href="pensfsearch.php">S. Family</a></li>
		<li id="pencondos" class=""><a href="penconsearch.php">Condos</a></li>
        <li><a href="pagmenu1.php" >&lt;&lt; Main Menu</a></li>
        <li id="pentitle">Foreclosures</li>
        <li><a class="logoff" href="index.php"> <img src="img/logoffonada.gif" alt="Logoff" title="Logoff"> </a></li>
      </ul>
      
      <ul id="psummary" class="invisible">
  	    <li id="psummarysfamily" class="activo"><a href="javascript:direccion('pssfsearch.php')" >S. Family</a></li>
		<li id="psummarycondos" class=""><a href="javascript:direccion('psconsearch.php')" >Condos</a></li>
        <li><a href="pagmenu1.php" >&lt;&lt; Main Menu</a></li>
        <li id="pstitle">P. Records</li>
        <li><a class="logoff" href="index.php"> <img src="img/logoffonada.gif" alt="Logoff" title="Logoff"> </a></li>
      </ul>
      
      <ul id="advanced" class="invisible">
  	    <li id="ssale" class="activo"><a href="javascript:void(0);" >P. A. Report</a></li>
        <li><a href="pagmenu1.php" >&lt;&lt; Main Menu</a></li>
        <li id="pstitle">Tools</li>
        <li><a class="logoff" href="index.php"> <img src="img/logoffonada.gif" alt="Logoff" title="Logoff"> </a></li>
      </ul>
      
      <?php }else{
	  		if(strcmp($_SESSION['current_MOD'],'PS_SF')==0){
	  ?>
      <ul id="psummary" class="visible">
      	<li id="psummarysfamily" class="activo"><a href="javascript:void(0)" >S. Family</a></li>
		<li id="psummarycondos" class="desactivo"><a href="javascript:void(0)" >Condos</a></li>
        <li><a href="index.php" >&lt;&lt; Main Menu</a></li>
        <li id="pstitle">P. Records</li>
      </ul>
      <?php }else{?>
      <ul id="psummary" class="visible">
      	<li id="psummarysfamily" class="desactivo"><a href="javascript:void(0)" >S. Family</a></li>
		<li id="psummarycondos" class="activo"><a href="javascript:void(0)" >Condos</a></li>
        <li><a href="index.php" >&lt;&lt; Main Menu</a></li>
        <li id="pstitle">P. Records</li>
      </ul>
      <?php }}?>
    </div>
    
  </div>
  <div id="submenuCompleto">
   <?php if(intval($_SESSION['datos_usr']['PRIVILEGE'])!=99){?>
  	<div id="submenumls" class="invisible">
  	 <ul id="mlssf" class="visible">
  	    <li id="mlssfsearch" class="activo"><a href="javascript:direccion('mlsfsearch.php')">Search</a></li>
		<li id="mlssfresult" class=""><a href="javascript: direccion('mlsfresult.php')">Results</a></li>
		<li id="mlssfcomp" class=""><a href="javascript: direccion('mlsfcomp.php')" >Comparables</a></li>
		<li id="mlssfcompact" class=""><a href="javascript: direccion('mlsfcompact.php')">Comp. Active</a></li>
        <li id="mlssfdistress" class=""><a href="javascript: direccion('mlsfdistress.php')">Distressed</a></li>
        <li id="mlssfcomprent" class=""><a href="javascript: direccion('mlsfcomprent.php')">Comp. Rental</a></li>
        <li id="mlssfstat" class=""><a href="javascript: direccion('mlsfcomest.php')">Statistics</a></li>
        <li id="mlssfuser" class=""><a href="javascript: direccion ('mlsfcompuser.php')" >User</a></li>
      </ul>
      
      <ul id="mlscon" class="invisible">
 		<li id="mlsconsearch" class="activo"><a href="javascript:direccion('mlsconsearch.php')">Search</a></li>
		<li id="mlsconresult" class=""><a href="javascript: direccion('mlsconresult.php')">Results</a></li>
		<li id="mlsconcomp" class=""><a href="javascript: direccion('mlsconcomp.php')" >Comparables</a></li>
		<li id="mlsconcompact" class=""><a href="javascript: direccion('mlsconcompact.php')">Comp. Active</a></li>
        <li id="mlscondistress" class=""><a href="javascript: direccion('mlscondistress.php')">Distressed</a></li>
        <li id="mlsconcomprent" class=""><a href="javascript: direccion('mlsconcomprent.php')">Comp. Rental</a></li>
        <li id="mlsconstat" class=""><a href="javascript: direccion('mlsconcomest.php')">Statistics</a></li>
        <li id="mlsconuser" class=""><a href="javascript: direccion('mlsconcompuser.php')" >User</a></li>
      </ul>
 	</div>

	
 	<div id="submenupen" class="invisible">
  	 <ul id="pensf" class="visible">
  	    <li id="pensfsearch" class="activo"><a href="javascript:direccion('pensfsearch.php')">Search</a></li>
		<li id="pensfresult" class=""><a href="javascript: direccion('pensfresult.php')">Results</a></li>
		<li id="pensfcomp" class=""><a href="javascript: direccion('pensfcomp.php')" >Comparables</a></li>
        <li id="pensfcompact" class=""><a href="javascript: direccion('pensfcompact.php')">Comp. Active</a></li>
        <li id="pensfdistress" class=""><a href="javascript: direccion('pensfdistress.php')">Distressed</a></li>
        <li id="pensfcomprent" class=""><a href="javascript: direccion('pensfcomprent.php')">Comp. Rental</a></li>
        <li id="pensfstat" class=""><a href="javascript: direccion('pensfcomest.php')">Statistics</a></li>
      </ul>
      <ul id="pencon" class="invisible">
 		<li id="penconsearch" class="activo"><a href="javascript:direccion('penconsearch.php')">Search</a></li>
		<li id="penconresult" class=""><a href="javascript: direccion('penconresult.php')">Results</a></li>
		<li id="penconcomp" class=""><a href="javascript: direccion('penconcomp.php')" >Comparables</a></li>
        <li id="penconcompact" class=""><a href="javascript: direccion('penconcompact.php')">Comp. Active</a></li>
        <li id="pencondistress" class=""><a href="javascript: direccion('pencondistress.php')">Distressed</a></li>
        <li id="penconcomprent" class=""><a href="javascript: direccion('penconcomprent.php')">Comp. Rental</a></li>
        <li id="penconstat" class=""><a href="javascript: direccion('penconcomest.php')">Statistics</a></li>
      </ul>
 	</div>

        
  	<div id="submenupsummary" class="invisible">
  	 <ul id="pssf" class="visible">
      	<li id="pssfsearch" class="activo"><a href="javascript:direccion('pssfsearch.php')">Search</a></li>
		<li id="pssfresult" class=""><a href="javascript: direccion('pssfresult.php')">Results</a></li>
		<li id="pssfcomp" class=""><a href="javascript: direccion('pssfcomp.php')" >Comparables</a></li>
        <li id="pssfcompact" class=""><a href="javascript: direccion('pssfcompact.php')">Comp. Active</a></li>
        <li id="pssfdistress" class=""><a href="javascript: direccion('pssfdistress.php')">Distressed</a></li>
        <li id="pssfcomprent" class=""><a href="javascript: direccion('pssfcomprent.php')">Comp. Rental</a></li>
        <li id="pssfstat" class=""><a href="javascript: direccion('pssfcomest.php')">Statistics</a></li>
      </ul>
      <ul id="pscon" class="invisible">
       	<li id="psconsearch" class="activo"><a href="javascript:direccion('psconsearch.php')">Search</a></li>
		<li id="psconresult" class=""><a href="javascript: direccion('psconresult.php')">Results</a></li>
		<li id="psconcomp" class=""><a href="javascript: direccion('psconcomp.php')" >Comparables</a></li>
        <li id="psconcompact" class=""><a href="javascript: direccion('psconcompact.php')">Comp. Active</a></li>
        <li id="pscondistress" class=""><a href="javascript: direccion('pscondistress.php')">Distressed</a></li>
        <li id="psconcomprent" class=""><a href="javascript: direccion('psconcomprent.php')">Comp. Rental</a></li>
        <li id="psconstat" class=""><a href="javascript: direccion('psconcomest.php')">Statistics</a></li>
      </ul>
    </div>
    
    <div id="submenuadvanced" class="invisible">
  	 <ul id="ssale" class="visible">
      	<li id="sssearch" class="activo"><a href="javascript:void(0)">Search</a></li>
		<li id="ssresult" class=""><a href="javascript:void(0)">Results</a></li>
		<li id="sscomp" class=""><a href="javascript: direccion('sscomp.php')" >Comparables</a></li>
        <li id="sscompact" class=""><a href="javascript: direccion('sscompact.php')">Comp. Active</a></li>
        <li id="ssdistress" class=""><a href="javascript: direccion('ssdistress.php')">Distressed</a></li>
        <li id="ss1mile" class=""><a href="javascript: direccion('ss1mile.php')">1 Mile</a></li>
      </ul>
    </div>
    <?php }else{?>
    <div id="submenupsummary" class="visible">
  	 <ul id="pssf" class="<?php if(strcmp($_SESSION['current_MOD'],'PS_SF')==0) echo('visible'); else echo('invisible');?>">
      	<li id="pssfsearch" class="desactivo"><a href="javascript:void(0)">Search</a></li>
		<li id="pssfresult" class="activo"><a href="javascript: direccion('pssfresult.php')">Results</a></li>
		<li id="pssfcomp" class=""><a href="javascript: direccion('pssfcomp.php')" >Comparables</a></li>
        <li id="pssfcompact" class="desactivo"><a href="javascript: void(0)">Comp. Active</a></li>
        <li id="pssfdistress" class="desactivo"><a href="javascript: direccion('pssfdistress.php')">Distressed</a></li>
        <li id="pssfcomprent" class="desactivo"><a href="javascript: void(0)">Comp. Rental</a></li>
        <li id="pssfstat" class="desactivo"><a href="javascript: void(0)">Statistics</a></li>
      </ul>
      <ul id="pscon" class="<?php if(strcmp($_SESSION['current_MOD'],'PS_SF')==0) echo('invisible'); else echo('visible');?>">
      	<li id="psconsearch" class="desactivo"><a href="javascript:void(0)">Search</a></li>
		<li id="psconresult" class="activo"><a href="javascript: direccion('psconresult.php')">Results</a></li>
		<li id="psconcomp" class=""><a href="javascript: direccion('psconcomp.php')" >Comparables</a></li>
        <li id="psconcompact" class="desactivo"><a href="javascript: void(0)">Comp. Active</a></li>
        <li id="pscondistress" class="desactivo"><a href="javascript: direccion('pscondistress.php')">Distressed</a></li>
        <li id="psconcomprent" class="desactivo"><a href="javascript: void(0)">Comp. Rental</a></li>
        <li id="psconstat" class="desactivo"><a href="javascript: void(0)">Statistics</a></li>
      </ul>
    </div>
    <?php } ?>

  </div>
  
  <div id="maincontent">
  <?php //require('Templates/toolbar.php');

  } 
  
function TemplateBodyMyAccount(){ ?>

<div id="maincontainer">
  <div id="header">
  	
    <div id="menu">
  	  <ul id="menuppal" class="visible">
  	    <li id="eac" class="activo"><a href="javascript:void(0);" onclick="document.getElementById('eac').className='activo';document.getElementById('mya').className='';document.getElementById('pssf').className='visible';document.getElementById('pscon').className='invisible';show('email',emailuser);document.getElementById('pssfresult').className='';document.getElementById('pssfcomp').className='';document.getElementById('pssfcompact').className='';document.getElementById('pssfsearch').className='activo';">Edit Account</a></li>
        <li id="mya" class=""><a href="javascript:void(0);"  onclick="document.getElementById('eac').className='';document.getElementById('mya').className='activo';document.getElementById('pssf').className='invisible';document.getElementById('pscon').className='visible';show('MyAffiliates',emailuser);">My Affiliates</a></li> 
        <li class=""><a href="pagmenu1.php">&lt;&lt; Main Menu</a></li>
      </ul>
    </div>
    
  </div>
  <div id="submenuCompleto">    
    <div id="submenupsummary" class="visible">
  	  <ul id="pssf" class="visible">
      	<li id="pssfsearch" class="activo"><a href="javascript:void(0)" onClick="show('email',emailuser);document.getElementById('pssfresult').className='';document.getElementById('pssfcomp').className='';document.getElementById('pssfcompact').className='';document.getElementById('pssfsearch').className='activo';" >Chg Email</a></li>
		<li id="pssfresult" class=""><a href="javascript:void(0)" onClick="show('password',emailuser);document.getElementById('pssfresult').className='activo';document.getElementById('pssfcomp').className='';document.getElementById('pssfcompact').className='';document.getElementById('pssfsearch').className='';">Chg Password</a></li>
		<li id="pssfcomp" class=""><a href="javascript: direccion('sscomp.php')" onClick="show('nickname',emailuser);document.getElementById('pssfresult').className='';document.getElementById('pssfcomp').className='activo';document.getElementById('pssfcompact').className='';document.getElementById('pssfsearch').className='';">Chg Nickname</a></li>
        <li id="pssfcompact" class=""><a href="javascript: direccion('sscompact.php')" onClick="show('cancel',emailuser);document.getElementById('pssfresult').className='';document.getElementById('pssfcomp').className='';document.getElementById('pssfcompact').className='activo';document.getElementById('pssfsearch').className='';">Cancel Account</a></li>
      </ul>
      <ul id="pscon" class="invisible">
      	<li id="psconsearch" class="activo"><a href="javascript:void(0)" onClick="show('MyAffiliates',emailuser);document.getElementById('psconresult').className='';document.getElementById('psconsearch').className='activo';">My Affiliates</a></li>
        <li id="psconresult" class=""><a href="javascript:void(0)" onClick="show('MyBanner',emailuser);document.getElementById('psconresult').className='activo';document.getElementById('psconsearch').className='';">My Banner</a></li>
      </ul>
    </div>
</div>
  
  <div id="maincontent">

<?php }

function TemplateBody2(){?>
<div id="maincontainer">
  <div id="headerlg">
  	<div id="menu">
  	  <ul>
	<!--	<li><a href="#">Contact Us</a></li>-->
      </ul>
    </div>
<!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
<div id="ciSlBh" style="z-index:100; "></div><div id="scSlBh" style="display:inline"></div><div id="sdSlBh" style="display:none"></div><script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
<!-- END ProvideSupport.com Graphics Chat Button Code -->
  </div>
  <div id="maincontent">
<?php }

function TemplatePie(){ ?>
  </div>
  
    <div id="piepag">
      <div align="center"><a href="pagmenu1.php">home</a> - <a href="aboutus.php">about us</a><?php if($_SESSION['datos_usr']['EMAIL']!='guest'){?> - <a href="myaccount.php">my account1</a><?php }?> - <a href="javascript:void(0);" onclick="window.open('http://messenger.providesupport.com/messenger/ximausa.html?ps_s=J1UQz3QrUhDd&ps_mht=true','','width=500px, height=450px')">support</a> - <a href="helpindex.php">tutorial</a> - <a href="contactus.php">contact us</a></div>
  </div>
</div>

<?php }

function TemplatePie2(){ ?>
	</div>
    <div id="piepag">
      <div align="center"><a href="<?php if($_SESSION['datos_usr']['EMAIL']!='guest'){?>pagmenu1.php<?php }else{?>index.php<?php }?>">home</a> - <a href="aboutus.php">about us</a><?php if($_SESSION['datos_usr']['EMAIL']!='guest'){?> - <a href="myaccount.php">my account2</a><?php }?> - <a href="javascript:void(0);" onclick="window.open('http://messenger.providesupport.com/messenger/ximausa.html?ps_s=J1UQz3QrUhDd&ps_mht=true','','width=500px, height=450px')">support</a> - <a href="helpindex.php">tutorial</a> - <a href="contactus.php">contact us</a></div>
  </div>
</div>

<?php } 

function TemplatePieSinMenu(){ ?>
	</div>
  
    <div id="piepag">
      <div align="center">&nbsp;</div>
  </div>
</div>
<?php } 

function TemplatePie3(){ ?>
	</div>
  
    <div id="piepag">
      <div align="center"><a href="index.php">home</a> - <a href="aboutus.php">about us</a> - <a href="javascript:psSlBhow();">support</a> - <a href="helpindex.php?indice_help=1">tutorial</a> - <a href="contactus.php">contact us</a></div>
  </div>
</div>
<?php } ?>
	