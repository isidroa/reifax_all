<?php  
	$starttime=tiempo();
/****************************************************/ 
	$table="psummaryiris";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `$table` (
  `Parcelid` varchar(20) character set latin1 NOT NULL default '0' COMMENT 'Codigo de identificacion de la Parcela',
  `Sparcel` varchar(4) character set latin1,
  `Ocode` varchar(4) character set latin1 NOT NULL default '0' COMMENT 'Codigo de identificacion SF condo de Xima',
  `CCode` varchar(4) character set latin1 NOT NULL default '0' COMMENT 'Codigo del tipo de propiedad',
  `Address` varchar(68) character set latin1 default NULL COMMENT 'Direccion',
  `Ac` varchar(2) character set latin1 default NULL COMMENT 'Tipo de aire acondicionado',
  `AssesedV` double(12,2) default '0.00' COMMENT 'Valor de apreciacion',
  `Astatus` varchar(2) character set latin1 default 'CC' COMMENT 'Status A en el mlsresidential',
  `Bath` double(4,1) default '0.0' COMMENT 'Cantidad de Ba�os',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `Beffective` mediumint(7) default '0' COMMENT 'Efective sqft',
  `Bheated` smallint(6) default '0',
  `BuildingV` double(12,2) default '0.00' COMMENT 'Valor del building',
  `CCodeD` varchar(40) character set latin1 default NULL COMMENT 'Descripcion del tipo de propiedad',
  `City` varchar(23) character set latin1 default NULL COMMENT 'Ciudad de la propiedad',
  `Dir` varchar(6) character set latin1 default NULL COMMENT 'Direccion',
  `Display` varchar(2) character set latin1 default NULL,
  `Folio` varchar(20) character set latin1 default NULL COMMENT 'Codigo de identificacion de la Parcela',
  `Homstead` double(12,2) default '0.00' COMMENT 'Epxension de',
  `LandV` double(12,2) default '0.00' COMMENT 'Valor de la tierra',
  `Legal` varchar(510) character set latin1 default NULL COMMENT 'Descripcion legal',
  `Lunit_type` varchar(6) character set latin1 default NULL,
  `Lunits` double(12,2) default '0.00',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida',
  `Owner` varchar(60) character set latin1 default NULL COMMENT 'Due�o',
  `Owner_A` varchar(50) character set latin1 default NULL COMMENT 'Direccion del due�o',
  `Owner_C` varchar(28) character set latin1 default NULL COMMENT 'Ciudad del due�o',
  `Owner_P` varchar(24) character set latin1 default NULL COMMENT 'Pais del due�o',
  `Owner_S` varchar(10) character set latin1 default NULL COMMENT 'Estado del due�o',
  `Owner_Z` varchar(20) character set latin1 default NULL COMMENT 'Zip del due�o',
  `OZip` varchar(5) character set latin1 default NULL COMMENT 'Zip del due�o substr(zip,1,5)',
  `Pool` varchar(1) character set latin1 default 'N' COMMENT 'Si tiene o no piscina',
  `PostDir` varchar(3) character set latin1 default NULL,
  `Rng` varchar(3) character set latin1 default NULL,
  `SaleDate` varchar(8) character set latin1 default NULL COMMENT 'Fecha de Venta',
  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
  `SDName` varchar(50) character set latin1 default NULL COMMENT 'Nombre de la subdivision',
  `Sec` varchar(4) character set latin1 default NULL,
  `SFP` double(12,2) default '0.00' COMMENT 'Precio por pie cuadrado',
  `Stories` tinyint(2) default '0' COMMENT 'Pisos de la propiedad',
  `Street` varchar(30) character set latin1 default NULL COMMENT 'Calle',
  `STType` varchar(6) character set latin1 default NULL COMMENT 'Tipo de Calle',
  `Stno` varchar(15) character set latin1 default NULL COMMENT 'Numero en la Calle',
  `TaxableV` double(12,2) default '0.00' COMMENT 'Valor de taxacion',
  `TSqft` int(12) default '0' COMMENT 'Area total de la propiedad',
  `Twn` varchar(4) character set latin1 default NULL,
  `Unit` varchar(7) character set latin1 default NULL COMMENT 'Numero de la propiedad',
  `Units` int(10) default '0',
  `WaterF` varchar(1) character set latin1 default 'N' COMMENT 'Si tiene o no agua colindante con la propiedad',
  `YrBuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
  `Zip` varchar(10) character set latin1 default NULL COMMENT 'Codigo Postal de la propiedad',
  KEY `Sparcel` USING BTREE (`Sparcel`),
  KEY `CCode` USING BTREE (`CCode`),
  KEY `Parcelid` (`Parcelid`),
  KEY `OZIP` USING BTREE (`OZip`),
  KEY `SalePrice` USING BTREE (`SalePrice`),
  KEY `SaleDate` USING BTREE (`SaleDate`)
)";
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/ 
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>