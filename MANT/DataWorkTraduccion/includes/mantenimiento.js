function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var inputs= document.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) 
		if (inputs[i].type == 'checkbox') 
			inputs[i].checked=false;;
}

function checkSeleccion(field, i) 
{
	if (i == 0) //Todos
	{ 
		if (field[0].checked == true) 
			for (i = 1; i < field.length; i++) field[i].checked = true;
		else
			for (i = 1; i < field.length; i++) field[i].checked = false;
	}
}

function buscarParametros()
{
	var field,i;
	var ret="";
	var ruta,texto;

	field=document.formulario.ckbtranferMlxMysql;
	ruta="transferData/";
	texto="Transferencia";
	for (i=0; i<field.length;i++)
	{
		if(field[i].checked==true && field[i].id!="*")
			ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
	}
	
	field=document.formulario.ckbtranferMlxMysql1;
	ruta="traduccion/";
	texto="Traduccion de las Tablas";
	for (i=0; i<field.length;i++)
	{
		if(field[i].checked==true && field[i].id!="*")
			ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
	}

	field=document.formulario.ckbCreaTblTrad;
	ruta="createtables/";
	texto="Creates de las Tablas";
	for (i=0; i<field.length;i++)
	{
		if(field[i].checked==true && field[i].id!="*")
			ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
	}

	if(ret=="") return "";
	else return "&parametros="+ret;
}

function ejecutarPHP()
{	
	var i,tiempo;
	var bd=document.getElementById("cbd").value;
/*	var field=document.formulario.rbTiempo;
	for (i=0; i<field.length;i++)if(field[i].checked==true)	tiempo=field[i].value;
*/	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd==""){alert("Debes seleccionar Base de Datos");return;}
//	if(tiempo==null || tiempo==""){alert("Debes seleccionar tiempo de ejecucion (Diario,Semanal)");return;}

	parametros=buscarParametros();
	if(parametros==null || parametros==""){alert("Debes seleccionar al menos una opcion a Ejecutar");return;}

	document.getElementById("divResultados").innerHTML='';
	document.getElementById("imgfilResultados").src="includes/up.png";
	document.getElementById("imgfilResultados").alt="Ocultar Resultados";
	document.getElementById("reloj").style.visibility="visible";
//alert("bd="+ bd + "&" + "tiempo="+ tiempo + parametros);return;
	//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=true;
	var ajax=nuevoAjax();
	ajax.open("POST", "mantenimiento_respuesta.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("bd="+ bd + parametros);
//	ajax.send("bd="+ bd + "&" + "tiempo="+ tiempo + parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			htmlTag.push(ajax.responseText);
			document.getElementById("divResultados").innerHTML=htmlTag.join('');
			document.getElementById("filResultados").style.display="inline";
			document.getElementById("reloj").style.visibility="hidden";
			//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=false;
			limpiarchecks();
		}
	}
}

