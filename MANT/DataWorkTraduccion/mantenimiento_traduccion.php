<html>
<head>
<title>Mantenimiento de Tablas Traducción</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO DE TABLAS: TRADUCCI&Oacute;N </th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
		  <th width="190" >BD: 
				<select name="cbd" id="cbd">
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
				</select>
<!--                <br>
                  <input type="radio" name="rbTiempo" value="diario">Diario
                  <input type="radio" name="rbTiempo" value="semanal">Semanal
-->			</th>
		</tr>

<!-- 1.) Tabla TRANSFERENCIA DE MLX(TXT) A MYSQL-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('tranferMlxMysql','imgtranferMlxMysql'); return false;">
    	<img id="imgtranferMlxMysql" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2" >TRADUCCION</th>
	<th width="190" ></th>
</tr>
  <tr  id="tranferMlxMysql" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">

<!-- 1.1) Transferencia -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowtranferMlxMysql','imgyellowtranferMlxMysql'); return false;">
				<img id="imgyellowtranferMlxMysql" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Transferencia</th>
			<th width="180"><input type="checkbox" name="ckbtranferMlxMysql"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbtranferMlxMysql, 0)">Todos</th>
		</tr>
   	    <tr  id="yellowtranferMlxMysql" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitetranferMlxMysql=array(
										array("tit"=>"MLSRESIDENTIAL","value"=>"mlxsfcon.php")
										,array("tit"=>"PENDES","value"=>"pendes.php")
										,array("tit"=>"PSUMMARY","value"=>"psummary.php")
							);
				for($i=0;$i<count($whitetranferMlxMysql);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitetranferMlxMysql[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbtranferMlxMysql"  id="<?php echo $whitetranferMlxMysql[$i]["value"] ?>" >
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		<td>
		</tr>

<!-- 1.2) Traducción de las Tablas -->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('yellowtranferMlxMysql1','imgyellowtranferMlxMysql1'); return false;">
				<img id="imgyellowtranferMlxMysql1" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Traducción de las Tablas</th>
			<th width="180"><input type="checkbox" name="ckbtranferMlxMysql1"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbtranferMlxMysql1, 0)">Todos</th>
		</tr>
   	    <tr  id="yellowtranferMlxMysql1" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$whitetranferMlxMysql1=array(
										array("tit"=>"MLSRESIDENTIAL","value"=>"mlsresidentialmlx.php")
										,array("tit"=>"PENDES","value"=>"pendes.php")
										,array("tit"=>"PSUMMARY","value"=>"psummary.php")
							);
				for($i=0;$i<count($whitetranferMlxMysql1);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $whitetranferMlxMysql1[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbtranferMlxMysql1"  id="<?php echo $whitetranferMlxMysql1[$i]["value"] ?>" >
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		<td>
		</tr>
	</table>	
  </td>
  </tr>

<!-- 2.) Create's de las Tablas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titCreaTblTrad','imgCreaTblTrad'); return false;">
    	<img id="imgCreaTblTrad" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Create's de las Tablas</th>
	<th width="190" ><input type="checkbox" name="ckbCreaTblTrad"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbCreaTblTrad, 0)">Todos</th>
</tr>
  <tr  id="titCreaTblTrad" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titCreaTblTrad=array(
							array("tit"=>"MLSRESIDENTIAL","value"=>"mlsresidential.php")
							,array("tit"=>"PENDES","value"=>"pendes.php")
							,array("tit"=>"PSUMMARY","value"=>"psummary.php")
				);
	for($i=0;$i<count($titCreaTblTrad);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titCreaTblTrad[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbCreaTblTrad" id="<?php echo $titCreaTblTrad[$i]["value"] ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->



<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
