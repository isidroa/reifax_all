function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function ocultar_mostrar(){
	if(document.getElementById("tipo_mantenimientod").checked==false){
		/*Actualizar Data Sincronizada*/
		document.getElementById("trUSinc").style.display = 'none';
		/*Hacer Backup de las Tablas*/
		document.getElementById("tablasBDiffvalue").style.display = '';
		document.getElementById("tablasBIm�genes").style.display = '';
		document.getElementById("tablasBPendes").style.display = '';
		document.getElementById("tablasBPsummary").style.display = '';
		document.getElementById("tablasBRental").style.display = '';
		/*Transferir la data a las Tablas*/
		document.getElementById("tablasTDiffvalue").style.display = '';
		document.getElementById("tablasTPendes").style.display = '';
		document.getElementById("tablasTPsummary").style.display = '';
		/*Ejecutar los php de Sincronizaci�n de Data (1/3)*/
		document.getElementById("tablasSinSincronizaMkvconMkpsPsumPendesY").style.display = '';
		/*Ejecutar los php de Sincronizaci&oacute;n de Data (3/3)*/
		document.getElementById("tablasSinSincronizarImagenesconMlnumber_NEW").style.display = '';
		/*Creando CSV a transferir de Data Mantenimiento-->Sistema*/
		document.getElementById("tablasCSVDiffvalue").style.display = '';
		document.getElementById("tablasCSVIm�genes").style.display = '';
		document.getElementById("tablasCSVPendes").style.display = '';
		document.getElementById("tablasCSVPsummary").style.display = '';
		document.getElementById("tablasCSVRental").style.display = '';
		/*Hacer Backup de las Tablas de Data del Sistema*/
		document.getElementById("tablasBFDiffvalue").style.display = '';
		document.getElementById("tablasBFIm�genes").style.display = '';
		document.getElementById("tablasBFPendes").style.display = '';
		document.getElementById("tablasBFPsummary").style.display = '';
		document.getElementById("tablasBFRental").style.display = '';
		/*Restore de los CSV de Data Mantenimiento-->Sistema*/
		document.getElementById("tablasRFDiffvalue").style.display = '';
		document.getElementById("tablasRFIm�genes").style.display = '';
		document.getElementById("tablasRFPendes").style.display = '';
		document.getElementById("tablasRFPsummary").style.display = '';
		document.getElementById("tablasRFRental").style.display = '';
		/*Validacion y confirmacion de data*/
		document.getElementById("tablasVquery.php").style.display = '';
		document.getElementById("tablasVPorcentajeCalculomarketvalue.php").style.display = '';
		document.getElementById("tablasVPorcentajeCalculomarketps.php").style.display = '';
	}else{
		/*Actualizar Data Sincronizada*/
		document.getElementById("trUSinc").style.display = '';
		/*Hacer Backup de las Tablas*/
		document.getElementById("tablasBDiffvalue").style.display = 'none';
		document.getElementById("tablasBIm�genes").style.display = 'none';
		document.getElementById("tablasBPendes").style.display = 'none';
		document.getElementById("tablasBPsummary").style.display = 'none';
		document.getElementById("tablasBRental").style.display = 'none';
		/*Transferir la data a las Tablas*/
		document.getElementById("tablasTDiffvalue").style.display = 'none';
		document.getElementById("tablasTPendes").style.display = 'none';
		document.getElementById("tablasTPsummary").style.display = 'none';
		/*Ejecutar los php de Sincronizaci�n de Data (1/3)*/
		document.getElementById("tablasSinSincronizaMkvconMkpsPsumPendesY").style.display = 'none';
		/*Ejecutar los php de Sincronizaci&oacute;n de Data (3/3)*/
		document.getElementById("tablasSinSincronizarImagenesconMlnumber_NEW").style.display = 'none';
		/*Creando CSV a transferir de Data Mantenimiento-->Sistema*/
		document.getElementById("tablasCSVDiffvalue").style.display = 'none';
		document.getElementById("tablasCSVIm�genes").style.display = 'none';
		document.getElementById("tablasCSVPendes").style.display = 'none';
		document.getElementById("tablasCSVPsummary").style.display = 'none';
		document.getElementById("tablasCSVRental").style.display = 'none';
		/*Hacer Backup de las Tablas de Data del Sistema*/
		document.getElementById("tablasBFDiffvalue").style.display = 'none';
		document.getElementById("tablasBFIm�genes").style.display = 'none';
		document.getElementById("tablasBFPendes").style.display = 'none';
		document.getElementById("tablasBFPsummary").style.display = 'none';
		document.getElementById("tablasBFRental").style.display = 'none';
		/*Restore de los CSV de Data Mantenimiento-->Sistema*/
		document.getElementById("tablasRFDiffvalue").style.display = 'none';
		document.getElementById("tablasRFIm�genes").style.display = 'none';
		document.getElementById("tablasRFPendes").style.display = 'none';
		document.getElementById("tablasRFPsummary").style.display = 'none';
		document.getElementById("tablasRFRental").style.display = 'none';
		/*Validacion y confirmacion de data*/
		document.getElementById("tablasVquery.php").style.display = 'none';
		document.getElementById("tablasVPorcentajeCalculomarketvalue.php").style.display = 'none';
		document.getElementById("tablasVPorcentajeCalculomarketps.php").style.display = 'none';
	}
}

function checkSeleccion(field, i) 
{
	if (i == 0) //Todos
	{ 
		if (field[0].checked == true) 
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = true;
	   	}
		else
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = false;
		}
	}
	else  // Seleccion de cualquiera
	{  
		if(field[0].id!="ejecTranfSinc")
			field[0].checked = false;

		if(field[0].id=="ejecTranfSinc")
			field[0].checked = true;
	}
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var field,i;
	

	field =document.formulario.ckbTranfSincCalcuRestore;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
}

function buscarParametros()
{
	var field,i;
	var ret="",datnew="";
	var ruta,texto;
	var cont=0;
	var swbd=0;
	var cad;

	field =document.formulario.ckbTranfSincCalcuRestore;
	ruta="backup/Dorestore/";
	texto="Restore de los CSV de Data Mantenimiento-->Sistema";
	cont=0;
	for (i=0; i<field.length;i++)
	{
		if(field[i].checked==true && field[i].id!="*")
		{
			/*if(field[i].id=='RestoreCsvToMysqlRental.php')
				ret+='"php"=>"'+field[i].id+'","ruta"=>"rental/","texto"=>"'+texto+'"^';
			else if(field[i].id=='RestoreCsvToMysqlMortgage.php')
				ret+='"php"=>"'+field[i].id+'","ruta"=>"mortgage/","texto"=>"'+texto+'"^';
			else*/
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
			cont++;
		}
	}

	if(cont>0)datnew="datanueva=dataservernewp&copypaste=0";

	if(ret=='') return '';
	return datnew+"&parametros="+ret;
}
function ValidarBD(bd)
{
	var letra=bd.substr(0,1);
	var i;
//SI BD ES DE MANTENIMINENTO, NINGUNO DE 
//ESTOS PHP DEBE ESTAR SELECCIONADO
	if(letra.toUpperCase()=='M') //MANTENIMIENTO
	{
		field =document.formulario.ckbTranfSincCalcuRestore;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 0;
				

	}
	else //SISTEMA
	{
		

	}
	return 2;
}

function ejecutarPHP()
{	
	var i;
	var bd=document.getElementById("cbd").value;
	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd=="")
		alert("Debes seleccionar Base de Datos");
	else
	{
		
		parametros=buscarParametros();
		if(parametros==null || parametros=="")
			alert("Debes seleccionar al menos una opcion a Ejecutar");
		else
		{
			ret=ValidarBD(bd);
			if(ret!=2)
			{
				if(ret==0)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Sistema.\n Debes seleccionar Base de Datos de Sistema");
				if(ret==1)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Mantenimiento.\n Debes seleccionar Base de Datos de Mantenimiento");
				return;
			}
			document.getElementById("divResultados").innerHTML='';
			document.getElementById("reloj").style.visibility="visible";
			if(document.getElementById("guardarAccess1").checked==true){
				var guardarAccess = document.getElementById("guardarAccess1").value;
				var fechaAccess = document.getElementById("fechaAccess1").value;
			}
			else
				var guardarAccess = document.getElementById("guardarAccess2").value;
				
			if(document.getElementById("tipo_mantenimientod").checked==true)
				var tipo_mantenimiento = document.getElementById("tipo_mantenimientod").value;
			else
				var tipo_mantenimiento = document.getElementById("tipo_mantenimientos").value;
				
			var ajax=nuevoAjax();
			ajax.open("POST", "mantenimiento_respuesta.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("bd="+bd+"&tipo_mantenimiento="+tipo_mantenimiento+"&fechaAccess="+fechaAccess+"&guardarAccess="+guardarAccess+"&"+parametros);
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					htmlTag.push(ajax.responseText);
					document.getElementById("divResultados").innerHTML=htmlTag.join('');
					document.getElementById("reloj").style.visibility="hidden";
					limpiarchecks();
				}
			}
		}//Al Menos un ckechboxes seleccionado
	}//IF Base de Datos
}

function checkSeleccionCSV(field){
	if(field[0].checked==true){
		for(i=0; i<field.length; i++)
			field[i].checked = true;
	}else{
		for(i=0; i<field.length; i++)
			field[i].checked = false;
	}
}

function parametrosCSV(field){
	var parametros='';
	for(i=0; i<field.length; i++){
		if(field[i].checked == true)
			parametros+=field[i].value+'*';
	}
	
	var ajax=nuevoAjax();
	ajax.open("POST", "crearCSVmantenimiento.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("para="+parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			eval(ajax.responseText);
		}
	}
}

function validarFecha(Cadena){
	
    var Fecha= new String(Cadena) 
	
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length)) 
  

    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
            alert('Fecha Incorrecta. A�o inv�lido')  
        return false  
    }  

    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        alert('Fecha Incorrecta. Mes inv�lido')  
        return false  
    }  

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        alert('Fecha Incorrecta. D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            alert('Fecha Incorrecta. D�a inv�lido')  
            return false  
        }  
    }  
 
  return true      
}