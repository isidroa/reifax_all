<html>
<head>
<title>Mantenimiento de Tablas</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO DE TABLAS</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
			<th width="190" >BD: 
				<select name="cbd">
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
					<option value="FLBROWARD" >FLBROWARD</option>
					<option value="FLDADE"    >FLDADE</option>
					<option value="FLPALMBEACH">FLPALMBEACH</option>
				</select> <br> SAVE: <input type="radio" name="guardarAccess" id="guardarAccess1" value=1>SI<input name="guardarAccess" type="radio" id="guardarAccess2" value=0 checked>NO
                <br>FECHA: <input name="fechaAccess" type="text" id="fechaAccess1" value="AAAA-MM-DD" size="12" maxlength="10" onFocus="javascript: this.select();" onBlur="javascript: if(document.getElementById('guardarAccess1').checked==true) validarFecha(this.value);">
			</th>
		</tr>

<!--Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titTransSinc','imgTransSinc'); return false;">
    	<img id="imgTransSinc" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2" >Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)</th>
	<th width="190" ><input type="checkbox" name="ckbTranfSinc"   id="ejecTranfSinc" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSinc, 0)">Todos</th>
</tr>
  <tr  id="titTransSinc" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">

		<!--Hacer Backup de las Tablas (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincBackup','imgTransSincBackup'); return false;">
				<img id="imgTransSincBackup" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Hacer Backup de las Tablas</th>
			<th width="180"></th>
		</tr>
   	    <tr  id="titTransSincBackup" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincBackup=array(
										array("tit"=>"Diffvalue","value"=>"BackupMysqlToCsvDiffvalue.php"),
										array("tit"=>"Imágenes","value"=>"BackupMysqlToCsvImagenes.php"),
										array("tit"=>"Latlong","value"=>"BackupMysqlToCsvLatlong.php"),
										array("tit"=>"Marketvalue","value"=>"BackupMysqlToCsvMarketvalue.php"),
										array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php"),
										array("tit"=>"Mlsresidential","value"=>"BackupMysqlToCsvMlsresidential.php"),
										array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
										array("tit"=>"Psummary","value"=>"BackupMysqlToCsvPsummary.php"),
										array("tit"=>"Rental","value"=>"BackupMysqlToCsvRental.php"),
										array("tit"=>"Rtmaster","value"=>"BackupMysqlToCsvRtmaster.php")
							);
				for($i=0;$i<count($titTransSincBackup);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincBackup[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc"  id="<?php echo $titTransSincBackup[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Pasar los CSV de access1 a las Tablas (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincAcc1Msq','imgTransSincAcc1Msq'); return false;">
				<img id="imgTransSincAcc1Msq" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Pasar los CSV de access1 a las Tablas</th>
		</tr>
   	    <tr  id="titTransSincAcc1Msq" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincAcc1Msq=array(
										array("tit"=>"Diffvalue","value"=>"TransferAccess1ToCsvDiffvalue.php"),
										array("tit"=>"Mlsresidential","value"=>"TransferAccess1ToCsvMlsResidential.php"),
										array("tit"=>"Pendes","value"=>"TransferAccess1ToCsvPendes.php"),
										array("tit"=>"Psummary","value"=>"TransferAccess1ToCsvPsummary.php"),
										array("tit"=>"Rtmaster","value"=>"TransferAccess1ToCsvRtmaster.php")
							);
				for($i=0;$i<count($titTransSincAcc1Msq);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincAcc1Msq[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincAcc1Msq[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+1) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Ejecutar los php de Sincronización de Data (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="3">Ejecutar los php de Sincronización de Data</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincSincroniza=array(
											 array("tit"=>"SincronizaLatlongconMlsPsum","value"=>"sincronizaLatlongconMlsPsum.php")
											,array("tit"=>"SincronizaMarketconMls","value"=>"sincronizaMarketconMls.php")
											,array("tit"=>"SincronizaMarketconPen","value"=>"sincronizaMarketconPen.php")
											,array("tit"=>"SincronizaMkvconMkpsPsumPendesY","value"=>"sincronizaMkvconMkpsPsumPendesY.php")
											,array("tit"=>"ActualizaWtfPoolPenMlsMkv","value"=>"actualizaWtfPoolPenMlsMkv.php")
											,array("tit"=>"ActualizaRtmDateoConMlsEntrydate","value"=>"actualizaRtmDateoConMlsEntrydate.php")
											,array("tit"=>"Fillsparcel","value"=>"fillsparcel.php")
											,array("tit"=>"ActualizaRemarkMlsresidential","value"=>"actualizaRemarkMlsresidential.php")
											,array("tit"=>"ActualizaPsummaryAstatusMlsresidential","value"=>"ActualizaPsummaryAstatusMlsresidential.php")
											,array("tit"=>"SincronizarImagenesconMlnumber_NEW","value"=>"sincronizarImagenesconMlnumber.php")
											,array("tit"=>"sincronizarLatlongconLatlongCounty_NEW","value"=>"sincronizarLatlongconLatlongCounty.php")
							);
//											,array("tit"=>"Email's de Realtors Mlsresidential (Backoffice)","value"=>"insertaRealtorBackofice.php")
				for($i=0;$i<count($titTransSincSincroniza);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincSincroniza[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSinc" id="<?php echo $titTransSincSincroniza[$i]["value"] ?>" value="<?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)+1) ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSinc, <?php echo ($i+count($titTransSincBackup)+count($titTransSincAcc1Msq)) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	</table>	
  </td>
  </tr>


<!--Transferir data Sincronizada y Calculada a Data del Sistema (DATA NUEVA)-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titTransSincCalcu','imgTransSincCalcu'); return false;">
    	<img id="imgTransSincCalcu" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Transferir data Sincronizada y Calculada Mant-Sist (DATA NUEVA)</th>
	<th width="190" ></th>
</tr>
  <tr  id="titTransSincCalcu" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">

		<!--Creando CSV a transferir al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransMantSis','imgTransMantSis'); return false;">
				<img id="imgTransMantSis" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Transferir Data de Tabla Mant-->Sis</th>
			<th width="180"><input type="checkbox" name="ckbTranfMantSis"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfMantSis, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransMantSis" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransMantSis=array(
								array("tit"=>"Latlong","value"=>"TransferMantToSisLatlong.php")
								,array("tit"=>"Marketvalue","value"=>"TransferMantToSisMarketValue.php")
								,array("tit"=>"MarketPs","value"=>"TransferMantToSisMarketPs.php")
							);
				for($i=0;$i<count($titTransMantSis);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransMantSis[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfMantSis" id="<?php echo $titTransMantSis[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfMantSis, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Creando CSV a transferir al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincBackupCalcu','imgTransSincBackupCalcu'); return false;">
				<img id="imgTransSincBackupCalcu" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Creando CSV a transferir de Data Mantenimiento-->Sistema</th>
			<th width="180"><input type="checkbox" name="ckbTranfSincCalcuBackup"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuBackup, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincBackupCalcu" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincBackupCalcu=array(
								array("tit"=>"Diffvalue","value"=>"BackupMysqlToCsvDiffvalue.php")
								,array("tit"=>"Imágenes","value"=>"BackupMysqlToCsvImagenes.php")
								,array("tit"=>"Latlong","value"=>"BackupMysqlToCsvLatlong.php")
								,array("tit"=>"Marketvalue","value"=>"BackupMysqlToCsvMarketvalue.php")
								,array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php")
								,array("tit"=>"Mlsresidential","value"=>"BackupMysqlToCsvMlsresidential.php")
								,array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php")
								,array("tit"=>"Psummary","value"=>"BackupMysqlToCsvPsummary.php")
								,array("tit"=>"Rental","value"=>"BackupMysqlToCsvRental.php")
								,array("tit"=>"Rtmaster","value"=>"BackupMysqlToCsvRtmaster.php")
							);
//								,array("tit"=>"Realtors","value"=>"BackupMysqlToCsvRealtors.php")
				for($i=0;$i<count($titTransSincBackupCalcu);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincBackupCalcu[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSincCalcuBackup" id="<?php echo $titTransSincBackupCalcu[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuBackup, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Server1: Hacer Backup de las Tablas (DATA VIEJA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titBackupS1','imgBackupS1'); return false;">
				<img id="imgBackupS1" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Hacer Backup de las Tablas de Data del Sistema</th>
			<th width="180"><input type="checkbox" name="ckbBackupS1"   id="*" value="*" onClick="checkSeleccion(document.formulario.ckbBackupS1, 0)">Todos</th>
		</tr>
   	    <tr  id="titBackupS1" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titBackupS1=array(
								array("tit"=>"Diffvalue","value"=>"BackupMysqlToCsvDiffvalue.php"),
								array("tit"=>"Imágenes","value"=>"BackupMysqlToCsvImagenes.php"),
								array("tit"=>"Latlong","value"=>"BackupMysqlToCsvLatlong.php"),
								array("tit"=>"Marketvalue","value"=>"BackupMysqlToCsvMarketvalue.php"),
								array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php"),
								array("tit"=>"Mlsresidential","value"=>"BackupMysqlToCsvMlsresidential.php"),
								array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
								array("tit"=>"Psummary","value"=>"BackupMysqlToCsvPsummary.php"),
								array("tit"=>"Rental","value"=>"BackupMysqlToCsvRental.php"),
								array("tit"=>"Rtmaster","value"=>"BackupMysqlToCsvRtmaster.php")
							);
				for($i=0;$i<count($titBackupS1);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titBackupS1[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbBackupS1"  id="<?php echo $titBackupS1[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbBackupS1, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Restore de los CSV transferidos al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincRestoreCalcu','imgTransSincRestoreCalcu'); return false;">
				<img id="imgTransSincRestoreCalcu" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Restore de los CSV de Data Mantenimiento-->Sistema </th>
			<th width="180"><input type="checkbox" name="ckbTranfSincCalcuRestore"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuRestore, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincRestoreCalcu" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<? 
				$titTransSincRestoreCalcu=array(
											array("tit"=>"Diffvalue","value"=>"RestoreCsvToMysqlDiffvalue.php")
											,array("tit"=>"Imágenes","value"=>"RestoreCsvToMysqlImagenes.php")
											,array("tit"=>"Latlong","value"=>"RestoreCsvToMysqlLatlong.php")
											,array("tit"=>"Marketvalue","value"=>"RestoreCsvToMysqlMarketvalue.php")
											,array("tit"=>"MarketPs","value"=>"RestoreCsvToMysqlMarketPs.php")
											,array("tit"=>"Mlsresidential","value"=>"RestoreCsvToMysqlMlsresidential.php")
											,array("tit"=>"Pendes","value"=>"RestoreCsvToMysqlPendes.php")
											,array("tit"=>"Psummary","value"=>"RestoreCsvToMysqlPsummary.php")
											,array("tit"=>"Rental","value"=>"RestoreCsvToMysqlRental.php")
											,array("tit"=>"Rtmaster","value"=>"RestoreCsvToMysqlRtmaster.php")
							);
//											,array("tit"=>"Realtors","value"=>"RestoreCsvToMysqlRealtors.php")
				for($i=0;$i<count($titTransSincRestoreCalcu);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincRestoreCalcu[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSincCalcuRestore" id="<?php echo $titTransSincRestoreCalcu[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuRestore, <?php echo ($i+1)  ?>)">
						  Ejecutar</td>
					</tr>
			<?
				}
			?>		
				</table>	
		
		<td>
		</tr>

	</table>	
  </td>
  </tr>


<!--Create's de las Tablas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titCreaTbl','imgCreaTbl'); return false;">

    	<img id="imgCreaTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Create's de las Tablas</th>
	<th width="190" ></th>
</tr>
  <tr  id="titCreaTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titCreaTbl=array(
					array("tit"=>"Countyllave","value"=>"create_countyllave.php"),
					array("tit"=>"Diffvalue","value"=>"create_diffvalue.php"),
					array("tit"=>"Imágenes","value"=>"create_imagenes.php"),
					array("tit"=>"Latlong","value"=>"create_latlong.php"),
					array("tit"=>"Marketvalue","value"=>"create_marketvalue.php"),
					array("tit"=>"MarketPs","value"=>"create_marketps.php"),
					array("tit"=>"Mlsresidential","value"=>"create_mlsresidential.php"),
					array("tit"=>"Mortgage","value"=>"create_mortgage.php"),
					array("tit"=>"ParcelsCentroids","value"=>"create_parcels_centroids.php"),
					array("tit"=>"Pendes","value"=>"create_pendes.php"),
					array("tit"=>"Psummary","value"=>"create_psummary.php"),
					array("tit"=>"Rental","value"=>"create_rental.php"),
					array("tit"=>"Rtmaster","value"=>"create_rtmaster.php"),
					array("tit"=>"Sectllave","value"=>"create_sectllave.php"),
					array("tit"=>"Stapagini","value"=>"create_stapagini.php")
				);
	for($i=0;$i<count($titCreaTbl);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titCreaTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbCreaTbl" id="<?php echo $titCreaTbl[$i]["value"] ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>

<!--Realizar Backup de Tablas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titBupTbl','imgBupTbl'); return false;">
    	<img id="imgBupTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Realizar Backup de Tablas</th>
	<th width="190" ><input type="checkbox" name="ckbBupTbl"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbBupTbl, 0)"> 
	  Todos</th>
</tr>
  <tr  id="titBupTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titBupTbl=array(
					array("tit"=>"Diffvalue","value"=>"BackupMysqlToCsvDiffvalue.php"),
					array("tit"=>"Imágenes","value"=>"BackupMysqlToCsvImagenes.php"),
					array("tit"=>"Latlong","value"=>"BackupMysqlToCsvLatlong.php"),
					array("tit"=>"Latlongsis","value"=>"BackupMysqlToCsvLatlongsis.php"),
					array("tit"=>"Marketvalue","value"=>"BackupMysqlToCsvMarketvalue.php"),
					array("tit"=>"Marketvaluesis","value"=>"BackupMysqlToCsvMarketvaluesis.php"),
					array("tit"=>"Mlsresidential","value"=>"BackupMysqlToCsvMlsresidential.php"),
					array("tit"=>"Mortgage","value"=>"BackupMysqlToCsvMortgage.php"),
					array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php"),
					array("tit"=>"Psummary","value"=>"BackupMysqlToCsvPsummary.php"),
					array("tit"=>"Rental","value"=>"BackupMysqlToCsvRental.php"),
					array("tit"=>"Rtmaster","value"=>"BackupMysqlToCsvRtmaster.php"),
					array("tit"=>"Stapagini","value"=>"BackupMysqlToCsvStapagini.php")
				);
	for($i=0;$i<count($titBupTbl);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titBupTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbBupTbl"  id="<?php echo $titBupTbl[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbBupTbl, <?php echo ($i+1)  ?>)">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>

<!--Realizar Restore de Tablas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titResTbl','imgResTbl'); return false;">
    	<img id="imgResTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Realizar Restore de Tablas</th>
	<th width="190" ><input type="checkbox" name="ckbResTbl"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbResTbl, 0)"> 
	  Todos</th>
</tr>
  <tr  id="titResTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titResTbl=array(
					array("tit"=>"Diffvalue","value"=>"RestoreCsvToMysqlDiffvalue.php"),
					array("tit"=>"Imágenes","value"=>"RestoreCsvToMysqlImagenes.php"),
					array("tit"=>"Latlong","value"=>"RestoreCsvToMysqlLatlong.php"),
					array("tit"=>"Latlongsis","value"=>"RestoreCsvToMysqlLatlongsis.php"),
					array("tit"=>"Marketvalue","value"=>"RestoreCsvToMysqlMarketvalue.php"),
					array("tit"=>"Marketvaluesis","value"=>"RestoreCsvToMysqlMarketvaluesis.php"),
					array("tit"=>"Mlsresidential","value"=>"RestoreCsvToMysqlMlsresidential.php"),
					array("tit"=>"Mortgage","value"=>"RestoreCsvToMysqlMortgage.php"),
					array("tit"=>"Pendes","value"=>"RestoreCsvToMysqlPendes.php"),
					array("tit"=>"Psummary","value"=>"RestoreCsvToMysqlPsummary.php"),
					array("tit"=>"Rental","value"=>"RestoreCsvToMysqlRental.php"),
					array("tit"=>"Rtmaster","value"=>"RestoreCsvToMysqlRtmaster.php"),
					array("tit"=>"Stapagini","value"=>"RestoreCsvToMysqlStapagini.php"),
					array("tit"=>"Parcels_Centroids","value"=>"RestoreCsvToMysqlPCentroids.php")
				);
	for($i=0;$i<count($titResTbl);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titResTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbResTbl" id="<?php echo $titResTbl[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbResTbl, <?php echo ($i+1)  ?>)">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>

<!--Transferir Data de los CSV de Access1 a MySql-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titAcs1Msq','imgAcs1Msq'); return false;">
    	<img id="imgAcs1Msq" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Transferir Data de los CSV de Access1 a MySql</th>
	<th width="190" ></th>
</tr>
  <tr  id="titAcs1Msq" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titAcs1Msq=array(
					array("tit"=>"Diffvalue","value"=>"TransferAccess1ToCsvDiffvalue.php"),
					array("tit"=>"Marketvalue","value"=>"TransferAccess1ToCsvMarketValue.php"),
					array("tit"=>"Mlsresidential","value"=>"TransferAccess1ToCsvMlsResidential.php"),
					array("tit"=>"Mortgage","value"=>"TransferAccess1ToCsvMortgage.php"),
					array("tit"=>"Parcels Centroids","value"=>"TransferAccess1ToCsvParcels_Centroids.php"),
					array("tit"=>"Pendes","value"=>"TransferAccess1ToCsvPendes.php"),
					array("tit"=>"Psummary","value"=>"TransferAccess1ToCsvPsummary.php"),
					array("tit"=>"Rental","value"=>"TransferAccess1ToCsvRental.php"),
					array("tit"=>"Rtmaster","value"=>"TransferAccess1ToCsvRtmaster.php")
				);
	for($i=0;$i<count($titAcs1Msq);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titAcs1Msq[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbAcs1Msq" id="<?php echo $titAcs1Msq[$i]["value"] ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>

<!--Verificacion de data en MySql-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titVerfDatMsq','imgVerfDatMsq'); return false;">
    	<img id="imgVerfDatMsq" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Verificación de data en MySql</th>
	<th width="190" ></th>
</tr>
  <tr  id="titVerfDatMsq" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titVerfDatMsq=array(
					 array("tit"=>"SincronizaLatlongconMlsPsum","value"=>"sincronizaLatlongconMlsPsum.php")
					,array("tit"=>"SincronizaMarketconMls","value"=>"sincronizaMarketconMls.php")
					,array("tit"=>"SincronizaMarketconPen","value"=>"sincronizaMarketconPen.php")
					,array("tit"=>"SincronizaMkvconMkpsPsumPendesY","value"=>"sincronizaMkvconMkpsPsumPendesY.php")
					,array("tit"=>"ActualizaWtfPoolPenMlsMkv","value"=>"actualizaWtfPoolPenMlsMkv.php")
					,array("tit"=>"ActualizaRtmDateoConMlsEntrydate","value"=>"actualizaRtmDateoConMlsEntrydate.php")
					,array("tit"=>"Fillsparcel","value"=>"fillsparcel.php")
					,array("tit"=>"ActualizaRemarkMlsresidential","value"=>"actualizaRemarkMlsresidential.php")
					,array("tit"=>"sincronizarLatlongconLatlongCounty_NEW","value"=>"sincronizarLatlongconLatlongCounty.php")
					,array("tit"=>"SincronizarImagenesconMlnumber_NEW","value"=>"sincronizarImagenesconMlnumber.php")
				);

	for($i=0;$i<count($titVerfDatMsq);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titVerfDatMsq[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbVerfDatMsq" id="<?php echo $titVerfDatMsq[$i]["value"] ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>
  
  
<!--Cantidad de Registros en Tablas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titCantRTbl','imgCantRTbl'); return false;">
    	<img id="imgCantRTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Validacion y confirmacion de data   <span id="new">NUEVO!!!</span></th>
	<th width="190" ><input type="checkbox" name="CantRTbl"  id="*" value="*" onClick="checkSeleccion(document.formulario.CantRTbl, 0)"> 
	  Todos</th>
</tr>
  <tr  id="titCantRTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titCantRTbl=array(
					array("tit"=>"Diffvalue","value"=>"cantRecordDiffvalue.php"),
					array("tit"=>"Imágenes","value"=>"cantRecordImagenes.php"),
					array("tit"=>"Latlong","value"=>"cantRecordLatlong.php"),
					array("tit"=>"MarketPS","value"=>"cantRecordMarketPS.php"),
					array("tit"=>"Marketvalue","value"=>"cantRecordMarketvalue.php"),
					array("tit"=>"Mlsresidential","value"=>"cantRecordMlsresidential.php"),
					array("tit"=>"Mortgage","value"=>"cantRecordMortgage.php"),
					array("tit"=>"Pendes","value"=>"cantRecordPendes.php"),
					array("tit"=>"Psummary","value"=>"cantRecordPsummary.php"),
					array("tit"=>"Rental","value"=>"cantRecordRental.php"),
					array("tit"=>"Rtmaster","value"=>"cantRecordRtmaster.php"),
					array("tit"=>"Validación de la Data por Querys","value"=>"query.php"),
					array("tit"=>"Porcentajes en calculo MarketValue","value"=>"PorcentajeCalculomarketvalue.php"),
					array("tit"=>"Porcentajes en calculo MarketPs","value"=>"PorcentajeCalculomarketps.php")
				);
	for($i=0;$i<count($titCantRTbl);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titCantRTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="CantRTbl" id="<?php echo $titCantRTbl[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.CantRTbl, <?php echo ($i+1)  ?>)">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->

<div id="pasos">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filPasos','imgfilPasos'); return false;">
				<img id="imgfilPasos" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Pasos a Seguir Transferencia de DATA NUEVA</th>
		</tr>
		<tr  id="filPasos" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divPasos" >
                              <p>TODO ESTO ES EN SERVER2<br>
  ==========================================================<br>
  <br>
  Pasar los CSV's de Access1 al directorio repectivo de BD, C:\XIMA\access1files\
  <br>
  <br>
  1.- Ejecutar esta URL http://208.109.178.110/datawork/mantenimiento.php<br>
  <br>
  
2.- Seleccionar la Base de datos (de Mantenimiento, las que tienen MXXXXXX).<br>
  <br>
  
3.- Seleccionar el Checkbox Ejecutar de Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA).<br>
  <br>
  4.- Verificar que todos los CheckBox de Hacer Backup de las Tablas, Pasar los CSV de access1 a 
      las Tablas y Ejecutar los php de Sincronización de Data esten todos seleccionados.<br>
      <br>
  
5.- Hacer click al boton Ejecutar, cuando termine los RESULTADOS SE MUESTRAN DEBAJO DEL BOTON EJECUTAR,
      esto va a tardar un poco ya que ejecutara todos los php uno por uno, pero cuando termine aparecen 
      todas las estaditicas de cada php.<br>
      <br>
  
6.- Si se produce un error en alguna ejecucion de los php's, como data mas grande de lo permitido para un 
      campo o los beds o esos errores que dan ... corriges el error y seleccionas a partir de ese php los 
      php que hacen falta por ejecutar.<br>
      <br>
      7.- Si NO se produce error (ojala y no pase ninguno..) se ejecuta la Sincronización de los Centroids de 
	  Latlong. Igual que en los calculos se ejecuta localmente en el servidor.(Esto por ser primera para 
	  ver el tiempo que tarda)<br>
	  URL--> http://localhost/DataWork/dataverificacion/sincronizarLatlongconLatlongCounty.php<br>
	  <br>
  
8.- Ahora se ejecutan los calculos. Este php se sigue ejecutando desde el servidor..<br>
  URL--> http://localhost/DataWork/calculos/calculoMarketValue.php<br>
  <br>
  
9.- Ahora se ejecutan las imagenes. Este php se sigue ejecutando desde el servidor..<br>
  URL--> http://localhost/DataWork/dataverificacion/TranferNameCasaBroDadDirPIC.php<br>
  <br>
 
10.- Cuando terminen los Calculos, Imagenes y Centroids ya se tiene transferida los access1, sincronizada y 
	   calculada la data, ahora viene la parte de pasar las tablas (Diffvalue, Imagenes, Latlong, Marketvalue,
	   Mlsresidential, Pendes, Psummary y Rtmaster) del Server2 al Server1 de flbroward y fldade..<br>
	   <br>
	   11.- Primero asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos" 
      que esta al lado del boton ejecutar, dale click y listo, el los deseleccionará todos.<br>
      <br>
12.- Ahora vas al segundo item en azul "Transferir data Sincronizada y Calculada Mant-Sist  (DATA NUEVA)" y seleccionas el CheckBox "Todos" del primer Item Amarillo &quot;Transferir Data de Tabla Mant--&gt;Sis&quot;, estos transferiran las tablas con los campos que deben tener a las tablas del Sistema que son diferentes a los de de Matenimiento (hasta ahora son solo Latlong y Marketvalue). Click al boton Ejecutar. </p>
                              <p> 13.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".</p>
                              <p>OJO DIRECTORIO NUEVO!!! ==> "C:\XIMA\dataserver1"<br>
                                14.- Ahora al segundo item amarillo "Creando CSV a transferir de Data Mantenimiento--&gt;Sistema", 
       el generará los CSV dentro de este directorio (es Nuevo, lo cree para esta parte) ==> "C:\XIMA\dataserver1" para la BD seleccionada. <br>
	   <br>
  
MONTANDO LA DATA NUEVA EN LA DATA DEL SISTEMA DE SERVER2 Y SERVER1 <br>
  ========================================================================<br>
  SOLO EN SERVER1: <br>
  ---------------------------------------<BR>
                              15.- Luego que termine de ejecutarse zipeas los files del C:\XIMA\dataserver1 y los ftpeas al server1.<br><br>
                              16.- Luego de ftpearlos al Server1 los colocas en el directorio "C:\XIMA\dataserver1" en el directorio de 
         la BD que le corresponde.<br><br>
         
PARA SERVER1 Y SERVER2.<BR>
  ---------------------------------------<BR>
                                17.- Ejecutar esta URL SERVER2: http://208.109.178.110/datawork/mantenimiento.php O SERVER1: http://68.178.206.51/datawork/mantenimiento.php<br>
                                <br>
                                18.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".<br>
                                <br>
                                OJJJJJJJOOOOOOO!!!!!!!!! ESTE IMPORTANTE!!!!!!!! BACKUP SERVER1 Y EN SERVER2<br>
                                19.- Vas al segundo item en azul "Transferir data Sincronizada y Calculada a Server1 (DATA NUEVA)"
           y seleccionas el CheckBox "Todos"  del tercer Item Amarillo "Hacer Backup de las Tablas de Data del Sistema", 
           para generar backup de la Data del sistema que existe.<br>
           <br>
           20.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".<br>
           <br>
                                OJJJJJJJOOOOOOO!!!!!!!!! ESTE IMPORTANTE!!!!!!!! RESTORE SERVER1 Y EN SERVER2<br>
           21.- Cuando termine de hacer backup seleccionas el CheckBox "Todos"(si aplica) del cuarto Item Amarillo 
     "Restore de los CSV de Data Mantenimiento--&gt;Sistema", y estos pasaran los csv que estan en el directorio "C:\XIMA\dataserver1"de las tablas 
           selecciona a la data del sistema.<br>
           <br>
           22.- Ejecutar el menu Calcular la cantidad de registros de las tablas, escojer todos, para ingresar la cantidad de registros de cada tabla en la base de datos para estadistica. </p>
							</div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>

<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
