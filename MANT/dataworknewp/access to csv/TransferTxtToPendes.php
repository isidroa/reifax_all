<?php  
	function ArreglaPctg($pctg)
	{
		$vec=explode(" ",$pctg);//separo por espacio 1/2 M
		$pctg=$vec[0];//toma la poscion 0 de la separacion anterior
		$tam=strlen($pctg);
		$ret="";
		for($i=0;$i<$tam;$i++)
		{
			$car=substr($pctg,$i,1);//tomo caracter por caracter
			if(is_numeric($car) || $car=="." || $car=="/")//solo numeros,puntos y slash
				$ret.=$car;
		}
		if($ret=="")$ret=0;//valida si tiene vacio pone 0
		if(strrchr($ret,"/")==true)
		{
			$vec=explode("/",$ret);//separo por espacio 1/2 M
			$ret=round(($vec[0]/$vec[1]),2);//evalua los 1/2=0.5
		}
		$ret=0+$ret; //valida los .5 --> 0+.5=0.5
		return $ret;
	}
	function ArreglaTexto($texto)
	{
		$texto=trim($texto);
		$texto=str_replace("'","\'",$texto);
		return $texto;
	}
	function ArreglaNumero($num)
	{
		if (strlen(trim($num))==0)
			return 0;
		return $num;
	}
	function ArreglaFecha($fecha)
	{
		$vec=explode("/",$fecha);//separo por /
		$dia=$vec[0];
		$mes=$vec[1];
		$anio=$vec[2];
		return $anio.$mes.$dia;
	}
		
	include("../libreria.php");
	$ret=explode("*",CabeceraPagina()); 
	$servidor=$ret[0];$nom_bd=$ret[1];$lh=$ret[2];
	$starttime=tiempo();
	PiePagina();
/****************************************************/
	if ($nom_bd=="fldade" or $nom_bd=="fldade1")$dir="fldade";
	if ($nom_bd=="mflbroward")$dir="mflbroward";

	$table2="PENDES"; 
	$text="INSERT INTO $table2 (
	
PARCELID	,
ASSESSEDVA	,
ATTORNEY	,
CASE_NUMBE	,
CITY		,
DEFOWNER1	,
DEFOWNER2	,
DEFSUMADDR	,
DEFSUMNAME	,
FILE_DATE	,
FINALBID	,
FIREPLACE	,
FORECLOSUR	,
GARAGE		,
HOMESTEAD	,
HOUSE_NUMB	,
JUDGEAMT	,
JUDGEDATE	,
JUDGEEQ		,
JUDGESALER	,
LANDSQFT	,
LIEN1AMT	,
LIEN1BOOK	,
LIEN1HOLDE	,
LIEN1PAGE	,
LIEN1TYPE	,
LIEN2AMT	,
LIEN2BOOK	,
LIEN2HOLDE	,
LIEN2PAGE	,
LIEN2TYPE	,
LIEN3AMT	,
LIEN3BOOK	,
LIEN3HOLDE	,
LIEN3PAGE	,
LIEN3TYPE	,
LIEN4AMT	,
LIEN4BOOK	,
LIEN4HOLDE	,
LIEN4PAGE	,
LIEN4TYPE	,
LIEN5AMT	,
LIEN5BOOK	,
LIEN5HOLDE	,
LIEN5PAGE	,
LIEN5TYPE	,
LIEN6AMT	,
LIEN6BOOK	,
LIEN6HOLDE	,
LIEN6PAGE	,
LIEN6TYPE	,
LIEN7AMT	,
LIEN7BOOK	,
LIEN7HOLDE	,
LIEN7PAGE	,
LIEN7TYPE	,
LOANVALRAT	,
MARKETVALU	,
MTG1AMT		,
MTG1BAL		,
MTG1BLNAMT	,
MTG1BLNDAT	,
MTG1BLNREM	,
MTG1BOOK	,
MTG1DATE	,
MTG1INTRAT	,
MTG1LASTPA	,
MTG1PAGE	,
MTG1PAYMEN	,
MTG1POSITI	,
MTG1RATETY	,
MTG1TYPE	,
MTG2AMT		,
MTG2BAL		,
MTG2BLNAMT	,
MTG2BLNDAT	,
MTG2BLNREM	,
MTG2BOOK	,
MTG2DATE	,
MTG2INTRAT	,
MTG2LASTPA	,
MTG2PAGE	,
MTG2PAYMEN	,
MTG2POSITI	,
MTG2RATETY	,
MTG2TYPE	,
MTG3AMT		,
MTG3BAL		,
MTG3BLNAMT	,
MTG3BLNDAT	,
MTG3BLNREM	,
MTG3BOOK	,
MTG3DATE	,
MTG3INTRAT	,
MTG3LASTPA	,
MTG3PAGE	,
MTG3PAYMEN	,
MTG3POSITI	,
MTG3RATETY	,
MTG3TYPE	,
MTG4AMT		,
MTG4BAL		,
MTG4BLNAMT	,
MTG4BLNDAT	,
MTG4BLNREM	,
MTG4BOOK	,
MTG4DATE	,
MTG4INTRAT	,
MTG4LASTPA	,
MTG4PAGE	,
MTG4PAYMEN	,
MTG4POSITI	,
MTG4RATETY	,
MTG4TYPE	,
MTG5AMT		,
MTG5BAL		,
MTG5BLNAMT	,
MTG5BLNDAT	,
MTG5BLNREM	,
MTG5BOOK	,
MTG5DATE	,
MTG5INTRAT	,
MTG5LASTPA	,
MTG5PAGE	,
MTG5PAYMEN	,
MTG5POSITI	,
MTG5RATETY	,
MTG5TYPE	,
MTG6AMT		,
MTG6BAL		,
MTG6BLNAMT	,
MTG6BLNDAT	,
MTG6BLNREM	,
MTG6BOOK	,
MTG6DATE	,
MTG6INTRAT	,
MTG6LASTPA	,
MTG6PAGE	,
MTG6PAYMEN	,
MTG6POSITI	,
MTG6RATETY	,
MTG6TYPE	,
MTG7AMT		,
MTG7BAL		,
MTG7BLNAMT	,
MTG7BLNDAT	,
MTG7BLNREM	,
MTG7BOOK	,
MTG7DATE	,
MTG7INTRAT	,
MTG7LASTPA	,
MTG7PAGE	,
MTG7PAYMEN	,
MTG7POSITI	,
MTG7RATETY	,
MTG7TYPE	,
PARCEL_SUB	,
PLAINTIFF1	,
PLAINTIFF2	,
PLAINTIFF3	,
PLAINTIFF4	,
PLAINTIFF5	,
PLAINTIFF6	,
PLAINTIFF7	,
POOL		,
POST_DIR	,
POSTAL_COD	,
PRE_DIR		,
PROP1BATH	,
PROP1BED	,
PROP1CONST	,
PROP1LEGAL	,
PROP1LUSE	,
PROP1SAMT	,
PROP1SDATE	,
PROP1SF		,
PROP1YRBLT	,
RECORD		,
RNG			,
SEC			,
SPA			,
STATE_ABBR	,
STREET_NAM	,
STREET_SUF	,
SUBDIV_NOP	,
TOTALIENS	,
TOTALMTG	,
TOTALPENDES	,
TWN			,
UNIT_NUMBE	,
POF
			) VALUES ";
	$q_ins=$text;
	mysql_query ("TRUNCATE TABLE ".$table2) or die(mysql_error());
	
	$ruta="C:\\\\XIMA\\\\access1files\\\\".$dir."\\\\".$table2."1.csv";

	$f1=fopen($ruta,"r");
	if(!$f1){echo "error abriendo el archivo: $archivo";die(mysql_error());}
	$num_reg=0;
	$band=0;
	while (!feof($f1)) 
	{
		$reg="";
		while( ($z=fgetc($f1))<>"\r" && !feof($f1))$reg.=$z;
		$row=explode ("\t", $reg); 
			if(count($row)==1)break;
//			$num_reg++;echo "<br><br>$num_reg entra ".count($row)." ".$reg;
//			print_r($row);$num_reg++;if($num_reg==5)exit();

$var1=ArreglaTexto($row[0]);
$var2=ArreglaNumero($row[1]);
$var3=ArreglaTexto($row[2]);
$var4=ArreglaTexto($row[3]);
$var5=ArreglaTexto($row[4]);
$var6=ArreglaTexto($row[5]);
$var7=ArreglaTexto($row[6]);
$var8=ArreglaTexto($row[7]);
$var9=ArreglaTexto($row[8]);
$var10=ArreglaFecha($row[9]);
$var11=ArreglaNumero($row[10]);
$var12=ArreglaTexto($row[11]);
$var13=ArreglaTexto($row[12]);
$var14=ArreglaTexto($row[13]);
$var15=ArreglaTexto($row[14]);
$var16=ArreglaTexto($row[15]);
$var17=ArreglaNumero($row[16]);
$var18=ArreglaFecha($row[17]);
$var19=ArreglaNumero($row[18]);
$var20=ArreglaTexto($row[19]);
$var21=ArreglaNumero($row[20]);
$var22=ArreglaNumero($row[21]);
$var23=ArreglaTexto($row[22]);
$var24=ArreglaTexto($row[23]);
$var25=ArreglaTexto($row[24]);
$var26=ArreglaTexto($row[25]);
$var27=ArreglaNumero($row[26]);
$var28=ArreglaTexto($row[27]);
$var29=ArreglaTexto($row[28]);
$var30=ArreglaTexto($row[29]);
$var31=ArreglaTexto($row[30]);
$var32=ArreglaNumero($row[31]);
$var33=ArreglaTexto($row[32]);
$var34=ArreglaTexto($row[33]);
$var35=ArreglaTexto($row[34]);
$var36=ArreglaTexto($row[35]);
$var37=ArreglaNumero($row[36]);
$var38=ArreglaTexto($row[37]);
$var39=ArreglaTexto($row[38]);
$var40=ArreglaTexto($row[39]);
$var41=ArreglaTexto($row[40]);
$var42=ArreglaNumero($row[41]);
$var43=ArreglaTexto($row[42]);
$var44=ArreglaTexto($row[43]);
$var45=ArreglaTexto($row[44]);
$var46=ArreglaTexto($row[45]);
$var47=ArreglaNumero($row[46]);
$var48=ArreglaTexto($row[47]);
$var49=ArreglaTexto($row[48]);
$var50=ArreglaTexto($row[49]);
$var51=ArreglaTexto($row[50]);
$var52=ArreglaNumero($row[51]);
$var53=ArreglaTexto($row[52]);
$var54=ArreglaTexto($row[53]);
$var55=ArreglaTexto($row[54]);
$var56=ArreglaTexto($row[55]);
$var57=ArreglaNumero($row[56]);
$var58=ArreglaNumero($row[57]);
$var59=ArreglaNumero($row[58]);
$var60=ArreglaNumero($row[59]);
$var61=ArreglaNumero($row[60]);
$var62=ArreglaTexto($row[61]);
$var63=ArreglaTexto($row[62]);
$var64=ArreglaTexto($row[63]);
$var65=ArreglaFecha($row[64]);
$var66=ArreglaTexto($row[65]);
$var67=ArreglaTexto($row[66]);
$var68=ArreglaTexto($row[67]);
$var69=ArreglaTexto($row[68]);
$var70=ArreglaTexto($row[69]);
$var71=ArreglaTexto($row[70]);
$var72=ArreglaTexto($row[71]);
$var73=ArreglaNumero($row[72]);
$var74=ArreglaNumero($row[73]);
$var75=ArreglaNumero($row[74]);
$var76=ArreglaTexto($row[75]);
$var77=ArreglaTexto($row[76]);
$var78=ArreglaTexto($row[77]);
$var79=ArreglaFecha($row[78]);
$var80=ArreglaTexto($row[79]);
$var81=ArreglaTexto($row[80]);
$var82=ArreglaTexto($row[81]);
$var83=ArreglaTexto($row[82]);
$var84=ArreglaTexto($row[83]);
$var85=ArreglaTexto($row[84]);
$var86=ArreglaTexto($row[85]);
$var87=ArreglaNumero($row[86]);
$var88=ArreglaNumero($row[87]);
$var89=ArreglaNumero($row[88]);
$var90=ArreglaTexto($row[89]);
$var91=ArreglaTexto($row[90]);
$var92=ArreglaTexto($row[91]);
$var93=ArreglaFecha($row[92]);
$var94=ArreglaTexto($row[93]);
$var95=ArreglaTexto($row[94]);
$var96=ArreglaTexto($row[95]);
$var97=ArreglaTexto($row[96]);
$var98=ArreglaTexto($row[97]);
$var99=ArreglaTexto($row[98]);
$var100=ArreglaTexto($row[99]);
$var101=ArreglaNumero($row[100]);
$var102=ArreglaNumero($row[101]);
$var103=ArreglaNumero($row[102]);
$var104=ArreglaTexto($row[103]);
$var105=ArreglaTexto($row[104]);
$var106=ArreglaTexto($row[105]);
$var107=ArreglaFecha($row[106]);
$var108=ArreglaTexto($row[107]);
$var109=ArreglaTexto($row[108]);
$var110=ArreglaTexto($row[109]);
$var111=ArreglaTexto($row[110]);
$var112=ArreglaTexto($row[111]);
$var113=ArreglaTexto($row[112]);
$var114=ArreglaTexto($row[113]);
$var115=ArreglaNumero($row[114]);
$var116=ArreglaNumero($row[115]);
$var117=ArreglaNumero($row[116]);
$var118=ArreglaTexto($row[117]);
$var119=ArreglaTexto($row[118]);
$var120=ArreglaTexto($row[119]);
$var121=ArreglaFecha($row[120]);
$var122=ArreglaTexto($row[121]);
$var123=ArreglaTexto($row[122]);
$var124=ArreglaTexto($row[123]);
$var125=ArreglaTexto($row[124]);
$var126=ArreglaTexto($row[125]);
$var127=ArreglaTexto($row[126]);
$var128=ArreglaTexto($row[127]);
$var129=ArreglaNumero($row[128]);
$var130=ArreglaNumero($row[129]);
$var131=ArreglaNumero($row[130]);
$var132=ArreglaTexto($row[131]);
$var133=ArreglaTexto($row[132]);
$var134=ArreglaTexto($row[133]);
$var135=ArreglaFecha($row[134]);
$var136=ArreglaTexto($row[135]);
$var137=ArreglaTexto($row[136]);
$var138=ArreglaTexto($row[137]);
$var139=ArreglaTexto($row[138]);
$var140=ArreglaTexto($row[139]);
$var141=ArreglaTexto($row[140]);
$var142=ArreglaTexto($row[141]);
$var143=ArreglaNumero($row[142]);
$var144=ArreglaNumero($row[143]);
$var145=ArreglaNumero($row[144]);
$var146=ArreglaTexto($row[145]);
$var147=ArreglaTexto($row[146]);
$var148=ArreglaTexto($row[147]);
$var149=ArreglaFecha($row[148]);
$var150=ArreglaTexto($row[149]);
$var151=ArreglaTexto($row[150]);
$var152=ArreglaTexto($row[151]);
$var153=ArreglaTexto($row[152]);
$var154=ArreglaTexto($row[153]);
$var155=ArreglaTexto($row[154]);
$var156=ArreglaTexto($row[155]);
$var157=ArreglaTexto($row[156]);
$var158=ArreglaTexto($row[157]);
$var159=ArreglaTexto($row[158]);
$var160=ArreglaTexto($row[159]);
$var161=ArreglaTexto($row[160]);
$var162=ArreglaTexto($row[161]);
$var163=ArreglaTexto($row[162]);
$var164=ArreglaTexto($row[163]);
$var165=ArreglaTexto($row[164]);
$var166=ArreglaTexto($row[165]);
$var167=ArreglaTexto($row[166]);
$var168=ArreglaTexto($row[167]);
$var169=ArreglaNumero($row[168]);
$var170=ArreglaNumero($row[169]);
$var171=ArreglaTexto($row[170]);
$var172=ArreglaTexto($row[171]);
$var173=ArreglaTexto($row[172]);
$var174=ArreglaNumero($row[173]);
$var175=ArreglaFecha($row[174]);
$var176=ArreglaNumero($row[175]);
$var177=ArreglaNumero($row[176]);
$var178=ArreglaNumero($row[177]);
$var179=ArreglaTexto($row[178]);
$var180=ArreglaTexto($row[179]);
$var181=ArreglaTexto($row[180]);
$var182=ArreglaTexto($row[181]);
$var183=ArreglaTexto($row[182]);
$var184=ArreglaTexto($row[183]);
$var185=ArreglaTexto($row[184]);
$var186=ArreglaNumero($row[185]);
$var187=ArreglaNumero($row[186]);
$var188=ArreglaNumero($row[187]);
$var189=ArreglaTexto($row[188]);
$var190=ArreglaTexto($row[189]);
$var191=ArreglaTexto($row[190]);	
			//if($band==1)$q_ins.=",";
			$q_ins.=" (
			'$var1',
$var2,
'$var3',
'$var4',
'$var5',
'$var6',
'$var7',
'$var8',
'$var9',
'$var10',
$var11,
'$var12',
'$var13',
'$var14',
'$var15',
'$var16',
$var17,
'$var18',
$var19,
'$var20',
$var21,
$var22,
'$var23',
'$var24',
'$var25',
'$var26',
$var27,
'$var28',
'$var29',
'$var30',
'$var31',
$var32,
'$var33',
'$var34',
'$var35',
'$var36',
$var37,
'$var38',
'$var39',
'$var40',
'$var41',
$var42,
'$var43',
'$var44',
'$var45',
'$var46',
$var47,
'$var48',
'$var49',
'$var50',
'$var51',
$var52,
'$var53',
'$var54',
'$var55',
'$var56',
$var57,
$var58,
$var59,
$var60,
$var61,
'$var62',
'$var63',
'$var64',
'$var65',
'$var66',
'$var67',
'$var68',
'$var69',
'$var70',
'$var71',
'$var72',
$var73,
$var74,
$var75,
'$var76',
'$var77',
'$var78',
'$var79',
'$var80',
'$var81',
'$var82',
'$var83',
'$var84',
'$var85',
'$var86',
$var87,
$var88,
$var89,
'$var90',
'$var91',
'$var92',
'$var93',
'$var94',
'$var95',
'$var96',
'$var97',
'$var98',
'$var99',
'$var100',
$var101,
$var102,
$var103,
'$var104',
'$var105',
'$var106',
'$var107',
'$var108',
'$var109',
'$var110',
'$var111',
'$var112',
'$var113',
'$var114',
$var115,
$var116,
$var117,
'$var118',
'$var119',
'$var120',
'$var121',
'$var122',
'$var123',
'$var124',
'$var125',
'$var126',
'$var127',
'$var128',
'$var129',
$var130,
$var131,
'$var132',
'$var133',
'$var134',
'$var135',
'$var136',
'$var137',
'$var138',
'$var139',
'$var140',
'$var141',
'$var142',
$var143,
$var144,
$var145,
'$var146',
'$var147',
'$var148',
'$var149',
'$var150',
'$var151',
'$var152',
'$var153',
'$var154',
'$var155',
'$var156',
'$var157',
'$var158',
'$var159',
'$var160',
'$var161',
'$var162',
'$var163',
'$var164',
'$var165',
'$var166',
'$var167',
'$var168',
$var169,
$var170,
'$var171',
'$var172',
'$var173',
$var174,
'$var175',
$var176,
$var177,
$var178,
'$var179',
'$var180',
'$var181',
'$var182',
'$var183',
'$var184',
'$var185',
'$var186',
'$var187',
'$var188',
'$var189',
'$var190',
'$var191'
			)";
			$ins++;
			$num_reg++;
			$band=1;
			if(($num_reg%5000)==0) echo "<br>num_reg: $num_reg , ins: $ins ";
			mysql_query($q_ins) or die("<br>num_reg: $num_reg , ins: $ins AQUI ERROR: $q_ins ".mysql_error());
			$q_ins=$text;
			
//			if($num_reg==1) break;
		}
		echo "<br>Num. de Registros del TXT: ".$num_reg;
		fclose($f1);

	echo "<br><br>FILES TXT INSERTADOS: ".$num_files;
	echo "<br>REGISTROS INSERTADOS A ".strtoupper($nom_bd).".".strtoupper($table2).": ".$ins." REGISTROS";

//"update rental as r, psummary as p set r.parcelid=p.parcelid where r.folio=p.parcelid;"

/****************************************************/
	$endtime=tiempo();
	echo "<br><br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";
?>