<?php   
	$starttime=tiempo();
/****************************************************/
	$table="rental";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `rental` (
  `Parcelid` varchar(20) NOT NULL default '0',
  `Sparcel` varchar(4) NOT NULL default '',
  `Status` varchar(2) NOT NULL default 'SN' COMMENT 'Status',
  `PropType` varchar(3) NOT NULL default 'SN' COMMENT 'Tipo de propiedad seg�n el MLS re1, re2 etc',
  `Address` varchar(32) default NULL COMMENT 'Direccion',
  `Agent` varchar(40) default NULL COMMENT 'Nombre del agente',
  `AgentEmail` varchar(100) default NULL COMMENT 'Email del Agente',
  `AgentLic` varchar(7) default NULL COMMENT 'Licencia del Agente',
  `AgentPh` varchar(20) default NULL COMMENT 'Telefono del Agente',
  `AgentPh2` varchar(20) default NULL COMMENT 'Telefono del Agente',
  `Apxtotsqft` mediumint(9) default '0' COMMENT 'Aproximated total square feet',
  `Area` varchar(6) default NULL COMMENT 'Codigo de zonificaion del county',
  `Bath` tinyint(2) default '0' COMMENT 'Cantidad de ba�os',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `BrokerPctg` varchar(12) default NULL COMMENT 'Porcentaje Comp to trans. Broker',
  `BuyerPctg` varchar(12) default NULL COMMENT 'Porcentaje Comp to buyer agent',
  `City` varchar(23) default NULL COMMENT 'Ciudad',
  `ClosingDT` varchar(10) default NULL COMMENT 'Fecha de cierre o desactivado del listado',
  `ConsType` varchar(50) default NULL COMMENT 'Tipo de construccion',
  `CoolD` varchar(40) default NULL COMMENT 'Aire acondicionado description',
  `County` varchar(16) default NULL COMMENT 'County',
  `Devlpmnt` varchar(20) default NULL COMMENT 'Urbanizacion o desarrollo',
  `Directns1` varchar(260) default NULL COMMENT 'Manera o forma de encontrar la direccion 1',
  `Dom` mediumint(3) default '0' COMMENT 'Dias en el mercado',
  `EntryDate` varchar(10) default NULL,
  `Folio` varchar(20) default NULL COMMENT 'Codificacion del condado de la propiedad para efectos legales',
  `Geoarea` varchar(4) default NULL COMMENT 'Codificacion geografica del area',
  `HBath` tinyint(2) default '0' COMMENT 'Cantidad de medios ba�os',
  `Latitude` double(15,10) default '0.00000000' COMMENT 'Latitud',
  `Ldate` varchar(10) default NULL COMMENT 'Fecha de listado',
  `ListType` varchar(4) default NULL COMMENT 'Tipo de listado',
  `Longitude` double(15,10) default '0.00000000' COMMENT 'Longitud',
  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
  `MLNumber` varchar(8) default NULL COMMENT 'Codigo de indentificaion del mls',
  `Muncode` varchar(4) default NULL COMMENT 'Codigo municipal',
  `OccupInfo` varchar(10) default NULL COMMENT 'Infomacion del ocupante',
  `OfficeFax` varchar(20) default NULL COMMENT 'Fax de la Oficina',
  `OfficeName` varchar(60) default NULL COMMENT 'Nombre de la Oficina',
  `OrgPrice` double(12,2) default '0.00' COMMENT 'Precio original',
  `ParcelN` varchar(4) default NULL COMMENT 'Numero de Parcela',
  `Period` varchar(1) default NULL,
  `Pool` varchar(1) default 'N' COMMENT 'Si tiene o no piscina',
  `Remark1` varchar(540) default NULL COMMENT 'Notas 1',
  `Sec` varchar(4) default NULL,
  `Sewer` varchar(40) default NULL COMMENT 'Informacion del as aguas negras',
  `State` varchar(2) default NULL COMMENT 'State',
  `Stno` mediumint(6) default '0' COMMENT 'Numero dela propiedad en la calle',
  `Street` varchar(24) default NULL COMMENT 'Nombre de la Calle',
  `Style` varchar(16) default NULL COMMENT 'Un tipo de codificacion ????',
  `Subdno` varchar(4) default NULL COMMENT 'Sub division Numero',
  `Twn` varchar(4) default NULL,
  `Type` varchar(10) default NULL COMMENT 'Tipo de propiedad',
  `Water1` varchar(40) default NULL COMMENT 'Aguas Blancas origen',
  `WaterF` varchar(1) default 'N' COMMENT 'Watwerfront si o no',
  `WaterFD` varchar(40) default NULL COMMENT 'Descripcion del agua colindante',
  `Yrbuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
  `Zip` varchar(6) default NULL COMMENT 'Codigo postal',
  `Zoning` varchar(12) default NULL COMMENT 'Zonificacion',
  `Dir` mediumint(6) default NULL,
  `Unit` varchar(8) default NULL,
  KEY `Parcelid` (`Parcelid`),
  KEY `folio` USING BTREE (`Folio`)
)";		
		
		
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>