<?php  
	$starttime=tiempo();
/****************************************************/
	$table="psummary";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `psummary` (
  `Parcelid` varchar(20) NOT NULL default '0' COMMENT 'Codigo de identificacion de la Parcela',
  `Sparcel` varchar(4) NOT NULL default '',
  `Ocode` varchar(4) NOT NULL default '0' COMMENT 'Codigo de identificacion SF condo de Xima',
  `CCode` varchar(4) NOT NULL default '0' COMMENT 'Codigo del tipo de propiedad',
  `Address` varchar(24) default NULL COMMENT 'Direccion',
  `Ac` varchar(2) default NULL COMMENT 'Tipo de aire acondicionado',
  `AssesedV` double(12,2) default '0.00' COMMENT 'Valor de apreciacion',
  `Astatus` varchar(2) default 'CC' COMMENT 'Status A en el mlsresidential',
  `Bath` double(4,1) default '0.0' COMMENT 'Cantidad de Baños',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `Beffective` mediumint(7) default '0' COMMENT 'Efective sqft',
  `Bheated` smallint(6) default '0',
  `BuildingV` double(12,2) default '0.00' COMMENT 'Valor del building',
  `CCodeD` varchar(40) default NULL COMMENT 'Descripcion del tipo de propiedad',
  `City` varchar(23) default NULL COMMENT 'Ciudad de la propiedad',
  `Dir` varchar(6) default NULL COMMENT 'Direccion',
  `Display` varchar(2) default NULL,
  `Folio` varchar(20) default NULL COMMENT 'Codigo de identificacion de la Parcela',
  `Homstead` double(12,2) default '0.00' COMMENT 'Epxension de',
  `LandV` double(12,2) default '0.00' COMMENT 'Valor de la tierra',
  `Legal` varchar(510) default NULL COMMENT 'Descripcion legal',
  `Lunit_type` varchar(6) default NULL,
  `Lunits` double(12,2) default '0.00',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida',
  `Owner` varchar(60) default NULL COMMENT 'Dueño',
  `Owner_A` varchar(50) default NULL COMMENT 'Direccion del dueño',
  `Owner_C` varchar(28) default NULL COMMENT 'Ciudad del dueño',
  `Owner_P` varchar(24) default NULL COMMENT 'Pais del dueño',
  `Owner_S` varchar(10) default NULL COMMENT 'Estado del dueño',
  `Owner_Z` varchar(20) default NULL COMMENT 'Zip del dueño',
  `OZip` varchar(5) default NULL COMMENT 'Zip del dueño substr(zip,1,5)',
  `Pool` varchar(1) default 'N' COMMENT 'Si tiene o no piscina',
  `PostDir` varchar(3) default NULL,
  `Rng` varchar(3) default NULL,
  `SaleDate` varchar(8) default NULL COMMENT 'Fecha de Venta',
  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
  `SDName` varchar(50) default NULL COMMENT 'Nombre de la subdivision',
  `Sec` varchar(4) default NULL,
  `SFP` double(12,2) default '0.00' COMMENT 'Precio por pie cuadrado',
  `Stories` tinyint(2) default '0' COMMENT 'Pisos de la propiedad',
  `Street` varchar(30) default NULL COMMENT 'Calle',
  `STType` varchar(6) default NULL COMMENT 'Tipo de Calle',
  `Stno` varchar(7) default NULL COMMENT 'Numero en la Calle',
  `TaxableV` double(12,2) default '0.00' COMMENT 'Valor de taxacion',
  `TSqft` int(12) default '0' COMMENT 'Area total de la propiedad',
  `Twn` varchar(4) default NULL,
  `Unit` varchar(7) default NULL COMMENT 'Numero de la propiedad',
  `Units` int(12) default '0',
  `WaterF` varchar(1) default 'N' COMMENT 'Si tiene o no agua colindante con la propiedad',
  `YrBuilt` mediumint(4) default '0' COMMENT 'Año de Construccion',
  `Zip` varchar(10) default NULL COMMENT 'Codigo Postal de la propiedad',
  PRIMARY KEY  USING BTREE (`Parcelid`),
  KEY `Sparcel` USING BTREE (`Sparcel`),
  KEY `CCode` USING BTREE (`CCode`),
  KEY `Astatus` USING BTREE (`Astatus`),
  KEY `City` USING BTREE (`City`),
  KEY `Address` USING BTREE (`Address`),
  KEY `Zip` USING BTREE (`Zip`),
  KEY `SDName` USING BTREE (`SDName`)
)"; 
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>

