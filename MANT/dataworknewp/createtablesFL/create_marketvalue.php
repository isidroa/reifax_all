<?php  
	$starttime=tiempo();
/****************************************************/
	$table="marketvalue";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `marketvalue` (
  `Parcelid` varchar(20) NOT NULL,
  `Sparcel` varchar(4) default NULL,
  `MarketMedia` double(15,5) default NULL,
  `Marketpctg` double(15,5) default NULL,
  `MarketValue` double(15,5) default NULL,
  `OffertMedia` double(15,5) default NULL,
  `OffertPctg` double(15,5) default NULL,
  `OffertValue` double(15,5) default NULL,
  `Pendes` varchar(1) NOT NULL default 'N',
  `Lien` varchar(1) NOT NULL default 'N',
  `DebtTV` double(12,2) default '0.00' COMMENT 'Porcentaje de deuda vs marketvalue',
  `OceanFront` varchar(1) default 'N' COMMENT 'Indica si es Ocean Front o no.',
  `Notes` mediumtext,
  `JUDGEDATE` varchar(10) default NULL COMMENT 'Scheduled Auction [yyyy-mm-dd] ',
  `FILE_DATE` varchar(10) default NULL COMMENT 'Filing Date [yyyy-mm-dd]08',
  PRIMARY KEY  USING BTREE (`Parcelid`),
  KEY `Sparcel` (`Sparcel`),
  KEY `Pendes` (`Pendes`)
)"; 
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>