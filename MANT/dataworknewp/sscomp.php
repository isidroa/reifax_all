<? 
session_start();
unset($_SESSION["query_taken"]);
include("seguridad.php");
//7.Feb.2008
if (isset($_COOKIE["countyUsr"]))  $_SESSION["county"]=$_COOKIE["countyUsr"];

$_SESSION["property"]=$_GET["pid"];
?>
<html><!-- InstanceBegin template="Templates/template1new.dwt" codeOutsideHTMLIsLocked="false" -->
<?php  require('Templates/Template.php'); ?>
		<head>
    
    <?php TemplateHeader(); ?>  
<!-- InstanceBeginEditable name="doctitle" -->
<title>Xima LLC. - Realtor Services</title>
<!-- InstanceEndEditable -->


<!-- InstanceBeginEditable name="head" --> 
<?php 
//Variable de ToolBar
$toolbar_map = 3;
$toolbar_email = $toolbar_excel = $toolbar_print = 3;
$toolbar_gps = false;
$toolbar_legend = $toolbar_label = $toolbar_filter = $toolbar_trash = $toolbar_graph = false;
?> 

<!------------------------------------>

<link rel="stylesheet" href="includes/css/gridstyles.css" TYPE="text/css" MEDIA="screen">
<link href="includes/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">

<script src="http://dev.virtualearth.net/mapcontrol/v4/MapControl.js"></script>
<script type="text/javascript" src="includes/jquery-1.1.3.1.js"></script>
<script type="text/javascript" src="includes/cookie.js"></script>
<script type="text/javascript" src="includes/grid_compar.js"></script>
<script type="text/javascript" src="includes/qsort.js"></script>
<script type="text/javascript" src="includes/dropmenu.js"></script>
<script type="text/javascript" src="includes/validaRow.js"></script>
<script type="text/javascript" src="includes/pagTag.js"></script>

<script>
<? echo "var priv=\"".intval($_SESSION['datos_usr']['PRIVILEGE'])."\";\r";
include("conexion.php");
$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','numformatted','decimals','align');//Search
$ArDfsCT=array('idtc','name','tabla','title','type','size','numformatted','decimal','align');//Search

if(intval($_SESSION['datos_usr']['PRIVILEGE'])>=60 && intval($_SESSION['datos_usr']['PRIVILEGE'])<=69){
echo "var no_mlnumber='yes';session_pen='Yes';";
$ArIDCT=array(102,61,96,131,72,71,106,126,129,296,114,77);
}else{
echo "var no_mlnumber='';";
$ArIDCT=array(102,97,61,96,131,72,71,106,126,129,296,114,77);
}
include ("fcamptit.php"); 
getCamptit("defa", $ArSqlCT, $ArDfsCT, $ArIDCT, "grid");
?>
	//var hdArray=[{'name':'parcelid','title':'folio','type':'string','align':'left','numformatted':false,'decimal':0},{'name':'mlnumber','title':'MLNumber','type':'string','align':'left','numformatted':false,'decimal':0},{'name':'address','title':'address','type':'text','align':'left','numformatted':false,'decimal':0},{'name':'lsqft','title':'lsqft','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'zip','title':'zip','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'beds','title':'beds','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'bath','title':'bath','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'pool','title':'pool','type':'text','align':'center','numformatted':false,'decimal':0},{'name':'waterf','title':'waterf','type':'text','align':'center','numformatted':false,'decimal':0},{'name':'yrbuilt','title':'yrbuilt','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'pendes','title':'foreclosure','type':'string','align':'center'},{'name':'saleprice','title':'saleprice','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'closingdt','title':'closingdt','type':'string','align':'right','numformatted':false,'decimal':0}];
	var sHfLocked="Sel.,Sta.,Ow.,Ag."
	hdArray=_defaStruct
	sHdDefa=hdArray
	//var sHdDefa=[{'name':'parcelid','type':'string','align':'left','numformatted':false,'decimal':0},{'name':'MLnumber','type':'string','align':'left','numformatted':false,'decimal':0},{'name':'address','type':'text','align':'left','numformatted':false,'decimal':0},{'name':'lsqft','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'zip','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'beds','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'bath','type':'number','align':'center','numformatted':false,'decimal':0},{'name':'pool','type':'text','align':'center','numformatted':false,'decimal':0},{'name':'waterf','type':'text','align':'center','numformatted':false,'decimal':0},{'name':'yrbuilt','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'pendes','type':'string','align':'center'},{'name':'saleprice','type':'number','align':'right','numformatted':true,'decimal':0},{'name':'closingdt','type':'string','align':'right','numformatted':false,'decimal':0}];

<?echo "var _MOD=\"".$_SESSION['current_MOD']."\";\r"?>
//global variables
var vFilas;
//VARIABLE PARA MOSTRAR LA INFO DE AGENT -[Maria Olivera]
var vOwner;
//VARIABLE PARA MOSTRAR LA INFO DE PENDES -[Maria Olivera]
var vAgent;
var vPendes;
var compact=0;//VARIABLE PARA DECIR COMPARABLES REPORTES
//Tipo de Comparable
var typeComp="comparable";
var which_table="mlsresidential";

<? echo "var curCounty='".strtolower($_SESSION["county"])."'\r";?>
//Para el proptype
<?echo "var curProper='".$_SESSION["proper"]."'\r";?>
<?echo "var id=\"".$_SESSION['id']."\";\r"?>
<?echo "var comparado=\"id=".$_GET["pid"]."\";\r"?>
<?echo "currentSel=\"".$_GET["pid"]."\";\r"?>
<?echo "var DB=\"".$_SESSION["county"]."\";\r"?>
<?echo "var my_filters=\"".$_SESSION['filters']."\";\r"?>

<?

//buscar el comparado y crear un mini-grid con sus datos.

$_mainTbl="";$_properField="";

$_inModule=$_SESSION['current_MOD'];

switch ($_inModule) {
    case "ML_SF":
		$_mainTbl="mlsresidential";
		$_properField="RE1";
        break;
    case "ML_CONDOS":
        $_mainTbl="mlsresidential";
		$_properField="RE2";
        break;
    case "PS_SF":
		$_mainTbl="psummary";
		$_properField="01";
        break;
    case "PS_CONDOS":
		$_mainTbl="psummary";
		$_properField="04";
        break;
		
}

$_mainMOd=split('_',$_inModule);if($_mainMOd[0]=="ML") $_mainMOd="MLS";else $_mainMOd=$_mainMOd[0];

$sql_comparado="Select
$_mainTbl.ParcelID,
$_mainTbl.address,
$_mainTbl.lsqft,
$_mainTbl.zip,
$_mainTbl.beds,
$_mainTbl.bath,
$_mainTbl.pool,
$_mainTbl.waterf,
$_mainTbl.yrbuilt,
$_mainTbl.lprice,
$_mainTbl.saleprice,
$_mainTbl.Status,
$_mainTbl.type,
Marketvalue.pendes,
Marketvalue.marketvalue,
Marketvalue.marketmedia,
Marketvalue.marketpctg,
Marketvalue.offertvalue,
mlsresidential.MLNumber,
LATLONG.LATITUDE,
LATLONG.LONGITUDE
FROM $_mainTbl LEFT JOIN (marketvalue,latlong) ON
(MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$_GET["pid"]. "';";

include("conexion.php");

$res = mysql_query($sql_comparado) or die(mysql_error());

$myrow= mysql_fetch_array($res, MYSQL_ASSOC);


$data_comparado.="{\"id\": \"".$myrow["ParcelID"].
				"\",\"ind\": 0".
				",\"address\": \"".$myrow["address"].
				"\",\"lsqft\": ".$myrow["lsqft"].
				",\"zip\": \"".$myrow["zip"].
				"\",\"mls\" : \"http://68.178.206.51/proyecto_xima/img/nophoto.gif".
				"\",\"beds\": ".$myrow["beds"].
				",\"bath\": ".$myrow["bath"].
				",\"pool\": \"".$myrow["pool"].
				"\",\"waterf\": \"".$myrow["waterf"].
				"\",\"yrbuilt\": ".$myrow["yrbuilt"].
				",\"x_price\": ".$myrow["lprice"].
				",\"lprice\": ".$myrow["lprice"].
				",\"status\": \"Subject".
				"\",\"status2\": \"".$myrow["Status"].
				"\",\"type\": \"".$myrow["type"].
				"\",\"pendes\": \"".$myrow["pendes"].
				"\",\"marketvalue\": ".$myrow["marketvalue"].
				",\"marketmedia\": ".$myrow["marketmedia"].
				",\"marketpctg\": ".$myrow["marketpctg"].
				",\"offertvalue\": ".$myrow["offertvalue"].
				",\"mlnumber\": \"".$myrow["MLNumber"].
				"\",\"xlat\": ".$myrow["LATITUDE"].
				",\"xlong\": ".$myrow["LONGITUDE"]."}";


echo "var datacompar=".$data_comparado.";\r";

echo "var _params4dodata=\"id=".$_GET["pid"]."&prop=$_properField&status=CS,CC&module=$_mainMOd&php_grid=mlsfresult.php&year=".$myrow["yrbuilt"]."&\"\r\n\r\n";


$sql_diff="SELECT diffvalue.lsqft as diff_lsqft, 
diffvalue.beds as diff_beds, 
diffvalue.bath as diff_bath,
diffvalue.zip as diff_zip
FROM diffvalue
WHERE diffvalue.parcelid='".$_GET["pid"]. "'";

$result = mysql_query($sql_diff) or die(mysql_error());

$mydiff= mysql_fetch_array($result, MYSQL_ASSOC);

if(is_null($mydiff["diff_bath"]) || $mydiff["diff_bath"]=="") $bath="0"; else $bath=$mydiff["diff_bath"];
if(is_null($mydiff["diff_beds"]) || $mydiff["diff_beds"]=="") $beds="0"; else $beds=$mydiff["diff_beds"];
if(is_null($mydiff["diff_lsqft"]) || $mydiff["diff_lsqft"]=="") $lsqft="0"; else $lsqft=$mydiff["diff_lsqft"];
if(is_null($mydiff["diff_zip"]) || $mydiff["diff_zip"]=="") $zip="0"; else $zip=$mydiff["diff_zip"];


$data_diff.="{\"bath\": \"".$bath.
				"\",\"beds\": \"".$beds.
				"\",\"zip\": \"".$zip.
				"\",\"lsqft\": \"".$lsqft."\"}";
				
echo "var datadiff=".$data_diff.";\r";
?>

var map;
function GetMap()
{
	//cargar virtual earth
		
		map = new VEMap("mymapcontroldiv");  
		
		
	///VEMap.LoadMap(VELatLong, zoom, style, fixed, mode, showSwitch);
	/*
	//
	VELatLong: A VELatLong object that represents the center of the map. Optional. 
	zoom: The zoom level to display. Valid values range from 1 through 19. Optional. Default is 4. 
	style: The map style. Valid values are a for aerial, h for hybrid, o for oblique (bird's eye), and r for road. Optional. Default is r. 
	fixed: A Boolean value that specifies whether the map view is displayed as a fixed map that the user cannot change. Optional. Default is false. 
	mode: A VEMapMode enumerator that specifies whether to load the map in 2D or 3D mode. Optional. Default is VEMapMode.Mode2D. 
	showSwitch: A Boolean value that specifies whether to show the map mode switch on the dashboard control. Optional. Default is true (the switch is displayed). 
	*/	
		map.LoadMap();
		map.Resize(920,350);
		//addButton();

//	doData("look4comparables.php",comparado+"&prop=RE2&status=CS,CC&year="+datacompar.yrbuilt+"&beds="+datacompar.beds+"&baths="+datacompar.bath+"&type="+datacompar.type+"&module=MLS&php_grid=mlsconresult.php");		
	doData("look4comparables.php",comparado+"&prop=<?=$_properField?>&status=CS,CC&year="+datacompar.yrbuilt+"&beds="+datacompar.beds+"&baths="+datacompar.bath+"&type="+datacompar.type+"&module=<?=$_mainMOd?>&php_grid=mlsfresult.php&");
	setup_compar();


}


function MapDispose() //dispose map etc
{
 	if (map!=null)
//	if (map && map.vemapcontrol) 
	{
	        map.Dispose();
			map = null;
	}
}



function calc_taken()
{
	//SMITH 21-12-2007: para que no salga error de mysql
	if (vFilas.length<=0){alert("There are no comparables!");return false}


var parameters;
/*OMITIDO
var param1='comparado="id"=>"'+datacompar["id"]+'","status"=>"'+datacompar["status2"]+'","lprice"=>'+datacompar["x_price"]+',"saleprice"=>'+datacompar["saleprice"]+',"lsqft"=>'+datacompar["lsqft"]+',"which_mod"=>"MLS_SF"&';
*/
//NEW
var param1='comparado="id"=>"'+datacompar["id"]+'","type"=>"COMP.SPL","which_mod"=>"<?=$_inModule?>"&';

var param2="comparables=";
var i2=0;
for (i=0;i<vFilas.length;i++)
{
	if (vFilas[i].taken==true)
		{
			if (i2>0){
				param2+="^";
			}	
			param2+='"price_sqft"=>'+(vFilas[i].saleprice/vFilas[i].lsqft)+',"Distance"=>'+vFilas[i].distance;
			i2++
		}
}

/*
if (i2<3) 
{
	alert("You must select at least three properties!")
	return
}
*/
	
	parameters=param1+param2;

		var ajax=nuevoAjax();	
		ajax.open("POST", "calc_mm_mp_mv.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send(parameters);
		ajax.onreadystatechange=function()
		{			
			if (ajax.readyState==4)	
			{
				var results=ajax.responseText;
				var arr_res=results.split("^")
				document.getElementById("mv").innerHTML=arr_res[0]
				document.getElementById("mm").innerHTML=arr_res[1]
				document.getElementById("mp").innerHTML=arr_res[2]
			}
		}

}


</script>
<style>
	div#registros {
	width:50%;
	clear:none;
	margin:0 0 0 5px;
	float:left;
	}
	</style>

<!--<style>
	div#registros {
	width:53%;
	clear:none;
	margin:0;
	float:left;
	}
	#gridpagingbar {
	/*width:15%;*/
	float:left;
	margin-right:0;
	/*display: inline;*/
	}
	</style>-->
<!-------------------------------------------------->


<!-- InstanceEndEditable -->
		</head>
<body>
	<?php TemplateBody( $toolbar_map, $toolbar_gps, $toolbar_legend, $toolbar_print,  $toolbar_label,  $toolbar_email, $toolbar_excel, $toolbar_filter, $toolbar_trash, $toolbar_graph, $toolbar_user);?>
<!-- InstanceBeginEditable name="contenido" -->

<div class="geniuswork"></div>
    <div class="visible" id="gridreg">
		
      <div id="mymapcontroldiv" class="mapoff" style=" border: 1px solid #4E9494;"></div>      
      <!--<div id="gridpagingbar"><a href="javascript:void(0)" onClick="javascript:showmap('mymapcontroldiv'); gridAjust(mymapcontroldiv.className);"><img src="img/mapbutton.jpg" alt="View Map"></a></div> -->	      
      <div id="myControl"></div>
     <!--<div id="myButton"><a href="javascript:void(0);" title="Legend" onClick="addControl(retorno);"></a></div>-->  		       
      
      
      <div id="registros"></div>      
    <div id="contcalcularcomp">
        <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0" id="calcularcomp">
          <tr>
            <td width="107" id="calculate"><div class="botoncalculate" onClick="calc_taken()"> Calculate</div></td>
      <td width="63"><div align="right" id="mv" >Unset
        
        </div></td>
      <td width="91"><div align="right" id="mm">Unset
        
        </div></td>
      <td width="81"><div align="right" id="mp" >Unset
        
        </div></td>
      <td width="18">&nbsp;</td>
    </tr>
        </table>
      </div>
      

	 <div id="inf_comparado" style="width:945; margin: 0 auto 0 auto;"  ></div>
      <div id="contenedor" class="scrollTableContainer"> 
        <div style="background-color:#FFFFE1;position: absolute;top:30%;left:40%;filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);"><img src="img/ac.gif">Please 
          wait, loading!...</div>
      </div>
    </div>
	<!-- PRUEBA--->
	      <!--div id="contenedor" class="scrollTableContainer" style="align:center;" > 
        <div style="background-color:#FFFFE1;position: absolute;top:30%;left:40%;filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);"><img src="img/ac.gif">Please 
          wait, loading!...</div>
      </div-->
	
    <div class="geniuswork"></div>
    <!--div id="mymapcontroldiv" style="align:center;position:relative;width:780px;height:280px;border: 1px solid #4E9494;"></div-->

<!--                CODIGO FINAL TABS            -->

<!-- InstanceEndEditable -->
	<?php TemplatePie(); ?>
<!-- InstanceBeginEditable name="EditRegion5" -->
<script>

//set map to run onload and dispose on exit
if (window.attachEvent) {
	window.attachEvent("onload", GetMap);
	window.attachEvent("onunload", MapDispose);	
} else {
	window.addEventListener("load", GetMap, false);
	window.addEventListener("unload", MapDispose, false);
}

show('menuppal'); 
show('advanced'); 
show('submenuadvanced');
desactivo('sssearch'); activo('sscomp');
desactivo('sscompact');

function run_grd()
{
	if (DONE==false) 
	{
		setTimeout("run_grd();",2000);
		return;
	}
	//	ordename("lsqft")
	ordename("lsqft","ASC","number")

}

function setup_compar()
{

//if (typeof vFilas!='object') 

if (DONE==false) 
{
	setTimeout("setup_compar();",2000);
	return;
}
	

	var myObj=document.getElementById("inf_comparado");
	
	getCasita("Subject","");	
	var myCompar=new Array();
	
var compar=new Array();
	
<? 
include("conexion.php");
$tipo="grid"; 
$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','numformatted','decimals','align');//Search
$ArDfsCT=array('idtc','name','tabla','title','type','size','numformatted','decimal','align');//Search
$ArIDCT=array(97,61,96,131,72,71,106,126,129,296,95,291,289,290); 
getCamptit("defa", $ArSqlCT, $ArDfsCT, $ArIDCT, "grid");
?>
compar=_defaStruct

	myCompar.push("<TABLE cellSpacing=0 cellPadding=0 align=center border=0 style=\"width:100%; border-collapse: collapse;\" >"+
	"<THEAD>"+
		"<TR style=\"background-color:#88AF3F;color:#FFFFFF\">"+
			"<TH class=\"cabecera\"  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >Sta.</TH>")
	myCompar.push("<TH class=\"cabecera\"  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\" >Info.</TH>");
				
				for(i=0; i< compar.length; i++){
				if(priv>=60 && priv<=69){
			//Si es un inversionista
					if(compar[i].name!='mlnumber'){
					myCompar.push("<TH class=\"cabecera\"  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >"+compar[i].title+" </TH>")
					}
				}else{
				myCompar.push("<TH class=\"cabecera\"  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >"+compar[i].title+" </TH>")
				}
			}

		myCompar.push("</TR></THEAD>")
		myCompar.push("<tbody><tr class=\"row1\" style=\"FONT-SIZE: 12px;color:#5E782C\"><td align=right  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  ><a href=\"javascript:void(0)\" onmouseout=\"document.getElementById('Pin_compara_"+ map.GUID+"').onmouseout();\" onclick=show_window(\"compara\")><img src=\"img/houses/"+lsImgCss[indImgCss].img+"\"></a></td>")
		var comparado,diff;
		

	//icono xima
	myCompar.push("<td align=center style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\" > <div class=\"suckerdiv\" id=\"conten_icon\" ></div> </td>");
	///
	
	for (i=0;i<compar.length;i++)
	{
		comparado=eval("datacompar."+compar[i].name);	
		diff=eval("datadiff."+compar[i].name);	
		
		if (compar[i].numformatted) //aplicar formato al numero ##,###.##
		{			
			comparado=numberFormat(comparado,compar[i].decimal);			
		}
		
		if(compar[i].name=='zip' || compar[i].name=='beds' || compar[i].name=='bath' || compar[i].name=='lsqft'){
		//Valores que pueden deter Diff 
			if(diff>0){
			myCompar.push("<td align=center style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"><a href=\"javascript:void(0)\" title=\"Click to see the information\" onclick=\"showDiff('0','"+lsImgCss[indImgCss].img+"','');\">"+comparado+"</a></td>");
			}
			else{
			myCompar.push("<td align=right  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >"+comparado+"</td>")
			}
		}// Fin de IF de diff
		
		else{
		//Valores comunes
			if(priv>=60 && priv<=69){
			//Si es un inversionista
				if(compar[i].name!='mlnumber'){
				myCompar.push("<td align=right  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >"+comparado+"</td>")
				}
			}else{
			//Si no es un inversionista
			myCompar.push("<td align=right  style=\"border-collapse:collapse;padding:0 5px 0 5px;white-space: nowrap;\"  >"+comparado+"</td>")
				}
		}//Fin de ELSE de valores comunes
		
	} // Fin de FOR
	
	myCompar.push("</tr></tbody></table>")

	myObj.innerHTML=myCompar.join("");
	
				document.getElementById("mv").innerHTML=numberFormat(datacompar.marketvalue,2)
				document.getElementById("mm").innerHTML=numberFormat(datacompar.marketmedia,2)

				document.getElementById("mp").innerHTML=numberFormat(datacompar.marketpctg,2)

	DONE=false

	datacompar.ind=""
	vFilas["compara"]=datacompar;

	DONE=true//=======================


	//verificar si hay comparables, imprimir no comparables
	if (vFilas.length<=0) {document.getElementById("contenedor").innerHTML="<br><br>There are no comparables!";return false}
//	run_grd()
	pagination(50,1)


	//7.Feb.2008, borrar esta cookie que proviene de users
	var mydate = new Date(); 
	mydate.setTime(mydate.getTime() - 1); 
	document.cookie = "countyUsr=; expires=" + mydate.toGMTString();
	//

}

</script>
<script type="text/javascript" src="includes/overlap.js"></script>
<!-- InstanceEndEditable -->
</body>
<script type="text/javascript" src="includes/tooltip.js"></script>
<!-- InstanceEnd --></html>