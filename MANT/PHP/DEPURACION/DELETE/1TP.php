<?php 
	include('../../../General/includes/conexion.php');
	$conn=conectar('flpalmbeach');
	
	// funcion que devuelve un array con los elementos de una hora
	function parteHora( $hora )
    {    
    	$horaSplit = explode(":", $hora);
		if( count($horaSplit) < 3 )
        {
        	$horaSplit[2] = 0;
		}
		return $horaSplit;
	}

    // funcion que devuelve la resta de dos horas en formato string
    function RestaHoras( $time1, $time2 )
    {
        list($hour1, $min1, $sec1) = parteHora($time1);
        list($hour2, $min2, $sec2) = parteHora($time2);
        return date('H:i:s', mktime( $hour1 - $hour2, $min1 - $min2, $sec1 - $sec2));
    }
	
	$TIMEINIHORA=date("H:i:s");	
	echo "<br><strong>TIEMPO INICIAL: ".$TIMEINIHORA."</strong><br>";
	//busco los parcelid que tienen duplicados y los listo.
	$que="SELECT * FROM psummary0 ORDER BY parcelid ASC,saledate ASC, saleprice ASC";
	$rest=mysql_query($que) or die('1T '.$que.mysql_error());
	
	$data=array();
	//recorro la lista de duplicados 
	while($r=mysql_fetch_assoc($rest)){
		$data[]=$r;
	}
	
	//print_r($data);
	$parcelid='';
	$data2=array();
	foreach($data as $k => $val){
		//if($k==0) print_r($val);
		if($k%10000==0)
			echo ($k+1).' Comp: '.$parcelid.' = '.$val['ParcelID'].'<br>';
		if($parcelid!='' && $parcelid!=$val['ParcelID']){
			$data2[] = $data[$k-1];
			//print_r($data[$k-1]);
			//echo '<br>';
		}
		$parcelid=$val['ParcelID'];
		
	}
	
	echo '<br><br>'.count($data2);
	echo "<br><strong>TIEMPO TOTAL DE EJECUCION DE TODOS LOS PHP's: ".RestaHoras(date("H:i:s"),$TIMEINIHORA)." | SEGUNDOS:";
	
	
	
/*	$num_reg=mysql_affected_rows();
	
	$num_reg_procesos=$num_reg;
	$actionID_procesos=-1;
	$ID_procesos=1;*/
?>