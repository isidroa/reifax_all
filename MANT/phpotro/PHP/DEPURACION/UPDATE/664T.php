<?php 
	//SPIDER que Sincroniza el ZIP de MLSRESIDENTIAL con el OZIP del PSUMMARY 
	echo "Hora Inicio: ".date("d-m-Y H:i:s")."<BR>";
	include ("../spiderweichert/dom/simple_html_dom.php");	

//////////////////////////////FUNCIONES//////////////////////////////
	////////Funcion para Restar las Horas////////
	if (!function_exists('RestarHoras')) {
		function RestarHoras($horaini,$horafin)
		{
			$horai=substr($horaini,0,2);
			$mini=substr($horaini,3,2);
			$segi=substr($horaini,6,2);

			$horaf=substr($horafin,0,2);
			$minf=substr($horafin,3,2);
			$segf=substr($horafin,6,2);

			$ini=((($horai*60)*60)+($mini*60)+$segi);
			$fin=((($horaf*60)*60)+($minf*60)+$segf);

			$dif=$fin-$ini;

			$difh=floor($dif/3600);
			$difm=floor(($dif-($difh*3600))/60);
			$difs=$dif-($difm*60)-($difh*3600);
			return date("H:i:s",mktime($difh,$difm,$difs));
		}
	}
	////////Funcion para hacer Spider con Yahoo////////
	if (!function_exists('yahoo')) {
		function yahoo($lat,$lon){
			//$url="http://where.yahooapis.com/geocode?q=".$row[1].",+".$row[2]."&gflags=R&appid=[yourappidhere]";
			$url="http://where.yahooapis.com/geocode?q=".$lat.",+".$lon."&gflags=R&appid=[yourappidhere]";
			$html=file_get_html($url,FALSE,$context);
			$C_I=0;//Contador de intentos
			if(!$html || $html == false){
				echo '<br/>Intentos Perdidos';
				do{
					$C_I++;
					sleep(1);
					$html = file_get_html($url);
				}while((!$html || $html == false) && $C_I<10);
			}
			$zip='';
			foreach($html->find('postal') as $d){
				$zip=trim($d->innertext);
				break;
			}
			//Vaciamos y Limpiamos la Variable en Memoria llamada $html
			$html->clear();			
			return $zip;
		}
	}
	////////Funcion para hacer Spider con Google////////
	if (!function_exists('google')) {
		function google($lat,$lon){
			//$url="http://maps.google.com/maps/api/geocode/xml?latlng=".$row[1].",".$row[2]."&sensor=false&callback=parseme";
			$url="http://maps.google.com/maps/api/geocode/xml?latlng=".$lat.",".$lon."&sensor=false&callback=parseme";
			$html=file_get_html($url,FALSE,$context);
			$C_I=0;//Contador de intentos
			if(!$html || $html == false){
				echo '<br/>Intentos Perdidos';
				do{
					$C_I++;
					sleep(1);
					$html = file_get_html($url);
				}while((!$html || $html == false) && $C_I<10);
			}
			$zip='';
			foreach($html->find('short_name') as $d){
				$T=trim($d->innertext);
				if($d->next_sibling()->innertext()=='postal_code'){
					$zip = $T;
					break;
				}
			}				
			//Vaciamos y Limpiamos la Variable en Memoria llamada $html
			$html->clear();						
			return $zip;
		}
	}
	////////Funcion para hacer Spider con Melissa////////
	if (!function_exists('melissa')) {
		function melissa($lat,$lon){
			//$url="http://www.melissadata.com/lookups/latlngzip4.asp?lat=".$row[1]."&lng=".$row[2]."&submit1=Submit";
			$url="http://www.melissadata.com/lookups/latlngzip4.asp?lat=".$lat."&lng=".$lon."&submit1=Submit";
			$html=file_get_html($url,FALSE,$context);
			$C_I=0;//Contador de intentos
			if(!$html || $html == false){
				echo '<br/>Intentos Perdidos';
				do{
					$C_I++;
					sleep(1);
					$html = file_get_html($url);
				}while((!$html || $html == false) && $C_I<10);
			}
			$zip='';
			foreach($html->find(".columresult") as $d){
				$T=trim($d->innertext);
				if($T=='Address'){
					$aux = $T=trim($d->next_sibling()->innertext());
					$zip = substr(str_replace('&nbsp;','',$aux),-10);
				}
			}				
			//Vaciamos y Limpiamos la Variable en Memoria llamada $html
			$html->clear();			
			return $zip;
		}
	}
//////////////////////////////FIN DE FUNCIONES//////////////////////////////
	$H_INI=date("H:i:s");
	$opts = array('http'=>array('method'=>"GET",'header'=>"Accept-language: en\r\n"."Cookie: foo=bar\r\n",'user_agent'=>'simple_html_dom'));
	$context = stream_context_create($opts);
	
	$tabla_Aux="latlong_aux";
	//Creo mi Tabla Auxiliar
	$query = "	
		CREATE TABLE IF NOT EXISTS $nom_bd.$tabla_Aux (
		  `parcelid` varchar(25) DEFAULT NULL,
		  `zip` varchar(25) DEFAULT NULL,
		  KEY `parcelid` (`parcelid`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";
	$result = mysql_query($query) or die ("664T: ERROR:A ".$query." - ".mysql_error());
	if(!$result)echo "Tabla Temporal No Creada<BR>"; else echo "Tabla Temporal Creada con Exito<BR>";
	
	//Limpio la Tabla
	$query="TRUNCATE TABLE $nom_bd.$tabla_Aux";
	//$result = mysql_query($query) or die("664T: ERROR:B ".$query." - ".mysql_error());
	if(!$result)echo "Tabla Temporal No Vaciada<BR>"; else echo "Tabla Temporal Vaciada con Exito<BR>";

	$query="SELECT a.PARCELID,a.LATITUDE,a.LONGITUDE,b.ozip FROM $nom_bd.latlong a
			INNER JOIN $nom_bd.psummary b ON (a.PARCELID=b.PARCELID)
			INNER JOIN $nom_bd.mlsresidential d ON (b.parcelid = d.parcelid)
			LEFT JOIN $nom_bd.$tabla_Aux c ON (a.PARCELID=c.PARCELID)
			WHERE (a.LATITUDE<>0 or a.LONGITUDE<>0) AND c.PARCELID IS NULL AND b.ozip<>d.zip
			LIMIT 5000"; //Este Query me Selecciona toda la Data del Psummary
	/*$query="SELECT a.parcelid,c.latitude,c.longitude,a.zip,b.zip FROM $condado.psummary a
			INNER JOIN $condado.mlsresidential b ON (a.parcelid=b.parcelid)
			INNER JOIN $condado.latlong c ON (a.parcelid=c.parcelid)
			LEFT JOIN $condado.$tabla_Aux d ON (b.PARCELID=d.PARCELID)
			WHERE a.zip<>b.zip AND d.PARCELID IS NULL";*/ //Este query Selecciona todo el psummary que esta en mlsresidential y en latlong pero a la ves no 
			//estan en la tabla auxiliar, esto se hace para evitar que si se detiene, comience de nuevo y cree registros repetidos
	/*$query="SELECT a.parcelid,c.latitude,c.longitude,a.zip,b.zip FROM $condado.psummary a
			LEFT JOIN $condado.mlsresidential b ON (a.parcelid=b.parcelid)
			INNER JOIN $condado.latlong c ON (a.parcelid=c.parcelid)
			LEFT JOIN $condado.latlong_aux d ON (a.parcelid=d.parcelid)
			WHERE ozip<>LEFT(owner_z,5) AND b.parcelid IS NULL AND d.parcelid IS NULL";*/ //este queri selecciona todos los parcelid de las propiedades que tengan
			//los zips diferentes entre el owner y la propiedad fisica como tal
	$res=mysql_query($query) or die("664T: ERROR: C: ".$query." - ".mysql_error());
	
	$query="";
	$C=0;$R=0;$GOOD=0;$BAD=0;
	while($row=mysql_fetch_array($res)){
		if($C>0 and trim($zip)<>''){$query.=",";}
		//busco por Yahoo
		$zip=yahoo($row[1],$row[2]);
		//si Yahoo no lo tiene lo busco por google
		if (strlen(trim($zip))<1){
			$zip=google($row[1],$row[2]);
		}
		//si yahoo ni google lo tiene lo busco por melissa
		if (strlen(trim($zip))<1){
			$zip=melissa($row[1],$row[2]);
		}		
		//si no lo tiene ninguno pues M.J.
		if($zip<>'')$query.="('".$row[0]."','".$zip."')";
		$C++;
		if($C==100){
			if($query<>''){ //si los 100 son diferentes a vacios
				if(substr($query,-1)==','){$query=substr($query,0,strlen($query)-1);} //Elimino si viene al final una coma (",")
				$query="INSERT INTO $nom_bd.$tabla_Aux (parcelid,zip) VALUES ".$query;
				mysql_query($query) or die("664T: ERROR: D: ".$query." - ".mysql_error());
				$C=0;
				$query="";
			}else{
				$C=0;
			}
		}
		$R++;
		echo "<BR>$R: Parcelid: $row[0] - Latitude: $row[1] - Longitude: $row[2] - Zip Psummary: $row[3] - Zip Mlsresidential: $row[4] - New Zip: $zip - ";
		if($row[3]==substr($zip,0,5)){
			echo "<span style='color:green'>GOOD</span>";
			$GOOD++;
		}else{
			echo "<span style='color:red'>BAD</span>";
			$BAD++;
		}
		echo " - Time: ".date("H:i:s");
		//sleep(1);
	}
	if($query<>""){
		if(substr($query,-1)==','){$query=substr($query,0,strlen($query)-1);} //Elimino si viene al final una coma (",")
		$query="INSERT INTO $nom_bd.$tabla_Aux (parcelid,zip) VALUES ".$query;
		mysql_query($query) or die("664T: ERROR: E: ".$query." - ".mysql_error());
	}

	//Elimino Mi Tabla Auxiliar
	$query="DROP TABLE IF EXISTS $nom_bd.$tabla_Aux";
	//$result = mysql_query($query) or die("664T: ERROR:F ".$query." - ".mysql_error());
	if(!$result)echo "<BR>Tabla Temporal No Eliminada"; else echo "<BR>Tabla Temporal Eliminada con Exito";
	
	$query="UPDATE $nom_bd.mlsresidential a
			INNER JOIN $nom_bd.psummary b ON (a.parcelid=b.parcelid)
			INNER JOIN $nom_bd.latlong c ON (a.parcelid=c.parcelid)
			INNER JOIN $nom_bd.$tabla_Aux d ON (a.parcelid=d.parcelid)
			SET
			  a.zip=LEFT(d.zip,5),
			  b.zip=d.zip,
			  b.ozip=LEFT(d.zip,5)
			WHERE a.zip<>b.ozip AND (c.LATITUDE<>0 OR c.LONGITUDE<>0)";
	mysql_query($query) or die("664T: ERROR: G: ".$query." - ".mysql_error());
	
	mysql_close($conexspider3);
	
	echo "<BR>Buenos: $GOOD";
	echo "<BR>Malos: $BAD";
	echo "<BR>Hora Fin: ".date("d-m-Y H:i:s")."<BR>Tiempo de Ejecucion: ".RestarHoras($H_INI,date("H:i:s"));
	//Otra URL para Estudiar
	//http://where.yahooapis.com/geocode?q=32.7780000000,+-96.7772000000&gflags=R&appid=[yourappidhere]
	//http://maps.google.com/maps/api/geocode/json?latlng=32.7699,-96.7842&sensor=false&callback=parseme
	//http://maps.google.com/maps/api/geocode/xml?latlng=32.7699,-96.7842&sensor=false&callback=parseme


	//$num_reg=mysql_affected_rows();
	//echo "<br />SPIDER que Sincroniza el ZIP de MLSRESIDENTIAL con el OZIP del PSUMMARY: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=664;
?>