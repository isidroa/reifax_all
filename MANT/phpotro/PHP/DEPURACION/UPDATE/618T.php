<?php 
	//Actualiza datos de venta del psummary con maxima venta del sales0 cuando fecha sales mayor que psummary 
	$qbd="DROP TABLE IF EXISTS `salesaux0`";
	mysql_query($qbd) or die("618T: ".$qbd.mysql_error());

	$qbd="CREATE TABLE  `salesaux0` (
		  `ParcelID` varchar(30) NOT NULL DEFAULT '0' COMMENT 'Codificacion del condado de la propiedad',
		  `date` varchar(8) NOT NULL COMMENT 'fecha venta',
		  `price` double(12,2) NOT NULL DEFAULT '0.00' COMMENT 'precio de venta',
		  `book` varchar(15) DEFAULT NULL COMMENT 'libro en el registro',
		  `page` varchar(15) DEFAULT NULL COMMENT 'pagina en el registro',
		  KEY `parcelid` (`ParcelID`) USING BTREE)";
	mysql_query($qbd) or die("618T: ".$qbd.mysql_error());
	
	$qbd="insert into salesaux0(ParcelID, date, price, book, page)
		select parcelid, date, price, book, page   from sales0 p
		where date=(select max(date) from sales0 s where s.parcelid=p.parcelid)";
	$res3=mysql_query($qbd) or die("613T: ".$qbd.mysql_error());
	$num_reg=mysql_affected_rows();
	
	$que="update psummary p inner join salesaux0 s on p.parcelid=s.parcelid set
		p.saledate=s.date, p.saleprice=s.price, p.book=s.book, p.page=s.page
		where s.date>p.saledate";
	mysql_query($que) or die("618T: ".$que.mysql_error());

	$num_reg=mysql_affected_rows();
	echo "<br />Actualiza datos de venta del psummary con maxima venta del sales0 cuando fecha sales mayor que psummary: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=618;
?>