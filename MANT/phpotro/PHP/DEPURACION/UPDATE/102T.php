<?php 
	//Actualiza entrydate con distribucion entre dos fechas
	
	/*//PARA PRUEBAS
	require('../../../general/includes/conexion.php');
	$conn=conectar('flbroward');	
	$total=800;
	$diacomenzar='20100715';
	$que="select $diacomenzar,datediff(NOW(),date($diacomenzar))";
	$result1=mysql_query($que) or die('102T '.$que.mysql_error());
	$rest1 = mysql_fetch_array($result1);
	$diacomenzar = $rest1[0];
	$inter = $rest1[1];
	*/

	//Actualiza file_date de pendes0 provisional para los nuevos pendes que no estaba en el calculo anterior 
	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%POTFPO'
				limit 1";
	$rest4=mysql_query($que) or die("589T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblRes=strtolower($r['tblResult']);	
	if($tblRes<>'')
	{
		$que="	UPDATE pendes0 p
				left join $tblRes n on n.parcelid=p.parcelid
				SET  p.file_date='21001231'
				WHERE (length(p.entrydate)=0 or p.entrydate is null or p.entrydate='' )
				and n.parcelid is null";
		mysql_query($que) or die("102T: ".$que.mysql_error());
	}
	
	//BUSCANDO TODOS LOS QUE NO TIENEN ENTRYDATE Y QUE TENGAN FILEDATE
	$que="SELECT entrydate 
			FROM pendes0 
			WHERE (length(entrydate)=0 or entrydate is null or entrydate='' )  AND 
			(length(FILE_DATE)>0 and FILE_DATE is not null)
			ORDER BY file_date DESC";
	$result=mysql_query($que) or die('102T '.$que.mysql_error());
	$total = mysql_num_rows($result);
	
	// SACO LA MAXIMA FECHA , Y LA DIFERENCIA ENTRE LA MAXIMAFECHA Y HOY
	$que="select replace(MAX(entrydate),'-',''),
				datediff(NOW(),replace(MAX(entrydate),'-',''))
			from pendes0";
	$result1=mysql_query($que) or die('102T '.$que.mysql_error());
	$rest1 = mysql_fetch_array($result1);
	
	if(strlen($rest1[0])>0 && $rest1[0]<>'')
	{//SI EXISTE FECHA TOMO LOS VALORES DE FECHA Y DIFERENCIA
		$diacomenzar = $rest1[0];
		$inter = $rest1[1];
	}
	else
	{//SI NO EXISTE FECHA ESTO ES PARA EL CASO DE LOS CONDADOS NUEVOS
		$que="select replace(date_sub(curdate(), interval 30 DAY ),'-',''),datediff(NOW(),replace(date_sub(curdate(), interval 30 DAY ),'-',''))";
		$result1=mysql_query($que) or die('102T '.$que.mysql_error());
		$rest1 = mysql_fetch_array($result1);
		$diacomenzar = $rest1[0];
		$inter = $rest1[1];
	}


	//VALIDAR SI DIA COMENZAR  ES VIERNES PASARLO A LUNES

	$dia=substr($diacomenzar,6,2);
	$mes=substr($diacomenzar,4,2);
	$ano=substr($diacomenzar,0,4);
	
	/*if($inter==6)
		$inter--;
	elseif($inter>=7 && $inter<=13)
		$inter-=3;
	elseif($inter>=14 && $inter<=20)
		$inter-=5;
	elseif($inter>=21 && $inter<=50)
		$inter-=11;*/
	
	$inter = $inter-(round($inter/7))*2;	
	if($inter>0) $limit = round($total/$inter,0); else $limit=0;
	if($limit>0) $for = round($total/$limit,0); else $for=0;
	
	
   	for($i=0; $i<$for; $i++)
	{
		//echo 
		$diacomenzar=date('Ymd', strtotime("+1 day", mktime(0,0,0,$mes,$dia,$ano)));
		$dia=substr($diacomenzar,6,2);
		$mes=substr($diacomenzar,4,2);
		$ano=substr($diacomenzar,0,4);
		
		while(date('N',mktime(0,0,0,$mes,$dia,$ano))==6 || date('N',mktime(0,0,0,$mes,$dia,$ano))==7)
		{//VALIDANDO QUE SEA SABADO O DOMINGO Y LE SUMA 1 DIA 
			$diacomenzar=date('Ymd', strtotime("+1 day", mktime(0,0,0,$mes,$dia,$ano)));
			$dia=substr($diacomenzar,6,2);
			$mes=substr($diacomenzar,4,2);
			$ano=substr($diacomenzar,0,4);
		}
			
		if(date('Ymd')<date('Ymd',mktime(0,0,0,$mes,$dia,$ano)))
		{//VALIDANDO QUE LA FECHA GENERADO NO SEA MAYOR A HOY
			$diacomenzar = date('Ymd');
			$dia=substr($diacomenzar,6,2);
			$mes=substr($diacomenzar,4,2);
			$ano=substr($diacomenzar,0,4);
		}
		$que="UPDATE pendes0  
			SET entrydate='".$diacomenzar."'
			WHERE  (length(entrydate)=0 or entrydate is null or entrydate='' )  AND 
			(length(FILE_DATE)>0 and FILE_DATE is not null)";
		if($i<($for-1))
			$que.=" LIMIT ".$limit;
		//echo "<br/>".$que;
		mysql_query($que) or die('102T '.$que.mysql_error());
	}
	$num_reg=mysql_affected_rows();

	//esto es una correccion de smith que les ponia file_date a los nuevos registros
	//al comienzo de este php los obligue a que se le ponga entrydate
	$que="	UPDATE pendes0 SET  file_date=''
			WHERE file_date='21001231' ";
	mysql_query($que) or die("102T: ".$que.mysql_error());
	
	$num_reg_procesos=$num_reg;
	$actionID_procesos=-1;
	$ID_procesos=102;

?>