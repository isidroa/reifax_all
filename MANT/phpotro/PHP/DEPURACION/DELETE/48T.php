<?php 

	//busco los parcelid que tienen duplicados y los listo.
/*
	$que="	SELECT parcelid,COUNT(parcelid) cant 
				FROM pendes0  
				GROUP BY parcelid 
				HAVING COUNT(parcelid)>1 
				ORDER BY COUNT(parcelid) DESC
			";
	$rest=mysql_query($que) or die('48T '.$que.mysql_error());
	
	//recorro la lista de duplicados 
	while($r=mysql_fetch_array($rest)){
		//ordeno cada conjunto de parcelid duplicados de manera que se ordene de menor a mayor 
		//por el saledate y saleprice, de modo que la fecha mas actual y de ser iguales fechas 
		//el mayor precio quede de ultimo, y asi borro todos los registros menos el ultimo.
		
		$que='DELETE FROM pendes0 WHERE parcelid="'.$r['parcelid'].'" ORDER BY pof ASC,file_date ASC LIMIT '.($r[1]-1);
		mysql_query($que) or die('48T '.$que.mysql_error());
	}
	$num_reg=mysql_affected_rows();
*/
	$que="	TRUNCATE TABLE pendes1";
	$rest=mysql_query($que) or die('48T '.$que.mysql_error());

/////////////////////////////////////////Comentado por JEsus y El Sr Freddy/////////////////////////////////////////
///////////////////////////////////////////////Proceso Viejo de Smith///////////////////////////////////////////////
/*	$order=' DESC ';
	if(substr(strtolower($nom_bd),0,2)=='ga' || substr(strtolower($nom_bd),0,2)=='tx')$order='';
	
	$que="	INSERT INTO pendes1 
			SELECT * 
			FROM pendes0  
			GROUP BY parcelid 
			ORDER BY  pof ASC,file_date $order
			";*/
/////////////////////////////////////////Agregado por JEsus y El Sr Freddy/////////////////////////////////////////
///////////////////////////////////////////////Proceso Viejo de Jesus///////////////////////////////////////////////
/*	$que="INSERT INTO pendes1
			SELECT * FROM
				(SELECT * FROM
					pendes0 a
				ORDER BY a.file_date DESC) temporal
			GROUP BY temporal.parcelid";
	$rest=mysql_query($que) or die('48T '.$que.mysql_error());
	
	
	$que="	TRUNCATE TABLE pendes0";
	$rest=mysql_query($que) or die('48T '.$que.mysql_error());
	
	$que="	INSERT INTO pendes0
			SELECT * FROM pendes1";
	$rest=mysql_query($que) or die('48T '.$que.mysql_error());
	$num_reg=mysql_affected_rows();
*/
	///////////////////Creo mi tabla temporal a utilizar con la misma estructura de pendes0///////////////////
	$query="SHOW CREATE TABLE pendes0";
	$result = mysql_query($query) or die("ERROR: 48|A: ".$query." - ".mysql_error());
	$result = mysql_fetch_array($result);
	$create = str_replace('`pendes0`','`pendes2`',$result[1]);
	$create = str_replace('CREATE TABLE ','CREATE TABLE IF NOT EXISTS ',$create);
	mysql_query($create) or die("ERROR: 48|B: ".$create." - ".mysql_error());
	
	///////////////////Vacio mi Tabla Pendes1///////////////////
	$query = "TRUNCATE TABLE pendes1";
	mysql_query($query) or die("ERROR: 48|C: ".$query." - ".mysql_error());
	
	///////////////////Inserto todos los Registros Duplicados en pendes1, ojo todos los duplicados por parcelid no agrupados///////////////////
	$query = "INSERT INTO pendes1
			SELECT a.* FROM pendes0 a INNER JOIN
				(SELECT * FROM pendes0 group by parcelid having count(*)>1) temporal ON (a.parcelid=temporal.parcelid)
			ORDER BY a.parcelid ASC, a.file_date DESC, a.entrydate DESC";
	mysql_query($query) or die("ERROR: 48|D: ".$query." - ".mysql_error());

	echo "<BR>Total de Insertados a Pendes1 ".mysql_affected_rows();
	///////////////////Borro todos los Registros de mi Tabla Pendes0 que estan duplicados, ya que esos los tengo en pendes1///////////////////
	$query = "DELETE pendes0 FROM pendes0 
				INNER JOIN pendes1  ON (pendes0.parcelid=pendes1.parcelid)";
	mysql_query($query) or die("ERROR: 48|E: ".$query." - ".mysql_error());
	
	///////////////////Inserto en pendes2 un unico registro de los duplicados quedando por defecto el de mayor fecha del campo File_date///////////////////
	$query = "INSERT INTO pendes2 SELECT * FROM pendes1 GROUP BY parcelid";
	mysql_query($query) or die("ERROR: 48|F: ".$query." - ".mysql_error());
	
	///////////////////Actualizo el Registro unico de Pendes2 con valores del pendes1 que tienen los demas duplicados para///////////////////
	///////////////////crear un unico registro mas solido, las consideraciones a tomas fueron:///////////////////
	/*
		1.- Listing_date con fecha mas cerca de la de hoy. Este criterio se realizo en el paso de la linea 17
		2.- Obtengo el judgedate con fecha mas lejana y se lo coloco al registro de pendes2
		3.- si el Entrydate del registro estandar esta vacio, lo busco en los demas duplicados y extraigo el de fecha mayor
			para colocarcelo al registro de pendes2, de lo contrario lo dejo vacio y el sistema se encarga de asiganarle fecha
		4.- Si el case_nomber es vacio, lo extraigo de los demas registros Duplicados ya que como es la misma propiedad el case nomber es el mismo,
			si de lo contrario no esta vacio dejo el mismo
		5.- Si el defowner esta vacio, lo extraigo de los demas registros duplicados ya que como es una misma propiedad debe ser el mismo,
			pero si son diferentes debo tomar el defowner con el listing_date mayor (file_date), si de lo contrario no esta vacio dejo el mismo
	*/
	$query = "UPDATE pendes2 a
				INNER JOIN pendes1 b ON (a.parcelid=b.parcelid)
				SET
				a.judgedate=IF(LENGTH(TRIM(a.judgedate))<1,(SELECT MAX(judgedate) FROM pendes1 WHERE parcelid=a.parcelid),a.judgedate),
				a.case_numbe=IF(LENGTH(TRIM(a.case_numbe))<3,(SELECT case_numbe FROM pendes1 WHERE parcelid=a.parcelid AND LENGTH(TRIM(case_numbe))>3 LIMIT 1),a.case_numbe),
				a.defowner1=IF(LENGTH(TRIM(a.defowner1))<1,(SELECT defowner1 FROM pendes1 WHERE parcelid=a.parcelid AND LENGTH(TRIM(defowner1))>0 ORDER BY entrydate DESC LIMIT 1),a.defowner1),
				a.entrydate=IF(LENGTH(TRIM(a.entrydate))<1,(SELECT MAX(entrydate) FROM pendes1 WHERE parcelid=a.parcelid),a.entrydate)";
	
	mysql_query($query) or die("ERROR: 48|G: ".$query." - ".mysql_error());
	
	///////////////////Inserto en Pendes0 los Registros Duplicados Depurados (un unico registro)///////////////////
	$query = "INSERT INTO pendes0 SELECT * from pendes2";
	mysql_query($query) or die("ERROR: 48|H: ".$query." - ".mysql_error());
	
	///////////////////Elimino mi tabla Temporal Pendes2///////////////////
	$query="DROP TABLE pendes2";
	mysql_query($query) or die("ERROR: 48|I: ".$query." - ".mysql_error());	
	
	$num_reg_procesos=$num_reg;
	$actionID_procesos=-1;
	$ID_procesos=48;
?>