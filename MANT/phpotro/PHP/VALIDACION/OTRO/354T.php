<?php 
	//Generar el EXRAY del condado en cuestion
function porcentaje($val){
	return number_format($val, 2, '.', '');
}

$tipo_xray=array('','00','01','02','03','04','08','11','99');

foreach($tipo_xray as $k => $val){
	$total=$total_P=$total_F=$total_sold=$total_sold_P=0;
	$total_no_sold_P=$total_sold_F=$total_no_sold_F=0;
	$total_P_0=$total_P_10=$total_P_20=$total_P_30=0;
	$total_sold_3=$total_owner_Y=$total_owner_N=0;
	$total_UD=$total_no_sold_UD=$total_sold_UD=0;
	$total_sold_PE=$total_sold_PE_F=$total_sold_PE_P=0;
	$cs=$dom=$domA=0;
	$sp12=$sp9=$sp6=$sp3=0;
	$total_P_0_mas=0;
	$asp12=$asp9=$asp6=$asp3=array();
	$qtCC1=$qtCC2=$qtCC3=$qtCC4=$qtCC5=$qtCC6=$qtCC7=$qtCC8=$qtCC9=$qtCC10=$qtCC11=$qtCC12=0;
	$qtCS1=$qtCS2=$qtCS3=$qtCS4=$qtCS5=$qtCS6=$qtCS7=$qtCS8=$qtCS9=$qtCS10=$qtCS11=$qtCS12=0;
	
	$latlong=0;
	
	$data=array();
	
	$fechames[11]=date('Ym');
	for($x=1; $x<12; $x++){
		$fechames[11-$x]=date('Ym',strtotime("-".$x." month"));
	}
	
	$query="SELECT p.parcelid, p.astatus, p.saledate, p.saleprice, mv.pendes, mv.ownocc, mv.debttv, mv.marketpctg, mv.sold, r.status, r.dom, ll.latitude, ll.longitude 
			FROM  psummary p 
			LEFT JOIN marketvalue mv ON (p.parcelid=mv.parcelid) 
			LEFT JOIN rtmaster r ON (p.parcelid=r.parcelid) 
			LEFT JOIN latlong ll on (p.parcelid=ll.parcelid)";		
	
	if($val!=''){
		$query.=" WHERE p.xcode='$val'";
	}
	
	//echo $query;
	$actual=date('Ym').'01';
	$desde=date('Ym',strtotime("-3 month")).'01';
	$desde6=date('Ym',strtotime("-6 month")).'01';
	$desde9=date('Ym',strtotime("-9 month")).'01';
	$desde12=date('Ym',strtotime("-12 month")).'01';
	
	$result=mysql_query($query) or die ($query.' '.mysql_error());
	while($r=mysql_fetch_array($result))
	{
		$contar=true;
		if($contar)
		{
			$total++;
			if($r['pendes']=='F' && $r['sold']=='N'){
				$total_F++;
				if($r['astatus']=='A')
					$total_sold_F++;
				else
					$total_no_sold_F++;
			}
			if($r['pendes']=='P' && $r['sold']=='N'){
				$total_P++;
				if($r['astatus']=='A')
					$total_sold_P++;
				else
					$total_no_sold_P++;
				
				if($r['debttv']<0)
					$total_P_0++;
				if($r['debttv']>=0 && $r['debttv']<100)
					$total_P_0_mas++;
				if($r['debttv']>=30 && $r['debttv']<100)
					$total_P_30++;
				
			}
			if($r['pendes']=='N' && $r['debttv']<0){
				$total_UD++;
				if($r['astatus']=='A'){
					$total_sold_UD++;
				}else{
					$total_no_sold_UD++;
				}
			}
			if($r['astatus']=='A'){
				$total_sold++;
				$domA+=$r['dom'];
				if($r['marketpctg']>=30){
					$total_sold_PE++;
					if($r['pendes']=='F')
						$total_sold_PE_F++;
					if($r['pendes']=='P')
						$total_sold_PE_P++;
				}
			}
			
			if($r['astatus']=='CS' || $r['astatus']=='CC'){
				if($r['status']=='CS'){
					$cs++;
					$dom+=$r['dom'];
				}
				if($r['saledate']>=$desde && $r['saledate']<=$actual && $r['saleprice']>=19000){
					$sp3++;
					$asp3[]=$r['saleprice'];
				}
				if($r['saledate']>=$desde6 && $r['saledate']<=$actual && $r['saleprice']>=19000){
					$sp6++;
					$asp6[]=$r['saleprice'];
				}
				if($r['saledate']>=$desde9 && $r['saledate']<=$actual && $r['saleprice']>=19000){
					$sp9++;
					$asp9[]=$r['saleprice'];
				}
				if($r['saledate']>=$desde12 && $r['saledate']<=$actual && $r['saleprice']>=19000){
					$sp12++;
					$asp12[]=$r['saleprice'];
				}
			}
			
			if($r['astatus']=='CS' || $r['astatus']=='CC'){
				if($r['saledate']>=$desde6 && $r['saledate']<=$actual && $r['saleprice']>=19000){
					$total_sold_3++;
				}
			}
			
			$inifecha=$fechames[0].'01'; $finfecha=$fechames[0].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS1++;
				
				if($r['astatus']=='CC') $qtCC1++;
			}
			
			$inifecha=$fechames[1].'01'; $finfecha=$fechames[1].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS2++;
				
				if($r['astatus']=='CC') $qtCC2++;
			}
			
			$inifecha=$fechames[2].'01'; $finfecha=$fechames[2].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS3++;
				
				if($r['astatus']=='CC') $qtCC3++;
			}
			
			$inifecha=$fechames[3].'01'; $finfecha=$fechames[3].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS4++;
				
				if($r['astatus']=='CC') $qtCC4++;
			}
			
			$inifecha=$fechames[4].'01'; $finfecha=$fechames[4].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS5++;
				
				if($r['astatus']=='CC') $qtCC5++;
			}
			
			$inifecha=$fechames[5].'01'; $finfecha=$fechames[5].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS6++;
				
				if($r['astatus']=='CC') $qtCC6++;
			}
			
			$inifecha=$fechames[6].'01'; $finfecha=$fechames[6].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS7++;
				
				if($r['astatus']=='CC') $qtCC7++;
			}
			
			$inifecha=$fechames[7].'01'; $finfecha=$fechames[7].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS8++;
				
				if($r['astatus']=='CC') $qtCC8++;
			}
			
			$inifecha=$fechames[8].'01'; $finfecha=$fechames[8].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS9++;
				
				if($r['astatus']=='CC') $qtCC9++;
			}
			
			$inifecha=$fechames[9].'01'; $finfecha=$fechames[9].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS10++;
				
				if($r['astatus']=='CC') $qtCC10++;
			}
			
			$inifecha=$fechames[10].'01'; $finfecha=$fechames[10].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS11++;
				
				if($r['astatus']=='CC') $qtCC11++;
			}
			
			$inifecha=$fechames[11].'01'; $finfecha=$fechames[11].'31';
			if($r['saledate']>=$inifecha && $r['saledate']<=$finfecha && $r['saleprice']>=19000){
				if($r['astatus']=='CS') $qtCS12++;
				
				if($r['astatus']=='CC') $qtCC12++;
			}
			
			if($r['ownocc']=='Y' || $r['ownocc']=='y')
				$total_owner_Y++;
			if($r['ownocc']=='N' || $r['ownocc']=='n')
				$total_owner_N++;
		}
	}
	$total_D=$total_F+$total_P;
	if($total>0){
		$pct_D=porcentaje(($total_D*100)/$total);
		$pct_F=porcentaje(($total_F*100)/$total);
		$pct_P=porcentaje(($total_P*100)/$total);
		$pct_sold=porcentaje(($total_sold*100)/$total);
		$pct_owner_N=porcentaje(($total_owner_N*100)/$total);
		$pct_owner_Y=porcentaje(($total_owner_Y*100)/$total);
		$pct_UD=porcentaje(($total_UD*100)/$total);
	}else{
		$pct_UD=$pct_D=$pct_F=$pct_P=$pct_sold=$pct_owner_Y=$pct_owner_N=0.00;
	}
	
	if($total_F>0){
		$pct_sold_F=porcentaje(($total_sold_F*100)/$total_F);
		$pct_no_sold_F=porcentaje(($total_no_sold_F*100)/$total_F);
	}else{
		$pct_sold_F=$pct_no_sold_F=0.00;
	}
	
	if($total_P>0){
		$pct_sold_P=porcentaje(($total_sold_P*100)/$total_P);
		$pct_no_sold_P=porcentaje(($total_no_sold_P*100)/$total_P);
		$pct_P_0=porcentaje(($total_P_0*100)/$total_P);
		$pct_P_0_mas=porcentaje(($total_P_0_mas*100)/$total_P);
		$pct_P_30=porcentaje(($total_P_30*100)/$total_P);
	}else{
		$pct_sold_P=$pct_no_sold_P=$pct_P_0=$pct_P_0_mas=$pct_P_30=0.00;
	}
	
	if($total_UD>0){
		$pct_sold_UD=porcentaje(($total_sold_UD*100)/$total_UD);
		$pct_no_sold_UD=porcentaje(($total_no_sold_UD*100)/$total_UD);
	}else{
		$pct_sold_UD=$pct_no_sold_UD=0.00;
	}
	
	if($total_sold>0){
		$pct_sold_PE=porcentaje(($total_sold_PE*100)/$total_sold);
		$pct_sold_PE_F=porcentaje(($total_sold_PE_F*100)/$total_sold);
		$pct_sold_PE_P=porcentaje(($total_sold_PE_P*100)/$total_sold);
		$a_days_a=porcentaje($domA/$total_sold);
	}else{
		$a_days_a=$pct_sold_PE=$pct_sold_PE_F=$pct_sold_PE_P=0.00;
	}
	
	$a_sold_3=round($total_sold_3/6,2);
	
	if($cs>0)
		$a_days=porcentaje($dom/$cs);
	else
		$a_days=0.00;
		
	
	sort($asp3,SORT_NUMERIC);
	sort($asp6,SORT_NUMERIC);
	sort($asp9,SORT_NUMERIC);
	sort($asp12,SORT_NUMERIC);
	
	if($sp3>0){
		$a_asp3=porcentaje(array_sum($asp3)/$sp3);
		if($sp3%2==0){ 
			$m=$sp3/2;
			$m_asp3=$asp3[$m-1];
			$m_asp3+=$asp3[$m];
			$m_asp3=porcentaje($m_asp3/2);
		}else{
			$m=($sp3+1)/2;
			$m_asp3=porcentaje($asp3[$m-1]);
		}
		
	}else
		$a_asp3=$m_asp3=0.00;

	if($sp6>0){
		$a_asp6=porcentaje(array_sum($asp6)/$sp6);
		if($sp6%2==0){ 
			$m=$sp6/2;
			$m_asp6=$asp6[$m-1];
			$m_asp6+=$asp6[$m];
			$m_asp6=porcentaje($m_asp6/2);
		}else{
			$m=($sp6+1)/2;
			$m_asp6=porcentaje($asp6[$m-1]);
		}
		
	}else
		$a_asp6=$m_asp6=0.00;
	
	if($sp9>0){
		$a_asp9=porcentaje(array_sum($asp9)/$sp9);
		if($sp9%2==0){ 
			$m=$sp9/2;
			$m_asp9=$asp9[$m-1];
			$m_asp9+=$asp9[$m];
			$m_asp9=porcentaje($m_asp9/2);
		}else{
			$m=($sp9+1)/2;
			$m_asp9=porcentaje($asp9[$m-1]);
		}
		
	}else
		$a_asp9=$m_asp9=0.00;
	
	if($sp12>0){
		$a_asp12=porcentaje(array_sum($asp12)/$sp12);
		if($sp12%2==0){ 
			$m=$sp12/2;
			$m_asp12=$asp12[$m-1];
			$m_asp12+=$asp12[$m];
			$m_asp12=porcentaje($m_asp12/2);
		}else{
			$m=($sp12+1)/2;
			$m_asp12=porcentaje($asp12[$m-1]);
		}
		
	}else
		$a_asp12=$m_asp12=0.00;
	
	if($total_sold_3>0)
		$invM=porcentaje($total_sold/$a_sold_3);
	else
		$invM=0.00;

	$query="INSERT INTO exray (total,distress,pctdistress,preforeclosure,pctpreforeclosure,foreclosure,pctforeclosure,upsidedown,pctupsidedown,sale,pctsale,ownerY,pctownerY,ownerN,pctownerN,
								total_sold_P,pcttotal_sold_P,total_no_sold_P,pcttotal_no_sold_P,total_P_0,pcttotal_P_0,total_P_0_mas,pcttotal_P_0_mas,total_P_30,pcttotal_P_30,total_sold_F,
								pcttotal_sold_F,total_no_sold_F,pcttotal_no_sold_F,total_sold_UD,pcttotal_sold_UD,total_no_sold_UD,pcttotal_no_sold_UD,total_sold_PE,pcttotal_sold_PE,
								total_sold_PE_P,pcttotal_sold_PE_P,total_sold_PE_F,pcttotal_sold_PE_F,invM,a_days,total_sold_3,a_sold_3,a_asp12,m_asp12,a_asp9,m_asp9,a_asp6,m_asp6,a_asp3,m_asp3,proptype,fecha)
			VALUE (	$total,$total_D,$pct_D,$total_P,$pct_P,$total_F,$pct_F,$total_UD,$pct_UD,$total_sold,$pct_sold,$total_owner_Y,$pct_owner_Y,$total_owner_N,$pct_owner_N,$total_sold_P,$pct_sold_P,
					$total_no_sold_P,$pct_no_sold_P,$total_P_0,$pct_P_0,$total_P_0_mas,$pct_P_0_mas,$total_P_30,$pct_P_30,$total_sold_F,$pct_sold_F,$total_no_sold_F,$pct_no_sold_F,$total_sold_UD,
					$pct_sold_UD,$total_no_sold_UD,$pct_no_sold_UD,$total_sold_PE,$pct_sold_PE,$total_sold_PE_P,$pct_sold_PE_P,$total_sold_PE_F,$pct_sold_PE_F,$invM,$a_days,$total_sold_3,$a_sold_3,
					$a_asp12,$m_asp12,$a_asp9,$m_asp9,$a_asp6,$m_asp6,$a_asp3,$m_asp3,'$val',NOW())";

	
	//echo "<br>".$query."<br>";
	mysql_query($query) or die ($query.' '.mysql_error());
	
	if($val==''){		
		ingreso(492,$county,0,$total);
		ingreso(493,$county,0,$total_D);
		ingreso(494,$county,0,$pct_D);
		ingreso(495,$county,0,$total_P);
		ingreso(496,$county,0,$pct_P);
		ingreso(497,$county,0,$total_F);
		ingreso(498,$county,0,$pct_F);
		ingreso(499,$county,0,$total_UD);
		ingreso(500,$county,0,$pct_UD);
		ingreso(501,$county,0,$total_sold);
		ingreso(502,$county,0,$pct_sold);
		ingreso(503,$county,0,$total_owner_Y);
		ingreso(504,$county,0,$pct_owner_Y);
		ingreso(505,$county,0,$total_owner_N);
		ingreso(506,$county,0,$pct_owner_N);
		ingreso(507,$county,0,$total_sold_P);
		ingreso(508,$county,0,$pct_sold_P);
		ingreso(509,$county,0,$total_no_sold_P);
		ingreso(510,$county,0,$pct_no_sold_P);
		ingreso(511,$county,0,$total_P_0);
		ingreso(512,$county,0,$pct_P_0);
		ingreso(513,$county,0,$total_P_0_mas);
		ingreso(514,$county,0,$pct_P_0_mas);
		ingreso(515,$county,0,$total_P_30);
		ingreso(516,$county,0,$pct_P_30);
		ingreso(517,$county,0,$total_sold_F);
		ingreso(518,$county,0,$pct_sold_F);
		ingreso(519,$county,0,$total_no_sold_F);
		ingreso(520,$county,0,$pct_no_sold_F);
		ingreso(521,$county,0,$total_sold_UD);
		ingreso(522,$county,0,$pct_sold_UD);
		ingreso(523,$county,0,$total_no_sold_UD);
		ingreso(524,$county,0,$pct_no_sold_UD);
		ingreso(525,$county,0,$total_sold_PE);
		ingreso(526,$county,0,$pct_sold_PE);
		ingreso(527,$county,0,$total_sold_PE_P);
		ingreso(528,$county,0,$pct_sold_PE_P);
		ingreso(529,$county,0,$total_sold_PE_F);
		ingreso(530,$county,0,$pct_sold_PE_F);
		ingreso(531,$county,0,$invM);
		ingreso(532,$county,0,$a_days);
		ingreso(533,$county,0,$total_sold_3);
		ingreso(534,$county,0,$a_sold_3);
		ingreso(535,$county,0,$a_asp12);
		ingreso(536,$county,0,$m_asp12);
		ingreso(537,$county,0,$a_asp9);
		ingreso(538,$county,0,$m_asp9);
		ingreso(539,$county,0,$a_asp6);
		ingreso(540,$county,0,$m_asp6);
		ingreso(541,$county,0,$a_asp3);
		ingreso(542,$county,0,$m_asp3);
		ingreso(669,$county,0,$qtCC12);
		ingreso(670,$county,0,$qtCC11);
		ingreso(671,$county,0,$qtCC10);
		ingreso(672,$county,0,$qtCC9);
		ingreso(673,$county,0,$qtCC8);
		ingreso(674,$county,0,$qtCC7);
		ingreso(675,$county,0,$qtCC6);
		ingreso(676,$county,0,$qtCC5);
		ingreso(677,$county,0,$qtCC4);
		ingreso(678,$county,0,$qtCC3);
		ingreso(679,$county,0,$qtCC2);
		ingreso(681,$county,0,$qtCC1);
	}
}

	$num_reg=mysql_affected_rows();

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=354;
?>


