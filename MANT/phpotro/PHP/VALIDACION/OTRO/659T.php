<?php 
	//Generar el Discount del Condado en Cuestion 
	if (!function_exists('porcentaje')) {
		function porcentaje($val){
			return number_format($val, 2, '.', '');
		}
	}
	
	$tipo_xray=array('','00','01','02','03','04','08','11','99');

	foreach($tipo_xray as $k => $val){
		$p_sold=$p_avg_day_ss=$p_avg_amo_dis=$p_avg_pct_dis=0;
		$f_sold=$f_avg_day_safd=$f_avg_day_sajd=$f_avg_amo_dis=$f_avg_pct_dis=0;

		//Pre-Foreclosed Sold
		$query = "SELECT count(*) FROM marketvalue m 
		INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
		WHERE m.pendes='P' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$p_sold=$r[0];
		
		//Average Days of Shortsale
		$query = "SELECT sum(datediff(NOW(),STR_TO_DATE(p.file_date,'%Y%m%d')))/count(*) 
		FROM pendes p
		INNER JOIN psummary ps ON (p.parcelid=ps.parcelid)
		INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
		WHERE m.pendes='P' and m.sold='S' and length(p.file_date)>0 and (m.totalpenmort-ps.saleprice) > 0";
		if($val != '')
			$query.=" AND ps.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$p_avg_day_ss=$r[0];
		
		//Average total kwon debt
		$query = "SELECT sum(m.totalpenmort)/count(*)
		FROM marketvalue m 
		INNER JOIN psummary p ON (m.parcelid=p.parcelid)
		WHERE m.pendes='P' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$deuda=$r[0];
		
		//Average total sale price
		$query = "SELECT sum(p.saleprice)/count(*)
		FROM psummary p
		INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
		WHERE m.pendes='P' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$venta=$r[0];
		
		//Average Amount of Discount
		$p_avg_amo_dis = $deuda - $venta;
		//Average Percentage of Discount
		if($deuda>0) $p_avg_pct_dis = 100-(($venta/$deuda)*100); else $p_avg_pct_dis = 0.00;
		
		
		//Foreclosed Sold
		$query = "SELECT count(*) FROM marketvalue m 
		INNER JOIN psummary p ON (m.parcelid=p.parcelid) 
		WHERE m.pendes='F' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$f_sold=$r[0];
		
		//Average Days of Sale After File Date
		$query = "SELECT sum(datediff(NOW(),STR_TO_DATE(p.file_date,'%Y%m%d')))/count(*)
		FROM pendes p
		INNER JOIN psummary ps ON (p.parcelid=ps.parcelid)
		INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
		WHERE m.pendes='F' and m.sold='S' and length(p.file_date)>0 and (m.totalpenmort-ps.saleprice) > 0";
		if($val != '')
			$query.=" AND ps.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$f_avg_day_safd=$r[0];
		
		//Average Days of Sale After Judgement Date (Auction)
		$query = "SELECT sum(datediff(NOW(),STR_TO_DATE(p.judgedate,'%Y%m%d')))/count(*)
		FROM pendes p
		INNER JOIN psummary ps ON (p.parcelid=ps.parcelid)
		INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
		WHERE m.pendes='F' and m.sold='S' and length(p.judgedate)>0 and (m.totalpenmort-ps.saleprice) > 0";
		if($val != '')
			$query.=" AND ps.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$f_avg_day_sajd=$r[0];
		
		//Average total kwon debt
		$query = "SELECT sum(m.totalpenmort)/count(*)
		FROM marketvalue m
		INNER JOIN psummary p ON (m.parcelid=p.parcelid)
		WHERE m.pendes='F' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$deuda=$r[0];
		
		//Average total sale price
		$query = "SELECT sum(p.saleprice)/count(*)
		FROM psummary p
		INNER JOIN marketvalue m ON (p.parcelid=m.parcelid)
		WHERE m.pendes='F' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0";
		if($val != '')
			$query.=" AND p.xcode='$val'";
			
		$result = mysql_query($query) or die("659T: ".$query.' '.mysql_error());
		$r = mysql_fetch_array($result);
		
		$venta=$r[0];
		
		//Average Amount of Discount
		$f_avg_amo_dis = $deuda - $venta;
		//Average Percentage of Discount
		if($deuda>0) $f_avg_pct_dis = 100-(($venta/$deuda)*100); else $f_avg_pct_dis = 0.00;
		
		
		//Validacion de los datos nullos o vacios
		if(strlen($p_sold)==0) $p_sold=0.00;
		if(strlen($p_avg_day_ss)==0) $p_avg_day_ss=0.00;
		if(strlen($p_avg_amo_dis)==0) $p_avg_amo_dis=0.00;
		if(strlen($p_avg_pct_dis)==0) $p_avg_pct_dis=0.00;
		if(strlen($f_sold)==0) $f_sold=0.00;
		if(strlen($f_avg_day_safd)==0) $f_avg_day_safd=0.00;
		if(strlen($f_avg_day_sajd)==0) $f_avg_day_sajd=0.00;
		if(strlen($f_avg_amo_dis)==0) $f_avg_amo_dis=0.00;
		if(strlen($f_avg_pct_dis)==0) $f_avg_pct_dis=0.00;
		
		//Insert discount table
		$query="INSERT INTO discount (proptype,fecha,p_sold,p_avg_day_ss,p_avg_amo_dis,p_avg_pct_dis,f_sold,f_avg_day_safd,f_avg_day_sajd,f_avg_amo_dis,f_avg_pct_dis)
		VALUE ('$val',NOW(),$p_sold,$p_avg_day_ss,$p_avg_amo_dis,$p_avg_pct_dis,$f_sold,$f_avg_day_safd,$f_avg_day_sajd,$f_avg_amo_dis,$f_avg_pct_dis)";
		
		mysql_query($query) or die ("659T: ".$query.' '.mysql_error());
		
		if($val==''){		
			ingreso(726,$county,0,$p_sold);
			ingreso(719,$county,0,$p_avg_day_ss);
			ingreso(720,$county,0,$p_avg_amo_dis);
			ingreso(721,$county,0,$p_avg_pct_dis);
			ingreso(727,$county,0,$f_sold);
			ingreso(722,$county,0,$f_avg_day_safd);
			ingreso(723,$county,0,$f_avg_day_sajd);
			ingreso(724,$county,0,$f_avg_amo_dis);
			ingreso(725,$county,0,$f_avg_pct_dis);
		}
	}

	$num_reg=mysql_affected_rows();
	echo "<br />Generar el Discount del Condado en Cuestion: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=659;
?>