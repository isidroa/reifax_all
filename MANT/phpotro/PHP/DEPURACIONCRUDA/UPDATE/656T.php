<?php 
	//Actuliza el YR_BUILT con la Sumatoria de los tamaños y el IMPRV_DET_AREA con el Tamaño del Living Area 
	
	//Creo mi Tabla Auxiliar
	$create_tabla_aux="	
		CREATE TABLE IF NOT EXISTS `depuracion_aux_jesus` (
		  `prop_id` varchar(25) DEFAULT NULL,
		  `valor` varchar(25) DEFAULT NULL,
		  KEY `prop_id` (`prop_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";
	mysql_query($create_tabla_aux) or die ("656T:A ".$create_tabla_aux." - ".mysql_error());

	$query="TRUNCATE TABLE depuracion_aux_jesus";
	mysql_query($query) or die("656T:B ".$query." : ".mysql_error());

	//inserto en la tabla temporal todos los parcelid con su respectivo total_area
	$query="INSERT INTO depuracion_aux_jesus 
			SELECT prop_id,SUM(imprv_det_area) FROM TXDENPAOPAC group by prop_id";
	mysql_query($query) or die("656T:C ".$query." : ".mysql_error());
	
	//Actualizo en calpo en YR_BUILT de la tabla TXDENPAOPAC con el Valor del Total Area (GROSS AREA)
	$query="UPDATE TXDENPAOPAC
		INNER JOIN depuracion_aux_jesus ON (TXDENPAOPAC.prop_id = depuracion_aux_jesus.prop_id)
		SET TXDENPAOPAC.YR_BUILT = depuracion_aux_jesus.valor";
	mysql_query($query) or die("656T:D ".$query." : ".mysql_error());
	
	$num_reg1=mysql_affected_rows();
	
	//Limpío la Tabla Temporal
	$query="TRUNCATE TABLE depuracion_aux_jesus";
	mysql_query($query) or die("656T:E ".$query." : ".mysql_error());
	
	//inserto en la tabla temporal todos los parcelid con su respectivo Living_area
	$query="INSERT INTO depuracion_aux_jesus 
			SELECT prop_id,SUM(imprv_det_area) FROM TXDENPAOPAC WHERE imprv_det_type_desc like 'MAIN%' group by prop_id";
	mysql_query($query) or die("656T:F ".$query." : ".mysql_error());

	//Actualizo en calpo en IMPRV_DET_AREA de la tabla TXDENPAOPAC con el Valor del LIVING AREA
	$query="UPDATE TXDENPAOPAC
		INNER JOIN depuracion_aux_jesus ON (TXDENPAOPAC.prop_id = depuracion_aux_jesus.prop_id)
		SET TXDENPAOPAC.IMPRV_DET_AREA = depuracion_aux_jesus.valor";
	mysql_query($query) or die("656T:G ".$query." : ".mysql_error());

	$num_reg2=mysql_affected_rows();
	
	//Elimino Mi Tabla Auxiliar
	$query="DROP TABLE IF EXISTS depuracion_aux_jesus";
	mysql_query($query) or die("656T:H ".$query." : ".mysql_error());
	
	echo "<br />Actuliza el YR_BUILT con la Sumatoria de los tamaños y el IMPRV_DET_AREA con el Tamaño del Living Area: ";
	echo "<br />Para el Living Area(IMPRV_DET_AREA) $num_reg1 y para el Gross Area(YR_BUILT) $num_reg2";

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=656;
?>