<?php
	//Actualiza el Campo prop_type_cd en TXDENPAOPAA con el campo imprv_type_cd de TXDENPAOPAF para Obtener el Tipo de Propiedad 
	
	//Primero que Todo altero la Tabla para poder crear un indice
	$query="ALTER TABLE `txdenpaopaa` MODIFY COLUMN `prop_id` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL";
	mysql_query($query) or die("Error 662T:A ".$query." - ".mysql_error());

	//Creo el Indice para que el inner join no demore demasiado
	$query="ALTER TABLE `txdenpaopaa` ADD INDEX `PROP_ID`(`prop_id`)";
	mysql_query($query) or die("Error 662T:B ".$query." - ".mysql_error());
	
	$query="UPDATE TXDENPAOPAA a
			INNER JOIN  TXDENPAOPAF b ON (b.prop_id=a.prop_id)
			SET a.prop_type_cd=
			IF(b.imprv_type_cd='I' AND (b.imprv_state_cd<>'A4' AND b.imprv_state_cd<>'B1' AND b.imprv_state_cd<>'A5' AND b.imprv_state_cd<>'B2'),
				'R',
				IF(b.imprv_type_cd='C' AND (b.imprv_state_cd<>'A4' AND b.imprv_state_cd<>'B1' AND b.imprv_state_cd<>'A5' AND b.imprv_state_cd<>'B2'),
					'P',
					IF(b.imprv_type_cd='COLMH' AND (b.imprv_state_cd<>'A4' AND b.imprv_state_cd<>'B1' AND b.imprv_state_cd<>'A5' AND b.imprv_state_cd<>'B2'),
						'MH',
						IF(b.imprv_type_cd='M' AND (b.imprv_state_cd<>'A4' AND b.imprv_state_cd<>'B1' AND b.imprv_state_cd<>'A5' AND b.imprv_state_cd<>'B2'),
							'MH',
							IF(b.imprv_state_cd='A4',
								'CON',
								IF(b.imprv_state_cd='A5',
									'CON',
									IF(b.imprv_state_cd='B1',
										'M+',
										IF(b.imprv_state_cd='B2',
											'M-',
											a.prop_type_cd
										)
									)
								)
							)
						)
					)
				)
			)";
			
	mysql_query($query) or die("Error 662T:C ".$query." - ".mysql_error());
	$num_reg=mysql_affected_rows();
	echo "<br />Actualiza el Campo prop_type_cd en TXDENPAOPAA con el campo imprv_type_cd de TXDENPAOPAF para Obtener el Tipo de Propiedad: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=662;
	
?>