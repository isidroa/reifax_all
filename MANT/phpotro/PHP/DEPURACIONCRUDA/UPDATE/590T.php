<?php 
//Corrige parcelid,beds,bath y yrbuilt de MSX con MLX o IRIS 

$ECHOMX=0;
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//BUSCANDO NOMBRE DE TABLA CRUDA PARA EL CONDADO 
	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%MSXMSX'
				limit 1";
	$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblResMsx=$r['tblResult'];


	$que="DROP TABLE IF EXISTS `mlsaux`";
	mysql_query($que) or die("590T: ".$que.mysql_error());

	$que="CREATE TABLE  `mlsaux` (
	  `ParcelID` varchar(30) NOT NULL DEFAULT '0' COMMENT 'Codificacion del condado de la propiedad',
	  `Bath` tinyint(2) DEFAULT '0' COMMENT 'Cantidad de ba�os',
	  `Beds` tinyint(2) DEFAULT '0' COMMENT 'Cantidad de cuartos',
	  `MLNumber` varchar(15) DEFAULT NULL COMMENT 'Codigo de identificacion del mls',
	  `Yrbuilt` mediumint(4) DEFAULT '0' COMMENT 'A�o de Construccion',
	  `psum` mediumint(4) DEFAULT 0 COMMENT '',
	  KEY `ParcelID` (`ParcelID`) USING BTREE,
	  KEY `Mlnumber` (`MLNumber`) USING BTREE
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	mysql_query($que) or die("590T: ".$que.mysql_error());

//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//ACTUALIZO DIRECTAMENTE MSX DIRECTAMENTE DESDE EL MLXSF
	$que="TRUNCATE TABLE `mlsaux`";
	mysql_query($que) or die("590T: ".$que.mysql_error());

	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%MLXSF'
				limit 1";
	$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblRes=$r['tblResult'];
	$funci1=$r['funcion'];
	$campop=$r['campop'];
	$tipoconsulta=$r['consulta'];
	if($funci1=='' || strlen($funci1)==0)//sino tiene funcion tomo el campo origen
		$funci1="x.`".$campop."`";
	else 
		$funci1=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
	
	if(strlen($tblRes)>0 && $tblRes<>'' && strlen($tipoconsulta)>0 && $tipoconsulta<>'no')
	{
	//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Mlnumber' and e.tblResult like '%MLXSF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campomlnumber=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Beds' and e.tblResult like '%MLXSF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobeds=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Bath' and e.tblResult like '%MLXSF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobath=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Yrbuilt' and e.tblResult like '%MLXSF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campoyrbuilt=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT $campomlnumber mls,COUNT($campomlnumber) cant
				FROM $tblRes x
				GROUP BY $campomlnumber
				HAVING COUNT($campomlnumber)>1
				ORDER BY COUNT($campomlnumber) DESC
				";
		$rest=mysql_query($que) or die('590T '.$que.mysql_error());
		$num_reg=0;
		//recorro la lista de duplicados 
		while($r=mysql_fetch_array($rest)){
			//ordeno cada conjunto de parcelid duplicados de manera que se ordene de menor a mayor 
			//por el saledate y saleprice, de modo que la fecha mas actual y de ser iguales fechas 
			//el mayor precio quede de ultimo, y asi borro todos los registros menos el ultimo.
			$cam=str_replace('x.','',$campomlnumber);
			$que="DELETE FROM $tblRes  WHERE $cam='".$r['mls']."' LIMIT ".($r[1]-1);
			mysql_query($que) or die('590T '.$que.mysql_error());
			$num_reg+=mysql_affected_rows();			
		}
		echo "<br />Eliminados mlnumbers duplicados en MLXSF: ".$num_reg;
		//------------------------------------------------------------------------------------------------------------------------------------------
		
		//echo 
		$que="SELECT $campomlnumber mlnumber, $funci1 parcelid,$campobeds beds,$campobath bath,$campoyrbuilt yrbuilt 
					FROM $tblResMsx f
					inner join $tblRes x on  $campomlnumber=f.`mlslistingid` ";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$contami=0;
		$contamio=0;
		$INVERSAI=0; 
		$MODINVERSA=1000; 
		$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
		while($r=mysql_fetch_array($rest4))
		{
			//echo "<br>";
			$contami++;
			$mlnumber=$r['mlnumber'];
			$parcelid=$r['parcelid'];
			$beds=$r['beds'];
			$bath=$r['bath'];
			$yrbuilt=$r['yrbuilt'];

			$psum=0;
			$elimino=0;
			if($parcelid<>'' && strlen($parcelid)>0)
			{
				$que="	SELECT parcelid, beds, bath, yrbuilt  FROM psummary WHERE parcelid='".strtolower($parcelid)."' limit 1";
				$rest6=mysql_query($que) or die("590T: ".$que.mysql_error());
				$r6=mysql_fetch_array($rest6);
				if($r6['parcelid']<>'' && strlen($r6['parcelid'])>0)
				{
					if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
						echo "<br> $contami.- ".$mlnumber." | ".$parcelid." | ".$r6['parcelid']." || ".$beds." | ".$r6['beds']." || ".$bath." | ".$r6['bath']." || ".$yrbuilt." | ".$r6['yrbuilt']." || --> UPDATE ";
					$contamio++;
					$psum=1;
				}
			}
			else
				$psum=2;

			if($INSERTAI>0)$queryinsinversa.=",";
			$queryinsinversa.="('".$parcelid."','".$mlnumber."','".$beds."','".$bath."','".$yrbuilt."',$psum)";
			$INSERTAI++;
			if($INSERTAI>1 && ($INSERTAI%$MODINVERSA)==0)
			{
				mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
				$INSERTAI=0;
				$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
			}					
		}//while de recorrido de los que linkean 
		if($INSERTAI>0)
		{
			mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
			$INSERTAI=0;
		}	

		$que="	UPDATE $tblResMsx m 
				INNER JOIN mlsaux x on x.mlnumber=m.mlslistingid
				SET m.apn=x.parcelid,
				m.beds=x.beds,
				m.baths=x.bath,
				m.yearbuilt=x.yrbuilt
				WHERE x.psum=1";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Actualizados parcelid,beds,bath y yrbuilt de MSX con MLXSF: ".$num_reg;

		$que="	DELETE $tblRes x FROM $tblRes x
				INNER JOIN mlsaux p ON ( $campomlnumber=p.mlnumber)";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con MLXSF: ".$num_reg;

		$que="	DELETE $tblRes x FROM $tblRes x
				INNER JOIN $tblResMsx p ON ( $funci1=p.apn)
				WHERE length(p.apn)>0 and p.apn is not null and length($funci1)>0 and $funci1  is not null";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con MLXSF por parcelid: ".$num_reg;
		
	}//IF MLXSF

//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//ACTUALIZO DIRECTAMENTE MSX DIRECTAMENTE DESDE EL MLXCON
	$que="TRUNCATE TABLE `mlsaux`";
	mysql_query($que) or die("590T: ".$que.mysql_error());

	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%MLXCON'
				limit 1";
	$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblRes=$r['tblResult'];
	$funci1=$r['funcion'];
	$campop=$r['campop'];
	$tipoconsulta=$r['consulta'];
	if($funci1=='' || strlen($funci1)==0)//sino tiene funcion tomo el campo origen
		$funci1="x.`".$campop."`";
	else 
		$funci1=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
	
	if(strlen($tblRes)>0 && $tblRes<>'' && strlen($tipoconsulta)>0 && $tipoconsulta<>'no')
	{
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Mlnumber' and e.tblResult like '%MLXCON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campomlnumber=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Beds' and e.tblResult like '%MLXCON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobeds=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Bath' and e.tblResult like '%MLXCON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobath=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Yrbuilt' and e.tblResult like '%MLXCON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campoyrbuilt=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT $campomlnumber mls,COUNT($campomlnumber) cant
				FROM $tblRes x
				GROUP BY $campomlnumber
				HAVING COUNT($campomlnumber)>1
				ORDER BY COUNT($campomlnumber) DESC
				";
		$rest=mysql_query($que) or die('590T '.$que.mysql_error());
		$num_reg=0;
		//recorro la lista de duplicados 
		while($r=mysql_fetch_array($rest)){
			//ordeno cada conjunto de parcelid duplicados de manera que se ordene de menor a mayor 
			//por el saledate y saleprice, de modo que la fecha mas actual y de ser iguales fechas 
			//el mayor precio quede de ultimo, y asi borro todos los registros menos el ultimo.
			$cam=str_replace('x.','',$campomlnumber);
			$que="DELETE FROM $tblRes  WHERE $cam='".$r['mls']."' LIMIT ".($r[1]-1);
			mysql_query($que) or die('590T '.$que.mysql_error());
			$num_reg+=mysql_affected_rows();			
		}
		echo "<br />Eliminados mlnumbers duplicados en MLXCON: ".$num_reg;
		//------------------------------------------------------------------------------------------------------------------------------------------
		
		//echo 
		$que="SELECT $campomlnumber mlnumber, $funci1 parcelid,$campobeds beds,$campobath bath,$campoyrbuilt yrbuilt 
					FROM $tblResMsx f
					inner join $tblRes x on  $campomlnumber=f.`mlslistingid` ";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$contami=0;
		$contamio=0;
		$INVERSAI=0; 
		$MODINVERSA=1000; 
		$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
		while($r=mysql_fetch_array($rest4))
		{
			//echo "<br>";
			$contami++;
			$mlnumber=$r['mlnumber'];
			$parcelid=$r['parcelid'];
			$beds=$r['beds'];
			$bath=$r['bath'];
			$yrbuilt=$r['yrbuilt'];

			$psum=0;
			$elimino=0;
			if($parcelid<>'' && strlen($parcelid)>0)
			{
				$que="	SELECT parcelid, beds, bath, yrbuilt  FROM psummary WHERE parcelid='".strtolower($parcelid)."' limit 1";
				$rest6=mysql_query($que) or die("590T: ".$que.mysql_error());
				$r6=mysql_fetch_array($rest6);
				if($r6['parcelid']<>'' && strlen($r6['parcelid'])>0)
				{
					if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
						echo "<br> $contami.- ".$mlnumber." | ".$parcelid." | ".$r6['parcelid']." || ".$beds." | ".$r6['beds']." || ".$bath." | ".$r6['bath']." || ".$yrbuilt." | ".$r6['yrbuilt']." || --> UPDATE ";
					$contamio++;
					$psum=1;
				}
			}
			else
				$psum=2;

			if($INSERTAI>0)$queryinsinversa.=",";
			$queryinsinversa.="('".$parcelid."','".$mlnumber."','".$beds."','".$bath."','".$yrbuilt."',$psum)";
			$INSERTAI++;
			if($INSERTAI>1 && ($INSERTAI%$MODINVERSA)==0)
			{
				mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
				$INSERTAI=0;
				$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
			}					
		}//while de recorrido de los que linkean 
		if($INSERTAI>0)
		{
			mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
			$INSERTAI=0;
		}	

		$que="	UPDATE $tblResMsx m 
				INNER JOIN mlsaux x on x.mlnumber=m.mlslistingid
				SET m.apn=x.parcelid,
				m.beds=x.beds,
				m.baths=x.bath,
				m.yearbuilt=x.yrbuilt
				WHERE x.psum=1";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Actualizados parcelid,beds,bath y yrbuilt de MSX con MLXCON: ".$num_reg;

		$que="	DELETE $tblRes x FROM $tblRes x
				INNER JOIN mlsaux p ON ( $campomlnumber=p.mlnumber)";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con MLXCON: ".$num_reg;

		$que="	DELETE $tblRes x FROM $tblRes x
				INNER JOIN $tblResMsx p ON ( $funci1=p.apn)
				WHERE length(p.apn)>0 and p.apn is not null and length($funci1)>0 and $funci1  is not null";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con MLXCON por parcelid: ".$num_reg;
		
	}//IF MLXCON

//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//ACTUALIZO DIRECTAMENTE MSX DIRECTAMENTE DESDE EL IRISF
	$que="TRUNCATE TABLE `mlsaux`";
	mysql_query($que) or die("590T: ".$que.mysql_error());

	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%IRISF'
				limit 1";
	$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblRes=$r['tblResult'];
	$funci1=$r['funcion'];
	$campop=$r['campop'];
	$tipoconsulta=$r['consulta'];
	if($funci1=='' || strlen($funci1)==0)//sino tiene funcion tomo el campo origen
		$funci1="x.`".$campop."`";
	else 
		$funci1=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
	
	if(strlen($tblRes)>0 && $tblRes<>'' && strlen($tipoconsulta)>0 && $tipoconsulta<>'no')
	{
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Mlnumber' and e.tblResult like '%IRISF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campomlnumber=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Beds' and e.tblResult like '%IRISF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobeds=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Bath' and e.tblResult like '%IRISF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobath=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Yrbuilt' and e.tblResult like '%IRISF' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campoyrbuilt=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT $campomlnumber mls,COUNT($campomlnumber) cant
				FROM $tblRes x
				GROUP BY $campomlnumber
				HAVING COUNT($campomlnumber)>1
				ORDER BY COUNT($campomlnumber) DESC
				";
		$rest=mysql_query($que) or die('590T '.$que.mysql_error());
		$num_reg=0;
		//recorro la lista de duplicados 
		while($r=mysql_fetch_array($rest)){
			//ordeno cada conjunto de parcelid duplicados de manera que se ordene de menor a mayor 
			//por el saledate y saleprice, de modo que la fecha mas actual y de ser iguales fechas 
			//el mayor precio quede de ultimo, y asi borro todos los registros menos el ultimo.
			$cam=str_replace('x.','',$campomlnumber);
			$que="DELETE FROM $tblRes  WHERE $cam='".$r['mls']."' LIMIT ".($r[1]-1);
			mysql_query($que) or die('590T '.$que.mysql_error());
			$num_reg+=mysql_affected_rows();			
		}
		echo "<br />Eliminados mlnumbers duplicados en IRISF: ".$num_reg;
		//------------------------------------------------------------------------------------------------------------------------------------------
		
		
		//echo 
		$que="SELECT $campomlnumber mlnumber, $funci1 parcelid,$campobeds beds,$campobath bath,$campoyrbuilt yrbuilt 
					FROM $tblResMsx f
					inner join $tblRes x on  $campomlnumber=f.`mlslistingid` ";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$contami=0;
		$contamio=0;
		$INVERSAI=0; 
		$MODINVERSA=1000; 
		$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
		while($r=mysql_fetch_array($rest4))
		{
			//echo "<br>";
			$contami++;
			$mlnumber=$r['mlnumber'];
			$parcelid=$r['parcelid'];
			$beds=$r['beds'];
			$bath=$r['bath'];
			$yrbuilt=$r['yrbuilt'];

			$psum=0;
			$elimino=0;
			if($parcelid<>'' && strlen($parcelid)>0)
			{
				$que="	SELECT parcelid, beds, bath, yrbuilt  FROM psummary WHERE parcelid='".strtolower($parcelid)."' limit 1";
				$rest6=mysql_query($que) or die("590T: ".$que.mysql_error());
				$r6=mysql_fetch_array($rest6);
				if($r6['parcelid']<>'' && strlen($r6['parcelid'])>0)
				{
					if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
						echo "<br> $contami.- ".$mlnumber." | ".$parcelid." | ".$r6['parcelid']." || ".$beds." | ".$r6['beds']." || ".$bath." | ".$r6['bath']." || ".$yrbuilt." | ".$r6['yrbuilt']." || --> UPDATE ";
					$contamio++;
					$psum=1;
				}
			}
			else
				$psum=2;

			if($INSERTAI>0)$queryinsinversa.=",";
			$queryinsinversa.="('".$parcelid."','".$mlnumber."','".$beds."','".$bath."','".$yrbuilt."',$psum)";
			$INSERTAI++;
			if($INSERTAI>1 && ($INSERTAI%$MODINVERSA)==0)
			{
				mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
				$INSERTAI=0;
				$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
			}					
		}//while de recorrido de los que linkean 
		if($INSERTAI>0)
		{
			mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
			$INSERTAI=0;
		}	

		$que="	UPDATE $tblResMsx m 
				INNER JOIN mlsaux x on x.mlnumber=m.mlslistingid
				SET m.apn=x.parcelid,
				m.beds=x.beds,
				m.baths=x.bath,
				m.yearbuilt=x.yrbuilt
				WHERE x.psum=1";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Actualizados parcelid,beds,bath y yrbuilt de MSX con IRISF: ".$num_reg;

		$campomlnumber=str_replace('x.',$tblRes.'.',$campomlnumber);
		$que="	DELETE $tblRes FROM $tblRes
				INNER JOIN mlsaux ON ( $campomlnumber=mlsaux.mlnumber)";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con IRISF: ".$num_reg;
		
		$funci1=str_replace('x.',$tblRes.'.',$funci1);
		$que="	DELETE $tblRes FROM $tblRes 
				INNER JOIN $tblResMsx ON ( $funci1=$tblResMsx.apn)
				WHERE length($tblResMsx.apn)>0 and $tblResMsx.apn is not null and length($funci1)>0 and $funci1  is not null";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con IRISF por parcelid: ".$num_reg;
		
	}//IF IRISF

//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//****************************************************************************************************************************************************************
//ACTUALIZO DIRECTAMENTE MSX DIRECTAMENTE DESDE EL IRICON
	$que="TRUNCATE TABLE `mlsaux`";
	mysql_query($que) or die("590T: ".$que.mysql_error());

	$que="SELECT e.tblResult,m.funcion,m.campop,t.consulta
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				left join `mantenimiento`.`traduccion` t on t.tblResult=e.tblResult
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Parcelid' and e.tblResult like '%IRICON'
				limit 1";
	$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
	$r=mysql_fetch_array($rest4);
	$tblRes=$r['tblResult'];
	$funci1=$r['funcion'];
	$campop=$r['campop'];
	$tipoconsulta=$r['consulta'];
	if($funci1=='' || strlen($funci1)==0)//sino tiene funcion tomo el campo origen
		$funci1="x.`".$campop."`";
	else 
		$funci1=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
	
	if(strlen($tblRes)>0 && $tblRes<>'' && strlen($tipoconsulta)>0 && $tipoconsulta<>'no')
	{
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Mlnumber' and e.tblResult like '%IRICON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campomlnumber=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Beds' and e.tblResult like '%IRICON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobeds=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Bath' and e.tblResult like '%IRICON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campobath=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT e.tblResult,m.funcion,m.campop
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`mapeo` m on e.structid=m.structid
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and m.`campo`='Yrbuilt' and e.tblResult like '%IRICON' limit 1";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest4);
		$tblRes=$r['tblResult'];
		$funci=$r['funcion'];
		$campop=$r['campop'];
		if($campop<>'' && strlen($campop)>0)
		{
			if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
				$funci="x.`".$campop."`";
			else 
				$funci=str_replace("`".$r['campop']."`","x.`".$r['campop']."`", $r['funcion']);
		}
		$campoyrbuilt=$funci;
		//------------------------------------------------------------------------------------------------------------------------------------------
		$que="	SELECT $campomlnumber mls,COUNT($campomlnumber) cant
				FROM $tblRes x
				GROUP BY $campomlnumber
				HAVING COUNT($campomlnumber)>1
				ORDER BY COUNT($campomlnumber) DESC
				";
		$rest=mysql_query($que) or die('590T '.$que.mysql_error());
		$num_reg=0;
		//recorro la lista de duplicados 
		while($r=mysql_fetch_array($rest)){
			//ordeno cada conjunto de parcelid duplicados de manera que se ordene de menor a mayor 
			//por el saledate y saleprice, de modo que la fecha mas actual y de ser iguales fechas 
			//el mayor precio quede de ultimo, y asi borro todos los registros menos el ultimo.
			$cam=str_replace('x.','',$campomlnumber);
			$que="DELETE FROM $tblRes  WHERE $cam='".$r['mls']."' LIMIT ".($r[1]-1);
			mysql_query($que) or die('590T '.$que.mysql_error());
			$num_reg+=mysql_affected_rows();			
		}
		echo "<br />Eliminados mlnumbers duplicados en IRICON: ".$num_reg;
		//------------------------------------------------------------------------------------------------------------------------------------------
		
		
		//echo 
		$que="SELECT $campomlnumber mlnumber, $funci1 parcelid,$campobeds beds,$campobath bath,$campoyrbuilt yrbuilt 
					FROM $tblResMsx f
					inner join $tblRes x on  $campomlnumber=f.`mlslistingid` ";
		$rest4=mysql_query($que) or die("590T: ".$que.mysql_error());
		$contami=0;
		$contamio=0;
		$INVERSAI=0; 
		$MODINVERSA=1000; 
		$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
		while($r=mysql_fetch_array($rest4))
		{
			//echo "<br>";
			$contami++;
			$mlnumber=$r['mlnumber'];
			$parcelid=$r['parcelid'];
			$beds=$r['beds'];
			$bath=$r['bath'];
			$yrbuilt=$r['yrbuilt'];

			$psum=0;
			$elimino=0;
			if($parcelid<>'' && strlen($parcelid)>0)
			{
				$que="	SELECT parcelid, beds, bath, yrbuilt  FROM psummary WHERE parcelid='".strtolower($parcelid)."' limit 1";
				$rest6=mysql_query($que) or die("590T: ".$que.mysql_error());
				$r6=mysql_fetch_array($rest6);
				if($r6['parcelid']<>'' && strlen($r6['parcelid'])>0)
				{
					if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
						echo "<br> $contami.- ".$mlnumber." | ".$parcelid." | ".$r6['parcelid']." || ".$beds." | ".$r6['beds']." || ".$bath." | ".$r6['bath']." || ".$yrbuilt." | ".$r6['yrbuilt']." || --> UPDATE ";
					$contamio++;
					$psum=1;
				}
			}
			else
				$psum=2;

			if($INSERTAI>0)$queryinsinversa.=",";
			$queryinsinversa.="('".$parcelid."','".$mlnumber."','".$beds."','".$bath."','".$yrbuilt."',$psum)";
			$INSERTAI++;
			if($INSERTAI>1 && ($INSERTAI%$MODINVERSA)==0)
			{
				mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
				$INSERTAI=0;
				$queryinsinversa="INSERT INTO mlsaux (parcelid,mlnumber,beds, bath, yrbuilt,psum)VALUES";		
			}					
		}//while de recorrido de los que linkean 
		if($INSERTAI>0)
		{
			mysql_query($queryinsinversa) or die("$queryinsinversa: ".mysql_error());
			$INSERTAI=0;
		}	

		$que="	UPDATE $tblResMsx m 
				INNER JOIN mlsaux x on x.mlnumber=m.mlslistingid
				SET m.apn=x.parcelid,
				m.beds=x.beds,
				m.baths=x.bath,
				m.yearbuilt=x.yrbuilt
				WHERE x.psum=1";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Actualizados parcelid,beds,bath y yrbuilt de MSX con IRICON: ".$num_reg;

		$campomlnumber=str_replace('x.',$tblRes.'.',$campomlnumber);
		$que="	DELETE $tblRes  FROM $tblRes 
				INNER JOIN mlsaux  ON ( $campomlnumber=mlsaux.mlnumber)";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con IRICON: ".$num_reg;

		$funci1=str_replace('x.',$tblRes.'.',$funci1);
		$que="	DELETE $tblRes FROM $tblRes 
				INNER JOIN $tblResMsx ON ( $funci1=$tblResMsx.apn)
				WHERE length($tblResMsx.apn)>0 and $tblResMsx.apn is not null and length($funci1)>0 and $funci1  is not null";
		if($ECHOMX==1 || ($contamio%$MODINVERSA)==0) 
			echo "<br>".$que;
		mysql_query($que) or die("590T: ".$que.mysql_error());
		$num_reg=mysql_affected_rows();
		echo "<br />Eliminados parcelid,beds,bath y yrbuilt de MSX con IRICON por parcelid: ".$num_reg;
	}//IF IRICON


	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=590;
?>