<?php 
	//Actualiza el Campo SITUS_CITY en PAOPAA para incluirlo en el PSUMMARY 

	//Creo mi Tabla Auxiliar
	$create_tabla_aux="	
		CREATE TABLE IF NOT EXISTS `depuracion_aux_jesus` (
		  `prop_id` varchar(25) DEFAULT NULL,
		  `valor` varchar(25) DEFAULT NULL,
		  KEY `prop_id` (`prop_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";
	mysql_query($create_tabla_aux) or die ("663T:A ".$create_tabla_aux." - ".mysql_error());

	$query="TRUNCATE TABLE depuracion_aux_jesus";
	mysql_query($query) or die("663T:B ".$query." : ".mysql_error());

	//inserto en la tabla temporal todos los parcelid con su respectiva Ciudad
	$query="INSERT INTO depuracion_aux_jesus 
		SELECT prop_id,REPLACE(REPLACE(entity_name,'CITY OF ',''),'TOWN OF ','') as city FROM TXDENPAOPAB
		WHERE 
			(LOCATE('CITY OF',entity_name)<>0 OR LOCATE('TOWN OF',entity_name)<>0)
			AND 
			entity_name NOT LIKE '%TIRZ%'";
	mysql_query($query) or die("663T:C ".$que.mysql_error());
	
	//Actualizo en calpo en SITUS_CITY de la tabla TXDENPAOPAA con el Campo de la Tabla Temporal que contiene la Ciudad Correcta
	$query="UPDATE TXDENPAOPAA
		INNER JOIN depuracion_aux_jesus ON (TXDENPAOPAA.prop_id = depuracion_aux_jesus.prop_id)
		SET TXDENPAOPAA.SITUS_CITY = depuracion_aux_jesus.valor";
	mysql_query($query) or die("663T:D ".$que.mysql_error());

	$num_reg=mysql_affected_rows();

	//Elimino Mi Tabla Auxiliar
	$query="DROP TABLE IF EXISTS depuracion_aux_jesus";
	mysql_query($query) or die("663T:E ".$query." : ".mysql_error());

	echo "<br />Actualiza el Campo SITUS_CITY en PAOPAA para incluirlo en el PSUMMARY: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=663;
?>