<?php 
	//Actualiza el IMPRV_ID con el Numero de Dormitorios 
	
	//Creo mi Tabla Auxiliar
	$create_tabla_aux="	
		CREATE TABLE IF NOT EXISTS `depuracion_aux_jesus` (
		  `prop_id` varchar(25) DEFAULT NULL,
		  `valor` varchar(25) DEFAULT NULL,
		  KEY `prop_id` (`prop_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";
	mysql_query($create_tabla_aux) or die ("657T:A ".$create_tabla_aux." - ".mysql_error());

	$query="TRUNCATE TABLE depuracion_aux_jesus";
	mysql_query($query) or die("657T:B ".$query." : ".mysql_error());

	//inserto en la tabla temporal todos los parcelid con su respectivo Numero de Dormitorios
	$query="INSERT INTO depuracion_aux_jesus 
			SELECT prop_id,imprv_attr_cd FROM TXDENPAOPAE WHERE imprv_attr_desc like '%Bedroom%' group by prop_id;";
	mysql_query($query) or die("657T:C ".$query." : ".mysql_error());
	
	//Actualizo en calpo en IMPRV_ID de la tabla TXDENPAOPAE con el Numero Total de Dormitorios
	$query="UPDATE TXDENPAOPAE
		INNER JOIN depuracion_aux_jesus ON (TXDENPAOPAE.prop_id = depuracion_aux_jesus.prop_id)
		SET TXDENPAOPAE.IMPRV_ID = depuracion_aux_jesus.valor";
	mysql_query($query) or die("657T:D ".$query." : ".mysql_error());

	$num_reg=mysql_affected_rows();

	//Elimino Mi Tabla Auxiliar
	$query="DROP TABLE IF EXISTS depuracion_aux_jesus";
	mysql_query($query) or die("657T:E ".$query." : ".mysql_error());

	echo "<br />Actualiza el IMPRV_ID con el Numero de Dormitorios: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=657;
?>