<?php 
	//Actualiza el Campo ParcelID de todas las Tablas Crudas del MLSRESIDENTIAL contra la Tabla Cruda del Psummary PAOPAA con el Campo geo_id 

	////////////////Funcion para obtener el parcelid////////////////
	if(!function_exists('obtener_campos_671')){
		function obtener_campos_671($campo,$nom_bd,$tabla,$letra,$funcion){
			//------------------------------------------------------------------------------------------------------------------------------------------
			//echo "<BR>".
			$query="SELECT e.tblResult,m.funcion,m.campop 
					FROM `mantenimiento`.`estructuras` e 
					left join `mantenimiento`.`mapeo` m ON e.structid=m.structid 
					WHERE lower(`bdResult`)='".strtolower($nom_bd)."' AND m.`campo`='$campo' AND e.tblResult='".$tabla."' LIMIT 1";
			$rest=mysql_query($query) or die("ERROR: 671|A: ".$query." - ".mysql_error());
			$r=mysql_fetch_array($rest);
			$tblRes=$r['tblResult'];
			$funci=$r['funcion'];
			$campop=$r['campop'];
			if($campop<>'' && strlen($campop)>0)
			{
				if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
					$funci="$letra.`".$campop."`";
				else 
					$funci=str_replace("`".$r['campop']."`","$letra.`".$r['campop']."`", $r['funcion']);
			}
			if($funcion==1)return $funci;else return $campop;
		}
	}

	////////////////Obtengo el Id del Condado////////////////
	$query = "SELECT idcounty FROM xima.lscounty WHERE bd='".strtolower($nom_bd)."'";
	$result = mysql_query($query) or die("ERROR: 671|B: ".$query." - ".mysql_error());
	$result = mysql_fetch_array($result);
	$idcounty = $result['idcounty'];
	
	////////////////Primero Verifico si Tiene Indice Creado////////////////
		$query = "SHOW INDEX FROM txdenpaopaa";
		$rest=mysql_query($query) or die("ERROR: A: ".$query." - ".mysql_error());
		$validar_indice=0;$validar_campo=0;$verifico_index=0;
		while($row=mysql_fetch_array($rest)){
			$verifico_index=1;
			//echo "<br>Column_name $row[Column_name]";
			if($row['Column_name']<>"geo_id"){
				$validar_indice=1;
				echo "<br>En La Tabla `txdenpaopaa` no tiene Indice creado para el Campo geo_id, se Va a Crear Automaticamente";
			}else{
				$validar_indice=0;
				break;
			}
		}
		//si no entro al while es porque no tiene ningun indice por lo tanto hay que crearlo
		if($verifico_index==0)
			$validar_indice=1;
		$query = "DESCRIBE `txdenpaopaa`";
		$rest=mysql_query($query) or die("ERROR: B: ".$query." - ".mysql_error());
		while($row=mysql_fetch_array($rest)){
			//echo "<br>$row[Field] $row[Type]";
			if(trim($row['Field'])=="geo_id"){
				if(trim($row['Type'])=="text"){
					$validar_campo=1;
					echo "<br>En La Tabla `txdenpaopaa` el Campo geo_id es tipo text, se Va a modificar por el tipo VARCHAR(50) Automaticamente";
				}
			}
		}
		////////////////altero la Tabla para poder crear un indice////////////////
		if($validar_campo==1){
			$query = "ALTER TABLE `txdenpaopaa` MODIFY COLUMN `geo_id` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL";
			mysql_query($query) or die("ERROR: C: ".$query." - ".mysql_error());		
			echo "<br>Se Modifico el Tipo de Campo para el geo_id de text a VARCHAR(50) de la Tabla `txdenpaopaa`";
		}
		////////////////Creo el Indice para que el inner join no demore demasiado////////////////
		if($validar_indice==1){
			$query = "ALTER TABLE `txdenpaopaa` ADD INDEX `geo_id`(`geo_id`)";
			mysql_query($query) or die("ERROR: D: ".$query." - ".mysql_error());		
			echo "<br>Se Creo el Indice para el Campo geo_id de la Tabla `txdenpaopaa`";
		}



	////////////////Elimino los Espacions en Blanco antes y despues de la cadena del geo_id y elimino los guiones (-)////////////////
	$query = "UPDATE TXDENPAOPAA a 
				SET a.geo_id=REPLACE(TRIM(a.geo_id),'-','')";
	mysql_query($query) or die("Error 671:I ".$query." - ".mysql_error());

	////////////////Obtengo las tablas que trabajan con el pendes que no sean el POTE y que la Consulta sea diferente de NO////////////////
	$query = "SELECT tblresult FROM mantenimiento.traduccion WHERE idcounty='".$idcounty."' AND tblid IN ('14','19') AND consulta<>'no' AND tblresult NOT LIKE '%POTSPO%'";
	$result = mysql_query($query) or die("ERROR: 671|E: ".$query." - ".mysql_error());
	while($r=mysql_fetch_array($result)){
		$campoIndiceMLS=obtener_campos_671("Parcelid",$nom_bd,$r['tblresult'],"a",1); //Campo Indice de CAda Tabla Cruda para el Parcelid
		$campoIndiceMLS_sin=obtener_campos_671("Parcelid",$nom_bd,$r['tblresult'],"a",0); //Campo Indice de CAda Tabla Cruda para el Parcelid sin la Funcion
		$campoIndiceP=obtener_campos_671("Parcelid",$nom_bd,"TXDENPAOPAA","b",1);		//Campo Indice de la Tabla TXDENPAOPAA para el PArcelid

/*		$query="SELECT $campoIndiceMLS_sin FROM $r[tblresult] LIMIT 1";
		$result1 = mysql_query($query) or die("ERROR: 671|F: ".$query." - ".mysql_error());
		$flags=mysql_field_flags($result1,0);
		////////////////Valido si no tiene una Key Generada////////////////
		if($flags<>"multiple_key"){
*/			
		$query = "SHOW INDEX FROM `$r[tblresult]`";
		$rest=mysql_query($query) or die("ERROR: A: ".$query." - ".mysql_error());
		$validar_indice=0;$validar_campo=0;$verifico_index=0;
		while($row=mysql_fetch_array($rest)){
			$verifico_index=1;
			//echo "<br>Column_name $row[Column_name]";
			if($row['Column_name']<>"$campoIndiceMLS_sin"){
				$validar_indice=1;
				echo "<br>En La Tabla `$r[tblresult]` no tiene Indice creado para el Campo $campoIndiceMLS_sin, se Va a Crear Automaticamente";
			}else{
				$validar_indice=0;
				break;
			}
		}
		//si no entro al while es porque no tiene ningun indice por lo tanto hay que crearlo
		if($verifico_index==0)
			$validar_indice=1;
		$query = "DESCRIBE `$r[tblresult]`";
		$rest=mysql_query($query) or die("ERROR: B: ".$query." - ".mysql_error());
		while($row=mysql_fetch_array($rest)){
			//echo "<br>$row[Field] $row[Type]";
			if(trim($row['Field'])=="$campoIndiceMLS_sin"){
				if(trim($row['Type'])=="text"){
					$validar_campo=1;
					echo "<br>En La Tabla `$r[tblresult]` el Campo $campoIndiceMLS_sin es tipo text, se Va a modificar por el tipo VARCHAR(50) Automaticamente";
				}
			}
		}
		////////////////altero la Tabla para poder crear un indice////////////////
		if($validar_campo==1){
			$query = "ALTER TABLE `$r[tblresult]` MODIFY COLUMN `$campoIndiceMLS_sin` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL";
			mysql_query($query) or die("ERROR: C: ".$query." - ".mysql_error());		
			echo "<br>Se Modifico el Tipo de Campo para el $campoIndiceMLS_sin de text a VARCHAR(50) de la Tabla `$r[tblresult]`";
		}
		////////////////Creo el Indice para que el inner join no demore demasiado////////////////
		if($validar_indice==1){
			$query = "ALTER TABLE `$r[tblresult]` ADD INDEX `$campoIndiceMLS_sin`(`$campoIndiceMLS_sin`)";
			mysql_query($query) or die("ERROR: D: ".$query." - ".mysql_error());		
			echo "<br>Se Creo el Indice para el Campo $campoIndiceMLS_sin de la Tabla `$r[tblresult]`";
		}
/*		}
*/
		////////////////Elimino los Espacions en Blanco antes y despues de la cadena Parc_no////////////////
		$query = "UPDATE $r[tblresult] a 
					SET a.lm_mst_parc_no=REPLACE(TRIM(a.lm_mst_parc_no),'-','')";
		mysql_query($query) or die("Error 671:I ".$query." - ".mysql_error());
					
		////////////////Actualizo el Parcelid de las tablas crudas////////////////
		echo  "<BR>";
		echo $query = "UPDATE $r[tblresult] a 
					INNER JOIN TXDENPAOPAA b ON ($campoIndiceMLS=b.geo_id) 
					SET a.$campoIndiceMLS_sin=$campoIndiceP";
		mysql_query($query) or die("ERROR: 671|J: ".$query." - ".mysql_error());
		
		////////////////Actualizo los Parcelid que comiencen con R o r y les agrego tantos ceros (0) necesiten a la izquierda////////////////
		////////////////para llegar a una longitud de 12////////////////
		$query = "UPDATE $r[tblresult] 
					SET $campoIndiceMLS_sin=LPAD(REPLACE(REPLACE(REPLACE(lm_mst_parc_no,'R',''),'-',''),'r',''),12,'0') 
					WHERE $campoIndiceMLS_sin LIKE 'R%'";
		mysql_query($query) or die("ERROR: 671|K: ".$query." - ".mysql_error());
	}

	$query="";
	//mysql_query($query) or die("671T: ".$query.mysql_error());

	$num_reg=mysql_affected_rows();
	echo "<br />Actualiza el Campo ParcelID de todas las Tablas Crudas del MLSRESIDENTIAL contra la Tabla Cruda del Psummary PAOPAA con el Campo geo_id: ".$num_reg;

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=671;
?>