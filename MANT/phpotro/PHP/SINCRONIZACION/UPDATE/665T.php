<?php 
	//Inserta NAL al Psummary, Actualiza Owner, Actualiza UNIT y Corrige Direcciones de Psummary 
	
	//VALIDAMOS QUE TENGA CHEQUEADA LA OPCION EN EL EJECUTADOR DE MAPEO EL CHECK QUE DICE NAL
	$nal_nalnal1='no';
	if(isset($_POST['nal_nalnal1']) && $_POST['nal_nalnal1']==true){$nal_nalnal1='si';}

	//VALIDAMOS QUE TENGA CHEQUEADA LA OPCION EN EL EJECUTADOR DE MAPEO EL CHECK QUE DICE UNIT
	$unit_unitutni1='no';
	if($_POST['unit_unitutni1']==true){$unit_unitutni1='si';}

	if($nal_nalnal1=='si'){
		////////////////////////////////////////Inicio de las Funciones/////////////////////////////////////
		function NAL_obtener_campos($campo,$nom_bd){
			//------------------------------------------------------------------------------------------------------------------------------------------
			//echo "<BR>".
			$query="SELECT e.tblResult,m.funcion,m.campop
					FROM `mantenimiento`.`estructuras` e
					left join `mantenimiento`.`mapeo` m ON e.structid=m.structid
					WHERE lower(`bdResult`)='".strtolower($nom_bd)."' AND m.`campo`='$campo' AND e.tblResult LIKE '%NALNAL' LIMIT 1";
			/*if($campo=='book')
				echo "<br>$query<br>";*/
			$rest=mysql_query($query) or die("ERROR: Function A: ".$query." - ".mysql_error());
			$r=mysql_fetch_array($rest);
			$tblRes=strtolower($r['tblResult']);
			$funci=$r['funcion'];
			$campop=$r['campop'];
			if($campop<>'' && strlen($campop)>0)
			{
				if($funci=='' || strlen($funci)==0)//sino tiene funcion tomo el campo origen
					$funci="n.`".$campop."`";
				else 
					$funci=str_replace("`".$r['campop']."`","n.`".$r['campop']."`", $r['funcion']);
			}
			return $funci;		
		}

		//Verificar si una cadena esta entre estas Opciones
		function NAL_existetexto($cad){
			if($cad=='AV')return true;
			if($cad=='HY')return true;
			if($cad=='CR')return true;
			if($cad=='WY')return true;
			if($cad=='BV')return true;
			if($cad=='TR')return true;
			if($cad=='LP')return true;
			if($cad=='VLF')return true;
			if($cad=='TL')return true;
			if($cad=='CIRCLE')return true;
			if($cad=='CIRCL')return true;
			if($cad=='CORT')return true;
			if($cad=='COURT')return true;
			if($cad=='PLACE')return true;
			if($cad=='STREET')return true;
			if($cad=='TERRACE')return true;
			if($cad=='LINE')return true;
			if($cad=='BOULEVARD')return true;
			if($cad=='DRIVE')return true;
			if($cad=='PARKWAY')return true;
			if($cad=='ROAD')return true;
			if($cad=='AVENUE')return true;
			if($cad=='RUN')return true;
			if($cad=='TRAIL')return true;
			if($cad=='TRACE')return true;
			//if($cad=='EAST')return true;
			//if($cad=='SOUTH')return true;
			//if($cad=='NORTH')return true;
			//if($cad=='WEST')return true;
			//Agregadas por Jesus
			if($cad=='WAY')return true;
			if($cad=='HW')return true;
			if($cad=='TIARE')return true;
			if($cad=='CI')return true;
			if($cad=='ST')return true;
			if($cad=='LN')return true;
			if($cad=='CARDINAL')return true;
			if($cad=='SEAS')return true;
			if($cad=='CT')return true;
			if($cad=='TER')return true;
			if($cad=='BLVD')return true;
			if($cad=='DR')return true;
			if($cad=='CIR')return true;
			if($cad=='RD')return true;
			if($cad=='PS')return true;
			if($cad=='PASS')return true;
			if($cad=='NOOK')return true;
			if($cad=='VISTA')return true;
			if($cad=='CALLE')return true;
			if($cad=='PL')return true;
			if($cad=='AVE')return true;
			if($cad=='RN')return true;
			if($cad=='AZEELE')return true;
			if($cad=='NO')return true;
			if($cad=='HWY')return true;
			if($cad=='LANE')return true;
			if($cad=='TERR')return true;
			if($cad=='VIEW')return true;
			if($cad=='TAHITI')return true;
			if($cad=='TROPICALIA')return true;
			if($cad=='CV')return true;
			if($cad=='REAL')return true;
			if($cad=='LOOP')return true;
			if($cad=='TROPICALIA')return true;
			if($cad=='CARMEN')return true;
			if($cad=='TRL')return true;
			if($cad=='PT')return true;
			if($cad=='ST')return true;
			
			return false;
		}	
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//funcion para Quitar Caracteres Especiales
		function NAL_corregirunit($cad){
			$cad=str_replace("PT","",$cad);
			$cad=str_replace("BLVD","",$cad);
			$cad=str_replace("CIR","",$cad);
			$cad=str_replace('WAY','',$cad);
			$cad=str_replace('CT','',$cad);
			$cad=str_replace('DR','',$cad);
			$cad=str_replace('TER','',$cad);
			$cad=str_replace('CV','',$cad);
			$cad=str_replace('CL','',$cad);
			$cad=str_replace('RD','',$cad);
			$cad=str_replace('LOOP','',$cad);
			$cad=str_replace('PKWY','',$cad);
			$cad=str_replace('LN','',$cad);
			$cad=str_replace('ST','',$cad);
			$cad=str_replace('PLZ','',$cad);
			$cad=str_replace('PL','',$cad);
			$cad=str_replace('AVE','',$cad);
			$cad=str_replace('CSWY','',$cad);
			$cad=str_replace('VLG','',$cad);
			$cad=str_replace('TRL','',$cad);
			return $cad;
		}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//funcion para Quitar Caracteres Especiales
		function NAL_quitarCaracteres($cad){

			$cad=str_replace("^","",$cad);
			$cad=str_replace("'","",$cad);
			$cad=str_replace("#","",$cad);
			$cad=str_replace(".","",$cad);
			$cad=str_replace('"','',$cad);
			$cad=str_replace('/','',$cad);
	/*		$cad=str_replace('  ',' ',$cad);
			$cad=str_replace('  ',' ',$cad);
			$cad=str_replace('   ',' ',$cad);
			$cad=str_replace('    ',' ',$cad);
			$cad=str_replace('     ',' ',$cad);
			$cad=str_replace('\\','',$cad);
			$cad=str_replace('\r\n',' ',$cad);*/
			return $cad;
		}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//buscar Texto en address para luego Aplicarle la Funcion CorregirAddress
		function NAL_BuscaenAddress($cad){
			if($cad=='AV')return true;
			if($cad=='HY')return true;
			if($cad=='CR')return true;
			if($cad=='WY')return true;
			if($cad=='BV')return true;
			if($cad=='TR')return true;
			if($cad=='LP')return true;
			if($cad=='VLF')return true;
			if($cad=='TL')return true;
			if($cad=='CIRCLE')return true;
			if($cad=='CIRCL')return true;
			if($cad=='CORT')return true;
			if($cad=='COURT')return true;
			if($cad=='PLACE')return true;
			if($cad=='STREET')return true;
			if($cad=='TERRACE')return true;
			if($cad=='LINE')return true;
			if($cad=='BOULEVARD')return true;
			if($cad=='DRIVE')return true;
			if($cad=='PARKWAY')return true;
			if($cad=='ROAD')return true;
			if($cad=='AVENUE')return true;
			if($cad=='RUN')return true;
			if($cad=='TRAIL')return true;
			if($cad=='TRACE')return true;
			//if($cad=='EAST')return true;
			//if($cad=='SOUTH')return true;
			//if($cad=='NORTH')return true;
			//if($cad=='WEST')return true;
			
	/*		if($cad=='CIR')return true;
			if($cad=='PL')return true;
			if($cad=='ST')return true;
			if($cad=='TER')return true;
			if($cad=='AVE')return true;
			if($cad=='LN')return true;
			if($cad=='DR')return true;
			if($cad=='BLVD')return true;
			if($cad=='PKWY')return true;
			if($cad=='RD')return true;
			if($cad=='AVE')return true;
			if($cad=='RN')return true;
			if($cad=='TRL')return true;
			if($cad=='E')return true;
			if($cad=='S')return true;
			if($cad=='N')return true;
			if($cad=='W')return true;*/
			return false;
		}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//Funcion para Corregir el Address del Psummary
		function NAL_CorregirAddress($cad){
			$cad=strtoupper($cad);
			
			$cad=str_replace('AV','AVE',$cad);
			$cad=str_replace('HY','HWY',$cad);
			$cad=str_replace('CR','CIR',$cad);
			if($cad!='HWY')$cad=str_replace('WY','WAY',$cad); // la Condicione porque chocaba con la Linea 7 HWY
			$cad=str_replace('BV','BLVD',$cad);
			if($cad!='TRACE' && $cad!='TRAIL')$cad=str_replace('TR','TER',$cad);
			$cad=str_replace('LP','LOOP',$cad);
			//$cad=str_replace('VLG','VILLAGE',$cad); No la Conocemos
			$cad=str_replace('TL','TRL',$cad);
			
			$cad=str_replace('CIRCLE','CIR',$cad);
			$cad=str_replace('CIRCL','CIR',$cad);
			$cad=str_replace('CORT','CT',$cad);
			$cad=str_replace('COURT','CT',$cad);
			$cad=str_replace('PLACE','PL',$cad);
			$cad=str_replace('STREET','ST',$cad);
			$cad=str_replace('TERRACE','TER',$cad);
			$cad=str_replace('LINE','LN',$cad);
			$cad=str_replace('DRIVE','DR',$cad);
			$cad=str_replace('BOULEVARD','BLVD',$cad);
			$cad=str_replace('PARKWAY','PKWY',$cad);
			$cad=str_replace('ROAD','RD',$cad);
			$cad=str_replace('AVENUE','AVE',$cad);
			$cad=str_replace('RUN','RN',$cad);
			$cad=str_replace('TRAIL','TRL',$cad);
			$cad=str_replace('TRACE','TRCE',$cad);
			//$cad=str_replace('EAST','E',$cad);
			//$cad=str_replace('SOUTH','S',$cad);
			//$cad=str_replace('NORTH','N',$cad);
			//$cad=str_replace('WEST','W',$cad);
			return $cad;
		}	
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		function NAL_existetexto_address($cad){
			if($cad=='AV')return true;
			if($cad=='HY')return true;
			if($cad=='CR')return true;
			if($cad=='WY')return true;
			if($cad=='BV')return true;
			if($cad=='TR')return true;
			if($cad=='LP')return true;
			if($cad=='VLF')return true;
			if($cad=='TL')return true;
			if($cad=='CIRCLE')return true;
			if($cad=='CIRCL')return true;
			if($cad=='CORT')return true;
			if($cad=='COURT')return true;
			if($cad=='PLACE')return true;
			if($cad=='STREET')return true;
			if($cad=='TERRACE')return true;
			if($cad=='LINE')return true;
			if($cad=='BOULEVARD')return true;
			if($cad=='DRIVE')return true;
			if($cad=='PARKWAY')return true;
			if($cad=='ROAD')return true;
			if($cad=='AVENUE')return true;
			if($cad=='RUN')return true;
			if($cad=='TRAIL')return true;
			if($cad=='TRACE')return true;
			//if($cad=='EAST')return true;
			//if($cad=='SOUTH')return true;
			//if($cad=='NORTH')return true;
			//if($cad=='WEST')return true;
			//Agregadas por Jesus
			if($cad=='WAY')return true;
			if($cad=='HW')return true;
			if($cad=='TIARE')return true;
			if($cad=='CI')return true;
			//if($cad=='ST')return true;
			if($cad=='LN')return true;
			//if($cad=='CARDINAL')return true;
			//if($cad=='SEAS')return true;
			if($cad=='CT')return true;
			if($cad=='TER')return true;
			if($cad=='BLVD')return true;
			if($cad=='DR')return true;
			if($cad=='CIR')return true;
			if($cad=='RD')return true;
			if($cad=='PS')return true;
			if($cad=='PASS')return true;
			if($cad=='NOOK')return true;
			if($cad=='VISTA')return true;
			if($cad=='CALLE')return true;
			if($cad=='PL')return true;
			if($cad=='AVE')return true;
			if($cad=='RN')return true;
			if($cad=='AZEELE')return true;
			if($cad=='NO')return true;
			if($cad=='HWY')return true;
			if($cad=='LANE')return true;
			if($cad=='TERR')return true;
			if($cad=='VIEW')return true;
			if($cad=='TAHITI')return true;
			if($cad=='TROPICALIA')return true;
			return false;
		}	
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		////////////////////////////////////////Fin de las Funciones/////////////////////////////////////

		echo "<BR>Hora Inicio: ".date("d-m-Y H:i:s")."<BR>";

		//BUSCAR TABLA DE NALNAL a  USAR ... 
		//echo "<br/>".
		$query="SELECT t.tblResult,t.consulta 
				FROM `mantenimiento`.`estructuras` e
				left join `mantenimiento`.`traduccion` t on (t.tblResult=e.tblResult)
				WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and t.tblid=13 and (t.tblResult like '%NALNAL') 
				limit 1";
		$rest=mysql_query($query) or die("ERROR: A: ".$query." - ".mysql_error());
		$r=mysql_fetch_array($rest);
		//echo "<br/>".
		$tblnalnal=strtolower($r['tblResult']);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		//validamos que Tenga Tabla Nal
		if(strlen(trim($tblnalnal))>0){
			//BUSCANDO EL NOMBRE INDICE DE LA TABLA NAL QUE ESTA EN TRADUCCION
			//echo "<br/>".
			$query="SELECT t.indice
						FROM `mantenimiento`.`estructuras` e
						LEFT JOIN `mantenimiento`.`traduccion` t ON (t.tblResult=e.tblResult)
						WHERE lower(`bdResult`)='".strtolower($nom_bd)."' AND e.tblResult='$tblnalnal'
						LIMIT 1";
			$rest=mysql_query($query) or die("ERROR: B: ".$query." - ".mysql_error());
			$r=mysql_fetch_array($rest);
			$indice=trim($r['indice']);
			if(strlen($indice)==0)$indice='Parcelid';

			//echo "<br/>".
			$query="SELECT e.tblResult,m.funcion,m.campop,t.consulta
						FROM `mantenimiento`.`estructuras` e
						LEFT JOIN `mantenimiento`.`mapeo` m ON (e.structid=m.structid)
						LEFT JOIN `mantenimiento`.`traduccion` t ON (t.tblResult=e.tblResult)
						WHERE lower(`bdResult`)='".strtolower($nom_bd)."' AND m.`campo`='$indice' AND e.tblResult='$tblnalnal'
						LIMIT 1";
			$rest=mysql_query($query) or die("ERROR: C: ".$query." - ".mysql_error());
			$r=mysql_fetch_array($rest);
			$funci1=$r['funcion'];
			$campop=$r['campop'];
			if($funci1=='' || strlen($funci1)==0)//sino tiene funcion tomo el campo origen
				$funci1="n.`".$campop."`";
			else 
				$funci1=str_replace("`".$r['campop']."`","n.`".$r['campop']."`", $r['funcion']);
			$campoIndice=trim($funci1);
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

			//Corregimos el Parcel ID para los Condados: Que se estan Duplicando en el Psummary
			if($nom_bd == "flpasco" or $nom_bd == "florange" or $nom_bd == "flvolusia" or $nom_bd == "flpolk"){
				$query = "UPDATE $tblnalnal set
						$campop = concat(
									Mid(
										replace(replace(replace(replace(replace(replace(replace(replace(replace(`$campop`,'\"',''),\"'\",''),'/',''),'\\\',''),'.',''),',',''),' ',''),'-',''),'_',''),5,2) ,
									Mid(
										replace(replace(replace(replace(replace(replace(replace(replace(replace(`$campop`,'\"',''),\"'\",''),'/',''),'\\\',''),'.',''),',',''),' ',''),'-',''),'_',''),3,2) ,
									Mid(
										replace(replace(replace(replace(replace(replace(replace(replace(replace(`$campop`,'\"',''),\"'\",''),'/',''),'\\\',''),'.',''),',',''),' ',''),'-',''),'_',''),1,2) ,
									Mid(
										replace(replace(replace(replace(replace(replace(replace(replace(replace(`$campop`,'\"',''),\"'\",''),'/',''),'\\\',''),'.',''),',',''),' ',''),'-',''),'_',''),7,13))";
				mysql_query($query) or die(mysql_error()."<BR>PROCESS UPDATE PARCELS IDS FOR SON COUNTYS");
			}
			//CONSTRUYENDO LOS CAMPOS DEL SELECT DE LA TABLA DEL NALNAL CON LAS FUNCIONES DE MAPEO 
			//CONSTRUYENDO LOS CAMPOS DEL INSERT INTO DE LA TABLA DEL PSUMMARY
			//echo  "<br/>".
			$query="SELECT m.`campo`,m.`campoP`, m.`funcion`
					FROM `mantenimiento`.mapeo m
					INNER JOIN `mantenimiento`.estructuras e ON (e.structid=m.structid)
					WHERE m.`tblID`=13 AND e.`tblResult`='$tblnalnal'";
			$rest=mysql_query($query) or die("ERROR: D: ".$query." - ".mysql_error());
			$selectfield='';
			$insertintofield='';
			$iwhi=0;
			while($r=mysql_fetch_array($rest))
			{
				$campoP=$r['campoP'];
				$campo=$r['campo'];
				$funcion=trim($r['funcion']);

				if($iwhi>0)
				{
					$selectfield.=',';
					$insertintofield.=',';
				}
				$insertintofield.=$campo;

				if($funcion=='')
					$selectfield.=' n.`'.$campoP.'`';
				else
					$selectfield.=parse_funcion($funcion,'n');

				$iwhi++;	
			}
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			if($nom_bd == "flduval")
			{//adicionado po smith 15/08/2012
				//QUITAR LAS R DEL NAL DE DUVAL DE $tblnalnal	
				//echo  "<BR>".
				$query="UPDATE $tblnalnal n
						SET n.parcel_id=replace($campoIndice,'R','')";
				$rest=mysql_query($query) or die("ERROR: E: ".$query." - ".mysql_error());
				echo  "<BR>QUITAR LAS R DEL NAL DE $tblnalnal: ".mysql_affected_rows();
			}
			//return;
			//INSERTANDO REGISTROS NUEVOS A PSUMAMRY DE $tblnalnal	
			//echo  "<BR>".
			$query="INSERT INTO psummary ($insertintofield)
					SELECT $selectfield 
					FROM $tblnalnal n
					LEFT JOIN psummary p ON (p.parcelid=$campoIndice)
					WHERE p.parcelid IS NULL AND LENGTH($campoIndice)>0";

			$rest=mysql_query($query) or die("ERROR: E: ".$query." - ".mysql_error());
			$cant_nal_insertados=mysql_affected_rows();
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			
			//Obtengo los Nombres de los Campos a Actualizar con su Respectiva funcion (si tienen) como vienen en el Mapeo
			$campoAddress=NAL_obtener_campos("address",$nom_bd);			//Owner Name
			$campoOwner=NAL_obtener_campos("owner",$nom_bd);			//Owner Name
			$campoOwner_a=NAL_obtener_campos("Owner_a",$nom_bd);		//Owner Address
			$campoOwner_z=NAL_obtener_campos("Owner_z",$nom_bd);		//Owner Zip
			$campoOwner_c=NAL_obtener_campos("Owner_c",$nom_bd);		//Owner City
			$campoOwner_s=NAL_obtener_campos("Owner_s",$nom_bd);		//Owner State
			$campoOwner_p=NAL_obtener_campos("Owner_p",$nom_bd);		//Owner Phone
			$campoXcode=NAL_obtener_campos("xcode",$nom_bd);			//Xcode
			$campoLegal=NAL_obtener_campos("legal",$nom_bd);			//Legal
			$campoAssesedV=NAL_obtener_campos("AssesedV",$nom_bd);		//AssesedValue
			$campoBuildingV=NAL_obtener_campos("BuildingV",$nom_bd);	//BuildingValue
			$campoLandV=NAL_obtener_campos("LandV",$nom_bd);			//LandValue
			$campoTaxableV=NAL_obtener_campos("TaxableV",$nom_bd);		//TaxableValue
			$campoSaleDate=NAL_obtener_campos("SaleDate",$nom_bd);		//SaleDate
			$campoSalePrice=NAL_obtener_campos("SalePrice",$nom_bd);	//SalePrice
			$campoZip=NAL_obtener_campos("zip",$nom_bd);				//ZIP
			$campoOzip=NAL_obtener_campos("ozip",$nom_bd);				//OZIP
			$campoYrbuilt=NAL_obtener_campos("yrbuilt",$nom_bd);		//YRBUILT
			$campoUnits=NAL_obtener_campos("units",$nom_bd);			//units
			$campoBook=NAL_obtener_campos("book",$nom_bd);				//book
			$campoPage=NAL_obtener_campos("page",$nom_bd);				//page
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			
			//Actualizo el XCODE en psummary para el tipo 99 con respecto al nalnal para Saber si una propiedad cambio su tipo
			$query="UPDATE psummary p
					INNER JOIN $tblnalnal n ON (p.parcelid=$campoIndice)
					SET p.xcode=$campoXcode
					WHERE p.xcode='99'";
			$rest=mysql_query($query) or die("ERROR: F: ".$query." - ".mysql_error());
			$cant_xcode=mysql_affected_rows();
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			
			//Actualizacion de campos AssesedV, BuildingV, LandV, TaxableV del psummary   -->XCODE,SLEGAL,PHY_ZIPCD (ZIP Y OZIP)
			//PONER LO QUE TRAE EL NAL PARA EL CAMPO BUILDINGV A AQUELLOS QUE TENGAN NEGTIVOS EN LA RESTA	//adicionado po smith 15/08/2012
			$query="UPDATE psummary p
					INNER JOIN $tblnalnal n ON (p.parcelid=$campoIndice)
					SET p.zip=$campoZip,
						p.ozip=$campoOzip,
						p.legal=$campoLegal,
						p.AssesedV=$campoAssesedV,
						p.BuildingV=IF(($campoAssesedV-$campoLandV)<0,n.JV,$campoAssesedV-$campoLandV),
						p.LandV=$campoLandV,
						p.Yrbuilt=$campoYrbuilt,
						p.units=$campoUnits,
						p.TaxableV=$campoAssesedV";
						//						p.AssesedV=$campoAssesedV,p.TaxableV=$campoTaxableV";p.BuildingV=$campoBuildingV,

			$rest=mysql_query($query) or die("ERROR: G: ".$query." - ".mysql_error());
			$cant_nal_actualizados=mysql_affected_rows();
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

			//Actualizacion de campos AssesedV, BuildingV, LandV, TaxableV del psummary   -->XCODE,SLEGAL,PHY_ZIPCD (ZIP Y OZIP)
			//PONER LOS BUILDINGV NEGTIVOS EN CERO
			$query="UPDATE psummary p SET p.BuildingV=0 where p.BuildingV<0";
			$rest=mysql_query($query) or die("ERROR: G: ".$query." - ".mysql_error());
			echo  "<BR>PONER LOS BUILDINGV NEGTIVOS EN CERO DE PSUMMARY: ".mysql_affected_rows();
			
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			//Actualizo los OWNER del Psummary que esten vacios o nulos con los OWNER del NALNAL que Tengan informacion
			//Esto Incluye el Owner: name, address, city, state and zip
			$query="UPDATE psummary p
					INNER JOIN $tblnalnal n ON (p.parcelid=$campoIndice)
					SET ";
						if($campoOwner<>'' && strlen($campoOwner)>0)$query.="p.owner=$campoOwner,";
						if($campoOwner_a<>'' && strlen($campoOwner_a)>0)$query.="p.owner_a=$campoOwner_a,";
						if($campoOwner_c<>'' && strlen($campoOwner_c)>0)$query.="p.owner_c=$campoOwner_c,";
						if($campoOwner_p<>'' && strlen($campoOwner_p)>0)$query.="p.owner_p=$campoOwner_p,";
						if($campoOwner_s<>'' && strlen($campoOwner_s)>0)$query.="p.owner_s=$campoOwner_s,";
						if($campoSaleDate<>'' && strlen($campoSaleDate)>0)$query.="p.saledate=$campoSaleDate,";
						if($campoSalePrice<>'' && strlen($campoSalePrice)>0)$query.="p.saleprice=$campoSalePrice,";
						if($campoBook<>'' && strlen($campoBook)>0)$query.="p.book=$campoBook,";
						if($campoPage<>'' && strlen($campoPage)>0)$query.="p.page=$campoPage,";
						if($campoOwner_z<>'' && strlen($campoOwner_z)>0)$query.="p.owner_z=$campoOwner_z ";
			//$query.=" WHERE (LENGTH(TRIM(p.owner))<1 OR p.owner IS NULL) AND LENGTH(TRIM(n.own_name))>1"; // y que tambien tenga fecha y monto en el NALNAL	//Comented By Jesus and freddy 01/03/2013
			$query.=" WHERE (TRIM(p.owner) <> TRIM($campoOwner)) AND  p.saledate < $campoSaleDate"; // y que tambien tenga fecha y monto en el NALNAL
			//echo "<br>$query<br>";
			$rest=mysql_query($query) or die("ERROR: H: ".$query." - ".mysql_error());
			$cant_own_actualizados=mysql_affected_rows();
			echo  "<BR>Actualizo los OWNER del Psummary : ".$cant_own_actualizados;
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

			//Actualizo los ADDRESS del Psummary  que esten vacios o nulos con los ADDRSS del RESIDENTIAL que Tengan informacion
			//y cuya direccion empiece con numero
			//echo "<BR>".
			$query="UPDATE psummary a
					inner join mlsresidential n on (a.parcelid=n.parcelid)
					SET a.address=n.address
					WHERE (LENGTH(TRIM(a.address))<1 OR a.address IS NULL) AND substring_index(n.address,' ',1) REGEXP '^[0-9]'";
			mysql_query($query) or die("ERROR: I:1 ".$query." - ".mysql_error());
			
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			//ACTUALIZO TODOS LOS BOOK Y PAGE, SIEMPRE Y CUANDO LOS OWNER SEAN IGUALES
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$query="UPDATE psummary a
					INNER JOIN $tblnalnal n ON (a.parcelid=$campoIndice)
					SET
						a.book=IF(LENGTH($campoBook)>0,$campoBook,a.book),
						a.page=IF(LENGTH($campoPage)>0,$campoPage,a.page),
						a.saledate=IF(LENGTH($campoSaleDate)>0,$campoSaleDate,a.page),
						a.saleprice=IF(LENGTH($campoSalePrice)>0,$campoSalePrice,a.page)
					WHERE a.owner=$campoOwner AND $campoSalePrice>=19000 AND a.saledate<$campoSaleDate";
			//mysql_query($query) or die("ERROR: I:2 ".$query." - ".mysql_error());
			
/*			//Creo mi Tabla Auxiliar Para actualizar los Registros Mucho mas Rapido
			$tabla_Aux="address_unit_aux";
			$query = "	
				CREATE TABLE IF NOT EXISTS $tabla_Aux (
				  `parcelid` varchar(25) DEFAULT NULL,
				  `address` varchar(50) DEFAULT NULL,
				  `unit` varchar(25) DEFAULT NULL,
				  KEY `parcelid` (`parcelid`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1";
			$result = mysql_query($query) or die ("ERROR: I: ".$query." - ".mysql_error());
			if(!$result)echo "Tabla Temporal No Creada<BR>"; else echo "Tabla Temporal Creada con Exito<BR>";

			
			//Limpio la Tabla
			$query="TRUNCATE TABLE $tabla_Aux";
			$result = mysql_query($query) or die("ERROR: J: ".$query." - ".mysql_error());
			if(!$result)echo "Tabla Temporal No Vaciada<BR>"; else echo "Tabla Temporal Vaciada con Exito<BR>";
			
			if($unit_unitutni1=='si'){ //Se valida porque no todos los condados tienen los unit en la tabla nalnal
				//Correccion de las Direcciones que pertencen al xcode='04' (Condominios) para extraer el unit y borrarlo de la direccion
				$query="SELECT a.parcelid,a.address,a.unit,TRIM(CONCAT(TRIM(b.phy_addr1),' ',TRIM(b.phy_addr2))) as phy_addr1 FROM psummary a
						INNER JOIN $tblnalnal b ON(a.parcelid=b.parcel_id)
						WHERE a.xcode='04'";
				$rest=mysql_query($query) or die("ERROR: K: ".$query." - ".mysql_error());
				$C=1;$Cuenta=0;$query="";
				while($row=mysql_fetch_array($rest)){
					if($Cuenta>0){$query.=",";}
					$bandera=0;
					$adres_UNIT=explode(" UNIT ",$row['phy_addr1']);
					$unit=NAL_quitarCaracteres($adres_UNIT[1]);
					$addressNAL=str_replace(" ".$unit,'',$adres_UNIT[0]); //Comento los cuatro (4) siguientes codigos porque estaba modificando y actualizando la direccion del psummary
																	//Con la que viene del nal nal
					if(count($adres_UNIT)==1){
						$adres_UN=explode(" UN ",$row['phy_addr1']);
						$unit=NAL_quitarCaracteres($adres_UN[1]);
						$addressNAL=str_replace(" ".$unit,'',$adres_UN[0]);
						if(count($adres_UN)==1){
							$adres_W=explode(" #",$row['phy_addr1']);
							$unit=NAL_quitarCaracteres($adres_W[1]);
							$addressNAL=str_replace(" ".$unit,'',$adres_W[0]);
							if(count($adres_W)==1){
								$unit2=explode(" ",$row['phy_addr1']);
								if(is_numeric($unit2[count($unit2)-1])){
									$unit=NAL_quitarCaracteres($unit2[count($unit2)-1]);
									$addressNAL=str_replace(" ".$unit,'',$row['phy_addr1']);
								}else{
									$bandera=1;
									if(!NAL_existetexto(trim($unit2[count($unit2)-1]))){
										$unit=NAL_quitarCaracteres($unit2[count($unit2)-1]); 
										//Armo la direccion sin la ultima posicion que vendria siendo el unit
										$addressNAL="";
										for($i=0;$i<count($unit2)-1;$i++){
											$addressNAL.=$unit2[$i]." ";
										}
										$addressNAL=trim($addressNAL);
									}else{
										$unit='';
										$addressNAL=$row['phy_addr1'];
									}
								}
							}
						}
					}
					$unit=trim(NAL_corregirunit($unit)); //Corregimos el Unit que no traiga nada de las Abreviaciones que utilizamos al final de nuestras direcciones
					
					//Arreglo la Direccion que Viene del Psummary
					$aux=explode(" ",NAL_quitarCaracteres($row['address']));
					$ndir="";
					//Recorro cada una de las Palabras
					foreach($aux as $string){
						//Busco si la Palabra Existe en mi Base de Datos que en este caso viene siendo una funcion
						if(NAL_BuscaenAddress($string)){
							//Corrijo la Palabra que encontre mala
							$string=NAL_CorregirAddress($string);
							$dir++;
						}
						$ndir .= $string." ";
					}
					$dir2=$ndir;
					$aux=explode(" ",$ndir);
					foreach($aux as $string){
						if(NAL_existetexto_address(trim($string))){
							//echo $string;
							$aux1=explode(trim($string),$ndir);
							$ndir=trim($aux1[0])." $string";
							break;
						}else{
							$ndir=$dir2;
						}
					}
*/
			//////////////Esto estaba comentado//////////////
			/*		if(is_numeric($aux[count($aux)-1])){
						for($i=0;$i<count($aux)-1;$i++){
							$address1.=$aux[$i]." ";
						}
						$address1=trim($address1);
					}else{
						if(!Nal_existetexto(trim($aux[count($aux)-1]))){
							for($i=0;$i<count($aux)-1;$i++){
								$address1.=$aux[$i]." ";
							}
							$address1=trim($address1);				
						}else{
							$address1=$row['address'];
						}
					}
			*/
			//////////////Hasta Aqui//////////////
/*					if(strlen(trim($ndir))<1){$ndir=$addressNAL;}
					$query.="('$row[parcelid]','$ndir','$unit')";
					$Cuenta++;
					if($Cuenta==100){ //Cada 100 Registros los Inserto en la Tabla Temporal
						$query="INSERT INTO $tabla_Aux (parcelid,address,unit) VALUES ".$query;
						mysql_query($query) or die("ERROR: L: ".$query." - ".mysql_error());
						$Cuenta=0;
						$query="";
					}*/
			//////////////Esto estaba comentado//////////////
			//		if($bandera==1){ // hago esta condicion para mostrar solo los que no tienen la palabra unit y que tampoco terminen en un numero
						/*echo $C++." ParcelID: $row[parcelid] <BR><strong>- Psummary: </strong>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-Address: $row[address] &nbsp;&nbsp;&nbsp;&nbsp; 2-Unit: $row[unit] &nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;<strong>- NalNal </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1- Address: $row[phy_addr1]
							<BR><strong>- Nuevo: </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1- Direccion: $ndir <BR> 
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Direccion sin Modificar: $dir2
							<BR> 
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Direccion del NAL: $addressNAL
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2- Unit: $unit Total Vector: ".count($adres_UNIT)."
							<BR><BR>";*/
			//		}
			//////////////Hasta Aqui//////////////
/*				}
				if($query<>""){
					$query="INSERT INTO $tabla_Aux (parcelid,address,unit) VALUES ".$query;
					mysql_query($query) or die("ERROR: M: ".$query." - ".mysql_error());
				}
				$query="UPDATE psummary a 
						INNER JOIN $tabla_Aux b ON (a.parcelid=b.parcelid)
						SET 
							a.unit=b.unit,
							a.address=b.address
						WHERE LENGTH(TRIM(b.unit))>0";
				mysql_query($query) or die("ERROR: M: ".$query." - ".mysql_error());
			} // fin de la validacion para saber su chekeo el unit en el ejecutador de mapeo
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		*/
		} //fin de la Validacion de si existe la tabla nal o no
/*
		//Correccion de todas las Direcciones del Condado que el xcode sea diferente a '04' (osea que no sean condominios)
		//Limpio la Tabla
		$query="TRUNCATE TABLE $tabla_Aux";
		$result = mysql_query($query) or die("ERROR: J: ".$query." - ".mysql_error());
		if(!$result)echo "Tabla Temporal No Vaciada<BR>"; else echo "Tabla Temporal Vaciada con Exito<BR>";
		
		$query="SELECT parcelid,address FROM psummary
				WHERE xcode<>'04' AND LENGTH(TRIM(address))>1 AND address IS NOT NULL";
		$rest=mysql_query($query) or die("ERROR: K: ".$query." - ".mysql_error());
		$C=1;$Cuenta=0;$query="";
		while($row=mysql_fetch_array($rest)){
			if($Cuenta>0){$query.=",";}

			//Arreglo la Direccion que Viene del Psummary
			$aux=explode(" ",NAL_quitarCaracteres($row['address']));
			$ndir="";
			//Recorro cada una de las Palabras
			foreach($aux as $string){
				//Busco si la Palabra Existe en mi Base de Datos que en este caso viene siendo una funcion
				if(NAL_BuscaenAddress($string)){
					//Corrijo la Palabra que encontre mala
					$string=NAL_CorregirAddress($string);
					$dir++;
				}
				$ndir .= $string." ";
			}
			$dir2=$ndir;
			$aux=explode(" ",$ndir);
			foreach($aux as $string){
				if(NAL_existetexto_address(trim($string))){
					//echo $string;
					$aux1=explode(" ".trim($string)." ",$ndir);
					$ndir=trim($aux1[0])." $string";
					break;
				}else{
					$ndir=$dir2;
				}
			}
			
			$query.="('$row[parcelid]','$ndir','')";
			$Cuenta++;
			if($Cuenta==100){ //Cada 100 Registros los Inserto en la Tabla Temporal
				$query="INSERT INTO $tabla_Aux (parcelid,address,unit) VALUES ".$query;
				mysql_query($query) or die("ERROR: L: ".$query." - ".mysql_error());
				$Cuenta=0;
				$query="";
			}*/
			//////////////Esto Estaba Comentado//////////////
	//		if($bandera==1){ // hago esta condicion para mostrar solo los que no tienen la palabra unit y que tampoco terminen en un numero
				/*echo $C++." ParcelID: $row[parcelid] <BR><strong>- Psummary: </strong>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-Address: $row[address] &nbsp;&nbsp;&nbsp;&nbsp; 2-Unit: $row[unit] &nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;<strong>- NalNal </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1- Address: $row[phy_addr1]
					<BR><strong>- Nuevo: </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1- Direccion: $ndir <BR> 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Direccion sin Modificar: $dir2
					<BR> 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Direccion del NAL: $addressNAL
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2- Unit: $unit Total Vector: ".count($adres_UNIT)."
					<BR><BR>";*/
	//		}
			//////////////Hasta Aqui//////////////
/*		}
		if($query<>""){
			$query="INSERT INTO $tabla_Aux (parcelid,address,unit) VALUES ".$query;
			mysql_query($query) or die("ERROR: M: ".$query." - ".mysql_error());
		}
		
		$query="UPDATE psummary a 
				INNER JOIN $tabla_Aux b ON (a.parcelid=b.parcelid)
				SET a.address=b.address";
		mysql_query($query) or die("ERROR: N: ".$query." - ".mysql_error());
*/
		//GENERAR LOS CSV DE LOS POTES DEL PSUMMARY
		$que="	SELECT l.`ximapro` FROM xima.lscounty l
				WHERE lower(l.`BD`)='".strtolower($nom_bd)."'
				limit 1";
		$rest3=mysql_query($que) or die("566T: ".$que.mysql_error());
		$r=mysql_fetch_array($rest3);
		$xpro=$r['ximapro'];
		//if($xpro=='Y')
		{
			$que="	SELECT `tblResult` 
					FROM `mantenimiento`.`estructuras`
					WHERE lower(`bdResult`)='".strtolower($nom_bd)."' and tblResult like '%POTPOT'
					limit 1";
			$rest4=mysql_query($que) or die("566T: ".$que.mysql_error());
			$r=mysql_fetch_array($rest4);
			$tblRes=strtolower($r['tblResult']);

			$table='psummary';
			
			if(strlen($tblRes)>0)//existe la estrutura
			{
				$ruta="$DISCO:\\\\XIMA\\\\download\\\\".substr($nom_bd,0,2)."\\\\".substr($nom_bd,2)."\\\\pote\\\\".$tblRes."\\\\pote1.csv";
				if (file_exists($ruta)) unlink($ruta);
				$query = "	SELECT *  
							INTO OUTFILE '".$ruta."'	
							FIELDS TERMINATED BY '\\t' 
							FROM $nom_bd.$table"; 
				$result = mysql_query($query) or die("168T: ".mysql_error());
				echo "<br>TABLA: ".strtoupper($table)." PATH: ".$ruta;
			}
			else
				echo '<BR><strong style="color:#FF0000">NO ESTA CREADA LA ESTRUTURA POTPOT PARA EL CONDADO: '.$nom_bd.'</strong>';
		}
		echo "<BR>Hora Fin: ".date("d-m-Y H:i:s")."<BR>";
		
		//$num_reg=mysql_affected_rows();
		//echo "<br />Inserta NAL al Psummary, Actualiza Owner, Actualiza UNIT y Corrige Direcciones de Psummary: ".$num_reg;
	}else{
		echo "<BR> No se Ejecuto el NAL ya que en el Ejecutador de Mapeo no se Selecciono";
	}

	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=665;
?>
