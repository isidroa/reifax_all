<?php 

if($nom_bd=='flbroward' || $nom_bd=='fldade')
{
	$inicon=substr($nom_bd,0,5);
	//$tabla = $inicon.'cfcf';
	$tabla = 'coufor';
	
	$que="SHOW INDEX FROM ".$tabla." WHERE KEY_NAME='index_case'";
	$res=mysql_query($que) or die("620T: ".$que.mysql_error());
	$total=mysql_num_rows($res);
	
	if($total==0){
		$que="ALTER TABLE ".$tabla." MODIFY COLUMN `CaseNum` VARCHAR(100) CHARACTER SET 
		latin1 COLLATE latin1_swedish_ci DEFAULT NULL, ADD INDEX `index_case`(`CaseNum`)";
		mysql_query($que) or die("620T: ".$que.mysql_error());
	}
	
	$que="update ".$tabla." c inner join pendes p on c.casenum=p.case_numbe
		set c.parcelid=p.parcelid
		where length(c.parcelid)=0";
	mysql_query($que) or die("620T: ".$que.mysql_error());
	
	$que="insert into pendes (case_numbe, parcelid, JUDGEAMT, FINALBID, Address, JUDGEDATE, pof)
		select left(c.casenum,20), c.parcelid, procs.numeros(c.JudgeAmount),procs.numeros(if(c.Maxbid<>'Hidden', c.MaxBid, 0.00)),
		c.Address, if(date(c.date)>curdate(),c.date,''), if(date(c.date)>curdate(),'F','P')
		from ".$tabla." c left join pendes p on c.parcelid=p.parcelid
		where length(c.parcelid)>0 and (p.parcelid is null or length(p.parcelid)=0)
		group by c.parcelid";
	mysql_query($que) or die("620T: ".$que.mysql_error());
	
	$num_reg=mysql_affected_rows();
	echo "<br />Registros nuevos insertados en pendes ".$num_reg;
	
	//Actualizacion del pendes en funcion del coufor
	$que="UPDATE pendes p 
		inner join ".$tabla." c on p.parcelid=c.parcelid
		SET p.case_numbe=left(c.casenum,20), p.JUDGEAMT=procs.numeros(c.JudgeAmount),
		p.FINALBID=procs.numeros(if(c.Maxbid<>'Hidden', c.MaxBid, 0.00)),
    p.Address=c.Address, p.JUDGEDATE=if(date(c.date)>curdate(),c.date,''),
    p.pof=if(date(c.date)>curdate(),'P','F')
    where length(c.parcelid)>0";
	mysql_query($que) or die("620T: ".$que.mysql_error());

	$num_reg=mysql_affected_rows();
	echo "<br />Actualizacion pendes con county foreclosure por parcelid: ".$num_reg;
	
	$que="UPDATE pendes p 
		inner join ".$tabla." c on p.parcelid=c.parcelid
		SET p.case_numbe=left(c.casenum,20), p.JUDGEAMT=procs.numeros(c.JudgeAmount), 
		p.FINALBID=procs.numeros(if(c.Maxbid<>'Hidden', 
		c.MaxBid, 0.00)), p.Address=c.Address,
		p.POF='P', p.JUDGEDATE=''   
		where length(c.parcelid)>0 and (c.status='Canceled Per County' or c.status='Canceled Per Order' 
		or c.status='Auction did not meet County Requirements' or c.status='Canceled Per Bankruptcy')";
	mysql_query($que) or die("620T: ".$que.mysql_error());

	$num_reg=mysql_affected_rows();
	echo "<br />Actualizacion pendes con county foreclosure por parcelid: ".$num_reg;
	
	$num_reg_procesos=$num_reg; 
	$actionID_procesos=-1;
	$ID_procesos=620;	
}
?>