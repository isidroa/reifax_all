<?php 
$MAXLONGPID=30;//LONGITUD GENERAL DE LOS PARCELID DE TODOS LOS CONDADOS

if($PRIMERMSX==0 || strlen($PRIMERMSX)==0 || $PRIMERMSX=='' )
{
	if(strlen($PRIMERMSX)==0 && $PRIMERMSX=='' )
		require('includes/conexion.php');
 
//----------------------------------------------------------------------------
	function getCaracteres($cad){
		$cad=strtoupper($cad);
		//echo $cad.'=';
		$cad=str_replace('CIRCLE','CIR',$cad);
		$cad=str_replace('CIRCL','CIR ',$cad);
		$cad=str_replace('CORT','CT',$cad);
		$cad=str_replace('COURT','CT',$cad);
		$cad=str_replace('PLACE','PL',$cad);
		$cad=str_replace('STREET','ST',$cad);
		$cad=str_replace('TERRACE','TER',$cad);
		$cad=str_replace('LINE','LN',$cad);
		$cad=str_replace('DRIVE','DR',$cad);
		$cad=str_replace('BOULEVARD','BLVD',$cad);
		$cad=str_replace('PARKWAY','PKWY',$cad);
		$cad=str_replace('ROAD','RD',$cad);
		$cad=str_replace('RUN','RN',$cad);
		$cad=str_replace('TRAIL','TRL',$cad);
		$cad=str_replace('TRACE','TRCE',$cad);
		if($cad=='AVENUE')$cad='AVE';
		if($cad=='AV')$cad='AVE';
		if($cad=='EAST')$cad='E';
		if($cad=='EA')$cad='E';
		if($cad=='SOUTH')$cad='S';
		if($cad=='SO')$cad='S';
		if($cad=='NORTH')$cad='N';
		if($cad=='NO')$cad='N';
		if($cad=='WEST')$cad='W';
		if($cad=='WE')$cad='W';
		return $cad;
	}
	
	function getCaracteres2($cad){
		$cad=strtoupper($cad);
		//echo $cad.'=';
		$cad=str_replace('CIR','CIRCLE',$cad);
	//	$cad=str_replace('CIR ','CIRCL',$cad);
		$cad=str_replace('CT','CORT',$cad);
		$cad=str_replace('PL','PLACE',$cad);
		$cad=str_replace('ST','STREET',$cad);
		$cad=str_replace('TER','TERRACE',$cad);
		$cad=str_replace('LN','LINE',$cad);
		$cad=str_replace('DR','DRIVE',$cad);
		$cad=str_replace('BLVD','BOULEVARD',$cad);
		$cad=str_replace('PKWY','PARKWAY',$cad);
		$cad=str_replace('RD','ROAD',$cad);
		$cad=str_replace('RN','RUN',$cad);
		$cad=str_replace('TRL','TRAIL',$cad);
		$cad=str_replace('TRCE','TRACE',$cad);
		if($cad=='AVE')$cad='AVENUE';
	//	if($cad=='AV')$cad='AVE';
		if($cad=='E')$cad='EAST';
	//	if($cad=='EA')$cad='E';
		if($cad=='S')$cad='SOUTH';
	//	if($cad=='SO')$cad='S';
		if($cad=='N')$cad='NORTH';
	//	if($cad=='NO')$cad='N';
		if($cad=='W')$cad='WEST';
	//	if($cad=='WE')$cad='W';
		return $cad;
	}
	
	function findCaracteres($cad){
		$cad=strtoupper($cad);
		$busq=$cad;
		//echo $cad.'=';
		$cad=str_replace('CIRCLE','CIR',$cad);
		$cad=str_replace('CIRCL','CIR ',$cad);
		$cad=str_replace('CORT','CT',$cad);
		$cad=str_replace('COURT','CT',$cad);
		$cad=str_replace('PLACE','PL',$cad);
		$cad=str_replace('STREET','ST',$cad);
		$cad=str_replace('TERRACE','TER',$cad);
		$cad=str_replace('LINE','LN',$cad);
		$cad=str_replace('DRIVE','DR',$cad);
		$cad=str_replace('BOULEVARD','BLVD',$cad);
		$cad=str_replace('PARKWAY','PKWY',$cad);
		$cad=str_replace('ROAD','RD',$cad);
		$cad=str_replace('AVENUE','AVE',$cad);
		$cad=str_replace('AV','AVE',$cad);
		$cad=str_replace('RUN','RN',$cad);
		$cad=str_replace('TRAIL','TRL',$cad);
		$cad=str_replace('TRACE','TRCE',$cad);
		$cad=str_replace('EAST','E',$cad);
		$cad=str_replace('EA','E',$cad);
		$cad=str_replace('SOUTH','S',$cad);
		$cad=str_replace('SO','S',$cad);
		$cad=str_replace('NORTH','N',$cad);
		$cad=str_replace('NO','N',$cad);
		$cad=str_replace('WEST','W',$cad);
		$cad=str_replace('WE','W',$cad);
		//echo $cad.'<br>';
		return $busq==$cad ? false : true;
	}
	
	function findCaracteres2($cad){
		$cad=strtoupper($cad);
		$busq=$cad;
		//echo $cad.'=';
		$cad=str_replace('CIR','CIRCLE',$cad);
	//	$cad=str_replace('CIR ','CIRCL',$cad);
		$cad=str_replace('CT','CORT',$cad);
		$cad=str_replace('PL','PLACE',$cad);
		$cad=str_replace('ST','STREET',$cad);
		$cad=str_replace('TER','TERRACE',$cad);
		$cad=str_replace('LN','LINE',$cad);
		$cad=str_replace('DR','DRIVE',$cad);
		$cad=str_replace('BLVD','BOULEVARD',$cad);
		$cad=str_replace('PKWY','PARKWAY',$cad);
		$cad=str_replace('RD','ROAD',$cad);
		$cad=str_replace('AVE','AVENUE',$cad);
	//	$cad=str_replace('AVE','AV',$cad);
		$cad=str_replace('RN','RUN',$cad);
		$cad=str_replace('TRL','TRAIL',$cad);
		$cad=str_replace('TRCE','TRACE',$cad);
		$cad=str_replace('E','EAST',$cad);
	//	$cad=str_replace('E','EA',$cad);
		$cad=str_replace('S','SOUTH',$cad);
	//	$cad=str_replace('S','SO',$cad);
		$cad=str_replace('N','NORTH',$cad);
	//	$cad=str_replace('N','NO',$cad);
		$cad=str_replace('W','WEST',$cad);
	//	$cad=str_replace('W','WE',$cad);
		//echo $cad.'<br>';
		return $busq==$cad ? false : true;
	}
	
	function quitarCaracteres2($cad)
	{
		$cad=strtoupper($cad);
		
		$cad=str_replace(' CIRCLE',' CIR ',$cad);
		$cad=str_replace(' CIRCL',' CIR ',$cad);
		$cad=str_replace(' CORT',' CT ',$cad);
		$cad=str_replace(' COURT',' CT ',$cad);
		$cad=str_replace(' PLACE',' PL ',$cad);
		$cad=str_replace(' STREET',' ST ',$cad);
		$cad=str_replace(' TERRACE',' TER ',$cad);
		$cad=str_replace(' LINE',' LN ',$cad);
		$cad=str_replace(' DRIVE',' DR ',$cad);
		$cad=str_replace(' BOULEVARD',' BLVD ',$cad);
		$cad=str_replace(' PARKWAY',' PKWY ',$cad);
		$cad=str_replace(' ROAD',' RD ',$cad);
		$cad=str_replace(' AVENUE',' AVE ',$cad);
		$cad=str_replace(' RUN',' RN ',$cad);
		$cad=str_replace(' TRAIL',' TRL ',$cad);
		$cad=str_replace(' TRACE',' TRCE ',$cad);
		$cad=str_replace(' EAST',' E ',$cad);
		$cad=str_replace(' SOUTH',' S ',$cad);
		$cad=str_replace(' NORTH',' N ',$cad);
		$cad=str_replace(' WEST',' W ',$cad);
	
		
		$cad=str_replace("^","",$cad);
		$cad=str_replace("'","",$cad);
		$cad=str_replace("#","",$cad);
		$cad=str_replace(".","",$cad);
		$cad=str_replace('"','',$cad);
		$cad=str_replace('/','',$cad);
		$cad=str_replace('  ',' ',$cad);
		$cad=str_replace('  ',' ',$cad);
		$cad=str_replace('   ',' ',$cad);
		$cad=str_replace('    ',' ',$cad);
		$cad=str_replace('     ',' ',$cad);
		$cad=str_replace('\\','',$cad);
		$cad=str_replace('-','',$cad);
		$cad=str_replace('\r\n',' ',$cad);
		return $cad;
	}
	
	function existetexto($cad)
	{
		$pos = strpos($cad, 'CIR');	if($pos!==false)return true;
		$pos = strpos($cad, 'PL');	if($pos!==false)return true;
		$pos = strpos($cad, 'ST');	if($pos!==false)return true;
		$pos = strpos($cad, 'TER');	if($pos!==false)return true;
		$pos = strpos($cad, 'AVE');	if($pos!==false)return true;
		$pos = strpos($cad, 'LN');	if($pos!==false)return true;
		$pos = strpos($cad, 'DR');	if($pos!==false)return true;
		$pos = strpos($cad, 'BLVD');	if($pos!==false)return true;
		$pos = strpos($cad, 'PKWY');	if($pos!==false)return true;
		$pos = strpos($cad, 'RD');	if($pos!==false)return true;
		$pos = strpos($cad, 'AVE');	if($pos!==false)return true;
		$pos = strpos($cad, 'RN');	if($pos!==false)return true;
		$pos = strpos($cad, 'TRL');	if($pos!==false)return true;
		$pos = strpos($cad, 'E');	if($pos!==false)return true;
		$pos = strpos($cad, 'S');	if($pos!==false)return true;
		$pos = strpos($cad, 'N');	if($pos!==false)return true;
		$pos = strpos($cad, 'W');	if($pos!==false)return true;
		return false;
	}
	
	function getOrdinal($number)
	{
		// get first digit
		$digit = abs($number) % 10;
		$ext = 'TH';
		$ext = ((abs($number) %100 < 21 && abs($number) %100 > 4) ? 'TH' : (($digit < 4) ? ($digit < 3) ? ($digit < 2) ? ($digit < 1) ? 'TH' : 'ST' : 'ND' : 'RD' : 'TH'));
		return $number.$ext;
	}
	
	function imprimir($array)
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
	
		// funcion que devuelve un array con los elementos de una hora
		function parteHora( $hora )
		{    
			$horaSplit = explode(":", $hora);
			if( count($horaSplit) < 3 )
			{
				$horaSplit[2] = 0;
			}
			return $horaSplit;
		}
	
		// funcion que devuelve la resta de dos horas en formato string
		function RestaHoras( $time1, $time2 )
		{
			list($hour1, $min1, $sec1) = parteHora($time1);
			list($hour2, $min2, $sec2) = parteHora($time2);
			return date('H:i:s', mktime( $hour1 - $hour2, $min1 - $min2, $sec1 - $sec2));
		}  	
	
		function tiempo()
		{
			$mtime = microtime();
			$mtime = explode(" ",$mtime);
			$mtime = $mtime[1] + $mtime[0];
			return $mtime;
		}
	
		function num_registros($tabla)
		{
			$query="SELECT COUNT(*)	FROM $tabla";
			$result = mysql_query($query) or die(mysql_error());
			$row=mysql_fetch_array($result);
	//echo $query." | ";
			return $row[0];
		}
		
		function parse_funcion($fun,$tipo){
			$aux = explode('`',$fun);
			$fun2='';
			foreach($aux as $k => $val){
				if(($k+1)<count($aux)){
					if($k%2==0)
						$fun2.=$val.$tipo.'.`';
					else
						$fun2.=$val.'`';
				}else
					$fun2.=$val;
			}
			return $fun2;
		}
	function quitarCaracteres3($cad)
	{
		$cad=strtoupper($cad);
		$cad=str_replace("^","",$cad);
		$cad=str_replace("'","",$cad);
		$cad=str_replace(".","",$cad);
		$cad=str_replace('"','',$cad);
		$cad=str_replace('/','',$cad);
		$cad=str_replace('  ',' ',$cad);
		$cad=str_replace('  ',' ',$cad);
		$cad=str_replace('   ',' ',$cad);
		$cad=str_replace('    ',' ',$cad);
		$cad=str_replace('     ',' ',$cad);
		$cad=str_replace('\\','',$cad);
		//$cad=str_replace('-','',$cad);
		$cad=str_replace('\r\n',' ',$cad);
		return $cad;
	}
	//ultimo dia del mes actual
	function UltimoDiaActualMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m'); 
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}

	function UltimoDiaAnteriorMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m')-1; 
		if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}

	function UltimoDiaAnteriorAnteriorMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m')-2; 
		if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
		if(date('m')==2){$VL_Ano--;$VL_Mes=11; }
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}

	function PrimerDiaAnteriorMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m')-1; 
		if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}

	function PrimerDiaAnteriorAnteriorMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m')-2; 
		if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
		if(date('m')==2){$VL_Ano--;$VL_Mes=11; }
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}
	
			
}//if($PRIMERMSX==1 || strlen($PRIMERMSX)==0 || $PRIMERMSX=='' )
//----------------------------------------------------------------------------
$TIMETOTALINI=tiempo();	
$TIMETOTALINIHORA=date("d/m/Y H:i:s");	
$TIMEINIHORA=date("H:i:s");	
echo "<br><strong>TIEMPO INICIO TOTAL: ".$TIMETOTALINIHORA."</strong><br>";

$starttimemain=tiempo();	
/****************************************************/
$CODIGOINDICADOR=date("YmdHis");	
//-----------------------------------------------
$TIMETRADUC = 0;
$TIMETRANS = 0;
$TIMEDEPU = 0;
$TIMESINC = 0;
$TIMECALC = 0;
$TIMEVALI = 0;
//-----------------------------------------------

$solotexto=0;//0->NO , 1->SI
if($_POST['solotexto']==1)$solotexto=1;

$tipo_mantenimiento=0; //0: Semanal, 1: Diario (mlsresidentialmlx), 2: Diario (Pendes).
$countproc=$_POST['countproc'];
$nom_bd=$_POST['cbd'];
$datanueva='dataserver1';
$phps="sintown";
$guardarAccess=$_POST['guardarAccess'];
$mes_estadistica=intval($_POST['mes_estadistica']);
if($_POST['genera_estadistica']==true) $genera_estadistica='si'; else $genera_estadistica='no';
if($guardarAccess==1)
{
	define("Libejecutar",true);
	define("Libfecha",$_POST['fechaAccess']);
}
else
	define("Libejecutar",false);
$todopsumm='no';
$calctip=0;
if($_POST['todopsumm']==true){$calctip=1;$todopsumm='si';}
$calctip=1;$todopsumm='si';

echo "<br><strong>fechaAccess: ".$_POST['fechaAccess'].", guardarAccess: $guardarAccess, todopsumm: $todopsumm, estadistica: $mes_estadistica </strong><br>";
//return;

$conn=conectar($nom_bd);
$q="select * from xima.parametros where campo='disco'";
$res3=mysql_query($q) or die($q.mysql_error());
$r3=mysql_fetch_array($res3);
$DISCO=$r3['valor'];

if($PRIMERMSX==0 || strlen($PRIMERMSX)==0 || $PRIMERMSX=='' )
	include('mantenLib.php');

$county = $nom_bd[0];
if($county=='M' || $county=='m') $county=strtolower(substr($nom_bd,3));
else $county=strtolower(substr($nom_bd,2));

//**************************************************************************************************************************************************************************************************************
$query="select pricecalculate from xima.lscounty where bd='$nom_bd'";
$rest7 = mysql_query($query) or die($query.mysql_error());
$r7=mysql_fetch_array($rest7);
$PRICECALCULATE=$r7['pricecalculate'];//LONGITUD GENERAL DE LOS PARCELID DE TODOS LOS CONDADOS
	
//**************************************************************************************************************************************************************************************************************
$array_param=str_replace('\\',"",$_POST['array_param']);

$tmp2='$vectp='.$array_param.';';	
eval($tmp2);
//	echo"<pre>";print_r($vectp);echo"<pre>";
$imaster=0;
for($ip=0;$ip<count($vectp);$ip++)
{
	echo "<br>";	
	echo "<h3>".($ip+1).".- ".strtoupper($vectp[$ip]["nproc"])." <i>".$nom_bd."</i></h3>";
	echo "****************************************************************";
	$vect2=$vectp[$ip]["arrp"];
	for($jp=0;$jp<count($vect2);$jp++)
	{
		$vect3=$vect2[$jp]["arrsp"];
		if(count($vect3)>0)
		{
			if(strpos($vect2[$jp]["nsproc"],'traduccion')===false && strpos($vect2[$jp]["nsproc"],'depuracion')===false ) $tieprecalc=0;
			else  $tieprecalc=1;
// " aqui:-> ".$tieprecalc."|";

			echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>".($jp+1).".- <u>Subproceso:</u> ".$vect2[$jp]["nsproc"]." <i>".$nom_bd."</i></strong>";
			echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----------------------------------------------------------------------------------------------------------<br>";
			for($kp=0;$kp<count($vect3);$kp++)
			{
				$id=$vect3[$kp];
				$php='ejecutaprocesos.php';
				$start_transf = 0;
				$start_traduc = 0;
				if($vect2[$jp]["nsproc"]=='traducciontransferencia')
				{$php='ejecutatransferencia.php';$tieprecalc=1;$start_transf=tiempo();}
				if($vect2[$jp]["nsproc"]=='traducciontraduccion')
				{$php='ejecutatraduccion.php';$tieprecalc=1;$start_traduc = tiempo();}
					require ($php);
				
				//Sumando los tiempos individuales			
				$TIMETRANS+=($start_transf)?(tiempo()-$start_transf):0;
				$TIMETRADUC+=($start_traduc)?(tiempo()-$start_traduc):0;
				$TIMEDEPU+=($tiempo_dep)?$tiempo_dep:0;
				$TIMESINC+=($tiempo_sinc)?$tiempo_sinc:0;
				$TIMECALC+=($tiempo_calc)?$tiempo_calc:0;
				$TIMEVALI+=($tiempo_vali)?$tiempo_vali:0;
				//---------------------------
				
				//SQL SEGUIMIENTO
				if($_POST['sqlSeg']!=''){
					$que = $_POST['sqlSeg'];
					$que = str_replace("\'","'",$que);
					$que = str_replace("\\\"","\"",$que);
					$result = mysql_query($que) or die("SqlSeguimiento: ".$que.mysql_error());
					$r = mysql_fetch_array($result);
					echo '<span style="color:#0A0;">';
					echo '<p>****************************************************</p>';
					echo '<p> Sql de Seguimiento</p>';
					echo '<p><strong>'.$que.' </strong></p>';
					echo '<p><strong>Resultado: '.$r[0].' </strong></p>';
					echo '<p>****************************************************</p>';
					echo '</span>';
				}
				//---------------------------
				$imaster++;
			}
			echo "<br>";	
		}
	}
}

$TIMETOTALFIN=tiempo();
$TIMETOTALSEG=$TIMETOTALFIN-$TIMETOTALINI;
echo "<br><br><strong>TIEMPO INICIO TOTAL: ".$TIMETOTALINIHORA."</strong><br>";
echo "<br><strong>TIEMPO FINAL TOTAL: ".date("d/m/Y H:i:s")."</strong><br>";
echo "<br><strong>TIEMPO TOTAL DE EJECUCION DE TODOS LOS PHP's: ".RestaHoras(date("H:i:s"),$TIMEINIHORA)." | SEGUNDOS:".$TIMETOTALSEG."<strong>";

//Ingresando tiempos
ingreso(568,$county,$TIMETRANS,$TIMETRANS);	
ingreso(569,$county,$TIMETRADUC,$TIMETRADUC);
ingreso(570,$county,$TIMEDEPU,$TIMEDEPU);
ingreso(571,$county,$TIMESINC,$TIMESINC);
ingreso(572,$county,$TIMECALC,$TIMECALC);
ingreso(573,$county,$TIMEVALI,$TIMEVALI);	

echo "|".$tieprecalc."|";
if($tieprecalc==1)
	ingreso(552,$county,$TIMETOTALSEG,$TIMETOTALSEG);//TIEMPO TOTAL precalculo
if($tieprecalc==0)
	ingreso(414,$county,$TIMETOTALSEG,$TIMETOTALSEG);//TIEMPO TOTAL sicronizacion
ingreso(551,$county,$calctip,$calctip);//TIPO DE CALCULO CORTO O LARGO
mysql_close();
?>