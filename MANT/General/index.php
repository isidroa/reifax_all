<html>
    <head>
        <title>Ejecutador de Mapeo</title>
        <script type="text/javascript" src="includes/mantenimiento.js"></script>
        <!--<script type="text/javascript" src="../LIB/firebug-lite.js"></script>-->
        <link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <?php 
			include('includes/conexion.php');
			$conn=conectar('xima'); 
        ?>
        

    </head>
    <body>
        <form name=formulario action="mantenimiento_respuesta.php" enctype="multipart/form-data" method="post" target="_blank">
            <div id="opciones" >
            	<!-- TITULO -->
                <div class="titulo">
                    MANTENIMIENTO DE DATA GENERAL
                </div>
                
                <!-- PARAMETROS -->
                <div class="subtitulo">
                    <!-- 
					<span class="tituloazul">
                        TAREAS  
                    </span>
<input name="guardarAccess" type="radio" id="guardarAccess2" value=0 >No
                    -->
					<span class="todos">
                        BD: 
                        <select name="cbd" id="cbd" onChange="if(this.value!=0){mostrar_datos(this.value,'idtraduccion');mostrar_datos(this.value,'iddepuracion');mostrar_datos(this.value,'idsincronizacion');mostrar_datos(this.value,'idcalculo');mostrar_datos(this.value,'idvalidaciones');}">
                            <option value="0" selected>Seleccione</option>
                                <?php
                                    $q='select DISTINCT state,county,bd from xima.lscounty order by county';
                                    $res=mysql_query($q) or die($q.mysql_error());
                                    while($r=mysql_fetch_array($res)){
                                        echo '<option value="'.strtolower($r['bd']).'">'.$r['state'].'/'.$r['county'].'</option>';
                                    }
                                ?>
                        </select>
						<?php 
							$q="select * from xima.parametros where campo='disco'";
                            $res3=mysql_query($q) or die($q.mysql_error());
                            $r3=mysql_fetch_array($res3);
							$disco=$r3['valor'];
						?>
						|Disk:<input name="disco" type="text" id="disco" value="<?php echo $disco ?>" size="2" maxlength="2" readonly="true" >						
						|Save:<input type="radio" name="guardarAccess" id="guardarAccess1" value=1 checked>Si
                		|Fecha:<input name="fechaAccess" type="text" id="fechaAccess1" value="<?php echo date('Y-m-d'); ?>" size="10" maxlength="10" onFocus="javascript: this.select();" onBlur="javascript: if(document.getElementById('guardarAccess1').checked==true) validarFecha(this.value);">
                        &nbsp;Estadistica<input type="checkbox" name="genera_estadistica" id="genera_estadistica" >Mes:<select name="mes_estadistica" id="mes_estadistica" style=" width:60px;" >
                	<option value=1 >ENE</option>
                    <option value=2 >FEB</option>
                    <option value=3 >MAR</option>
                    <option value=4 >ABR</option>
                    <option value=5 >MAY</option>
                    <option value=6 >JUN</option>
                    <option value=7 >JUL</option>
                    <option value=8 >AGO</option>
                    <option value=9 >SEP</option>
                    <option value=10>OCT</option>
                    <option value=11>NOV</option>
                    <option value=12>DIC</option>
                    </select>
				<input type="checkbox" name="todosphps" id="todosphps"  onChange="checkAllProcesos()" >Ejecuci&oacute;n
				<input type="checkbox" name="todopsumm" id="todopsumm" >Psummary
                    </span>
                </div>
                
                

                
				<div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('idseguimiento','imgseguimiento'); return false;">
                            <img id="imgseguimiento" src="includes/down.png" border="0" >
                        </a>
                        SQL de Seguimiento
						<div id="idseguimiento" style="display:none;">
							<p><textarea rows="2" cols="70" name="sqlSeg" id="sqlSeg"></textarea></p>
						</div>
                    </span>
                </div>
                <!-- TRADUCCION -->
                <div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('idtraduccion','imgtraduccion'); return false;">
                            <img id="imgtraduccion" src="includes/down.png" border="0" >
                        </a>
                        TRADUCCI&Oacute;N
                    </span>
                    <span class="todos">
                        <input type="checkbox" name="traduccion"  id="t*" value="t" onClick="checkSeleccion(document.formulario.traduccion, 1)">
                        Todos
                    </span>
                </div>
                <div id="idtraduccion" style="display:none;">
                </div>
                
                <!-- DEPURACION -->
                <div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('iddepuracion','imgdepuracion'); return false;">
                            <img id="imgdepuracion" src="includes/down.png" border="0" >
                        </a>
                        DEPURACI&Oacute;N
                    </span>
                    <span class="todos">
                        <input type="checkbox" name="depuracion"  id="d*" value="d" onClick="checkSeleccion(document.formulario.depuracion, 1)">
                        Todos
                    </span>
                </div>
                <div id="iddepuracion" style="display:none">
		        </div>
                
                <!-- SINCRONIZACION -->
                <div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('idsincronizacion','imgsincronizacion'); return false;">
                            <img id="imgsincronizacion" src="includes/down.png" border="0" >
                        </a>
                        SINCRONIZACI&Oacute;N
                    </span>
                    <span class="todos">
                        <input type="checkbox" name="sincronizacion"  id="s*" value="s" onClick="checkSeleccion(document.formulario.sincronizacion, 1)">
                        Todos
                    </span>
                </div>
                <div id="idsincronizacion" style="display:none">
                </div>
                
                <!-- CALCULO -->
                <div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('idcalculo','imgcalculo'); return false;">
                            <img id="imgcalculo" src="includes/down.png" border="0" >
                        </a>
                        CALCULO
                    </span>
                    <span class="todos">
                        <input type="checkbox" name="calculo"  id="c*" value="c" onClick="checkSeleccion(document.formulario.calculo, 1)">
                        Todos
                    </span>
                </div>
                <div id="idcalculo" style="display:none">
                </div>
                
                <!-- VALIDACIONES -->
                <div class="fondoazul">
                    <span class="tituloazul">
                        <a href="#" onClick="javascript:expandir('idvalidaciones','imgvalidaciones'); return false;">
                            <img id="imgvalidaciones" src="includes/down.png" border="0" >
                        </a>
                        VALIDACIONES
                    </span>
                    <span class="todos">
                        <input type="checkbox" name="validaciones"  id="v*" value="v" onClick="checkSeleccion(document.formulario.validaciones, 1)">
                        Todos
                    </span>
                </div>
                <div id="idvalidaciones" style="display:none">
                </div>
                <!-- BOTON EJECUTAR -->
                <div class="subtitulo">
                    <span class="tituloazul">
                        <INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" >
                        <input type="hidden" name="json_param" id="json_param" value="">
                        <input type="hidden" name="array_param" id="array_param" value="">
                        <input type="hidden" name="countproc" id="countproc" value="">						
                        <a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a>
                    </span>
                    <span class="todos">
                    </span>
                </div>
                
            </div>
        </form>
    </body>
</html>
