function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function expandir(fila,imagen)
{
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar";
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Mostrar";
	}

}

function limpiarchecks()
{
	var inputs= document.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) 
		if (inputs[i].type == 'checkbox') 
			inputs[i].checked=false;
}

function checkAllProcesos()
{
	var bd=document.getElementById("cbd").value;
	if(bd==null || bd=="0" || bd==""){alert("Debes seleccionar Base de Datos");return;}
	
	var inputs= document.getElementsByTagName('input');
	var tpchk=document.getElementById('todosphps').checked;
	for (var i = 0; i < inputs.length; i++) 
		if (inputs[i].type=='checkbox' && inputs[i].name!='todosphps' && inputs[i].name!='todopsumm'  && inputs[i].name!='genera_estadistica' ) 
			inputs[i].checked=tpchk;
}
function checkSeleccion(field, i) 
{
	document.getElementById('todosphps').checked=false;
	if (i == 0) //Todos
	{ 
		if (field[0].checked == true) 
			for (i = 1; i < field.length; i++) field[i].checked = true;
		else
			for (i = 1; i < field.length; i++) field[i].checked = false;
	}else{
		var inputs= document.getElementsByTagName('input');
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].type == 'checkbox' && inputs[i].name.substring(0,field.name.length) == field.name){
				if(field.checked==true)
					inputs[i].checked=true;
				else
					inputs[i].checked=false;
			}
		}
	}
}


function buscarParametros()
{
	//t*,d*,s*,c*,v*
	//traduccion,depuracion,sincronizacion,calculo,validaciones
	var countproc=0;
	var field,x=0,y=0;
	var datosarr = '';
	var datos = '{';
	var traduccion = '"traduccion":{';
	var depuracion = ',"depuracion":{';
	var sincronizacion = ',"sincronizacion":{';
	var calculo = ',"calculo":{';
	var validaciones = ',"validaciones":{';
	var retorno = true;
	
	var inputs= document.getElementsByTagName('input');
	
	datosarr += 'array(';
	
	var name='traduccion';
	datosarr += 'array("nproc"=>"'+name+'","arrp"=>array(';
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox' && inputs[i].name != name && inputs[i].name.substring(0,name.length) == name && inputs[i].id.indexOf('*')!=-1){
			if(x>0)
			{
				traduccion+=',';
				datosarr +=',';
			}
			traduccion+='"'+inputs[i].name+'":[';
			datosarr +='array("nsproc"=>"'+inputs[i].name+'","arrsp"=>array(';
			
			eval('field=document.formulario.'+(inputs[i].name));
			y=0;
			for(var j=0; j<field.length; j++)
			{
				if(field[j].checked == true && field[j].id.indexOf('*')==-1)
				{
					retorno=false;
					if(y>0)
					{	
						traduccion+=',';
						datosarr +=',';
					}
					traduccion+='"'+field[j].id+'"';
					datosarr +='"'+field[j].id+'"';
					y++;
					countproc++;
				}
			}
			traduccion+=']';
			datosarr += '))';
			//if(y>0)datosarr += ',';
			x++;
		}
	}
	traduccion+='}';
	datosarr += ')),';
	
	x=0;
	var name='depuracion';
	datosarr += 'array("nproc"=>"'+name+'","arrp"=>array(';
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox' && inputs[i].name != name && inputs[i].name.substring(0,name.length) == name && inputs[i].id.indexOf('*')!=-1){
			if(x>0)
			{
				depuracion+=',';
				datosarr +=',';
			}

			depuracion+='"'+inputs[i].name+'":[';
			datosarr +='array("nsproc"=>"'+inputs[i].name+'","arrsp"=>array(';
			
			eval('field=document.formulario.'+(inputs[i].name));
			y=0;
			for(var j=0; j<field.length; j++)
			{
				if(field[j].checked == true && field[j].id.indexOf('*')==-1){
					retorno=false;
					if(y>0) 
					{
						depuracion+=',';
						datosarr +=',';
					}
					depuracion+='"'+field[j].id+'"';
					datosarr +='"'+field[j].id+'"';
					y++;
					countproc++;
				}
			}
			
			depuracion+=']';
			datosarr += '))';
			//if(y>0)datosarr += ',';
			x++;
		}
	}
	depuracion+='}';
	datosarr += ')),';
	
	x=0;
	var name='sincronizacion';
	datosarr += 'array("nproc"=>"'+name+'","arrp"=>array(';
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox' && inputs[i].name != name && inputs[i].name.substring(0,name.length) == name && inputs[i].id.indexOf('*')!=-1){
			if(x>0)
			{
				sincronizacion+=',';
				datosarr +=',';
			}
			
			sincronizacion+='"'+inputs[i].name+'":[';
			datosarr +='array("nsproc"=>"'+inputs[i].name+'","arrsp"=>array(';
			
			eval('field=document.formulario.'+(inputs[i].name));
			y=0;
			for(var j=0; j<field.length; j++){
				if(field[j].checked == true && field[j].id.indexOf('*')==-1){
					retorno=false;
					if(y>0) 
					{
						sincronizacion+=',';
						datosarr +=',';
					}
					sincronizacion+='"'+field[j].id+'"';
					datosarr +='"'+field[j].id+'"';
					y++;
					countproc++;
				}
			}
			
			sincronizacion+=']';
			datosarr += '))';
			//if(y>0)datosarr += ',';
			x++;
		}
	}
	sincronizacion+='}';
	datosarr += ')),';
	
	x=0;
	var name='calculo';
	datosarr += 'array("nproc"=>"'+name+'","arrp"=>array(';
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox' && inputs[i].name != name && inputs[i].name.substring(0,name.length) == name && inputs[i].id.indexOf('*')!=-1){
			if(x>0) 
			{
				calculo+=',';
				datosarr +=',';
			}
			
			calculo+='"'+inputs[i].name+'":[';
			datosarr +='array("nsproc"=>"'+inputs[i].name+'","arrsp"=>array(';
			
			eval('field=document.formulario.'+(inputs[i].name));
			y=0;
			for(var j=0; j<field.length; j++){
				if(field[j].checked == true && field[j].id.indexOf('*')==-1){
					retorno=false;
					if(y>0)
					{
						calculo+=',';
						datosarr +=',';
					}
					calculo+='"'+field[j].id+'"';
					datosarr +='"'+field[j].id+'"';
					y++;
					countproc++;
				}
			}
			
			calculo+=']';
			datosarr += '))';
			//if(y>0)datosarr += ',';
			x++;
		}
	}
	calculo+='}';
	datosarr += ')),';
	
	x=0;
	var name='validaciones';
	datosarr += 'array("nproc"=>"'+name+'","arrp"=>array(';
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox' && inputs[i].name != name && inputs[i].name.substring(0,name.length) == name && inputs[i].id.indexOf('*')!=-1){
			if(x>0)
			{
				validaciones+=',';
				datosarr +=',';
			}
			
			validaciones+='"'+inputs[i].name+'":[';
			datosarr +='array("nsproc"=>"'+inputs[i].name+'","arrsp"=>array(';
			
			eval('field=document.formulario.'+(inputs[i].name));
			y=0;
			for(var j=0; j<field.length; j++){
				if(field[j].checked == true && field[j].id.indexOf('*')==-1){
					retorno=false;
					if(y>0)
					{
						validaciones+=',';
						datosarr +=',';
					}
					validaciones+='"'+field[j].id+'"';
					datosarr +='"'+field[j].id+'"';
					y++;
					countproc++;
				}
			}
			
			validaciones+=']';
			datosarr += '))';
			//if(y>0)datosarr += ',';
			x++;
		}
	}
	validaciones+='}';
	datosarr += ')';
	
	
	datos+=traduccion+depuracion+sincronizacion+calculo+validaciones+'}';
	datosarr +=	'))';
//alert(datos);

document.getElementById('json_param').value=datos;
document.getElementById('array_param').value=datosarr;
document.getElementById('countproc').value=countproc;

	return retorno;
}

function ejecutarPHP()
{	
	var bd=document.getElementById("cbd").value;
	
	if(bd==null || bd=="0" || bd==""){alert("Debes seleccionar Base de Datos");return;}
	if(buscarParametros()){alert('Debes seleccionar al menos una opcion');return;}
	
	document.formulario.submit();
	limpiarchecks();
}

function mostrar_datos(bd,mod){
	
	var ajax=nuevoAjax();
	ajax.open("POST", "mostrar.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("bd="+bd+"&mod="+mod);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			document.getElementById(mod).innerHTML="";
			//alert(ajax.responseText);
			eval(ajax.responseText);
			//alert('despues');
			limpiarchecks();
		}
	}
	
}

function validarFecha(Cadena){
	
    var Fecha= new String(Cadena) 
	
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length)) 
  

    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
            alert('Fecha Incorrecta. A�o inv�lido')  
        return false  
    }  

    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        alert('Fecha Incorrecta. Mes inv�lido')  
        return false  
    }  

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        alert('Fecha Incorrecta. D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            alert('Fecha Incorrecta. D�a inv�lido')  
            return false  
        }  
    }  
 
  return true      
}