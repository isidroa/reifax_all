<?php  
	$starttime=tiempo();
/****************************************************/
	$table="diffvalue";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `diffvalue` (
  `ParcelID` varchar(20) NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
  `Bath` double(5,2) default NULL COMMENT 'Cantidad de Baños',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `ClosingDT` varchar(8) default NULL COMMENT 'Fecha de cierre o de desactivado del listado',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida',
  `Pool` varchar(1) default 'N' COMMENT 'Si tiene o no piscina',
  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
  `TSqft` int(12) default '0' COMMENT 'Area total de la propiedad',
  `Value` varchar(1) default NULL,
  `WaterF` varchar(1) default 'N' COMMENT 'Si tiene o no agua colindante con la propiedad',
  `PropType` varchar(3) default NULL,
  `Zip` varchar(5) default NULL,
  KEY `ParcelID` USING BTREE (`ParcelID`)
)";		
	mysql_query($sql) or die(mysql_error()); 
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>