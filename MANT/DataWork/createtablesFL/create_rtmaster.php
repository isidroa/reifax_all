<?php  
	$starttime=tiempo(); 
/****************************************************/
	$table="rtmaster";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `rtmaster` (
  `Parcelid` varchar(20) NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
  `Sparcel` varchar(4) default NULL,
  `Status` varchar(2) NOT NULL default 'SN' COMMENT 'Status',
  `PropType` varchar(3) NOT NULL default 'SN' COMMENT 'Tipo de propiedad según el MLS re1, re2 etc',
  `Address` varchar(24) default NULL COMMENT 'Direccion',
  `Bath` tinyint(2) default '0' COMMENT 'Cantidad de baños',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `City` varchar(23) default NULL COMMENT 'Ciudad',
  `ClosingDT` varchar(8) default NULL COMMENT 'Fecha de cierre o de desactivado del listado',
  `County` varchar(16) default NULL COMMENT 'County',
  `DateO` varchar(8) default NULL COMMENT 'Fecha de listado',
  `DateR` varchar(8) default NULL COMMENT 'Fecha de listado',
  `Dom` mediumint(3) default '0' COMMENT 'Dias en el mercado',
  `Ldate` varchar(8) default NULL COMMENT 'Fecha de listado',
  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
  `Pool` varchar(1) default 'N' COMMENT 'Si tiene o no piscina',
  `RDD` varchar(8) default NULL COMMENT 'Record delete date',
  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
  `Tsqft` int(12) default '0' COMMENT 'Total de sqft',
  `Type` varchar(6) default NULL COMMENT 'Tipo de propiedad',
  `WaterF` varchar(1) default 'N' COMMENT 'Si tiene o no agua colindante con la propiedad',
  `Xd` varchar(8) default NULL COMMENT 'Expiracion date',
  `Yrbuilt` mediumint(4) default '0' COMMENT 'Año de Construccion',
  `Zip` varchar(6) default NULL COMMENT 'Codigo postal',
  PRIMARY KEY  USING BTREE (`Parcelid`),
  KEY `Status` USING BTREE (`Status`),
  KEY `PropType` USING BTREE (`PropType`),
  KEY `Sparcel` (`Sparcel`)
)";
	mysql_query($sql) or die(mysql_error()); 
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>