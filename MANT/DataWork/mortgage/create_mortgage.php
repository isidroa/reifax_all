<?php  
	$starttime=tiempo();
/****************************************************/
	$table="Mortgage";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE ".$table." 
			(
`Parcelid`		varchar(28)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI  DEFAULT null COMMENT '',
`Record`		DOUBLE(15,5) 												default	0 	 COMMENT '',
		
`Block`			varchar(80)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyer1`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyer2`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyer1_st`		varchar(20)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyeradd1`		varchar(40)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyeradd2`		varchar(40)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyeradd3`		varchar(30)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyeradd4`		varchar(30)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyercity`		varchar(30)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyerstate`	varchar(2)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyercount`	varchar(20)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Buyerzip`		varchar(15)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Countyid`		varchar(5)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Doctype`		varchar(16)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Docdesc`		varchar(32)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Grantor`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Grantor_st`	varchar(20)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Instdate`		varchar(10)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Lots`			varchar(80)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Metes`			varchar(80)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_bor1`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_bor2`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_orbk`		varchar(5)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_orpg`		varchar(5)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_recdat`	varchar(10)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_insdat`	varchar(10)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_lender`	varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_len_ad`	varchar(80)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_doc_ty`	varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_doc_dc`	varchar(40)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_rattyp`	varchar(20)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_amount`	double(12,2)												default	0 	 COMMENT '',
`Mtg_term`		varchar(6)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Mtg_intrst`	double(15,5)												default	0 	 COMMENT '',
`Multi`			varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Orbk`			varchar(5)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Orpg`			varchar(5)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Portion`		varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Recdate`		varchar(8)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Saleppsf`		double(12,2)												default	0    COMMENT '',
`Saleprice`		double(12,2)												default	0    COMMENT '',
`Stampprice`	double(12,2)												default	0    COMMENT '',
`Subdiv`		varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Titleco`		varchar(60)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Tr_nf`			varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Unsure`		varchar(50)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Usecode`		varchar(2)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',
`Usedesc`		varchar(32)	CHARACTER SET LATIN1 COLLATE LATIN1_SWEDISH_CI	default	null COMMENT '',

		  		INDEX USING BTREE (`Parcelid`)
			)"; 
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>