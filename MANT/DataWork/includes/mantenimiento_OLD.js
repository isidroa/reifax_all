function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function ocultar_mostrar(){
	if(document.getElementById("tipo_mantenimientod").checked==false){
		document.getElementById("trUSinc").style.display = 'none';
		document.getElementById("tablasTraDiffvalue").style.display = '';
		document.getElementById("tablasTraPendes").style.display = '';
		document.getElementById("tablasTraPsummary").style.display = '';
		document.getElementById("tablasSinSincronizaMarketconPen").style.display = '';
		document.getElementById("tablasSinSincronizaMkvconMkpsPsumPendesY").style.display = '';
		document.getElementById("tablasSinSincronizarImagenesconMlnumber_NEW").style.display = '';
	}else{
		document.getElementById("trUSinc").style.display = '';
		document.getElementById("tablasTraDiffvalue").style.display = 'none';
		document.getElementById("tablasTraPendes").style.display = 'none';
		document.getElementById("tablasTraPsummary").style.display = 'none';
		document.getElementById("tablasSinSincronizaMarketconPen").style.display = 'none';
		document.getElementById("tablasSinSincronizaMkvconMkpsPsumPendesY").style.display = 'none';
		document.getElementById("tablasSinSincronizarImagenesconMlnumber_NEW").style.display = 'none';
	}
}

function checkSeleccion(field, i) 
{
	if (i == 0) //Todos
	{ 
		if (field[0].checked == true) 
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = true;
	   	}
		else
		{
			for (i = 1; i < field.length; i++)
				field[i].checked = false;
		}
	}
	else  // Seleccion de cualquiera
	{  
		if(field[0].id!="ejecTranfSinc")
			field[0].checked = false;

		if(field[0].id=="ejecTranfSinc")
			field[0].checked = true;
	}
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var field,i;
	field =document.formulario.ckbTranfSinc;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.ckbUSinc;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.ckbTranfMantSis;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.ckbTranfSincCalcuBackup;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.ckbBackupS1;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.ckbTranfSincCalcuRestore;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	field =document.formulario.CantRTbl;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
}

function buscarParametros()
{
	var field,i;
	var ret="",datnew="";
	var ruta,texto;
	var cont=0;
	var swbd=0;
	var cad;

	field=document.formulario.ckbTranfSinc;
	if(document.formulario.ckbTranfSinc[0].checked==true)//Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)
	{
		for (i=0; i<field.length;i++)
		{
			cad=field[i].id.substr(0,6);
			switch(cad)
			{
				case "Backup":
					ruta="backup/Dobackup/";
					texto="Hacer Backup de las Tablas";
					break;
				case "Transf":
					ruta="access to csv/";
					texto="Pasar los CSV de access1 a las Tablas";
					break;
				default:	
					ruta="dataverificacion/";
					texto="Ejecutar los php de Sincronizacion de Data";
			}

			if(field[i].checked==true && field[i].id!="ejecTranfSinc")
			{
				cont++;
				if(field[i].id=='BackupMysqlToCsvRental.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"rental/","texto"=>"'+texto+'"^';
				else
					if(field[i].id=="insertaRealtorBackofice.php")
						ret+='"php"=>"'+field[i].id+'","ruta"=>"anexos/","texto"=>"'+texto+'"^';
					else
						ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
			}
		}
		if(cont==(field.length-1)) datnew="datanueva=csvfiles&copypaste=1";
		else datnew="datanueva=csvfiles&copypaste=0";
	}
	else
	{
		datnew="datanueva=&copypaste=0";
		
		field =document.formulario.ckbUSinc;
		ruta="access to csv/";
		texto="Actualizar Data Sincronizada";
		cont=0;
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				cont++;
			}
		}
		if(cont>0)datnew="datanueva=dataserver1&copypaste=0";
		if(cont==(field.length-1))datnew="datanueva=dataserver1&copypaste=0";
		
		field =document.formulario.ckbTranfMantSis;
		ruta="transferMantToSis/";
		texto="Transferir Data de Tabla Mant-->Sis";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
		}
		
		field =document.formulario.ckbTranfSincCalcuBackup;
		ruta="backup/Dobackup/";
		texto="Creando CSV a transferir de Data Mantenimiento-->Sistema";
		cont=0;
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
			{
				if(field[i].id=='BackupMysqlToCsvRental.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"rental/","texto"=>"'+texto+'"^';
				else if(field[i].id=='BackupMysqlToCsvMortgage.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"mortgage/","texto"=>"'+texto+'"^';
				else
					ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				cont++;
			}
		}
		if(cont>0)datnew="datanueva=dataserver1&copypaste=0";
		if(cont==(field.length-1))datnew="datanueva=dataserver1&copypaste=0";
		
		field=document.formulario.ckbBackupS1;
		ruta="backup/Dobackup/";
		texto="Hacer Backup de las Tablas de Data del Sistema";
		cont=0;
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
			{
				if(field[i].id=='BackupMysqlToCsvRental.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"rental/","texto"=>"'+texto+'"^';
				else if(field[i].id=='BackupMysqlToCsvMortgage.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"mortgage/","texto"=>"'+texto+'"^';
				else
					ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				cont++;
			}
		}
		if(cont>0)datnew="datanueva=csvfiles&copypaste=0";
		if(cont==(field.length-1))datnew="datanueva=csvfiles&copypaste=1";
		
		field =document.formulario.ckbTranfSincCalcuRestore;
		ruta="backup/Dorestore/";
		texto="Restore de los CSV de Data Mantenimiento-->Sistema";
		cont=0;
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
			{
				if(field[i].id=='RestoreCsvToMysqlRental.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"rental/","texto"=>"'+texto+'"^';
				else if(field[i].id=='RestoreCsvToMysqlMortgage.php')
					ret+='"php"=>"'+field[i].id+'","ruta"=>"mortgage/","texto"=>"'+texto+'"^';
				else
					ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				cont++;
			}
		}
//		datnew="datanueva=dataserver1&copypaste=0";
		if(cont>0)datnew="datanueva=dataserver1&copypaste=0";
		//if(cont==(field.length-1))datnew="datanueva=dataserver1&copypaste=1";

		field =document.formulario.CantRTbl;
		ruta="cantidad/";
		texto="Cantidad Registros en Tablas";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && field[i].id!="*")
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
	}
	//alert(datnew+"&parametros="+ret);
	if(ret=="") return "";
	else return datnew+"&parametros="+ret;
}
function ValidarBD(bd)
{
	var letra=bd.substr(0,1);
	var i;
//SI BD ES DE MANTENIMINENTO, NINGUNO DE 
//ESTOS PHP DEBE ESTAR SELECCIONADO
	if(letra.toUpperCase()=='M') //MANTENIMIENTO
	{
		field=document.formulario.ckbBackupS1;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 0;
	
		field =document.formulario.ckbTranfSincCalcuRestore;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 0;
	}
	else //SISTEMA
	{
		field=document.formulario.ckbTranfSinc;
		if(document.formulario.ckbTranfSinc[0].checked==true)//Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)
			return 1;

		field =document.formulario.ckbTranfMantSis;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;
		field =document.formulario.ckbTranfSincCalcuBackup;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;
		/*
		field=document.formulario.ckbCreaTbl;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;

		field =document.formulario.ckbBupTbl;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;

		field =document.formulario.ckbResTbl;
		for (i=0; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;
		*/
	}
	return 2;
}

function ejecutarPHP()
{	
	var i;
	var bd=document.getElementById("cbd").value;
	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd=="")
		alert("Debes seleccionar Base de Datos");
	else
	{
		
		parametros=buscarParametros();
		if(parametros==null || parametros=="")
			alert("Debes seleccionar al menos una opcion a Ejecutar");
		else
		{
			ret=ValidarBD(bd);
			if(ret!=2)
			{
				if(ret==0)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Sistema.\n Debes seleccionar Base de Datos de Sistema");
				if(ret==1)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Mantenimiento.\n Debes seleccionar Base de Datos de Mantenimiento");
				return;
			}
			//expandir('filResultados','imgfilResultados');
			document.getElementById("divResultados").innerHTML='';
			document.getElementById("imgfilResultados").src="includes/up.png";
			document.getElementById("imgfilResultados").alt="Ocultar Resultados";
			document.getElementById("reloj").style.visibility="visible";
			if(document.getElementById("guardarAccess1").checked==true){
				var guardarAccess = document.getElementById("guardarAccess1").value;
				var fechaAccess = document.getElementById("fechaAccess1").value;
			}
			else
				var guardarAccess = document.getElementById("guardarAccess2").value;
				
			if(document.getElementById("tipo_mantenimientod").checked==true)
				var tipo_mantenimiento = document.getElementById("tipo_mantenimientod").value;
			else
				var tipo_mantenimiento = document.getElementById("tipo_mantenimientos").value;
				
			controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=true;
			//habilitacion(true);
			//alert("bd="+ bd + "&" + parametros);
			var ajax=nuevoAjax();
			ajax.open("POST", "mantenimiento_respuesta.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("bd="+bd+"&tipo_mantenimiento="+tipo_mantenimiento+"&fechaAccess="+fechaAccess+"&guardarAccess="+guardarAccess+"&"+parametros);
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					htmlTag.push(ajax.responseText);
					document.getElementById("divResultados").innerHTML=htmlTag.join('');
					document.getElementById("filResultados").style.display="inline";
					document.getElementById("reloj").style.visibility="hidden";
					controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=false;
					limpiarchecks();
					//habilitacion(false);
				}
			}
		}//Al Menos un ckechboxes seleccionado
	}//IF Base de Datos
}

function checkSeleccionCSV(field){
	if(field[0].checked==true){
		for(i=0; i<field.length; i++)
			field[i].checked = true;
	}else{
		for(i=0; i<field.length; i++)
			field[i].checked = false;
	}
}

function parametrosCSV(field){
	var parametros='';
	for(i=0; i<field.length; i++){
		if(field[i].checked == true)
			parametros+=field[i].value+'*';
	}
	
	var ajax=nuevoAjax();
	ajax.open("POST", "crearCSVmantenimiento.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("para="+parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			eval(ajax.responseText);
		}
	}
}

function validarFecha(Cadena){
	
    var Fecha= new String(Cadena) 
	
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length)) 
  

    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
            alert('Fecha Incorrecta. A�o inv�lido')  
        return false  
    }  

    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        alert('Fecha Incorrecta. Mes inv�lido')  
        return false  
    }  

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        alert('Fecha Incorrecta. D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            alert('Fecha Incorrecta. D�a inv�lido')  
            return false  
        }  
    }  
 
  return true      
}