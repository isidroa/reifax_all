<?php  
	$signos = array(",",".","=","+","-","%","/");
	if($tipo_mantenimiento==1)
		$rtm='drtmaster';
	else
		$rtm='rtmaster';
	
	
	$starttime=tiempo();
/****************************************************/
	$query="	SELECT DISTINCT sparcel 
				FROM $rtm r 
				group by sparcel 
			";
	$ins=0;
	$result = mysql_query($query) or die(mysql_error());
	while ($row= mysql_fetch_array($result, MYSQL_ASSOC))// QUERY DE SECTIONS QUE ESTAN EN LATLONG
	{
		$sparcel = $row['sparcel'];
		str_replace($signos,"",$sparcel,$aux);
		if($aux==0){
			$nombre = 'rtmtw'.$sparcel;
			$nomMarket = $nombre.'market';
			$nomOffert = $nombre.'offert';
			
			$query="CREATE TABLE `$nombre` (
				  `Parcelid` varchar(20) NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
				  `Sparcel` varchar(4) NOT NULL default '',
				  `Status` varchar(2) NOT NULL default 'SN' COMMENT 'Status',
				  `PropType` varchar(3) NOT NULL default 'SN' COMMENT 'Tipo de propiedad seg�n el MLS re1, re2 etc',
				  `Bath` double(4,1) default '0.0' COMMENT 'Cantidad de ba�os',
				  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
				  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
				  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
				  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
				  `Type` varchar(6) default NULL COMMENT 'Tipo de propiedad',
				  `Yrbuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
				  PRIMARY KEY  USING BTREE (`Parcelid`),
				  KEY `Status` USING BTREE (`Status`),
				  KEY `PropType` USING BTREE (`PropType`),
				  KEY `Sparcel` (`Sparcel`),
				  KEY `Bath` USING BTREE (`Bath`),
				  KEY `Beds` USING BTREE (`Beds`),
				  KEY `Lprice` USING BTREE (`Lprice`),
				  KEY `Lsqft` USING BTREE (`Lsqft`),
				  KEY `SalePrice` USING BTREE (`SalePrice`),
				  KEY `Type` USING BTREE (`Type`),
				  KEY `Yrbuilt` USING BTREE (`Yrbuilt`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
			if(mysql_query($query)){
				echo "<br>CREADA TABLA $nombre";
				unset($query);
			}
			
			$query =	"CREATE TABLE `$nomMarket` (
				  `Parcelid` varchar(20) character set latin1 NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
				  `Sparcel` varchar(4) character set latin1 NOT NULL,
				  `Status` varchar(2) character set latin1 NOT NULL default 'SN' COMMENT 'Status',
				  `PropType` varchar(3) character set latin1 NOT NULL default 'SN' COMMENT 'Tipo de propiedad seg�n el MLS re1, re2 etc',
				  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
				  `ClosingDT` varchar(8) character set latin1 default NULL COMMENT 'Fecha de cierre o de desactivado del listado',
				  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
				  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
				  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
				  `Type` varchar(6) character set latin1 default NULL COMMENT 'Tipo de propiedad',
				  `Yrbuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
				  KEY `Parcelid` USING BTREE (`Parcelid`),
				  KEY `Sparcel` USING BTREE (`Sparcel`),
				  KEY `Status` USING BTREE (`Status`),
				  KEY `PropType` USING BTREE (`PropType`),
				  KEY `Beds` USING BTREE (`Beds`),
				  KEY `Lprice` USING BTREE (`Lprice`),
				  KEY `Lsqft` USING BTREE (`Lsqft`),
				  KEY `SalePrice` USING BTREE (`SalePrice`),
				  KEY `Type` USING BTREE (`Type`),
				  KEY `Yrbuilt` USING BTREE (`Yrbuilt`)
				)";
			if(mysql_query($query)){
				echo "<br>CREADA TABLA `$nomMarket`";
				unset($query);
			}
			
			$query =	"CREATE TABLE `$nomOffert` (
				  `Parcelid` varchar(20) character set latin1 NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
				  `Sparcel` varchar(4) character set latin1 NOT NULL,
				  `Status` varchar(2) character set latin1 NOT NULL default 'SN' COMMENT 'Status',
				  `PropType` varchar(3) character set latin1 NOT NULL default 'SN' COMMENT 'Tipo de propiedad seg�n el MLS re1, re2 etc',
				  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
				  `ClosingDT` varchar(8) character set latin1 default NULL COMMENT 'Fecha de cierre o de desactivado del listado',
				  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
				  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
				  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
				  `Type` varchar(6) character set latin1 default NULL COMMENT 'Tipo de propiedad',
				  `Yrbuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
				  KEY `Parcelid` USING BTREE (`Parcelid`),
				  KEY `Sparcel` USING BTREE (`Sparcel`),
				  KEY `Status` USING BTREE (`Status`),
				  KEY `PropType` USING BTREE (`PropType`),
				  KEY `Beds` USING BTREE (`Beds`),
				  KEY `Lprice` USING BTREE (`Lprice`),
				  KEY `Lsqft` USING BTREE (`Lsqft`),
				  KEY `SalePrice` USING BTREE (`SalePrice`),
				  KEY `Type` USING BTREE (`Type`),
				  KEY `Yrbuilt` USING BTREE (`Yrbuilt`)
				)";
			if(mysql_query($query)){
				echo "<br>CREADA TABLA `$nomOffert`";
				unset($query);
			}
			
			if($tipo_mantenimiento!=1)
			{
				mysql_query("TRUNCATE TABLE `$nombre`") or die (mysql_error());
				mysql_query("TRUNCATE TABLE `$nomMarket`") or die (mysql_error());
				mysql_query("TRUNCATE TABLE `$nomOffert`") or die (mysql_error());
			}
//			else { borrar de los RTMTW... lo que esta en drtmaster	}
			
			$query = "	INSERT INTO `$nombre` 
						SELECT `parcelid`,`sparcel`,`status`,`proptype`, 
							`bath`,`beds`,`lprice`,`lsqft`,`saleprice`,
							`type`,`yrbuilt` 
						FROM $rtm 
						WHERE sparcel='$sparcel' AND status='A'";
			mysql_query($query) or die(mysql_error());
			$num = mysql_affected_rows();
			
			$query = "	INSERT INTO `$nomMarket` 
						SELECT `parcelid`,`sparcel`,`status`,`proptype`, 
							`beds`,`closingdt`,`lprice`,`lsqft`,`saleprice`,
							`type`,`yrbuilt` 
						FROM $rtm 
						WHERE sparcel='$sparcel' AND (status='CC' OR status='CS') AND length(ClosingDT)=8 and date(ClosingDT)>=DATE_SUB(curdate(), interval 366 day)";
			mysql_query($query) or die(mysql_error());
			$num1 = mysql_affected_rows();
			
			$query = "	INSERT INTO `$nomOffert` 
						SELECT `parcelid`,`sparcel`,`status`,`proptype`, 
							`beds`,`closingdt`,`lprice`,`lsqft`,`saleprice`,
							`type`,`yrbuilt` 
						FROM $rtm 
						WHERE sparcel='$sparcel' AND status='A'";
			mysql_query($query) or die(mysql_error());
			$num2 = mysql_affected_rows();
			
			echo "<br>INSERTADOS EN $nombre: $num <br>";
			echo "<br>INSERTADOS EN $nomMarket: $num1 <br>";
			echo "<br>INSERTADOS EN $nomOffert: $num2 <br>";
			$ins++;
		}
	}	

/****************************************************/
	$endtime=(tiempo()- $starttime);
	echo "<br><br>TOTAL TIEMPO DE EJECUCION DEL PHP: ".$endtime." SEGUNDOS";
?> 
