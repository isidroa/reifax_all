<?php 
	if($tipo_mantenimiento==1){
		$mls='mlsresidentialmlx';
		$rtm='drtmaster';
	}else{
		$mls='mlsresidential';
		$rtm='rtmaster';
	}
	
	$starttime=tiempo();
	echo "Base de Datos: ".strtoupper($nom_bd)."<br>";	
/****************************************************/

	$county = $nom_bd[0];
	if($county=='M' || $county=='m') $county=substr($nom_bd,3);
	else $county=substr($nom_bd,2);

/*****************************************************/

$impecho=100;

//SINGLE FAMILY
	if($tipo_mantenimiento==1){
		//BUSCAR LOS LATLONG POR SF PARA PSUMMARY Y EDITARLOS
		$query="SELECT	pc.parcelid, pc.latitude, pc.longitude 
				FROM	psummary psum, parcels_centroids pc, latlong ll 
				WHERE	psum.parcelid=pc.parcelid 
						and ll.parcelid=pc.parcelid 
						and psum.ccode='01'
						and ll.centroids<>'Y' 
				";
		$result = mysql_query($query) or die(mysql_error());
		$num_filas=mysql_num_rows($result);
		$i=0;
		while ($row= mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$query="UPDATE latlong 
					SET latitude=".$row['latitude'].", 
						longitude=".$row['longitude'].", 
						centroids='Y' 
					WHERE parcelid='".$row['parcelid']."'";
			mysql_query($query) or die(mysql_error());	
			$i++;
			if(($i%$impecho)==0) echo "<br>".$i." ".$query;		
		}
		echo "<br> LATLONG SF PARA PSUMMARY: ".$i." REGISTROS";
		$endtime=tiempo()-$starttime;
		echo "<br>TIEMPO: ".$endtime." SEGUNDOS<br>";
		
		ingreso(67,$county,$endtime,$i);//ActionID 67
		
		unset($row);
	}
////----------------------------------------------------------------------------------------------------------------------------
	//BUSCAR LOS LATLONG POR SF PARA MLSRESIDENTIAL Y EDITARLOS
	$query="SELECT	pc.parcelid, pc.latitude, pc.longitude 
			FROM	$mls mls, parcels_centroids pc, latlong ll 
			WHERE	mls.parcelid=pc.parcelid 
					and ll.parcelid=pc.parcelid 
					and mls.proptype='RE1'
					and ll.centroids<>'Y' 
			";

	$result = mysql_query($query) or die(mysql_error());
	$num_filas=mysql_num_rows($result);
	$i=0;
	while ($row= mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$query="UPDATE latlong 
				SET latitude=".$row['latitude'].", 
					longitude=".$row['longitude'].", 
					centroids='Y' 
				WHERE parcelid='".$row['parcelid']."'";
		mysql_query($query) or die(mysql_error());		
		$i++;
		if(($i%$impecho)==0) echo "<br>".$i." ".$query;		
	}
	echo "<br> LATLONG SF PARA MLSRESIDENTIAL: ".$i." REGISTROS";
	$endtime=tiempo()-$starttime;
	echo "<br>TIEMPO: ".$endtime." SEGUNDOS<br>";
	
	ingreso(68,$county,$endtime,$i);//ActionID 68
	
	unset($row);
////----------------------------------------------------------------------------------------------------------------------------

//CONDOMINIOS
	if($tipo_mantenimiento==1){
		//BUSCAR LOS LATLONG POR CONDOS PARA PSUMMARY Y EDITARLOS
		$query="SELECT l.parcelid 
				FROM latlong l,psummary p
				WHERE p.parcelid=l.parcelid
				and p.ccode='04'
				and l.centroids<>'Y' ";
		$result = mysql_query($query) or die(mysql_error());
		$num_filas=mysql_num_rows($result);
		$i=0;
		$contpid=0;
		$contspar=0;
		$contgoogle=0;
		$contnoesta=0;
		while ($row= mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$pid=$row['parcelid'];
			$que="	SELECT parcelid,latitude,longitude 
					FROM parcels_centroids 
					WHERE parcelid='".$pid."'
					LIMIT 1";
			$res1=mysql_query($que) or die(mysql_error());  
			$row1= mysql_fetch_array($res1, MYSQL_ASSOC);   
	
			if(mysql_num_rows($res1)>0)//si esta --> select centroids pid1=pid----> mete los lact y long completo parcelids centroids='Y'
			{
				$query="UPDATE latlong 
						SET latitude=".$row1['latitude'].", 
							longitude=".$row1['longitude'].", 
							centroids='Y' 
						WHERE parcelid='".$pid."'";
				mysql_query($query) or die(mysql_error());	 	
				$contpid++;	
				mysql_free_result($res1);
			}
			else//sino --> select centroids sparcel=sunstring(pid,9) limti 1			si esta lo modifico
			{
				mysql_free_result($res1);
				$que="	SELECT parcelid,latitude,longitude 
						FROM parcels_centroids 
						WHERE sparcel='".substr($pid,0,$substr)."'
						LIMIT 1";
				$res2=mysql_query($que) or die(mysql_error());  
				$row2= mysql_fetch_array($res2, MYSQL_ASSOC);
		
				if(mysql_num_rows($res2)>0)
				{
					$query="UPDATE latlong 
							SET latitude=".$row2['latitude'].", 
								longitude=".$row2['longitude'].", 
								centroids='Y' 
							WHERE parcelid='".$pid."'";
					mysql_query($query) or die(mysql_error());	 	
					$contspar++;
					mysql_free_result($res2);
				}
				//Forma de GoogleMaps.
				else{$contnoesta++;				
				}	
				
			}
			$i++;
		}
		$endtime = tiempo()-$starttime;
		echo "<br>PSUMMARY";
		echo "<br>LATLONG Centroids<>'Y': ".$i." REGISTROS";
		ingreso(69,$county,0,$i);//ActionID 69
		echo "<br>LATLONG SI ESTA EN PC.PARCELID=LL.PARCELID: ".$contpid." REGISTROS";
		ingreso(70,$county,0,$contpid);//ActionID 70
		echo "<br>LATLONG SI ESTA EN PC.SPARCEL=SUBSTR(LL.PARCELID,$substr): ".$contspar." REGISTROS";
		ingreso(71,$county,0,$contspar);//ActionID 71
		echo "<br>LATLONG SI ESTA EN GOOGLEMAPS: ".$contgoogle." REGISTROS";
		ingreso(72,$county,0,$contgogle);//ActionID 72
		echo "<br>LATLONG NO CONSIGUIO EN CENTROIDS: ".$contnoesta." REGISTROS";
		ingreso(73,$county,$endtime,$contnoesta);//ActionID 73
		echo "<br>TIEMPO: ".$endtime." SEGUNDOS";
		
		unset($row,$row1,$row2);
	}

	//BUSCAR LOS LATLONG POR CONDOS PARA MLSRESIDENTIAL Y EDITARLOS
	$query="SELECT l.parcelid 
			FROM latlong l,$mls p
			WHERE p.parcelid=l.parcelid
			and p.proptype='RE2'
			and l.centroids<>'Y' ";
	$result = mysql_query($query) or die(mysql_error());
	$num_filas=mysql_num_rows($result);
	$i=0;
	$contpid=0;
	$contspar=0;
	$contgoogle=0;
	$contnoesta=0;
	while ($row= mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$pid=$row['parcelid'];
		$que="	SELECT parcelid,latitude,longitude 
				FROM parcels_centroids 
				WHERE parcelid='".$pid."'
				LIMIT 1";
		$res1=mysql_query($que) or die(mysql_error());  
		$row1= mysql_fetch_array($res1, MYSQL_ASSOC);   

		if(mysql_num_rows($res1)>0)//si esta --> select centroids pid1=pid----> mete los lact y long completo parcelids centroids='Y'
		{
			$query="UPDATE latlong 
					SET latitude=".$row1['latitude'].", 
						longitude=".$row1['longitude'].", 
						centroids='Y' 
					WHERE parcelid='".$pid."'";
			mysql_query($query) or die(mysql_error());	 	
			$contpid++;	
			mysql_free_result($res1);
		}
		else//sino --> select centroids sparcel=sunstring(pid,9) limti 1			si esta lo modifico
		{
			mysql_free_result($res1);
			$que="	SELECT parcelid,latitude,longitude 
					FROM parcels_centroids 
					WHERE sparcel='".substr($pid,0,$substr)."'
					LIMIT 1";
			$res2=mysql_query($que) or die(mysql_error());  
			$row2= mysql_fetch_array($res2, MYSQL_ASSOC);
	
			if(mysql_num_rows($res2)>0)
			{
				$query="UPDATE latlong 
						SET latitude=".$row2['latitude'].", 
							longitude=".$row2['longitude'].", 
							centroids='Y' 
						WHERE parcelid='".$pid."'";
				mysql_query($query) or die(mysql_error());			
				$contspar++;
				mysql_free_result($res2);
			}
			else{$contnoesta++;
			}
		}
		$i++;
	}
	$endtime=tiempo()-$starttime;
	echo "<br>LATLONG Centroids<>'Y': ".$i." REGISTROS";
	ingreso(74,$county,0,$i);//ActionID 74
	echo "<br>LATLONG SI ESTA EN PC.PARCELID=LL.PARCELID: ".$contpid." REGISTROS";
	ingreso(75,$county,0,$contpid);//ActionID 75
	echo "<br>LATLONG SI ESTA EN PC.SPARCEL=SUBSTR(LL.PARCELID,$substr): ".$contspar." REGISTROS";
	ingreso(76,$county,0,$contspar);//ActionID 76
	echo "<br>LATLONG SI ESTA EN GOOGLEMAPS: ".$contgoogle." REGISTROS";
	ingreso(77,$county,0,$contgoogle);//ActionID 77
	echo "<br>LATLONG NO CONSIGUIO EN CENTROIDS: ".$contnoesta." REGISTROS";
	ingreso(78,$county,$endtime,$contnoesta);//ActionID 78
	echo "<br>TOTAL TIEMPO DE EJECUCION DEL PHP: ".$endtime." SEGUNDOS";

?> 