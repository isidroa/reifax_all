<?php  
	$starttime=tiempo();
/****************************************************/
	$table="mortgage";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `mortgage` (
  `PARCELID` varchar(28) collate latin1_general_ci default NULL,
  `RECORD` double(15,5) default NULL,
  `BLOCK` varchar(80) collate latin1_general_ci default NULL,
  `BUYER1` varchar(60) collate latin1_general_ci default NULL,
  `BUYER2` varchar(60) collate latin1_general_ci default NULL,
  `BUYER1_ST` varchar(20) collate latin1_general_ci default NULL,
  `BUYERADD1` varchar(40) collate latin1_general_ci default NULL,
  `BUYERADD2` varchar(40) collate latin1_general_ci default NULL,
  `BUYERADD3` varchar(30) collate latin1_general_ci default NULL,
  `BUYERADD4` varchar(30) collate latin1_general_ci default NULL,
  `BUYERCITY` varchar(30) collate latin1_general_ci default NULL,
  `BUYERSTATE` varchar(2) collate latin1_general_ci default NULL,
  `BUYERCOUNT` varchar(20) collate latin1_general_ci default NULL,
  `BUYERZIP` varchar(15) collate latin1_general_ci default NULL,
  `COUNTYID` varchar(5) collate latin1_general_ci default NULL,
  `DOCTYPE` varchar(16) collate latin1_general_ci default NULL,
  `DOCDESC` varchar(32) collate latin1_general_ci default NULL,
  `GRANTOR` varchar(60) collate latin1_general_ci default NULL,
  `GRANTOR_ST` varchar(20) collate latin1_general_ci default NULL,
  `INSTDATE` varchar(10) collate latin1_general_ci default NULL,
  `LOTS` varchar(80) collate latin1_general_ci default NULL,
  `METES` varchar(80) collate latin1_general_ci default NULL,
  `MTG_BOR1` varchar(60) collate latin1_general_ci default NULL,
  `MTG_BOR2` varchar(60) collate latin1_general_ci default NULL,
  `MTG_ORBK` varchar(5) collate latin1_general_ci default NULL,
  `MTG_ORPG` varchar(5) collate latin1_general_ci default NULL,
  `MTG_RECDAT` varchar(10) collate latin1_general_ci default NULL,
  `MTG_INSDAT` varchar(10) collate latin1_general_ci default NULL,
  `MTG_LENDER` varchar(60) collate latin1_general_ci default NULL,
  `MTG_LEN_AD` varchar(80) collate latin1_general_ci default NULL,
  `MTG_DOC_TY` varchar(50) collate latin1_general_ci default NULL,
  `MTG_DOC_DC` varchar(40) collate latin1_general_ci default NULL,
  `MTG_RATTYP` varchar(20) collate latin1_general_ci default NULL,
  `MTG_AMOUNT` double(15,5) default NULL,
  `MTG_TERM` varchar(6) collate latin1_general_ci default NULL,
  `MTG_INTRST` double(15,5) default NULL,
  `MULTI` varchar(50) collate latin1_general_ci default NULL,
  `ORBK` varchar(5) collate latin1_general_ci default NULL,
  `ORPG` varchar(5) collate latin1_general_ci default NULL,
  `PORTION` varchar(50) collate latin1_general_ci default NULL,
  `RECDATE` varchar(10) collate latin1_general_ci default NULL,
  `SALEPRICE` double(15,5) default NULL,
  `SALEPPSF` double(15,5) default NULL,
  `STAMPPRICE` double(15,5) default NULL,
  `SUBDIV` varchar(50) collate latin1_general_ci default NULL,
  `TITLECO` varchar(60) collate latin1_general_ci default NULL,
  `TR_NF` varchar(50) collate latin1_general_ci default NULL,
  `USECODE` varchar(2) collate latin1_general_ci default NULL,
  `USEDESC` varchar(32) collate latin1_general_ci default NULL,
  `UNSURE` varchar(50) collate latin1_general_ci default NULL,
  KEY `PARCELID` (`PARCELID`)
)"; 
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>