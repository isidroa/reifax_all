<?php  
	$starttime=tiempo();
/****************************************************/
	$table="Marketvalue";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `marketvalue` (
  `Parcelid` varchar(20) character set latin1 NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
  `Sparcel` varchar(4) character set latin1 NOT NULL default '',
  `MarketMedia` double(12,2) default '0.00' COMMENT 'media del mercado',
  `MarketPctg` double(12,2) default '0.00' COMMENT 'Porcentaje de mercado',
  `MarketValue` double(12,2) default '0.00' COMMENT 'Valor de mercado',
  `OffertMedia` double(12,2) default '0.00' COMMENT 'Media de la oferta',
  `OffertPctg` double(12,2) default '0.00' COMMENT 'Porcentaje de oferta',
  `OffertValue` double(12,2) default '0.00' COMMENT 'Valor de Oferta',
  `Pendes` varchar(1) character set latin1 default 'N',
  `Lien` varchar(1) character set latin1 default 'N',
  `DebtTV` double(12,2) default '0.00' COMMENT 'Porcentaje de deuda vs marketvalue',
  `OceanFront` varchar(1) character set latin1 default 'N' COMMENT 'Indica si es Ocean Front o no.',
  `Notes` mediumtext character set latin1,
  `FILE_DATE` varchar(10) character set latin1 default NULL COMMENT 'Filing Date [yyyy-mm-dd]08',
  `JUDGEDATE` varchar(10) character set latin1 default NULL COMMENT 'Scheduled Auction [yyyy-mm-dd] ',
  `controlFecha` date default NULL,
  PRIMARY KEY  (`Parcelid`),
  KEY `Parcelid` USING BTREE (`Parcelid`),
  KEY `Sparcel` USING BTREE (`Sparcel`),
  KEY `Pendes` USING BTREE (`Pendes`)
)"; 
	mysql_query($sql) or die(mysql_error());
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>