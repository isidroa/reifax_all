<?php  
	$starttime=tiempo(); 
/****************************************************/
	$table="rtmaster3";
	mysql_query("DROP TABLE IF EXISTS ".$table) or die(mysql_error());

	$sql =	"CREATE TABLE `rtmaster3` (
  `Parcelid` varchar(20) character set latin1 NOT NULL default '0' COMMENT 'Codificacion del condado de la propiedad',
  `Sparcel` varchar(4) character set latin1 NOT NULL,
  `Status` varchar(2) character set latin1 NOT NULL default 'SN' COMMENT 'Status',
  `PropType` varchar(3) character set latin1 NOT NULL default 'SN' COMMENT 'Tipo de propiedad seg�n el MLS re1, re2 etc',
  `Beds` tinyint(2) default '0' COMMENT 'Cantidad de cuartos',
  `ClosingDT` varchar(8) character set latin1 default NULL COMMENT 'Fecha de cierre o de desactivado del listado',
  `Lprice` double(12,2) default '0.00' COMMENT 'Precio de Lista',
  `Lsqft` mediumint(9) default '0' COMMENT 'Area construida en sqft',
  `SalePrice` double(12,2) default '0.00' COMMENT 'Precio de venta',
  `Type` varchar(6) character set latin1 default NULL COMMENT 'Tipo de propiedad',
  `Yrbuilt` mediumint(4) default '0' COMMENT 'A�o de Construccion',
  KEY `Parcelid` USING BTREE (`Parcelid`),
  KEY `Sparcel` USING BTREE (`Sparcel`),
  KEY `Status` USING BTREE (`Status`),
  KEY `PropType` USING BTREE (`PropType`),
  KEY `Beds` USING BTREE (`Beds`),
  KEY `Lprice` USING BTREE (`Lprice`),
  KEY `Lsqft` USING BTREE (`Lsqft`),
  KEY `SalePrice` USING BTREE (`SalePrice`),
  KEY `Type` USING BTREE (`Type`),
  KEY `Yrbuilt` USING BTREE (`Yrbuilt`)
)";
	mysql_query($sql) or die(mysql_error()); 
	echo "<br>TABLA: ".strtoupper($table);
/****************************************************/
	$endtime=tiempo();
	echo "<br>TIEMPO DE EJECUCION DEL PHP: ".($endtime - $starttime)." SEGUNDOS";

?>