<html>
<head>
<title>Mantenimiento de Tablas</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO SEMANAL</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="450" >TAREAS</th>
			<th width="220" >BD:<?php
                                include('includes/conexion.php');
								$conn=conectar('xima'); 
								$q='select DISTINCT state,county,bd from xima.lscounty order by county';
								$res=mysql_query($q) or die($q.mysql_error());
								$cad1='';$cad2='';
								while($r=mysql_fetch_array($res))
								{
									$cad1.='<option value="'.strtolower($r['bd']).'">'.strtoupper($r['bd']).' 1</option>';
									$cad2.='<option value="'.strtolower($r['bd']).'1">'.strtoupper($r['bd']).' 2</option>';
								}
								?>
				<select name="cbd" id="cbd" style=" width:150px;" >
					<option value="0" selected>Seleccione</option>
                    <optgroup title="FL" label="FL 1">
								<?php echo $cad1 ?>
<!--
					<option value="FLBROWARD" >FLBROWARD 1</option>
					<option value="FLDADE"    >FLDADE 1</option>
					<option value="FLPALMBEACH">FLPALMBEACH 1</option>
					<option value="FLOSCEOLA">FLOSCEOLA 1</option>
					<option value="FLLAKE">FLLAKE 1</option>
					<option value="FLORANGE">FLORANGE 1</option>
					<option value="FLPASCO">FLPASCO 1</option>
					<option value="FLPOLK">FLPOLK 1</option>
					<option value="FLSEMINOLE">FLSEMINOLE 1</option>
					<option value="FLVOLUSIA">FLVOLUSIA 1</option>
					<option value="FLHILLSBOROUGH">FLHILLSBOROUGH 1</option>
-->
                    </optgroup>
                    <optgroup title="FL" label="FL 2">
								<?php echo $cad2 ?>
<!--
					<option value="FLBROWARD1" >FLBROWARD 2</option>
					<option value="FLDADE1"    >FLDADE 2</option>
					<option value="FLPALMBEACH1">FLPALMBEACH 2</option>
					<option value="FLOSCEOLA1">FLOSCEOLA 2</option>
					<option value="FLLAKE1">FLLAKE 2</option>
					<option value="FLORANGE1">FLORANGE 2</option>
					<option value="FLPOLK1">FLPOLK 2</option>
					<option value="FLPASCO1">FLPASCO 2</option>
					<option value="FLSEMINOLE1">FLSEMINOLE 2</option>
					<option value="FLVOLUSIA1">FLVOLUSIA 2</option>
					<option value="FLHILLSBOROUGH1">FLHILLSBOROUGH 2</option>
-->
                    </optgroup>
				</select> 

                <br> SAVE: <input type="radio" name="guardarAccess" id="guardarAccess1" value=1>SI<input name="guardarAccess" type="radio" id="guardarAccess2" value=0 checked>NO
                <br> FECHA: <input name="fechaAccess" type="text" id="fechaAccess1" value="AAAA-MM-DD" size="12" maxlength="10" onFocus="javascript: this.select();" onBlur="javascript: if(document.getElementById('guardarAccess1').checked==true) validarFecha(this.value);">
                <br> <input type="radio" name="tipo_mantenimiento" id="tipo_mantenimientod" onClick="ocultar_mostrar();" value=1>Diario<input name="tipo_mantenimiento" type="radio" id="tipo_mantenimientos" value=0 onClick="ocultar_mostrar();" checked>Semanal
			</th>
		</tr>

<!--Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titTransSinc','imgTransSinc'); return false;">
    	<img id="imgTransSinc" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2" >Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA)</th>
	<th width="190" ></th>
</tr>
  <tr  id="titTransSinc" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">

		<!--Pasar los CSV de access1 a las Tablas (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincAcc1Msq','imgTransSincAcc1Msq'); return false;">
				<img id="imgTransSincAcc1Msq" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2">Transferir la data a las Tablas</th>
			<th width="180"><input type="checkbox" name="ckbTransData"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTransData, 0);"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincAcc1Msq" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titTransData=array(
										array("tit"=>"Diffvalue","value"=>"TransferAccess1ToCsvDiffvalue.php"),
										array("tit"=>"Mlsresidential","value"=>"TransferAccess1ToCsvMlsResidential.php"),
										array("tit"=>"Pendes","value"=>"TransferAccess1ToCsvPendes.php"),
										array("tit"=>"Psummary","value"=>"TransferAccess1ToCsvPsummary.php"),
										array("tit"=>"Rtmaster","value"=>"TransferAccess1ToCsvRtmaster.php"),
										array("tit"=>"LatlongMLX","value"=>"TransferAccess1ToCsvLatlongmlx.php")
							);
				for($i=0;$i<count($titTransData);$i++)
				{
			?>	 
					<tr id="tablasT<?php echo($titTransData[$i]["tit"]);?>">
						<td width="15"></td>
						<td width="350" ><?php echo $titTransData[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTransData" id="<?php echo $titTransData[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbTransData, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Ejecutar los php de Sincronización de Data (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincSincroniza','imgTransSincSincroniza'); return false;">
				<img id="imgTransSincSincroniza" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2">Ejecutar los php de Sincronización de Data</th>
			<th width="180"><input type="checkbox" name="ckbSincData"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbSincData, 0);"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincSincroniza" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titSincData=array(
											 array("tit"=>"SincronizaLatlongconMlsPsum","value"=>"sincronizaLatlongconMlsPsum.php")
											,array("tit"=>"SincronizaMarketconMls","value"=>"sincronizaMarketconMls.php")
											,array("tit"=>"SincronizaMarketconPen","value"=>"sincronizaMarketconPen.php")
											,array("tit"=>"SincronizaMkvconMkpsPsumPendesY","value"=>"sincronizaMkvconMkpsPsumPendesY.php")
											,array("tit"=>"ActualizaWtfPoolPenMlsMkv","value"=>"actualizaWtfPoolPenMlsMkv.php")
											,array("tit"=>"ActualizaRtmDateoConMlsEntrydate","value"=>"actualizaRtmDateoConMlsEntrydate.php")
											,array("tit"=>"Fillsparcel","value"=>"fillsparcel.php")
											,array("tit"=>"ActualizaRemarkMlsresidential","value"=>"actualizaRemarkMlsresidential.php")	
											,array("tit"=>"ActualizaPsummaryAstatusMlsresidential","value"=>"ActualizaPsummaryAstatusMlsresidential.php")
											,array("tit"=>"sincronizarImagenesconMlnumber","value"=>"sincronizarImagenesconMlnumber.php")
											,array("tit"=>"sincronizarLatlongconLatlongCounty","value"=>"sincronizarLatlongconLatlongCounty.php")
											,array("tit"=>"CreacionSectLlave","value"=>"creacionSectllave.php")
											,array("tit"=>"FillSparcelLatLongMLS","value"=>"fillsparcelLatlong.php")
											,array("tit"=>"FillSparcelMLS","value"=>"fillsparcelMLS.php")
											,array("tit"=>"CreacionRTMTW","value"=>"creacionRTMTW.php")
							);
				for($i=0;$i<count($titSincData);$i++)
				{
			?>	 
					<tr  id="tablasSin<?php echo $titSincData[$i]["tit"] ?>" >
						<td width="15"></td>
						<td width="350" ><?php echo $titSincData[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbSincData" id="<?php echo $titSincData[$i]["value"] ?>" value="<?php echo ($i+1) ?>"  onClick="checkSeleccion(document.formulario.ckbSincData, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>
	</table>	
  </td>
  </tr>

<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->

  <tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titCalculolink','imgCalculolink'); return false;">
    	<img id="imgCalculolink" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">CALCULOS</th>
	<th width="190" ></th>
</tr>
  <tr  id="titCalculolink" style="display: none">
  <script>
  	function calc_href(){
		var fecha = document.getElementById('fechaAccess1').value;
		var bd = document.getElementById('cbd').selectedIndex;
		//alert(bd);
		document.getElementById('hrefcalculos').href='calculos/calculoMarketValue.php?bd='+bd+'&fecha='+fecha;
	}
  </script>
  <td colspan="4">
	 <a id="hrefcalculos" href="" onClick="calc_href();" target="_blank"><br>Ir a pagina de Calculos</a><br><br>
  </td>
  </tr>

<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->

<!--Transferir data Sincronizada y Calculada a Data del Sistema (DATA NUEVA)-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titTransSincCalcu','imgTransSincCalcu'); return false;">
    	<img id="imgTransSincCalcu" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Transferir data Sincronizada y Calculada Mant-Sist (DATA NUEVA)</th>
	<th width="190" ></th>
</tr>
  <tr  id="titTransSincCalcu" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">
		
        <!--Actualizando data sincronizada a tablas grandes solo en diario-->
		<tr class="titulomedio2" id="trUSinc" style="display:none;" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titUSinc','imgUSinc'); return false;">
				<img id="imgUSinc" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Actualizar Data Sincronizada <span id="new">(Solo en Diario)</span></th>
			<th width="180"><input type="checkbox" name="ckbUSinc"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbUSinc, 0);"> 
		      Todos</th>
		</tr>
   	    <tr  id="titUSinc" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titUSinc=array(
								array("tit"=>"Mlsresidential","value"=>"updateMlsresidential.php")
								,array("tit"=>"Rtmaster","value"=>"updateRtmaster.php")
							);
				for($i=0;$i<count($titUSinc);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titUSinc[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbUSinc" id="<?php echo $titUSinc[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbUSinc, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>
        
		<!--Creando CSV a transferir al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransMantSis','imgTransMantSis'); return false;">
				<img id="imgTransMantSis" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Transferir Data de Tabla Mant-->Sis</th>
			<th width="180"><input type="checkbox" name="ckbTranfMantSis"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfMantSis, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransMantSis" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titTransMantSis=array(
								array("tit"=>"Latlong","value"=>"TransferMantToSisLatlong.php")
								,array("tit"=>"Marketvalue","value"=>"TransferMantToSisMarketValue.php")
								,array("tit"=>"MarketPs","value"=>"TransferMantToSisMarketPs.php")
							);
				for($i=0;$i<count($titTransMantSis);$i++)
				{
			?>	 
					<tr >
						<td width="15"></td>
						<td width="350" ><?php echo $titTransMantSis[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfMantSis" id="<?php echo $titTransMantSis[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfMantSis, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>

		<!--Creando CSV a transferir al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincBackupCalcu','imgTransSincBackupCalcu'); return false;">
				<img id="imgTransSincBackupCalcu" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Creando CSV de Data Sistema</th>
			<th width="180"><input type="checkbox" name="ckbTranfSincCalcuBackup"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuBackup, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincBackupCalcu" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titTransSincBackupCalcu=array(
								array("tit"=>"Diffvalue","value"=>"BackupMysqlToCsvDiffvalue.php")
								,array("tit"=>"Imágenes","value"=>"BackupMysqlToCsvImagenes.php")
								,array("tit"=>"Latlong","value"=>"BackupMysqlToCsvLatlong.php")
								,array("tit"=>"Marketvalue","value"=>"BackupMysqlToCsvMarketvalue.php")
								,array("tit"=>"MarketPs","value"=>"BackupMysqlToCsvMarketPs.php")
								,array("tit"=>"Mlsresidential","value"=>"BackupMysqlToCsvMlsresidential.php")
								,array("tit"=>"Pendes","value"=>"BackupMysqlToCsvPendes.php")
								,array("tit"=>"Psummary","value"=>"BackupMysqlToCsvPsummary.php")
								,array("tit"=>"Rental","value"=>"BackupMysqlToCsvRental.php")
								,array("tit"=>"Rtmaster","value"=>"BackupMysqlToCsvRtmaster.php")
							);
//								,array("tit"=>"Realtors","value"=>"BackupMysqlToCsvRealtors.php")
				for($i=0;$i<count($titTransSincBackupCalcu);$i++)
				{
			?>	 
					<tr id="tablasCSV<?php echo $titTransSincBackupCalcu[$i]["tit"] ?>">
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincBackupCalcu[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSincCalcuBackup" id="<?php echo $titTransSincBackupCalcu[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuBackup, <?php echo ($i+1) ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>

	</table>	
  </td>
  </tr>

<!-- *********************************************************************************************************************-->
<!--Transferir data Sincronizada y Calculada a Data del Sistema (DATA NUEVA)-->		
<tr class="titulomedio" >
	<th width="15">
		<a href="#" onClick="javascript:expandir('titBRFL','imgBRFL'); return false;">
    	<img id="imgBRFL" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">BackUp - Restore (BASE DE DATOS FL)</th>
	<th width="190" ></th>
</tr>
  <tr  id="titBRFL" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesDatNew" align="center">
		
       

		<!--Restore de los CSV transferidos al server1 (DATA NUEVA)-->
		<tr class="titulomedio2" >
			<th width="15">
				<a href="#" onClick="javascript:expandir('titTransSincRestoreCalcu','imgTransSincRestoreCalcu'); return false;">
				<img id="imgTransSincRestoreCalcu" src="includes/down.png" border="0" ></a>
			</th>
			<th colspan="2" width="455">Restore de los CSV de Data Mantenimiento-->Sistema </th>
			<th width="180"><input type="checkbox" name="ckbTranfSincCalcuRestore"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuRestore, 0)"> 
		      Todos</th>
		</tr>
   	    <tr  id="titTransSincRestoreCalcu" style="display: none">
		<td colspan="4">
				 <table class="tblopcionesfilas" align="center">
			<?php 
				$titTransSincRestoreCalcu=array(
											array("tit"=>"Diffvalue","value"=>"RestoreCsvToMysqlDiffvalue.php")
											,array("tit"=>"Imágenes","value"=>"RestoreCsvToMysqlImagenes.php")
											,array("tit"=>"Latlong","value"=>"RestoreCsvToMysqlLatlong.php")
											,array("tit"=>"Marketvalue","value"=>"RestoreCsvToMysqlMarketvalue.php")
											,array("tit"=>"Mlsresidential","value"=>"RestoreCsvToMysqlMlsresidential.php")
											,array("tit"=>"Mortgage","value"=>"RestoreCsvToMysqlMortgage.php")
											,array("tit"=>"Pendes","value"=>"RestoreCsvToMysqlPendes.php")
											,array("tit"=>"Psummary","value"=>"RestoreCsvToMysqlPsummary.php")
											,array("tit"=>"Rental","value"=>"RestoreCsvToMysqlRental.php")
											,array("tit"=>"Rtmaster","value"=>"RestoreCsvToMysqlRtmaster.php")
											,array("tit"=>"Sales","value"=>"RestoreCsvToMysqlSales.php")
											,array("tit"=>"EXRAY","value"=>"RestoreCsvToMysqlExray.php")
											,array("tit"=>"Discount","value"=>"RestoreCsvToMysqlDiscount.php")
											,array("tit"=>"Estadistica","value"=>"RestoreCsvToMysqlEstadistica.php")
											,array("tit"=>"Properties PHP","value"=>"RestoreCsvToMysqlPropertiesPhp.php")
											,array("tit"=>"Properties Search","value"=>"RestoreCsvToMysqlPropertiesSearch.php")
											,array("tit"=>"Auction","value"=>"RestoreCsvToMysqlAuction.php")
											,array("tit"=>"Probates","value"=>"RestoreCsvToMysqlProbates.php")
											,array("tit"=>"Expired","value"=>"RestoreCsvToMysqlExpired.php")
							);
//,array("tit"=>"MarketPs","value"=>"RestoreCsvToMysqlMarketPs.php")		,array("tit"=>"Realtors","value"=>"RestoreCsvToMysqlRealtors.php")
				for($i=0;$i<count($titTransSincRestoreCalcu);$i++)
				{
			?>	 
					<tr id="tablasRF<?php echo $titTransSincRestoreCalcu[$i]["tit"] ?>">
						<td width="15"></td>
						<td width="350" ><?php echo $titTransSincRestoreCalcu[$i]["tit"] ?></td>
						<td width="135" ><input type="checkbox" name="ckbTranfSincCalcuRestore" id="<?php echo $titTransSincRestoreCalcu[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.ckbTranfSincCalcuRestore, <?php echo ($i+1)  ?>)">
						  Ejecutar</td>
					</tr>
			<?php 
				}
			?>		
				</table>	
		
		<td>
		</tr>

	</table>	
  </td>
  </tr>
<!-- *********************************************************************************************************************-->



  <tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titCantRTbl','imgCantRTbl'); return false;">
    	<img id="imgCantRTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Validacion y confirmacion de data</th>
	<th width="190" ><input type="checkbox" name="CantRTbl"  id="*" value="*" onClick="checkSeleccion(document.formulario.CantRTbl, 0)"> 
	  Todos</th>
</tr>
  <tr  id="titCantRTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<?php 
	$titCantRTbl=array(
					array("tit"=>"Diffvalue","value"=>"cantRecordDiffvalue.php"),
					array("tit"=>"Imágenes","value"=>"cantRecordImagenes.php"),
					array("tit"=>"Latlong","value"=>"cantRecordLatlong.php"),
					array("tit"=>"MarketPS","value"=>"cantRecordMarketPS.php"),
					array("tit"=>"Marketvalue","value"=>"cantRecordMarketvalue.php"),
					array("tit"=>"Mlsresidential","value"=>"cantRecordMlsresidential.php"),
					array("tit"=>"Mortgage","value"=>"cantRecordMortgage.php"),
					array("tit"=>"Pendes","value"=>"cantRecordPendes.php"),
					array("tit"=>"Psummary","value"=>"cantRecordPsummary.php"),

					array("tit"=>"Rental","value"=>"cantRecordRental.php"),
					array("tit"=>"Rtmaster","value"=>"cantRecordRtmaster.php"),
					array("tit"=>"Validación de la Data por Querys","value"=>"query.php"),
					array("tit"=>"Porcentajes en calculo MarketValue","value"=>"PorcentajeCalculomarketvalue.php"),
					array("tit"=>"Porcentajes en calculo MarketPs","value"=>"PorcentajeCalculomarketps.php"),
					array("tit"=>"Cantidad Foreclosure y Preforclosure","value"=>"CantidadPOF.php"),
					array("tit"=>"Cantidad de registros MLSresidential.Status='A'","value"=>"CantidadMLSresidentialstatusA.php"),
					array("tit"=>"Cantidad de Pendes con Mortgage=0","value"=>"CantidadPendesMTGCERO.php"),
					array("tit"=>"Cantidad de DebtTv=0","value"=>"CantidadDebttvCero.php")
				);//array("tit"=>"Arreglo de Porcentajes MarketValuePS","value"=>"ArregloPorcentajesMarketValuePS.php"),
	for($i=0;$i<count($titCantRTbl);$i++)
	{
?>	 
		<tr id="tablasV<?php echo $titCantRTbl[$i]["value"] ?>">
			<td width="15"></td>
			<td width="490" ><?php echo $titCantRTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="CantRTbl" id="<?php echo $titCantRTbl[$i]["value"] ?>" value="<?php echo ($i+1)  ?>"  onClick="checkSeleccion(document.formulario.CantRTbl, <?php echo ($i+1)  ?>)">
			  Ejecutar</td>
		</tr>
<?php 
	}
?>		
	</table>	
  </td>
  </tr>
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->

<div id="pasos">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filPasos','imgfilPasos'); return false;">
				<img id="imgfilPasos" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Pasos a Seguir Transferencia de DATA NUEVA</th>
		</tr>
		<tr  id="filPasos" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divPasos" >
                              <p><br>TODO ESTO ES EN SERVER2<br>
  ==========================================================<br>
  <br>
  Colocar los CSV's de Access1 en el directorio C:\XIMA\access1files\   en la carpeta de BD
que corresponde  <br>
  <br>
  1.- Ejecutar esta URL http://localhost/MANT/MENUMANTENIMIENTO.php y seleccionar la primera opcion "Mantenimiento Semanal" <br>
  <br>
  
2.- Seleccionar la Base de datos (de Mantenimiento, las que tienen MXXXXXX).<br>
  <br>
  
3.- Para efectos de registro y control de los procesos que se van a ejecutar, seleccionar la opcion "SI" en el RadioButton del Save, 
seguidamente ingresar la fecha que llevará el proceso de mantenimiento semanal.
<br> NOTA: se debe elejir la misma fecha para todo el mantenimiento durante los 3 condados...<br>
  <br>
  
4.- Desplegar el primer Item Azul -> Transferir y Sincronizar Data de Access1 a MySql (DATA NUEVA).<br>
  <br>

5.- Seleccionar el Checkbox "Todos" del primer Item amarillo (Hacer Backup de las Tablas). <br><br>
	  
6.- Verificar que cada uno de los CheckBox de Hacer Backup de las Tablas, esten seleccionados.<br>
      <br>
  
7.- Hacer click al boton Ejecutar, cuando termine los RESULTADOS SE MUESTRAN DEBAJO DEL BOTON EJECUTAR,
     junto con las estaditicas de cada php.<br>
      <br>

8.- Repetir los pasos 5, 6 y 7 para los siguientes items amarillos (Transferir la data a las Tablas, 
Ejecutar los php de Sincronización de Data (1/3), 
Ejecutar los php de Sincronización de Data (2/3), 
Ejecutar los php de Sincronización de Data (3/3)).<br>
      <br>

9.- Si se produce un error en alguna ejecucion de los php's, como data mas grande de lo permitido para un 
      campo o los beds o esos errores que dan ... corriges el error y seleccionas a partir de ese php los 
      php que hacen falta por ejecutar.<br>
      <br>
10.- IMPORTANTE: las opciones de "sincronizarLatlongconLatlongCounty" y "SincronizarImagenesconMlnumber"
del ultimo item amarillo (Ejecutar los php de Sincronización de Data (3/3)) 
deben haberse ejecutado para poder realizar los calculos, si no se ejecutaron, seleccionar esta opcion y verificar que realmente se ejecute
	  <br><br>
11.- Si NO se produce error (ojala y no pase ninguno..) se ejecutan los calculos...<br>
	  <br>
  
12.- Para los calculos, Desplegar el segundo Item azul y hacer clic al enlace...<br>
 <br>
  <br>
 
13.- Cuando terminen los Calculos, Imagenes y Centroids ya se tiene sincronizada y 
	   calculada la data, ahora viene la parte de pasar las tablas (Diffvalue, Imagenes, Latlong, Marketvalue,
	   Mlsresidential, Pendes, Psummary y Rtmaster) del Server2 al Server1 de flbroward y fldade..<br>
	   <br>
	   14.- Primero asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos" 
      que esta al lado del boton ejecutar, dale click y listo, el deseleccionará todos los checks.<br>
      <br>
15.- Ahora vas al tercer item en azul "Transferir data Sincronizada y Calculada Mant-Sist  (DATA NUEVA)" y seleccionas el CheckBox "Todos" del primer Item Amarillo &quot;Transferir Data de Tabla Mant--&gt;Sis&quot;, estos transferiran las tablas con los campos que deben tener a las tablas del Sistema que son diferentes a los de de Matenimiento (hasta ahora son solo Latlong, Marketvalue y MarketPs). Click al boton Ejecutar. </p>
                              <p> 16.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".</p>
                              <p>OJO DIRECTORIO NUEVO!!! ==> "C:\XIMA\dataserver1"<br>
                                17.- Ahora al segundo item amarillo "Creando CSV a transferir de Data Mantenimiento--&gt;Sistema", 
       el generará los CSV dentro de este directorio (es Nuevo, lo cree para esta parte) ==> "C:\XIMA\dataserver1" para la BD seleccionada. <br>
	   <br>
  
MONTANDO LA DATA NUEVA EN LAs BASES DE DATOS DEL SISTEMA DE SERVER2 Y SERVER1 <br>
  ========================================================================<br>
  SOLO EN SERVER1: <br>
  ---------------------------------------<BR>
                              18.- Luego que termine de ejecutarse zipeas los files del C:\XIMA\dataserver1 y los ftpeas al server1.<br><br>
                              19.- Luego de ftpearlos al Server1 los colocas en el directorio "C:\XIMA\dataserver1" en el directorio de 
         la BD que le corresponde.<br><br>
         
PARA SERVER1 Y SERVER2.<BR>
  ---------------------------------------<BR>
                                20.- Ejecutar esta URL SERVER2: http://208.109.178.110/MANT/datawork/mantenimiento.php O SERVER1: http://68.178.206.51/MANT/datawork/mantenimiento.php<br>
                                <br>
                                21.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".<br>
                                <br>
                                OJJJJJJJOOOOOOO!!!!!!!!!  IMPORTANTE!!!!!!!! BACKUP SERVER1 Y EN SERVER2<br>
                                22.- Vas al cuarto item en azul "BackUp - Restore (BASE DE DATOS FL)"
           y seleccionas el CheckBox "Todos" del primer Item Amarillo "Hacer Backup de las Tablas de Data del Sistema", 
           para generar backup de la Data del sistema que existe.<br>
           <br>
           23.- Asegurate de que no existan CheckBox seleccionados, para ello esta el enlace "Limpiar Todos".<br>
           <br>
                                OJJJJJJJOOOOOOO!!!!!!!!!  IMPORTANTE!!!!!!!! RESTORE SERVER1 Y EN SERVER2<br>
           24.- Cuando termine de hacer el backup seleccionas el CheckBox "Todos"(si aplica) del segundo Item Amarillo 
     "Restore de los CSV de Data Mantenimiento--&gt;Sistema", y estos pasaran los csv que estan en el directorio "C:\XIMA\dataserver1"de las tablas 
           selecciona a la data del sistema.<br>
           <br>
		     ========================================================================<br>
  			SOLO EN SERVER2: <br>
           25.- Ejecutar Item azul de Validacion y confirmacion de data, escojer todos, para ingresar la cantidad de registros de cada tabla en la base de datos para estadistica, y los otros procesos que esten alli </p>
							</div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>

<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
