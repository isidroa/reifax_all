<?php
//LOS CALCULOS DE MLS
//================================================================
	$starttimemls=tiempo();
//--------------------------------------------------------------------------------
	$ret[0]="mflbroward";
	$ret[1]="mfldade";
	$salidatotal.="<br><br>-------------------------------------------------------------------";
	$salidatotal.="<br>MLS RECALCULO ".date("d/m/Y h:i:s a");
	$salidatotal.="<br>-------------------------------------------------------------------";
	for($ibd=$infbd;$ibd<$supbd;$ibd++)
	{
		$nom_bd=$ret[$ibd];

		$ebd="<br><br>Base de Datos: ".strtoupper($nom_bd)."<br>";
//		$salidatotal.=$ebd;
		
		$re12[0]="RE1";
		$re12[1]="RE2";
		for($ire=$infre;$ire<$supre;$ire++)
		{
			$cproptype=$re12[$ire];
			$qzip="";
			if($tzip<>"" && $tzip<>NULL) $qzip =" AND rtm.zip='".$tzip."' ";
			$proper=strtoupper($cproptype);
			$salida="";
	
			mysql_query("RESET QUERY CACHE") or die("Error RESET QUERY CACHE".mysql_error());      
		
			if($proper=="RE1")
			{
				$query = "select 
						rtm.parcelid,
						rtm.sparcel,
						rtm.lsqft, 
						rtm.status,
						rtm.lprice,
						rtm.saleprice,
						ll.latitude,
						ll.longitude 
						From `$rtm` rtm , latlong ll, marketvalue mkv 
						Where rtm.parcelid=ll.parcelid and rtm.parcelid=mkv.parcelid  
						And (rtm.lsqft > 0) And  rtm.PROPTYPE='$proper' 
						And mkv.controlfecha is null".$qzip;
				if($pid<>'')$query.=$pid;
				if($tsec<>'')$query.=$section;
				if($lim>0)$query.=" limit $lim "; 
		
				$con_data=0;
				$con_actualizados=0;
				$con_actualizados_no=0;
				$con_market=0;
				$con_offert=0;
				$sin_data=0;
				$contador=0;
				$result = mysql_query($query) or die(mysql_error());      
//echo "<br>re1 $query<br>";
				while ($roww= mysql_fetch_array($result, MYSQL_ASSOC))
				{
					$contador++;
					$pid_actualizado=0;	
					$my_status=strtoupper($roww["status"]);
					$my_xprice=( ($my_status=="CS") || ($my_status=="CC")) ?$roww["saleprice"] :$roww["lprice"];
					$el_comparado=$roww["parcelid"];
					//if($my_xprice>=50000)// && $roww["latitude"]<>0 && $roww["longitude"]<>0)
					{
						$lat=$roww["latitude"];
						$lon=$roww["longitude"];
						$lsq=$roww["lsqft"];
						$current_parcel6=$roww["sparcel"];
						$current_parcel8=substr($el_comparado,0,8);
						for($imo=$infval;$imo<$supval;$imo++)
						{
							$media=0;
							$mp=0;
							$mv=0;
							if($imo==0)
							{	
								$tblrtmaster="rtmaster2";
								$cvalue="market"; 
								$stat =" rtm.status='CS' OR rtm.status='CC'";
								$q_pricesqft=" rtm.saleprice ";
								$price_where=" rtm.saleprice>=50000 ";
//								$year_where=" and ( rtm.closingdt>= DATE_SUB( curdate(), interval '365' day)) ";
							}
							if($imo==1)
							{
								$tblrtmaster="rtmaster3";
								$cvalue="offert";
								$stat =" rtm.status='A' OR rtm.status='B' OR rtm.status='PS' ";
								$q_pricesqft=" rtm.lprice ";
								$price_where=" rtm.lprice>=50000 ";
//								$year_where=" ";
							}
							$dtcomparables=array();

							$_calculo="sqrt((69.1* (ll.latitude- $lat))*(69.1*(ll.latitude-$lat))+(69.1*((ll.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((ll.longitude-($lon))*cos($lat/57.29577951))))";
									//and ($stat) 
							$sql_0_5="	SELECT rtm.ParcelID
										FROM $tblrtmaster rtm  force index(`Sparcel`) 
										Where 	rtm.sparcel='$current_parcel6'
												and rtm.PROPTYPE='$proper' 
												and rtm.lsqft>0 and $price_where 
												and rtm.lsqft>=($lsq*0.9) and rtm.lsqft<=($lsq*1.1) 
										"; 
		
//echo "<br>$sql_0_5<br>";
							$resultdis1 = mysql_query($sql_0_5) or die($sql_0_5." | ".mysql_error());      
							$orwhere="";
							$j=0;
							while ($rowwd1= mysql_fetch_array($resultdis1, MYSQL_ASSOC))
							{
								if($rowwd1["ParcelID"]<>$el_comparado)
								{
									if($j>0) $orwhere.=" OR ";
									$orwhere.=" rtm.ParcelID='".$rowwd1["ParcelID"]."' ";
									$j++;
								}
							}
							if($orwhere<>"")
							{
								$sql_distancia_0_5="	SELECT rtm.ParcelID, rtm.status,
														rtm.lprice,rtm.lsqft,
													($q_pricesqft/rtm.lsqft) as pricesqft,
													$_calculo as Distance
													FROM $tblrtmaster rtm , latlong ll 
													Where 	rtm.parcelid=ll.PARCELID 
															and ($orwhere) 
															and $_calculo<=0.5";
//echo "<br>$sql_distancia_0_5<br>";
								if( saveData($sql_distancia_0_5,'true','re1')==false) 
								{
									$sql_distancia_1_0="SELECT rtm.ParcelID, rtm.status,
														rtm.lprice,rtm.lsqft,
														($q_pricesqft/rtm.lsqft) as pricesqft,
														$_calculo as Distance
														FROM $tblrtmaster rtm, latlong ll 
														Where 	rtm.parcelid=ll.PARCELID 
																and ($orwhere) 
																and $_calculo<=1.0";
									saveData($sql_distancia_1_0,'false','re1');
//echo "<br>$sql_distancia_1_0<br>";
								}
							}//if $orwhere<>"" 0.5
							$cant=count($dtcomparables);
							if($cant>0)
							{
								$pid_actualizado=1;	
								usort($dtcomparables, 'compare');
								$num_filas=count($dtcomparables);
								$centro=floor($num_filas/2)-1;			
								$val_middle=$dtcomparables[$centro]["pricesqft"];
								$val_next=$dtcomparables[$centro+1]["pricesqft"];			
								$xparcel=(($num_filas%2)==0) ? ($centro):$centro+1;		
								$media=(($num_filas%2)==0) ? ($val_middle+$val_next)/2 :$val_next;
								$mv=$lsq*$media;
								
								$con_data++;
								if($con_data<50 || ($con_data%100)==0)
								{
									echo "num_fil: $cant | lsq: $lsq | con_tot: $contador    | Parcelid: ".$el_comparado."  | ".$cvalue."Media: ".$media."  | ".$cvalue."Value: ".$mv."  | ".$cvalue."Pctg: ".$mp."<br>";
									//echo "Tiempo: ".(tiempo() - $starttimemls)." seg <br>";	

									if($impcomp==1)
									{
										echo "COMPARABLES <BR>";
										$tr=0;
										while($tr<$cant)
										{
											$resml=mysql_query("SELECT mlnumber FROM $mls where parcelid='".$dtcomparables[$tr]["ParcelID"]."'");
											$rwoml= mysql_fetch_array($resml, MYSQL_ASSOC);
											echo " ".($tr+1);
											echo " Parcelid: ".$dtcomparables[$tr]["ParcelID"].
												 " MLNumber: ".$rwoml["mlnumber"].
												 " Status: ".$dtcomparables[$tr]["status"].
												 " Pricesqft: ".$dtcomparables[$tr]["pricesqft"].
												 " Lsqft: ".$dtcomparables[$tr]["lsqft"].
												 " LDate: ".$dtcomparables[$tr]["ldate"].
												 " LPrice: ".$dtcomparables[$tr]["lprice"].
												 " Distancia: ".$dtcomparables[$tr]["Distance"];
											echo "<br>";
											$tr++;
										}							
										echo "<br>";					
									}
								}
							}
							else 
							{
								$sin_data++;
		//echo "Sin data ind: $sin_data    | num_fil: $cant    | con_tot: $contador    | Parcelid: ".$el_comparado."  | Mlnumber: ".$rowsmith[0]."  | ZIP: ".$rowsmith[1]."<br>";
							}
							if($cvalue=="market") 
							{
								//		Marketpctg=".$mp." 
								$sql_update = "	UPDATE marketvalue SET MarketMedia=".$media.", 
												MarketValue=".$mv.", 
												controlFecha=CURRENT_DATE() 
												WHERE parcelid='".$el_comparado."';";
												
								if($cant>0)$con_market++;
							}
							if($cvalue=="offert") 				
							{
								//	OffertPctg=".$mp." 
								$sql_update = "	UPDATE marketvalue SET OffertMedia=".$media.", 
												OffertValue=".$mv.", 
												controlFecha=CURRENT_DATE() 
												WHERE parcelid='".$el_comparado."';";
								if($cant>0)$con_offert++;
							}
							if($actualiza==1)
								mysql_query($sql_update) or die($sql_update." ".mysql_error());
							unset($dtcomparables);
							//echo "<br>";		
						}//fin for de MARKET Y OFFERT
					}//fin if $my_xprice>=50000
					if($pid_actualizado==1)$con_actualizados++; else $con_actualizados_no++;
				}//while comparados
				if($contador==0){
					$endtime=(tiempo() - $starttimemls);
					$salida.="<br>No se encontraron registros.";
					/*ingreso(83,$county,0,0);
					ingreso(84,$county,0,0);
					ingreso(133,$county,$endtime,0);*/
				}
				else
				{
					$endtime=(tiempo() - $starttimemls);
					$salida="<br>Value: MARKET Proptype: ".strtoupper($cproptype).
							"<br>COMPARADOS CON COMPARABLES Y ACTUALIZADOS EN MARKETVALUE: ".$con_market.
							"<br>Value: OFFERT Proptype: ".strtoupper($cproptype).
							"<br>COMPARADOS CON COMPARABLES Y ACTUALIZADOS EN MARKETVALUE: ".$con_offert.
							"<br>TOTAL COMPARADOS SELECT: ".$contador.
							"<br>TOTAL COMPARADOS ACTUALIZADOS: ".$con_actualizados.
							"<br>TOTAL COMPARADOS SIN COMPARABLES: ".$con_actualizados_no.
							"<br>PORCENTAJE SIN COMPARABLES: ".round((($con_actualizados_no*100)/$contador),2)."%".
							"<br>TIEMPO: ".$endtime." seg <br>";	
					
					/*ingreso(83,$county,0,$con_market);
					ingreso(84,$county,0,$con_offert);
					ingreso(133,$county,$endtime,round((($con_actualizados_no*100)/$contador),2));		*/
					//ActionId 83 $con_market
					//ActionId 84 $con_offert				
				}
				$salidatotal.=$salida;
				echo $salida."<br><br>";			
			}//fin if SF RE1
			else
			{
				$starttimemls2=tiempo();
				$query ="select  
							rtm.parcelid,
							rtm.sparcel,
							rtm.status,
							rtm.beds,
							rtm.bath,
							rtm.type,
							rtm.saleprice,
							rtm.lprice,		
							rtm.yrbuilt,
							rtm.lprice,
							ll.latitude,
							ll.longitude
							From `$rtm` rtm , latlong ll, marketvalue mkv 
							Where rtm.parcelid=ll.parcelid and rtm.parcelid=mkv.parcelid  
							And  rtm.PROPTYPE='$proper' And mkv.controlfecha is null".$qzip;
				if($pid<>'')$query.=$pid;
				if($tsec<>'')$query.=$section;
				if($lim>0)$query.=" limit $lim "; 

				$con_data=0;
				$con_actualizados=0;
				$con_actualizados_no=0;
				$con_market=0;
				$con_offert=0;
				$sin_data=0;
				$contador=0;
				$result = mysql_query($query) or die(mysql_error());      
//echo "<br>re2 $query<br>";
				while ($roww= mysql_fetch_array($result, MYSQL_ASSOC))
				{
					$contador++;
					$pid_actualizado=0;	
					$my_status=strtoupper($roww["status"]);
					$my_xprice=( ($my_status=="CS") || ($my_status=="CC")) ?$roww["saleprice"] :$roww["lprice"] ;
					$el_comparado=$roww['parcelid'];
//					if($my_xprice>=30000)// && $roww["latitude"]<>0 && $roww["longitude"]<>0)
					{
						$com_year=$roww["yrbuilt"];
						$com_beds=$roww["beds"];
						$com_baths=$roww["bath"];
						$type=$roww["type"];
						$lat=$roww["latitude"];
						$lon=$roww["longitude"];
						$current_parcel6=$roww["sparcel"];
						$current_parcel8=substr($el_comparado,0,8);
							
						for($imo=$infval;$imo<$supval;$imo++)
						{
							$media=0;
							$mp=0;
							$mv=0;
							if($imo==0)
							{	
								$tblrtmaster="rtmaster2";
								$cvalue="market"; 
								$stat =" rtm.status='CS' OR rtm.status='CC'";
								$q_pricesqft=" rtm.saleprice ";
								$price_where=" rtm.saleprice>=30000 ";
								//$year_where=" and ( rtm.closingdt>= DATE_SUB( curdate(), interval '365' day)) ";
							}
							if($imo==1)
							{
								$tblrtmaster="rtmaster3";
								$cvalue="offert";
								$stat =" rtm.status='A' OR rtm.status='B' OR rtm.status='PS' ";
								$q_pricesqft=" rtm.lprice ";
								$price_where=" rtm.lprice>=30000 ";
								//$year_where=" ";
							}
							$dtcomparables=array();
							$_calculo="sqrt((69.1* (ll.latitude- $lat))*(69.1*(ll.latitude-$lat))+(69.1*((ll.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((ll.longitude-($lon))*cos($lat/57.29577951))))";
								//and ($stat) 
							$sql_0_5="	SELECT rtm.ParcelID
										FROM $tblrtmaster rtm  force index(`Sparcel`) 
										Where 	rtm.sparcel='$current_parcel6' 
												and rtm.PROPTYPE='$proper' 												
												and $price_where and rtm.type='$type' and 
												rtm.beds=$com_beds and rtm.yrbuilt>=($com_year-3) and 
												rtm.yrbuilt<=($com_year+3) 												
										"; 
		
//echo "<br>$sql_0_5<br>";
							$resultdis3 = mysql_query($sql_0_5) or die(mysql_error());      
							$orwhere="";
							$j=0;
							while ($rowwd1= mysql_fetch_array($resultdis3, MYSQL_ASSOC))
							{
								if($rowwd1["ParcelID"]<>$el_comparado)
								{
									if($j>0) $orwhere.=" OR ";
									$orwhere.=" rtm.ParcelID='".$rowwd1["ParcelID"]."' ";
									$j++;
								}
							}
							if($orwhere<>"")
							{
								$sql_distancia_0_5="	SELECT rtm.ParcelID, rtm.status,
														rtm.lprice,rtm.lsqft,
													$q_pricesqft as pricesqft,
													$_calculo as Distance
													FROM $tblrtmaster rtm , latlong ll 
													Where 	rtm.parcelid=ll.PARCELID 
															and ($orwhere) 
															and $_calculo<=0.1";
//echo "<br>$sql_distancia_0_5<br>";
								if( saveData($sql_distancia_0_5,'true','re2')==false) 
								{
									$sql_distancia_1_0="SELECT rtm.ParcelID, rtm.status,
														rtm.lprice,rtm.lsqft,
														$q_pricesqft as pricesqft, 
														$_calculo as Distance
														FROM $tblrtmaster rtm, latlong ll 
														Where 	rtm.parcelid=ll.PARCELID 
																and ($orwhere)
																and $_calculo<=0.2";
									saveData($sql_distancia_1_0,'false','re2');
//echo "<br>$sql_distancia_1_0<br>";
								}
							}//if $orwhere<>"" 0.5

							$cant=count($dtcomparables);
							if($cant>0)
							{
								$pid_actualizado=1;	
								usort($dtcomparables, 'compare');
								$num_filas=count($dtcomparables);
								$centro=floor($num_filas/2)-1;			
								$val_middle=$dtcomparables[$centro]["pricesqft"];
								$val_next=$dtcomparables[$centro+1]["pricesqft"];			
								$xparcel=(($num_filas%2)==0) ? ($centro):$centro+1;		
								$media=(($num_filas%2)==0) ? ($val_middle+$val_next)/2 :$val_next;
								$mv=$media;
								$con_data++;
								if($con_data<50 || ($con_data%100)==0)
								{
									echo "num_fil: $cant    | con_tot: $contador    | Parcelid: ".$el_comparado."  | ".$cvalue."Media: ".$media."  | ".$cvalue."Value: ".$mv."  | ".$cvalue."Pctg: ".$mp."<br>";
									//echo "Tiempo: ".(tiempo() - $starttimemls)." seg <br>";	
		//echo "ind: $con_data | num_fil: $cant | con_tot: $contador | Parcelid: ".$el_comparado." | Mlnumber: ".$rowsmith[0]." | ZIP: ".$rowsmith[1]." | ".$cvalue." Media: ".$media."  | ".$cvalue."Value: ".$mv."  | ".$cvalue."Pctg: ".$mp."<br>";
									if($impcomp==1)
									{
										echo "COMPARABLES <BR>";
										$tr=0;
										while($tr<$cant)
										{
											echo " ".($tr+1);
											echo " Parcelid: ".$dtcomparables[$tr]["ParcelID"].
												 " Status: ".$dtcomparables[$tr]["status"].
												 " Pricesqft: ".$dtcomparables[$tr]["pricesqft"].
												 " Lsqft: ".$dtcomparables[$tr]["lsqft"].
												 " LPrice: ".$dtcomparables[$tr]["lprice"].
												 " Distancia: ".$dtcomparables[$tr]["Distance"];
											echo "<br>";
											$tr++;
										}							
										echo "<br>";					
									}	
								}
							}		
							else //if($cant>0) 
							{
								$sin_data++;
		//echo "Sin data ind: $sin_data    | num_fil: $cant    | con_tot: $contador    | Parcelid: ".$el_comparado."  | Mlnumber: ".$rowsmith[0]."  | ZIP: ".$rowsmith[1]."<br>";
							}

							if($cvalue=="market") 
							{
//												Marketpctg=".$mp.", 
								$sql_update = "	UPDATE marketvalue SET MarketMedia=".$media.", 
												MarketValue=".$mv.", 
												controlFecha=CURRENT_DATE() 
												WHERE parcelid='".$el_comparado."';";
								if($cant>0)$con_market++;
							}
							if($cvalue=="offert") 				
							{
//												OffertPctg=".$mp.", 
								$sql_update = "	UPDATE marketvalue SET OffertMedia=".$media.", 
												OffertValue=".$mv.", 
												controlFecha=CURRENT_DATE() 
												WHERE parcelid='".$el_comparado."';";
								if($cant>0)$con_offert++;
							}
							if($actualiza==1)
								mysql_query($sql_update) or die($sql_update." ".mysql_error());
								
							unset($dtcomparables);
						}//fin for de MARKET Y OFFERT
					}//fin if $my_xprice>=30000
					if($pid_actualizado==1)$con_actualizados++; else $con_actualizados_no++;
				}//while comparados
				if($contador==0){
					$endtime=(tiempo() - $starttimemls2);
					$salida.="<br>No se encontraron registros.";
					ingreso(85,$county,0,0);
					ingreso(86,$county,0,0);
					ingreso(133,$county,$endtime,0);
				}
				else
				{
					$endtime=(tiempo() - $starttimemls2);
					$salida="<br>Value: MARKET Proptype: ".strtoupper($cproptype).
							"<br>COMPARADOS CON COMPARABLES Y ACTUALIZADOS EN MARKETVALUE: ".$con_market.
							"<br>Value: OFFERT Proptype: ".strtoupper($cproptype).
							"<br>COMPARADOS CON COMPARABLES Y ACTUALIZADOS EN MARKETVALUE: ".$con_offert.
							"<br>TOTAL COMPARADOS SELECT: ".$contador.
							"<br>TOTAL COMPARADOS ACTUALIZADOS: ".$con_actualizados.
							"<br>TOTAL COMPARADOS SIN COMPARABLES: ".$con_actualizados_no.
							"<br>PORCENTAJE SIN COMPARABLES: ".round((($con_actualizados_no*100)/$contador),2)."%".
							"<br>TIEMPO: ".$endtime." seg <br>";	
					
					ingreso(85,$county,0,$con_market);
					ingreso(86,$county,0,$con_offert);
					ingreso(134,$county,$endtime,round((($con_actualizados_no*100)/$contador),2));		
					//ActionId 85 $con_market
					//ActionId 86 $con_offert				
				}
				$salidatotal.=$salida;
				echo $salida;
				//mysql_query("ALTER TABLE rtmaster DROP INDEX ClosingDT, DROP INDEX Lsqft");
			}//fin if condos RE2

		}//fin FOR DE RE1 Y RE2
	}//fin FOR DE BASE DE DATOS 
/****************************************************/
	$endtimemls=tiempo();
	$timeMls2=$endtimemls - $starttimemls;
	$salidatotal.="<br>TIEMPO DE EJECUCION DEL PHP: ".$timeMls2." SEGUNDOS";

?> 
