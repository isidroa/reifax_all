DROP DATABASE IF EXISTS `mantenimiento`;
CREATE DATABASE `mantenimiento`;
USE `mantenimiento`;



DROP TABLE IF EXISTS `administracion`;
CREATE TABLE `administracion` (
  `ID` int(16) NOT NULL auto_increment,
  `ActionID` int(16) NOT NULL,
  `CountyID` int(3) NOT NULL default '1' COMMENT '1=BROWARD, 2=DADE, 3=PALM BEACH',
  `fecha` date NOT NULL,
  `TEjecucion` double(8,5) NOT NULL default '0.00000',
  `Records` int(16) NOT NULL default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;