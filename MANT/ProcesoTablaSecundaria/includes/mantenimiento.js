function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function checkSeleccion(field, final) 
{
	if (field[0].checked == false) 
	{
		for (i = 1; i < field.length; i++)
			field[i].checked = false;
	}
	if (field[0].checked == true) 
	{
		for (i = 1; i < field.length; i++)
			field[i].checked = true;
	}
	else if(field[1].checked == true)
	{
		for (i = 2; i < final; i++)
			field[i].checked = true;
	}else{
		for (i = 0; i < field.length; i++)
			field[i].checked = false;
	}
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var field,i;
	
	field =document.formulario.ckbRental;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbMortgage;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
}

function buscarParametros()
{
	var field,i;
	var ret="",datnew="";
	var ruta,texto;
	var cont=0;
	var swbd=0;
	var cad;

	
		field =document.formulario.ckbRental;
		ruta="Rental/";
		texto="Rental";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				
				datnew="datanueva=csvfiles&copypaste=0";
				if(i==12 || i==14)
				{
					datnew="datanueva=dataserver1&copypaste=0";
				}
				if(i==10)
				{
					ruta="../DataWork/dataverificacion/";
					texto="Rental";
				}
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'","id"=>"'+field[i].value+'"^';
			}
			
		}
		
		field =document.formulario.ckbMortgage;
		ruta="Mortgage/";
		texto="Mortgage";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
				if(i==3 || i==5)
				{
					datnew="datanueva=dataserver1&copypaste=0";
				}
			}
		}
	//alert(datnew+"&parametros="+ret);
	if(ret=="") return "";
	else return datnew+"&parametros="+ret;
}

function ejecutarPHP()
{	
	var i;
	var bd=document.getElementById("cbd").value;
	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd=="")
		alert("Debes seleccionar Base de Datos");
	else
	{
		
		parametros=buscarParametros();
		if(parametros==null || parametros=="")
			alert("Debes seleccionar al menos una opcion a Ejecutar");
		else
		{

			ret=ValidarBD(bd);
			if(ret!=2)
			{
				if(ret==0)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Sistema.\n Debes seleccionar Base de Datos de Sistema");
				if(ret==1)
				alert("Seleccionastes la ejecuci�n de un PHP que trabaja directamente con Data de Mantenimiento.\n Debes seleccionar Base de Datos de Mantenimiento");
				return;
			}
			
			document.getElementById("divResultados").innerHTML='';
			document.getElementById("imgfilResultados").src="includes/up.png";
			document.getElementById("imgfilResultados").alt="Ocultar Resultados";
			document.getElementById("reloj").style.visibility="visible";
			//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=true;
			
			if(document.getElementById("guardarAccess1").checked==true){
				var guardarAccess = document.getElementById("guardarAccess1").value;
				var fechaAccess = document.getElementById("fechaAccess1").value;
			}
			else
				var guardarAccess = document.getElementById("guardarAccess2").value;
			
			var ajax=nuevoAjax();
			ajax.open("POST", "mantenimiento_respuesta.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("bd="+bd+"&fechaAccess="+fechaAccess+"&guardarAccess="+guardarAccess+"&"+parametros);
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					htmlTag.push(ajax.responseText);
					document.getElementById("divResultados").innerHTML=htmlTag.join('');
					document.getElementById("filResultados").style.display="inline";
					document.getElementById("reloj").style.visibility="hidden";
					//controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=false;
					limpiarchecks();
					//habilitacion(false);
				}
			}
		}//Al Menos un ckechboxes seleccionado
	}//IF Base de Datos
}

function ValidarBD(bd)
{
	var letra=bd.substr(0,1);
	var i;
//SI BD ES DE MANTENIMINENTO, NINGUNO DE 
//ESTOS PHP DEBE ESTAR SELECCIONADO
	if(letra.toUpperCase()=='M') //MANTENIMIENTO
	{
		
		field =document.formulario.ckbRental;
		for (i=13; i<field.length;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 0;

	}
	else //SISTEMA
	{
		field=document.formulario.ckbRental;
		for (i=0; i<13;i++)
			if(field[i].checked==true && field[i].id!="*")
				return 1;

	}
	return 2;
}


function validarFecha(Cadena){
	
    var Fecha= new String(Cadena) 
	
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length)) 
  

    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
            alert('Fecha Incorrecta. A�o inv�lido')  
        return false  
    }  

    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        alert('Fecha Incorrecta. Mes inv�lido')  
        return false  
    }  

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        alert('Fecha Incorrecta. D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            alert('Fecha Incorrecta. D�a inv�lido')  
            return false  
        }  
    }  
 
  return true      
}