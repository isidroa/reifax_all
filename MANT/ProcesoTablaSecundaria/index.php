<html>
<head>
<title>MANTENIMIENTO DE TABLAS SECUNDARIAS</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">MANTENIMIENTO DE TABLAS SECUNDARIAS</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
			<th width="190" >BD: 
				<select name="cbd" id='cbd'>
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
					<option value="MFLOSCEOLA">MFLOSCEOLA</option>
					<option value="MFLLAKE">MFLLAKE</option>
					<option value="MFLORANGE">MFLORANGE</option>
					<option value="FLBROWARD" >FLBROWARD</option>
					<option value="FLDADE"    >FLDADE</option>
					<option value="FLPALMBEACH">FLPALMBEACH</option>
					<option value="FLOSCEOLA">FLOSCEOLA</option>
					<option value="FLLAKE">FLLAKE</option>
					<option value="FLORANGE">FLORANGE</option>
					<option value="FLBROWARD1" >FLBROWARD2</option>
					<option value="FLDADE1"    >FLDADE2</option>
					<option value="FLPALMBEACH1">FLPALMBEACH2</option>
					<option value="FLOSCEOLA1">FLOSCEOLA2</option>
					<option value="FLLAKE1">FLLAKE2</option>
					<option value="FLORANGE1">FLORANGE2</option>
				</select>
				<br> SAVE: <input type="radio" name="guardarAccess" id="guardarAccess1" value=1>SI<input name="guardarAccess" type="radio" id="guardarAccess2" value=0 checked>NO
                <br> FECHA: <input name="fechaAccess" type="text" id="fechaAccess1" value="AAAA-MM-DD" size="12" maxlength="10" onFocus="javascript: this.select();" onBlur="javascript: if(document.getElementById('guardarAccess1').checked==true) validarFecha(this.value);">
			</th>
		</tr>

<!--Rental -->		

	<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titRental','imgRental'); return false;">
    	<img id="imgRental" src="includes/down.png" border="0" ></a>
	</th>

<th colspan="2">RENTAL </th>
	<th width="190" ><input type="checkbox" name="ckbRental"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbRental, 0)"> 
	  Todos  </th>
</tr>
  <tr  id="titRental" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titRental=array(
					array("tit"=>"BackUpRental","value"=>"BackUpRental.php","id"=>"1"),
					array("tit"=>"TransferRental","value"=>"TransferRental.php","id"=>"2"),
					array("tit"=>"parcelidRental","value"=>"parcelidRental.php","id"=>"3"),
					array("tit"=>"ArreglaFechaRental","value"=>"ArreglaFechaRental.php","id"=>"4"),
					array("tit"=>"EliminarDuplicadoRental","value"=>"EliminarDuplicadoRental.php","id"=>"5"),
					array("tit"=>"parcelidMlnumberRental","value"=>"parcelidMlnumberRental.php","id"=>"6"),
					array("tit"=>"sincronizaLatlongconMlsRental","value"=>"sincronizaLatlongconMlsRental.php","id"=>"7"),
					array("tit"=>"sincronizarLatlongconLatlongCounty","value"=>"sincronizarLatlongconLatlongCounty.php","id"=>"8"),
					array("tit"=>"fillsparcel","value"=>"fillsparcel.php","id"=>"9"),
					array("tit"=>" fillsparcelLatlong","value"=>"fillsparcelLatlong.php","id"=>"10"),
					array("tit"=>"fillsparcelMLS.php","value"=>"fillsparcelMLS.php","id"=>"11"),
					array("tit"=>"CrearCSVRental","value"=>"CrearCSVRental.php","id"=>"12"),
					array("tit"=>"BackUpRental (FL)","value"=>"BackUpRental.php","id"=>"13"),
					array("tit"=>"RestoreRental (FL)","value"=>"RestoreRental.php","id"=>"14")
				);
	for($i=0;$i<count($titRental);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titRental[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbRental" id="<?php echo $titRental[$i]["value"] ?>" value="<?php echo $titRental[$i]["id"]  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>


<!--Mortgage -->		

	<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titMortgage','imgMortgage'); return false;">
    	<img id="imgMortgage" src="includes/down.png" border="0" ></a>
	</th>

<th colspan="2">MORTGAGE </th>
	<th width="190" ><input type="checkbox" name="ckbMortgage"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbMortgage, 0)"> 
	  Todos  </th>
</tr>
  <tr  id="titMortgage" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titMortgage=array(
					array("tit"=>"BackUpMortgage","value"=>"BackUpMortgage.php"),
					array("tit"=>"TransferMortgage","value"=>"TransferMortgage.php"),
					array("tit"=>"CrearCSVMortgage","value"=>"CrearCSVMortgage.php"),
					array("tit"=>"BackUpMortgage (FL)","value"=>"BackUpMortgage.php"),
					array("tit"=>"RestoreMortgage (FL)","value"=>"RestoreMortgage.php"),
				);
	for($i=0;$i<count($titMortgage);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titMortgage[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbMortgage" id="<?php echo $titMortgage[$i]["value"] ?>" value="<?php echo ($i+1)  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>

  
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->

<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
