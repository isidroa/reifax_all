﻿<?php 	 
if (!function_exists('formatear')) {
	function formatear($cad,$format)
	{
		$cad=html2txt($cad);///QUITANDOLE CUALQUIER ETIQUETA HTML QUE HAYA	
		$cad=onlyChars($cad);			
		switch($format)
		{
			case 'string':
				$cad=str_replace('<BR>','',$cad);
				$cad=str_replace("'",'',$cad);
				$cad=str_replace('"','',$cad);
			break;

			case 'stringparcelid':
				$cad=str_replace('<BR>','',$cad);
				$cad=str_replace("'",'',$cad);
				$cad=str_replace('"','',$cad);
				$cad=str_replace('-','',$cad);
				$cad=str_replace('.','',$cad);
				$cad=substr($cad,0,30);
			break;
			
			case 'numberfloat':
				IF(strlen($cad)==0)$cad='0';
				$cad=floatval(str_replace(',','',str_replace('$','',$cad)));
			break;

			case 'stringemail':
				$pos=strpos($cad,'<A HREF');
				if ($pos !== false)
				{
					$aux=substr($cad,$pos+5);
					$pini=strpos($aux,'>')+1;
					$pfin=strpos($aux,'<')-1;
					$cad=substr($aux,$pini,($pfin-$pini));
				}
				$cad=str_replace('<BR>','',$cad);
				$cad=str_replace('´','',$cad);
				$cad=str_replace('`','',$cad);
				$cad=str_replace("'",'',$cad);
				$cad=str_replace('"','',$cad);
			break;

			case 'date':
				$timestamp=strtotime($cad);
				$cad=date('Ymd',$timestamp);
			break;
		}

		return $cad;
	}
}
if (!function_exists('html2txt')) {
	function html2txt($document)
	{
		$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
					   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
					   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
					   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
					);
		$text = preg_replace($search, '', $document);
		$text=str_replace('\\','',$text);
		$text=str_replace('~','*',$text);
		$text=str_replace('^','',$text);
		return $text;
	} 
}
if (!function_exists('onlyChars')) {
	function onlyChars($string)
	{
		$strlength = strlen($string);
		$retString = "";
		for($i = 0; $i < $strlength; $i++)
		{
			if(ord($string[$i]) >= 32 && ord($string[$i]) <= 125){
				$retString .= $string[$i];
			}
		}
	   
		return $retString;   
	}	
}
	class Fecha {
	  var $fecha;
	  function Fecha($anio=0, $mes=0, $dia=0) 
	  {
		   if($anio==0) $anio = Date("Y");
		   if($mes==0) $mes = Date("m");
		   if($dia==0) $dia = Date("d");
		   $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
	  }
	  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
	  {
		   $array_date = explode("-", $this->fecha);
		   $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
	  }
	 
	  function getFecha() { return $this->fecha; }
	}

if (!function_exists('parteHoramsx')) {
	// funcion que devuelve un array con los elementos de una hora
	function parteHoramsx( $hora )
    {    
    	$horaSplit = explode(":", $hora);
		if( count($horaSplit) < 3 )
        {
        	$horaSplit[2] = 0;
		}
		return $horaSplit;
	}
}

if (!function_exists('RestaHorasmsx')) {
    // funcion que devuelve la resta de dos horas en formato string
    function RestaHorasmsx( $time1, $time2 )
    {
        list($hour1, $min1, $sec1) = parteHoramsx($time1);
        list($hour2, $min2, $sec2) = parteHoramsx($time2);
        return date('H:i:s', mktime( $hour1 - $hour2, $min1 - $min2, $sec1 - $sec2));
    }  	
}  	

if (!function_exists('tiempomsx')) {
	function tiempomsx()
	{
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		return $mtime;
	}
}

if (!function_exists('ordenarPormsx')) {
	function ordenarPormsx($vector,$field,$orden)
	{
		foreach($vector as $i=>$valor)
			$ordenar[$i]=doubleval($vector[$i][$field]);
		if($orden=='asc') $orden=SORT_ASC;
		else $orden=SORT_DESC;
		array_multisort($ordenar,$orden,SORT_NUMERIC,$vector);
		return($vector);
	}
}

if (!function_exists('BorrarDirectoriomsx')) {
	function BorrarDirectoriomsx($directorio) 
	{
		//definimos el path de acceso
		//$path ="C:\\Servidor\\htdocs\\MANT\\msxalladiario\\".$directorio;
		$path =$_SERVER['DOCUMENT_ROOT']."\\MANT\\msxalladiario\\".$directorio;
		//abrimos el directorio
		$dir = opendir($path);
		//Mostramos las informaciones
		$i=0;
		while ($elemento = readdir($dir))
		{ 
			$i++;
			if($i>2)
			   unlink($path.$elemento);
		}
		//Cerramos el directorio
		closedir($dir); 
	}
}
/*
if (!function_exists('curl_get_file_contentsmsx')) {
	function curl_get_file_contentsmsx($URL){
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }		
}
*/
if (!function_exists('curl_get_file_contentsmsx')) {
	function curl_get_file_contentsmsx($URL){
		/*$countProxy=rand (1,2);
		switch($countProxy){
			case 1:
				$proxyName="us.proxymesh.com";
				break;
			case 2:
				$proxyName="us-il.proxymesh.com";
				$countProxy=0;
				break;
		}*/
		$proxyName="reipropertyfax.crawlera.com";
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10');
        curl_setopt($c, CURLOPT_PROXYPORT, '8010');
		curl_setopt($c, CURLOPT_PROXYTYPE, 'HTTP');
		curl_setopt($c, CURLOPT_PROXY, $proxyName);
		curl_setopt($c, CURLOPT_PROXYUSERPWD, 'reipropertyfax:doVkrvkR4o');
  
		$contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
			

			
    }		
}


if (!function_exists('buscardireccionrealtor')) {
	function buscardireccionrealtor($bd) 
	{
		$que="select m.mlnumber, m.address from ".$bd.".msxaddress m where m.marca='P'";
		$res=mysql_query($que) or die($que.mysql_error());
		$total=mysql_num_rows($res);
		echo "<br>Total registros a procesar: ".$total;
		$p=0;
		$arrayData=Array(Array(),Array());
		while($r=mysql_fetch_array($res)){
			$p++;
			$mlslistingid=$r['mlnumber'];
			$addressMls=$r['address'];
			$url='http://www.realtor.com/realestateandhomes-search?pgsz=3&mlslid='.$mlslistingid;
			$source = curl_get_file_contentsmsx($url);//@file_get_contents or die('se ha producido un error');
			echo '<br>'.$p.' '.$mlslistingid;	
			if(!$source || $source ==false )//hacer el ciclo de verificar que se dañe 
			{	
				$i=0;
				do
				{
					$i++;
					$source = curl_get_file_contentsmsx($url);//@file_get_contentsor die('se ha producido un error');
					if($i==100){
						break;
					}
				}while(!$source || $source ==false );
			}
			$html=str_get_html($source);
			if($html->find('.primaryAction',0)!=null){
				$cadena=$html->find('.primaryAction',0)->innertext();
				$cadena=substr($cadena,4);
				$posicion=strpos($cadena,'</em>');
				$address=substr($cadena,0,$posicion);
				//echo $address;
				//echo '<br>'.strtolower(substr(trim($address),0,5)).' -- '.strtolower(substr(trim($addressMls),0,5));
				if(strtolower(substr(trim($address),0,5))==strtolower(substr(trim($addressMls),0,5)) && trim($address)!=''){
					if(trim($address)!=trim($addressMls)){
						$arrayData[0][]=$mlslistingid;
						$arrayData[1][]=$address;
					}	
				}
			}
			$html->clear();
			unset($html);	
		}
		$tam=count($arrayData[0]);
		$queryD='';
		$queryB="insert into msxaddress (mlnumber,address) values ";
		for($j=0;$j<$tam;$j++){
			echo '<br>'.$arrayData[0][$j].' '.$arrayData[1][$j];
			$query="update ".$bd.".msxaddress set address='".quitarCaracteres21($arrayData[1][$j])."',marca='Y'
					where mlnumber='".$arrayData[0][$j]."'";
			//echo '<br>'.$query;
			mysql_query($query) or die("error ".$query.mysql_error());	
				
		}
		$query="update ".$bd.".msxaddress set marca='N' where marca<>'Y'";
		//echo '<br>'.$query;
		mysql_query($query) or die("error ".$query.mysql_error());
		$fecha=time();
		echo '<br>Final '.date("H:i:s",$fecha);
		
	}//fin funcion	
}
?>

