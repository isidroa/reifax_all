<?php
	require("capa_calculo_estadistica.php");
	$ret=explode("*",CabeceraPagina()); 
	PiePagina();
	
	$nom_bd=$ret[0]; // flbroward, fldade, flpalmbeach
	switch($nom_bd)
	{
		case "mflbroward": $county="BROWARD";
							$total_zip = array(33004, 33008, 33009, 33019, 33020, 33021, 33022, 33023, 33024, 33025, 33026, 33027, 33028, 33029, 33060, 33061, 33062, 33063, 33064, 33065, 33066, 33067, 33068, 33069, 33071, 33072, 33073, 33074, 33075, 33076, 33077, 33081, 33082, 33083, 33084, 33093, 33097, 33301, 33302, 33303, 33304, 33305, 33306, 33307, 33308, 33309, 33310, 33311, 33312, 33313, 33314, 33315, 33316, 33317, 33318, 33319, 33320, 33321, 33322, 33323, 33324, 33325, 33326, 33327, 33328, 33329, 33330, 33331, 33332, 33334, 33335, 33336, 33337, 33338, 33339, 33340, 33345, 33346, 33348, 33349, 33351, 33355, 33359, 33388, 33394, 33441, 33442, 33443);
			break;
		case "mfldade": $county="DADE";
							$total_zip = array(33002, 33010, 33011, 33012, 33013, 33014, 33015, 33016, 33017, 33018, 33030, 33031, 33032, 33033, 33034, 33035, 33039, 33054, 33055, 33056, 33090, 33092, 33101, 33102, 33107, 33109, 33110, 33111, 33112, 33114, 33116, 33119, 33121, 33122, 33124, 33125, 33126, 33127, 33128, 33129, 33130, 33131, 33132, 33133, 33134, 33135, 33136, 33137, 33138, 33139, 33140, 33141, 33142, 33143, 33144, 33145, 33146, 33147, 33148, 33149, 33150, 33151, 33152, 33153, 33154, 33155, 33156, 33157, 33158, 33159, 33160, 33161, 33162, 33163, 33164, 33165, 33166, 33167, 33168, 33169, 33170, 33172, 33173, 33174, 33175, 33176, 33177, 33178, 33179, 33180, 33181, 33182, 33183, 33184, 33185, 33186, 33187, 33188, 33189, 33190, 33193, 33194, 33195, 33196, 33197, 33199, 33222, 33231, 33233, 33234, 33238, 33239, 33242, 33243, 33245, 33247, 33255, 33256, 33257, 33261, 33265, 33266, 33269, 33280, 33283, 33296, 33299, 34141);
			break;
		case "mflpalmbeach": $county="PALMBEACH";
							$total_zip = array(33401, 33402, 33403, 33404, 33405, 33406, 33407, 33408, 33409, 33410, 33411, 33412, 33413, 33414, 33415, 33416, 33417, 33418, 33419, 33420, 33421, 33422, 33424, 33425, 33426, 33427, 33428, 33429, 33430, 33431, 33432, 33433, 33434, 33435, 33436, 33437, 33438, 33439, 33440, 33444, 33445, 33446, 33447, 33448, 33449, 33454, 33458, 33459, 33460, 33461, 33462, 33463, 33464, 33465, 33466, 33467, 33468, 33469, 33470, 33472, 33473, 33474, 33476, 33477, 33478, 33480, 33481, 33482, 33483, 33484, 33486, 33487, 33488, 33493, 33496, 33497, 33498, 33499);
			break;
		default: die("Seleccione base de datos");
			break;
	}
	$year_r=$ret[1]; // a�o exacto YYYY
	$month_r=$ret[2]; // mes (1-12)
	$zip=$ret[3]; // 1=Todos ZIP  ; 2=Sin ZIP  ; 3=Ambos
	$proptype_opc=$ret[4]; // RE1, RE2, ambos
	//echo "<br>proptype_opc: ".$proptype_opc;
	//echo $nom_bd." ".$year=$ret[1]." ".$month=$ret[2]." ".$zip." ".$proptype_opc;
	//require("sel_fechas");
	//$fechaNow = valor en la base de datos del mes a calcular
	//$fechaCompar = valor en la base de datos del mes --> 
	//anterior al que se quiere calcular, sirve para hacer los DIFF
	
	echo "<br> ---BASE DE DATOS: ".$nom_bd." ".$month_r."-".$year_r;
	
	
	if($proptype_opc=="re1" || $proptype_opc=="ambos")
	{
		
		if($zip==1 || $zip==3)// hacer calculos para todos los zips o ambos
		{
			echo "<br> --Calculo con ZIP para RE1-- <br>";
			include("calculo_RE1zip.php");
		}
		if($zip==2 || $zip==3)// hacer calculos sin zip o ambos
		{
			echo "<br> --Calculo sin ZIP para RE1-- <br>";
			include("calculo_RE1.php");
		}
	}
	
	if($proptype_opc=="re2" || $proptype_opc=="ambos")
	{
		if($zip==1 || $zip==3)// hacer calculos para todos los zips o ambos
		{
			echo "<br> --Calculo con ZIP para RE2-- <br>";
			include("calculo_RE2zip.php");
		}
		if($zip==2 || $zip==3)// hacer calculos sin zip o ambos
		{
			echo "<br> --Calculo sin ZIP para RE2-- <br>";
			include("calculo_RE2.php");
		}
	}
	
	
	

?>