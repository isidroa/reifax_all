function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

function checkSeleccion(field, final) 
{
	if (field[0].checked == true) 
	{
		for (i = 2; i < field.length; i++)
			field[i].checked = true;
	}
	else if(field[1].checked == true)
	{
		for (i = 2; i < final; i++)
			field[i].checked = true;
	}else{
		for (i = 0; i < field.length; i++)
			field[i].checked = false;
	}
}

function expandir(fila,imagen)
{
	var texto;
	if(fila=="filResultados")texto=" Resultados";
	else texto=" listados de Php a ejecutar";
	
	
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="includes/up.png";
		document.getElementById(imagen).alt="Ocultar "+texto;
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="includes/down.png";
		document.getElementById(imagen).alt="Muestra "+texto;
	}

}

function limpiarchecks()
{
	var field,i;
	
	field =document.formulario.ckbResTbl;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbArrFec;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
		
	field =document.formulario.ckbArrDat;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbEliDup;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbPiMLN;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbRTM;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
	
	field =document.formulario.ckbVerfDatMsq;
	for (i=0; i<field.length;i++)
		field[i].checked=false;
}

function buscarParametros()
{
	var field,i;
	var ret="",datnew="";
	var ruta,texto;
	var cont=0;
	var swbd=0;
	var cad;

	
		field =document.formulario.ckbResTbl;
		ruta="Parcelid/";
		texto="Establecer Parcelid ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
		
		field =document.formulario.ckbArrFec;
		ruta="ArrFecha/";
		texto="Arreglar Fecha ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
		
		field =document.formulario.ckbArrDat;
		ruta="ArrData/";
		texto="Arreglar Campos en General ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
				
		field =document.formulario.ckbEliDup;
		ruta="Duplicado/";
		texto="Eliminar Duplicado ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
		
		field =document.formulario.ckbPiMLN;
		ruta="Parcelid/";
		texto="Parcelid por Mlnumber ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
		
		field =document.formulario.ckbRTM;
		ruta="rtmaster/";
		texto="Creaci�n de Rtmaster ";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
			{
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
				datnew="datanueva=csvfiles&copypaste=0";
			}
		}
		
		field =document.formulario.ckbVerfDatMsq;
		ruta="backup/";
		texto="Generar CSV de Access1 para DataWork";
		for (i=0; i<field.length;i++)
		{
			if(field[i].checked==true && (field[i].id!="*" && field[i].id!="**"))
				ret+='"php"=>"'+field[i].id+'","ruta"=>"'+ruta+'","texto"=>"'+texto+'"^';
		}
	
	if(ret=="") return "";
	else return datnew+"&parametros="+ret;
}

function ejecutarPHP()
{	
	var i;
	var bd=document.getElementById("cbd").value;
	var parametros="";
	var controls,ret;
	var htmlTag=new Array();
	if(bd==null || bd=="0" || bd=="")
		alert("Debes seleccionar Base de Datos");
	else
	{
		
		parametros=buscarParametros();
		if(parametros==null || parametros=="")
			alert("Debes seleccionar al menos una opcion a Ejecutar");
		else
		{

			document.getElementById("divResultados").innerHTML='';
			document.getElementById("imgfilResultados").src="includes/up.png";
			document.getElementById("imgfilResultados").alt="Ocultar Resultados";
			document.getElementById("reloj").style.visibility="visible";
			controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=true;

			var ajax=nuevoAjax();
			ajax.open("POST", "mantenimiento_respuesta.php", true);
			ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajax.send("bd="+bd+"&"+parametros);
			ajax.onreadystatechange=function()
			{
				if (ajax.readyState==4)	
				{
					htmlTag.push(ajax.responseText);
					document.getElementById("divResultados").innerHTML=htmlTag.join('');
					document.getElementById("filResultados").style.display="inline";
					document.getElementById("reloj").style.visibility="hidden";
					controls=document.all.tags("input");for(var i=0;i<controls.length;i++)controls[i].disabled=false;
					limpiarchecks();
					//habilitacion(false);
				}
			}
		}//Al Menos un ckechboxes seleccionado
	}//IF Base de Datos
}

