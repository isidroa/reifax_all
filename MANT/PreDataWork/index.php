<html>
<head>
<title>Pre-Mantenimiento de Tablas</title>
<script type="text/javascript" src="includes/mantenimiento.js"></script>
<link href="includes/mantenimiento.css" rel="stylesheet" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body  > 
<form name=formulario>
<div id="opciones" >
	<table class="tblopciones" cellspacing="0" >
	<tbody>
		<tr class="titulo">
			<th colspan="4">PRE MANTENIMIENTO DE TABLAS</th>
		</tr>
		<tr class="subtitulo">
			<th width="15" >&nbsp;</th>
			<th width="15" >&nbsp;</th>
			<th width="480" >TAREAS</th>
			<th width="190" >BD: 
				<select name="cbd">
					<option value="0" selected>Seleccione</option>
					<option value="MFLBROWARD" >MFLBROWARD</option>
					<option value="MFLDADE"    >MFLDADE</option>
					<option value="MFLPALMBEACH">MFLPALMBEACH</option>
				</select>
			</th>
		</tr>

<!--Establecer Parcelid por Folio -->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titResTbl','imgResTbl'); return false;">
    	<img id="imgResTbl" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Establecer Parcelid </th>
	<th width="190" ><input type="checkbox" name="ckbResTbl"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbResTbl, 0)"> 
	  Todos  <input type="checkbox" name="ckbResTbl"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbResTbl, 3)"> 
	  Semanales</th>
</tr>
  <tr  id="titResTbl" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titResTbl=array(
					array("tit"=>"Mlsresidential","value"=>"parcelidMlsresidential.php"),
				);
	for($i=0;$i<count($titResTbl);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titResTbl[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbResTbl" id="<?php echo $titResTbl[$i]["value"] ?>" value="<?php echo ($i+1)  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
  </tr>


<!--arreglar fechas-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titArrFec','imgArrFec'); return false;">
    	<img id="imgArrFec" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Arreglar Campos Fechas</th>
	<th width="190" ><input type="checkbox" name="ckbArrFec"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbArrFec, 0)"> 
	  Todos  <input type="checkbox" name="ckbArrFec"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbArrFec, 4)"> 
	  Semanales</th>
</tr>
<tr  id="titArrFec" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titArrFec=array(
					array("tit"=>"Mlsresidential","value"=>"ArreglaFechaMlsResidential.php"),
				);
	for($i=0;$i<count($titArrFec);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titArrFec[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbArrFec"  id="<?php echo $titArrFec[$i]["value"] ?>" value="<?php echo ($i+1)  ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
</tr>


<!--arreglar Data-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titArrDat','imgArrDat'); return false;">
    	<img id="imgArrDat" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Arreglar Campos en General</th>
	<th width="190" ><input type="checkbox" name="ckbArrDat"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbArrDat, 0)"> 
	  Todos  <input type="checkbox" name="ckbArrDat"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbArrDat, 14)"> 
	  Semanales</th>
</tr>
<tr  id="titArrDat" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titArrDat=array(
					array("tit"=>"ClosingDT menor de 8 (mlsresidential)","value"=>"mlsClosingDTmenordeOcho.php"),
					array("tit"=>"Arreglo del PropType","value"=>"sincronizaPropType.php")
				);
	for($i=0;$i<count($titArrDat);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titArrDat[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbArrDat"  id="<?php echo $titArrDat[$i]["value"] ?>" value="<?php echo ($i+1)  ?>" >
			  Ejecutar</td>
		</tr>
<?
	}
?>		
	</table>	
  </td>
</tr>

 <!--Eliminar diplicados de Parcelid -->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titEliDup','imgEliDup'); return false;">
    	<img id="imgEliDup" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Eliminar Duplicados</th>
	<th width="190" ><input type="checkbox" name="ckbEliDup"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbEliDup, 0)"> 
	  Todos  <input type="checkbox" name="ckbEliDup"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbEliDup, 3)"> 
	  Semanales</th>
</tr>
  <tr  id="titEliDup" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titEliDup=array(
					array("tit"=>"Mlsresidential","value"=>"EliminarDuplicadoMlsresidential.php")
				);
	for($i=0;$i<count($titEliDup);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titEliDup[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbEliDup" id="<?php echo $titEliDup[$i]["value"] ?>" value="<?php echo ($i+1)  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>				
	</table>	
  </td>
  </tr>

 <!--Asignar Parcelid el Mlnumber -->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titPiMLN','imgPiMLN'); return false;">
    	<img id="imgPiMLN" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Parcelid por Mlnumber</th>
	<th width="190" ><input type="checkbox" name="ckbPiMLN"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbPiMLN, 0)"> 
	  Todos  <input type="checkbox" name="ckbPiMLN"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbPiMLN, 3)"> 
	  Semanales</th>
</tr>
  <tr  id="titPiMLN" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titPiMLN=array(
					array("tit"=>"Mlsresidential","value"=>"parcelidMlnumberMlsresidential.php")
				);
	for($i=0;$i<count($titPiMLN);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titPiMLN[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbPiMLN" id="<?php echo $titPiMLN[$i]["value"] ?>" value="<?php echo ($i+1)  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>				
	</table>	
  </td>
  </tr>
  
 <!--Creacion de Rtmaster-->		
<tr class="titulomedio">
	<th width="15">
		<a href="#" onClick="javascript:expandir('titRTM','imgRTM'); return false;">
    	<img id="imgRTM" src="includes/down.png" border="0" ></a>
	</th>
	<th colspan="2">Creaci�n de Rtmaster</th>
	<th width="190" ><input type="checkbox" name="ckbRTM"  id="*" value="*" onClick="checkSeleccion(document.formulario.ckbRTM, 0)"> 
	  Todos  <input type="checkbox" name="ckbRTM"  id="**" value="**" onClick="checkSeleccion(document.formulario.ckbRTM, 3)"> 
	  Semanales</th>
</tr>
  <tr  id="titRTM" style="display: none">
  <td colspan="4">
   	 <table class="tblopcionesfilas" align="center">
<? 
	$titRTM=array(
					array("tit"=>"DRtmaster","value"=>"creacionRtmaster.php"),
					array("tit"=>"Acomodar ClosingDT","value"=>"closingdtRtmaster.php"),
					array("tit"=>"Acomodar SalePrice","value"=>"salepriceRtmaster.php")
				);
	for($i=0;$i<count($titRTM);$i++)
	{
?>	 
		<tr >
			<td width="15"></td>
			<td width="490" ><?php echo $titRTM[$i]["tit"] ?></td>
			<td width="180" ><input type="checkbox" name="ckbRTM" id="<?php echo $titRTM[$i]["value"] ?>" value="<?php echo ($i+1)  ?>">
			  Ejecutar</td>
		</tr>
<?
	}
?>				
	</table>	
  </td>
  </tr>
  
<!--Boton Ejecutar-->		
		<tr >
			<td colspan="3" >
			  <div align="center"><INPUT class="botonsml" id="botEjecutar" type="button" value="Ejecutar" onClick="javascript:ejecutarPHP(); return false;" ></div>
			</td>
			<td >
			<div align="center"><a href="#" onClick="javascript:limpiarchecks(); return false;"> Limpiar Todos </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
</div>
<br><br>
<div id="reloj"><img src="includes/ac.gif">Ejecutando PHP's!...</div><!--Reloj-->

<div id="resultados">
	<table class="tblresultados">
	<tbody>
		<tr class="titulomostrar">
			<th width="15" align="left">
				<a href="#" onClick="javascript:expandir('filResultados','imgfilResultados'); return false;">
				<img id="imgfilResultados" src="includes/down.png" border="0" ></a>
			</th>
			<th width="685">Resultados</th>
		</tr>
		<tr  id="filResultados" style="display: none">
			<td colspan="2">
				<table >
				<tbody>
					<tr>
						<td > 
							<div id="divResultados" ></div>
						</td>
					</tr>
				</tbody>
				</table>
			</td>	
	    </tr>
	</tbody>
	</table>
</div>
</form>
</body>
</html>
