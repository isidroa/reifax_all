Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
	var procesos='',idReg='';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=pote',
		fields: [
			{name: 'poteID', type: 'int'},
			'textopote',
			'procesos',
			'orden'
		]
	});
	
	var storePro = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=poteProces',
		fields: [
			{name: 'procID', type: 'int'},
			'ejecucion',
			'debugeo',
			'proceso',
			'descr',
			'php',
			'consulta',
			'tipo_proceso',
			'modificaciones'
		]
	});
	
	var storeProAdd = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=poteProces',
		fields: [
			{name: 'procID', type: 'int'},
			'ejecucion',
			'debugeo',
			'proceso',
			'descr',
			'php',
			'consulta',
			'tipo_proceso'
		],
		sortInfo: {field: "debugeo", direction: "ASC"}
	});
	var storeHist = new Ext.data.JsonStore({
			root: 'results',
			url: 'includes/php/grid_data.php?tipo=bitacora&seccion=pote',
			fields: [
				{name: 'idbitacora', type: 'int'},
				'id_registro',
				'condado',
				'tabla',
				'campo',
				'old',
				'new',
				'usuario',
				'fecha',
				'hora',
				'descripcion',
				'accion'
			],
			listeners: {
				beforeload: {
					fn: function(store,opt){
						opt.params.idReg=idReg;
						idReg='';
					}
				}
			}
		});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                },
			{
				 id: 'history_button',
                 tooltip: 'Click to View History',
				 iconCls:'icon',
				 icon: 'includes/img/History.png',
                 handler: doHist 
                }
			]
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "pote", 
					key: 'poteID',
					keyID: oGrid_Event.record.data.poteID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.poteID;
		}
		selected+=')';
		
		if(selected=='()'){
			Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return 0;
		}
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "pote", 
					key: 'poteID',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminaci�n');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		var gridProAdd = new Ext.grid.GridPanel({
			store: storeProAdd,
			iconCls: 'icon',
			columns: [	  
				{id:'procID',header: "Process ID", width: 60, align: 'center', sortable: true, dataIndex: 'procID'},
				{header: 'Execute ID', width: 60, sortable: true, align: 'center', dataIndex: 'ejecucion'},
				{header: 'Debug', width: 150, sortable: true, align: 'center', dataIndex: 'debugeo'},
				{header: 'Name', width: 150, sortable: true, align: 'center', dataIndex: 'proceso'},
				{header: 'Description', width: 250, sortable: true, align: 'center', dataIndex: 'descr'},
				{header: 'PHP', width: 150, sortable: true, align: 'center', dataIndex: 'php'},
				{header: 'Q. Type', width: 60, sortable: true, align: 'left', dataIndex: 'consulta'},
				{header: 'P. Type', width: 60, sortable: true, align: 'left', dataIndex: 'tipo_proceso'}
			],
			sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
			height:200,
			title:'Pote Process'
		});
		gridProAdd.getSelectionModel().on('rowselect', doProcessAdd);
		
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Pote',
			bodyStyle:'padding:5px 5px 0',
			width: 500,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			
			items: [{
					xtype: 'textfield',
					fieldLabel: 'Texto',
					name: 'texto',
					emptyText:'',
					allowBlank:false
				},{
					xtype: 'textfield',
					id:'selectPro',
					fieldLabel: 'Procesos',
					name: 'procesos',
					emptyText:'()',
					readOnly: true,
					value : '()',
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'pote'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'column',
			width       : 500,
			height      : 400,
			modal	 	: true,
			plain       : true,
			items		: [simple,gridProAdd],

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});
		
		storeProAdd.load();
        win.show(button);	
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}
	
	////////////////////Bitacora////////////////////////////////////////////////////////////////
	
	function doEditHist(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "bitacora", 
					key: 'idbitacora',
					keyID: oGrid_Event.record.data.idbitacora,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					storeHist.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					storeHist.commitChanges();
				}                                     
			 }
		);  
	}
	
	function doHist(){
		var button = Ext.get('add_butt');
		var xg = Ext.grid;
		var selec = grid.selModel.getSelections();
		
		var pagingBarHist = new Ext.PagingToolbar({
			pageSize: 50,
			store: storeHist,
			displayInfo: true,
			displayMsg: '<b>Total: {2}</b>',
			emptyMsg: "No topics to display"        
        });	
		
		var expander = new Ext.ux.grid.RowExpander({
			tpl : new Ext.Template(
				'<div style="width:200px;background:#999999;color:#fff;padding:5px;"><p><b>Descripcion:</b> {descripcion}</p></div>'
			)
		});

		
	var gridHist = new Ext.grid.EditorGridPanel({
		store: storeHist,
		iconCls: 'icon',
		columns: [	
			expander,
			{header: 'Descripcion', width: 350, sortable: true, align: 'left',dataIndex:'descripcion',editor: new Ext.form.TextArea({allowBlank: false})}, 
			{header: 'Fecha', width: 85, sortable: true, align: 'left', dataIndex:'fecha'},
			{header: 'Hora', width: 85, sortable: true, align: 'left', dataIndex:'hora'},
			{header: 'Usuario', width: 75, sortable: true, align: 'center', dataIndex:'usuario'},
			{header: 'Campo', width: 75, sortable: true, align: 'center', dataIndex:'campo'},
			{header: 'Valor Anterior', width: 85, sortable: true, align: 'center',dataIndex:'old'},
			{header: 'Valor Nuevo', width: 85, sortable: true, align: 'center',dataIndex:'new'},
			{header: 'Tabla', width: 85, sortable: true, align: 'center',dataIndex:'tabla'},
			{header: 'Id Registro', width: 85, sortable: true, align: 'center',dataIndex:'id_registro'},
			{header: 'Condado', width: 85, sortable: true, align: 'center',dataIndex:'condado'},
			{header: 'Accion', width: 85, sortable: true, align: 'left',dataIndex:'accion'},
			{id:'ID Bitacora',header: "Bitacora ID", width: 80, align: 'center', sortable: true,dataIndex:'idbitacora'}
			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:450,
		rowHeight: 34,
		width: screen.width,
		frame:true,
		title:'Bitacora',
		loadMask:true,
        plugins: expander,
        collapsible: true,
		tbar: pagingBarHist 
	});
	
	gridHist.on('render',function(){
		if(selec.length==0){
				storeHist.load({params:{start:0, limit:200}});
		}else{
				idReg = selec[0].json.poteID;
				storeHist.load({params:{start:0, limit:200,idReg:idReg}});
		}
	});
	gridHist.addListener('afteredit', doEditHist);
	
		
		winHist = new Ext.Window({
			layout      : 'fit',
			autoWidth: true,
			height      : 450,
			modal	 	: true,
			plain       : true,
			items		: gridHist,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					winHist.close();
				}
			}]
		});
        
        winHist.show(button);
	}
	
	////////////////////Fin Bitacora////////////////////////////////////////////////////////////////
	
	
	function doProcess(sm, rowIdx, r){
		procesos=r.data.procesos;
		storePro.load({params:{procesos:procesos}});
	}
	
	function doProcessAdd(sm, rowIdx, r){
		var valor=r.data.ejecucion;
		var text=Ext.getCmp('selectPro');
		var cont=text.getValue();

		cont=cont.replace('(','');
		cont=cont.replace(')','');

		if(cont.indexOf(valor)==-1){
			if(cont!='')
				Ext.getCmp('selectPro').setValue('('+cont+','+valor+')');
			else
				Ext.getCmp('selectPro').setValue('('+cont+valor+')');
		}else{
			var contAux=cont.split(',');
			cont='(';
		 	var i=0;
			for(var val in contAux){
				if(contAux[val]!=valor && typeof(contAux[val])=='string'){
					if(i>0) cont+=',';
					cont+=contAux[val];
					i++;
				}
			}
			cont+=')';
			Ext.getCmp('selectPro').setValue(cont);
		}
		
	}
	
	function viewPHP(sm, rowIdx, r){
		var php=r.data.php;
		var procID=r.data.procID;
		var modificaciones=r.data.modificaciones;
		
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Reading PHP...',
				url: 'includes/php/grid_data.php', 
				method: 'GET', 
				params: { 
					tipo: "validaciones",
					oper: 'php',
					php: php					
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminaci�n');
				},
				success:function(response,options){
					var rest=response.responseText;
					var arres=rest.split('^');
					if(arres[0]!='sucess')
						Ext.MessageBox.alert('Warning',arres[1]);
					
					Ext.getCmp('PHP_show').setValue(arres[1]);
				}                                
			 }
		); 
		
		var simple = new Ext.FormPanel({
			frame:true,
			title: 'PHP View',
			bodyStyle:'padding:5px 5px 0',
			defaults: {width: 630},

			items: [{
					fieldLabel: 'PHP',
					xtype: 'textarea',
					name: 'PHP_show',
					id: 'PHP_show',
					height:450,
					readOnly: false
				}
			]
		});
		
		win = new Ext.Window({
                layout      : 'fit',
                width       : 800,
                height      : 500,
				modal	 	: true,
                plain       : true,
                items		: simple,

                buttons: [
				{
                    text     : 'Save',
                    handler  : function(){
						Ext.MessageBox.confirm('Confirm','Esta seguro que desea Cambiar este PHP',function(btn){
							if(btn=='yes'){
								//submit to server
								Ext.Ajax.request(
									{  
										waitMsg: 'Editing PHP...',
										url: 'includes/php/grid_edit.php', 
										method: 'POST', 
										params: { 
											tipo: "editProcess",
											oper: 'php',
											php: php,
											content:Ext.getCmp('PHP_show').getValue()		
										},
										
										failure:function(response,options){
											Ext.MessageBox.alert('Warning','No se pudo hacer la edici�n');
										},
										success:function(response,options){
											Ext.MessageBox.alert('Confirm','Se realizaron los cambios con exito');
											Ext.MessageBox.show({
											   title: 'Bitacora',
											   msg: 'Ingresa la descripci�n para la Bitacora:',
											   width:400,
											   buttons: Ext.MessageBox.OK,
											   multiline: true,
											   fn: function(btn, text){
													Ext.Ajax.request(
														{  
															waitMsg: 'Salvando Descripci�n...',
															url: 'includes/php/grid_edit.php', 
															method: 'POST', 
															params: { 
																tipo: "poteProces", 
																key: 'procID',
																keyID: procID,
																field: 'modificaciones',
																value: text,
																originalValue: modificaciones
															},
															
															failure:function(response,options){
																Ext.MessageBox.alert('Warning','No se pudo actualizar la Bitacora');
															},
															success:function(response,options){
																Ext.MessageBox.alert('Confirm','Se ingreso la Descripci�n');
															}                                
														 }
													);
													//fin funcion
											   }
											});
										}                                
									 }
								); 
								
							}
						});
					}
                },{
                    text     : 'Close',
                    handler  : function(){
						win.close();
					}
                }]
        });
		win.show();
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: '../LIB/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			{id:'poteID',header: "Pote ID", width: 80, align: 'center', sortable: true, dataIndex: 'poteID'},
			{header: 'Name', width: 250, sortable: true, align: 'center', dataIndex: 'textopote',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Order', width: 85, sortable: true, align: 'center', dataIndex: 'orden',hidden:true},
			{header: 'Process', width: 600, sortable: true, align: 'center', dataIndex: 'procesos',hidden:false,editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:300,
		frame:true,
		title:'Pote',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	var gridPro = new Ext.grid.GridPanel({
		store: storePro,
		iconCls: 'icon',
		columns: [	  
			{id:'procID',header: "Process ID", width: 60, align: 'center', sortable: true, dataIndex: 'procID'},
			{header: 'Execute ID', width: 60, sortable: true, align: 'center', dataIndex: 'ejecucion'},
			{header: 'Debug', width: 150, sortable: true, align: 'center', dataIndex: 'debugeo'},
			{header: 'Name', width: 150, sortable: true, align: 'center', dataIndex: 'proceso'},
			{header: 'Description', width: 250, sortable: true, align: 'center', dataIndex: 'descr'},
			{header: 'Modificaciones', width: 250, sortable: true, align: 'center', dataIndex: 'modificaciones',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'PHP', width: 150, sortable: true, align: 'center', dataIndex: 'php'},
			{header: 'Q. Type', width: 60, sortable: true, align: 'left', dataIndex: 'consulta'},
			{header: 'P. Type', width: 60, sortable: true, align: 'left', dataIndex: 'tipo_proceso'}
		],
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		height:200,
		width: screen.width,
		//autoHeight: true,
		frame:true,
		title:'Process of Selected Pote',
		loadMask:true 
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: [grid,gridPro]
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
	grid.getSelectionModel().on('rowselect', doProcess);
	gridPro.getSelectionModel().on('rowselect', viewPHP);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});