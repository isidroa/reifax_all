Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,debug='',maxProcess=1,idReg='';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=valiProces',
		fields: [
			{name: 'procID', type: 'int'},
			'ejecucion',
			'debugeo',
			'proceso',
			'descr',
			'php',
			'consulta',
			'tipo_proceso',
			'modificaciones'
		],
		sortInfo: {field: "debugeo", direction: "ASC"},
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.debug=debug;
				}
			}
        }
	});
	
	var storeHist = new Ext.data.JsonStore({
			root: 'results',
			url: 'includes/php/grid_data.php?tipo=bitacora&seccion=procesos&tipoProc=validacion',
			fields: [
				{name: 'idbitacora', type: 'int'},
				'id_registro',
				'condado',
				'tabla',
				'campo',
				'old',
				'new',
				'usuario',
				'fecha',
				'hora',
				'descripcion',
				'accion'
			],
			listeners: {
				beforeload: {
					fn: function(store,opt){
						opt.params.idReg=idReg;
						idReg='';
					}
				}
			}
		});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                },
			{
				 id: 'php_butt',
                 tooltip: 'Click to View PHP',
				 iconCls:'icon',
				 icon: 'includes/img/historico.png',
                 handler: viewPHP 
                },
			{
				 id: 'history_button',
                 tooltip: 'Click to View History',
				 iconCls:'icon',
				 icon: 'includes/img/History.png',
                 handler: doHist 
                }
			]
    });
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.procID;
		}
		selected+=')';
		
		if(selected=='()'){
			Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return 0;
		}
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "valiProces", 
					key: 'procID',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminaci�n');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		///maxProcess////////////////////
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_data.php', 
				method: 'GET', 
				params: { 
					tipo: "maxProcess" 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se obtubo el maxProcess');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					maxProcess=rest.maxProcess;
					Ext.getCmp('ejecID').setValue(maxProcess);
				}                                
			 }
		);
		/////////////////////////////////
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Validate Process',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			
			items: [{
					fieldLabel: 'Ejecution ID*',
					id: 'ejecID',
					xtype: 'textfield',
					name: 'id',
					hiddenName: 'id',
					value: maxProcess,
					allowBlank:false
				},{
					fieldLabel: 'Process Name*',
					xtype: 'textfield',
					name: 'name',
					hiddenName: 'name',
					allowBlank:false
				},{
					fieldLabel: 'Process Desc.*',
					xtype: 'textfield',
					name: 'desc',
					hiddenName: 'desc',
					allowBlank:false
				},{
					xtype: 'combo',
					name: 'type1',
					hiddenName: 'que',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['DELETE', 'Delete'],
							['INSERT', 'Insert'],
							['TRUNCATE', 'Truncate'],
							['UPDATE', 'Update'],
							['OTRO', 'Otro']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'Type Q.*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select is Type Q...',
					selectOnFocus:true,
					allowBlank:false
				},{
					fieldLabel: 'Debug Desc.*',
					xtype: 'textfield',
					name: 'deb',
					hiddenName: 'deb',
					allowBlank:false
				},{
					xtype: 'combo',
					name: 'type2',
					hiddenName: 'typ',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['D', 'Diario'],
							['E', 'Especial'],
							['S', 'Semanal'],
							['M', 'Mensual'],
							['T', 'Todos']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'Type P.*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select is Type P...',
					selectOnFocus:true,
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'valiProces'
				},{
					xtype: 'hidden',
					name: 'fuente',
					hiddenName: 'fuente',
					value: 'VALI'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});
        
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}
	
	////////////////////Bitacora////////////////////////////////////////////////////////////////
	
	function doEditHist(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "bitacora", 
					key: 'idbitacora',
					keyID: oGrid_Event.record.data.idbitacora,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					storeHist.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					storeHist.commitChanges();
				}                                     
			 }
		);  
	}
	
	function doHist(){
		var button = Ext.get('add_butt');
		var xg = Ext.grid;
		var selec = grid.selModel.getSelections();
		
		var pagingBarHist = new Ext.PagingToolbar({
			pageSize: 50,
			store: storeHist,
			displayInfo: true,
			displayMsg: '<b>Total: {2}</b>',
			emptyMsg: "No topics to display"        
        });	
		
		var expander = new Ext.ux.grid.RowExpander({
			tpl : new Ext.Template(
				'<div style="width:200px;background:#999999;color:#fff;padding:5px;"><p><b>Descripcion:</b> {descripcion}</p></div>'
			)
		});

		
	var gridHist = new Ext.grid.EditorGridPanel({
		store: storeHist,
		iconCls: 'icon',
		columns: [	
			expander,
			{header: 'Descripcion', width: 350, sortable: true, align: 'left',dataIndex:'descripcion',editor: new Ext.form.TextArea({allowBlank: false})}, 
			{header: 'Fecha', width: 85, sortable: true, align: 'left', dataIndex:'fecha'},
			{header: 'Hora', width: 85, sortable: true, align: 'left', dataIndex:'hora'},
			{header: 'Usuario', width: 75, sortable: true, align: 'center', dataIndex:'usuario'},
			{header: 'Campo', width: 75, sortable: true, align: 'center', dataIndex:'campo'},
			{header: 'Valor Anterior', width: 85, sortable: true, align: 'center',dataIndex:'old'},
			{header: 'Valor Nuevo', width: 85, sortable: true, align: 'center',dataIndex:'new'},
			{header: 'Tabla', width: 85, sortable: true, align: 'center',dataIndex:'tabla'},
			{header: 'Id Registro', width: 85, sortable: true, align: 'center',dataIndex:'id_registro'},
			{header: 'Condado', width: 85, sortable: true, align: 'center',dataIndex:'condado'},
			{header: 'Accion', width: 85, sortable: true, align: 'left',dataIndex:'accion'},
			{id:'ID Bitacora',header: "Bitacora ID", width: 80, align: 'center', sortable: true,dataIndex:'idbitacora'}
			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:450,
		rowHeight: 34,
		width: screen.width,
		frame:true,
		title:'Bitacora',
		loadMask:true,
        plugins: expander,
        collapsible: true,
		tbar: pagingBarHist 
	});
	
	gridHist.on('render',function(){
		if(selec.length==0){
				storeHist.load({params:{start:0, limit:200}});
		}else{
				idReg = selec[0].json.procID;
				storeHist.load({params:{start:0, limit:200,idReg:idReg}});
		}
	});
	gridHist.addListener('afteredit', doEditHist);
	
		
		winHist = new Ext.Window({
			layout      : 'fit',
			autoWidth: true,
			height      : 450,
			modal	 	: true,
			plain       : true,
			items		: gridHist,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					winHist.close();
				}
			}]
		});
        
        winHist.show(button);
	}
	
	////////////////////Fin Bitacora////////////////////////////////////////////////////////////////

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "valiProces", 
					key: 'procID',
					keyID: oGrid_Event.record.data.procID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doFilter(combo,record,index){
		debug = combo.getValue();
		store.load({params:{start:0, limit:200, debug:debug}});
	}
	
	function viewPHP(){
		var button = Ext.get('php_butt');
		var selec = grid.selModel.getSelections();
		if(selec.length==0){
			Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for view php to work.');
			return 0;
		}
		var php=selec[0].json.php;
		var procID=selec[0].json.procID;
		var modificaciones=selec[0].json.modificaciones;
		
		
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Reading PHP...',
				url: 'includes/php/grid_data.php', 
				method: 'GET', 
				params: { 
					tipo: "validaciones",
					oper: 'php',
					php: php					
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminaci�n');
				},
				success:function(response,options){
					var rest=response.responseText;
					var arres=rest.split('^');
					if(arres[0]!='sucess')
						Ext.MessageBox.alert('Warning',arres[1]);
					
					Ext.getCmp('PHP_show').setValue(arres[1]);
				}                                
			 }
		); 
		
		var simple = new Ext.FormPanel({
			frame:true,
			title: 'PHP View',
			bodyStyle:'padding:5px 5px 0',
			defaults: {width: 630},

			items: [{
					fieldLabel: 'PHP',
					xtype: 'textarea',
					name: 'PHP_show',
					id: 'PHP_show',
					height:450,
					readOnly: false
				}
			]
		});
		
		win = new Ext.Window({
                layout      : 'fit',
                width       : 800,
                height      : 500,
				modal	 	: true,
                plain       : true,
                items		: simple,

                buttons: [
				{
                    text     : 'Save',
                    handler  : function(){
						Ext.MessageBox.confirm('Confirm','Esta seguro que desea Cambiar este PHP',function(btn){
							if(btn=='yes'){
								//submit to server
								Ext.Ajax.request(
									{  
										waitMsg: 'Editing PHP...',
										url: 'includes/php/grid_edit.php', 
										method: 'POST', 
										params: { 
											tipo: "editProcess",
											oper: 'php',
											php: php,
											content:Ext.getCmp('PHP_show').getValue()		
										},
										
										failure:function(response,options){
											Ext.MessageBox.alert('Warning','No se pudo hacer la edici�n');
										},
										success:function(response,options){
											Ext.MessageBox.alert('Confirm','Se realizaron los cambios con exito');
											Ext.MessageBox.show({
											   title: 'Bitacora',
											   msg: 'Ingresa la descripci�n para la Bitacora:',
											   width:400,
											   buttons: Ext.MessageBox.OK,
											   multiline: true,
											   fn: function(btn, text){
													Ext.Ajax.request(
														{  
															waitMsg: 'Salvando Descripci�n...',
															url: 'includes/php/grid_edit.php', 
															method: 'POST', 
															params: { 
																tipo: "valiProces", 
																key: 'procID',
																keyID: procID,
																field: 'modificaciones',
																value: text,
																originalValue: modificaciones
															},
															
															failure:function(response,options){
																Ext.MessageBox.alert('Warning','No se pudo actualizar la Bitacora');
															},
															success:function(response,options){
																Ext.MessageBox.alert('Confirm','Se ingreso la Descripci�n');
															}                                
														 }
													);
													//fin funcion
											   }
											});
										}                                
									 }
								); 
								
							}
						});
					}
                },{
                    text     : 'Close',
                    handler  : function(){
						win.close();
					}
                }]
        });
		win.show(button);
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		columns: [	  
			{id:'procID',header: "Process ID", width: 60, align: 'center', sortable: true, dataIndex: 'procID'},
			{header: 'Execute ID', width: 60, sortable: true, align: 'center', dataIndex: 'ejecucion'},
			{header: 'Debug', width: 150, sortable: true, align: 'center', dataIndex: 'debugeo',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Name', width: 150, sortable: true, align: 'center', dataIndex: 'proceso',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Description', width: 250, sortable: true, align: 'center', dataIndex: 'descr',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Modificaciones', width: 250, sortable: true, align: 'center', dataIndex: 'modificaciones',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'PHP', width: 150, sortable: true, align: 'center', dataIndex: 'php'},
			{header: 'Q. Type', width: 60, sortable: true, align: 'left', dataIndex: 'consulta',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'P. Type', width: 60, sortable: true, align: 'left', dataIndex: 'tipo_proceso',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		frame:true,
		title:'Validate Process',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 id: 'tblDest',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'tabla'],
		 			data : Ext.combos_selec.valiDebug
		   		 }),
				 editable: false,
				 displayField:'tabla',
				 valueField: 'id',
				 name: 'tblDest',
				 hiddenName: 'debug',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a Debug...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});