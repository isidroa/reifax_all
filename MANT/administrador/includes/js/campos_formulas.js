Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=campos_formulas',
		fields: [
			{name: 'idfc', type: 'int'},
			'campodest',
			{name: 'idfor', type: 'int'},
			'tipo'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
            {
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                }
		]});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';

		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.idfc;
		}
		selected+=')';

		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "campos_formulas", 
					key: 'idfc',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Formula',
			bodyStyle:'padding:5px 5px 0',
			width: 500,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			layout: 'table',
			layoutConfig: {
				columns: 2
			},
			
			items: [{xtype: 'label',text: 'Campo Destino*:',style: 'font-size:10px;',width:70},{
					fieldLabel: 'Campo Destino*',
					xtype: 'textfield',
					name: 'campodest',
					allowBlank:false
				},{xtype: 'label',text: 'Formula*:',style: 'font-size:10px;',width:70},{
					xtype: 'combo',
					 store: new Ext.data.SimpleStore({
						fields: ['idfor', 'nombre'],
						data : Ext.combos_selec.formula
					 }),
					 editable: false,
					 fieldLabel: 'Formula',
					 displayField:'nombre',
					 valueField: 'idfor',
					 name: 'formula1',
					 hiddenName: 'idfor',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Formula...',
					 selectOnFocus:true,
					 allowBlank:false,
					 colspan: 3,
					 width: 400
				},{xtype: 'label',text: 'Tipo*:',style: 'font-size:10px;',width:70},{
					xtype: 'combo',
					triggerAction: 'all',
					lazyRender:true,
					mode: 'local',
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'idTipo',
							'tipo'
						],
						data: [[1, 'G'], [2, 'S']]
					}),
					editable: false,
					valueField: 'tipo',
					hiddenName: 'tipo_campo',
					width:100,
					displayField: 'tipo'
				},{
					xtype: 'hidden',
					name: 'tipo',
					value: 'campos_formulas'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 600,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});

        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "campos_formulas", 
					key: 'idfc',
					keyID: oGrid_Event.record.data.idfc,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}
			 }
		);  
	}; 
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon-grid',
		columns: [	  
			{id:'idfc',header: "Campo ID", width: 80, align: 'center', sortable: true, dataIndex: 'idfc'},
			{header: 'Campo Destino', width: 200, sortable: true, align: 'center', dataIndex: 'campodest',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Formula ID', width: 300, sortable: true, align: 'center', dataIndex: 'idfor',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Tipo', width: 80, sortable: true, align: 'center', dataIndex: 'tipo',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Campos de Formulas',
		loadMask:true,
		
		tbar: pagingBar 

	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});