Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
	var select_bd = '-1';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=estructuras',
		fields: [
			{name: 'structID', type: 'int'},
			'stcounty',
			'procedencia',
			'tblResult',
			'bdResult',
			'csvName',
			'csvExt',
			'descr'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.filter=select_bd;
				}
			}
        }
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                },
			{
				 id: 'copy_butt',
                 tooltip: 'Click to Copy selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/copy.gif',
                 handler: doCopy 
                }
			]
    });
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.structID;
		}
		selected+=')';
		
		if(selected=='()'){
			Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return 0;
		}
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "estructuras", 
					key: 'structID',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doCopy(){
		var button=Ext.get('copy_butt');
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.structID;
		}
		selected+=')';
		
		if(selected=='()'){
			Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the copy to work.');
			return 0;
		}
		
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'Copy Structures',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Coping Rows...',
			
			items: [{
					xtype: 'combo',
					id: 'county1c',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'county'],
						data : Ext.combos_selec.bd
					}),
					editable: false,
					displayField:'county',
					valueField: 'id',
					name: 'county1',
					hiddenName: 'idcounty',
					fieldLabel: 'State/County*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a State/County...',
					selectOnFocus:true,
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'ids',
					hiddenName: 'ids',
					value: selected
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'estructuras'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});

        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		}); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Structures',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			
			items: [{
					xtype: 'combo',
					id: 'county1',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'county'],
						data : Ext.combos_selec.bd
					}),
					editable: false,
					displayField:'county',
					valueField: 'id',
					name: 'county1',
					hiddenName: 'idcounty',
					fieldLabel: 'State/County*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a State/County...',
					selectOnFocus:true,
					allowBlank:false
				},{
					xtype: 'combo',
					id: 'proc1',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'proc'],
						data : Ext.combos_selec.procedencia
					}),
					editable: false,
					displayField:'proc',
					valueField: 'id',
					name: 'proc1',
					hiddenName: 'proid',
					fieldLabel: 'Procedencia*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a Procedencia...',
					selectOnFocus:true,
					allowBlank:false
				},{
					fieldLabel: 'CSV Name*',
					xtype: 'textfield',
					name: 'csvname',
					hiddenName: 'csvname',
					vtype: 'alpha',
					allowBlank:false
				},{
					xtype: 'combo',
					name: 'type1',
					hiddenName: 'csvext',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['CSV', 'CSV'],
							['TXT', 'TXT'],
							['MYSQL', 'MYSQL']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'CSV Ext.*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select is Ext...',
					selectOnFocus:true,
					allowBlank:false
				},{
					fieldLabel: 'Description*',
					xtype: 'textfield',
					name: 'desc',
					hiddenName: 'desc',
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'estructuras'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								//win.close();
								simple.getForm().reset();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "estructuras", 
					key: 'structID',
					keyID: oGrid_Event.record.data.structID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doFilter(combo,record,index){
		select_bd = combo.getValue();
		store.load({params:{start:0, limit:200, filter:select_bd}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		columns: [	  
			{id:'structID',header: "Structures ID", width: 80, align: 'center', sortable: true, dataIndex: 'structID'},
			{header: 'State/County', width: 85, sortable: true, align: 'center', dataIndex: 'stcounty',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Procedencia', width: 85, sortable: true, align: 'center', dataIndex: 'procedencia',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Tbl Result', width: 85, sortable: true, align: 'center', dataIndex: 'tblResult',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'BD Result', width: 85, sortable: true, align: 'center', dataIndex: 'bdResult',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'CSV Name', width: 100, sortable: true, align: 'center', dataIndex: 'csvName',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'CSV Ext', width: 100, sortable: true, align: 'center', dataIndex: 'csvExt',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Description', width: 100, sortable: true, align: 'center', dataIndex: 'descr',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Structures',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 id: 'county2',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'county'],
		 			data : Ext.combos_selec.bd
		   		 }),
				 editable: false,
				 displayField:'county',
				 valueField: 'id',
				 name: 'county2',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State/County...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});