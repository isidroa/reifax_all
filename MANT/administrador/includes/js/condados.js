Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=condados',
		fields: [
			{name: 'idCounty', type: 'int'},
			'State',
			'County',
			'BD',
			'zone',
			{name: 'AvgLat', type: 'float'},
			{name: 'AvgLong', type: 'float'},
			{name: 'MinLat', type: 'float'},
			{name: 'MinLong', type: 'float'},
			{name: 'MaxLat', type: 'float'},
			{name: 'MaxLong', type: 'float'},
			{name: 'longPID', type: 'int'},
			{name: 'is_showed', type: 'int'}
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.filter=select_st;
				}
			}
        },
		sortInfo: {field: "County", direction: "ASC"}
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
            {
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                }
		]});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.idCounty;
		}
		selected+=')';

		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "condados", 
					key: 'idCounty',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New County',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			
			items: [{
					xtype: 'combo',
					id: 'state1',
					editable: false,
					displayField:'state',
					valueField: 'id',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'state'],
						data : Ext.combos_selec.states
					}),
					name: 'state1',
					hiddenName: 'state', 
					fieldLabel: 'State*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a state...',
					selectOnFocus:true,
					allowBlank:false
				},{
					fieldLabel: 'County*',
					xtype: 'textfield',
					name: 'county',
					allowBlank:false
				},{
					xtype: 'combo',
					id: 'zone1',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'zone'],
						data : Ext.combos_selec.zones
					}),
					editable: false,
					displayField:'zone',
					valueField: 'id',
					name: 'zone1',
					hiddenName: 'zone',
					fieldLabel: 'Zone*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a zone...',
					selectOnFocus:true,
					allowBlank:false
				}, {
					xtype: 'combo',
					name: 'show1',
					hiddenName: 'show',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							[1, 'Yes'],
							[0, 'No']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'Showed?*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select is Showed...',
					selectOnFocus:true,
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'condados'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "condados", 
					key: 'idCounty',
					keyID: oGrid_Event.record.data.idCounty,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doFilter(combo,record,index){
		select_st = combo.getValue();
		store.load({params:{start:0, limit:200, filter:select_st}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
	function is_showed(val){
		if(val==1)
			return 'Yes';
		else
			return 'No';
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: '../LIB/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			{id:'idCounty',header: "County ID", width: 80, align: 'center', sortable: true, dataIndex: 'idCounty'},
			{header: 'State', width: 85, sortable: true, align: 'center', dataIndex: 'State',editor: new Ext.form.ComboBox({
               typeAhead: true,
               triggerAction: 'all',
               store: new Ext.data.SimpleStore({
			 		fields: ['st', 'state'],
		 			data : Ext.combos_selec.states
		   		 }),
			   mode: 'local',
			   triggerAction: 'all',
			   editable: false,
			   selectOnFocus:true,
			   displayField:'state',
			   valueField:'st',
               listClass: 'x-combo-list-small'
            })},
			{header: 'County', width: 85, sortable: true, align: 'center', dataIndex: 'County',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'BD', width: 85, sortable: true, align: 'center', dataIndex: 'BD',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Zone', width: 85, sortable: true, align: 'center', dataIndex: 'zone',editor: new Ext.form.ComboBox({
               typeAhead: true,
               triggerAction: 'all',
               store: new Ext.data.SimpleStore({
						fields: ['id', 'zone'],
						data : Ext.combos_selec.zones
					}),
			   mode: 'local',
			   triggerAction: 'all',
			   editable: false,
			   selectOnFocus:true,
			   displayField:'zone',
               listClass: 'x-combo-list-small'
            })
},
			{header: 'Avg. Latitude', width: 100, sortable: true, align: 'center', dataIndex: 'AvgLat',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: false,
               maxValue: 1000
			   ,decimalPrecision: 10
           })},
			{header: 'Avg. Longitude', width: 100, sortable: true, align: 'center', dataIndex: 'AvgLong',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: true,
               maxValue: 1000,
			   minValue: -1000
			   ,decimalPrecision: 10
           })},
			{header: 'Min. Latitude', width: 100, sortable: true, align: 'center', dataIndex: 'MinLat',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: false,
               maxValue: 100000
			   ,decimalPrecision: 10
           })},
			{header: 'Min. Longitude', width: 100, sortable: true, align: 'center', dataIndex: 'MinLong',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: true,
               maxValue: 100000,
			   minValue: -1000
			   ,decimalPrecision: 10
           })},
			{header: 'Max. Latitude', width: 100, sortable: true, align: 'center', dataIndex: 'MaxLat',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: false,
               maxValue: 100000
			   ,decimalPrecision: 10
           })},
			{header: 'Max. Longitude', width: 100, sortable: true, align: 'center', dataIndex: 'MaxLong',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: true,
               maxValue: 100000,
			   minValue: -1000
			   ,decimalPrecision: 10
           })},
			{header: 'Long. PID', width: 85, sortable: true, align: 'center', dataIndex: 'longPID',editor: new Ext.form.NumberField({
               allowBlank: false,
               allowNegative: false,
			   allowDecimals : false,
               maxValue: 100
           })},
			{header: 'Is Showed?', width: 85, sortable: true, align: 'center', renderer: is_showed, dataIndex: 'is_showed',editor: new Ext.form.ComboBox({
               typeAhead: true,
               triggerAction: 'all',
               store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							[1, 'Yes'],
							[0, 'No']
						]
					}),
			   mode: 'local',
			   triggerAction: 'all',
			   editable: false,
			   selectOnFocus:true,
			   displayField:'tipo',
			   valueField: 'id',
               listClass: 'x-combo-list-small'
            })}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Counties',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['st', 'state'],
		 			data : Ext.combos_selec.states
		   		 }),
				 editable: false,
				 displayField:'state',
				 valueField: 'st',
				 name: 'county2',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});