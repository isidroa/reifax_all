Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=xrayCompData',
		fields: [
			'accion',
			'actual',
			'nueva',
			'diff'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display"
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doFilter(combo,record,index){
		select_st = combo.getValue();
		var _loading_win=new Ext.Window({
			 width:170,
			 autoHeight: true,
			 resizable: false,
			 modal: true,
			 border:false,
			 closable:false,
			 plain: false,
			 html:'<div style="background-color: rgb(255, 255, 225);"><img src="includes/img/ac.gif"/><b>Please wait, loading!...</b></div>'
		});
		
		_loading_win.show();
		
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_data.php?tipo=xrayComp', 
				method: 'POST',
			 	timeout :600000,
				params: { 
					filter: select_st,
					filter2:select_pt
				},
				
				failure:function(response,options){
					_loading_win.close();
					Ext.MessageBox.alert('Warning','ERROR');
				},
				success:function(response,options){
					_loading_win.close();
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.load({params:{start:0, limit:200}});
				}                                
			 }
		);
	}
	
	function doFilter2(combo,record,index){
		select_pt = combo.getValue();
		var _loading_win=new Ext.Window({
			 width:170,
			 autoHeight: true,
			 resizable: false,
			 modal: true,
			 border:false,
			 closable:false,
			 plain: false,
			 html:'<div style="background-color: rgb(255, 255, 225);"><img src="includes/img/ac.gif"/><b>Please wait, loading!...</b></div>'
		});
		
		_loading_win.show();
		
		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_data.php?tipo=xrayComp', 
				method: 'POST',
			 	timeout :6000000,
				params: { 
					filter: select_st,
					filter2:select_pt
				},
				
				failure:function(response,options){
					_loading_win.close();
					Ext.MessageBox.alert('Warning','ERROR');
				},
				success:function(response,options){
					_loading_win.close();
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.load({params:{start:0, limit:200}});
				}                                
			 }
		); 
		
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: '../LIB/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			{id:'accion',header: "Action", width: 300, align: 'left', sortable: true, dataIndex: 'accion'},
			{header: 'Actual Data', width: 80, sortable: true, align: 'center', dataIndex: 'actual'},
			{header: 'New Data', width: 80, sortable: true, align: 'center', dataIndex: 'nueva'},
			{header: 'Differential', width: 80, sortable: true, align: 'center', dataIndex: 'diff'}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Comparable XRay',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['st', 'state'],
		 			data : Ext.combos_selec.bd
		   		 }),
				 editable: false,
				 displayField:'state',
				 valueField: 'state',
				 name: 'county2',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State/Counties...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                },{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'value'],
		 			data: [
							['-1', 'County'],
							['01', 'SF'],
							['04', 'Condos']
						]
		   		 }),
				 editable: false,
				 displayField:'value',
				 valueField: 'id',
				 name: 'proptype',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a PropType...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter2
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	//grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});