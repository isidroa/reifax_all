function nuevoAjax()
{ 
	var xmlhttp=false; 
	try 
	{ xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 	}
	catch(e)
	{ 	try	{ xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); } 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
	return xmlhttp; 
}



function validateUser()
{
	document.getElementById("msgError").innerHTML="&nbsp;";
	var email=document.getElementById("txtEmail").value;
	var pwd=document.getElementById("txtPwd").value;
	if(email=="")
	{
		document.getElementById("msgError").innerHTML="Please indicate the E-Mail.";
		return;
	}
	if(pwd=="")
	{
		document.getElementById("msgError").innerHTML="Please indicate the Password";
		return;
	}
	var ajax=nuevoAjax();
	ajax.open("POST", "login.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("email="+email+"&pwd="+pwd);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var result=ajax.responseText;
//			alert(result);//return;
			if(result==0)
				document.getElementById("msgError").innerHTML="Sorry, the E-Mail/Password is not valid.";
			else
				location.href="condados.php";
		}
	}
}