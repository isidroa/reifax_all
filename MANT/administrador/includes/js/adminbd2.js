Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var firstGridStore = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=adminbd',
		fields: [
			{name: 'idCounty', type: 'int'},
			'County',
			'lugar'
		]
	});
	
	var secondGridStore = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=adminbd',
		fields: [
			{name: 'idCounty', type: 'int'},
			'County',
			'lugar'
		]
	});
///////////FIN Cargas de data dinamica///////////////

	var combo = new Ext.form.ComboBox({
			store: new Ext.data.SimpleStore({
					fields: ['id', 'program'],
					data : [
					[0, 'Xima3'],
					[1, 'Xima'],
					[2, 'Proyecto_Xima']
				] // from states.js
				}),
			displayField:'program',
			valueField: 'id',
			editable: false,
			selectOnFocus: true,
			value: 2,
			mode: 'local',
			triggerAction: 'all',
			width:135
	    });	
	
	combo.addListener("select",function(combo,record,index){
		firstGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:0}});
		secondGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:1}});
	});


	// Generic fields array to use in both store defs.
	var fields = [
	   {name: 'County', mapping : 'County'}
	];

	// Column Model shortcut array
	var cols = [
		{ id : 'County', header: "County", width: 160, sortable: true, dataIndex: 'County'}
	];
    
	// declare the source Grid
    var firstGrid = new Ext.grid.GridPanel({
        store            : firstGridStore,
        columns          : cols,
		split			 : false,
		frame			 : true,
        stripeRows       : true,
        autoExpandColumn : 'County',
        width            : screen.width/2,
		region           : 'west',
        title            : 'Data Base 1'
    });
	
	firstGrid.addListener('rowdblclick',function(grid,index,e){
		var record = grid.getStore().getAt(index);
		//alert(record.data.County);
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_add.php', 
				method: 'POST', 
				params: { 
					tipo: "adminbd", 
					program: combo.getValue(),
					valor: 1,
					county: record.data.County
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					firstGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:0}});
					secondGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:1}});
				}                                
			 }
		);
	});
	
    // create the destination Grid
    var secondGrid = new Ext.grid.GridPanel({
        store            : secondGridStore,
        columns          : cols,
		split			 : false,
		frame			 : true,
        stripeRows       : true,
        autoExpandColumn : 'County',
        width            : screen.width/2,
		region           : 'center',
        title            : 'Data Base 2'
    });
	
	secondGrid.addListener('rowdblclick',function(grid,index,e){
		var record = grid.getStore().getAt(index);
		//alert(record.data.County);
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_add.php', 
				method: 'POST', 
				params: { 
					tipo: "adminbd", 
					program: combo.getValue(),
					valor: 0,
					county: record.data.County
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					firstGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:0}});
					secondGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:1}});
				}                                
			 }
		);
	});

	
	//Simple 'border layout' panel to house both grids
	var displayPanel = new Ext.Panel({
		height   : 470,
		frame    : true,
		title    : 'Programs db Administration',
		layout   : 'border',
		bodyBorder: true,
		border: true,
		loadMask : true,
		items    : [
			firstGrid,
			secondGrid
		],
		tbar:combo
	});
	
	
	(function() {
		displayPanel.doLayout();
		Ext.getCmp('menu_page').doLayout();
		console.clear();
	}).defer(1500);
	
	(function() {
		Ext.getCmp('menu_page').doLayout();
		console.clear();
	}).defer(1500);

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			Height:25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			id:'Viewport-center',  
			autoHeight: true,
			items: displayPanel
		}],
		listeners: {
            render : function(){

			}
		}
	});
	
//////////////FIN VIEWPORT////////////////////////////////
	
	//carga de data
	firstGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:0}});
	secondGridStore.load({params:{start:0, limit:200,program:combo.getValue(),tipo:1}});

});