Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=xrayCalc',
		fields: [
			{name: 'idxray', type: 'int'},
			{name: 'zip', type: 'int'},
			{name: 'total', type: 'int'},
			{name: 'distress', type: 'int'},
			{name: 'pctdistress', type: 'float'},
			{name: 'preforeclosure', type: 'int'},
			{name: 'pctpreforeclosure', type: 'float'},
			{name: 'foreclosure', type: 'int'},
			{name: 'pctforeclosure', type: 'float'},
			{name: 'upsidedown', type: 'int'},
			{name: 'pctupsidedown', type: 'float'},
			{name: 'sale', type: 'int'},
			{name: 'pctsale', type: 'float'},
			{name: 'ownerY', type: 'int'},
			{name: 'pctownerY', type: 'float'},
			{name: 'ownerN', type: 'int'},
			{name: 'pctownerN', type: 'float'},
			{name: 'total_sold_P', type: 'int'},
			{name: 'pcttotal_sold_P', type: 'float'},
			{name: 'total_no_sold_P', type: 'int'},
			{name: 'pcttotal_no_sold_P', type: 'float'},
			{name: 'total_P_0', type: 'int'},
			{name: 'pcttotal_P_0', type: 'float'},
			{name: 'total_P_0_mas', type: 'int'},
			{name: 'pcttotal_P_0_mas', type: 'float'},
			{name: 'total_P_30', type: 'int'},
			{name: 'pcttotal_P_30', type: 'float'},
			{name: 'total_sold_F', type: 'int'},
			{name: 'pcttotal_sold_F', type: 'float'},
			{name: 'total_no_sold_F', type: 'int'},
			{name: 'pcttotal_no_sold_F', type: 'float'},
			{name: 'total_sold_UD', type: 'int'},
			{name: 'pcttotal_sold_UD', type: 'float'},
			{name: 'total_no_sold_UD', type: 'int'},
			{name: 'pcttotal_no_sold_UD', type: 'float'},
			{name: 'total_sold_PE', type: 'int'},
			{name: 'pcttotal_sold_PE', type: 'float'},
			{name: 'total_sold_PE_P', type: 'int'},
			{name: 'pcttotal_sold_PE_P', type: 'float'},
			{name: 'total_sold_PE_F', type: 'int'},
			{name: 'pcttotal_sold_PE_F', type: 'float'},
			{name: 'invM', type: 'float'},
			{name: 'a_days', type: 'float'},
			{name: 'total_sold_3', type: 'float'},
			{name: 'a_sold_3', type: 'float'},
			{name: 'a_asp12', type: 'float'},
			{name: 'm_asp12', type: 'float'},
			{name: 'a_asp9', type: 'float'},
			{name: 'm_asp9', type: 'float'},
			{name: 'a_asp6', type: 'float'},
			{name: 'm_asp6', type: 'float'},
			{name: 'a_asp3', type: 'float'},
			{name: 'm_asp3', type: 'float'},
			'proptype',
			'fecha'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.filter=select_st;
					opt.params.filter2=select_pt;
				}
			}
        }
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display"
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doFilter(combo,record,index){
		select_st = combo.getValue();
		store.load({params:{start:0, limit:200, filter:select_st, filter2:select_pt}});
	}
	
	function doFilter2(combo,record,index){
		select_pt = combo.getValue();
		store.load({params:{start:0, limit:200, filter:select_st, filter2:select_pt}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: '../LIB/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			{id:'idxray',header: "ID", width: 80, align: 'center', sortable: true, dataIndex: 'idxray'},
			{header: 'Fecha', width: 150, sortable: true, align: 'center', dataIndex: 'fecha'},
			{header: 'PropType', width: 65, sortable: true, align: 'center', dataIndex: 'proptype'},
			{header: 'Zip', width: 85, sortable: true, align: 'center', dataIndex: 'zip'},
			{header: 'Total', width: 85, sortable: true, align: 'center', dataIndex: 'total'},
			{header: 'distress', width: 85, sortable: true, align: 'center', dataIndex: 'distress'},
			{header: 'pctdistress', width: 85, sortable: true, align: 'center', dataIndex: 'pctdistress'},
			{header: 'preforeclosure', width: 85, sortable: true, align: 'center', dataIndex: 'preforeclosure'},
			{header: 'pctpreforeclosure', width: 85, sortable: true, align: 'center', dataIndex: 'pctpreforeclosure'},
			{header: 'foreclosure', width: 85, sortable: true, align: 'center', dataIndex: 'foreclosure'},
			{header: 'pctforeclosure', width: 85, sortable: true, align: 'center', dataIndex: 'pctforeclosure'},
			{header: 'upsidedown', width: 85, sortable: true, align: 'center', dataIndex: 'upsidedown'},
			{header: 'pctupsidedown', width: 85, sortable: true, align: 'center', dataIndex: 'pctupsidedown'},
			{header: 'sale', width: 85, sortable: true, align: 'center', dataIndex: 'sale'},
			{header: 'pctsale', width: 85, sortable: true, align: 'center', dataIndex: 'pctsale'},
			{header: 'ownerY', width: 85, sortable: true, align: 'center', dataIndex: 'ownerY'},
			{header: 'pctownerY', width: 85, sortable: true, align: 'center', dataIndex: 'pctownerY'},
			{header: 'ownerN', width: 85, sortable: true, align: 'center', dataIndex: 'ownerN'},
			{header: 'pctownerN', width: 85, sortable: true, align: 'center', dataIndex: 'pctownerN'},
			{header: 'total_sold_P', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_P'},
			{header: 'pcttotal_sold_P', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_P'},
			{header: 'total_no_sold_P', width: 85, sortable: true, align: 'center', dataIndex: 'total_no_sold_P'},
			{header: 'pcttotal_no_sold_P', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_no_sold_P'},
			{header: 'total_P_0', width: 85, sortable: true, align: 'center', dataIndex: 'total_P_0'},
			{header: 'pcttotal_P_0', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_P_0'},
			{header: 'total_P_0_mas', width: 85, sortable: true, align: 'center', dataIndex: 'total_P_0_mas'},
			{header: 'pcttotal_P_0_mas', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_P_0_mas'},
			{header: 'total_P_30', width: 85, sortable: true, align: 'center', dataIndex: 'total_P_30'},
			{header: 'pcttotal_P_30', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_P_30'},
			{header: 'total_sold_F', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_F'},
			{header: 'pcttotal_sold_F', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_F'},
			{header: 'total_no_sold_F', width: 85, sortable: true, align: 'center', dataIndex: 'total_no_sold_F'},
			{header: 'pcttotal_no_sold_F', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_no_sold_F'},
			{header: 'total_sold_UD', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_UD'},
			{header: 'pcttotal_sold_UD', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_UD'},
			{header: 'total_no_sold_UD', width: 85, sortable: true, align: 'center', dataIndex: 'total_no_sold_UD'},
			{header: 'pcttotal_no_sold_UD', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_no_sold_UD'},
			{header: 'total_sold_PE', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_PE'},
			{header: 'pcttotal_sold_PE', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_PE'},
			{header: 'total_sold_PE_P', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_PE_P'},
			{header: 'pcttotal_sold_PE_P', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_PE_P'},
			{header: 'total_sold_PE_F', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_PE_F'},
			{header: 'pcttotal_sold_PE_F', width: 85, sortable: true, align: 'center', dataIndex: 'pcttotal_sold_PE_F'},
			{header: 'invM', width: 85, sortable: true, align: 'center', dataIndex: 'invM'},
			{header: 'a_days', width: 85, sortable: true, align: 'center', dataIndex: 'a_days'},
			{header: 'total_sold_3', width: 85, sortable: true, align: 'center', dataIndex: 'total_sold_3'},
			{header: 'a_sold_3', width: 85, sortable: true, align: 'center', dataIndex: 'a_sold_3'},
			{header: 'a_asp12', width: 85, sortable: true, align: 'center', dataIndex: 'a_asp12'},
			{header: 'm_asp12', width: 85, sortable: true, align: 'center', dataIndex: 'm_asp12'},
			{header: 'a_asp9', width: 85, sortable: true, align: 'center', dataIndex: 'a_asp9'},
			{header: 'm_asp9', width: 85, sortable: true, align: 'center', dataIndex: 'm_asp9'},
			{header: 'a_asp6', width: 85, sortable: true, align: 'center', dataIndex: 'a_asp6'},
			{header: 'm_asp6', width: 85, sortable: true, align: 'center', dataIndex: 'm_asp6'},
			{header: 'a_asp3', width: 85, sortable: true, align: 'center', dataIndex: 'a_asp3'},
			{header: 'm_asp3', width: 85, sortable: true, align: 'center', dataIndex: 'm_asp3'}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Calculate XRay',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['st', 'state'],
		 			data : Ext.combos_selec.bd
		   		 }),
				 editable: false,
				 displayField:'state',
				 valueField: 'state',
				 name: 'county2',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State/Counties...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                },{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'value'],
		 			data: [
							['-1', 'County'],
							['01', 'SF'],
							['04', 'Condos']
						]
		   		 }),
				 editable: false,
				 displayField:'value',
				 valueField: 'id',
				 name: 'proptype',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a PropType...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter2
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	//grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});