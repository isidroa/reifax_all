Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	var selected='';
	Ext.QuickTips.init();//habilitar los tooltips
    Ext.form.Field.prototype.msgTarget = 'side';//errores a la derecha
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=maestro',
		sortInfo: {field: "tblName", direction: "ASC"},
		fields: [
			{name: 'maesID', type: 'int'},
			'tblName',
			'bdName',
			'basica'
		]
	});

///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[
			{
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel
                }
			,{
 				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd
               }
			,{
				 id: 'query_butt',
                 tooltip: 'Click to Query View',
				 iconCls:'icon',
				 icon: 'includes/img/setsearch.gif',
                 handler: doQueryView
                }
			,{
				 id: 'runfull_butt',
                 tooltip: 'Click to Run Full',
				 iconCls:'icon',
				 icon: 'includes/img/runfull.gif',
                 handler: function(){
                        doRunfull('ALL')
                    }
                }
			,{
				 id: 'create_butt',
                 tooltip: 'Click to Create Folder',
		 disabled: true,
				 iconCls:'icon',
				 icon: 'includes/img/create.gif',
				 handler: doCreate
                }
		]});
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function doAdd(){
		var button = Ext.get('add_butt');
		var simple = new Ext.FormPanel({
			labelWidth: 100,
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Master',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',


			items: [{
					fieldLabel: 'Tbl Name*',
					xtype: 'textfield',
					name: 'tbl',
					allowBlank:false
				},{
					fieldLabel: 'BD Name*',
					xtype: 'textfield',
					name: 'bd',
					value:'mantenimiento',
					readOnly:true,
					allowBlank:false
				},{
					fieldLabel: 'Create*',
					xtype: 'textfield',
					name: 'create',
					allowBlank:false
				},{
					fieldLabel: 'Alter',
					xtype: 'textfield',
					name: 'alter'
				},{
					xtype: 'combo',
					name: 'basica',
					hiddenName: 'basica',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['Y', 'Yes'],
							['N', 'No']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'Basica?*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select ...',
					selectOnFocus:true,
					allowBlank:false

				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'maestro'
				}
			],

			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});

		win = new Ext.Window({
                layout      : 'fit',
                width       : 500,
                height      : 300,
				modal	 	: true,
                plain       : true,
                items		: simple,

                buttons: [{
                    text     : 'Close',
                    handler  : function(){
						win.close();
					}
                }]
        });

		win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}//fin function doAdd()

	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		selected='(';

		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.maesID;
		}
		selected+=')';

		if(selec.length==0)
		{	Ext.MessageBox.alert('Warning','Seleccione registro a eliminar');
			return;
		}
		if(selec.length>1)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar solo un registro');
			return;
		}

		Ext.MessageBox.confirm('Delete', 'Confirma la eliminacion?', eliminaReg);
	}//function doDel(){

	function eliminaReg(btn)
	{
		if(btn=='no')return;
		Ext.Ajax.request(
			{
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php',
				method: 'POST',
				params: {
				tipo: "maestro",
					key: 'maesID',
					ID: selected
				},

				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);

					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);

					store.reload();
				}
			 }
		);
	}//function eliminaReg(btn)


	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}

		//submit to server
		Ext.Ajax.request(
			{
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php',
				method: 'POST',
				params: {
					tipo: "maestro",
					key: 'maesID',
					keyID: oGrid_Event.record.data.maesID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},

				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					store.rejectChanges();
				},

				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);

					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);

					store.commitChanges();
				}
			 }
		);
	}; //doEdit(oGrid_Event) {

	function doQueryView(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var activo='';
		selected='(';

		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.maesID;
			activo=selec[i].json.maesID;
		}
		selected+=')';

		if(selec.length==0)
		{	Ext.MessageBox.alert('Warning','Seleccione registro a Ver/Editar Query');
			return;
		}
		if(selec.length>1)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar solo un registro');
			return;
		}

		///Query view lo////////////////////
		Ext.Ajax.request(
			{
				waitMsg: 'Query view loading...',
				url: 'includes/php/grid_data.php',
				method: 'GET',
            timeout : 120000,
				params: {
					tipo: "maestro",
					oper: "ver",
					id: activo
				},

				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error al mostrar create o alter');
				},
				success:function(response,options){
					var rest=response.responseText;////Ext.util.JSON.decode();
					var arres=rest.split('^');
					if(arres[0]!='sucess')
						Ext.MessageBox.alert('Warning',rest.msg);

					Ext.getCmp('qCreate_show').setValue(arres[1]);
					Ext.getCmp('qAlter_show').setValue(arres[2]);
				}
			 }
		);
		/////////////////////////////////

		var button = Ext.get('query_butt');
		var simple = new Ext.FormPanel({
			frame:true,
			title: 'Query View',
			bodyStyle:'padding:5px 5px 0',
			defaults: {width: 630},
			waitMsgTarget : 'Adding Row...',

			items: [{
					fieldLabel: 'Create *',
					xtype: 'textarea',
					name: 'qCreate_show',
					id: 'qCreate_show',
					height:300,
					readOnly: true
				},{
					fieldLabel: 'Alter',
					xtype: 'textarea',
					name: 'qAlter_show',
					id: 'qAlter_show'
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'maestro'
				},{
					xtype: 'hidden',
					name: 'idmaes',
					hiddenName: 'idmaes',
					value: 'maestro'
				}
			],

			buttons: [{
				text: 'Run',
				tooltip: 'Run for county',
				handler  : function(){
                        doRunfull('');
                    }
			}]
		});

		win = new Ext.Window({
                layout      : 'fit',
                width       : 800,
                height      : 500,
				modal	 	: true,
                plain       : true,
                items		: simple,

                buttons: [{
                    text     : 'Close',
                    handler  : function(){
						win.close();
					}
                }]
        });
		win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}//function doQueryView(){

	function doRunfull(todos){
		var activo='ALL';
		if(todos!='ALL')
		{
			var selec = grid.selModel.getSelections();
			var i=0;
			var selected='(';

			for(i=0; i<selec.length; i++){
				if(i>0) selected+=',';
				selected+=selec[i].json.maesID;
				activo=selec[i].json.maesID;
			}
			selected+=')';
			var alte=Ext.getCmp('qAlter_show').getValue();
		}
//		Ext.MessageBox.alert('Warning',activo);return;

		//submit to server
		Ext.Ajax.request(
			{
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php',
				method: 'POST',
            timeout : 120000000,
				params: {
					tipo: "maestrorun",
					key: 'maesID',
					ID: activo,
					alter: alte
				},
				failure:function(response,options){
				   console.log(options);
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
					//win.close();
					store.reload();
				}
			 }
		);
	}

	function doCreate(){
		Ext.MessageBox.alert('Warning','Crear directorios');
	}

//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders//////////////////////////
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		/*renderTo: 'tblregistros-container',*/
		columns: [
			{id:'maesID',header: "MaesID", width: 80, align: 'center', sortable: true, dataIndex: 'maesID'},
			{header: 'Tbl. Name', width: 85, sortable: true, align: 'center', dataIndex: 'tblName',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'BD Name', width: 85, sortable: true, align: 'center', dataIndex: 'bdName',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Basica', width: 85, sortable: true, align: 'center', dataIndex: 'basica',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Master',
		loadMask:true,
		tbar: pagingBar
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: [grid]
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//	grid.getSelectionModel().on('rowselect', doMapeo);
//////////////////FIN Listener///////////////////////

/////////////mostrar grid///////////////////////////
	//grid.render('tblregistros-container');
/////////////FIN mostrar grid////////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////

});