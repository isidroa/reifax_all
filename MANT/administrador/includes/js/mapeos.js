Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,idReg='';
	var bd_sel='';tblD_sel='',tblO_sel='';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=mapeos',
		fields: [
			{name: 'mapID', type: 'int'},
			'tabla',
			'tblResult',
			'campo',
			'campop',
			'prioridad',
			'funcion',
			'descripcion'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.bd=bd_sel;
					opt.params.tblOri=tblO_sel;
					opt.params.tblDes=tblD_sel;
				}
			}
        }
	});
	
	var storeHist = new Ext.data.JsonStore({
			root: 'results',
			url: 'includes/php/grid_data.php?tipo=bitacora&seccion=mapeo',
			fields: [
				{name: 'idbitacora', type: 'int'},
				'id_registro',
				'condado',
				'tabla',
				'campo',
				'old',
				'new',
				'usuario',
				'fecha',
				'hora',
				'descripcion',
				'accion'
			],
			listeners: {
				beforeload: {
					fn: function(store,opt){
						opt.params.idReg=idReg;
						idReg='';
					}
				}
			}
		});
	
	
	
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
            {
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                },
			{
				 id: 'history_button',
                 tooltip: 'Click to View History',
				 iconCls:'icon',
				 icon: 'includes/img/History.png',
                 handler: doHist 
                }	
		]});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.mapID;
		}
		selected+=')';

		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "mapeos", 
					key: 'mapID',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Mappings',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300,bodyStyle:'padding:20px'},
			waitMsgTarget : 'Adding Row...',
			layout: 'table',
			layoutConfig: {
				columns: 4
			},
			
			
			items: [{xtype: 'label',text: 'State/County*:',style: 'font-size:10px;',width:70},{
					 xtype: 'combo',
					 id: 'combo_condado',
					 store: new Ext.data.SimpleStore({
						fields: ['id', 'tabla'],
						data : Ext.combos_selec.bd
					 }),
					 editable: false,
					 fieldLabel: 'State/County*',
					 displayField:'tabla',
					 valueField: 'id',
					 name: 'bd',
					 hiddenName: 'bd',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a State/County...',
					 selectOnFocus:true,
					 allowBlank:false,
					 colspan:3,
					 width: 665,
					 listeners: {
					 	'select': doFilterAdd
					 }
				},{xtype: 'label',text: 'Tbl Destino*:',style: 'font-size:10px;',width:70},{
					 xtype: 'combo',
					 id: 'combo_tbl_des',
					 store: new Ext.data.SimpleStore({
						fields: ['id', 'tabla'],
						data : Ext.combos_selec.tblDestino
					 }),
					 editable: false,
					 fieldLabel: 'Tbl Destino*',
					 displayField:'tabla',
					 valueField: 'id',
					 name: 'tblDest',
					 hiddenName: 'tblid',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Tbl Destino...',
					 selectOnFocus:true,
					 allowBlank:false,
					 listeners: {
					 	'select': doFilterAdd1
					 }
				},{xtype: 'label',text: 'Tbl Origen*:',style: 'font-size:10px;',width:70},{
  					 xtype: 'combo',
					 id: 'tblResultAdd',
					 store: new Ext.data.SimpleStore({
						fields: ['id', 'tblResult','bd'],
						data : Ext.combos_selec.tblOrigen
					 }),
					 editable: false,
					 fieldLabel: 'Tbl Origen*',
					 displayField:'tblResult',
					 valueField: 'id',
					 name: 'tblOri',
					 hiddenName: 'tblResult',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Tbl Origen...',
					 selectOnFocus:true,
					 allowBlank:false,
					 listeners: {
					 	'select': doFilterAdd2
					 },
					 lastQuery:''
				},{xtype: 'label',text: 'Cmp Destino*:',style: 'font-size:10px;',width:70},{
					 xtype: 'combo',
					 id: 'cmpDestinoAdd',
					 store: new Ext.data.SimpleStore({
						fields: ['id', 'tabla','tabla2'],
						data : Ext.combos_selec.cmpDestino
					 }),
					 editable: false,
					 fieldLabel: 'Cmp Destino*',
					 displayField:'tabla',
					 valueField: 'id',
					 name: 'cmpDest',
					 hiddenName: 'campo',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Cmp Destino...',
					 selectOnFocus:true,
					 allowBlank:false,
					 lastQuery:''
				},{xtype: 'label',text: 'Cmp Origen*:',style: 'font-size:10px;',width:70},{
  					 xtype: 'combo',
					 id: 'cmpOrigenAdd',
					 store: new Ext.data.SimpleStore({
						fields: ['id', 'tblResult','tabla2'],
						data : Ext.combos_selec.cmpOrigen
					 }),
					 editable: false,
					 fieldLabel: 'Cmp Origen*',
					 displayField:'tblResult',
					 valueField: 'id',
					 name: 'cmpOri',
					 hiddenName: 'campop',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Cmp Origen...',
					 selectOnFocus:true,
					 allowBlank:false,			
					 listeners: {
					 	'select': activarComboFormula
					 },
					 lastQuery:''
				},{xtype: 'label',text: 'Formula:',style: 'font-size:10px;',width:70},{
					 xtype: 'combo',
					 store: new Ext.data.SimpleStore({
						fields: ['formula', 'nombre'],
						data : Ext.combos_selec.formula
					 }),
					 editable: false,
					 fieldLabel: 'Formula',
					 displayField:'nombre',
					 valueField: 'formula',
					 name: 'formula1',
					 id: 'formula1',
					 hiddenName: 'formula',
					 mode: 'local',
					 triggerAction: 'all',
					 emptyText:'Select a Formula...',
					 selectOnFocus:true,
					 allowBlank:true,
					 listeners: {
					 	'select': seleccionarComboFormula
					 },
					 colspan: 3,
					 width: 665
				},{xtype: 'label',text: '',style: 'font-size:10px;',width:70},{
					fieldLabel:'',
					labelSeparator:'',
					id:'map_formula',
					xtype: 'textarea',
					name: 'formula',
					hiddenName: 'formula',
					allowBlank:true,
					readOnly: true,
					colspan: 3,
					height:80,
					width: 665
				},{xtype: 'label',text: 'Function:',style: 'font-size:10px;',width:70},{
					fieldLabel: 'Function',
					id:'map_function',
					xtype: 'textarea',
					name: 'funcion',
					hiddenName: 'funcion',
					allowBlank:true,
					colspan:3,
					height:90,
					width: 665
				},{xtype: 'label',text: 'Description:',style: 'font-size:10px;',width:70},{
					fieldLabel: 'Description',
					xtype: 'textfield',
					name: 'descripcion',
					hiddenName: 'descripcion',
					allowBlank:true,
					colspan:3,
					width: 665
				},{xtype: 'label',text: 'Prioridad*:',style: 'font-size:10px;',width:70},{
					xtype: 'combo',
					name: 'prioridad1',
					hiddenName: 'prioridad',
					store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['1', '1'],
							['2', '2'],
							['3', '3'],
							['4', '4'],
							['5', '5']
						]
					}),
					editable: false,
					displayField:'tipo',
					valueField: 'id',
					typeAhead: true,
					fieldLabel: 'Prioridad*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select is Prioridad...',
					selectOnFocus:true,
					allowBlank:false,
					colspan:3,
					width: 665
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'mapeos',
					colspan:4
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								//win.close();
								var con = Ext.getCmp('combo_condado').getValue();
								var tbl_or = Ext.getCmp('tblResultAdd').getValue();
								var tbl_des = Ext.getCmp('combo_tbl_des').getValue();
								//alert(con+' '+tbl_or+' '+tbl_des);
								simple.getForm().reset();
								Ext.getCmp('combo_condado').setValue(con);
								Ext.getCmp('tblResultAdd').setValue(tbl_or);
								Ext.getCmp('combo_tbl_des').setValue(tbl_des);
								
								store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			layout      : 'fit',
			width       : 770,
			height      : 450,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});
        
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
		win.addListener("show",function(win){
			if(Ext.getCmp('county1').getValue()!='')
			{
				Ext.getCmp('combo_condado').setValue(Ext.getCmp('county1').getValue());
				seleccionarCampoTblresult();
			}
			if(Ext.getCmp('tblDest').getValue()!='')
			{
				Ext.getCmp('combo_tbl_des').setValue(Ext.getCmp('tblDest').getValue());
				seleccionarCampoDestino();
			}
			if(Ext.getCmp('tblOri').getValue()!='')
			{	
				Ext.getCmp('tblResultAdd').setValue(Ext.getCmp('tblOri').getValue());
				seleccionarCampoOrigen();
			}
		});
	}
	////////////////////Bitacora////////////////////////////////////////////////////////////////
	
	function doEditHist(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "bitacora", 
					key: 'idbitacora',
					keyID: oGrid_Event.record.data.idbitacora,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					storeHist.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					storeHist.commitChanges();
				}                                     
			 }
		);  
	}
	
	function doHist(){
		var button = Ext.get('add_butt');
		var xg = Ext.grid;
		var selec = grid.selModel.getSelections();
		
		var pagingBarHist = new Ext.PagingToolbar({
			pageSize: 50,
			store: storeHist,
			displayInfo: true,
			displayMsg: '<b>Total: {2}</b>',
			emptyMsg: "No topics to display"        
        });	
		
		var expander = new Ext.ux.grid.RowExpander({
			tpl : new Ext.Template(
				'<div style="width:200px;background:#999999;color:#fff;padding:5px;"><p><b>Descripcion:</b> {descripcion}</p></div>'
			)
		});

		
	var gridHist = new Ext.grid.EditorGridPanel({
		store: storeHist,
		iconCls: 'icon',
		columns: [	
			expander,
			{header: 'Descripcion', width: 350, sortable: true, align: 'left',dataIndex:'descripcion',editor: new Ext.form.TextArea({allowBlank: false})}, 
			{header: 'Fecha', width: 85, sortable: true, align: 'left', dataIndex:'fecha'},
			{header: 'Hora', width: 85, sortable: true, align: 'left', dataIndex:'hora'},
			{header: 'Usuario', width: 75, sortable: true, align: 'center', dataIndex:'usuario'},
			{header: 'Campo', width: 75, sortable: true, align: 'center', dataIndex:'campo'},
			{header: 'Valor Anterior', width: 85, sortable: true, align: 'center',dataIndex:'old'},
			{header: 'Valor Nuevo', width: 85, sortable: true, align: 'center',dataIndex:'new'},
			{header: 'Tabla', width: 85, sortable: true, align: 'center',dataIndex:'tabla'},
			{header: 'Id Registro', width: 85, sortable: true, align: 'center',dataIndex:'id_registro'},
			{header: 'Condado', width: 85, sortable: true, align: 'center',dataIndex:'condado'},
			{header: 'Accion', width: 85, sortable: true, align: 'left',dataIndex:'accion'},
			{id:'ID Bitacora',header: "Bitacora ID", width: 80, align: 'center', sortable: true,dataIndex:'idbitacora'}
			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:450,
		rowHeight: 34,
		width: screen.width,
		frame:true,
		title:'Bitacora',
		loadMask:true,
        plugins: expander,
        collapsible: true,
		tbar: pagingBarHist 
	});
	
	gridHist.on('render',function(){
		if(selec.length==0){
				storeHist.load({params:{start:0, limit:200}});
		}else{
				idReg = selec[0].json.mapID;
				storeHist.load({params:{start:0, limit:200,idReg:idReg}});
		}
		
	});
	gridHist.addListener('afteredit', doEditHist);
	
		
		winHist = new Ext.Window({
			layout      : 'fit',
			autoWidth: true,
			height      : 450,
			modal	 	: true,
			plain       : true,
			items		: gridHist,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					winHist.close();
				}
			}]
		});
        
        winHist.show(button);
	}
	
	////////////////////Fin Bitacora////////////////////////////////////////////////////////////////

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "mapeos", 
					key: 'mapID',
					keyID: oGrid_Event.record.data.mapID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edición');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doFilter(combo,record,index){
		bd_sel = combo.getValue();
		tblO_sel='';
		
		var tblO = Ext.getCmp('tblOri');        
        tblO.clearValue();
		var bd_name = record.data.county.replace('/','');
		bd_name= bd_name.substr(0,5);
		var allrecord=tblO.store.getAt(0);
        tblO.store.filter('bd', bd_name.toUpperCase());	
		tblO.store.insert(0,allrecord);
		store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel, tblOri:tblO_sel}});
	}
	
	function doFilterAdd(combo,record,index){
		var tblO = Ext.getCmp('tblResultAdd');        
        tblO.clearValue();
		var bd_name = record.data.tabla.replace('/','');
		bd_name= bd_name.substr(0,5);
        tblO.store.filter('bd', bd_name.toUpperCase());
	}
	function seleccionarCampoTblresult(){
		var tblO = Ext.getCmp('tblResultAdd');        
        tblO.clearValue();
		var name='';
		for(i=0;i<Ext.getCmp('combo_condado').store.getCount() && name=='';i++)
		{
			if(Ext.getCmp('combo_condado').store.getAt(i).get('id')==Ext.getCmp('combo_condado').getValue())
			{
				name=Ext.getCmp('combo_condado').store.getAt(i).get('tabla').replace('/','');
			}
		}
        tblO.store.filter('bd', name.substr(0,5).toUpperCase());
	}
	
	function doFilterAdd1(combo,record,index){
		var tblO = Ext.getCmp('cmpDestinoAdd');        
        tblO.clearValue();
		var name = record.data.tabla;
        tblO.store.filter('tabla2', name);
	}
	function seleccionarCampoDestino(){
		var tblO = Ext.getCmp('cmpDestinoAdd');        
        tblO.clearValue();
		var name='';
		for(i=0;i<Ext.getCmp('combo_tbl_des').store.getCount() && name=='';i++)
		{
			if(Ext.getCmp('combo_tbl_des').store.getAt(i).get('id')==Ext.getCmp('combo_tbl_des').getValue())
				name=Ext.getCmp('combo_tbl_des').store.getAt(i).get('tabla')
		}
        tblO.store.filter('tabla2', name);
	}
	
	function doFilterAdd2(combo,record,index){
		var tblO = Ext.getCmp('cmpOrigenAdd');        
        tblO.clearValue();
		var name = record.data.tblResult;
        tblO.store.filter('tabla2', name);
	}
	function seleccionarCampoOrigen(){
		var tblO = Ext.getCmp('cmpOrigenAdd');        
        tblO.clearValue();
		var name='';
		for(i=0;i<Ext.getCmp('tblResultAdd').store.getCount() && name=='';i++)
		{
			if(Ext.getCmp('tblResultAdd').store.getAt(i).get('id')==Ext.getCmp('tblResultAdd').getValue())
				name=Ext.getCmp('tblResultAdd').store.getAt(i).get('tblResult')
		}
        tblO.store.filter('tabla2', name);
	}
	
	function doFilter2(combo,record,index){
		tblD_sel = combo.getValue();
		store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel, tblOri:tblO_sel}});
	}
	
	function doFilter3(combo,record,index){
		tblO_sel = combo.getValue();
		store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel, tblOri:tblO_sel}});
	}
	
	function seleccionarComboFormula()
	{
		var combo=Ext.getCmp('formula1');
//alert('fun: '+combo.getValue());
		Ext.getCmp('map_formula').setValue(combo.getValue());
		var fun=combo.getValue();
		var campo='`'+Ext.getCmp('cmpOrigenAdd').getValue()+'`';
		while(fun.indexOf('`A`')!=-1){
			var encontrado = false;  
			if(campo.indexOf('$')!=-1){
				campo = campo.replace(/\$/g,'?');
				encontrado = true;
			}
			fun=fun.replace('`A`',campo);
			if(encontrado){
				fun=fun.replace(/\?/g,'$');
			}
		}
		Ext.getCmp('map_function').setValue(fun);
	}
	
	function activarComboFormula()
	{
/*		if(length(Ext.getCmp('cmpDestinoAdd').getValue())==0 || Ext.getCmp('cmpDestinoAdd').getValue()=='')
		{	Ext.MessageBox.alert('Warning','Must select source field');return;}
*/		

//		alert('|'+Ext.getCmp('cmpDestinoAdd').getValue()+'|');

		var name='';
		for(i=0;i<Ext.getCmp('cmpDestinoAdd').store.getCount() && name=='';i++)
		{
			if(Ext.getCmp('cmpDestinoAdd').store.getAt(i).get('id')==Ext.getCmp('cmpDestinoAdd').getValue())
				name=Ext.getCmp('cmpDestinoAdd').store.getAt(i).get('id');
		}
//		alert(name);

		Ext.Ajax.request({  
			waitMsg: 'Wait please...',
			url: 'includes/php/operaciones.php', 
			method: 'POST', 
			params: { 
			tipo: "comboformula", 
				key: name
			}, 
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','No se puede hacer la eliminación');
			},
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
				{Ext.MessageBox.alert('Warning',rest.msg);return}
				//for(i=0;i<Ext.getCmp('formula1').store.getCount();i++)	alert('for: '+Ext.getCmp('formula1').store.getAt(i).get('nombre'));
				Ext.getCmp('formula1').setValue(rest.msg);
				seleccionarComboFormula();
			}                                
		 });		


		/*var combo=Ext.getCmp('formula1');
		Ext.getCmp('map_formula').setValue(combo.getValue());
		var fun=combo.getValue();
		var campo='`'+Ext.getCmp('cmpOrigenAdd').getValue()+'`';
		while(fun.indexOf('`A`')!=-1){									  
			fun=fun.replace('`A`',campo);
		}
		Ext.getCmp('map_function').setValue(fun);*/
	}

//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		columns: [	  
			{id:'mapID',header: "Mapeo ID", width: 80, align: 'center', sortable: true, dataIndex: 'mapID'},
			{header: 'Tbl Destino', width: 85, sortable: true, align: 'center', dataIndex: 'tabla',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Tbl Origen', width: 85, sortable: true, align: 'center', dataIndex: 'tblResult',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Cmp Destino', width: 85, sortable: true, align: 'center', dataIndex: 'campo',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Cmp Origen', width: 85, sortable: true, align: 'center', dataIndex: 'campop',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Prioridad', width: 75, sortable: true, align: 'center', dataIndex: 'prioridad',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Funcion', width: 250, sortable: true, align: 'left', dataIndex: 'funcion',editor: new Ext.form.TextField({allowBlank: true})},
			{header: 'Description', width: 200, sortable: true, align: 'left', dataIndex: 'descripcion',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Mappings',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',
				{
                 xtype: 'combo',
				 id: 'county1',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'county'],
		 			data : Ext.combos_selec.bd
		   		 }),
				 editable: false,
				 displayField:'county',
				 valueField: 'id',
				 name: 'county1',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State/County...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                },
			'-',{
                 xtype: 'combo',
				 id: 'tblDest',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'tabla'],
		 			data : Ext.combos_selec.tblDestino
		   		 }),
				 editable: false,
				 displayField:'tabla',
				 valueField: 'id',
				 name: 'tblDest',
				 hiddenName: 'tblID',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a Tbl Destino...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter2
					 }
                },
			'-',{
                 xtype: 'combo',
				 id: 'tblOri',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'tblResult','bd'],
		 			data : Ext.combos_selec.tblOrigen
		   		 }),
				 editable: false,
				 displayField:'tblResult',
				 valueField: 'id',
				 name: 'tblOri',
				 hiddenName: 'tblResult',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a Tbl Origen...',
				 selectOnFocus:true,
				 allowBlank:true,
				 lastQuery:'',
				 listeners: {
					 'select': doFilter3
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel, tblOri:tblO_sel}});
/////////////FIN Inicializar Grid////////////////////
 
});