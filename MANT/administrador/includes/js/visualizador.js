Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){

    // create the Data Store
	var select_bd=1,select_tipo=1;
    
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?p=visualizador',
		fields: [
			{name: 'ActionID', type: 'int'},
			{name: 'fecha', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'Records', type: 'float'}
		]
	});
	
	var store1 = new Ext.data.JsonStore({
		url: 'county.php',
		autoLoad: true,
		fields: [
			{name: 'id', type: 'int'},
			'county'
		]
	});
	
	var combo = new Ext.form.ComboBox({
			store: store1,
			displayField:'county',
			valueField: 'id',
			editable: false,
			selectOnFocus: true,
			value: 'flbroward',
			mode: 'local',
			triggerAction: 'all',
			width:135
	    });	
	
	combo.addListener("select",function(combo,record,index){
		select_bd = combo.getValue();
		store.load({params:{start:0, limit:25,bd: select_bd, tipo: select_tipo}});
		
	});
	
	var combo2 = new Ext.form.ComboBox({
			store: new Ext.data.SimpleStore({
					fields: ['id', 'tipo'],
					data : [
					[1, 'Records'],
					[2, 'Time (Seg)']
				] // from states.js
				}),
			displayField:'tipo',
			valueField: 'id',
			editable: false,
			selectOnFocus: true,
			value: 1,
			mode: 'local',
			triggerAction: 'all',
			width:135
	    });	
	
	combo2.addListener("select",function(combo,record,index){
		select_tipo = combo.getValue();
		store.load({params:{start:0, limit:25,bd: select_bd, tipo: select_tipo}});
		
	});
	
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 1,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
            '-',combo,'-',combo2
		]});
 
	// create the Grid
	var grid = new Ext.grid.GridPanel({
		store: store,
		columns: [
			{id:'ActionID',header: "Action ID", width: 80, sortable: true, dataIndex: 'ActionID'},
			{header: 'Date', width: 85, sortable: true, renderer: Ext.util.Format.dateRenderer('Y-m-d'),  dataIndex: 'fecha'},
			{header: 'Records/Time (seg)', width: 250, sortable: true, dataIndex: 'Records'}
		],
		viewConfig: {
        	forceFit: true
		},
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		height:600,
		frame:true,
		title:'Visualizador',
		
		tbar: pagingBar 

	});
	
//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
	// load data from the url ( data.php )
	store.load({params:{start:0, limit:25,bd: select_bd, tipo: select_tipo}});
 
});