Ext.onReady(function(){
   var tb = new Ext.Toolbar({
		id:'menu_page',
		items: [{
		 	text:'Mantenimiento',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Condados',
							href: 'condados.php'
						}),
						new Ext.menu.Item({
							text: 'Tablas',
							href: 'tablas.php'
						}),
						new Ext.menu.Item({
							text: 'Formulas',
							menu: new Ext.menu.Menu({
								items: [
									new Ext.menu.Item({
										text: 'Formulas',
										href: 'formulas.php'
									}),
									new Ext.menu.Item({
										text: 'Campos de Formulas',
										href: 'campos_formulas.php'
									})
								]
							})
						}),
						new Ext.menu.Item({
							text: 'Procedencias',
							href: 'procedencias.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'BD Basicas',
							href: 'basica.php'
						}),
						new Ext.menu.Item({
							text: 'Maestro',
							href: 'maestro.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Analizador de Data',
							href: 'visualizador.php'
						}),
						'-',new Ext.menu.Item({
							text: 'Administrator DB',
							href: 'adminbd.php'
						}),{
							text: 'XRay',
							menu: new Ext.menu.Menu({
								items: [
									new Ext.menu.Item({
										text: 'Calculado',
										href: 'xrayCalculado.php'
									}),
									new Ext.menu.Item({
										text: 'Comparativo',
										href: 'xrayComparado.php'
									})
								]
							})
						}
					]
				})  
			},{
		 	text:'Traducción',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Estructuras',
							href: 'estructuras.php'
						}),
						new Ext.menu.Item({
							text: 'Mapeos',
							href: 'mapeos.php'
						}),
						new Ext.menu.Item({
							text: 'Traducción',
							href: 'traduccion.php'
						})
					]
				})  
			},{
		 	text:'Depuración Crudas',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Depuración Crudas',
							href: 'depuracionescrudas.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos Crudas',
							href: 'depcrudasprocesos.php'
						})
					]
				})  
			},{
		 	text:'Depuración',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Depuración',
							href: 'depuraciones.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos',
							href: 'procesos.php'
						})
					]
				})  
			},{
		 	text:'Sincronización',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Sincronización',
							href: 'sincronizaciones.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos',
							href: 'sincroprocesos.php'
						})
					]
				})  
			},{
		 	text:'Calculo',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Calculo',
							href: 'calculos.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos',
							href: 'calcuprocesos.php'
						})
					]
				})  
			},{
		 	text:'Validación',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Validación',
							href: 'validaciones.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos',
							href: 'valiprocesos.php'
						})
					]
				})  
			},{
			text:'Pote',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Pote',
							href: 'pote.php'
						}),
						new Ext.menu.Item({
							text: 'Procesos',
							href: 'poteProcesos.php'
						})
					]
				})  
			},{
				text: 'Logout',
				/*href: 'logout.php'*/
				handler: function(){document.location='logout.php';}
			}
		],
		autoShow: true
	});
	
});