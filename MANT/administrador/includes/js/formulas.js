Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=formulas',
		fields: [
			{name: 'idfor', type: 'int'},
			'nombre',
			'formula'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
            {
                 tooltip: 'Click to Delete selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/delete.gif',
                 handler: doDel 
                },
			{
				 id: 'add_butt',
                 tooltip: 'Click to Add Row',
				 iconCls:'icon',
				 icon: 'includes/img/add.gif',
                 handler: doAdd 
                }
		]});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';

		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.idfor;
		}
		selected+=')';

		//submit to server
		Ext.Ajax.request( 
			{  
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_del.php', 
				method: 'POST', 
				params: { 
					tipo: "formulas", 
					key: 'idfor',
					ID: selected
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la eliminaci�n');
				},
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.reload();
				}                                
			 }
		); 
	}
	
	function doAdd(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'New Formula',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Adding Row...',
			
			items: [{
					fieldLabel: 'Nombre*',
					xtype: 'textfield',
					name: 'nombre',
					allowBlank:false
				},{
					fieldLabel: 'Formula*',
					xtype: 'textarea',
					name: 'formula',
					allowBlank:false,
					height: 120
				},{
					xtype: 'hidden',
					name: 'tipo',
					value: 'formulas'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
		win = new Ext.Window({
			
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
				}
			}]
		});

        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "formulas", 
					key: 'idfor',
					keyID: oGrid_Event.record.data.idfor,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}
			 }
		);  
	}; 
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon-grid',
		columns: [	  
			{id:'idfor',header: "Formula ID", width: 80, align: 'center', sortable: true, dataIndex: 'idfor'},
			{header: 'Nombre', width: 200, sortable: true, align: 'center', dataIndex: 'nombre',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Formula', width: 300, sortable: true, align: 'center', dataIndex: 'formula',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,
		frame:true,
		title:'Formulas',
		loadMask:true,
		
		tbar: pagingBar 

	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});