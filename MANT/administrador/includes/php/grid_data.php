<?php
	include("conexion.php");
	$conex=conectar("mantenimiento");
	
	$modulo=$_GET['tipo'];
	$start=$_POST['start'];
	$limit=$_POST['limit'];
	
	switch($modulo){
		case 'condados':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						idCounty,State,County,
						BD,AvgLat,AvgLong,
						MinLat,MinLong,
						MaxLat,MaxLong,
						is_showed,zone,
						longPID
					FROM xima.lscounty
					ORDER BY idCounty
					LIMIT ".$start.",".$limit;
		break;
		
		case 'tablas':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						tblID,tabla
					FROM mantenimiento.tablas
					ORDER BY tblID
					LIMIT ".$start.",".$limit;
		break;
		
		case 'formulas':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						idfor,nombre,formula
					FROM mantenimiento.formulas
					ORDER BY idfor
					LIMIT ".$start.",".$limit;
		break;
		
		case 'campos_formulas':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						idfc, campodest, idfor, tipo
					FROM mantenimiento.formulacampo
					ORDER BY idfc
					LIMIT ".$start.",".$limit;
		break;
		
		case 'procedencias':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						proID,procedencia
					FROM mantenimiento.procedencias
					ORDER BY proID
					LIMIT ".$start.",".$limit;
		break;
		
		case 'basicas':
			$que="	SELECT SQL_CALC_FOUND_ROWS
						mbdID,bd,visible
					FROM mantenimiento.maestro_basicas
					ORDER BY mbdID
					LIMIT ".$start.",".$limit;
		break;

		case 'maestro':
			if($_GET['oper']=='ver')
			{
				$id=$_GET['id'];
				$que="select qCreate,qAlter from mantenimiento.maestro where maesID=$id";
				$res=mysql_query($que) or die($que.mysql_error()); 
				$r=mysql_fetch_array($res);
				$crea=$r['qCreate'];
				$alt=$r['qAlter'];
				echo 'sucess^'.$crea.'^'.$alt;
				//echo "{success:true, msg:'Succes', qCreate:'".$crea."'}";
				return;
			}
			else
			{
				$que="	SELECT SQL_CALC_FOUND_ROWS maesID,tblName,bdName,basica  
						FROM mantenimiento.maestro
						ORDER BY maesID
						LIMIT ".$start.",".$limit;
			}
		break;
		
		case 'estructuras':
			if(isset($_POST['filter']) && $_POST['filter']!=-1) $filter='WHERE e.idCounty='.$_POST['filter'];
			else $filter='';
			$que="	SELECT SQL_CALC_FOUND_ROWS e.structID, concat(l.state,'/',l.county) as stcounty, 
						p.procedencia, e.tblResult, e.bdResult, e.csvName, e.csvExt, e.descr 
					FROM mantenimiento.estructuras e 
					LEFT JOIN xima.lscounty l ON (e.idCounty=l.idCounty) 
					LEFT JOIN mantenimiento.procedencias p ON (e.proID=p.proID)
					$filter
					ORDER BY e.tblResult,p.procedencia  
					LIMIT ".$start.",".$limit;
		break;
		
		case 'bitacora':
			if(isset($_POST['filter']) && $_POST['filter']!=-1) $filter='WHERE e.idCounty='.$_POST['filter'];
			else $filter='';
			$cond = 'Where 1=1 ';
			$cond .= (isset($_GET['seccion'])) ? 'and tabla=\''.$_GET['seccion'].'\'' : '';
			$cond .= (isset($_GET['tipoProc'])) ? ' and condado like \'%'.$_GET['tipoProc'].'%\'' : '';
			$cond .= (isset($_POST['idReg']) && $_POST['idReg']!='' ) ? ' and id_registro = '.$_POST['idReg'] : '';
			
			$que="	SELECT idbitacora,id_registro, condado, tabla ,campo ,old, new, usuario, fecha, hora, descripcion, accion
					FROM mantenimiento.bitacora b 
					$cond
					order by fecha DESC					
					LIMIT ".$start.",".$limit;
		break;
		
		case 'mapeos':
			$filter='WHERE 1=1';
			
			if(isset($_POST['bd']) && $_POST['bd']!=-1 && $_POST['bd']!='') 
				$filter.=' AND e.idCounty='.$_POST['bd'];
			if(isset($_POST['tblDes']) && $_POST['tblDes']!=-1 && $_POST['tblDes']!='') 
				$filter.=' AND t.tblID='.$_POST['tblDes'];
			if(isset($_POST['tblOri']) && $_POST['tblOri']!=-1 && $_POST['tblOri']!='') 
				$filter.=' AND e.tblResult=\''.$_POST['tblOri'].'\'';
			if(isset($_POST['campos']) && $_POST['campos']!='') 
				$filter.=' AND m.mapID IN '.$_POST['campos'];
			
			$que="	SELECT SQL_CALC_FOUND_ROWS
					m.mapID, e.tblResult, t.tabla, m.campo, m.campop, m.funcion, c.Desc as descripcion, m.prioridad 
					FROM mantenimiento.mapeo m 
					LEFT JOIN mantenimiento.estructuras e ON (e.structID=m.structID) 
					LEFT JOIN mantenimiento.tablas t ON (m.tblID=t.tblID)
					LEFT JOIN xima.camptit c ON (m.campo=c.Campos and replace(t.tabla,'0','')=c.Tabla)
					$filter
					ORDER BY t.tabla,m.campo,m.prioridad 
					LIMIT ".$start.",".$limit;
			
			/*$result=mysql_query ($que) or die ($que.mysql_error ());
			$data= array();
			while($r2=mysql_fetch_array($result))
				$data[]=$r2;
			
			$que="show columns from ".$data[0]['tabla'];
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data2= array();
			while($r2=mysql_fetch_array($result))
				$data2[]=$r2;
			
			$data3=array();
			$i=0;
			foreach($data2 as $k => $val){
				$f=$val['Field'];
				$t=$val['Type'];
				$encontrado=false;
				foreach($data as $x => $val1){
					if(strtolower($val1['tabla'])==strtolower($f)){
						$encontrado=true;
						
					}
				}
				if($encontrado){
					$data3[$i]=$data[]
				}
			}			
			
			echo '({"total":"'.count($data).'","results":'.json_encode($data2).'})';
			return true;*/
		break;
		
		case 'visualizador':
			$condado_grid_filter=$_POST['bd'];
			if($_POST['tipo']==1){
				$que="	SELECT SQL_CALC_FOUND_ROWS ActionID,fecha,sum(Records) as Records 
						FROM administracion
						WHERE fecha>date_sub(curdate(),INTERVAL 30 DAY) and countyID=$condado_grid_filter
						GROUP BY fecha,ActionID
						ORDER BY fecha
						";
			}else{
				$que="	SELECT SQL_CALC_FOUND_ROWS ActionID,fecha,sum(TEjecucion) as Records 
						FROM administracion
						WHERE fecha>date_sub(curdate(),INTERVAL 30 DAY) and countyID=$condado_grid_filter
						GROUP BY fecha,ActionID
						ORDER BY fecha
						";
			}
		break;
		
		case 'adminbd':
			$pro=$_POST['program'];
			$tipo=$_POST['tipo'];

			$que="	SELECT 
					x.idCounty,x.County, l.bd as lugar
					FROM xima.lscounty x
					INNER JOIN xima.lsprogram l ON (x.idCounty=l.idcounty)
					WHERE l.idprogram=$pro and l.bd=$tipo
					ORDER BY x.County";
			/*$result=mysql_query ($que) or die ($que.mysql_error ());
			$data= array();
			$data2= array();
			while($r2=mysql_fetch_array($result))
				$data[]=$r2;
			
			foreach($data as $k => $val){
				$name='bd_'.strtolower(str_replace(' ','',$val['County']));
				
				if($r[$name]===$tipo){
					$data[$k]['lugar']=$r[$name];
					$data2[]=$data[$k];
				}
			}
			
			echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
			return true;*/
			
		break;
		
		case 'traducciones':
			$filter='WHERE 1=1';
			
			if(isset($_POST['bd']) && $_POST['bd']!=-1 && $_POST['bd']!='') 
				$filter.=' AND l.idCounty='.$_POST['bd'];
			if(isset($_POST['tblDes']) && $_POST['tblDes']!=-1 && $_POST['tblDes']!='') 
				$filter.=' AND t.tblID='.$_POST['tblDes'];
			
			
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					m.traID, concat(l.state,'/',l.county) as stcounty, t.tabla, m.tblResult, m.campos, m.consulta, m.indice 
					FROM mantenimiento.traduccion m 
					LEFT JOIN mantenimiento.tablas t ON (m.tblID=t.tblID)
					LEFT JOIN xima.lscounty l ON (m.idCounty=l.idCounty)
					$filter
					ORDER BY l.state,l.county";
		break;
		
		case 'depuracionescrudas':
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
				$filter='WHERE 1=1';
				
				if(isset($_POST['bd']) && $_POST['bd']!=-1 && $_POST['bd']!='') 
					$filter.=' AND l.idCounty='.$_POST['bd'];
				if(isset($_POST['tblDes']) && $_POST['tblDes']!=-1 && $_POST['tblDes']!='') 
					$filter.=' AND t.tblID='.$_POST['tblDes'];
					
				$que="	SELECT SQL_CALC_FOUND_ROWS 
						d.depcruID, concat(l.state,'/',l.county) as stcounty, ucase(d.description) tabla, d.procesos  
						FROM mantenimiento.depuracioncruda d 
						LEFT JOIN xima.lscounty l ON (d.idCounty=l.idCounty)
						$filter
						ORDER BY l.state,l.county,d.orden
						LIMIT ".$start.",".$limit;
			}
		break;
		case 'depProcescrudas':
			$filter='WHERE fuente="DEPCRUDA"';
/*			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
*/				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso,status_proceso,county_afectado, modificaciones  
					FROM mantenimiento.procesos
					$filter
					";
					
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		
		case 'depuraciones':
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
				$filter='WHERE 1=1';
				
				if(isset($_POST['bd']) && $_POST['bd']!=-1 && $_POST['bd']!='') 
					$filter.=' AND l.idCounty='.$_POST['bd'];
				if(isset($_POST['tblDes']) && $_POST['tblDes']!=-1 && $_POST['tblDes']!='') 
					$filter.=' AND t.tblID='.$_POST['tblDes'];
					
				$que="	SELECT SQL_CALC_FOUND_ROWS 
						d.depID, concat(l.state,'/',l.county) as stcounty, t.tabla, d.procesos  
						FROM mantenimiento.depuracion d 
						LEFT JOIN mantenimiento.tablas t ON (d.tblID=t.tblID)
						LEFT JOIN xima.lscounty l ON (d.idCounty=l.idCounty)
						$filter
						ORDER BY l.state,l.county,d.orden
						LIMIT ".$start.",".$limit;
			}
		break;
		case 'depProces':
			$filter='WHERE fuente="DEP"';
			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso,status_proceso,county_afectado, modificaciones  
					FROM mantenimiento.procesos
					$filter";
					
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		
		case 'sincronizaciones':			
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					sincID, textosinc, procesos, orden   
			  		FROM mantenimiento.sincronizacion
					ORDER BY orden 
					LIMIT ".$start.",".$limit;
			}
		break;
		case 'sincProces':
			$filter='WHERE fuente="SINC"';
			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso,modificaciones  
					FROM mantenimiento.procesos
					$filter";
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		
		case 'calculos':	
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
				$que="	SELECT SQL_CALC_FOUND_ROWS 
					calcID, textocalc, procesos, orden  
			  		FROM mantenimiento.calculo
					ORDER BY orden 
					LIMIT ".$start.",".$limit;
			}
		break;
		case 'calcProces':
			$filter='WHERE fuente="CALC"';
			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso, modificaciones
					FROM mantenimiento.procesos
					$filter";
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		
		case 'validaciones':			
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
				$que="	SELECT SQL_CALC_FOUND_ROWS 
					valiID, textovali, procesos, orden  
			  		FROM mantenimiento.validacion
					ORDER BY orden 
					LIMIT ".$start.",".$limit;
			}
		break;
		
		case 'valiProces':
			$filter='WHERE fuente="VALI"';
			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso, modificaciones  
					FROM mantenimiento.procesos
					$filter";
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		case 'pote':			
			if($_GET['oper']=='php'){
				$php=$_GET['php'];
				
				$gestor = @fopen($php, "r");
				$bufer='';
				if ($gestor) {
					while (!feof($gestor)) {
						$bufer .= fgets($gestor, 4096);
					}
					fclose ($gestor);
				}
				
				
				echo 'sucess^'.$bufer;

				return;
			}else{
				$que="	SELECT SQL_CALC_FOUND_ROWS 
					poteID, textopote, procesos, orden  
			  		FROM mantenimiento.pote 
					ORDER BY orden 
					LIMIT ".$start.",".$limit;
			}
		break;
		case 'poteProces':
			$filter='WHERE fuente="POTE"';
			
			if(isset($_POST['procesos']) && $_POST['procesos']!='') 
				$filter.=' AND ejecucion IN '.$_POST['procesos'];
			if(isset($_POST['debug']) && $_POST['debug']!='' && $_POST['debug']!=-1)
				$filter.=' AND substr(debugeo,1,INSTR(debugeo," "))="'.$_POST['debug'].'"';
				
			$que="	SELECT SQL_CALC_FOUND_ROWS 
					procID,ejecucion,proceso,descr,php,consulta,debugeo,tipo_proceso, modificaciones  
					FROM mantenimiento.procesos
					$filter";
			if(isset($_POST['procesos']) && $_POST['procesos']!=''){
				$process=explode(',',str_replace(array('(',')'),'',$_POST['procesos']));
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				$ejecucion=array();
				$data2= array();
				while($r2=mysql_fetch_array($result)){
					$data[]=$r2;
					$ejecucion[]=$r2['ejecucion'];
				}
				
				foreach($process as $k => $val){
					$lugar=array_search($val,$ejecucion);
					$data2[]=$data[$lugar];
				}
				
				echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
				return true;
			}
		break;
		
		case 'maxProcess':			
			$que="SELECT MAX(ejecucion) FROM mantenimiento.procesos ";
			$rest=mysql_query($que) or die("{success:false, msg:\"".$que."<br>".mysql_error()."\"}");
			$r=mysql_fetch_array($rest);
			$ejecID2=is_numeric($r[0]) ? $r[0] : 1;
			$ejecID2++;
			echo "{success:true, msg:'Succes', maxProcess:$ejecID2}";
			return 0;
		break;
		
		case 'xrayCalc':			
			$county_xray = str_replace('/','',$_POST['filter']);
			$proptype_xray = $_POST['filter2'];
			
			$conex=conectar($county_xray);
			$que="SELECT * FROM exray ";
			if($proptype_xray!=-1)
				$que.=" WHERE proptype='$proptype_xray' ";
			else
				$que.=" WHERE proptype='' ";
			$que.=" ORDER BY fecha DESC";
		break;
		
		case 'xrayCompData':			
			$que="SELECT * FROM xima.xray ";
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$data2 = array();
			
			while ($row=mysql_fetch_object($result))
				$data [] = $row;
			
			$data2[]=array("accion"=>"Total Properties","actual"=>$data[0]->total,"nueva"=>$data[1]->total,"diff"=>$data[2]->total);
			$data2[]=array("accion"=>"Distress Properties","actual"=>$data[0]->distress,"nueva"=>$data[1]->distress,"diff"=>$data[2]->distress);
			$data2[]=array("accion"=>"Pct. Distress Properties","actual"=>$data[0]->pctdistress,"nueva"=>$data[1]->pctdistress,"diff"=>$data[2]->pctdistress);
			$data2[]=array("accion"=>"Pre-Foreclosure","actual"=>$data[0]->preforeclosure,"nueva"=>$data[1]->preforeclosure,"diff"=>$data[2]->preforeclosure);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosure","actual"=>$data[0]->pctpreforeclosure,"nueva"=>$data[1]->pctpreforeclosure,"diff"=>$data[2]->pctpreforeclosure);
			$data2[]=array("accion"=>"Foreclosure","actual"=>$data[0]->foreclosure,"nueva"=>$data[1]->foreclosure,"diff"=>$data[2]->foreclosure);
			$data2[]=array("accion"=>"Pct. Foreclosure","actual"=>$data[0]->pctforeclosure,"nueva"=>$data[1]->pctforeclosure,"diff"=>$data[2]->pctforeclosure);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure","actual"=>$data[0]->upsidedown,"nueva"=>$data[1]->upsidedown,"diff"=>$data[2]->upsidedown);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure","actual"=>$data[0]->pctupsidedown,"nueva"=>$data[1]->pctupsidedown,"diff"=>$data[2]->pctupsidedown);
			$data2[]=array("accion"=>"Active For Sale","actual"=>$data[0]->sale,"nueva"=>$data[1]->sale,"diff"=>$data[2]->sale);
			$data2[]=array("accion"=>"Pct. Active For Sale","actual"=>$data[0]->pctsale,"nueva"=>$data[1]->pctsale,"diff"=>$data[2]->pctsale);
			$data2[]=array("accion"=>"Owner Occupied","actual"=>$data[0]->ownerY,"nueva"=>$data[1]->ownerY,"diff"=>$data[2]->ownerY);
			$data2[]=array("accion"=>"Pct. Owner Occupied","actual"=>$data[0]->pctownerY,"nueva"=>$data[1]->pctownerY,"diff"=>$data[2]->pctownerY);
			$data2[]=array("accion"=>"Not Owner Occupied","actual"=>$data[0]->ownerN,"nueva"=>$data[1]->ownerN,"diff"=>$data[2]->ownerN);
			$data2[]=array("accion"=>"Pct. Not Owner Occupied","actual"=>$data[0]->pctownerN,"nueva"=>$data[1]->pctownerN,"diff"=>$data[2]->pctownerN);
			$data2[]=array("accion"=>"Pre-Foreclosures Active For Sale","actual"=>$data[0]->total_sold_P,"nueva"=>$data[1]->total_sold_P,"diff"=>$data[2]->total_sold_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures Active For Sale","actual"=>$data[0]->pcttotal_sold_P,"nueva"=>$data[1]->pcttotal_sold_P,"diff"=>$data[2]->pcttotal_sold_P);
			$data2[]=array("accion"=>"Pre-Foreclosures Not For Sale","actual"=>$data[0]->total_no_sold_P,"nueva"=>$data[1]->total_no_sold_P,"diff"=>$data[2]->total_no_sold_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures Not For Sale","actual"=>$data[0]->pcttotal_no_sold_P,"nueva"=>$data[1]->pcttotal_no_sold_P,"diff"=>$data[2]->pcttotal_no_sold_P);
			$data2[]=array("accion"=>"Pre-Foreclosures with less than 0% equity","actual"=>$data[0]->total_P_0,"nueva"=>$data[1]->total_P_0,"diff"=>$data[2]->total_P_0);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures with less than 0% equity","actual"=>$data[0]->pcttotal_P_0,"nueva"=>$data[1]->pcttotal_P_0,"diff"=>$data[2]->pcttotal_P_0);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 0% equity","actual"=>$data[0]->total_P_0_mas,"nueva"=>$data[1]->total_P_0_mas,"diff"=>$data[2]->total_P_0_mas);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 0% equity","actual"=>$data[0]->pcttotal_P_0_mas,"nueva"=>$data[1]->pcttotal_P_0_mas,"diff"=>$data[2]->pcttotal_P_0_mas);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 30% equity","actual"=>$data[0]->total_P_30,"nueva"=>$data[1]->total_P_30,"diff"=>$data[2]->total_P_30);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures with more than 30% equity","actual"=>$data[0]->pcttotal_P_30,"nueva"=>$data[1]->pcttotal_P_30,"diff"=>$data[2]->pcttotal_P_30);
			$data2[]=array("accion"=>"Foreclosures Active For Sale","actual"=>$data[0]->total_sold_F,"nueva"=>$data[1]->total_sold_F,"diff"=>$data[2]->total_sold_F);
			$data2[]=array("accion"=>"Pct. Foreclosures Active For Sale","actual"=>$data[0]->pcttotal_sold_F,"nueva"=>$data[1]->pcttotal_sold_F,"diff"=>$data[2]->pcttotal_sold_F);
			$data2[]=array("accion"=>"Foreclosures Not For Sale","actual"=>$data[0]->total_no_sold_F,"nueva"=>$data[1]->total_no_sold_F,"diff"=>$data[2]->total_no_sold_F);
			$data2[]=array("accion"=>"Pct. Foreclosures Not For Sale","actual"=>$data[0]->pcttotal_no_sold_F,"nueva"=>$data[1]->pcttotal_no_sold_F,"diff"=>$data[2]->pcttotal_no_sold_F);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure Active For Sale","actual"=>$data[0]->total_sold_UD,"nueva"=>$data[1]->total_sold_UD,"diff"=>$data[2]->total_sold_UD);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure Active For Sale","actual"=>$data[0]->pcttotal_sold_UD,"nueva"=>$data[1]->pcttotal_sold_UD,"diff"=>$data[2]->pcttotal_sold_UD);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure Not For sale","actual"=>$data[0]->total_no_sold_UD,"nueva"=>$data[1]->total_no_sold_UD,"diff"=>$data[2]->total_no_sold_UD);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure Not For sale","actual"=>$data[0]->pcttotal_no_sold_UD,"nueva"=>$data[1]->pcttotal_no_sold_UD,"diff"=>$data[2]->pcttotal_no_sold_UD);
			$data2[]=array("accion"=>"Properties For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE,"nueva"=>$data[1]->total_sold_PE,"diff"=>$data[2]->total_sold_PE);
			$data2[]=array("accion"=>"Pct. Properties For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE,"nueva"=>$data[1]->pcttotal_sold_PE,"diff"=>$data[2]->pcttotal_sold_PE);
			$data2[]=array("accion"=>"Pre-Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE_P,"nueva"=>$data[1]->total_sold_PE_P,"diff"=>$data[2]->total_sold_PE_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE_P,"nueva"=>$data[1]->pcttotal_sold_PE_P,"diff"=>$data[2]->pcttotal_sold_PE_P);
			$data2[]=array("accion"=>"Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE_F,"nueva"=>$data[1]->total_sold_PE_F,"diff"=>$data[2]->total_sold_PE_F);
			$data2[]=array("accion"=>"Pct. Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE_F,"nueva"=>$data[1]->pcttotal_sold_PE_F,"diff"=>$data[2]->pcttotal_sold_PE_F);
			
			$data2[]=array("accion"=>"Total Sold in the Last 3 months","actual"=>$data[0]->total_sold_3,"nueva"=>$data[1]->total_sold_3,"diff"=>$data[2]->total_sold_3);
			$data2[]=array("accion"=>"Average Sold in the last 3 moths","actual"=>$data[0]->a_sold_3,"nueva"=>$data[1]->a_sold_3,"diff"=>$data[2]->a_sold_3);
			$data2[]=array("accion"=>"Months of inventory","actual"=>$data[0]->invM,"nueva"=>$data[1]->invM,"diff"=>$data[2]->invM);
			$data2[]=array("accion"=>"Average Days on Market","actual"=>$data[0]->a_days,"nueva"=>$data[1]->a_days,"diff"=>$data[2]->a_days);
			
			
			$data2[]=array("accion"=>"Average Sold Price 12 months","actual"=>$data[0]->a_asp12,"nueva"=>$data[1]->a_asp12,"diff"=>$data[2]->a_asp12);
			$data2[]=array("accion"=>"Median Sold Price 12 months","actual"=>$data[0]->m_asp12,"nueva"=>$data[1]->m_asp12,"diff"=>$data[2]->m_asp12);
			$data2[]=array("accion"=>"Average Sold Price 9 months","actual"=>$data[0]->a_asp9,"nueva"=>$data[1]->a_asp9,"diff"=>$data[2]->a_asp9);
			$data2[]=array("accion"=>"Median Sold Price 9 months","actual"=>$data[0]->m_asp9,"nueva"=>$data[1]->m_asp9,"diff"=>$data[2]->m_asp9);
			$data2[]=array("accion"=>"Average Sold Price 6 months","actual"=>$data[0]->a_asp6,"nueva"=>$data[1]->a_asp6,"diff"=>$data[2]->a_asp6);
			$data2[]=array("accion"=>"Median Sold Price 6 months","actual"=>$data[0]->m_asp6,"nueva"=>$data[1]->m_asp6,"diff"=>$data[2]->m_asp6);
			$data2[]=array("accion"=>"Average Sold Price 3 months","actual"=>$data[0]->a_asp3,"nueva"=>$data[1]->a_asp3,"diff"=>$data[2]->a_asp3);
			$data2[]=array("accion"=>"Median Sold Price 3 months","actual"=>$data[0]->m_asp3,"nueva"=>$data[1]->m_asp3,"diff"=>$data[2]->m_asp3);
			
			
			
			
			echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
			
			return 0;
		break;
		
		case 'xrayComp':
			function porcentaje($val){
				return number_format($val, 2, '.', '');
			}
			
			$array=array();
			$array2=array();
			
			$county_xray = str_replace('/','',$_POST['filter']);
			$proptype_xray = $_POST['filter2']==-1?'':$_POST['filter2'];
			
			
			$que='SELECT bd_'.strtolower(str_replace('FL','',$county_xray)).' FROM xima.lsprogram l where id=1;';
			$rest=mysql_query ($que) or die ($que.mysql_error ());
			$r=mysql_fetch_array($rest);
			if($r['0']=='0' || $r['0']==0){ $actual=$county_xray; $nueva=$county_xray.'1';}
			else{ $actual=$county_xray.'1'; $nueva=$county_xray;}
			
			$conex=conectar($actual);
			include("xray.php");
			
			$array=array("tipo"=>"BD1","data"=>"Actual","proptype"=>$proptype_xray,"total"=>$total,"distress"=>$total_D,"pctdistress"=>$pct_D,"preforeclosure"=>$total_P,"pctpreforeclosure"=>$pct_P,"foreclosure"=>$total_F,"pctforeclosure"=>$pct_F,"upsidedown"=>$total_UD,"pctupsidedown"=>$pct_UD,"sale"=>$total_sold,"pctsale"=>$pct_sold,"ownerY"=>$total_owner_Y,"pctownerY"=>$pct_owner_Y,"ownerN"=>$total_owner_N,"pctownerN"=>$pct_owner_N,"total_sold_P"=>$total_sold_P,"pcttotal_sold_P"=>$pct_sold_P,"total_no_sold_P"=>$total_no_sold_P,"pcttotal_no_sold_P"=>$pct_no_sold_P,"total_P_0"=>$total_P_0,"pcttotal_P_0"=>$pct_P_0,"total_P_0_mas"=>$total_P_0_mas,"pcttotal_P_0_mas"=>$pct_P_0_mas,"total_P_30"=>$total_P_30,"pcttotal_P_30"=>$pct_P_30,"total_sold_F"=>$total_sold_F,"pcttotal_sold_F"=>$pct_sold_F,"total_no_sold_F"=>$total_no_sold_F,"pcttotal_no_sold_F"=>$pct_no_sold_F,"total_sold_UD"=>$total_sold_UD,"pcttotal_sold_UD"=>$pct_sold_UD,"total_no_sold_UD"=>$total_no_sold_UD,"pcttotal_no_sold_UD"=>$pct_no_sold_UD,"total_sold_PE"=>$total_sold_PE,"pcttotal_sold_PE"=>$pct_sold_PE,"total_sold_PE_P"=>$total_sold_PE_P,"pcttotal_sold_PE_P"=>$pct_sold_PE_P,"total_sold_PE_F"=>$total_sold_PE_F,"pcttotal_sold_PE_F"=>$pct_sold_PE_F,"invM"=>$invM,"a_days"=>$a_days,"total_sold_3"=>$total_sold_3,"a_sold_3"=>$a_sold_3,"a_asp12"=>$a_asp12,"m_asp12"=>$m_asp12,"a_asp9"=>$a_asp9,"m_asp9"=>$m_asp9,"a_asp6"=>$a_asp6,"m_asp6"=>$m_asp6,"a_asp3"=>$a_asp3,"m_asp3"=>$m_asp3);
			
			$conex=conectar($nueva);
			include("xray.php");
			
			$array2=array("tipo"=>"BD2","data"=>"Nueva","proptype"=>$proptype_xray,"total"=>$total,"distress"=>$total_D,"pctdistress"=>$pct_D,"preforeclosure"=>$total_P,"pctpreforeclosure"=>$pct_P,"foreclosure"=>$total_F,"pctforeclosure"=>$pct_F,"upsidedown"=>$total_UD,"pctupsidedown"=>$pct_UD,"sale"=>$total_sold,"pctsale"=>$pct_sold,"ownerY"=>$total_owner_Y,"pctownerY"=>$pct_owner_Y,"ownerN"=>$total_owner_N,"pctownerN"=>$pct_owner_N,"total_sold_P"=>$total_sold_P,"pcttotal_sold_P"=>$pct_sold_P,"total_no_sold_P"=>$total_no_sold_P,"pcttotal_no_sold_P"=>$pct_no_sold_P,"total_P_0"=>$total_P_0,"pcttotal_P_0"=>$pct_P_0,"total_P_0_mas"=>$total_P_0_mas,"pcttotal_P_0_mas"=>$pct_P_0_mas,"total_P_30"=>$total_P_30,"pcttotal_P_30"=>$pct_P_30,"total_sold_F"=>$total_sold_F,"pcttotal_sold_F"=>$pct_sold_F,"total_no_sold_F"=>$total_no_sold_F,"pcttotal_no_sold_F"=>$pct_no_sold_F,"total_sold_UD"=>$total_sold_UD,"pcttotal_sold_UD"=>$pct_sold_UD,"total_no_sold_UD"=>$total_no_sold_UD,"pcttotal_no_sold_UD"=>$pct_no_sold_UD,"total_sold_PE"=>$total_sold_PE,"pcttotal_sold_PE"=>$pct_sold_PE,"total_sold_PE_P"=>$total_sold_PE_P,"pcttotal_sold_PE_P"=>$pct_sold_PE_P,"total_sold_PE_F"=>$total_sold_PE_F,"pcttotal_sold_PE_F"=>$pct_sold_PE_F,"invM"=>$invM,"a_days"=>$a_days,"total_sold_3"=>$total_sold_3,"a_sold_3"=>$a_sold_3,"a_asp12"=>$a_asp12,"m_asp12"=>$m_asp12,"a_asp9"=>$a_asp9,"m_asp9"=>$m_asp9,"a_asp6"=>$a_asp6,"m_asp6"=>$m_asp6,"a_asp3"=>$a_asp3,"m_asp3"=>$m_asp3);
			
			//echo '({"total":"2","results":['.json_encode($array).','.json_encode($array2).']})';
			mysql_query ('TRUNCATE TABLE xima.xray') or die (mysql_error ());
			$que='INSERT INTO xima.xray VALUES ("'.$array['tipo'].'","'.$array['data'].'","'.$array['proptype'].'",'.$array['total'].','.$array['distress'].','.$array['pctdistress'].','.$array['preforeclosure'].','.$array['pctpreforeclosure'].','.$array['foreclosure'].','.$array['pctforeclosure'].','.$array['upsidedown'].','.$array['pctupsidedown'].','.$array['sale'].','.$array['pctsale'].','.$array['ownerY'].','.$array['pctownerY'].','.$array['ownerN'].','.$array['pctownerN'].','.$array['total_sold_P'].','.$array['pcttotal_sold_P'].','.$array['total_no_sold_P'].','.$array['pcttotal_no_sold_P'].','.$array['total_P_0'].','.$array['pcttotal_P_0'].','.$array['total_P_0_mas'].','.$array['pcttotal_P_0_mas'].','.$array['total_P_30'].','.$array['pcttotal_P_30'].','.$array['total_sold_F'].','.$array['pcttotal_sold_F'].','.$array['total_no_sold_F'].','.$array['pcttotal_no_sold_F'].','.$array['total_sold_UD'].','.$array['pcttotal_sold_UD'].','.$array['total_no_sold_UD'].','.$array['pcttotal_no_sold_UD'].','.$array['total_sold_PE'].','.$array['pcttotal_sold_PE'].','.$array['total_sold_PE_P'].','.$array['pcttotal_sold_PE_P'].','.$array['total_sold_PE_F'].','.$array['pcttotal_sold_PE_F'].','.$array['invM'].','.$array['a_days'].','.$array['total_sold_3'].','.$array['a_sold_3'].','.$array['a_asp12'].','.$array['m_asp12'].','.$array['a_asp9'].','.$array['m_asp9'].','.$array['a_asp6'].','.$array['m_asp6'].','.$array['a_asp3'].','.$array['m_asp3'].'),("'.$array2['tipo'].'","'.$array2['data'].'","'.$array2['proptype'].'",'.$array2['total'].','.$array2['distress'].','.$array2['pctdistress'].','.$array2['preforeclosure'].','.$array2['pctpreforeclosure'].','.$array2['foreclosure'].','.$array2['pctforeclosure'].','.$array2['upsidedown'].','.$array2['pctupsidedown'].','.$array2['sale'].','.$array2['pctsale'].','.$array2['ownerY'].','.$array2['pctownerY'].','.$array2['ownerN'].','.$array2['pctownerN'].','.$array2['total_sold_P'].','.$array2['pcttotal_sold_P'].','.$array2['total_no_sold_P'].','.$array2['pcttotal_no_sold_P'].','.$array2['total_P_0'].','.$array2['pcttotal_P_0'].','.$array2['total_P_0_mas'].','.$array2['pcttotal_P_0_mas'].','.$array2['total_P_30'].','.$array2['pcttotal_P_30'].','.$array2['total_sold_F'].','.$array2['pcttotal_sold_F'].','.$array2['total_no_sold_F'].','.$array2['pcttotal_no_sold_F'].','.$array2['total_sold_UD'].','.$array2['pcttotal_sold_UD'].','.$array2['total_no_sold_UD'].','.$array2['pcttotal_no_sold_UD'].','.$array2['total_sold_PE'].','.$array2['pcttotal_sold_PE'].','.$array2['total_sold_PE_P'].','.$array2['pcttotal_sold_PE_P'].','.$array2['total_sold_PE_F'].','.$array2['pcttotal_sold_PE_F'].','.$array2['invM'].','.$array2['a_days'].','.$array2['total_sold_3'].','.$array2['a_sold_3'].','.$array2['a_asp12'].','.$array2['m_asp12'].','.$array2['a_asp9'].','.$array2['m_asp9'].','.$array2['a_asp6'].','.$array2['m_asp6'].','.$array2['a_asp3'].','.$array2['m_asp3'].'),("DIFF","DIFF","'.$array2['proptype'].'",'.($array['total']-$array2['total']).','.($array['distress']-$array2['distress']).','.($array['pctdistress']-$array2['pctdistress']).','.($array['preforeclosure']-$array2['preforeclosure']).','.($array['pctpreforeclosure']-$array2['pctpreforeclosure']).','.($array['foreclosure']-$array2['foreclosure']).','.($array['pctforeclosure']-$array2['pctforeclosure']).','.($array['upsidedown']-$array2['upsidedown']).','.($array['pctupsidedown']-$array2['pctupsidedown']).','.($array['sale']-$array2['sale']).','.($array['pctsale']-$array2['pctsale']).','.($array['ownerY']-$array2['ownerY']).','.($array['pctownerY']-$array2['pctownerY']).','.($array['ownerN']-$array2['ownerN']).','.($array['pctownerN']-$array2['pctownerN']).','.($array['total_sold_P']-$array2['total_sold_P']).','.($array['pcttotal_sold_P']-$array2['pcttotal_sold_P']).','.($array['total_no_sold_P']-$array2['total_no_sold_P']).','.($array['pcttotal_no_sold_P']-$array2['pcttotal_no_sold_P']).','.($array['total_P_0']-$array2['total_P_0']).','.($array['pcttotal_P_0']-$array2['pcttotal_P_0']).','.($array['total_P_0_mas']-$array2['total_P_0_mas']).','.($array['pcttotal_P_0_mas']-$array2['pcttotal_P_0_mas']).','.($array['total_P_30']-$array2['total_P_30']).','.($array['pcttotal_P_30']-$array2['pcttotal_P_30']).','.($array['total_sold_F']-$array2['total_sold_F']).','.($array['pcttotal_sold_F']-$array2['pcttotal_sold_F']).','.($array['total_no_sold_F']-$array2['total_no_sold_F']).','.($array['pcttotal_no_sold_F']-$array2['pcttotal_no_sold_F']).','.($array['total_sold_UD']-$array2['total_sold_UD']).','.($array['pcttotal_sold_UD']-$array2['pcttotal_sold_UD']).','.($array['total_no_sold_UD']-$array2['total_no_sold_UD']).','.($array['pcttotal_no_sold_UD']-$array2['pcttotal_no_sold_UD']).','.($array2['total_sold_PE']-$array['total_sold_PE']).','.($array['pcttotal_sold_PE']-$array2['pcttotal_sold_PE']).','.($array['total_sold_PE_P']-$array2['total_sold_PE_P']).','.($array['pcttotal_sold_PE_P']-$array2['pcttotal_sold_PE_P']).','.($array['total_sold_PE_F']-$array2['total_sold_PE_F']).','.($array['pcttotal_sold_PE_F']-$array2['pcttotal_sold_PE_F']).','.($array['invM']-$array2['invM']).','.($array['a_days']-$array2['a_days']).','.($array['total_sold_3']-$array2['total_sold_3']).','.($array['a_sold_3']-$array2['a_sold_3']).','.($array['a_asp12']-$array2['a_asp12']).','.($array['m_asp12']-$array2['m_asp12']).','.($array['a_asp9']-$array2['a_asp9']).','.($array['m_asp9']-$array2['m_asp9']).','.($array['a_asp6']-$array2['a_asp6']).','.($array['m_asp6']-$array2['m_asp6']).','.($array['a_asp3']-$array2['a_asp3']).','.($array['m_asp3']-$array2['m_asp3']).')';
			
			mysql_query ($que) or die ($que.mysql_error ());
			echo "{success:true}";
			return 0;
			
		break;
	}
	
	$result=mysql_query ($que) or die ($que.mysql_error ());
 
	$data = array();
	
	while ($row=mysql_fetch_object($result))
		$data [] = $row;
		
	$result2 = mysql_query("SELECT FOUND_ROWS();" ) or die (mysql_error());
	$num_row=mysql_fetch_row($result2);
	echo '({"total":"'.$num_row[0].'","results":'.json_encode($data).'})';
	//echo '({"total":"'.$num_row[0].'","results":'.json_encode($que).'})';
?>