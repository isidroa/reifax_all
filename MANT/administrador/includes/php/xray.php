<?php 
	//Generar el EXRAY del condado 
	$val=$proptype_xray;

	$total=$total_P=$total_F=$total_sold=$total_sold_P=0;
	$total_no_sold_P=$total_sold_F=$total_no_sold_F=0;
	$total_P_0=$total_P_10=$total_P_20=$total_P_30=0;
	$total_sold_3=$total_owner_Y=$total_owner_N=0;
	$total_UD=$total_no_sold_UD=$total_sold_UD=0;
	$total_sold_PE=$total_sold_PE_F=$total_sold_PE_P=0;
	$cs=$dom=0;
	$sp12=$sp9=$sp6=$sp3=0;
	$total_P_0_mas=0;
	$asp12=$asp9=$asp6=$asp3=array();
	
	$latlong=0;
	
	$data=array();
	
	
	$query="SELECT p.parcelid, p.astatus, r.closingdt, r.saleprice, mv.pendes, mv.ownocc, mv.debttv, mv.marketpctg, r.status, r.dom, ll.latitude, ll.longitude 
			FROM marketvalue mv  
			LEFT JOIN psummary p ON (p.parcelid=mv.parcelid) 
			LEFT JOIN rtmaster r ON (p.parcelid=r.parcelid) 
			LEFT JOIN latlong ll on (p.parcelid=ll.parcelid)";		
	
	if($val!=''){
		$query.=" WHERE p.ccode='$val'";
	}
	
	//echo $query;
	$actual=date('Ymd');
	$desde=date('Ymd',strtotime("-3 month"));
	$desde6=date('Ymd',strtotime("-6 month"));
	$desde9=date('Ymd',strtotime("-9 month"));
	$desde12=date('Ymd',strtotime("-12 month"));
	
	$result=mysql_query($query) or die ($query.' '.mysql_error());
	while($r=mysql_fetch_array($result))
	{
		$contar=true;
		if($contar)
		{
			$total++;
			if($r['pendes']=='F'){
				$total_F++;
				if($r['status']=='A')
					$total_sold_F++;
				else
					$total_no_sold_F++;
			}
			if($r['pendes']=='P'){
				$total_P++;
				if($r['status']=='A')
					$total_sold_P++;
				else
					$total_no_sold_P++;
				
				if($r['debttv']<0)
					$total_P_0++;
				if($r['debttv']>=0 && $r['debttv']<100)
					$total_P_0_mas++;
				if($r['debttv']>=30 && $r['debttv']<100)
					$total_P_30++;
				
			}
			if($r['pendes']=='N' && $r['debttv']<0){
				$total_UD++;
				if($r['status']=='A'){
					$total_sold_UD++;
				}else{
					$total_no_sold_UD++;
				}
			}
			if($r['status']=='A'){
				$total_sold++;
				if($r['marketpctg']>=30){
					$total_sold_PE++;
					if($r['pendes']=='F')
						$total_sold_PE_F++;
					if($r['pendes']=='P')
						$total_sold_PE_P++;
				}
			}
			
			if($r['status']=='CS' || $r['status']=='CC'){
				$cs++;
				$dom+=$r['dom'];
				if($r['closingdt']>=$desde && $r['closingdt']<=$actual && $r['saleprice']>=19000){
					$sp3++;
					$asp3[]=$r['saleprice'];
				}
				if($r['closingdt']>=$desde6 && $r['closingdt']<=$actual && $r['saleprice']>=19000){
					$sp6++;
					$asp6[]=$r['saleprice'];
				}
				if($r['closingdt']>=$desde9 && $r['closingdt']<=$actual && $r['saleprice']>=19000){
					$sp9++;
					$asp9[]=$r['saleprice'];
				}
				if($r['closingdt']>=$desde12 && $r['closingdt']<=$actual && $r['saleprice']>=19000){
					$sp12++;
					$asp12[]=$r['saleprice'];
				}
			}
			
			if($r['status']=='CS' || $r['status']=='CC'){
				if($r['closingdt']>=$desde && $r['closingdt']<=$actual && $r['saleprice']>=19000){
					$total_sold_3++;
				}
			}
			
			if($r['ownocc']=='Y' || $r['ownocc']=='y')
				$total_owner_Y++;
			if($r['ownocc']=='N' || $r['ownocc']=='n')
				$total_owner_N++;
		}
	}
	
	$total_D=$total_F+$total_P;
	if($total>0){
		$pct_D=porcentaje(($total_D*100)/$total);
		$pct_F=porcentaje(($total_F*100)/$total);
		$pct_P=porcentaje(($total_P*100)/$total);
		$pct_sold=porcentaje(($total_sold*100)/$total);
		$pct_owner_N=porcentaje(($total_owner_N*100)/$total);
		$pct_owner_Y=porcentaje(($total_owner_Y*100)/$total);
		$pct_UD=porcentaje(($total_UD*100)/$total);
	}else{
		$pct_UD=$pct_D=$pct_F=$pct_P=$pct_sold=$pct_owner_Y=$pct_owner_N=0.00;
	}
	
	if($total_F>0){
		$pct_sold_F=porcentaje(($total_sold_F*100)/$total_F);
		$pct_no_sold_F=porcentaje(($total_no_sold_F*100)/$total_F);
	}else{
		$pct_sold_F=$pct_no_sold_F=0.00;
	}
	
	if($total_P>0){
		$pct_sold_P=porcentaje(($total_sold_P*100)/$total_P);
		$pct_no_sold_P=porcentaje(($total_no_sold_P*100)/$total_P);
		$pct_P_0=porcentaje(($total_P_0*100)/$total_P);
		$pct_P_0_mas=porcentaje(($total_P_0_mas*100)/$total_P);
		$pct_P_30=porcentaje(($total_P_30*100)/$total_P);
	}else{
		$pct_sold_P=$pct_no_sold_P=$pct_P_0=$pct_P_0_mas=$pct_P_30=0.00;
	}
	
	if($total_UD>0){
		$pct_sold_UD=porcentaje(($total_sold_UD*100)/$total_UD);
		$pct_no_sold_UD=porcentaje(($total_no_sold_UD*100)/$total_UD);
	}else{
		$pct_sold_UD=$pct_no_sold_UD=0.00;
	}
	
	if($total_sold>0){
		$pct_sold_PE=porcentaje(($total_sold_PE*100)/$total_sold);
		$pct_sold_PE_F=porcentaje(($total_sold_PE_F*100)/$total_sold);
		$pct_sold_PE_P=porcentaje(($total_sold_PE_P*100)/$total_sold);
	}else{
		$pct_sold_PE=$pct_sold_PE_F=$pct_sold_PE_P=0.00;
	}
	
	$a_sold_3=round($total_sold_3/3,2);
	
	if($cs>0)
		$a_days=porcentaje($dom/$cs);
	else
		$a_days=0.00;
		
	
	sort($asp3,SORT_NUMERIC);
	sort($asp6,SORT_NUMERIC);
	sort($asp9,SORT_NUMERIC);
	sort($asp12,SORT_NUMERIC);
	
	if($sp3>0){
		$a_asp3=porcentaje(array_sum($asp3)/$sp3);
		if($sp3%2==0){ 
			$m=$sp3/2; 
			$m_asp3=porcentaje($asp3[$m]);
		}else{
			$m=ceil($sp3/2);
			$m_asp3=$asp3[$m];
			$m=floor($sp3/2);
			$m_asp3+=$asp3[$m];
			$m_asp3=porcentaje($m_asp3/2);
		}
		
	}else
		$a_asp3=$m_asp3=0.00;
		
	if($sp6>0){
		$a_asp6=porcentaje(array_sum($asp6)/$sp6);
		if($sp6%2==0){ 
			$m=$sp6/2; 
			$m_asp6=porcentaje($asp6[$m]);
		}else{
			$m=ceil($sp6/2);
			$m_asp6=$asp6[$m];
			$m=floor($sp6/2);
			$m_asp6+=$asp6[$m];
			$m_asp6=porcentaje($m_asp6/2);
		}
		
	}else
		$a_asp6=$m_asp6=0.00;
		
	if($sp9>0){
		$a_asp9=porcentaje(array_sum($asp9)/$sp9);
		if($sp9%2==0){ 
			$m=$sp9/2; 
			$m_asp9=porcentaje($asp9[$m]);
		}else{
			$m=ceil($sp9/2);
			$m_asp9=$asp9[$m];
			$m=floor($sp9/2);
			$m_asp9+=$asp9[$m];
			$m_asp9=porcentaje($m_asp9/2);
		}
		
	}else
		$a_asp9=$m_asp9=0.00;
		
	if($sp12>0){
		$a_asp12=porcentaje(array_sum($asp12)/$sp12);
		if($sp12%2==0){ 
			$m=$sp12/2; 
			$m_asp12=porcentaje($asp12[$m]);
		}else{
			$m=ceil($sp12/2);
			$m_asp12=$asp12[$m];
			$m=floor($sp12/2);
			$m_asp12+=$asp12[$m];
			$m_asp12=porcentaje($m_asp12/2);
		}
		
	}else
		$a_asp12=$m_asp12=0.00;
	
	if($total_sold_3>0)
		$invM=porcentaje($total_sold/$a_sold_3);
	else
		$invM=0.00;

	
?>