<script language="javascript">
Ext.namespace('Ext.combos_selec');

Ext.combos_selec.zones = [
        ['NORTH', 'North'],
        ['CENTER', 'Center'],
        ['CENTER2', 'Center 2'],
        ['SOUTH', 'South'],
        ['EAST', 'East'],
        ['WEST', 'West']
    ];
</script>