Ext.BLANK_IMAGE_URL='../LIB/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,idReg='';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
	var bd_sel='';tblD_sel='';campos='';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=traducciones',
		fields: [
			{name: 'traID', type: 'int'},
			'stcounty',
			'tabla',
			'tblResult',
			'campos',
			'consulta',
			'indice'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.bd=bd_sel;
					opt.params.tblDes=tblD_sel;
				}
			}
        }
	});
	
	var storeMap = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'includes/php/grid_data.php?tipo=mapeos',
		fields: [
			{name: 'mapID', type: 'int'},
			'tabla',
			'tblResult',
			'campo',
			'campop',
			'prioridad',
			'funcion',
			'descripcion'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.campos=campos;
				}
			}
        }
	});
	
	var storeHist = new Ext.data.JsonStore({
			root: 'results',
			url: 'includes/php/grid_data.php?tipo=bitacora&seccion=traduccion',
			fields: [
				{name: 'idbitacora', type: 'int'},
				'id_registro',
				'condado',
				'tabla',
				'campo',
				'old',
				'new',
				'usuario',
				'fecha',
				'hora',
				'descripcion',
				'accion'
			],
			listeners: {
				beforeload: {
					fn: function(store,opt){
						opt.params.idReg=idReg;
						idReg='';
					}
				}
			}
		});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
				 id: 'copy_butt',
                 tooltip: 'Click to Copy selected Rows',
				 iconCls:'icon',
				 icon: 'includes/img/copy.gif',
                 handler: doCopy 
                },
			{
				 id: 'history_button',
                 tooltip: 'Click to View History',
				 iconCls:'icon',
				 icon: 'includes/img/History.png',
                 handler: doHist 
                }	
			]
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "traduccion", 
					key: 'traID',
					keyID: oGrid_Event.record.data.traID,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doCopy(){
		var button=Ext.get('copy_butt');
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';
		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.traID;
		}
		selected+=')';
		
		if(selected=='()'){
			Ext.Msg.alert("Failure", "please select a translation to copy");
			return 0;
		}
		
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			url:'includes/php/grid_add.php',
			frame:true,
			title: 'Copy Translations',
			bodyStyle:'padding:5px 5px 0',
			width: 400,
			defaults: {width: 300},
			waitMsgTarget : 'Coping Rows...',
			
			items: [{
					xtype: 'combo',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'county'],
						data : Ext.combos_selec.bd
					}),
					editable: false,
					displayField:'county',
					valueField: 'id',
					name: 'county1',
					hiddenName: 'idcounty',
					fieldLabel: 'State/County*',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a State/County...',
					selectOnFocus:true,
					allowBlank:false
				},{
					xtype: 'hidden',
					name: 'ids',
					hiddenName: 'ids',
					value: selected
				},{
					xtype: 'hidden',
					name: 'tipo',
					hiddenName: 'tipo',
					value: 'mapeos'
				}
			],
	
			buttons: [{
				text: 'Save',
				handler  : function(){
                        simple.getForm().submit({
							success: function(form, action) {
								win.hide();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
                    }
			}]
		});
		
        if(!win){
            win = new Ext.Window({
                
                layout      : 'fit',
                width       : 500,
                height      : 300,
				modal	 	: true,
                closeAction : 'hide',
                plain       : true,
                items		: simple,

                buttons: [{
                    text     : 'Close',
                    handler  : function(){
						win.hide();
					}
                }]
            });
        }
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		}); 
	}
	
	////////////////////Bitacora////////////////////////////////////////////////////////////////
	
	function doEditHist(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'includes/php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "bitacora", 
					key: 'idbitacora',
					keyID: oGrid_Event.record.data.idbitacora,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					storeHist.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					storeHist.commitChanges();
				}                                     
			 }
		);  
	}
	
	function doHist(){
		var button = Ext.get('add_butt');
		var xg = Ext.grid;
		var selec = grid.selModel.getSelections();
		
		var pagingBarHist = new Ext.PagingToolbar({
			pageSize: 50,
			store: storeHist,
			displayInfo: true,
			displayMsg: '<b>Total: {2}</b>',
			emptyMsg: "No topics to display"        
        });	
		
		var expander = new Ext.ux.grid.RowExpander({
			tpl : new Ext.Template(
				'<div style="width:200px;background:#999999;color:#fff;padding:5px;"><p><b>Descripcion:</b> {descripcion}</p></div>'
			)
		});

		
	var gridHist = new Ext.grid.EditorGridPanel({
		store: storeHist,
		iconCls: 'icon',
		columns: [	
			expander,
			{header: 'Descripcion', width: 350, sortable: true, align: 'left',dataIndex:'descripcion',editor: new Ext.form.TextArea({allowBlank: false})}, 
			{header: 'Fecha', width: 85, sortable: true, align: 'left', dataIndex:'fecha'},
			{header: 'Hora', width: 85, sortable: true, align: 'left', dataIndex:'hora'},
			{header: 'Usuario', width: 75, sortable: true, align: 'center', dataIndex:'usuario'},
			{header: 'Campo', width: 75, sortable: true, align: 'center', dataIndex:'campo'},
			{header: 'Valor Anterior', width: 85, sortable: true, align: 'center',dataIndex:'old'},
			{header: 'Valor Nuevo', width: 85, sortable: true, align: 'center',dataIndex:'new'},
			{header: 'Tabla', width: 85, sortable: true, align: 'center',dataIndex:'tabla'},
			{header: 'Id Registro', width: 85, sortable: true, align: 'center',dataIndex:'id_registro'},
			{header: 'Condado', width: 85, sortable: true, align: 'center',dataIndex:'condado'},
			{header: 'Accion', width: 85, sortable: true, align: 'left',dataIndex:'accion'},
			{id:'ID Bitacora',header: "Bitacora ID", width: 80, align: 'center', sortable: true,dataIndex:'idbitacora'}
			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:450,
		rowHeight: 34,
		width: screen.width,
		frame:true,
		title:'Bitacora',
		loadMask:true,
        plugins: expander,
        collapsible: true,
		tbar: pagingBarHist 
	});
	
	gridHist.on('render',function(){
		if(selec.length==0){
				storeHist.load({params:{start:0, limit:200}});
		}else{
				idReg = selec[0].json.mapID;
				storeHist.load({params:{start:0, limit:200,idReg:idReg}});
		}
		
	});
	gridHist.addListener('afteredit', doEditHist);
	
		
		winHist = new Ext.Window({
			layout      : 'fit',
			autoWidth: true,
			height      : 450,
			modal	 	: true,
			plain       : true,
			items		: gridHist,

			buttons: [{
				text     : 'Close',
				handler  : function(){
					winHist.close();
				}
			}]
		});
        
        winHist.show(button);
	}
	
	////////////////////Fin Bitacora////////////////////////////////////////////////////////////////
	
	function doFilter(combo,record,index){
		bd_sel = combo.getValue();	
		store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel}});
	}
	
	function doFilter2(combo,record,index){
		tblD_sel = combo.getValue();
		store.load({params:{start:0, limit:200, bd:bd_sel, tblDes:tblD_sel}});
	}
	
	function doMapeo(sm, rowIdx, r){
		campos=r.data.campos;
		storeMap.load({params:{start:0, limit:2000, campos:campos}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: '../LIB/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			{id:'traID',header: "Translation ID", width: 80, align: 'center', sortable: true, dataIndex: 'traID'},
			{header: 'stcounty', width: 85, sortable: true, align: 'center', dataIndex: 'stcounty'},
			{header: 'Table Des.', width: 85, sortable: true, align: 'center', dataIndex: 'tabla'},
			{header: 'Table Ori.', width: 85, sortable: true, align: 'center', dataIndex: 'tblResult'},
			{header: 'Fields', width: 85, sortable: true, align: 'center', dataIndex: 'campos',hidden:true},
			{header: 'Query', width: 85, sortable: true, align: 'center', dataIndex: 'consulta', editor: new Ext.form.ComboBox({
               typeAhead: true,
               triggerAction: 'all',
               store: new Ext.data.SimpleStore({
							fields: ['id', 'tipo'],
							data : [
							['insert', 'Insert'],
							['update/insert', 'Update/Insert'],
							['updates', 'Update'],
							['no', 'No Action']
						]
					}),
			   mode: 'local',
			   triggerAction: 'all',
			   editable: false,
			   selectOnFocus:true,
			   displayField:'tipo',
			   valueField: 'id',
               listClass: 'x-combo-list-small'
            })},
			{header: 'Index', width: 85, sortable: true, align: 'center', dataIndex: 'indice',editor: new Ext.form.TextField({allowBlank: false})}
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:300,
		width: screen.width,
		frame:true,
		title:'Translations',
		loadMask:true,
		
		tbar: pagingBar 

	});
	
	var gridMap = new Ext.grid.GridPanel({
		store: storeMap,
		iconCls: 'icon',
		columns: [	  
			{id:'mapID',header: "Mapeo ID", width: 80, align: 'center', sortable: true, dataIndex: 'mapID'},
			{header: 'Tbl Destino', width: 85, sortable: true, align: 'center', dataIndex: 'tabla',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Tbl Origen', width: 85, sortable: true, align: 'center', dataIndex: 'tblResult',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Cmp Destino', width: 85, sortable: true, align: 'center', dataIndex: 'campo',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Cmp Origen', width: 85, sortable: true, align: 'center', dataIndex: 'campop',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Prioridad', width: 75, sortable: true, align: 'center', dataIndex: 'prioridad',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Funcion', width: 250, sortable: true, align: 'left', dataIndex: 'funcion',editor: new Ext.form.TextField({allowBlank: false})},
			{header: 'Description', width: 200, sortable: true, align: 'left', dataIndex: 'descripcion',editor: new Ext.form.TextField({allowBlank: false})}
		],
		sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		height:200,
		width: screen.width,
		//autoHeight: true,
		frame:true,
		title:'Mappings of Selected Translation',
		loadMask:true 
	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',
				{
                 xtype: 'combo',
				 id: 'county1',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'county'],
		 			data : Ext.combos_selec.bd
		   		 }),
				 editable: false,
				 displayField:'county',
				 valueField: 'id',
				 name: 'county1',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a State/County...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter
					 }
                },
			'-',{
                 xtype: 'combo',
				 id: 'tblDest',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'tabla'],
		 			data : Ext.combos_selec.tblDestino
		   		 }),
				 editable: false,
				 displayField:'tabla',
				 valueField: 'id',
				 name: 'tblDest',
				 hiddenName: 'tblID',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a Tbl Destino...',
				 selectOnFocus:true,
				 allowBlank:true,
				 listeners: {
					 'select': doFilter2
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: [grid,gridMap]
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
	grid.getSelectionModel().on('rowselect', doMapeo);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});