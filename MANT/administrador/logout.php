<?php  
ob_start ();  
ini_set("session.gc_maxlifetime", 18000); 
session_start ();  
session_unset ();  
session_destroy ();  
header ("Location: index.php");  
ob_end_flush ();  
?> 