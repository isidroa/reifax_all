<?php
	class connect {
		public $conMaster;
		var $ipMasterLocal	=	'reifaxcalculo.cdhpwwjssf1c.us-west-2.rds.amazonaws.com';
		var $ipMasterWan	=	'reifaxcalculo.cdhpwwjssf1c.us-west-2.rds.amazonaws.com';
		public $params			=	array(
			'dataBase'			=>	'xima',
			'typeIp'			=>	'local',
			'mysqlType'			=>	'root',
			'master'			=>	FALSE,
			'ftpType'			=>	'XIMA',
			'MySQLtimeZone'		=>	'US/Eastern',
			'mapeoWithMaster'	=>	FALSE,
			'activeLogErrors'	=>	TRUE,
			'downloadFiles'		=>	TRUE,
			'hideAllMessages'	=>	TRUE,
			'timeExecution'		=>	'',
			'note'				=>	'',
			'optionalConex'		=>	'',
			'options'			=>	array()
		);

		public function connect($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	(object)$default;

			$this->connectInit();
			if($default->downloadFiles){
				$this->downloadFiles($default);
			}

			return true;
		}
		public function mysqlByIp($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);

			$ipToUse	=	$this->getIp($default->typeIp);
			if($default->typeIp=='local')
				$where	=	"a.serverIpLocal='{$ipToUse}'";
			else
				$where	=	"a.serverIpWan='{$ipToUse}'";

			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers
				WHERE
					{$where} AND
					b.mysqlType='{$default->mysqlType}'";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$result	=	$result->fetch_object();

			return	$this->mysqlConnect(
				($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan),
				$result->mysqlUser,
				$result->mysqlPass,
				$result->mysqlPort,
				$default->dataBase,
				$default->options
			);
		}
		public function mysqlByServer($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);

			if($default->master===false)
				$where	=	"a.serverNumber='{$default->serverNumber}'";
			else
				$where	=	"a.master=1";

			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers
				WHERE
					{$where} AND
					b.mysqlType='{$default->mysqlType}'";
			if(!$result	=	$this->conMaster->query($query)){
				echo 'Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error;
				exit;
			}
			$result	=	$result->fetch_object();
			return	$this->mysqlConnect(
				($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan),
				$result->mysqlUser,
				$result->mysqlPass,
				$result->mysqlPort,
				$default->dataBase,
				$default->options
			);
		}
		public function ftpGetData($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			$data		=	new StdClass();

			if($default->master===false){
				$where	=	"a.serverNumber='{$default->serverNumber}'";
			}else{
				$where	=	"a.master=1";
			}

			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a
					INNER JOIN mantenimiento.servers_ftp b on a.id_servers=b.id_servers
				WHERE
					{$where} AND
					b.ftpType='{$default->ftpType}'";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$result	=	$result->fetch_object();
			$data->ftpServer=	($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan);
			$data->ftpUser	=	$result->ftpUser;
			$data->ftpPass	=	$result->ftpPass;
			$data->ftpPort	=	$result->ftpPort;
			$data->ftpDir	=	$result->ftpRemoteDir;
			return	$data;
		}
		public function connectInit($optional=array()){	//Conection with Master Server
			$mysqli	=	mysqli_init();
			if(count($optional)>0){
				$this->addOptionals($mysqli,$optional);
				$this->options=array();
			}
			$mysqli->real_connect($this->ipMasterLocal,'masterdb','er1426xy!','xima');
			if($mysqli->connect_errno){
				die('Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
			}
			$mysqli->query('SET NAMES UTF8');
			$mysqli->query("SET time_zone='{$this->params['MySQLtimeZone']}'");
			$this->conMaster = $mysqli;
		}
		public function checkMaster($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	(object)$default;
			$query	=	'SELECT * FROM mantenimiento.servers_conexions WHERE '.($default->typeIp=='local'	?	'serverIpLocal'	:	'serverIpWan').'="'.$this->getIp($default->typeIp).'" AND serverNumber="UMath"';
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			return ($result->num_rows>0	?	true	:	false);
		}
		public function checkExtractor($optional=array()){
			//$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	(object)$default;
			$query	=	'SELECT * FROM mantenimiento.servers_conexions WHERE '.($default->typeIp=='local'	?	'serverIpLocal'	:	'serverIpWan').'="'.$this->getIp($default->typeIp).'" AND extractor=2';
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			return ($result->num_rows>0	?	true	:	false);
		}
		public function mapeoServers($optional=array()){	//Conection with Mapeo Server's
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			if($default->mapeoWithMaster===false)
				$where	=	"AND a.`master`<>1";
			else
				$where	=	'';
			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers
				WHERE a.mapeo='1' {$where}";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$connections	=	array();
			while($row	=	$result->fetch_object()){
				$connections[]=$this->mysqlConnect(
					($default->typeIp=='local'	?	$row->serverIpLocal	:	$row->serverIpWan),
					$row->mysqlUser,
					$row->mysqlPass,
					$row->mysqlPort,
					$default->dataBase,
					$default->options
				);
			}
			return $connections;
		}
		private function mysqlConnect($serverIp,$serverUser,$serverPass,$serverPort,$dataBase,$optional=array()){
			$mysqli	=	mysqli_init();
			if(count($optional)>0){
				$this->addOptionals($mysqli,$optional);
				$this->options=array();
			}
			$mysqli->real_connect($serverIp, $serverUser, $serverPass, $dataBase, $serverPort);
			if($mysqli->connect_errno){
				die('Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
			}
			$mysqli->query('SET NAMES UTF8');
			$mysqli->query("SET time_zone='{$this->params['MySQLtimeZone']}'");
			return $mysqli;
		}
		public function getIp($typeIp){
			$ip="";
			/*if($typeIp=='local'){
				$exec		=	exec("hostname"); //the "hostname" is a valid command in both windows and linux
				$hostname	=	trim($exec); //remove any spaces before and after
				$ip			=	gethostbyname($hostname); //resolves the hostname using local hosts resolver or DNS
			}else
				$ip	=	$_SERVER['LOCAL_ADDR'];*/
			if($typeIp=='local'){
				/*$ipRes = shell_exec('ipconfig');
				$ipPattern = '/IPv4 Address[^:]+: ' .
				   '([\d]{1,3}\.[\d]' .
				   '{1,3}\.[\d]{1,3}' .
				   '\.[\d]{1,3})/';
				if (preg_match_all($ipPattern, $ipRes, $matches)) {
					foreach($matches[1] as $value){
						if(strpos($value,'192.168.1.')!==false){
							$ip=$value;
							break;
						}
					}
				}
				if(empty($ip)){
					$command	=	"/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'";
					$ip = exec ($command);
				}*/
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					$ipRes = shell_exec('ipconfig');
					$ipPattern = '/IPv4 Address[^:]+: ' .
					   '([\d]{1,3}\.[\d]' .
					   '{1,3}\.[\d]{1,3}' .
					   '\.[\d]{1,3})/';
					if (preg_match_all($ipPattern, $ipRes, $matches)) {
						foreach($matches[1] as $value){
							if(strpos($value,'192.168.1.')!==false){
								$ip=$value;
								break;
							}
						}
					}
				}else{
					$command	=	"/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'";
					$ip = exec ($command);
				}
				if(empty($ip)){
					die(PHP_EOL.'Can"t resolve local ip'.PHP_EOL);
				}
			}else{
				/*$ip=@file_get_contents('http://bot.whatismyipaddress.com/');
				if(empty($ip)){
					$ip=@file_get_contents('http://icanhazip.com/');
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://myexternalip.com/raw');
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://ip.appspot.com/');
				}
				if(empty($ip)){
					$ip=preg_replace('/[\D\.]/','',@file_get_contents('http://checkip.dyndns.org/'));
				}*/
				if(preg_match('/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/',@file_get_contents('http://whatismyv6.com/'),$results)){
					$ip=$results[0];
				}
				if(preg_match('/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/',@file_get_contents('http://ip4.me/'),$results)){
					$ip=$results[0];
				}
				if(preg_match('/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/',@file_get_contents('http://ip6.me/'),$results)){
					$ip=$results[0];
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://phihag.de/ip/');
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://icanhazip.com/');
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://myexternalip.com/raw');
				}
				if(empty($ip)){
					$ip=@file_get_contents('http://ip.appspot.com/');
				}
				if(empty($ip)){
					$ip=preg_replace('/[\D\.]/','',@file_get_contents('http://checkip.dyndns.org/'));
				}
				if(empty($ip)){
					$ip=$ip=@file_get_contents('http://bot.whatismyipaddress.com/');
				}
			}
			return $ip;
		}
		public function getServerInfo($optional=array()){
			$this->resetOptions();
			$default=	array_merge($this->params,$optional);
			$default=	json_decode(json_encode($default),FALSE);
			$query	=	'SELECT * FROM mantenimiento.servers_conexions WHERE '.($default->typeIp=='local'	?	'serverIpLocal'	:	'serverIpWan').'="'.$this->getIp($default->typeIp).'"';
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			return $result->fetch_object();
		}
		public function resetOptions(){
			$this->params	=	array(
				'dataBase'			=>	'xima',
				'typeIp'			=>	'local',
				'mysqlType'			=>	'root',
				'master'			=>	FALSE,
				'ftpType'			=>	'XIMA',
				'MySQLtimeZone'		=>	'US/Eastern',
				'mapeoWithMaster'	=>	FALSE,
				'activeLogErrors'	=>	TRUE,
				'downloadFiles'		=>	TRUE,
				'hideAllMessages'	=>	TRUE,
				'timeExecution'		=>	'',
				'note'				=>	'',
				'optionalConex'		=>	'',
				'options'			=>	array()
			);
		}
		private function addOptionals(&$mysqli,$optional){
			foreach($optional as $option){
				$mysqli->options($option->name,$option->value);
			}
		}
		public function downloadFiles($options=array()){
			if($this->checkMaster()==FALSE){
				$ftpData	=	$this->ftpGetData(array('typeIp'=>'WAN','serverNumber'=>'UMath','ftpType'=>'HTDOCS'));

				$id_ftp=ftp_connect($ftpData->ftpServer,$ftpData->ftpPort) or die(PHP_EOL .'FTP: No se pudo conectar al Servidor '.$ftpData->ftpServer);	//Obtiene un manejador del Servidor FTP
				if(!ftp_login($id_ftp,$ftpData->ftpUser,$ftpData->ftpPass)){		//Se loguea al Servidor FTP
					die(PHP_EOL .'FTP: No se pudo Logear al Servidor '.$ftpData->ftpServer);
				}
				ftp_pasv($id_ftp,true);
				if (!ftp_chdir($id_ftp, 'mant/classes')) {
					die('No se pudo cambiar al directorio');
				}
				foreach(ftp_nlist($id_ftp, '.') as $file){
					if(!in_array($file,array('connection.class.php'))){
						if(is_dir('/var/www/mant/classes/')){
							$localDir='/var/www/mant/classes/';
						}
						else if(is_dir('/var/www/html/mant/classes/')){
							$localDir='/var/www/mant/classes/';
						}
						else{
							$localDir='C:/inetpub/wwwroot/mant/classes/';
						}
						if(!ftp_get($id_ftp,$localDir.$file,$file,FTP_BINARY)){
							die('Error get File FTP');
						}else{
							if($options->hideAllMessages==FALSE){
								echo PHP_EOL .'File: '.$file.' was imported successfuly';
							}
						}
					}
				}
				ftp_close($id_ftp);
			}
			if($this->checkExtractor()){
				echo "down extractor files";
				$ftpData	=	$this->ftpGetData(array('typeIp'=>'WAN','serverNumber'=>'Extractor1','ftpType'=>'HTDOCS'));
				$id_ftp=ftp_connect($ftpData->ftpServer,$ftpData->ftpPort) or die(PHP_EOL .'FTP: No se pudo conectar al Servidor '.$ftpData->ftpServer);	//Obtiene un manejador del Servidor FTP
				if(!ftp_login($id_ftp,$ftpData->ftpUser,$ftpData->ftpPass)){		//Se loguea al Servidor FTP
					die(PHP_EOL .'FTP: No se pudo Logear al Servidor '.$ftpData->ftpServer);
				}
				ftp_pasv($id_ftp,true);
				if (!ftp_chdir($id_ftp, 'mant/scraping')) {
					die('No se pudo cambiar al directorio');
				}
				foreach(ftp_nlist($id_ftp, '.') as $file){
					if(!in_array($file,array('connection.class.php'))){
						if(is_dir('/var/www/mant/scraping/')){
							$localDir='/var/www/mant/scraping/';
						}
						else if(is_dir('/var/www/html/mant/scraping/')){
							$localDir='/var/www/html/mant/scraping/';
						}
						else{
							$localDir='C:/inetpub/wwwroot/mant/scraping/';
						}
						if(!ftp_get($id_ftp,$localDir.$file,$file,FTP_BINARY)){
							die('Error get File FTP');
						}else{
							if($options->hideAllMessages==FALSE){
								echo PHP_EOL .'File: '.$file.' was imported successfuly';
							}
						}
					}
				}
				ftp_close($id_ftp);
			}
		}
		public function ObtainRealServer($options=array()){
			$options	=	(object)$options;
			$query		=	"SELECT SQL_NO_CACHE serverFiles,serverData FROM xima.lscounty WHERE bd='{$options->bd}'";
			$result		=	$this->conMaster->query($query);
			$dataQuery	=	$result->fetch_assoc();
			$result->free_result();
			$conI		=	$this->mysqlByServer(array('typeIp'=>'WAN','serverNumber'=>$dataQuery['serverData']));
			return	array(
				'conI'			=>	$conI,
				'serverFiles'	=>	$dataQuery['serverFiles'],
				'serverData'	=>	$dataQuery['serverData']
			);
		}
	}
