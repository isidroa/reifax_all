<?php
	//class download extends connect {
	class managerDownload {
		var $seccon		=	null;
		var $con		=	null;
		var $petitions	=	array();
		var $index		=	0;
		var $ctrlProxy	=	0;
		var $response	=	array();
		var $parcelid	=	array();
		function __construct($optional=array()){
			return true;
		}
		function init(){
			$this->con		=	curl_multi_init();
			$this->petitions=	array();
			$this->index	=	0;
			$this->response	=	array();
			$this->parcelid	=	array();
		}
		function stratSession($options=array()){
			$mostrar=false;
			$options=array_merge(
				array(
					'cookie'=>'',
					'isMobile'	=> false,
					'post'=>'FALSO',
					'referer'=>'FALSO',
					'notProxy'=>FALSE,
					'CURLOPT_TIMEOUT'=>FALSE,
					'CURLOPT_COOKIEJAR'=>FALSE,
					'CURLOPT_CONNECTTIMEOUT_MS'=>FALSE,
					'CURLOPT_RETURNTRANSFER'=>TRUE,
					'CURLOPT_FOLLOWLOCATION'=>FALSE,
					'CURLOPT_SSL_VERIFYHOST'=>FALSE,
					'CURLOPT_SSL_VERIFYPEER'=>FALSE
				),
				$options
			);
			$options	=	(object)$options;
			
			$this->seccon = curl_init();
			
			if(!$options->isMobile){
				curl_setopt($this->seccon, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1');
			}
			else{
				curl_setopt($this->seccon, CURLOPT_USERAGENT, 'Mozilla/5.0 (Android; Mobile; rv:22.0) Gecko/22.0 Firefox/22.0');
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_TIMEOUT!=FALSE){
				curl_setopt($this->seccon, CURLOPT_TIMEOUT, $options->CURLOPT_TIMEOUT);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_CONNECTTIMEOUT_MS==TRUE){
				curl_setopt($this->seccon,CURLOPT_CONNECTTIMEOUT_MS, 200);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_RETURNTRANSFER==TRUE){
				curl_setopt($this->seccon, CURLOPT_RETURNTRANSFER, true);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_FOLLOWLOCATION==TRUE){
				curl_setopt($this->seccon, CURLOPT_FOLLOWLOCATION, true);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_COOKIEJAR==TRUE){
				$cookieFile = '/var/temp/curlcookie'.$this->index.'.txt';
				curl_setopt($this->seccon, CURLOPT_COOKIEJAR, $cookieFile);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->cookie!=''){
				curl_setopt($this->seccon, CURLOPT_COOKIE, $options->cookie);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->referer!="FALSO"){
				$options->referer		=	str_replace("{%ParcelID%}",$options->dato,$options->referer);
				curl_setopt($this->seccon, CURLOPT_REFERER, $options->referer);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_SSL_VERIFYHOST==TRUE){
				curl_setopt($this->seccon, CURLOPT_SSL_VERIFYHOST, false);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
			if($options->CURLOPT_SSL_VERIFYPEER==TRUE){
				curl_setopt($this->seccon, CURLOPT_SSL_VERIFYPEER, false);
			}
			if($mostrar){
				echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');
			}
		}
		function getHtml($options=array()){
			$mostrar=false;
			$options	=	(object)$options;
			if($mostrar){
				echo '<pre>';print_r($options);echo '</pre>';
			}
			
			$proxy=$this->getProxy(
				array( 
					'county'	=>$options->condado,
					'conMaster'	=>$options->conMaster
				)
			);
			if($mostrar){
				echo '<pre>';print_r($proxy);echo '</pre>';
			}
			if(preg_match('/http:/',$options->url)){
				curl_setopt($this->seccon, CURLOPT_PROXYTYPE, 'HTTP');
			}else{
				curl_setopt($this->seccon, CURLOPT_PROXYTYPE, 'HTTPS');
				if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
				curl_setopt($this->seccon, CURLOPT_SSL_VERIFYHOST, false);
				if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
				curl_setopt($this->seccon, CURLOPT_SSL_VERIFYPEER, false);
			}
			if(isset($options->post)){
                $countPost  =   count(explode('&',$options->post));
                curl_setopt($this->seccon,CURLOPT_POST, $countPost);
                curl_setopt($this->seccon,CURLOPT_POSTFIELDS, $options->post);
            }

			curl_setopt($this->seccon, CURLOPT_PROXYPORT, $proxy->port);
			curl_setopt($this->seccon, CURLOPT_PROXY, $proxy->url);
			curl_setopt($this->seccon, CURLOPT_PROXYUSERPWD, $proxy->user.':'.$proxy->pass);
			curl_setopt($this->seccon, CURLOPT_URL, $options->url);
			$result = curl_exec($this->seccon);
			return $result;
		}
		function closeSession(){
			curl_close($this->seccon);
		}
		function getProxy($options=array()){
			$options	=	(object)$options;
			if($options->county!=''){
				$sql="select IdCounty id from xima.lscounty where BD='{$options->county}';";
				if(!$result=$options->conMaster->query($sql)){
					die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
				}
				if($result->num_rows<=0){
					$sql="select id from xima.data_servers_other where name='{$options->county}';";
					if(!$result=$options->conMaster->query($sql)){
						die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
					}
				}
				if($result){
					if($result->num_rows>0){
						$result=$result->fetch_object();
						$sql="SELECT
									p.*,if(b.count is null,0,b.count) as total
								FROM
									xima.data_servers_proxys p
										left join
									xima.data_servers_proxy_nc e ON p.id = e.idProxy  AND e.idCounty ={$result->id}
										left join
									xima.data_servers_proxys_count b ON b.idProxy = p.id AND b.idCounty={$result->id}
								where e.idProxy is null
								order by b.count asc LIMIT 1";
						if(!$result1=$options->conMaster->query($sql)){
							die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
						}
						$data=$result1->fetch_object();
						$sql="
							REPLACE INTO xima.data_servers_proxys_count
								SELECT '{$result->id}-{$data->id}',{$result->id},{$data->id},if(count is null,1,MAX(count)+1) FROM xima.data_servers_proxys_count WHERE idCounty='{$result->id}'";
						$temp=0;
						//$msc=microtime(true);
						do{
							if(!$inserto=$options->conMaster->query($sql)){
								$temp++;
								echo PHP_EOL .'Error Insert Control Proxy Intent N: '.$temp;
								if($temp>=20){
									die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
								}
								//usleep(100000);
							}
						}while(!$inserto);
						//$msc=microtime(true)-$msc;
						//echo PHP_EOL .($msc*1000).' milliseconds'; 
					}else{
						$data=$result->fetch_object();
						$sql="SELECT
								*
							FROM
								xima.data_servers_proxys
							order by rand() LIMIT 1";
						if(!$result=$options->conMaster->query($sql)){
							die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
						}
						$data=$result->fetch_object();
					}
				}else{
					$sql="SELECT * FROM xima.data_servers_proxys
							order by rand() LIMIT 1";
					if(!$result=$options->conMaster->query($sql)){
						die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
					}
					$data=$result->fetch_assoc();
				}
			}else{
				$sql="SELECT
							*
						FROM
							xima.data_servers_proxys
						order by rand() LIMIT 1";
				var_dump($options);
				if(!$result=$options->conMaster->query($sql)){
					die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
				}
				$data=$result->fetch_object();
				$sql="
					UPDATE xima.data_servers_proxys
						SET count=count+1
					WHERE id={$data->id}";
				if(!$options->conMaster->query($sql)){
					die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $options->conMaster->errno . ') ' . $options->conMaster->error);
				}
			}
			return $data;
		}
		function add($options){
			$options=array_merge(
				array(
					'cookie'=>'',
					'post'=>'FALSO',
					'referer'=>'FALSO',
					'notProxy'=>FALSE,
					'CURLOPT_TIMEOUT'=>TRUE,
					'CURLOPT_COOKIEJAR'=>TRUE,
					'CURLOPT_CONNECTTIMEOUT_MS'=>FALSE,
					'CURLOPT_RETURNTRANSFER'=>TRUE,
					'CURLOPT_FOLLOWLOCATION'=>TRUE,
					'CURLOPT_SSL_VERIFYHOST'=>TRUE,
					'CURLOPT_SSL_VERIFYPEER'=>TRUE
				),
				$options
			);
			$options	=	(object)$options;
			
			if(in_array($options->condado,array('flcolliera'))){	
				$mostrar=TRUE;
			}else{	$mostrar=FALSE;	}
			if($mostrar){	echo '<pre>';print_r($options);echo '</pre>';	}
			$proxy	=	$this->getProxy(
				array(
					'county'=>$options->condado,
					'conMaster'=>$options->conMaster
				)
			);
			if($mostrar){	echo PHP_EOL .'Agregado al Proxy: '.$proxy->url.' --> '.date('d-m-Y H:i:s  u');	}
			$this->petitions[$this->index]=curl_init($options->url);
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			curl_setopt($this->petitions[$this->index], CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10 Gecko/20100101 Firefox/15.0.1');
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->CURLOPT_TIMEOUT!=FALSE){
				curl_setopt($this->petitions[$this->index], CURLOPT_TIMEOUT, $options->CURLOPT_TIMEOUT);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->CURLOPT_CONNECTTIMEOUT_MS!==FALSE){
				curl_setopt($this->petitions[$this->index],CURLOPT_CONNECTTIMEOUT_MS, $options->CURLOPT_CONNECTTIMEOUT_MS);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->CURLOPT_RETURNTRANSFER==TRUE){
				curl_setopt($this->petitions[$this->index], CURLOPT_RETURNTRANSFER, true);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->CURLOPT_FOLLOWLOCATION==TRUE){
				curl_setopt($this->petitions[$this->index], CURLOPT_FOLLOWLOCATION, true);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->CURLOPT_COOKIEJAR==TRUE){
				$cookieFile = "/var/temp/curlcookie".$this->index.".txt";
				curl_setopt($this->petitions[$this->index], CURLOPT_COOKIEJAR, $cookieFile);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if(!empty($options->cookie)){
				curl_setopt($this->petitions[$this->index], CURLOPT_COOKIE, $options->cookie);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->post!='FALSO'){
				$countPost	=	count(explode('&',$options->post));
				$options->post		=	str_replace("{%ParcelID%}",$options->dato,$options->post);
				curl_setopt($this->petitions[$this->index],CURLOPT_POST, $countPost);
				curl_setopt($this->petitions[$this->index],CURLOPT_POSTFIELDS, $options->post);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($options->referer!='FALSO'){
				$options->referer		=	str_replace("{%ParcelID%}",$options->dato,$options->referer);
				curl_setopt($this->petitions[$this->index], CURLOPT_REFERER, $options->referer);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if(preg_match('/https:/',$options->url)){
				if($options->CURLOPT_SSL_VERIFYHOST==TRUE){
					curl_setopt($this->petitions[$this->index], CURLOPT_SSL_VERIFYHOST, false);
				}
				if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
				if($options->CURLOPT_SSL_VERIFYPEER==TRUE){
					curl_setopt($this->petitions[$this->index], CURLOPT_SSL_VERIFYPEER, false);
				}
			}
			
			if(!$options['notProxy']){
				if(preg_match('/http:/',$options->url)){
					curl_setopt($this->petitions[$this->index], CURLOPT_PROXYTYPE, 'HTTP');
				}else{
					curl_setopt($this->petitions[$this->index], CURLOPT_PROXYTYPE, 'HTTPS');
					if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
					if($options->CURLOPT_SSL_VERIFYHOST==TRUE){
						curl_setopt($this->petitions[$this->index], CURLOPT_SSL_VERIFYHOST, false);
					}
					if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
					if($options->CURLOPT_SSL_VERIFYPEER==TRUE){
						curl_setopt($this->petitions[$this->index], CURLOPT_SSL_VERIFYPEER, false);
					}
				}
				curl_setopt($this->petitions[$this->index], CURLOPT_PROXYPORT, $proxy->port);
				curl_setopt($this->petitions[$this->index], CURLOPT_PROXY, $proxy->url);
				curl_setopt($this->petitions[$this->index], CURLOPT_PROXYUSERPWD, $proxy->user.':'.$proxy->pass);
			}
			if($mostrar){	echo PHP_EOL .'LINE: '. __LINE__ .' Time: '.date('d-m-Y H:i:s  u');	}
			if($mostrar){	echo '<pre>';print_r($options);echo '</pre>';	}
			curl_multi_add_handle ($this->con,$this->petitions[$this->index]);
			$this->parcelid[$this->index]=$options->dato;
			$this->index++;
			if($mostrar){	echo PHP_EOL .'fin agregar paquete descarga: '.date('d-m-Y H:i:s  u');	}
		}
		function execute($options){
			//global $conMaster,$con,$Errors,$bd;
			$options	=	(object)$options;
			$timeIni	=	time();
			do {
				curl_multi_exec($this->con,$active);
				curl_multi_select($this->con);
			} while ($active);
			$timeEnd=time();
			/*if(($timeEnd-$timeIni)>5){
				$Errors->pasRegisterControl(
					array(
						'optionalConex'=>$options->conMaster,
						'dataBase'=>$options->bd,
						'line'=>__LINE__,
						'file'=>__FILE__,
						'parcelid'=>implode(',',$this->parcelid),
						'timeExecution'=>($timeEnd-$timeIni)
					)
				);
			}*/

			for($i =0;	$i<$this->index;	$i++) {
				$this->response[$i]=curl_multi_getcontent($this->petitions[$i]);
				curl_multi_remove_handle($this->con,$this->petitions[$i]);
			}
			curl_multi_close($this->con);
		}
        function additionalSpiders($options){
            $options    =   (object)$options;
            switch($options->condado){
                case "flhernando":
    
                    $fields = array(
                        'AcreageTractSize'=>'',     'Block'=>'',                            'GoToPage'=>'',     'H_Parcel'=>'1',        'HeatedSF_From'=>'',
                        'HeatedSF_To'=>'',          'LEGAL'=>'',                            'L_Parcel'=>'1',    'Lot'=>'',
                        'Neighborhood_Code'=>'',    'OwnerName'=>'',                        'PARCELID'=>'',     'PIN'=>$ParcelId,       'PageCount'=>'50',
                        'Range'=>'',                'SaleBook'=>'',                         'SaleDateFrom'=>'', 'SaleDateTo'=>'',
                        'SalePage'=>'',             'SalePriceFrom'=>'',                    'SalePriceTo'=>'',  'Sale_Vimp'=>'',        'SearchMenu'=>'',
                        'Section'=>'',              'StreetName'=>'',                       'StreetNumber'=>'', 'StreetType'=>'',
                        'Subd'=>'',                 'TPP_BusinessName'=>'',                 'TPP_Key'=>'',  'TPP_PIN'=>'',          'Z_Parcel'=>'1',
                        'TabView'=>'',              'Township'=>'',                         'Use_Code'=>'',     'YearBuilt_From'=>'',   'YearBuilt_To'=>'',
                        'bHandoff'=>'',             'button_Search'=>'Run Basic Search >>', 'windowHeight'=>'619',                      'windowWidth'=>'1846'
                    );
                    
                    $fields_string  =   "";
                    foreach($fields as $key=>$value){
                        $fields_string .= $key.'='.$value.'&';
                    }
                    rtrim($fields_string, '&');
                    $this->stratSession();
                    $htmlGoogle =   $this->getHtml(array('url'    =>  $options->url, 'conMaster' => $options->conMaster, 'condado' => $options->condado, 'post' => $fields_string));
                    $this->closeSession();
                        
                    $tempContent        =   $options->managerParseo->getXpathHtml($tempHtml,false);
                    $results            =   $options->managerParseo->queryDom($tempContent,"//input[@name='SearchResults_File']");
    
                    if($results->length>0){
                        $SearchResults_File =   $results->item(0)->getAttribute('value');
                    }
                    $fields = array(
                        'CurrentPage'       => 1,   'H_Parcel' => 1,            'L_Parcel' => 1,            'Z_Parcel' => 1,                'LastSaleOnly' => '',           'Map_Rec' => '',
                        'OrderBy' => 'PIN',         'PageCount' => 50,          'ResultsMenu' => 3,         'SearchMenu' => 3,              'ShowDownloadButton' => '',     'SearchResults_File' => $SearchResults_File,
                        'Show_Rec' => 1,            'iRecordCount' => 1,        'iTotalPage' => 1,          'windowHeight' => 863,          'windowWidth' => 1846
                    );
                    //echo '<pre>';
                    //print_r($fields);
                    //echo '</pre>';
                    $fields_string  =   "";
                    foreach($fields as $key=>$value){
                        $fields_string .= $key.'='.$value.'&';
                    }
                    rtrim($fields_string, '&');
                break;
            }
            return  array(
                'post'  =>  $fields_string
            );
        }
	}
?>