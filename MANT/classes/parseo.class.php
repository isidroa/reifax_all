<?php
	class managerParseo extends ReiFax {
	    var $conMaster;
	    var $managerDownload;
	    var $managerErrors;
	    var $proxyGoogle=9993;
	    
	    public function __construct($params=array()) {
	        $params    =   (object)$params;
	        $this->conMaster        =$params->conMaster;
	        $this->managerDownload  =$params->managerDownload;
	        $this->managerErrors    =$params->managerErrors;
	        return true;
	    }
	    
	    public function csvPsummary($params=array()) {
	        $params		=   (object)$params;
			$bd			=	$params->bd;
			$tablaPaosal=	$params->tablaPaosal;
			$origen		=	$params->origen;
			$tempDir	=	'/var/xima/download/fl/'.strtolower(substr($bd,2,strlen($bd)));
	        if(!is_dir($tempDir)){
	            if(!mkdir($tempDir,0777,true)){
	                echo 'Error en la Creación del directorio '.$tempDir.'/ # '.implode(' ',error_get_last());
	            }
	        }
	        $tempDir='/var/xima/download/fl/'.strtolower(substr($bd,2,strlen($bd))).'/previoPsummary.csv';
	        if(file_exists($tempDir)){
	            echo PHP_EOL .'El Archivo local Existe : '.$tempDir;
	            if(unlink($tempDir)){
	                echo PHP_EOL .'El Archivo Mencionado fue Eliminado';
	            }else{
	                die(PHP_EOL .'El Archivo Mencionado NO Pudo ser Eliminado '.implode(' ',error_get_last()));
	            }
	        }else{
	            echo PHP_EOL .'El Archivo local NO Existe : '.$tempDir;
	        }
	        unset($conI);
	        $serverData     =   $this->ObtainRealServer(
	        	array(
	        		'bd'=>$bd
				)
			);
	        $conI           =   $serverData['conI'];
	        
	        $serverFiles    =   $serverData['serverFiles'];
	        $serverDatas    =   $serverData['serverData'];
	        unset($serverData);
	        echo PHP_EOL .'Sevidor Origen Files: '.$serverFiles;
	        echo PHP_EOL .'Sevidor Origen Data: '.$serverDatas;
	
	        $query = "
	            SELECT SQL_NO_CACHE t.tblResult,t.consulta
	            FROM `mantenimiento`.`estructuras` e
	            LEFT JOIN `mantenimiento`.`traduccion` t ON (t.tblResult=e.tblResult)
	            WHERE lower(`bdResult`)='".strtolower($bd)."' AND t.tblid=13 AND (t.tblResult LIKE '%NALNAL')
	            LIMIT 1";
	        if(!$result     =   $conI->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conI->errno . ') ' . $conI->error);
	        }
	        $result     =   $result->fetch_array();
	        $tblnalnal  =   strtolower($result['tblResult']);
	
	        //Verificamos si el Parcel_id tiene indice y si es tipo varchar, si no le creamos el indice y lo cambiamos tipo text a varchar(50)
	        $query =    "SHOW INDEX FROM  $bd.$tblnalnal";
	        if(!$result=    $conI->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conI->errno . ') ' . $conI->error);
	        }
	        $validar_indice=0;$validar_campo=0;
	        $parcelIdTblSelected=$this->getMapFields(
	        	array(
	        		'campo'=>'Parcelid',
	        		'nom_bd'=>$bd,
	        		'tabla'=>'NALNAL',
	        		'letra'=>'',
	        		'funcion'=>0
	        	),
	        	array(
	        		'conI'=>$conI
				)
			);
	        $parcelIdTblSelectedFunc=$this->getMapFields(
	        	array(
	        		'campo'=>'Parcelid',
	        		'nom_bd'=>$bd,
	        		'tabla'=>'NALNAL',
	        		'letra'=>'a.',
	        		'funcion'=>1
	        	),
	        	array(
	        		'conI'=>$conI
				)
			);
	        while($row=$result->fetch_assoc()){
	            if(strtoupper($row['Column_name'])==strtoupper(str_replace("`",'',$parcelIdTblSelected))){
	                $validar_indice=1;
	                break;
	            }
	        }
	        //si no entro al while es porque no tiene ningun indice por lo tanto hay que crearlo
	        $query =    "DESCRIBE $bd.$tblnalnal";
	        if(!$result=    $conI->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conI->errno . ') ' . $conI->error);
	        }
	        while($row=$result->fetch_assoc()){
	            if(strtolower(trim($row['Field']))==strtolower(str_replace("`",'',$parcelIdTblSelected))){
	                if('text'==trim($row['Type'])){
	                    $validar_campo=1;
	                    echo PHP_EOL ."En La Tabla {$tblnalnal} el Campo {$parcelIdTblSelected} es tipo text, se Va a modificar por el tipo VARCHAR(50) Automaticamente";
	                }
	            }
	        }
	        if($validar_campo==1){
	            $query = "ALTER TABLE $bd.`$tblnalnal` MODIFY COLUMN $parcelIdTblSelected VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL";
	            if(!$conI->query($query)){
	                die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conI->errno . ') ' . $conI->error);
	            }
	            echo PHP_EOL ."Se Modifico el Tipo de Campo para el {$parcelIdTblSelected} de text a VARCHAR(50) de la Tabla ".$tblnalnal;
	        }
	        if($validar_indice==0){
	            echo PHP_EOL ."En La Tabla {$tblnalnal} no tiene Indice creado para el Campo {$parcelIdTblSelected}, se Va a Crear Automaticamente";
	            $query = "ALTER TABLE $bd.`$tblnalnal` ADD INDEX `Index_1`($parcelIdTblSelected)";
	            if(!$conI->query($query)){
	                die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conI->errno . ') ' . $conI->error);
	            }
	            echo PHP_EOL ."Se Creo el Indice para el Campo {$parcelIdTblSelected} de la Tabla ".$tblnalnal;
	        }
	        if($bd=='flmadison'){
	            $campoPid   =   'ALTKEY';
	            $campoAno   =   'SALEYR1';
	            $campoMes   =   'SALEMO1';
	        }elseif($bd=='florange'){
	            $campoPid   =   'parcel_id';
	            $campoAno   =   'SALE_YR1';
	            $campoMes   =   'SALE_MO1';
	        }else{
	            $campoPid   =   'ALT_KEY';
	            $campoAno   =   'SALE_YR1';
	            $campoMes   =   'SALE_MO1';
	        }
	        //Spepcial Cases
	        if($bd=='flmarion')
	            $specialCase    =   "AND {$parcelIdTblSelectedFunc} NOT LIKE '%+%'";
	        else
	            $specialCase    =   '';
	        $query  =   "
	            SELECT a.parcelid,IF(a.pid IS NULL,'',TRIM(a.pid)) AS pid, a.saledate FROM $bd.psummary a
	            UNION ALL
	            SELECT $parcelIdTblSelectedFunc AS parcelid, a.$campoPid AS pid, procs.saledate_nal(`$campoAno`,`$campoMes`) AS saledate FROM $bd.$tblnalnal a
	                LEFT JOIN $bd.psummary b ON $parcelIdTblSelectedFunc=b.parcelid
	                WHERE b.parcelid IS NULL $specialCase
	        ";
	        $this->query_to_csv($conI,$query,'/var/xima/download/fl/'.strtolower(substr($bd,2,strlen($bd))).'/previoPsummary.csv');
	        echo PHP_EOL .'Se genero el CSV Remoto -- Totales: ==> '.$conI->affected_rows;
	        
	        if($params->withPrevio){
	            montarCsvPrevio($bd,$origen,$tablaPaosal,'previoPsummary.csv');
	        }
	    }
	
	
	    public function montarCsvPrevio($bd,$origen,$tabla,$nameCsv=true){
	        verificaAuxiliar($bd);
	        $query  =   "truncate table {$bd}.{$origen}";
	        if(!$con->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	        }
	        $nameCsv    =   ($nameCsv   === true)   ?   'previo.csv'    :   $nameCsv;
	        $condado    =   strtolower(substr($bd,2));
	        //if(in_array($bd,array('flbroward','florange'))){
	            $query  =   "
	                LOAD DATA LOCAL INFILE '/var/xima/download/fl/{$condado}/{$nameCsv}' INTO TABLE {$bd}.auxiliar
	                FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\n'
	                (@col1,@col2,@col3) set parcelid=@col1,pid=@col2;";
	            if(!$this->con->query($query)){
	                die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	            }
	            echo PHP_EOL .'Rows Imported from CSV: '.$this->con->affected_rows;
	        
	        return true;
	    }
	
	    public function verificaAuxiliar($bd){
	        //Eliminamos la Tabla Auxiliar si existe
	        if(!$this->con->query("DROP TABLE IF EXISTS $bd.`auxiliar`")){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	        }
	        //Verifica tabla auxiliar para spiders previos
	        $queryCrear =   "CREATE TABLE  $bd.`auxiliar` (
	            `ParcelID` varchar(100) NOT NULL DEFAULT '',
	            `pid` varchar(100) DEFAULT NULL,
	            KEY `pid` (`pid`) USING BTREE,
	            KEY `parcelid` (`ParcelID`) USING BTREE
	            ) ENGINE=InnoDB;
	        ";
	        if(!$tablas=$this->con->query('show tables from '.$bd)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' .  $this->con->errno . ') ' . $this->con->error);
	        }
	        $existe     =   false;
	        while ($tabla = $tablas->fetch_assoc()) {
	            if ('auxiliar'== implode('',$tabla)){
	                $existe =   true;
	                break;
	            }
	        }
	        $tablas->free_result();
	        if(!$existe){
	            //Crear tabla
	            if(! $this->con->query($queryCrear)){
	                die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' .  $this->con->errno . ') ' .  $this->con->error);
	            }
	        }
	    }
	
	    public  function copyControlPas($optionals){
	        $optionals  =   json_decode(json_encode($optionals),FALSE);
	        $query="TRUNCATE TABLE {$optionals->bd}.pascontrol";
	        if(!$this->con->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	        }
	        $query="SELECT * FROM {$optionals->bd}.pascontrol";
	        if(!$result= $this->conMaster->query($query)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->conMaster->errno . ') ' .  $this->conMaster->error);
	        }
	        $count  =   1;
	        $rows   =   '';
	        while ($row = $result->fetch_assoc()) {
	            $rows   .=($count>1 ? ',' : '').'(\''.implode('\',\'',$row).'\')';
	            if($count>=100){
	                $query="INSERT INTO {$optionals->bd}.pascontrol VALUES {$rows}";
	                if(!$this->con->query($query)){
	                    die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	                }
	                $rows   =   '';
	                $count  =   0;
	            }
	            $count++;
	        }
	        if($count>1){
	            $query="INSERT INTO {$optionals->bd}.pascontrol VALUES {$rows}";
	            if(!$this->con->query($query)){
	                die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	            }
	        }
	    }
	    public function verifyControlExcecute($bd,&$con){
	
	        $queryCrear =   "CREATE TABLE  $bd.`pascontrol` (
	            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	            `server` VARCHAR(20) NULL,
	            `line` VARCHAR(5) NULL,
	            `file` TEXT NULL,
	            `time` VARCHAR(100) NULL,
	            `parcelid` VARCHAR(45) NULL,
	            `timeExecution` VARCHAR(45) NULL,
	            PRIMARY KEY (`id`),
	            KEY `parcelid` (`parcelid`)
	        ) ENGINE=InnoDB;
	        ";
	        if(!$tablas=$con->query('show tables from '.$bd)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $con->errno . ') ' . $con->error);
	        }
	        $existe     =   false;
	        while ($tabla = $tablas->fetch_assoc()) {
	            if ('pascontrol'== implode('',$tabla)){
	                $existe =   true;
	                break;
	            }
	        }
	        $tablas->free_result();
	        if(!$existe){
	            //Crear tabla
	            if(!$con->query($queryCrear)){
	                die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $con->errno . ') ' . $con->error);
	            }
	        }
	    }
	    public function getXpathHtml($htmlDom,$pregMatch){
	
	        if($pregMatch <> false){
	            //We get the code block comments from Zillow that seem this json format
	            preg_match_all($pregMatch,$htmlDom,$addr);
	
	            //replacing all slashes from \ that interfere with the code
	            $htmlDom    =   str_replace('\\','',$addr[1][0]);
	
	        }
	
	        $oldSetting =   libxml_use_internal_errors( true );
	        libxml_clear_errors();
	
	        //New DOMDocument
	        $dom    =   new DOMDocument();
	
	        //We load it with data from cUrl ($ html)
	        @$dom->loadHTML($htmlDom);
	
	        //Uses XPath to Obtain the Object DOM previously obtained
	        $xpath = new DOMXPath( $dom );
	
	        libxml_clear_errors();
	        libxml_use_internal_errors( $oldSetting );
	        libxml_use_internal_errors( $oldSetting );
	
	        return $xpath;
	    }
	    public function clearAllVariables($array){
	        foreach($array as $index => $value)
	            $array[$index]='';
	
	        return $array;
	    }
	
	    public function queryDom($xpath,$query){
	
	        //We make the XPath Query
	        $arrayToExplode =   $xpath->query($query);
	
	        return $arrayToExplode;
	    }
	
	    public function eraseBlankSpaces($string){
	        return preg_replace('/\s+/', ' ', $string);
	    }
	
	    public function in_array_r($needle, $haystack, $strict = true) {
	        foreach ($haystack as $item) {
	            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
	                return true;
	            }
	        }
	        return false;
	    }
	
	    public function correctAddress($address){
	        $address    =   trim($address);
	        $address    =   str_replace("'","\'",$address);
	        $address    =   str_replace("FT LAUDERDALE","FORT LAUDERDALE",$address);
	        $address    =   str_replace("MIAMI BCH","MIAMI BEACH",$address);
	        $address    =   str_replace("JACKSONVILLE BCH","JACKSONVILLE BEACH",$address);
	        return $address;
	    }
	
	    public function string_to_ascii($input){
	        foreach (str_split($input) as $obj)
	            $output .= '&#' .ord($obj) . ';';
	        return $output;
	    }
	    public function getHtmlDom($links,$options=array()){
	        $temp_dom   =   new DOMDocument();
	        $temp_dom->appendChild($temp_dom->importNode($links,true));
	        $result     =   $temp_dom->saveHtml();
	        $result =   preg_replace('/[[:^print:]]/','',$result);
	        if(count($options)>0){
	            foreach($options as $index  =>  $internal){
	                if('prevReplace'==$index){
	                    foreach($internal as $values){
	                        $result =   str_replace($values['search'],$values['to'],$result);
	                    }
	                }elseif('prevPregReplace'==$index){
	                    foreach($internal as $values){
	                        $result =   preg_replace($values['search'],$values['to'],$result);
	                    }
	                }
	            }
	        }
	        return $result;
	    }
	    public function clearTrash($trash,$pattern,$trascribeToLatin=false){
	         $value  =   trim(preg_replace($pattern,'',strip_tags(html_entity_decode(str_replace(array('&nbsp;','&nbsp'),' ',$trash)))));
	        
	         $value  =   $this->eraseBlankSpaces($value);
	         if($trascribeToLatin)
	            $value  =   $this->transcribe_cp1252_to_latin1(preg_replace('/\\\\x([a-f0-9]{2})/imu', '',$value));
	        return $value;
	
	    }
	    public function googleSearch($address,$options=array()){
	        $Var = array(
	            'ownerZip'=>'','ownerCountry'=>'','ownerState'=>'','ownerCity'=>'','ownerAddress'=>''
	        );
	        $ownerCity2='';
	        $urlGoogle      =   'http://maps.googleapis.com/maps/api/geocode/json?address='.str_replace(' ','+',trim($address)).'&sensor=true';
	        $this->managerDownload->stratSession();
	        $htmlGoogle =   $this->managerDownload->getHtml(array('url'    =>  $urlGoogle, 'conMaster' => $this->conMaster, 'condado' => $this->proxyGoogle));
	        $this->managerDownload->closeSession();
	        if(isset($objectGoogle->status) and 'OK'==$objectGoogle->status){
	            $objectGoogle   =   json_decode($htmlGoogle);
	            if('OK' ==  $objectGoogle->status){
	                if(count($objectGoogle->results)    ==  1){
	                    $posibleNewAddress  =   trim(strtoupper($objectGoogle->results[0]->formatted_address));
	                    $tempAddressGoogle  =   explode(" ",$posibleNewAddress);
	                    if($tempAddressGoogle[count($tempAddressGoogle)-1]  ==  "NA")
	                        $addressGoogle  =   substr($addressGoogle,0,strlen($addressGoogle)-2);
	                    $addressGoogle  =   $objectGoogle->results[0]->address_components;
	                    foreach($addressGoogle as $index    =>  $data){
	                        if('postal_code'    ==  $data->types[0]){
	                            $Var['ownerZip']    =   strtoupper($data->long_name);
	                        }
	                        if('country'    ==  $data->types[0]){
	                            $Var['ownerCountry']=   empty($data->long_name) ?   strtoupper($data->short_name)   :   strtoupper($data->long_name);
	                        }
	                        if('administrative_area_level_1'    ==  $data->types[0]){
	                            $Var['ownerState']  =   strtoupper($data->short_name);
	                        }
	                        if('administrative_area_level_2'    ==  $data->types[0]){
	                            $Var['ownerCity']   =   strtoupper($data->long_name);
	                        }
	                        if('locality'   ==  $data->types[0]){
	                            $ownerCity2         =   strtoupper($data->long_name);
	                        }
	                    }
	                    $Var['ownerCity']   =   empty($Var['ownerCity'])    ?   $ownerCity2 :   $Var['ownerCity'];
	                    $Var['ownerCity']   =   preg_replace('/[^a-zA-Z0-9-\s]/', '', $Var['ownerCity']);
	                    $Var['ownerAddress']=   trim(str_replace(
	                                                array(
	                                                    $Var['ownerCountry'],
	                                                    $Var['ownerZip'].',',
	                                                    ' '.$Var['ownerState'].' ',
	                                                    ',',
	                                                    '\''
	                                                ),'',$posibleNewAddress));
	
	                    foreach($Var as $index => $value){
	                        //echo preg_replace_callback('/\\\\([a-f0-9]{2})/imu', 'cbHex',$value);
	                        $value      =   preg_replace('/\\\\x([a-f0-9]{2})/imu', '',$value);
	                        $Var[$index]=   trim($this->clearTrash($value,'/[[:^print:]]/'));
	                    }
	
	                    return $Var;
	                }else{
	                    return array();
	                }
	            }else{
	                return array();
	            }
	        }else{
	            return array();
	        }
	    }
	   public function transcribe_cp1252_to_latin1($cp1252) {
	        return strtr(
	            $cp1252,
	            $this->loadCharacterRepHex()
	        );
	    }
	    
	    /***********END ADDITIONAL SPIDER FOR GET LINK HTML************/
	    public function verifyVariables ($variables){
	
	        foreach($variables as $index => $value){
	            ob_start();
	                //var_dump($value);
	                $resultDump = ob_get_clean();
	                $variables[$index]  =   array($value,$resultDump);
	        }
	        echo '<pre>';print_r($variables);echo '</pre>';
	    }
	    public function loadCharacterRepHex() {
	        if(!isset($this->characterRepHex)){
	            $sql='select SQL_NO_CACHE * from mantenimiento.data_escape_character';
	            if(!$res=$this->conMaster->query($sql)){
	               // die(PHP_EOL .'Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $conMaster->errno . ') ' . $conMaster->error);
	            }
	    
	            $characterRepHex=array();
	            while($data=$res->fetch_assoc()){
	                $this->characterRepHex[$data['character']]=$data['replace'];
	            }
	        }
	        return $this->characterRepHex;
	    }
	    public function strpos_array($haystack, $needles) {
	        if ( is_array($needles) ) {
	            $res    =   array();
	            foreach ($needles as $str) {
	                $start  =   0;
	                while(($pos = strpos($haystack,$str,$start)) !== FALSE) {
	                    if(strlen($haystack)-strlen($str)==$pos){
	                        $haystack   =   trim(substr_replace($haystack,'',$pos,strlen($haystack)));
	                    }
	                    if(0==$pos){
	                        $haystack   =   trim(substr_replace($haystack,'',0,strlen($str)));
	                    }
	                    $res[]  =   array('result'=>$pos,'case'=>$str,'owner'=>$haystack);
	                    $start  =   $pos+1;
	                }
	            }
	            return  count($res)>0   ?   $res    :   FALSE;
	        } else {
	            return FALSE;
	        }
	    }
	    public function repairOwner($data){
	        $cases  =   array('&','C/O','%');
	        $result =   strpos_array($data['initialOwner'], $cases);
	        if($result){
	            return $result[count($result)-1]['owner'];
	        }else{
	            return $data['initialOwner'];
	        }
	    } 
	    
	    /*********
	    *
	        PASS CONTROL
	    *
	    ******************************************/
	
	    public function recordPassControl($bd,$con){
	        echo PHP_EOL .'Registrando numeros para: '.$bd;
	        $control=date("YmdHi");
	        $tabla='paopao';
	        $sql="SELECT SQL_NO_CACHE IdCounty,serverFiles,serverData,bd,county from xima.lscounty where bd='{$bd}'";
	        if(!$res=$this->conMaster->query($sql)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' .  $this->conMaster->errno . ') ' .  $this->conMaster->error);
	        }
	        $data=$res->fetch_assoc();
	        $idCounty=$data['IdCounty'];
	        $serv=$data['serverData'];
	        $bd=$data['bd'];
	
	
	        $sql="SELECT SQL_NO_CACHE a.*
	                FROM mantenimiento.actionpass a
	                order by a.orden";
	        if(!$res=$this->conMaster->query($sql)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' .  $this->conMaster->errno . ') ' . $this->conMaster->error);
	        }
	        $sql="SELECT SQL_NO_CACHE count(*) cant from {$bd}.{$tabla}";
	        if(!$res=$this->con->query($sql)){
	            die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	        }
	        $data=$res->fetch_assoc();
	        $total=$data['cant'];
	
	        $respuesta=array();
	        $arrayPos=array();
	        while($data=$res->fetch_assoc()){
	            $arrayPos[]=$data['actionID'];
	            $data['current']=$total;
	            $query=str_replace('{tabla}',$tabla,str_replace('{bd}',$bd,$data['query']));
	            if($query!=''){
	                if(!$resultData=$this->con->query($query)){
	                    die(PHP_EOL .'Error: '. __LINE__ .' File: '. __FILE__ .' MySQL: (' . $this->con->errno . ') ' . $this->con->error);
	                }
	                while($dataQuery=$resultData->fetch_assoc()){
	                    $this->recordPass($data['actionID'],$county,$dataQuery['cant'],$total,$control);
	                }
	            }
	
	        }
	
	    }
	
	    public function recordPass($action,$county,$records,$total,$control){
	        $sql="INSERT INTO `mantenimiento`.`administracionpass` (`ActionID`, `CountyID`,  `fecha`, `TEjecucion`, `Records`,Total , `indicador`)
	                VALUES ({$action},{$county}, NOW(), 0, {$records},{$total},{$control});";
	        $res=mysql_query($sql,$this->conMaster) or die ($sql.mysql_error($this->conMaster));
	    }
	    
	    public function query_to_csv(&$db_conn, $query, $filename, $headers = true) {
	        touch($filename);
	        @chmod($filename,0777);
	        $fp = fopen($filename, 'w');
	            $result = $db_conn->query($query) or die($db_conn->error);
	            if($headers) {// output header row (if at least one row exists)
	                $row = $result->fetch_assoc();
	                if($row) {
	                    fputcsv($fp, array_keys($row),"\t"  );
	                    //$result->data_seek(0);// reset pointer back to beginning
	                }
	            }
	            while($row = $result->fetch_assoc()) {
	                fputcsv($fp, $row,"\t" );
	            }
	            fclose($fp);
	        unset($fp,$result,$row);
	    }
	}
?>    