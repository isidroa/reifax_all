<?php
	class managerErrors extends connect {
		var $AP='';	//Additional Parameter
		function managerErrors($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	(object)$default;

			$this->connect($optional);

			if(isset($default->AP) and !empty($default->AP)){
				$this->AP	=	$this->conMaster->real_escape_string($default->AP);
			}

			if($default->activeLogErrors){
				$this->activeLogErrors();
			}

			return true;
		}
		public function activeLogErrors(){
			set_error_handler(array($this,'registerError'));
			//ini_set('display_errors','off');
			//error_reporting( E_ALL );
		}
		public function registerError($errno, $errstr, $errfile, $errline, $errcontext){
			$errno	=	$this->conMaster->real_escape_string($errno);
			$errstr	=	$this->conMaster->real_escape_string($errstr);
			$query	=	"INSERT INTO mantenimiento.phpLogErrors VALUES (null,'{$this->getIp('WAN')}','{$errno}','{$errstr}','{$errfile}','{$errline}','{$this->AP}',NOW())";
			if(!$this->conMaster->query($query)){
				die('Error Saving Log Error, Contact the Administrator XD : '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
		}

		public function pasRegisterControl($optional=array()){
			$this->resetOptions();
			$default=	array_merge($this->params,$optional);
			$con	=	$default['optionalConex'];
			$default=	(object)$default;

			$query	=	"SELECT * from {$default->dataBase}.pascontrol WHERE parcelid='{$default->parcelid}'";
			$result	=	$con->query($query);
			if($result->num_rows<=0){
				$query	=	"
					REPLACE INTO {$default->dataBase}.pascontrol
						VALUES (
							null,
							'{$this->getIp('WAN')}',
							'{$default->line}',
							'{$default->file}',
							NOW(),
							'{$default->parcelid}',
							'{$default->timeExecution}'
						)";
			}else{
				$query	=	"
					UPDATE {$default->dataBase}.pascontrol
						SET
							`server`='{$this->getIp('WAN')}',
							`line`='{$default->line}',
							`file`='{$default->file}',
							`time`=NOW(),
							`timeExecution`='{$default->timeExecution}'
                  WHERE
                     `parcelid`='{$default->parcelid}'
               ";
			}
				if(!$con->query($query)){
					die('Error Saving Pas Control Time, Contact the Administrator XD : '. __LINE__ .' MySQL: (' . $con->errno . ') ' . $con->error);
				}
		}

		public function insertlogs($optional=array()){
			$default	=	array_merge($this->params,$optional);
			$default	=	(object)$default;
			$queryIns	=	"
				INSERT INTO
					xima.logsfull (`usersource`, `usertarget`, `operation`, `query`, `insertdate`, `php`)
				VALUES
					('{$default->usersource}','{$default->usertarget}','{$default->operation}','".$default->con->real_escape_string($default->query)."',NOW(),'{$default->file}')";
			$default->con->query($queryIns) or die("{success:false, msg:\"".$queryIns."<br>".$default->con->error()."\"}");
		}

		public function showMysqlErrors($optional=array()){
			$optional=array_merge(array('show'=>'array'),$optional);
			$optional=(object)$optional;
			$backTrace=debug_backtrace();
			$backTrace=(object)$backTrace[0];
			if($optional->show=='string'){
				echo BreakLine .'Line: '. $backTrace->line .' File: '. $backTrace->file .' MySQL: (' . $optional->conexion->errno . ') ' . $optional->conexion->error;
			}else if($optional->show=='array'){
				echo BreakLine;
				print_r(array(
						'Line'=>$backTrace->line,
						'File'=>$backTrace->file,
						'MysqlNumError'=>$optional->conexion->errno,
						'MysqlError'=>$optional->conexion->error,
						'query'=>isset($optional->query) ? $optional->query : ''
					)
				);
			}
		}
	}
