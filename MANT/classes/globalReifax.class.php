<?php

class ReiFax extends connect {

    var $documentRoot = 'c:/inetpub/wwwroot';

    public function __construct($optional = array()) {
        $this->connect($optional);
        return true;
    }

    public function combineData($array1, $array2) {
        foreach ($array2 as $k => $v) {
            if (is_array($array1)) {
                if ($array1[$k]) {
                    if (is_array($array1[$k])) {
                        if (is_object($array2[$k])) {
                            $array2->$k = json_decode(json_encode($array2[$k]), true);
                        }
                        $array1[$k] = combineData($array1[$k], $array2[$k]);
                    } elseif (is_object($array1[$k])) {
                        if (is_array($array2->$k)) {
                            $array2->$k = json_decode(json_encode($array2->$k));
                        }
                        $array1[$k] = combineData($array1[$k], $array2->$k);
                    } else {
                        if (is_float($array1[$k] && ($array1[$k] == 0 || $array1[$k] == 0.0))) {
                            $array1[$k] = $array2[$k];
                        }
                        if (is_int($array1[$k]) && $array1[$k] == 0) {
                            $array1[$k] = $array2[$k];
                        }
                        $array1[$k] = trim($array1[$k]);
                        if (is_string($array1[$k] && ($array1[$k] == '' || $array1[$k] == '--'))) {
                            $array1[$k] = $array2[$k];
                        }
                    }
                } else {
                    $array1[$k] = $array2[$k];
                }
            } elseif (is_object($array1)) {
                if ($array1->$k) {
                    if (is_object($array1->$k)) {
                        if (is_array($array2->$k)) {
                            $array2->$k = json_decode(json_encode($array2->$k));
                        }
                        $array1->$k = combineData($array1->$k, $array2->$k);
                    } elseif (is_array($array1->$k)) {
                        if (is_object($array2->$k)) {
                            $array2->$k = json_decode(json_encode($array2[$k]), true);
                        }
                        $array1->$k = combineData($array1->$k, $array2->$k);
                    } else {
                        if (is_float($array1->$k && ($array1->$k == 0 || $array1->$k == 0.0))) {
                            $array1->$k = $array2->$k;
                        }
                        if (is_int($array1->$k) && $array1->$k == 0) {
                            $array1->$k = $array2->$k;
                        }
                        $array1->$k = trim($array1->$k);
                        if (is_string($array1->$k && ($array1->$k == '' || $array1->$k == '--'))) {
                            $array1->$k = $array2->$k;
                        }
                    }
                } else {
                    $array1->$k = $array2->$k;
                }
            }
        }
        return $array1;
    }

    /*     * *
      getMapFields(
      array(
      'nom_bd'='Obligatorio Nombre De La Base De Datos',
      'campo'='Obligatorio Nombre Del Campo Destino Del Mapeo',
      'tabla'='Obligatorio Nombre Final De La Tabla Mapeada',
      'bdmant'='Opcional Especificamos el nombre de la Base de Datos donde esta el Mapeo, Por Default se llama mantenimiento',
      'letra'='Opcional Letra Para Especificar La Referencia Del Campo En Un Inner Join, Por Defecto es sin Letra',
      'funcion'='Opcional Especifica si se trae el campo con funcion o sin funcion, Por Default es FALSE'
      ),
      array(
      'conI'='Especificamos la Conexion con el Servidor Maestro donde se Encuentra el Mapeo'
      )
      );
     */

    public function getMapFields($optional = array(), $conexions = array()) { //Obtener campos del mapeo
        $this->params = array_merge(
                $this->params, array(
            'bdmant' => 'mantenimiento',
            'funcion' => FALSE,
            'letra' => ''
                )
        );
        $default = array_merge($this->params, $optional);
        $default = (object) $default;
        $conexions = (object) $conexions;

        $query = "SELECT SQL_NO_CACHE e.tblResult,m.funcion,m.campop
					FROM `{$default->bdmant}`.`estructuras` e
					LEFT JOIN `{$default->bdmant}`.`mapeo` m ON e.structid=m.structid
					WHERE lower(`bdResult`)='" . strtolower($default->nom_bd) . "' AND m.`campo`='{$default->campo}' AND e.tblResult LIKE '%{$default->tabla}' LIMIT 1";
        if (!$result = $conexions->conI->query($query)) {
            die(PHP_EOL . 'Error: ' . __LINE__ . ' File: ' . __FILE__ . ' MySQL: (' . $conexions->conI->errno . ') ' . $conexions->conI->error);
        }
        $r = $result->fetch_assoc();
        $result->free_result();
        $tblRes = $r['tblResult'];
        $funci = $r['funcion'];
        $campop = $r['campop'];
        if ($campop <> '' && strlen($campop) > 0) {
            if ($funci == '' || strlen($funci) == 0) {//sino tiene funcion tomo el campo origen
                $funci = $default->letra . '`' . $campop . '`';
            } else {
                $funci = str_replace('`' . $r['campop'] . '`', $default->letra . '`' . $r['campop'] . '`', $r['funcion']);
            }
        }
        if ($default->funcion === TRUE) {
            return $funci;
        } else {
            return $default->letra . '`' . $campop . '`';
        }
    }

    public function query_to_csv($params = array()) {
        $default = array('db_conn' => NULL, 'query' => '', 'filename' => '', 'headers' => true);
        $options = (object) array_merge($default, $params);
        $temp = explode('/', $options->filename);
        array_pop($temp);
        $temp = implode('/', $temp);
        if (!is_writable($temp)) {
            mkdir($temp, 0777, true);
        }
        touch($options->filename);
        @chmod($options->filename, 0777);
        $fp = fopen($options->filename, 'w');
        $result = $options->db_conn->query($options->query) or die($options->db_conn->error . $options->query);
        if ($options->headers) {// output header row (if at least one row exists)
            $row = $result->fetch_assoc();
            if ($row) {
                fputcsv($fp, array_keys($row), chr(9));
                $result->data_seek(0); // reset pointer back to beginning
            }
        }
        echo PHP_EOL.'TOTAL SQL: '.$result->num_rows;
        $c=1;
        while ($row = $result->fetch_assoc()) {
          echo PHP_EOL.'Linea: '.$c++;
            fputcsv($fp, $row, chr(9));
        }
        fclose($fp);
        unset($fp, $result, $row);
        $fp = $result = $row = '';
    }

    public function getMemoryUsed() {
        //If its Windows
        //Tested on Win XP Pro SP2. Should work on Win 2003 Server too
        //Doesn't work for 2000
        //If you need it to work for 2000 look at http://us2.php.net/manual/en/function.memory-get-usage.php#54642
        if (substr(PHP_OS, 0, 3) == 'WIN') {
            if (substr(PHP_OS, 0, 3) == 'WIN') {
                $output = array();
                exec('tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output);
                return preg_replace('/[\D]/', '', $output[5]) * 1024;
            }
        } else {
            //We now assume the OS is UNIX
            //Tested on Mac OS X 10.4.6 and Linux Red Hat Enterprise 4
            //This should work on most UNIX systems
            $pid = getmypid();
            exec('ps -eo%mem,rss,pid | grep ' . $pid, $output);
            $output = explode(' ', trim($output[0]));
            //rss is given in 1024 byte units
            return $output[1] * 1024;
        }
    }

    public function getCpuUsed() {
        if (stristr(PHP_OS, 'win')) {
            $wmi = new COM("Winmgmts://");
            $server = $wmi->execquery("SELECT LoadPercentage FROM Win32_Processor");
            $cpu_num = 0;
            $load_total = 0;
            foreach ($server as $cpu) {
                $cpu_num++;
                $load_total += $cpu->loadpercentage;
            }
            $load = round($load_total / $cpu_num);
        } else {
            $sys_load = sys_getloadavg();
            $load = $sys_load[0];
        }

        return (int) $load;
    }

    public function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    /**
     * 	Creada por: Smith
     * 	Fecha: 10/06/2015
     * 	Descripcion:
     * 		Metodo que trae la cantidad de registros en funcion del query que se pasa por parametro.
     *
     * 	Ejemplo de invocacion y llamada al metodo:
     * 		include($_SERVER['DOCUMENT_ROOT'].'/mant/classes/globalReifax.class.php');
     * 		$Connect=	new ReiFax();
     * 		$query="SELECT count(*) cant FROM $bdResult.$table ";
     * 		$CANTIDAD=$Connect->getCountTable(array('db_conn' => $conex ,'query' => $query))

     * 	Atributos (parametros) que se pasan al metodo:
     * 		getCountTable(
     * 			array(
     * 				'db_conn' --> 'Obligatorio. Especificamos la Conexion con el Servidor donde se ejecuta el query',
     * 				'query'   --> 'Obligatorio. Consulta o query a ejecutar. IMPORTATE: el alias del count(*) debe ser "cant" ',
     * 			)
     * 		);
     */
    public function getCountTable($params = array()) {
        $default = array('db_conn' => NULL, 'query' => '');
        $options = (object) array_merge($default, $params);

        $result = $options->db_conn->query($options->query) or die($options->db_conn->error . $options->query);
        $row = $result->fetch_assoc();
        return $row['cant'];
    }

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 30/09/2015
     * 	Descripcion:
     * 		Metodo que trae el contenido de una tabla cualquiera con campos especificos y condicional especifico.
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		getTableValues(
     * 			array(
     * 				'table'   --> 'Obligatorio. especificamos la tabla con la base de datos que vamos a trabajar por defecto esta xima.ximausrs',
     * 				'where' --> 'No Obligatorio. Especificamos un condicional en caso de que exista',
     * 				'fields'   --> 'No Obligatorio. especificamos los campos que va a traer por defecto trae todos',
     * 			)
     * 		);
     */
     public function getTableValues($optional=array(),$conexions=array()){
  		$default = array_merge(
  			array(
          'table'=>'xima.ximausrs',
  				'where'=>'',
  				'fields'=>'*'
  			),
  			$optional
  		);
  		$default = (object) $default;
  		$conexions = (object) $conexions;
  		$query = "SELECT {$default->fields} FROM {$default->table} WHERE {$default->where}";
  		$result = $conexions->conReiFax->query($query) or die($query.'---'.$conexions->conReiFax->error);
  		$data=array();
  		while($row=$result->fetch_assoc()){
  			array_push($data,$row);
  		}
  		return $data;
  	}


    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 30/09/2015
     * 	Descripcion:
     * 		Metodo que trae el contenido de la tabla camptit convertiendo las descripciones en Variables visuales con el estandar de las llaves y los porcentajes.
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		getCamptitTransform(
     * 			array(
     * 				'values'   --> 'array con el contenido de la tabla'
     * 			)
     * 		);
     */
  	public function getCamptitTransform($optional=array()){
  		$default = array_merge(array(),$optional);
  		$default = (object) $default;

  		$temp=array();
  		foreach ($default->values as $key => $value) {
  			$temp[$value['IDTC']]='{%'.$value['Desc'].'%}';
  		}
  		return $temp;
  	}

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 01/10/2015
     * 	Descripcion:
     * 		Metodo que trae el contenido de los campos tipo variable_user con sus respectivos valores a ser cambiados de acuerdo al valor
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		changeVariablesUser(
     * 			array(
     * 				'valueToWork'   --> 'Obligatorio. Valor original de la cadena concatenada'
     * 				'postValues'   --> 'Obligatorio. array con todas las variables utilizadas dentro de este metodo que fueron enviadas por post'
     * 			)
     * 		);
     */
  	public function changeVariablesUser($optional=array()){
    	$default = array_merge(array(),$optional);
    	$default = (object) $default;
    	$toReturn=$default->valueToWork;
    	$toReturn = str_replace('{%Today Date%}',date('m/d/Y'),$toReturn);
    	$toReturn = str_replace('{%Initial Deposit%}',number_format($default->postValues['deposit'],2),$toReturn);
    	$toReturn = str_replace('{%Additional Deposit%}',number_format($default->postValues['additionalDeposit'],2),$toReturn);
    	$toReturn = str_replace('{%Inspection Date%}',$default->postValues['inspection'],$toReturn);
    	$toReturn = str_replace('{%Acceptance Date%}',$default->postValues['dateAcc'],$toReturn);
    	$toReturn = str_replace('{%Closing Date%}',$default->postValues['dateClo'],$toReturn);
    	$toReturn = str_replace('{%Balance To Close%}',number_format((int)$default->postValues['offer']-(int)$default->postValues['deposit']-(int)$default->postValues['additionalDeposit'],2),$toReturn);
    	if (preg_match('/{%733%}/',$toReturn)){	//"{%Offer Price%}"
    		$toReturn = str_replace('{%733%}',number_format((int)$default->postValues['offer'],2),$toReturn);
    	}
    	$toReturn = str_replace('{%Buyer Agent%}',$default->postValues['buyeragent'],$toReturn);
    	$toReturn = str_replace('{%Buyer Broker%}',$default->postValues['buyerbroker'],$toReturn);
    	$toReturn = str_replace('{%Seller Name%}',$default->postValues['sellname'],$toReturn);
    	$toReturn = str_replace('{%Seller Address Line 1%}',$default->postValues['saddress1'],$toReturn);
    	$toReturn = str_replace('{%Seller Address Line 2%}',$default->postValues['saddress2'],$toReturn);
    	$toReturn = str_replace('{%Seller Address Line 3%}',$default->postValues['saddress3'],$toReturn);

    	return $toReturn;
    }

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 01/10/2015
     * 	Descripcion:
     * 		Metodo que trae el contenido de los campos tipo variable_user con sus respectivos valores a ser cambiados de acuerdo al valor
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		modifyValues(
     * 			array(
     * 				'IDTC'   --> 'Obligatorio. identificador de la variable en el Camptit'
     * 				'valuesById'   --> 'Obligatorio. array con todas las variables del camptit con su respectivo valor'
     * 				'postValues'   --> 'Obligatorio. array con todas las variables utilizadas dentro de este metodo que fueron enviadas por post'
     * 			)
     * 		);
     */
    public function modifyValues($optional=array()){
    	$default = array_merge(array(),$optional);
    	$default = (object) $default;
    	$toReturn='';
    	switch($default->IDTC){
    		case "16":	//Legal Description
    			$toReturn="Legal Description as Shown in Public Records";
    			break;
    		case "20":	//Owner Name == Seller
    			if(strlen(trim($default->postValues['sellname']))<1){
    				$toReturn=$default->valuesById[$default->IDTC];
    			}else{
    				$toReturn=$default->postValues['sellname'];
    			}
    			break;
    		case "95":	//Listing Price
    			$toReturn=number_format($default->valuesById[$default->IDTC],2);
    			break;
    		case "716":	//Contac Name
    			if(strlen(trim($default->postValues['listingagent']))<1)
    				$toReturn=$default->valuesById[$default->IDTC];
    			else
    				$toReturn=$default->postValues['listingagent'];
    			break;
    		case "717":	//Contac Email 1
    			if(strlen(trim($default->postValues['remail']))<1)
    				$toReturn=$default->valuesById[$default->IDTC];
    			else
    				$toReturn=$default->postValues['remail'];
    			break;
    		case "734":	//Buyer Name
    			if(strlen(trim($default->postValues['buyername']))<1){
    				$toReturn=$default->valuesById[$default->IDTC];
    			}else{
    				$toReturn=$default->postValues['buyername'];
    			}
    			break;
    		case "735":	//Buyer Address Line 1
    			$toReturn=$default->postValues['baddress1'];
    			break;
    		case "738":	//Buyer Address Line 2
    			$toReturn=$default->postValues['baddress2'];
    			break;
    		case "739":	//Buyer Address Line 3
    			$toReturn=$default->postValues['baddress3'];
    			break;
    		case "XX11":	//Seller Name
    			$toReturn=$default->postValues['sellname'];
    			break;
    		case "XX12":	//Seller Address Line 1
    			$toReturn=$default->postValues['saddress1'];
    			break;
    		case "XX13":	//Seller Address Line 2
    			$toReturn=$default->postValues['saddress2'];
    			break;
    		case "XX14":	//Seller Address Line 3
    			$toReturn=$default->postValues['saddress3'];
    			break;
    		case "743":	//Contac Company
    			if(strlen(trim($default->postValues['listingbroker']))<1){
    				$toReturn=$default->valuesById[$default->IDTC];
    			}else{
    				$toReturn=$default->postValues['listingbroker'];
    			}
    			break;
    		case "XX1":	//Date Today
    			$toReturn=date("m/d/Y");
    			break;
    		case "XX2":	//Initial Deposit
    			$toReturn=number_format($default->postValues['deposit'],2);
    			break;
    		case "XX3": //Inspections Days
    			$toReturn=$default->postValues['inspection'];
    			break;
    		case "XX4":	//Acceptance Date
    			$toReturn=date($default->postValues['dateAcc']);
    			break;
    		case "XX5":	//Closing Date
    			$toReturn=date($default->postValues['dateClo']);
    			break;
    		case "XX6":	//Balance To Close
    			$Balance_To_Close = (int)$default->postValues['offer']-(int)$default->postValues['deposit']-(int)$default->postValues['additionalDeposit'];
    			$toReturn=number_format($Balance_To_Close,2);
    			break;
    		case "XX7":	//Offer Price
    			$offer_price=(int)$default->postValues['offer'];
    			$toReturn=number_format($offer_price,2);
    			break;
    		case "XX8":	//Buyer's Agent
    			$toReturn=$default->postValues['buyeragent'];
    			break;
    		case "XX9":	//Buyer's Broker
    			$toReturn=$default->postValues['buyerbroker'];
    			break;
    		case "XX10"://Additional Deposit
    			$toReturn=number_format($default->postValues['additionalDeposit'],2);
    			break;
    		default:
    			$toReturn=$default->valuesById[$default->IDTC];
    	}
    	return $toReturn;
    }

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 9/10/2015
     * 	Descripcion:
     * 		Metodo que transforma el contenido de descripcion al valor del campo id llamado IDTC de la tabla Camptit
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		templateEncode(
     * 			array(
     * 				'text'   --> 'Obligatorio cadena a codificar'
     * 				'camptit'   --> 'oBligatorio Array Valores para realizar la codificacion'
     * 			)
     * 		);
     */
    public function templateEncode($optional=array()){
      $verify = array('text','camptit');
    	$default = array_merge(array(),$optional);
    	$default = (object) $default;
      $this->verifyVars(array('verify'=>$verify,'toCompare'=>$default));

      foreach($default->camptit as $key => $value) {
        $default->text=str_replace($value,'{%'.$key.'%}',$default->text);
      }
      return $default->text;
    }

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 9/10/2015
     * 	Descripcion:
     * 		Metodo que transforma el contenido del campo con el valor del id llamado IDTC de la tabla Camptit al valor de la descricpion
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		templateEncode(
     * 			array(
     * 				'text'   --> 'Obligatorio cadena a codificar'
     * 				'camptit'   --> 'oBligatorio Array Valores para realizar la codificacion'
     * 			)
     * 		);
     */
    public function templateDecode($optional=array()){
      $verify = array('text','camptit');
    	$default = array_merge(array(),$optional);
    	$default = (object) $default;
      $this->verifyVars(array('verify'=>$verify,'toCompare'=>$default));

      foreach($default->camptit as $key => $value) {
        $default->text=str_replace('{%'.$key.'%}',$value,$default->text);
      }
      return $default->text;
    }

    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 9/10/2015
     * 	Descripcion:
     * 		Metodo que transforma el contenido de descripcion al valor del campo id llamado IDTC de la tabla Camptit
     *

     * 	Atributos (parametros) que se pasan al metodo:
     * 		verifyVars(
     * 			array(
     * 				'verify'   --> 'Obligatorio array con variables obligatorias'
     * 				'toCompare'   --> 'oBligatorio Objeto con las variables pasadas a comparar si existen'
     * 			)
     * 		);
     */
    public function verifyVars($optional=array()){
    	$default = array_merge(array(),$optional);
    	$default = (object) $default;
      //print_r($default);
      if(!isset($default->verify)){
        $this->verifyText(array(
          'type'=>1,
          'message'=>'Undefined verify var'
        ));
      }
      if(!isset($default->toCompare)){
        $this->verifyText(array(
          'type'=>1,
          'message'=>'Undefined toCompare var'
        ));
      }

      foreach($default->verify as $key => $value) {
        $control=false;
        foreach($default->toCompare as $var => $valueField) {
          if($value===$var){
            $control=true;
          }
        }
        if(!$control){
          $this->verifyText(array(
            'type'=>1,
            'message'=>'Undefined '.$value.' var'
          ));
        }
      }
    }
    /**
     * 	Creada por: Jesus Marquez
     * 	Fecha: 9/10/2015
     * 	Descripcion:
     * 		Metodo que imprime el Error
     *

     * 	verifyText (parametros) que se pasan al metodo:
     * 		verifyText(
     * 			array(
     * 				'type'   --> 'Obligatorio'
     * 				'message'   --> 'oBligatorio'
     * 			)
     * 		);
     */
    private function verifyText($optional=array()){
      $default = array_merge(array(),$optional);
    	$default = (object) $default;
      echo "
      <table border='1' style='border:solid;border-width:1px;font-size:13px;font-family:Courier New' cellpadding=0 cellspacing=0>
        <tr style='background-color:#FA5858'>
          <td>
            Error: ReiFax
          </td>
        </tr>";
      foreach(debug_backtrace() as $trace){
        echo "
          <tr>
            <td>
              File Name: ".$trace['file']."
            </td>
          </tr>
          <tr>
            <td>
              Line Number: ".$trace['line']."
            </td>
          </tr>
          <tr>
            <td>
              Function Name: ".$trace['function']."
            </td>
          </tr>
          <tr style='border-bottom-color:#000000;'>
            <td>
              Class Name: ".$trace['class']."
            </td>
          </tr>
        ";
      }
      echo "
      <tr style='background-color:#F7D358'>
        <td>
          {$default->message}
        </td>
      </tr>
      </table>";
    }
}
