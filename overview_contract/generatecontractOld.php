<?php
	//Carga los fields del archivo que contiene el form generado con pdftk
	function load_field_data( $field_report_fn ){
	   $ret_val= array();

	   $fp= fopen( $field_report_fn, "r" );
	   if( $fp ) {
			$line= '';
			$rec= array();
			while(($line= fgets($fp, 2048))!== FALSE) {
				 $line= rtrim( $line ); // remueve espacios en blanco
				 if( $line== '---' ) {
					if( 0< count($rec) ) { // final del registro
					   $ret_val[]= $rec;
					   $rec= array();
					}
					continue; // salta a la siguiente liena
				 }

				 // divide la linea entre nombre y valor 
				 $data_pos= strpos( $line, ':' );
				 $name= substr( $line, 0, $data_pos+ 1 );
				 $value= substr( $line, $data_pos+ 2 );

				 if( $name== 'FieldStateOption:' ) {
					// empaqueta FieldStateOption en su propio array 
					if( !array_key_exists('FieldStateOption:',$rec) ) {
					   $rec['FieldStateOption:']= array();
					}
					$rec['FieldStateOption:'][]= $value;
				 }
				 else {
					$rec[ $name ]= $value;
				 }
			}
		  if( 0< count($rec)) { // empaqueta el final del registro
			 $ret_val[]= $rec;
		  }

		  fclose( $fp );
	   }

	   return $ret_val;
	}
	include("../properties_conexion.php");
	include("../FPDF/limpiar.php");
	
	limpiardirpdf2('../FPDF/generated/');	//Se borran los pdf viejos
    
	$tipo=$_POST['type'];
	$county =  $_POST['county'];
	$parcelid = $_POST['pid'];
	$userid = $_COOKIE['datos_usr']['USERID'];
	$arrayCamp=Array('txtseller','txtbuyer','txtaddress','txtcounty','txtlegal1','txtlegal2','txtprice','txtlistingsales','txtbuyeraddress1','txtselleraddress1','txtpage');
	conectarPorBd('fl'.strtolower($county));
	$valores=Array();
	
	//Obtener valores del  usuario
	$query="SELECT x.`HOMETELEPHONE`, x.`ADDRESS`, x.`STATE`, x.`CITY`, x.`NAME`, x.`SURNAME` from xima.ximausrs x where 	userid=".$userid;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$valores[1]=$r['NAME'].' '.$r['SURNAME'];
	$valores[8]=$r['ADDRESS'];
	
	//Obtener valores public record
	$query="SELECT owner, address, ozip, city, owner_a, phonenumber1 from psummary where parcelid='".$parcelid."'";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$valores[0]=$r['owner'];
	$valores[3]=$county;
	$valores[9]=$r['owner_a'];
	$address=$r['address'];
	$city=$r['city'];
	$zip=$r['ozip'];
	$valores[4]="Folio Number: ".$parcelid."  Legal Description as Shown in Public Records.";
	//Obtener datos mlsresidential
	$query="SELECT lprice, agent, address, city, zip from mlsresidential where parcelid='".$parcelid."'";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$valores[6]=$r['lprice'];
	$valores[7]=$r['agent'];
	if($address==''){
		$address=$r['address'];
	}
	if($city==''){
		$city=$r['city'];
	}
	if($zip==''){
		$zip=$r['zip'];
	}
	$valores[2]=$address.', '.$city.', '.$zip;
	$valores[10]='1';
	$contratos=Array('ContractPurchase','leadBasedPaint','ResidentialContract');
	$camposContratos=Array('contract','lead','residential');
	//Recorre Form fields para obtener array de campos a llenar
	$campos=Array();
	$field_arr= load_field_data( $camposContratos[$type].'.pdf.fields' );
	foreach( $field_arr as $field ) { // itera en los campos
		$campos[]=$field['FieldName:'];
	}
	require_once( 'forge_fdf.php' );

	$fdf_data_strings= array();
	$fdf_data_names= array();
	
	//Manda valores a la plantilla pdf
	foreach( $campos as $value ) {
	   // translate tildes back to periods
	   //$fdf_data_strings[ strtr(str_replace('_',' ',$key), '~', '.') ]= $value;
	   $encontrado=true;
	   $count=0;
	   while($encontrado && $count<count($arrayCamp)){
			if($value==$arrayCamp[$count]){
				$encontrado=false;
				$fdf_data_strings[ strtr($arrayCamp[$count], '~', '.') ]= $valores[$count];
			}
			$count++;
	   }
	   if($encontrado){
	   		$fdf_data_strings[ strtr($value, '~', '.') ]= '';
	   }
	}
	// ignore these in this example
	$fields_hidden= array();
	$fields_readonly= array();
	
	$fdf= forge_fdf( '',
							$fdf_data_strings,
							$fdf_data_names,
							$fields_hidden,
							$fields_readonly );
	//echo $fdf; break;
	$fdf_fn= tempnam( '.', 'fdf' );
	$fp= fopen( $fdf_fn, 'w' );
	if( $fp ) {
	   fwrite( $fp, $fdf );
	   fclose( $fp );
		
	   $ruta= getCwd(); //break;
	   $file='FPDF/generated/'.$contratos[$type].$parcelid.'.pdf';
	   passthru(   $ruta.'/pdftk/pdftk '.$contratos[$type].'.pdf fill_form '. $fdf_fn.
				   ' output C:/inetpub/wwwroot/'.$file );
	   $archivo='C:/inetpub/wwwroot/'.$file  ; 
	   unlink( $fdf_fn ); // delete temp file
	   echo "{success:true, pdf:'$file'}";
	}
	else { // error
	   echo '{msg: unable to write temp fdf file}';
	}
?>