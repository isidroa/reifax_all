<?php
/**
 * generatecontract.php
 *
 * Generate the contract for download.
 *
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *          Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
 * @version 04.04.2011
 */

include "../properties_conexion.php";
include "../FPDF/limpiar.php";

// Delete old PDF
limpiardirpdf2('../FPDF/generated/');

$type      = $_POST['type'];
$county    = $_POST['county'];
$parcelid  = $_POST['pid'];

$contrOpc  = $_POST['options'];

$offer     = $_POST['offer'];
$dateAcc   = $_POST['dateAcc'];
$dateClo   = $_POST['dateClo'];

$rname     = $_POST['rname'];
$remail    = $_POST['remail'];
$mlnumber  = $_POST['mlnaux'];
$address   = $_POST['addr'];

$sendmail  = $_POST['sendmail'];
$sendme    = $_POST['sendme'];

// offer dateAcc dateClo rname remail sendmail sendme

function SendEMail($attach,$mln,$address,$userid,$name,$email,$send,$copy) {

    $boundary = md5(uniqid(time()));
    $copy     = 0;

    $query = "SELECT email,name,surname FROM xima.ximausrs WHERE userid='$userid'";
    $rs    = mysql_query($query);
    $row   = mysql_fetch_array($rs);

    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: multipart/mixed;boundary=\"$boundary\"\r\n";
    $header  .= "This is a multi-part message in MIME format.\r\n";
    $headers .= "From: {$row[1]} {$row[2]} <no-reply@reifax.com>\r\n";
    $headers .= "Reply-To: {$row[0]}\r\n";
    $headers .= "Return-path: no-reply@reifax.com";

    $subject  = "Offer for the MLS number $mln";

    $body     = "--$boundary\n";

    $body    .= "Dear Mr/Mrs $name\r\n\r\n";
    $body    .= "Please find attached a contract with our offer regarding the ";
    $body    .= "property with the address: {$address}\r\n";
    $body    .= "We wait for your prompt response\r\n\r\n";
    $body    .= "Regards\r\n";

    $body    .= "--$boundary\n";
    $fp       = fopen($attach, "r");
    $file     = fread($fp, filesize($attach));
    $file     = chunk_split(base64_encode($file));
    $filename = basename($attach);
    $body    .= "Content-type: application/pdf; name=$filename\r\n";
    $body    .= "Content-transfer-encoding:base64\r\n\r\n";
    $body    .= $file. "\r\n\r\n";
    $body    .= "--$boundary\n";


    if ($send == 'on') {
        $sendTo   = strtolower($email);
        mail($sendTo, $subject, $body, $headers);
    }

    if ($copy == 'on') {
        $sendTo   = strtolower($row[0]);
        mail($sendTo, $subject, $body, $headers);
    }


}


/*

*/

$userid    = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

$arrayCamp = Array('txtseller','txtbuyer','txtaddress',
                   'txtcounty','txtlegal1','txtlegal2',
                   'txtprice','txtlistingsales','txtbuyeraddress1',
                   'txtselleraddress1','txtpage','txtdatebuyer1',
                   'txtdatebuyer2','txtcollectedfunds');

conectarPorBd('fl'.strtolower($county));

// ----------------------

$valores      = Array();
$balancevalue = 0;
$query        = "SELECT * FROM xima.contracts_default c
                    WHERE contract='".$type."' AND `defaultoption`='Y' AND userid=".$userid;
$result       = mysql_query($query) or die($query.mysql_error());
$total        = mysql_num_rows($result);


if ( $total>0 ) {

    $r              = mysql_fetch_array($result);
    $valores[1]     = $r['name'];
    $valores[8]     = $r['address'];
    $usatemplate    = $r['template'];
    $activotemplate = $r['activetemplate'];

    if ( $usatemplate=='Y' && $activotemplate=='Y' ) {

        $id    = $r['id'];
        $q     = "SELECT min(id) AS minid FROM xima.contracts_default c
                    WHERE contract='".$type."' AND userid=".$userid;
        $res   = mysql_query($q) or die($q.mysql_error());
        $ro    = mysql_fetch_array($res);
        $minid = $ro['minid'];

        if ( $id==$minid )
            $pdfusar = '1';
        else
            $pdfusar = '2';

        $borrartemplate = 'Y';

    } else {

        $pdfusar        = 'default';
        $borrartemplate = 'N';
    }

} else {

    $query  = "SELECT
                x.`HOMETELEPHONE`, x.`ADDRESS`, x.`STATE`, x.`CITY`, x.`NAME`, x.`SURNAME`
                FROM xima.ximausrs x WHERE userid=".$userid;

    $result         = mysql_query($query) or die($query.mysql_error());
    $r              = mysql_fetch_array($result);
    $valores[1]     = $r['NAME'].' '.$r['SURNAME'];
    $valores[8]     = $r['ADDRESS'];
    $pdfusar        = 'default';
    $borrartemplate = 'N';

}

// ----------------------

//Obtener valores public record
$query        = "SELECT owner, address, ozip, city, owner_a, phonenumber1 from psummary where parcelid='".$parcelid."'";
$result       = mysql_query($query) or die($query.mysql_error());
$r            = mysql_fetch_array($result);
$valores[0]   = $r['owner'];
$valores[3]   = $county;
$valores[9]   = $r['owner_a'];
$address      = $r['address'];
$city         = $r['city'];
$zip          = $r['ozip'];
$valores[4]   = "Folio Number: ".$parcelid."  Legal Description as Shown in Public Records.";

//Obtener datos mlsresidential
$query        = "SELECT lprice, agent, address, city, zip from mlsresidential where parcelid='".$parcelid."'";
$result       = mysql_query($query) or die($query.mysql_error());
$r            = mysql_fetch_array($result);

if ($contrOpc == '1')
    $r['lprice'] = $offer;

$valores[6]   = number_format($r['lprice'],'2','.',',');
$balancevalue = $balancevalue+$r['lprice'];
$valores[7]   = $r['agent'];

if($address=='')
    $address  = $r['address'];

if($city=='')
    $city     = $r['city'];

if($zip=='')
    $zip      = $r['zip'];

$valores[2]   = $address.', '.$city.', '.$zip;
$valores[10]  = '1';

$fecha        = date('m-d-Y');
$valores[11]  = $fecha;
$valores[12]  = $fecha;

// ----------------------

$contratos       = Array('ContractPurchase','leadBasedPaint','ResidentialContract');
$camposContratos = Array('contract','lead','residential');

$ruta            = getCwd();
$s               = 0;

if ($pdfusar!='default') {
    //Tomar plantilla de cliente
    $templatePdf = $contratos[$type].$userid.$pdfusar;

    //$nombre='C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/campos.pdf.fields';
    $orig  = "C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/{$templatePdf}.pdf";
    $dest  = "{$ruta}/{$templatePdf}.pdf";
    copy($orig,$dest);

    $nombre      = $ruta.'/campos.pdf.fields';

    if (file_exists($nombre))
        unlink($nombre);

    passthru( $ruta.'/pdftk/pdftk '.$templatePdf.'.pdf dump_data_fields >> '.$nombre );

    $fields = $nombre;

} else {
    $fields      = $camposContratos[$type].'.pdf.fields';
    $templatePdf = $contratos[$type];
}

// ----------------------

//echo $userid.' '.$fields.' '.$templatePdf;
//Recorre Form fields para obtener array de campos a llenar
$campos                 = Array();
$valoresPredeterminados = Array();
$field_arr              = load_field_data($fields);

foreach( $field_arr as $field ) { // itera en los campos
    $campos[]=$field['FieldName:'];
    $valoresPredeterminados[]=$field['FieldValue:'];

    if ( $field['FieldName:']=='txtdeposit' || $field['FieldName:']=='txtaditional' || $field['FieldName:']=='txtotherprice' )
        $balancevalue=$balancevalue-$field['FieldValue:'];

}

$valores[13] = number_format($balancevalue,'2','.',',');


// ----------------------

require_once( 'forge_fdf.php' );

$fdf_data_strings = array();
$fdf_data_names   = array();

//Manda valores a la plantilla pdf
foreach( $campos as $key => $value ) {
    // translate tildes back to periods
    // $fdf_data_strings[ strtr(str_replace('_',' ',$key), '~', '.') ]= $value;

    $encontrado = true;
    $count      = 0;
    while ($encontrado && $count<count($arrayCamp)) {

        if ($value==$arrayCamp[$count]) {
            $encontrado = false;
            $fdf_data_strings[ strtr($arrayCamp[$count], '~', '.') ] = $valores[$count];
        }

        $count++;

    }
    if($encontrado)
        $fdf_data_strings[ strtr($value, '~', '.') ] = $valoresPredeterminados[$key];

//  if(strtr('txtdateclosing', '~', '.'))
//      echo "{success:true, pdf:'$count'}";

}
if ($contrOpc == '1') {
    $fdf_data_strings[ strtr('txtdateacceptance1', '~', '.') ] = $dateAcc;
    $fdf_data_strings[ strtr('txtdateclosing', '~', '.') ]     = $dateClo;
}
// ----------------------

// ignore these in this example
$fields_hidden   = array();
$fields_readonly = array();

$fdf= forge_fdf( '', $fdf_data_strings,
                     $fdf_data_names,
                     $fields_hidden,
                     $fields_readonly );
//echo $fdf; //break;
$fdf_fn = tempnam( '.', 'fdf' );
$fp     = fopen( $fdf_fn, 'w' );

if( $fp ) {

   fwrite( $fp, $fdf );
   fclose( $fp );

   $file    = 'FPDF/generated/'.$contratos[$type].$parcelid.'.pdf';

   $aux = "";
   if ($contrOpc == '1')
       $aux = "flatten";

   passthru("{$ruta}/pdftk/pdftk {$templatePdf}.pdf fill_form {$fdf_fn} output C:/inetpub/wwwroot/{$file} $aux");

   $archivo = 'C:/inetpub/wwwroot/'.$file  ;

    if ($sendmail=='on' or $sendme=='on')
        SendEMail($archivo,$mlnumber,$address,$userid,$rname,$remail,$sendmail,$sendme);


   unlink( $fdf_fn ); // delete temp file

   if($borrartemplate=='Y')
        unlink($templatePdf.'.pdf');

   echo "{success:true, pdf:'$file', number:'$mlnumber'}";

} else {

   echo '{msg: unable to write temp fdf file}';

}


// ----------------------

//Carga los fields del archivo que contiene el form generado con pdftk
function load_field_data( $field_report_fn ) {

    $ret_val = array();
    $fp      = fopen( $field_report_fn, "r" );

    if( $fp ) {
        $line = '';
        $rec  = array();

        while(($line= fgets($fp, 2048))!== FALSE) {

            $line = rtrim( $line ); // remueve espacios en blanco

            if( $line== '---' ) {
                if( 0 < count($rec) ) { // final del registro
                    $ret_val[] = $rec;
                    $rec       = array();
                }
                continue; // salta a la siguiente liena
            }

            // divide la linea entre nombre y valor
            $data_pos = strpos( $line, ':' );
            $name     = substr( $line, 0, $data_pos+ 1 );
            $value    = substr( $line, $data_pos+ 2 );

            if( $name == 'FieldStateOption:' ) {
                // empaqueta FieldStateOption en su propio array
                if( !array_key_exists('FieldStateOption:',$rec) )
                    $rec['FieldStateOption:']= array();

                $rec['FieldStateOption:'][]= $value;

            } else
                $rec[ $name ]= $value;

        }

        if( 0< count($rec))  // empaqueta el final del registro
            $ret_val[]= $rec;

        fclose( $fp );
    }

    return $ret_val;

}

?>
