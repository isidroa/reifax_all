<div align="left" class="search_realtor_fondo" id="FR_search_div" style="display:none;padding-left:5px;">
    <table border="0" cellpadding="0" cellspacing="0"  style="font-size:12px; margin:auto;">
        <tr class="search_realtor_titulo">    	    	
            <td colspan="2" width="17%">Data</td>                
            <td colspan="2" width="17%">Search By</td>                              
            <td colspan="2" width="17%">State</td>         
            <td colspan="2"  width="17%" >County</td>      	
            <td colspan="2"  width="17%" >Type</td>
            <td width="17%" >Foreclosed Status</td>
        </tr>
        <tr>    	    	
            <td><select name="combo_search_types" id="FR_combo_search_types" style="width:145px;" onchange="doSearchTypeFilter(this.value);">
            <option value="PR" >Public Records</option> 
            <option value="FS" >For Sale</option>
            <option value="FR" selected="selected">For Rent</option>
            <option value="FO" >Foreclosures</option> 
            <option value="BO" >By Owner</option>
            <option value="BOR" >By Owner Rent</option>
            </select></td>
            <td width="5">&nbsp;</td>
            <td><select name="combo_search_by" id="FR_combo_search_by" style="width:145px;" onchange="doSearchByFilter(this.value);">
            <option value="LOCATION" <?php if($search_by_type=='LOCATION') echo 'selected="selected"';?>>Location</option>
            <option value="MAP" <?php if($search_by_type=='MAP') echo 'selected="selected"';?>>Map</option>
            </select></td>
            <td width="5">&nbsp;</td>
            <td><select name="state_search" id="FR_state_search" style="width:145px;" onchange="doChangeState(this.value);">
            <?php
                $query='select idstate,state FROM xima.lsstate where is_showed="Y" order by state';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($state_search==$r['idstate'])
                        echo '<option value="'.$r['idstate'].'" selected="selected">'.$r['state'].'</option>';
                    else
                        echo '<option value="'.$r['idstate'].'">'.$r['state'].'</option>';
                }
            ?>
            </select></td>
            <td width="5">&nbsp;</td>
            <td>
            <select name="county_search" id="FR_county_search" style="width:145px;" onchange="if(search_by_type=='MAP') mapSearch.centerMapCounty(this.value,true); else mapSearch.centerMapCounty(this.value,false); search_county=this.value;">
            <?php
                $query='select idcounty,county FROM xima.lscounty where ximapro="Y" and idstate='.$state_search.' order by county';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($county_search==$r['idcounty'])
                        echo '<option value="'.$r['idcounty'].'" selected="selected">'.$r['county'].'</option>';
                    else
                        echo '<option value="'.$r['idcounty'].'">'.$r['county'].'</option>';
                }
            ?>
            </select></td>
            <td width="5">&nbsp;</td>
            <td>
                <select name="proptype" id="FR_proptype" style="width:145px;" onchange="verEquity(this.value,'FR_pequity','FR_tpequity');"><option value="">Any Type</option><option value="01" >Single Family</option><option value="04" >Condo/Town/Villa</option><option value="08" >Multi Family</option><option value="11" >Commercial</option><option value="00" >Vacant Land</option><option value="02" >Mobile Home</option></select>
            </td>
            <td width="5">&nbsp;</td>
            <td>
            <select name="pendes" id="FR_pendes" style="width:140px;"><option value="-1">Any</option><option value="N" >No</option><option value="P" >Pre-Foreclosed</option><option value="F" >Foreclosed</option></select>
            </td>
        </tr>
        <tr class="search_realtor_titulo">
        	<td colspan="6" >
                <span id="FR_tsearch_l">Location</span>
            </td>
            <td colspan="4" id="FR_tmlnumber">Parcelid</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="5">
                <input type="text" name="search" id="FR_search" size="40" maxlength="2048" style="font-size:17px; width:475px;" value="Address, City or Zip Code" onfocus="colocarDefault('FR_search',this.value);" onblur="colocarDefault2('FR_search',this.value);">
            </td>
            <td width="5">&nbsp;</td>
            <td colspan="3">
                <!--<input type="text" name="mlnumber" id="FR_mlnumber" style="width:310px;" value="Mlnumber" onfocus="if(this.value=='Mlnumber') this.value='';" onblur="if(this.value=='') this.value='Mlnumber';">-->
                <input type="text" name="parcelid" id="FR_parcelid" style="width:310px;" value="Parcelid" onfocus="if(this.value=='Parcelid') this.value='';" onblur="if(this.value=='') this.value='Parcelid';">
            </td>
            <td width="5">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="search_realtor_titulo">    	    	
            <td colspan="4" id="FR_tprice" >Price Range</td>                
            <td colspan="2" id="FR_tbeds" >Beds</td>                
            <td colspan="2" id="FR_tbath" >Baths</td>                
            <td colspan="2" id="FR_tsqft" >Sqft</td>                 
            <td id="FR_toccupied">O. Occupied</td>       	
        </tr>
        <tr>
        	<td colspan="2">
                <span id="FR_price_dol1" style="color:#FFF;vertical-align:top;">$</span><input id="FR_price_low" style="width:138px;" type="text" name="price_low" value="min" size="6" onfocus="if(this.value=='min')this.value='';" onblur="if(this.value=='')this.value='min';"><span id="FR_price_to" style="color:#FFF;vertical-align:top;padding-left:3px;">to</span>
            </td>
            <td>
                <span id="FR_price_dol2" style="color:#FFF;vertical-align:top;">$</span><input id="FR_price_hi" style="width:138px;" type="text" name="price_hi" value="max" size="6" onfocus="if(this.value=='max')this.value='';" onblur="if(this.value=='')this.value='max';">
            </td>        
            <td width="5">&nbsp;</td>
            <td>
                <select name="bed" id="FR_bed" style="width:145px;"><option value="-1">Any</option><option value="1" >1+</option><option value="2" >2+</option><option value="3" >3+</option><option value="4" >4+</option><option value="5" >5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td>
                <select name="bath" id="FR_bath" style="width:145px;"><option value="-1">Any</option><option value="1" >1+</option><option value="2" >2+</option><option value="3" >3+</option><option value="4" >4+</option><option value="5" >5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td>
                <select name="sqft" id="FR_sqft" style="width:145px;"><option value="-1">Any</option><option value="250" >250+</option><option value="500" >500+</option><option value="1000" >1,000+</option><option value="1250" >1,250+</option><option value="1500" >1,500+</option><option value="1750" >1,750+</option><option value="2000" >2,000+</option><option value="2250">2,250+</option><option value="2500">2,500+</option><option value="2750" >2,750+</option><option value="3000" >3,000+</option><option value="3250" >3,250+</option><option value="3500" >3,500+</option><option value="3750" >3,750+</option><option value="4000" >4,000+</option><option value="5000" >5,000+</option><option value="10000" >10,000+</option></select>
            </td>       	
            <td width="5">&nbsp;</td>
            <td>
               	<select name="occupied" id="FR_occupied" style="width:140px;"><option value="-1">Any</option><option value="Y">Yes</option><option value="N">No</option></select>
            	<input type="hidden" name="realtor" id="FR_realtor" value="<?php echo $realtor; ?>" />
            </td>
        </tr>    
    </table>
</div>