<div id="grid-example<?php echo '_'.$_GET['subTypeTab'];?>"></div>
<?php
	if(isset($_GET['typeTab'])){
		switch($_GET['typeTab']){
			case 'buyers':
				if($_GET['subTypeTab']=='outState' || $_GET['subTypeTab']=='internationalBuyers'){
					$groupParams='&resultby=ownername&filter_buyer_owns=1&search_filter_groupby=COTRIN';
					$groupParams.='&search_filter_groupby_type='.$_GET['subTypeTab'];
				}else{
					$groupParams='&resultby=ownername&filter_buyer_owns=1&search_filter_groupby=COTR';
					$groupParams.='&search_filter_groupby_type='.$_GET['subTypeTab'];
				}
			break;
			case 'mortgage':
				if($_GET['subTypeTab']=='lender'){
					$groupParams='&resultby=lender&filter_buyer_owns=0';
					$groupParams.='&search_filter_groupby_type='.$_GET['typeTab'].$_GET['subTypeTab'];
				}
			break; 
			case 'foreclosures':
				if($_GET['subTypeTab']=='plaintiff' || $_GET['subTypeTab']=='sold'){ 
					$groupParams='&resultby=plaintiff&filter_buyer_owns=0';
					$groupParams.='&search_filter_groupby_type='.$_GET['typeTab'].$_GET['subTypeTab'];
				}
			break;
			case 'bank':
				if($_GET['subTypeTab']=='plaintiff' || $_GET['subTypeTab']=='sold'){ 
					$groupParams='&resultby=plaintiff&filter_buyer_owns=0';
					$groupParams.='&search_filter_groupby_type='.$_GET['typeTab'].$_GET['subTypeTab'];
				}
			break;
		}
	}
?>
<script>
	var selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?> = new Array();
	var AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>=false;
	var limitRG<?php echo '_'.$_GET['subTypeTab'];?>=50;
	var filterDate<?php echo '_'.$_GET['subTypeTab'];?> = null;
	var filterName<?php echo '_'.$_GET['subTypeTab'];?> = null;
	discardLocation = null;
	analyze_type = 'null';
	analyze_ptype = 'null';
	
///////////Cargas de data dinamica///////////////
	var storeRG<?php echo '_'.$_GET['subTypeTab'];?> = new Ext.data.Store({
		proxy:      new Ext.data.HttpProxy({ url: 'coresearch.php?resultType=advance&systemsearch=<?php echo $_POST['systemsearch'] ?>&groupbylevel=1<?php echo $groupParams;?>', timeout: 3600000 }),
		baseParams: {search_filter_groupby_date: filterDate<?php echo '_'.$_GET['subTypeTab'];?>, 
			search_filter_groupby_name: filterName<?php echo '_'.$_GET['subTypeTab'];?>},
		reader: new Ext.data.JsonReader(),
		remoteSort: true,
		listeners: {
			'load': function(store,data,obj){
				if (AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>){
					Ext.get(gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>=true;
					gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getSelectionModel().selectAll();
					selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>=new Array();
				}else{
					AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>=false;
					var sel = [];
					if(selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>.length > 0){
						for(val in selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>){
							var ind = gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getStore().find('pid',selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
	});
///////////FIN Cargas de data dinamica///////////////
    
	var smRG<?php echo '_'.$_GET['subTypeTab'];?> = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>.indexOf(record.get('pid'))==-1)
					selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>.push(record.get('pid'));
				
				if(Ext.fly(gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?> = selected_dataRG<?php echo '_'.$_GET['subTypeTab'];?>.remove(record.get('pid'));
				AllCheckRG<?php echo '_'.$_GET['subTypeTab'];?>=false;				
			}
		}
	});
		

//-------------------- Creacion del Grid----------------
	var gridRG<?php echo '_'.$_GET['subTypeTab'];?> = new Ext.grid.GridPanel({
		renderTo: 'grid-example<?php echo '_'.$_GET['subTypeTab'];?>',	
		loadMask : true,	  
        store: storeRG<?php echo '_'.$_GET['subTypeTab'];?>,
		columns: [],
		border: false,
		enableColLock: false,
		height: windowHeight-200,  //dimensiones del grid
        width: 'auto',
		sm: smRG<?php echo '_'.$_GET['subTypeTab'];?>,
		listeners: {
			"rowdblclick": function(grid, row, e) {
				var record = this.store.getAt(row);
				var pid = record.get('pid');
				var groupselect = record.get('groupselect');
				//alert(pid+': '+owner);
				if(document.getElementById('resultFromGroupTab')){
					var tab = tabs.getItem('resultFromGroupTab');
					tabs.remove(tab);
				}
				
				discardLocation = null;
				analyze_type = 'null';
				analyze_ptype = 'null';
				
				tabs.add({
					title: '<?php echo $_POST['title'];?>',
					id: 'resultFromGroupTab',
					autoLoad: {url: 'result_tabs/properties_advance_result.php?<?php echo substr($groupParams,1);?>', timeout: 10800, scripts: true, params: {systemsearch: '<?php echo $_POST['systemsearch'];?>', groupbylevel: 2, groupselect: groupselect, subTypeTab: '<?php echo $_GET['subTypeTab'];?>', search_filter_groupby_date: filterDate<?php echo '_'.$_GET['subTypeTab'];?>, search_filter_groupby_name: filterName<?php echo '_'.$_GET['subTypeTab'];?>, search_filter_discardLocation: discardLocation, search_filter_analyze_type: analyze_type, search_filter_analyze_ptype: analyze_ptype}},
					closable: true, 
					tbar: new Ext.Toolbar({
						id:'menu_result_advFG',
						cls: 'no-border',
						width: 'auto',
						items: [' ',
							{
								 tooltip: 'Click to Show/Hide Map',
								 text: 'Map',
								 id: 'toolbarMapResultAdvFG',
								 iconCls:'icon',
								 icon: 'http://www.reifax.com/img/toolbar/map.png',
								 iconAlign: 'top',
								 width: 40,
								 scale: 'medium',
								 enableToggle: true,
								 handler: function(){
									if(document.getElementById('mapResultAdvFG').style.display=='none'){
										document.getElementById('mapResultAdvFG').style.display='';
										if(user_loged)
											mapResultAdvFG.map.Resize(system_width-20,320);
										else
											mapResultAdvFG.map.Resize(620,320);
											
										mapResultAdvFG.map.SetMapView(arrLatLongFG);
									}else
										document.getElementById('mapResultAdvFG').style.display='none';
								 }
							},{
								 tooltip: 'Click to Print Report',
								 text: 'Pdf',
								 iconCls:'icon',
								 iconAlign: 'top',
								 width: 40,
								 icon: 'http://www.reifax.com/img/toolbar/printer.png',
								 scale: 'medium',
								 handler: function(){
									if(!user_loged || user_web){ login_win.show(); return false;} 
									if(user_block){ Ext.MessageBox.alert('Warning','You are not allowed to view - print detailed information.'); return false;}
									
									var parcelids_res='';
									if(!AllCheckRFG){
										var results = selected_dataRFG;
										if(results.length > 0){
											parcelids_res=results[0];
											for(i=1; i<results.length; i++){
												parcelids_res+=','+results[i];
											}
										}else{
											Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
										}
									}
									
									loading_win.show();
									Ext.Ajax.request( 
										{  
											waitMsg: 'Printing Report...',
											url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
											method: 'POST', 
											params: {
												userweb:user_web,
												realtorid:user_webid,
												parcelids_res:parcelids_res,
												template_res: -1,
												groupbylevel: 2,
												groupselect: groupselect,
												resultby: 'ownername',
												filter_buyer_owns: 1,
												search_filter_groupby: '<?php echo (($_GET['subTypeTab']=='outState' || $_GET['subTypeTab']=='internationalBuyers')? 'COTRIN' : 'COTR');?>',
												search_filter_groupby_type: '<?php echo $_GET['subTypeTab'];?>',
												search_filter_groupby_date: filterDate<?php echo '_'.$_GET['subTypeTab'];?>,
												search_filter_groupby_name: filterName<?php echo '_'.$_GET['subTypeTab'];?>,
												search_filter_discardLocation: discardLocation,
												search_filter_analyze_type: analyze_type, 
												search_filter_analyze_ptype: analyze_ptype
											},
											
											failure:function(response,options){
												Ext.MessageBox.alert('Warning','Your result must have less than 1000 records.');
												loading_win.hide();
											},
											success:function(response,options){
												var rest = Ext.util.JSON.decode(response.responseText);
												//alert(rest.pdf);
												var url='http://www.reifax.com/'+rest.pdf;
												//alert(url);
												loading_win.hide();
												window.open(url);
												
											}                                
										 }
									);
								 }
							},{
								tooltip: 'Click to Excel Report',
								text: 'Excel',
								iconCls:'icon',
								iconAlign: 'top',
								width: 40,
								icon: 'http://www.reifax.com/img/toolbar/excel.png',
								scale: 'medium',
								handler: function(){
									if(!user_loged || user_web){ login_win.show(); return false;} 
									if(user_block){ Ext.MessageBox.alert('Warning','You are not allowed to view - print Excel detailed information.'); return false;} 
									
									var ownerShow='false';
									var parcelids_res='';
									if(!AllCheckRFG){
										var results = selected_dataRFG;
										if(results.length > 0){
											parcelids_res=results[0];
											for(i=1; i<results.length; i++){
												parcelids_res+=','+results[i];
											}
										}else{
											Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
										}
									}
									
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Excel Report...',
										url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
										method: 'POST', 
										params: {
											userweb:user_web,
											realtorid:user_webid,
											ownerShow: ownerShow,
											parcelids_res:parcelids_res,
											template_res: -1,
											groupbylevel: 2,
											groupselect: groupselect,
											resultby: 'ownername',
											filter_buyer_owns: 1,
											search_filter_groupby: '<?php echo (($_GET['subTypeTab']=='outState' || $_GET['subTypeTab']=='internationalBuyers')? 'COTRIN' : 'COTR');?>',
											search_filter_groupby_type: '<?php echo $_GET['subTypeTab'];?>',
											search_filter_groupby_date: filterDate<?php echo '_'.$_GET['subTypeTab'];?>,
											search_filter_groupby_name: filterName<?php echo '_'.$_GET['subTypeTab'];?>,
											search_filter_discardLocation: discardLocation,
											search_filter_analyze_type: analyze_type, 
											search_filter_analyze_ptype: analyze_ptype
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Your result must have less than 1000 records.');
										},
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.excel;
											loading_win.hide();
											//alert(url);
											location.href= url;
											//window.open(url);
										}                                
									});
									
									
								}
								
							},{
								tooltip: 'Click to Analyze',
								text: 'Analyze',
								iconCls:'icon',
								iconAlign: 'top',
								width: 40,
								icon: 'http://www.reifax.com/img/toolbar/analyze.png',
								scale: 'medium',
								handler: function(){
									analyze_type='holders'//cashBuyers,holders
									analyze_ptype=''//search campo Type
									
									var simple = new Ext.FormPanel({
										labelWidth: 150, 
										url:'',
										frame:true,
										title: 'Analyze Owner',
										bodyStyle:'padding:5px 5px 0',
										width: 400,
										waitMsgTarget : 'Analizing...',
										
										items: [{
												xtype: 'combo',
												editable: false,
												displayField:'title',
												valueField: 'val',
												store: new Ext.data.SimpleStore({
													fields: ['val', 'title'],
													data : [
														['cashBuyers','Cash'],
														['holders','Holders']
													]
												}),
												name: 'analyze_type_name',
												hiddenName: 'analyze_type',
												fieldLabel: 'Type',
												mode: 'local',
												value: analyze_type,
												triggerAction: 'all',
												selectOnFocus:true,
												allowBlank:false
											},{
												xtype: 'combo',
												editable: false,
												displayField:'title',
												valueField: 'val',
												store: new Ext.data.SimpleStore({
													fields: ['val', 'title'],
													data : [
															['','Any Type'],
															['01','Single Family'],
															['04','Condo/Town/Villa'],
															['03','Multi Family +10'],
															['08','Multi Family -10'],
															['11','Commercial'],
															['00','Vacant Land'],
															['02','Mobile Home'],
															['99','Other']
													]
												}),
												name: 'analyze_ptype_name',
												hiddenName: 'analyze_ptype',
												fieldLabel: 'Property Type',
												mode: 'local',
												value: analyze_ptype,
												triggerAction: 'all',
												selectOnFocus:true,
												allowBlank:false
											}
										],
								
										buttons: [{
											text: 'Apply',
											handler  : function(){
												var values = simple.getForm().getValues();
												analyze_type=values.analyze_type;
												analyze_ptype=values.analyze_ptype;
												discardLocation = true;
												
												storeRFG.setBaseParam('search_filter_discardLocation', discardLocation);
												storeRFG.setBaseParam('search_filter_analyze_type', analyze_type);
												storeRFG.setBaseParam('search_filter_analyze_ptype', analyze_ptype);
												Ext.getCmp('pagingRFG').doLoad(0);
												win.close();
											}
										},{
											text: 'Restar',
											handler  : function(){
												analyze_type='null';
												analyze_ptype='null';
												discardLocation = null;
												
												storeRFG.setBaseParam('search_filter_discardLocation', discardLocation);
												storeRFG.setBaseParam('search_filter_analyze_type', analyze_type);
												storeRFG.setBaseParam('search_filter_analyze_ptype', analyze_ptype);
												Ext.getCmp('pagingRFG').doLoad(0);
												win.close();
											}
										}]
									});
									win = new Ext.Window({
										
										layout      : 'fit',
										width       : 370,
										height      : 200,
										modal	 	: true,
										plain       : true,
										items		: simple,
							
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
									win.addListener("beforeshow",function(win){
										simple.getForm().reset();
									});
								}
							},
							'->',{ 
								 tooltip: 'Click to Close <?php echo $_POST['title'];?>',
								 text: 'Close',
								 iconCls:'icon',
								 iconAlign: 'top',
								 width: 40,
								 icon: 'http://www.reifax.com/img/cancel.png',
								 scale: 'medium',
								  
								 handler: function(){
									 var tab = tabs.getItem('resultFromGroupTab');
									 tabs.remove(tab);
								 }
							  }
						],
						autoShow: true
					})
				}).show();
	
				if(Ext.isIE){
					var tab = tabs.getItem('resultFromGroupTab');
					tabs.setActiveTab(tab);
					tab.getEl().repaint();
				} 
			}
		},
		tbar: new Ext.PagingToolbar({
			id: 'pagingRG<?php echo '_'.$_GET['subTypeTab'];?>',
            pageSize: limitRG<?php echo '_'.$_GET['subTypeTab'];?>,
            store: storeRG<?php echo '_'.$_GET['subTypeTab'];?>,
            displayInfo: true,
			displayMsg: 'Total: {2} Records',
			emptyMsg: "No records to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 records per page.',
				text: 50,
				handler: function(){
					limitRG<?php echo '_'.$_GET['subTypeTab'];?>=50;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').pageSize = limitRG<?php echo '_'.$_GET['subTypeTab'];?>;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 records per page.',
				text: 80,
				handler: function(){
					limitRG<?php echo '_'.$_GET['subTypeTab'];?>=80;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').pageSize = limitRG<?php echo '_'.$_GET['subTypeTab'];?>;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 records per page.',
				text: 100,
				handler: function(){
					limitRG<?php echo '_'.$_GET['subTypeTab'];?>=100;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').pageSize = limitRG<?php echo '_'.$_GET['subTypeTab'];?>;
					Ext.getCmp('pagingRG<?php echo '_'.$_GET['subTypeTab'];?>').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			})
			]
        })
	});

 
 	storeRG<?php echo '_'.$_GET['subTypeTab'];?>.on('metachange', function(){
		if(typeof(storeRG<?php echo '_'.$_GET['subTypeTab'];?>.reader.jsonData.columns) === 'object') {
			var columns = [];
			columns.push(smRG<?php echo '_'.$_GET['subTypeTab'];?>);
			Ext.each(storeRG<?php echo '_'.$_GET['subTypeTab'];?>.reader.jsonData.columns, function(column){
				columns.push(column);
			});

			gridRG<?php echo '_'.$_GET['subTypeTab'];?>.getColumnModel().setConfig(columns);
		}
	});

	/////////////Inicializar Grid////////////////////////
	storeRG<?php echo '_'.$_GET['subTypeTab'];?>.load();
	/////////////FIN Inicializar Grid////////////////////



</script>