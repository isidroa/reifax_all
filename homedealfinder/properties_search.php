<?php  
if(!isset($_COOKIE['datos_usr']['USERID'])){
	echo '<script> document.location=\'http://www.reifax.com\';</script>';
}	
	include('properties_conexion.php');
	conectar();

   	include("templates/properties_template.php");
?>
<head>
<?php tagHeadHeader();?>
<script type="text/javascript" src="includes/properties_generator.js"></script>
<script type="text/javascript" src="includes/properties_myaccount.js"></script>
<script type="text/javascript" src="includes/properties_draw.js"></script>
<link href="includes/css/draw.css" rel="stylesheet" type="text/css" />
</head>
<?php 
	$cliente=1;
	include('advertisingLoadNew.php');
?>
<body>
<div align="center">
	<div id="body_central">
        <div id="layout_center" align="center" style=" position:relative;"><?php topLoginHeader(true, false, true, true, true, true, true, false);?>
          <div align="center" style="width:100%; margin:auto;height:100%; clear:both;">
                <div id="tabs" style="height:100%;"></div>
           </div>
        </div>
         
        <div id="layout_top"><div id="T1" style="height:10px; vertical-align:middle;"><?php echo $top;?></div></div>
       
        <div id="layout_bottom"><div id="B1" style="height:110px; vertical-align:middle;"><?php echo $bottom;?></div></div>
        
        <div id="layout_left"><?php echo $left;?></div>
        
        <div id="layout_right"><?php echo $right;?></div>
	</div>
</div>
<?php googleanalytics(); ?>

<script>
<?php 	
	echo "var realtor_block=false;";
	echo "var investor_block=false;";
	echo "var webmaster_block=false;";
	
	echo "var user_web=false;";
	echo "var owner_web=false;";	
	echo "var master_web=false;";	
	echo "var user_webid='';";
	echo "var owner_webid='';";
	echo "var master_webid='';";
	
	echo "var order='';";
	echo "var search_type='FS';";
			
	if(isset($_COOKIE['datos_usr']['USERID']) || isset($_SESSION['datos_usr'])){
		echo "var user_loged=true;"; 
		echo "var user_name_menu='".$_COOKIE['datos_usr']['NAME'].' '.$_COOKIE['datos_usr']['SURNAME']."';";
		echo "var useridlogin='".$_COOKIE['datos_usr']['USERID']."';";
		echo "systemwhat=1;";
		
		//User Default State
		$query='SELECT idstate FROM userstate WHERE defstate=1 AND userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			$defstate=$r[0];
		}
		
		//User Default County
		$query='SELECT idcounty FROM usercounty WHERE defcounty=1 AND userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			$defcounty=$r[0];
		}
	}else{
		echo "var user_loged=false;";
		echo "var user_name_menu='';";
		echo "var useridlogin='';";
	}
?>

Ext.QuickTips.init();
var mapSearch = null;
var tabs=tabs3=viewport=null;
var search_by_type='LOCATION';
var search_state='<?php echo isset($defstate) ? $defstate : 1; ?>';
var search_county='<?php echo isset($defcounty) ? $defcounty : 1; ?>'; 
var user_block=false;

var system_width=document.body.offsetWidth;
	
if(system_width>1000) system_width=980;
//if(system_width>1260) system_width=1260;

document.getElementById('layout_center').style.width= system_width+'px';

function doSearchTypeFilter(value){
	value='FS';
	search_type = value;
	//ocultar todos los search
	if(document.getElementById('PR_search_div')) document.getElementById('PR_search_div').style.display='none';
	if(document.getElementById('FR_search_div')) document.getElementById('FR_search_div').style.display='none';
	if(document.getElementById('FS_search_div')) document.getElementById('FS_search_div').style.display='none';
	if(document.getElementById('FO_search_div')) document.getElementById('FO_search_div').style.display='none';
	if(document.getElementById('BO_search_div')) document.getElementById('BO_search_div').style.display='none';
	if(document.getElementById('BOR_search_div')) document.getElementById('BOR_search_div').style.display='none';
	//se muestra el search seleccionado
	document.getElementById(search_type+'_search_div').style.display='';
	
	document.getElementById('pformul').reset();
	
	document.getElementById(search_type+'_state_search').value=search_state;
	doChangeState(search_state);
	
	
	if(search_by_type=='MAP_OFF' || search_by_type=='MAP' || search_by_type=='LOCATION'){
		if(search_by_type=='MAP'){ 
			doSearchByFilter('MAP_OFF');
			search_by_type='MAP';
		}
		
		document.getElementById(search_type+'_combo_search_by').value=search_by_type;
		doSearchByFilter(search_by_type);
	}else if(search_by_type=='PARCELID'){
		document.getElementById(search_type+'_combo_search_by').value=search_by_type;
		doSearchByFilter(search_by_type);
	}else if(search_by_type=='MLNUMBER'){
		if(search_type=='BO' || search_type=='BOR' || search_type=='FS' || search_type=='FR'){
			document.getElementById(search_type+'_combo_search_by').value=search_by_type;
			doSearchByFilter(search_by_type);
		}else{
			search_by_type='LOCATION';
			document.getElementById(search_type+'_combo_search_by').value=search_by_type;
			doSearchByFilter(search_by_type);
		}
	}else if(search_by_type=='CASE'){
		if(search_type=='FO'){
			document.getElementById(search_type+'_combo_search_by').value=search_by_type;
			doSearchByFilter(search_by_type);
		}else{
			search_by_type='LOCATION';
			document.getElementById(search_type+'_combo_search_by').value=search_by_type;
			doSearchByFilter(search_by_type);
		}
	}
}


function doSearchByFilter(value){
	search_by_type = value;
	
	if(search_by_type=='MAP_OFF'){
		mapSearch.control_map();
		search_by_type='LOCATION'
	}
	if(search_by_type=='MAP'){
		mapSearch.control_map();
		mapSearch.centerMapCounty(document.getElementById(search_type+'_county_search').value,true);
		
		cambiarDefault(search_type+'_search','Address, City or Zip Code');
	}
	if(search_by_type=='LOCATION'){
		cambiarDefault(search_type+'_search','Address, City or Zip Code');
	}
	if(search_by_type=='MLNUMBER'){
		cambiarDefault(search_type+'_search','Mlnumber');
	}
	if(search_by_type=='PARCELID'){
		cambiarDefault(search_type+'_search','Parcelid');
	}
	if(search_by_type=='CASE'){
		cambiarDefault(search_type+'_search','Case-Number');
	}
	if(search_by_type=='GPS'){
		cambiarDefault(search_type+'_search','My Location');
	}
}

function doChangeState(newState){
	search_state=newState;
	var comboCounty = document.getElementById(search_type+'_county_search');
	comboCounty.length=0;
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Waiting...',
		url: 'properties_setCounty.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			idstate: newState
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var results=Ext.decode(response.responseText), i=0;

			while(i<results.total){
				if(search_county!=results.records[i].idcounty)
					comboCounty.options[comboCounty.options.length] = new Option(results.records[i].county,results.records[i].idcounty,false);
				else
					comboCounty.options[comboCounty.options.length] = new Option(results.records[i].county,results.records[i].idcounty,true);
				i++;
			}
		}
	});
}

Ext.onReady(function(){
	search_type='FS';
	search_by_type='LOCATION';
	master_web=false;
	var system_width  = document.body.offsetWidth;
	var system_height = window.innerHeight;
		
	if(system_width>1000) system_width=980;
	//if(system_width>1260) system_width=1260;
	
	tabs = new Ext.TabPanel({
		id: 'principal',
        renderTo: 'tabs',
        activeTab: 1, 
		height: 2850,
		width: system_width,
        plain:true,
		enableTabScroll:true,
		defaults:{	autoScroll: false},
        items:[
		{
			title: ' My Settings ',
			id: 'settingTab',
			autoLoad: {
				url: 'properties_settings.php', 
				timeout: 10800, 
				scripts:true
			}
		},{
			title: ' Search ',
			id: 'searchTab',
			autoLoad: { 
				url: 'searchs_types/basic_search.php', 
				scripts:true, 
				params: {
					userweb:user_web, 
					realtorid:user_webid,
					masterweb:master_web, 
					masterid: master_webid, 
					search_type: search_type, 
					search_by_type: search_by_type, 
					state_search: search_state, 
					county_search: search_county
				}
			},
			tbar: new Ext.Toolbar({
				cls: 'no-border-search',
				width: 'auto',
				items: [' ',{
						tooltip: 'Click to Search by Map',
						iconCls:'icon',
						iconAlign: 'top',
						text: 'Map',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/map.png',
						scale: 'medium',
						handler: function (){
							if(document.getElementById("mapSearch").style.display=='none'){
								doSearchByFilter('MAP');
								document.getElementById(search_type+'_combo_search_by').value = 'MAP';
							}else{
								doSearchByFilter('MAP_OFF');
								document.getElementById(search_type+'_combo_search_by').value = 'LOCATION';
							}
						}
					},{
						tooltip: 'Click to Manage Search',
						text: 'Manage<br>Search',
						iconCls:'icon',
						iconAlign: 'top',
						width: 60,
						height: 70,
						autoWidth: false,
						icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
						scale: 'medium',
						handler: function(){
							ShowSavedSearch();
						}
					},{
						tooltip: 'Click to find your Location',
						text: 'GPS<br>Location',
						iconCls:'icon',
						iconAlign: 'top',
						width: 60,
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/location.png',
						scale: 'medium',
						handler: function(){
							if(search_by_type!='GPS'){
								var formlocation = new Ext.FormPanel({
									frame:true,
									title: 'Get Location',
									width: 400,
									waitMsgTarget : 'Saving Documents...',
									items: [{
										xtype		:'combo',
										name		:'distance',
										id			:'distance',
										hiddenName	:'idtype',
										store		:new Ext.data.SimpleStore({
											fields	:['iddisc'],
											data 	: [['0.1'],['0.2'],['0.3'],['0.4'],['0.5'],['0.6'],['0.7'],['0.8'],['0.9'],['1'],['1.1'],['1.2'],['1.3'],['1.4'],['1.5'],['1.6'],['1.7'],['1.8'],['1.9'],['2']]
										}),
										editable	: false,
										displayField:'iddisc',
										valueField	:'iddisc',
										typeAhead	:true,
										fieldLabel	:'Distance Miles',
										mode		:'local',
										triggerAction: 'all',
										emptyText	:'Select ...',
										selectOnFocus:true,
										allowBlank	:false,
										value		:'0.5',
										width		:100
									},{
										xtype: 'hidden',
										id: 'tipomap',
										value:'Basic'
									}],
									buttons: [{
										text: 'Get',
										handler: getLocationGps
									},{
										text: 'Cancel',
										handler  : function(){
											win.close();
										}
									}]
								});
								
								var win = new Ext.Window({
									layout      : 'fit',
									id: 'ventanalocation',
									width       : 400,
									height      : 170,
									modal	 	: true,
									plain       : true,
									items		: formlocation,
									closeAction : 'hide'
								});
								
								win.show();
							}else{
								doSearchByFilter('LOCATION');	
							}
						}
					},'->'
					,{
						iconCls:'icon',
						iconAlign: 'top',
						icon: 'http://www.reifax.com/img/toolbar/reset.png',
						scale: 'medium',
						text: 'Reset',
						width: 60, 
						height: 70,
						handler  : function(){
							document.getElementById('pformul').reset();
							if(mapSearch!=null) mapSearch.cleaner();				
						}
					},{
						iconCls:'icon',
						iconAlign: 'top',
						icon: 'http://www.reifax.com/img/toolbar/find.png',
						scale: 'medium',
						text: 'Home Deal <br> Finder',
						width: 60, 
						height: 70,
						handler  : function(){
							searchForm(search_type+'_search','home');
						}				
					}]
			})
		}],
		
		listeners: {
			'tabchange': function(tabpanel,tab){
				if(tab){
					//chequeo de logeo
					if(user_loged)
						valid_session();
						
				}
			}
		}
    });
	
	
	var ancho = document.body.offsetWidth;
	if(ancho < 1024) ancho = 1024;
	if(ancho > 1260) ancho = 1260;
	
	/* viewport = new Ext.Panel({
		  renderTo: 'body_central',
		  id: 'viewport',
		  layout:'border',
		  monitorResize: true,
		  hideBorders: true,
		  width: ancho,
		  height: 2900,
		  items:[
			{
				region: 'south',
				id: 'south_panel',
				layout: 'hbox',
				height: 120,
				minSize: 120,
				maxSize: 120,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_bottom',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'east',
				id: 'east_panel',
				layout: 'vbox',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_right',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'west',
				id: 'west_panel',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_left',
				listeners: {beforeexpand: function(){if(user_loged) return false;}}
			},
			{
				region: 'center',
				id: 'center_panel',
				layout: 'hbox',
				width: 'auto',
				layoutConfig:{align:'middle'},
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_center'
			}
		  ]
	});
	
	if(document.body.offsetWidth < 1200){
		 var west = Ext.getCmp('west_panel');
		 west.collapse();
	}*/
	
	viewport = {
		setHeight: function(){
		},
		getHeight: function(){
		}
	};
	
	if(user_web) {session_release();}
	if(owner_web) {session_release();}
	if(master_web) {session_release();}
	if(!master_web && !owner_web && !user_web && user_loged && systemwhat==1) ajax_submit_loged(useridlogin); else logout_menu();
});
</script>
</body>
</html>