<?php
/**
 * saveaddons.php
 *
 * Save the addons of a document.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 07.04.2011
 */

include_once "../../properties_conexion.php"; 
conectar();
$path='C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs';

// Allowed images 
$error   = "";
$userid  = $_COOKIE['datos_usr']["USERID"];
$type    = $_POST['type_a'];

// Delete addons and end execution if the command its recieve
if ($_GET['cmd'] == 'del') {//strlen($_GET['addon']) and 

	$query = "SELECT id,document FROM xima.contracts_addonscustom
				WHERE userid = $userid AND place = {$_GET['addon']} AND type = {$_GET['type_a']}";
	$rscr      = mysql_query($query) or die($query.mysql_error());
	$rowcr = mysql_fetch_array($rscr);
	$document = $rowcr['document'];
	if($document<>'') unlink($path.'/addons/'.$document);

	$query = "DELETE FROM xima.contracts_addonscustom
				WHERE userid = $userid AND place = {$_GET['addon']} AND type = {$_GET['type_a']}";
	@mysql_query($query);	
	
	echo "Addon deleted";
	return;
	
} else {

	// Check the form fields
	for ($i == 1 ; $i <=6 ; $i ++) {
		
		// ---
		$addon_act  = $_POST["addon_act{$i}"] == 'on' ? '1' : '0';
		$addon_name = $_POST["addon_name{$i}"];
		$sql        = '';
		

		if ($_FILES["addo{$i}"]['name']!='') {
			
			$filename = $_FILES["addo{$i}"]['name'];
			
			if (strlen($addon_name)==0)
				$addon_name = $filename;
			
			$extension = strtolower(strrchr($filename,'.'));
			
			if($extension != '.pdf') {
				$extension = '';
				$error     = "The uploaded file its not a valid PDF";

			}
			// No errors were found?
			if($error == "") {
				
				$newFileName = "$type-$userid-"."$i".$extension;
				
				@unlink(getcwd().'/addons/'.$newFileName);
				
				if (!move_uploaded_file($_FILES["addo{$i}"]['tmp_name'], $path.'/addons/'.$newFileName)) {
					$error .= "Fallo al copiar. $newFileName";
					$newFileName = '';
				} else 
					@unlink($_FILES["addo{$i}"]['tmp_name']);
				
			}
		}
		
        $query = "SELECT id FROM xima.contracts_addonscustom
                    WHERE userid = $userid AND place = $i AND type = $type";
        $rs    = mysql_query($query);

        if (mysql_num_rows($rs)>0) {
			
			if ($newFileName != '')
				$sql = ",document = '$newFileName'";
			
			$query = "UPDATE xima.contracts_addonscustom 
						SET 
							addon_act = '$addon_act',
							addon_name = '$addon_name' $sql
	                    WHERE userid = $userid AND place = $i AND type = $type";
					  
		} else {
			
			if (!empty($addon_name) and !empty($newFileName)) {
				
				$query = "INSERT INTO xima.contracts_addonscustom (userid, type, place, addon_act, addon_name, document) 
						  VALUES ($userid,$type,$i,'$addon_act','$addon_name','$newFileName')";
			
			}
		}
		
		if (mysql_query($query))
			$resp = array('success'=>'true','mensaje'=>'Settings saved successfully');
		else
			$resp = array('success'=>'true','mensaje'=>mysql_error()."$query");
		
	}
	
	if ($error != "") 
		$resp = array('success'=>'true','mensaje'=>$error);
	
	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);

}

?>	