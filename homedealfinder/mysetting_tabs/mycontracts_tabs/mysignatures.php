<?php
/**
 * mysignatures.php
 *
 * All the options to update the user signature for the contracts.
 * It allows to draw or upload an signuature image.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 01.04.2011
 */

include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];
$signs     = null;
$path      = 'mysetting_tabs/mycontracts_tabs/signatures/';

// Get all the images order by signature type
// the type its from 1 to 8, the first 4 elements are buyer's and 
// the second 4 elementes are for seller's
$query     = "SELECT imagen,type FROM xima.contracts_signature  
				WHERE userid = $userid ORDER BY type";
$rs        = mysql_query($query);
if ($rs) {
	while ($row = mysql_fetch_array($rs)) {
		$signs["{$row['type']}"] = $row[0];
	}
}

?>

<script type="text/javascript" src="FileUploadField.js"></script>

<div id="mySignID" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<input type="hidden" name="_lookup" id="_lookup">


<script type="text/javascript">
function forceRefresh() {

	var tab     = tabs3.getActiveTab();
	var updater = tab.getUpdater();
	updater.update({
		url   : 'mysetting_tabs/mycontracts_tabs/mysignatures.php?'+new Date().getTime(),
		cache : false
	});

}

function eliminarFirma(op) {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/savesigns.php?cmd=del&type='+op,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			forceRefresh();
		}
	});
	
}


// Special xtype to render an image to the panel

Ext.ux.Image = Ext.extend(Ext.Component, {
    url  : '<?=$path?>/noimage.png',
    autoEl: {
        tag: 'img',
        src: Ext.BLANK_IMAGE_URL,
        cls: 'tng-managed-image'
    },
    onRender: function() {
        Ext.ux.Image.superclass.onRender.apply(this, arguments);
        this.el.on('load', this.onLoad, this);
        if(this.url){
            this.setSrc(this.url+'?'+new Date().getTime());
        }
    },
    onLoad: function() {
        this.fireEvent('load', this);
    }, 
    setSrc: function(src) {
        this.el.dom.src = src;
    }
});
Ext.reg('image', Ext.ux.Image);



// Form panel for all the signature options

mySignForm = new Ext.FormPanel({
	frame      : true,
	autoWidth  : true,
	fileUpload : true,
	method     : 'POST',
	bodyStyle  : 'padding-left:100px;text-align:left;',
	id         : 'formSig',
	name       : 'formSig',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			title        : 'Buyer Information',
			border       : true,
			bodyStyle    : 'padding: 10px; border: 2px solid #cccccc',
			width        : 700,
			layoutConfig : {
				columns  : 2
			},
			items        : [
				{
					xtype       : 'label',
					text        : 'Buyer Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Buyer Second Signature ',
					cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
							<?php
							echo strlen($signs[1])>0 ?  "url: '{$path}{$signs[1]}'," : "" ;
							?>
							xtype       : 'image'
						}
						<?php
						if(strlen($signs[1])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(1);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [
						{
							<?php
							echo strlen($signs[2])>0 ?  "url: '{$path}{$signs[2]}'," : "" ;
							?>
							xtype       : 'image',
							width       : 350,
							height      : 50,
						}
						<?php
						if(strlen($signs[2])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(2);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 5
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=1','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file1',
							emptyText   : 'Upload Image',
							name        : 'image1',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=2','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file2',
							emptyText   : 'Upload Image',
							name        : 'image2',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					height      : 20,
					colspan     : 2
                },{
					xtype       : 'label',
					text        : 'Buyer Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Buyer Second Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[3])>0 ?  "url: '{$path}{$signs[3]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[3])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(3);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[4])>0 ?  "url: '{$path}{$signs[4]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[4])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(4);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=3','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file3',
							emptyText   : 'Upload Image',
							name        : 'image3',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=4','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file4',
							emptyText   : 'Upload Image',
							name        : 'image4',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				}
			]
		},{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			title        : 'Seller Information',
			bodyStyle    : 'padding: 10px; border: 2px solid #cccccc',
			width        : 700,
			layoutConfig : {
				columns  : 2
			},
			items        : [
				{
					xtype       : 'label',
					text        : 'Seller Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Seller Second Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[5])>0 ?  "url: '{$path}{$signs[5]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[5])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(5);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[6])>0 ?  "url: '{$path}{$signs[6]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[6])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(6);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=5','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file5',
							emptyText   : 'Upload Image',
							name        : 'image5',
							width       : 180,
							buttonText  : 'Browse...'
						}					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=6','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file6',
							emptyText   : 'Upload Image',
							name        : 'image6',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					height      : 20,
					colspan     : 2
                },{
					xtype       : 'label',
					text        : 'Seller Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Seller Second Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[7])>0 ?  "url: '{$path}{$signs[7]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[7])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(7);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[8])>0 ?  "url: '{$path}{$signs[8]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[8])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(8);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=7','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file7',
							emptyText   : 'Upload Image',
							name        : 'image7',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=8','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file8',
							emptyText   : 'Upload Image',
							name        : 'image8',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				}
			]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Signatures</b></span>',
		handler : function(){
			if (mySignForm.getForm().isValid()) {
				mySignForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savesigns.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						forceRefresh();
					}
				});
			}
		}
	}]
});
mySignForm.render('mySignID');
</script>
