<?php 
header("Cache-Control: no-store, no-cache, must-revalidate");
include("../../properties_conexion.php");
require_once '../../../backoffice/php/mcripty.php';
conectar();

$query='SELECT * FROM xima.creditcard WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result = mysql_query($query) or die($query.mysql_error());
$datos_usr=mysql_fetch_array($result);
?>
<form method="post" id="mformul"  >
	<div align="center" style=" background-color:#EAEAEA; height:auto;margin-top:-5px;padding-top:10px;">

		<table border="0" cellpadding="0" cellspacing="0" style="font-size:12px;" >
        	<tr>
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
           	</tr>
        	<tr>
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:16px; text-align:center">Change Credit Card Data</td>                
           	</tr>
         	<tr>
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
           	</tr>
         	<tr>
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
           	</tr>

                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Card Type: </td>                
                            <td width="350px"><select name="cardtype" id="cardtype" onfocus="limpiarMsgError(4)" >
                                                <option value="none">- Select -</option>
                                                <option value="MasterCard" <?php if($datos_usr['cardname']=="MasterCard") echo ' selected="selected" '; ?>>Master Card</option>
                                                <option value="Visa" <?php if($datos_usr['cardname']=="Visa") echo ' selected="selected" '; ?>>Visa</option>
                                                <option value="AmericanExpress" <?php if($datos_usr['cardname']=="AmericanExpress") echo ' selected="selected" '; ?>>American Express</option>
                                                <option value="Discover" <?php if($datos_usr['cardname']=="Discover") echo ' selected="selected" '; ?>>Discover</option>
                                            </select> 	</td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Holder Name: </td>                
                            <td width="350px"><input name="holder" type="text" maxlength="45" size="50" id="holder"  value="<?php echo $datos_usr['cardholdername'] ?>" onfocus="limpiarMsgError(4)"/></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Card Number: </td>                
                            <td width="350px"><input name="cardnumber" type="text" maxlength="43" size="50" id="cardnumber" 
                            	value="<?php echo substr(mcrypt_Fe7a0a89bd($datos_usr['cardnumber'] ),0,4).'********'.substr(mcrypt_Fe7a0a89bd($datos_usr['cardnumber'] ),12,4) ?>" onfocus="limpiarMsgError(4)"/></td>
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Expiration Date: </td>                
                            <td width="350px">
                            	<input name="exdate1" type="text" maxlength="2" size="7" id="exdate1" value="<?php echo $datos_usr['expirationmonth'] ?>" onfocus="limpiarMsgError(4)"/>
                            	&nbsp;<label>MM</label><label id="inner3"></label>&nbsp;&nbsp;
                                <input name="exdate2" type="text" maxlength="4" size="7" id="exdate2" value="<?php echo $datos_usr['expirationyear'] ?>" onfocus="limpiarMsgError(4)"/>
                                &nbsp;<label>YYYY</label></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">CSV Number: </td>                
                            <td width="350px"><input name="csv" type="text" maxlength="18" size="7" id="csv"  value="<?php echo mcrypt_Fe7a0a89bd($datos_usr['csvnumber'] ) ?>" onfocus="limpiarMsgError(4)"/></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Address: </td>                
                            <td width="350px"><input name="sameAdd" type="text"  maxlength="48" size="50" id="sameAdd"  value="<?php echo $datos_usr['billingaddress'] ?>" onfocus="limpiarMsgError(4)"/></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">City: </td>                
                            <td width="350px"><input name="sameCity" type="text" id="sameCity" maxlength="28" size="50"  value="<?php echo $datos_usr['billingcity'] ?>" onfocus="limpiarMsgError(4)"/></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">State: </td>                
                            <td width="350px"><select name="sameState"  id="sameState" onfocus="limpiarMsgError(4)">
                                                <option value="none">-Select A State-</option><option <?php if($datos_usr['billingstate']=="Alabama") echo ' selected="selected" '; ?> value="Alabama">Alabama</option><option <?php if($datos_usr['billingstate']=="Alaska") echo ' selected="selected" '; ?> value="Alaska">Alaska</option><option <?php if($datos_usr['billingstate']=="American Samoa") echo ' selected="selected" '; ?> value="American Samoa">American Samoa</option><option <?php if($datos_usr['billingstate']=="Arkansas") echo ' selected="selected" '; ?> value="Arkansas">Arkansas</option><option <?php if($datos_usr['billingstate']=="California") echo ' selected="selected" '; ?> value="California">California</option><option <?php if($datos_usr['billingstate']=="Connecticut") echo ' selected="selected" '; ?> value="Connecticut">Connecticut</option><option <?php if($datos_usr['billingstate']=="District of Columbia") echo ' selected="selected" '; ?> value="District of Columbia">District of Columbia</option><option <?php if($datos_usr['billingstate']=="Florida") echo ' selected="selected" '; ?> value="Florida">Florida</option><option <?php if($datos_usr['billingstate']=="Georgia") echo ' selected="selected" '; ?> value="Georgia">Georgia</option><option <?php if($datos_usr['billingstate']=="Hawaii") echo ' selected="selected" '; ?> value="Hawaii">Hawaii</option><option <?php if($datos_usr['billingstate']=="Illinois") echo ' selected="selected" '; ?> value="Illinois">Illinois</option><option <?php if($datos_usr['billingstate']=="Iowa") echo ' selected="selected" '; ?> value="Iowa">Iowa</option><option <?php if($datos_usr['billingstate']=="Kentucky") echo ' selected="selected" '; ?> value="Kentucky">Kentucky</option><option <?php if($datos_usr['billingstate']=="Maine") echo ' selected="selected" '; ?> value="Maine">Maine</option><option <?php if($datos_usr['billingstate']=="Marshall Islands") echo ' selected="selected" '; ?> value="Marshall Islands">Marshall Islands</option><option <?php if($datos_usr['billingstate']=="Massachusetts") echo ' selected="selected" '; ?> value="Massachusetts">Massachusetts</option><option <?php if($datos_usr['billingstate']=="Minnesota") echo ' selected="selected" '; ?> value="Minnesota">Minnesota</option><option <?php if($datos_usr['billingstate']=="Missouri") echo ' selected="selected" '; ?> value="Missouri">Missouri</option><option <?php if($datos_usr['billingstate']=="Nebraska") echo ' selected="selected" '; ?> value="Nebraska">Nebraska</option><option <?php if($datos_usr['billingstate']=="Nevada") echo ' selected="selected" '; ?> value="Nevada">Nevada</option><option <?php if($datos_usr['billingstate']=="New Brunswick") echo ' selected="selected" '; ?> value="New Brunswick">New Brunswick</option><option <?php if($datos_usr['billingstate']=="New Jersey") echo ' selected="selected" '; ?> value="New Jersey">New Jersey</option><option <?php if($datos_usr['billingstate']=="New York") echo ' selected="selected" '; ?> value="New York">New York</option><option <?php if($datos_usr['billingstate']=="North Carolina") echo ' selected="selected" '; ?> value="North Carolina">North Carolina</option><option <?php if($datos_usr['billingstate']=="Northwest Territory") echo ' selected="selected" '; ?> value="Northwest Territory">Northwest Territory</option><option <?php if($datos_usr['billingstate']=="Ohio") echo ' selected="selected" '; ?> value="Ohio">Ohio</option><option <?php if($datos_usr['billingstate']=="Ontario") echo ' selected="selected" '; ?> value="Ontario">Ontario</option><option <?php if($datos_usr['billingstate']=="Palau") echo ' selected="selected" '; ?> value="Palau">Palau</option><option <?php if($datos_usr['billingstate']=="Prince Edward Island") echo ' selected="selected" '; ?> value="Prince Edward Island">Prince Edward Island</option><option <?php if($datos_usr['billingstate']=="Quebec") echo ' selected="selected" '; ?> value="Quebec">Quebec</option><option <?php if($datos_usr['billingstate']=="Saskatchewan") echo ' selected="selected" '; ?> value="Saskatchewan">Saskatchewan</option><option <?php if($datos_usr['billingstate']=="South Dakota") echo ' selected="selected" '; ?> value="South Dakota">South Dakota</option><option <?php if($datos_usr['billingstate']=="Texas") echo ' selected="selected" '; ?> value="Texas">Texas</option><option <?php if($datos_usr['billingstate']=="Vermont") echo ' selected="selected" '; ?> value="Vermont">Vermont</option><option <?php if($datos_usr['billingstate']=="Virginia") echo ' selected="selected" '; ?> value="Virginia">Virginia</option><option <?php if($datos_usr['billingstate']=="West Virginia") echo ' selected="selected" '; ?> value="West Virginia">West Virginia</option><option <?php if($datos_usr['billingstate']=="Wyoming") echo ' selected="selected" '; ?> value="Wyoming">Wyoming</option></select></td>                
                        </tr>
                        <tr>
                            <td width="200px" style="color:#274F7B; font-weight:bold;">Zip Code: </td>                
                            <td width="350px"><input name="sameZip" type="text" id="sameZip" maxlength="6" size="5"  value="<?php echo $datos_usr['billingzip'] ?>" onfocus="limpiarMsgError(4)"/></td>
                        </tr>
                        <tr>
                            <td colspan="2" id="msgerror3" style="color: #FF0000; font-weight:bold; font-size:12px; text-align:center; ">&nbsp;</td>                
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="button" class="boton" id="submit"  onclick="validaChangeCreditCard()" value="Accept" ></td>                
                        </tr>
                        <tr>
                            <td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
                        </tr>
		</table>
   	</div>
</form>