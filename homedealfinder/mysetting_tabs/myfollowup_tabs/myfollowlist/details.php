<?php
	include("../../../properties_conexion.php");
	conectar();
	include ("../../../properties_getgridcamptit.php"); 	 
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowlistDetails_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowlistDetails_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowlistDetails_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
            <div id="myfollowlistDetails_filters"></div><br />
            <div id="myfollowlistDetails_properties" align="left"></div> 
            <div align="left" style="color:#F00; font-size:14px;">
                Right Click -> Overview / Contract 
                <span style="margin-left:15px; margin-right:15px;">
                      -     
                </span>
                Double Click -> Follow History
            </div>
        </div>
	</div>
</div>
<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/Printer-all.js"></script>
<script>
	var limitmyfollowlistDetails = 50;
	
	//filter variables
	var filteraddressmyfollowlistDetails 	= '';
	var filtermlnumbermyfollowlistDetails 	= '';
	var filteragentmyfollowlistDetails 		= '';
	var filterstatusmyfollowlistDetails 	= 'ALL';
	var filterndatemyfollowlistDetails 		= '';
	var filterndatebmyfollowlistDetails 	= '';
	var filterntaskmyfollowlistDetails 		= '-2';
	var filtercontractmyfollowlistDetails 	= '-1';
	var filterpofmyfollowlistDetails 		= '-1';
	var filteremdmyfollowlistDetails 		= '-1';
	var filterademdumsmyfollowlistDetails 	= '-1';
	var filtermsjmyfollowlistDetails 		= '-1';
	var filterhistorymyfollowlistDetails 	= '-1';
	var filterfield							= 'address';
	var filterdirection						= 'ASC';
	
	var formmyfollowlistDetails = new Ext.FormPanel({
		url:'mysetting_tabs/myfollowup_tabs/properties_followlist.php',
		frame:true,
		bodyStyle:'padding:5px 5px 0;text-align:left;',
		collapsible: true,
		collapsed: false,
		title: 'Filters',
		renderTo: 'myfollowlistDetails_filters',
		id: 'formmyfollowlistDetails',
		name: 'formmyfollowlistDetails',
		items:[{
			xtype: 'fieldset',
			title: 'Filters',
			layout: 'table',
			layoutConfig: {columns:3},
			defaults: {width: 320},
			
			items: [{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Address',
					name		  : 'faddress',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filteraddressmyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Mlnumber',
					name		  : 'fmlnumber',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filtermlnumbermyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Agent',
					name		  : 'fagent',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filteragentmyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fndate',
				items	: [{
					xtype		  : 'datefield',
					fieldLabel	  : 'Task Date',
					width		  : 130,
					editable	  : false,
					format		  : 'Y-m-d',
					name		  : 'fndate',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filterndatemyfollowup=newvalue;
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fndateb',
				items	: [{
					xtype		  : 'datefield',
					fieldLabel	  : 'Between',
					width		  : 130,
					editable	  : false,
					format		  : 'Y-m-d',
					name		  : 'fndateb',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filterndatebmyfollowup=newvalue;
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fntask',
				items	: [{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Task',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-2','-Select-'],
							['-1','All Task'],
							['0','No Task'],
							['1','Send SMS'],
							['2','Receive SMS'],
							['3','Send FAX'],
							['4','Receive FAX'],
							['5','Send EMAIL'],
							['6','Receive EMAIL'],
							['7','Send DOC'],
							['8','Receive DOC'],
							['9','Make CALL'],
							['10','Receive CALL'],
							['11','Send R. MAIL'],
							['12','Receive R. MAIL'],
							['13','Send OTHER'],
							['14','Receive OTHER']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fntaskname',
					value         : '-2',
					hiddenName    : 'fntask',
					hiddenValue   : '-2',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterntaskmyfollowlistDetails = record.get('valor');
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Status', 
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['ALL','All Status'],
							['A','Active'],
							['NA','Non-Active'],
							['NF','Not for Sale'],
							['S','Sold']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fstatusname',
					value         : 'ALL',
					hiddenName    : 'fstatus',
					hiddenValue   : 'ALL',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterstatusmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Contract',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fcontractname',
					value         : '-1',
					hiddenName    : 'fcontract',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filtercontractmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Proof of Funds',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fpofname',
					value         : '-1',
					hiddenName    : 'fpof',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterpofmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'EMD',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'femdname',
					value         : '-1',
					hiddenName    : 'femd',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filteremdmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Addendums',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fademdumsname',
					value         : '-1',
					hiddenName    : 'fademdums',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterademdumsmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Message',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['1','Yes'],
							['0','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fmsjname',
					value         : '-1',
					hiddenName    : 'fmsj',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filtermsjmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'History',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['1','Yes'],
							['0','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fhistoryname',
					value         : '-1',
					hiddenName    : 'fhistory',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterhistorymyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			}]
		}],
		buttons:[
			{
				iconCls		  : 'icon',
				icon		  : 'http://www.reifax.com/img/toolbar/search.png',
				scale		  : 'medium',
				width		  : 120,
				text		  : 'Search&nbsp;&nbsp; ',
				handler  	  : function(){
					storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
				}				
			},{
				iconCls		  : 'icon',
				icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
				scale		  : 'medium',
				width		  : 120,
				text		  : 'Reset&nbsp;&nbsp; ',  
				handler  	  : function(){
					filteraddressmyfollowlistDetails = '';
					filtermlnumbermyfollowlistDetails = '';
					filteragentmyfollowlistDetails = '';
					filterstatusmyfollowlistDetails = 'ALL';
					filterndatemyfollowlistDetails = '';
					filterndatebmyfollowlistDetails = '';
					filterntaskmyfollowlistDetails = '-2';
					filtercontractmyfollowlistDetails = '-1';
					filterpofmyfollowlistDetails = '-1';
					filteremdmyfollowlistDetails = '-1';
					filterademdumsmyfollowlistDetails = '-1';
					filtermsjmyfollowlistDetails = '-1';
					filterhistorymyfollowlistDetails = '-1';	
					
					Ext.getCmp('formmyfollowlistDetails').getForm().reset();
					
					storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
				}
			}
		]
	});
	
	formmyfollowlistDetails.collapse();
	
	var storemyfollowlistDetails = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php',
		autoLoad: true,
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'history', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $_POST['userid'];?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowlistDetails;
				obj.params.mlnumber=filtermlnumbermyfollowlistDetails;
				obj.params.agent=filteragentmyfollowlistDetails;
				obj.params.status=filterstatusmyfollowlistDetails;
				obj.params.ndate=filterndatemyfollowlistDetails;
				obj.params.ndateb=filterndatebmyfollowlistDetails;
				obj.params.ntask=filterntaskmyfollowlistDetails;
				obj.params.contract=filtercontractmyfollowlistDetails;
				obj.params.pof=filterpofmyfollowlistDetails;
				obj.params.emd=filteremdmyfollowlistDetails;
				obj.params.ademdums=filterademdumsmyfollowlistDetails;
				obj.params.msj=filtermsjmyfollowlistDetails;
				obj.params.history=filterhistorymyfollowlistDetails;
			}
		}
    });
	
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/drop-no.gif" />';
		else return '<img src="../../../img/drop-yes.gif" />';
	}
	
	function historyRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/notes/new_history.png" />';
		else return '';
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/notes/new_msj.png" />';
		else return '';
	}
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive a Call." src="../../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../../img/notes/reci_other.png" />'; break;
		}
	}
	
		
	var gridmyfollowlistDetails = new Ext.grid.GridPanel({
		renderTo: 'myfollowlistDetails_properties',
		cls: 'grid_comparables',
		width: 920,
		height: 400,
		store: storemyfollowlistDetails,
		stripeRows: true,
		columns: [  
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'M', width: 25, sortable: true, tooltip: 'New Messages Chat.', dataIndex: 'msj', renderer: msjRender},{header: 'H', width: 25, sortable: true, tooltip: 'New or Updated History.', dataIndex: 'history', renderer: historyRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					else
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent'}
				,{header: 'Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Offer.', dataIndex: 'offer'}
				,{header: 'C. Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer'}
				,{header: 'Next Date', width: 60, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate'}
				,{header: 'T', width: 25, sortable: true, tooltip: 'Next Task.', dataIndex: 'ntask', renderer: taskRender}
				,{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate'}
				,{header: 'C', width: 25, sortable: true, tooltip: 'Contract Sent.', dataIndex: 'contract', renderer: checkRender}
				,{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}
				,{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}
				,{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}";
		   ?>
		], 
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowlistDetails',
            pageSize: limitmyfollowlistDetails,
            store: storemyfollowlistDetails,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowlistDetails=50;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowlistDetails=80;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowlistDetails=100;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),new Ext.Button({
				tooltip: 'Click to print details.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 3,
							userid: <?php echo $_POST['userid'];?>,
							address: filteraddressmyfollowlistDetails,
							mlnumber: filtermlnumbermyfollowlistDetails,
							agent: filteragentmyfollowlistDetails,
							status: filterstatusmyfollowlistDetails,
							ndate: filterndatemyfollowlistDetails,
							ndateb: filterndatebmyfollowlistDetails,
							ntask: filterntaskmyfollowlistDetails,
							contract: filtercontractmyfollowlistDetails,
							pof: filterpofmyfollowlistDetails,
							emd: filteremdmyfollowlistDetails,
							ademdums: filterademdumsmyfollowlistDetails,
							msj: filtermsjmyfollowlistDetails,
							history: filterhistorymyfollowlistDetails,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to export Excel.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 3,
							userid: <?php echo $_POST['userid'];?>,
							address: filteraddressmyfollowlistDetails,
							mlnumber: filtermlnumbermyfollowlistDetails,
							agent: filteragentmyfollowlistDetails,
							status: filterstatusmyfollowlistDetails,
							ndate: filterndatemyfollowlistDetails,
							ndateb: filterndatebmyfollowlistDetails,
							ntask: filterntaskmyfollowlistDetails,
							contract: filtercontractmyfollowlistDetails,
							pof: filterpofmyfollowlistDetails,
							emd: filteremdmyfollowlistDetails,
							ademdums: filterademdumsmyfollowlistDetails,
							msj: filtermsjmyfollowlistDetails,
							history: filterhistorymyfollowlistDetails,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			})]
        }),
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_POST['userid'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'Contract its not generated or not in system.');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				
				if(document.getElementById('reportsTab')){
					var tab = tabs.getItem('reportsTab');
					tabs.remove(tab);
				}
				
				tabs.add({
					title: ' Follow ',
					id: 'reportsTab', 
					closable: true,
					items: [
						new Ext.TabPanel({
							id: 'historyTab',
							activeTab: 0,
							width: ancho,
							height: tabs.getHeight(),
							plain:true,
							listeners: {
								'tabchange': function( tabpanel, tab ){
									if(tab.id=='followhistoryInnerTab')
										if(document.getElementById('follow_history_grid')) followstore.load();
									else if(tab.id=='followsheduleInnerTab')
										if(document.getElementById('follow_schedule_grid')) followstore_schedule.load();
								}
							},
							items:[	
								{
									title: 'History',
									id: 'followhistoryInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false}
								},{
									title: 'Schedule',
									id: 'followsheduleInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false}
								},{
									title: 'Messages',
									id: 'followmessagesInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followmessages.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false},
									tbar: new Ext.Toolbar({
										cls: 'no-border',
										width: 'auto',
										items: [' ',{
											 tooltip: 'Click to post message.',
											 iconCls:'icon',
											 iconAlign: 'top',
											 width: 40,
											 icon: 'http://www.reifax.com/img/add.gif',
											 scale: 'medium',
											 handler: function(){
												var simple = new Ext.FormPanel({
													url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followmessages.php',
													frame: true,
													title: 'Follow Messages',
													width: 450,
													waitMsgTarget : 'Waiting...',
													labelWidth: 75,
													defaults: {width: 330},
													labelAlign: 'left',
													items: [{
																xtype	  :	'textarea',
																name	  : 'sms',
																fieldLabel: 'Message',
																value	  : '',
																blankText : 'post a Message...',
																height	  : 200
															},{
																xtype     : 'hidden',
																name      : 'type',
																value     : 'insert'
															},{
																xtype     : 'hidden',
																name      : 'userid',
																value     : record.get('userid')
															},{
																xtype     : 'hidden',
																name      : 'pid',
																value     : record.get('pid') 
															}],
													
													buttons: [{
															text: 'Post',
															handler: function(){
																loading_win.show();
																simple.getForm().submit({
																	success: function(form, action) {
																		loading_win.hide();
																		win.close();
																		Ext.Msg.alert("Follow Messages", 'Posted messages sucesfully.');
																		followstore_messages.load();
																	},
																	failure: function(form, action) {
																		loading_win.hide();
																		Ext.Msg.alert("Failure", "ERROR");
																	}
																});
															}
														},{
															text: 'Reset',
															handler  : function(){
																simple.getForm().reset();
																win.close(); 
															} 
														}]
													});
												 
												var win = new Ext.Window({
													layout      : 'fit',
													width       : 440,
													height      : 350,
													modal	 	: true,  
													plain       : true,
													items		: simple,
													closeAction : 'close',
													buttons: [{
														text     : 'Close',
														handler  : function(){
															win.close();
														}
													}]
												});
												win.show();
											}
										}]
									})
								}
							]
						})
					],
					tbar: new Ext.Toolbar({
						cls: 'no-border',
						width: 'auto',
						items: [' ',
							'->',{
								 tooltip: 'Click to Close Reports',
								 iconCls:'icon',
								 iconAlign: 'top',
								 width: 40,
								 icon: 'http://www.reifax.com/img/cancel.png',
								 scale: 'medium',
								  
								 handler: function(){
									 var tab = tabs.getItem('reportsTab');
									 tabs.remove(tab);
								 }
							}
						]
					})
				}).show();
			}
		}
	});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowlistDetails_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowlistDetails_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>