<?php  
	define('FPDF_FONTPATH','FPDF/font/');
	set_include_path(get_include_path() . PATH_SEPARATOR . '../Excel/Classes/');
	
	$bd=$_POST['db'];
	$comparado=$_POST['pid'];
	$comparables=$_POST['array_taken'];
	$type_comp=$_POST['type_comp'];
	$_calculo="truncate(sqrt((69.1* (latlong.latitude- #lat))*(69.1*(latlong.latitude-#lat))+(69.1*((latlong.longitude-(#lon))*cos(#lat/57.29577951)))*(69.1*((latlong.longitude-(#lon))*cos(#lat/57.29577951)))),2)";
	
	if($type_comp=='distress' && $comparables==''){
		ob_start();
			$_POST['id']=$comparado;
			$_POST['bd']=$bd;
			$_POST['userid']=$_COOKIE['datos_usr']['USERID'];
			
			include('../properties_1mile_distress.php');
			$comparables='"'.implode('","',$array_parcelIds).'"';
		ob_end_clean();
	}else{
		include('../properties_conexion.php');
		include '../properties_getgridcamptit.php';
	}
	
	include '../Excel/Classes/PHPExcel.php';
	include '../Excel/Classes/PHPExcel/Writer/Excel5.php';

	$excel = new PHPExcel();

	$excel->getProperties()->setCreator("REI Property Fax");
	$excel->getProperties()->setTitle("REI Property Fax Reports");
	$excel->getProperties()->setSubject("REI Property Fax Reports");
	$excel->setActiveSheetIndex(0);
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
	'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
	'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
	'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
	'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ');
	
	conectar();
	$id = getArray($type_comp,'comparables',true);
	$ArTab=getCamptitTipo($id, "Tabla",'defa');	
	$myArrF=getCamptitTipo($id, "Campos",'defa');
	$myArrTit=getCamptitTipo($id, "Titulos",'defa');
	$myArrNum = count($myArrTit);
	
	$select='psummary.parcelid as pid';
	$joint = array('psummary');
	for($i=0; $i< count($myArrF); $i++){	//Formacion de Select
		$select.=",";
		
		if(array_search($ArTab[$i],$joint)===false)
			$joint[]=$ArTab[$i];
		
		$select.=$ArTab[$i].".".$myArrF[$i]."\r";
	}
	
	foreach($myArrTit as $i=>$val){
		$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
		$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
	}
	
	//echo $select;
	$xSql='SELECT '.$select.' FROM ';
	
	foreach($joint as $k => $val){
		if($k==0){
			$xSql.="$val ";
		}else
			$xSql.="LEFT JOIN $val ON (".$joint[0].".parcelid=$val.parcelid) ";
	}
	
	$xSql.='WHERE '.$joint[0].'.parcelid=\''.$comparado.'\'';
	
	//Formacion de Tabla
	conectarPorBD($bd);
	$sql_tabla=$xSql;
	$fila_tabla=3;
	$fila_tabla_inicio=3;
	require("../Excel/tabla.php");
		
	$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			array(
				'font'    => array(
					'bold'      => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'top'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'bottom'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'right'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'left'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					)
				),
				'fill' => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						'argb' => 'FF89b039'
					)
				)
			)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($myArrNum)].'2' );
	
	$id = getArray($_POST['type_comp'],'comparables',false);
	$ArTab=getCamptitTipo($id, "Tabla",'defa');	
	$myArrF=getCamptitTipo($id, "Campos",'defa');
	$myArrTit=getCamptitTipo($id, "Titulos",'defa');
	$myArrNum = count($myArrTit);
	
	$select='psummary.parcelid as pid, rtmaster.status as rstatus';
	$que='SELECT latitude,longitude FROM latlong WHERE parcelid="'.$comparado.'"';
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$lat=$r['latitude'];
	$lon=$r['longitude'];
	
	$_calculo=str_replace('#lat',$lat,$_calculo);
	$_calculo=str_replace('#lon',$lon,$_calculo);
	
	$select.=", $_calculo as Distance";
	$joint = array('psummary','rtmaster','latlong');
	
	$orderby='';
	for($i=0; $i< count($myArrF); $i++){	//Formacion de Select
		$select.=",";
		
		if(array_search($ArTab[$i],$joint)===false)
			$joint[]=$ArTab[$i];
		
		$select.=$ArTab[$i].".".$myArrF[$i]."\r";
		
		if(isset($_POST['orderField']) && ($myArrF[$i]==$_POST['orderField'])){
			$orderby=' ORDER BY `'.$ArTab[$i].'`.'.$myArrF[$i].' '.$_POST['orderDir']; 
		}
	}
	if(!isset($_POST['orderField'])) $orderby=' ORDER BY Distance ASC';
	if(isset($_POST['orderField']) && ($_POST['orderField']=='Distance' || $_POST['orderField']=='status')) $orderby=' ORDER BY Distance '.$_POST['orderDir'];

	$myArrF=array_merge(array('Distance'),$myArrF);
	$myArrTit=array_merge(array('Dist.'),$myArrTit); 
	$myArrNum = count($myArrTit);
	
	foreach($myArrTit as $i=>$val){
		$excel->getActiveSheet()->setCellValue($abc[$i+1].'5', $val);
		$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
	}
	
	//echo $select;
	$xSql='SELECT '.$select.' FROM ';
	
	foreach($joint as $k => $val){
		if($k==0){
			$xSql.="$val ";
		}else
			$xSql.="LEFT JOIN $val ON (".$joint[0].".parcelid=$val.parcelid) ";
	}
	
	$xSql.='WHERE '.$joint[0].'.parcelid IN ('.$comparables.')';
	
	if($type_comp=='active')
		$xSql.=' AND rtmaster.status=\'A\' ';
	elseif($type_comp=='comp')
		$xSql.=' AND (rtmaster.status=\'CC\' OR rtmaster.status=\'CS\') ';
	
	$xSql.=$orderby;
	
	//Formacion de Tabla
	$sql_tabla=$xSql;
	$fila_tabla=6;
	$fila_tabla_inicio=6;
	require("../Excel/tabla.php");
		
	$excel->getActiveSheet()->getStyle('B5')->applyFromArray(
			array(
				'font'    => array(
					'bold'      => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'top'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'bottom'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'right'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'left'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					)
				),
				'fill' => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						'argb' => 'FF87cefa'
					)
				)
			)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B5'), 'B5:'.$abc[($myArrNum)].'5' );
	
	$excel->setActiveSheetIndex(0);
	$objWriter = new PHPExcel_Writer_Excel5($excel);
	$nombre = 'Excel/archivos/comp_'.strtotime("now").'.xls';
	$objWriter->save('C:/inetpub/wwwroot/'.$nombre);
	echo "{success:true, excel:'$nombre'}";
?>