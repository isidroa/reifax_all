<?php
	session_start();
	include('conexion.php');
	mysql_select_db('xima') or die(mysql_error());
	
	$usuarioID = $_SESSION['datos_usr']['USERID'];
	$ssid = $_GET['ssid'];
	$tipo = $_GET['tipo'];
	
	$query = "SELECT * FROM shortsale where shortsaleid=$ssid";
	$resp = mysql_query($query) or die(mysql_error());
	$r = mysql_fetch_array($resp);
	
	$id = $r['shortsaleid'];
	$codpro = $r['codpro'];
	$codbank = $r['codbank'];
	$userid = $r['usuarioid'];
	$parcelid = $r['parcelid'];
	
	//eliminar las imagenes que quedan
	$directorio = opendir('img/documents/'); 
	while ($archivo = readdir($directorio)){ 
		if( $archivo !='.' && $archivo !='..'){ 
			//si no es un directorio, lo borramos 
			$time = explode('.',$archivo);
			if($time[0]<=(gettimeofday(true)-86400))
			unlink('img/documents/'.$archivo); 
		} 
	} 
	closedir($directorio); 
?>
<html><!-- InstanceBegin template="/Templates/templatepopup.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" type="image/ico" href="http://www.ximausa.com/favicon.ico"> 
<link href="includes/css/layout.css" rel="stylesheet" type="text/css" />
<link href="includes/css/searchstyles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/js/mostrardiv.js"></script>
<!-- InstanceBeginEditable name="doctitle" -->
<title>XIMA-USA  -  Realtor Services </title>
<script type="text/javascript" src="includes/document.js"></script>
<style type="text/css">
	.selectBoxArrow{
		margin-top:1px;
		float:left;
		position:absolute;
		right:1px;
		

	}	
	.selectBoxInput{
		border:0px;
		padding-left:1px;
		height:20px;
		position:absolute;
		top:0px;
		left:0px;
	}

	.selectBox{
		border:1px solid #7f9db9;
		height:20px;	
	
	}
	.selectBoxOptionContainer{
		position:absolute;
		border:1px solid #7f9db9;
		height:100px;
		background-color:#FFF;
		left:-1px;
		top:20px;
		visibility:hidden;
		overflow:auto;
	}
	.selectBoxAnOption{
		font-family:arial;
		font-size:12px;
		cursor:default;
		margin:1px;
		overflow:hidden;
		white-space:nowrap;
	}
	.selectBoxIframe{
		position:absolute;
		background-color:#FFF;
		border:0px;
		z-index:999;
	}	
	</style>
	<script type="text/javascript">
	/************************************************************************************************************
	Editable select
	Copyright (C) September 2005  DTHMLGoodies.com, Alf Magne Kalleland
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
	
	Dhtmlgoodies.com., hereby disclaims all copyright interest in this script
	written by Alf Magne Kalleland.
	
	Alf Magne Kalleland, 2006
	Owner of DHTMLgoodies.com
		
	************************************************************************************************************/	

	
	// Path to arrow images
	var arrowImage = 'img/select_arrow.gif';	// Regular arrow
	var arrowImageOver = 'img/select_arrow_over.gif';	// Mouse over
	var arrowImageDown = 'img/select_arrow_down.gif';	// Mouse down

	
	var selectBoxIds = 0;
	var currentlyOpenedOptionBox = false;
	var editableSelect_activeArrow = false;
	

	
	function selectBox_switchImageUrl()
	{
		if(this.src.indexOf(arrowImage)>=0){
			this.src = this.src.replace(arrowImage,arrowImageOver);	
		}else{
			this.src = this.src.replace(arrowImageOver,arrowImage);
		}
		
		
	}
	
	function selectBox_showOptions()
	{
		if(editableSelect_activeArrow && editableSelect_activeArrow!=this){
			editableSelect_activeArrow.src = arrowImage;
			
		}
		editableSelect_activeArrow = this;
		
		var numId = this.id.replace(/[^\d]/g,'');
		var optionDiv = document.getElementById('selectBoxOptions' + numId);
		if(optionDiv.style.display=='block'){
			optionDiv.style.display='none';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='none';
			this.src = arrowImageOver;	
		}else{			
			optionDiv.style.display='block';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='block';
			this.src = arrowImageDown;	
			if(currentlyOpenedOptionBox && currentlyOpenedOptionBox!=optionDiv)currentlyOpenedOptionBox.style.display='none';	
			currentlyOpenedOptionBox= optionDiv;			
		}
	}
	
	function selectOptionValue()
	{
		var parentNode = this.parentNode.parentNode;
		var textInput = parentNode.getElementsByTagName('INPUT')[0];
		textInput.value = this.innerHTML;	
		this.parentNode.style.display='none';	
		document.getElementById('arrowSelectBox' + parentNode.id.replace(/[^\d]/g,'')).src = arrowImageOver;
		
		if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + parentNode.id.replace(/[^\d]/g,'')).style.display='none';
		
	}
	var activeOption;
	function highlightSelectBoxOption()
	{
		if(this.style.backgroundColor=='#316AC5'){
			this.style.backgroundColor='';
			this.style.color='';
		}else{
			this.style.backgroundColor='#316AC5';
			this.style.color='#FFF';			
		}	
		
		if(activeOption){
			activeOption.style.backgroundColor='';
			activeOption.style.color='';			
		}
		activeOption = this;
		
	}
	
	function createEditableSelect(dest)
	{

		dest.className='selectBoxInput';		
		var div = document.createElement('DIV');
		div.style.styleFloat = 'left';
		div.style.width = dest.offsetWidth + 16 + 'px';
		div.style.position = 'relative';
		div.id = 'selectBox' + selectBoxIds;
		var parent = dest.parentNode;
		parent.insertBefore(div,dest);
		div.appendChild(dest);	
		div.className='selectBox';
		div.style.zIndex = 10000 - selectBoxIds;
		
		if(selectBoxIds>0){
			document.getElementById(('arrowSelectBox' + (selectBoxIds-1))).style.display='none';
			document.getElementById(('selectBoxOptions' + (selectBoxIds-1))).style.display='none';
		}
		
			var img = document.createElement('IMG');
			img.src = arrowImage;
			img.className = 'selectBoxArrow';
			
			img.onmouseover = selectBox_switchImageUrl;
			img.onmouseout = selectBox_switchImageUrl;
			img.onclick = selectBox_showOptions;
			img.id = 'arrowSelectBox' + selectBoxIds;
	
			div.appendChild(img);

		var optionDiv = document.createElement('DIV');
		optionDiv.id = 'selectBoxOptions' + selectBoxIds;
		optionDiv.className='selectBoxOptionContainer';
		optionDiv.style.width = div.offsetWidth-2 + 'px';
		div.appendChild(optionDiv);
		
		if(navigator.userAgent.indexOf('MSIE')>=0){
			var iframe = document.createElement('<IFRAME src="about:blank" frameborder=0>');
			iframe.style.width = optionDiv.style.width;
			iframe.style.height = optionDiv.offsetHeight + 'px';
			iframe.style.display='none';
			iframe.id = 'selectBoxIframe' + selectBoxIds;
			div.appendChild(iframe);
		}
		
		if(dest.getAttribute('selectBoxOptions')){
			var options = dest.getAttribute('selectBoxOptions').split(';');
			var optionsTotalHeight = 0;
			var optionArray = new Array();
			for(var no=0;no<options.length;no++){
				var anOption = document.createElement('DIV');
				anOption.innerHTML = options[no];
				anOption.className='selectBoxAnOption';
				anOption.onclick = selectOptionValue;
				anOption.style.width = optionDiv.style.width.replace('px','') - 2 + 'px'; 
				anOption.onmouseover = highlightSelectBoxOption;
				optionDiv.appendChild(anOption);	
				optionsTotalHeight = optionsTotalHeight + anOption.offsetHeight;
				optionArray.push(anOption);
			}
			if(optionsTotalHeight > optionDiv.offsetHeight){				
				for(var no=0;no<optionArray.length;no++){
					optionArray[no].style.width = optionDiv.style.width.replace('px','') + 'px'; 	
				}	
			}		
			optionDiv.style.display='none';
			optionDiv.style.visibility='visible';
		}
		
		selectBoxIds = selectBoxIds + 1;
	}	
	
	</script>
<!-- InstanceEndEditable --> 

<!-- InstanceBeginEditable name="head" -->
<!--INICIO DEL AREA PARA CONTENIDO DEL HEAD-->


<!--NO COLOCAR NADA FUERA DE ESTE ESPACIO-->

<link href="includes/css/popup.css" rel="stylesheet" type="text/css">
<!--FIN DEL AREA PARA CONTENIDO DEL HEAD-->
<!-- InstanceEndEditable -->
</head> 

<body class="popbodyup">   
<div id="maincontainer">

  <div id="maincontent"><!-- InstanceBeginEditable name="contenido" -->  
  <!--INICIO DEL AREA PARA CONTENIDO DEL BODY-->
  <!-- ***************************FORMULARIO PARA SIGNUP *************************** Maria -->
  <div align="left" style="width:800px;">
  <h2 align="center"><strong>Documents Edit</strong></h2>
  <div class="colhalf" style="width:750px; margin-left:10px">
  <table border="0" cellpadding="3" cellspacing="0" width="100%">
  	<tr>
    	<td width="55%">
        	<?php if($tipo==0){?><input type="button" name="delete document" value="Delete" onClick="delete_doc(<?php echo $ssid;?>)"><?php }?><input type="button" name="update document" value="Refresh" onClick="grid_doc(<?php echo $id;?>)">
            <div style="border:thin solid; height:500px; padding: 10px; overflow:auto; position: relative;" id="block_doc">
                <div style="background-color:#FFFFE1;position: absolute;top:35%;left:20%;filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);"><img src="img/ac.gif">
                	Please wait, loading!...
                </div>
            </div>
        </td>
        <td width="45%">
        <h2 align="center"><strong>Add Document</strong></h2>
        <div style="border:thin solid; height:500px; padding: 10px; overflow:auto; position: relative;">
        	<table border="0" align="center" cellpadding="0" cellspacing="5">
                <tr>
                	<td style="font-size:12px; text-align:right">Document Name: </td><td><input type="text" name="doctitle" id="doctitle" value="" onClick="this.select();"></td>
                </tr>
                <tr>
                	<td colspan="2" style="color:#FF0000; text-align:center; font-size: 11px">* Click the icon to insert one page image (JPEG/JPG)<br> or PDF documents.</td>
                </tr>
                <tr>
                	<td colspan="2">
                    <table border="0" cellpadding="0" cellspacing="10" id="docimage" style="text-align:center;">
                    	<tr>
                            <td><img src="img/nophoto.gif" lugar="img/nophoto.gif" width="45" height="45" id="docimage1" style="padding:0 5 5 0"  onClick="window.open('docupload.php?id=docimage1','window','dependent=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no resizable=no,copyhistory=no,height=100,width=300,left=600,top=380');"><br><img src="img/left_doc.png"><img src="img/add_doc.png"><img src="img/del_doc.png"><img src="img/rigth_doc.png"></td>
                            <td><img src="img/nophoto.gif" lugar="img/nophoto.gif" width="45" height="45" id="docimage1" style="padding:0 5 5 0"  onClick="window.open('docupload.php?id=docimage1','window','dependent=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no resizable=no,copyhistory=no,height=100,width=300,left=600,top=380');"></td>
                            <td><img src="img/nophoto.gif" lugar="img/nophoto.gif" width="45" height="45" id="docimage1" style="padding:0 5 5 0"  onClick="window.open('docupload.php?id=docimage1','window','dependent=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no resizable=no,copyhistory=no,height=100,width=300,left=600,top=380');"></td>
                            <td><img src="img/nophoto.gif" lugar="img/nophoto.gif" width="45" height="45" id="docimage1" style="padding:0 5 5 0"  onClick="window.open('docupload.php?id=docimage1','window','dependent=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no resizable=no,copyhistory=no,height=100,width=300,left=600,top=380');"></td>
                    	</tr>
                        <tr>
                            <td></td>
                            <td><img src="img/left_doc.png"><img src="img/add_doc.png"><img src="img/del_doc.png"><img src="img/rigth_doc.png"></td>
                            <td><img src="img/left_doc.png"><img src="img/add_doc.png"><img src="img/del_doc.png"><img src="img/rigth_doc.png"></td>
                            <td><img src="img/left_doc.png"><img src="img/add_doc.png"><img src="img/del_doc.png"><img src="img/rigth_doc.png"></td>
                    	</tr>
                    </table>
                    </td>
                </tr>
                <tr>
                	<td colspan="2" align="right"><input type="button" value="Add" onClick="add_doc(<?php echo $id.','.$userid.','.$usuarioID.',\''.$tipo.'\'';?>);"></td>
                </tr>
            </table>
        </div>
        </td>
    </tr>
  </table>
  </div>
  </div>
<!--NO COLOCAR NADA FUERA DE ESTE ESPACIO-->
  <!--FIN DEL AREA PARA CONTENIDO DEL HEAD-->
<script>
if (window.attachEvent) {
	window.attachEvent("onload",function() {grid_doc(<?php echo $id;?>)});
} else {
	window.addEventListener("load", function() {grid_doc(<?php echo $id;?>)}, false);
}
</script>
<!-- InstanceEndEditable --></div>
  
 </div>

<!-- InstanceBeginEditable name="EditRegion5" --><!-- InstanceEndEditable -->

</body>
<script type="text/javascript" src="includes/tooltip.js"></script>
<!-- InstanceEnd --></html>