<?php 
$_SESSION["SERVER"]='www.ximausa.com';
$_SESSION["FOLDER"];
$_SERVERXIMA="http://www.ximausa.com/";

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES CON MAPA
	function tagHeadHeader1()
	{
?>
    	<title>XIMAUSA | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
		<meta http-equiv="Content-Language" content="EN-US"> 
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. Xima USA, a web-based property search system" />
		<meta name="title" content="XIMA Inc - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using Xima USA web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@ximausa.com">
		<meta name="copyright" content="XIMA, LLC - 2007">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.ximausa.com/favicon.ico">
		
		<script type="text/javascript" src="http://dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6"></script>
		<script type="text/javascript" src="MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="MANT/LIB/ext/ext-all.js"></script>

		<link rel="stylesheet" type="text/css" href="MANT/LIB/ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css" href="MANT/LIB/ext/resources/css/xtheme-xima.css" />

		<link href="includes/css/layout2.css" rel="stylesheet" type="text/css" />
		<link href="includes/css/properties_css.css" rel="stylesheet" type="text/css" />

<?php 
	}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES SIN MAPA
	function tagHeadHeader2()
	{
?>
    	<title>XIMAUSA | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
		<meta http-equiv="Content-Language" content="EN-US"> 
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. Xima USA, a web-based property search system" />
		<meta name="title" content="XIMA Inc - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using Xima USA web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@ximausa.com">
		<meta name="copyright" content="XIMA, LLC - 2007">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.ximausa.com/favicon.ico">
		
		<script type="text/javascript" src="MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="MANT/LIB/ext/ext-all.js"></script>

		<link rel="stylesheet" type="text/css" href="MANT/LIB/ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css" href="MANT/LIB/ext/resources/css/xtheme-xima.css" />

		<link href="includes/css/layout2.css" rel="stylesheet" type="text/css" />
		<link href="includes/css/properties_css.css" rel="stylesheet" type="text/css" />

<?php 
	}
?>

<?php 
	//TOP HEADER: logo xima xima y links de Register Now! | Log In | Log Out | About Us | Contact Us | Training | Tutorial
	function topLoginHeader()
	{
		global $_SERVERXIMA;
?>
			<div align="center" style="height:80px; width:800px;">
            	<div align="left" style="float:right; width:550px; height:23px; line-height:23px; background:url(img/tabs-login.jpg) repeat-x; padding-left:2px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;">
                        	<a href="http://www.ximausa.com/register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="properties_myaccount.php" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                        	<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" width="16" height="16" title="Logoff"  ></a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<!--<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" ></a>
                            <span style="color:#999999; ">|&nbsp;</span>-->
                        	<a href="properties_search.php">Go Search</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/aboutus.php">About Us</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/contactus.php">Contact Us</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/trainindex.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/helpindex.php?indice_help=1">Tutorial</a>
                        </li>
                    </ul>
                </div>
                <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                    <img src="img/foreclosures-foreclosed-homes1.png" width="125" height="50" alt="Xima USA, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                </a>
                <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
<div id="ciSlBh" style="z-index:100; position: absolute; left:1px; "></div><div id="scSlBh" style="background:url(img/customer.png) no-repeat; background-position:top center; padding:5px 5px 5px 5px; z-index:200; position: absolute; top:20px; left:500px; margin: -2px 0px 0px 0px; height:60px; width:150px; "></div><div id="sdSlBh"></div><script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
			<!-- END ProvideSupport.com Graphics Chat Button Code -->
            </div>
<?php 
	}
?>

<?php 
	//TOP HEADER: logo xima xima y links de Register Now! | Log In | Log Out | About Us | Contact Us | Training | Tutorial
	function topLoginHeader2()
	{
		global $_SERVERXIMA;
?>
			<div align="center" style="height:60px; width:800px;">
            	<div align="left" style="float:right; width:550px; height:23px; line-height:23px; background:url(img/tabs-login.jpg) repeat-x; padding-left:2px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;">
                        	<a href="http://www.ximausa.com/register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="properties_myaccount.php" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                        	<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" width="16" height="16" title="Logoff"  ></a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/aboutus.php">About Us</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/contactus.php">Contact Us</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/trainindex.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/helpindex.php?indice_help=1">Tutorial</a>
                        </li>
                    </ul>
                </div>
                <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                    <img src="img/foreclosures-foreclosed-homes1.png" width="125" height="50" alt="Xima USA, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                </a>
            </div>
<?php 
	}
?>

<?php 
	//infromacion para el google-analytics
	function googleanalytics()
	{
?>
		<script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
        try {
        var pageTracker = _gat._getTracker("UA-7654830-1");
        pageTracker._trackPageview();
        } catch(err) {}</script>
<?php 
	}
?>