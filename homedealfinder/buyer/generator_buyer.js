
function searchForm_buyer(id,tipo){
	var value=document.getElementById(id).value;
	var long=document.getElementById(id).value.length;
	var county=document.getElementById('county_search_buyer').value;
   
	if(long>0 && value!='Address' && value!='Parcelid')
	
		GuardarSearch_buyer(tipo)
}	

function GuardarSearch_buyer(tipo){
     
	//var _search='';
//	if(document.getElementById('search_buyer_t').value=='Address'||document.getElementById('search_buyer_t').value=='Name')
//	_search=' '; else
	_search=document.getElementById('search_buyer_t').value;

  	var proptype='';
	if(document.getElementById(search_type+'_proptype'))proptype=document.getElementById(search_type+'_proptype').value;	
	var price_low='';
	if(document.getElementById(search_type+'_price_low'))price_low=document.getElementById(search_type+'_price_low').value;	
	var price_hi='';
	if(document.getElementById(search_type+'_price_hi'))price_hi=document.getElementById(search_type+'_price_hi').value;	
	var bed=-1;
	if(document.getElementById(search_type+'_bed'))bed=document.getElementById(search_type+'_bed').value;	
	var bath=-1;
	if(document.getElementById(search_type+'_bath'))bath=document.getElementById(search_type+'_bath').value;	
	var sqft=-1;
	if(document.getElementById(search_type+'_sqft'))sqft=document.getElementById(search_type+'_sqft').value;	
	var pendes=-1;
	if(document.getElementById(search_type+'_pendes'))pendes=document.getElementById(search_type+'_pendes').value;	
	var occupied=-1;
	if(document.getElementById(search_type+'_occupied'))occupied=document.getElementById(search_type+'_occupied').value;
	var mapa_search_latlong='-1';
	if(document.getElementById('mapa_search_latlong'))mapa_search_latlong=document.getElementById('mapa_search_latlong').value;	
	var realtor='-1';
	if(document.getElementById(search_type+'_realtor'))realtor=document.getElementById(search_type+'_realtor').value;
	var equity=-1;
	if(document.getElementById(search_type+'_pequity'))equity=document.getElementById(search_type+'_pequity').value;
	var entrydate='';
	if(document.getElementById(search_type+'_entrydate'))entrydate=document.getElementById(search_type+'_entrydate').value;
	if(search_by_type=='MAP' && mapa_search_latlong=='-1' && _search==''){
		alert('Map shape/polygon is requiered to execute the search');
		return false;
	}
	
			
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_coresearch.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			search_type:'BU',
			tsearch: 'location',
			search: _search,
			county: document.getElementById('county_search_buyer').value,
			proptype: document.getElementById('proptype_buyer').value,
			price_low: price_low,
			price_hi: price_hi,
			date_low:document.getElementById('date_low_buyer').value,
			date_hi:document.getElementById('date_hi_buyer').value,
			bed: bed,
			bath: bath,
			sqft: sqft,
			pendes: pendes,
			owns:document.getElementById('owns_buyer').value,
			resultby: document.getElementById('result_search_buyer').value,
			groupby:document.getElementById('combo_search_types_buyer').value,
			pequity: equity,
			search_mapa: mapa_search_latlong,
			entrydate: entrydate,
			occupied: occupied
			
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			systemsearch= 'basic';
			buyer=true;
			createResultbuyer();
		}                                
	});
}



function createResultbuyer(){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultbuyerTab')){
		var tab = tabs.getItem('resultbuyerTab');
		tabs.remove(tab);
	}
	
	if(document.getElementById('overviewbuyerTab')){
		var tab = tabs.getItem('overviewbuyerTab');
		tabs.remove(tab);
	}
	if(document.getElementById('overviewbuyerTab2')){
		var tab = tabs.getItem('overviewbuyerTab2');
		tabs.remove(tab);
	}
	 if(document.getElementById('overviewbuyerTab3')){
		var tab = tabs.getItem('overviewbuyerTab3');
		tabs.remove(tab);
	}
		
	
	tabs.add({
		title: ' Result Group',
		id: 'resultbuyerTab',
		autoLoad: {url: 'buyer/properties_result_buyer1.php', scripts: true, discardUrl:true, nocache:true, params:{systemsearch:systemsearch}},
		tbar: new Ext.Toolbar({
				id:'menu_result_adv3',
				cls: 'no-border',
				width: 'auto',
				items: [' ',
					{
						 tooltip: 'Click to Print Report',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.ximausa.com/img/toolbar/printer.png',
						 scale: 'medium',
						 //hidden:icon_result,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print detailed information.');
							
							var parcelids_res2='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res2=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res2+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request( 
								{  
									waitMsg: 'Printing Report...',
									url: 'toolbars_types/properties_pdf.php?systemsearch=basic&groupbylevel=1', 
									method: 'POST', 
									params: {
										userweb:user_web,
										realtorid:user_webid,
										parcelids_res:parcelids_res2,
										template_res:'Default'
									},
									
									failure:function(response,options){
										Ext.MessageBox.alert('Warning','file can not be generated');
										loading_win.hide();
									},
									success:function(response,options){
										var rest = Ext.util.JSON.decode(response.responseText);
										//alert(rest.pdf);
										var url='http://www.ximausa.com/'+rest.pdf;
										//alert(url);
										loading_win.hide();
										window.open(url);
										
									}                                
								 }
							);
						 }
					
					},{
						tooltip: 'Click to Excel Report',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.ximausa.com/img/toolbar/excel.png',
						scale: 'medium',
						//hidden:icon_result,
						handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print Excel detailed information.');
							
							var ownerShow='false';
							var parcelids_res2='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res2=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res2+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',
								url: 'toolbars_types/properties_excel.php?systemsearch=basic', 
								method: 'POST', 
								params: {
									userweb:user_web,
									realtorid:user_webid,

									ownerShow: ownerShow,
									parcelids_res:parcelids_res2,
									template_res:'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.ximausa.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
							
							
						}
					}					  
				],
				autoShow: true
			}),
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultbuyerTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function pagingResultbuyer(page){
	window.scrollTo(0,0);
	var tab2 = tabs.getItem('resultbuyerTab');
	tabs.setActiveTab(tab2);
	
	var updaterbuyer = tab2.getUpdater();
	updaterbuyer.update({url:'buyer/properties_result_buyer.php?page='+page, scripts: true, discardUrl:true, nocache:true,
				   params:{userweb:user_web,systemsearch:systemsearch} });
}

function orderByResultbuyer(order,dir){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_order_by_result_buyer.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			order: order+' '+dir
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			pagingResultbuyer(0);	
		}                                
	});
}

function createoverviewbuyer(owner,ownera){
	
		if(document.getElementById('overviewbuyerTab')){
		var tab = tabs.getItem('overviewbuyerTab');
		tabs.remove(tab);
	}
	if(document.getElementById('overviewbuyerTab2')){
		var tab = tabs.getItem('overviewbuyerTab2');
		tabs.remove(tab);
	}
	
	window.scrollTo(0,0);
	var nivel=2;	
		
	var url='buyer/properties_result_buyer2.php?owner='+owner+'&ownera='+ownera+'&nivel='+nivel;
		 	
	tabs.add({
		title: ' Result Owner ',
		id: 'overviewbuyerTab',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, params:{userweb:user_web}},
		tbar: new Ext.Toolbar({
				id:'menu_result_adv4',
				cls: 'no-border',
				width: 'auto',
				items: [' ',
					{
						 tooltip: 'Click to Print Report',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.ximausa.com/img/toolbar/printer.png',
						 scale: 'medium',
						 //hidden:icon_result,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print detailed information.');
							
							var parcelids_res2='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res2=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res2+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request( 
								{  
									waitMsg: 'Printing Report...',
									url: 'toolbars_types/properties_pdf.php?systemsearch=basic', 
									method: 'POST', 
									params: {
										userweb:user_web,
										realtorid:user_webid,
										parcelids_res:parcelids_res2,
										template_res:'Default'
									},
									
									failure:function(response,options){
										Ext.MessageBox.alert('Warning','file can not be generated');
										loading_win.hide();
									},
									success:function(response,options){
										var rest = Ext.util.JSON.decode(response.responseText);
										//alert(rest.pdf);
										var url='http://www.ximausa.com/'+rest.pdf;
										//alert(url);
										loading_win.hide();
										window.open(url);
										
									}                                
								 }
							);
						 }
					
					},{
						tooltip: 'Click to Excel Report',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.ximausa.com/img/toolbar/excel.png',
						scale: 'medium',
						//hidden:icon_result,
						handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print Excel detailed information.');
							
							var ownerShow='false';
							var parcelids_res2='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res2=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res2+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',
								url: 'toolbars_types/properties_excel.php?systemsearch=basic', 
								method: 'POST', 
								params: {
									userweb:user_web,
									realtorid:user_webid,

									ownerShow: ownerShow,
									parcelids_res:parcelids_res2,
									template_res:'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.ximausa.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
							
							
						}
					}					  
				],
				autoShow: true
			}),
		closable: true,
		
				}).show();
		
		
	if(Ext.isIE){
		var tab = tabs.getItem('overviewbuyerTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
	
}

function createoverviewbuyer2(owner,ownera){
	
	 if(document.getElementById('overviewbuyerTab2')){
		var tab = tabs.getItem('overviewbuyerTab2');
		tabs.remove(tab);
	} 
	 
	window.scrollTo(0,0);
	
	var url='buyer/properties_advance_result2.php?owner='+owner+'&ownera='+ownera+'&type=BU';
		 	
	tabs.add({
		title: ' Result Property',
		id: 'overviewbuyerTab2',
		autoLoad: {url: url, scripts: true, discardUrl:true, nocache:true, params:{userweb:user_web}},
		tbar: new Ext.Toolbar({
				id:'menu_result_adv2',
				cls: 'no-border',
				width: 'auto',
				items: [' ',
					{
						 tooltip: 'Click to Show/Hide Map',
						 id: 'toolbarmapResultAdvbuyer',
						 iconCls:'icon',
						 icon: 'http://www.ximausa.com/img/toolbar/map.png',
						 iconAlign: 'top',
						 width: 40,
						 //hidden:icon_result,
						 scale: 'medium',
						 enableToggle: true,
						 handler: function(){
							if(document.getElementById('mapResultAdvbuyer').style.display=='none'){
								document.getElementById('mapResultAdvbuyer').style.display='';
								if(user_loged)
									mapResultAdvbuyer.map.Resize(system_width-20,320);
								else
									mapResultAdvbuyer.map.Resize(620,320);
									
								mapResultAdvbuyer.map.SetMapView(arrLatLongbuyer);
							}else
								document.getElementById('mapResultAdvbuyer').style.display='none';
						 }
					},{
						 tooltip: 'Click to View Legend',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.ximausa.com/img/toolbar/legend.png',
						 // hidden:icon_result,
						 scale: 'medium',
						 handler: function(){
							var dataLegend = [
								['S','http://www.ximausa.com/img/houses/verdetotal.png','Subject'],
								['A-F','http://www.ximausa.com/img/houses/verdel.png','Active Forclosed'],
								['A-F-S','http://www.ximausa.com/img/houses/verdel_s.png','Active Forclosed Sold'],
								['A-P','http://www.ximausa.com/img/houses/verdel.png','Active Pre-Forclosed'],
								['A-P-S','http://www.ximausa.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
								['A-N','http://www.ximausa.com/img/houses/verdeb.png','Active'],
								['CC-F','http://www.ximausa.com/img/houses/cielol.png','By Owner Forclosed'],
								['CC-F-S','http://www.ximausa.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
								['CC-P','http://www.ximausa.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
								['CC-P-S','http://www.ximausa.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
								['CC-N','http://www.ximausa.com/img/houses/cielo.png','By Owner'],
								['CS-F','http://www.ximausa.com/img/houses/marronl.png','Closed Sale Forclosed'],
								['CS-F-S','http://www.ximausa.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
								['CS-P','http://www.ximausa.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
								['CS-P-S','http://www.ximausa.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
								['CS-N','http://www.ximausa.com/img/houses/marronb.png','Closed Sale'],
								['N-F','http://www.ximausa.com/img/houses/grisl.png','Non-Active Forclosed'],
								['N-F-S','http://www.ximausa.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
								['N-P','http://www.ximausa.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
								['N-P-S','http://www.ximausa.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
								['N-N','http://www.ximausa.com/img/houses/grisb.png','Non-Active']
							];
							 
							 var store = new Ext.data.ArrayStore({
								idIndex: 0,
								fields: [
									'status', 'url', 'description'
								]
							 });
							
							store.loadData(dataLegend);
							
							
							 var listView = new Ext.list.ListView({
								store: store,
								multiSelect: false,
								emptyText: 'No Legend to display',
								columnResize: false,
								columnSort: false,
								columns: [{
									header: 'Color',
									width: .15,
									dataIndex: 'url',
									tpl: '<img src="{url}">'
								},{
									header: 'Status',
									width: .2,
									dataIndex: 'status'
								},{
									header: 'Description',
									dataIndex: 'description'
								}]
							});
							
							var win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: listView,
					
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
						 tooltip: 'Click to Print Report',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.ximausa.com/img/toolbar/printer.png',
						 scale: 'medium',
						 //hidden:icon_result,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print detailed information.');
							
							var parcelids_res='';
							if(!AllCheckR){
								var results = selected_dataR3;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request( 
								{  
									waitMsg: 'Printing Report...',
									url: 'toolbars_types/properties_pdf.php?systemsearch=basic', 
									method: 'POST', 
									params: {
										userweb:user_web,
										realtorid:user_webid,
										parcelids_res:parcelids_res,
										template_res: Ext.getCmp('templateCombo2').getValue()
									},
									
									failure:function(response,options){
										Ext.MessageBox.alert('Warning','file can not be generated');
										loading_win.hide();
									},
									success:function(response,options){
										var rest = Ext.util.JSON.decode(response.responseText);
										//alert(rest.pdf);
										var url='http://www.ximausa.com/'+rest.pdf;
										//alert(url);
										loading_win.hide();
										window.open(url);
										
									}                                
								 }
							);
						 }
					},{
						 tooltip: 'Click to Print Labels',
						 //text: 'Labels',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 //hidden:icon_mylabel,
						 icon: 'http://www.ximausa.com/img/toolbar/label.png',
						 scale: 'medium',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print Labels detailed information.');
							
							var simple = new Ext.FormPanel({
								labelWidth: 150, 
								url:'toolbars_types/properties_label.php?systemsearch=basic',
								frame:true,
								title: 'Property Labels',
								bodyStyle:'padding:5px 5px 0',
								width: 400,
								waitMsgTarget : 'Generated Labels...',
								
								items: [{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[5160,5160],
													[5161,5161],
													[5162,5162],
													[5197,5197],
													[5163,5163]
											]
										}),
										name: 'label_type',
										fieldLabel: 'Label Type',
										mode: 'local',
										value: 5160,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[8,8],
													[9,9],
													[10,10]
											]
										}),
										name: 'label_size',
										fieldLabel: 'Label Size',
										mode: 'local',
										value: 8,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[0,'Owner'],
													[1,'Property']
											]
										}),
										name: 'address_type',
										fieldLabel: 'Address',
										mode: 'local',
										value: 0,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['L','Left'],
													['C','Center']
											]
										}),
										displayField:'title',
										valueField: 'val',
										name: 'align_type',
										fieldLabel: 'Alingment',
										mode: 'local',
										value: 'L',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['N','No'],
													['Y','Yes']
											]
										}),
										name: 'resident_type',
										fieldLabel: 'Current Resident Or',
										mode: 'local',
										value: 'N',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'hidden',
										name: 'type',
										value: 'result'
									}
								],
						
								buttons: [{
									text: 'Apply',
									handler  : function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													//Ext.Msg.alert("Failure", action.result.pdf);
													var url='http://www.ximausa.com/'+action.result.pdf;
													loading_win.hide();
													window.open(url);
													//window.open(url,'Print Labels',"fullscreen",'');
												},
												failure: function(form, action) {
													Ext.Msg.alert("Failure", action.result.msg);
													loading_win.hide();
												}
											});
										}
								},{
									text: 'Cancel',
									handler  : function(){
											simple.getForm().reset();
										}
								}]
							});
							win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: simple,
					
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							win.addListener("beforeshow",function(win){
								simple.getForm().reset();
							});
						 }
					},{
						tooltip: 'Click to Excel Report',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.ximausa.com/img/toolbar/excel.png',
						scale: 'medium',
						//hidden:icon_result,
						handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block) Ext.MessageBox.alert('Warning','You are not allowed to view - print Excel detailed information.');
							
							var ownerShow='false';
							var parcelids_res='';
							if(!AllCheckR){
								var results = selected_dataR3;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
								}
							}
							
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',
								url: 'toolbars_types/properties_excel.php?systemsearch=basic', 
								method: 'POST', 
								params: {
									userweb:user_web,
									realtorid:user_webid,

									ownerShow: ownerShow,
									parcelids_res:parcelids_res,
									template_res: Ext.getCmp('templateCombo2').getValue()
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.ximausa.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
							
							
						}
					},{
						 tooltip: 'Click to Manage Template',
						 iconCls:'icon',
						 icon: 'http://www.ximausa.com/img/toolbar/template.png',
						 iconAlign: 'top',
						 width: 40,
						 hidden:true,
						 scale: 'medium',
						 handler: function(){
							ShowManageTemplate();
						 }
					}
                        ,new Ext.form.ComboBox({
						id: 'templateCombo2',
						hidden:true,
						fieldLabel: '',
						triggerAction: 'all',
						mode: 'remote',
						forceSelection: true,
						store: new Ext.data.JsonStore({
							url: 'properties_manage_template.php',
							id: 0,
							fields: [
								'tID',
								'tname'
							]
						}),
						value: 'Default',
						width: 130,
						valueField: 'tID',
						displayField: 'tname',
						listeners:{
							'select': function (combo,record,index){
								ResultTemplate=record.data.tID;
								AllCheckR=false;
								selected_dataR3=new Array();
								gridR3.getSelectionModel().clearSelections();
								storeR.load({'params': {'ResultTemplate': ResultTemplate}});
							}
						}
					}),
					'->',{
						 tooltip: 'Click to Close Result',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.ximausa.com/img/cancel.png',
						 scale: 'medium',
						  
						 handler: function(){
							 var tab = tabs.getItem('overviewbuyerTab2');
							 tabs.remove(tab);
						 }
					  }
				],
				autoShow: true
			}), 
		closable: true,
		
		
				}).show();
		
		
	if(Ext.isIE){
		var tab = tabs.getItem('overviewbuyerTab2');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
	
}