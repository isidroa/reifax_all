<?php
	require_once('../resources/template/template.php');
	
	

	$_GET['resultType'] = 'basic';
	$_GET['systemsearch'] = 'basic';
	$max_page_pagin=$max_page_pagin_r=20;
	set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot\\'. PATH_SEPARATOR);
	include('coresearch.php');
	//print_r($list_reg);
	$num_rows=$limit_cant_reg;
	//Variables for Paging
	$num_page_pagin=$num_rows>0?ceil($num_rows_all/$num_rows):0;
	if($max_page_pagin>$num_page_pagin) $max_page_pagin=$num_page_pagin;
	$star_for=$max_page_pagin>0?floor($num_limit_page/$max_page_pagin)*$max_page_pagin:0;
	$end_for=$star_for+$max_page_pagin;
	if($end_for>$num_page_pagin) $end_for=$num_page_pagin;
	$star_page=($num_limit_page*$limit_cant_reg)+1;
	$tablaporder='psummary';
		if($type_search=='PR' || $type_search=='FO' || $type_search=='MO') $tablaporder='psummary';
		elseif($type_search=='FS') $tablaporder='mlsresidential';
		elseif($type_search=='FR'){	$tablaporder='rental';	}
	setcookie('type_search',$type_search,0,'/','reifax.com');
?>
<!DOCTYPE HTML>
<html>
<meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0" />
<!-- Head of ReiFax Website -->
<?php ReiFaxHead(1);?>
<style>
body{
	overflow:hidden;
}
	.botonOrdenamiento{
		width:24px;
		height:24px;
		display:block;
		border: 1px solid #AAAAAA;
	}
	.botonOrdenamiento:hover{
		border: 1px solid #666666;
		background-color:#FFF;
		-moz-box-shadow: 1px 1px 2px #666666;
		-webkit-box-shadow: 1px 1px 2px #666666;
	}
	#result_orderby_asc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-asc.gif) center no-repeat;
    	border-radius: 4px 0 0 4px;
	}
	#result_orderby_desc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-desc.gif) center no-repeat;
    	border-radius: 0 4px 4px 0;
	}
	.chzn-container-single .chzn-single {
		border-radius: 0;
		height: 27px;
	}
	.content{
		margin-right:0px;
		overflow: scroll;
	}
	@media screen and (min-width:990px) {
		.view{
			height:auto !important;
		}
		.container{
			width:100%;
			height:100%;
		}
		#containerList{
			float: right;
			height: 100%;
			overflow: hidden;
			width: 630px;
		}
		#toolbarPagin{
			position:fixed; width:630px; z-index:3;
		}
		#containerList .clearEmpty:first-child{
			margin-top:70px;
		}
		#containerList .result2{
			cursor:pointer;
		}
		#containerMapPrincipal{
			position:relative;
			height: 100%;
			float: right;
		}
		.standarView{
			display: block;
		}
		.mobileView{
			display:none;
		}
		.reportingframework{
			margin-bottom:0;
		}
		input[name=locationSearch]{
			width: 443px;
		}
	}
	@media screen and (max-width:990px) {
		.view{
			height:auto !important;
		}
		 html,
		 body {
			height: 100%;
		  } 
		.container{
			width:990px;
			height:100%;
		}
		.standarView{
			display:none;
		}
		.mobileView{
			display: block;
		}
		#containerList{
			float: right;
			overflow: hidden;
			width: 445px;
		}
		input[name=locationSearch]{
			width: 363px;
		}
		#toolbarPagin{
		}
		#containerList .result2{
			cursor:pointer;
		}
		#containerMapPrincipal{
			position:relative;
			height: 100%;
			width: 445px;
			float: right;
		}
		.reportingframework{
			margin-bottom:0;
		}
	}
	.backMenu2 > form > ul > li{
		float:left;
		margin-left:10px;
		list-style:none;
		margin-top:4px;
	}
.arrow_box {
	width:160px;
	position: relative;
	background: #ffffff;
	border: 2px solid #000000;
	margin-left: -70px;
    margin-top: -30px;
	border-radius:3px;
	padding:0;
	-webkit-box-shadow: 0 10px 6px -6px #777;
	-moz-box-shadow: 0 10px 6px -6px #777;
	box-shadow: 0 10px 6px -6px #777;
}
.arrow_box h5{
	background:#5eab1f;
	color:#FFF;
	margin:0;
	font-size:0.8em;
	padding:0px;
	text-align:center;
    overflow:hidden;
    white-space:nowrap;
    text-overflow: ellipsis;
	margin-bottom:2px;
}
.arrow_box p{
    overflow:hidden;
    white-space:nowrap;
	font-size: 13px;
	padding:0 4px;
	margin: 0;
    text-overflow: ellipsis;
}
.arrow_box a{
	color:#5eab1f;
	font-weight:bold;
}
.arrow_box:after, .arrow_box:before {
	top: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(255, 255, 255, 0);
	border-top-color: #ffffff;
	border-width: 10px;
	margin-left: -10px;
}
.arrow_box:before {
	border-color: rgba(0, 0, 0, 0);
	border-top-color: #000000;
	border-width: 13px;
	margin-left: -13px;
}
.result2 .view {
  padding: 6px 0;
  font-size: 14px;
}
.result2  h3 {
    margin-bottom: 2px;
}
.result2 .view p span{
    overflow:hidden;
    white-space:nowrap;
    text-overflow: ellipsis;
}
.containerListProperty{
	max-height:100%;
	overflow:scroll;
}
</style>

<body>
<div class="backMenu">
<?php ReiFaxHeaderSearch();?>
</div>
<div class="backMenu2">
	<form id="formFilter">
    	<ul>
        	<li>
                <select name="search_filter_proptype" class="fieldtext" data-placeholder="Property Type"  style="width:146px">
		                <option value=""></option>
                        <option value="">Any</option>
                        <option value="01">Single Family</option>
                        <option value="04">Condo/Town/Villa</option>
                        <option value="03">Multi Family +10</option>
                        <option value="08">Multi Family -10</option>
                        <option value="11">Commercial</option>
                        <option value="00">Vacant Land</option>
                        <option value="02">Mobile Home</option>
                        <option value="99">Other</option>
                </select>
            </li>
        	<li>
            	<select class="fieldtext" name="search_filter_sqft" data-placeholder="Gross Area" style="width:146px">
	                <option value=""></option>
                    <option value="-1">Any</option>
                    <option value="250" >250+</option>
                    <option value="500" >500+</option>
                    <option value="1000" >1,000+</option>
                    <option value="1250" >1,250+</option>
                    <option value="1500" >1,500+</option>
                    <option value="1750" >1,750+</option>
                    <option value="2000" >2,000+</option>
                    <option value="2250">2,250+</option>
                    <option value="2500">2,500+</option>
                    <option value="2750" >2,750+</option>
                    <option value="3000" >3,000+</option>
                    <option value="3250" >3,250+</option>
                    <option value="3500" >3,500+</option>
                    <option value="3750" >3,750+</option>
                    <option value="4000" >4,000+</option>
                    <option value="5000" >5,000+</option>
                    <option value="10000" >10,000+</option>
                </select>
            </li>
        	<li>
                <select class="fieldtext" name="search_filter_bed" data-placeholder="Beds" style="width:146px;height: 22px;">
                	<option value=""></option>
                    <option value="-1">Any</option>
                    <option value="1" >1+</option>
                    <option value="2" >2+</option>
                    <option value="3" >3+</option>
                    <option value="4" >4+</option>
                    <option value="5" >5+</option>
                </select>
            </li>
            <li>
                <select class="fieldtext" name="search_filter_bath" data-placeholder="Baths" style="width:146px; height: 22px;">
                	<option value=""></option>
                    <option value="-1">Any</option>
                    <option value="1" >1+</option>
                    <option value="2" >2+</option>
                    <option value="3" >3+</option>
                    <option value="4" >4+</option>
                    <option value="5" >5+</option>
                </select>
            </li>
        </ul>
    </form>
</div>
<div class="container">
    <!-- Header of ReiFax Website -->
	<div id="containerList">
    	<div>
		<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
                         <span style="font-size:18px" class="title bold bluetext">
                            Properties Found: <?php echo $num_rows_all ?>
                         </span>    
            	</div>
        	</div>
        </div>
       <div class="result2Option">
                	<div class="">
                         <div style="position:absolute; top:1px; left:10px;">
                            <div class="botonDesplazamiento">
                                <span class="bluetext">Order By:</span>
                            </div>
                            <div class="botonDesplazamiento">
                                <select id="orderByTo" style="width:120px; height: 22px;">
                                    <option value="ORDER BY p.county,p.address">County</option>
                                    <option value="ORDER BY p.address">Address</option>
                                    <option value="ORDER BY mlnumber">Mlnumber</option>
                                    <option value="ORDER BY p.parcelid">Parcel ID</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.yrbuilt">Year Built</option>
                                    <option value="ORDER BY dom">DOM</option>
                                    <option value="ORDER BY larea">Living Area</option>
                                    <option value="ORDER BY garea">Gross Area</option>
                                    <option value="ORDER BY tsqft">Lot Size</option>
                                    <option value="ORDER BY waterf">Water Front</option>
                                    <option value="ORDER BY pool">Pool</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.beds">Beds</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.bath">Baths</option>
                                    <option value="ORDER BY p.city">City</option>
                                    <option value="ORDER BY p.zip">Zip Code</option>
                                    <option value="ORDER BY price">Price</option>
                                    <option value="ORDER BY p.xcoded">Property Types</option>
                                </select>
                            </div>
							<div class="botonDesplazamiento">
                                <a href="#" class="botonOrdenamiento" rel='ASC' id="result_orderby_asc">&nbsp;
                                </a>
                            </div>
                            <div class="botonDesplazamiento">
                                <a href="#" class="botonOrdenamiento" rel='DESC' id="result_orderby_desc">&nbsp;
                                </a>
                            </div>
                        </div>
                         <div style="position:absolute; top:1px; right:10px;">
                              <?php
                             if($num_limit_page>0)
                             {
                                 echo '
                                 <div class="botonDesplazamiento standarView">
                                    <a href="?page=0&order='.$_GET['order'].'" class="buttongreen flatButton">|&lt;</a>
                                    <a href="?page='.($num_limit_page-1) .'&order='.$_GET['order'].'" class="buttongreen flatButton">&lt;</a>
                                </div>';
                             }
                             ?>
                             <div class="botonDesplazamiento">
                                   <select id="changePageTo" style="width:110px; height: 22px;">
                                     <?php
                                        for($j=0;$j<$num_page_pagin;$j++)
                                        {
                                            echo ($num_limit_page==$j)?'<option value="?page='. $j .'&order='.$_GET['order'].'" selected>Page '.($j+1).'</option>':'<option value="?page='. $j .'&order='.$_GET['order'].'">Page '.($j+1).'</option>';
                                        }
                                    ?>
                                     </select>
                             </div>
                             <?php
                             if($num_limit_page!=($num_page_pagin-1))
                             {
                                echo'
                                 <div class="botonDesplazamiento standarView">
                                    <a href="?page='.($num_limit_page+1)  .'&order='.$_GET['order'].'" class="buttongreen flatButton">&gt;</a>
                                    <a href="?page='.($num_page_pagin-1)  .'&order='.$_GET['order'].'" class="buttongreen flatButton">&gt;|</a>
                                 </div>';
                             }
                             ?>
                         </div>
                    </div>
					<div class="clearEmpty"></div>
                </div>
            </div>
            <div class="containerListProperty">
       	<?php 
			$i=($num_limit_page*10)+1;
			foreach($list_reg as $key => $val)
			{
				
				if($type_search=='FO'){
					$val['price']=NULL;
					$val['address']=preg_replace('/^\d*\s*/','',$val['address']);
				}
				if($val['xcoded']!='Vacant Land'){
					$val['beds']=$val['beds']==0?'N/A':$val['beds'];
					$val['bath']=$val['bath']=='0.0'?'N/A':$val['bath'];
					$val['sqft']=$val['sqft']==0?'N/A':$val['sqft'];
				}
				echo '
				<div class="result2" data-parcel="'.$val['parcelid'].'" data-lat="'.$val['latitude'].'" data-log="'.$val['longitude'].'">';
				//print_r($val['tieneImg']);print_r($val['imagen']);
				if($val['tieneImg'] == 'Y'){
					echo 
					'<div class="view">
						<div id="map_result_'.$key.'" class="img containerMapView" style="width:135px; height:135px; overflow:hidden; position:relative;">
							<img border="0" src="'.$val['imagen'].'">
						</div>
					</div>';
					//echo "aquii ... ".$list_reg[0]['latitude'].','.$list_reg[0]['longitude'];
				// http://prntscr.com/8s1rz3
				// http://prntscr.com/8s1v2g
				}else	{
					echo 
					'<div class="view">
						<div id="map_result_'.$key.'" class="img containerMapView" style="width:135px; height:135px; overflow:hidden; position:relative;">
							<img border="0" src="//maps.googleapis.com/maps/api/staticmap?center='.$val['latitude'].','.$val['longitude'].'&size=135x135&zoom=19&maptype=satellite&key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4">
						</div>
					</div>';
					//echo "aquii ... ".$list_reg[0]['latitude'].','.$list_reg[0]['longitude'];
				}	
				echo
					'<div class="title"> <!--<span class="price">$'.number_format($val['price'],0,'.',',').'</span> --><a href="'.$val['url'].'">'.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].'</a></div>
					
					<div class="view mobileView">
						'.((trim($val['price'])=='' || $val['price']==NULL || $val['price']==0)?'':'<h3><span class="price">$'.number_format($val['price'],0,'.',',').'</span></h3>').'
						<p>
							<span>REIFax Value $'.number_format($val['marketvalue'],0,'.',',').'</span><br>
							<span>'.substr($val['xcoded'],0,25).'</span>
							<span>'.($val['pendes']=='N' ? 'Not in Foreclosure' : ($val['pendes']=='P' ? 'Pre-Foreclosed' : 'Foreclosed')).'</span><br>
							<span>'.$val['beds'].' beds, '.$val['bath'].' baths, '.$val['sqft'].' sqft</span><br>
							<span>Built in '.$val['yrbuilt'].'</span><br>
						</p>
					</div>
					<div class="view standarView">'.((trim($val['price'])=='' || $val['price']==NULL || $val['price']==0)?'':'<h3><span class="price">$'.number_format($val['price'],0,'.',',').'</span></h3>').'
						<p>
							<span>REIFax Value $'.number_format($val['marketvalue'],0,'.',',').'</span><br>
							<span>'.substr($val['xcoded'],0,25).'</span><br>
							<span>'.($val['pendes']=='N' ? 'Not in Foreclosure' : ($val['pendes']=='P' ? 'Pre-Foreclosed' : 'Foreclosed')).'</span>
						</p>
					</div>
					<div class="view standarView">
						<p>
							<span>'.$val['county'].'</span><br>
							<span>'.$val['beds'].' beds, '.$val['bath'].' baths, '.$val['sqft'].' sqft</span><br>
							<span>Built in '.$val['yrbuilt'].'</span><br>
							<span>'.(($val['dom']!='')?$val['dom'].' Days on REIFax':'').'</span><br>
						</p>
					</div>
						<a href="'.$val['url'].'" rel="'.$val['parcelid'].'|'.$val['county'].'" class="buttonblue">View Details</a>
					<div class="clearEmpty"></div>
				</div>
					<div class="clearEmpty"></div>
				';
			$i++;
			}
		?>
        </div>
	</div>
	<div id="containerMapPrincipal"></div>
     <input type="hidden" name="containerMapPrincipal_search_latlong" id="containerMapPrincipal_search_latlong" value="-1" />
</div>
<div class="clearEmpty"></div>
</body>

</html>
<script src="../resources/js/chosen.jquery.min.js"></script> 
<script src="/includes/properties_draw.js"></script>
<script language="javascript">
	$(document).ready(function (){
		/****************
		*	funciones de busqueda
		****************************/
		$('#typeSerach').chosen({disable_search_threshold: 15});
		var query=getUrlVars()['q'];
		if(query){
			query=query.split('|');
			$.each(query,function (i,e){
				var temp=e.split(':');
				$('[name="'+temp[0]+'"]').val(temp[1]);
			}); 
		}
		/****************
		*	funciones de filtrado
		****************************/
		$('.backMenu2 select').chosen({disable_search_threshold: 20}).change(function (){
			var campo=$(this).attr('name');
			var value=$(this).val();
			$.ajax({
				url: '../../properties_filter_by_result.php', 
				type: 'POST',
				data: {
					campo:campo,	
					value:value
				},
				success: function ()
				{
					var query=getUrlVars()['q'];
					if(query){
						query=query.split('|');
						var test=new RegExp(campo);
						$.each(query,function (i,e){
							if(test.test(e)){
								query.splice(i, 1);
							}
						});
					}
					else
						var query=new Array();
					query.push(campo+':'+value);
					query=query.join('|');
					window.location='?q='+query;
				}
			})
		});
		/****************
		*	funciones de renderizado
		****************************/
			var width=$(document).width();
			var height=$(window).height();
		
		$("#containerList").css({
			height:height-80
		});
		
		$(".containerListProperty").css({
			height:height-120
		});
		
		/****************
		*	carga puntos mapa principal
		****************************/
		$.ajax({
			url:"/result/getPointMap.php",
			type:'GET',
			dataType:"json",
			success: function(res){
					var arrLatLong=new Array();
					var statusText={
								'S':'Subject',
								'A-F':'Active Foreclosed',
								'A-F-S':'Active Foreclosed Sold',
								'A-P':'Active Pre-Foreclosed',
								'A-P-S':'Active Pre-Foreclosed Sold',
								'A-N':'Active',
								'CC-F':'By Owner Foreclosed',
								'CC-F-S':'By Owner Foreclosed Sold',
								'CC-P':'By Owner Pre-Foreclosed',
								'CC-P-S':'By Owner Pre-Foreclosed Sold',
								'CC-N':'By Owner',
								'CS-F':'Closed Sale Foreclosed',
								'CS-F-S':'Closed Sale Foreclosed Sold',								
								'CS-P':'Closed Sale Pre-Foreclosed',
								'CS-P-S':'Closed Sale Pre-Foreclosed Sold',
								'CS-N':'Closed Sale',
								'N-F':'Non-Active Foreclosed',
								'N-F-S':'Non-Active Foreclosed Sold',
								'N-P':'Non-Active Pre-Foreclosed',
								'N-P-S':'Non-Active Pre-Foreclosed Sold',
								'N-N':'Non-Active'
					}
					$.each(res.list,function (i,e){
							var cod=getPointColor(e.status,e.pendes,e.sold);
							var pointColor=lsHexCssPoint[cod];
							<?php
								if($type_search=='FO'){
									echo "
										e.address=e.address.replace(/\d*/,'');
										e.price=e.marketvalue;
									";
								}
							?>
							if(e.xcoded='Vacant Land'){
								e.beds=e.beds==0?'N/A':e.beds;
								e.bath=e.bath=='0.0'?'N/A':e.bath;
								e.sqft=e.sqft==0?'N/A':e.sqft;
							}
							e.price=(e.price=='' || e.price==0 || typeof e.price == 'undefined' )?e.marketvalue:e.price;
							containerMapPrincipal.addPushpinInfoboxMini(
								e.parcelid, 
								e.latitude,
								e.longitude,
								e.address,
								e.price,
								e.beds,
								e.bath,
								e.sqft,
								statusText[cod],
								e.url,
								pointColor
							);	
						});
					
					containerMapPrincipal.getCenterPins();
				},
			error: function (){
				
				}
		});
		
		/*****************
		*	seteando dimenciones dinamicas
		****************************
		****************************/
		
		
		//inicializando pmapa  
		var	containerMapPrincipal = new XimaMap('containerMapPrincipal','containerMapPrincipal_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_circle');
		
		
		containerMapPrincipal._IniMAP(<?php echo $list_reg[0]['latitude'].','.$list_reg[0]['longitude'];?>);
		
		$("#containerMapPrincipal,#containerMapPrincipal > div").css({
			width:width-($("#containerList").width()),
			height:height-80
		});
		
		//inicializando pmapa  
		$('#toolbarPagin').css({
			width:$("#containerList").width()
		});
		containerMapPrincipal.map.setOptions({ width: width-($("#containerList").width()), height: height-80 });
		
		
		/******************
		*
		funciones para la lista de propiedades
		*
		*****************************************/
		$('.result2').bind('mouseenter',function (){  //levantando puspin en el mapa
			var pin = containerMapPrincipal.getPushpin($(this).attr('data-parcel'));
    		if(pin !== false){
				containerMapPrincipal.pinMouseOver(pin);
			}
			var _selt=this;
			setTimeout(function (){
				var i=$(_selt).attr('data-parcel');
				if($('#miniMapInfobox'+i).length){
					var imgTemp = document.createElement('img');
					imgTemp.src = 'http://maps.googleapis.com/maps/api/staticmap?center='+$(_selt).attr('data-lat')+','+$(_selt).attr('data-log')+'&size=60x60&zoom=19&maptype=satellite&key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4';
					$('#miniMapInfobox'+i).html(imgTemp);
				}
			},700);
		}).each(function(index, element) {
			//redireccionar al click
            $(element).bind('click',function (e){
				window.location=$(this).find('a:eq(1)').attr('href');
        	});
        });
		 
		$(window).resize(function(e) {
			var width=$(window).width();
			var height=$(window).height();
			if(width>990){
				$("#containerList").css({
					height:height-80
				});
				$(".containerListProperty").css({
					height:height-120
				});
		
				$('#toolbarPagin').css({
					width:$("#containerList").width()
				});
				$("#containerMapPrincipal,#containerMapPrincipal > div").css({
					width:width-($("#containerList").width()),
					height:height-80
				});
				containerMapPrincipal.map.setOptions({ width: width-($("#containerList").width()), height: height-80 });
			}
			else{
				$("#containerList").css({
					height:height-80
				});
				$(".containerListProperty").css({
					height:height-120
				});
		
				$("#containerMapPrincipal,#containerMapPrincipal > div").css({
					height:height-80
				});
				containerMapPrincipal.map.setOptions({ height: height-80 });
			}
        }); 
		loadCounty();
		$('#buttonSearch').bind('click',initSearch);
		$('#changePageTo').chosen({disable_search_threshold: 20}).change(function (){
			window.location=$(this).val();
		});
		$('#orderByTo').attr('value',(getUrlVars()['order'])?getUrlVars()['order'].replace(/(-)/gi,' '):'').chosen().change(ordenar);
		$('.botonOrdenamiento').bind('click',ordenar); 
		var prevPageHeight = 0;

/*
		function setHeight() {
			$('#container').height( window.innerHeight );
			prevPageHeight = window.innerHeight;
		}
		
		setInterval( function() {
		  if ( window.innerHeight != prevPageHeight ) {
			setHeight();
		  }
		}, 500);
		
		setHeight(); 
		*/
	});
	
	function loadCounty(){
		$('.reports .msgLoad').fadeTo(200,0.5);
		$.ajax({
			type	:'POST',
			url		:'/resources/php/properties.php',
			data	: "ejecutar=countylist&state=1",
			dataType:'json',
			success	:function (resul){
				$('.County,#SearchCounty,#countySearch').html('').append('<option value="">Select a county</option>');
				$(resul).each(function (index,data){
					$('.County,#SearchCounty,#countySearch').append('<option value="'+data.id+'">'+data.county+'</option>');
				});
				$('#countySearch').chosen();
			}
		})
	};
function ordenar()
{
	var order=$('#orderByTo').val();
	var dir=($(this).attr('rel'))?$(this).attr('rel'):'ASC';
	
	$.ajax({
		url: '../../properties_order_by_result.php', 
		type: 'POST',
		data: 'order='+order+' '+dir,
		success: function ()
		{
			window.location='?page=0&order='+order.replace(/(\s)/gi,'-');
		}
	})
}
$.cookie('urlResult',window.location,{path:'/'});
function initSearch(e)
{
	e.preventDefault();
	$('body').fadeOut(200);
	var parametrosEnviar='';
	var parametros=[
		{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
		{campo: 'county',	valor : ($('#countySearch').val())?$('#countySearch').val():''},
		{campo: 'tsearch', 	valor : 'location'},
		{campo: 'proptype', 	valor : ''},
		{campo: 'price_low', 	valor : ''},
		{campo: 'price_hi', 	valor : ''},
		{campo: 'bed', 	valor : -1},
		{campo: 'bath', 	valor : -1},
		{campo: 'sqft', 	valor : -1},
		{campo: 'pequity', 	valor : -1},
		{campo: 'pendes', 	valor : -1},
		{campo: 'search_mapa', 	valor : -1},
		{campo: 'search_type', 	valor : ($('#typeSerach').val())?$('#typeSerach').val():''},
		{campo: 'search_mapa', 	valor : '-1'},
		{campo: 'occupied', 	valor : -1}
	]
	$(parametros).each(function (index){
		parametrosEnviar+=this.campo+'='+this.valor+'&';
		});
	$.ajax({
		url		:'/properties_coresearch.php',
		type	:'POST',
		data	:parametrosEnviar,
		success	:function (resul){
			window.location='/result/indexAngela.php';
			}
	});
	
}
		
</script>