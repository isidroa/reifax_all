<?php
	require_once('../resources/template/template.php');
	$_GET['resultType'] = 'basic';
	$_GET['systemsearch'] = 'basic';
	set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot\\'. PATH_SEPARATOR);
	include('coresearch.php');
	//Variables for Paging
	$num_rows=$limit_cant_reg;
	$max_page_pagin=10;
	$num_page_pagin=$num_rows>0?ceil($num_rows_all/$num_rows):0;
	if($max_page_pagin>$num_page_pagin) $max_page_pagin=$num_page_pagin;
	$star_for=$max_page_pagin>0?floor($num_limit_page/$max_page_pagin)*$max_page_pagin:0;
	$end_for=$star_for+$max_page_pagin;
	if($end_for>$num_page_pagin) $end_for=$num_page_pagin;
	$star_page=($num_limit_page*$limit_cant_reg)+1;
	$tablaporder='psummary';
		if($type_search=='PR' || $type_search=='FO' || $type_search=='MO') $tablaporder='psummary';
		elseif($type_search=='FS') $tablaporder='mlsresidential';
		elseif($type_search=='FR'){	$tablaporder='rental';	}
	setcookie('type_search',$type_search,0,'/','reifax.com');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(1);?>
<style>
	.botonOrdenamiento{
		width:24px;
		height:24px;
		display:block;
		border: 1px solid #AAAAAA;
	}
	.botonOrdenamiento:hover{
		border: 1px solid #666666;
		background-color:#FFF;
		-moz-box-shadow: 1px 1px 2px #666666;
		-webkit-box-shadow: 1px 1px 2px #666666;
	}
	#result_orderby_asc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-asc.gif) center no-repeat;
    	border-radius: 4px 0 0 4px;
	}
	#result_orderby_desc{
		background: url(http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/hmenu-desc.gif) center no-repeat;
    	border-radius: 0 4px 4px 0;
	}
	.chzn-container-single .chzn-single {
		border-radius: 0;
		height: 27px;
	}
	.result2Option{
		height:auto;
	}
	.container{
		width:100%;
	}
	.content{
		margin-right:0px;
		overflow: scroll;
	}
</style>

<body>
<div class="backMenu"></div>
<div class="backMenu2"></div>
<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeaderSearch();?>
	
    <!--ReiFax "You are here" Text  -->
    <div style="clear:both"></div>
    <div id="contentPrincipal" style="width:100%;">
  	<!-- Advertising Right Block-->
    <!-- Center Content-->
    <div style="float:left; width:50%; height:1080px">
    <div id="map_result" class="img" style="width:100%; height:100%; overflow:hidden; position:relative; float:left">
    </div>
    </div>
    <div class="content" style="float:left; width:50%;">
    
        
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
                         <span style="font-size:18px" class="title bold bluetext">
                            Properties Found: <?php echo $num_rows_all ?>
                         </span>    
            	</div>
        	</div>
        </div>
    	<div class="result2Option">
			 <div class="panes reports">
                    	<div id="search1" style="display:block" class="title">
                        	<form id="formSearch">
                            <input type="hidden" name="typeSearch">
                                <div class="contentText">
                                	<label class="whitetext">
                                            Property Type 
                                     </label><br>
                                            <select name="propertySearch" class="fieldtext" style="width:146px">	
                                                    <option value="">Any Property Type</option>
                                                    <option value="01">Single Family</option>
                                                    <option value="04">Condo/Town/Villa</option>
                                                    <option value="03">Multi Family +10</option>
                                                    <option value="08">Multi Family -10</option>
                                                    <option value="11">Commercial</option>
                                                    <option value="00">Vacant Land</option>
                                                    <option value="02">Mobile Home</option>
                                                    <option value="99">Other</option>
                                            </select>
                                </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Sqft 
                                            </label><br>
                                            <select class="fieldtext" name="sqftSearch" style="width:146px">
                                            	<option value="-1">Any</option>
                                                <option value="250" >250+</option>
                                                <option value="500" >500+</option>
                                                <option value="1000" >1,000+</option>
                                                <option value="1250" >1,250+</option>
                                                <option value="1500" >1,500+</option>
                                                <option value="1750" >1,750+</option>
                                                <option value="2000" >2,000+</option>
                                                <option value="2250">2,250+</option>
                                                <option value="2500">2,500+</option>
                                                <option value="2750" >2,750+</option>
                                                <option value="3000" >3,000+</option>
                                                <option value="3250" >3,250+</option>
                                                <option value="3500" >3,500+</option>
                                                <option value="3750" >3,750+</option>
                                                <option value="4000" >4,000+</option>
                                                <option value="5000" >5,000+</option>
                                                <option value="10000" >10,000+</option>
                                            </select>
                                </div>
                            <div> 
                                	<div class="contentText">
                                    <label class="whitetext">Sold Range</label><br>
                                    <input class="fieldtext" placeholder="$ Min" name="MinSearch" style="width:146px">
                                    <input class="fieldtext" placeholder="$ Max" name="MaxSearch" style="width:146px">
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Beds</label><br>
                                    <select class="fieldtext" name="bedsSearch" style="width:146px;height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Baths</label><br>
                                    <select class="fieldtext" name="bathSearch" style="width:146px; height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                            </form>
                            </div>
                            </div>
                            </div>
		
		
		
       <!--Text container-->
       <?php
			if($num_rows_all==0)
			{
				echo '
					<div class="centertext">
						<div class="clear"></div>
						<h2>Property Result: 0 (Please review your search and try again)</h2>
					</div>
					
				';
			}
			else
			{
				?>
		<div class="result2Option">
			<div class="">
				 <div style="position:absolute; top:1px; left:10px;">
					<div class="botonDesplazamiento">
						<span class="bluetext">Order By:</span>
					</div>
					<div class="botonDesplazamiento">
						<select id="orderByTo" style="width:120px; height: 22px;">
							<option value="ORDER BY p.county,p.address">County</option>
							<option value="ORDER BY p.address">Address</option>
							<option value="ORDER BY mlnumber">Mlnumber</option>
							<option value="ORDER BY p.parcelid">Parcel ID</option>
							<option value="ORDER BY <?php echo $tablaporder ?>.yrbuilt">Year Built</option>
							<option value="ORDER BY dom">DOM</option>
							<option value="ORDER BY larea">Living Area</option>
							<option value="ORDER BY garea">Gross Area</option>
							<option value="ORDER BY tsqft">Lot Size</option>
							<option value="ORDER BY waterf">Water Front</option>
							<option value="ORDER BY pool">Pool</option>
							<option value="ORDER BY <?php echo $tablaporder ?>.beds">Beds</option>
							<option value="ORDER BY <?php echo $tablaporder ?>.bath">Baths</option>
							<option value="ORDER BY p.city">City</option>
							<option value="ORDER BY p.zip">Zip Code</option>
							<option value="ORDER BY price">Price</option>
							<option value="ORDER BY p.xcoded">Property Types</option>
						</select>
					</div>
					<div class="botonDesplazamiento">
						<a href="#" class="botonOrdenamiento" rel='ASC' id="result_orderby_asc">&nbsp;
						</a>
					</div>
					<div class="botonDesplazamiento">
						<a href="#" class="botonOrdenamiento" rel='DESC' id="result_orderby_desc">&nbsp;
						</a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
       
       	<?php 
			}
			$i=($num_limit_page*10)+1;
			foreach($list_reg as $key => $val)
			{
				echo '
				<div class="result2">
					<div class="title"> <span class="price">$'.number_format($val['price'],0,'.',',').'</span> '.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].' <span class="Num">'.$i.'</span></div>
					<div class="view">
						<div id="map_result_'.$key.'" class="img" style="width:135px; height:135px; overflow:hidden; position:relative;">
						</div>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>City:</td>
								<td class="">'.$val['city'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Parcel ID:</td>
								<td class="">'.$val['parcelid'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Type:</td>
								<td class="">'.substr($val['xcoded'],0,25).'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Beds/Baths:</td>
								<td class="">'.$val['beds'].'/'.$val['bath'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Year Built:</td>
								<td class="">'.$val['yrbuilt'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Dom:</td>
								<td class="">'.$val['dom'].'</td>
							</tr>
						</table>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>County:</td>
								<td class="">'.$val['county'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Living Area:</td>
								<td class="">'.$val['larea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Gross Area:</td>
								<td class="">'.$val['garea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Lot Size:</td>
								<td class="">'.$val['tsqft'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Water Front:</td>
								<td class="">'.$val['waterf'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Pool:</td>
								<td class="">'.$val['pool'].'</td>
							</tr>
						</table>
					</div>
						<a href="#" rel="'.$val['parcelid'].'|'.$val['county'].'" class="buttonblue">View Details</a>
					<div class="clearEmpty"></div>
				</div>
					<div class="clearEmpty"></div>
				';
			$i++;
			}
		?>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <!--
    <?php //ReiFaxFooter();?>
//-->
</div>

</body>

</html>
<script src="../resources/js/chosen.jquery.min.js"></script> 
<script src="/includes/properties_draw.js"></script>
<script language="javascript">
<?php if(count($list_reg)>0){?>

			var	map_result = new XimaMap('map_result','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_circle');
			map_result._IniMAPResult(<?php echo $list_reg[0]['latitude'].','.$list_reg[0]['longitude'];?>);
			
		<?php foreach($list_reg as $k => $val){	?>
			
			var	map_result_<?php echo $k;?> = new XimaMap('map_result_<?php echo $k;?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_circle');
			map_result_<?php echo $k;?>._IniMAPResult(<?php echo $val['latitude'].','.$val['longitude'];?>);
			
			getCasita(<?php echo '"'.$val['status'].'","'.$val['pendes'].'","'.$val['sold'].'"';?>);
			
			
			map_result_<?php echo $k;?>.addPushpin(
				<?php echo $val['latitude'].','.$val['longitude'];?>,
				'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
				'<?php echo ($star_page+$k);?>'
			);			
			
		<?php }?>
		
<?php }?>
	$(document).ready(function (){
		var height=$(window).height();
		var width=$(window).width();
		$('#contentPrincipal > div').css({
				height	:height-100,
				width	:width/2
		});
		loadSearch($('#Search'));
		loadCounty();
		//$('.County, .PR_proptype').bind('change',refreshData);
		/*
		$('#buttonSearch').bind('click',initSearch);
		
		$('.result2').bind('mouseenter',function (){
			
			$(this).find('.buttonblue').slideDown(100).fadeIn(100);
		}).bind('mouseleave',function (){
			$(this).find('.buttonblue').slideUp(100).fadeOut(100)
		}).find('.buttonblue').bind('click',function (e){
			
			e.preventDefault();
			var data=$(this).attr('rel').split('|');
			$.cookie('parselid',data[0], { path: '/' });
			$.cookie('county',data[1], { path: '/' });
			window.location='../overview/index.php';
		});
		
		
		$('#changePageTo').chosen().change(function (){
			window.location=$(this).val();
		});
		$('#orderByTo').attr('value',(getUrlVars()['order'])?getUrlVars()['order'].replace(/(-)/gi,' '):'').chosen().change(ordenar);
		$('.botonOrdenamiento').bind('click',ordenar);*/
	});
	
	function loadCounty(){
		$('.reports .msgLoad').fadeTo(200,0.5);
		$.ajax({
			type	:'POST',
			url		:'/resources/php/properties.php',
			data	: "ejecutar=countylist&state=1",
			dataType:'json',
			success	:function (resul){
				$('.County,#SearchCounty,#countySearch').html('').append('<option value="">Select a county</option>');
				$(resul).each(function (index,data){
					$('.County,#SearchCounty,#countySearch').append('<option value="'+data.id+'">'+data.county+'</option>');
					
				});
				$('#countySearch').chosen();
				if(window.loadInt){
					//$('.reports .msgLoad').fadeOut(200);
				}
				else{
					//IniCounty.init($('#County'),triggerIni);
				};
			}
		})
	};
function ordenar()
{
	var order=$('#orderByTo').val();
	var dir=($(this).attr('rel'))?$(this).attr('rel'):'ASC';
	
	$.ajax({
		url: '../../properties_order_by_result.php', 
		type: 'POST',
		data: 'order='+order+' '+dir,
		success: function ()
		{
			window.location='?page=0&order='+order.replace(/(\s)/gi,'-');
		}
	})
}
$.cookie('urlResult',window.location,{path:'/'});
if($.cookie('type_search')=='FO')
{
	$('.result2 .title').each(function (index){
		var aux=parseInt($(this).html().split('</span>')[1]);
		$(this).html($(this).html().replace(aux,''));		
	})
	$('.result2 table td:contains("Parcel ID:")').each(function (index){
		//console.debug($(this).html());
		$(this).next().html('Restricted Information')
	})
}
	
	function initSearch(e)
	{
		e.preventDefault();
		$('body').fadeOut(200);
		var parametrosEnviar='';
		var parametros=[
			{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
			{campo: 'county',	valor : ($('select[name=countySearch]').val())?$('select[name=countySearch]').val():''},
			{campo: 'tsearch', 	valor : 'location'},
			{campo: 'proptype', 	valor : ($('select[name=propertySearch]').val())?$('select[name=propertySearch]').val():''},
			{campo: 'price_low', 	valor : ($('input[name=MinSearch]').val())?$('input[name=MinSearch]').val():''},
			{campo: 'price_hi', 	valor : ($('input[name=MaxSearch]').val())?$('input[name=MaxSearch]').val():''},
			{campo: 'bed', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'bath', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'sqft', 	valor : ($('select[name=sqftSearch]').val())?$('select[name=sqftSearch]').val():-1},
			{campo: 'pequity', 	valor : -1},
			{campo: 'pendes', 	valor : -1},
			{campo: 'search_mapa', 	valor : -1},
			{campo: 'search_type', 	valor : ($('input[name=typeSearch]').val())?$('input[name=typeSearch]').val():''},
			{campo: 'search_mapa', 	valor : '-1'},
			{campo: 'occupied', 	valor : -1}
		]
		$(parametros).each(function (index){
			parametrosEnviar+=this.campo+'='+this.valor+'&';
			});
		$.ajax({
			url		:'/properties_coresearch.php',
			type	:'POST',
			data	:parametrosEnviar,
			success	:function (resul){
				window.location='result/index.php';
				}
		});
		
	}
		
</script>