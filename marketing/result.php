<?php
 $sufijo = explode('.',$_SERVER['SERVER_NAME']);
 if( !($sufijo[0]=='www' || $sufijo[0]=='reifax') ){
	 header('Location: http://'.$_SERVER['SERVER_NAME'].'/properties_search.php');
 }
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<head>
        <meta charset="utf-8">
        <title>Index</title>
        <meta name="robots" content="all">
        <meta name="rating" content="General">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="2 days">
        <meta name="category" content="Real Estate Search">
        <meta name="author" content="REIFAX">
        <meta name="reply-to" content="support@reifax.com">
        <meta name="copyright" content="REIFAX - 2011">
        <link rel="stylesheet" type="text/css" href="/resources/css/css.css"/> 
        <link rel="stylesheet" type="text/css" href="/resources/css/chosen.css"/> 
        <link rel="stylesheet" type="text/css" href="/resources/css/menu2.css"/> 
        <link rel="stylesheet" type="text/css" href="/resources/css/advertising.css"/> 
        <link rel="stylesheet" type="text/css" href="/resources/css/jquery.cleditor.css"/> 
        <script type="text/javascript" language="javascript" src="/includes/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="/resources/js/cookies.js"></script>
        <script type="text/javascript" language="javascript" src="/resources/js/jquery.upload-1.0.2.min.js"></script>
        <script type="text/javascript" language="javascript" src="/resources/js/ubicacion.js"></script>
        <script type="text/javascript" language="javascript" src="/resources/js/jquery.cleditor.min.js"></script>
		<script type="text/javascript" language="javascript" src="/resources/js/flowplayer-3.2.6.min.js"></script>
        <script type="text/javascript" language="javascript" src="/resources/js/functionGlobal.js"></script> 
        <link rel="shortcut icon" type="image/ico" href="/favicon.png">
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-29726343-1', 'auto');
		  ga('send', 'pageview');

		</script>
</head>
<body>

<div class="container">
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                <div id="advertisingFramework">
                </div>
        </div>
        <!-- Center Content-->
        <div class="content" style="min-height:1300px;">
            <!-- Advertising Rotative Top-->
            <!--
            		INICIO DEL SEARCH
            //-->
                    <div>
                        <ul class="css-tabs small mediunTabs" id="Search">
                            <li>
                                <a class="current" href="#PR">Public Records</a>
                            </li>
                            <li>
                                <a class="" href="#FO">Foreclosure</a>
                            </li>
                            <li>
                                <a class="" href="#FS">For Sale</a>
                            </li>
                            <li>
                                <a class="" href="#FR">For Rent</a>
                            </li>
                            <!--
							<li>
                                <a class="" href="#BO">By Owner Sale</a>
                            </li>
                            <li>
                                <a class="" href="#BOR">By Owner Rent</a>
                            </li>
							-->
                        </ul>
                    </div>
                    <div class="panes reports">
                    	<div id="search1" style="display:block" class="title">
                        	<form id="formSearch">
                            <input type="hidden" name="typeSearch">
                        	<div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            State
                                            </label><br>
                                            <select class="fieldtext" name="stadeSearch" style="width:146px">
                                            <?php
                                            conectar('xima');
                                                $sql="SELECT * FROM lsstate where is_showed='Y';";
                                                $resultado=mysql_query($sql);
                                                while($data=mysql_fetch_assoc($resultado))
                                                {
                                                    echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                                }
                                            ?>
                                            </select>
                                </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            County
                                            </label><br>
                                            <select id="SearchCounty" name="countySearch" class="fieldtext" style="width:146px">
                                            </select>
                              	</div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Property Type 
                                            </label><br>
                                            <select name="propertySearch" class="fieldtext" style="width:146px">	
                                                    <option value="">Any Property Type</option>
                                                    <option value="01">Single Family</option>
                                                    <option value="04">Condo/Town/Villa</option>
                                                    <option value="03">Multi Family +10</option>
                                                    <option value="08">Multi Family -10</option>
                                                    <option value="11">Commercial</option>
                                                    <option value="00">Vacant Land</option>
                                                    <option value="02">Mobile Home</option>
                                                    <option value="99">Other</option>
                                            </select>
                                        </label>
                                </div>
                                <div class="contentText">
                                	<a href="#" target="_blank" id="buttonSearch" class="buttonblue bigButton">Search</a>
                                </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                            <div class="contentText">
                                	<label class="whitetext">
                                    	Location
                                   	</label><br>
                                    <input style="width:463px;" name="locationSearch" placeholder="Address, City or Zip Code" class="fieldtext" type="text">
                            </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Sqft 
                                            </label><br>
                                            <select class="fieldtext" name="sqftSearch" style="width:146px">
                                            	<option value="-1">Any</option>
                                                <option value="250" >250+</option>
                                                <option value="500" >500+</option>
                                                <option value="1000" >1,000+</option>
                                                <option value="1250" >1,250+</option>
                                                <option value="1500" >1,500+</option>
                                                <option value="1750" >1,750+</option>
                                                <option value="2000" >2,000+</option>
                                                <option value="2250">2,250+</option>
                                                <option value="2500">2,500+</option>
                                                <option value="2750" >2,750+</option>
                                                <option value="3000" >3,000+</option>
                                                <option value="3250" >3,250+</option>
                                                <option value="3500" >3,500+</option>
                                                <option value="3750" >3,750+</option>
                                                <option value="4000" >4,000+</option>
                                                <option value="5000" >5,000+</option>
                                                <option value="10000" >10,000+</option>
                                            </select>
                                        </label>
                                </div>
            				<div class="clear"></div>
                            <div>
                                	<div class="contentText">
                                    <label class="whitetext">Sold Range</label><br>
                                    <input class="fieldtext" placeholder="$ Min" name="MinSearch" style="width:146px">
                                    <input class="fieldtext" placeholder="$ Max" name="MaxSearch" style="width:146px">
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Beds</label><br>
                                    <select class="fieldtext" name="bedsSearch" style="width:146px;height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Baths</label><br>
                                    <select class="fieldtext" name="bathSearch" style="width:146px; height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                        </div>
                        </div>
                        </form>
                        <!-- fin de los tab//-->                     
        </div>
                    
        <div class="clear">&nbsp;</div> 
    </div>

</div>

</body>

</html>
<script type="text/javascript">
	function loadCounty(){
		$('.reports .msgLoad').fadeTo(200,0.5);
		console.debug("ejecutar=countylist&state="+$('#State').val());
		$.ajax({
			type	:'POST',
			url		:'/resources/php/properties.php',
			data	: "ejecutar=countylist&state="+$('#State').val(),
			dataType:'json',
			success	:function (resul){
				$('.County,#SearchCounty').html('');
				$(resul).each(function (index,data){
					$('.County,#SearchCounty').append('<option value="'+data.id+'">'+data.county+'</option>');
				});
				if(window.loadInt){
				$('.reports .msgLoad').fadeOut(200);
				}
				else{
					IniCounty.init($('#County'),triggerIni);
				};
			}
		})
	};
	function  punticos(valor)
	{
		var punto=-1;
		var respuesta='';
		for(i=0;i<valor.length+1;i++)
		{
			if(punto==3)
			{
				respuesta=valor.charAt(valor.length - i)+','+respuesta;
				punto=1;
			}
			else
			{
				respuesta=valor.charAt(valor.length - i)+respuesta;
				punto++;
			}
		}
		return respuesta;
	}
	var mes=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
	$(document).ready(function (){ 
		loadCounty();
		$('.County, .PR_proptype').bind('change',refreshData);
		$('#buttonSearch').bind('click',initSearch);
	});
	function initSearch(e)
	{
		e.preventDefault();
		$('body').fadeOut(200);
		var parametrosEnviar='';
		var parametros=[
			{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
			{campo: 'county',	valor : ($('select[name=countySearch]').val())?$('select[name=countySearch]').val():''},
			{campo: 'tsearch', 	valor : 'location'},
			{campo: 'proptype', 	valor : ($('select[name=propertySearch]').val())?$('select[name=propertySearch]').val():''},
			{campo: 'price_low', 	valor : ($('input[name=MinSearch]').val())?$('input[name=MinSearch]').val():''},
			{campo: 'price_hi', 	valor : ($('input[name=MaxSearch]').val())?$('input[name=MaxSearch]').val():''},
			{campo: 'bed', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'bath', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'sqft', 	valor : ($('select[name=sqftSearch]').val())?$('select[name=sqftSearch]').val():-1},
			{campo: 'pequity', 	valor : -1},
			{campo: 'pendes', 	valor : -1},
			{campo: 'search_mapa', 	valor : -1},
			{campo: 'search_type', 	valor : ($('input[name=typeSearch]').val())?$('input[name=typeSearch]').val():''},
			{campo: 'search_mapa', 	valor : '-1'},
			{campo: 'occupied', 	valor : -1}
		]
		$(parametros).each(function (index){
			parametrosEnviar+=this.campo+'='+this.valor+'&';
			});
		$.ajax({
			url		:'/properties_coresearch.php',
			type	:'POST',
			data	:parametrosEnviar,
			success	:function (resul){
				window.location='result/index.php';
				}
		});
		
	}
	function  refreshData(){
			if($(this).is('.County'))
			{
				$('.County').attr('value',$(this).val());
			}
			else
			{
				$('.PR_proptype').attr('value',$(this).val());
			}	
			$('.selectProptype').html($('#PR_proptype option:selected').html());
			$('.selectCounty').html($('#County option:selected').html());
			$('.selectState').html($('#State option:selected').html());
					window.loadInt=true;
					$('.reports .msgLoad').fadeTo(200,0.5);
					$.ajax({
						type	:'POST', 
						url		:'/resources/php/properties.php',
						data	: "ejecutar=propertieslist&county="+$('#County').val()+'&propType='+$('#PR_proptype').val(),
						dataType:'json',
						success	:function (resul)
								{
									var fecha=resul.fechaEray.split(' ')[0].split('-');
									fecha=mes[fecha[1]-1]+' '+fecha[2]+', '+fecha[0];
									$('#fechaEray').html(fecha);
									fecha=resul.fechaDisc.split(' ')[0].split('-');
									fecha=mes[fecha[1]-1]+' '+fecha[2]+', '+fecha[0];
									$('#fechaDisc').html(fecha);
									$(resul.data).each(function (index,data){
											var valor=data.valor.split('.');
											$('#'+data.campo).html((data.campo.substring(0,2)=='pc')? number_format(data.valor,2):punticos(valor[0]));
										})
										$('.total').html('100.00');
										$('.reports .msgLoad').fadeOut(200);
								}
						})
			}
		function loadAdvertising (){
			$.ajax({
				url		:'/resources/php/advertiser.php',
				type	:'POST',
				data	:'option=list&county='+$('#County').val(),
				dataType:'json',
				success	:function (respuesta){
					var respuesta=(respuesta)?respuesta:new Array();
					var interna=0;
					if($('#County').val()==2){
						var adv_default={
							1:{
								url	:'//transactionalfundingfl.com/how-transactional-funding-works-for-real-estate-investors/',
								img	:'//www.reifax.com/resources/img/advertise/publicidad_xy-1.png'
								},
							2:{
								url	:'//www.reifax.com/register/ddRegister.php',
								img	:'//www.reifax.com/resources/img/advertise/D8.png'
								},
							3:{
								url	:'//www.reifax.com/company/become.php#',
								img	:'//www.reifax.com/resources/img/advertise/D2.png'
								},
							4:{
								url	:'//messenger.providesupport.com/messenger/ximausa.html#" target="_blank"',
								img	:'//www.reifax.com/resources/img/advertise/D3.png'
								},
							5:{
								url	:'//www.reifax.com/company/contactUs.php#',
								img	:'//www.reifax.com/resources/img/advertise/D6.png'
								},
							6:{
								url	:'//www.reifax.com/training/overviewTraining.php#',
								img	:'//www.reifax.com/resources/img/advertise/D5.png'
								},
							7:{
								url	:'#',
								img	:'#'
								}
						}
					}else{
						var adv_default={
							1:{
								url	:'//www.reifax.com/company/become.php#',
								img	:'//www.reifax.com/resources/img/advertise/D2.png'
								},
							2:{
								url	:'//messenger.providesupport.com/messenger/ximausa.html#" target="_blank"',
								img	:'//www.reifax.com/resources/img/advertise/D3.png'
								},
							3:{
								url	:'//www.reifax.com/company/contactUs.php#',
								img	:'//www.reifax.com/resources/img/advertise/D6.png'
								},
							4:{
								url	:'//www.reifax.com/training/overviewTraining.php#',
								img	:'//www.reifax.com/resources/img/advertise/D5.png'
								},
							5:{
								url	:'#',
								img	:'#'
								},
							6:{
								url	:'#',
								img	:'#'
								},
							7:{
								url	:'#',
								img	:'#'
								}
						}
					}
					
					
					for(i=1;i<8;i++)
					{
						if(respuesta[i])
						{
							$('#advertisingFramework').append('<a class="yourAdHere" target="_blank" style="background:url('+respuesta[i].img+') center no-repeat;" href="'+respuesta[i].url+'"></a>');
						}
						else
						{
							interna++;
							$('#advertisingFramework').append('<a class="yourAdHere" style="background:url('+adv_default[interna].img+') center no-repeat;" href="'+adv_default[interna].url+'"></a>');
						}
					}
					
				}
				
			})	
		}
		function triggerIni()
		{
			$('.County,#SearchCounty').attr('value',$('.County:eq(0)').val());
			$('.PR_proptype').attr('value',$('.PR_proptype:eq(0)').val());
			refreshData();
			loadAdvertising();
		}
</script>