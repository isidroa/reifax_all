<?php
/**
 * savemailsett.php
 *
 * Save the mail server settings
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 11.07.2011
 */

include "../../properties_conexion.php"; 
conectar();

$userid = $_COOKIE['datos_usr']["USERID"];

mysql_query("DELETE FROM xima.contracts_mailsettings WHERE userid = $userid");

/*$into   = "userid, server, port, username, password, 
 		   imap_server, imap_port, imap_username, imap_password";*/
$into   = "userid, server, port, username, password";

/*$values = "'".$userid."','".addslashes($_POST['server'])."','{$_POST['port']}',
					'{$_POST['username']}','{$_POST['password']}', 
		   '".addslashes($_POST['imap_server'])."','{$_POST['imap_port']}',
					'{$_POST['imap_username']}','{$_POST['imap_password']}'";*/
$values = "'".$userid."','".addslashes($_POST['server'])."','{$_POST['port']}',
					'{$_POST['username']}','{$_POST['password']}'";
					
$query  = "INSERT INTO xima.contracts_mailsettings ($into) VALUES ($values)";

if (mysql_query($query))
	$resp = array('success'=>'true','savesett'=>'yes','mensaje'=>'Settings saved!');
else
	$resp = array('success'=>'true','savesett'=>'no','mensaje'=>$query.' '.mysql_error());
					
echo json_encode($resp);

?>	