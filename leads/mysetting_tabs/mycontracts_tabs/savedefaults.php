<?php
/**
 * savedefaults.php
 *
 * Save the options of the templates for contracts.
 * 
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *			Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and  fixes
 * @version 04.04.2011
 */

include "../../properties_conexion.php"; 

conectar();

/*
 * alexbariv: Function pending for delete.
 * Replaced by strip_tags and addslashes below.
 *
function limpiarcaracter($cad) {
	$cad = str_replace("\n","<BR>",$cad);
	$cad = str_replace("<BR>"," ",$cad);
	$cad = str_replace("'","",$cad);
	$cad = str_replace("\'","",$cad);
	$cad = str_replace('"',"",$cad);
	$cad = str_replace('\"',"",$cad);
	return $cad;
}
*/

$userid        = $_COOKIE['datos_usr']["USERID"];

$contratos     = Array('ContractPurchase','leadBasedPaint','ResidentialContract');

$contract      = $_POST['type'];

$id1           = $_POST['id1'];
$option1       = $_POST['option1'];
$name1         = addslashes(strip_tags($_POST['name1']));
$address1      = addslashes(strip_tags($_POST['address1']));
$actitemplate1 = $_POST['activetemplate1'];

$id2           = $_POST['id2']; 
$option2       = $_POST['option2'];
$name2         = addslashes(strip_tags($_POST['name2']));
$address2      = addslashes(strip_tags($_POST['address2']));
$actitemplate2 = $_POST['activetemplate2'];

$namepdf1      = $contratos[$type].$userid.'1.pdf';
$namepdf2      = $contratos[$type].$userid.'2.pdf';

if ( strlen($id1)==0 && strlen($id2)==0 ) {
	
	$query  = "SELECT id FROM xima.contracts_default c 
				WHERE contract='".$contract."' AND userid=".$userid." ORDER BY id";
	$result = mysql_query($query) or die($query.mysql_error());
	$total  = mysql_num_rows($result);
	if($total>0){
		$arrayids = Array();
		while($r = mysql_fetch_array($result)){
			$arrayids[] = $r['id'];
		}
		$id1 = $arrayids[0];
		$id2 = $arrayids[1];
	}
}

$error = '';

// FIX ONLY ONE TEMPLATE SELECTED
/*
$update = "UPDATE xima.contracts_default 
			SET defaultoption='N',activetemplate='N',template='N' 
			WHERE userid=".$userid;		
$res    = mysql_query($update) or die("Error: ".$update." ".mysql_error());
*/


/*
 *
 *  Upload and save TEMPLATE 1
 *  
 */

if($option1=='on')
	$default='Y';
else
	$default='N';	

if($actitemplate1=='on')
	$defaulttem='Y';
else
	$defaulttem='N';	

$nameFile  = $_FILES['pdf1']['name'];
$extension = explode(".",$nameFile);
$aux       = count($extension)-1; // If the name of the file has dotts, the last is the extension

if (strlen($_FILES['pdf1']['tmp_name'])>0) {

	if (file_exists(getcwd().'/template_upload/'.$namepdf1))
		@unlink(getcwd().'/template_upload/'.$namepdf1); 
	
	if (move_uploaded_file ($_FILES['pdf1']['tmp_name'],getcwd().'/template_upload/'.$namepdf1))
		$subio = 'Y';
	else
		$subio = 'N';
		
} else {
	
	if(file_exists(getcwd().'/template_upload/'.$namepdf1))
		$subio = 'Y';
	else
		$subio = 'N';
	
}
if ( strlen($id1) == 0 ) {
	
	$error .= ' '.$_FILES['pdf1']['error'];;
	$insert = "insert into xima.contracts_default (name, address, contract, `defaultoption`, userid, template, activetemplate) 
				values ('".$name1."','".$address1."','".$contract."','".$default."',".$userid.",'".$subio."','".$defaulttem."')";
	$res    = mysql_query($insert) or die("Error: ".mysql_error());		
	
} else {
		
	$error  .= ' '.$_FILES['pdf1']['error'];;
	$update  = "UPDATE xima.contracts_default SET 
					name='".$name1."', address='".$address1."',
					`defaultoption`='".$default."',activetemplate='".$defaulttem."',
					template='".$subio."' 
				WHERE id=".$id1." AND userid=".$userid;
				
	$res    = mysql_query($update) or die("Error: ".$update." ".mysql_error());
	
}






/*
 *
 *  Upload and save TEMPLATE 2
 *  
 */

if($option2=='on')
	$default='Y';
else
	$default='N';	

if($actitemplate2=='on')
	$defaulttem='Y';
else
	$defaulttem='N';
	
$nameFile  = $_FILES['pdf2']['name'];
$extension = explode(".",$nameFile);
$aux       = count($extension)-1; // If the name of the file has dotts, the last is the extension

if (strlen($_FILES['pdf2']['tmp_name'])>0) {

	if (file_exists(getcwd().'/template_upload/'.$namepdf2))
		@unlink(getcwd().'/template_upload/'.$namepdf2); 
	
	if (move_uploaded_file ($_FILES['pdf2']['tmp_name'],getcwd().'/template_upload/'.$namepdf2))
		$subio = 'Y';
	else
		$subio = 'N';
		
} else {
	
	if(file_exists(getcwd().'/template_upload/'.$namepdf2))
		$subio = 'Y';
	else
		$subio = 'N';
	
}
if ( strlen($id2) == 0 ) {
	
	$error .= ' '.$_FILES['pdf2']['error'];;
	$insert = "insert into xima.contracts_default (name, address, contract, `defaultoption`, userid, template, activetemplate) 
				values ('".$name2."','".$address2."','".$contract."','".$default."',".$userid.",'".$subio."','".$defaulttem."')";
	$res    = mysql_query($insert) or die("Error: ".mysql_error());		
	
} else {
		
	$error  .= ' '.$_FILES['pdf2']['error'];;
	$update  = "UPDATE xima.contracts_default SET 
					name='".$name2."', address='".$address2."',
					`defaultoption`='".$default."',activetemplate='".$defaulttem."',
					template='".$subio."' 
				WHERE id=".$id2." AND userid=".$userid;
				
	$res    = mysql_query($update) or die("Error: ".$update." ".mysql_error());
	
}


/*
 *
 *  
 *  
 */

$resp = array('success'=>'true','mensaje'=>'Template has been successfully created.');

echo json_encode($resp);
	
?>	