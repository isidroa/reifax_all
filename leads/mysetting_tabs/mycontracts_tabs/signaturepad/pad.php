<?php
/**
 * pad.php
 *
 * Creates the interface to make the online signature
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 01.04.2011
 */
 
require_once 'signature-to-image.php';
include "../../../properties_conexion.php"; 
conectar();
$userid  = $_COOKIE['datos_usr']["USERID"];
$f       = strlen($_GET['f'])> 0 ? $_GET['f'] : $_POST['f'];

// Control Variable
$ok      = 0;

// Check if the param send by the link its numeric
if (!ctype_digit($f)) 
	die();

// Get the signature in Json format
if ($_POST['output']!='') {
	
	$newFileName = "$userid-"."$f.png";
    $img         = sigJsonToImage($_POST['output']);
	
	// Delete (if) the previous image and copy the new
	@unlink($newFileName);
    imagepng($img, "../signatures/$newFileName");
    imagedestroy($img);
	
	// Delete the row of the previous image and insert a new one
	$query = "DELETE FROM xima.contracts_signature 
				WHERE userid = $userid AND type = $f";
	@mysql_query($query);	

	$query = "INSERT INTO xima.contracts_signature (userid, imagen, type) 
			  VALUES ($userid,'$newFileName',$f)";

	if (mysql_query($query))
		$ok = 1;
	else
		$ok = 0;
		
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Signature</title>
        <link rel="stylesheet" type="text/css" href="jquery.signaturepad.css">
        <script type="text/javascript" src="jquery-1.4.2.min.js"></script>
        <!--[if lt IE 9]><script src="flashcanvas.js"></script><![endif]-->
        <script type="text/javascript" src="jquery.signaturepad.min.js"></script>
        <script type="text/javascript" src="json2.min.js"></script>

    </head>
    <body>
    <div style="text-align:center;width:100%px;">
    <br>
    
	<?php
	// First Screen, with the Drawing Canvas
	if ($ok == 0) {
	?>
    
    <form method="post" action="#" class="sigPad">
    
        <p class="drawItDesc">Draw your signature</p>
        
        <div style="font-size:small">
        
        	<em> This option works better with an HTML5 Browser Capable </em>
            
        </div>

        <ul class="sigNav">
        
            <li class="clearButton"><a href="#clear">Clear</a></li>

        </ul>

        <div class="sig sigWrapper">
            <div class="typed"></div>
            <canvas class="pad" width="198" height="55"></canvas>
            <input type="hidden" name="output" class="output">
        </div>

        <button type="submit">Save Signature</button>

    </form>
    <script type="text/javascript">
    $(document).ready( function(){
		$('.sigPad').signaturePad({
			drawOnly   : true,
			penColour  : '#000000',
			lineColour : '#ffffff'
		})
    })
    </script>
    
    <?php
	// Second Screen, with a message and the close function
	}else{
	?>
    
    <script type="text/javascript">
    window.opener.forceRefresh();
	window.close();
    </script>
    
    

    <?php
	}
	?>
    
    </div>
    </body>
</html>
