<?php
/**
 * generatecontract.php
 *
 * Generate the contract for download.
 * 
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *			Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
 * @version 07.04.2011
 */

include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];

$query    = "SELECT platinum_invest FROM xima.ximausrs WHERE userid='$userid'";
$rs       = mysql_query($query);
$rpinv    = mysql_fetch_array($rs);
$platinum = $rpinv[0];

$query     = 'SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result    = mysql_query($query) or die($query.mysql_error());
$datos_usr = mysql_fetch_array($result);

$path      = 'mysetting_tabs/mycontracts_tabs/signatures/';

// Get all the images order by signature type
// the type its from 1 to 8, the first 4 elements are buyer's and 
// the second 4 elementes are for seller's
$query     = "SELECT imagen,type FROM xima.contracts_signature  
				WHERE userid = $userid ORDER BY type";
$rs        = mysql_query($query);
if ($rs) {
	while ($row = mysql_fetch_array($rs)) {
		$signs["{$row['type']}"] = $row[0];
	}
}
?>
<style type="text/css">
.x-form-file-wrap {
	position     : relative;
	height       : 22px;
	width        : 210px;
}
.x-form-file-wrap .x-form-file {
	position     : absolute;
	right        : 0;
	-moz-opacity : 0;
	filter       : alpha(opacity: 0);
	opacity      : 0;
	z-index      : 2;
	height       : 22px;
}
.x-form-file-wrap .x-form-file-btn {
	position     : absolute;
	right        : 0;
	z-index      : 1;
}
.x-form-file-wrap .x-form-file-text {
	position     : absolute;
	left         : 0;
	z-index      : 3;
	color        : #777;
}
.formulario {
	padding-top  : 2px; 
	color        : #274F7B; 
	font-weight  : bold; 
	font-size    : 14px;"
}
</style>

<!-- + + + + -->
<script type="text/javascript">
function tempLink() {
var tab     = tabs3.getActiveTab();
var updater = tab.getUpdater();
updater.update({
	url   : 'mysetting_tabs/mycontracts_tabs/mycontracts_new.php',
	cache : false
});
}
</script>
<a href="javascript:tempLink()">Refresh interface</a>

<!-- + + + + -->

<script type="text/javascript" src="FileUploadField.js"></script>

<div id="div_interface" class="formulario"></div>

<!--
<div id="div_formulario" class="formulario"></div>

<div id="mySignID" class="formulario"></div>

<div id="div_addons" class="formulario"></div>

<div id="div_scrow" class="formulario"></div>
-->
<script type="text/javascript">

// Special xtype to render an image to the panel

Ext.ux.Image = Ext.extend(Ext.Component, {
    url  : '<?=$path?>/noimage.png',
    autoEl: {
        tag: 'img',
        src: Ext.BLANK_IMAGE_URL,
        cls: 'tng-managed-image'
    },
    onRender: function() {
        Ext.ux.Image.superclass.onRender.apply(this, arguments);
        this.el.on('load', this.onLoad, this);
        if(this.url){
            this.setSrc(this.url+'?'+new Date().getTime());
        }
    },
    onLoad: function() {
        this.fireEvent('load', this);
    }, 
    setSrc: function(src) {
        this.el.dom.src = src;
    }
});
Ext.reg('image', Ext.ux.Image);


// ----------------------------------------------------

function getTemplate(){
	var idcontract     = Ext.getCmp('ctype1').getValue();
	var arrayContratos = ['ContractPurchase','leadBasedPaint','ResidentialContract'];
	var url            = 'overview_contract/'+arrayContratos[idcontract]+'.pdf';
	
	window.open(url);
}

function getTplSign(){
	var idcontract     = Ext.getCmp('ctype1').getValue();
	var arrayContratos = ['ContractPurchase','leadBasedPaint','ResidentialContract'];
	var url            = 'overview_contract/adjuntaFirma.php?d='+arrayContratos[idcontract];
	
	window.open(url);
}

// ----------------------------------------------------

var contracts = new Ext.data.SimpleStore({
					fields : ['id','name'],
					data   : [
						['0','As is Sales and Purchase Far Bar contract'],
						['1','Lead_Base contract'],
						['2','Far Bar Sales and Purchase contract']
					]
				});




/*
 *
 *
 * FORM PANEL - INFO AND ACTIVE TEMPLATE
 * 
 *
 *
 */

var formulario = new Ext.FormPanel({
	border     : true,
	bodyStyle  : 'padding: 10px;text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formul1',
	name       : 'formul1',
	items      : [
		{
			xtype         : 'combo',
			name          : 'ctype1',
			id            : 'ctype1',
			hiddenName    :'type',
			fieldLabel    : '<b>Contract Type</b>',
			typeAhead     : true,
			store         : contracts,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			emptyText     : 'Select ...',
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value         : '0',
			width         : 300,
			listeners     : {
				'select'  : buscar
			}
		},{
			xtype         : 'spacer',
			height        : 10
		},{
			xtype         : 'panel',
			id            : 'panelContractEdit',
			layout        : 'form',
			items         : [
				{
					xtype        : 'label',
					id           : 'labelinfo',
					text         : 'The default name is and the Contract Template is ',
					align        : 'center',
					style        : {
						marginBottom : '10px'
					}
				},{
					xtype        :'spacer',
					height       : 10
				},{
					xtype        : 'panel',
					layout       : 'table',
					layoutConfig : { columns : 4 },
					rowspan      : 2,
					items        : [
						{
							xtype    : 'button',
							text     : 'Get Template',
							handler  : getTemplate
						},{
							// Button added by alexbariv
							xtype    : 'button',
							text     : 'Get Template with Signature',
							handler  : getTplSign
						}
					]
				},{
					xtype        :'panel',
					layout       :'table',
					defaults     : {
						bodyStyle  : 'padding:5px; marginLeft: 10px;marginTop:10px'
					},
					layoutConfig : {
						columns  : 4
					},
					items: [
						{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							rowspan    : 2,
							items      : [
								{
									xtype      : 'checkbox',
									fieldLabel : 'Option 1',
									id         : 'option1',
									name       : 'option1',
									onClick    : function() {
										// This onClick was added by alexbariv 01.04.2011
										// It gives the radio behavior to the checkboxes 
										// option1 and option2
										var op = Ext.getCmp('option2').getValue();
										if (op == true) 
											Ext.getCmp('option2').setValue(false)
										var op = Ext.getCmp('option1').getValue();
										if (op == false) 
											Ext.getCmp('option1').setValue(true)
									}
								},{   
									xtype  : 'hidden',
									name   : 'id1',
									id     : 'id1'
								}
							]
						},{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							rowspan    : 2,
							items      : [
								{
									xtype      : 'textfield',
									id         : 'name1',
									fieldLabel : 'Name',
									name       : 'name1',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address1',
									fieldLabel : 'Address',
									name       : 'address1',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								}
							]
						},{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							items      : [
								{
									xtype      : 'label',
									text       : '',
									fieldLabel : 'Template',
									id         : 'template1',
									name       : 'template1'
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'activetemplate1',
									name       : 'activetemplate1',
									onClick    : function() {
										// This onClick was added by alexbariv 06.04.2011
										// It gives the radio behavior to the checkboxes 
										// and ensures that an option its actived with the tpl
										var op = Ext.getCmp('activetemplate2').getValue();
										if (op == true) {
											Ext.getCmp('activetemplate2').setValue(false);
											Ext.getCmp('option2').setValue(false);
											Ext.getCmp('activetemplate1').setValue(true);
											Ext.getCmp('option1').setValue(true);
										}
										var op = Ext.getCmp('activetemplate1').getValue();
										if (op == false) {
											Ext.getCmp('activetemplate2').setValue(true);
											Ext.getCmp('option2').setValue(true);
											Ext.getCmp('activetemplate1').setValue(false);
											Ext.getCmp('option1').setValue(false);
										}
									}
							  	}
							]
						},{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							width      : 300,
							rowspan    : 2,
							labelWidth : 50,
							items      : [
								{
									xtype       :'fileuploadfield',
									id          :'form-file1',
									emptyText   :'Select pdf',
									fieldLabel  :'Pdf',
									name        :'pdf1',
									buttonText  : 'Browse...'
								}
							]
						}
					]
				},{
					xtype        : 'panel',
					layout       : 'table',
					defaults     : {
						bodyStyle  : 'padding:5px; marginLeft: 10px'
					},
					layoutConfig : {
						columns    : 4
					},
					items        : [
						{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							rowspan    : 2,
							items      : [
								{
									xtype      : 'checkbox',
									fieldLabel : 'Option 2',
									id         : 'option2',
									name       : 'option2',
									onClick    : function() {
										// This onClick was added by alexbariv 01.04.2011
										// It gives the radio behavior to the checkboxes 
										// option1 and option2
										var op = Ext.getCmp('option2').getValue();
										if (op == false) 
											Ext.getCmp('option2').setValue(true)
											
										var op = Ext.getCmp('option1').getValue();
										if (op == true) 
											Ext.getCmp('option1').setValue(false)
									}
								},{  
									xtype : 'hidden',
									name  :'id2',
									id    :'id2'
								}
							]
						},{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							rowspan    : 2,
							items      : [
								{
									xtype      : 'textfield',
									id         : 'name2',
									fieldLabel : 'Name',
									name       : 'name2',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address2',
									fieldLabel : 'Address',
									name       : 'address2',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								}
							]
						},{
							xtype      : 'panel',
							border     : 'false',
							layout     : 'form',
							labelWidth : 60,
							items      : [
								{
									xtype      : 'label',
									text       : '',
									fieldLabel : 'Template',
									id         : 'template2',
									name       : 'template2'
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'activetemplate2',
									name       : 'activetemplate2',
									onClick    : function() {
										// This onClick was added by alexbariv 06.04.2011
										// It gives the radio behavior to the checkboxes 
										// and ensures that an option its actived with the tpl
										var op = Ext.getCmp('activetemplate2').getValue();
										if (op == false) {
											Ext.getCmp('activetemplate2').setValue(true);
											Ext.getCmp('option2').setValue(true);
											Ext.getCmp('activetemplate1').setValue(false);
											Ext.getCmp('option1').setValue(false);
										}
										var op = Ext.getCmp('activetemplate1').getValue();
										if (op == true) {
											Ext.getCmp('activetemplate2').setValue(false);
											Ext.getCmp('option2').setValue(false);
											Ext.getCmp('activetemplate1').setValue(true);
											Ext.getCmp('option1').setValue(true);
										}
									}							
								}
							]
						},{
							xtype      :'panel',
							border     :'false',
							layout     :'form',
							width      : 300,
							labelWidth : 50,
							rowspan    : 2,
							items      : [
								{
									xtype       :'fileuploadfield',
									id          :'form-file2',
									emptyText   :'Select pdf',
									fieldLabel  :'Pdf',
									name        :'pdf2',
									buttonText  : 'Browse...'
								}
							]
						}
					]
				}
			]
		}
	],
	buttonAlign : 'center',
	buttons     : [
		{
			text        : '<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save Templates</b></span>',
			handler     : function() {
				if(formulario.getForm().isValid()) {
					formulario.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savedefaults.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp    = a.result;
							var tab     = tabs3.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
								cache : false
							});
							Ext.MessageBox.alert('', resp.mensaje);
						},
						failure : function(response, o){ }	
					});
				}
			}
		}
	]
});

// formulario.render('div_formulario');



/*
 *
 *
 * FORM PANEL - NO SIGNATURE
 * 
 *
 *
 */
 
 
 
 
 
 
/*
 *
 *
 * FORM PANEL - SIGNATURE
 * 
 *
 *
 */

mySignForm = new Ext.FormPanel({
	frame      : true,
	autoHeight : true,
	collapsible: true,
	collapsed  : true,
	autoWidth  : true,
	fileUpload : true,
	method     : 'POST',
	bodyStyle  : 'padding-left:100px;text-align:left;',
	id         : 'formSig',
	name       : 'formSig',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			title        : 'Buyer Information',
			border       : true,
			bodyStyle    : 'padding: 10px; border: 2px solid #cccccc',
			width        : 700,
			layoutConfig : {
				columns  : 2
			},
			items        : [
				{
					xtype       : 'label',
					text        : 'Buyer Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Buyer Second Signature ',
					cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
							<?php
							echo strlen($signs[1])>0 ?  "url: '{$path}{$signs[1]}'," : "" ;
							?>
							xtype       : 'image'
						}
						<?php
						if(strlen($signs[1])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(1);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [
						{
							<?php
							echo strlen($signs[2])>0 ?  "url: '{$path}{$signs[2]}'," : "" ;
							?>
							xtype       : 'image',
							width       : 350,
							height      : 50,
						}
						<?php
						if(strlen($signs[2])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(2);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 5
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=1','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file1',
							emptyText   : 'Upload Image',
							name        : 'image1',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=2','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file2',
							emptyText   : 'Upload Image',
							name        : 'image2',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					height      : 20,
					colspan     : 2
                },{
					xtype       : 'label',
					text        : 'Buyer Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Buyer Second Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[3])>0 ?  "url: '{$path}{$signs[3]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[3])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(3);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[4])>0 ?  "url: '{$path}{$signs[4]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[4])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(4);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=3','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file3',
							emptyText   : 'Upload Image',
							name        : 'image3',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=4','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file4',
							emptyText   : 'Upload Image',
							name        : 'image4',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				}
			]
		},{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			title        : 'Seller Information',
			bodyStyle    : 'padding: 10px; border: 2px solid #cccccc',
			width        : 700,
			layoutConfig : {
				columns  : 2
			},
			items        : [
				{
					xtype       : 'label',
					text        : 'Seller Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Seller Second Signature ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[5])>0 ?  "url: '{$path}{$signs[5]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[5])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(5);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[6])>0 ?  "url: '{$path}{$signs[6]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[6])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(6);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=5','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file5',
							emptyText   : 'Upload Image',
							name        : 'image5',
							width       : 180,
							buttonText  : 'Browse...'
						}					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=6','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file6',
							emptyText   : 'Upload Image',
							name        : 'image6',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					height      : 20,
					colspan     : 2
                },{
					xtype       : 'label',
					text        : 'Seller Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'label',
					text        : 'Seller Second Initials ',
                    cls         : 'x-form-item',
			        labelStyle  : ''
                },{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[7])>0 ?  "url: '{$path}{$signs[7]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[7])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(7);}
						}
						<?php
						}
						?>
					]
				},{
					xtype       : 'panel',
					bodyStyle   : 'padding: 10px;',
					width       : 350,
					height      : 50,
					autoHeight  : true,
					items       : [{
						<?php
						echo strlen($signs[8])>0 ?  "url: '{$path}{$signs[8]}'," : "" ;
						?>
						xtype       : 'image'
						}
						<?php
						if(strlen($signs[8])>0) {
						?>
						,{
							xtype       : 'button',
							text        : 'Delete',
							handler     : function() {eliminarFirma(8);}
						}
						<?php
						}
						?>
					]
                },{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=7','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file7',
							emptyText   : 'Upload Image',
							name        : 'image7',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				},{
					xtype       : 'panel',
					layout       : 'table',
					layoutConfig : {
						columns  : 3
					},
					items       : [
						{
							xtype       : 'button',
							text        : 'Draw Signature',
							handler     : function(){
								var option = 'width=400px,height=300px';
								window.open('mysetting_tabs/mycontracts_tabs/signaturepad/pad.php?f=8','Signature',option);
							}
						},{
							xtype       : 'box',
							autoEl      : {
								html    : '&nbsp;or&nbsp;'
							}
						},{
							xtype       : 'fileuploadfield',
							id          : 'form-file8',
							emptyText   : 'Upload Image',
							name        : 'image8',
							width       : 180,
							buttonText  : 'Browse...'
						}
					]					
				}
			]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Signatures</b></span>',
		handler : function(){
			if (mySignForm.getForm().isValid()) {
				mySignForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savesigns.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						forceRefresh();
					}
				});
			}
		}
	}]
});
// mySignForm.render('mySignID');


var addonsForm = new Ext.FormPanel({
	border     : true,
	autoHeight : true,
	collapsible: true,
	collapsed  : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formul2',
	name       : 'formul2',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			width        : 700,
			layoutConfig : {
				columns  : 5
			},
			items       : [
				{
					html        : 'All the additional documents will be placed at the end of the contracts, with a new page for each one.',
					cls         : 'x-form-item',
					colspan     : 5,
					style       : {
						marginBottom : '10px'
					}
				},{
					html        : 'Contract Type',
					cls         : 'x-form-item',
					style       : {
						marginBottom : '10px'
					}
				},{
					xtype         : 'combo',
					name          : 'ctype2',
					id            : 'ctype2',
					hiddenName    :'type_a',
					typeAhead     : true,
					store         : contracts,
					triggerAction : 'all',
					mode          : 'local',
					editable      : false,
					emptyText     : 'Select ...',
					selectOnFocus : true,
					allowBlank    : false,
					displayField  : 'name',
					valueField    : 'id',
					value         : '0',
					width         : 300,
					listeners     : {
						'select'  : function() {
							var op = Ext.getCmp('ctype2').getValue();
							Ext.getCmp('addon_val1').setValue('');
							Ext.getCmp('addon_val2').setValue('');
							Ext.getCmp('addon_val3').setValue('');
							Ext.getCmp('addon_val4').setValue('');
							listarAddons(op);
						}
					},
					colspan     : 4,
					style        : {
						marginBottom : '10px'
					}
				},{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-aon1',
					emptyText   : 'Upload Document',
					name        : 'addo1',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'addon_val1',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarAddon(1);}
				},{
					html        : '&nbsp;', colspan : 5, style : { height : '10px' }
				},{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-aon2',
					emptyText   : 'Upload Document',
					name        : 'addo2',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{ 
					xtype       : 'textfield',
					id          : 'addon_val2',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarAddon(2);}
				},{
					html        : '&nbsp;', colspan : 5, style : { height : '10px' }
				},{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-aon3',
					emptyText   : 'Upload Document',
					name        : 'addo3',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'addon_val3',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarAddon(3);}
				},{
					html        : '&nbsp;', colspan : 5, style : { height : '10px' }
				},{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-aon4',
					emptyText   : 'Upload Document',
					name        : 'addo4',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'addon_val4',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarAddon(4);}
				}
			]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Addons</b></span>',
		handler : function(){
			if (addonsForm.getForm().isValid()) {
				addonsForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});
// addonsForm.render('div_addons');


var scrowForm = new Ext.FormPanel({
	autoHeight : true,
	collapsible: true,
	collapsed  : true,
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formul3',
	name       : 'formul3',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			width        : 700,
			layoutConfig : {
				columns  : 5
			},
			items       : [{
					html        : 'Select a Header',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-scrow1',
					emptyText   : 'Upload Image',
					name        : 'scrow1',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'scrow_val1',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarScrow(1);}
				},{
					html        : '&nbsp;', colspan : 5, style : { height : '10px' }
				},{
					html        : 'Select a Footer',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-scrow2',
					emptyText   : 'Upload Image',
					name        : 'scrow2',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{ 
					xtype       : 'textfield',
					id          : 'scrow_val2',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarScrow(2);}
				}
			]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Images</b></span>',
		handler : function(){
			if (scrowForm.getForm().isValid()) {
				scrowForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});
/*
var platinum = '<?=$platinum?>';
if (platinum == '1')
	scrowForm.render('div_scrow');
*/

// ----------------------------------------------------

var interface = new Ext.FormPanel(
	{
		title     : 'Contract Template Administration',
		bodyStyle : 'background-color:#f1f1f1;padding: 10px; border: 2px solid #cccccc',
		autoHeight: true,
		items     : [
			{
				xtype          : 'fieldset',
				title          : '1.- Establish basic information and active template',
				autoHeight     : true,
				collapsed      : false,
				collapsible    : true,
				autoHeight     : true,
				items          :[
					formulario				 
				]
			},{
				xtype          : 'fieldset',
				title          : '2.- Get Contract Template without Signature',
				autoHeight     : true,
				collapsed      : true,
				collapsible    : true,
				items          :[
					{
						xtype    : 'button',
						text     : 'Download File',
						handler  : getTemplate
					}			 
				]
			},{
				xtype          : 'fieldset',
				title          : '2.- Get Contract Template with signature',
				autoHeight     : true,
				collapsed      : true,
				collapsible    : true,
				items          :[
					mySignForm
					,{
						xtype    : 'button',
						text     : 'Download File',
						handler  : getTplSign
					}
				]
			},{
				xtype          : 'fieldset',
				title          : '4.- Upload and Set additional documents',
				autoHeight     : true,
				collapsed      : true,
				collapsible    : true,
				items          :[
					addonsForm
				]
			},{
				xtype          : 'fieldset',
				title          : '3.- Upload and Set scrow deposit letter options',
				autoHeight     : true,
				collapsed      : true,
				collapsible    : true,
				items          :[
					scrowForm
				]
			}
		]
	}
);
interface.render('div_interface');

// ----------------------------------------------------

function buscar() {
	Ext.Ajax.request({
		waitMsg     :'Wait please...',
		url         : 'mysetting_tabs/mycontracts_tabs/buscar.php',
		method      : 'POST',
		params      : {
			idvalue : Ext.getCmp('ctype1').getValue()
		},
		failure     : function(response,options) {
			Ext.MessageBox.alert('Warning','Error user show');
		},
		success     : function(response,options) {
			var rest = Ext.util.JSON.decode(response.responseText);
			if (rest.succes==false) {
				Ext.MessageBox.alert('Warning',rest.msg); return;
			}
			arrtask = rest.results;
			if ( arrtask.length>0 ) {
				
				Ext.getCmp('name1').setValue(arrtask[0].name);
				Ext.getCmp('form-file1').setValue('');
				Ext.getCmp('address1').setValue(arrtask[0].address);
				Ext.getCmp('id1').setValue(arrtask[0].id);
				
				if ( arrtask[0].defaultoption == 'Y' ) {
					Ext.getCmp('option1').setValue(true);
					Ext.getCmp('labelinfo').setText('The default name is '+arrtask[0].name);
				} else
					Ext.getCmp('option1').setValue(false);
				
				if(arrtask[0].template=='Y') 
					Ext.getCmp('template1').setText('On');
				else 
					Ext.getCmp('template1').setText('Off');
				
				if(arrtask[0].activetemplate=='Y')
					Ext.getCmp('activetemplate1').setValue(true);
				else
					Ext.getCmp('activetemplate1').setValue(false);
				
			} else {
				
				Ext.getCmp('form-file1').setValue('');
				Ext.getCmp('form-file2').setValue('');
				Ext.getCmp('id1').setValue('');
				Ext.getCmp('name1').setValue('');
				Ext.getCmp('address1').setValue('');
				Ext.getCmp('option1').setValue(false);
				Ext.getCmp('template1').setText('Off');
				Ext.getCmp('activetemplate1').setValue(false);
				Ext.getCmp('id2').setValue('');
				Ext.getCmp('name2').setValue('');
				Ext.getCmp('address2').setValue('');
				Ext.getCmp('option2').setValue(false);
				Ext.getCmp('template2').setText('Off');
				Ext.getCmp('activetemplate2').setValue(false);
				
			}
			if ( arrtask.length>1 ) {
				Ext.getCmp('form-file2').setValue('');
				Ext.getCmp('id2').setValue(arrtask[1].id);
				Ext.getCmp('name2').setValue(arrtask[1].name);
				Ext.getCmp('address2').setValue(arrtask[1].address);
				if (arrtask[1].defaultoption=='Y') {
					Ext.getCmp('option2').setValue(true);
					Ext.getCmp('labelinfo').setText('The default name is '+arrtask[1].name);
				} else
					Ext.getCmp('option2').setValue(false);
				
				if(arrtask[1].template=='Y')
					Ext.getCmp('template2').setText('On');
				else
					Ext.getCmp('template2').setText('Off');
				
				if(arrtask[1].activetemplate=='Y')
					Ext.getCmp('activetemplate2').setValue(true);
				else
					Ext.getCmp('activetemplate2').setValue(false);
				
			} else {
				
				Ext.getCmp('form-file1').setValue('');
				Ext.getCmp('form-file2').setValue('');
				Ext.getCmp('id1').setValue('');
				Ext.getCmp('name1').setValue('');
				Ext.getCmp('address1').setValue('');
				Ext.getCmp('option1').setValue(false);
				Ext.getCmp('template1').setText('Off');
				Ext.getCmp('activetemplate1').setValue(false);
				Ext.getCmp('id2').setValue('');
				Ext.getCmp('name2').setValue('');
				Ext.getCmp('address2').setValue('');
				Ext.getCmp('option2').setValue(false);
				Ext.getCmp('template2').setText('Off');
				Ext.getCmp('activetemplate2').setValue(false);
				
			}
		}
	})
}

buscar();



// ----------------------------------------------------

function listarAddons(op) {

	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listaddons.php?type='+op,
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)

			if (resp.addon1!=null)
				Ext.getCmp('addon_val1').setValue(resp.addon1.document);
				
			if (resp.addon2!=null)
				Ext.getCmp('addon_val2').setValue(resp.addon2.document);
				
			if (resp.addon3!=null)
				Ext.getCmp('addon_val3').setValue(resp.addon3.document);
				
			if (resp.addon4!=null)
				Ext.getCmp('addon_val4').setValue(resp.addon4.document);
				
		}
	});
	
}

listarAddons(0);

function listarScrows() {

	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listascrows.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)

			if (resp.scrow1!=null)
				Ext.getCmp('scrow_val1').setValue(resp.scrow1.imagen);
				
			if (resp.scrow2!=null)
				Ext.getCmp('scrow_val2').setValue(resp.scrow2.imagen);
				
		}
	});
	
}

if (platinum == 1)
	listarScrows();

function eliminarAddon(op) {
	
	var type = Ext.getCmp('ctype2').getValue();
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php?cmd=del&addon='+op+'&type_a='+type,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			Ext.getCmp('addon_val'+op).setValue('');
		}
	});
}

function eliminarScrow(op) {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php?cmd=del&scrow='+op,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			Ext.getCmp('addon_val'+op).setValue('');
		}
	});
	
}

function forceRefresh() {

	var tab     = tabs3.getActiveTab();
	var updater = tab.getUpdater();
	updater.update({
		url   : 'mysetting_tabs/mycontracts_tabs/mycontracts_new.php?'+new Date().getTime(),
		cache : false
	});

}

function eliminarFirma(op) {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/savesigns.php?cmd=del&type='+op,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			forceRefresh();
		}
	});
	
}

</script>