<?php
/**
 * generatecontract.php
 *
 * Generate the contract for download.
 * 
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *			Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
 * @version 07.04.2011
 */

include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];

$query    = "SELECT platinum_invest,setstate FROM xima.ximausrs WHERE userid='$userid'";
$rs       = mysql_query($query);
$rp       = mysql_fetch_array($rs);
$platinum = $rp[0];
$usrstate = explode(',',$rp[1]);

$query     = 'SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result    = mysql_query($query) or die($query.mysql_error());
$datos_usr = mysql_fetch_array($result);

$query     = 'SELECT * FROM xima.contracts_custom WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$rscr      = mysql_query($query) or die($query.mysql_error());
$countcr   = mysql_num_rows($rscr);

$checked   = '';
$combo     = '';
$contratos = '';
$i         = 0;

while ($rowcr = mysql_fetch_array($rscr)) {
	if ($i == 0)
		$checked = $rowcr['id'];

	$combo     .= "['{$rowcr['id']}','{$rowcr['name']}'],";
	$contratos .= "arrayContratos[{$rowcr['id']}]='{$rowcr['filename']}';\n";
	$i++;
}
?>
<style type="text/css">
.x-form-file-wrap {
	position     : relative;
	height       : 22px;
	width        : 210px;
}
.x-form-file-wrap .x-form-file {
	position     : absolute;
	right        : 0;
	-moz-opacity : 0;
	filter       : alpha(opacity: 0);
	opacity      : 0;
	z-index      : 2;
	height       : 22px;
}
.x-form-file-wrap .x-form-file-btn {
	position     : absolute;
	right        : 0;
	z-index      : 1;
}
.x-form-file-wrap .x-form-file-text {
	position     : absolute;
	left         : 0;
	z-index      : 3;
	color        : #777;
}
.formulario {
	padding-top  : 2px; 
	color        : #274F7B; 
	font-weight  : bold; 
	font-size    : 14px;"
}
</style>



<script type="text/javascript" src="FileUploadField.js"></script>

<div id="div_formulario" class="formulario"></div>

<div id="div_addons" class="formulario"></div>

<div id="div_scrow" class="formulario"></div>

<script type="text/javascript">

// ----------------------------------------------------

function getContract(){
	
	var arrayContratos = new Array() 
	<?=$contratos?>
	
	var idcontract     = Ext.getCmp('ctype1').getValue();		
	var url            = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+arrayContratos[idcontract];

	window.open(url);
}

function deleteContract(){
	var idcontract     = Ext.getCmp('ctype1').getValue();		
	if(idcontract && idcontract!=0)
	{
		Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(opt){
			if(opt == 'yes')																
			{
				
				Ext.Ajax.request({
					url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php?cmd=del&id='+idcontract,
					waitMsg : 'Deleting...',
					success : function(r) {
						Ext.MessageBox.alert('',r.responseText);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});					
					}
				});
			}
		});
	}

}

// ----------------------------------------------------------------

var contracts = new Ext.data.SimpleStore({
	fields : ['id','name'],
	data   : [
		<?=$combo?>
		['0','Upload new contract']
	]
});

var contractsAdd = new Ext.data.SimpleStore({
	fields : ['id','name'],
	data   : [
		<?=$combo?>
	]
});


// ----------------------------------------------------------------

var formulario = new Ext.FormPanel({
	title      : 'Contract Templates Administration ',
	border     : true,
	bodyStyle  : 'padding: 10px;text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formul1',
	name       : 'formul1',
	items      : [
		{
			xtype         : 'combo',
			name          : 'ctype1',
			id            : 'ctype1',
			hiddenName    :'type',
			fieldLabel    : '<b>Contract Name</b>', 
			typeAhead     : true,
			store         : contracts,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			emptyText     : 'Select ...',
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value         : '0',
			width         : 300,
			listeners     : {
				'select'  : function(){
					seleccionadorComboContratos();				
				}
			}
		},{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 4 },
			rowspan      : 2,
			items        : [
				/*{
					xtype    : 'button',
					text     : 'Download Contract',
					handler  : getContract
				},*/{
					// Button added by alexbariv
					xtype    : 'button',
					text     : 'Delete contract',
					style    : {
						marginRight : 30
					},
					handler  : deleteContract
				}
			]
		},{
			xtype         : 'panel',
			<?php
			if ($countcr > 0)
				echo "hidden : true,";
			?>
			id            : 'panelContractUpload',
			layout        : 'form',
			items         : [
				{
					xtype      : 'textfield',
					id         : 'pdfname',
					fieldLabel : 'PDF Name',
					name       : 'pdfname',
					width      : '200'
				},{
					xtype       :'fileuploadfield',
					id          :'idupload',
					emptyText   :'Select pdf',
					fieldLabel  :'Upload File',
					name        :'pdfupload',
					buttonText  : 'Browse...'
				}
			]
		},{
			xtype         : 'panel',
			id            : 'panelContractEdit',
			layout        : 'form',
			items         : [
				{
					xtype        :'spacer',
					height       : 10
				},{
					xtype        :'panel',
					layout       :'table',
					<?php
					if ($countcr == 0)
						echo "hidden : true,";
					?>
					defaults     : {
						bodyStyle  : 'padding:5px; marginLeft: 10px;marginTop:10px'
					},
					layoutConfig : {
						columns  : 4
					},
					items: [
						{
							xtype          : 'fieldset',
							checkboxToggle : false,
							title          : 'Buyer 1',
							id             : 'idtemplate1',
							border         : 'false',
							layout         : 'form',
							labelWidth     : 60,
							rowspan        : 2,
							style          : {
								marginLeft : 40
							},
							items          : [
								{
									xtype      : 'textfield',
									id         : 'name1',
									fieldLabel : 'Name',
									name       : 'name1',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address1',
									fieldLabel : 'Address',
									name       : 'address1',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'option1',
									name       : 'option1',
									onClick    : function() {
										
										if (Ext.getCmp('option2').getValue() == true) 
											Ext.getCmp('option2').setValue(false)
											
										if (Ext.getCmp('option1').getValue() == false) 
											Ext.getCmp('option1').setValue(true)
											
									}
								}
							]
						},{
							xtype          : 'fieldset',
							checkboxToggle : false,
							title          : 'Buyer 2',
							id             : 'idtemplate2',
							border         : 'false',
							layout         : 'form',
							labelWidth     : 60,
							rowspan        : 2,
							style          : {
								marginLeft : 40
							},
							items          : [
								{
									xtype      : 'textfield',
									id         : 'name2',
									fieldLabel : 'Name',
									name       : 'name2',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address2',
									fieldLabel : 'Address',
									name       : 'address2',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'option2',
									name       : 'option2',
									onClick    : function() {
										
										if (Ext.getCmp('option1').getValue() == true) 
											Ext.getCmp('option1').setValue(false)
											
										if (Ext.getCmp('option2').getValue() == false) 
											Ext.getCmp('option2').setValue(true)
											
									}
								}
							]
						}
					]
				}
			]
		}
	],
	buttonAlign : 'center',
	buttons     : [
		{
			text        : '<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save</b></span>',
			handler     : function() {
				
				if(Ext.getCmp('ctype1').getValue()=='0' || Ext.getCmp('ctype1').getValue()==0 )
				{
					Ext.Msg.show({
					   title      : 'Warning!',
					   msg        : 'The following Document Generator feature of REIFAX.com (herein after known as REIFAX) is for the sole purpose of modifying existing documents that the user uploads to the system.<br/>No documents will be supplied by REIFAX.<br/>The user MUST HAVE legal authority to upload, modify, change, fill-in and use any document he intends to modify and submit to another buyer or seller.<br/>Legal authority means that if a copyrighted document is uploaded, the user MUST HAVE written permission of the person or entity who drafted the document, or have paid an authorized re-seller for the use of that document, i.e. FAR/BAR forms.<br/>By uploading any document, the user hereby agrees to these terms and conditions for using the REIFAX Document Generator.<br/>Violation of these terms and conditions could result in the user violating copyright laws.<br/>The only information that will be filled in by REIFAX, on any form, if applicable is information that is available in the public record.<br/>Any and all other information will have to be supplied by the user.',
					   width      : 550,
					   buttons    : Ext.MessageBox.OKCANCEL,
					   fn         : myCallbackDisclaimer,
					   icon       : Ext.MessageBox.INFO
					})
				}
				else
				{
					if(formulario.getForm().isValid()) {
						formulario.getForm().submit({
							url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php',
							waitMsg : 'Saving...',
							success : function(f, a){ 
								var resp    = a.result;
								var tab     = tabs3.getActiveTab();
								var updater = tab.getUpdater();
								if (resp.mensaje == 'Contract saved successfully!') {
									updater.update({
										url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
										cache : false
									});
								}
								Ext.MessageBox.alert('', resp.mensaje);
							},
							failure : function(response, o){ }	
						});
					}
				}
			}			 
		}
	]
});


var myCallbackDisclaimer = function(btn, text) {
	if(btn == 'ok')		
	{
						if(formulario.getForm().isValid()) {
							formulario.getForm().submit({
								url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php',
								waitMsg : 'Saving...',
								success : function(f, a){ 
									var resp    = a.result;
									var tab     = tabs3.getActiveTab();
									var updater = tab.getUpdater();
									if (resp.mensaje == 'Contract saved successfully!') {
										updater.update({
											url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
											cache : false
										});
									}
									Ext.MessageBox.alert('', resp.mensaje);
								},
								failure : function(response, o){ }	
							});
						}

	}
}


var seleccionadorComboContratos = function() {
					if (Ext.getCmp('ctype1').getValue() == 0 ) {
						Ext.getCmp('panelContractUpload').show();
						Ext.getCmp('panelContractEdit').hide();
					} else  {
						Ext.getCmp('panelContractUpload').hide();
						Ext.getCmp('panelContractEdit').show();						
						buscar();
						Ext.getCmp('ctype2').setValue(Ext.getCmp('ctype1').getValue());		
						listarAddons();
					}
}
   
var seleccionadorComboContratosAdicionales = function() {

}
    


formulario.render('div_formulario');
<?php
if ($countcr > 0)
	echo "Ext.getCmp('ctype1').setValue($checked);";
?>

var addonsForm = new Ext.FormPanel({
	title      : 'Additional Documents of the Contracts',
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	<?php
	if ($countcr == 0)
		echo "hidden : true,";
	?>
	method     : 'POST',
	id         : 'formul2',
	name       : 'formul2',
	items      : [
		{
			xtype         : 'spacer',
			height        : 10
		},{
			xtype         : 'box',
			html          : 'All the additional documents will be placed at the end of the contracts, with a new page for each one.',
			cls           : 'x-form-item'
		},{
			xtype         : 'combo',
			name          : 'ctype2',
			id            : 'ctype2',
			hiddenName    : 'type_a',
			typeAhead     : true,
			store         : contractsAdd ,
			fieldLabel    : '<b>Contract Type</b>',  
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			emptyText     : 'Select ...',
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value         : '<?=$checked?>',
			width         : 300,
			listeners     : {
				'select'  : function() {
					listarAddons();
					Ext.getCmp('ctype1').setValue(Ext.getCmp('ctype2').getValue());		
					seleccionadorComboContratos(); 					
				}
			}
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			width        : 800,
			layoutConfig : {
				columns  : 6
			},
			items       : [
				<?php
				for ($aI = 1 ; $aI <= 6; $aI ++) {
				?>
				{
					xtype       : 'checkbox',
					id          : 'addon_act<?=$aI?>',
					style       : {
						marginLeft : '30px'
					}
				},{
					html        : '&nbsp;Name:&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'addon_name<?=$aI?>'
				},{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-aon<?=$aI?>',
					emptyText   : 'Upload Document',
					name        : 'addo<?=$aI?>',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarAddon(<?=$aI?>);},
					id          : 'addon_del<?=$aI?>',
					hidden      : true
				},{
					html        : '&nbsp;', colspan : 6, style : { height : '10px' }
				},
				<?php
				}
				?>
				

			]
		},{
			xtype       : 'box',
			html        : 'Use the checkbox field to select the active documents.',
			cls         : 'x-form-item'
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Addons</b></span>',
		handler : function(){
			if (addonsForm.getForm().isValid()) {
				addonsForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});
addonsForm.render('div_addons');


var scrowForm = new Ext.FormPanel({
	title      : 'Escrow Deposit Letter',
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	<?php
	if ($countcr == 0)
		echo "hidden : true,";
	?>
	method     : 'POST',
	id         : 'formul3',
	name       : 'formul3',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			width        : 700,
			layoutConfig : {
				columns  : 5
			},
			items       : [{
					html        : 'Select a Header',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-scrow1',
					emptyText   : 'Upload Image',
					name        : 'scrow1',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'textfield',
					id          : 'scrow_val1',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarScrow(1);}
				},{
					html        : '&nbsp;', colspan : 5, style : { height : '10px' }
				},{
					html        : 'Select a Footer',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				},{
					xtype       : 'fileuploadfield',
					id          : 'form-scrow2',
					emptyText   : 'Upload Image',
					name        : 'scrow2',
					width       : 180,
					buttonText  : 'Browse...'
				},{
					html        : '&nbsp;&nbsp;Current File&nbsp;',
					cls         : 'x-form-item'
				},{ 
					xtype       : 'textfield',
					id          : 'scrow_val2',
					emptyText   : 'None',
					disabled    : true,
					readonly    : true
				},{
					xtype       : 'button',
					text        : 'Delete',
					handler     : function() {eliminarScrow(2);}
				}
			]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Images</b></span>',
		handler : function(){
			if (scrowForm.getForm().isValid()) {
				scrowForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});
var platinum = '<?=$platinum?>';
if (platinum == '1')
	scrowForm.render('div_scrow');


// ----------------------------------------------------

function buscar() {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listcustomcontr.php',
		method  : 'POST',
		params    : {
			id : Ext.getCmp('ctype1').getValue()
		},
		waitMsg : 'Getting Info',
		success : function(r) {

			var resp   = Ext.decode(r.responseText)

			if (resp.data!=null) {
				
				Ext.getCmp('name1').setValue(resp.data.tpl1_name);
				Ext.getCmp('address1').setValue(resp.data.tpl1_addr);
				
				Ext.getCmp('name2').setValue(resp.data.tpl2_name);
				Ext.getCmp('address2').setValue(resp.data.tpl2_addr);
				
				Ext.getCmp('option1').setValue(false);
				Ext.getCmp('option2').setValue(false);
				
				if (resp.data.tplactive == '1')
					Ext.getCmp('option1').setValue(true);
					
				if (resp.data.tplactive == '2')
					Ext.getCmp('option2').setValue(true);
					
			} 
		}
	});
	
}

buscar();



// ----------------------------------------------------

function listarAddons() {

	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listaddons.php',
		method  : 'POST',
		params    : {
			type  : Ext.getCmp('ctype2').getValue()
		},
		waitMsg : 'Getting Info',
		success : function(r) {

			var resp   = Ext.decode(r.responseText)

			for (w=1; w<=6; w++){
					Ext.getCmp('addon_name'+w).setValue('');
					Ext.getCmp('addon_act'+w).setValue(false);
					Ext.getCmp('addon_del'+w).hide();
			}

			for (i=0; i<6; i++){

				if (resp.data!=null) {
	
					if (resp.data[i]!=null) {
						
						k = resp.data[i].place;
							
						Ext.getCmp('addon_name'+k).setValue(resp.data[i].name);
						
						if(resp.data[i].active=='1')
							Ext.getCmp('addon_act'+k).setValue(true);
						else
							Ext.getCmp('addon_act'+k).setValue(false);
							
						Ext.getCmp('addon_del'+k).show();
						
					}
					
				} else {

					w = i + 1;

					Ext.getCmp('addon_name'+w).setValue('');
					Ext.getCmp('addon_act'+w).setValue(false);
					Ext.getCmp('addon_del'+w).hide();
				
				}
			}
				
		}
	});
	
}

listarAddons();

function listarScrows() {

	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listascrows.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)

			if (resp.scrow1!=null)
				Ext.getCmp('scrow_val1').setValue(resp.scrow1.imagen);
				
			if (resp.scrow2!=null)
				Ext.getCmp('scrow_val2').setValue(resp.scrow2.imagen);
				
		}
	});
	
}

if (platinum == 1)
	listarScrows();

function eliminarAddon(op) 
{
	var type = Ext.getCmp('ctype2').getValue();
	if(type && type!=0)
	{
		Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(opt)
		{
			if(opt == 'yes')																
			{
				Ext.Ajax.request({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php?cmd=del&addon='+op+'&type_a='+type,
					waitMsg : 'Deleting...',
					success : function(r) 
					{
						Ext.MessageBox.alert('',r.responseText);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});					
						/*Ext.getCmp('addon_act'+op).setValue(false);
						Ext.getCmp('addon_name'+op).setValue('');
						Ext.getCmp('addon_del'+op).hide();
						*/
					}
				});
			}
		});
	}
}

function eliminarScrow(op) {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php?cmd=del&scrow='+op,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			Ext.getCmp('addon_val'+op).setValue('');
		}
	});
	
}

</script>
