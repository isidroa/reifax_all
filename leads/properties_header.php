<?php 
$db=$currentMod=$pid=$dbid=$listing=$agent=$liens=$remark=$psummary=$owner=$phones=$mortgage=$sales=$pendes=$imagenes=$status_pro=$lprice_pro=$prop_type=$lat=$long=$myrow=0;

function header_html($dbid1,$pid1){
	global $db,$currentMod,$pid,$dbid,$listing,$agent,$liens,$remark,$psummary,$owner,$phones,$mortgage,$sales,$pendes,$imagenes,$status_pro,$lprice_pro,$prop_type,$lat,$long,$myrow;
	$pid=$pid1;
	$dbid=$dbid1;
	$_SESSION["county"]=$dbid; 
	include("../../../../../conexion.php"); 
	
	$sql="select mlsresidential.parcelid from mlsresidential where mlsresidential.Parcelid='$pid'";
	$sql1="select pendes.parcelid from pendes where pendes.parcelid='$pid' and pendes.totaliens >0";
	$sql2="SELECT MORTGAGE.parcelid FROM MORTGAGE WHERE MORTGAGE.PARCELID='$pid'";
	$sql3="select parcelid from mlsresidential where Parcelid='$pid' AND (length(replace(remark1,' ',''))>0 || length(replace(remark2,' ',''))>0 || length(replace(remark3,' ',''))>0 || length(replace(remark4,' ',''))>0 || length(replace(remark5,' ',''))>0)";
	$sql4="select mlsresidential.parcelid from mlsresidential where mlsresidential.Parcelid='$pid' and mlsresidential.status='A'";
	$sql5="Select psummary.parcelid from psummary where psummary.parcelid='$pid'";
	$sql8="SELECT parcelid FROM sales where parcelid='$pid'";
	$sql9="SELECT parcelid FROM psummary where parcelid='$pid' AND (phonename<>'' || psummary.phonenumber1<>'' || psummary.phonenumber2<>'')";
	$sql10="select pendes.parcelid from pendes where pendes.parcelid='$pid'";
	$sql11="select i.parcelid from imagenes i where i.parcelid='$pid'";
	
	$result = mysql_query($sql) or die (mysql_error());
	$result1 = mysql_query($sql1) or die (mysql_error());
	$result2 = mysql_query($sql2) or die (mysql_error());
	$result3 = mysql_query($sql3) or die (mysql_error());
	$result4 = mysql_query($sql4) or die (mysql_error());
	$result5 = mysql_query($sql5) or die (mysql_error());
	$result8 = mysql_query($sql8) or die (mysql_error());
	$result9 = mysql_query($sql9) or die (mysql_error());
	$result10 = mysql_query($sql10) or die (mysql_error());
	$result11 = mysql_query($sql11) or die (mysql_error());
	
	$listing= mysql_num_rows($result4);
	$agent= mysql_num_rows($result);
	$liens= mysql_num_rows($result1);
	$remark= mysql_num_rows($result3);
	$psummary=$owner= mysql_num_rows($result5);
	$phones = mysql_num_rows($result9);
	$mortgage= mysql_num_rows($result2);
	$sales = mysql_num_rows($result8);
	$pendes = mysql_num_rows($result10);
	$imagenes = mysql_num_rows($result11);
	
	$sql_comparado="Select p.address,p.city,p.ozip,p.lsqft,p.bheated,p.tsqft,p.beds,p.bath,p.yrbuilt,p.ccoded,p.ccode,l.latitude,l.longitude,p.saleprice,p.saledate,m.status,m.lprice,ma.marketvalue FROM psummary p LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid) LEFT JOIN marketvalue ma ON (p.parcelid=ma.parcelid) LEFT JOIN latlong l ON (p.parcelid=l.parcelid) Where p.parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$myrow= mysql_fetch_array($res);
	
	$status_pro=$myrow['status'];
	$lprice_pro=$myrow['lprice'];
	$prop_type=$myrow['ccode'];
	if($prop_type='01') $_SESSION['current_MOD'].='PS_SF'; else $_SESSION['current_MOD'].='PS_CONDOS';
	$lat=$myrow['latitude'];
	$long=$myrow['longitude'];
	$currentMod=$_SESSION['current_MOD'];
}

function datos_propiedad(){
	global $myrow;
	echo $myrow['bheated'].' sqft / '.$myrow['beds'].' beds / '.$myrow['bath'].' baths / '.$myrow['ccoded'];
}

function script_html(){
	global $imagenes,$lat,$long,$pid,$dbid,$currentMod,$prop_type,$listing,$mortgage,$pendes,$db;?>
	<script>
	var map = mapS = null;
	var tiene_ima=<?php echo $imagenes.';';?>
	var img_src = null;

	var SpaceNeedle = new VELatLong(<?php echo $lat.','.$long;?>);
	
	function GetMap(tipo)
	{
		//alert('getmap');
		if(document.getElementById(tipo+'_report_button') && document.getElementById(tipo+'_report_button').innerHTML==''){
			if(tipo!='comparable' && tipo!='imagenes'){
				
				map = new VEMap(tipo+'_mymap');
				map.LoadMap(SpaceNeedle, 15);
				map.HideDashboard();
				map.AttachEvent("onobliqueenter", OnObliqueEnterHandler);
				var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
				pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
					"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
				map.AddShape(pin);
				
				
				
				if(tipo!='listing' || (tipo=='listing' && tiene_ima==0)){
					mapS = new VEMap(tipo+'_mymapS');
					mapS.LoadMap(SpaceNeedle, 15);
					mapS.HideDashboard();
					var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
					pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
						"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
					mapS.AddShape(pinS);
				}else{
					
					var data=document.getElementById('img_array_data').value;
					img_src=data.split(',');
					
					new Ext.Slider({
						id: 'slidercomp_img',					
						renderTo: 'slider_ima',
						width: 200,
						value:0,
						increment: 1,
						minValue: 0,
						maxValue: (img_src.length-1),
						listeners: {
							change: change_value_img
						}
					});
				}
				
				renderButtons(tipo);
			}else{
				if(tipo=='comparable'){
					map = new VEMap(tipo+'_mymap');
					map.LoadMap(SpaceNeedle, 15);
					//map.HideDashboard();
					var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
					pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;' >"+
						"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
					map.AddShape(pin);
					renderButtons(tipo);
					
				//////////////////////////////////////////////////////////////////////////////
				loading_win.show();
				Ext.Ajax.request( 
				{  
					waitMsg: 'Loading Comparables',
					url: '../../../../../look4comparables.php', 
					method: 'POST',
					timeout :600000,
					params: { 
						id: '<?php echo $pid;?>',
						module: 'PS',
						php_grid: '',
						prop: '<?php echo $prop_type;?>',
						status: 'CS,CC',
						_fromSS: true,
						county: <?php echo $dbid;?>,
						sessionMOD: '<?php echo $currentMod;?>',
						reset: true
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:successComp                        
				 }
			);
				//////////////////////////////////////////////////////////////////////////////////////		
				}else{
					renderButtons(tipo);
					pageLoad(2500);
				}
			}
		}else{
			if(document.getElementById(tipo+'_report_button') && document.getElementById(tipo+'_report_button').innerHTML==''){
			}else{
				setTimeout('GetMap("'+tipo+'");',2);
			}
		}
	 }

var tabs2=null;
<?php if(!isset($_SESSION['properties_search'])){?>
var viewport=null;
<?php }?>
Ext.onReady(function(){

		tabs2 = new Ext.TabPanel({
        renderTo: 'tabs2',
        activeTab: 0,
        width:650,
		height: 1200,
        plain:true,
		enableTabScroll:false,
        defaults:{	autoScroll: false
				},
        items:[
			  {
				title: 'Overview',
				listeners: {activate: function(){adjust_hei('psummary');}},
				autoLoad: {url: '../../../../../properties_data.php?tipo=psummary&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
			  }
			 <?php if($listing>0){?>
			 ,{
                title: 'Listing',
				listeners: {activate: function(){GetMap('listing');adjust_hei('listing');}},
                autoLoad: {url: '../../../../../properties_data.php?tipo=listing&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
            }
			<?php }if($imagenes>0){?>
			 ,{
                title: 'Pictures',
				listeners: {activate: function(){GetMap('imagenes');adjust_hei('imagenes');}},
                autoLoad: {url: '../../../../../properties_data.php?tipo=imagenes&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
            }
			<?php }?>
			,{
                title: 'Comparables',
				listeners: {activate: function(){GetMap('comparable');adjust_hei('comparable');}},
                autoLoad: {url: '../../../../../properties_data.php?tipo=comparable&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
            }
			<?php if($mortgage>0){?>
			,{
                title: 'Mortgage',
				listeners: {activate: function(){GetMap('mortgage');adjust_hei('mortgage');reloadTab('mortgage');}},
                autoLoad: {url: '../../../../../properties_data.php?tipo=mortgage&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
            }
			<?php }if($pendes>0){?>
			,{
                title: 'Foreclosures',
				listeners: {activate: function(){GetMap('pendes');adjust_hei('pendes');reloadTab('pendes');}},
                autoLoad: {url: '../../../../../properties_data.php?tipo=pendes&db=<?php echo $db;?>&pid=<?php echo $pid;?>', scripts: true}
            }
			<?php }?>
        ]
    });

		//alert(document.width+' '+document.body.offsetWidth);
		<?php if(!isset($_SESSION['properties_search'])){?>
		var ancho=document.body.offsetWidth;
		if(ancho>1260) ancho=1260;
		
		 viewport = new Ext.Panel({
			  renderTo: 'body_central',
			  id: 'viewport',
			  layout:'border',
			  monitorResize: true,
			  hideBorders: true,
			  width: ancho,
			  height: 1525,
			  items:[
				{
					region: 'north',
					id: 'north_panel',
					layout: 'hbox',
					height: 120,
					minSize: 120,
                    maxSize: 120,
                    collapsible: false,
					contentEl: 'layout_top'
				},
				{
					region: 'south',
					id: 'south_panel',
					layout: 'hbox',
					height: 120,
					minSize: 120,
                    maxSize: 120,
                    collapsible: false,
					contentEl: 'layout_bottom'
				},
				{
					region: 'east',
					id: 'east_panel',
					layout: 'vbox',
					width: 300,
					minSize: 300,
                    maxSize: 300,
                    collapsible: true,
					collapseMode: 'mini',
					contentEl: 'layout_right',
					listeners: {beforeexpand: function(){return false;}}
				},
				{
					region: 'west',
					id: 'west_panel',
					width: 300,
					minSize: 300,
					maxSize: 300,
					collapsible: true,
					collapseMode: 'mini',
					contentEl: 'layout_left',
					listeners: {beforeexpand: function(){return false;}}
				},
				{
					region: 'center',
					id: 'center_panel',
					layout: 'hbox',
					layoutConfig:{align:'middle'},
                    collapsible: false,
					contentEl: 'layout_center'
				}
			  ]
		});
		
		if(document.body.offsetWidth < 1200){
			 var west = Ext.getCmp('west_panel');
			 west.collapse();
		}
		<?php }else{?>
		GetMap('psummary');
		<?php }?>
});

// This function renders a block of buttons
    function renderButtons(title){
        new ButtonPanel(
            title,
            [
			{
                text: 'Distress Report',
				iconCls:'icon',
                icon: 'http://96.31.84.77/img/ximaicon/distress.png',
				iconAlign: 'top',
				scale: 'medium',
				style: 'margin-rigth:5px;',
				listeners: {click: function (boton,evento){
					window.open('../../../../../properties_reports.php?type=distress&module=<?php echo $currentMod;?>&db=<?php echo $db;?>&pid=<?php echo $pid;?>');
				}} 
            },
			{
                text: 'Property Analisys',
				iconCls:'icon',
                icon: 'http://96.31.84.77/img/ximaicon/ssale.png',
				iconAlign: 'top',
				scale: 'medium',
				style: 'margin-rigth:5px;',
				listeners: {click: function (boton,evento){
					window.open('../../../../../properties_reports.php?type=par&module=<?php echo $currentMod;?>&db=<?php echo $db;?>&pid=<?php echo $pid;?>');
				}}
            },
			{
                text: 'BPO Report',
				iconCls:'icon',
                icon: 'http://96.31.84.77/img/ximaicon/bpo.png',
				iconAlign: 'top',
				scale: 'medium',
				style: 'margin-rigth:5px;',
				listeners: {click: function (boton,evento){
					window.open('../../../../../properties_reports.php?type=bpo&module=<?php echo $currentMod;?>&db=<?php echo $db;?>&pid=<?php echo $pid;?>');
				}}
            },
			{
                text: 'XRay Report',
				iconCls:'icon',
                icon: 'http://96.31.84.77/img/ximaicon/exray.png',
				iconAlign: 'top',
				scale: 'medium',
				listeners: {click: function (boton,evento){
					window.open('../../../../../properties_reports.php?type=xray&module=<?php echo $currentMod;?>&db=<?php echo $db;?>&pid=<?php echo $pid;?>');
				}}
            }
			]
        );
    }



</script>
<style type="text/css">
.carousel-component { 
    padding:8px 16px 4px 16px;
    margin:0 auto 0 auto;
}

.carousel-component .carousel-list li { 
    margin:4px;
    width:79px; /* img width is 75 px from flickr + a.border-left (1) + a.border-right(1) + 
                   img.border-left (1) + img.border-right (1)*/
    height:93px; /* image + row of text (87) + border-top (1) + border-bottom(1) + margin-bottom(4) */
    /*    margin-left: auto;*/ /* for testing IE auto issue */
}

.carousel-component .carousel-list li a { 
    display:block;
    border:1px solid #e2edfa;
    outline:none;
}

.carousel-component .carousel-list li a:hover { 
    border: 1px solid #aaaaaa; 
}

.carousel-component .carousel-list li img { 
    border:1px solid #999;
    display:block; 
}
                                
.carousel-component .carousel-prev { 
    position:absolute;
    top:40px;
    z-index:3;
    cursor:pointer; 
    left:5px; 
}

.carousel-component .carousel-next { 
    position:absolute;
    top:40px;
    z-index:3;
    cursor:pointer; 
    right:5px; 
}
</style><?php
}
?>