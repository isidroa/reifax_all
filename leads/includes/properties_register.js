		var formulregister = new Ext.FormPanel({
			url:'php/response_add.php',
			frame:true,
			bodyStyle:'padding:5px 5px 0',
			items: [{
					xtype: 'fieldset',
					title: 'Contact Information',
					autoHeight: true,
					items: [{
						layout:'column',
						items:[{
							columnWidth:.33,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype: 'textfield',
								id: 'txtFirstName',
								name: 'txtFirstName',
								fieldLabel: '<span style="color:#F00">*</span> First Name',
								allowBlank:false,
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtAddress1',
								name: 'txtAddress1',
								fieldLabel: '&nbsp;&nbsp; Address',
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtZip',
								name: 'txtZip',
								fieldLabel: '&nbsp;&nbsp; Zip Code',
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtEmail',
								name: 'txtEmail',
								fieldLabel: '<span style="color:#F00">*</span> E-Mail',
								allowBlank:false,
								vtype:'email',
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtPassword',
								name: 'txtPassword',
								fieldLabel: '<span style="color:#F00">*</span> Password',
								allowBlank:false,
								vtype:'password',
								width: 170
							}]
						},{
							columnWidth:.33,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype:'textfield',
								id: 'txtLastName',
								name: 'txtLastName',
								fieldLabel: '<span style="color:#F00">*</span> Last Name',
								allowBlank:false,
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtCity',
								name: 'txtCity',
								fieldLabel: '&nbsp;&nbsp;&nbsp; City',
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtPhone',
								name: 'txtPhone',
								fieldLabel: '<span style="color:#F00">*</span> Phone',
								allowBlank:false,
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtEmail2',
								name: 'txtEmail2',
								fieldLabel: '<span style="color:#F00">*</span> Verify E-Mail',
								allowBlank:false,
								vtype:'email',
								width: 170
							}, {
								xtype:'textfield',
								id: 'txtPassword2',
								name: 'txtPassword2',
								fieldLabel: '<span style="color:#F00">*</span> Verify Password',
								allowBlank:false,
								vtype:'password',
								width: 170
							}]
						},{
							columnWidth:.34,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype: 'combo',
								name: 'ncountry',
								id: 'ncountry',
								hiddenName: 'scountry',
								store: new Ext.data.SimpleStore({
										fields: ['valuec', 'textc'],
										data : [['US', 'United State']]
								}),				
								editable: false,
								displayField: 'textc',
								valueField: 'valuec',
								typeAhead: true,
								fieldLabel: '<span style="color:#F00">*</span> Country',
								mode: 'local',
								triggerAction: 'all',
								emptyText:'Select ...',
								selectOnFocus:true,
								autoSelect:true,
								width: 170	
							},{
								xtype: 'combo',
								name: 'nstate',
								id: 'nstate',
								hiddenName: 'sState',
								store: new Ext.data.SimpleStore({
										fields: ['values', 'texts'],
										data : [['Florida', 'Florida']]
								}),				
								editable: false,
								displayField: 'texts',
								valueField: 'values',
								typeAhead: true,
								fieldLabel: '<span style="color:#F00">*</span> State',
								mode: 'local',
								triggerAction: 'all',
								emptyText:'Select ...',
								selectOnFocus:true,
								autoSelect:true,
								width: 170	
							}, {
								xtype:'textfield',
								id: 'txtMobil',
								name: 'txtMobil',
								fieldLabel: '&nbsp;&nbsp;&nbsp; Mobile',
								width: 170
							},{
								xtype: 'textfield',
								id: 'nickname',
								name: 'nickname',
								fieldLabel: '<span style="color:#F00">*</span> Nickname',
								allowBlank:false,
								width: 170
							}]
						}]
					}]
			},{
				xtype: 'fieldset',
				title: 'Product Information',
				autoHeight: true,
				items: [{
						layout:'column',
						items:[{
							columnWidth:.5,
							layout: 'form',
							labelWidth: 120, 
							items: [{
								xtype: 'textfield',
								id: 'prom',
								name: 'prom',
								fieldLabel: '&nbsp;&nbsp; Promotion Code',
								width: 170
							}]
						}]				
					},{
						layout:'column',
						items:[{
							columnWidth:.3,
							layout: 'form',
							html:'&nbsp;'
						},{
							columnWidth:.7,
							layout: 'form',
							items: [{
								xtype: 'radiogroup',
								fieldLabel: '<span style="color:#F00">*</span> Please select frequency',
						        labelStyle: ' width: 200px',
								columns: 5,
					            vertical: true,
								items: [
									{boxLabel: 'Monthly', name: 'rbTiempoPago', inputValue: 'm', checked: true},
									{boxLabel: 'Annually', name: 'rbTiempoPago', inputValue: 'a',}
								]
							},{
									xtype: 'combo',
									name: 'cbstateprice',
									id: 'cbstateprice',
									hiddenName: 'cbGrupoState',
									store: new Ext.data.SimpleStore({
											fields: ['values', 'texts'],
											data : [['FL', 'Florida']]
									}),				
									editable: false,
									displayField: 'texts',
									valueField: 'values',
									typeAhead: true,
									fieldLabel: '<span style="color:#F00">*</span> Please select state',
									labelStyle: ' width: 200px',
									mode: 'local',
									triggerAction: 'all',
									emptyText:'Select ...',
									selectOnFocus:true,
									autoSelect:true,
									width: 170	
							},{
									xtype: 'combo',
									name: 'cbcountyprice',
									id: 'cbcountyprice',
									hiddenName: 'cbGrupoCounty',
									store: new Ext.data.SimpleStore({
											fields: ['values', 'texts'],
											data : [['FL', 'Florida']]
									}),				
									editable: false,
									displayField: 'texts',
									valueField: 'values',
									typeAhead: true,
									fieldLabel: '<span style="color:#F00">*</span> Please select county',
									labelStyle: ' width: 200px',
									cls:'text-align:left',
									mode: 'local',
									triggerAction: 'all',
									emptyText:'Select ...',
									selectOnFocus:true,
									autoSelect:true,
									width: 170	
							},{
									layout: 'absolute',
									title: 'Panel 1',
									width: 620,	
									height: 300,	
									html: 'Positioned at x:50, y:50'									
							}]
						}]				
				}]
			},{
				xtype: 'fieldset',
				title: 'Billing Information',
				autoHeight: true,
				items: [{
						layout:'column',
						items:[{
							columnWidth:.33,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype: 'combo',
								name: 'ncardtype',
								id: 'ncardtype',
								hiddenName: 'cardtype',
								store: new Ext.data.SimpleStore({
										fields: ['valuec', 'textc'],
										data : [['MasterCard', 'Master Card'],['Visa', 'Visa'],
												['AmericanExpress', 'American Express'],['Discover', 'Discover'],]
								}),				
								editable: false,
								allowBlank:false,
								displayField: 'textc',
								valueField: 'valuec',
								typeAhead: true,
								fieldLabel: '<span style="color:#F00">*</span> Card Type',
								mode: 'local',
								triggerAction: 'all',
								emptyText:'Select ...',
								selectOnFocus:true,
								autoSelect:true,
								width: 170	
							},{
								xtype: 'textfield',
								id: 'cardnumber',
								name: 'cardnumber',
								fieldLabel: '<span style="color:#F00">*</span> Card Number',
								allowBlank:false,
								vtype:'numeric',
								width: 170
							}, {
								xtype:'textfield',
								id: 'sameAdd',
								name: 'sameAdd',
								fieldLabel: '<span style="color:#F00">*</span> Address',
								allowBlank:false,
								width: 170
							}, {
								xtype:'textfield',
								id: 'sameZip',
								name: 'sameZip',
								fieldLabel: '<span style="color:#F00">*</span> Zip Code',
								allowBlank:false,
								width: 170
							}]
						},{
							columnWidth:.33,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype:'textfield',
								id: 'holder',
								name: 'holder',
								fieldLabel: '<span style="color:#F00">*</span> Holder Name',
								allowBlank:false,
								width: 170
							}, {
								xtype: 'compositefield',
								fieldLabel: '<span style="color:#F00">*</span>  Expiration Date',
								combineErrors: false,
								items: [{
									   	xtype:'numberfield',
										id: 'exdate1',
										name: 'exdate1',
										width: 40,
									   	allowBlank: false
								   	},{
									   	xtype: 'displayfield',
									   	value: 'MM'
									},{
									   	xtype:'numberfield',
										id: 'exdate2',
										name: 'exdate2',
										width: 60,
									   	allowBlank: false
								   },{
									   xtype: 'displayfield',
									   value: 'YYYY'
								}]
							}, {
								xtype:'textfield',
								id: 'sameCity',
								name: 'sameCity',
								fieldLabel: '<span style="color:#F00">*</span> City',
								allowBlank:false,
								width: 170
							}]
						},{
							columnWidth:.34,
							layout: 'form',
							labelWidth: 110, 
							items: [{
								xtype:'textfield',
								id: 'csv',
								name: 'csv',
								fieldLabel: '<span style="color:#F00">*</span> CSV Number',
								allowBlank:false,
								width: 170
							},{
								xtype: 'combo',
								name: 'nstateb',
								id: 'nstateb',
								hiddenName: 'sStateB',
								store: new Ext.data.SimpleStore({
										fields: ['values', 'texts'],
										data : [['Florida', 'Florida']]
								}),				
								allowBlank:false,
								editable: false,
								displayField: 'texts',
								valueField: 'values',
								typeAhead: true,
								fieldLabel: '<span style="color:#F00">*</span> State',
								mode: 'local',
								triggerAction: 'all',
								emptyText:'Select ...',
								selectOnFocus:true,
								autoSelect:true,
								width: 170	
							}]
						}]
					}]
			},{
				xtype: 'fieldset',
				title: '',
				autoHeight: true,
				items: [{
					xtype: 'checkbox',
					fieldLabel: '',
					boxLabel: '<b>I declare to have read, understood, and accepted the <a href="TermsAndConditions.pdf" target="_blank" >Terms and Conditions.</a></b>',
					name: 'accept',
					id: 'accept'
				}]
			}],
			buttons: [{
					text: 'Save',
					cls: 'x-btn-text-icon',			
					icon: 'img/save.gif',
	                formBind: true,
					handler  : function(){
							formulregister.getForm().submit({
								method: 'POST',
								waitTitle: 'Please wait..',
		                        waitMsg: 'Sending data...',
								success: function() {
									store.reload();
									Ext.Msg.alert("Success", "Insertion successfully");
									wind.close();
								},
								failure: function(form, action) {
	                                obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.errors.reason);
								}
							});
						   }
				},{
					text: 'Cancel',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind.close();
				   }
			}]	
		});
