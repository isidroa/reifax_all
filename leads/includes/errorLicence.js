//FUNCION LLAMADA EN onblur EN LA PAGINA register.php EN EL CAMPO email
//COMPRUEBA QUE LA LICENCIA DE BROKER Y/O REALTOR NO EXISTA EN LA BASE DE DATOS

function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(E){
				xmlhttp = false;
			}
		}
		if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
			xmlthttp = new XMLHttpRequest();
		}
		return xmlhttp;
}
	
function checkUserNumber(id){
	var error=document.getElementById("msgNumber");
	var licence= document.getElementById(''+id+'').value;
	var msg;
	
	//if(id=="txtRealtor")	msg="Please enter a Licence Number";
	//if(id=="txtBroker")		msg="Please enter a Broker Number";
	
	//if(licence.value==""){
		//alert(msg)
		//return false;
	//}else{
	if(licence!=''){
		ajax=objetoAjax();
		ajax.open("POST", "includes/errorLicence.php",true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("licence="+licence+"&id="+id);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				var resultado=ajax.responseText;
				if(resultado!="") document.getElementById("msgNumber").style.visibility="visible"; else document.getElementById("msgNumber").style.visibility="hidden";
				
				if(resultado!="")	document.getElementById("btnsubmit").disabled=true; 
			else if(resultado=="") document.getElementById("btnsubmit").disabled=false;
			
				document.getElementById("msgNumber").innerHTML=resultado;
			}
		}
	}
	else{
		document.getElementById("msgNumber").innerHTML='';
		document.getElementById("msgNumber").style.visibility="hidden";
	}
}
