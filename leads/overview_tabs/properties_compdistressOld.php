<? 
	$db_data=$_GET['db'];
	$pid=$_GET['pid'];
	$xcode=$_GET['xcode'];
	$do_paging = isset($_GET['document'])  ? false : true;
	if(isset($_GET['overview_comp'])) $overview_comp=true;

	if(!isset($par_no_conexion)){
		include("../properties_conexion.php");
		conectarPorBD($db_data);
	}
	if(!isset($_POST['no_func'])){
		include ("../properties_getgridcamptit.php");
	}
	$que="SELECT idcounty from xima.lscounty WHERE bd='".str_replace('1','',$db_data)."'";
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$county=$r[0];
	
	$loged=false;
	$block=true;
	if(isset($_COOKIE['datos_usr']['USERID'])){
		$loged=true;
		
		$query='select block_county,block_commercial,block_realtorweb from xima.xima_system_var WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		$block=($r['block_county']=='Y' || $r['block_commercial']=='Y' || $r['block_realtorweb']=='Y') ? true : false;
		$commercial=$r['block_commercial']=='Y' ? 2 : 1;
		$realtorweb=$r['block_realtorweb']=='Y' ? true : false;
		
		if($realtorweb){
			$query='select adverrealtorweb from xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']['USERID'];
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			if($r[0]==0){ $loged=false; } 
		}
	}
	
	if(!$loged || $block){
		echo '<div style="font-size:12px; margin-top: 5px;">';
		if($realtorweb){
			echo 'This feature is only available for full registered users.<br>';
		}elseif($commercial==2){
			echo 'This feature is not available for not commercial users.<br>';
		}else{
			echo 'This feature is only available for registered users.<br>';
			echo 'If you are a Register User please <a href="javascript:void();" onclick="login_win.show();return false;">log In</a>.<br>';
			echo '<a href="https:www.reifax.com/register.php" style=" color:red;">Register Now!!!!</a>';
		}
		echo '</div>';
		return false;	
	}
	
	$map="distress_mymap";
	$grid_render="compdistress_sujeto";
	$grid_render2="compdistress_comparables";
	$pagin_comp_tol="pagingDistress";
	if(isset($par_no_conexion)){
		$map="Rdistress_mymap";
		$grid_render="Rcompdistress_sujeto";
		$grid_render2="Rcompdistress_comparables";
		$pagin_comp_tol="RpagingDistress";
	}
	if(isset($overview_comp)){
		$map="Cdistress_mymap";
		$grid_render="Ccompdistress_sujeto";
		$grid_render2="Ccompdistress_comparables";
		$pagin_comp_tol="CpagingDistress";
	}
 
	$_mainTbl="psummary";
	$_zip="$_mainTbl.ozip,";
	$_larea="$_mainTbl.bheated,";
	$_unit="$_mainTbl.unit,";
	$pricesSel="$_mainTbl.saleprice";
	

	$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
	$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,Marketvalue.pendes,Marketvalue.marketvalue,
	Marketvalue.marketmedia,Marketvalue.marketpctg,Marketvalue.offertvalue,Marketvalue.offertmedia,Marketvalue.offertpctg, LATLONG.LATITUDE,LATLONG.LONGITUDE
	FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
	ON (MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$pid."';";
	
	$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
	$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
	
	$dt_comparado="";
	$myrow["xlat"]=$myrow["LATITUDE"];	$myrow["xlong"]=$myrow["LONGITUDE"];
	foreach($myrow as $clave=>$valor)	
	{	if($dt_comparado!="") $dt_comparado.=",";	
		$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
	}	

	$data_comparado=json_decode("[{".$dt_comparado."}]");
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
	$ArIDCT = getArray('distress','comparables',true);
	
	$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$colsCompar=str_replace(  "'",'"', $colsCompar);
	$colsCompar=json_decode($colsCompar);

	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
	$ArIDCT = getArray('distress','comparables',false);

	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
			
?>
<div align="center" id="todo_compdistress_panel">
<div align="center" style="width:100%; margin:auto; margin-top:10px;">
	<div id="<?php echo $map; ?>" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>
<br clear="all" />
<div id="distress_data_div" align="center" style="margin:auto; width:100%;">
	<div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
    <div id="<?php echo $grid_render2; ?>"></div>
</div>
</div>
<script>
    var limitDistress=50;
	mapOverview = new VEMap('<?php echo $map; ?>');
	var guidDistress=mapOverview.GUID;
	<?php if(isset($par_no_conexion)){?>
	var guidDistressR=mapOverview.GUID;
	<?php }if(isset($overview_comp)){?>
	var guidDistressC=mapOverview.GUID;
	<?php }?>
	mapOverview.LoadMap(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>), 15);
		
	
	function gridgetcasita(value, metaData, record, rowIndex){
		if(value=='SUBJECT'){
			return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
		}else{
			var aux=value.split('_');
			return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
		} 
	}
	
	var arrayDataSujetoDistress = [
		<?php 
			foreach($data_comparado as $x => $val){
				echo "['SUBJECT'";
				foreach($colsCompar as $k => $v){
					eval("\$xdata=\$val->".$v->name.";");
					echo ",'$xdata'";
				}
				echo ']';
			}
		?>
	];
	
	var storeSujetoDistress = new Ext.data.ArrayStore({
        fields: [
           <?php 
		   		echo "'status'";
		   		foreach($colsCompar as $k=>$val){
		   			echo ",'".$val->name."'";
				}
		   ?>
        ]
    });
	
	var gridSujetoDistress = new Ext.grid.GridPanel({
		renderTo: '<?php echo $grid_render; ?>',
		cls: 'grid_comparado',
		width: 'auto',
		height: 65,
		store: storeSujetoDistress,
		columns: [
			<?php 
		   		echo "{header: 'Sta.', width: 30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
		   		foreach($colsCompar as $k=>$val){
		   			echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
				}
		   ?>
		],
		listeners: {
			"mouseover": function(e) {
				var row;
				if((row = this.getView().findRowIndex(e.getTarget())) !== false){
					var record = this.store.getAt(row);
					
					document.getElementById("PinCompDistress_0_<?php echo $pid;?>_<?php echo $db_data;?>_"+<?php if(isset($par_no_conexion)) echo 'guidDistressR'; elseif(isset($overview_comp)) echo 'guidDistressC'; else echo 'guidDistress';?>).onmouseover();
				}
			},
			"mouseout": function(e) {
				VEPushpin.Hide();
			}
		}
	});
	
	storeSujetoDistress.loadData(arrayDataSujetoDistress);
	if(Ext.isIE){
		gridSujetoDistress.getEl().focus();
	}

	var storeDistress = new Ext.data.Store({
        url: 'http://www.reifax.com/xima3/properties_1mile_distress.php',
		baseParams: {'no_func': true,'bd': '<?php echo $db_data;?>', 'id': '<?php echo $pid;?>','userid':<?php echo $_COOKIE['datos_usr']['USERID'];?>},
		reader: new Ext.data.JsonReader(),
		remoteSort: true,
		listeners: {
			'load': function(store,data,obj){
				mapOverview.Clear();
				var arrLatLong= new Array();
				var pin = new VEPushpin(
					'PinCompDistress_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
					new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
					'<?php echo $_SERVERXIMA.'img/houses/';?>verdetotal.png',
					'<?php echo $data_comparado[0]->address;?>',
					'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="ImageCompDistress_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="<?php echo $_SERVERXIMA.'img/nophotocasa.jpg';?>"></a></div>'+
					'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
					'  <tr> '+
					'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
					'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
					'  </tr><tr> '+
					'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
					'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
					'  </tr><tr> '+
					'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
					'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
					'  </tr> <tr> '+
					'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
					'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
					'  </tr> <tr> '+
					'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
					'    <td>Subject</td>'+
					'  </tr></table></div>',
					'',
					'',
					''
				);
				mapOverview.ClearInfoBoxStyles();
				mapOverview.AddPushpin(pin);
				arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
				
				for(k in data){
					if(Ext.isNumber(parseInt(k))){
						var id = data[k].get('status').split('_');
						var ind = id[0];
						var status = id[1].split('-')[0];
						var pendes = id[1].split('-')[1];
						
						getCasita(status,pendes);
						pin = new VEPushpin(
							'PinCompDistress_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>',
							new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')),
							'<?php echo $_SERVERXIMA.'img/houses/';?>'+lsImgCss[indImgCss].img,
							data[k].get('pin_address'),
							'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="ImageCompDistress_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>'+'" height="105px" width="135px;" src="<?php echo $_SERVERXIMA.'img/nophotocasa.jpg';?>"></a></div>'+
							'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
							'  <tr> '+
							'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
							'    <td>'+data[k].get('pin_lsqft')+'</td>'+
							'  </tr><tr> '+
							'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
							'    <td>'+data[k].get('pin_larea')+'</td>'+
							'  </tr><tr> '+
							'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
							'    <td>'+data[k].get('pin_bed')+'/'+data[k].get('pin_bath')+'</td>'+
							'  </tr> <tr> '+
							'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
							'    <td>'+data[k].get('pin_saleprice')+'</td>'+
							'  </tr> <tr> '+
							'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
							'    <td>'+lsImgCss[indImgCss].explain+'</td>'+
							'  </tr> <tr> '+
							'    <td colspan=2 style=" font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" onclick="createOverview(<?php echo $county;?>,\''+data[k].get('pid')+'\',\''+status+'\','+user_web+',true);">Click here for Overview Comp</td>'+
							'  </tr></table></div>',
							'',
							'',
							''
						);
						mapOverview.ClearInfoBoxStyles();
						mapOverview.AddPushpin(pin);
						arrLatLong.push(new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')));
					}
				}
				
				mapOverview.Resize(system_width,350);
				mapOverview.SetMapView(arrLatLong);
				
				<?php if(!isset($par_no_conexion)){?>
				if ( Ext.fly(Ext.fly(gridDistress.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
					AllCheckDistress=true;
					gridDistress.getSelectionModel().selectAll();
				}else{
					AllCheckDistress=false;
					var sel = [];
					if(selected_dataDistress.length > 0){
						for(val in selected_dataDistress){
							var ind = gridDistress.getStore().find('pid',selected_dataDistress[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridDistress.getSelectionModel().selectRows(sel);
					}
				}
				<?php }?>
				var alto = parseInt(data.length*22)+70;
				gridDistress.setHeight(alto);
				
			}
		}
    });
	
	<?php if(!isset($par_no_conexion)){?>
	var selected_dataDistress = new Array();
	var AllCheckDistress=false;
	
	var smDistress = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataDistress.indexOf(record.get('pid'))==-1)
					selected_dataDistress.push(record.get('pid'));
				
				if(Ext.fly(gridDistress.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckDistress=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataDistress = selected_dataDistress.remove(record.get('pid'));
				AllCheckDistress=false;
			}
		}
	});
	<?php }?>
	
	var gridDistress = new Ext.grid.GridPanel({
		renderTo: '<?php echo $grid_render2; ?>',
		cls: 'grid_comparables',
		width: 'auto',
		height: 300,
		store: storeDistress,
		columns: [],
		<?php if(!isset($par_no_conexion) && !$overview_comp){?>
		sm: smDistress,
		<?php }?>
		listeners: {
			"mouseover": function(e) {
				var row;
				if((row = this.getView().findRowIndex(e.getTarget())) !== false){
					var record = this.store.getAt(row);
					var id = record.get('status').split('_');
					var numeroPin=id[0];
					
					document.getElementById("PinCompDistress_"+numeroPin+"_"+record.get('pid')+"_<?php echo $db_data;?>_"+<?php if(isset($par_no_conexion)) echo 'guidDistressR'; elseif(isset($overview_comp)) echo 'guidDistressC'; else echo 'guidDistress';?>).onmouseover();
				}
			},
			"mouseout": function(e) {
				VEPushpin.Hide();
			}
			<?php if(!isset($par_no_conexion)){?>,
			"rowdblclick": function(grid, row, e) {
				var record = this.store.getAt(row);
				var pid = record.get('pid');
				var status = record.get('status').split('_')[1].split('-')[0];
				createOverview(<?php echo $county;?>,pid,status,user_web,true);
			}
			<?php }?>
		}
		<?php if($do_paging){?>,
		tbar: new Ext.PagingToolbar({
			id: '<?php echo $pagin_comp_tol;?>',
            pageSize: limitDistress,
            store: storeDistress,
            displayInfo: true,
			displayMsg: 'Total: {2} Properties',
			emptyMsg: "No properties to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 properties per page.',
				text: 50,
				handler: function(){
					limitDistress=50;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_compDistress_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 properties per page.',
				text: 80,
				handler: function(){
					limitDistress=80;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_compDistress_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 properties per page.',
				text: 100,
				handler: function(){
					limitDistress=100;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
					Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_compDistress_group'
			})
			]
        })
		<?php }?>
	});
	
	storeDistress.on('metachange', function(){
		if(typeof(storeDistress.reader.jsonData.columns) === 'object') {
			var columns = [];
			<?php if(!isset($par_no_conexion)){?>
			columns.push(smDistress);
			<?php }?>
			Ext.each(storeDistress.reader.jsonData.columns, function(column){
				columns.push(column);
			});
			gridDistress.getColumnModel().setConfig(columns);
		}
	});

	<?php if($do_paging){?>
		storeDistress.load({params:{start:0, limit:limitDistress}});
	<?php }else{?>
		storeDistress.load();
	<?php }?>


	<?php if(!isset($par_no_conexion)){?>
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_compdistress_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_compdistress_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
	<?php }?>
</script>