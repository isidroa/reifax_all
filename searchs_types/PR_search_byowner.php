<?php 	
	include('../Data_conexion.php');
	conectar();
	
	 $realtor=$_POST['userweb']=="false" ? false:true;	
	 $fondo_search=$_POST['fondo_search'];
	$search_type = $_POST['search_type'];
	$search_by_type = $_POST['search_by_type'];
	$state_search = $_POST['state_search'];
	$county_search = $_POST['county_search'];
	
?>
<form enctype="application/x-www-form-urlencoded" method="post" id="pformul" action="properties_coresearch.php">
<div align="left" class="search_realtor_fondo">
    	<table width="620" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
        	<tr>    	    	
            	<td colspan="4" style="color:#FFF; font-weight:bold;">Properties</td>                
                <td colspan="6" style="color:#FFF; font-weight:bold;">Search By</td>                
                <td colspan="2" style="color:#FFF; font-weight:bold;">State</td>         
                <td width="130" style="color:#FFF; font-weight:bold;">County</td>      	
           	</tr>
            <tr>    	    	
            	<td colspan="3"><select name="combo_search_types" id="combo_search_types" style="width:115px;" onchange="doSearchTypeFilter(this.value);">
                <? if (!$realtor){ ?> 
                <option value="PR" <?php if($search_type=='PR') echo 'selected="selected"';?>>Public Records</option> 
                <?php }?>
                <option value="FS" <?php if($search_type=='FS') echo 'selected="selected"';?>>For Sale</option>
                <option value="FR" <?php if($search_type=='FR') echo 'selected="selected"';?>>For Rent</option>
                <? if (!$realtor){ ?> 
                	<option value="FO" <?php if($search_type=='FO') echo 'selected="selected"';?>>Foreclosures</option> 
				<? } ?>
                </select></td>
                <td width="5">&nbsp;</td>
                <td colspan="5"><select name="combo_search_by" id="combo_search_by" style="width:105px;" onchange="doSearchByFilter(this.value);">
                <option value="LOCATION" <?php if($search_by_type=='LOCATION') echo 'selected="selected"';?>>Location</option>
                <?php if($search_type=='PR' || $search_type=='FO'){?><option value="PARCELID" <?php if($search_by_type=='PARCELID') echo 'selected="selected"';?>>Parcelid</option><?php }?>
                <?php if($search_type=='FS' || $search_type=='FR'){?><option value="MLNUMBER" <?php if($search_by_type=='MLNUMBER') echo 'selected="selected"';?>>Mlnumber</option><?php }?>
                <?php if($search_type=='FO'){?><option value="CASE" <?php if($search_by_type=='CASE') echo 'selected="selected"';?>>Case-Number</option><?php }?>
                <? if (!$realtor){ ?> 
                <option value="MAP" <?php if($search_by_type=='MAP') echo 'selected="selected"';?>>Map</option>
                <? } ?>
                </select></td>
                <td width="5">&nbsp;</td>
                <td width="127"><select name="state_search" id="state_search" style="width:120px;" onchange="doChangeState(this.value);">
                <?php
					$query='select idstate,state FROM xima.lsstate where is_showed="Y" and idstate=1 order by state';
					$result=mysql_query($query) or die($query.mysql_error());
					$xs=0;
					while($r=mysql_fetch_array($result)){
						if($state_search==$r['idstate'])
							echo '<option value="'.$r['idstate'].'" selected="selected">'.$r['state'].'</option>';
						else
							echo '<option value="'.$r['idstate'].'">'.$r['state'].'</option>';
					}
				?>
                </select></td>
                <td width="5">&nbsp;</td>
                <td width="130"><select name="county_search" id="county_search" style="width:110px;" onchange="if(search_by_type=='MAP') mapSearch.centerMapCounty(this.value,true); else mapSearch.centerMapCounty(this.value,false); search_county=this.value;">
                <?php
					$query='select idcounty,county FROM xima.lscounty where ximapro="Y" and idstate='.$state_search.' order by county';
					$result=mysql_query($query) or die($query.mysql_error());
					$xs=0;
					while($r=mysql_fetch_array($result)){
						if($county_search==$r['idcounty'])
							echo '<option value="'.$r['idcounty'].'" selected="selected">'.$r['county'].'</option>';
						else
							echo '<option value="'.$r['idcounty'].'">'.$r['county'].'</option>';
					}
				?>
                </select></td>
           	</tr>
            <tr>    	    	
            	<td colspan="10" style="color:#FFF; font-weight:bold;">
                	<span id="tsearch_l">Location</span>
              	</td>                                
                <td colspan="2" id="tproptype" style="color:#FFF; font-weight:bold;">Type</td>                 
                <td width="130" id="tpropfore" style="color:#FFF; font-weight:bold;">Foreclosure Status</td> 
                	
          	</tr>        
            <tr>             	
            	<td colspan="9">
                	<input type="text" name="search" id="search" size="40" maxlength="2048" style="font-size:17px;" value="Address, City or Zip Code" onfocus="colocarDefault('search',this.value);" onblur="colocarDefault2('search',this.value);">
               	</td>                
                <td width="5">&nbsp;</td>               
                <td width="127">
               
                	<select id="proptype" name="proptype" onchange="verEquity(this.value,'pequity','tpequity');" style="width:120px;"><option value="">Any Type</option><option value="01">Single Family</option><option value="04">Condo/Town/Villa</option><option value="03">Multi Family +10</option><option value="08">Multi Family -10</option><option value="11">Commercial</option><option value="00">Vacant Land</option><option value="02">Mobile Home</option><option value="99">Other</option></select>
                    
              	</td>   	        
                <td width="5">&nbsp;</td>        
                <td width="130">
               
                	<select id="pendes" name="pendes" style="width:110px;"><option value="-1">Any</option><option value="N">No</option><option value="P">Pre-Foreclosed</option><option value="F">Foreclosed</option></select>
                   
               	</td>   	
         	</tr>        
            <tr>    	    	
            	<td colspan="4" id="tprice" style="color:#FFF; font-weight:bold;">Sold Range</td>                
                <td colspan="2" id="tbeds" style="color:#FFF; font-weight:bold;">Beds</td>                
                <td colspan="2" id="tbath" style="color:#FFF; font-weight:bold;">Baths</td>                
                <td colspan="2" id="tsqft" style="color:#FFF; font-weight:bold;">Sqft</td> 
                     
                <td colspan="2" id="tpequity" style="color:#FFF; font-weight:bold;">Debt Equity</td>
				                    	
                <td width="130">&nbsp;</td>
          	</tr>        
            <tr>        
            	<td width="50" valign="middle">
                	<span id="price_dol1" style="color:#FFF;vertical-align:top;">$</span><input name="price_low" type="text" id="price_low" style="width:40px;" onfocus="if(this.value=='min')this.value='';" onblur="if(this.value=='')this.value='min';" value="min" size="6">
      </td>        
                <td width="18" id="price_to" style="text-align:center;color:#FFF;">to</td>        
                <td width="50" valign="middle">
                	<span id="price_dol2" style="color:#FFF;vertical-align:top; ">$</span><input id="price_hi" style="width:40px;" type="text" name="price_hi" value="max" size="6" onfocus="if(this.value=='max')this.value='';" onblur="if(this.value=='')this.value='max';">
               	</td>        
            <td width="5">&nbsp;</td>        
                <td width="63" valign="middle">
                	<select id="bed" name="bed" style=""><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
               	</td>        
              <td width="5">&nbsp;</td>        
                <td width="63" valign="middle">
                	<select id="bath" name="bath" style=""><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
               	</td>        
              <td width="5" valign="middle">&nbsp;</td>        
                <td width="93" valign="middle">
               	  <select id="sqft" name="sqft" style=""><option value="-1">Any</option><option value="250">250+</option><option value="500">500+</option><option value="1000">1,000+</option><option value="1250">1,250+</option><option value="1500">1,500+</option><option value="1750">1,750+</option><option value="2000">2,000+</option><option value="2250">2,250+</option><option value="2500">2,500+</option><option value="2750">2,750+</option><option value="3000">3,000+</option><option value="3250">3,250+</option><option value="3500">3,500+</option><option value="3750">3,750+</option><option value="4000">4,000+</option><option value="5000">5,000+</option><option value="10000">10,000+</option></select>
               	</td>       	
              <td width="5" valign="middle">&nbsp;</td>            	
                <td width="127" valign="middle">
                
                <select name="pequity" id="pequity" style="width:118px;"><option value="-1">Any</option><option value="<-10">Less Than -10</option><option value="<0">Less Than 0</option><option value=">=0">Equal or Greater 0</option><option value=">10">Greater Than 10</option><option value=">20">Greater Than 20</option><option value=">30">Greater Than 30</option><option value=">40">Greater Than 40</option><option value=">50">Greater Than 50</option><option value=">60">Greater Than 60</option></select>
			
                </td>        
              <td width="5">&nbsp;</td>    	
                <td width="130" style="padding-top:0px;">
                	<img src="img/button_search.png" alt="Xima Usa Search" onClick="searchForm('search','P');">
                    <input type="hidden" name="mapa_search_latlong" id="mapa_search_latlong" value="-1" />
               	</td>   	
          	</tr>    
     	</table>
   	</div>
</form>
<div id="mapSearch" style=" display:none;width:100%;height:320;border: medium solid #b8dae3;position:relative; margin-bottom:5px;"></div>
<script> 
	_IniMAP(); 
	if(document.getElementById('county_search').value!=-1) OcultarMostrar(true);
</script>