<?php
	$url_county 	= $_GET['county'];
	$url_city 		= $_GET['city'];
	$url_zip 		= $_GET['zip'];
	$url_address 	= $_GET['address'];
	
	$url_city = $url_city=='NOCITY' ? 'NO-CITY' : $url_city;
	$url_zip = $url_zip=='NOZIP' ? 'NO-ZIP' : $url_zip;
	$url_address = $url_address=='NOADDRESS' ? 'NO-ADDRESS' : $url_address;
	
	$county = trim(str_replace('-',' ',$url_county)); 
	$city = trim(str_replace('-',' ',$url_city)); 
	$zip = trim(str_replace('-',' ',$url_zip));
	$address = trim(str_replace('-',' ',$url_address));
	$pid = $_GET['pid'];
	
	if(strpos($pid,'.php')!==false){
		$php = explode('.',$pid);
		$add = explode('-',$php[0]);
		
		$pid = $add[(count($add)-1)];
	}
	
	require_once($_SERVER['DOCUMENT_ROOT']."/resources/template/template.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	
	$db=conectarPorNameCounty($county);
	$sql_comparado="Select 
	p.state, p.address,p.city,p.zip,p.xcoded, m.remark, m.agent, m.officeemail, 
	p.xcode,p.beds,p.bath,p.sqft,p.price,ps.waterf,ps.pool,p.unit, m.status, 
	l.latitude,l.longitude,ma.marketvalue
	FROM properties_php p
	LEFT JOIN psummary ps ON (p.parcelid=ps.parcelid)
	LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
	LEFT JOIN marketvalue ma ON (p.parcelid=ma.parcelid)
	LEFT JOIN latlong l ON (p.parcelid=l.parcelid)
	Where p.parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$r= mysql_fetch_array($res);
	
	$currentMOD='PS_';
	$state=$r['state'];
	$unit=$r['unit'];
	$xcoded=$r['xcoded'];
	$marketvalue=$r['marketvalue'];
	
	$xcode=$r['xcode'];
	$beds=$r['beds'];
	$bath=$r['bath'];
	$sqft=$r['sqft'];
	$price=$r['price'];
	$pendes=$r['pendes'];
	$debttv=$r['debttv'];
	$pendesTxt = ($pendes=='N' ? 'Not Forclosures' : ($pendes=='P' ? 'Pre-foreclosed' : 'Foreclosures'));
	
	
	$sql1="SELECT MORTGAGE.parcelid FROM MORTGAGE WHERE MORTGAGE.PARCELID='$pid'";
	$res1 = mysql_query($sql1) or die(mysql_error());
	$mortgage =mysql_num_rows($res1);
	
	$sql2="select pendes.parcelid from pendes where pendes.parcelid='$pid' and (pof='F' or pof='P')";
	$res2 = mysql_query($sql2) or die(mysql_error());
	$pendes =mysql_num_rows($res2);	
	
?>
<!DOCTYPE HTML>
<html>
    <!-- Head of ReiFax Website -->
    <?php ReiFaxHeadExtjs3(1,($address.', '.$city.', FL, '.$zip.' - REIFax.com'),('Home value and property details for '.$address.', '.$city.', FL, '.$zip.', '.$county.', '.$xcoded.', '.$pendesTxt.' - '.$sqft.' sq ft, '.$beds.' bedrooms, '.$bath.' bathrooms.'),($address.', '.$city.', FL, '.$zip.', '.$county.', '.$xcoded.', '.$pendesTxt.', real estate, real estate search, home value, property details.'));?>
<body>
	<div class="container">
        <!-- Header of ReiFax Website -->
        <?php ReiFaxHeader();?>
        
        <!--ReiFax "You are here" Text  -->
        <div class="bluetext" id="heretext">
            <div class="cuote big">&nbsp;</div>
            You are here:
            <a href="http://www.reifax.com/" rel="nofollow">
                <span class="bluetext underline">ReiFax Home</span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county;?>/" rel="nofollow">
                <span class="bluetext underline"><?php echo $county;?></span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county.'/'.$url_city;?>/" rel="nofollow">
                <span class="bluetext underline"><?php echo $city;?></span></a> &gt; 
           	<a href="http://www.reifax.com/FLORIDA/<?php echo $url_county.'/'.$url_city.'/'.$url_zip;?>/" rel="nofollow">
                <span class="bluetext underline"><?php echo $zip;?></span></a> &gt; 
            <a href="http://www.reifax.com/FLORIDA/<?php echo $url_county.'/'.$url_city.'/'.$url_zip.'/'.$url_address;?>/" rel="nofollow">
                <span class="bluetext underline"><?php echo $address;?></span></a> &gt;
            
            <span class="fuchsiatext"><?php echo $pid;?></span>
        </div>
        
        <div id="contentPrincipal">
            <!-- Advertising Right Block-->
            <div class="sidebarright">
                            <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                                <img src="http://www.reifax.com/resources/img/advertise/D3.png">
                            </a>
                            <a href="http://www.reifax.com/company/contactUs.php">
                                <img src="http://www.reifax.com/resources/img/advertise/D6.png">
                            </a>
            </div>
            
            <div class="content" style="min-height:500px;">
                <div class="panel detailsTitle">
                    <div class="center">
                        <div id="map">
                            
                        </div>
                        <div class="describe">
                            <div class="title">
                                 <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>
                            </div>
                            <div class="relevante">
                                <?php echo $sqft.' sqft / '.$beds.' beds / '.$bath.' baths / '.$xcoded;?>    
                            </div>
                            <div class="price">
                                <span>
                                    <?php echo '$'.number_format(round($marketvalue),0,'.',',');?>
                                </span>
                            </div>          
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <!-- inicio del overview//-->
                <div class="overviewDetails" style="width:670px; overflow:hidden;">
                    <div>
                         <ul class="css-tabs small" id="detailsOverview">
                            <li>
                                <a class="current" id="overviewDetailInit" href="#tabDetailsOverview">Details</a>
                            </li>
                            <li>
                                <a href="#tabComparablesOverview" rel="views/comparableDetails.php?xcode=+<?php echo $xcode ?>">Comparables</a>
                            </li>
                            <li>
                                <a class="" href="#tabComparablesAcOverview" rel="">Comp. Active</a>
                            </li>
                            <li>
                                <a class="" href="#tabDistressOverview" rel="">Distress</a>
                            </li>
                            <li>
                                <a class="" href="#tabComparablesRenOverview" rel="">Comp. Rental</a>
                            </li>
                            <?php
                                if($mortgage>0)
                                {
                                    echo '
                                    <li>
                                        <a class="" href="#tabMortgageOverview" rel="">Mortgage</a>
                                    </li>';
                                }
                                if($pendes>0)
                                {
                                    echo '
                                    <li>
                                        <a class="" href="#tabForeclosureOverview" rel="">Foreclosure</a>
                                    </li>
                                    ';
                                }
                            ?>
                         </ul>
                     </div>
                    <div class="panes mediunTabs">
                        <div id="tabDetailsOverview">
                        </div>
                        <div id="tabComparablesOverview">
                        </div>
                        <div id="tabComparablesAcOverview" class="restricted">
                        </div>
                        <div id="tabDistressOverview" class="restricted">
                        </div>
                        <div id="tabComparablesRenOverview" class="restricted">
                        </div>
                        <?php
                                if($mortgage>0)
                                {
                                    echo '
                                    <div id="tabMortgageOverview" class="restricted">
                                    </div>';
                                }
                                if($pendes>0)
                                {
                                    echo '
                                    <div id="tabForeclosureOverview" class="restricted">
                                    </div>
                                    ';
                                }
                            ?>
                    </div>
                </div>
      	</div>
  	</div>
</body>
</html>
<style>
	a.MapPushpinBase div {
	  color: #000000 !important;
	  font-size: 9pt !important;
	}
</style>
<script src="/includes/properties_draw.js"></script>

<script language="javascript">
	var dirPro="<?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>";
	var countyBuy="<?php echo $county ?>";
	var parcelIdBuy="<?php echo $pid ?>";
	var user_web=false;
	
	$(document).ready(function (){
		
		loadPanel($('#detailsOverview'));
		var	map = new XimaMap('map','map_mapa_search_latlong','_control_mapa_div','_pan','_draw','_poly','_clear','_maxmin','_clear');
		map._IniMAPOverview(new Microsoft.Maps.Location(<?php echo $r['latitude'].','.$r['longitude'];?>),'birdseye');
		
		getCasita("A","N","N");
		
		map.addPushpin(
			<?php echo $r['latitude'].','.$r['longitude'];?>,
			'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
			''
		);

		$('#tabDetailsOverview').load('/overview/views/overviewDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>');
		$('#tabComparablesOverview').load('/overview/views/comparableDetails.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>&xcode=<?php echo $xcode ?>');
		$('.restricted').load('/overview/views/restricted.php?county=<?php echo $county ?>&pid=<?php echo $pid ?>');

	});
</script>