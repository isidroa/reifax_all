<?php
$_SERVERXIMA="://www.reifax.com/";
//if(isset($_GET['webuser'])){$r=true;}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES CON MAPA
	function tagHeadHeader($mapa=true,$secure=false)
	{
		global $_SERVERXIMA;
		$link = /*$secure ? 'https' : */'http';
		?>
    	<title>REIFAX | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
         <!--<meta content='IE=8' http-equiv='X-UA-Compatible'/>--> <!--Comentada por Jesus-->
         <!--<meta content='IE=9' http-equiv='X-UA-Compatible'/>--> <!--Agregada por Jesus-->
<?php
		if(isset($_COOKIE['datos_usr']['cleancache']) && $_COOKIE['datos_usr']['cleancache']=='Y')
		{
?>
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<?php
		}
?>
        <meta http-equiv="Content-Language" content="EN-US">
		<!--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">
		<link rel="shortcut icon" type="image/ico" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>favicon.png">


        <?php if($mapa){?>
        	<!--<script charset="UTF-8" type="text/javascript" src="<?php echo $link;?>://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>-->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4&libraries=drawing"></script>
        <?php }?>
		<!-- ExtJS -->
		<script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/ext-all.js"></script>
        <script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/examples/ux/ux-all.js"></script>
        <script>
			Ext.override(Ext.form.ComboBox, {
				editable: false
			});
		</script>

        <!-- JQuery -->
        <script type="text/javascript" language="javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		<script type="text/javascript" language="javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/jquery.carouFredSel-4.3.3-packed.js"></script>

        <!-- htmlEditor -->
        <script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/examples/form/plugins/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
        <script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/examples/form/plugins/Ext.ux.form.HtmlEditor.Word.js"></script>

        <!-- data view plugins -->
		<script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/ux/DataView-more.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/ux/data-view.css"/>

        <!-- fileuploadinput -->
        <!--<script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/ux/FileUploadField.js"></script>-->

        <!-- geolocalización -->
     	<!--<script src="<?php echo $link;?>://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>-->
        <!--<script type="text/javascript" src="<?php echo $link;?>://maps.google.com/maps/api/js?sensor=true"></script>-->
        <script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/geo.js"></script>

        <!-- Functions Emails -->
        <script type="text/javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/myEmail.js?<?php echo filemtime(dirname(__FILE__).'/../includes/myEmail.js'); ?>"></script>

        <!--<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/ux/fileuploadfield.css"/>-->
    	<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>MANT/LIB/ext3.4/resources/css/xtheme-xima.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/css/layout2.css?<?php echo filemtime(dirname(__FILE__).'/../includes/css/layout2.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/css/properties_css.css?<?php echo filemtime(dirname(__FILE__).'/../includes/css/properties_css.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/css/advertising_css.css?<?php echo filemtime(dirname(__FILE__).'/../includes/css/advertising_css.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>includes/css/jquery-ui-1.8.18.custom.css" />

        <!-- Functions Videos -->
		<link rel="stylesheet" type="text/css" href="<?php echo $link;?><?php echo $_SERVERXIMA;?>resources/css/cssvideos.css?<?php echo filemtime(dirname(__FILE__).'/../resources/css/cssvideos.css'); ?>" />
		<script type="text/javascript" language="javascript" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>resources/js/flowplayer-3.2.6.min.js"></script>

	<!--Functions Edit PDF's-->
		<script>
			if (typeof Range.prototype.createContextualFragment == "undefined") {
			    Range.prototype.createContextualFragment = function (html) {
			        var doc = window.document;
			        var container = doc.createElement("div");
			        container.innerHTML = html;
			        var frag = doc.createDocumentFragment(), n;
			        while ((n = container.firstChild)) {
				  frag.appendChild(n);
			        }
			        return frag;
			    };
			}
		</script>
		<!-- System Global Functions By Jesus-->
    <script type="text/javascript" src="/includes/globalFunctions.js?<?php echo filemtime(dirname(__FILE__).'/../includes/globalFunctions.js'); ?>"></script>
		
		<script src="/custom_contract/includes/js/kineticNew.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/includes/js/kineticNew.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/compatibility.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/compatibility.js'); ?>"></script>
		<!-- In production, only one script (pdf.js) is necessary -->
		<!-- In production, change the content of PDFJS.workerSrc below ->
		<script type="text/javascript" src="/custom_contract/src/core.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/core.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/util.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/util.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/api.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/api.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/canvas.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/canvas.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/obj.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/obj.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/function.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/function.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/charsets.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/charsets.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/cidmaps.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/cidmaps.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/colorspace.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/colorspace.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/crypto.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/crypto.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/evaluator.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/evaluator.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/fonts.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/fonts.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/glyphlist.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/glyphlist.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/image.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/image.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/metrics.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/metrics.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/parser.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/parser.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/pattern.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/pattern.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/stream.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/stream.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/worker.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/worker.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/external/jpgjs/jpg.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/external/jpgjs/jpg.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/jpx.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/jpx.js'); ?>"></script-->

		<script type="text/javascript" src="/custom_contract/src/shared/util.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/shared/util.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/api.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/api.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/metadata.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/metadata.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/canvas.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/canvas.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/webgl.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/webgl.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/pattern_helper.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/pattern_helper.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/font_loader.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/font_loader.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/annotation_helper.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/annotation_helper.js'); ?>"></script>


		<style type="text/css">
			.search-item{border:1px solid #fff;padding:3px;background-position:right bottom;background-repeat:no-repeat;}
			.desc{padding-right:10px;}
			.name{font-size:16px !important;color:#000022;}

			li:hover .subMenuHelp{
				visibility:visible;
			}
			li:hover .textHelpMenu{
				display:none;
			}
			.subMenuHelp{
				visibility:hidden;
				position:relative;
				border: 1px solid #888888;
				box-shadow: 0 0 2px;
				position: relative;
			}
			.subMenuHelp li{
				float:none;
				display:block;
				padding:3px 5px;
			}

			.subMenuHelp li:hover{
				background:#D0FF9D;
				color:#212121;
			}

			.subMenuHelp li:hover a{
				color:#212121;
			}
		</style>

		<script type="text/javascript">
			// Specify the main script used to create a new PDF.JS web worker.
			// In production, change this to point to the combined `pdf.js` file.
			PDFJS.workerSrc = '/custom_contract/src/worker_loader.js?v=280';
		</script>

        <!--El Siguiente JS es para el Control de los Navegadores-->
        <script type="text/javascript" src="/custom_contract/includes/js/navegadores.js"></script>
	<!--END Functions Edit PDF's-->

<?php 	//handlingError();
	}

	function topLoginHeader($advertising=true, $aboutus=true, $login=true, $register=true, $usa=true, $contactus=true, $livesupport=true, $trainning=true, $secure=false, $tickets=true,$help=true)
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		$link = /*$secure ? 'https' : */'http';

		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
			$result=mysql_query($que) or die($que.mysql_error());
			$r=mysql_fetch_array($result);
			$htmls=$r['master_htmls'];?>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
	                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
    	  	</table>
           	<p>&nbsp;</p>

		<? }else{   ?>
			<div align="center" style="height:110px; width:100%; position:relative; padding-top:10px;">

                <div style="float:left;">
                    <a href="properties_search.php<?php echo $realtor ? '?webuser='.$realtorid : ($investor ? '?webowner='.$investorid : '');?>" target="_self">
                    <?php if ($realtor){
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?><?php echo $_SERVERXIMA;?><?php echo "$photo2";?>" width="240" alt="Company Realtor Web">
                    <?php } elseif ($investor){
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?><?php echo $_SERVERXIMA;?><?php echo "$photo2";?>" width="240" alt="Company Investor Web">
                    <?php } elseif ($webmaster){
						$query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
						$result = mysql_query($query) or die($query.mysql_error());
						$datos_usr=mysql_fetch_array($result);
						$photo=$datos_usr['profimg'];
						$photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?><?php echo $_SERVERXIMA;?><?php echo "$photo2";?>" width="240" alt="Company Webmaster Web">
					<?php }else{?>
                    	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/foreclosures-foreclosed-homes1.png" width="240" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                    <?php } ?>
                    </a>
                </div>

              <div align="left" class="reifaxMenu">
                	<ul>
                        <?php if($realtor || $investor){?>
                        	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?><?php echo $photo;?>" height="140px"  style="float:left;" />
                        <?php }?>

						<?php if($register){?>
                        <!--<li id="top_m_reg">
                        	<a href="properties_planspricing.php">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/shopcart.png" />
                                <br />
                                Plans /
                                <br />
                                Sign Up
                           	</a>
                        </li>-->
                        <?php }?>

                        <?php if($login){?>
                        <li id="top_m_login">
                            <a href="#" onClick="login_win.show(); return false;">
                                <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/login.png" />
                                <br />
                                Log
                                <br />
                                In
                            </a>
                        </li>

                        <li id="top_m_user" style="display:none;">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF; display:none;" title="My Account">
                            </a>

                        	<a href="#" onClick="session_release(); return false;" title="Log Out">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/logout.png" />
                                <br />
                                Log
                                <br />
                                Out
                            </a>
                        </li>
                        <?php }?>

                        <?php if($aboutus){?>
                        <li>
                            <a href="<?php echo $link;?><?php echo $_SERVERXIMA;?>settings/myProducts.php">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/about.png" />
                                <br />
                            	My
                                <br />
                                Products
                           	</a>
                        </li>
                        <?php }?>

                        <?php if($usa){?>
                        <li>
                        	<a href="<?php echo $link;?><?php echo $_SERVERXIMA;?>FLORIDA/state.php" target="_blank">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/properties.png" />
                                <br />
                            	USA
                            </a>
                        </li>
                        <?php }?>

                        <?php if($contactus){?>
                        <!--<li>
                            <a href="properties_contactus.php<?php echo $realtor ? '?webuser='.$realtorid : ($investor ? '?webowner='.$investorid : '');?>">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/contact.png" />
                                <br />
                            	Contact
                                <br />
                                Us
                           	</a>
                        </li>-->
                        <?php }?>

                        <?php if($trainning){?>
                        <!--<li id="top_m_training" >
                        	<a href="properties_training.php" target="_blank">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/trainning.png" />
                                <br />
                            	Training
                            </a>
                        </li>-->
                        <?php }?>

                        <?php if($tickets){?>
                        <li id="top_m_training" >
                        	<a href="http<?php echo $_SERVERXIMA;?>settings/tickets.php" target="_blank">
                            	<img id="iconTicket" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/ticket.png" />
                                <br />
                            	Help<br>
                                Ticket
                            </a>
                        </li>
                        <script>
						$.ajax({
							url:'check_ticket.php',
							dataType: 'JSON',
							success:function (res){
								if(res.msg){
										$('#iconTicket').attr('src',"<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/ticketRed.png");
								}
								else{
										$('#iconTicket').attr('src',"<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/ticket.png");
								}
							}
						})
						</script>
                        <?php }?>

                        <?php if($help){?>
                        <li id="top_m_training" >
                        	<a href="/help/" target="_blank">
                            	<img id="iconTicket" src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/faq.png" />
                                <br />
                            	Help
                            </a>
                            <!--<ul class="subMenuHelp">
                            	<li>
                                	<a target="_blank" href="http://reifax.com/training/webinars.php">Webinars</a>
                                </li>
                            	<li>
                                	<a target="_blank" href="http://reifax.com/help">Help</a>
                                </li>
                            </ul>-->
                        </li>
                        <?php }?>
                        <?php if($livesupport){?>
                        <li>
                            <a href="#">
                            	<img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/menu/support_on.png" />
                                <br />
                            	Live
                                <br />
                                Support
                           	</a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
				<?php if($livesupport){?>
				<!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
				<div id="cijExP" style="z-index:100;position:relative; left:1px;"></div><div id="scjExP" style="no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:5px; right:8px;  margin: -2px 0px 0px 0px;"></div><div id="sdjExP" style="display:none"></div><script type="text/javascript">var sejExP=document.createElement("script");sejExP.type="text/javascript";var sejExPs=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/0htcpga2mc7g91j73xzaeq7b9r/safe-standard.js?ps_h=jExP&ps_t="+new Date().getTime();setTimeout("sejExP.src=sejExPs;document.getElementById('sdjExP').appendChild(sejExP)",1)</script><noscript><div style="display:inline"><a href="<?php echo $link;?>://www.providesupport.com?messenger=0htcpga2mc7g91j73xzaeq7b9r">Live Support</a></div></noscript>
				<!-- END ProvideSupport.com Graphics Chat Button Code -->
                <!-- BEGIN ProvideSupport.com Graphics Chat Button Code--
                <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:5px; right:8px;  margin: -2px 0px 0px 0px; "></div><div id="sdSlBh"></div> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <noscript><div style="display:inline"><a href="<?php echo $link;?>://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
                <!-- END ProvideSupport.com Graphics Chat Button Code -->
                <?php }?>
            </div>

            <?php if($advertising){?>

            <!--<div class="image_carousel" id="image_carousel">
                <div id="carrouselTop">
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/lead generator.jpg" width="660" height="300" onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';" />
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/mobile app.jpg" width="660" height="300" onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';"  />
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/buyer list.jpg" width="660" height="300"  onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';" />
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/contract generator.jpg" width="660" height="300"  onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';" />
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/Home Deals Finder.jpg" width="660" height="300"  onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';" />
                    <img src="<?php echo $link;?><?php echo $_SERVERXIMA;?>img/advertising/foreclosure for sale.jpg" width="660" height="300"  onclick="document.location='<?php echo $link;?><?php echo $_SERVERXIMA;?>properties_planspricing.php';" />
                </div>
                <div class="clearfix"></div>
                <div class="pagination" id="carrouselTop_pag"></div>
            </div>


            <script type="text/javascript" language="javascript">
				$(function() {
					//	Basic carousel
					$('#carrouselTop').carouFredSel({
						items: 1,
						auto: {
							play: true,
							pauseDuration: 6000
						},
						scroll: {
							items: 1,
							duration: 600,
							pauseOnHover: true
						},
						pagination : {
							container	: "#carrouselTop_pag",
							duration	: 600
						}
					});
				});
			</script>-->
            <?php }?>
<?php
 } }


	 //infromacion para el google-analytics
	function googleanalytics()
	{
?>
        <script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-3129734-3']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
<?php
	}
?>
