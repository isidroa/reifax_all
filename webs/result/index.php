<?php
	require_once('../resources/template/template.php');
	$_GET['resultType'] = 'basic';
	$_GET['systemsearch'] = 'basic';
	set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot\\xima3'. PATH_SEPARATOR);
	include('../../../coresearch.php');
	//Variables for Paging
	$num_rows=$limit_cant_reg;
	$max_page_pagin=10;
	$num_page_pagin=$num_rows>0?ceil($num_rows_all/$num_rows):0;
	if($max_page_pagin>$num_page_pagin) $max_page_pagin=$num_page_pagin;
	$star_for=$max_page_pagin>0?floor($num_limit_page/$max_page_pagin)*$max_page_pagin:0;
	$end_for=$star_for+$max_page_pagin;
	if($end_for>$num_page_pagin) $end_for=$num_page_pagin;
	$star_page=($num_limit_page*$limit_cant_reg)+1;
	$tablaporder='psummary';
		if($type_search=='PR' || $type_search=='FO' || $type_search=='MO') $tablaporder='psummary';
		elseif($type_search=='FS') $tablaporder='mlsresidential';
		elseif($type_search=='FR'){	$tablaporder='rental';	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(1);?>
<style>
	.botonOrdenamiento{
		width:24px;
		height:24px;
		display:block;
		border: 1px solid #AAAAAA;
	}
	.botonOrdenamiento:hover{
		border: 1px solid #666666;
		background-color:#FFF;
		-moz-box-shadow: 1px 1px 2px #666666;
		-webkit-box-shadow: 1px 1px 2px #666666;
	}
	#result_orderby_asc{
		background: url(http://www.ximausa.com/MANT/LIB/ext/resources/images/default/grid/hmenu-asc.gif) center no-repeat;
    	border-radius: 4px 0 0 4px;
	}
	#result_orderby_desc{
		background: url(http://www.ximausa.com/MANT/LIB/ext/resources/images/default/grid/hmenu-desc.gif) center no-repeat;
    	border-radius: 0 4px 4px 0;
	}
</style>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Search</span>
    </div>
    <div id="contentPrincipal" style="min-height:1050px;">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/advertise.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
                         <span style="font-size:18px" class="title bold bluetext">
                            Properties Found: <?php echo $num_rows_all ?>
                         </span>    
            	</div>
        	</div>
        </div>
       <!--Text container-->
       <?php
			if($num_rows_all==0)
			{
				echo '
					<div class="centertext">
						<div class="clear"></div>
						<h2>Property Result: 0 (Please review your search and try again)</h2>
					</div>
					
				';
			}
			else
			{
				?>
       <div class="result2Option">
                	<div class="">
                         <div style="position:absolute; top:1px; left:10px;">
                            <div class="botonDesplazamiento">
                                <span class="bluetext">Order By:</span>
                            </div>
                            <div class="botonDesplazamiento">
                                <select id="orderByTo" style="width:120px; height: 22px;">
                                    <option value="ORDER BY p.county,p.address">County</option>
                                    <option value="ORDER BY p.address">Address</option>
                                    <option value="ORDER BY mlnumber">Mlnumber</option>
                                    <option value="ORDER BY p.parcelid">Parcel ID</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.yrbuilt">Year Built</option>
                                    <option value="ORDER BY dom">DOM</option>
                                    <option value="ORDER BY larea">Living Area</option>
                                    <option value="ORDER BY garea">Gross Area</option>
                                    <option value="ORDER BY tsqft">Lot Size</option>
                                    <option value="ORDER BY waterf">Water Front</option>
                                    <option value="ORDER BY pool">Pool</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.beds">Beds</option>
                                    <option value="ORDER BY <?php echo $tablaporder ?>.bath">Baths</option>
                                    <option value="ORDER BY p.city">City</option>
                                    <option value="ORDER BY p.zip">Zip Code</option>
                                    <option value="ORDER BY price">Price</option>
                                    <option value="ORDER BY p.xcoded">Property Types</option>
                                </select>
                            </div>
							<div class="botonDesplazamiento">
                                <a href="#" class="botonOrdenamiento" rel='ASC' id="result_orderby_asc">&nbsp;
                                </a>
                            </div>
                            <div class="botonDesplazamiento">
                                <a href="#" class="botonOrdenamiento" rel='DESC' id="result_orderby_desc">&nbsp;
                                </a>
                            </div>
                        </div>
                         <div style="position:absolute; top:1px; right:10px;">
                              <?php
                             if($num_limit_page>0)
                             {
                                 echo '
                                 <div class="botonDesplazamiento">
                                    <a href="?page=0&order='.$_GET['order'].'" class="buttongreen">|&lt;</a>
                                    <a href="?page='.($num_limit_page-1) .'&order='.$_GET['order'].'" class="buttongreen">&lt;</a>
                                </div>';
                             }
                             ?>
                             <div class="botonDesplazamiento">
                                   <select id="changePageTo" style="width:110px; height: 22px;">
                                     <?php
                                        for($j=0;$j<$num_page_pagin;$j++)
                                        {
                                            echo ($num_limit_page==$j)?'<option value="?page='. $j .'&order='.$_GET['order'].'" selected>Page '.($j+1).'</option>':'<option value="?page='. $j .'&order='.$_GET['order'].'">Page '.($j+1).'</option>';
                                        }
                                    ?>
                                     </select>
                             </div>
                             <?php
                             if($num_limit_page!=($num_page_pagin-1))
                             {
                                echo'
                                 <div class="botonDesplazamiento">
                                    <a href="?page='.($num_limit_page+1)  .'&order='.$_GET['order'].'" class="buttongreen">&gt;</a>
                                    <a href="?page='.($num_page_pagin-1)  .'&order='.$_GET['order'].'" class="buttongreen">&gt;|</a>
                                 </div>';
                             }
                             ?>
                         </div>
                    </div>
                </div>
       
       
       
       	<?php 
			}
			foreach($list_reg as $key => $val)
			{
				echo '
				<div class="result2">
					<div class="title"> <span class="price">$'.number_format($val['price'],0,'.',',').'</span> '.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].' <span class="Num">'.$i.'</span></div>
					<div class="view">
						<div id="map_result_'.$key.'" class="img" style="width:135px; height:135px; overflow:hidden; position:relative;">
						</div>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>City:</td>
								<td class="">'.$val['city'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Parcel ID:</td>
								<td class="">'.$val['parcelid'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Type:</td>
								<td class="">'.substr($val['xcoded'],0,25).'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Beds/Baths:</td>
								<td class="">'.$val['beds'].'/'.$val['bath'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Year Built:</td>
								<td class="">'.$val['yrbuilt'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Dom:</td>
								<td class="">'.$val['dom'].'</td>
							</tr>
						</table>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>County:</td>
								<td class="">'.$val['county'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Living Area:</td>
								<td class="">'.$val['larea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Gross Area:</td>
								<td class="">'.$val['garea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Lot Size:</td>
								<td class="">'.$val['tsqft'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Water Front:</td>
								<td class="">'.$val['waterf'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Pool:</td>
								<td class="">'.$val['pool'].'</td>
							</tr>
						</table>
					</div>
						<a href="#" rel="'.$val['parcelid'].'|'.$val['county'].'" class="buttonblue">View Details</a>
					<div class="clearEmpty"></div>
				</div>
					<div class="clearEmpty"></div>
				';
			$i++;
			}
		?>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script src="../resources/js/chosen.jquery.min.js"></script> 
<script src="/includes/properties_draw.js"></script>
<script language="javascript">
<?php if(count($list_reg)>0){?>

		<?php foreach($list_reg as $k => $val){	?>
			
			var	map_result_<?php echo $k;?> = new XimaMap('map_result_<?php echo $k;?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
			map_result_<?php echo $k;?>.map = new VEMap('map_result_<?php echo $k;?>');
			map_result_<?php echo $k;?>.map.LoadMap(new VELatLong(<?php echo $val['latitude'].','.$val['longitude'];?>), 1, VEMapStyle.Birdseye,true,VEMapMode.Mode2D,false);
			map_result_<?php echo $k;?>.map.Resize(135,135);
			map_result_<?php echo $k;?>.map.HideDashboard();
			map_result_<?php echo $k;?>.map.HideScalebar();
			
			getCasita(<?php echo '"'.$val['status'].'","'.$val['pendes'].'","'.$val['sold'].'"';?>);
			var pin = new VEShape(VEShapeType.Pushpin, new VELatLong(<?php echo $val['latitude'].','.$val['longitude'];?>));
			pin.SetCustomIcon('http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img);
			map_result_<?php echo $k;?>.map.AddShape(pin);
			
		<?php }?>
		
<?php }?>

	$(document).ready(function (){
		
		
		$('.result2').bind('mouseenter',function (){
			
			$(this).find('.buttonblue').slideDown(100).fadeIn(100);
		}).bind('mouseleave',function (){
			$(this).find('.buttonblue').slideUp(100).fadeOut(100)
		}).find('.buttonblue').bind('click',function (e){
			
			e.preventDefault();
			var data=$(this).attr('rel').split('|');
			$.cookie('parselid',data[0], { path: '/' });
			$.cookie('county',data[1], { path: '/' });
			window.location='../overview/index.php';
		});
		
		
		$('#changePageTo').chosen().change(function (){
			window.location=$(this).val();
		});
		$('#orderByTo').attr('value',(getUrlVars()['order'])?getUrlVars()['order'].replace(/(-)/gi,' '):'').chosen().change(ordenar);
		$('.botonOrdenamiento').bind('click',ordenar);
	});
	
function ordenar()
{
	var order=$('#orderByTo').val();
	var dir=($(this).attr('rel'))?$(this).attr('rel'):'ASC';
	
	$.ajax({
		url: '../../properties_order_by_result.php', 
		type: 'POST',
		data: 'order='+order+' '+dir,
		success: function ()
		{
			window.location='?page=0&order='+order.replace(/(\s)/gi,'-');
		}
	})
}
$.cookie('urlResult',window.location,{path:'/'});
		
</script>