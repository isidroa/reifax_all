<?php
	require_once('../resources/template/template.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Search</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/advertise.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Search</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       	<?php 
			$_GET['resultType'] = 'basic';
			$_GET['systemsearch'] = 'basic';
			set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot\\xima3'. PATH_SEPARATOR);
			include('../../coresearch.php');
			//echo $query;
			//print_r($list_reg);
			foreach($list_reg as $key => $val)
			{
				echo '
				<div class="result">
					<div class="title"> <span style="color:#ed9f18">$ '.number_format($val['price'],0,'.',',').'</span> '.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].'</div>
					<div class="view">
						<img src="">
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold">City:</td>
								<td class="">'.$val['city'].'</td>
							</tr>
							<tr>
								<td class="bold">Parcel ID:</td>
								<td class="">'.$val['parcelid'].'</td>
							</tr>
							<tr>
								<td class="bold">Type:</td>
								<td class="">'.substr($val['xcoded'],0,25).'</td>
							</tr>
							<tr>
								<td class="bold">Beds/Baths:</td>
								<td class="">'.$val['beds'].'/'.$val['bath'].'</td>
							</tr>
							<tr>
								<td class="bold">Year Built:</td>
								<td class="">'.$val['yrbuilt'].'</td>
							</tr>
							<tr>
								<td class="bold">Dom:</td>
								<td class="">'.$val['dom'].'</td>
							</tr>
						</table>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold">County:</td>
								<td class="">'.$val['county'].'</td>
							</tr>
							<tr>
								<td class="bold">Living Area:</td>
								<td class="">'.$val['larea'].'</td>
							</tr>
							<tr>
								<td class="bold">Gross Area:</td>
								<td class="">'.$val['garea'].'</td>
							</tr>
							<tr>
								<td class="bold">Lot Size:</td>
								<td class="">'.$val['tsqft'].'</td>
							</tr>
							<tr>
								<td class="bold">Water Front:</td>
								<td class="">'.$val['waterf'].'</td>
							</tr>
							<tr>
								<td class="bold">Pool:</td>
								<td class="">'.$val['pool'].'</td>
							</tr>
						</table>
					</div>
					<div class="clearEmpty"></div>
				</div>
				<div class="clear"></div>
				';
			}
		?>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
	$(document).ready(function (){
		/*
			valores por default
		*/
		
	});

</script>