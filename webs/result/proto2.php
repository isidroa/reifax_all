<?php
	require_once('../resources/template/template.php');
	$_GET['resultType'] = 'basic';
	$_GET['systemsearch'] = 'basic';
	set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot\\xima3'. PATH_SEPARATOR);
	include('../../coresearch.php');
	//Variables for Paging
	$num_rows=$limit_cant_reg;
	$max_page_pagin=10;
	$num_page_pagin=$num_rows>0?ceil($num_rows_all/$num_rows):0;
	if($max_page_pagin>$num_page_pagin) $max_page_pagin=$num_page_pagin;
	$star_for=$max_page_pagin>0?floor($num_limit_page/$max_page_pagin)*$max_page_pagin:0;
	$end_for=$star_for+$max_page_pagin;
	if($end_for>$num_page_pagin) $end_for=$num_page_pagin;
	$star_page=($num_limit_page*$limit_cant_reg)+1;
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Result</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">                    
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/advertise.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Properties Found: <?php echo $num_rows_all ?>
                     </span>
                     <div style="position:absolute; top:2px; right:10px;">
                         <?php
						 if($num_limit_page>0)
						 {
							 echo '<a href="?page=0" class="buttongreen">|&lt;</a>
							 <a href="?page='.($num_limit_page-1).'" class="buttongreen">&lt;</a>';
						 }
						 ?>
						 <select style="width:40px;">
                         <?php
						 	for($j=0;$j<$num_page_pagin;$j++)
							{
								echo '<option value="?page='. $j .'">'.($j+1).'</option>';
							}
						?>
                         </select>
                         <?php
						 if($num_limit_page==$num_page_pagin-1)
						 {
                         	echo'<a href="?page='.($num_limit_page+1) .'" class="buttongreen">&gt;</a>
                         	<a href="?page='.($num_page_pagin-1) .'" class="buttongreen">&gt;|</a>';
						 }
						 ?>
                     </div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       	<?php 
			$i=1;
			foreach($list_reg as $key => $val)
			{
				echo '
				<div class="result3">
					<div class="title"> <span class="price">$'.number_format($val['price'],0,'.',',').'</span> '.$val['address'].' '.$val['unit'].', '.$val['city'].', '.$val['zip'].' <span class="Num">'.$i.'</span></div>
					<div class="view">
						<img src="">
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>City:</td>
								<td class="">'.$val['city'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Parcel ID:</td>
								<td class="">'.$val['parcelid'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Type:</td>
								<td class="">'.substr($val['xcoded'],0,25).'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Beds/Baths:</td>
								<td class="">'.$val['beds'].'/'.$val['bath'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Year Built:</td>
								<td class="">'.$val['yrbuilt'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Dom:</td>
								<td class="">'.$val['dom'].'</td>
							</tr>
						</table>
					</div>
					<div class="view">
						<table>
							<tr>
								<td class="bold"><div class="cuote"></div>County:</td>
								<td class="">'.$val['county'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Living Area:</td>
								<td class="">'.$val['larea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Gross Area:</td>
								<td class="">'.$val['garea'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Lot Size:</td>
								<td class="">'.$val['tsqft'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Water Front:</td>
								<td class="">'.$val['waterf'].'</td>
							</tr>
							<tr>
								<td class="bold"><div class="cuote"></div>Pool:</td>
								<td class="">'.$val['pool'].'</td>
							</tr>
						</table>
					</div>
						<a href="#" class="buttongreen">View Details</a>
					<div class="clearEmpty"></div>
				</div>
				<div class="clearEmpty"></div>
				';
				$i++;
			}
		?>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
	$(document).ready(function (){
		$('.result3').bind('mouseenter',function (){
			console.debug(this);
			$(this).find('.buttongreen').slideDown(100).fadeIn(100);
		}).bind('mouseleave',function (){
			$(this).find('.buttongreen').slideUp(100).fadeOut(100)
		})
		/*
			valores por default
		*/
		
	});

</script>