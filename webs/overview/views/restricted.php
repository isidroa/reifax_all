<div class="titlePrincipal">
        	<span>
            	Privileged Information 
            </span>
        </div>
        <div class="history bold">
        	<p>
        	More property information for this property is available to our registered users or by purchasing our Home Fax Report. By purchasing our Home Fax Report you will get:
            </p>
            <div style="float:left; width:330px; height:60px; margin-top:16px;">
            	<ul>
                	<li>
                    	<div class="cuote"></div>Active Comparables
                    </li>
                	<li>
                    	<div class="cuote"></div>Distressed Comparables
                    </li>
                	<li>
                    	<div class="cuote"></div>Rental Comparables (if any)
                    </li>
                </ul>
            </div>
            <div style="float:left; width:330px; height:60px; margin-top:16px;">
            	<ul>
                	<li>
                    	<div class="cuote"></div>Owner Information
                    </li>
                	<li>
                    	<div class="cuote"></div>Mortgage Information (if any)
                    </li>
                	<li>
                    	<div class="cuote"></div>Foreclosure Information (if any)
                    </li>
                </ul>
            </div>
            
        </div>
        <div style="float:left; width:310px; height:150px; padding:0 10px; margin-top:20px; text-align:center">
        	<p class="bold">
            	To register for one of our REIFax products click on the "Sign Up" button below
            </p>
        	<a href="#" >
            	<img style=" height: 95px; margin: 5px 0; width: 290px;" src="../resources/img/logo-reifax.png">
            </a><br>
        	<a href="../store/index.php" class="buttonblue">Sign Up</a>
        </div>
        <div style="float:left; border-left: solid 1px #8AB420; margin-top:20px; width:310px; padding:0 10px; height:170px;text-align:center">
        	<p class="bold">
            	To buy the Home Fax Report click on the "Buy Now" button below
            </p>
        	<a href="#" >
            	<img src="../resources/img/HomeFax.png">
            </a><br>
        	<a href="../register/registerHomeFax.php"  class="buttonblue">Buy Now</a>
        </div>
<div class="clear">
</div>
<script>
	$('a[href$="registerHomeFax.php"]').attr('href','http://www.reifax.com/register/registerHomeFax.php?property='+dirPro+'&county='+countyBuy+'&pid='+parcelIdBuy);
</script>