<?php
	require_once('../resources/template/template.php');
	require_once("../resources/php/properties_conexion.php");
	$county=$_COOKIE['county'];
	$pid=$_COOKIE['parselid'];
	$db=conectarPorNameCounty($county);
	$sql_comparado="Select 
	p.state, p.address,p.city,p.zip,p.xcoded, m.remark, m.agent, m.officeemail, 
	p.xcode,p.beds,p.bath,p.sqft,p.price,ps.waterf,ps.pool,p.unit, m.status, 
	l.latitude,l.longitude,ma.marketvalue
	FROM properties_php p
	LEFT JOIN psummary ps ON (p.parcelid=ps.parcelid)
	LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
	LEFT JOIN marketvalue ma ON (p.parcelid=ma.parcelid)
	LEFT JOIN latlong l ON (p.parcelid=l.parcelid)
	Where p.parcelid='$pid';";	
	$res = mysql_query($sql_comparado) or die(mysql_error());
	$r= mysql_fetch_array($res);
	
	$currentMOD='PS_';
	$city=strlen($r['city'])>0 ? trim($r['city']):'NOCITY';
	$zip=strlen($r['zip'])>0 ? $r['zip']:'NOZIP';
	$address=strlen($r['address'])>0 ? trim($r['address']):'NOADDRESS';
	$state=$r['state'];
	$unit=$r['unit'];
	$xcoded=$r['xcoded'];
	$marketvalue=$r['marketvalue'];
	
	$xcode=$r['xcode'];
	$beds=$r['beds'];
	$bath=$r['bath'];
	$sqft=$r['sqft'];
	$price=$r['price'];
	$pendes=$r['pendes'];
	$debttv=$r['debttv'];
	
	
	$sql1="SELECT MORTGAGE.parcelid FROM MORTGAGE WHERE MORTGAGE.PARCELID='$pid'";
	$res1 = mysql_query($sql1) or die(mysql_error());
	$mortgage =mysql_num_rows($res1);
	
	$sql2="select pendes.parcelid from pendes where pendes.parcelid='$pid' and (pof='F' or pof='P')";
	$res2 = mysql_query($sql2) or die(mysql_error());
	$pendes =mysql_num_rows($res2);

	
?>
<!DOCTYPE HTML>
<html>
<!-- Head of ReiFax Website -->
<?php ReiFaxHeadExtjs3(1);?>
<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="#" class="returnResul">
           	<span class="bluetext underline">Result</span></a> &gt; 
        	<span class="fuchsiatext">Overview</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
    				<a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D6.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content" style="min-height:500px;">
        <div class="panel detailsTitle">
        	<div class="center">
              	<div id="map">
                 	
                </div>
                <div class="describe">
        	        <div class="title">
                    	 <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>
                    </div>
              		<div class="relevante">
						<?php echo $sqft.' sqft / '.$beds.' beds / '.$bath.' baths / '.$xcoded;?>    
                    </div>
                    <div class="price">
                    	<span>
                    		<?php echo '$'.number_format(round($marketvalue),0,'.',',');?>
                        </span>
                    </div>          
                </div>
                <div class="clear">
                </div>
            </div>
      	</div>
        <!-- inicio del overview//-->
        <div class="overviewDetails" style="width:670px; overflow:hidden;">
        	<div>
                 <ul class="css-tabs small" id="detailsOverview">
                    <li>
                        <a class="current" id="overviewDetailInit" href="#tabDetailsOverview">Details</a>
                    </li>
                    <li>
                        <a href="#tabComparablesOverview" rel="views/comparableDetails.php?xcode=+<?php echo $xcode ?>">Comparables</a>
                    </li>
                    <li>
                        <a class="" href="#tabComparablesAcOverview" rel="">Comp. Active</a>
                    </li>
                    <li>
                        <a class="" href="#tabDistressOverview" rel="">Distress</a>
                    </li>
                    <li>
                        <a class="" href="#tabComparablesRenOverview" rel="">Comp. Rental</a>
                    </li>
                    <?php
						if($mortgage>0)
						{
							echo '
							<li>
								<a class="" href="#tabMortgageOverview" rel="">Mortgage</a>
							</li>';
						}
						if($pendes>0)
						{
							echo '
							<li>
								<a class="" href="#tabForeclosureOverview" rel="">Foreclosure</a>
							</li>
							';
						}
					?>
                 </ul>
             </div>
            <div class="panes mediunTabs">
            	<div id="tabDetailsOverview">
                </div>
            	<div id="tabComparablesOverview">
                </div>
            	<div id="tabComparablesAcOverview" class="restricted">
                </div>
            	<div id="tabDistressOverview" class="restricted">
                </div>
            	<div id="tabComparablesRenOverview" class="restricted">
                </div>
                <?php
						if($mortgage>0)
						{
							echo '
							<div id="tabMortgageOverview" class="restricted">
							</div>';
						}
						if($pendes>0)
						{
							echo '
							<div id="tabForeclosureOverview" class="restricted">
							</div>
							';
						}
					?>
            </div>
        </div>
       <!--Text container-->
        <div id="mapResult" style="display:none;width:100%;height:320;border: medium solid #b8dae3;position:relative;margin-bottom:5px;"></div>
        <input type="hidden" name="result_mapa_search_latlong" id="result_mapa_search_latlong" value="-1" />
				
        
    </div>
	    <div class="clear" style="margin-top:10px;">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>

<script src="../resources/js/chosen.jquery.min.js"></script>
<script src="/includes/properties_draw.js"></script>

<script language="javascript">
	var dirPro="<?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit; echo ', '. $city.', '.$state.', '.$zip;?>";
	var countyBuy="<?php echo $_COOKIE['county'] ?>";
	var parcelIdBuy="<?php echo $_COOKIE['parselid'] ?>";
	var user_web=false;
	
	$(document).ready(function (){
		$('.returnResul').attr('href',$.cookie('urlResult'));
		loadPanel($('#detailsOverview'));
		var	map = new XimaMap('map','map_mapa_search_latlong','_control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
		map.map = new VEMap('map');
		map.map.LoadMap(new VELatLong(<?php echo $r['latitude'].','.$r['longitude'];?>), 15, VEMapStyle.Birdseye);
		map.map.Resize(350,200);
		getCasita("A","N","N");
		var pin = new VEShape(VEShapeType.Pushpin, new VELatLong(<?php echo $r['latitude'].','.$r['longitude'];?>));
		pin.SetCustomIcon('http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img);
		map.map.AddShape(pin); 
		map.map.HideDashboard();
		map.map.HideScalebar();
		
		$('#tabDetailsOverview').load('views/overviewDetails.php');
		$('#tabComparablesOverview').load('views/comparableDetails.php?xcode=<?php echo $xcode ?>');
		$('.restricted').load('views/restricted.php');
	});
</script>