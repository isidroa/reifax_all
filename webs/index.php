<?php
 $sufijo = explode('.',$_SERVER['SERVER_NAME']);
 //((($sufijo[0])=='www') || (($sufijo[0])=='reifax')) ? null : (header('Location: '.$_SERVER['SERVER_NAME'].'/properties_search.php'));
	require_once('resources/php/properties_conexion.php');
 	require_once('resources/template/template.php');
	
if(!$_COOKIE['userweb'] || $_COOKIE['userwebid']!=$_GET['user'])
{
	userweb();
}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body style="margin-top:10px;">

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: <span class="greentext">REI</span>Fax Home
    </div>
  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                <div id="advertisingFramework">
                    <a href="http://www.reifax.com/xima3/website/company/contactUs.php">
                    	<img src="https://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                </div>
        </div>
        <!-- Center Content-->
        <div class="content" style="min-height:250px;">
            <!-- Advertising Rotative Top-->
            <!--
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <!--
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C1.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C2.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C3.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C4.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C5.png) no-repeat;">
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                //-->
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    /*$(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });*/
                </script>
            <div class="clear">&nbsp;</div>
            <!--
            		INICIO DEL SEARCH
            //-->
                    <div>
                        <ul class="css-tabs small mediunTabs" id="Search">
                            <li>
                                <a class="current" href="#PR">Public Records</a>
                            </li>
                        	<?php 
							if($_COOKIE['userweb']['realtorweb'])
							{
								echo'
									<li>
										<a class="" href="#FO">Foreclosure</a>
									</li>';
							}
							?>
                        </ul>
                    </div>
                    <div class="panes reports">
                    	<div id="search1" style="display:block" class="title">
                        	<form id="formSearch">
                            <input type="hidden" name="typeSearch">
                        	<div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            State
                                            </label><br>
                                            <select class="fieldtext" name="stadeSearch" id="stadeSearch" style="width:146px">
                                            <?php
                                            conectar('xima');
                                                $sql="SELECT * FROM lsstate where is_showed='Y';";
                                                $resultado=mysql_query($sql);
                                                while($data=mysql_fetch_assoc($resultado))
                                                {
                                                    echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                                }
                                            ?>
                                            </select>
                                </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            County
                                            </label><br>
                                            <select id="SearchCounty" name="countySearch" class="fieldtext County" style="width:146px">
                                            </select>
                              	</div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Property Type 
                                            </label><br>
                                            <select name="propertySearch" class="fieldtext" style="width:146px">	
                                                    <option value="">Any Property Type</option>
                                                    <option value="01">Single Family</option>
                                                    <option value="04">Condo/Town/Villa</option>
                                                    <option value="03">Multi Family +10</option>
                                                    <option value="08">Multi Family -10</option>
                                                    <option value="11">Commercial</option>
                                                    <option value="00">Vacant Land</option>
                                                    <option value="02">Mobile Home</option>
                                                    <option value="99">Other</option>
                                            </select>
                                        </label>
                                </div>
                                <div class="contentText">
                                	<a href="search/index.php" target="_blank" id="buttonSearch" class="buttonblue bigButton">Search</a>
                                </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                            <div class="contentText">
                                	<label class="whitetext">
                                    	Location
                                   	</label><br>
                                    <input style="width:463px;" name="locationSearch" placeholder="Address, City or Zip Code" class="fieldtext" type="text">
                            </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Sqft 
                                            </label><br>
                                            <select class="fieldtext" name="sqftSearch" style="width:146px">
                                            	<option value="-1">Any</option>
                                                <option value="250" >250+</option>
                                                <option value="500" >500+</option>
                                                <option value="1000" >1,000+</option>
                                                <option value="1250" >1,250+</option>
                                                <option value="1500" >1,500+</option>
                                                <option value="1750" >1,750+</option>
                                                <option value="2000" >2,000+</option>
                                                <option value="2250">2,250+</option>
                                                <option value="2500">2,500+</option>
                                                <option value="2750" >2,750+</option>
                                                <option value="3000" >3,000+</option>
                                                <option value="3250" >3,250+</option>
                                                <option value="3500" >3,500+</option>
                                                <option value="3750" >3,750+</option>
                                                <option value="4000" >4,000+</option>
                                                <option value="5000" >5,000+</option>
                                                <option value="10000" >10,000+</option>
                                            </select>
                                        </label>
                                </div>
            				<div class="clear"></div>
                            <div>
                                	<div class="contentText">
                                    <label class="whitetext">Sold Range</label><br>
                                    <input class="fieldtext" placeholder="$ Min" name="MinSearch" style="width:146px">
                                    <input class="fieldtext" placeholder="$ Max" name="MaxSearch" style="width:146px">
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Beds</label><br>
                                    <select class="fieldtext" name="bedsSearch" style="width:146px;height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Baths</label><br>
                                    <select class="fieldtext" name="bathSearch" style="width:146px; height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                        </div>
                        </div>
                        </form>
                        <!-- fin de los tab//-->
                    
                    <!--
            	
                <div style="width:680px; overflow:hidden;">
                <div class="msgLoad">
                </div>
                <div>
                    <ul class="css-tabs" id="xray">
                        <li>
                            <a class="current" href="#panel1">X-Ray Report</a>
                        </li>
                        <li>
                            <a class="" href="#panel2">Discount Report</a>
                        </li>
                    </ul>
                </div>
                <div class="panes reports">
                	<div id="panel1" class="panelContenedor">
              		  <div class="title" style="height:30px; margin-bottom:0px; border-bottom:solid 2px #FFF;">
                         <div style="margin-left:45px; color:#FFF; font-weight:bold; font-size:13px;">
                            Looking for Real Estate Statistics? You are in the right place.<br>Specific Information for: <span class="selectProptype"></span> in <span class="selectCounty"></span> County, <span class="selectState"></span>. As of <span id="fechaEray"></span>
                          </div>	
                      </div>
                           <table>
                                    <tr>
                                        <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                                        <div style="float:left; margin-left:45px; color:#FFF; text-align:center;">
                                            <label class="whitetext">
                                    State
                                    </label><br>
                                    <select class="shortBox" id="State" name="state">
                                    <?php
                                        $sql="SELECT * FROM lsstate where is_showed='Y';";
                                        $resultado=mysql_query($sql);
                                        while($data=mysql_fetch_assoc($resultado))
                                        {
                                            echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    County
                                    </label><br>
                                    <select class="shortBox County" id="County" name="cunty">
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    Property Type 
                                    </label><br>
                                    <select class="PR_proptype" name="proptype" id="PR_proptype">	
                                            <option value="">Any Property Type</option>
                                            <option value="01">Single Family</option>
                                            <option value="04">Condo/Town/Villa</option>
                                            <option value="03">Multi Family +10</option>
                                            <option value="08">Multi Family -10</option>
                                            <option value="11">Commercial</option>
                                            <option value="00">Vacant Land</option>
                                            <option value="02">Mobile Home</option>
                                            <option value="99">Other</option>
                                    </select>
                                </label>
                                </div>
                                <div style="clear:both"></div>
                                        </th>
                                        <th style="height:30px;padding: 3px 0px 8px 0px;"  class="title">
                                            <span class="whitetext">Quantity</span>
                                        </th>
                                        <th style="height:30px;padding:3px 0px 8px 0px;" class="title">
                                            <span class="whitetext">%</span>&nbsp;&nbsp;&nbsp;
                                        </th>
                                    </tr>
                                </table>
                            <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Properties - Total</span>
                                        </th>
                                        <th>
                                            <span class="greentext" id="total">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties Owner-occupied
                                        </td>
                                        <td>
                                            <span id="ownerY">--</span>
                                        </td>
                                        <td>
                                            <span id="pctownerY">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Properties Non Owner Occupied
                                        </td>
                                        <td>
                                            <span id="ownerN">--</span>
                                        </td>
                                        <td>
                                            <span id="pctownerN">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Distressed Properties
                                        </td>
                                        <td>
                                            <span id="distress">--</span>
                                        </td>
                                        <td>
                                            <span id="pctdistress">--</span>
                                        </td>
                                    </tr>
                                    
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties
                                        </td>
                                        <td>
                                            <span id="preforeclosure">--</span>
                                        </td>
                                        <td>
                                            <span id="pctpreforeclosure">--</span>
                                        </td>
                                    </tr>
                                    
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties
                                        </td>
                                        <td>
                                            <span id="foreclosure">--</span>
                                        </td>
                                        <td>
                                            <span id="pctforeclosure">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties - Not Pre-Foreclosed/Not Foreclosed 
                                        </td>
                                        <td>
                                            <span id="upsidedown">--</span>
                                        </td>
                                        <td>
                                            <span id="pctupsidedown">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties - Active For Sale
                                        </td>
                                        <td>
                                            <span id="sale">--</span>
                                        </td>
                                        <td>
                                            <span id="pctsale">--</span>
                                        </td>
                                    </tr>
                                    
                                </table>
                                <div class="clear">&nbsp;</div> 
                                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Pre-Foreclosed Properties - Total</span>
                                        </th>
                                        <th>
                                            <span id="titlePreforeclosure" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - Active For Sale 
                                        </td>
                                        <td>
                                            <span id="total_sold_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - Not For Sale 
                                        </td>
                                        <td>
                                            <span id="total_no_sold_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - With Negative Equity (Upside Down)  	
                                            </td>
                                        <td>
                                            <span id="total_P_0">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_0">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - 0% equity or more
                                        </td>
                                        <td>
                                            <span id="total_P_0_mas">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_0_mas">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - 30% equity or more
                                        </td>
                                        <td>
                                            <span id="total_P_30">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_30">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Foreclosed Properties - Total</span>
                                        </th>
                                        <th>
                                            <span id="titleforeclosure" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties - Active For Sale 
                                        </td>
                                        <td>
                                            <span id="total_sold_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_F">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties - Not For Sale
                                        </td>
                                        <td>
                                            <span id="total_no_sold_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_F">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Total</span>
                                        </th>
                                        <th>
                                            <span id="titleupsidedown" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Active For Sale 	
                                        </td>
                                        <td>
                                            <span id="total_sold_UD">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_UD">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Not For sale
                                        </td>
                                        <td>
                                            <span id="total_no_sold_UD">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_UD">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Properties Active For Sale - Total</span>
                                        </th>
                                        <th>
                                            <span id="titlesale" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties For Sale - 30% + equity      
                                        </td>
                                        <td>
                                            <span id="total_sold_PE">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE">--</span>
                                        </td>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties For Sale - 30% + equity 
                                        </td>
                                        <td>
                                            <span id="total_sold_PE_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties For Sale - 30% + equity         
                                        </td>
                                        <td>
                                            <span id="total_sold_PE_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE_F">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Other Useful Statistics</span>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Total Properties Sold in the last 6 months
                                        </td>
                                        <td>
                                            <span id="total_sold_3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Properties Sold in the last 6 months
                                        </td>
                                        <td>
                                            <span id="a_sold_3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Total Properties Active For Sale
                                        </td>
                                        <td>
                                            <span id="othersale">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Months of inventory
                                        </td>
                                        <td>
                                            <span id="invM">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days on Market
                                        </td>
                                        <td>
                                            <span id="a_days">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 12 months
                                        </td>
                                        <td>
                                            $<span id="m_asp12">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 9 months
                                        </td>
                                        <td>
                                            $<span id="m_asp9">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 6 months
                                        </td>
                                        <td>
                                            $<span id="m_asp6">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 3 months
                                        </td>
                                        <td>
                                            $<span id="m_asp3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr>
                                 </table>
                    </div>
                    <!--
                    	final primer panel
                    //-->
                    <!--
                    
                    <div id="panel2" class="panelContenedor">
              		  <div class="title" style="height:30px; margin-bottom:0px; border-bottom:solid 2px #FFF;">
                             <div style="margin-left:45px; color:#FFF; font-weight:bold; font-size:13px;">
                                Discount statistics of Foreclosed and Pre-Foreclosed properties.<br>Specific Information for: <span class="selectProptype"></span> in <span class="selectCounty"></span> County, <span class="selectState"></span>. As of <span id="fechaDisc"></span>
                              </div>	
                          </div>
                          <table>
                                    <tr>
                                        <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                                        <div style="float:left; margin-left:45px; color:#FFF; text-align:center;">
                                            <label class="whitetext">
                                    State
                                    </label><br>
                                    <select class="shortBox" id="State2" name="state2">
                                    <?php
                                        $sql="SELECT * FROM lsstate where is_showed='Y';";
                                        $resultado=mysql_query($sql);
                                        while($data=mysql_fetch_assoc($resultado))
                                        {
                                            echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    County
                                    </label><br>
                                    <select class="shortBox County" id="County2" name="cunty2">
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    Property Type 
                                    </label><br>
                                    <select class="PR_proptype" name="proptype2" id="PR_proptype2">	
                                            <option value="">Any Property Type</option>
                                            <option value="01">Single Family</option>
                                            <option value="04">Condo/Town/Villa</option>
                                            <option value="03">Multi Family +10</option>
                                            <option value="08">Multi Family -10</option>
                                            <option value="11">Commercial</option>
                                            <option value="00">Vacant Land</option>
                                            <option value="02">Mobile Home</option>
                                            <option value="99">Other</option>
                                    </select>
                                </label>
                                </div>
                                <div style="clear:both"></div>
                                        </th>
                                        <th style="height:30px;padding: 3px 10px 8px 0px;" class="title">
                                            <span class="whitetext">Quantity</span>
                                        </th>
                                    </tr>
                                </table>
                            <div class="clear">&nbsp;</div>                                             
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Pre-Foreclosed Sold</span>
                                        </th>
                                        <th>
                                            <span id="p_sold" class="greentext">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days of Short Sale 
                                        </td>
                                        <td>
                                            <span id="p_avg_day_ss">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Amount of Discount 
                                        </td>
                                        <td>
                                            $<span id="pc_avg_amo_dis">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Percentage of Discount  	
                                            </td>
                                        <td>
                                            <span id="p_avg_pct_dis">--</span>%
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Foreclosed Sold</span>
                                        </th>
                                        <th>
                                            <span id="f_sold" class="greentext">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days of Sale After File Date 
                                        </td>
                                        <td>
                                            <span id="f_avg_day_safd">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Days of Sale After Judgement Date (Auction)
                                        </td>
                                        <td>
                                            <span id="f_avg_day_sajd">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Amount of Discount 
                                        </td>
                                        <td>
                                            $<span id="pcf_avg_amo_dis">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Percentage of Discount
                                        </td>
                                        <td>
                                            <span id="f_avg_pct_dis">--</span>%
                                        </td>
                                    </tr>
                                 </table>
                    </div>
                    <!--
                    	final segundo panel
                    //-->
                    
                    <!--
                </div>
                                 
                                 </div>
                                 <div class="impar" style="padding:15px; font-size:11px; color:#AAA; text-align:justify">
                                 Information at this web site is provided solely for informational purposes and does not constitute an offer to sell, rent, or advertise real estate. Web site owner does not make any warranties or representations concerning any of the information at this web site, which is deemed reliable but is not guaranteed and should be independently verified.
                                 </div>
                                 
                    </div>
                    
                    //-->
                    
            </div>
        <div class="clear">&nbsp;</div> 
    </div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script type="text/javascript">
	menuClick('menu-home');
	function loadCounty(){
		$('.reports .msgLoad').fadeTo(200,0.5);
		$.ajax({
			type	:'POST',
			url		:'resources/php/properties.php',
			data	: "ejecutar=countylist&state="+$('#stadeSearch').val(),
			dataType:'json',
			success	:function (resul)
					{
							$('#SearchCounty').html('');
							$(resul).each(function (index,data){
								$('#SearchCounty').append('<option value="'+data.id+'">'+data.county+'</option>');
							});
								IniCounty.init($('#SearchCounty'),triggerIni);
					}
			})
		};
	function  punticos(valor)
	{
		var punto=-1;
		var respuesta='';
		for(i=0;i<valor.length+1;i++)
		{
			if(punto==3)
			{
				respuesta=valor.charAt(valor.length - i)+','+respuesta;
				punto=1;
			}
			else
			{
				respuesta=valor.charAt(valor.length - i)+respuesta;
				punto++;
			}
		}
		return respuesta;
	}
	var mes=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
	
	/**************************
	*	FUNCIONES A EJECUTAR CUANDO SE CARGUE E DOM
	**************************/
	$(document).ready(function (){ 
		loadSearch($('#Search'));
		loadCounty();
		//$('.County, .PR_proptype').bind('change',refreshData);
		$('#buttonSearch').bind('click',initSearch);
	});
	
	/**************************
	*	FUNCION PARA LANZAR EL SEARCH
	**************************/
	
	function initSearch(e)
	{
		e.preventDefault();
		$('body').fadeOut(200);
		var parametrosEnviar='';
		var parametros=[
			{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
			{campo: 'county',	valor : ($('select[name=countySearch]').val())?$('select[name=countySearch]').val():''},
			{campo: 'tsearch', 	valor : 'location'},
			{campo: 'proptype', 	valor : ($('select[name=propertySearch]').val())?$('select[name=propertySearch]').val():''},
			{campo: 'price_low', 	valor : ($('input[name=MinSearch]').val())?$('input[name=MinSearch]').val():''},
			{campo: 'price_hi', 	valor : ($('input[name=MaxSearch]').val())?$('input[name=MaxSearch]').val():''},
			{campo: 'bed', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'bath', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'sqft', 	valor : ($('select[name=sqftSearch]').val())?$('select[name=sqftSearch]').val():-1},
			{campo: 'pequity', 	valor : -1},
			{campo: 'pendes', 	valor : -1},
			{campo: 'search_mapa', 	valor : -1},
			{campo: 'search_type', 	valor : ($('input[name=typeSearch]').val())?$('input[name=typeSearch]').val():''},
			{campo: 'search_mapa', 	valor : '-1'},
			{campo: 'occupied', 	valor : -1}
		]
		$(parametros).each(function (index){
			parametrosEnviar+=this.campo+'='+this.valor+'&';
			});
		//console.debug(parametrosEnviar);
		$.ajax({
			url		:'/properties_coresearch.php',
			type	:'POST',
			data	:parametrosEnviar,
			success	:function (resul){
				//console.debug(resul);
				window.location='result/index.php';
				}
		});
		
	}
	function  refreshData(){
			if($(this).is('.County'))
			{
				$('.County').attr('value',$(this).val());
			}
			else
			{
				$('.PR_proptype').attr('value',$(this).val());
			}	
			$('.selectProptype').html($('#PR_proptype option:selected').html());
			$('.selectCounty').html($('#County option:selected').html());
			$('.selectState').html($('#State option:selected').html());
					window.loadInt=true;
					/*$('.reports .msgLoad').fadeTo(200,0.5);
					$.ajax({
						type	:'POST', 
						url		:'resources/php/properties.php',
						data	: "ejecutar=propertieslist&county="+$('#County').val()+'&propType='+$('#PR_proptype').val(),
						dataType:'json',
						success	:function (resul)
								{
									$('#fechaDisc').html(fecha);
									$(resul.data).each(function (index,data){
											var valor=data.valor.split('.');
											$('#'+data.campo).html((data.campo.substring(0,2)=='pc')? number_format(data.valor,2):punticos(valor[0]));
										})
										$('.total').html('100.00');
										$('.reports .msgLoad').fadeOut(200);
								}
						})*/
			}
		
		function triggerIni()
		{
			$('.County').attr('value',$('.County:eq(0)').val());
			$('.PR_proptype').attr('value',$('.PR_proptype:eq(0)').val());
			//refreshData();
			//loadAdvertising();
		}
</script>