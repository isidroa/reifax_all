// JavaScript Document
/*
	obtener variables get
*/
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}
function tooltips ()
{
	/*
		acciones para los whaths this
	*/
	$(".whatsThis").bind('mouseenter',function (){$(this).find('div').fadeIn(200);}).bind('mouseleave',function (){$(this).find('div').stop(false,true).fadeOut(200);}).bind('click',function () {return false;});
	 
/*security notice*/
	$('#learnNotificationSSL').bind('mouseenter',function (){
		 $('#NotificationSSL').fadeIn(200);
	}).parent().parent().bind('mouseleave',function (){$('#NotificationSSL').fadeOut(200);})


}
/*****************
**		FORMATEOS DE NUMEROS
**********************/
function number_format (number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = (s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)!='')?s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep): '00';
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}


var lsImgCss={
'A-F':{'img':'verdel.png','explain':'Active Foreclosed'},
'A-F-S':{'img':'verdel_s.png','explain':'Active Foreclosed Sold'},
'A-P':{'img':'verdel.png','explain':'Active Pre-Foreclosed'},
'A-P-S':{'img':'verdel_s.png','explain':'Active Pre-Foreclosed Sold'}, 
'A-N':{'img':'verdeb.png','explain':'Active'},
'A-N-S':{'img':'verdeb_s.png','explain':'Active Sold'},
'P-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'P-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'P-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'P-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'P-N': {'img':'grisb.png','explain':'Non-Active'},
'P-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'CC-F': {'img':'cielol.png','explain':'By Owner Foreclosed'},
'CC-F-S': {'img':'cielol_s.png','explain':'By Owner Foreclosed Sold'},
'CC-P': {'img':'cielol.png','explain':'By Owner Pre-Foreclosed'},
'CC-P-S': {'img':'cielol_s.png','explain':'By Owner Pre-Foreclosed Sold'},
'CC-N': {'img':'cielo.png','explain':'By Owner'},
'CC-N-S': {'img':'cielo_s.png','explain':'By Owner Sold'},
'CS-P': {'img':'marronl.png','explain':'Closed Sale Pre-Foreclosed'},
'CS-P-S': {'img':'marronl_s.png','explain':'Closed Sale Pre-Foreclosed Sold'},
'CS-F': {'img':'marronl.png','explain':'Closed Sale Foreclosed'},
'CS-F-S': {'img':'marronl_s.png','explain':'Closed Sale Foreclosed Sold'},
'CS-N': {'img':'marronb.png','explain':'Closed Sale'},
'CS-N-S': {'img':'marronb_s.png','explain':'Closed Sale Sold'},
'PS-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'PS-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'PS-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'PS-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'PS-N': {'img':'grisb.png','explain':'Non-Active'},
'PS-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'SP-': {'img':'grisdiamante.png','explain':'Unknow'},
'T-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'T-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'T-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'T-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'T-N': {'img':'grisb.png','explain':'Non-Active'},
'T-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'R-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'R-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'R-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'R-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'R-N': {'img':'grisb.png','explain':'Non-Active'},
'R-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'E-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'E-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'E-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'E-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'E-N': {'img':'grisb.png','explain':'Non-Active'},
'E-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'C-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'C-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'C-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'C-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'C-N': {'img':'grisb.png','explain':'Non-Active'},
'C-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'Q-N': {'img':'grisb.png','explain':'Non-Active'},
'Q-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'Q-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'Q-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'Q-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'Q-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'W-N': {'img':'grisb.png','explain':'Non-Active'},
'W-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'W-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'W-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'W-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'W-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'X-N': {'img':'grisb.png','explain':'Non-Active'},
'X-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'X-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'X-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'X-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'X-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'SUBJECT': {'img':'verdetotal.png','explain':'Subject'},
'USER_CAR': {'img':'xxima.png','explain':'User'},
'B-N': {'img':'grisb.png','explain':'Non-Active'},
'B-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'B-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'B-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'B-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'B-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'N-N': {'img':'grisb.png','explain':'Non-Active'},
'N-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'N-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'N-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'N-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'N-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'}};
var indImgCss='';

function getCasita(status,pendes,sold)
{
  if(pendes=='L') pendes='N';
  if(sold=='S') sold='-S'; else sold='';
   switch (status.toUpperCase())
   {
    case "A":
     indImgCss="A-"+pendes+sold;     
     break;
    case "P":
     indImgCss="P-"+pendes+sold;
     break;
    case "CC":    
     indImgCss="CC-"+pendes+sold;
     break;
    case "CS":    
     indImgCss="CS-"+pendes+sold;
     break;
    case "PS":
     indImgCss="PS-"+pendes+sold;
     break;
    case "SP":
     indImgCss="SP-"+pendes+sold;
     break;
    case "T":
     indImgCss="T-"+pendes+sold;
     break;
    case "R":
     indImgCss="R-"+pendes+sold;
     break;
    case "E":
     indImgCss="E-"+pendes+sold;
     break;
    case "C":
     indImgCss="C-"+pendes+sold;
     break;
    case "Q":
     indImgCss="Q-"+pendes+sold;
     break;
    case "R":
     indImgCss="R-"+pendes+sold;
     break;
    case "T":
     indImgCss="T-"+pendes+sold;
     break;
    case "W":
     indImgCss="W-"+pendes+sold;
     break;
    case "X":
     indImgCss="X-"+pendes+sold;
     break;
    case "B":
     indImgCss="B-"+pendes+sold;
     break;
    case "N":
     indImgCss="N-"+pendes+sold;
     break;
    case "SUBJECT":
     indImgCss="SUBJECT";break
   }

 
}
