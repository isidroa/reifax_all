//Function to Active Menu.
function menuClick(idClick){
	if(document.getElementById('menu-home')) document.getElementById('menu-home').className = '';
	if(document.getElementById('menu-products')) document.getElementById('menu-products').className = '';
	if(document.getElementById('menu-support')) document.getElementById('menu-support').className = '';
	if(document.getElementById('menu-training')) document.getElementById('menu-training').className = '';
	if(document.getElementById('menu-company')) document.getElementById('menu-company').className = '';
	if(document.getElementById('menu-store')) document.getElementById('menu-store').className = '';
	
	if(document.getElementById(idClick)) document.getElementById(idClick).className = 'active';
}

//Slide Down & Up of Sub-Menu Display
(function($){
	//cache nav
	var nav = $("header #menu");

	//add indicators and hovers to submenu parents
	nav.find("li").each(function() {
		if ($(this).find("ul").length > 0) {
			//show subnav on hover
			$(this).mouseenter(function() {
				$(this).find("ul").stop(true, true).slideDown();
			});

			//hide submenus on exit
			$(this).mouseleave(function() {
				$(this).find("ul").stop(true, true).slideUp();
			});
		}
	});
	
})(jQuery);