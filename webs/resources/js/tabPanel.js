// JavaScript Document
function loadPanel(menu)
{
	//inicializando el barmenu
	$($(menu).find('.current')).each(function (){
		//verifico cual es el q se activa por defecto
		$($(this).attr('href')).addClass('active');
		});
	//extraigo el contenedor de los paneles, los paneles y el ancho q se va a colocar por defecto
	var contenPaneles=$(menu).parent().next();
	var paneles=contenPaneles.find('> div');
	var anchoPanel=contenPaneles.find('> div:eq(0)').css('width').replace('px','');
	// se establece el ancho del panel padre
	contenPaneles.css('width',paneles.length*anchoPanel);
	// se setean los estilos de los paneles
	$(menu).parent().next().find('> div').css({	
			float	:'left',
			display	:'block'
		}).each(function (index,obj){
			//y agrego la informacion de posicion para visualizar
			$(obj).attr('pos',(index*anchoPanel)).css({
					width	: (anchoPanel)+'px'
				});
	});
	// agrego las funciones para las pestañas
	$(menu).find('a').bind('click',function (e){
		e.preventDefault(); // evito q se evecute la funcion del click
		var panel=$($(this).attr('href')); // extraigo el panel al cual apunta
		var padre=panel.parent(); //extraigo el padre o el contenedor del panel
		padre.animate({  // realizo las animaciones necesarias
			'margin-left': '-'+panel.attr('pos')+'px', 
			'height' : (parseInt(panel.css('height'))+20)+'px'
		},'slow');
		// amplio o reduzco el contenedor para ajustarlo al panel
		padre.parent().animate({height:(parseInt(panel.css('height'))+parseInt(padre.prev().css('height')+20))+'px'});
		// actulizo el estilo a la ventana activa
		$(this).parent().parent().find('.current').removeClass('current');
		$(this).addClass('current');
	})
}
function loadSearch(menu)
{
	$($(menu).find('.current')).each(function (){
		$('input[name=typeSearch]').attr('value',$(this).attr('href').replace('#',''));
	});
	$(menu).find('a').bind('click',function (e){
		e.preventDefault();
		if(!$(this).is('.current'))
		{
			var panel=$(this).attr('href');		
			$('input[name=typeSearch]').attr('value',$(this).attr('href').replace('#',''));
			
			$(this).parent().parent().find('.current').removeClass('current');
			$(this).addClass('current');
		}
	})
}