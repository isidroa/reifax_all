// JavaScript Document
IniCounty={
	init:function (object,callback){
		this.object=object;
		this.callback=callback;
		($.cookie('condadoOrigen'))?this.cargaCache():this.obtenerCoordenadas();
		},
	obtenerCoordenadas:function (){
		$.ajax({
			dataType: 'json',
			type	:'get',
			url		:'/resources/php/getlatlong.php',
			success	:this.success_callbackLocation,
			error	:this.error_callbackLocation
		})
	},
	success_callbackLocation:function (resul)
		{
			IniCounty.object.attr('value',resul.idcounty);
			$.cookie('condadoOrigen',resul.idcounty);
			(IniCounty.callback)?IniCounty.callback():IniCounty.object.trigger('change'); 
		},
	error_callbackLocation: function ()
	{
		$.cookie('condadoOrigen', '2');
		IniCounty.object.attr('value','2');
		(IniCounty.callback)?IniCounty.callback():IniCounty.object.trigger('change');
	},
	cargaCache:function ()
	{
		IniCounty.object.attr('value',$.cookie('condadoOrigen'));
		(IniCounty.callback)?IniCounty.callback():IniCounty.object.trigger('change');
	}
}