<?php require_once('../template/template.php');?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">log In Error</span>
    </div>
    <div id="contentPrincipal">
    <!-- Center Content-->
    <div style="width:100%;" class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">log In Error</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
       
		<div class="panel">
           	 	<div class="center centertext register contactUs">
                    <div class="centertext">
                	<h2 style="padding-left:15px;">Your email or password is incorrect</h2>
                    <a href="#" class="bigButton buttonblue" onclick="history.back(-1);">Return</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
</script>