<?php
	include('simple_html_dom.php');
	function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
       
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
    return $_SERVER['REMOTE_ADDR'];
	}
	
	$url='http://whatismyipaddress.com/ip/'.getRealIP();
	
	$html=file_get_html($url);
	
	$idgeo=$html->find('#Geolocation-Information',0);
	
	$table=$idgeo->next_sibling();
	$ths=$table->find('th');
	
	foreach($ths as $th){
		if($th->text()=='Latitude:'){
			$latitude=$th->next_sibling()->text();
		}else if($th->text()=='Longitude:'){
			$longitude=$th->next_sibling()->text();
		}
	}
	echo json_encode(array(
					'latitude'	=>	$latitude,
					'longitude'	=>	$longitude)
					);
?>