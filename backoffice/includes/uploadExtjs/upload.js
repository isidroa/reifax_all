
function showUploadWindow(){

    var uploadwindow = new Ext.Window({
            title       :'Form Upload',
            width       :325,
            closable    : true,
            resizable   : false,
            modal	 	: true,
            plain       : true,
            items       :[fp = new Ext.FormPanel({
                            fileUpload  :true,
                            width       :320,
                            frame       :true,
                            autoheight  :true,
                            bodyStyle: 'padding: 10px 10px 0 10px;',
                            labelWidth  : 60,
                            defaults    :{
                                            anchor  : '95%',
                                            msgTarget: 'side'
                                        },
                            items       :[{
                                            xtype       : 'fileuploadfield',
                                            id          : 'form-file1',
                                            emptyText   : 'Select an image',
                                            fieldLabel  : 'Image',
                                            name        : 'photo',
                                            buttonCfg   : {
                                                            text: '',
                                                            iconCls: 'x-icon-upload'
                                                        }
                                        }],
                            buttons     :[{
                                            text        :'Save',
                                            cls         :'x-btn-text-icon',
                                            icon        :'img/save.gif',
                                            handler     :function(){
                                                            if(fp.getForm().isValid()){
                                                                fp.getForm().submit({
                                                                    url     : 'includes/uploadExtjs/phpupload.php',
                                                                    waitMsg : 'Uploading...',
                                                                    success : function(fp, o){
                                                                                obj = Ext.util.JSON.decode(o.response.responseText);
                                                                                Ext.Msg.alert("Success", obj.errors.reason);
                                                                                uploadwindow.close();
                                                                                Ext.getCmp('idimg').setValue(obj.errors.idimg);
                                                                                Ext.getCmp('descpicture').setValue(obj.errors.namepicture);
                                                                                document.getElementById("picture").src=obj.errors.url
                                                                            },
                                                                    failure: function(fp, o){
                                                                                obj = Ext.util.JSON.decode(o.response.responseText);
                                                                                Ext.Msg.alert("Failure", obj.errors.reason);
                                                                            }
                                                                });
                                                            }
                                                        }
                                        },{
                                            text        :'Reset',
                                            cls         :'x-btn-text-icon',
                                            icon        :'img/refresh.png',
                                            handler     :function(){
                                                            fp.getForm().reset();
                                                        }
                                        }]
                            })
                        ]
    });
    uploadwindow.show();
}




