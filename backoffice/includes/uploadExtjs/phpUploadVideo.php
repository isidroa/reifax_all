<?php
    session_start();
    include("../../backend/php/conexion.php");

    $iduser = $_SESSION['idTemp'];
    $idraff = $_POST['idraff'];

    //buscamos el idcont
    $query = "select c.* from uhcontest c WHERE c.idraff = {$idraff} AND c.iduser = {$iduser}";
    $result = mysql_query($query) or die ("{success:false}");
    $rowIdcont = mysql_fetch_object($result);

    $idcont = $rowIdcont->idcont;

    //buscamos si ya tiene video montado
    $query="SELECT * FROM uhload l WHERE l.idcont ='{$idcont}' AND l.type ='2'";
    $result =mysql_query($query);

    if(mysql_num_rows($result) > 0){
        echo "{success: false, tipoError:'4'}";
        return;
    }

    $nameFile = $_FILES['video']['name'];
    $extension = explode(".",$nameFile);
    $ext=count($extension);

    $size = $_FILES['video']['size'];

    
    if($size < 10485760 ){

        if($extension[$ext-1]=='flv' || $extension[$ext-1]=='swf' || $extension[$ext-1]=='mp4' || $extension[$ext-1]=='avi' || $extension[$ext-1]=='mov' || $extension[$ext-1]=='wmv' || $extension[$ext-1]=='mpeg'){

            //data para general el nuevo directorio, si es necesario
            $filenewname=strtotime(date("YmdHis")).rand(100,999);
            $urltabla="img/videos/".date("Ym")."/".$filenewname.".flv";
            $directory="../../backend/img/videos/".date("Ym")."/";

            // si no existe el directorio, lo creamos
            if(!is_dir($directory)){
                mkdir($directory);
            }

            //movemos el video al directorio indicado
            move_uploaded_file ($_FILES['video']['tmp_name'],$directory.$_FILES['video']['name']);
            rename($directory.$_FILES['video']['name'], $directory.$filenewname.".".$extension[$ext-1]);

            //transformto el video en formato flv
            if($extension[$ext-1]!='flv'){
                passthru("C:\\ffmpeg\\ffmpeg.exe -i ".$directory.$filenewname.".".$extension[$ext-1]." -ab 56 -ar 44100 -b 200 -r 15 -s 520x330 -f flv ".$directory.$filenewname.".flv");
            }

            //guardamos la ruta del video en la bd
            $query="INSERT INTO uhload (idcont,filename,url,insertdate,save,type) VALUES ('{$idcont}','{$filenewname}','{$urltabla}',now(),'1','2')";
            mysql_query($query) or die("{success: false, errors: { reason: \"".mysql_error()."\" }}");

            //borramos el archivo
            unlink($directory.$filenewname.".".$extension[$ext-1]);

            echo "{success: true, tipoError:'1'}";
        }else{
            echo "{success: false, tipoError:'2'}";
        }
    }else{
            echo "{success: false, tipoError:'3'}";
    }
    
?>