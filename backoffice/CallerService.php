<?php
/****************************************************
CallerService.php

This file uses the constants.php to get parameters needed 
to make an API call and calls the server.if you want use your
own credentials, you have to change the constants.php

Called by TransactionDetails.php, ReviewOrder.php, 
DoDirectPaymentReceipt.php and DoExpressCheckoutPayment.php.

****************************************************/
require_once 'constants.php';

$API_UserName=API_USERNAME;


$API_Password=API_PASSWORD;


$API_Signature=API_SIGNATURE;


$API_Endpoint =API_ENDPOINT;


$version=VERSION;

session_start();

/**
  * hash_call: Function to perform the API call to PayPal using API signature
  * @methodName is name of API  method.
  * @nvpStr is nvp string.
  * returns an associtive array containing the response from the server.
*/


function hash_call($methodName,$nvpStr)
{
	//declaring of global variables
	global $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header;

	//setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	//turning off the server and peer verification(TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POST, 1);
    //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
   //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
	if(USE_PROXY)
	curl_setopt ($ch, CURLOPT_PROXY, PROXY_HOST.":".PROXY_PORT); 

	//NVPRequest for submitting to server
	$nvpreq="METHOD=".urlencode($methodName)."&VERSION=".urlencode($version)."&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature).$nvpStr;

	//setting the nvpreq as POST FIELD to curl
	curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

	//getting response from server
	$response = curl_exec($ch);

	//convrting NVPResponse to an Associative Array
	$nvpResArray=deformatNVP($response);
	$nvpReqArray=deformatNVP($nvpreq);
	$_SESSION['nvpReqArray']=$nvpReqArray;

	if (curl_errno($ch)) {
		// moving to display page to display curl errors
		  $_SESSION['curl_error_no']=curl_errno($ch) ;
		  $_SESSION['curl_error_msg']=curl_error($ch);
		  $location = "APIError.php";
			//echo 'Error: '.curl_errno($ch).' : '.curl_error($ch);
		  return false;
	 } else {
		 //closing the curl
		//echo 'Todo Bien';
		//print_r($nvpResArray);	
			curl_close($ch);
	  }

return $nvpResArray;
}

/** This function will take NVPString and convert it to an Associative Array and it will decode the response.
  * It is usefull to search for a particular key and displaying arrays.
  * @nvpstr is NVPString.
  * @nvpArray is Associative Array.
  */

function deformatNVP($nvpstr)
{

	$intial=0;
 	$nvpArray = array();


	while(strlen($nvpstr)){
		//postion of Key
		$keypos= strpos($nvpstr,'=');
		//position of value
		$valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

		/*getting the Key and Value values and storing in a Associative Array*/
		$keyval=substr($nvpstr,$intial,$keypos);
		$valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
		//decoding the respose
		$nvpArray[urldecode($keyval)] =urldecode( $valval);
		$nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
     }
	return $nvpArray;
}

function parseState($state){
 
	switch(strtolower($state)){
		case 'wy': case 'wyoming': $st='WY'; break; 
		case 'wi': case 'wisconsin': $st='WI'; break; 
		case 'wv': case 'west virginia': $st='WV'; break; 
		case 'wa': case 'washington': $st='WA'; break; 
		case 'va': case 'virginia': $st='VA'; break; 
		case 'vt': case 'vermont': $st='VT'; break; 
		case 'ut': case 'utah': $st='UT'; break; 
		case 'tx': case 'texas': $st='TX'; break; 
		case 'tn': case 'tennessee': $st='TN'; break; 
		case 'sd': case 'south dakota': $st='SD'; break; 
		case 'sc': case 'south carolina': $st='SC'; break; 
		case 'ri': case 'rhode island': $st='RI'; break; 
		case 'pa': case 'pennsylvania': $st='PA'; break; 
		case 'or': case 'oregon': $st='OR'; break; 
		case 'ok': case 'oklahoma': $st='OK'; break; 
		case 'oh': case 'ohio': $st='OH'; break; 
		case 'nd': case 'north dakota': $st='ND'; break; 
		case 'nc': case 'north carolina': $st='NC'; break; 
		case 'ny': case 'new york': $st='NY'; break; 
		case 'nm': case 'new mexico': $st='NM'; break; 
		case 'nj': case 'new jersey': $st='NJ'; break; 
		case 'nh': case 'new hampshire': $st='NH'; break; 
		case 'nv': case 'nevada': $st='NV'; break; 
		case 'ne': case 'nebraska': $st='NE'; break; 
		case 'mt': case 'montana': $st='MT'; break; 
		case 'mo': case 'missouri': $st='MO'; break; 
		case 'ms': case 'mississippi': $st='MS'; break; 
		case 'mn': case 'minnesota': $st='MN'; break; 
		case 'mi': case 'michigan': $st='MI'; break; 
		case 'ma': case 'massachusetts': $st='MA'; break; 
		case 'md': case 'maryland': $st='MD'; break; 
		case 'me': case 'maine': $st='ME'; break; 
		case 'la': case 'louisiana': $st='LA'; break; 
		case 'ky': case 'kentucky': $st='KY'; break; 
		case 'ks': case 'kansas': $st='KS'; break; 
		case 'ia': case 'iowa': $st='IA'; break; 
		case 'in': case 'indiana': $st='IN'; break; 
		case 'il': case 'illinois': $st='IL'; break; 
		case 'id': case 'idaho': $st='ID'; break; 
		case 'hi': case 'hawaii': $st='HI'; break; 
		case 'ga': case 'georgia': $st='GA'; break; 
		case 'fl': case 'florida': $st='FL'; break; 
		case 'dc': case 'district of columbia': $st='DC'; break; 
		case 'de': case 'delaware': $st='DE'; break; 
		case 'ct': case 'connecticut': $st='CT'; break; 
		case 'co': case 'colrado': $st='CO'; break; 
		case 'ca': case 'california': $st='CA'; break; 
		case 'ar': case 'arkansas': $st='AR'; break; 
		case 'az': case 'arizona': $st='AZ'; break; 
		case 'ak': case 'alaska': $st='AK'; break; 
		case 'al': case 'alabama': $st='AL'; break; 		
	}
	
	return $st;
}

function tradAVS($av){
	switch($av){
		case 'A': $res = 'A: match address only (no ZIP)'; break;
		case 'W': $res = 'W: match 9 digit ZIP code only (no address)'; break;
		case 'X': $res = 'X: exact match (address + 9-digit ZIP code)'; break;
		case 'Y': $res = 'Y: match (address + 5-digit ZIP code)'; break;
		case 'Z': $res = 'Z: match 5 digit ZIP code only (no address)'; break;
		case 'N': $res = 'N: no match'; break;
		case 'R': $res = 'R: retry'; break;
		case 'S': $res = 'S: service not supported'; break;
		case 'U': $res = 'U: unavailable'; break;
		case 'B': $res = 'B: address only (no ZIP)'; break;
		case 'D': $res = 'D: address and postal code match'; break;
		case 'F': $res = 'F: address and postal code match (only for UK)'; break;
		case 'P': $res = 'P: match only postal code'; break;
		case 'C': $res = 'C: no match'; break;
		case 'I': $res = 'I: international unavailable'; break;
		case 'G': $res = 'G: global unavailable'; break;
	}
	
	return $res;
}

function tradCVV($cv){
	
	switch($cv){
		case 'M': $res='M: CVV code match'; break;
		case 'N': $res='N: no match'; break;
		case 'U': $res='U: unavailable'; break;
		case 'S': $res='S: service not supported'; break;
		case 'P': $res='P: not processed'; break;
		case 'X': $res='X: no response'; break;
	}
	
	return $res;
}
?>
