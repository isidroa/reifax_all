<?php 
class Fecha {
  var $fecha;
  function Fecha($anio=0, $mes=0, $dia=0) 
  {
       if($anio==0) $anio = Date("Y");
       if($mes==0) $mes = Date("m");
       if($dia==0) $dia = Date("d");
       $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
  }

  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
  {
       $array_date = explode("-", $this->fecha);
       $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
  }
 
  function getFecha() { return $this->fecha; }
}

//compara dos fechas 
function compara_fechas($fecha1,$fecha2)
{
	if(strtotime($fecha1)>strtotime($fecha2)) return 1;
	if(strtotime($fecha1)==strtotime($fecha2)) return 0;
	if(strtotime($fecha1)<strtotime($fecha2)) return -1;
}

//Convierte fecha de normal a mysql 
function fechaNormal2Mysql($fecha)
{ 
    ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
    return $lafecha; 
} 

function arreglarFechasInvalidasresp($fecha)
{
//	$fecha=fechaNormal2Mysql($fecha);//convierte 30/02/2008 a 2008-02-28
	$timestamp=strtotime($fecha);
	$fecha=date('Y-m-d',$timestamp);
	return $fecha;
}

//arregla las fechas invalidas
function arreglarFechasInvalidas($fecha)
{
	$timestamp=strtotime($fecha);
	$fecha=date('Y-m-d',$timestamp);
	return $fecha;
}

//ultimo dia del mes actual
function UltimoDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

//ultimo dia del mes anterior
function UltimoDiaAnteriorMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m')-1; 
	if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

//primer dia del mes actual
function PrimerDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}
//primer dia del mes anterior
function PrimerDiaAnteriorMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m')-1; 
	if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}


function insertlogs($usersource, $usertarget, $operation, $query)
{
	$query=str_replace("'","",$query);
	$queryIns="insert into  xima.logsfull (`usersource`, `usertarget`, `operation`, `query`, `insertdate`, `php`)
				values('$usersource','$usertarget','$operation','$query',NOW(),'".$_SERVER["PHP_SELF"]."')";
	mysql_query($queryIns) or die("{success:false, msg:\"".$queryIns."<br>".mysql_error()."\"}");				
	
}


	$ECHOS=1;//0-->NO EJECUTAR LOS ECHOS 1-->EJECUTAR LOS ECHOS 

	include("php/conexion.php");
	$conex=conectar("xima");
	include("../properties_getprecio.php");

	
	//VALIDACION PARA CUANDO EL ZCRON  O CUALQUIER PERSONA EJECUTE LA COBRANZA MAS DE DOS VECES AL DIA, SOLO LO HAGA UNA VEZ.
	if(!isset($_GET['st']) && $_GET['st']<>'1')
	{
		$sl=rand(0, 400);
		echo "<br>".date("Y-m-d H:i:s");	
		echo "<br>Sleep: $sl ";
		sleep($sl);
		echo "<br>".date("Y-m-d H:i:s");	
	}
	echo "<br>".
	$query="SELECT valor FROM `parametros` where campo ='cobranzadiaria' and valor=curdate()";
	$res1=mysql_query($query) or die($query.mysql_error());
	$row1= mysql_fetch_array($res1, MYSQL_ASSOC);
	if($row1['valor']<>'')	die("<HR/>NO HAY COBRANZA YA SE EJECUTO EN LA OTRA SESION.. OJO!!!!!! <HR/>");
	echo "<HR/>PROCESAR COBRANZA <HR/>";
	$query="UPDATE parametros p SET valor =curdate() WHERE p.`campo`='cobranzadiaria'";
	mysql_query($query) or die($query.mysql_error());
	

//SI ES DIA 1 DE MES HACER COBRAR LAS COMISIONES Y LOS CIERRES DE MES
	if(date('d')=='01' || date('d')=='1' )
	{
		//HACER INGRESO DE LAS COMISIONES 
		include("comisiones.php");

		//HACER INGRESO DE LAS APERTURAS Y CIERRES 
		include("aperturascierres.php");
	}

	echo "<HR/>";
//return;

//HACIENDO EL COBRO DE LOS 'active','inactive','freeze','freezeinac',free(se les cobra 0)
	$primeravez=0;
	$fechahoy=' ';
	$querycuota="SELECT distinct x1.userid,x1.name,x1.surname 
				FROM xima.usr_cobros u
				INNER JOIN (xima.ximausrs x1)ON(x1.userid=u.userid)
				INNER JOIN (xima.cobro_porcentajes sp)ON(sp.userid=u.userid)
				INNER JOIN xima.usr_productobase s on s.idproductobase= u.idproductobase
				INNER JOIN xima.usr_producto p on p.idproducto= s.idproducto
				INNER JOIN xima.status t on t.idstatus= u.idstatus
				WHERE p.pu=0 and u.idstatus in (1,3,5,6,7,4) and x1.userid not in (4,5,73,20)
				order by x1.userid ,p.idproducto desc "; //AND X1.USERID in (2468,2543,889,2542,2880,1284,958,2119,2886,2892,2737) ,1690,3911
				
if($ECHOS==1)echo $querycuota.'<br/>';
	$rescuota=mysql_query($querycuota) or die($querycuota.mysql_error());
	$i=1;
	$comparadoanterior='';
	$OJOUSERS='';
	while ($row= mysql_fetch_array($rescuota, MYSQL_ASSOC))
	{
		$insertacuota=0;
		$SaltarUsurario=0;
		$comparado=$row['userid'];
		$namecomparado=$row['name'];
		$surnamecomparado=$row['surname'];
		if($ECHOS==1)echo '<br><br><br><hr><hr>'.$i.'.- Userid: '.$comparado.'<hr><hr>';
		
		$querycuota="SELECT distinct DAY(NOW()) diahoy, MONTH(NOW()) meshoy, YEAR(NOW()) aniohoy,
							x1.userid,x1.name,x1.surname,u.fechacobro paydate, DAY(u.fechacobro) diausuario,
							MONTH(u.fechacobro) mesusuario, YEAR(u.fechacobro) aniousuario,
						t.status,p.name nameprod,u.idproductobase,p.idproducto, u.idstatus, u.freedays,sp.saldo
					FROM xima.usr_cobros u
					INNER JOIN (xima.ximausrs x1)ON(x1.userid=u.userid)
					INNER JOIN (xima.cobro_porcentajes sp)ON(sp.userid=u.userid)
					INNER JOIN xima.usr_productobase s on s.idproductobase= u.idproductobase
					INNER JOIN xima.usr_producto p on p.idproducto= s.idproducto
					INNER JOIN xima.status t on t.idstatus= u.idstatus
					WHERE p.pu=0 and u.idstatus in (1,3,5,6,7,4)  AND x1.USERID in ($comparado)
					order by x1.userid ,p.idproducto desc ";
		if($ECHOS==1)echo $querycuota.'<br/>';
		$rescomparado=mysql_query($querycuota) or die($querycuota.mysql_error());
		unset($arraux);
		while ($rowcomparado=mysql_fetch_array($rescomparado, MYSQL_ASSOC)) $arraux[]=$rowcomparado;
		
		//echo "<pre>";print_r($arraux);echo "</pre>";
		
		unset($arrcomparado);
		$productoanterior=0;
		foreach($arraux as $arrval )
		{
			$insertar=1;
			if($productoanterior==2 && $arrval['idproducto']==1)
				$insertar=0;
			if($productoanterior==3 && $arrval['idproducto']==2)
				$insertar=0;
				
			if($insertar==1)
				$arrcomparado[]=$arrval;
				
			$productoanterior=$arrval['idproducto'];
		}
		//echo "<br>ARRCOMPARADO <pre>";print_r($arrcomparado);echo "</pre>";		
		
		foreach($arrcomparado as $arrval )
		{			
			$diausuario=$arrval['diausuario'];
			$paydatecomparado=$arrval['paydate'];
			$producto=str_replace("'","\'",$arrval['nameprod']);

			$query="SELECT c.saldo FROM xima.cobro_porcentajes c WHERE c.userid=$comparado";
			$ressaldo=mysql_query($query) or die($query.mysql_error());
			$rowsaldo= mysql_fetch_array($ressaldo, MYSQL_ASSOC);
			$saldocomparado=$rowsaldo['saldo'];
			
			//COBRANDO LAS CUOTAS MENSUALES A LOS USUARIOS QUE SE LES VENCE HOY Y A LOS QUE NOS DEBEN.
			if($arrval['meshoy']==1 || $arrval['meshoy']=='01')
			{
				if($arrval['diahoy']>=$arrval['diausuario'])
					$fecha=$arrval['aniohoy']."-".$arrval['meshoy']."-".$arrval['diausuario'];
				else
					$fecha=($arrval['aniohoy']-1)."-12-".$arrval['diausuario'];
			}
			else
			{
				$fecultimodia=UltimoDiaActualMes();
				$timest2=strtotime($fecultimodia);
				$ultdia=date('d',$timest2);
			
				//caso del ultimo dia de mes, como 30 junio, que le cobre a los  del 31  tambien
				if($ultdia==date('d') && $arrval['diausuario']>$ultdia)
					$arrval['diausuario']=$ultdia;		

				$fecha=$arrval['aniohoy']."-".$arrval['meshoy']."-".$arrval['diausuario'];
			}

			$SaltarUsurario=1;
			if($arrval['diausuario']==date('d'))$SaltarUsurario=0;
			
			//VERIFICAR LOS TRIALS SI SE PASAN O NO A ACTIVOS PARA COBRARARLES 
			if($arrval['idstatus']==1)
			{
				if($arrval['freedays']>0)
				{
					$freedays=$arrval['freedays'];
					$query="SELECT DATEDIFF(curdate(),'$paydatecomparado') diffe ";
					$res1=mysql_query($query) or die($query.mysql_error());
					$row1= mysql_fetch_array($res1, MYSQL_ASSOC);
					$diffe=$row1['diffe'];	
						
					echo "<br/>TRIALS====> comparado=$comparado | freedays=$freedays | paydatecomparado=$paydatecomparado | diffe=$diffe";
					if($diffe>=($freedays-5))
					{
						$aux=$arrval['idproducto'];
						if($aux==2) $aux='1,2';
						if($aux==3) $aux='1,2,3';						
						echo "<br> -->".
						$query="UPDATE xima.usr_cobros u 
								INNER JOIN usr_productobase p on p.idproductobase=u.idproductobase
								SET  u.idstatus=3,u.freedays=0 
								WHERE userid=$comparado and p.idproducto in ($aux)";
						mysql_query($query) or die($query.mysql_error());
					}
					else
					{
						echo " --> TODAVIA EN  TRIAL";
						$SaltarUsurario=1;
					}
				}
				else 
				{
					$SaltarUsurario=1;
					echo "<br><H1>USUARIO: $comparado PRODUCTO: $producto TRIAL CON FREEDAYS VACIO ---------------->>>>>>>> OJO REVISAR </H1>";
					$OJOUSERS.="<br>USUARIO: $comparado PRODUCTO: $producto TRIAL CON FREEDAYS VACIO ---------------->>>>>>>> OJO REVISAR ";
				}
			}

			if($ECHOS==1)echo "<br>$comparado // ".$fecha." // ".$SaltarUsurario;
			if($SaltarUsurario==0)
			{
				if($arrval['idproducto']==2) $arrval['idproducto']='1,2';
				if($arrval['idproducto']==3) $arrval['idproducto']='1,2,3';

				$arrval['priceprod']=getpriceuser($comparado,$diausuario,'yes',$arrval['idproducto']);//($userid,$dia=0,$status='no',$idproducto=0)

				if($ECHOS==1)echo 	'<br> Montocuota: '.$arrval['priceprod'].' Paydate: '.$arrval['paydate'].' Iproducto: '.$arrval['idproducto'].
									' producto: '.$producto.' status: '.$arrval['status'];

				$fecha=arreglarFechasInvalidas($fecha);
				$fechaevalcomparado=$fecha;
				if($arrval['idstatus']==6 || $arrval['idstatus']==7)//' freeze' 'freezeinac'
				{
					$montocuota=15*-1;	
					$idoper=16;
				}
				else
				{
					$montocuota=$arrval['priceprod']*-1;
					$idoper=4;
				}

				$fechainsert=date("Y-m-d H:i:s");
				$ejecutaupdatesaldo=1;			
				if($arrval['idstatus']==4)//si es status free  no se le cobra
				{
					$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`,`cobradordesc`)
							VALUES ($comparado,'$fechainsert',0,4,'$producto')";
					if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
					mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

					$query="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
								VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Free')";					
					if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
					mysql_query($query) or die($query.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO	
				}
				else//si NO es status free  
				{
					$query="SELECT * FROM xima.f_frecuency f WHERE userid=$comparado";
					$resq=mysql_query($query) or die($query.mysql_error());
					$rowq= mysql_fetch_array($resq, MYSQL_ASSOC);
					$payfrecuencycomparado=$rowq['idfrecuency'];
	if($ECHOS==1)echo '<br/>payfrecuencycomparado: '.$payfrecuencycomparado;
					if($payfrecuencycomparado==2)//pagos anuales
					{
						$amact=date('Ym');
						if($arrval['mesusuario']<10)$arrval['mesusuario']='0'.$arrval['mesusuario'];
						$amuser=$arrval['aniousuario'].$arrval['mesusuario'];
						$query="SELECT PERIOD_DIFF($amact,$amuser) difer";
						$resf=mysql_query($query) or die($query.mysql_error());
						$rowf= mysql_fetch_array($resf, MYSQL_ASSOC);
						$diferanual=$rowf['difer'];
						if($ECHOS==1)echo '<br/>meses diferencia: '.$diferanual.'|Ym Act: '.$amact.'|Ym User: '.$amuser;//.$query..;

						if(($diferanual%12)==0)//LE TOCA PAGAR EL PAGO ANUAL...
						{
							//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
							$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`,`cobradordesc`) 
									VALUES ($comparado,'$fechainsert',$montocuota,$idoper,'$producto')";
							if($ECHOS==1)	echo '<br>PAGO ANUAL-->'.$query;
							mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO
 insertlogs($comparado, $comparado, 'Bill payment', $query);
							$newsaldo=$saldocomparado+$montocuota;
							//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
							$query="	UPDATE xima.cobro_porcentajes SET  saldo=$newsaldo WHERE userid=$comparado";
							if($ECHOS==1)	echo '<br>-->'.$query;
							mysql_query($query) or die($query.mysql_error());
 insertlogs($comparado, $comparado, 'Bill payment', $query);

							//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
							//quedando la resta de la cuota con el saldo en cero o mas, se le ingresa un pago de cero
							if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga --> Userid: '.$comparado.' Fechaevaluar: '.$fechainsert.' Paydate: '.$arrval['paydate'].' Montocuota: '.$montocuota;
							if($newsaldo>=0)
							{
								$query="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
									VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Saldo Favor')";					
								if($ECHOS==1)	echo '<br>EXONERO SALDO A FAVOR-->'.$query;
								mysql_query($query) or die($query.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
 insertlogs($comparado, $comparado, 'Bill payment', $query);
							}								
						}
						else//LE TOCA EXONERACION DE PAGO ESTE MES...
						{
							$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`,`cobradordesc`)
									VALUES ($comparado,'$fechainsert',0,$idoper,'$producto')";
							if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
							mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO
 insertlogs($comparado, $comparado, 'Bill payment', $query);

							$query="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
									VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Anual')";					
							if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
							mysql_query($query) or die($query.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
 insertlogs($comparado, $comparado, 'Bill payment', $query);
						}
					}
					else if($payfrecuencycomparado==1)//pagos mensuales
					{
						//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
						$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`,`cobradordesc`) 
								VALUES ($comparado,'$fechainsert',$montocuota,$idoper,'$producto')";
						if($ECHOS==1)	echo '<br>'.$query;
						mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO
 insertlogs($comparado, $comparado, 'Bill payment', $query);
							
						//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
						$newsaldo=$saldocomparado+$montocuota;
						$query="	UPDATE xima.cobro_porcentajes SET  saldo=$newsaldo WHERE userid=$comparado";
						if($ECHOS==1)	echo '<br>-->'.$query;
						mysql_query($query) or die($query.mysql_error());
 insertlogs($comparado, $comparado, 'Bill payment', $query);
							
						//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
						//quedando la resta de la cuota con el saldo en cero o mas, se le ingresa un pago de cero
						if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga mensual --> Userid: '.$comparado.' newsaldo: '.$newsaldo.' saldocomparado: '.$saldocomparado.' Paydate: '.$arrval['paydate'].' Montocuota: '.$montocuota;
						if($newsaldo>=0)
						{
							$query="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
								VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Saldo Favor')";					
							if($ECHOS==1)	echo '<br>EXONERO SALDO A FAVOR-->'.$query;
							mysql_query($query) or die($query.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
 insertlogs($comparado, $comparado, 'Bill payment', $query);
						}
					}//pagos mensuales
				}//si NO es status free 
				if($ECHOS==1)echo 	'<br> PROCESADO ';				
			}//Saltar usuario	
			$i++;
		}//fin foreach de todos los productos de un comparado
	}//fin while todos los comparados


	
//HACIENDO EL COBRO DE LOS 'ASSISTANT1'
	$primeravez=0;
	$fechahoy=' ';
	$querycuota="SELECT distinct x1.userid,x1.name,x1.surname 
				FROM xima.usr_cobros u
				INNER JOIN (xima.ximausrs x1)ON(x1.userid=u.userid)
				INNER JOIN (xima.cobro_porcentajes sp)ON(sp.userid=u.userid)
				INNER JOIN xima.usr_productobase s on s.idproductobase= u.idproductobase
				INNER JOIN xima.usr_producto p on p.idproducto= s.idproducto
				INNER JOIN xima.status t on t.idstatus= u.idstatus
				WHERE p.pu=0 and u.idstatus in (10) 
				order by x1.userid ,p.idproducto desc "; //AND X1.USERID in (2468,2543,889,2542,2880,1284,958,2119,2886,2892,2737)
				
if($ECHOS==1)echo $querycuota.'<br/>';
	$rescuota=mysql_query($querycuota) or die($querycuota.mysql_error());
	$i=1;
	$comparadoanterior='';
	$OJOUSERS='';
	while ($row= mysql_fetch_array($rescuota, MYSQL_ASSOC))
	{
		$insertacuota=0;
		$SaltarUsurario=0;
		$comparado=$row['userid'];
		$namecomparado=$row['name'];
		$surnamecomparado=$row['surname'];
		if($ECHOS==1)echo '<br><br><br><hr><hr>'.$i.'.- Userid: '.$comparado.'<hr><hr>';
		
		$querycuota="SELECT distinct DAY(NOW()) diahoy, MONTH(NOW()) meshoy, YEAR(NOW()) aniohoy,
							x1.userid,x1.name,x1.surname,u.fechacobro paydate, DAY(u.fechacobro) diausuario,
							MONTH(u.fechacobro) mesusuario, YEAR(u.fechacobro) aniousuario,
						t.status,p.name nameprod,u.idproductobase,p.idproducto, u.idstatus, u.freedays,sp.saldo
					FROM xima.usr_cobros u
					INNER JOIN (xima.ximausrs x1)ON(x1.userid=u.userid)
					INNER JOIN (xima.cobro_porcentajes sp)ON(sp.userid=u.userid)
					INNER JOIN xima.usr_productobase s on s.idproductobase= u.idproductobase
					INNER JOIN xima.usr_producto p on p.idproducto= s.idproducto
					INNER JOIN xima.status t on t.idstatus= u.idstatus
					WHERE p.pu=0 and u.idstatus in (10)  AND x1.USERID in ($comparado)
					order by x1.userid ,p.idproducto desc ";
		if($ECHOS==1)echo $querycuota.'<br/>';
		$rescomparado=mysql_query($querycuota) or die($querycuota.mysql_error());
		unset($arraux);
		while ($rowcomparado=mysql_fetch_array($rescomparado, MYSQL_ASSOC)) $arraux[]=$rowcomparado;
		
		//echo "<pre>";print_r($arraux);echo "</pre>";
		
		unset($arrcomparado);
		$productoanterior=0;
		foreach($arraux as $arrval )
		{
			$insertar=1;
			if($productoanterior==2 && $arrval['idproducto']==1)
				$insertar=0;
			if($productoanterior==3 && $arrval['idproducto']==2)
				$insertar=0;
				
			if($insertar==1)
				$arrcomparado[]=$arrval;
				
			$productoanterior=$arrval['idproducto'];
		}
		//echo "<br>ARRCOMPARADO <pre>";print_r($arrcomparado);echo "</pre>";		
		
		foreach($arrcomparado as $arrval )
		{			
			$diausuario=$arrval['diausuario'];
			$paydatecomparado=$arrval['paydate'];
			$producto=str_replace("'","\'",$arrval['nameprod']);

			$query="SELECT c.saldo FROM xima.cobro_porcentajes c WHERE c.userid=$comparado";
			$ressaldo=mysql_query($query) or die($query.mysql_error());
			$rowsaldo= mysql_fetch_array($ressaldo, MYSQL_ASSOC);
			$saldocomparado=$rowsaldo['saldo'];
			
			//COBRANDO LAS CUOTAS MENSUALES A LOS USUARIOS QUE SE LES VENCE HOY Y A LOS QUE NOS DEBEN.
			if($arrval['meshoy']==1 || $arrval['meshoy']=='01')
			{
				if($arrval['diahoy']>=$arrval['diausuario'])
					$fecha=$arrval['aniohoy']."-".$arrval['meshoy']."-".$arrval['diausuario'];
				else
					$fecha=($arrval['aniohoy']-1)."-12-".$arrval['diausuario'];
			}
			else
			{
				$fecultimodia=UltimoDiaActualMes();
				$timest2=strtotime($fecultimodia);
				$ultdia=date('d',$timest2);
			
				//caso del ultimo dia de mes, como 30 junio, que le cobre a los  del 31  tambien
				if($ultdia==date('d') && $arrval['diausuario']>$ultdia)
					$arrval['diausuario']=$ultdia;		

				$fecha=$arrval['aniohoy']."-".$arrval['meshoy']."-".$arrval['diausuario'];
			}

			$SaltarUsurario=1;
			if($arrval['diausuario']==date('d'))$SaltarUsurario=0;
			


			if($ECHOS==1)echo "<br>$comparado // ".$fecha." // ".$SaltarUsurario;
			if($SaltarUsurario==0)
			{
				if($arrval['idproducto']==2) $arrval['idproducto']='1,2';
				if($arrval['idproducto']==3) $arrval['idproducto']='1,2,3';

				$arrval['priceprod']=getpriceuser($comparado,$diausuario,'yes',$arrval['idproducto']);//($userid,$dia=0,$status='no',$idproducto=0)

				if($ECHOS==1)echo 	'<br> Montocuota: '.$arrval['priceprod'].' Paydate: '.$arrval['paydate'].' Iproducto: '.$arrval['idproducto'].
									' producto: '.$producto.' status: '.$arrval['status'];

				$fecha=arreglarFechasInvalidas($fecha);
				$fechaevalcomparado=$fecha;

				$montocuota=$arrval['priceprod']*-1;
				$idoper=4;

				$fechainsert=date("Y-m-d H:i:s");
				$ejecutaupdatesaldo=1;			

				//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
				$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`,`cobradordesc`) 
						VALUES ($comparado,'$fechainsert',$montocuota,$idoper,'$producto')";
				if($ECHOS==1)	echo '<br>'.$query;
				mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO
 insertlogs($comparado, $comparado, 'Bill payment ASSISTANT', $query);
							
				//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
				$newsaldo=$saldocomparado+$montocuota;
				$query="	UPDATE xima.cobro_porcentajes SET  saldo=$newsaldo WHERE userid=$comparado";
				if($ECHOS==1)	echo '<br>-->'.$query;
				mysql_query($query) or die($query.mysql_error());
 insertlogs($comparado, $comparado, 'Bill payment ASSISTANT', $query);
							
				//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
				//quedando la resta de la cuota con el saldo en cero o mas, se le ingresa un pago de cero
				if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga mensual --> Userid: '.$comparado.' newsaldo: '.$newsaldo.' saldocomparado: '.$saldocomparado.' Paydate: '.$arrval['paydate'].' Montocuota: '.$montocuota;
				if($ECHOS==1)echo 	'<br> PROCESADO ';				
			}//Saltar usuario	
			$i++;
		}//fin foreach de todos los productos de un comparado
	}//fin while todos los comparados
	
		
	echo "<hr>OJO USER:<br>".$OJOUSERS."<hr>";
?>
