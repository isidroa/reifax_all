<?php 
if($_POST['cobrador']<>"automatico")	session_start();

	$bkouserid=$_SESSION['bkouserid'];
	include 'CallerService.php';
function quitarCaracteres($cad)
{
	$cad=str_replace("^","",$cad);
	$cad=str_replace("'","`",$cad);
	$cad=str_replace('"','`',$cad);
	$cad=str_replace('\r\n','',$cad);
	$cad=nl2br($cad);
	return $cad;
}

if(!isset($XLSaffiliados) && ($_POST['cobrador']<>"automatico"))
{
	class Fecha {
	  var $fecha;
	  function Fecha($anio=0, $mes=0, $dia=0) 
	  {
		   if($anio==0) $anio = Date("Y");
		   if($mes==0) $mes = Date("m");
		   if($dia==0) $dia = Date("d");
		   $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
	  }
	  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
	  {
		   $array_date = explode("-", $this->fecha);
		   $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
	  }
	 
	  function getFecha() { return $this->fecha; }
	}
	
	//compara dos fechas 
	function compara_fechas($fecha1,$fecha2)
	{
		if(strtotime($fecha1)>strtotime($fecha2)) return 1;
		if(strtotime($fecha1)==strtotime($fecha2)) return 0;
		if(strtotime($fecha1)<strtotime($fecha2)) return -1;
	}
	
	//arregla las fechas invalidas
	function arreglarFechasInvalidas($fecha)
	{
		$timestamp=strtotime($fecha);
		$fecha=date('Y-m-d',$timestamp);
		return $fecha;
	}
	
	//ultimo dia del mes actual
	function UltimoDiaActualMes($fecha)
	{
		$timestamp=strtotime($fecha);
		$VL_Ano=date('Y',$timestamp);
		$VL_Mes=date('m',$timestamp); 
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}
	
	//primer dia del mes actual
	function PrimerDiaActualMes($fecha)
	{
		$timestamp=strtotime($fecha);
		$VL_Ano=date('Y',$timestamp);
		$VL_Mes=date('m',$timestamp); 
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}
	//ultimo dia del mes anterior
	function UltimoDiaAnteriorMes()
	{
		$VL_Ano = date ('Y'); 
		$VL_Mes = date ('m')-1; 
		if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
		$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
		$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
		return $FechaFin ;
	}
}
?>
<?php 
if($_POST['wherepdf']<>'XLShistorico' && $_POST['cobrador']<>"automatico")
	include("includes/php/conexion.php");

$grid=$_POST['grid'];
/***************************************** GRID affMarketingUserInfo **************************************************/
if ($grid=="affMarketingUserInfo")
{
	$conex=conectar("xima");

	$limitpag=$_POST['curamount'];
	$pagina=($_POST['curpage']-1)*$limitpag;
	$acumunico=$_POST['acumunico'];
	$acumresidual=$_POST['acumresidual'];

	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	else if(isset($XLSaffiliados))$userid=$XLSaffiliados;		
	if($userid=='' || $userid=NULL)
	{
		$VHeader="new Array('Item','Aff.','Name','Surname','UserID','AE','Status','U. Type','PayDate','P. Code','P. Price',
					'Balance','Mon.','U.%','R1.%','R2.%','','','','')";
		$VFields="[{'name':'affiliated','type':'text','align':'center'},{'name':'name','type':'text','align':'left'},
					{'name':'surname','type':'text','align':'left'},
					{'name':'userid','type':'text','align':'center'},{'name':'executive','type':'text','align':'center'},
					{'name':'status','type':'text','align':'left'},
					{'name':'usertype','type':'text','align':'left'},
					{'name':'paydate','type':'text','align':'left'},{'name':'procode','type':'text','align':'center'},
					{'name':'priceprod','type':'text','align':'right'},{'name':'saldo','type':'decimal','align':'right'},
					{'name':'mesu','type':'text','align':'center'},
					{'name':'pctgu','type':'text','align':'right'},{'name':'pctgr','type':'text','align':'right'},
					{'name':'pctgr2lc','type':'text','align':'right'}]";
		
		$query="SELECT SQL_CALC_FOUND_ROWS (select count(x2.executive) FROM xima.ximausrs x2 
						WHERE x2.executive=x1.userid  and (x2.status='active' or x2.status='free') 
						GROUP BY x2.executive) affiliated, 
						(select count(*) FROM admin.historico h
						WHERE h.useridcomparado=x1.userid AND h.idoper=2
						) history,
						x1.name,x1.surname,x1.userid,x1.usertype,x1.status ,
						date(x1.paydate) paydate, x1.executive, x1.notes,
						p.procode,x1.priceprod,sp.saldo,sp.crr,sp.dependent,
						sp.pctgu,sp.pctgr,sp.mesu,sp.pctgr2lc,sp.pctgr2lv   
				FROM xima.ximausrs x1 
				LEFT JOIN (admin.propretvl p)ON(p.id=x1.idprod) 
				LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid)
				WHERE x1.status<>'suspended'  AND x1.toaffiliate='Y' 
				";// AND x1.paydate<=date('2008-02-29') AND x1.status<>'inactive'
		$limitpag=$_POST['curamount'];
		$pagina=($_POST['curpage']-1)*$limitpag;
		
		$order="";
		if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
		{
			if($_POST['campoOrdenSent']=='ASC')$campoSentido='DESC';
			else $campoSentido='ASC';
			$order=" ORDER BY ".$_POST['campoOrden']." ".$campoSentido." ";
		}
		$query.=$order;
		$query.=" LIMIT $pagina,$limitpag ";
		$i=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":".$row['userid'].",
						\"namefull\":\"".strtoupper(htmlentities ($row['name']))." ".strtoupper(htmlentities ($row['surname']))."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"usertype\":\"".$row['usertype']."\",
						\"executive\":\"".$row['executive']."\",
						\"status\":\"".ucwords(strtolower($row['status']))."\",
						\"paydate\":\"".$row['paydate']."\",
						\"procode\":\"".$row['procode']."\",
						\"notes\":\"".quitarCaracteres($row['notes'])."\",
						\"priceprod\":\"".trim($row['priceprod'])."\"";
/*				$Filas.=',"affiliated":"'.$row['affiliated'].'"'; //."\",
				$Filas.=',"history":"'.$row['history'].'"';
				$Filas.=',"saldo":"'.$row['saldo'].'"';
				$Filas.=',"mesu":"'.$row['mesu'].'"';
				$Filas.=',"pctgu":"'.intval($row['pctgu']).'"';
				$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
				$Filas.=',"pctgr2lc":"'.intval($row['pctgr2lc']).'"';
				$Filas.=',"pctgr2lv":"'.intval($row['pctgr2lv']).'"';
*/
			if($_POST['userid']=='' || $_POST['userid']==NULL)//COMPARADOS
			{
				$Filas.=',"affiliated":"'.$row['affiliated'].'"';
				$Filas.=',"history":"'.$row['history'].'"';
				$Filas.=',"saldo":"'.$row['saldo'].'"';
				//$Filas.=',"crr":"'.$row['crr'].'"';
				//$Filas.=',"dependent":"'.$row['dependent'].'"';
				$Filas.=',"mesu":"'.$row['mesu'].'"';
				$Filas.=',"titulo":"'.$row['titulo'].'"';
				$Filas.=',"pctgu":"'.intval($row['pctgu']).'"';
				$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
				$Filas.=',"pctgr2lc":"'.intval($row['pctgr2lc']).'"';
				//$Filas.=',"pctgr2lv":"'.intval($row['pctgr2lv']).'"';
			}
			else//COMPARABLES
			{
				$Filas.=',"unicocomparable":"'.$row['unicocomparable'].'"';
				$Filas.=',"residualcomparable":"'.$row['residualcomparable'].'"';
				$Filas.=',"totalresidualcomparable":"'.$row['totalresidualcomparable'].'"';
				$Filas.=',"totalresidualcomparable":"'.$row['totalresidualcomparable'].'"';
			}
				
						
			$Filas.="}";
			$i++;
		}
	
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);	
		$TotalRegistros=$num_row[0];
	
	}
	else
	{
		if(isset($_POST['userid']) && !isset($XLSaffiliados) )$userid=$_POST['userid'];
		else $userid=$XLSaffiliados;		
		if($_POST['historygrid']==0 || $_POST['historygrid']=='')
		{
			$stat=" and (x1.status='active' or x1.status='free') AND x1.toaffiliate='Y' ";
			$suspHeader="";
			$suspFields="";
			$suspQueryCampos="";
			$suspQueryLeftJoin="";
			$suspFilas="";
			
			if($_POST['usersuspended']==1)


			{
				 $stat=" and (x1.status='suspended') ";
				$suspHeader=",'Suspended'";
				$suspFields=",{'name':'datesuspended','type':'text','align':'left'}";
				$suspQueryCampos=" ,date(sc.fechaUpd) datesuspended ";
				$suspQueryLeftJoin=" LEFT JOIN (admin.statuscli sc)ON(sc.userid=x1.userid) ";
			} 
			if($_POST['usersuspended']==2)
			{
				 $stat=" and (x1.status='inactive') ";
				$suspHeader=",'Inactived'";
				$suspFields=",{'name':'dateinactived','type':'text','align':'left'}";
				$suspQueryCampos=" ,date(sc.fechaUpd) dateinactived ";
				$suspQueryLeftJoin=" LEFT JOIN (admin.statuscli sc)ON(sc.userid=x1.userid) ";
			}
			
			$VHeader="new Array('Item','PayDate','Name','Surname','UserID','Email','Phone','AE','Status'".$suspHeader.",'U. Type','P. Price')";
//,'Unique','Residual','Residual2L'
			$VFields="[{'name':'paydate','type':'text','align':'left'},{'name':'name','type':'text','align':'left'},
						{'name':'surname','type':'text','align':'left'},{'name':'userid','type':'text','align':'center'},
						{'name':'email','type':'text','align':'left'},{'name':'hometelephone','type':'text','align':'left'},
						{'name':'executive','type':'text','align':'center'},
						{'name':'status','type':'text','align':'left'}".$suspFields.",{'name':'usertype','type':'text','align':'left'},
						{'name':'priceprod','type':'decimal','align':'right'}]";
//,{'name':'unicocomparable','type':'decimal','align':'right'},{'name':'residualcomparable','type':'decimal','align':'right'},{'name':'residual2lcomparable','type':'decimal','align':'right'}	
			$query="SELECT SQL_CALC_FOUND_ROWS x1.name,x1.surname,x1.userid
					,x1.usertype,x1.status,x1.executive,x1.hometelephone,x1.email
					,date(x1.paydate) paydate
					,p.procode,x1.priceprod,sp2.ganxhijos,
					(select count(x2.executive) FROM xima.ximausrs x2 
						WHERE x2.executive=x1.userid  and (x2.status='active' or x2.status='free') 
						GROUP BY x2.executive) affiliated
					$suspQueryCampos
					FROM xima.ximausrs x1
					LEFT JOIN (admin.propretvl p)ON(p.id=x1.idprod)
					LEFT JOIN (xima.ximausrs x2,admin.saldoporcentaje sp)ON(x2.userid='".$userid."' AND sp.userid='".$userid."' )
					LEFT JOIN (admin.saldoporcentaje sp2)ON(sp2.userid=x1.userid)					
					$suspQueryLeftJoin 
					WHERE x1.executive='".$userid."'  $stat ";

//,(sp2.pctgu*(sp2.pagoxima/100)) unicocomparable,(sp2.pctgr*(sp2.pagoxima/100)) residualcomparable,(sp2.pctgr2lc*(sp2.pagoxima/100)) residual2lcomparable  

			if($limitpag==600 &&	$pagina==0)
			{
				$acumunico=0;
				$acumresidual=0;
				$restotalru=mysql_query($query) or die($query.mysql_error());
				while ($rowrestotalru=mysql_fetch_array($restotalru, MYSQL_ASSOC))
				{
					$acumunico+=$rowrestotalru['unicocomparable'];
					$acumresidual+=$rowrestotalru['residualcomparable'];
				}
			}				
			$limitpag=$_POST['curamount'];
			$pagina=($_POST['curpage']-1)*$limitpag;
			
			$order="";
			if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
			{
				if($_POST['campoOrdenSent']=='ASC')$campoSentido='DESC';
				else $campoSentido='ASC';
				$order=" ORDER BY ".$_POST['campoOrden']." ".$campoSentido." ";
			}
			$query.=$order;
			$query.=" LIMIT $pagina,$limitpag ";//10
		
			$i=0;
			$Filas="";
			$res=mysql_query($query) or die($query.mysql_error());
			while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
			{
				if($i>0)$Filas.=",";
				$Filas.="{\"userid\":".$row['userid'].",
							\"namefull\":\"".strtoupper(htmlentities ($row['name']))." ".strtoupper(htmlentities ($row['surname']))."\",
							\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
							\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
							\"email\":\"".strtolower($row['email'])."\",
							\"hometelephone\":\"".$row['hometelephone']."\",
							\"usertype\":\"".$row['usertype']."\",
							\"executive\":\"".$row['executive']."\",
							\"status\":\"".ucwords(strtolower($row['status']))."\",
							\"paydate\":\"".$row['paydate']."\",
							\"procode\":\"".$row['procode']."\",
							\"priceprod\":\"".trim($row['priceprod'])."\"";
				if($stat<>"" && $_POST['usersuspended']==1)$Filas.=',"datesuspended":"'.$row['datesuspended'].'"';
				if($stat<>"" && $_POST['usersuspended']==2)$Filas.=',"dateinactived":"'.$row['dateinactived'].'"';
				if($_POST['userid']=='' || $_POST['userid']==NULL)//COMPARADOS
				{
					$Filas.=',"affiliated":"'.$row['affiliated'].'"';
					$Filas.=',"history":"'.$row['history'].'"';
					$Filas.=',"mesu":"'.$row['mesu'].'"';
					$Filas.=',"pctgu":"'.intval($row['pctgc']).'"';
					$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
				}
				else//COMPARABLES
				{
/*					$Filas.=',"unicocomparable":"'.$row['unicocomparable'].'"';
					$Filas.=',"residualcomparable":"'.$row['residualcomparable'].'"';
					$Filas.=',"residual2lcomparable":"'.$row['residual2lcomparable'].'"';
					$Filas.=',"totalresidualcomparable":"'.$row['totalresidualcomparable'].'"';
					$Filas.=',"totalresidualcomparable":"'.$row['totalresidualcomparable'].'"';
					$Filas.=',"ganxhijos":"'.$row['ganxhijos'].'"';
*/					
					$Filas.=',"affiliated":"'.$row['affiliated'].'"';
					
				}
							
				$Filas.="}";
				$i++;
			}
		
			$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
			$num_row=mysql_fetch_row($res);	
			$TotalRegistros=$num_row[0];
		}
		else//ES LA SEGUNDA TABLA Y SE VA A MOSTRAR  EN EL GRID DE HISTORICO
		{
			$fechaInicial="'".$_POST['fechaInicial']."'";
			$fechaFinal="'".$_POST['fechaFinal']."'";
			if(!isset($exportWhere))$exportWhere='';
			if($exportWhere=='pdf')
			{
				$ArFields=array(
								array('name'=>'fechatrans','type'=>'text','align'=>'center','title'=>'Date','size'=>'18'),
								array('name'=>'titulo','type'=>'text','align'=>'left','title'=>'Transaction','size'=>'65'),
								array('name'=>'useridcomparable','type'=>'text','align'=>'center','title'=>'UserID','size'=>'12'),
								array('name'=>'namecomparable','type'=>'text','align'=>'left','title'=>'Name','size'=>'30'),
								array('name'=>'surnamecomparable','type'=>'text','align'=>'left','title'=>'Surname','size'=>'30'),
								array('name'=>'email','type'=>'text','align'=>'center','title'=>'Email','size'=>'45'),
								array('name'=>'hometelephone','type'=>'text','align'=>'center','title'=>'Phone','size'=>'20'),
								array('name'=>'paydatecomparable','type'=>'text','align'=>'center','title'=>'Paydate','size'=>'18'),
								array('name'=>'procode','type'=>'text','align'=>'center','title'=>'P. Code','size'=>'15'),
								array('name'=>'priceprodcomparable','type'=>'text','align'=>'right','title'=>'P. Price','size'=>'15'),
								array('name'=>'monto','type'=>'text','align'=>'right','title'=>'Balance','size'=>'20')
							   );
//								array('name'=>'statuscomparable','type'=>'text','align'=>'center','title'=>'Status','size'=>'15'),
//								array('name'=>'usertypecomparable','type'=>'text','align'=>'center','title'=>'U. Type','size'=>'15'),'P. Code',
			}
			$VHeader="new Array('Item','Date','Transaction','UserID','Name','Surname','Email','Phone','Paydate','Status','U. Type','P. Price','Balance')";
	//		$VFields="new Array('name','surname','userid','status','usertype','paydate','procode','priceprod')";
			$VFields="[{'name':'fechatrans','type':'text','align':'left'},{'name':'titulo','type':'text','align':'left'},
						{'name':'useridcomparable','type':'text','align':'center'},{'name':'namecomparable','type':'text','align':'left'},
						{'name':'surnamecomparable','type':'text','align':'left'},
						{'name':'email','type':'text','align':'left'},{'name':'hometelephone','type':'text','align':'left'},
						{'name':'paydatecomparable','type':'text','align':'left'},
						{'name':'statuscomparable','type':'text','align':'left'},{'name':'usertypecomparable','type':'text','align':'left'},
						{'name':'priceprodcomparable','type':'decimal','align':'right'},
						{'name':'monto','type':'decimal','align':'right'}]";
						
			$query="SELECT SQL_CALC_FOUND_ROWS date(h.fechatrans) fechatrans,
						o.titulo,h.monto,o.idoper,
						h.useridcomparable,h.namecomparable,h.surnamecomparable,
						h.statuscomparable,date(h.paydatecomparable) paydatecomparable,
						h.usertypecomparable,h.priceprodcomparable,p.procode,
						h.idcobro,f.notas,x1.email,x1.hometelephone ,sp.ganxhijos  ,y.transactionid
					FROM admin.historico h 
					LEFT JOIN (admin.operaciones o)ON(o.idoper=h.idoper) 
					LEFT JOIN (admin.propretvl p)ON(p.id=h.idprodcomparable) 
					LEFT JOIN (admin.fechacobro f)ON(f.idcobro=h.idcobro)
					LEFT JOIN (admin.paypal y)ON(y.paypalid=f.paypalid)
					LEFT JOIN (xima.ximausrs x1)ON(x1.userid=h.useridcomparable)
					LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=h.useridcomparable)
					WHERE h.useridcomparado=".$userid."  
					AND h.eliminado='N'
					AND h.fechatrans BETWEEN $fechaInicial AND $fechaFinal 
					ORDER BY h.fechatrans,h.idtrans";//
			if($exportWhere<>'pdf')
			{
				$i=0;
				$haysaldofinal=0;
				$Filas="";
				$res=mysql_query($query) or die($query.mysql_error());
				while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
				{
					if($i>0)$Filas.=",";
					$Filas.="{\"useridcomparable\":\"".$row['useridcomparable']."\",
								\"fechatrans\":\"".$row['fechatrans']."\",
								\"titulo\":\"".$row['titulo']."\",
								\"namecomparable\":\"".ucwords(strtolower(htmlentities ($row['namecomparable'])))."\",
								\"surnamecomparable\":\"".ucwords(strtolower(htmlentities ($row['surnamecomparable'])))."\",
								\"email\":\"".strtolower($row['email'])."\",
								\"hometelephone\":\"".$row['hometelephone']."\",
								\"paydatecomparable\":\"".$row['paydatecomparable']."\",
								\"usertypecomparable\":\"".$row['usertypecomparable']."\",
								\"statuscomparable\":\"".$row['statuscomparable']."\",
								\"procode\":\"".$row['procode']."\",
								\"priceprodcomparable\":\"".$row['priceprodcomparable']."\",
								\"idoper\":\"".$row['idoper']."\",
								\"monto\":\"".$row['monto']."\",
								\"idcobro\":\"".$row['idcobro']."\",
								\"transactionid\":\"".$row['transactionid']."\",
								\"ganxhijos\":\"".$row['ganxhijos']."\",
								\"notas\":\"".$row['notas']."\"
								";
					$Filas.="}";
					$i++;
					if($row['idoper']==3)$haysaldofinal=1;
				}
				if(($haysaldofinal==0 || $_POST['fullhist']==1) && $i>1 )
				{
					$query2="SELECT saldo FROM admin.saldoporcentaje sp
							WHERE sp.userid=".$_POST['userid'];
					$res=mysql_query($query2) or die($query2.mysql_error());
					$row= mysql_fetch_array($res, MYSQL_ASSOC);
					$Filas.=",{\"userid\":\" \",
								\"fechatrans\":\"".date('Y-m-d')."\",
								\"titulo\":\"CURRENT BALANCE \",
								\"name\":\" \",
								\"surname\":\" \",
								\"name\":\" \",
								\"surname\":\" \",
								\"paydate\":\" \",
								\"usertype\":\" \",
								\"status\":\" \",
								\"procode\":\" \",
								\"priceprod\":\" \",
								\"idoper\":\"3\",
								\"monto\":\"".$row['saldo']."\",
								\"idcobro\":\"\",
								\"ganxhijos\":\"\",
								\"notas\":\"\"";
					$Filas.="}";
				}
				
				$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
				$num_row=mysql_fetch_row($res);			
				$TotalRegistros=$num_row[0];
			}//if $exportWhere
		}
	}
	if($exportWhere<>'pdf')
	{
		echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		mysql_close($conex);
	}
	else
	{
		//echo "^aqqqii ".$Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		$xSql=$query;
	}
	
	
}//if ($grid=="affMarketingUserInfo")

/***************************************** GRID affMarketingDeudores **************************************************/
if ($grid=="affMarketingDeudores" )
{
	$conex=conectar("xima");
	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	if($userid=='' || $userid=NULL)
	{
$VHeader="new Array('','','Item','Name','Surname','UserID','Status','U. Type','PayDate','P. Code','P. Price','Card Type','Card Number',






			'MonthU','Uni. %','Res1. %','Balance')";//,''
$VFields="[{'name':'name','align':'left'},{'name':'surname','align':'left'},
			{'name':'userid','align':'center'},{'name':'status','type':'text','align':'left'},
			{'name':'usertype','align':'left'},{'name':'paydate','align':'left'},
			{'name':'procode','align':'center'},{'name':'priceprod','align':'right'},
			{'name':'cardtype','align':'left'},{'name':'cardnumber','align':'left'},
			{'name':'mesu','align':'center'},
			{'name':'pctgu','align':'right'},{'name':'pctgr','align':'right'},
			{'name':'saldo','align':'right'}]";
		
		$query="SELECT SQL_CALC_FOUND_ROWS DISTINCT 
						x1.name,x1.surname,x1.userid,x1.usertype,x1.status ,
						date(x1.paydate) paydate, x1.notes,x1.cardtype,x1.cardnumber,
						p.procode,x1.priceprod,sp.saldo,sp.pctgu,sp.pctgr,sp.mesu, 
				(select count(*) FROM admin.historico h1
				WHERE h1.useridcomparado=x1.userid  and (h1.idoper=4 || h1.idoper=16)
				) cobros
				FROM xima.ximausrs x1
				LEFT JOIN (admin.propretvl p)ON(p.id=x1.idprod)
				LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid)
				LEFT JOIN (admin.historico h)ON(h.useridcomparado=x1.userid)
				WHERE (x1.status='active' or x1.status='freeze') AND x1.toaffiliate='Y' 
				AND sp.deudor='Y'";
		$query=$query.$qwhe;
		
		$limitpag=$_POST['curamount'];
		$pagina=($_POST['curpage']-1)*$limitpag;
		
		$order="";
		if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
		{
			if($_POST['campoOrdenSent']=='ASC')$campoSentido='DESC';
			else $campoSentido='ASC';
			$camOrden=$_POST['campoOrden'];
//			if($camOrden=='paydate') $camOrden=" date_format(paydate,'%d') ";
			$order=" ORDER BY ".$camOrden." ".$campoSentido." ";
		}
		$query.=$order;
		$query.=" LIMIT $pagina,$limitpag ";
	
		$i=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		$acumCobro=0;
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":".$row['userid'].",
						\"namefull\":\"".strtoupper(htmlentities ($row['name']))." ".strtoupper(htmlentities ($row['surname']))."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"usertype\":\"".$row['usertype']."\",
						\"status\":\"".ucwords(strtolower($row['status']))."\",
						\"paydate\":\"".$row['paydate']."\",
						\"procode\":\"".$row['procode']."\",
						\"cardtype\":\"".$row['cardtype']."\",
						\"cardnumber\":\"".$row['cardnumber']."\",
						\"cobros\":\"".$row['cobros']."\",
						\"notes\":\"".quitarCaracteres($row['notes'])."\",
						\"priceprod\":\"".trim($row['priceprod'])."\"";
			$Filas.=',"saldo":"'.$row['saldo'].'"';
			$Filas.=',"pctgu":"'.intval($row['pctgu']).'"';
			$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
			$Filas.=',"mesu":"'.$row['mesu'].'"';
			$Filas.="}";
			$acumCobro=$acumCobro+trim($row['priceprod']);
			$i++;
		}
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);	
		$TotalRegistros=$num_row[0];
		echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$paypal."^".$acumCobro;
	}
	else
	{
		if($_POST['realizarcobro']==1)//Realizar el cobro de la cuota
		{
			
			if($bkouserid=='')	$cobrador=$_POST['cobrador'];
			else $cobrador='cobroBKO: '.$bkouserid;

			$cbPaypal=$_POST['cbPaypal'];
//if($cbPaypal=='true')return;
			$controlsmith="";

			$queryDeud="SELECT 	x1.userid,x1.name,x1.surname,x1.status,x1.paydate,x1.usertype,
						x1.idprod,x1.priceprod,sp.saldo,
						x1.email,x1.cardtype,
						x1.cardholdername,x1.cardnumber,x1.expirationdate,
						x1.csvnumber,x1.billingaddress,x1.billingcity,x1.billingstate,
						x1.billingzip 
						FROM xima.ximausrs x1 
						LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid) 
						WHERE x1.userid IN(".$_POST['userid'].")";
//echo "<br>".$queryDeud."<br>";
			$resDeu=mysql_query($queryDeud) or die($queryDeud.mysql_error());
			$cobrosi=0;
			$cobrono=0;
			$cuenta=0;
			while($row=mysql_fetch_array($resDeu, MYSQL_ASSOC))
			{
				$fecha_pago=date('Y-m-d H:i:s');//$_POST['fechapago'].' '.
				$query2="SELECT h.monto
						FROM admin.historico h
						WHERE h.useridcomparado=".$row['userid']."
						AND h.fechatrans>=(SELECT MAX(h2.fechatrans) fechatrans FROM admin.historico h2
						WHERE h2.useridcomparado=h.useridcomparado AND h2.idoper=2)";
				$res2=mysql_query($query2) or die($query2.mysql_error());
				$sumandotodo=0;
				while($row2= mysql_fetch_array($res2, MYSQL_ASSOC))$sumandotodo+=$row2['monto'];						
				$calculandosaldo=abs($sumandotodo);
//echo "<br><br><br>userid: ".$row['userid']." pago: ".$calculandosaldo;

				if($cbPaypal=='true')	require('paypal_cobro.php');
				else
				{
					$creditCardType = $row['cardtype'];
					$creditCardNumber = $row['cardnumber'];
					$resArray['AMT']=$amount = $calculandosaldo;//$_POST['montocobrar'];
					$email = $row['email'];
					$useridpay = $row['userid'];
					$resArray['TRANSACTIONID']='TRANSACTION'.strtotime(date('Y-m-d H:i:s'));
					$resArray['ACK']=$paypal='SUCCESS';

					$queryPay='Insert into admin.paypal(estado,transactionID,amount,AVS,CVV,fecha,email,userid) values("'.$resArray['ACK'].'",
								"'.$resArray['TRANSACTIONID'].'",'.$resArray['AMT'].',"","",NOW(),"'.$email.'",'.$useridpay.')';
//echo $queryPay;return;
					mysql_query($queryPay) or die($queryPay.mysql_error());					
				}

	$controlsmith=" --> CardHolderName:".$row['cardholdername']." User:".$row['name']." ".$row['surname']." (".$row['userid'].") Amount: ".abs($sumandotodo)." Hoy: ".$fecha_pago." cobro en paypal ";
//echo " paypal: ".$paypal;
	
				if(strtoupper($resArray["ACK"])!='FAILURE' || $cbPaypal=='false')
				{
					$queryIns="SELECT paypalid FROM admin.paypal WHERE transactionID='".$resArray['TRANSACTIONID']."'";
					$res5=mysql_query($queryIns) or die($queryIns.mysql_error());
					$row5=mysql_fetch_array($res5, MYSQL_ASSOC);
					if($row5['paypalid']=='') $row5['paypalid']=0;
												
					$queryIns="INSERT INTO admin.fechacobro (idcobro,fechacobro,userid,monto,idoper,paypalid,cobrador)
						VALUES(null,'".$fecha_pago."',".$row['userid'].",".abs($sumandotodo).",5,".$row5['paypalid'].",'$cobrador')";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO EN TABLA FECHASCOBRO
					$calculandosaldo+=$row['saldo'];
//echo "<br>".$queryIns;
					$queryIns="SELECT idcobro FROM admin.fechacobro 
							WHERE fechacobro='".$fecha_pago."' AND userid=".$row['userid']."
							AND idoper=5";
					$res3=mysql_query($queryIns) or die($queryIns.mysql_error());
					$row3=mysql_fetch_array($res3, MYSQL_ASSOC);

					if(	$row['status']=='freeze')$idop=17;
					else $idop=5;
//echo "<br>".$queryIns;
					$queryIns="INSERT INTO admin.historico (idtrans,fechatrans,idoper,useridcomparado,monto,namecomparado,surnamecomparado,statuscomparado,paydatecomparado,usertypecomparado,idprodcomparado,priceprodcomparado,idcobro)
						VALUES(null,'".$fecha_pago."',".$idop.",".$row['userid'].",".abs($sumandotodo).",'".$row['name']."','".$row['surname']."','".$row['status']."','".$row['paydate']."','".$row['usertype']."',".$row['idprod'].",".$row['priceprod'].",".$row3['idcobro'].")";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO
//echo "<br>".$queryIns;
					if($calculandosaldo>=0) $qdeu=" ,deudor='N' ";
					else $qdeu=" ,deudor='Y' ";
					
					$queryIns="	UPDATE admin.saldoporcentaje SET  tipopago=0, saldo=$calculandosaldo,pagoxima=pagoxima+".$sumandotodo.$qdeu."
							WHERE userid=".$row['userid'];
					mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO
	//echo "<br>".$queryIns;
	$controlsmith.=" | ".$resArray['TRANSACTIONID']." | guarda en historico";
					$cobrosi++;
				}
				else
					$cobrono++;

				$text=$paypal.$controlsmith." ACTIVOS\r\n";
				$f1=fopen("cobrospaypal.txt","a+");
				fputs($f1,$text);
				fclose($f1);
				$cuenta++; 
			}//while de clientes deudoras
			echo $cuenta."^".$cobrosi."^".$cobrono;
		}//if($_POST['realizarcobro']==1)
	}
	mysql_close($conex);

}//if ($grid=="affMarketingDeudores")

/***************************************** GRID affMarketingDeudoresInac **************************************************/
if ($grid=="affMarketingDeudoresInac")
{
	$conex=conectar("xima");

	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	if($userid=='' || $userid=NULL)
	{
$VHeader="new Array('','','Item','Name','Surname','UserID','Status','U. Type','PayDate','P. Code','P. Price','Card Type','Card Number',
			'MonthU','Uni. %','Res1. %','Balance','Paypal Error')";
$VFields="[{'name':'name','align':'left'},{'name':'surname','align':'left'},
			{'name':'userid','align':'center'},{'name':'status','type':'text','align':'left'},
			{'name':'usertype','align':'left'},{'name':'paydate','align':'left'},
			{'name':'procode','align':'center'},{'name':'priceprod','align':'right'},
			{'name':'cardtype','align':'left'},{'name':'cardnumber','align':'left'},
			{'name':'mesu','align':'center'},
			{'name':'pctgu','align':'right'},{'name':'pctgr','align':'right'},
			{'name':'saldo','align':'right'},
			{'name':'paypal','align':'left'}]";
		
		$query="SELECT SQL_CALC_FOUND_ROWS DISTINCT 
						x1.name,x1.surname,x1.userid,x1.usertype,x1.status,x1.paypal,
						date(x1.paydate) paydate,x1.notes,x1.cardtype,x1.cardnumber,
						p.procode,x1.priceprod,sp.saldo,sp.pctgu,sp.pctgr,sp.mesu 
				FROM xima.ximausrs x1
				LEFT JOIN (admin.propretvl p)ON(p.id=x1.idprod)
				LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid)
				LEFT JOIN (admin.historico h)ON(h.useridcomparado=x1.userid)
				LEFT JOIN (admin.statuscli st)ON(st.userid=x1.userid)
				WHERE (x1.status='inactive' or x1.status='freezeinac')			";// AND x1.toaffiliate='Y' AND sp.deudor='Y'";

		$limitpag=$_POST['curamount'];
		$pagina=($_POST['curpage']-1)*$limitpag;
		
		$order="";
		if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
		{
			if($_POST['campoOrdenSent']=='ASC')$campoSentido='DESC';
			else $campoSentido='ASC';
			$camOrden=$_POST['campoOrden'];
			if($camOrden=='paydate') $camOrden=" date_format(paydate,'%d') ";
//			$order=" ORDER BY if((SELECT max(f.fechacobro) FROM admin.fechacobro f
//where f.idoper=5 and f.userid=x1.userid) is null,NOW(),(SELECT max(f.fechacobro) FROM admin.fechacobro f
//where f.idoper=5 and f.userid=x1.userid) ), ".$camOrden." ".$campoSentido." ";
		}
		else
		{
			//$order=" ORDER BY if((SELECT max(f.fechacobro) FROM admin.fechacobro f
//where f.idoper=5 and f.userid=x1.userid) is null,NOW(),(SELECT max(f.fechacobro) FROM admin.fechacobro f
//where f.idoper=5 and f.userid=x1.userid) )";
			$order=" ORDER BY (SELECT max(h1.fechatrans) FROM admin.historico h1
where h1.idoper=4 and h1.useridcomparado=x1.userid) desc ";
		}
		$query.=$order;
		$query.=" LIMIT $pagina,$limitpag ";

	
		$i=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":".$row['userid'].",
						\"namefull\":\"".strtoupper(htmlentities ($row['name']))." ".strtoupper(htmlentities ($row['surname']))."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"usertype\":\"".$row['usertype']."\",
						\"status\":\"".ucwords(strtolower($row['status']))."\",
						\"paydate\":\"".$row['paydate']."\",
						\"notes\":\"".quitarCaracteres($row['notes'])."\",
						\"paypal\":\"".ucwords(strtolower($row['paypal']))."\",
						\"procode\":\"".$row['procode']."\",
						\"cardtype\":\"".$row['cardtype']."\",
						\"cardnumber\":\"".$row['cardnumber']."\",
						\"priceprod\":\"".trim($row['priceprod'])."\"";
			$Filas.=',"saldo":"'.$row['saldo'].'"';
			$Filas.=',"pctgu":"'.intval($row['pctgu']).'"';
			$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
			$Filas.=',"mesu":"'.$row['mesu'].'"';
			$Filas.="}";
			$i++;
		}
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);	
		$TotalRegistros=$num_row[0];
		echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$paypal;
	}
	else
	{
		if($_POST['realizarcobro']==1)//Realizar el cobro de la cuota
		{
			$cobrador='cobInacBko:'.$bkouserid;

			$cbPaypal=$_POST['cbPaypal'];
//if($cbPaypal=='true')return;
			$controlsmith="";

			$queryDeud="SELECT 	x1.userid,x1.name,x1.surname,x1.status,x1.paydate,x1.usertype,
						x1.idprod,x1.priceprod,sp.saldo,
						x1.email,x1.cardtype,
						x1.cardholdername,x1.cardnumber,x1.expirationdate,
						x1.csvnumber,x1.billingaddress,x1.billingcity,x1.billingstate,
						x1.billingzip 
						FROM xima.ximausrs x1 
						LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid) 
						WHERE x1.userid IN(".$_POST['userid'].")";
//echo "<br>".$queryDeud."<br>";
			$resDeu=mysql_query($queryDeud) or die($queryDeud.mysql_error());
			$cobrosi=0;
			$cobrono=0;
			$cuenta=0;
			while($row=mysql_fetch_array($resDeu, MYSQL_ASSOC))
			{
				$fecha_pago=date('Y-m-d H:i:s');//$_POST['fechapago'].' '.
				$query2="SELECT h.monto
						FROM admin.historico h
						WHERE h.useridcomparado=".$row['userid']."
						AND h.fechatrans>=(SELECT MAX(h2.fechatrans) fechatrans FROM admin.historico h2
						WHERE h2.useridcomparado=h.useridcomparado AND h2.idoper=2)  AND h.eliminado='N'";
				$res2=mysql_query($query2) or die($query2.mysql_error());
				$sumandotodo=0;
				while($row2= mysql_fetch_array($res2, MYSQL_ASSOC))$sumandotodo+=$row2['monto'];						
				$calculandosaldo=abs($sumandotodo);
//echo "<br><br><br>userid: ".$row['userid']." pago: ".$calculandosaldo;


				if($cbPaypal=='true')
				{				
//echo "<br><br><br>userid--->2: ".$row['userid']." pago: ".$calculandosaldo;
					require('paypal_cobro.php');
//echo "<br><br><br>userid--->3: ".$row['userid']." pago: ".$calculandosaldo;return;
				}
				else
				{
					$creditCardType = $row['cardtype'];
					$creditCardNumber = $row['cardnumber'];
					$resArray['AMT']=$amount = $calculandosaldo;//$_POST['montocobrar'];
					$email = $row['email'];
					$useridpay = $row['userid'];
					$resArray['TRANSACTIONID']='TRANSACTION'.strtotime(date('Y-m-d H:i:s'));
					$resArray['ACK']=$paypal='SUCCESS';

					$queryPay='Insert into admin.paypal(estado,transactionID,amount,AVS,CVV,fecha,email,userid) values("'.$resArray['ACK'].'",
								"'.$resArray['TRANSACTIONID'].'",'.$resArray['AMT'].',"","",NOW(),"'.$email.'",'.$useridpay.')';
//echo $queryPay;return;
					mysql_query($queryPay) or die($queryPay.mysql_error());					
				}

	$controlsmith=" -->  CardHolderName:".$row['cardholdername']." User:".$row['name']." ".$row['surname']." (".$row['userid'].") Amount: ".abs($sumandotodo)." Hoy: ".$fecha_pago." cobro en paypal ";
//echo " paypal: ".$paypal;
	
				if(strtoupper($resArray["ACK"])!='FAILURE' || $cbPaypal=='false')
				{
					$queryIns="SELECT paypalid FROM admin.paypal WHERE transactionID='".$resArray['TRANSACTIONID']."'";
					$res5=mysql_query($queryIns) or die($queryIns.mysql_error());
					$row5=mysql_fetch_array($res5, MYSQL_ASSOC);
					if($row5['paypalid']=='') $row5['paypalid']=0;
												
					$queryIns="INSERT INTO admin.fechacobro (idcobro,fechacobro,userid,monto,idoper,paypalid,cobrador)
						VALUES(null,'".$fecha_pago."',".$row['userid'].",".abs($sumandotodo).",5,".$row5['paypalid'].",'$cobrador')";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO EN TABLA FECHASCOBRO
					$calculandosaldo+=$row['saldo'];
//echo "<br>".$queryIns;
					$queryIns="SELECT idcobro FROM admin.fechacobro 
							WHERE fechacobro='".$fecha_pago."' AND userid=".$row['userid']."
							AND idoper=5";
					$res3=mysql_query($queryIns) or die($queryIns.mysql_error());
					$row3=mysql_fetch_array($res3, MYSQL_ASSOC);

					if(	$row['status']=='freezeinac')
					{$st='freeze';$idop=17;}
					else {$st='active';$idop=5;}
//echo "<br>".$queryIns;
					$queryIns="INSERT INTO admin.historico (idtrans,fechatrans,idoper,useridcomparado,monto,namecomparado,surnamecomparado,statuscomparado,paydatecomparado,usertypecomparado,idprodcomparado,priceprodcomparado,idcobro)
						VALUES(null,'".$fecha_pago."',".$idop.",".$row['userid'].",".abs($sumandotodo).",'".$row['name']."','".$row['surname']."','".$row['status']."','".$row['paydate']."','".$row['usertype']."',".$row['idprod'].",".$row['priceprod'].",".$row3['idcobro'].")";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO
//echo "<br>".$queryIns;
					if($calculandosaldo>=0) $qdeu=" ,deudor='N' ";
					else $qdeu=" ,deudor='Y' ";
					
					$queryIns="	UPDATE admin.saldoporcentaje SET  tipopago=0, saldo=$calculandosaldo,pagoxima=pagoxima+".abs($sumandotodo).$qdeu."
							WHERE userid=".$row['userid'];
					mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO

					$query="UPDATE xima.ximausrs SET status='".$st."',statuspat='' WHERE userid=".$row['userid'];
					mysql_query($query) or die($query.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO		
					
					$query="INSERT INTO admin.statuscli (statid,userid,fechaupd,beforestat,afterstat,userid_upd)VALUES(null,".$row['userid'].",NOW(),'".$row['status']."','".$st."',$bkouserid)";
					mysql_query($query) or die($query.mysql_error());
					
	//echo "<br>".$queryIns;
	$controlsmith.=" | ".$resArray['TRANSACTIONID']." | guarda en historico";
					$cobrosi++;
				}
				else
					$cobrono++;
					
				$text=$paypal.$controlsmith." INACTIVOS\r\n";
				$f1=fopen("cobrospaypal.txt","a+");
				fputs($f1,$text);
				fclose($f1);
				$cuenta++; 
			}//while de clientes deudoras

			echo $cuenta."^".$cobrosi."^".$cobrono;
		}//if($_POST['realizarcobro']==1)
	}//else
}//if ($grid=="affMarketingDeudoresInac")

/***************************************** GRID affMarketingCobros **************************************************/
if ($grid=="affMarketingCobros")
{
	$conex=conectar("xima");
	$limitpag=$_POST['curamount'];
	$pagina=($_POST['curpage']-1)*$limitpag;
	$acumunico=$_POST['acumunico'];
	$acumresidual=$_POST['acumresidual'];
	$diauno=$_POST['diauno'];
	$diahoy=$_POST['diahoy'];
	$idtrasaction=$_POST['idtrasaction'];


	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	else if(isset($XLSaffiliados))$userid=$XLSaffiliados;		

	if(isset($_POST['userid']) && !isset($XLSaffiliados) )$userid=$_POST['userid'];
	else $userid=$XLSaffiliados;		

	$diauno="'".$_POST['diauno']."'";
	$diahoy="'".$_POST['diahoy']."'";
	//if(!isset($exportWhere))$exportWhere='';
	if($exportWhere=='pdf')
	{
		$ArFields=array(
				array('name'=>'name','type'=>'text','align'=>'left','title'=>'Name','size'=>'30'),
				array('name'=>'surname','type'=>'text','align'=>'left','title'=>'Surname','size'=>'30'),
				array('name'=>'userid','type'=>'text','align'=>'center','title'=>'UserID','size'=>'12'),
				array('name'=>'status','type'=>'text','align'=>'center','title'=>'Status','size'=>'20'),
				array('name'=>'paydate','type'=>'text','align'=>'center','title'=>'Paydate','size'=>'18'),
				array('name'=>'cardholdername','type'=>'text','align'=>'left','title'=>'Holder Name','size'=>'50'),
				array('name'=>'cardnumber','type'=>'text','align'=>'left','title'=>'Card Number','size'=>'30'),
				array('name'=>'fechacobro','type'=>'text','align'=>'center','title'=>'Date Trans.','size'=>'15'),
				array('name'=>'transactionid','type'=>'text','align'=>'right','title'=>'Paypal Trans.','size'=>'30'),
				array('name'=>'monto','type'=>'text','align'=>'right','title'=>'Amount','size'=>'20'),
				array('name'=>'cobrador','type'=>'text','align'=>'right','title'=>'Source','size'=>'20')
				);
	}
	
	$VHeader="new Array('','Item','','Name','Surname','UserID','Status','Paydate','Holder Name','Card Number','Date Trans.','Paypal Trans.','Amount','Source')";
	$VFields="[{'name':'name','type':'text','align':'left'},{'name':'surname','type':'text','align':'left'},
				{'name':'userid','type':'text','align':'center'},{'name':'status','type':'text','align':'left'},
				{'name':'paydate','type':'text','align':'left'},{'name':'cardholdername','type':'text','align':'left'},
				{'name':'cardnumber','type':'text','align':'left'},
				{'name':'fechacobro','type':'text','align':'left'},{'name':'transactionid','type':'text','align':'left'},
				{'name':'monto','type':'decimal','align':'right'},{'name':'cobrador','type':'text','align':'left'}]";
	$qwhe='';						
	if($idtrasaction=='')
		$qwhe=" AND date(f.fechacobro) BETWEEN $diauno AND $diahoy ";
	else
		$qwhe=" AND p.transactionid='$idtrasaction' ";

	$query="SELECT SQL_CALC_FOUND_ROWS x.name,x.surname,x.notes,x.status,x.cardholdername,x.paydate,
				f.userid,date(f.fechacobro) fechacobro,f.monto,f.cobrador,p.transactionid,x.cardnumber,
				(select count(*) FROM admin.historico h 
				WHERE h.useridcomparado=f.userid  and (h.idoper=4 || h.idoper=16)
				) cobros
			FROM admin.fechacobro f
			LEFT JOIN xima.ximausrs x ON (f.userid=x.userid)
			LEFT JOIN admin.paypal p ON (f.paypalid=p.paypalid)
			WHERE x.status<>'free' AND f.idoper=5 AND x.name is not null $qwhe 
			AND f.monto>0 AND f.eliminado='N'
			ORDER BY f.fechacobro";//x.userid
	if($exportWhere<>'pdf')
	{
		$i=0;
		$haysaldofinal=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":\"".$row['userid']."\",
						\"fechacobro\":\"".$row['fechacobro']."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"status\":\"".$row['status']."\",
						\"paydate\":\"".$row['paydate']."\",
						\"cardholdername\":\"".$row['cardholdername']."\",
						\"cardnumber\":\"".$row['cardnumber']."\",
						\"monto\":\"".$row['monto']."\",
						\"cobros\":\"".$row['cobros']."\",
						\"cobrador\":\"".$row['cobrador']."\",
						\"transactionid\":\"".$row['transactionid']."\",
						\"notes\":\"".quitarCaracteres($row['notes'])."\"
						";
			$Filas.="}";
			$i++;
		}
				
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);			
		$TotalRegistros=$num_row[0];

		echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$diauno."^".$diahoy."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		mysql_close($conex);
	}//if $exportWhere
	else
	{
		//echo "^aqqqii ".$Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		$xSql=$query;
	}
	

	
}//if ($grid=="affMarketingCobros")

/***************************************** GRID affMarketingRefunds **************************************************/
if ($grid=="affMarketingRefunds")
{
	$conex=conectar("xima");
	$limitpag=$_POST['curamount'];
	$pagina=($_POST['curpage']-1)*$limitpag;
	$acumunico=$_POST['acumunico'];
	$acumresidual=$_POST['acumresidual'];
	$diauno=$_POST['diauno'];
	$diahoy=$_POST['diahoy'];
	$idtrasaction=$_POST['idtrasaction'];


	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	else if(isset($XLSaffiliados))$userid=$XLSaffiliados;		

	if(isset($_POST['userid']) && !isset($XLSaffiliados) )$userid=$_POST['userid'];
	else $userid=$XLSaffiliados;		

	$diauno="'".$_POST['diauno']."'";
	$diahoy="'".$_POST['diahoy']."'";
	//if(!isset($exportWhere))$exportWhere='';
	if($exportWhere=='pdf')
	{
		$ArFields=array(
				array('name'=>'name','type'=>'text','align'=>'left','title'=>'Name','size'=>'30'),
				array('name'=>'surname','type'=>'text','align'=>'left','title'=>'Surname','size'=>'30'),
				array('name'=>'userid','type'=>'text','align'=>'center','title'=>'UserID','size'=>'12'),
				array('name'=>'status','type'=>'text','align'=>'center','title'=>'Status','size'=>'20'),
				array('name'=>'paydate','type'=>'text','align'=>'center','title'=>'Paydate','size'=>'18'),
				array('name'=>'cardholdername','type'=>'text','align'=>'left','title'=>'Holder Name','size'=>'50'),
				array('name'=>'fechacobro','type'=>'text','align'=>'center','title'=>'Date Trans.','size'=>'15'),
				array('name'=>'transactionid','type'=>'text','align'=>'right','title'=>'Paypal Trans.','size'=>'30'),
				array('name'=>'monto','type'=>'text','align'=>'right','title'=>'Amount','size'=>'20'),
				array('name'=>'cobrador','type'=>'text','align'=>'right','title'=>'Source','size'=>'20')
				);
	}
	
	$VHeader="new Array('','Item','','Name','Surname','UserID','Status','Paydate','Holder Name','Date Trans.','Paypal Trans.','Amount','Source')";
	$VFields="[{'name':'name','type':'text','align':'left'},{'name':'surname','type':'text','align':'left'},
				{'name':'userid','type':'text','align':'center'},{'name':'status','type':'text','align':'left'},
				{'name':'paydate','type':'text','align':'left'},{'name':'cardholdername','type':'text','align':'left'},
				{'name':'fechacobro','type':'text','align':'left'},{'name':'transactionid','type':'text','align':'left'},
				{'name':'monto','type':'decimal','align':'right'},{'name':'cobrador','type':'text','align':'left'}]";
	$qwhe='';						
	if($idtrasaction=='')
		$qwhe=" AND date(f.fechacobro) BETWEEN $diauno AND $diahoy ";
	else
		$qwhe=" AND p.transactionid='$idtrasaction' ";

	$query="SELECT SQL_CALC_FOUND_ROWS x.name,x.surname,x.notes,x.status,x.cardholdername,x.paydate,
				f.userid,date(f.fechacobro) fechacobro,f.monto,f.cobrador,p.transactionid,
				(select count(*) FROM admin.historico h 
				WHERE h.useridcomparado=f.userid  and (h.idoper=4 || h.idoper=16)
				) cobros
			FROM admin.fechacobro f
			LEFT JOIN xima.ximausrs x ON (f.userid=x.userid)
			LEFT JOIN admin.paypal p ON (f.paypalid=p.paypalid)
			WHERE x.status<>'free' AND f.idoper=6 AND x.name is not null $qwhe 
			AND f.monto>0 AND f.eliminado='N'
			ORDER BY f.fechacobro";//f.fechacobro
	if($exportWhere<>'pdf')
	{
		$i=0;
		$haysaldofinal=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":\"".$row['userid']."\",
						\"fechacobro\":\"".$row['fechacobro']."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"status\":\"".$row['status']."\",
						\"paydate\":\"".$row['paydate']."\",
						\"cardholdername\":\"".$row['cardholdername']."\",
						\"monto\":\"".$row['monto']."\",
						\"cobros\":\"".$row['cobros']."\",
						\"cobrador\":\"".$row['cobrador']."\",
						\"transactionid\":\"".$row['transactionid']."\",
						\"notes\":\"".quitarCaracteres($row['notes'])."\"
						";
			$Filas.="}";
			$i++;
		}
				
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);			
		$TotalRegistros=$num_row[0];

		echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$diauno."^".$diahoy."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		mysql_close($conex);
	}//if $exportWhere
	else
	{
		//echo "^aqqqii ".$Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query."^".$acumunico."^".$acumresidual." ".$haysaldofinal;
		$xSql=$query;
	}
	

	
}//if ($grid=="affMarketingRefunds")

/***************************************** GRID affMarketingComisiones **************************************************/
if ($grid=="affMarketingComisiones")
{
	$conex=conectar("xima");

	$userid='';
	if(isset($_POST['userid']))$userid=$_POST['userid'];
	if($userid=='' || $userid=NULL)
	{
$VHeader="new Array('Item','Affiliate','Name','Surname','UserID','Status','U. Type','PayDate','P. Code','P. Price',
			'MonthU','Uni. %','Res1. %','Amount','')";
$VFields="[{'name':'affiliated','type':'text','align':'center'},{'name':'name','align':'left'},{'name':'surname','align':'left'},
			{'name':'userid','align':'center'},{'name':'status','type':'text','align':'left'},
			{'name':'usertype','align':'left'},{'name':'paydate','align':'left'},
			{'name':'procode','align':'center'},{'name':'priceprod','align':'right'},{'name':'mesu','align':'center'},
			{'name':'pctgu','align':'right'},{'name':'pctgr','align':'right'},
			{'name':'saldo','align':'right'}]";
		
		$query="SELECT SQL_CALC_FOUND_ROWS DISTINCT 
						x1.name,x1.surname,x1.userid,x1.usertype,x1.status ,
						(select count(x2.executive) FROM xima.ximausrs x2 
						WHERE x2.executive=x1.userid  and (x2.status='active' or x2.status='free') 
						GROUP BY x2.executive) affiliated,
						date(x1.paydate) paydate,
						p.procode,x1.priceprod,sp.saldo,sp.pctgu,sp.pctgr,sp.mesu,sp.comision 
				FROM xima.ximausrs x1
				LEFT JOIN (admin.propretvl p)ON(p.id=x1.idprod)
				LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid)
				LEFT JOIN (admin.historico h)ON(h.useridcomparado=x1.userid)
				WHERE  (x1.status='free'  OR x1.status='active') AND x1.toaffiliate='Y' 
				AND sp.comision>0
				";//AND sp.saldo>0 
//				AND (select count(x2.executive) FROM xima.ximausrs x2 WHERE x2.executive=x1.userid  and (x2.status='active' or x2.status='free') 
//						GROUP BY x2.executive) >1

		$limitpag=$_POST['curamount'];
		$pagina=($_POST['curpage']-1)*$limitpag;
		
		$order="";
		if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
		{
			if($_POST['campoOrdenSent']=='ASC')$campoSentido='DESC';
			else $campoSentido='ASC';
			$camOrden=$_POST['campoOrden'];
//			if($camOrden=='paydate') $camOrden=" date_format(paydate,'%d') ";
			$order=" ORDER BY ".$camOrden." ".$campoSentido." ";
		}
		$query.=$order;
		$query.=" LIMIT $pagina,$limitpag ";
	
		$i=0;
		$Filas="";
		$res=mysql_query($query) or die($query.mysql_error());
		while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
		{
			if($i>0)$Filas.=",";
			$Filas.="{\"userid\":".$row['userid'].",
						\"namefull\":\"".strtoupper(htmlentities ($row['name']))." ".strtoupper(htmlentities ($row['surname']))."\",
						\"name\":\"".ucwords(strtolower(htmlentities ($row['name'])))."\",
						\"surname\":\"".ucwords(strtolower(htmlentities ($row['surname'])))."\",
						\"usertype\":\"".$row['usertype']."\",
						\"status\":\"".ucwords(strtolower($row['status']))."\",
						\"paydate\":\"".$row['paydate']."\",
						\"procode\":\"".$row['procode']."\",
						\"priceprod\":\"".trim($row['priceprod'])."\"";
			$Filas.=',"affiliated":"'.$row['affiliated'].'"';
			$Filas.=',"saldo":"'.$row['comision'].'"';
			$Filas.=',"pctgu":"'.intval($row['pctgu']).'"';
			$Filas.=',"pctgr":"'.intval($row['pctgr']).'"';
			$Filas.=',"mesu":"'.$row['mesu'].'"';
			$Filas.="}";
			$i++;
		}
		$res=mysql_query("SELECT FOUND_ROWS()" ) or die (mysql_error());
		$num_row=mysql_fetch_row($res);	
		$TotalRegistros=$num_row[0];
		
	}
	else
	{
		if($_POST['realizarpago']==1)//Realizar el cobro de la cuota
		{

			$fec="'".$_POST['fechapago']." ".date("H:i:s")."'";
			$montopagar=str_replace(',','',$_POST['montopagar']);
//echo "<br>".$query." ".$fec;
			$queryIns="	SELECT 	x1.name,x1.surname,x1.status,x1.paydate,x1.usertype,
						x1.idprod,x1.priceprod,sp.saldo  
						FROM xima.ximausrs x1 
						LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid) 
						WHERE x1.userid=".$_POST['userid'];
			mysql_query($queryIns) or die($queryIns.mysql_error());
			$res=mysql_query($queryIns) or die($queryIns.mysql_error());
			$row=mysql_fetch_array($res, MYSQL_ASSOC);

			$notes=str_replace("\r\n","<BR>",$_POST['notas']);
			$queryIns="INSERT INTO admin.fechacobro (idcobro,fechacobro,userid,monto,nrodocumento,notas,idoper)
					VALUES(null,$fec,".$_POST['userid'].",".($montopagar*-1).",'".$_POST['nrodocumento']."','".$notes."',10)";
			mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO EN TABLA FECHASCOBRO
echo "<br>".$queryIns;
			$queryIns="SELECT idcobro FROM admin.fechacobro 
						WHERE fechacobro=".$fec." AND userid=".$_POST['userid']."
						AND idoper=10";
			$res2=mysql_query($queryIns) or die($queryIns.mysql_error());
			$row2=mysql_fetch_array($res2, MYSQL_ASSOC);
			mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO EN TABLA FECHASCOBRO
echo "<br>".$queryIns;
			$queryIns="INSERT INTO admin.historico (idtrans,fechatrans,idoper,useridcomparado,monto,namecomparado,surnamecomparado,statuscomparado,paydatecomparado,usertypecomparado,idprodcomparado,priceprodcomparado,idcobro)
					VALUES(null,$fec,10,".$_POST['userid'].",".($montopagar*-1).",'".$row['name']."','".$row['surname']."','".$row['status']."','".$row['paydate']."','".$row['usertype']."',".$row['idprod'].",".$row['priceprod'].",".$row2['idcobro'].")";
			mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO de comision
echo "<br>".$queryIns;
			$queryIns="	UPDATE admin.saldoporcentaje SET saldo=saldo-".$montopagar.",deudor='N'
						,comision=comision-".$montopagar."  
						WHERE userid=".$_POST['userid'];
			mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO
echo "<br>".$queryIns;
		}
	}
	mysql_close($conex);
	echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$query;
}//if ($grid=="affMarketingComisiones")

//***************************************** GRID affMarketingCashFlow **************************************************
if ($grid=="affMarketingCashFlow")
{
	$conex=conectar("xima");
	$DaysCashFlowDefault=$_POST['DaysCashFlowDefault'];
																																//'P. Code','Phone',
	$VHeader="new Array('Item','Fecha Cobro','Name','Surname','UserID','Email','Card Type','PayDate','Status','U. Type','P. Price','Amount')";
	//,{'name':'procode','type':'text','align':'center'},{'name':'hometelephone','type':'text','align':'left'}
	$VFields="[{'name':'evaldate','type':'text','align':'left'},{'name':'name','type':'text','align':'left'},
				{'name':'surname','type':'text','align':'left'},{'name':'userid','type':'text','align':'center'},
				{'name':'email','type':'text','align':'left'},
				{'name':'cardtype','type':'text','align':'left'},{'name':'paydate','type':'text','align':'left'},
				{'name':'status','type':'text','align':'left'},
				{'name':'usertype','type':'text','align':'left'},
				{'name':'priceprod','type':'decimal','align':'right'},{'name':'discount','type':'decimal','align':'right'}]";

	//$limitpag=$_POST['curamount'];
	//$pagina=($_POST['curpage']-1)*$limitpag;
	include("CalculateCashFlow.php");

	$i=0;
	$Filas="";
	$TotalRegistros=count($maindata);
	while ($i<$TotalRegistros)
	{
		if($i>0)$Filas.=",";
		$Filas.="{\"evaldate\":\"".$maindata[$i]['evaldate']."\",
					\"name\":\"".ucwords(strtolower(htmlentities ($maindata[$i]['name'])))."\",
					\"surname\":\"".ucwords(strtolower(htmlentities ($maindata[$i]['surname'])))."\",
					\"userid\":\"".$maindata[$i]['userid']."\",
					\"cardtype\":\"".$maindata[$i]['cardtype']."\",
					\"email\":\"".$maindata[$i]['email']."\",
					\"paydate\":\"".$maindata[$i]['paydate']."\",
					\"status\":\"".$maindata[$i]['status']."\",
					\"usertype\":\"".$maindata[$i]['usertype']."\",
					\"priceprod\":\"".$maindata[$i]['priceprod']."\",
					\"discount\":\"".$maindata[$i]['discount']."\",
					\"idoper\":\"".$maindata[$i]['idoper']."\",
					\"item\":\"".$maindata[$i]['item']."\"
					";
		$Filas.="}";
//					\"procode\":\"".$maindata[$i]['procode']."\",
//					\"hometelephone\":\"".$maindata[$i]['hometelephone']."\",
		$i++;
	}
	mysql_close($conex);
	echo $Filas."^".$VHeader."^".$VFields."^".$TotalRegistros."^".$limitpag."^".$pagina."^".$FechaInicial2."^".$FechaFinal2."^".$query;
}//if ($grid=="affMarketingCashFlow")



if ($grid=="fechacombohistorico")
{
	$conex=conectar('admin');
	$query1="SELECT distinct date_format(h.fechatrans,'%Y-%b-%d') fechaInicial,
					date_format(h.fechatrans,'%Y-%b') fechaInicialAnioMes,
					h.fechatrans fechahoraInicial
			FROM admin.historico h 
			WHERE h.useridcomparado=".$_POST['userid']." AND  h.idoper=2
			ORDER BY h.fechatrans,h.idtrans";// DESC";
	$result=mysql_query($query1) or die($query1.mysql_error());
	while ($row= mysql_fetch_array($result, MYSQL_ASSOC))$vectorsaldoinicial[]=$row;

	$query2="SELECT distinct date_format(h.fechatrans,'%Y-%b-%d') fechaFinal,
					date_format(h.fechatrans,'%Y-%b') fechaFinalAnioMes,
					h.fechatrans fechahoraFinal
			FROM admin.historico h 
			WHERE h.useridcomparado=".$_POST['userid']." AND  h.idoper=3
			ORDER BY h.fechatrans,h.idtrans";// DESC";
	$result=mysql_query($query2) or die($query2.mysql_error());
	while ($row= mysql_fetch_array($result, MYSQL_ASSOC))$vectorsaldofinal[]=$row;
	$cantini=count($vectorsaldoinicial);

	$cantfin=count($vectorsaldofinal);
	$fechas="";
	$queryIns="	SELECT x1.status FROM xima.ximausrs x1 WHERE x1.userid=".$_POST['userid'];
	mysql_query($queryIns) or die($queryIns.mysql_error());
	$res=mysql_query($queryIns) or die($queryIns.mysql_error());
	$row=mysql_fetch_array($res, MYSQL_ASSOC);
//echo "ini: ".$cantini." fin: ".$cantfin."<br>";

	for($i=0;$i<$cantini;$i++)
	{
		if($i>0)$fechas.=",";
		if($i<$cantfin)
		{
			$fechas.="{\"fechaAnioMes\":\"".$vectorsaldoinicial[$i]['fechaInicialAnioMes']."\",
						\"fechaInicial\":\"".$vectorsaldoinicial[$i]['fechaInicial']."\",
						\"fechahoraInicial\":\"".$vectorsaldoinicial[$i]['fechahoraInicial']."\",
						\"fechaFinal\":\"".$vectorsaldofinal[$i]['fechaFinal']."\",
						\"fechahoraFinal\":\"".$vectorsaldofinal[$i]['fechahoraFinal']."\"
					  }";
		}
		else
		{
			$fechaAM=date('Y-M');
			if($row['status']=='active')$fechaAM=$vectorsaldoinicial[$i]['fechaInicialAnioMes'];

			$fechas.="{\"fechaAnioMes\":\"".$fechaAM."\",
						\"fechaInicial\":\"".$vectorsaldoinicial[$i]['fechaInicial']."\",
						\"fechahoraInicial\":\"".$vectorsaldoinicial[$i]['fechahoraInicial']."\",
						\"fechaFinal\":\"".date('Y-m-d H:i:s')."\",
						\"fechahoraFinal\":\"".date('Y-m-d H:i:s')."\"
					  }";
		}
//		$fechas.="<br>";
	}
	
	mysql_close($conex);
	echo $fechas."^".$query1." | ".$query2;
}//if ($grid=="fechacombohistorico")


$query=" ";
$msg="";
$oper=$_POST['oper'];
if($oper=="modificar")
{
	if($_POST['campo']=='status')//MODIFICAR EL STATUS ADEMAS DE XIMAUSRS EN STATUSCLI
	{
		$conex=conectar('admin');

		$query="SELECT status FROM xima.ximausrs WHERE userid=".$_POST['ID'];
		$result = mysql_query($query) or die(mysql_error());
		$row=mysql_fetch_array($result);
		$beforestat=$row[0];
	}

	$ID=$_POST['ID'];
	if(isset($_POST['campo']))//ES MODIFICACION DE DOBLECLICK EN CELDA DE FILA
	{
		$conex=conectar('admin');
		if($_POST['campo']=='transactionid')
		{
			$info=$_POST['info'];
			$query="SELECT max(paypalid) FROM admin.paypal 
					WHERE userid=".$ID;
			$res=mysql_query($query) or die($query." ".mysql_error());
			$row=mysql_fetch_row($res);	

			$query=" UPDATE admin.paypal SET transactionid='".$info."' WHERE paypalid=".$row[0];
			mysql_query($query) or die($query." ".mysql_error());
		}
		else
		{
			if($_POST['campo']=='paydate' || $_POST['campo']=='status')$query=" UPDATE xima.ximausrs ";
			else $query=" UPDATE saldoporcentaje ";
			
			if($_POST['type']=='number')
			{
				$info=$_POST['info'];
				if($info=='' || $info==NULL)$info=0;
				$query.=" SET ".$_POST['campo']."=".$info;
			}	
			if($_POST['type']=='text')
			{
				$info=$_POST['info'];
				if($_POST['campo']=='procode')$info=strtoupper($_POST['info']);
				$query.=" SET ".$_POST['campo']."='".$info."' ";
			}	
			$query.=" WHERE userid=".$_POST['ID'];
			mysql_query($query) or die($query." ".mysql_error());
			if(mysql_affected_rows()==0 && $_POST['campo']<>'paydate')
			{
				$query="SELECT userid FROM saldoporcentaje 
						WHERE userid=".$_POST['ID'];
				mysql_query($query) or die($query." ".mysql_error());
				if(mysql_affected_rows()==0)
				{
					$query="SELECT priceprod FROM xima.ximausrs
							WHERE userid=".$_POST['ID'];
					$res=mysql_query($query) or die($query." ".mysql_error());
					$num_row=mysql_fetch_row($res);	
					$presigmenor=$num_row[0]*(-1);
					
					$query="INSERT INTO saldoporcentaje (idaffmkt,userid,saldo,".$_POST['campo'].")
							VALUES(null,".$_POST['ID'].",".$presigmenor.",".$info.")";
					mysql_query($query) or die($query." ".mysql_error());
				}		
			}
		
			if($_POST['campo']=='status')//MODIFICAR EL STATUS ADEMAS DE XIMAUSRS EN STATUSCLI
			{
				$conex=conectar('admin');//echo 'aqui: '.strtolower($beforestat).'|'.strtolower($info);
				if(strtolower($beforestat)<>strtolower($info))
				{
					$query="INSERT INTO statuscli (statid,userid,fechaupd,beforestat,afterstat,userid_upd)VALUES(null,$ID,NOW(),'".$beforestat."','".$info."',$bkouserid)";//echo 'aqui2 '.$query;
					mysql_query($query) or die(mysql_error());
				}
			}
		}
		echo $query;
		mysql_close($conex);
	}
}

if($oper=="buscarpagocomision")
{
	$ID=$_POST['ID'];
	$cobropago=$_POST['cobropago'];

	$conex=conectar('admin');
	$query=" SELECT idcobro,date(fechacobro) fechacobro,userid,monto,nrodocumento,notas,idoper
			 FROM admin.fechacobro  
			 WHERE userid=".$_POST['ID']." 
			 AND idcobro=$cobropago";
	$res=mysql_query($query) or die($query." ".mysql_error());
	$row=mysql_fetch_array($res, MYSQL_ASSOC);
	mysql_close($conex);
	if($exportWhere<>'pdf')
		echo $row['fechacobro']."^".$row['monto']."^".$row['nrodocumento']."^".str_replace("<BR>","\r\n",$row['notas'])."^".$query;
}

if($oper=="voucherupdate")
{
	$ID=$_POST['ID'];
	$idcobro=$_POST['cobropagocomision'];
	$fechacobro=$_POST['fechacobro']." ".date('H:i:s');
	$montocobrar=$_POST['montocobrar'];
	$nrodoc=$_POST['nrodoc'];
	$notas=$_POST['notas'];
	$conex=conectar('admin');

	$query="SELECT idoper,monto,userid,date(fechacobro) fechacobro FROM admin.fechacobro WHERE idcobro=$idcobro";
	mysql_query($query) or die($query.mysql_error());//BUSCAR OPERACION DE PAGO EN TABLA FECHASCOBRO
	$res=mysql_query($query) or die($query.mysql_error());
	$row=mysql_fetch_array($res, MYSQL_ASSOC);


	$query="UPDATE fechacobro SET nrodocumento='$nrodoc',notas='$notas'
			,fechacobro='$fechacobro' ,monto=$montocobrar 
			WHERE userid=".$ID." AND idcobro=".$idcobro;
	mysql_query($query) or die($query.mysql_error());//UPDATE PAGO EN TABLA FECHASCOBRO
echo $query;

	$query="UPDATE admin.historico SET fechatrans='$fechacobro',monto=$montocobrar WHERE idcobro=".$idcobro;
	mysql_query($query) or die($query.mysql_error());//DELETE PAGO EN TABLA DE HISTORICO 
echo $query;

	if($row['monto']<>$montocobrar)
	{
		$montocobrar=$row['monto']-$montocobrar;
		$query="UPDATE admin.saldoporcentaje SET saldo=saldo-".$montocobrar."
				WHERE userid=".$row['userid'];
		mysql_query($query) or die($query.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO 
echo $query;
	}
	mysql_close($conex);
}

if($oper=="voucherdelete")
{
	$ID=$_POST['ID'];
	$idcobro=$_POST['cobropagocomision'];
	$conex=conectar('admin');
	
	$query="SELECT idoper,monto,userid FROM admin.fechacobro WHERE idcobro=$idcobro";
	mysql_query($query) or die($query.mysql_error());//BUSCAR OPERACION DE PAGO EN TABLA FECHASCOBRO
	$res=mysql_query($query) or die($query.mysql_error());
	$row=mysql_fetch_array($res, MYSQL_ASSOC);
	
	$query="UPDATE admin.fechacobro SET eliminado='Y' WHERE idcobro=".$idcobro;
	mysql_query($query) or die($query.mysql_error());//DELETE PAGO EN TABLA FECHASCOBRO

	$query="UPDATE admin.historico SET eliminado='Y' WHERE idcobro=".$idcobro;
	mysql_query($query) or die($query.mysql_error());//DELETE PAGO EN TABLA DE HISTORICO 

	$query="UPDATE admin.saldoporcentaje SET saldo=saldo-".$row['monto']."
			WHERE userid=".$row['userid'];
	mysql_query($query) or die($query.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO 

	mysql_close($conex);
	echo $query;
}

if($oper=="creddeb")
{
	$ID=$_POST['ID'];
	$cbOperation=$_POST['cbOperation'];
	$txtFechaCobro=$_POST['txtFechaCobro'];
	$montopagar=$_POST['txtMontoCobrar'];
	$txtNroDocumento=$_POST['txtNroDocumento'];
	$txtNotas=str_replace("\r\n","<BR>",$_POST['txtNotas']);
	$conex=conectar('admin');
	if($cbOperation==14 || $cbOperation==15)$montopagar=($montopagar*-1);//si es debito resta 

	$txtFechaCobro=$txtFechaCobro." ".date('H:i:s');
	$queryIns="INSERT INTO admin.fechacobro (idcobro,fechacobro,userid,monto,nrodocumento,notas,idoper)
				VALUES(null,'$txtFechaCobro',".$ID.",".$montopagar.",'".$txtNroDocumento."','".$txtNotas."',$cbOperation)";
	mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO CRED/DEB EN TABLA FECHASCOBRO
echo "<br>\n".$queryIns;

	$queryIns="	SELECT 	x1.name,x1.surname,x1.status,x1.paydate,x1.usertype,
				x1.idprod,x1.priceprod,sp.saldo  
				FROM xima.ximausrs x1 
				LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid) 
				WHERE x1.userid=".$ID;
	mysql_query($queryIns) or die($queryIns.mysql_error());
	$res=mysql_query($queryIns) or die($queryIns.mysql_error());
	$row=mysql_fetch_array($res, MYSQL_ASSOC);
echo "<br>\n".$queryIns;
	$sal=$row['saldo']+$montopagar;
	if($cbOperation==12)
		$prep=" ,prepago=".$montopagar;
	$queryIns="	UPDATE admin.saldoporcentaje SET saldo=".$sal."$prep WHERE userid=".$ID;
	mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO
echo "<br>\n".$queryIns;
	$queryIns="SELECT MAX(idcobro) idcobro FROM admin.fechacobro 
				WHERE userid=".$ID."
				AND idoper=$cbOperation";
	$res2=mysql_query($queryIns) or die($queryIns.mysql_error());
	$row2=mysql_fetch_array($res2, MYSQL_ASSOC);

	$queryIns="INSERT INTO admin.historico (idtrans,fechatrans,idoper,useridcomparado,monto,namecomparado,surnamecomparado,statuscomparado,paydatecomparado,usertypecomparado,idprodcomparado,priceprodcomparado,idcobro)
			VALUES(null,'$txtFechaCobro',$cbOperation,".$ID.",".$montopagar.",'".$row['name']."','".$row['surname']."','".$row['status']."','".$row['paydate']."','".$row['usertype']."',".$row['idprod'].",".$row['priceprod'].",".$row2['idcobro'].")";
	mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO de comision

	if($cbOperation==15)//insertando el registro de ajuste cuando es un refun
	{
		$cbOperation=6;
		$montopagar=($montopagar*-1);
		$txtNotas="Refund Adjustment";
		$queryIns="INSERT INTO admin.fechacobro (idcobro,fechacobro,userid,monto,nrodocumento,notas,idoper)
					VALUES(null,'$txtFechaCobro',".$ID.",".$montopagar.",'','".$txtNotas."',$cbOperation)";
		mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO CRED/DEB EN TABLA FECHASCOBRO
	echo "<br>\n".$queryIns;
	if($sal<0)
	{
		$queryIns="	UPDATE admin.saldoporcentaje SET deudor='Y' WHERE userid=".$ID;
		mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO CRED/DEB EN TABLA FECHASCOBRO
	}
	
		$queryIns="	SELECT 	x1.name,x1.surname,x1.status,x1.paydate,x1.usertype,
					x1.idprod,x1.priceprod,sp.saldo  
					FROM xima.ximausrs x1 
					LEFT JOIN (admin.saldoporcentaje sp)ON(sp.userid=x1.userid) 
					WHERE x1.userid=".$ID;
		mysql_query($queryIns) or die($queryIns.mysql_error());
		$res=mysql_query($queryIns) or die($queryIns.mysql_error());
		$row=mysql_fetch_array($res, MYSQL_ASSOC);
	echo "<br>\n".$queryIns;
		$sal=$row['saldo']+$montopagar;
	
		if($sal>=0) $qdeu=" ,deudor='N' ";
		else $qdeu=" ,deudor='Y' ";
					
		$queryIns="	UPDATE admin.saldoporcentaje SET saldo=".$sal.$qdeu." WHERE userid=".$ID;
		mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO
	echo "<br>\n".$queryIns;
		$queryIns="SELECT MAX(idcobro) idcobro FROM admin.fechacobro 
					WHERE userid=".$ID."
					AND idoper=$cbOperation";
		$res2=mysql_query($queryIns) or die($queryIns.mysql_error());
		$row2=mysql_fetch_array($res2, MYSQL_ASSOC);

		$queryIns="INSERT INTO admin.historico (idtrans,fechatrans,idoper,useridcomparado,monto,namecomparado,surnamecomparado,statuscomparado,paydatecomparado,usertypecomparado,idprodcomparado,priceprodcomparado,idcobro)
				VALUES(null,'$txtFechaCobro',6,".$ID.",".$montopagar.",'".$row['name']."','".$row['surname']."','".$row['status']."','".$row['paydate']."','".$row['usertype']."',".$row['idprod'].",".$row['priceprod'].",".$row2['idcobro'].")";
		mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO de comision

	}
echo "<br>\n".$queryIns;
	mysql_close($conex);
}


if($grid=="reversepays")
{
	$i=0;
	$conex=conectar('admin');
	$query='SELECT userid FROM xima.ximausrs WHERE userid in ('.$_POST['userid'].')';
	$res=mysql_query($query) or die($query.mysql_error());
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{
		$query2 ="SELECT idcobro,monto,fechacobro,paypalid,cobrador  FROM admin.fechacobro where userid=".$row['userid']." order by idcobro desc LIMIT 1";
		$res2=mysql_query($query2) or die($query2.mysql_error());
		$row2= mysql_fetch_array($res2, MYSQL_ASSOC);
		if($row2['idcobro']<>'')
		{

			$fechacobro=$row2['idcobro'].'$'.$row2['fechacobro'].'$'.$row2['paypalid'].'$'.$row2['cobrador'];
//echo $query2."|".$fechacobro;
			$query2 ="DELETE FROM admin.fechacobro WHERE idcobro=".$row2['idcobro'];
			$res2=mysql_query($query2) or die($query2.mysql_error());
		}
		
		$query3 ="SELECT idtrans, fechatrans,monto,idcobro,useridcomparado FROM admin.historico where useridcomparado=".$row['userid']." and idoper=5 order by idtrans desc LIMIT 1";
		$res3=mysql_query($query3) or die($query3.mysql_error());
		$row3= mysql_fetch_array($res3, MYSQL_ASSOC);
		if($row3['idtrans']<>'')
		{
			$historico=$row3['idtrans'].'$'.$row3['fechatrans'].'$'.$row3['useridcomparado'].'$'.$row3['monto'].'$'.$row3['idcobro'];
			$query3 ="DELETE FROM admin.historico WHERE idtrans=".$row3['idtrans'];
			$res3=mysql_query($query3) or die($query3.mysql_error());
		}

		$query4 ="SELECT idaffmkt,userid,saldo,deudor FROM admin.saldoporcentaje WHERE userid=".$row['userid'];
		$res4=mysql_query($query4) or die($query4.mysql_error());
		$row4= mysql_fetch_array($res4, MYSQL_ASSOC);
		if($row4['idaffmkt']<>'')
		{
			$saldoporcentaje=$row4['idaffmkt'].'$'.$row4['userid'].'$'.$row4['saldo'].'$'.$row4['deudor'];
			$query4 ="UPDATE admin.saldoporcentaje SET saldo=saldo-".$row3['monto'].",deudor='Y' WHERE idaffmkt=".$row4['idaffmkt'];
			$res4=mysql_query($query4) or die($query4.mysql_error());
		}
		
		$query5 ="INSERT INTO admin.reversepayslog(fechacobro,historico,saldoporcntaje,userid,userdo) VALUES('".$fechacobro."','".$historico."','".$saldoporcentaje."',".$row['userid'].",".$bkouserid.")";
		$res5=mysql_query($query5) or die($query5.mysql_error());

		$i++;
	}
	if($i>0)echo "1^Reverse charges done succesfully";
	else echo "0^Error: ".$query;

}
?>