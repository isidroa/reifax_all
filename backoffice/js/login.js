Ext.BLANK_IMAGE_URL='../ext/resources/images/default/s.gif';
Ext.onReady(function(){
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
 
	// Create a variable to hold our EXT Form Panel. 
	// Assign various config options as seen.	 
    	var login = new Ext.FormPanel({ 
        labelWidth:80,
        url:'php/login.php', 
        frame:true, 
        title:'Backoffice - Account Login', 
        defaultType:'textfield',
		monitorValid:true,
	// Specific attributes for the text fields for username / password. 
	// The "name" attribute defines the name of variables sent to the server.
//,vtype: 'email'
        items:[{ 
                fieldLabel:'Email', 
                name:'email', 
                allowBlank:false
            },{ 
                fieldLabel:'Password', 
                name:'pwd', 
                inputType:'password', 
                allowBlank:false
            }],
 
	// All the magic happens after the user clicks the button     
        buttons:[{ 
                text:'Login',
                formBind: true,	 
                // Function that fires when user clicks the button 
                handler:function(){ 
                    login.getForm().submit({ 
                        method:'POST', 
                        waitTitle:'Connecting', 
                        waitMsg:'Sending data...',
 
                        success: function(form, action) {
							document.location='conexionAdmin.php';
							//document.location='adminTaskManager.php';
						},
						failure: function(form, action) {
							Ext.Msg.alert("Failure", action.result.msg);
						} 
                    }); 
                } 
            }] 
    });
 
 
	// This just creates a window to wrap the login form. 
	// The login object is passed to the items collection.       
    var win = new Ext.Window({
        layout:'fit',
        width:300,
        height:150,
        closable: false,
        resizable: false,
        plain: true,
        border: false,
		modal: true,
		draggable: false,
        items: [login]
	});
	win.show();
});