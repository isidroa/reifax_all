	function previewHtmlEmail(idtmail)
	{
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			params: { 
				tipo : 'previewEmails',
				idtmail: idtmail
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);

				var form = new Ext.form.FormPanel({
					baseCls: 'x-plain',
					labelWidth: 55,
					items: [{
						xtype:'textfield',
						fieldLabel: 'Name',
						name: 'name',
						id: 'name',
						width:690,
						value: variable.name,
						allowBlank: false  
					},{
						xtype:'textfield',
						fieldLabel: 'Subject',
						name: 'subject',
						id: 'subject',
						width:690,
						value: variable.subject,
						allowBlank: false  
					},{
						xtype: 'htmleditor',
						id: 'bodyemail',
						id: 'bodyemail',
						hideLabel: true,
						height: 500,
						width:750,
						value: variable.body,
						allowBlank: false  
					}],
					buttonAlign: 'center',
					buttons: [{
						text: '<b>Update Template</b>',
						cls: 'x-btn-text-icon',			
						icon: 'images/disk.png',
						formBind: true,
						handler: function(b){
							var form = b.findParentByType('form');
							//form.getForm().fileUpload = true;
							if (form.getForm().isValid()) {
								form.getForm().submit({
									url: 'php/grid_edit.php',
									waitTitle   :'Please wait!',
									waitMsg     :'Loading...',
									params: {
										tipo : 'templateEmails',
										idtmail: idtmail										
									},	
									timeout: 100000,
									method :'POST',
									success: function(form, action) {
										w.close();
										var store1 = Ext.getCmp("gridpanel").getStore();
										store1.reload();
										Ext.Msg.alert('Success', action.result.msg);
										
									},
									failure: function(form, action) {
										Ext.Msg.alert('Error', action.result.msg);
									}
								});
							}
						}
					},'->',{
						text: 'Close',
						cls: 'x-btn-text-icon',			
						icon: 'images/cross.gif',
						handler: function(b){
							w.close();
						}
					}]
				});
				
				
				var w = new Ext.Window({
					title: 'Update Template Email', 
					width: 800,
					height: 620,
					layout: 'fit',
					plain: true,
					bodyStyle: 'padding:5px;',
					items: form
				});
				w.show();
				w.addListener("beforeshow",function(w){
					form.getForm().reset();
				});
			}
		})
	}
	
	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=templateEmails',
		fields: [
			{name: 'idtmail', type: 'int'}
			,{name: 'userid', type: 'int'}
			,'insertdate'
			,'name'
			,'subject'
			,'bodyshort'
			,'usercreator'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'add_button',
			tooltip: 'Click to Add Template Email',
			handler: AddWindow 
        },{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/delete.gif',
			id: 'del_butt',
			tooltip: 'Click to Delete Template Email',
			handler: doDel 
        }]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////

	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idtmail;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}

	function doDel(){
		Ext.MessageBox.confirm('Delete Template Email','Are you sure delete row?.',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					var obtenidos=obtenerSeleccionados();
					if(!obtenidos){
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'You must select a row, by clicking on it, for the delete to work.',
							buttons: Ext.MessageBox.OK,
							icon:Ext.MessageBox.ERROR
						});						
						obtenerTotal();
						return;
					}
					//alert(obtenidos);//return;		
					obtenidos=obtenidos.replace('(','');
					obtenidos=obtenidos.replace(')','');

					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo : 'templateEmails',
							idtmail: obtenidos			
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
							store.reload();
						},
						
						success:function(response,options){
						
							store.reload();
						}
					}); 
				}
			}
		)
	}
	
	function AddWindow()
	{

		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype:'textfield',
				fieldLabel: 'Name',
				name: 'name',
				id: 'name',
				width:690,
				allowBlank: false  
			},{
				xtype:'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'subject',
				width:690,
				allowBlank: false  
			},{
				xtype: 'htmleditor',
				id: 'bodyemail',
				id: 'bodyemail',
				hideLabel: true,
				height: 500,
				width:750,
				allowBlank: false  
			}],
			buttonAlign: 'center',
			buttons: [{
				text: '<b>Create Template</b>',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
                formBind: true,
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/grid_add.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Loading...',
							params: {
								tipo : 'templateEmails'										
							},	
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								w.close();
								store.reload();
								Ext.Msg.alert('Success', action.result.msg);
								
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},'->',{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'New Template Email', 
			width: 800,
			height: 620,
			layout: 'fit',
			plain: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
		w.addListener("beforeshow",function(w){
			form.getForm().reset();
		});
	}//fin function doAdd()

//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderpreviewemail(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlEmail({0})" ><img src="images/editar.png" border="0" title="Preview email" alt="Preview email"></a>',record.data.idtmail);
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Template Email',
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: 'Creation Date', width: 120, align: 'left', sortable: true, dataIndex: 'insertdate'}
			,{header: 'Creator User', width: 120, sortable: true, align: 'left', dataIndex: 'usercreator'}
			,{header: 'IDU', width: 40, sortable: true, align: 'center', dataIndex: 'idtmail'}
			,{header: 'Name', width: 250, sortable: true, align: 'left', dataIndex: 'name'} 
			,{header: 'Subject', width: 250, sortable: true, align: 'left', dataIndex: 'subject'}
			,{header: 'Body', width: 300, align: 'left', sortable: true, dataIndex: 'bodyshort'}
			,{header: ' ', width: 30, sortable: true, align: 'center', dataIndex: 'body', renderer: renderpreviewemail}
		],
		clicksToEdit:2,
		height:470,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
 
});
