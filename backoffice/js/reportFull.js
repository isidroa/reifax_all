Ext.onReady(function(){
	// create the Data Store
    var store = new Ext.data.JsonStore({
        root: 'data',
        totalProperty: 'total',
        idProperty: 'userid',
        remoteSort: true,
        fields: [
            {name: 'userid', type: 'int'},
            {name: 'name', type: 'string'},
            {name: 'status', type: 'string'},
            {name: 'professional_esp', type: 'string'},
            {name: 'username', type: 'string'},
            {name: 'imap_username', type: 'string'},
            {name: 'escrowAgent1', type: 'string'},
            {name: 'escrowAgent2', type: 'string'},
            {name: 'imagen1', type: 'string'},
            {name: 'imagen2', type: 'string'},
            {name: 'imagen3', type: 'string'},
            {name: 'imagen4', type: 'string'},
            {name: 'initialDeposit', type: 'string'},
            {name: 'additionalDeposit', type: 'string'},
            {name: 'investor_offer_factor', type: 'string'},
            {name: 'investor_offer_round', type: 'string'},
            {name: 'investor_offer_type', type: 'string'},
            {name: 'documents', type: 'string'}
        ],

        // load using script tags for cross domain, if the data in on the same domain as
        // this page, an HttpProxy would be better
        proxy: new Ext.data.HttpProxy({
            url: 'php/grid_data2.php?tipo=users-reportFull',
            timeout: 0,
            method: 'POST'
        })
    });
    store.setDefaultSort('userid', 'desc');

    var changeBackground = function(value, metaData, record, rowIndex, colIndex, store) {
		if(value==1){
			var colorF="B0FFC5";	//var colorF="green";
		}else{
			var colorF="FFB0C4";	//var colorF="red";
		}
		//metaData.attr = 'ext:qtip="' + data.feature + ', Total: ' + value + '"';
		return	'<div class="x-grid3-cell-inner" style="background-color:#'+colorF+';">'+
					//'<span style="color:'+colorT+';">'+
					//	text+' Count('+value+')'+
					//'</span>'+
					'&nbsp;'+
				'</div>';
    }
    // pluggable renders
    function renderTopic(value, p, record){
        return String.format(
                '<b><a href="http://extjs.com/forum/showthread.php?t={2}" target="_blank">{0}</a></b><a href="http://extjs.com/forum/forumdisplay.php?f={3}" target="_blank">{1} Forum</a>',
                value, record.data.forumtitle, record.id, record.data.forumid);
    }
    function renderLast(value, p, r){
        return String.format('{0}<br/>by {1}', value.dateFormat('M j, Y, g:i a'), r.data['lastposter']);
    }

    var grid = new Ext.grid.GridPanel({
        //width:700,
        //height:500,
        title:'Special Report',
        //forceFit: true,
        store: store,
        loadMask:true,
        viewConfig: {
        	//autoFill:true,
			//forceFit: true,
			enableRowBody:true,
            showPreview:true,
			getRowClass : function(record, rowIndex, p, store){
                if(this.showPreview){
                    p.body = '<div class="special-report-bo">'+record.data.documents+'</div>';
                    return 'x-grid3-row-expanded';
                }
                return 'x-grid3-row-collapsed';
            }
	    },
	    //trackMouseOver:false,
        //disableSelection:true,
        // grid columns
        columns:[
        	new Ext.grid.RowNumberer(),
        	new Ext.grid.CheckboxSelectionModel(),
        {
            id: 'userid', header: "UserId", dataIndex: 'userid', width: 60, sortable: true,
            renderer:function (val){
				return '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+val+')">'+val+'</a>';
			}
        },{
            header: "Name", dataIndex: 'name', width: 150, sortable: true
        },{
            header: "Status", dataIndex: 'status', width: 80, sortable: true
        },{
            header: "Email Reader", dataIndex: 'username', width: 230, sortable: true
        },{
            header: "Email Delivery", dataIndex: 'imap_username', width: 230, sortable: true
        },{
            header: "Escrow One", dataIndex: 'escrowAgent1', width: 230, sortable: true
        },{
            header: "Escrow Two", dataIndex: 'escrowAgent2', width: 230, sortable: true
        },{
            header: "S1", dataIndex: 'imagen1', width: 30, sortable: true, renderer:changeBackground
        },{
            header: "I1", dataIndex: 'imagen3', width: 30, sortable: true, renderer:changeBackground
        },{
            header: "S2", dataIndex: 'imagen2', width: 30, sortable: true, renderer:changeBackground
        },{
            header: "I2", dataIndex: 'imagen4', width: 30, sortable: true, renderer:changeBackground
        },{
            header: "Ini Deposit", dataIndex: 'initialDeposit', width: 70, sortable: true
        },{
            header: "Add Deposit", dataIndex: 'additionalDeposit', width: 80, sortable: true
        },{
            header: "Freddy", dataIndex: 'professional_esp', width: 50, sortable: true, renderer:changeBackground
        },{
            header: "F. Factor", dataIndex: 'investor_offer_factor', width: 60, sortable: true
        },{
            header: "F. Round", dataIndex: 'investor_offer_round', width: 60, sortable: true
        },{
            header: "F. Type", dataIndex: 'investor_offer_type', width: 60, sortable: true
        }/*,{
            header: "Documents", dataIndex: 'documents', sortable: true, renderer:function(val){
            	 return '<div style="white-space:nowrap !important;">'+ val +'</div>';
            }
        }*/],
		// paging bar on the bottom
        tbar: new Ext.PagingToolbar({
            pageSize: 100,
            store: store,
            displayInfo: true,
            displayMsg: 'Displaying topics {0} - {1} of {2}',
            emptyMsg: "No topics to display",
            items:[
                '-',
			{
				xtype	: 'button',
				iconCls	: 'x-icon-exportExcel',
				tooltip	: 'Export To Excel',
				handler	: function(){
					var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
					myMask.show();	
					Ext.Ajax.request({ 
						url : 'Excel/xlsReportFull.php',  
						success : function(response) {
							var res=Ext.util.JSON.decode(response.responseText);
							myMask.hide();
							downloadURL('http://reifax.com/backoffice/Excel/'+res.file);
						}  
					});  
				}
			},{
                pressed: true,
                enableToggle:true,
                text: 'Show Contracts',
                //cls: 'x-btn-text-icon details',
                toggleHandler: function(btn, pressed){
                    var view = grid.getView();
                    view.showPreview = pressed;
                    view.refresh();
                }
            }]
        })
    });
    // trigger the data store load
    store.load({params:{start:0, limit:100}});

	/*var win = new Ext.Window({
		title: 'Documents with Additionals for '+tempData.name+' '+tempData.surname,
		closable:true,
		width:1024,
		height:500,
		layout: 'fit',
		items: []
	}).show();*/

	var view = new Ext.Viewport({
		layout: 'border',
		renderTo : Ext.getBody(),
		hideBorders: true,
		monitorResize: true,
		items: [{
			region		:	'north',
			height		:	25,
			items		:	Ext.getCmp('menu_page')
		},{
			region			:	'center',
			deferredRender	:	false,
			layout			:	"fit",
			items			:	[grid],
			height			:	Ext.getBody().getViewSize().height-25
		}/*,{
			region		:	'south',
			//items		:	userGroup.footer,
			height		:	25
		}*/]
	}).show();

	var downloadURL = function downloadURL(url) {
	    var hiddenIFrameID = 'hiddenDownloader',
	        iframe = document.getElementById(hiddenIFrameID);
	    if (iframe === null) {
	        iframe = document.createElement('iframe');
	        iframe.id = hiddenIFrameID;
	        iframe.style.display = 'none';
	        document.body.appendChild(iframe);
	    }
	    iframe.src = url;
	};

});
Ext.QuickTips.init()