Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.chart.Chart.CHART_URL = 'includes/ext/resources/charts.swf';
Ext.onReady(function(){

   Ext.QuickTips.init();
   
    
	//*GRID//
	var filter_statistic_select_date="Equal";
	var filter_statistic_select_dateA="";
	var filter_statistic_select_dateB="";
	
	var store= new Ext.data.JsonStore({
		 proxy: new Ext.data.HttpProxy({ url: "php/grid_data.php?tipo=contract-statitic", timeout:"60000" }),
		//url:"",
		root: 'results',					
		totalProperty: 'total',
		fields: ['USERID','NAME','AFFILIATIONDATE','PRODUCT','ENV','TotEnv','countD'],
		remoteSort: true,
		sortInfo: {
			field: 'ENV', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				obj.params.select_date =  filter_statistic_select_date;
				obj.params.select_dateA =  filter_statistic_select_dateA;
				obj.params.select_dateB =  filter_statistic_select_dateB;
				obj.params.sinContract = Ext.getCmp('check_contract').getValue();
			}
		}
	});
	var dataInt=new Ext.data.SimpleStore({
		fields: ['id'],
			data  : [
			  ['Equal'],
			  ['Greater Than'],
			  ['Less Than'],
			  ['Equal or Less'],
			  ['Equal or Greater'],
			  ['Between']
			]
		});
	var pagingBar = new Ext.PagingToolbar({
		pageSize: 50,
		store: store,
		displayInfo: true,
		displayMsg: '<b>Displaying {0} - {1} of {2}</b>',
		emptyMsg: "No topics to display",
		items:[{
				width:150,
				xtype:'label',
				text: 'Range Date:',
				style: 'margin-right:5px',
				align: 'center'
		},{
				width:120,
				id: 'com_date',
				xtype:'combo',
				store: dataInt,
				editable: false,
				valueField: 'id',
				displayField: 'id',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value:filter_statistic_select_date,
				listeners: {
					 'select':function (combo, record, index){
						  filter_statistic_select_date = combo.getValue()
						if(combo.getValue()=='Between'){
							Ext.getCmp("date").setWidth(100);
							Ext.getCmp('date_b').setVisible(true);
						}else{
							Ext.getCmp("date").setWidth(100);
							Ext.getCmp('date_b').setVisible(false);
						}
					 }
				}	
			},{
					width:100,
					xtype:'datefield',
					editable	  : false,
					format		  : 'm/d/y',
					id: 'date',
					value:filter_statistic_select_dateA,
					emptyText		:	'Start Date',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filter_statistic_select_dateA= Ext.util.Format.date(newvalue,'Y-m-d');
						}
						
					}
			},{
					width:100,
					xtype:'datefield',
					editable	  : false,
					format		  : 'm/d/y',
					emptyText		:	'Due Date',
					id: 'date_b',
					value:filter_statistic_select_dateB,
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filter_statistic_select_dateB= Ext.util.Format.date(newvalue,'Y-m-d');
						}
					}	
			},{
				xtype: 'checkbox',
				style: 'margin-left:5px;margin-right:-5px;margin-top:-2px',
				id:'check_contract'
			},{
				width:150,
				xtype:'label',
				text: 'Sin Contratos',
				style: 'margin-right:5px;',
				align: 'center'
			},{
					xtype	: 'button',
					text	: 'Add Filter',
					iconCls	: 'x-icon-filter',
					handler:function(){
						store.load({
							params:{
								select_date :  filter_statistic_select_date,
								select_dateA :  filter_statistic_select_dateA,
								select_dateB :  filter_statistic_select_dateB,
								sinContract:Ext.getCmp('check_contract').getValue()
							}
						});
					}
			},{
					xtype	: 'button',
					text	: 'Remove Filter',
					iconCls	: 'x-icon-filter',
					handler:function(){
						filter_statistic_select_date="Equal";
						filter_statistic_select_dateA="";
						filter_statistic_select_dateB="";
						Ext.getCmp('date').setValue("");
						Ext.getCmp('date_b').setValue("");
						Ext.getCmp('com_date').setValue("Equal");
						Ext.getCmp('date_b').setVisible(false);
						Ext.getCmp('check_contract').setValue(false);
						store.load({
							params:{
								select_date :  filter_statistic_select_date,
								select_dateA :  filter_statistic_select_dateA,
								select_dateB :  filter_statistic_select_dateB,
								sinContract:Ext.getCmp('check_contract').getValue()
							}
						});
					}	
		},{
			width:50,
			xtype:'label',
			text: '',
			align: 'center'
		},{
			width:130,
			xtype:'label',
			id: 'total',
			style: 'font-size:13px;margin-left:20px',
			text: 'Total:0',
			align: 'center'
		},{
			width:20,
			xtype:'label',
			text: '',
			align: 'center'
		},{
			width:150,
			xtype:'label',
			id: 'average',
			style: 'font-size:13px; margin-left:10px',
			text: 'Monthly Average:0',
			align: 'right'
		}], 
		listeners: {
			afterrender : function(){
				Ext.getCmp('date_b').hide(); 
			}
		}
	});
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'x-icon-users',
		icon: 'img/users.png',
		columns: [	
			 new Ext.grid.RowNumberer()
			,{header: 'ID User',		width: 70, sortable: true, align: 'left',dataIndex: 'USERID' }
			,{header: 'Name',	width: 200, sortable: true, align: 'left',dataIndex: 'NAME'  }
			,{header: 'Date of Entry',	width: 120, sortable: true, align: 'left' ,dataIndex: 'AFFILIATIONDATE' }	
			,{header: 'Product',	width: 120, sortable: true, align: 'left' ,dataIndex: 'PRODUCT' }	
			,{header: 'Contracts sent',	width: 100, sortable: true, align: 'center' ,dataIndex: 'ENV' }
		],
		frame:true,
		tbar: pagingBar,
		loadMask:true,
		title:'Contracts Statistic'
	});		
	
	//*FIN DEL GRID*//
	
	//*GRID Graphics*//
	var TypoPrint = 1; 
	var storeG = new Ext.data.JsonStore({
	totalProperty:'total',
	root         :'results',
	url          :'php/grid_data.php?tipo=contract-statitic-graphics',
	fields		:[{name: 'day', type: 'string'}
		,'total'
		,'totalD'
		,{name: 'Avr', type: 'float'}	
	]
	});
	
	
	var fecha=new Date();
	var diahoy=fecha.getDate();
	var mes=fecha.getMonth() +1;
	var anio=fecha.getFullYear();
	var meses=['January','February','March','April','May','June','July','August','September','October','November','December'];
	

	var panel = new Ext.Panel({
		id: 'panel',
		title:'Contracts '+meses[mes-1]+' '+anio,
		iconCls      :'x-icon-settings',
		width        :'99.8%',
		frame: true,
		height: 450,
		layout: 'fit',
		items: {
			xtype: 'columnchart',
			store: storeG,
			xField: 'day',
			yField: 'total',
			yAxis: new Ext.chart.NumericAxis({
				title: 'C o n t r a c t s',
				labelRotation: 90
			}),
			xAxis: new Ext.chart.CategoryAxis({
                title: 'Day'
            }),
			legend: 'Hola',
			extraStyle: {
				yAxis: {
					titleRotation: -90
				}
			}
		}
   });
   var dataMonths =new Ext.data.SimpleStore({
						fields: ['id','name'],
							data  : [
							  ['1','January'],
							  ['2','February'],
							  ['3','March'],
							  ['4','April'],
							  ['5','May'],
							  ['6','June'],
							  ['7','July'],
							  ['8','August'],
							  ['9','September'],
							  ['10','October'],
							  ['11','November'],
							  ['12','December']
							]
						});
    var dataYear =new Ext.data.SimpleStore({
					fields: ['id'],
					data: generateData()
				});

	var dataType=new Ext.data.SimpleStore({
	fields: ['id','name'],
		data  : [
		  ['1','Monthly'],
		  ['2','Yearly']
		]
	});	
   
   var mypanel = new Ext.form.FormPanel({
        frame:true,
		title:'Graphics',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{xtype: 'compositefield',
					labelWidth: 5,
					fieldLabel: 'Select Month',
					items:[{
							width:100,
							id:'month',
							xtype:'combo',
							store: dataMonths,
							mode: 'local',
							valueField: 'id',
							displayField: 'name',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: mes,
							listeners: {
											 'select': function(){
													loadgraphic();
												}	
										}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'Year',
							align: 'center'
						},{
							width:80,
							id:'year',
							xtype:'combo',
							store: dataYear,
							mode: 'local',
							valueField: 'id',
							displayField: 'id',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: anio,
							listeners: {
											 'select': function(){
													loadgraphic();
												}
										}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'Type',
							align: 'center'
						},{
							width:80,
							id:'type',
							xtype:'combo',
							store: dataType,
							mode: 'local',
							valueField: 'id',
							displayField: 'name',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: '1',
							listeners: {
								 'select': function(field,newvalue,oldvalue){
										loadgraphic();
										if(field.getValue()==2){
											Ext.getCmp('radio-type').getEl().show(); 
										}else{
											Ext.getCmp('radio-type').getEl().hide(); 
										}
								 }
							}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							xtype:'button',
							text:'Refresh',
							iconCls: 'x-tbar-loading',
							handler:function(){ 
								loadgraphic();
							}
						},{
							layout: 'table',
							layoutConfig:	{
								columns	:	3
							},
							items	:	[{
								width:130,
								xtype:'label',
								id: 'totalG',
								style: 'font-size:15px;',
								text: 'Total:',
								align: 'center'
							},{
								width:150,
								xtype:'label',
								id: 'averageG',
								style: 'font-size:15px; margin-left:10px',
								text: 'Average:',
								align: 'right'
							},{
								width:20,
								xtype:'label',
								text: '',
								align: 'center'
							},{
								layout: 'form',
								labelWidth: 10,
								id:'radio-type',
								colspan: 2,
								bodyStyle: 'margin-top:-4px; margin-left:-12px',
								items:[{   
									xtype	: 'radiogroup',
									width	: 200,
									id:'radio-value',
									 items: [{
												  boxLabel: 'Quantity',
												  width: 50, 
												  name: 'tipo', 
												  inputValue: '1', 
												  checked: true,
												  onClick: function(e){
													  TypoPrint=1;
													  calculateAVe()
												  }
											  },{
												  boxLabel: 'Average', 
												  name: 'tipo', 
												  inputValue: '2',
												  onClick: function(e){
													   TypoPrint=2;
													  calculateAVe()
												  }
												  
											  }  
										 ]
									}]
							},{
								width:20,
								xtype:'label',
								text: ' ',
								align: 'center'
							}]
						}
					],listeners: {
						 'afterrender': function(){
							Ext.getCmp('radio-type').getEl().hide(); 
						 }
					}},panel	
				]
	})
   
	//***************//	
	var tabPanel = new Ext.TabPanel({  
		border: false,  
		activeTab: 0,  
		enableTabScroll:true,  
		height: 570,	
		items:[mypanel,Ext.searchUsers.form]  
	});
	
	Ext.searchUsers.search=function(){
		tabPanel.setActiveTab(tabPanel.add(grid));
		store.load();
		pagingBar.bind(store);
	}
	 
	var viewport = new Ext.Viewport({
	layout       :'border',
	hideBorders  :true,
	monitorResize:true,
	items        :[{
			region  :'north',
			height  :25,
			items   :Ext.getCmp('menu_page')
		},{
			region  :'center',
			autoHeight:true,
			items:tabPanel
		}]
	}); 
	store.addListener('load',function(){
   		obtenerTotal();
   });
   
   storeG.addListener('load',function(){
   		obtenerTotalG();
		if(Ext.getCmp("type").getValue()==2)
			calculateAVe();
   });
   
   storeG.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), userid:'', year: Ext.getCmp("year").getValue(), type: Ext.getCmp("type").getValue()}});

   
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		var countDay=0;
		if(store.getCount()>0){
			acum = totales[0].data.TotEnv;
			countDay = totales[0].data.countD;
		}
		
		var mayor=0;
		for(i=0;i<store.getCount();i++){
			if(Math.abs(totales[i].data.countD)>mayor){
				mayor=Math.abs(totales[i].data.countD)
			}
		}
		

		Ext.getCmp('total').setText('Total: '+acum);	
		if(store.getCount()>0){
			if(filter_statistic_select_date!='Less Than' && filter_statistic_select_date!='Equal or Less'){
				Ext.getCmp('average').setText('Average: '+Math.abs((acum/countDay).toFixed(2)));
			}else{
				Ext.getCmp('average').setText('Average: '+Math.abs((acum/mayor).toFixed(2)));
			}
			
		}else{
			Ext.getCmp('average').setText('Average: 0');
		}
	}
	function obtenerTotalG(){
		var totales = storeG.getRange(0,storeG.getCount());
		var i;
		var acum=0;
		for(i=0;i<storeG.getCount();i++){
			acum = acum + parseInt(totales[i].data.total);
		}
		Ext.getCmp('totalG').setText('Total: '+acum);	
		if(storeG.getCount()>0){
			Ext.getCmp('averageG').setText('Monthly Average: '+(acum/storeG.getCount()).toFixed(2));
		}	
	}

	function loadgraphic(){
		storeG.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), userid:'', year: Ext.getCmp("year").getValue(), type: Ext.getCmp("type").getValue()}});
		if(Ext.getCmp("type").getValue()==1){
			panel.setTitle('Contracts '+meses[Ext.getCmp("month").getValue()-1]+' '+Ext.getCmp("year").getValue());
		}else{
			panel.setTitle('Contracts '+Ext.getCmp("year").getValue());
		}
	}
	
	function calculateAVe(){
		if(TypoPrint==1){
			var campo="totalD";
		}else{
			var campo="Avr";
		}
		storeG.each(function(record){
			record.set("total",record.get(campo))
		});
	}
	function generateData(){
		var data = [];
		data.push([anio-2]);
		data.push([anio-1]);
		data.push([anio]);
		return data;
	}
	   
});

