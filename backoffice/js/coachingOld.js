// JavaScript Document
Ext.ns('coaching');

coaching=
{
	init: function ()
	{
		coaching.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total',
			id:'readerCoaching'
			},
			coaching.record 
			);
			
		coaching.store = new Ext.data.Store({  
			id: 'coaching',  
			proxy: coaching.dataProxy,  
			baseParams: {
				accion	:'consultar'
			},  
			reader: coaching.gridReader  
		});
		
		coaching.pager = new Ext.PagingToolbar({  
			store: coaching.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 30,
			items:[ 
				'|'
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-save',
					handler	: coaching.save,
					text	: 'Save Changes'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: coaching.cancel,
					text	: 'Cancel Change'
				}
			]  
		});
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {format : 'H:i',
				minValue: '9:00 AM',
				maxValue: '6:00 PM',
				increment: 30});
				
		var opcionesCombo= [
        ['1', 'Assigned undated'],
        ['2', 'Assigned date'],
        ['3', 'completed'],
        ['4', 'canceled'] ];
		
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		coaching.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
			{  
				header: 'Id Coaching',  
				dataIndex: 'id', 
				sortable: true,  
				width: 30  
			},{  
				header: 'Userid',  
				dataIndex: 'userid', 
				sortable: true
			},{  
				header: 'User',  
				sortable: true, 
				dataIndex: 'usr_name'
			},{  
				header: 'Email',
				sortable: true, 
				dataIndex: 'email'
			},{  
				header: 'Product',
				sortable: true, 
				dataIndex: 'producto'
			},{  
				header: 'Time',
				sortable: true, 
				dataIndex: 'periodo'
			},{  
				header: 'Status',
				sortable: true, 
				dataIndex: 'status',
				editor	: combo
			},{  
				header: 'Registration Date',  
				dataIndex: 'date_register', 
				sortable: true,
				renderer: Ext.util.Format.dateRenderer('Y/m/d')  
			},{   
				header: 'Coaching Date',
				sortable: true, 
				dataIndex: 'date_coaching',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField
			},{   
				header: 'Coaching Hour',
				sortable: true, 
				dataIndex: 'hourCoaching',
				editor	:timeField
			}
			]  
		);

		
		coaching.store.load();		
		coaching.grid = new Ext.grid.EditorGridPanel({  
            store: coaching.store,
			tbar: coaching.pager,   
			cm: coaching.columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true ,
			loadMask: true,
			border :false
        });  
		
		coaching.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: coaching.grid,
					autoHeight: true
			}]
		});
		
	},
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'userid', type: 'int'},    
			{name: 'status', type: 'string'},
			{name: 'date_register', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_coaching', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'hourCoaching', type: 'string'},
			{name: 'usr_name', type: 'string'},
			{name: 'email' , type: 'string'},
			{name: 'producto' , type: 'string'},
			{name: 'periodo' , type: 'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesCoaching.php',   // Servicio web  
			method: 'POST'                          // Método de envío  
	}),
	save	:function ()
	{
		var grid=coaching.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcionesCoaching.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
				}  
			});  
		}  
			
	},
	cancel: function ()
	{
		coaching.grid.getStore().rejectChanges();
	}
}

Ext.onReady(coaching.init);