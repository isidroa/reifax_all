Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=foreclosurereleased',
		fields: [
			{name: 'idpen', type: 'int'},
			'parcelid',
			'attorney',
			'judgedate',
			'file_date',
			'judgeamt',
			'checkdate',
			'compare',
			'reinsertdate'
		],
		listeners: {
			beforeload: {
				fn: function(store,opt){
					opt.params.filter=select_st;
				}
			}
        }
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[{
			tooltip: 'Click to Delete selected Rows',
			text:'Delete',
			iconCls:'icon',
			icon: 'images/delete.gif',
            handler: doDel 
		},{
			text:'Add',
			id: 'add_butt',
            tooltip: 'Click to Add Row',
			iconCls:'icon',
			icon: 'images/add.gif',
            handler: doAdd 
		},{
			text:'Add Days',
			id: 'adddays_butt',
            tooltip: 'Click to Add Days to field Checkdate',
			iconCls:'icon',
			icon: 'images/editar.png',
            handler: doAddDays 
		}
		]});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idpen;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}

	function doDel(){
		Ext.MessageBox.confirm('Delete Row','Are you sure delete row?.',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					var obtenidos=obtenerSeleccionados();
					if(!obtenidos){
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'You must select a row, by clicking on it, for the delete to work.',
							buttons: Ext.MessageBox.OK,
							icon:Ext.MessageBox.ERROR
						});						
						obtenerTotal();
						return;
					}
					//alert(obtenidos);//return;		
					obtenidos=obtenidos.replace('(','');
					obtenidos=obtenidos.replace(')','');

					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo : 'foreclosurereleased',
							ID: obtenidos,
							bd: Ext.getCmp('county2').getValue() 
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
							store.reload();
						},
						
						success:function(response,options){
						
							store.reload();
						}
					}); 
				}
			}
		)
	}

	function doAdd(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.form.FormPanel({
			url:'php/grid_add.php',
			frame:true,
			
			waitMsgTarget : 'Adding Row...',
			items: [{
					xtype: 'combo',
					//tpl: '<tpl for="."><div ext:qtip="{text}" class="x-combo-list-item" style="width=250px">{text}</div></tpl>',					 
					store: new Ext.data.SimpleStore({
						fields: ['id', 'text'],
						data : Ext.combos_selec.counties
					}),
					fieldLabel: 'County',
					editable: false,
					displayField:'text',
					valueField: 'id',
					id: 'county1',
					name: 'county1',
					hiddenName: 'county1', 
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select a county...',
					selectOnFocus:true,
					allowBlank:false
                },{
					xtype: 'compositefield',
					fieldLabel:'Parcelid',
					width: 280,
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Parcelid',
						name: 'parcelid',
						id: 'parcelid',
						allowBlank:false,
						width: 200
					},{
						xtype: 'displayfield', 
						value:' ',
						width: 4
					},{
						xtype: 'button', 
						icon:'images/setsearch.gif',
						text:'Search',
						tooltip : 'Click to search parcelid',				
						listeners : {
							click : function()
							{
								if(Ext.getCmp('county1').getValue()=='' || Ext.getCmp('parcelid').getValue()=='')
								{
									Ext.MessageBox.alert('Warning','Please select county and insert parcelid');
									return;
								}
											
								Ext.getCmp('attorney').setValue('Loading..');
								Ext.getCmp('judgeamt').setValue('Loading..');
								Ext.getCmp('judgedate').setValue('Loading..');
								Ext.getCmp('filedate').setValue('Loading..');
								//submit to server
								Ext.Ajax.request( 
									{   
										waitMsg: 'Saving changes...',
										url: 'php/grid_data.php?tipo=getpendes', 
										method: 'POST',
										params: {
											bd: Ext.getCmp('county1').getValue(),
											parcelid: Ext.getCmp('parcelid').getValue()
										},
										
										failure:function(response,options){
											Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
											store.rejectChanges();
										},
										
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											
											if(rest.succes==false)
											{
												Ext.MessageBox.alert('Warning',rest.msg);
											}
											else											
											{
												Ext.getCmp('attorney').setValue(rest.attorney);
												Ext.getCmp('judgeamt').setValue(rest.judgeamt);
												Ext.getCmp('judgedate').setValue(rest.judgedate);
												Ext.getCmp('filedate').setValue(rest.filedate);
												Ext.getCmp('checkdate').setValue(rest.checkdate);
											}
										}                                     
									}
								);						
							}						
						}
					}]
				},{
					xtype: 'textfield',
					fieldLabel: 'Attorney',
					name: 'attorney',
					id: 'attorney',
					readOnly: true,
					allowBlank:false,
					width: 250
				},{
					xtype: 'textfield',
					fieldLabel: 'Judgeamt',
					name: 'judgeamt',
					id: 'judgeamt',
					readOnly: true,
					allowBlank:false,
					width: 150
				},{
					xtype: 'textfield',
					fieldLabel: 'Judgedate',
					name: 'judgedate',
					id: 'judgedate',
					readOnly: true,
					allowBlank:false,
					width: 150
				},{
					xtype: 'textfield',
					fieldLabel: 'Filedate',
					name: 'filedate',
					id: 'filedate',
					readOnly: true,
					allowBlank:false,
					width: 150
				},{
					xtype: 'datefield',
					fieldLabel: 'Check Date',
					allowBlank:false,
					format: 'Y-m-d',
					name : 'checkdate',
					id : 'checkdate',
					width: 150
				},{
					xtype: 'hidden',
					name : 'tipo',
					id : 'tipo',
					value : 'foreclosurereleased'
				}
			],
			buttons: [{
				text: 'Save',
				handler  : function(){
				    if (simple.getForm().isValid()) {
                        simple.getForm().submit({
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
				}	
			},{
				text: 'Cancel',
				handler  : function(){
					win.close();
                    }
			}]
		});
		win = new Ext.Window({
			title: 'New Foreclosure Released',
			layout      : 'fit',
			width       : 500,
			height      : 300,
			modal	 	: true,
			plain       : true,
			items		: simple 
		});
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doAddDays(){
	    var button = Ext.get('add_butt');
		var simple = new Ext.form.FormPanel({
			url:'php/grid_add.php',
			frame:true,			
			waitMsgTarget : 'Adding Row...',
			items: [{
				xtype: 'numberfield',
				fieldLabel: 'Day to add',
				name: 'daystoadd',
				id: 'daystoadd',
				allowBlank:false,
				allowDecimals : false,
				allowNegative : true,
				width: 250
			},{
				xtype: 'hidden',
				name : 'tipo',
				id : 'tipo',
				value : 'foreclosurereleasedadddays'
			}],
			buttons: [{
				text: 'Save',
				handler  : function(){
				    if (simple.getForm().isValid()) {
                        simple.getForm().submit({
							waitTitle   :'Please wait!',
							//waitMsg     :'Saving changes...',
							success: function(form, action) {
								win.close();
							    store.reload();
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.msg);
							}
						});
                    }
				}	
			},{
				text: 'Cancel',
				handler  : function(){
					win.close();
                    }
			}]
		});
		win = new Ext.Window({
			title: 'Add Days to Checkdate',
			layout      : 'fit',
			width       : 420,
			height      : 130,
			modal	 	: true,
			plain       : true,
			items		: simple 
		});
        win.show(button);
		win.addListener("beforeshow",function(win){
			simple.getForm().reset();
		});
	}

	function doEdit(oGrid_Event) {

		if (oGrid_Event.value instanceof Date)
		{   
			var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
		} else
		{
			var fieldValue = oGrid_Event.value;
		}    
				
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "condados", 
					key: 'idCounty',
					keyID: oGrid_Event.record.data.idCounty,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
				}                                     
			 }
		);  
	}; 
	
	function doFilter(combo,record,index){
		select_st = combo.getValue();
		store.load({params:{start:0, limit:200, bd:select_st}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
	function is_showed(val){
		if(val==1)
			return 'Yes';
		else
			return 'No';
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
			
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon',
		icon: 'includes/ext/resources/images/default/grid/columns.gif',
		columns: [	  
			new Ext.grid.RowNumberer()
			,mySelectionModel
			,{id:'idpen',header: "ID", width: 50, align: 'center', sortable: true, dataIndex: 'idpen'}
			,{header: 'Parcelid', width: 150, sortable: true, align: 'left', dataIndex: 'parcelid'}
			,{header: 'File Date', width: 120, sortable: true, align: 'left', dataIndex: 'file_date'}
			,{header: 'Judge Date', width: 120, sortable: true, align: 'left', dataIndex: 'judgedate'}
			,{header: 'Judge Amount', width: 120, sortable: true, align: 'left', dataIndex: 'judgeamt'}
			,{header: 'Check Date', width: 120, sortable: true, align: 'left', dataIndex: 'checkdate'}
			,{header: 'Compared', width: 70, sortable: true, align: 'center', dataIndex: 'compare'}
			,{header: 'Re Insert Date', width: 120, sortable: true, align: 'left', dataIndex: 'reinsertdate'}
		],
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width,
		frame:true,
		title:'Foreclosure Released',
		loadMask:true,
		tbar: pagingBar 
	});
	
	grid.on('render',function(){
		var secondTbar = new Ext.Toolbar({
			renderTo: this.tbar,
			items:[
				'Filter: ',{
                 xtype: 'combo',
				 store: new Ext.data.SimpleStore({
			 		fields: ['id', 'text'],
		 			data : Ext.combos_selec.counties
		   		 }),
				 editable: false,
				 displayField:'text',
				 valueField: 'id',
				 name: 'county2',
				 id: 'county2',
				 hiddenName: 'idcounty',
				 mode: 'local',
				 triggerAction: 'all',
				 emptyText:'Select a county...',
				 selectOnFocus:true,
				 allowBlank:true,
				 value:'flbroward',
				 listeners: {
					 'select': doFilter
					 }
                }
		] 
		});    
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});


