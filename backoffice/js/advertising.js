Ext.ns("advertising");
advertising = {
	globalVar:{},
	stores:{},
	combos:{},
	init : function() {
		advertising.store = new Ext.data.JsonStore({
			id	: 'id',
			autoLoad: true,
			remoteSort :true,
			url: 'php/funcionesAdvertising.php', //donde busca la informacion
			baseParams:{
					accion: 'consultar'
			},
			root: 'data',                       //el objeto donde esta la informacion 
			fields: [							//los campos que contendran la data
					{name: 'userid'},
					{name: 'county'},
					{name: 'position'},
					{name: 'nombre'},
					{name: 'purchaseDate'},
					{name: 'postingDate'},
					{name: 'expirationDate'},
					{name: 'img'},
					{name: 'url'},
					{name: 'Diference'},
					{name: 'name'},
					{name: 'price'},
					{name: 'urlimg'},
					{name: 'urladvertise'}
			]
		});	
		advertising.globalVar.opcionessta=[
		['1','1'],
		['2','2'],
		['3','3'],
		['4','4'],
		['5','5'],
		['6','6'],
		['7','7'],
		['8','8']
		];
		
		advertising.stores.storesta= new Ext.data.SimpleStore({
			fields: ['idsta','position'],
			data: advertising.globalVar.opcionessta
		});
		advertising.globalVar.opcionesstatus=[
		['1','Disabled'],
		['2','Busy'],
		['3','Awaiting Authorization'],
		['4','Active'],
		['5','Rejected IMG'],
		['6','Rejected URL']
		];
		
		advertising.stores.storestatus= new Ext.data.SimpleStore({
			fields: ['idstatus','status'],
			data: advertising.globalVar.opcionesstatus
		});
		advertising.globalVar.opcionesfrecuency=[
		['1','Monthly'],
		['2','Annually']
		];
	    advertising.stores.storefre= new Ext.data.SimpleStore({
			fields: ['idfrecuency','name'],
			data: advertising.globalVar.opcionesfrecuency
		});
	    advertising.storeCounty= new Ext.data.JsonStore({
		url: 'php/combosCountyAdvertising.php',
		root : 'dataCounty',
		autoLoad: true,
		fields: [
					{name: 'idcounty'},
					{name: 'county'}
				]				
		});
		advertising.storeUser= new Ext.data.JsonStore({
		url: 'php/combosAdvertising.php',
		root : 'dataUsers',
		autoLoad: true,
		fields: [
					{name: 'userid'},
					{name: 'name'}
				]				
		});
		var panelpag = new Ext.form.FormPanel({
			width: 550,
			layout: 'table',
			id:'advertisingFilter',
			name: 'advertisingFilter',
			padding: 3,
			defaults: {
				bodyStyle:'margin-left:5px'
			},
			layoutConfig:{
				columns	:	2
			},
			style	:{
				background:'none'
			},
			border: false,
			items: [{
				layout	: 'table',
				border: false,
				items	: [{
						xtype:'label',
						name: 'lblcontact',
						text: 'Contact'
					},{
						xtype:'combo',
						name:'contact',
						id:'contact',
						listWidth:250,
						store: advertising.storeUser,
						valueField: 'userid',
						displayField:'name',
						allowBlank:false,
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true
					},{
						xtype:'label',
						name: 'lblcounty',
						text: 'County'
					},{
						xtype	: 'combo',
						store: advertising.storeCounty,
						displayField:'county',
						name:'county',
						id		: 'county',
						valueField:'idcounty',
						width: 80,
						triggerAction:'all',
						mode:'local'
					},{
						xtype:'label',
						name: 'lblposition',
						text: 'Position'
					},{
						xtype	: 'combo',
						fieldLabel:'Position',
						store: advertising.stores.storesta,
						displayField:'position',
						name:'position',
						valueField:'idsta',
						hiddenvalue:'position',
						width: 40,
						triggerAction:'all',
						mode:'local'
					},{
						xtype:'label',
						name: 'lblstatus',
						text: 'Status'
					},{
						xtype	: 'combo',
						fieldLabel:'Status',
						store: advertising.stores.storestatus,
						displayField:'status',
						name:'status',
						valueField:'idstatus',
						hiddenvalue:'status',
						width: 90,
						triggerAction:'all',
						mode:'local'
					}
				]
			}]
		});
	    
		advertising.store.load();                   //cargando el store
		advertising.tb = new Ext.PagingToolbar({   
			store		: advertising.store, 
			displayInfo	: true,  
			displayMsg	: '{0} - {1} of {2} Registros',  
			emptyMsg	: 'No hay Registros Disponibles',  
			pageSize	: 30,
			items		:[ 
				{
					xtype	:'button',
					id		: 'exc_a',
					text	: ' Accept',
					tooltip	: ' Accept',
					iconCls : 'icon',
					icon    : 'images/adagree-20.png',
					xtype	: 'button',
					handler : function(){
								data	= advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids+=(ids=='')?data[i].data.userid:','+data[i].data.userid;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									success: function (response){
										var text = Ext.decode(response.responseText); 
										if(text.success === false){
											if(text.msg == 'R')
												Ext.MessageBox.alert('Warning', 'Error: Without IMG or URL');
										}else if(text.msg == 'A') 
													Ext.MessageBox.alert('Warning', 'Error: Is Active');
												else Ext.MessageBox.alert('Warning', 'Ok');
										advertising.grid.getStore().reload();
									},
									failure : function() {
										Ext.MessageBox.alert('Warning', 'Error Accept');
									},
									params: { 
										accion: 'accept',
										ids	: ids
									}
								});
							}
					},'|',{
					//xtype	: 'button',
					id		: 'exc_ri',
					text	: ' Reject IMG',
					tooltip	: ' Reject IMG',
					iconCls : 'icon',
					icon    : 'images/adno-20.png',
					xtype	: 'button',
					handler	: function (){
								data	=	advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids += (ids == '')?data[i].data.userid:','+data[i].data.userid;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									params: { 
										accion: 'rejecti',
										ids	: ids
									},
									success: function (response){
										var text = Ext.decode(response.responseText); 
										if(text.success === false)
											Ext.MessageBox.alert('Warning', 'Error: Without image');
										else Ext.MessageBox.alert('Warning', 'Ok');
										advertising.grid.getStore().reload();
									},
									failure : function() {
										Ext.MessageBox.alert('Warning', 'Error Reject IMG');
									}
								});
							}
					},'|',{
					//xtype	: 'button',
					id		: 'exc_ru',
					text	: ' Reject URL',
					tooltip	: ' Reject URL',
					iconCls : 'icon',
					icon    : 'images/adWno-20.png',
					xtype	: 'button',
					handler	: function (){
								data	=	advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids += (ids == '')?data[i].data.userid:','+data[i].data.userid;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									params: { 
										accion: 'rejectu',
										ids	: ids
									},
									success: function (response){
										var text = Ext.decode(response.responseText); 
										if(text.success === false)
											Ext.MessageBox.alert('Warning', 'Error: Without url');
										else Ext.MessageBox.alert('Warning', 'Ok');
										advertising.grid.getStore().reload();
									},
									failure : function() {
										Ext.MessageBox.alert('Warning', 'Error Reject URL');
									}
								});
					}
				},'|',{
					//xtype	: 'button',
					id		: 'exc_d',
					text	: ' Disabled',
					tooltip	: ' Disabled',
					iconCls : 'icon',
					icon    : 'images/ad_inactive-20.png',
					xtype	: 'button',
					handler	: function (){
								data	=	advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids += (ids == '')?data[i].data.userid:','+data[i].data.userid;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									params: { 
										accion: 'disabled',
										ids	: ids
									},
									success: function (response){
										Ext.MessageBox.alert('Warning', 'Disabled');
										advertising.grid.getStore().reload();
									},
									failure : function() {
										Ext.MessageBox.alert('Warning', 'Error Disabled');
									}
								});
					}
				},
				panelpag,
				{
					xtype	: 'button',
					text	: 'Filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						advertising.store.load({
							params:{
								status:Ext.getCmp('advertisingFilter').getForm().findField("status").getValue(),
								contact:Ext.getCmp('advertisingFilter').getForm().findField("contact").getValue(),
								position:Ext.getCmp('advertisingFilter').getForm().findField("position").getValue(),
							}
						})
					}
				},
				{
					xtype	: 'button',
					text	: 'Reset',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						advertising.store.load();
						Ext.getCmp('advertisingFilter').getForm().reset();
					}
				}
			]
		});
		
				advertising.tb.on('beforechange',function(bar,params){
					params.status	= Ext.getCmp('advertisingFilter').getForm().findField("status").getValue();
					params.contact	= Ext.getCmp('advertisingFilter').getForm().findField("contact").getValue();
					params.position	= Ext.getCmp('advertisingFilter').getForm().findField("position").getValue();
				});
			
		
		//crearemos el grid donde se mostrara la informacion cargada en el store
		sm 	= new Ext.grid.CheckboxSelectionModel();
		
		/**********
			definicion de editores
		**********************/
		var combouser		= new Ext.form.ComboBox(advertising.combos.comboUser);
		var comboGridStatus	= new Ext.form.ComboBox({
					xtype	: 'combo',
					fieldLabel:'Status',
					store: advertising.stores.storestatus,
					displayField:'status',
					name:'status',
					valueField:'idstatus',
					hiddenvalue:'status',
					width: 60,
					triggerAction:'all',
					mode:'local'
				});
		var comboGridPos=new Ext.form.ComboBox({
					xtype	: 'combo',
					fieldLabel:'Position',
					store: advertising.stores.storesta,
					displayField:'position',
					name:'position',
					valueField:'idsta',
					hiddenvalue:'position',
					width: 60,
					triggerAction:'all',
					mode:'local'
				});
		var comboGridFre=new Ext.form.ComboBox({
					xtype	: 'combo',
					fieldLabel:'Frecuency',
					store: advertising.stores.storefre,
					displayField:'name',
					name:'name',
					valueField:'name',
					hiddenvalue:'name',
					width: 60,
					triggerAction:'all',
					mode:'local'
				});
		var textEdit=new Ext.form.TextField();
		//alert('e:'+enabledate);
			
		    advertising.grid= new Ext.grid.EditorGridPanel({
			store: advertising.store,                             //asignamos el store
			tbar : advertising.tb,                                // le incluimos el toolbar
			columns: [
				sm,{	header:'Userid', 
					dataIndex:'userid',
					editor	:textEdit,
					width:20,
					sortable: true,
					renderer :function (value, p, record){
						return String.format(
						'<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
					}
				},{	header:'Purchase',
					width:30,
					dataIndex:'purchaseDate',                    // le asignamos a cada columna una propiedad del record del store
					sortable: true
				},{	header:'Posting',
					width:30,
					dataIndex:'postingDate',
					sortable: true
				},{	header:'Expiration', 
					width:30,
					dataIndex:'expirationDate',
					sortable: true
				},{	header:'Remaining days', 
					width:40,
					dataIndex:'Diference',
					sortable: true
				},{	header:'IMG', 
					xtype: 'actioncolumn',
					dataIndex:'urlimg',
					width: 20,
					renderer: function(urlimg) {
						//alert(urlimg);
						if(urlimg != null)
							if (urlimg.substr(0,7) != "http://")
								return String.format('<a href="http://'+urlimg+'" title="IMG" target="_blank"><img src="images/view-20.png"/></a>');
							else	return String.format('<a href="'+urlimg+'" title="IMG" target="_blank"><img src="images/view-20.png"/></a>');
						else return String.format('<a title="IMG" target="_blank"></a>');
					}
				},{	header:'URL', 
					xtype: 'actioncolumn',
					dataIndex:'urladvertise',
					width: 20,
					renderer: function(urladvertise) {
						if(urladvertise != null)
	      					if (urladvertise.substr(0,7) != "http://")
								return String.format('<a href="http://'+urladvertise+'" title="URL" target="_blank"><img src="images/ad_W-20.png"/></a>');
							else return String.format('<a href="'+urladvertise+'" title="URL" target="_blank"><img src="images/ad_W-20.png"/></a>');
						else return String.format('<a title="URL" target="_blank"></a>');
					}
				},{	header:'County', 
					width:40,
					dataIndex:'county',
					//editor:comboGridPos,
					sortable: true
					/*renderer: function(val){
							var record = comboGridFre.findRecord(comboGridFre.valueField, val);
							return record ? record.get(comboGridFre.displayField) : val;
					}*/
				},{	header:'Position', 
					width:20,
					dataIndex:'position',
					//editor:comboGridPos,
					sortable: true,
					renderer: function(val){
							var record = comboGridFre.findRecord(comboGridFre.valueField, val);
							return record ? record.get(comboGridFre.displayField) : val;
					}
				},{	header:'Frecuency', 
					dataIndex:'name',
					width:30,
					editor:comboGridFre,
					sortable: true,
					renderer: function(val){
							var record = comboGridFre.findRecord(comboGridFre.valueField, val);
							return record ? record.get(comboGridFre.displayField) : val;
					}
				},{	header:'Price', 
					dataIndex:'price',
					width:20,
					sortable: true,
					renderer : function(price){return Ext.util.Format.usMoney(price)}
				},{	header:'Status',
					dataIndex:'nombre',
					editor:comboGridStatus,
					width:140,
					sortable: true,
					renderer: function(val){
							var record = comboGridStatus.findRecord(comboGridStatus.valueField, val);
							return record ? record.get(comboGridStatus.displayField) : val;
					}
				}/*,{
					xtype: 'actioncolumn',
					width: 15,
					items: [{
							icon   : 'images/ad no-20.png',
							tooltip: 'Reject IMG',
							handler: function(){
								data	=	advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids += (ids == '')?data[i].data.id:','+data[i].data.id;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									success: function (){
									   advertising.grid.getStore().reload();
									},
									failure:  function (){
								
									},
									params: { 
										accion: 'rejecti',
										ids	: ids
									}
								});
							}
						}
					]
				},{
					xtype: 'actioncolumn',
					width: 15,
					items: [
						{
							icon   : 'images/ad W no-20.png',
							tooltip: 'Reject URL',
							handler: function(){
								data	=	advertising.grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids += (ids == '')?data[i].data.id:','+data[i].data.id;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesAdvertising.php',
									success: function (){
									   advertising.grid.getStore().reload();
									},
									failure:  function (){
								
									},
									params: { 
										accion: 'rejectu',
										ids	: ids
									}
								});
							}
						}
					]
				}*/
				],
				sm: sm,
				border: false,
					viewConfig: {
						forceFit:true
					},
				autoScroll: true,
				border: false,
				stripeRows: true,
				height:Ext.getBody().getViewSize().height-55,			//debe tener un ancho para que muestre todas las filas
				listeners:{
					afteredit: function (model){
						var grid		= model.grid;
						var modified 	= grid.getStore().getModifiedRecords();
						
						if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
							var recordsToSend = [];  
							Ext.each(modified, function(record) {
								recordsToSend.push(Ext.apply({id:record.id},record.data));  
							});  
						  
							grid.el.mask('Saving...', 'x-mask-loading');
							grid.stopEditing();  
						  
							recordsToSend = Ext.encode(recordsToSend);
						  
							Ext.Ajax.request({ 
								url : 'php/funcionesTaskManager.php',  
								params :
									{
										records : recordsToSend,
										accion	:'update'
									},  
								scope:this,  
								success : function(response) {
									grid.el.unmask();  
									grid.getStore().commitChanges();  
								}  
							});  
						}  
					}
				}
			});
	                                    
		advertising.marco= new Ext.Viewport({         // ahora mostramos el grid en un viewport
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: advertising.grid,
					autoHeight: true
			}]
		});
	     // creamos el listener del dobleclick de la fila del grid
			/*advertising.grid.on('rowdblclick',function(grid,index,event){
				 var record= grid.getStore().getAt(index);                   //obtengo el record y el indice de la fila clikeada	
				 //console.log(record);
					//Ext.
				});*/
	}
}
Ext.onReady(advertising.init);
