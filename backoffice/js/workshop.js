
	function itemGridSelected()
	{
		var selec = Grid.selModel.getSelections();
		var i=0;
		var selected='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.idwor;
		}
		selected+=')';
		if(i==0)selected=false;		
		return selected;
	}	
		
	function closeTabs(){
		var tab;
		window.scrollTo(0,0);
		if(document.getElementById('secondTab')){
			tab = tabPanel.getItem('secondTab');
			tabPanel.remove(tab);
		}
	}
	
	function AddVolumen(idwor){
		Ext.Ajax.request({  
			waitTitle: 'Please wait..',
			waitMsg: 'Sending data...',
			url: 'php/response_add.php', 
			method: 'POST', 
			params: { 
				oper: "addvolumen", 
				idvalue: idwor
			},
			success:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.MessageBox.alert('Success',d.errors.reason);
				store.reload();
				closeTabs();
				tabPanel.add({
					title   :'Workshop Overview',
					id      :'secondTab',
					height  :300,
					closable:true,
					autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
				}).show();	
			},
			failure:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Failure', d.errors.reason);
			}
		});		
	}
	
	function AddSubject(idwor){
		Ext.Ajax.request({  
			waitTitle: 'Please wait..',
			waitMsg: 'Sending data...',
			url: 'php/response_add.php', 
			method: 'POST', 
			params: { 
				oper: "addsubject", 
				idvalue: idwor
			},
			success:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.MessageBox.alert('Success',d.errors.reason);
				store.reload();
				closeTabs();
				tabPanel.add({
					title   :'Workshop Overview',
					id      :'secondTab',
					height  :300,
					closable:true,
					autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
				}).show();	
			},
			failure:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Failure', d.errors.reason);
			}
		});		
	}
	
	function editTitleSubject(idwor,idsub,name,descrip){
		var formul = new Ext.FormPanel({
			url:'php/response_edit.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'namesubject',
				name: 'namesubject',
				fieldLabel: '<span style="color:#F00">*</span> Name',
				allowBlank:false,
				allowDecimals : true,
				width: 250,
				value: name
			},{
				xtype:'textarea',
				id: 'descriptionsubject',
				name: 'descriptionsubject',
				fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> Description',
				allowBlank:true,
				width: 250,
				height: 80,
				value: descrip						
			}],
			buttons: [{
					text: 'Save',
					cls: 'x-btn-text-icon',			
					icon: 'img/save.gif',
	                formBind: true,
					handler  : function(){
						if(formul.getForm().isValid())
						{
							formul.getForm().submit({
								method: 'POST',
								params: {
									oper : 'subject',
									idsub : idsub 
								},								
								waitTitle: 'Please wait..',
		                        waitMsg: 'Sending data...',
								success: function(form, action) {
									wind.close();
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Success",obj.errors.reason);
									closeTabs();
									tabPanel.add({
										title   :'Workshop Overview',
										id      :'secondTab',
										height  :300,
										closable:true,
										autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
									}).show();	
								},
								failure: function(form, action) {
	                                obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.errors.reason);
								}
							});
						}
					}
				},{
					text: 'Cancel',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind.close();
				   }
			}]				
		});
		var wind = new Ext.Window({
				title: 'Edit Subject',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 200,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});		
	}
	
	function editTitleVolumen(idwor,idvol,name,descrip){
		var formul = new Ext.FormPanel({
			url:'php/response_edit.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'namevolumen',
				name: 'namevolumen',
				fieldLabel: '<span style="color:#F00">*</span> Name',
				allowBlank:false,
				allowDecimals : true,
				width: 250,
				value: name
			},{
				xtype:'textarea',
				id: 'descriptionvolumen',
				name: 'descriptionvolumen',
				fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> Description',
				allowBlank:true,
				width: 250,
				height: 80,
				value: descrip						
			}],
			buttons: [{
					text: 'Save',
					cls: 'x-btn-text-icon',			
					icon: 'img/save.gif',
	                formBind: true,
					handler  : function(){
						if(formul.getForm().isValid())
						{
							formul.getForm().submit({
								method: 'POST',
								params: {
									oper : 'volumen',
									idvol : idvol 
								},								
								waitTitle: 'Please wait..',
		                        waitMsg: 'Sending data...',
								success: function(form, action) {
									wind.close();
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Success",obj.errors.reason);
									closeTabs();
									tabPanel.add({
										title   :'Workshop Overview',
										id      :'secondTab',
										height  :300,
										closable:true,
										autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
									}).show();	
								},
								failure: function(form, action) {
	                                obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.errors.reason);
								}
							});
						}
					}
				},{
					text: 'Cancel',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind.close();
				   }
			}]				
		});
		var wind = new Ext.Window({
				title: 'Edit Volumen',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 200,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});		
	}
	
	function DeleteColumnRow(idwor){
		
		
		var storeVolumensComboDelete= new Ext.data.JsonStore({
			url:'php/comboVolumens.php?w='+idwor,
			root: 'data',					
			totalProperty: 'num',
			fields: ['idvol','namevolumen']
		});
		storeVolumensComboDelete.load();
		
		var storeSubjectComboDelete= new Ext.data.JsonStore({
			url:'php/comboSubject.php?w='+idwor,
			root: 'data',					
			totalProperty: 'num',
			fields: ['idsub','namesubject']
		});
		storeSubjectComboDelete.load();
		
		var formul4 = new Ext.FormPanel({
			frame:true,
			layout: 'form',
			border:false,
			items:[{
				xtype: 'compositefield',
				fieldLabel:'Volumen',
				items: [{
					xtype: 'combo',
					store: storeVolumensComboDelete,
					emptyText : 'Select volumen',
					fieldLabel:'Volumen ',
					id: 'voldelete',
					name: 'voldelete',
					hiddenName: 'UD_voldelete', 
					valueField: 'idvol',
					displayField:'namevolumen',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true,
					//allowBlank: false,
					width: 150
				},{
					xtype: 'button', 
					icon:'img/delete.gif',
					tooltip : 'Click to delete  volumen',				
					text : 'Delete',
					width: 70,				
					listeners : {
						click : function()
						{
							if (Ext.getCmp('voldelete').getValue()!='') {
								Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
									if (btn == 'yes'){									
										Ext.Ajax.request({  
												waitTitle: 'Please wait..',
												waitMsg: 'Sending data...',
												url: 'php/response_delete.php', 
												method: 'POST', 
												params: { 
													oper: "volumen", 
													idvalue: Ext.getCmp('voldelete').getValue()
												},
												success:function(response,options){
													wind3.close();
													store.reload();
													closeTabs();
													tabPanel.add({
														title   :'Workshop Overview',
														id      :'secondTab',
														height  :300,
														closable:true,
														autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
													}).show();	
												},
												failure:function(response,options){
													var d = Ext.util.JSON.decode(response.responseText);
													Ext.Msg.alert('Failure', d.errors.reason);
												}
											});	
										}
									})
							}
							else
								Ext.Msg.alert('Warning', 'Please select volumen');
						}						
					}
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Subject',
				items: [{
					xtype: 'combo',
					store: storeSubjectComboDelete,
					emptyText : 'Select subject',
					fieldLabel:'Subject ',
					id: 'subdelete',
					name: 'subdelete',
					hiddenName: 'UD_subdelete', 
					valueField: 'idsub',
					displayField:'namesubject',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true,
					//allowBlank: false,
					width: 150
				},{
					xtype: 'button', 
					icon:'img/delete.gif',
					tooltip : 'Click to delete  Subject',				
					text : 'Delete',
					width: 70,				
					listeners : {
						click : function()
						{
							if (Ext.getCmp('subdelete').getValue()!='') {
								Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
									if (btn == 'yes'){
								
										Ext.Ajax.request({  
												waitTitle: 'Please wait..',
												waitMsg: 'Sending data...',
												url: 'php/response_delete.php', 
												method: 'POST', 
												params: { 
													oper: "subject", 
													idvalue: Ext.getCmp('subdelete').getValue()
												},
												success:function(response,options){
													wind3.close();
													store.reload();
													closeTabs();
													tabPanel.add({
														title   :'Workshop Overview',
														id      :'secondTab',
														height  :300,
														closable:true,
														autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
													}).show();	
												},
												failure:function(response,options){
													var d = Ext.util.JSON.decode(response.responseText);
													Ext.Msg.alert('Failure', d.errors.reason);
												}
										});	
									}
								})
							}
							else
								Ext.Msg.alert('Warning', 'Please select subject');							
						}						
					}
				}]
			}],
			buttons: [{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'img/exit.gif',
				handler  : function(){
					wind3.close();
					closeTabs();
					tabPanel.add({
						title   :'Workshop Overview',
						id      :'secondTab',
						height  :300,
						closable:true,
						autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
					}).show();	
				}						
			}]				
		});
		
		var wind3 = new Ext.Window({
			title: 'Delete Window',
			iconCls: 'x-icon-templates',
			layout      : 'fit',
			width       : 400,
			height      : 150,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: formul4				
		});
	
		wind3.show();
		wind3.addListener("beforeshow",function(wind3){
				formul4.getForm().reset();
		});
	}
	
	function AddWorkshop()
	{
		var storeComboFrecuency= new Ext.data.JsonStore({
			url:'php/comboFrecuency.php',
			root: 'data',					
			totalProperty: 'num',
			fields: ['idfre','frecuency']
		});
		storeComboFrecuency.load();
	
		var formul = new Ext.FormPanel({
			url:'php/response_add.php',
			frame:true,
			//bodyStyle:'padding:5px 5px 0',			
			items: [{
				layout:'column',
				border:false,
				items:[{
					layout: 'form',
					columnWidth:.6,
					border:false,
					items:[{
						xtype: 'textfield',
						id: 'workshop',
						name: 'workshop',
						fieldLabel: '<span style="color:#F00">*</span> Workshop',
						allowBlank:false,
						allowDecimals : true,
						width: 250
					},{
						xtype:'numberfield',
						id: 'totalvolumes',
						name: 'totalvolumes',
						fieldLabel: '<span style="color:#F00">*</span> Total Volumes',
						allowBlank:false,
						allowDecimals : false,
						width: 100
					},{
						xtype:'textfield',
						id: 'namevolumes',
						name: 'namevolumes',
						fieldLabel: '<span style="color:#F00">*</span> Text Volumes',
						allowBlank:false,
						width: 150
					},{
						xtype:'numberfield',
						id: 'totalsubject',
						name: 'totalsubject',
						fieldLabel: '<span style="color:#F00">*</span> Total Subject',
						allowBlank:false,
						width: 100
					},{
						xtype:'textarea',
						id: 'description',
						name: 'description',
						fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> Description',
						allowBlank:true,
						width: 250,
						height: 100						
					}]
				},{
					layout: 'form',
					border:false,
					columnWidth:.4,
					items:[{
						xtype:'numberfield',
						id: 'price',
						name: 'price',
						fieldLabel: '<span style="color:#F00">*</span> Price ',
						allowBlank:false,
						allowDecimals : true,
						width: 150
					},{
						xtype: 'combo',
						store: storeComboFrecuency,
						emptyText : 'Select item',
						fieldLabel: '<span style="color:#F00">*</span> Freq. Pay',
						id: 'frecuencypay',
						name: 'frecuencypay',
						hiddenName: 'UD_frecuencypay', 
						valueField: 'idfre',
						displayField:'frecuency',
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true,
						//allowBlank: false,
						width: 150
					},{
						xtype: 'combo',
						store: storeComboFrecuency,
						emptyText : 'Select item',
						fieldLabel: '<span style="color:#F00">*</span> Freq. Delivery',
						id: 'frecuencydelivery',
						name: 'frecuencydelivery',
						hiddenName: 'UD_frecuencydelivery', 
						valueField: 'idfre',
						displayField:'frecuency',
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true,
						//allowBlank: false,
						width: 150
					},{
						xtype:'textfield',
						id: 'deliveryvolumen',
						name: 'deliveryvolumen',
						fieldLabel: '<span style="color:#F00">*</span> Delivery Volum.',
						allowBlank:false,
						width: 100
					}]					
				}]
			}],					
			buttons: [{
					text: 'Save',
					cls: 'x-btn-text-icon',			
					icon: 'img/save.gif',
	                formBind: true,
					handler  : function(){
						if(formul.getForm().isValid())
						{
							formul.getForm().submit({
								method: 'POST',
								params: {
									oper : 'workshop'										
								},								
								waitTitle: 'Please wait..',
		                        waitMsg: 'Sending data...',
								success: function() {
									store.reload();
									Ext.Msg.alert("Success", "Insertion successfully");
									wind.close();
								},
								failure: function(form, action) {
	                                obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.errors.reason);
								}
							});
						}
					}
				},{
					text: 'Cancel',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind.close();
				   }
			}]				
		});
		var wind = new Ext.Window({
				title: 'New Workshop',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 750,
				height      : 300,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function doAdd()
	
	function EditWorkshop(idwor)
	{

		Ext.Ajax.request({  
			waitTitle: 'Please wait..',
			waitMsg: 'Sending data...',
			url: 'php/response_grid.php', 
			method: 'POST', 
			params: { 
				oper: "onlyworkshop", 
				idwor: idwor
			},
			success:function(response,options){
				var e = Ext.decode(response.responseText);
				var storeComboFrecuency= new Ext.data.JsonStore({
					url:'php/comboFrecuency.php',
					root: 'data',					
					totalProperty: 'num',
					fields: ['idfre','frecuency']
				});
				storeComboFrecuency.load();

				var formul = new Ext.FormPanel({
					url:'php/response_edit.php',
					frame:true,
					//bodyStyle:'padding:5px 5px 0',			
					items: [{
						layout:'column',
						border:false,
						items:[{
							layout: 'form',
							columnWidth:.6,
							border:false,
							items:[{
								xtype: 'textfield',
								id: 'workshop',
								name: 'workshop',
								fieldLabel: '<span style="color:#F00">*</span> Workshop',
								allowBlank:false,
								allowDecimals : true,
								value: e.nameworkshop,
								width: 250
							},{
								xtype: 'combo',
								store:new Ext.data.SimpleStore({
									fields: ['value','text'],
									data  : [
										['Y','Yes'],
										['N','No']
									]
								}),
								//emptyText : 'Select item',
								fieldLabel: '<span style="color:#F00">*</span> Active',
								id: 'activews',
								name: 'activews',
								hiddenName: 'activews', 
								hiddenValue:  e.active, 
								valueField: 'value',
								displayField:'text',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								allowBlank: false,
								value: e.active,																
								width: 150
							},{
								xtype:'numberfield',
								id: 'totalvolumes',
								name: 'totalvolumes',
								fieldLabel: '<span style="color:#F00">*</span> Total Volumes',
								allowBlank:false,
								allowDecimals : false,
								value: e.countvolumen,
								readOnly: true,
								width: 100
							},{
								xtype:'numberfield',
								id: 'totalsubject',
								name: 'totalsubject',
								fieldLabel: '<span style="color:#F00">*</span> Total Subject',
								allowBlank:false,
								value: e.countsubject,
								readOnly: true,
								width: 100
							},{
								xtype:'textarea',
								id: 'description',
								name: 'description',
								fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> Description',
								allowBlank:true,
								value: e.description,
								width: 250,
								height: 100						
							}]
						},{
							layout: 'form',
							border:false,
							columnWidth:.4,
							items:[{
								xtype:'textfield',
								id: 'price',
								name: 'price',
								fieldLabel: '<span style="color:#F00">*</span> Price ',
								allowBlank:false,
								allowDecimals : true,
								value: e.price,								
								width: 150
							},{
								xtype: 'combo',
								store: storeComboFrecuency,
								emptyText : 'Select item',
								fieldLabel: '<span style="color:#F00">*</span> Freq. Pay',
								id: 'frecuencypay',
								name: 'frecuencypay',
								hiddenName: 'frecuencypay', 
								hiddenValue:  e.idfrepay, 
								valueField: 'idfre',
								displayField:'frecuency',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								allowBlank: false,
								value: e.fpay,																
								width: 150
							},{
								xtype: 'combo',
								store: storeComboFrecuency,
								emptyText : 'Select item',
								fieldLabel: '<span style="color:#F00">*</span> Freq. Delivery',
								id: 'frecuencydelivery',
								name: 'frecuencydelivery',
								hiddenName: 'frecuencydelivery', 
								hiddenValue: e.idfredeliver, 
								valueField: 'idfre',
								displayField:'frecuency',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								allowBlank: false,
								value: e.fdeliver,																
								width: 150
							},{
								xtype:'textfield',
								id: 'deliveryvolumen',
								name: 'deliveryvolumen',
								fieldLabel: '<span style="color:#F00">*</span> Delivery Volum.',
								allowBlank:false,
								value: e.deliveryvolume,																
								width: 100
							}]					
						}]
					}],					
					buttons: [{
							text: 'Save',
							cls: 'x-btn-text-icon',			
							icon: 'img/save.gif',
							formBind: true,
							handler  : function(){
								if(formul.getForm().isValid())
								{
									formul.getForm().submit({
										method: 'POST',
										params: {
											oper : 'workshopfull',
											idwor: idwor											
										},								
										waitTitle: 'Please wait..',
										waitMsg: 'Sending data...',
										success: function() {
											store.reload();
											Ext.Msg.alert("Success", "Update successfully");
											wind.close();
										},
										failure: function(form, action) {
											obj = Ext.util.JSON.decode(action.response.responseText);
											Ext.Msg.alert("Failure", obj.errors.reason);
										}
									});
								}
							}
						},{
							text: 'Cancel',
							cls: 'x-btn-text-icon',			
							icon: 'img/exit.gif',
							handler  : function(){
								wind.close();
						   }
					}]				
				});
				var wind = new Ext.Window({
						title: 'Edit Workshop',
						iconCls: 'x-icon-templates',
						layout      : 'fit',
						width       : 750,
						height      : 300,
						resizable   : false,
						modal	 	: true,
						plain       : true,
						items		: formul				
					});
			
				wind.show();
				wind.addListener("beforeshow",function(wind){
						formul.getForm().reset();
				});
						
			},
			failure:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Failure', d.errors.reason);
			}
		});	
	
	}//fin function EditWorkshop()

	function CopyWorkshop()
	{
		var selected=itemGridSelected();
		var j=0;
		if(selected.length>2)j=1;
		if(j==0)
		{	Ext.MessageBox.alert('Warning','You must select a row.');
			return;
		}
		selected=selected.replace('(','');
		selected=selected.replace(')','');
		if(selected.split(',').length>1){
			Ext.MessageBox.alert('Warning','You must select a row.');
			return;
		}

		var selec = Grid.selModel.getSelections();
		var i=0;
		var workshop='';		
		for(i=0; i<selec.length; i++){
			if(selec[i].json.idwor==selected)
			{
				workshop=selec[i].json.nameworkshop;
			}
		}
	
		var formul = new Ext.FormPanel({
			url:'php/response_add.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'copyworkshop',
				name: 'copyworkshop',
				fieldLabel: '<span style="color:#F00">*</span> Workshop',
				allowBlank:false,
				value: 'Copy - '+workshop,
				width: 250
			}],
			buttons: [{
					text: 'Save',
					cls: 'x-btn-text-icon',			
					icon: 'img/save.gif',
	                formBind: true,
					handler  : function(){
						if(formul.getForm().isValid())
						{
							formul.getForm().submit({
								method: 'POST',
								params: {
									oper : 'copyworkshop',
									idwor: selected 
								},								
								waitTitle: 'Please wait..',
		                        waitMsg: 'Sending data...',
								success: function() {
									store.reload();
									Ext.Msg.alert("Success", "Insertion successfully");
									wind.close();
								},
								failure: function(form, action) {
	                                obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.errors.reason);
								}
							});
						}
					}
				},{
					text: 'Cancel',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind.close();
				   }
			}]				
		});
		var wind = new Ext.Window({
				title: 'Copy Workshop ',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 130,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function CopyWorkshop()
	
	function EditCellMatrix(idwor,namewor,idvol,namevol,idsub,namesub)
	{
		var clickaddfile=0;
		
		deleteVolumen=function (val)  		
		{
			Ext.Ajax.request({  
					waitTitle: 'Please wait..',
					waitMsg: 'Sending data...',
					url: 'php/response_delete.php', 
					method: 'POST', 
					params: { 
						oper: "fileslist", 
						idvalue: val
					},
					success:function(response,options){
						storeVolumens.reload();
						var d = Ext.util.JSON.decode(response.responseText);
						clickaddfile=1;
						//Ext.MessageBox.alert('Success',d.errors.reason);
					},
					failure:function(response,options){
						var d = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert('Failure', d.errors.reason);
					}
			});		
		}//function deleteVolumen(btn)			
		
		previewfile=function (val,ext)  		
		{
			var wind4 = new Ext.Window({
				title: 'View File',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 470,
				height      : 400,
				resizable   : true,
				modal	 	: true,
				plain       : true,
				autoScroll : true,
				maximizable : true,
				autoLoad 	: {url:'php/previewfile.php', scripts: true, params:{val:val,ext:ext}}				
			});		
			wind4.show();
		}//function previewfile(btn)		
		
		///////////Data store Volumens///////////////
		var storeVolumens = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'results',
			url: 'php/response_grid.php?idvol='+idvol+'&idsub='+idsub+'&oper=fileslist',
			fields: [
				{name: 'idvsub', type: 'int'}
				,'nameresource'	
				,'sizefile'	
				,'pathresource'	
			],
			remoteSort: true
		});
		storeVolumens.load();
		////////////////Toolbar//////////////////////
		var ToolbarVolumens = new Ext.Toolbar({
			items  : [{
				id: 'add_button9',
				tooltip: 'Click to Refresh Grid',
				iconCls:'icon',
				icon: 'img/view-refresh.png',
				text: 'Refresh',
				handler:function(){storeVolumens.reload();}
			}]			
		});
		
		function renderOperationFile(val, p, record){
			var arraux=record.data.pathresource.split('.');
			var ext=arraux[arraux.length-1].toLowerCase();
			var textcad='<a href="'+record.data.pathresource+'" target="_blank"><img src="img/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
			if(ext=='flv' || ext=='mp4' || ext=='gif' || ext=='jpeg' || ext=='jpg' || ext=='png' || ext=='bmp' || ext=='tif')
				textcad='<a href="javascript:void(0)" onClick="previewfile(\''+record.data.pathresource+'\',\''+ext+'\')"><img src="img/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
				
			var retorno= String.format(	'<a href="javascript:void(0)" onClick="deleteVolumen({0})" ><img src="img/delete.gif" border="0" title="Click to delete file"></a>&nbsp;'+
										textcad,val);
										
			return retorno;
		}
		
		var GridVolumens = new Ext.grid.EditorGridPanel({
			store: storeVolumens,
			iconCls: 'x-icon-users',
			icon: 'img/users.png',
			tbar: ToolbarVolumens, 
			columns: [	
				 new Ext.grid.RowNumberer()
				,{header: 'File',		width: 250, sortable: true, align: 'left', dataIndex: 'nameresource'}
				,{header: 'File Size',	width: 70, sortable: true, align: 'right', dataIndex: 'sizefile'}
				,{header: " ", 			width: 50, align: 'center', sortable: true, dataIndex: 'idvsub',renderer: renderOperationFile}						
				
			],
			clicksToEdit:2,
			sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
			height:220,
			width: '99.8%',
			frame:true,
			loadMask:true
		});		

		var storeVolumensCombo= new Ext.data.JsonStore({
			url:'php/comboVolumens.php?w='+idwor,
			root: 'data',					
			totalProperty: 'num',
			fields: ['idcla','name']
		});

        var uploadForm = new Ext.FormPanel({
			fileUpload  :true,
			width: '99.8%',
            frame       :true,
            autoheight  :true,
            labelWidth  : 60,
            defaults    :{
				anchor  : '95%',
                msgTarget: 'side'
			},
            items       :[{
				xtype: 'textfield',
				id: 'namewor',
				name: 'namewor',
				fieldLabel: 'Workshop',
				value: namewor,
				allowBlank:false,
				readOnly: true,
				width: 320
			},{
				xtype: 'textfield',
				id: 'namesub',
				name: 'namesub',
				fieldLabel: 'Subject',
				value: namesub,
				allowBlank:false,
				readOnly: true,
				width: 320
			},{
				xtype: 'textfield',
				id: 'namevol',
				name: 'namevol',
				fieldLabel: 'Volumen',
				value: namevol,
				allowBlank:false,
				readOnly: true,
				width: 320
			},{
				xtype       :'fileuploadfield',
				name        : 'upfile',
                id          : 'form-file1',
				emptyText   : 'Select a file',
                fieldLabel  :'File',
				allowBlank: true,
                buttonCfg   :{
					text    :'',
                    iconCls :'x-icon-upload'
                }
			}],
            buttons     :[{
				text        :'Clear',
                cls         :'x-btn-text-icon',
                icon        :'img/refresh.png',
                handler     :function(){
					uploadForm.getForm().reset();
                }
			},{
				text:'Add File',
                cls:'x-btn-text-icon',
                icon:'img/add.gif',
                handler:function(){
					if(uploadForm.getForm().isValid()){
						if(Ext.getCmp('form-file1').getValue()==''){Ext.Msg.alert("Warning","Please select a File");	return;}	
						clickaddfile=1;						
						uploadForm.getForm().submit({
							url     :'includes/uploadExtjs/phpupload.php',
							waitMsg :'Uploading...',
							params:{
								idwor:idwor,
								idvol:idvol,
								idsub:idsub
							},
                            success :function(form,action){
								//Ext.Msg.alert("Succes",action.response.responseText);								
								//uploadForm.getForm().reset();
								Ext.getCmp('form-file1').setValue('');
								storeVolumens.reload();
							},
                            failure :function(form,action){
								obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.errors.reason);
								//Ext.Msg.alert("Failure",action.response.responseText);		
							}
						});
					}
				}
			}]
		});

		var formul2 = new Ext.FormPanel({
			frame:true,
			layout: 'form',
			border:false,
			items: [
				uploadForm
				,GridVolumens
			],
			buttons: [{
					text: 'Close',
					cls: 'x-btn-text-icon',			
					icon: 'img/exit.gif',
					handler  : function(){
						wind3.close();
						if(clickaddfile==1)
						{
							closeTabs();
							tabPanel.add({
								title   :'Workshop Overview',
								id      :'secondTab',
								height  :300,
								closable:true,
								autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
							}).show();	
						}						
				   }
			}]				
		});
		
		var wind3 = new Ext.Window({
			title: 'Edit Window',
			iconCls: 'x-icon-templates',
			layout      : 'fit',
			width       : 500,
			height      : 470,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: formul2				
		});
	
		wind3.show();
		wind3.addListener("beforeshow",function(wind3){
				formul2.getForm().reset();
		});
	}//fin function AddClassWindow()
	
	function EditWorkshopMatriz(idwor)
	{
		closeTabs();
	
		tabPanel.add({
			title   :'Workshop Overview',
			id      :'secondTab',
			height  :300,
			closable:true,
			autoLoad    :{url: 'php/showWorkshopMatriz.php', scripts: true, params:{idwor:idwor}}
		}).show();
	}

