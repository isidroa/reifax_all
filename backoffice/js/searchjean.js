var arrmail;
var fechaInicial;
var fechaFinal;
var nombre;
var apellido;
var dirbkoffice='';
var wherepage='affMarketingHistFull';//variable que dice de que pagina se esta ejecutando
var VFilas2;
var VHeader2;
var VFields2;
function nuevoAjax()
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	//var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	
//			
///////////FIN Cargas de data dinamica///////////////
		
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Form//////////////////////////////////
		
	var tabPanel = new Ext.TabPanel({  
		border: false,  
		activeTab: 0,  
		enableTabScroll:true,  
		height: 570,	
		items:[Ext.searchUsers.form,]  
	 });
	Ext.searchUsers.search=function(){
		updateTab('gridsearch','Resultado','users2.php');
	}
	
	function addTab(tabTitle, targetUrl){
		tabPanel.add(grid);
		store.load(/*{params:{start:0, limit:100}}*/);
		/////////////FIN Inicializar Grid////////////////////
		pagingBar.bind(store);
    }
	function updateTab(tabId,title, url) {
    	
		var tab = tabPanel.getItem(tabId);
    	if(tab){
    		tab.getUpdater().update(url);
    		tab.setTitle(title);
    	}else{
    		tab = addTab(title,url);
    	}
		
    	tabPanel.setActiveTab(1);
    }
	
	var store = new Ext.data.JsonStore({
		idProperty: 'userid',
		remoteSort : true,
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=users',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=users', timeout: 3600000 }),
		//baseParams  :{start:0, limit:100},
		reader: new Ext.data.JsonReader(),						
		fields: [
			{name: 'id', type: 'int'},
			{name: 'userid', type: 'int'},
			'name',
			'surname',
			'email',
			'status',
			'privilege',
			'usertype',
			'accept',
			'executive',
			'affiliation',
			'fechaupd',
			'officenum',
			'procode',
			'notes',
			'pricefull',
			'pricecommercial',
			'platinum',
			'buyerspro',
			'leadsgenerator',
			'homefinder',
			'realtorweb',
			'investorweb',
			'nombres',
			'PASS',
			'blacklist',
			'idfrec',
			'freedays',
			'marcalogin',
			'totalprice',
			{name: 'veceslog', type: 'int'},
			'senddateemail'
		]
	});
	
	var panelpag = new Ext.form.FormPanel({
		width: 415,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			fieldLabel: 'Template Email',
			items:		
			[/*{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				disabled: true, 
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				xtype: 'label',
				text: 'Template Email',
				width: 100 
			},*/{
				width:300,
				fieldLabel:'Template Email',
				name:'cbTemplateEmailGrid',
				id:'cbTemplateEmailGrid',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: true,
				editable: false,
				emptyText : 'Select to view see email template',
				listeners: {
					'select': searchTemplatEmail
				} 
			}]
		}]
	});

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 50,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Displaying {0} - {1} of {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
            pressed: true,
            enableToggle:false,
            text: '<b>Send Email</b>',
            handler: enviaEmail
        },{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
		},'-',
			panelpag
		]
    });
	
////////////////FIN barra de pagineo//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}

	function obtenerSeleccionadosNameEmail(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+='"'+selec[i].data.name+' '+ selec[i].data.surname+'" <'+selec[i].data.email+'>';
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function fillEmail(){
		//store.setBaseParam('idtmail',);
		//store.load({params:{start:0, limit:100}});
		Ext.getCmp("subject").setValue('Loading..');
		Ext.getCmp("bodyemail").setValue('Loading..');
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			waitTitle   :'Please wait!',
			waitMsg     :'Loading...',
			params: { 
				tipo : 'previewEmails',
				idtmail: Ext.getCmp("cbTemplateEmail").getValue()
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);
				Ext.getCmp("subject").setValue(variable.subject);
				Ext.getCmp("bodyemail").setValue(variable.body);
				storeVolumens.setBaseParam('idtmail', Ext.getCmp("cbTemplateEmail").getValue());
				storeVolumens.load(/*{params:{start:0, limit:100}}*/);
			}
		})
		
	}

	function sendTo(){
		if(Ext.getCmp("rbsendtoAll").getValue()==true)
		{
			Ext.getCmp("toids").setValue('.');
			Ext.getCmp("toids").setVisible(false);
		
		}
		if(Ext.getCmp("rbsendtoCustom").getValue()==true)
		{
			var aux=Ext.getCmp("toid").getValue();
			if(aux==false || aux=='false')aux='';
			Ext.getCmp("toids").setValue(aux);
			Ext.getCmp("toids").setVisible(true);
		
		}
	}

	var storeVolumens ='';
	function enviaEmail(){
	
		deleteVolumen=function  (val)  		
		{
			Ext.Ajax.request({  
					waitTitle: 'Please wait..',
					waitMsg: 'Sending data...',
					url: 'php/grid_del.php', 
					method: 'POST', 
					params: { 
						tipo: "fileslist", 
						ID: val
					},
					success:function(response,options){
						storeVolumens.reload();
						var d = Ext.util.JSON.decode(response.responseText);
						clickaddfile=1;
						//Ext.MessageBox.alert('Success',d.errors.reason);
					},
					failure:function(response,options){
						var d = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert('Failure', d.errors.reason);
					}
			});		
		}//function deleteVolumen(btn)			

		///////////Data store Volumens///////////////
		storeVolumens = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'results',
			url: 'php/grid_data.php?&tipo=fileslist',
			fields: [
				{name: 'idatt', type: 'int'}
				,'namefile'	
				,'sizefile'	
				,'pathsaved'	
			],
			remoteSort: true
		});

		var ToolbarVolumens = new Ext.Toolbar({
			items  : [{
				id: 'add_button9',
				tooltip: 'Click to Refresh Attachments Grid',
				iconCls:'icon',
				icon: 'images/view-refresh.png',
				text: 'Refresh',
				handler:function(){storeVolumens.reload();}
			}]			
		});
		
		function renderOperationFile(val, p, record){
			var arraux=record.data.pathsaved.split('.');
			var ext=arraux[arraux.length-1].toLowerCase();
			var textcad='';
			//var textcad='<a href="'+record.data.pathsaved+'" target="_blank"><img src="images/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
			//if(ext=='flv' || ext=='mp4' || ext=='gif' || ext=='jpeg' || ext=='jpg' || ext=='png' || ext=='bmp' || ext=='tif')
			//	textcad='<a href="javascript:void(0)" onClick="previewfile(\''+record.data.pathsaved+'\',\''+ext+'\')"><img src="images/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
				
			var retorno= String.format(	'<a href="javascript:void(0)" onClick="deleteVolumen({0})" ><img src="images/delete.gif" border="0" title="Click to delete file"></a>&nbsp;'+
										textcad,val);
										
			return retorno;
		}
		
		var GridVolumens = new Ext.grid.EditorGridPanel({
			store: storeVolumens,
			tbar: ToolbarVolumens, 
			columns: [	
				 new Ext.grid.RowNumberer()
				,{header: 'File Name',		width: 300, sortable: true, align: 'left', dataIndex: 'namefile'}
				,{header: 'File Size',	width: 120, sortable: true, align: 'right', dataIndex: 'sizefile'}
				,{header: " ", 			width: 50, align: 'center', sortable: true, dataIndex: 'idatt',renderer: renderOperationFile}						
				
			],
			clicksToEdit:2,
			sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
			height:120,
			width: '99.8%',
			frame:true,
			loadMask:true
		});		
        var uploadForm = new Ext.FormPanel({
			fileUpload  :true,
			width: '99.8%',
            frame       :true,
            autoheight  :true,
            labelWidth  : 60,
            defaults    :{
				anchor  : '95%',
                msgTarget: 'side'
			},
            items       :[{
					layout:'column',
					items:[{
						columnWidth:.85,
						layout: 'form',
						items: [{
							xtype       :'fileuploadfield',
							name        : 'upfile',
							id          : 'form-file1',
							emptyText   : 'Select a file',
							fieldLabel  :'File',
							allowBlank: true,
							width: 480,
							buttonCfg   :{
								text    :'',
								cls: 'x-btn-text-icon',			
								icon: 'images/saveas.png'
							}
						}]
					},{
						columnWidth:.15,
						layout: 'form',
						items: [{
							xtype: 'button',
							text:'<b>Add File</b>',
							cls:'x-btn-text-icon',
							icon:'images/add.png',
							width: 95,
							height: 25,
							handler:function(){
								//alert('estamos en eso');return;
								var tempmail=Ext.getCmp("cbTemplateEmail").getValue();
								if(tempmail==0 || tempmail=='' ){Ext.Msg.alert("Warning","Please select a Template Email");	return;}	
								if(uploadForm.getForm().isValid()){
									if(Ext.getCmp('form-file1').getValue()==''){Ext.Msg.alert("Warning","Please select a File");	return;}	
									clickaddfile=1;						
									uploadForm.getForm().submit({
										url     :'includes/uploadExtjs/phpupload.php',
										waitMsg :'Uploading...',
										params:{
											'idtmail': tempmail
										},
										success :function(form,action){
											//Ext.Msg.alert("Succes",action.response.responseText);								
											//uploadForm.getForm().reset();
											Ext.getCmp('form-file1').setValue('');
											storeVolumens.setBaseParam('idtmail',tempmail);
											storeVolumens.load(/*{params:{start:0, limit:100}}*/);
										},
										failure :function(form,action){
											obj = Ext.util.JSON.decode(action.response.responseText);
											Ext.Msg.alert("Failure", obj.errors.reason);
											//Ext.Msg.alert("Failure",action.response.responseText);		
										}
									});
								}
							}
						}]
					}]
			}]
		});
	
		var obtenidos=selectionPaging.showSelectedRows();
		
		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype: 'textarea',
				fieldLabel:'Toid',
				id:'toids',
				name:'toids',
				value: obtenidos,
				allowBlank: false,
				readOnly:true,
				width:620,
				height:60  
			}/*,{
				xtype: 'hidden',
				id: 'toid',
				name: 'toid',
				value: obtenidos
			}*/,{
				width:640,
				fieldLabel:'Template',
				name:'cbTemplateEmail',
				id:'cbTemplateEmail',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				hiddenName: 'cbidtmail',
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				editable: false,
				//value: activews,
				listeners: {
					'select': fillEmail
				} 
			},{
				xtype:'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'subject',
				width:640,
				allowBlank: false  
			},{
				xtype: 'htmleditor',
				id: 'bodyemail',
				id: 'bodyemail',
				hideLabel: true,
				height: 190,
				width:700,
				allowBlank: false  
			},{
				xtype: 'fieldset',
				title: 'Attachments',
				autoHeight: true,
				items: [uploadForm
				,GridVolumens
				]
			}],
			buttonAlign: 'center',
			buttons: [{
				text: 'Update Template',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
				formBind: true,
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
					if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/grid_edit.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Loading...',
							params: {
								tipo : 'templateEmails',
								idtmail:  Ext.getCmp("cbTemplateEmail").getValue()										
							},	
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								Ext.Msg.alert('Success', 'Update succesfully');
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},{
				text: '<b>Send Email</b>',
				cls: 'x-btn-text-icon',			
				icon: 'images/sendemail.jpg',				
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/sendingEmail.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Sending email...',
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								selectionPaging.clearSelections();
								w.close();
								store.reload();
								//Ext.Msg.alert('Success', action.result.msg);
								
								var variable = Ext.decode(action.response.responseText);
								var resulthtml=	'<table  border="0" cellpadding="0" cellspacing="0" style=" font-size:8pt">'+
												'	  <tr bgcolor="#FFFFCC" align="center">'+
												'		<td width="40px">&nbsp;</td>'+
												'		<td width="70px"><b>Userid</b></td>'+
												'		<td width="150px"><b>Name</b></td>'+
												'		<td width="180px"><b>Email</b></td>'+
												'		<td width="60px"><b>Send</b></td>'+
												'		<td width="320px"><b>Msg</b></td>'+
												'	  </tr>'
												;
								
								for (var i=0;i<variable.data.length;i++)
								{
									
									
									resulthtml+= '	<tr bgcolor="'+((i%2)==0?'#ffffff':'#fefee9')+'" >'+
												'		<td align="center">'+(i+1)+'</td>'+
												'		<td align="center">'+variable.data[i]['userid']+'</td>'+
												'		<td>'+variable.data[i]['toname']+'</td>'+
												'		<td>'+variable.data[i]['toemail']+'</td>'+
												'		<td align="center">'+(variable.data[i]['sending']==1?'<img src="images/check.png" border="0" />':'<img src="images/cross.gif" border="0" />')+'</td>'+
												'		<td>'+(variable.data[i]['errorsending'].length>0?variable.data[i]['errorsending']:'&nbsp;')+'</td>'+
												'	</tr>'
											;
  
								}
								resulthtml+='</table>';
								
								
								var w1 = new Ext.Window({
									title: 'Response Email', 
									width: 750,
									height: 500,
									closable :true,
									modal: true,
									layout: 'fit',
									autoScroll : true,
									plain: true,
									bodyStyle: 'padding:5px;',
									html: resulthtml
								});
								w1.show();								
							},
							failure: function(form, action) {
								selectionPaging.clearSelections();
								store.reload();
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},
				'->'
			,{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'Send Email', 
			width: 750,
			height: 580,//550
			layout: 'fit',
			plain: true,
			modal: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
	}
	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.load();
	}

	function searchTemplatEmail(){
		store.setBaseParam('idtmail',Ext.getCmp("cbTemplateEmailGrid").getValue());
		store.load(/*{params:{start:0, limit:100}}*/);
	}

		
	function mostrarEmail()//Muestra lista de los email que se enviaron
	{
		var htmlTag="";
		var i;
		htmlTag+='<table cellspacing="0" class="tblEmail"><tbody>';
		htmlTag+='<tr class="subtitulo"><th width="235">Sending Email</th><th width="15"> <a href="#" onClick="javascript:visibleDeudor(\'WinEmail\',\'ocultar\'); return false;"><div align="right"><img alt="Close Window" src="includes/img/cerrar.png" border="0"></div></a></th></tr>';
		htmlTag+='<tr class="color1"><td colspan="2"><br>MESSAGE SENT: '+subject+' <br>To the following Users: <br>';
		for(i=0;i<arrmail.length;i++)
		{
			htmlTag+=arrmail[i].email+'<br>';
		}
		htmlTag+='</td></tr>';
		htmlTag+='<tr class="color1"><td colspan="2" ><div align="center"><INPUT class="botonEmail" id="botAceptaEmail" type="button" value="OK" onclick="javascript:visibleDeudor(\'WinEmail\',\'ocultar\'); return false;" ></div></td></tr>';
		htmlTag+='<tr><td></td></tr>';
		htmlTag+='</tbody></table>';
	//	alert(htmlTag);
		var html=htmlTag.join('');
		var winEmail=new Ext.Window({
						 title: 'Email',
						 width: 500,
						 height: 450,
					 	 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 html: html
				});
		winEmail.show();
	}
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("parametro=users&adicional=1");
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		var ID =	oGrid_Event.record.data.userid;
		campo=oGrid_Event.field;
		info=fieldValue;
		type='text';
		if(fieldValue=='BEGIN')
		{Ext.MessageBox.alert('Warning','Error select BEGIN');store.reload();return;}
		var ajax=nuevoAjax();
		ajax.open("POST", "respuesta_users.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("campo="+campo+"&info="+info+"&type="+type+"&oper=modificar"+"&ID="+ID);
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{  
				store.commitChanges();
				store.reload();
			}
		}
	}; 
//////////////////Manejo de Eventos//////////////////////
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	function obtenerTotal(){
		//Ext.MessageBox.alert('Warningsmith',store.data.fullids);

		/*Ext.getCmp('total').setText('Loading...');//'Total Amount: '+(acum).toFixed(2));	
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			params: { 
				tipo : 'users',
				only:'getTotalPrice'
			},
			failure:function(response,options){
				//Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);
				Ext.getCmp('total').setText(variable.totalprice);//'Total Amount: '+(acum).toFixed(2));	
			}
		})
		*/
/*
		var totales = store.getRange(0,store.getCount());
		//var i;
		var acum=0;
		//for(i=0;i<store.getCount();i++)
		//acum = acum + parseFloat(totales[0].data.totalprice);
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
*/
	}	
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function checkCommercial(val,p,record){
		if(val>0){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function checkRealtor(val,p,record){
		if(val==1){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function checkInvestor(val,p,record){
		if(val==1){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{0}">',val);
		}	
	}

 
    function changeColor(val, p, record){
        if(record.data.blacklist == 'Y')
		{
            return '<span style="color:red;font-weight: bold;">' + val + '</span>';
        }
        if(record.data.idfrec == 2)
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
        return val;
    }
	
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
  var myView = new Ext.grid.GridView();
   myView.getRowClass = function(record, index, rowParams, store) {
		
        if(record.data['blacklist'] == 'Y')return 'rowRed';
        if(record.data['idfrec'] == 2)return 'rowGreen';
		if(record.data['marcalogin'] == 'Y')return 'orange-row';
   };
	var selectionPaging = new Ext.ux.grid.RowSelectionPaging();
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		view: myView,
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{				header: ' ', 			width: 30, sortable: true, align: 'center', dataIndex: 'notes', renderer: comment}
			,{				header: 'Name', 		width: 90, sortable: true, align: 'left', dataIndex: 'name',renderer: changeColor}//,editor: new Ext.form.TextField({allowBlank: false})
			,{				header: 'Last Name', 	width: 90, align: 'left', sortable: true, dataIndex: 'surname',renderer: changeColor}
			,{id:'userid',	header: 'User ID', 		width: 45, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{				header: 'Email', 		width: 160, align: 'left', sortable: true, dataIndex: 'email',editor: new Ext.form.TextField()}
			,{				header: 'Password', 	width: 100, align: 'left', sortable: true, dataIndex: 'PASS',editor: new Ext.form.TextField()}			
			,{				header: 'Status', 		width: 75, align: 'left', sortable: true, dataIndex: 'status'}
			,{				header: 'Price', 		width: 70, align: 'right', sortable: true, dataIndex: 'pricefull', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{				header: '30 logs', width: 50, sortable: true, align: 'center', tooltip: 'Last 30 logs', dataIndex: 'veceslog'}
			//,{				header: 'User Type', 	width:100, sortable: true, align: 'left', dataIndex: 'usertype'}
			//,{				header: 'Accept', 		width:50, sortable: true, align: 'center', dataIndex: 'accept'}
			,{				header: 'Executive', 	width:150, sortable: true, align: 'left', dataIndex: 'nombres',hidden :true}
			,{				header: 'Sing Up', 		width: 70, sortable: true, align: 'left', dataIndex: 'affiliation'}
			,{				header: 'Trial', 	width: 35, sortable: true, align: 'center', dataIndex: 'freedays'}
			,{				header: "Send Date Email", width: 115, align: 'left', sortable: true, dataIndex: 'senddateemail'}
			,{				header: "Suspended Date", width: 115, align: 'left', sortable: true, dataIndex: 'fechaupd'}
			/*,{				header: 'Product Price', width: 95, sortable: true, align: 'right', dataIndex: 'preci0'}
			,{				header: 'PayDate', width: 95, sortable: true, align: 'center', dataIndex: 'fechacobro'}*/
		],
		clicksToEdit:2,
		sm: mySelectionModel,
        plugins: [selectionPaging],
		height:470,
		frame:true,
		title:"Search Results",
		loadMask:true,
		tbar: pagingBar
		
	});	
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: tabPanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
	store.addListener('load', obtenerTotal);	
//////////////////FIN Listener///////////////////////
	
});

