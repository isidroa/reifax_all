function nuevoAjax()
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false;  
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=deudores',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=deudores', timeout: 3600000 }),
		reader: new Ext.data.JsonReader(),				
		baseParams  :{start:0, limit:200},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status' 
			//,'usertype'
			,'blacklist'
			,'idfrec'
			,'paydated'
			,'priceprod'
			,'lastpay'
			,'cardname'
			,'payduplicity'
			,'saldo'
			,'amountsinformato'
			,'notes'
			,'marcalogin'
			,{name: 'veceslog', type: 'int'}
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	/*var panelpag = new Ext.form.FormPanel({
		width: 700,
		layout: 'form',
		items: [{
			width:200,
			xtype:'label',
			id: 'total',
			text: 'Total Amount: 0.00',
	        margins: '5 0 0 50',
			align: 'center'
		}]
	});*/

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 20000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			'-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
			}
			,'-'
			,{
				width:200,
				xtype:'label',
				id: 'total',
				text: 'Total Amount: 0.00',
				margins: '5 0 0 50',
				align: 'center'
			}
			//,panelpag
		]
    });
////////////////FIN barra de pagineo//////////////////////
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");
		ajax.send("parametro=activos");
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}
//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}
	
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}
	
	function cobrarSeleccionados(){
		Ext.MessageBox.confirm('Procesar Cobro','Desea procesar el Cobro de la Cuota?. Verifique el Check de PAYPAL o AUTHORIZENET.',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					var paypal='false';
					if(document.forms[0].radiocobros[0].checked ==true)paypal='true';
					var authorizenet='false';
					if(document.forms[0].radiocobros[1].checked ==true)authorizenet='true';
					//alert( "paypal = " +  paypal+"|"+"authorizenet = " +  authorizenet+"|" );return;

					Ext.Msg.wait('Processing... please wait!');  
					Ext.getCmp('total').setText('Please wait..!!');	
					var obtenidos=obtenerSeleccionados();
					//alert( var include_type = mypanel.getValues()['radiocobros'] );
					if(!obtenidos){
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Debes seleccionar al menos un usuario para realizar el cobro',
							buttons: Ext.MessageBox.OK,
							icon:Ext.MessageBox.ERROR
						});						
						obtenerTotal();
						return;
					}
					//alert(obtenidos);//return;		
					obtenidos=obtenidos.replace('(','');
					obtenidos=obtenidos.replace(')','');
					//Ext.MessageBox.alert('Warning',obtenidos+'--'+Ext.getCmp('chpaypal').getValue() );return;
					
					Ext.Ajax.request( 
						{  
							waitMsg: 'Saving changes...',
							url: 'php/grid_data.php', 
							method: 'POST', 
							params: { 
								tipo: "cobrarselactivos", 
								users: obtenidos,
								cbPaypal: paypal,
								chauthorizenet: authorizenet
							},
							
							failure:function(response,options){
								Ext.Msg.hide();
								Ext.MessageBox.alert('Warning','Cobros no procesados');
								store.reload();
							},
							success:function(response,options){
								Ext.Msg.hide();
								var rest = Ext.util.JSON.decode(response.responseText);
								Ext.MessageBox.alert('Cobros',rest.msg);
								store.reload();
							}                                
						 }
					); 						
				}
			}
		);
	}
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		for(i=0;i<store.getCount();i++)
			acum = acum + parseFloat(totales[i].data.amountsinformato);
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
	}	

	function runcharges(){
		//location.href="http://localhost/backoffice/cobranzadiaria.php?st=1";
		window.open("http://localhost/backoffice/cobranzadiaria.php?st=1")		
	}
		
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){//onclick="mostrarDetalleUsuers({0},\'customerservices\')"
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }		
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}	
	}
    function changeColor(val, p, record){
        if(record.data.payduplicity == '1' || record.data.payduplicity == 1 )
		{
            return '<span style="color:red;font-weight: bold;">' + val + '</span>';
        }
        if(record.data.blacklist == 'Y')
		{
            return '<span style="color:red;font-weight: bold;">' + val + '</span>';
        }
        if(record.data.idfrec == 2)
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
        return val;
    }
	function payduplicity(val, p, record){
        if(val == '1' || val == 1 )
			return '<img src="images/warning.png" border="0" ext:qtip="CHECK  USER. DOUBLE CHARGE"></a>';
		return;	
	}
	
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
  var myView = new Ext.grid.GridView();
   myView.getRowClass = function(record, index, rowParams, store) {
		
        if(record.data.payduplicity == '1' || record.data.payduplicity == 1 )return 'rowRed';
        if(record.data['blacklist'] == 'Y')return 'rowRed';
        if(record.data['idfrec'] == 2)return 'rowGreen';
		if(record.data['marcalogin'] == 'Y')return 'orange-row';
   };
   
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		view: myView,
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'payduplicity', renderer: payduplicity}
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{id: 'userid', header: "User ID", width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name',renderer: changeColor}
			,{header: "Surname", width: 100, align: 'left', sortable: true, dataIndex: 'surname',renderer: changeColor}			
			,{header: "Status", width: 80, align: 'left', sortable: true, dataIndex: 'status'}
			//,{header: 'User Type', width: 180, sortable: true, align: 'left', dataIndex: 'usertype'}
			,{header: 'Paydate', width: 80, sortable: true, align: 'center', dataIndex: 'paydated'}
			,{header: 'Price', width: 60, sortable: true, align: 'right', dataIndex: 'priceprod'}
			,{header: 'Card Type', width: 100, sortable: true, align: 'left', dataIndex: 'cardname'}
			//,{header: 'Card Number', width: 130, sortable: true, align: 'left', dataIndex: 'cardnumber'}
			,{header: 'Last 30 logs', width: 80, sortable: true, align: 'center', tooltip: 'Last 30 logs', dataIndex: 'veceslog'}
			,{header: 'Last Pay', width: 250, sortable: true, align: 'left', dataIndex: 'lastpay'}
			,{header: 'Balance', width: 100, sortable: true, align: 'right', dataIndex: 'saldo'}
		],
		//clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar 
	});
	
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Cobros Activos',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
		name: 'panelform',
		id: 'panelform',
        items: [{
					xtype: 'compositefield',
					fieldLabel: '<b>Opciones</b>',
					items:[/*{
						width:120,
						id:'historico',
						xtype:'button',
						text:'Historico',
						handler: showHistorico
					},*/{
						width:120,
						id:'cobrarsel',
						xtype:'button',
						text:'Charge Selected',
						handler: cobrarSeleccionados
					},
					{xtype: 'radio', width:120, boxLabel: '<font color="red">&nbsp;<b>PAYPAL</b></font>',  name: 'radiocobros', id: 'radiocobros1'},
					{xtype: 'radio', width:120, boxLabel: '<font color="red">&nbsp;<b>AUTHORIZE</b></font>', name: 'radiocobros', id: 'radiocobros2', checked : true}/*,
					{
						width:120,
						id:'butruncharges',
						xtype:'button',
						text:'RUN CHARGES',
						handler: runcharges
					}*/
					]
				}, 
				grid ]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
