// JavaScript Document
Ext.ns('logsUserGroup');

logsUserGroup={
	init: function ()
	{ 
	
		/******************************************
		********* CONFIGUARCIONES GENERALES DEL GRID
		*******************************************/
		// declaro el lector para el drid
		logsUserGroup.gridReader = new Ext.data.JsonReader({  
			root: 'logs', 
			totalProperty: 'total', 
			id: 'id'},
			logsUserGroup.Record 
			);
		// declaro el store del grid
		logsUserGroup.dataStore = new Ext.data.Store({  
			id: 'id_log',  
			proxy: logsUserGroup.dataProxy,  
			baseParams: {
				accion	:'listLogs'
			},
			remoteSort:true,
			reader: logsUserGroup.gridReader  
		});
		// declaro la barra para el pagino y los filtros
		logsUserGroup.pager = new Ext.PagingToolbar({  
			store: logsUserGroup.dataStore, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Records',  
			emptyMsg: 'No records are available',  
			pageSize: 100  
		});
		
		
		
		// declaro las columnas del grid
		logsUserGroup.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
			{  
				header: 'Id log',  
				dataIndex: 'id_log', 
				sortable: true,  
				width: 60  
			},{  
				header: 'Client',
				sortable: true, 
				dataIndex: 'usr_client',  
				width: 250  
			},{  
				header: 'Group',
				sortable: true, 
				dataIndex: 'group',  
				width: 250  
			},{  
				header: 'Action',  
				dataIndex: 'action', 
				sortable: true,  
				width: 350  
			},{  
				header: 'Apply',  
				sortable: true, 
				dataIndex: 'usr_program',  
				width: 250
			},{  
				header: 'Authorized',
				sortable: true, 
				dataIndex: 'usr_admin',  
				width: 250  
			},{
				header: 'Date',  
				dataIndex: 'date', 
				sortable: true,  
				width: 100,  
				renderer: Ext.util.Format.dateRenderer('Y/m/d   H:i:s')  
			}
			]  
		);
		// declaro el grid
		logsUserGroup.gridStatistics = new Ext.grid.GridPanel({  
			id: 'list_report',
			store: logsUserGroup.dataStore,  
		    bbar: logsUserGroup.pager,   
			cm: logsUserGroup.columnMode,
				viewConfig: {
					forceFit:true
				},
			enableColLock:false,
			//autoHeight : true,
			loadMask: true,
			border :false,
			layout	: 'fit',
			height	: 430,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
			/*
			listeners: {
				'rowcontextmenu' : logsUserGroup.menuText,
				'rowdblclick' : logsUserGroup.showDeatils
			}*/
		});
		// cargo el store
		logsUserGroup.dataStore.load();
		
		// declaro el toolbar
		logsUserGroup.toolbar=new Ext.form.FormPanel({
        frame : true,
		border :false,
        items : [{
					xtype: 'fieldset',
					style:{
						background : '#FFF'
						},
					title: 'Search',
					layout:'column',
					items: [{
							layout: 'form',
							columnWidth: .33,
							items:[
							{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsers
								}),
								hiddenName: 'client', 
								valueField: 'userid',
								displayField:'name',
								fieldLabel:	'Client',
								allowBlank:false,
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								 style: {
									fontSize: '16px'
								},
								width:160,
								listWidth:300
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[
							{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsersAdmin
								}),
								hiddenName: 'applied', 
								valueField: 'userid',
								displayField:'name',
								fieldLabel:	'Applied',
								allowBlank:false,
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								 style: {
									fontSize: '16px'
								},
								width:160,
								listWidth:300
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsersAdmin
								}),
								hiddenName: 'authorized', 
								valueField: 'userid',
								displayField:'name',
								fieldLabel:	'Authorized',
								allowBlank:false,
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								 style: {
									fontSize: '16px'
								},
								width:160,
								listWidth:300
							}]
						},{ 
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel : 'Min Date',  
								xtype	: 'datefield',  
								name 	: 'mindate',
								format	: 'Y-m-d',
								width 	: 160
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel : 'Max Date',  
								xtype	: 'datefield',  
								format	: 'Y-m-d',
								name 	: 'maxdate', 
								width 	: 160 
							}]
						}/*,
						{
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel	: 'Show user',
								xtype	: 'radiogroup',
								//name	: 'mostrar',
								width	: 250,
								columns	: 3, //muestra los radiobuttons en dos columnas  
								 items: [  
									  {boxLabel: 'All', width: 50, name: 'mostrar', inputValue: 'all', checked: true},  
									  {boxLabel: 'Registered', name: 'mostrar', inputValue: 'registrados'},  
									  {boxLabel: 'Other', name: 'mostrar', inputValue: 'otros'}
								 ]
							}]
						}*/]
		}],
		buttons:[{
				xtype 	: 'button',
				text	: 'filter',
				handler	: logsUserGroup.filter
			},
			{
				xtype 	: 'button',
				text	: 'Reset',
				handler	: logsUserGroup.reset
			}]
		})
		logsUserGroup.panel=new Ext.Panel({
			items:[logsUserGroup.toolbar,logsUserGroup.gridStatistics]
			});
		// declaro la vista donde se va a mostrar el grid
		logsUserGroup.pag = new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: logsUserGroup.panel,
					autoHeight: true
			}]
		});
		
		logsUserGroup.pager.on('beforechange',function(bar,params){  
				params.client=logsUserGroup.toolbar.getForm().findField("client").getValue();
				params.applied=logsUserGroup.toolbar.getForm().findField("applied").getValue();
				params.authorized=logsUserGroup.toolbar.getForm().findField("authorized").getValue();
				params.maxdate=logsUserGroup.toolbar.getForm().findField("maxdate").getValue();
				params.mindate=logsUserGroup.toolbar.getForm().findField("mindate").getValue();
		}); 
	},
	// declaro el record para los datos q se van a tomar de la consulta
	Record : new Ext.data.Record.create([ 
			{name: 'id_log', type: 'int'},
			{name: 'action', type: 'string'},    
			{name: 'ejecutor', type: 'int'},
			{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'mando', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'user_group' , type: 'string'},
			{name: 'usr_client' , type: 'string'},
			{name: 'usr_program' , type: 'string'},
			{name: 'usr_admin' , type: 'string'},
			{name: 'group', type :'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesLogUserGroup.php',   // Servicio web  
			method: 'POST'                          // Método de envío  
	}),
	// filtrar las conexiones
	filter: function ()
	{
		logsUserGroup.dataStore.load({
			params:{
				client:logsUserGroup.toolbar.getForm().findField("client").getValue(),
				applied:logsUserGroup.toolbar.getForm().findField("applied").getValue(),
				authorized:logsUserGroup.toolbar.getForm().findField("authorized").getValue(),
				maxdate:logsUserGroup.toolbar.getForm().findField("maxdate").getValue(),
				mindate:logsUserGroup.toolbar.getForm().findField("mindate").getValue(),
				start:0,
				limit:100
			}
		});
	},
	reset:function ()
	{
		logsUserGroup.toolbar.getForm().reset();
		logsUserGroup.dataStore.load({
			params:{
				start:0,
				limit:100
			}
		});

	}
	,
	// menu emergente
	menuText: function (grid, index, event) {
      event.stopEvent();
      var record = grid.getStore().getAt(index);
      var menu = new Ext.menu.Menu({
            items: [{
                text: 'Show History Session',
                handler: function() {
					var detailWin=
					{
						title 	: 'History for the Session: '+record.get('id_registerexternal'),
						userid	: record.get('userid'),
						county	: record.get('County')
					}
					var config=
					{
						session:record.get('id_registerexternal'),
						listFull:true
					}
					logsUserGroup.viewDetail(config,detailWin) 
                }
            },{
                text: 'Show History User',
                handler: function() {
					if(record.get('userid'))
					{
						var detailWin=
						{
							title 	: 'History for the user: '+record.get('userid'),
							userid	: record.get('userid'),
							county	: record.get('County')
						}
						var config=
						{
							userid:record.get('userid'),
							listFull:true
						}
						logsUserGroup.viewDetail(config,detailWin) 
					}
					else
					{
						Ext.Msg.alert('Error', 'There is no user assigned to this connection');
					}
                }
            },{
                text: 'Show History IP Address',
                handler: function() {
					var detailWin=
						{
							title 	: 'History for the ip: '+record.get('ip_address'),
							userid	: record.get('userid'),
							county	: record.get('County')
						}
					var config=
					{
						ip:record.get('ip_address'),
						listFull:true
					}
					logsUserGroup.viewDetail(config,detailWin) 
                }            
			}]
        }).showAt(event.xy);
	},
	// menu emergente
	showDeatils: function (grid,index, event) {
     	var record = grid.getStore().getAt(index);
		var detailWin=
			{
				title 	: 'History for the Session: '+record.get('id_registerexternal'),
				userid	: record.get('userid'),
				county	: record.get('County')
			}
		var config=
			{
				session	:record.get('id_registerexternal'),
				listFull:true
			}
		logsUserGroup.viewDetail(config,detailWin)  
	},
	
/////////////
//	FIN DEL OBJETO
////////////
}



Ext.onReady(logsUserGroup.init);
	
	
	
	