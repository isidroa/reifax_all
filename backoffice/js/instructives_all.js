/*Empieza nueva ventana para Gestionar*/
 function viewHtmlToManage(instructive_id) //Ver ventana para gestionar
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id,
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                var readdd = variable.istatus;
                var aprueba = variable.aprueba;
                var edita = variable.edita;
                if(appro == 1)
                {
                    var aprobar = true;
                }
                if(readdd ==1)
                {
                    var readonlys = true;
                }
                var showMsg = true;
                if(readdd==1)
                {
                    var showMsg = false;
                }
                var mostrarCombo = true;
                if(aprueba==1)
                {
                    var mostrarCombo = false;
                }
                var btnEdit= [{
                        text: '<b>Save </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        disabled: aprobar,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }]
             //       }
                   // else
                   /* {
                        var btnEdit = [{
                        text: '<b>It was approved</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }
                */
                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [
                        {
                            xtype:'textfield',
                            name:'mensaje',
                            fieldLabel:'Message',
                            value: 'Instructive approved, it can not be modified',
                            width:600,
                            hidden: showMsg,
                            readOnly:true

                        },
                        {//
                            xtype       :'combo',
                            id          :'istatus',
                            name        :'istatus',
                            hiddenName  :'istatus',
                            fieldLabel  :'Status',
                            allowBlank  :true,
                            width       :200,
                            store       :new Ext.data.SimpleStore({
                                            fields: ['id', 'type'],
                                            data : [['1', 'Approved'], ['0', 'Not Approved']]
                                        }),
                            displayField:'type',
                            valueField  :'id',
                            mode        :'local',
                            triggerAction:'all',
                            emptyText   :'Select..',
                            selectOnFocus:true,
                            value:variable.istatus,
                            disabled: aprobar,
                            hidden: mostrarCombo
                          },//
                    {
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'title',
                        id: 'title',
                        width:690,
                        value: variable.title,
                        allowBlank: false,
                        readOnly: readonlys

                    },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Group',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'instructives_type_id'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                }),
                    displayField:'instructives_type_id',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    readOnly: readonlys,
                    value:variable.instructive_type,//valueField:'iditg',
                   // listeners: {'select': getComboID}
                    listeners: {
                           select: {
                               fn:function(combo, value) {
                        var idcombo = Ext.getCmp('instructives_type_id').getValue();
                            var comboCity = Ext.getCmp('id_topics');
                                comboCity.setReadOnly(false);
                                comboCity.setDisabled(false);
                                comboCity.clearValue();
                                comboCity.store.filter('instructives_type_id', idcombo);

                                   
                              }
                           }
                        }
                  },

                  {
                           xtype       :'combo',
                           name        :'Select Topic',
                           fieldLabel  :'Select Topic',
                           width       :610,
                           hiddenName  :'iditg',
                           displayField:'topics',
                           valueField  :'iditg',
                           value:variable.topics,  //variable.instructive_type,//valueField:'iditg',
                           allowBlank  :false,
                           readOnly: true,
                           id:'id_topics',
                           store: new Ext.data.JsonStore({
                                autoLoad:true,
                                url:'php/grid_data.php?tipo=topics_group',
                                remoteSort: false,
                                idProperty: 'id',
                                root: 'results',
                                totalProperty: 'total',
                                fields: ['iditg','instructives_type_id','topics']
                            }),
                            triggerAction:'all',
                            mode:'local',
                           lastQuery:'',
                           
                    },
                    {
                            xtype:'hidden',
                            id: 'idtinsa',
                            name: 'idtinsa',
                            value: "Ext.getCmp('id_topics').getValue()",
                            hiddenName: 'idtinsa',
                        },


                        /*
                        {       xtype :'button',
                                text: 'Add',
                                  cls: 'x-btn-text-icon',
                                icon: 'images/add.png',
                                width: 40,
                               // handler: Adduser // Abre Ventana Registrar Instructivo
                        }
                        

                    ]
                },
                    /*{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administration'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    value:variable.instructive_type_id,
                    readOnly: readonlys

                  },*/
                  {
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false,
                        readOnly: readonlys
                  },
                  {
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 300,
                        width:750,
                        value: variable.description,
                        allowBlank: false,
                        //readOnly: readonlys
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
                });


                var w = new Ext.Window({
                    title: 'Manage Instructive',
                    width: 800,
                    height: 550,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

/*
 if(record.data.istatus !='N')
        {
            return '';
        }
        else
        {
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToEdit({0})" ><img src="images/editar.png" border="0" title="Edit Instructive" alt="Edit Instructive"></a>',record.data.instructive_id);

        }
        */
/*Termina nueva ventana para Gestionar*/

function renderViewtoManage(val, p, record){
        var creador = record.data.creator_userid;
        var sesionid= record.data.user_session;
        var approver_userid = record.data.approver_userid;
        //return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',record.data.instructive_id);
        //return 'C: '+creador+'SID'+sesionid;
        if(creador ==sesionid) 
        { 
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
        }
        if(approver_userid ==sesionid) 
        { 
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
        }

        if(record.data.istatus == 0)
        {
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
        }
        if(record.data.aprueba == 1)
        {
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
        }
    }

function previewHtmlEmail(idtmail)
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'previewinstructions',
                idtmail: idtmail
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Titulo',
                        name: 'titulo',
                        id: 'titulo',
                        width:690,
                        value: variable.titulo,
                        allowBlank: false
                    },
{
            xtype: 'itemselector',
            name: 'asignadon',
            hiddenName  : 'asignado',
            fieldLabel: 'Assigned to',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: new Ext.data.SimpleStore({
                fields: ['value', 'text'],
                data : Ext.combos_selec.dataUsersProgramm,

            }),
                displayField: 'text',
                valueField: 'value',
                value: variable.todo
            },{
                width: 250,
                height: 300,
                 store: [["",""]],
                 valueField: 'value',
                 displayField: 'text',

                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField('asignadon').reset();
                    }
                }]
            }]
        }
/*{
            xtype: 'multiselect',
            fieldLabel: 'Assigned to',
            name: 'asignadon',
            hiddenName  : 'asignado',
            width: 250,
            height: 300,
            allowBlank:false,
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            value: variable.to
        },{
            xtype       : 'combo',
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            fieldLabel  : 'Assigned to',
            name        : 'asignadon',
            hiddenName  : 'asignado',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            },
            value: variable.to
    }*/,
                    {
                        xtype: 'htmleditor',
                        id: 'descripcion',
                        id: 'descripcion',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.descripcion,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [{
                        text: '<b>Update Instructions</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php?typeHelp'+typeHelp,
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructions',
                                        idtmail: idtmail
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    },'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    },{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }]
                });


                var w = new Ext.Window({
                    title: 'Update Instructions',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

    function viewHtmlReadInstructive(instructive_id)
    {

        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var leido= variable.date_readed;
                var estatus = variable.istatus;
                var date_readed = variable.date_readed;
               // alert(estatus);
                if(date_readed == null){ 
                var prueba= [{
                        text: '<b> I Read</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/check.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true; 
                           // if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_read',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });

                        }
                    }];
                }
                else
                {
                    var prueba= [{
                        text: '<b>I Read</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/check.png',
                        disabled: true
                    }];    
                }

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'titulo',
                        id: 'titulo',
                        width:690,
                        value: variable.title,
                        allowBlank: false,
                        readOnly: true
                    },{
                        xtype: 'htmleditor',
                        id: 'descripcion',
                        id: 'descripcion',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.description,
                        allowBlank: false,
                        readOnly: true
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    prueba,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Read instructions',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////

var storeUser= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
            });
///////////Cargas de data dinamica///////////////
 var storeMenu2= new Ext.data.JsonStore({
                    url:'php/grid_data_instructives.php?tipo=group_list',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idgroup','group_name']
                });

var storeTypes = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_type',
        fields: [
            {name: 'instructive_type_id', type: 'int'}
            ,'instructive_type'
        ]
    });

var storeGroups = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_group',
        fields: [
            {name: 'iditg', type: 'int'}
            ,{name: 'instructives_type_id', type: 'int'}
            ,'topics'
        ]
    }); 
    var store = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=instructives',
        fields: [
            {name: 'instructive_id', type: 'int'}
            ,{name: 'instructive_type', type: 'int'}
            ,'cabecera'
            ,'title'
            ,'description'
            ,'usercreator'
            ,'creation_date'
            ,'date_readed'
            ,'istatus'
            ,'edita'
            ,'user_session'
            ,'creator_userid'
            ,'approver_userid'
            ,'aprueba'
        ]
    });
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[     {
                        iconCls:'icon',
                        cls: 'x-btn-text-icon',
                        icon: 'images/add.png',
                        id: 'add_button',
                        tooltip: 'Click to Add Instructive',
                        handler: AddWindow // Abre Ventana Registrar Instructivo
                    },     
                    {
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Group'
                    },{
                        xtype:'combo',
                        name:'asic',
                        id:'asic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['instructive_type_id', 'name'],
                            data : Ext.combos_selec.dataUserGroups
                        }),
                        valueField: 'instructive_type_id',
                        displayField:'name',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        },
                        value: '0',
                        listeners: {'select': logstatistic}

                    },
                    {
                        xtype:'label',
                        name: 'lbltopic',
                        text: 'Topic'
                    },
                    {
                        xtype:'combo',
                        name:'id_topic',
                        id:'id_topic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['id_topic', 'name'],
                            data : Ext.combos_selec.dataUserTopics
                        }),
                        valueField: 'id_topic',
                        displayField:'name',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        },
                        value: '0',
                        listeners: {'select': filterTopic}

                    }

        ]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////

    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idtins;
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }
    function logstatistic()
    {
        store.setBaseParam('idtype',Ext.getCmp("asic").getValue());    
        //store.setBaseParam('iduser',Ext.getCmp("asic").getValue());
        store.load({params:{start:0, limit:100}});
    }

    function filterTopic()
    {
        store.setBaseParam('idtopic',Ext.getCmp("id_topic").getValue());    
        //store.setBaseParam('iduser',Ext.getCmp("asic").getValue());
        store.load({params:{start:0, limit:100}});
    }

    function doDel(){
        Ext.MessageBox.confirm('Delete Instructions','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();
                    if(!obtenidos){
                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    //alert(obtenidos);//return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');

                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_del.php',
                        method: 'POST',
                        params: {
                            tipo : 'instructions',
                            idtmail: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }

    function AddWindow()
    {
    var usersProgrammers = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.dataUsersProgramm,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }
        });
    var Topics = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.dataTopics,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }
        });
///
//
// Data Stores 
//
var storeTypes = new Ext.data.JsonStore({
        //autoLoad: true,
        totalProperty: 'num',
        root: 'data',
        url: 'php/grid_data.php?tipo=topics_type',
        fields: [
            {name: 'instructive_type_id', type: 'int'}
            ,'instructive_type'
        ]
    });

var storeGroups = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_group',
        fields: [
            {name: 'iditg', type: 'int'}
            ,{name: 'instructives_type_id', type: 'int'}
            ,'topics'
        ]
    });



    function getComboID(combo,record,index)
    {
        var ValorSeleccionado = Ext.getCmp('instructives_type_id').getValue(); 
        if(ValorSeleccionado == 6)
        {
            Ext.getCmp('users_single').show();
        }
        else
        {
            Ext.getCmp('users_single').hide();
        }
        
         Ext.getCmp('topics').show();  
           Ext.getCmp('topics').enable();
          // storeMenu2.load({params:{instructives_type_id:ValorSeleccionado}});
           //alert(ValorSeleccionado);
           
    }//TERMINA GETCOMBOID

var btnEdit= [{
                        text: '<b>Save </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        width: 100,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }];
        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                xtype:'textfield',
                fieldLabel: 'Title',
                name: 'title',
                id: 'title',
                width:640,
                allowBlank: false
            },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Group',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'instructives_type_id'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                }),
                    displayField:'instructives_type_id',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                   // listeners: {'select': getComboID}
                    listeners: {
                           select: {
                               fn:function(combo, value) {
                        var idcombo = Ext.getCmp('instructives_type_id').getValue();
                        if(idcombo == 6)
                        {
                            Ext.getCmp('users_single').show();
                        }
                        else
                        {
                            Ext.getCmp('users_single').hide();
                        }
                        /*  
                       var comboCity = Ext.getCmp('combo-city');
                       //set and disable cities
                       comboCity.setDisabled(true);
                       comboCity.setValue('');
                       comboCity.store.removeAll();
                       //reload city store and enable city combobox
                       comboCity.store.reload({
                           params: { stateId: combo.getValue() }
                       });
                       comboCity.setDisabled(false);
                  }
               }
                        */

                                var comboCity = Ext.getCmp('id_topics');
                                comboCity.setDisabled(false);
                                comboCity.clearValue();
                                comboCity.store.filter('instructives_type_id', idcombo);

                                   
                              }
                           }
                        }
                  },
                  {
                    xtype: 'compositefield',
                    fieldLabel: 'Topic',
                    msgTarget : 'side',
                    anchor    : '-20',
                    defaults: {
                        flex: 1
                    },
                    items: [
                        {
                           xtype       :'combo',
                           name        :'Topic',
                           fieldLabel  :'Topic',
                           width       :580,
                           hiddenName  :'iditg',
                           displayField:'topics',
                            allowBlank  :false,
                           valueField:'iditg',
                           disabled:true,
                           id:'id_topics',
                           store: new Ext.data.JsonStore({
                                autoLoad:true,
                                url:'php/grid_data.php?tipo=topics_group',
                                remoteSort: false,
                                idProperty: 'id',
                                root: 'results',
                                totalProperty: 'total',
                                fields: ['iditg','instructives_type_id','topics']
                            }),
                            triggerAction:'all',
                            mode:'local',
                           lastQuery:''
                        },
                        {       xtype :'button',
                                text: 'Add',
                                  cls: 'x-btn-text-icon',
                                icon: 'images/add.png',
                                width: 40,
                                handler: Adduser // Abre Ventana Registrar Instructivo
                        }
                       
                        

                    ]
                },
                {
                text: 'View',
                                handler: function(){
                                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                                    form.getForm("asignadon").getValues(true));

                                }
                },
                {
                 xtype: 'itemselector',
                    id   :'users_single',
                    name: 'users_single',
                    hiddenName  : 'users_single',
                    fieldLabel: 'Select users',
                    hidden: true,

                    imagePath: 'includes/ext/examples/ux/images/',
                    multiselects: [{
                        width: 250,
                        height: 200,
                        store: usersProgrammers,
                        displayField: 'text',
                        valueField: 'value'
                    },{
                        width: 250,
                        height: 200,
                        store: [['','']],
                        displayField: 'text',
                        valueField: 'value',
                        tbar:[{
                            text: 'clear',
                            handler:function(){
                                form.getForm().findField().reset();
                            }
                        }]
                    }] 
            },
            
            {
                xtype: 'htmleditor',
                id: 'description',
                id: 'description',
                hideLabel: true,
                height: 250,
                width:700,
                allowBlank: false
            }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Create Instructive</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php',
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'instructive_add'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                store.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
        });


        var w = new Ext.Window({
            title: 'New Instructive',
            width: 750,
            height: 650,
            layout: 'fit',
            modal: true,
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();

        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()

    //Inicio Add user

    function Adduser()
    {
        var formula = new Ext.FormPanel({
            url:'php/grid_data_instructives.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [
                   {
                        xtype:'combo',
                        store:storeMenu2,
                        fieldLabel:'Group',
                        id:'idgroup',
                        name:'idgroup',
                        hiddenName: 'idgroup',
                        valueField: 'idgroup',
                        displayField: 'group_name',
                        triggerAction: 'all',
                        emptyText:'Select a group',
                        allowBlank: false,
                        width:200
                        //listeners: {'select': showmenu}
                    },
                    {
                        xtype:'textfield',
                        fieldLabel: 'Topic name',
                        name: 'topics',
                        id: 'topics',
                        width:450,
                        allowBlank: false
                    }
                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formula.getForm().isValid())
                    {
                        formula.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'groups_topics_add'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                //storeUsersapro.reload();
                                var storeww = Ext.getCmp("id_topics").getStore();
                                    storeww.reload();
                                var comboTopics = Ext.getCmp('id_topics');
                                    comboTopics.setDisabled(true);
                                    comboTopics.setValue('');

                                    //comboGrupos.setDisabled();        


/*
 var comboCity = Ext.getCmp('combo-city');
                       //set and disable cities
                       comboCity.setDisabled(true);
                       comboCity.setValue('');
                       comboCity.store.removeAll();
                       //reload city store and enable city combobox
                       comboCity.store.reload({
                           params: { stateId: combo.getValue() }
                       });
                       comboCity.setDisabled(false);
*/                                        

                                obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New topic',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 650,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formula
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formula.getForm().reset();
        });
        
    }
//Termina add user   
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
    function renderfemail(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlEmail({0})" ><img src="images/editar.png" border="0" title="Preview email" alt="Preview email"></a>',record.data.idtins);
    }

    function renderReadInstructive(val, p, record){
        var creador = record.data.creator_userid;
        var sesionid= record.data.user_session;
        var approver_userid = record.data.approver_userid;
        //return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',record.data.instructive_id);
        //return 'C: '+creador+'SID'+sesionid;
        if(creador ==sesionid) { return '';}
        if(approver_userid ==sesionid) { return '';}

        return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',record.data.instructive_id);

        /*if(record.data.date_readed !='Unread')
        {
            //return '';
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',record.data.instructive_id);
        }
        else
        {
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',record.data.instructive_id);
        }*/
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    var myView2 = new Ext.grid.GridView();
    myView2.getRowClass = function(record, index, rowParams, store) {
        var creador = record.data.creator_userid;
        var sesionid= record.data.user_session;
        var approver_userid = record.data.approver_userid;
        if(approver_userid ==sesionid) 
         {
         // return '';
             if(record.data['date_readed'] == 'Unread')return '';
             record.data['date_readed'] == '';
         }
         if(creador ==sesionid) 
         {
         // return '';
             if(record.data['date_readed'] == 'Unread')return '';
         }
         if(record.data['date_readed'] == 'Unread')return 'rowRed';
         /*else
         { 
            if(record.data['date_readed'] == 'Unread')return 'rowRed';
         }
         if(approver_userid ==sesionid) { return '';}
         if(record.data['date_readed'] == 'Unread')return 'rowRed';
         if(record.data['istatus'] == 0)return 'rowGrey';*/
    };

    var grid = new Ext.grid.EditorGridPanel({
        title:wherepage,
        view: myView2,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            ,{header: 'ID', width: 30, align: 'left', sortable: true, dataIndex: 'instructive_id'}
            ,{header: 'Group / Topic', width: 140, sortable: true, align: 'left', dataIndex: 'cabecera'}
            ,{header: 'Titulo', width: 160, sortable: true, align: 'left', dataIndex: 'title'}
            ,{header: 'Body', width: 320, sortable: true, align: 'left', dataIndex: 'description'}
            ,{header: 'Created by', width: 150, sortable: true, align: 'left', dataIndex: 'usercreator'}
            ,{header: 'Creation Date', width: 130, sortable: true, align: 'left', dataIndex: 'creation_date'}
           // ,{header: 'Asigned', width: 150, align: 'left', sortable: true, dataIndex: 'userasigned'}
            ,{header: 'Date Read', width: 130, align: 'left', sortable: true, dataIndex: 'date_readed'}
            ,{header: 'Read ', width: 40, sortable: true, align: 'center', dataIndex: 'instructive_id', renderer: renderReadInstructive}
            ,{header: 'Manage', width: 50, sortable: true, align: 'center', dataIndex: 'instructive_id', renderer: renderViewtoManage}
        ],
        clicksToEdit:2,
        height:470,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },{
            region:'center',
            autoHeight: true,
            items: grid
        }]
    });
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
    store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
