function nuevoAjax() 
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=cobrosrealizados',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=cobrosrealizados', timeout: 3600000 }),
		reader: new Ext.data.JsonReader(),				
		baseParams  :{start:0, limit:200},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status' 
			,'paydated'
			,'blacklist'
			,'idfrec'
			,'cardholdername'
			,'cardnumber'
			,'fecha'
			,'transactionid'
			,'paypalid'
			,'amount'
			,'amountsinformato'
			,'priceprod'
			,'cobrador'
			,'source'
			,'notes'
			,'cantpagos'
			,{name: 'veceslog', type: 'int'}
		]
	});

///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	/*var panelpag = new Ext.form.FormPanel({
		width: 700,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			fieldLabel: 'Range Date',
			items:		
			[{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				width:95,
				xtype:'label',
				id: 'total',
				text: 'Total Amount: 0.00'
			},{
				width:80,
				xtype:'label',
				id: 'cobpay',
				text: 'T. PAYPAL: 0.00'			
			  
			},{
				width:65,
				xtype:'label',
				id: 'cobauth',
				text: 'T. AUTHORIZE: 0.00'
			}]
		}]
	});*/

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 20000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			'-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
			}
			,'-' 
			//,panelpag
			,{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				width:95,
				xtype:'label',
				id: 'total',
				text: 'Total Amount: 0.00'
			},{
				width:80,
				xtype:'label',
				id: 'cobpay',
				text: 'T. PAYPAL: 0.00'			
			  
			},{
				width:65,
				xtype:'label',
				id: 'cobauth',
				text: 'T. AUTHORIZE: 0.00'
			}
		]
    });

	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");		
		ajax.send("parametro=cobros&dayfrom="+Ext.getCmp('dayfrom').getValue().format('Y-m-d')+"&dayto="+Ext.getCmp('dayto').getValue().format('Y-m-d'));
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}	
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		//alert(obtenidos)
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}
	function ReversePay(){
		Ext.Msg.confirm('Reverse Pay','Please confirm reverse pay?', function(btn){
			if (btn == 'yes'){
				var selec = grid.selModel.getSelections();
				var i=0;
				var obtenidos='';		
				for(i=0; i<selec.length; i++){
					if(i>0) obtenidos+=',';
					obtenidos+=selec[i].json.paypalid;
				}
				//alert(obtenidos)
				if(!obtenidos || obtenidos=='')
				{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar el reverso del pago');
					return;
				}
				//alert('aqui'+obtenidos)
				Ext.Ajax.request( 
				{  
					waitMsg: 'Loading...',
					url: 'php/grid_edit.php', 
					method: 'POST', 
					timeout: 100000,
					params: { 
						obtenidos : obtenidos,
						tipo:'reversepays'
					},
					failure:function(response,options){
						Ext.MessageBox.alert('Warning','Operacion fallida');
					},
					success:function(response){ 
						var rest = Ext.util.JSON.decode(response.responseText);
						if(rest.success==false)
							Ext.MessageBox.alert('Warning',rest.msg);						
						else
						{
							Ext.MessageBox.alert('Success',rest.msg);
							store.load();
						}
					}
				})
			}
		});		
	}
	
	
	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.load();
	}
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var porcion = '';
		var acum=0;
		var paycob=0;
		var authcob=0;		
		for(i=0;i<store.getCount();i++)
		{
			porcion = totales[i].data.source.substring(0, 6);
			acum = acum + parseFloat(totales[i].data.amountsinformato);
			//alert(totales[i].data.amountsinformato+" * "+acum+" * "+totales[i].data.priceprod);
			if (porcion == 'PAYPAL')
			{
			    paycob = paycob + parseFloat(totales[i].data.amountsinformato);
			} else if (porcion == 'AUTHOR')
			{

			    authcob = authcob + parseFloat(totales[i].data.amountsinformato);
			}			
		}
		Ext.getCmp('total').setText('Total: '+(acum).toFixed(2));
		Ext.getCmp('cobpay').setText('Paypal: '+(paycob).toFixed(2));
		Ext.getCmp('cobauth').setText('Authorize: '+(authcob).toFixed(2)); 		
	}

	
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		var paypalid =oGrid_Event.record.data.paypalid;
		var campo=oGrid_Event.field;
		//alert(fieldValue+paypalid+campo)
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_edit.php', 
			method: 'POST', 
			timeout: 100000,
			params: { 
				info : fieldValue,
				paypalid : paypalid,
				campo: campo, 
				tipo:'modtransidpaypal'
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Operacion fallida');
			},
			success:function(response){ 
				var rest = Ext.util.JSON.decode(response.responseText);
				if(rest.success==false)
					Ext.MessageBox.alert('Warning',rest.msg);						
				else
				{
					Ext.MessageBox.alert('Success',rest.msg);
					store.load();
				}
			}
		})		
	}; 	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){//onclick="mostrarDetalleUsuers({0},\'customerservices\')"
	
		var cad='<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a> ';
		
		if(record.data.cantpagos<=1)cad=cad+'<font color=red >New!! </font>';
        return String.format(cad,value);
    }		
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}		
	}
    function changeColor(val, p, record){
        if(record.data.blacklist == 'Y')
		{
            return '<span style="color:red;font-weight: bold;">' + val + '</span>';
        }
         if(record.data.idfrec == 2)
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
       return val;
    }
	
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
  var myView = new Ext.grid.GridView();
   myView.getRowClass = function(record, index, rowParams, store) {
		
        if(record.data['blacklist'] == 'Y')return 'rowRed';
        if(record.data['idfrec'] == 2)return 'rowGreen';
		
   };
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		view: myView,
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{header: "User ID", width: 80, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 80, sortable: true, align: 'left', dataIndex: 'name',renderer: changeColor}
			,{header: "Surname", width: 80, align: 'left', sortable: true, dataIndex: 'surname',renderer: changeColor}			
			,{header: "Status", width: 60, align: 'left', sortable: true, dataIndex: 'status'}
			,{header: 'Paydate', width: 70, sortable: true, align: 'center', dataIndex: 'paydated'}
			//,{header: 'Price', width: 50, sortable: true, align: 'right', dataIndex: 'priceprod'}
			,{header: 'Last 30 logs', width: 80, sortable: true, align: 'center', tooltip: 'Last 30 days logs', dataIndex: 'veceslog'}
			,{header: 'Holder Name', width: 120, sortable: true, align: 'left', dataIndex: 'cardholdername'}
			//,{header: 'Card Number', width: 130, sortable: true, align: 'left', dataIndex: 'cardnumber'}
			,{header: 'Date Trans.', width: 115, sortable: true, align: 'center', dataIndex: 'fecha'}
			,{header: 'Transaction ID', width: 125, sortable: true, align: 'left', dataIndex: 'transactionid',editor: new Ext.form.TextField({allowBlank: false})}
			,{header: 'Amount', width: 60, sortable: true, align: 'right', dataIndex: 'amount'}
			,{header: 'Source', width: 160, sortable: true, align: 'left', dataIndex: 'source'}
			,{header: 'For', width: 110, sortable: true, align: 'left', dataIndex: 'cobrador'}
		],			
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Cobros Realizados',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{
					xtype: 'compositefield',
					fieldLabel: '<b>Opciones</b>',
					items:[/*{
						width:120,
						id:'historico',
						xtype:'button',
						text:'Historico',
						handler: showHistorico
					},*/{
						width:120,
						id:'reverse',
						xtype:'button',
						text:'Reverse Pay',
						handler: ReversePay
					},{
						width:120,
						xtype:'checkbox',
						boxLabel: '<font color="red">&nbsp;<b>.</b></font>',
						name: 'chpaypal'
					}]
				}, 
				grid ]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
