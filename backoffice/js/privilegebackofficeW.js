/*function editParents(idw)
{

	Ext.Ajax.request({
		waitMsg: 'Saving changes...',
		url: 'php/grid_data_yanW.php',
		method: 'POST',
		params: {
			idw: idw,
			tipo: 'getworkshop'
		},

		failure:function(response,options){
			Ext.MessageBox.alert('Warning','Error editing');
			store.rejectChanges();
		},

		success:function(response,options){

			var rest = Ext.util.JSON.decode(response.responseText);

			var formul = new Ext.FormPanel({
				url:'php/grid_edit_yan.php',
				frame:true,
				layout: 'form',
				border:false,
				items: [{
					xtype: 'textfield',
					id: 'title',
					name: 'title',
					fieldLabel: '<span style="color:#F00">*</span> Title',
					value: rest.title,
					allowBlank:false,
					width: 250
				},{
					xtype:'textfield',
					id: 'speaker',
					name: 'speaker',
					fieldLabel: '<span style="color:#F00">*</span> Speaker',
					value: rest.speaker,
					allowBlank:false,
					width: 250
				},{
					xtype:'numberfield',
					id: 'price',
					name: 'price',
					fieldLabel: '<span style="color:#F00">*</span> Price',
					allowBlank:false,
					value: rest.pricews,
					allowDecimals : true,
					width: 100
				},{
					xtype: 'datefield',
					id: 'when',
					name: 'when',
					fieldLabel: '<span style="color:#F00">*</span> When',
					allowBlank:false,
					width: 250,
					format: 'Y-m-d',
					editable: false,
					value: rest.date
				},{
					xtype:'timefield',
					fieldLabel: '<span style="color:#F00">*</span> Hour',
					id: 'hour',
					name: 'hour',
					width: 100,
					allowBlank:false,
					editable :false,
					increment: 15,
					format: 'H:i',
					value: rest.hour,
					minValue: '8:00',
					maxValue: '18:00'
				},{
					xtype:'textfield',
					id: 'whennote',
					name: 'whennote',
					fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> When Note',
					allowBlank:true,
					width: 250
				},{
					xtype:'textarea',
					id: 'linkmap',
					name: 'linkmap',
					fieldLabel: '<span style="color:#F00">*</span> Link Map',
					value: rest.linkmap,
					allowBlank:false,
					width: 250,
					height: 60
				},{
					xtype:'textarea',
					id: 'address',
					name: 'address',
					fieldLabel: '<span style="color:#F00">*</span> Address',
					allowBlank:false,
					value: rest.address,
					width: 250,
					height: 60
				},{
					xtype:'textfield',
					id: 'city',
					name: 'city',
					fieldLabel: '<span style="color:#F00">*</span> City',
					value: rest.city,
					allowBlank:false,
					width: 250
				},{
					width: 150,
					fieldLabel:'<span style="color:#F00">*</span> State',
					name:'cbState',
					id:'cbState',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'text'],
						data : Ext.combos_selec.storeState
					}),
					editable :false,
					mode: 'local',
					valueField: 'id',
					displayField: 'text',
					hiddenName: 'ocState',
					triggerAction: 'all',
					selectOnFocus: true,
					allowBlank: false,
					value: 'FL'
				},{
					xtype:'numberfield',
					id: 'zipcode',
					name: 'zipcode',
					fieldLabel: '<span style="color:#F00">*</span> Zipcode',
					allowBlank:false,
					value: rest.zipcode,
					maxLength: 5, // for validation
					width: 100
				},{
					xtype:'textfield',
					id: 'phone',
					name: 'phone',
					fieldLabel: '<span style="color:#F00">*</span> Phone',
					allowBlank:false,
					value: rest.phone,
					width: 250
				},{
					width: 150,
					fieldLabel:'<span style="color:#F00">*</span> Active',
					name:'cbActive',
					id:'cbActive',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'text'],
						data : [
							['1', 'Active'],
							['0', 'No Active']
						]
					}),
					editable :false,
					mode: 'local',
					valueField: 'id',
					displayField: 'text',
					hiddenName: 'ocActive',
					triggerAction: 'all',
					selectOnFocus: true,
					allowBlank: false,
					value: rest.status,
					listeners: {
						'select': function()
						{
							if(Ext.getCmp("cbActive").getValue()==1){
								Ext.MessageBox.show({
									title: 'Warning',
									msg: 'If you activate this workshop, this is the one that will appear on the webpage',
									buttons: Ext.MessageBox.OK,
									icon:Ext.MessageBox.ERROR
								});
								return;
							}
							return;
						}
					}
				},{
					width: 150,
					fieldLabel:'<span style="color:#F00">*</span> Close',
					name:'cbclose',
					id:'cbclose',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['id', 'text'],
						data : [
							['1', 'YES'],
							['0', 'NO']
						]
					}),
					editable :false,
					mode: 'local',
					valueField: 'id',
					displayField: 'text',
					hiddenName: 'occlose',
					triggerAction: 'all',
					selectOnFocus: true,
					allowBlank: false,
					value: rest.close
				}],
				buttons: [{
					text: 'Cancel',
					cls: 'x-btn-text-icon',
					icon: 'images/cross.gif',
					handler  : function(){
						wind.close();
				   }
				},{
					text: 'Save',
					cls: 'x-btn-text-icon',
					icon: 'images/disk.png',
					formBind: true,
					handler  : function(){
						if(formul.getForm().isValid())
						{
							formul.getForm().submit({
								method: 'POST',
								params: {
									tipo : 'manageworkshop',
									idw: idw
								},
								waitTitle: 'Please wait..',
								waitMsg: 'Sending data...',
								success: function(form, action) {
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Success", obj.msg);
									wind.close();
									location.href='manageWorkshop.php';
								},
								failure: function(form, action) {
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure", obj.msg);
								}
							});
						}
					}
				}]
			});
			var wind = new Ext.Window({
					title: 'Edit Workshop',
					iconCls: 'x-icon-templates',
					layout      : 'fit',
					width       : 430,
					height      : 520,
					resizable   : false,
					modal	 	: true,
					plain       : true,
					items		: formul
				});

			wind.show();
			wind.addListener("beforeshow",function(wind){
					formul.getForm().reset();
			});

		}
	});
}//fin function editworkshop()
///////////////INICIO DE ADD CHILDS /////////////////////////
function viewnotes2(val, p, record){
	//alert("ID"+record.data.userid+"Name "+record.data.fname+" LastName"+ record.data.sname+" Email"+record.data.email);
	//if(record.data.edit=='N') return '';
	return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editChild(\''+record.data.userid+'\',\''+record.data.fname+'\',\''+record.data.sname+'\',\''+record.data.email+'\',\''+record.data.phone+'\')"><img src="images/editar.png" border="0" ></a>';
	//return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editParents('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
}
function OperationChild(val, p, record){
	if (record.data.idstatus=='Assistant2Suspended'){
		return '<a href="javascript:void(0)" class="itemusers" title="Active Assistant." onclick="OperationChilds(\''+record.data.userid+'\',\'A\')"><img src="images/adagree15.png" border="0" ></a>';
	}else{
		return '<a href="javascript:void(0)" class="itemusers" title="Suspended Child." onclick="OperationChilds(\''+record.data.userid+'\',\'S\')"><img src="images/adno-15.png" border="0" ></a>';
	}
}
function editChild(id,fname,sname,email,phone){
	$("#txtname").val(fname);
	$("#txtsurname").val(sname);
	$("#txtemail").val(email);
	$("#txtphone").val(phone);
	$("#txttipo").val("Editar");
	$("#txtuserid").val(id);
}

function OperationChilds(idc,tipo){
	if (tipo=='A'){
		var stipo='activechild';
		var msg  ='Active';
	}else{
		var stipo='suspendedchild';
		var msg  ='Suspended';
	}
	var confirmar = false;
	Ext.Msg.confirm(msg,'Please confirm '+msg+'?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request(
			{
				waitMsg: 'Saving changes...',
				url: 'php/grid_data_yanW.php?tipo='+stipo,
				method: 'POST',
				params: {
					idc: idc
				},

				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanelch").getStore();
					store1.rejectChanges();
					var store2 = Ext.getCmp("gridpanel").getStore();
					store2.rejectChanges();

				},

				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);

					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);

					var store1 = Ext.getCmp("gridpanelch").getStore();
					store1.commitChanges();
					store1.reload();

					var store2 = Ext.getCmp("gridpanel").getStore();
					store2.commitChanges();
					store2.reload();

				}
			 }
		);
		}
	});
}



    function execToggleCheck(node, isCheck){
      if(node) {
       //node.expand();
       node.cascade(function(){
         if (this.attributes.cls=="file") {
           this.ui.toggleCheck(isCheck);
           this.attributes.checked=isCheck;
         }
       });
      }
     }

	function editChilds(idp)
	{

		Ext.Ajax.request({
			waitMsg: 'Saving changes...',
			url: 'php/grid_data_yanW.php',
			method: 'POST',
			params: {
				idw: idp,
				tipo:'getUser'
			},

			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},

			success:function(response,options){
				var storech = new Ext.data.JsonStore({
					totalProperty: 'total',
					root: 'results',
					url: 'php/grid_data_yanW.php?tipo=allchilds&idp='+idp,
					fields: [
						{name: 'userid', type: 'int'}
						,'fname'
						,'sname'
						,'email'
						,'idstatus'
					]
				});

                var treeprivilege = new Ext.tree.TreePanel({
                    //renderTo:'tree-div',
                    //title: 'Privileges',
                    id: 'menubko',
                    useArrows:true,
                    autoScroll:true,
                    animate:true,
                    enableDD:true,
                    containerScroll: true,
                    rootVisible: false,
                    frame: true,
                    root: {
                        nodeType: 'async',
                        text: 'Privilege',
                        expanded:true
                    },
                    dataUrl: 'php/grid_data_yanW.php?tipo=getPrivilegeUsers&id2='+idp, //consulto usuario
                });

                treeprivilege.on({
                    'checkchange': {
                        fn: function(node,checked) {
                            var n = node.attributes;
                            if(n.cls == 'folder')
                            {
                                if (checked)
                                {
									var ti= "todo";
                                    node.expand();
 									var msg = '', selNodes = treeprivilege.getChecked();
                            		Ext.each(selNodes, function(node){
                                	console.log(node.text);
                                	if(msg.length > 0){
                                    	msg += ', ';
                                	}
                                	msg += node.id;
									$.get("php/grid_data_yanW.php?tipo=modifPrivilegeUsers",{iduser: idp, datos: msg, tip: ti });

                            });
                                }
                                else
                                {
									var ti= "todo";
                                    node.collapse();
									var msg = '', selNodes = treeprivilege.getChecked();
                            		Ext.each(selNodes, function(node){
                                	console.log(node.text);
	                                if(msg.length > 0){
	                                    msg += ', ';
	                                }
                                	msg += node.id;
									$.get("php/grid_data_yanW.php?tipo=modifPrivilegeUsers",{iduser: idp, datos: msg, tip: ti });
		                            });
                                }
                                execToggleCheck(node,checked);
                            }
 							if(n.cls == 'file')
 							{
                                if (checked)
                                {
                                	var ti= "uno";
									$.get("php/grid_data_yanW.php?tipo=modifPrivilegeUsers",{iduser: idp, datos: n.id, tip: ti });
                               	}
                               	else
                               	{
									var ti= "no";	
									$.get("php/grid_data_yanW.php?tipo=modifPrivilegeUsers",{iduser: idp, datos: n.id, tip: ti });
	                            }

                            }
                        },
                        scope: this
                    }
                });

				var parents = Ext.util.JSON.decode(response.responseText);
				var windch = new Ext.Window({
						title: parents.typeuser+': '+ parents.fullname+' ('+parents.iduser+')',
						iconCls: 'x-icon-templates',
						layout      : 'fit',
						width       : 650,
						height      : 520,
						resizable   : false,
						modal	 	: true,
						plain       : true,
						items		: treeprivilege
                });

				windch.show();

				windch.addListener("beforeshow",function(windch){
				    treeprivilege.getRootNode().expand();
						//formul.getForm().reset();
                        //treeprivilege.getRootNode().expand(true);
				});

				//storech.load({params:{start:0, limit:100}});
				//store.load({params:{start:0, limit:100}});

			}
		});
	}
//////////////FIN ADD CHILDS ///////////////////////////////
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request(
			{
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php',
				method: 'POST',
				params: {
					cont: idcont
				},

				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},

				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);

					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);

					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		);
		}
	});
}

Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

/*
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data_yanW.php?tipo=privilegeUsers',
		fields: [
			{name: 'iduser', type: 'int'}
         //{name: 'idworkshop', type: 'int'}
			,'names'
			,'email'
			,'assistants'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
*/
////////////////barra de pagineo//////////////////////
	/*var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',
			icon: 'images/add.png',
			id: 'add_button',
			text: 'Add',
			tooltip: 'Click to Add Workshop',
			handler: AddWindow,


        }]
   });*/


////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	/*function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.iduser;
         //marcados+=selec[i].json.idworkshop;
			//alert(marcados)
		}
		marcados+=')';
		if(i==0)marcados=false;
		return marcados;
	}


	function copyworkshop(){
		var com;
		var j=0;
		var obtenidos=obtenerSeleccionados();
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');

		//alert(obtenidos);return;
		if(obtenidos.search(",")>0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it');
			return;
		}
		Ext.Ajax.request({
			waitMsg: 'Saving changes...',
			url: 'php/grid_add_yan.php',
			method: 'POST',
			params: {
				idw: obtenidos,
				tipo: 'copyworkshop'

			},

			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},

			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);

				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);

				store.commitChanges();
				store.reload();
			}
		});

	}

	//Inicio Funcion AddWindows
	function AddWindow()
	{
		var formul = new Ext.FormPanel({
			url:'php/grid_add_yan.php',
			frame:true,
			layout: 'form',
			border:false,
			items: {
            xtype:'tabpanel',
            plain:true,
            activeTab: 0,
            height:235,
            defaults:{bodyStyle:'padding:10px'},
            items:[{
               title: 'Customer Information',
               items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [{
							xtype:'hidden',
							id: 'txtstatus',
							name: 'txtstatus',
							value: 'Assistant1Active'
						},	{
                  	xtype:'textfield',
                  	fieldLabel: 'First Name',
                  	id: 'txtname',
                  	name: 'txtname',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'Last Name',
                  	id: 'txtsurname',
                  	name: 'txtsurname',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'Phone',
                  	id: 'txtphone',
                  	name: 'txtphone',
                  	width: 200,
                  	allowBlank: true
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'Email Address',
                  	id: 'txtemail',
                  	name: 'txtemail',
                  	vtype:'email',
                  	width: 200,
                  	allowBlank: false
                  }]
                  //Assistant1Active
               }]
            },{
                title: 'Billing Address',
                items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [{
                  	xtype:'textfield',
                  	fieldLabel: 'Address',
                  	id: 'txtbaddress',
                  	name: 'txtbaddress',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'City',
                  	id: 'txtbcity',
                  	name: 'txtbcity',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'State',
                  	id: 'txtbstate',
                  	name: 'txtbstate',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'numberfield',
                  	fieldLabel: 'Zip Code',
                  	id: 'txtbzip',
                  	name: 'txtbzip',
                  	width: 200,
                  	allowBlank: false
                  }]
               }]
            },{
                title: 'Mailing Address',
                items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items: [
                  {
                     xtype:'checkbox',
                     id:'sameAddres',
                     name: 'sameAddres',
                     inputValue: 'Administrator',
                     boxLabel: 'Check if mailing address is the same as billing address ',
                     handler : function(){
                        if($('input[name=sameAddres]').is(':checked')){
                           $("#txtmaddress").val($("#txtbaddress").val());
                           $("#txtmcity").val($("#txtbcity").val());
                           $("#txtmstate").val($("#txtbstate").val());
                           $("#txtmzip").val($("#txtbzip").val());
                        }else{
                           $("#txtmaddress").val('');
                           $("#txtmcity").val('');
                           $("#txtmstate").val('');
                           $("#txtmzip").val('');
                        }
                     }
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'Address',
                  	id: 'txtmaddress',
                  	name: 'txtmaddress',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'City',
                  	id: 'txtmcity',
                  	name: 'txtmcity',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'textfield',
                  	fieldLabel: 'State',
                  	id: 'txtmstate',
                  	name: 'txtmstate',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype:'numberfield',
                  	fieldLabel: 'Zip Code',
                  	id: 'txtmzip',
                  	name: 'txtmzip',
                  	width: 200,
                  	allowBlank: false
                  }]
               }]
            },{
                title: 'Credit Card Information',
                items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [{
                  	xtype:'textfield',
                  	fieldLabel: 'Full Name',
                  	id: 'txtnamecard',
                  	name: 'txtnamecard',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype		:'combo',
                  	id			:'typecard',
                  	name		:'typecard',
                  	hiddenName	:'cexecutive',
                  	fieldLabel	:'Type',
                  	allowBlank	:false,
                  	width		:200,
                  	store		:new Ext.data.SimpleStore({
                  					fields: ['id', 'username'],
                  					data : [['MasterCard', 'MasterCard'], ['American Express', 'American Express'],['Visa','Visa'],['Discover','Discover']]
                  				}),
                  	displayField:'username',
                  	valueField	:'id',
                  	mode		:'local',
                  	triggerAction:'all',
                  	emptyText	:'Select..',
                  	selectOnFocus:true

                  },{
                  	xtype:'numberfield',
                  	fieldLabel: 'Number',
                  	id: 'txtcardnumber',
                  	name: 'txtcardnumber',
                  	width: 200,
                  	allowBlank: false
                  },{
                  	xtype: 'compositefield',
                     fieldLabel: 'Expire Date',
                     width: 200,
                     items:
                     [{
                        xtype: 'combo',
                        hiddenName: 'txtmonthcard',
                        emptyText: 'Month',
                        hideLabel: true,
                        width: 100,
                        mode: 'local',

                        store: new Ext.data.ArrayStore({
                          id: 0,
                          fields: ['id','name'],
                          data: [[1, 'January'], [2, 'February'], [3, 'March'], [4, 'April'], [5, 'May'], [6, 'June'], [7, 'July'], [8, 'August'],[9, 'September'], [10, 'October'], [11, 'November'], [12, 'December']],
                        }),
                        valueField: 'id',
                        displayField: 'name',
                        allowBlank: false,
                        forceSelection: true
                     },{
                        xtype: 'numberfield',
                        name: 'txtyearcard',
                        emptyText: 'Year',
                        hideLabel: true,
                        width: 70,
                        minValue: new Date().getFullYear(),
                        allowBlank: false

                     }]
                  },{
                  	xtype:'numberfield',
                    inputType: 'password',
                  	fieldLabel: 'Secure Code',
                  	id: 'txtsecurecode',
                  	name: 'txtsecurecode',
                  	width: 200,
                  	allowBlank: false
                  }]
               }]
            }]
            },
			buttons: [{
				text: 'Cancel',
				cls: 'x-btn-text-icon',
				icon: 'images/cross.gif',
				handler  : function(){
					wind.close();
			   }
			},{
				text: 'Save',
				cls: 'x-btn-text-icon',
				icon: 'images/disk.png',
            formBind: true,
				handler  : function(){
					if(formul.getForm().isValid())
					{
						formul.getForm().submit({
							method: 'POST',
							params: {
								tipo : 'manageParents'
							},
							waitTitle: 'Please wait..',
	                  waitMsg: 'Sending data...',
							success: function(form, action) {
								store.reload();
                        obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Success", obj.msg);
								wind.close();
							},
							failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.msg);
							}
						});
					}
				}
			}]
		});
		var wind = new Ext.Window({
				title: 'New Parents',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 500,
				height      : 300,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul
			});

		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}
	//fin function doAdd()

	function searchActive(){
		if(Ext.getCmp("cbActive").getValue()==1){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'If you activate this workshop, this is the one that will appear on the webpage',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});
			return;
		}
		return;
	}
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
        return String.format('<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }

	function viewchild(val, p, record){
		if(record.data.edit=='N') return '';
		return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the Clilds." onclick="editChilds('+record.data.iduser+')"><img src="images/editar.png" border="0" ></a>';
		//return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editParents('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }

	function viewnstatus(val, p, record){
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
	function viewlink(val, p, record){

		return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
	}
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
/*	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Privilege Menu Users',
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [
			new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: viewchild}
         ,{header: 'ID', width: 40, sortable: true, align: 'center', dataIndex: 'iduser',renderer: renderTopic}
			,{header: 'Name', width: 180, sortable: true, align: 'left', dataIndex: 'names'}
			,{header: 'Email', width: 170, align: 'left', sortable: true, dataIndex: 'email'}
         	//,{header: 'Afiliados', width: 150, align: 'left', sortable: true, dataIndex: 'assistants'}
		],
		clicksToEdit:2,
		height:470,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar
	});*/
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
/*	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
*/
Ext.onReady(function(){
var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',
			icon: 'images/add.png',
			id: 'add_button',
			text: 'Add',
			tooltip: 'Click to Add Workshop',
			//handler: AddWindow,


        }]
   });
    Ext.QuickTips.init();
 var xg = Ext.grid;

function viewnotes2(val, p, record){
    //alert("ID"+record.data.userid+"Name "+record.data.fname+" LastName"+ record.data.sname+" Email"+record.data.email);
    //if(record.data.edit=='N') return '';
    return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editChild(\'1\')"> <input type="checkbox" name="vehicle" value="Bike"></a>';
    //return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editParents('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
}


    // shared reader
    var reader = new Ext.data.ArrayReader({}, [
       {name: 'company'},
       {name: 'price', type: 'float'},
       {name: 'change', type: 'float'},
       {name: 'pctChange', type: 'float'},
       {name: 'lastChange', type: 'date', dateFormat: 'n/j h:ia'},
       {name: 'industry'},
       {name: 'desc'}
    ]);

    var store = new Ext.data.GroupingStore({
            reader: reader,
            data: xg.dummyData,
           // dataUrl: 'php/grid_data_yanW.php?tipo=getPrivilegeUsers&id2=', //consulto usuario
            sortInfo:{field: 'company', direction: "ASC"},
            groupField:'industry',
            //autoLoad: true
            //php/grid_data_yanW.php?tipo=getPrivilegeUsers&id2='+idp
        });

    var grid = new xg.GridPanel({
        store: store,
        columns: [
            {id:'company',header: "Modulo", width: 60, sortable: true, dataIndex: 'company'},
          //  {header: "Price", width: 20, sortable: true, renderer: Ext.util.Format.usMoney, dataIndex: 'price'},
            {header: "Status", width: 20, sortable: true, dataIndex: 'change', renderer: viewnotes2},
            {header: "Modulo", width: 20, sortable: true, dataIndex: 'industry'},
           // {header: "Last Updated", width: 20, sortable: true, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'lastChange'}
        ],

        view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
        }),

        frame:true,
        width: 800,
        height: 800,
        collapsible: true,
        animCollapse: false,
        title: 'User Privilege',
        iconCls: 'icon-grid',
        tbar: pagingBar,
       /* fbar  : ['->', {
            text:'Clear Grouping',
            iconCls: 'icon-clear-group',
            handler : function(){
                store.clearGrouping();
            }
        }],*/
        renderTo: document.body
    });
});

var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data_yanW.php?tipo=privilegeUsers',
		fields: [
			{name: 'iduser', type: 'int'}
         //{name: 'idworkshop', type: 'int'}
			,'names'
			,'email'
			,'assistants'
		]
	});


// Array data for the grids/*

Ext.grid.dummyData = [
    ['3m Co',71.72,0.02,0.03,'4/2 12:00am', 'Users'],
    ['Alcoa Inc',29.01,0.42,1.47,'4/1 12:00am', 'Users'],
    ['Altria Group Inc',83.81,0.28,0.34,'4/3 12:00am', 'Users'],
    ['American Express Company',52.55,0.01,0.02,'4/8 12:00am', 'Finance'],
    ['American International Group, Inc.',64.13,0.31,0.49,'4/1 12:00am', 'Services'],
    ['AT&T Inc.',31.61,-0.48,-1.54,'4/8 12:00am', 'Services'],
    ['Boeing Co.',75.43,0.53,0.71,'4/8 12:00am', 'Users'],
    ['Caterpillar Inc.',67.27,0.92,1.39,'4/1 12:00am', 'Services'],
    ['Citigroup, Inc.',49.37,0.02,0.04,'4/4 12:00am', 'Finance'],
    ['E.I. du Pont de Nemours and Company',40.48,0.51,1.28,'4/1 12:00am', 'Users'],
    ['Exxon Mobil Corp',68.1,-0.43,-0.64,'4/3 12:00am', 'Users'],
    ['General Electric Company',34.14,-0.08,-0.23,'4/3 12:00am', 'Users'],
    ['General Motors Corporation',30.27,1.09,3.74,'4/3 12:00am', 'Automotive'],
    ['Hewlett-Packard Co.',36.53,-0.03,-0.08,'4/3 12:00am', 'Computer'],
    ['Honeywell Intl Inc',38.77,0.05,0.13,'4/3 12:00am', 'Users'],
    ['Intel Corporation',19.88,0.31,1.58,'4/2 12:00am', 'Computer'],
    ['International Business Machines',81.41,0.44,0.54,'4/1 12:00am', 'Computer'],
    ['Johnson & Johnson',64.72,0.06,0.09,'4/2 12:00am', 'Medical'],
    ['JP Morgan & Chase & Co',45.73,0.07,0.15,'4/2 12:00am', 'Finance'],
    ['McDonald\'s Corporation',36.76,0.86,2.40,'4/2 12:00am', 'Food'],
    ['Merck & Co., Inc.',40.96,0.41,1.01,'4/2 12:00am', 'Medical'],
    ['Microsoft Corporation',25.84,0.14,0.54,'4/2 12:00am', 'Computer'],
    ['Pfizer Inc',27.96,0.4,1.45,'4/8 12:00am', 'Services', 'Medical'],
    ['The Coca-Cola Company',45.07,0.26,0.58,'4/1 12:00am', 'Food'],
    ['The Home Depot, Inc.',34.64,0.35,1.02,'4/8 12:00am', 'Retail'],
    ['The Procter & Gamble Company',61.91,0.01,0.02,'4/1 12:00am', 'Users'],
    ['United Technologies Corporation',63.26,0.55,0.88,'4/1 12:00am', 'Computer'],
    ['Verizon Communications',35.57,0.39,1.11,'4/3 12:00am', 'Services'],
    ['Wal-Mart Stores, Inc.',45.45,0.73,1.63,'4/3 12:00am', 'Retail'],
    ['Walt Disney Company (The) (Holding Company)',29.89,0.24,0.81,'4/1 12:00am', 'Services']
];
console.debug(Ext.grid.dummyDatad);
// add in some dummy descriptions
/*
for(var i = 0; i < Ext.grid.dummyData.length; i++){
    Ext.grid.dummyData[i].push('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed metus nibh, sodales a, porta at, vulputate eget, dui. Pellentesque ut nisl. Maecenas tortor turpis, interdum non, sodales non, iaculis ac, lacus. Vestibulum auctor, tortor quis iaculis malesuada, libero lectus bibendum purus, sit amet tincidunt quam turpis vel lacus. In pellentesque nisl non sem. Suspendisse nunc sem, pretium eget, cursus a, fringilla vel, urna.<br/><br/>Aliquam commodo ullamcorper erat. Nullam vel justo in neque porttitor laoreet. Aenean lacus dui, consequat eu, adipiscing eget, nonummy non, nisi. Morbi nunc est, dignissim non, ornare sed, luctus eu, massa. Vivamus eget quam. Vivamus tincidunt diam nec urna. Curabitur velit.');
}*/
