Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	var loading_win=new Ext.Window({
		 width:170,
		 autoHeight: true,
		 resizable: false,
		 modal: true,
		 border:false,
		 closable:false,
		 plain: true,
		 html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="images/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
	});
function sendregister()
{
	var txtamountpay = Ext.getCmp('txtamountpay').getValue();
	var txtfreedays = Ext.getCmp('txtfreedays').getValue();
	var txtname = Ext.getCmp('txtname').getValue();
	var txtsurname = Ext.getCmp('txtsurname').getValue();
	var txtemail = Ext.getCmp('txtemail').getValue();
	var txtpass = Ext.getCmp('txtpass').getValue();
	var typeuser = Ext.getCmp('mypanel').getForm().getValues()['typeuser'];
	var procode=Ext.getCmp('executive').getValue();
	if(typeuser=='' || typeuser==undefined){
		alert("Please select the 'user type' to be added.");
		return;
	}
		//alert(typeuser);
	
	if(typeuser=='free' )
		//location.href="http://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=0&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
		location.href="https://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=0&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
	else if( typeuser=='active' )
		location.href="https://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=1&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
		//location.href="http://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=1&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
	else if(typeuser=='trial')
	{
		if(txtfreedays=='' || txtfreedays==0){
			alert('Please enter free days');
			return false;
		}
		/*if(txtamountpay=='' || txtamountpay==0){
			alert('Please enter amount pay');
			return false;
		}*/
		location.href="https://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=2&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
		//location.href="http://www.reifax.com/register/customerRegister.php?rbko=1&freeactive=2&bkou="+bkou+"&freedays="+txtfreedays+"&txtamountpay="+txtamountpay+"&procode="+procode;
	}
	else if(typeuser=='Programmer' || typeuser=='Administrator' || typeuser=='CustomerServices')
	{
		loading_win.show();
		if(txtname==''){
			alert('Please enter a name');
			return false;
		}else if(txtsurname==''){
			alert('Please enter a surname');
			return false;
		}else if(txtemail==''){
			alert('Please enter a email');
			return false;
		}else if(txtpass==''){
			alert('Please enter a password');
			return false;
		}
		if(Ext.getCmp('mypanel').getForm().isValid()){
			Ext.Ajax.request({
				url: 'php/saveUser.php',
				method: 'POST', 
				timeout: 100000,
				params: { 
					txtname : txtname,
					txtsurname:txtsurname,
					txtemail: txtemail,
					txtpass: txtpass,
					usertype: typeuser
				},
				failure:function(response,options){
					loading_win.hide();
					Ext.MessageBox.alert('Error','User not registered');
				},
				success: function(response){
					var variable = Ext.decode(response.responseText);
					loading_win.hide();
					mypanel.getForm().reset();
					Ext.MessageBox.alert('',variable.mensaje);
				}
			
			});
		}else{
			alert('Error in the form');
		}
		
	}
	
}

//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        id: 'mypanel',
		title: 'New Users',
		url:'php/grid_edit2.php',
        bodyStyle:'padding:5px 5px 0',
        width: 450,
        items: [{
            xtype: 'fieldset',
            title: 'Free/Active Users',
            autoHeight: true,
            items: [{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'free',
				fieldLabel: 'Register?',
                boxLabel: 'Free',				
			},{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'active',
				fieldLabel: '',
                boxLabel: 'Active',				
			}]
        },{
            xtype: 'fieldset',
            title: 'Trials Users',
            autoHeight: true,
            items: [{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'trial',
				allowDecimals: false,
				allowNegative: false, 
				fieldLabel: 'Register?',
                boxLabel: 'Trial'
			},{
				xtype:'numberfield',
				fieldLabel: 'Free Days',
				allowDecimals: true,
				allowNegative: false, 
				id: 'txtfreedays',
				name: 'txtfreedays',
				width: 100
			},{
				xtype:'numberfield',
				fieldLabel: 'Amount Pay',
				allowDecimals: true,
				allowNegative: false, 
				id: 'txtamountpay',
				name: 'txtamountpay',
				width: 100
			},{
				xtype		:'combo',
				id			:'executive',
				name		:'executive',
				hiddenName	:'cexecutive',
				fieldLabel	:'Executive',
				allowBlank	:true,
				width		:150,
				store		:new Ext.data.SimpleStore({
								fields: ['id', 'username'],
								data : Ext.combos_selec.comboUsers
							}),
				//editable	:false,
				displayField:'username',
				valueField	:'id',
				mode		:'local',
				triggerAction:'all',
				emptyText	:'Select..',
				selectOnFocus:true
			}]
        },{
            xtype: 'fieldset',
            title: 'Administrator/Customer/Programmer/Salesman Users',
            autoHeight: true,
			collapsible: true,
			collapsed: true, // fieldset initially collapsed			
            items: [{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'Administrator',
				fieldLabel: 'Register?',
                boxLabel: 'Administrator'		
			},{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'CustomerServices',
				fieldLabel: '',
                boxLabel: 'Customer Services'			
			},{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'Programmer',
				fieldLabel: '',
                boxLabel: 'Programmer'			
			},{
				xtype:'radio',
                name: 'typeuser',
                inputValue: 'Salesman',
				fieldLabel: '',
                boxLabel: 'Salesman'			
			},{
				xtype:'textfield',
				fieldLabel: 'Name',
				id: 'txtname',
				name: 'txtname',
				width: 200
			},{
				xtype:'textfield',
				fieldLabel: 'Surname',
				id: 'txtsurname',
				name: 'txtsurname',
				width: 200
			},{
				xtype:'textfield',
				fieldLabel: 'Email',
				id: 'txtemail',
				name: 'txtemail',
				vtype:'email',
				width: 200
			},{
				xtype:'textfield',
				fieldLabel: 'Password',
				id: 'txtpass',
				name: 'txtpass',
				width: 200
			}]
        },{
			xtype: 'hidden',
			name: 'tipo',
			hiddenName: 'tipo',
			value: 'newuser'
		}],
		buttons: [{
            text: 'Register'
			,handler  : sendregister
        },{
            text: 'Reset',
			handler: function(){
				mypanel.getForm().reset();
			}
        }]
	});

	var mypanel2 = new Ext.form.FormPanel({
        frame:true,
        id: 'mypanel2',
		title: 'New Short Sales Processor',
        bodyStyle:'padding:5px 5px 0',
        width: 450,
        items: [{
			xtype: 'hidden',
			name: 'typeuserss',
			hiddenName: 'typeuserss',
			value: 'ShortSaleProcessor'
		},{
			xtype:'textfield',
			fieldLabel: 'Name',
			id: 'txtnamess',
			name: 'txtnamess',
			width: 200,
			allowBlank: false 
		},{
			xtype:'textfield',
			fieldLabel: 'Surname',
			id: 'txtsurnamess',
			name: 'txtsurnamess',
			width: 200,
			allowBlank: false 
		},{
			xtype:'textfield',
			fieldLabel: 'Processor Name',
			id: 'txtpnamess',
			name: 'txtpnamess',
			width: 200,
			allowBlank: false 
		},{
			xtype:'textfield',
			fieldLabel: 'Email',
			id: 'txtemailss',
			name: 'txtemailss',
			vtype:'email',
			width: 200,
			allowBlank: false 
		},{
			xtype:'textfield',
			fieldLabel: 'Password',
			id: 'txtpassss',
			name: 'txtpassss',
			width: 200,
			allowBlank: false 
		},{
			xtype:'textfield',
			fieldLabel: 'Phone Number',
			id: 'txtphoness',
			name: 'txtphoness',
			width: 200,
			allowBlank: false 
		}],
		buttons: [{
			text: 'Register',
			handler  : function(){
				if(mypanel2.getForm().isValid())
				{
					mypanel2.getForm().submit({
						url: 'php/saveUser.php',
						method: 'POST', 
						timeout: 100000,
						params: {
							tipo : 'newuser'
						},								
						waitTitle: 'Please wait..',
						waitMsg: 'Sending data...',
						success: function(form, action) {
							obj = Ext.util.JSON.decode(action.response.responseText);
							Ext.Msg.alert("Success", obj.mensaje);
							mypanel2.getForm().reset();
						},
						failure: function(form, action) {
							obj = Ext.util.JSON.decode(action.response.responseText);
							Ext.Msg.alert("Failure", obj.mensaje);
						}
					});
				}
			}
		},{
            text: 'Reset',
			handler: function(){
				mypanel2.getForm().reset();
			}
        }]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
            layout:'column',
			items:[{
                columnWidth:.35,
                baseCls:'x-plain',
                bodyStyle:'padding:5px 0 5px 5px',
                items:[mypanel]
            },{
                columnWidth:.5,
                baseCls:'x-plain',
                bodyStyle:'padding:5px 0 5px 5px',
				items:[mypanel2]
			}]
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
/////////////FIN Inicializar Grid////////////////////
//Ext.MessageBox.alert('Warning','HABLAR CON SMITH PRIMERO');
});
