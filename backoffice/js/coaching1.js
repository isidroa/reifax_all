// JavaScript Document
Ext.ns('coaching');

coaching=
{
	init: function ()
	{
		coaching.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total',
			id:'readerCoaching'
			},
			coaching.record 
			);
			
		coaching.store = new Ext.data.Store({  
			id: 'coaching',  
			proxy: coaching.dataProxy,  
			baseParams: {
				accion	:'consultar'
			},  
			reader: coaching.gridReader  
		});
		
		var panelpag = new Ext.form.FormPanel({
			layout: 'table',
			id:'coachingFilter',
			padding: 3,
			defaults: {
				bodyStyle:'margin-left:5px'
			},
			layoutConfig:{
				columns	:	2
			},
			style	:{
				background:'none'
			},
			border: false,
			items: [{
				layout	: 'table',
				border: false,
				items	: [{
						xtype:'label',
						id: 'lblstatus',
						text: 'Estatus'
					},{
					xtype	: 'combo'
					,fieldLabel:'Status'
					,displayField:'name'
					,value :'in (1,2)'
					,name:'status'
					,valueField:'id'
					,store: new Ext.data.SimpleStore({
						 fields:['id', 'name']
						,data:[
							['','All'],
							['in (1,2)', 'Active'],
							['in (3)', 'Completed'],
							['in (3,4,5)', 'Inactive'],
							['in (4)', 'Canceled'],
							['in (5)', 'Expired']
						]
					})
					,width: 70
					,triggerAction:'all'
					,mode:'local'
				},{
					xtype	: 'textfield',
					name	: 'name',
					emptyText	:'Email, Name, Userid'
				}]
			},{
				layout	: 'table',
				border: false,/*
				items	: [{
						xtype:'label',
						id: 'date',
						text: ' Range Date'
					},{
						xtype	: 'combo',
						fieldLabel:'Date'
						,displayField:'name'
						,name:'type_date'
						,id:'type_date'
						,valueField:'id'
						,store: new Ext.data.SimpleStore({
							 fields:['id', 'name']
							,data:[
								['1', 'Equal'],
								['2', 'Greater Than'],
								['3', 'Less Than'],
								['4', 'Equal or Less'],
								['5', 'Equal or Greater'],
								['6', 'Between']
							]
						})
						,width: 90
						,triggerAction:'all'
						,mode:'local'
						,listeners	  : {
							select: function(combo, record, index) {
								var val=combo.getValue();
								if(val==6){
									Ext.getCmp('labelto').show(); 
									Ext.getCmp('duedate').show(); 
								}else{
									Ext.getCmp('labelto').hide(); 
									Ext.getCmp('duedate').hide(); 
								}
							}
						}
					},{
						xtype		  : 'datefield',
						width		  : 120,
						editable	  : false,
						format		  : 'Y/m/d',
						emptyText		:	'Start Date',
						name		  : 'startdate',
						id			  : 'startdate'
					},{
						width: 20,
						xtype: 'label',
						id :'labelto',
						visibility:'hidden',
						defaults: {
							visibility: 'hidden'
						},
						text: 'To'
					},{
						xtype		  : 'datefield',
						width		  : 120,
						editable	  : false,
						format		  : 'Y/m/d',
						emptyText		:	'Due Date',
						name		  : 'duedate',
						id			  : 'duedate'
					}]*/
			}]
		});
		coaching.pager = new Ext.PagingToolbar({  
			store: coaching.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 30,
			items:[ 
				'|'
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-add',
					handler	: coaching.new.init,
					text	: 'New Coaching'
				},((userid==5)?
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: coaching.delete,
					text	: 'Delete'
				}:{}),
				'|',
				{
					xtype	:'button',
					iconCls	:'x-icon-save',
					handler	: coaching.save,
					text	: 'Save Changes'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: coaching.cancel,
					text	: 'Cancel Change'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-reload',
					handler	: coaching.reset,
					text	: 'Reset Date'
				},
				{
					xtype	:'button',
					icon: 'images/excel.png',
					handler	: coaching.export_excel
				},
				'|',panelpag,
				{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						coaching.store.load({
							params:{
								status:Ext.getCmp('coachingFilter').getForm().findField("status").getValue(),
								q	:Ext.getCmp('coachingFilter').getForm().findField("name").getValue()
								//typeDate:Ext.getCmp('coachingFilter').getForm().findField("type_date").getValue(),
								/*startDate: Ext.util.Format.date(
									Ext.getCmp('coachingFilter').getForm().findField("startdate").getValue()
									,'Y-m-d'
								),
								dueDate: Ext.util.Format.date(
									Ext.getCmp('coachingFilter').getForm().findField("duedate").getValue()
									,'Y-m-d'
								)*/
							}
						})
					}
				},{
					xtype	: 'button',
					text	: 'Remove',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						Ext.getCmp('coachingFilter').getForm().reset();
						/*
						Ext.getCmp('labelto').hide(); 
						Ext.getCmp('duedate').hide(); 
						Ext.getCmp('type_date').setValue(1);*/
						coaching.store.load({
							params:{
								status:Ext.getCmp('coachingFilter').getForm().findField("status").getValue(),
								q	:Ext.getCmp('coachingFilter').getForm().findField("name").getValue()/*
								typeDate:Ext.getCmp('coachingFilter').getForm().findField("type_date").getValue(),
								startDate: Ext.util.Format.date(
									Ext.getCmp('coachingFilter').getForm().findField("startdate").getValue()
									,'Y-m-d'
								),
								dueDate: Ext.util.Format.date(
									Ext.getCmp('coachingFilter').getForm().findField("duedate").getValue()
									,'Y-m-d'
								)*/
							}
						})
					}
				},{
					xtype:'text',
					id	:'relojCaracas'
				},{
					xtype:'text',
					id	:'relojUsa'
				}
			], 
			listeners: {
				afterrender : function(){
					/*
					Ext.getCmp('labelto').hide(); 
					Ext.getCmp('duedate').hide(); 
					Ext.getCmp('type_date').setValue(1);*/
				}
			}
		});
		coaching.pager.on('beforechange',function(bar,params){
				params.status	=Ext.getCmp('coachingFilter').getForm().findField("status").getValue();
				params.q		=Ext.getCmp('coachingFilter').getForm().findField("name").getValue();
		});
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {
				forceSelection:true,
				minValue: '8:00 AM',
				maxValue: '6:00 PM',
				increment: 30});
				
		var opcionesCombo= [
        ['1', 'Active'],
        ['3', 'Completed'],
        ['5', 'Expired'],
        ['4', 'Canceled'] ];
		
		var opcionesStatus= {
			1	:	'Active',
			3	:	'Completed',
			5	:	'Expired',
			4	:	'Canceled'
		}
		
		
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		
		var storeUserAdmin= new Ext.data.SimpleStore({
					fields: ['userid', 'name'],
					data : Ext.combos_selec.dataUsers2
			});
		var comboUser = new Ext.form.ComboBox({
			store: storeUserAdmin,
			hiddenName	: 'userPro',
			valueField: 'userid',
			displayField:'name',
			 style: {
                fontSize: '16px'
            },
			typeAhead: true,
			triggerAction: 'all',
			mode: 'local',
			selectOnFocus:true
		})
		
		
		
		coaching.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
				coaching.sm,
			{  
				header: 'Id Coaching',  
				dataIndex: 'id', 
				sortable: true,  
				width: 30  
			},{  
				header: 'Userid',  
				dataIndex: 'userid', 
				sortable: true,
				width: 55,
				renderer :function (value, p, record){
					return String.format(
							'<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
				}	
			},{  
				header: 'User',  
				sortable: true, 
				dataIndex: 'usr_name'
			},{  
				header: 'Registration Date',  
				dataIndex: 'date_register', 
				sortable: true,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				width: 70
			},{  
				header: 'Days',
				sortable: true, 
				dataIndex: 'dias', 
				renderer: function(value, metaData, record, rowIndex, colIndex, store) { 
						var now		= (record.get('status')=='Active')?new Date():record.get('date_lastUpdate');
						var diferece= now - record.get('date_register');
						var dias 	= Math.floor(diferece / (1000 * 60 * 60 * 24));
						dias=dias<0?0:dias;
						if(record.get('dias')<11)
	      					metaData.attr ='style="background-color:#4BB148; color: #FFF; font-weight:bold;"'; 
						else if(record.get('dias')<21)
	      					metaData.attr ='style="background-color:#FCD209; font-weight:bold;"'; 
						else 
	      					metaData.attr ='style="background-color:#E04828; color: #FFF; font-weight:bold;"'; 
					return dias; 
   				} ,
				width: 35
			},{  
				header: 'Home Phone',
				sortable: true, 
				dataIndex: 'home'
			},{  
				header: 'Mobile Phone',
				sortable: true, 
				dataIndex: 'movile'
			},{  
				header: 'Product',
				sortable: true, 
				dataIndex: 'producto'
			},{  
				header: 'Time',
				sortable: true, 
				dataIndex: 'periodo',
				width: 55
			},{  
				header: 'Status',
				sortable: true, 
				dataIndex: 'status',
				renderer:function (val){
					if(opcionesStatus[val]){
						return opcionesStatus[val];
					}
					else{
						return val;
					}
				},
				editor	: combo
			},{ 
				header: 'Available time',  
				dataIndex: 'time_user', 
				sortable: false,
				renderer: coaching.tooltip  ,
				width: 130
			},{   
				header: 'Date',
				sortable: true, 
				dataIndex: 'date_coaching',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{   
				header: 'Hour',
				sortable: true, 
				dataIndex: 'hourCoaching',
				editor	:timeField,
				width: 65
			},{   
				header: 'Execution Date',
				sortable: true, 
				dataIndex: 'date_execute',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{  
				header: 'Mandated',
				sortable: false, 
				dataIndex: 'mandated',
				editor	: comboUser,
				width: 120,
				renderer:function(id){
					id=parseInt(id);
					if(id){
						var index = storeUserAdmin.find('userid',id);  
						if(index>-1){  
								var record = storeUserAdmin.getAt(index);
								return record.get('name');  
							}  
						return value;
						}
					else{
						return '';
					}
				}
			},{  
				header: 'Note',
				sortable: true, 
				dataIndex: 'note',
				renderer: function (value, metaData, record, rowIndex, colIndex, store){
					var id=record.get('id');
					var userid=record.get('userid');
					var usr_name=record.get('usr_name');
					return String.format('<a href="javascript:void(0)"  onclick="coaching.coachinOpenNote({1},{2},\'{3}\')" style="display: block; float: left; height: 16px; margin-right: 5px; width: 16px;" class="x-icon-add"></a>{0}',value,id,userid,usr_name);
				}
			}
			]  
		);
		coaching.pager.on('beforechange',function(bar,params){
				params.status=Ext.getCmp('coachingFilter').getForm().findField("status").getValue();
		});
		coaching.store.load({
							params:{
								status:Ext.getCmp('coachingFilter').getForm().findField("status").getValue()
							}
						});	
		coaching.grid = new Ext.grid.EditorGridPanel({
			height: 500,
            store: coaching.store,
			tbar: coaching.pager,   
			cm: coaching.columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true ,
			sm:coaching.sm,
			loadMask: true,
			border :false
        }); 
		
		coaching.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: coaching.grid,
					autoHeight: true
			}]
		});
		
	},
	sm:new Ext.grid.CheckboxSelectionModel()
	,
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'userid', type: 'int'},    
			{name: 'mandated', type: 'string'},    
			{name: 'status', type: 'string'},
			{name: 'date_register', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_coaching', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_lastUpdate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_execute', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'hourCoaching', type: 'string'},
			{name: 'usr_name', type: 'string'},
			{name: 'time_user', type: 'string'},
			{name: 'email' , type: 'string'},
			{name: 'producto' , type: 'string'},
			{name: 'home' , type: 'string'},
			{name: 'movile' , type: 'string'},
			{name: 'note' , type: 'string'},
			{name: 'dias' , type: 'int'},
			{name: 'periodo' , type: 'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesCoaching.php',   // Servicio web  
			method: 'POST'                          // M�todo de env�o  
	}),
	save	:function ()
	{
		var grid=coaching.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcionesCoaching.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
				}  
			});  
		}  
			
	},
	export_excel:function(){
		var grid=coaching.grid;
		grid.el.mask('Exporting...', 'x-mask-loading');
		
		Ext.Ajax.request({
			method :'POST',
		   	url: 'Excel/xlsCoaching.php',
		   	success: function ( result, request ){
			 	grid.el.unmask();  
				window.open('Excel/d.php?nombre='+result.responseText,'','width=50,height=50');
				return(true); 
			},
		   	failure:  function (){
			   grid.el.unmask();  
			},
		  	 params: { 
			 	parametro: 'Export_grid',
				status:Ext.getCmp('coachingFilter').getForm().findField("status").getValue(),
				typeDate:Ext.getCmp('coachingFilter').getForm().findField("type_date").getValue(),
				startDate: Ext.util.Format.date(
					Ext.getCmp('coachingFilter').getForm().findField("startdate").getValue()
					,'Y-m-d'
				),
				dueDate: Ext.util.Format.date(
					Ext.getCmp('coachingFilter').getForm().findField("duedate").getValue()
					,'Y-m-d'
				)
			}
		});
		

	},
	reset: function (){
		var cols=coaching.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].set){
				var date=cols[i].set('date_coaching','');
				var date=cols[i].set('hourCoaching','');
			}
		}
	},
	coachinOpenNote: function (id,userid,name){
		
		var grid = new Ext.grid.GridPanel({
			baseCls			: 'cellComplete',
			store: new Ext.data.Store({  
				id: 'coaching', 
				autoLoad :true,
				proxy: coaching.dataProxy,  
				baseParams: {
					accion	:'listarNotes',
					id	: id
				},  
				reader:new Ext.data.JsonReader({  
				root: 'coacing', 
				totalProperty: 'total',
				},
					 new Ext.data.Record.create([
						{name: 'id', type: 'id'},
						{name: 'note', type: 'string' },
						{name: 'date_note', type: 'date', dateFormat: 'Y-m-d'}
					])   
				)
			}),
			colModel: new Ext.grid.ColumnModel({
				columns: [{  
						header	: 'Note',  
						dataIndex	: 'note', 
						//height		:80,
						sortable	: false,
						style		: 'white-space:normal !important;',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							metaData.style = "line-height:18px;";
							var text = "<p style='cursor:pointer;'>"+value+"</p>";
							return text;
						},
						width: 357,
							
					},{  
						header	: 'Date',  
						sortable: false, 
						width	: 100,
						renderer: Ext.util.Format.dateRenderer('Y/m/d'),
						dataIndex	: 'date_note'
					}
				]
			}),
			height	: 200,
			width	: 477,
			frame	: true
		});
		var form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [
						{
							xtype	:'hidden',
							name	:'id',
							value	: id
						},{
							xtype	:'hidden',
							name	:'accion',
							value	:'recordNote'
						},{
							xtype	: 'textarea',
							emptyText: 'New Note',
							name	: 'note',
							allowBlank :false,
							hideLabel:true,
							height: 80,
							width: 430
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Close',
							handler	: function ()
							{
								btn=this;
								var form=this.findParentByType('form');
								var win=this.findParentByType('window');
								if(form.getForm().isValid())
								{
									form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new nota...',
										url: 'php/funcionesCoaching.php',
										scope : this,
										params :{
											accion : 'recordNote'
										},
										success	: function (form,action)
										{
											//coaching.store.load();
											win.close();
											win.destroy();
										}
									})
								}
								else{
									win.close();
									win.destroy();
								}
							}
						}]
				})
		var win=new Ext.Window({
			modal	:true,
			title: 'Notes of '+id, 
			closable: false,
			resizable: false,
			maximizable: false,
			plain: true,
			border: false,
			width: 490,
			height: 430,
			items	:[
				{
					xtype	:'panel',
					bodyStyle	:'font-size:14px;',
					pandding	:13,
					html	:'<strong>User:</strong> '+name+' (<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+userid+')">'+userid+'</a>)'
				},
				grid,
				form
			]
		});
		win.show();
	},
	cancel: function ()
	{
		coaching.grid.getStore().rejectChanges();
	},
	delete:function (){
		data=coaching.grid.getSelectionModel().getSelections();
		if(data.length){
			var ids='';
			for(i=0;i<data.length;i++){
				ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
			}
		}
		Ext.Ajax.request({
			method :'POST',
		   	url: 'php/funcionesCoaching.php',
		   	success: function (){
			   coaching.grid.getStore().reload();
			},
		   	failure:  function (){
			   
			},
		  	 params: { 
			 	accion: 'delete',
				ids	: ids
			}
		});
	}
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	new:{
		init:function (){
			if(coaching.new.win)
			{
				coaching.new.form.getForm().reset();
				coaching.new.win.show();
			}
			else{
				coaching.new.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsers
							}),
							hiddenName: 'client', 
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Client',
							allowBlank:false,
							//name:'client',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['val', 'text'],
								data : [['6','1 Hour'],['8','2 Hour'],['9','3 Hour'],['7','4 Hour']]
							}),
							hiddenName: 'time', 
							valueField: 'val',
							displayField:'text',
							fieldLabel:	'Time',
							allowBlank:false,
							//name:'client',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['val', 'text'],
								data : [['1','REIFax Residential'],['2','REIFax Platinum'],['3','REIFax Professional'],['7','Home Deals Finder'],['6','Buyer�s Pro'],['8','Leads Generator']]
							}),
							hiddenName: 'product', 
							valueField: 'val',
							displayField:'text',
							fieldLabel:	'Product',
							allowBlank:false,
							//name:'client',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},new Ext.form.DateField({
						fieldLabel:	'Coaching Date',
						format: 'Y-m-d',
						name	:'dateCoaching',
						 style: {
							fontSize: '16px'
						},})
					,new Ext.form.TimeField({
						fieldLabel:	'Coaching Hour',
						forceSelection:true,
						name	:'hour',
						minValue: '9:00 AM',
						maxValue: '6:00 PM',
						increment: 30,
						 style: {
							fontSize: '16px'
						},})
					,{
						xtype:'combo',
						store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsers2
						}),
						fieldLabel:	'Mandated',
						hiddenName	: 'userPro',
						valueField: 'userid',
						displayField:'name',
						 style: {
							fontSize: '16px'
						},
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(coaching.new.form.getForm().isValid())
								{
									coaching.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new coaching...',
										url: 'php/funcionesCoaching.php',
										scope : this,
										params :{
											accion : 'recordCoaching'
										},
										success	: function (form,action)
										{
											coaching.new.form.getForm().reset();
											coaching.new.win.hide();
											coaching.store.load({
																params:{
																	status:Ext.getCmp('coachingFilter').getForm().findField("status").getValue()
																}
															});	
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								coaching.new.form.getForm().reset();
							}
						}]
				})
				
				
				coaching.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New Coaching', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 360,
					height: 265,
					items	:[
						coaching.new.form
					]
				});
				coaching.new.win.show();
			}
		}
	}
}

Ext.onReady(coaching.init);
Ext.QuickTips.init()