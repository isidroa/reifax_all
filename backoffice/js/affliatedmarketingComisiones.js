function nuevoAjax() 
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=affMarketing',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=affMarketing', timeout: 3600000 }),
		reader: new Ext.data.JsonReader(),						
		baseParams  :{start:0, limit:50, paffmarkmonth:mes,	paffmarkyear:anio},
		fields: [
			{name: 'Userid', type: 'int'}
			,'Name'
			,'surname'
			,'Operation' 
			,'Date'
			,'payamount'
			,'Amount'
			,'Aff_Userid'
			,'Aff_Name'
			,'Aff_status'
			,'Posi'
			,'affiliationdate'
		]
	});
	
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.PagingToolbar({
		
		//layout: 'form',
		items: [{
				width:100,
				xtype:'label',
				id: 'empty',
				text: 'User Partner'
			},{
				xtype: 'combo',
				store: new Ext.data.SimpleStore({
					fields: ['userid', 'name'],
					data : Ext.combos_selec.dataUserscom
				}),
				id: 'cbuseridclientscom',
				name: 'cbuseridclientscom',
				hiddenName: 'cbpexecutivecom', 
				valueField: 'userid',
				displayField:'name',
				typeAhead: true,
				triggerAction: 'all',
				mode: 'local',
				selectOnFocus:true,
				value:'*',
				width: 270
				//,listeners: {'select': showamounttobepay}		
			},{
				xtype:'combo',
				width:100,
				id:'affmonth',
				name:'affmonth',
				store: new Ext.data.SimpleStore({
					fields: ['id','text'],
					data : [['01','January'],['02','February'],['03','March'],['04','April'],['05','May'],['06','June'],['07','July'],['08','August'],['09','September'],['10','October'],['11','November'],['12','December']]
				}),
				editable: false,
				valueField: 'id',
				displayField: 'text',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: mes
			},{
				xtype:'combo',
				width:60,
				id:'affyear',
				name:'affyear',
				store: new Ext.data.SimpleStore({
					fields: ['id','text'],
					data : Ext.combos_selec.dataYears 
				}),
				editable: false,
				valueField: 'id',
				displayField: 'text',
				typeAhead: true,
				mode: 'local',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: anio
			},{
				xtype: 'button',
				width:60,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchAffMarketing
			},{
				xtype: 'button',
				width:18,
				id:'exc_butt',
				pressed: true,
				enableToggle: true,
				name:'exc_butt',
				handler: ExportExcel,
				tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png'
			}/*,{
				xtype: 'button',
				width:18,
				id:'exc_butts',
				pressed: true,
				enableToggle: true,
				name:'exc_butts',
				handler: ExportExcelFull,
				tooltip: 'Export to Excel Full',
				iconCls:'icon',
				icon: 'images/historico.png'
			}*/,{
				width:135,
				xtype:'label',
				id: 'empty',
				text: ' '
			},{
				width:500,
				xtype:'label',
				id: 'total',
				//text: 'Total commission earned on the month of '+fechacompl+': $0.00',
		        //margins: '5 0 0 50',
				align: 'right'
			}
			
		]
	});

////////////////FIN barra de pagineo//////////////////////

	function ExportExcel()
	{		
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");		
		ajax.send("parametro=affMarketing&paffmarkmonth="+Ext.getCmp('affmonth').getValue()+"&paffmarkyear="+Ext.getCmp('affyear').getValue()+"&userid="+Ext.getCmp('cbuseridclientscom').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}	
	
	function ExportExcelFull()
	{		
		/*var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");		
		ajax.send("parametro=affMarketing&paffmarkmonth="+Ext.getCmp('affmonth').getValue()+"&paffmarkyear="+Ext.getCmp('affyear').getValue()+"&userid="+Ext.getCmp('cbuseridclientscom').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
		*/
	}	
//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		//alert(obtenidos)
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}
	
	
	function searchAffMarketing(){
		store.setBaseParam('paffmarkmonth',Ext.getCmp("affmonth").getValue());
		store.setBaseParam('paffmarkyear',Ext.getCmp("affyear").getValue());
		store.setBaseParam('userid',Ext.getCmp("cbuseridclientscom").getValue());
		store.load();
		//obtenerTotal();
	}
	function obtenerTotal(){
		if(Ext.getCmp("cbuseridclientscom").getValue()!='*')
		{
			var arrayRapido = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
			var totales = store.getRange(0,store.getCount());
			var i,yearc,monthc;
			var acum=0;
			var fecha = new Date();
			var year=Ext.getCmp("affyear").getValue();
			var month=Ext.getCmp("affmonth").getValue();
			for(i=0;i<store.getCount();i++)
				acum = acum + parseFloat(totales[i].data.Amount);
			var cad='';
			if(year=='')
			{monthc=arrayRapido[fecha.getMonth()-1]; yearc=fecha.getFullYear();}
			else
			{monthc=arrayRapido[month-1]; yearc=year;}
			cad=monthc+' '+yearc;
			Ext.getCmp('total').setText('( USERID: '+Ext.getCmp("cbuseridclientscom").getValue()+' ) Total commission earned on the month of '+cad+': $ '+(acum).toFixed(2));	
		}
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){//onclick="mostrarDetalleUsuers({0},\'customerservices\')"
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }		
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}	
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	
	var grid = new Ext.grid.EditorGridPanel({
		//renderTo: 'gridPanelAff',
        //title: '<?php echo $_COOKIE['datos_usr']['NAME']." ".$_COOKIE['datos_usr']['SURNAME']." (".$_COOKIE['datos_usr']['USERID'].")";?>',
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			// new Ext.grid.RowNumberer()
			{header: " ", width:50, align: 'center', sortable: true, dataIndex: 'Posi'}
			,{header: "Date", width: 80, align: 'left', sortable: true, dataIndex: 'Date'}
			,{header: "Description", width: 260, align: 'left', sortable: true, dataIndex: 'Operation'}			
			,{header: 'User ID', width: 60, sortable: true, align: 'center', dataIndex: 'Aff_Userid',renderer: renderTopic}
			,{header: 'Customer Name', width: 180, sortable: true, align: 'left', dataIndex: 'Aff_Name'}
			,{header: 'Affiliation', width: 120, sortable: true, align: 'left', dataIndex: 'affiliationdate'}
			,{header: 'Customer Payment ', width: 120, sortable: true, align: 'right', dataIndex: 'payamount',
				renderer: function(v, params, record){
					if(record.data.payamount!='')
						return Ext.util.Format.usMoney(record.data.payamount);
				}}
			,{header: 'Customer Status', width: 100, sortable: true, align: 'left', dataIndex: 'Aff_status'}
			,{header: 'Comm. Amount', width: 100, sortable: true, align: 'right', dataIndex: 'Amount',
				renderer: function(v, params, record){
					if(record.data.Amount!='')
						return Ext.util.Format.usMoney(record.data.Amount);
				}}
		],			
		height:470,
		//width: 950,
		frame:false,
		loadMask:true,
		border: false,
		tbar: panelpag 
	});
	
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Users Commissions',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [grid ]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
