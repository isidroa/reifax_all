	
	function editWorkshop(idw)
	{
	
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/grid_data.php', 
			method: 'POST',
			params: {
				idw: idw,
				tipo: 'getworkshop' 
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},
			
			success:function(response,options){
			
				var rest = Ext.util.JSON.decode(response.responseText);
				
				var formul = new Ext.FormPanel({
					url:'php/grid_edit.php',
					frame:true,
					layout: 'form',
					border:false,
					items: [{
						xtype: 'textfield',
						id: 'title',
						name: 'title',
						fieldLabel: '<span style="color:#F00">*</span> Title',
						value: rest.title,
						allowBlank:false,
						width: 250
					},{
						xtype:'textfield',
						id: 'speaker',
						name: 'speaker',
						fieldLabel: '<span style="color:#F00">*</span> Speaker',
						value: rest.speaker,
						allowBlank:false,
						width: 250
					},{
						xtype:'numberfield',
						id: 'price',
						name: 'price',
						fieldLabel: '<span style="color:#F00">*</span> Price',
						allowBlank:false,
						value: rest.pricews,
						allowDecimals : true,
						width: 100
					},{
						xtype: 'datefield',
						id: 'when',
						name: 'when',
						fieldLabel: '<span style="color:#F00">*</span> When',
						allowBlank:false,
						width: 250,
						//format: 'l, F dS Y',
						format: 'Y-m-d',
						editable: false,
						value: rest.date
					},{ 
						xtype:'timefield',
						fieldLabel: '<span style="color:#F00">*</span> Hour',
						id: 'hour',
						name: 'hour',
						width: 100,
						allowBlank:false,
						editable :false,
						increment: 15,
						format: 'H:i',
						value: rest.hour,
						minValue: '8:00',
						maxValue: '18:00'
					},{
						xtype:'textfield',
						id: 'whennote',
						name: 'whennote',
						fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> When Note',
						allowBlank:true,
						width: 250
					},{
						xtype:'textarea',
						id: 'linkmap',
						name: 'linkmap',
						fieldLabel: '<span style="color:#F00">*</span> Link Map',
						value: rest.linkmap,
						allowBlank:false,
						width: 250,
						height: 60						
					},{
						xtype:'textarea',
						id: 'address',
						name: 'address',
						fieldLabel: '<span style="color:#F00">*</span> Address',
						allowBlank:false,
						value: rest.address,
						width: 250,
						height: 60						
					},{
						xtype:'textfield',
						id: 'city',
						name: 'city',
						fieldLabel: '<span style="color:#F00">*</span> City',
						value: rest.city,
						allowBlank:false,
						width: 250
					},{
						width: 150,
						fieldLabel:'<span style="color:#F00">*</span> State',
						name:'cbState',
						id:'cbState',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['id', 'text'],
							data : Ext.combos_selec.storeState 
						}),
						editable :false,
						mode: 'local',
						valueField: 'id',
						displayField: 'text',
						hiddenName: 'ocState',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'FL'
					},{
						xtype:'numberfield',
						id: 'zipcode',
						name: 'zipcode',
						fieldLabel: '<span style="color:#F00">*</span> Zipcode',
						allowBlank:false,
						value: rest.zipcode,
						maxLength: 5, // for validation
						width: 100
					},{
						xtype:'textfield',
						id: 'phone',
						name: 'phone',
						fieldLabel: '<span style="color:#F00">*</span> Phone',
						allowBlank:false,
						value: rest.phone,
						width: 250
					},{
						width: 150,
						fieldLabel:'<span style="color:#F00">*</span> Active',
						name:'cbActive',
						id:'cbActive',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['id', 'text'],
							data : [
								['1', 'Active'],
								['0', 'No Active']
							] 
						}),
						editable :false,
						mode: 'local',
						valueField: 'id',
						displayField: 'text',
						hiddenName: 'ocActive',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: rest.status,
						listeners: {
							'select': function()
							{
								if(Ext.getCmp("cbActive").getValue()==1){
									Ext.MessageBox.show({
										title: 'Warning',
										msg: 'If you activate this workshop, this is the one that will appear on the webpage',
										buttons: Ext.MessageBox.OK,
										icon:Ext.MessageBox.ERROR
									});						
									return;			
								}		
								return;			
							}
						}
					},{
						width: 150,
						fieldLabel:'<span style="color:#F00">*</span> Close',
						name:'cbclose',
						id:'cbclose',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['id', 'text'],
							data : [
								['1', 'YES'],
								['0', 'NO']
							] 
						}),
						editable :false,
						mode: 'local',
						valueField: 'id',
						displayField: 'text',
						hiddenName: 'occlose',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: rest.close 
					}],
					buttons: [{
						text: 'Cancel',
						cls: 'x-btn-text-icon',			
						icon: 'images/cross.gif',
						handler  : function(){
							wind.close();
					   }
					},{
						text: 'Save',
						cls: 'x-btn-text-icon',			
						icon: 'images/disk.png',
						formBind: true,
						handler  : function(){
							if(formul.getForm().isValid())
							{
								formul.getForm().submit({
									method: 'POST',
									params: {
										tipo : 'manageworkshop',										
										idw: idw 	
									},								
									waitTitle: 'Please wait..',
									waitMsg: 'Sending data...',
									success: function(form, action) {
										obj = Ext.util.JSON.decode(action.response.responseText);
										Ext.Msg.alert("Success", obj.msg);
										wind.close();
										location.href='manageWorkshop.php';
									},
									failure: function(form, action) {
										obj = Ext.util.JSON.decode(action.response.responseText);
										Ext.Msg.alert("Failure", obj.msg);
									}
								});
							}
						}
					}]				
				});
				var wind = new Ext.Window({
						title: 'Edit Workshop',
						iconCls: 'x-icon-templates',
						layout      : 'fit',
						width       : 430,
						height      : 520,
						resizable   : false,
						modal	 	: true,
						plain       : true,
						items		: formul				
					});
			
				wind.show();
				wind.addListener("beforeshow",function(wind){
						formul.getForm().reset();
				});
				
			}
		}); 
	}//fin function editworkshop() 
	
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=manageWorkshop',
		fields: [
			{name: 'idworkshop', type: 'int'}
			,'title'
			,'speaker'
			,'whend' 
			,'status'			
			,'comment'	
			,'phone'	
			,'pricews'
			,'address'
			,'linkmap'
			,'edit'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'add_button',
			text: 'Add',
			tooltip: 'Click to Add Workshop',
			handler: AddWindow 
        },{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/copy.gif',
			id: 'cp_button',
			tooltip: 'Click to Copy Workshop',
			text: 'Copy',
			handler: copyworkshop 
        }]
    });
////////////////FIN barra de pagineo//////////////////////
	//////////////////Manejo de Eventos//////////////////////


	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idworkshop;
			//alert(marcados)
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}

	
	function copyworkshop(){
		var com;
		var j=0;
		var obtenidos=obtenerSeleccionados();
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		
		//alert(obtenidos);return;		
		if(obtenidos.search(",")>0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it');
			return;
		}
		//Ext.MessageBox.alert('Warning','SIIIII VA by clicking on it');			return;		
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/grid_add.php', 
			method: 'POST',
			params: {
				idw: obtenidos,
				tipo: 'copyworkshop'
				
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},
			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
					
				store.commitChanges();
				store.reload();
			}
		}); 

	}
	
	function AddWindow()
	{
		var formul = new Ext.FormPanel({
			url:'php/grid_add.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'title',
				name: 'title',
				fieldLabel: '<span style="color:#F00">*</span> Title',
				allowBlank:false,
				width: 250
			},{
				xtype:'textfield',
				id: 'speaker',
				name: 'speaker',
				fieldLabel: '<span style="color:#F00">*</span> Speaker',
				allowBlank:false,
				width: 250
			},{
				xtype:'numberfield',
				id: 'price',
				name: 'price',
				fieldLabel: '<span style="color:#F00">*</span> Price',
				allowBlank:false,
				allowDecimals : true,
				width: 100
			},{
				xtype: 'datefield',
				id: 'when',
				name: 'when',
				fieldLabel: '<span style="color:#F00">*</span> When',
				allowBlank:false,
				width: 250,
				//format: 'l, F dS Y',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{ 
               	xtype:'timefield',
				fieldLabel: '<span style="color:#F00">*</span> Hour',
                id: 'hour',
                name: 'hour',
				width: 100,
				allowBlank:false,
				editable :false,
				increment: 15,
				format: 'H:i',
                value: '10:00',
                minValue: '8:00',
                maxValue: '18:00'
            },{
				xtype:'textfield',
				id: 'whennote',
				name: 'whennote',
				fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> When Note',
				allowBlank:true,
				width: 250
			},{
				xtype:'textarea',
				id: 'linkmap',
				name: 'linkmap',
				fieldLabel: '<span style="color:#F00">*</span> Link Map',
				allowBlank:false,
				width: 250,
				height: 60						
			},{
				xtype:'textarea',
				id: 'address',
				name: 'address',
				fieldLabel: '<span style="color:#F00">*</span> Address',
				allowBlank:false,
				width: 250,
				height: 60						
			},{
				xtype:'textfield',
				id: 'city',
				name: 'city',
				fieldLabel: '<span style="color:#F00">*</span> City',
				allowBlank:false,
				width: 250
			},{
				width: 150,
				fieldLabel:'<span style="color:#F00">*</span> State',
				name:'cbState',
				id:'cbState',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'text'],
					data : Ext.combos_selec.storeState 
				}),
				editable :false,
				mode: 'local',
				valueField: 'id',
				displayField: 'text',
				hiddenName: 'ocState',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: 'FL'
			},{
				xtype:'numberfield',
				id: 'zipcode',
				name: 'zipcode',
				fieldLabel: '<span style="color:#F00">*</span> Zipcode',
				allowBlank:false,
				maxLength: 5, // for validation
				width: 100
			},{
				xtype:'textfield',
				id: 'phone',
				name: 'phone',
				fieldLabel: '<span style="color:#F00">*</span> Phone',
				allowBlank:false,
				width: 250
			},{
				width: 150,
				fieldLabel:'<span style="color:#F00">*</span> Active',
				name:'cbActive',
				id:'cbActive',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'text'],
					data : [
						['1', 'Active'],
						['0', 'No Active']
					] 
				}),
				editable :false,
				mode: 'local',
				valueField: 'id',
				displayField: 'text',
				hiddenName: 'ocActive',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				value: '0',
				listeners: {
					'select': searchActive

				} 
			}],
			buttons: [{
				text: 'Cancel',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler  : function(){
					wind.close();
			   }
			},{
				text: 'Save',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
                formBind: true,
				handler  : function(){
					if(formul.getForm().isValid())
					{
						formul.getForm().submit({
							method: 'POST',
							params: {
								tipo : 'manageworkshop'										
							},								
							waitTitle: 'Please wait..',
	                        waitMsg: 'Sending data...',
							success: function(form, action) {
								store.reload();
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Success", obj.msg);
								wind.close();
							},
							failure: function(form, action) {
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.msg);
							}
						});
					}
				}
			}]				
		});
		var wind = new Ext.Window({
				title: 'New Workshop',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 500,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function doAdd()

	function searchActive(){
		if(Ext.getCmp("cbActive").getValue()==1){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'If you activate this workshop, this is the one that will appear on the webpage',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;			
		}		
		return;			
	}
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function viewnotes(val, p, record){
		if(record.data.edit=='N') return '';
		
		return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editWorkshop('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }
	function viewnstatus(val, p, record){
		
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
	function viewlink(val, p, record){
		
		return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Manage Workshop',
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: viewnotes}
			,{header: 'IDU', width: 30, sortable: true, align: 'center', dataIndex: 'idworkshop'}
			,{header: 'Workshop', width: 170, sortable: true, align: 'left', dataIndex: 'title'}
			,{header: 'Speaker', width: 150, align: 'left', sortable: true, dataIndex: 'speaker'}
			,{header: 'Date', width: 130, align: 'left', sortable: true, dataIndex: 'whend'}
			,{header: 'Address', width: 330, sortable: true, align: 'left', dataIndex: 'address', renderer: viewlink}
			,{header: 'Phone', width: 100, align: 'left', sortable: true, dataIndex: 'phone'}
			//,{header: 'Hour', width: 80, sortable: true, align: 'left', dataIndex: 'hour'}
			,{header: 'Price', width: 70, sortable: true, align: 'right', dataIndex: 'pricews', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{header: 'Active', width: 50, sortable: true, align: 'center', dataIndex: 'status', renderer: viewnstatus}
		],
		clicksToEdit:2,
		height:470,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
 
});
