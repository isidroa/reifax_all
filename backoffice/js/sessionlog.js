Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.chart.Chart.CHART_URL = 'includes/ext/resources/charts.swf';
Ext.onReady(function(){

   Ext.QuickTips.init();

   /*
    * carga la data en el grid
    */
   var TypoPrint = 1; 
   var store = new Ext.data.JsonStore({
       totalProperty:'total',
       root         :'results',
       url          :'php/grid_data.php?tipo=sessionlogs',
       fields		:[{name: 'day', type: 'string'}
						,'total'
						,'totalD'
						,{name: 'Avr', type: 'float'}	
					]
   });
   
   var fecha=new Date();
	var diahoy=fecha.getDate();
	var mes=fecha.getMonth() +1;
	var anio=fecha.getFullYear();
	var meses=['January','February','March','April','May','June','July','August','September','October','November','December'];

   
   
   var panel = new Ext.Panel({
		id: 'panel',
		title:'Session Logs '+meses[mes-1]+' '+anio,
		iconCls      :'x-icon-settings',
		width        :'99.8%',
		frame: true,
		height: 420,
		layout: 'fit',
		items: {
			xtype: 'columnchart',
			store: store,
			xField: 'day',
			yField: 'total',
			yAxis: new Ext.chart.NumericAxis({
				title: 'V i s i t s',
				labelRotation: 90
			}),
			xAxis: new Ext.chart.CategoryAxis({
                title: 'Day'
            }),
			legend: 'Hola',
			extraStyle: {
				yAxis: {
					titleRotation: -90
				}
			}
		}
   });
   var reader = new Ext.data.JsonReader({
        successProperty	: 'success',
		messageProperty	: 'message',
		idProperty	: 'id',
		root		: 'data',
        fields: [
            {name: 'fechgrup0', type: 'int'},
            {name: 'sessiondate', type: 'string'},
            {name: 'login_ip', type: 'string'}
            
        ]
    });
	var gstore =new Ext.data.GroupingStore({
				url		: 'php/grid_data.php?tipo=historylogs',
				reader		: reader,
				sortInfo        : {field:'fechgrup0', direction:'ASC'},
				groupField	: 'fechgrup0'
			});	
	var elemento = new Ext.ux.grid.GroupSummary();
	var grid = new Ext.grid.EditorGridPanel({
				store	: gstore,
				plugins	: elemento,
				title:"<H1 align='center'>Connections<H1>",
				columns : [new Ext.grid.RowNumberer(),
					{
						 header:"Day",
						 dataIndex: "fechgrup0",
						 hideable: false,
						 sortable : true,
						 groupable: true,
						 width: 100
					},{
						header		: "Date Time",
						dataIndex	: "sessiondate",
						width		: 295,
						sortable	: true,
						groupable	: true,
						summaryType	: "count",
						summaryRenderer: function(v, params){
								return ((v === 0 || v > 1) ? '(' + v +' Items)' : '(1 Item)');
							}
					},{
						header		: "IP",
						dataIndex	: "login_ip",
						width		: 295,
						sortable	: true,
						groupable	: true,
					}
				],
				view : new Ext.grid.GroupingView({
					//startCollapsed : true,
					forceFit:true,
					showGroupName: false,
					enableNoGroups:false,
					enableGroupingMenu:false,
					hideGroupedColumn: true
				}),
				sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
				clicksToEdit: 1,
				collapsible: true,
				animCollapse: false,
				trackMouseOver: false,
			});	
	var tabPanel = new Ext.TabPanel({  
            border: false,  
            activeTab: 0,  
            enableTabScroll:true,  
			height: 530,	
			items:[panel,grid]  
         });
	function generateData(){
		var data = [];
		data.push([anio-2]);
		data.push([anio-1]);
		data.push([anio]);
		return data;
	}
	
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		for(i=0;i<store.getCount();i++){
			acum = acum + parseInt(totales[i].data.total);
		}
		Ext.getCmp('total').setText('Total: '+acum);	
		if(store.getCount()>0){
			Ext.getCmp('average').setText('Average: '+(acum/store.getCount()).toFixed(2));
		}	
	}

	var dataMonths =new Ext.data.SimpleStore({
						fields: ['id','name'],
							data  : [
							  ['1','January'],
							  ['2','February'],
							  ['3','March'],
							  ['4','April'],
							  ['5','May'],
							  ['6','June'],
							  ['7','July'],
							  ['8','August'],
							  ['9','September'],
							  ['10','October'],
							  ['11','November'],
							  ['12','December']
							]
						});
    var dataYear =new Ext.data.SimpleStore({
					fields: ['id'],
					data: generateData()
				});

	var dataType=new Ext.data.SimpleStore({
						fields: ['id','name'],
							data  : [
							  ['1','Monthly'],
							  ['2','Yearly']
							]
						});					
   var mypanel = new Ext.form.FormPanel({
        frame:true,
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{xtype: 'compositefield',
					labelWidth: 5,
					fieldLabel: 'Select Month',
					items:[{
							width:100,
							id:'month',
							xtype:'combo',
							store: dataMonths,
							mode: 'local',
							valueField: 'id',
							displayField: 'name',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: mes,
							listeners: {
											 'select': function(){
													searchSession();
												}	
										}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'Year',
							align: 'center'
						},{
							width:80,
							id:'year',
							xtype:'combo',
							store: dataYear,
							mode: 'local',
							valueField: 'id',
							displayField: 'id',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: anio,
							listeners: {
											 'select': function(){
													searchSession();
												}
										}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'Type',
							align: 'center'
						},{
							width:80,
							id:'type',
							xtype:'combo',
							store: dataType,
							mode: 'local',
							valueField: 'id',
							displayField: 'name',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: '1',
							listeners: {
								 'select': function(field,newvalue,oldvalue){
										searchSession();
										if(field.getValue()==2){
											Ext.getCmp('radio-type').getEl().show(); 
										}else{
											Ext.getCmp('radio-type').getEl().hide(); 
										}
								 }
							}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'User',
							align: 'center'
						},{
							xtype		:'combo',
							id			:'iduser',
							name		:'iduser',
							hiddenName	:'cuser',
							fieldLabel	:'User',
							allowBlank	:true,
							width		:250,
							store		:new Ext.data.SimpleStore({
											fields: ['id', 'username'],
											data : Ext.combos_selec.cbUsers
										}),
							editable	:true,
							displayField:'username',
							valueField	:'id',
							mode		:'local',
							triggerAction:'all',
							emptyText	:'Select..',
							selectOnFocus:true,
							autoSelect: true,
							listeners: {
											 'select': function(){
													searchSession();
												}
										}
						},{
							xtype:'button',
							text:'Refresh',
							iconCls: 'x-tbar-loading',
							handler:function(){ 
								searchSession();
							}
						},{
							layout: 'table',
							layoutConfig:	{
								columns	:	3
							},
							items	:	[{
								width:130,
								xtype:'label',
								id: 'total',
								style: 'font-size:15px;',
								text: 'Total:',
								align: 'center'
							},{
								width:150,
								xtype:'label',
								id: 'average',
								style: 'font-size:15px; margin-left:10px',
								text: 'Average:',
								align: 'right'
							},{
								width:20,
								xtype:'label',
								text: '',
								align: 'center'
							},{
								layout: 'form',
								labelWidth: 10,
								id:'radio-type',
								colspan: 2,
								bodyStyle: 'margin-top:-4px; margin-left:-12px',
								items:[{   
									xtype	: 'radiogroup',
									width	: 200,
									id:'radio-value',
									 items: [{
												  boxLabel: 'Quantity',
												  width: 50, 
												  name: 'tipo', 
												  inputValue: '1', 
												  checked: true,
												  onClick: function(e){
													  TypoPrint=1;
													  calculateAVe()
												  }
											  },{
												  boxLabel: 'Average', 
												  name: 'tipo', 
												  inputValue: '2',
												  onClick: function(e){
													   TypoPrint=2;
													  calculateAVe()
												  }
												  
											  }  
										 ]
									}]
							},{
								width:20,
								xtype:'label',
								text: ' ',
								align: 'center'
							}]
						}
					],listeners: {
						 'afterrender': function(){
							Ext.getCmp('radio-type').getEl().hide(); 
						 }
					}},tabPanel	
				]
	})
	
	 var viewport = new Ext.Viewport({
       layout       :'border',
       hideBorders  :true,
       monitorResize:true,
       items        :[{
                        region  :'north',
                        height  :25,
                        items   :Ext.getCmp('menu_page')
                    },{
                        region  :'center',
                        autoHeight:true,
                        items   :mypanel
                    }]
   });
   store.addListener('load',function(){
   		obtenerTotal();
		if(Ext.getCmp("type").getValue()==2)
			calculateAVe();
   });
	/*
    * lee los datos y los carga
    */
	store.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), year: Ext.getCmp("year").getValue(), userid: Ext.getCmp("iduser").getValue(), type: Ext.getCmp("type").getValue()}});
	

    function searchSession(){
		var mensaje = Ext.MessageBox.show({
			msg: 'Searching...',
			progressText: 'Searching...',
			width:300,
			wait:true,
			waitConfig: {interval:100},
			icon:'ext-mb-download',
			animEl: 'samplebutton'
			});
		tabPanel.setActiveTab(0);
		store.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), year: Ext.getCmp("year").getValue(),userid: Ext.getCmp("iduser").getValue(), type: Ext.getCmp("type").getValue()}});
		gstore.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), year: Ext.getCmp("year").getValue(), userid: Ext.getCmp("iduser").getValue(), type: Ext.getCmp("type").getValue()}});
		if(Ext.getCmp("iduser").getValue()==''){
			if(Ext.getCmp("type").getValue()==1){
				panel.setTitle('Session Logs '+meses[Ext.getCmp("month").getValue()-1]+' '+Ext.getCmp("year").getValue());
			}else{
				panel.setTitle('Session Logs '+Ext.getCmp("year").getValue());
			}
		}else{
			if(Ext.getCmp("type").getValue()==1){
				panel.setTitle('Session Logs '+meses[Ext.getCmp("month").getValue()-1]+' '+Ext.getCmp("year").getValue()+' for UserId '+Ext.getCmp("iduser").getValue());
				grid.setTitle('Connections '+meses[Ext.getCmp("month").getValue()-1]+' '+Ext.getCmp("year").getValue()+' for UserId '+Ext.getCmp("iduser").getValue());
			}else{
				panel.setTitle('Session Logs '+Ext.getCmp("year").getValue()+' for UserId '+Ext.getCmp("iduser").getValue());
				grid.setTitle('Connections '+Ext.getCmp("year").getValue()+' for UserId '+Ext.getCmp("iduser").getValue());
			}
		}
		mensaje.hide();	
    }
	
	function calculateAVe(){
		if(TypoPrint==1){
			var campo="totalD";
		}else{
			var campo="Avr";
		}
		store.each(function(record){
			record.set("total",record.get(campo))
		});
	}

       
});