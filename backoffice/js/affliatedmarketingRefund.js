function nuevoAjax()
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por 
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=refundrealizados',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=refundrealizados', timeout: 3600000 }),
		reader: new Ext.data.JsonReader(),						
		baseParams  :{start:0, limit:200},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status' 
			,'paydated'
			,'cardholdername'
			,'cardnumber'
			,'fecha'
			,'transactionid'
			,'amount'
			,'amountsinformato'
			,'priceprod'
			,'cobrador'
			,'source'
			,'notes'
			,'notasrefund'
		]
	});
	
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var hoy  = new Date();	
/*
	var panelpag = new Ext.form.FormPanel({
		width: 700,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			fieldLabel: 'Range Date',
			items:		
			[{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value:  new Date(hoy.getFullYear(), hoy.getMonth() , 1)	
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				width:200,
				xtype:'label',
				id: 'total',
				text: 'Total Amount: 0.00',
		        margins: '5 0 0 50',
				align: 'center'
			}]
		}]
	});
*/
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 20000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			'-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
			}
			,'-'
			//,panelpag
			,{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value:  new Date(hoy.getFullYear(), hoy.getMonth() , 1)	
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
                    xtype       :'combo',
                    id          :'type_operacion',
                    name        :'type_operacion',
                    hiddenName  :'type_operacion',
                    typeAhead: true,
                    fieldLabel  :'Type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['15', 'Refund'], ['14', 'Debits'], ['13', 'Credits']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    editable: false,
                  	forceSelection:true,
                  	value: '15',
                    
            },{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search -</b>',
				handler: searchCobros
			},{
				width:200,
				xtype:'label',
				id: 'total',
				text: 'Total Amount: 0.00',
		        margins: '5 0 0 50',
				align: 'center'
			}
		]
    });

	
////////////////FIN barra de pagineo//////////////////////
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");		
		ajax.send("parametro=refunds&rfrom="+Ext.getCmp('dayfrom').getValue().format('Y-m-d')+"&rto="+Ext.getCmp('dayto').getValue().format('Y-m-d')+"&type="+Ext.getCmp('type_operacion').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}
//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		//alert(obtenidos)
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}
	function cobrarSeleccionados(){
		var com;
		var j=0;
		var marcados="(";
		var arrayRecords = store.getRange(0,store.getCount());
		for(i=0;i<arrayRecords.length;i++){
			com="del"+arrayRecords[i].get('userid');
			
			if(document.getElementById(com.toString()).checked==true){
				if(j>0) marcados+=',';
				
				marcados+=arrayRecords[i].get('userid');
				j++;
			}
		}
		marcados+=")";
		return marcados;
	}
	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.setBaseParam('type_operacion',Ext.getCmp("type_operacion").getValue());
		store.load();
	}
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		for(i=0;i<store.getCount();i++)
		{
			acum = acum + parseFloat(totales[i].data.amountsinformato);
			//alert(totales[i].data.amountsinformato+" * "+acum+" * "+totales[i].data.priceprod);
		}
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){//onclick="mostrarDetalleUsuers({0},\'customerservices\')"
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }		
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}	
	}
	function notasrefundf(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('{0}...<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}	
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{id:'userid',header: "User ID", width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: "Surname", width: 100, align: 'left', sortable: true, dataIndex: 'surname'}			
			,{header: "Status", width: 80, align: 'left', sortable: true, dataIndex: 'status'}
			,{header: 'Paydate', width: 80, sortable: true, align: 'center', dataIndex: 'paydated'}
			//,{header: 'Price', width: 60, sortable: true, align: 'right', dataIndex: 'priceprod'}
			,{header: 'Date Trans.', width: 130, sortable: true, align: 'center', dataIndex: 'fecha'}
			,{header: 'Amount', width: 80, sortable: true, align: 'right', dataIndex: 'amount'}
			,{header: 'Source', width: 130, sortable: true, align: 'left', dataIndex: 'source'}
			,{header: 'For', width: 130, sortable: true, align: 'left', dataIndex: 'cobrador'}
			,{header: 'Description', width: 150, sortable: true, align: 'left', dataIndex: 'notasrefund', renderer: notasrefundf}
		],			
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Refunds Realizados',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{
					xtype: 'compositefield',
					fieldLabel: '<b>Opciones</b>',
					items:[/*{
						width:120,
						id:'historico',
						xtype:'button',
						text:'Historico',
						handler: showHistorico
					},*/{
						width:120,
						id:'cobrarsel',
						xtype:'button',
						text:'Otro'
					},{
						width:120,
						xtype:'checkbox',
						boxLabel: '<font color="red">&nbsp;<b>.</b></font>',
						name: 'chpaypal'
					}]
				}, 
				grid ]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
});
