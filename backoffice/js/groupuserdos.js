  function previewHtmlMenu(idtmail)
    {
       // alert(idtmail);
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data_instructives.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'group_get_edit',
                idtmail: idtmail
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
   var storeMenu2= new Ext.data.JsonStore({
                    url:'php/grid_data_instructives.php?tipo=group_list',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idgroup','group_name']
                });

    var storeUser2= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
                });

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{ xtype:'hidden',
                            id: 'idgroup_act',
                            name: 'idgroup_act',
                            value: variable.idgroup,
                            hiddenName: 'idgroup_act',
                        },{ xtype:'hidden',
                            id: 'userid',
                            name: 'userid',
                            value: variable.userid,
                            hiddenName: 'userid',
                        }, {
                                        xtype:'combo',
                                        store:storeMenu2,
                                        fieldLabel:'Group',
                                        id:'id_group_dd',
                                        name:'id_group_dd',
                                        hiddenName: 'id_group_dd',
                                        valueField: 'idgroup',
                                        displayField: 'group_name',
                                        triggerAction: 'all',
                                        emptyText:'Select a menu',
                                        allowBlank: false,
                                        width:200,
                                        value: variable.grupo
                                    },

                   {
                                        xtype:'combo',
                                        store:storeUser2,
                                        fieldLabel:'User',
                                        id:'typeuser4',
                                        name:'typeuser4',
                                        hiddenName: 'typeuser4',
                                        valueField: 'idmen2',
                                        displayField: 'caption2',
                                        triggerAction: 'all',
                                        emptyText:'Select a user',
                                        allowBlank: false,
                                        width:200,
                                        value: variable.user

                                    }],
                    buttonAlign: 'center',
                    buttons: [{
                        text: '<b>Update User Group</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_data_instructives.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'group_edit',
                                        idtmail: idtmail
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    },'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Update User Group',
                    width: 300,
                    height: 150,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:10px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


    function execToggleCheck(node, isCheck){
      if(node) {
       //node.expand();
       node.cascade(function(){
         if (this.attributes.cls=="file") {
           this.ui.toggleCheck(isCheck);
           this.attributes.checked=isCheck;
         }
       });
      }
     }


//////////////FIN ADD CHILDS ///////////////////////////////


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////


///////////Cargas de data dinamica///////////////
    var store = new Ext.data.JsonStore({//store grupos asignados
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data_instructives.php?tipo=groupuser',
        fields: [
            {name: 'idgroups_users', type: 'int'}
         //{name: 'idworkshop', type: 'int'}
            ,'userid'
            ,'nombre'
            ,'grupo'
            ,'gruponame'

        ]
    });

    var storeUsersapro = new Ext.data.JsonStore({//store grupos asignados
        autoLoad: true,
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data_instructives.php?tipo=group_add_aprovver',
        fields: [
            {name: 'idiua', type: 'int'}
         //{name: 'idworkshop', type: 'int'}
            ,'userid'
            ,'nombre'
           

        ]
    });
    var storeMenu= new Ext.data.JsonStore({
                    url:'php/grid_data_instructives.php?tipo=group_list',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idgroup','group_name']
                });

    var storeUser= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
                });

     var storeMenu3= new Ext.data.JsonStore({
                    url:'php/comboMenu.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen','caption']
                });
//          ,'procode'
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button_g',
            text: 'Add Group',
            tooltip: 'Click to Add GROUP',
            handler: Addgroup
            
        },
        {
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            text: 'Add User',
            tooltip: 'Click to Add User Group',
            handler: AddMenu            
        },
        {
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/delete.gif',
            id: 'del_butt',
            tooltip: 'Click to Delete User of Group',
            text: 'Delete',
            handler: doDel
        },{
                                        xtype:'combo',
                                        store:storeMenu,

                                        fieldLabel:'logstatistic',
                                        id:'typemenu2',
                                        name:'typemenu2',
                                        hiddenName: 'typemenu3',
                                        valueField: 'idgroup',
                                        displayField: 'group_name',
                                        triggerAction: 'all',
                                        emptyText:'Select a group',
                                        allowBlank: false,
                                        width:200,
                                        listeners: {'select': logstatistic}
                                    },{
                xtype: 'button',
                width:80,
                id:'searchc',
                pressed: true,
                enableToggle: true,
                name:'searchc',
                text:'&nbsp;<b>Search</b>',
                handler: logstatistic
            }]
   });

   var pagingBar2 = new Ext.PagingToolbar({
            store: storeUsersapro,
            displayInfo: true,
            displayMsg: '{0} - {1} of {2} Registros',
            emptyMsg: 'No hay Registros Disponibles',
            pageSize: 100,
            items:[{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            tooltip: 'Click to Add User',
            handler: Adduser
        }
        ]
    });



////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idgroups_users;
         //marcados+=selec[i].json.idworkshop;
            //alert(marcados)
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }
function logstatistic(){
store.setBaseParam('idmen',Ext.getCmp("typemenu2").getValue());

        store.load({params:{start:0, limit:100}});
    }



        function doDel(){
        Ext.MessageBox.confirm('Delete User Group','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();

                    if(!obtenidos){

                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    ////return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');
                    //alert(obtenidos);
                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_data_instructives.php',
                        method: 'POST',
                        params: {
                            tipo : 'group_delete',
                            idmen: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }

//Inicio funcion ADD GROUP
    function Addgroup()
    {

        var formul = new Ext.FormPanel({
            url:'php/grid_data_instructives.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [
                   {
                        xtype:'textfield',
                        fieldLabel: 'Group name',
                        name: 'group',
                        id: 'group',
                        width:450,
                        allowBlank: false
                    }
                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formul.getForm().isValid())
                    {
                        formul.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'group_add_group'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                store.reload();
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New Group',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 500,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formul
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formul.getForm().reset();
        });
    }

//Fin Funcion Menu

//Inicio funcion Menu
    function AddMenu()
    {

        var formul = new Ext.FormPanel({
            url:'php/grid_data_instructives.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [{
                        xtype:'combo',
                        store:storeMenu,
                        fieldLabel:'Group',
                        id:'typemenu',
                        name:'typemenu',
                        hiddenName: 'typemenu',
                        valueField: 'idgroup',
                        displayField: 'group_name',
                        triggerAction: 'all',
                        emptyText:'Select a group',
                        allowBlank: false,
                        width:200
                        //listeners: {'select': showmenu}
                   },
                   {
                        xtype:'combo',
                        store:storeUser,
                        fieldLabel:'User',
                        id:'typeuser',
                        name:'typeuser',
                        hiddenName: 'typeuser',
                        valueField: 'idmen2',
                        displayField: 'caption2',
                        triggerAction: 'all',
                        emptyText:'Select a user',
                        allowBlank: false,
                        width:200
                        //listeners: {'select': showmenu}
                    }
                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formul.getForm().isValid())
                    {
                        formul.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'group_add_user'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                store.reload();
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New User Group',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 400,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formul
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formul.getForm().reset();
        });
    }

//Fin Funcion ADD GROUP

//Inicio Add user

    function Adduser()
    {
        var formula = new Ext.FormPanel({
            url:'php/grid_data_instructives.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [
                   {
                        xtype:'combo',
                        store:storeUser,
                        fieldLabel:'User',
                        id:'typeuser',
                        name:'typeuser',
                        hiddenName: 'typeuser',
                        valueField: 'idmen2',
                        displayField: 'caption2',
                        triggerAction: 'all',
                        emptyText:'Select a user',
                        allowBlank: false,
                        width:200
                        //listeners: {'select': showmenu}
                    }
                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formula.getForm().isValid())
                    {
                        formula.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'group_add_user_aprove'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                storeUsersapro.reload();
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New User Approver',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 400,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formula
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formula.getForm().reset();
        });
        
    }
//Termina add user        

    function searchActive(){
        if(Ext.getCmp("cbActive").getValue()==1){
            Ext.MessageBox.show({
                title: 'Warning',
                msg: 'If you activate this workshop, this is the one that will appear on the webpage',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            return;
        }
        return;
    }
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
function renderpreviewmenu(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlMenu({0})" ><img src="images/editar.png" border="0" ></a>',record.data.idgroups_users);
    }
    /*function renderTopic(value, p, record){
        return String.format('<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="editParents({0})"><img src="images/editar.png" border="0" ></a>',value);
    }*/

    function viewchild(val, p, record){
        if(record.data.edit=='N') return '';

        return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the Menu." onclick="AddMenu2()"><img src="images/editar.png" border="0" ></a>';
        //return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editParents('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }

    function viewnstatus(val, p, record){
        return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
    }
    function viewlink(val, p, record){

        return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    var grid = new Ext.grid.EditorGridPanel({
        title:wherepage,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            ,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: renderpreviewmenu}//
         //,{header: 'ID', width: 40, sortable: true, align: 'center', dataIndex: 'idgroups_users' }
            ,{header: 'Iduser', width: 70, sortable: true, align: 'left', dataIndex: 'userid'}
            ,{header: 'Nombre', width: 200, align: 'left', sortable: true, dataIndex: 'nombre'}
            ,{header: 'Group', width: 200, align: 'left', sortable: true, dataIndex: 'gruponame'}

        ],
        clicksToEdit:2,
        height:550,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });
//          ,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    var griddos = new Ext.grid.EditorGridPanel({
        title:'Approvers Users',
        id:"gridpanel2",
        store: storeUsersapro,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            //,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: renderpreviewmenu}//
         //,{header: 'ID', width: 40, sortable: true, align: 'center', dataIndex: 'idgroups_users' }
            ,{header: 'Iduser', width: 70, sortable: true, align: 'left', dataIndex: 'userid'}
            ,{header: 'Nombre', width: 200, align: 'left', sortable: true, dataIndex: 'nombre'}
            ,{
        xtype: 'actioncolumn',
        width: 30,
        items: [
            {
                icon    : 'images/delete-icon.png',                // Use a URL in the icon config
                tooltip : 'Remove user',
                scope   :   this,
                handler : function(grid, rowIndex, columnIndex, e) {
                    var rec = storeUsersapro.getAt(rowIndex);
                   Ext.MessageBox.alert('User removed','This user was removed');
                   // var idgroup = rec.get('idgroup');
                    var userid = rec.get('userid');
                   /*alert('User delete');
                    var rec = grid.getStore( ).getAt(rowIndex);
                    console.debug(rec);
                    */

// Revisar este comportamiento -wape
                    Ext.Ajax.request({
                        waitMsg: 'Loading...',
                        url: 'php/grid_data_instructives.php',
                        scope:this,
                        method: 'POST',
                        params: {
                            userid : userid,
                            tipo  :'group_remove_useraprove'
                        },
                        success:function (){
                            griddos.getStore( ).load();
                        }
                    });

                  //  console.debug(rec);
                }
            }
        ]
        }
           // ,{header: 'Group2', width: 200, align: 'left', sortable: true, dataIndex: 'gruponame'}


        ],
        clicksToEdit:2,
        height:550,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar2
    });
//          ,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },
        {
            region:'center',
            autoHeight: true,
            items:[{
                    xtype   : 'panel',
                   // width   : 500,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        grid
                    ]
                },{
                    xtype   : 'panel',
                    //flex    : 1,
                    height:Ext.getBody().getViewSize().height-200,
                    items   : [
                        griddos
                    ]
                }]
        }]
        /*
        items:[{
                    xtype   : 'panel',
                   // width   : 500,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        grid
                    ]
                },{
                    xtype   : 'panel',
                    //flex    : 1,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        gridNolaboralDetail
                    ]
                }]
        */
    });
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
    store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
