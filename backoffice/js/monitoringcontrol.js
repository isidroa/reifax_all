
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/funcionesMonitoring.php', 
		method: 'POST',
		fields: [
			'ipaddr'
			,{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'cant' 
			,'hometelephone' 
			,'email' 
			,'credit' 
			,'lastdate'
			,'country'
			,'state'
			,'city'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 10000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",			
		items:[/*{
			id: 'del_butt',
			tooltip: 'Delete selected row',
			iconCls:'icon',
			icon: 'images/delete.gif'
			//,handler: doDel
		},{
			width:400,
			fieldLabel:'Workshop',
			name:'cbWorkshop',
			id:'cbWorkshop',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['idworkshop', 'textcombo'],
				data : Ext.combos_selec.storeWS 
			}),
			mode: 'local',
			valueField: 'idworkshop',
			displayField: 'textcombo',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value: activews,
			listeners: {
				'select': searchWS
			} 
		}*/]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idrecord_workshop;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}

	function doDel(){
		
		var obtenidos=obtenerSeleccionados();
		//alert( var include_type = mypanel.getValues()['radiocobros'] );
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		//Ext.MessageBox.alert('Message',obtenidos);return;

		Ext.MessageBox.confirm('Users Workshop','Are you sure you want to delete this row?',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo: 'usersWorkshop',
							ID:  obtenidos
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							
							if(rest.succes==false)
								Ext.MessageBox.alert('Warning',rest.msg);
								
							store.reload();
						}
					}); 
				}
			}
		)
	}
	function searchWS(){
		store.setBaseParam('ws',Ext.getCmp("cbWorkshop").getValue());
		store.load({params:{start:0, limit:100}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	function viewnotes(val, p, record){
		var comment = record.data.comment;
		return  '<img src="images/notes.png" border="0" ext:qtip="'+comment+'">';
	}
	function viewnstatus(val, p, record){
		
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Monitoring Control', 
		id:"gridpanel",
		store: store, 
		iconCls: 'icon-grid',  
		columns: [	
			new Ext.grid.RowNumberer()
			,mySelectionModel
			//,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: viewnotes}
			,{header: 'Ip Address', width: 100, align: 'left', sortable: true, dataIndex: 'ipaddr'}
			,{header: 'Quantity', width: 70, sortable: true, align: 'center', dataIndex: 'cant'}
			,{header: 'country', width: 90, sortable: true, align: 'left', dataIndex: 'country'}
			,{header: 'state', width: 90, sortable: true, align: 'left', dataIndex: 'state'}
			,{header: 'city', width: 90, sortable: true, align: 'left', dataIndex: 'city'}
			,{header: 'Userid', width: 50, sortable: true, align: 'center', dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: 'Surname', width: 100, align: 'left', sortable: true, dataIndex: 'surname'}
			,{header: 'Phone', width: 120, sortable: true, align: 'left', dataIndex: 'hometelephone'}
			,{header: 'Email', width: 170, sortable: true, align: 'left', dataIndex: 'email'}
			,{header: 'CreditCard', width: 80, sortable: true, align: 'center', dataIndex: 'credit'}
			,{header: 'Last Visit', width: 120, sortable: true, align: 'left', dataIndex: 'lastdate'}
		],
		clicksToEdit:2,
		height:470,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	//store.setBaseParam('ws',activews);
	//store.load({params:{start:0, limit:100}});
	store.setBaseParam('accion', 'monitoringcontrol'); 
	store.load();
/////////////FIN Inicializar Grid////////////////////
 
});
