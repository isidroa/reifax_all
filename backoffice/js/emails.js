function copiarClipboard(meintext){
	if (window.clipboardData)
    {

		window.clipboardData.setData("Text", meintext);

   }
   else if (window.netscape)
   {

	   // you have to sign the code to enable this, or see notes below
	   netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

	   var clip = Components.classes['@mozilla.org/widget/clipboard;1']
		   .createInstance(Components.interfaces.nsIClipboard);
	   if (!clip) return;

	   // maak een transferable
	   var trans = Components.classes['@mozilla.org/widget/transferable;1']
		   .createInstance(Components.interfaces.nsITransferable);
	   if (!trans) return;

	   // specificeer wat voor soort data we op willen halen; text in dit geval
	   trans.addDataFlavor('text/unicode');

	   // om de data uit de transferable te halen hebben we 2 nieuwe objecten
	   // nodig om het in op te slaan
	   var str = new Object();
	   var len = new Object();

	   var str = Components.classes["@mozilla.org/supports-string;1"]
		   .createInstance(Components.interfaces.nsISupportsString);

	   var copytext=meintext;

	   str.data=copytext;

	   trans.setTransferData("text/unicode",str,copytext.length*2);

	   var clipid=Components.interfaces.nsIClipboard;

	   if (!clip) return false;

	   clip.setData(trans,null,clipid.kGlobalClipboard);

   }
   return false;
}
  
function mostrarDetalleEmail(idemail)
{
	win=new Ext.Window({
						 title: 'Email Detail',
						 width: 585,
						 height: 333,
						 bodyStyle:'padding: 11px',
						 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 autoLoad: 'emaildetail.php?cont='+idemail
				});
	win.show();			
}
function marcaEmails(idcont){
	var marca='N';
	if(document.getElementById("cb"+idcont).checked==true)marca='Y';
	Ext.Ajax.request( 
		{   
			waitMsg: 'Saving changes...',
			url: 'php/grid_edit.php', 
			method: 'POST',
			params: {
				tipo: "marcaemail", 
				key: 'idcont',
				keyID: idcont,
				field: 'view',
				value: marca
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				var store1 = Ext.getCmp("gridpanel").getStore();
				store1.rejectChanges();
			},
			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
					
				var store1 = Ext.getCmp("gridpanel").getStore();
				store1.commitChanges();
				store1.reload();
			}
		 }
	);  
				
}
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=emails',
		fields: [
			{name: 'idcont', type: 'int'}
			,'emaildate'
			,'source'
			,'name' 
			,'email'
			,'company'
			,'phone'
			,'city'
			,'view'			
			,'comment'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			{
				id: 'del_butt',
                tooltip: 'Delete selected Emails',
				iconCls:'icon',
				icon: 'images/delete.gif',
                handler: doDel
			}
		]
    });
////////////////FIN barra de pagineo//////////////////////
	//////////////////Manejo de Eventos//////////////////////
	function copiarData(grid,rowIndex,cellIndex, e){
		var indice = grid.getColumnModel().getDataIndex(cellIndex);
		//alert(grid.getColumnModel().getDataIndex(cellIndex));
		copiarClipboard(grid.getStore().getRange(rowIndex, rowIndex)[0].get(indice));
		
	}
	function doDel(){
		var com;
		var j=0;
		var eliminar="(";
		var arrayRecords = store.getRange(0,store.getCount());
		for(i=0;i<arrayRecords.length;i++){
			com="del"+arrayRecords[i].get('idcont');
			
			if(document.getElementById(com.toString()).checked==true){
				if(j>0) eliminar+=',';
				
				eliminar+=arrayRecords[i].get('idcont');
				j++;
			}
		}
		eliminar+=")";
		if(j==0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return;
		}
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/eliminaremail.php', 
			method: 'POST',
			params: {
				cont: eliminar 
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},
			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
					
				store.commitChanges();
				store.reload();
			}
		}); 

	}
	

	function contextMenu(grid,rowIndex,cellIndex, e){
		var coords = e.getXY();
		contextMenu = new Ext.menu.Menu({
		  items: [{
						text: 'Copy',
						handler: copiarData.createDelegate(this,[grid,rowIndex,cellIndex, e])	
				}]

		})
		//copy_clip('hola');
		contextMenu.showAt([coords[0], coords[1]]);
		Ext.getBody().on("contextmenu", Ext.emptyFn, null, {preventDefault:true});
	}
		
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function check(val, p, record){
		var marcado='';
		if(val=='Y'){
			marcado='checked';
		}
		var idc=record.data.idcont;
		var comment = record.data.comment;
		var retorno= String.format('<input type="checkbox" name="cb'+idc+'"  id="cb'+idc+'" onClick="marcaEmails('+idc+')" '+marcado+'>',idc);
		retorno+= String.format('<a href="javascript:void(0)" onClick="mostrarDetalleEmail({0})" >',idc);
		retorno+= String.format('<img src="images/notes.png" border="0" ext:qtip="'+comment+'"></a>',idc);
		
		return retorno;
	}
	function deleteImg(val, p, record){
		var retorno= String.format('<a href="javascript:void(0)" onClick="borraEmail({0})" >',val);
		retorno+= String.format('<img src="images/delete.gif" border="0" ext:qtip="Eliminar"></a>',val);
		return retorno;
	}
	function checkEmailDelete(val,p,record){
		return String.format('<input type="checkbox" name="del'+val+'"  id="del'+val+'">',val);
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			new Ext.grid.RowNumberer()
			,{header: '', width: 40, dataIndex: 'view', renderer: check}
			,{header: 'Date', width: 180, sortable: true, align: 'left', dataIndex: 'emaildate'}
			,{header: 'Source', width: 140, align: 'left', sortable: true, dataIndex: 'source'}
			,{header: 'Name', width: 200, align: 'left', sortable: true, dataIndex: 'name',editor: new Ext.form.TextField({allowBlank: false})}
			,{header: 'Email', width: 200, align: 'left', sortable: true, dataIndex: 'email',editor: new Ext.form.TextField({allowBlank: false})}
			,{header: 'Phone', width: 150, sortable: true, align: 'left', dataIndex: 'phone', editor: new Ext.form.TextField({allowBlank: false})}
			,{header: 'Delete', width: 50, sortable: true, align: 'center', dataIndex: 'idcont', renderer: checkEmailDelete}
		],
		clicksToEdit:2,
		height:470,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		width: screen.width,//'99.8%',
		frame:true,
		title:'Emails',
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('cellcontextmenu', contextMenu);
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});
