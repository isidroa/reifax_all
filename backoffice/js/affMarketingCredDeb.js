Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		
	}
	
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Credits/Debits/Refund',
		url:'php/grid_edit.php',
        bodyStyle:'padding:5px 5px 0',
        width: 500,
		defaults: {width: 250},
        items: [{
			id:'cbUsers',
			name:'cbUsers',
			hiddenName: 'cbUsers',
			xtype:'combo',
			fieldLabel: 'User',
			store: new Ext.data.SimpleStore({
			fields: ['userid', 'nameuser'],
				data : Ext.combos_selec.dataUsers
			}),
			valueField: 'userid',
			displayField: 'nameuser',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			emptyText:'Select'
		},{
			id:'cbOperation',
			name:'cbOperation',
			hiddenName: 'cbOperation',
			xtype:'combo',
			fieldLabel: 'Operation',
			store: new Ext.data.SimpleStore({
			fields: ['idoper', 'operation'],
				data :[
					['14','Debit'],
					['13','Credit'],
					['15','Refund'],
					['4','Bill Payment']
				]
			}),
			valueField: 'idoper',
			displayField: 'operation',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			emptyText:'Select'
		},{
			xtype:'datefield',
			fieldLabel: 'Date',
			id: 'txtFechaCobro',
			name: 'txtFechaCobro',
			allowBlank: false,
	        format: 'Y-m-d',
			value: new Date()
		},{
			xtype:'numberfield',
			fieldLabel: 'Amount',
			allowBlank: false,
			allowDecimals: true,
			allowNegative: false, 
			id: 'txtMontoCobrar',
			name: 'txtMontoCobrar'
		},{
			xtype:'textfield',
			fieldLabel: 'Document N�',
			id: 'txtNroDocumento',
			name: 'txtNroDocumento'
		},{
			xtype: 'textarea',
			fieldLabel: 'Notes',
			allowBlank: false,
			id: 'txtNotas',
			name: 'txtNotas'
		},{
			xtype: 'hidden',
			name: 'tipo',
			hiddenName: 'tipo',
			value: 'creddebrefund'
		}],
		buttons: [{
            text: 'Save',
			handler  : function(){
				mypanel.getForm().submit({
					success: function(form, action) {
						Ext.Msg.alert("Success", action.result.msg);
						mypanel.getForm().reset();
					},
					failure: function(form, action) {
						if(action.result)
						Ext.Msg.alert("Failure", action.result.msg);
					}
				});
			}
        },{
            text: 'Reset',
			handler: function(){
				mypanel.getForm().reset();
			}
        }]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
/////////////FIN Inicializar Grid////////////////////
});
