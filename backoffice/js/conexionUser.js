// JavaScript Document
var ACTIVATE=0;
var TRABAJANDO=0;
var icorte=0;var varcorte=15;

Ext.ns('schedule');
schedule.details={};
	

Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
	ACTIVATE=Ext.util.Cookies.get('activeConexion')=='true'?1:0;
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica/////////////////
var store = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'results',
	url: 'saveconnections.php',
	remoteSort : true,
	fields: [
		{name: 'userid', type: 'int'}
		,'nameuser'
		,'status' 
		,'lastdate'
	],
	listeners:{
		'beforeload':function (){
			Ext.getCmp('btnConnect').setText('<b>Loading, please wait</b>');
			TRABAJANDO=1;
		},
		'load':function (that, records, options){
			TRABAJANDO=0;
			for (var k in that.data.map){
				//console.debug(that.data.map[k].json.processClient);
				if(!that.data.map[k].json.processClient){
					ACTIVATE=0;
					var clientdate=new Date().format('Y-m-d H:i:s'); 
					Ext.getCmp('btnConnect').setText('<b>CONNECT</b>');
					Ext.getCmp('btnConnect').setIcon('images/play.png');
				}
				break;
			}
			if(ACTIVATE){
				Ext.getCmp('btnConnect').setText('<b>DISCONNECT</b>');
				var hoy=new Date();  
				var hourcuerrent=hoy.getHours();
				var clientdate=new Date().format('Y-m-d H:i:s'); //hoy.getFullYear()+'-'+hoy.getMonth()+'-'+hoy.getDay()+' '+hourcuerrent+':'+hoy.getMinutes()+':'+hoy.getSeconds();  
				var colorfa='148530';
				if( (hourcuerrent>=12 && hourcuerrent<14 ) || hourcuerrent>=18)
					colorfa='853114';
				var aux="("+(icorte+1)+"). <font color='"+colorfa+"'><strong>[OK]</strong></font>. HI, <strong>"+username+"</strong>. <br>Date:  <strong>"+clientdate+'</strong>. IP:  <strong>'+ipclient+'</strong>.<hr>';
				Ext.getCmp('logsconnections').setValue(aux+Ext.getCmp('logsconnections').getValue());
				icorte++;
			}else{
				Ext.getCmp('btnConnect').setText('<b>CONNECT</b>');
			}
		},
		'loadexception':function (){
			TRABAJANDO=0;
			if(ACTIVATE){
				Ext.getCmp('btnConnect').setText('<b>DISCONNECT</b>');
				var hoy=new Date();  
				var hourcuerrent=hoy.getHours();
				var clientdate=new Date().format('Y-m-d H:i:s'); //hoy.getFullYear()+'-'+hoy.getMonth()+'-'+hoy.getDay()+' '+hourcuerrent+':'+hoy.getMinutes()+':'+hoy.getSeconds();  
				
				var colorfa='FF0000';
				if( (hourcuerrent>=12 && hourcuerrent<14 ) || hourcuerrent>=18)
					colorfa='853114';
				
				var aux="("+(icorte+1)+"). <font color='"+colorfa+"'><strong>[FAIL]</strong></font>. HI, <strong>"+username+"</strong>.<br>Date:  <strong>"+clientdate+'</strong>. IP:  <strong>'+ipclient+'</strong>.<hr>';
				Ext.getCmp('logsconnections').setValue(aux+Ext.getCmp('logsconnections').getValue());
				icorte++;
			}
			else{
				Ext.getCmp('btnConnect').setText('<b>CONNECT</b>');
			}
		}
	}
});
/***************************
 * STORE FOR Stadistica
 */

var storeStat = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'data',
	baseParams:{
		opcion:'readStat',
		userid: userid,
		dateInit:'2014-01-01',
		dateEnd:new Date().format('Y-m-d')
	},
	autoSave	:true,
	autoLoad	:true,
	url: 'php/funcionesControlConex.php',
	remoteSort : false,
	fields: [
		{name: 'userid', type: 'int'},
		{name: 'nameUser', type: 'string'},
		{name: 'horario'},
		{name: 'Fhorario'},
		{name: 'statusLaboral'},
		{name: 'status'}
	]
});


///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
var updateClock = function(){  
	var texto='<div align="center"><font size="7" color="FF0000" face="courier new"><b>'+new Date().format('G:i:s')+'</b></font></div>';
    Ext.getCmp('optionspanel').setValue(texto);  
}   
  
var task = {  
    run: updateClock, //the function to run  
    interval: 1000 //every second  
}   
  
var runner = new Ext.util.TaskRunner();  
runner.start(task); //start runing the task every one second  


var task2 = {  
    run: function (){
		var hoy=new Date();  
		var hourcuerrent=hoy.getHours();
		var clientdate=new Date().format('Y-m-d H:i:s'); 
		
		store.load({
			params:{
				active		:ACTIVATE,
				userid 		: userid,
				username 	: username,
				ip 			: ipclient,
				timedateclient : clientdate,
				icorte		:icorte
			}
		});
	},   
    interval: refreshConex 
}   
  
var runner2 = new Ext.util.TaskRunner();  
runner2.start(task2); 

function changeColor(val, p, record){
	if(record.data.status == 'OFFLINE')
	{
		return '<span style="color:red;font-weight: bold;">' + val + '</span>';
	}
	if(record.data.status == 'ONLINE')
	{
		return '<span style="color:green;font-weight: bold;">' + val + '</span>';
	}
	return val;
}

//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
var grid = new Ext.grid.EditorGridPanel({
	id:"gridpanel",
	store: store,
	iconCls: 'icon-grid',
	columns: [	
		new Ext.grid.RowNumberer()
		,{header: 'User', width: 200, sortable: true, align: 'left', dataIndex: 'nameuser'}
		,{header: 'Status', width: 80, align: 'center', sortable: true, dataIndex: 'status',renderer: changeColor}
		,{header: 'Last Conex', width: 130, align: 'left', sortable: true, dataIndex: 'lastdate'}
	],
	viewConfig: {
		forceFit:true, 
		getRowClass : function (row, index) { 
				var cls = ''; 
				var col = row.data;
				if(col.status == 'OFFLINE')
				{
					cls = 'red-row'
				}
				 return cls; 
			  } 
	},
	frame:true,
	autoHeight:true,
	loadMask:true,
});

/***************************
 * grid Stadistica 
 */
 
 
var gridStat = new Ext.grid.EditorGridPanel({
	store: storeStat,
	iconCls: 'icon-grid',
	border:false,
	tbar:new Ext.PagingToolbar({  
			store: storeStat,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[/*'Between:',
				{
					xtype	: 'datefield',
					name	: 'BetweenI',
					format	: 'Y-m-d',
					value	: initSemana(),
					listeners	: {
					}
				},'To',
				{
					xtype	: 'datefield',
					name	: 'BetweenE',
					format	: 'Y-m-d',
					value	: new Date(),
					listeners	: {
					}
				},
				{
					text	: 'filter',
					scope	: this,
					handler	: function (){
						var fechas=gridStat.getTopToolbar( ).findByType('datefield');
						schedule.detailsInitDate=fechas[0].getValue( );
						schedule.detailsEndDate=fechas[1].getValue( );
						if(schedule.details){
							if (Ext.getCmp('scheduleDetailsGridStat')){
								var fechasDetails=schedule.details.detailsConexTab.getTopToolbar( ).findByType('datefield');
								fechasDetails[0].setValue(fechas[0].getValue( ));
								fechasDetails[1].setValue(fechas[1].getValue( ));
							}
						}
						fechas[0]= new Date(fechas[0].getValue( ));
						fechas[1]= new Date(fechas[1].getValue( ));
						storeStat.load({
							params:{
								dateInit:fechas[0].format('Y-m-d') ,
								dateEnd:fechas[1].format('Y-m-d') 
							}
						})
					}
				}*/
			]
	}),
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'Userid', width: 50, sortable: true, align: 'left', dataIndex: 'userid'},
		{header: 'User', width: 300, sortable: true, align: 'left', dataIndex: 'nameUser'},
		{header: 'Worked Hours', width: 100, sortable: true, align: 'right', dataIndex: 'horario',renderer:conversionTiempoDias},
		{header: 'Absences', width: 100, sortable: true, align: 'right', dataIndex: 'statusLaboral',renderer:conversionTiempoDiasNegativos},
		{header: 'Extra Hours', width: 100, sortable: true, align: 'right', dataIndex: 'Fhorario',renderer:conversionTiempoDias},
		{header: 'Balance', width: 100, sortable: true, align: 'right', dataIndex: 'status',renderer:conversionTiempoDias}
	],
	viewConfig: {
		forceFit:true, 
		getRowClass : function (row, index) { 
			var cls = ''; 
			var col = row.data;
			if(col.status <0)
			{
				cls = 'red-row'
			}
				 return cls; 
		} 
	},
	autoHeight:true,
	loadMask:true,
});
gridStat.getTopToolbar( ).on('beforechange',function(bar,params){
	var fechas=gridStat.getTopToolbar( ).findByType('datefield');
		fechas[0]= new Date(fechas[0].getValue( ));
		fechas[1]= new Date(fechas[1].getValue( ));
		params.dateInit=fechas[0].format('Y-m-d') ;
		params.dateEnd=fechas[1].format('Y-m-d');
})

















/////////////////FIN Grid////////////////////////////


	/******************************************
	 **		inicio del panl para las desconexiones 
	 * /
	var storeStat = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'data',
			autoLoad:true,
			baseParams:{
				opcion:'readDetailsDesconect'
			},
			url: 'php/funcionesControlConex.php',
			remoteSort : false,
			fields: [
				{name: 'dateDesconect1'},
				{name: 'dateDesconect2'},
				{name: 'type'},
				{name: 'timeDesconect'}
			]
		});
		/*
		var gridStat = new Ext.grid.EditorGridPanel({
			store: storeStat,
			iconCls: 'icon-grid',
			border:false,
			tbar:new Ext.PagingToolbar({  
					store: storeStat,
					displayInfo: true,  
					displayMsg: '{0} - {1} of {2} Registros',  
					emptyMsg: 'No hay Registros Disponibles',  
					pageSize: 50,
					items:[
					'|','Between:',
					{
						xtype	: 'datefield',
						format	: 'Y-m-d',
						name	: 'BetweenI',
						value	: initSemana(),
					},'To',
					{
						xtype	: 'datefield',
						name	: 'BetweenE',
						format	: 'Y-m-d',
						value	: new Date()
					},{xtype: 'tbspacer'},
					{
						text	: 'filter',
						scope	: this,
						handler	: function (){
							var fechas=schedule.details.gridStat.getTopToolbar( ).findByType('datefield');
							var combo=schedule.details.gridStat.getTopToolbar( ).findByType('combo');
							fechas[0]= new Date(fechas[0].getValue( ));
							fechas[1]= new Date(fechas[1].getValue( ));
							schedule.details.userid=combo[0].getValue( );
							schedule.details.storeStat.load({
								params:{
									userid:schedule.details.userid,
									status:combo[1].getValue( ),
									dateInit:fechas[0].format('Y-m-d') ,
									dateEnd:fechas[1].format('Y-m-d') 
								}
							})
						}
					}
					]
			}),
			columns: [	
				new Ext.grid.RowNumberer(),
				{header: 'Date init', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect1'},
				{header: 'Date init', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect2'},
				{header: 'Time', width: 200, sortable: true, align: 'left', dataIndex: 'timeDesconect', renderer:conversionTiempo}
			],
			viewConfig: {
				forceFit:true, 
				getRowClass : function (row, index) { 
					var cls = ''; 
					var col = row.data;
					if(col.type ==1)
					{
						cls = 'red-row'
					}
					else{
						cls = 'green-row'
					}
						 return cls; 
				} 
			},
			autoHeight:true,
			loadMask:true,
		});
	
*/

	
//////////////VIEWPORT////////////////////////////////			
	
	schedule.pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			xtype:'tabpanel',
			region:'center',
		    activeTab: 0,
		    items: [{
		        title: 'Conexion',
				layout: 'border',
				hideBorders: true,
		        items:[
		        /*****************
		         * inicio del primer tab 
		         */
		        {
					region:'west',
					width:400,
					items:[
					{
						xtype:'fieldset',
						layout: 'form',
						border:false,
						labelWidth:150,
						items:[{
							xtype: 'htmleditor',
							width: 380,
							hideLabel :true,
							height: 70,
							border:false,
							readOnly: true,
							id: 'optionspanel',
							name: 'optionspanel',
							enableAlignments : false,
							enableColors : false,
							enableFont : false,
							enableFontSize : false,
							enableFormat : false,
							enableLinks : false,
							enableLists : false,
							enableSourceEdit : false
						},{
							xtype:'button',
							scale: 'large',
							id	:'btnConnect',
							width:380,
							text: ACTIVATE==1?'<b>DISCONNECT</b>':'<b>CONNECT</b>',
							icon: ACTIVATE==1?'images/stop.png':'images/play.png',
							handler  : function(){
								if(TRABAJANDO){
									return true;
								}
								icorte=0;
								if(ACTIVATE==0)
								{
									ACTIVATE=1;
									var clientdate=new Date().format('Y-m-d H:i:s'); 
									store.load({
										params:{
											active		:ACTIVATE,
											userid 		: userid,
											username 	: username,
											ip 			: ipclient,
											timedateclient : clientdate,
											icorte		:icorte
										}
									});
									this.setText('<b>DISCONNECT</b>');
									this.setIcon('images/stop.png');
									Ext.util.Cookies.set('activeConexion',true)
								}
								else
								{
									ACTIVATE=0;
									Ext.util.Cookies.set('activeConexion',false);
									Ext.Ajax.request({  
										waitMsg: 'Loading...',
										url: 'php/funcionesControlConex.php', 
										scope:this,
										method: 'POST', 
										//timeout: 100000,
										params: { 
											userid 	: userid,
											opcion	:'desconectUser'
										},
										success:function (){
											var clientdate=new Date().format('Y-m-d H:i:s'); 
											store.load({
												params:{
													active		:ACTIVATE,
													userid 		: userid,
													username 	: username,
													ip 			: ipclient,
													timedateclient : clientdate,
													icorte		:icorte
												}
											});
											this.setText('<b>CONNECT</b>');
											this.setIcon('images/play.png');
										}
									});
								}				
							}
		
						}]
					},
					{
						layout: 'form',
						title:'Connections Logs',
						border:false,
						items:[{
							xtype: 'htmleditor',
							width: 400,
							height:Ext.getBody().getViewSize().height-200,
							readOnly: true,
							id: 'logsconnections',
							name: 'logsconnections',
							enableAlignments : false,
							hideLabel :true,
							enableColors : false,
							enableFont : false,
							enableFontSize : false,
							enableFormat : false,
							enableLinks : false,
							enableLists : false,
							enableSourceEdit : false
						}]
					}
					]
				},{
					region:'center',
					id	:'containerConexCurrent',
					autoHeight: true,
					items: {}
				}
		        /**********
		         **fin del primer tab 
		         */
		        ]
		    },{
		        title: 'Statistical',
		        items:[
		        	gridStat
		        ]
		    }]
		}]
	});
	
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
//setTimeout("SendConexion()",1000);  
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////



		schedule.details={};
		schedule.details.storeStat = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'data',
			baseParams:{
				opcion:'readDetailsConex'
			},
			url: 'php/funcionesControlConex.php',
			remoteSort : false,
			fields: [
				{name: 'userid', type: 'int'},
				{name: 'nameUser', type: 'string'},
				{name: 'horario'},
				{name: 'Fhorario'},
				{name: 'statusLaboral'},
				{name: 'status'},
				{name: 'record'},
				{name: 'date'},
				{name: 'reason'},
				{name: 'useridExec'},
				{name: 'normal'},
				{name: 'execute'},
				{name: 'dateAssing'}
			]
		});
		schedule.details.storeStat2 = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'data',
			baseParams:{
				opcion:'readDetailsConexMod'
			},
			url: 'php/funcionesControlConex.php',
			remoteSort : false,
			fields: [
				{name: 'userid', type: 'int'},
				{name: 'nameUser', type: 'string'},
				{name: 'horario'},
				{name: 'Fhorario'},
				{name: 'statusLaboral'},
				{name: 'status'},
				{name: 'record'},
				{name: 'date'},
				{name: 'reason'},
				{name: 'useridExec'},
				{name: 'normal'},
				{name: 'execute'},
				{name: 'dateAssing'}
			]
		});
		
		/******************************************
		 **		inicio del panl para las desconexiones 
		 */
		schedule.details.storeDesconect = new Ext.data.JsonStore({
				totalProperty: 'total',
				root: 'data',
				baseParams:{
					opcion:'readDetailsDesconect'
				},
				url: 'php/funcionesControlConex.php',
				remoteSort : false,
				fields: [
					{name: 'dateDesconect1'},
					{name: 'dateDesconect2'},
					{name: 'type'},
					{name: 'timeDesconect'}
				]
			});
			
			schedule.details.gridDesconect = new Ext.grid.EditorGridPanel({
				store: schedule.details.storeDesconect,
				height:Ext.getBody().getViewSize().height-100,
				iconCls: 'icon-grid',
				border:false,
				/*tbar:new Ext.PagingToolbar({  
						store: schedule.details.storeDesconect,
						displayInfo: true,  
						displayMsg: '{0} - {1} of {2} Registros',  
						emptyMsg: 'No hay Registros Disponibles',  
						pageSize: 50
				}),*/
				columns: [	
					new Ext.grid.RowNumberer(),
					{header: 'Type', width: 200, sortable: true, align: 'left', dataIndex: 'type',renderer:function (val){
						if(val==1){
							return 'Offline';
						}
						else 
							return  'Online';
					}},
					{header: 'Start', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect1'},
					{header: 'End', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect2'},
					{header: 'Time', width: 200,  align: 'left', dataIndex: 'timeDesconect', renderer:conversionTiempo}
				],
				viewConfig: {
					forceFit:true, 
					getRowClass : function (row, index) { 
						var cls = ''; 
						var col = row.data;
						if(col.type ==1)
						{
							cls = 'red-row'
						}
						else{
							cls = 'green-row'
						}
							 return cls; 
					} 
				},
				loadMask:true,
			});
			
		schedule.details.gridStat = new Ext.grid.EditorGridPanel({
			id:'scheduleDetailsGridStat',
			store: schedule.details.storeStat,
				height:(Ext.getBody().getViewSize().height-100)/2,
			iconCls: 'icon-grid',
			border:false,
			tbar:new Ext.PagingToolbar({  
					store: schedule.details.storeStat,
					displayInfo: true,  
					displayMsg: '{0} - {1} of {2} Registros',  
					emptyMsg: 'No hay Registros Disponibles',  
					pageSize: 50,
					items:[
					]
			}),
			columns: [	
				new Ext.grid.RowNumberer(),
				{header: 'Date', width: 100, sortable: true, align: 'right', dataIndex: 'dateAssing'},
				{header: 'Worked Hours', width: 100, sortable: true, align: 'right', dataIndex: 'horario',renderer:conversionTiempoDias},
				{header: 'Absences', width: 100, sortable: true, align: 'right', dataIndex: 'statusLaboral',renderer:conversionTiempoDiasNegativos},
				{header: 'Extra Hours', width: 100, sortable: true, align: 'right', dataIndex: 'Fhorario',renderer:conversionTiempoDias},
				{header: 'Balance', width: 100, sortable: true, align: 'right', dataIndex: 'status',renderer:conversionTiempoDias}
			],
			viewConfig: {
				forceFit:true
			},
			loadMask:true,
		});
		schedule.details.gridStat2 = new Ext.grid.EditorGridPanel({
			id:'scheduleDetailsGridStat2',
			store: schedule.details.storeStat2,
				height:(Ext.getBody().getViewSize().height-100)/2,
			iconCls: 'icon-grid',
			border:false,
			tbar:new Ext.PagingToolbar({  
					store: schedule.details.storeStat2,
					displayInfo: true,  
					displayMsg: '{0} - {1} of {2} Registros',  
					emptyMsg: 'No hay Registros Disponibles',  
					pageSize: 50,
					items:[
					]
			}),
			columns: [	
				new Ext.grid.RowNumberer(),
				
				
				{header: 'Reason', width: 300, sortable: true, align: 'left', dataIndex: 'reason'},
				{header: 'Responsable', width: 80, sortable: true, align: 'left', dataIndex: 'execute'},
				{header: 'Date Assing', width: 100, sortable: true, align: 'right', dataIndex: 'dateAssing'},
				{header: 'type', width: 100, sortable: true, align: 'right', dataIndex: 'normal',renderer:function (val){
					if(val==1){
						return 'In Schedule';
					}
					else if(val==2){
						return 'Out Schedule';
					}
					else if(val==4){
						return 'By user';
					}
					else{
						return 'System';
					}
				}},
				{header: 'Value', width: 100, sortable: true, align: 'right', dataIndex: 'record',renderer:conversionTiempoDias},
			],
			viewConfig: {
				forceFit:true
			},
			loadMask:true,
		});
		
		schedule.details.detailsConexTab=new Ext.Panel({
										title	: 'Details',
										layout	: 'hbox',
										id		: 'detailsConexTab',
										tbar	:	[
										'Between:',
					{
						xtype	: 'datefield',
						format	: 'Y-m-d',
						name	: 'BetweenI',
						value	: (schedule.detailsInitDate)?schedule.detailsInitDate:'2014-01-01',
					},'To',
					{
						xtype	: 'datefield',
						name	: 'BetweenE',
						format	: 'Y-m-d',
						value	: (schedule.detailsEndDate)?schedule.detailsEndDate:new Date()
					},{xtype: 'tbspacer'},
					{
						text	: 'filter',
						scope	: this,
						handler	: function (){
							var fechas=schedule.details.detailsConexTab.getTopToolbar( ).findByType('datefield');
							fechas[0]= new Date(fechas[0].getValue( ));
							fechas[1]= new Date(fechas[1].getValue( ));
							schedule.details.storeStat.load({
								params:{
									userid:schedule.details.userid,
									dateInit:fechas[0].format('Y-m-d') ,
									dateEnd:fechas[1].format('Y-m-d') 
								}
							});
							schedule.details.storeStat2.load({
								params:{
									userid:schedule.details.userid,
									dateInit:fechas[0].format('Y-m-d') ,
									dateEnd:fechas[1].format('Y-m-d') 
								}
							});
							
							schedule.details.storeDesconect.load({
									params:{
										userid:schedule.details.userid,
										dateInit:fechas[0].format('Y-m-d') ,
										dateEnd:fechas[1].format('Y-m-d') 
									}
								});
						}
					}],
				id		: 'detailsConexion', 
				items:[{
					xtype	: 'panel',
					width	: 800, 
					height:Ext.getBody().getViewSize().height-100,
					items	: [
						schedule.details.gridStat,
						schedule.details.gridStat2
				]},{
					xtype	: 'panel',
					flex	: 1, 
					height:Ext.getBody().getViewSize().height-100,
					items	: [
						schedule.details.gridDesconect
				]}
				]
			});
	schedule.details.userid=userid;
	schedule.details.storeStat.load({
			params:{
				userid:schedule.details.userid,
				status:1,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		})
	schedule.details.storeStat2.load({
			params:{
				userid:schedule.details.userid,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		})
	schedule.details.storeDesconect.load({
			params:{
				userid:schedule.details.userid,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		});
	schedule.pag.findByType('tabpanel')[0].add(schedule.details.detailsConexTab);








/////////////FIN Inicializar Grid////////////////////
});



function initSemana(){
	var today = new Date();
	var initSemana= new Date(today.getTime() - ((today.format('N')-1) * 24 * 3600 * 1000));
	return new Date(initSemana);
}
function conversionTiempoDias (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				signo='';
			}
			var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini-(dias*28800))/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)+(dias*28800)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}
			else if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
		}
function conversionTiempoDiasNegativos (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				return  '0 m 0 s';
			}
			//var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini)/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			/*if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}*/
			if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
	
}
function conversionTiempo (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				signo='';
			}
			//var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini)/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			/*if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}*/
			if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
		}