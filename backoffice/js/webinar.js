// JavaScript Document
Ext.ns('webinar');

webinar=
{
	init: function ()
	{
		webinar.gridReader = new Ext.data.JsonReader({  
			root: 'videos', 
			totalProperty: 'total',
			id:'readerwebinar'
			},
			webinar.record 
			);

		webinar.store = new Ext.data.Store({  
			id: 'webinar',  
			proxy: webinar.dataProxy,  
			baseParams: {
				accion	:'list'
			},  
			reader: webinar.gridReader  
		});
		
		
		webinar.pager = new Ext.PagingToolbar({  
			store: webinar.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 30,
			items:[ 
				{
					xtype	:'button',
					iconCls	:'x-icon-add',
					handler	: webinar.new.init,
					text	: 'New webinar'
				},
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: webinar.delete,
					text	: 'Delete'
				},
				'|'
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-save',
					handler	: webinar.save,
					text	: 'Save Changes'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: webinar.cancel,
					text	: 'Cancel Change'
				}
			], 
			listeners: {
			}
		});
		
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {
				forceSelection:true,
				minValue: '8:00 AM',
				maxValue: '6:00 PM',
				increment: 30});
				
		var opcionesCombo= [
        ['1', 'Active'],
        ['2', 'Inactive'] ];
		
		var opcionesStatus= {
			1	:	'Active',
			2	:	'Inactive'
		}
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		
		var storeUserAdmin= new Ext.data.SimpleStore({
					fields: ['userid', 'name'],
					data : Ext.combos_selec.dataUsersAdmin
			});
		var comboUser = new Ext.form.ComboBox({
			store: storeUserAdmin,
			hiddenName	: 'userPro',
			valueField: 'userid',
			displayField:'name',
			 style: {
                fontSize: '16px'
            },
			typeAhead: true,
			triggerAction: 'all',
			mode: 'local',
			selectOnFocus:true
		})
		
		
		
		webinar.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
				webinar.sm,
			{  
				header: 'Id webinar',  
				dataIndex: 'id', 
				sortable: true,  
				width: 25  
			},{  
				header: 'Made By',  
				dataIndex: 'madeBy',
				editor	: comboUser, 
				renderer:function (val){
					var record = comboUser.findRecord(comboUser.valueField, val);
					return record ? record.get(comboUser.displayField) : val;
				},
				sortable: true,
				width: 70
			},{  
				header		: 'video',  
				dataIndex	: 'name', 
				sortable	: true,
				editor		:textField,
				width		: 150
			},{  
				header: 'idYoutube',  
				dataIndex: 'idYoutube', 
				sortable: true,
				renderer:function (val){
					return '<a href="#" onClick="viewWebinar(\''+val+'\')">'+val+'</a>';
				},
				width: 50
			},{  
				header: 'Shower',  
				dataIndex: 'show', 
				renderer:function (val){
					if(val){
						return 'Yes';
					}
					else{
						return 'No';
					}
				},
				sortable: true,
				width: 25
			},{  
				header: 'status',  
				dataIndex: 'status',
				editor	: combo, 
				renderer:function (val){
					if(opcionesStatus[val]){
						return opcionesStatus[val];
					}
					else{
						return val;
					}
				},
				sortable: true,
				width: 30
			},{  
				header: 'fecha',  
				dataIndex: 'fecha', 
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				sortable: true,
				width: 50
			},{  
				header		: 'description',  
				dataIndex	: 'description', 
				sortable	: true,
				editor		: textField,
				width		: 180
			}
			]  
		);
		
		webinar.store.load();	
		webinar.grid = new Ext.grid.EditorGridPanel({
			height: 500,
            store: webinar.store,
			tbar: webinar.pager,   
			cm: webinar.columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true ,
			sm:webinar.sm,
			loadMask: true,
			border :false
        }); 
		
		webinar.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: webinar.grid,
					autoHeight: true
			}]
		});
		
	},
	sm:new Ext.grid.CheckboxSelectionModel()
	,
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'madeBy', type: 'string'},
			{name: 'show', type: 'bool'},
			{name: 'name', type: 'string'},    
			{name: 'idYoutube', type: 'string'},    
			{name: 'urlVideo', type: 'string'},
			{name: 'urlImg', type: 'date', dateFormat: 'string'},
			{name: 'status', type: 'int'},
			{name: 'fecha', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'description', type: 'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcioneswebinar.php',   // Servicio web  
			method: 'POST'                          // Método de envío  
	}),
	save	:function ()
	{
		var grid=webinar.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcioneswebinar.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
			   		webinar.grid.getStore().reload();
				}  
			});  
		}  
			
	},
	reset: function (){
		var cols=webinar.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].set){
				var date=cols[i].set('date_webinar','');
				var date=cols[i].set('hourwebinar','');
			}
		}
	},
	cancel: function ()
	{
		webinar.grid.getStore().rejectChanges();
	},
	delete:function (){
		data=webinar.grid.getSelectionModel().getSelections();
		if(data.length){
			var ids='';
			for(i=0;i<data.length;i++){
				ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
			}
		}
		Ext.Ajax.request({
			method :'POST',
		   	url: 'php/funcioneswebinar.php',
		   	success: function (){
			   webinar.grid.getStore().reload();
			},
		   	failure:  function (){
			   
			},
		  	 params: { 
			 	accion: 'delete',
				ids	: ids
			}
		});
	}
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	new:{
		init:function (){
			if(webinar.new.win)
			{
				webinar.new.form.getForm().reset();
				webinar.new.win.show();
			}
			else{
				webinar.new.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [{    
						height 	: 170,
    					width	: 200,
						xtype	:	'textarea',
						name	:	'url',
				        fieldLabel: 'Url'
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(webinar.new.form.getForm().isValid())
								{
									webinar.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new webinar...',
										url: 'php/funcionesWebinar.php',
										scope : this,
										params :{
											accion : 'recordWebinar'
										},
										success	: function (form,action)
										{
											webinar.new.form.getForm().reset();
											webinar.new.win.hide();
											webinar.store.load();	
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								webinar.new.form.getForm().reset();
							}
						}]
				})
				
				
				webinar.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New webinar', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 360,
					height: 265,
					items	:[
						webinar.new.form
					]
				});
				webinar.new.win.show();
			}
		}
	}
}
function viewWebinar(id){
	window.open('http://www.youtube.com/embed/'+id+'?rel=0','View Webinar','width=720,height=500');
}

Ext.onReady(webinar.init);
Ext.QuickTips.init()