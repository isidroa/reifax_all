// JavaScript Document
Ext.ns('usaControl');

Ext.override(Ext.form.ComboBox, {
      onTypeAhead : function(){
   }
});

usaControl={
	var	:{
		checksSel : new Array()// ,
		// record: new Ext.data.Record.create()
	},
	view	:{
		TextFields	:{
			anonimo	:
				new Ext.form.TextField({
					allowBlank: false
				})
		},
		combos	:{

		},
		SelectionModels	:{
			gridPrincipal	:	new Ext.grid.CheckboxSelectionModel({singleSelect: false}),
			actionGrid		:	new Ext.grid.CheckboxSelectionModel({singleSelect: true}),
		}
	},
	stores	: {
    	States	:new Ext.data.JsonStore({
			url		:'php/funcionesNewusa.php',
			root	:'data',
			autoLoad	:true,
			baseParams : {
				option	:'getState',
			},
			fields: [{
				name:	'idcounty'
			},{
				name:	'state'
			},{
				name:	'statecode'
			},{
				name:	'bd'
			}]
		}),
    	Checks	:new Ext.data.JsonStore({
			url		:'php/funcionesNewusa.php',
			root	:'data',
			autoLoad	:true,
			listeners : {
				load	: function (){ return usaControl.controllers.updateGridModel()}
			},
			baseParams : {
				option	:'getChecks',
			},
			fields:[
					{
						name:	'ID'
					},{
						name:	'ActionID'
					},{
						name:	'CountyID'
					},{
						name:	'fecha'
					},{
						name:	'TEjecucion'
					},{
						name:	'Records'
					},{
						name:	'idprocMapeo'
					},{
						name:	'indicador'
					},{
						name:	'eliminado'
					},{
						name:	'ejecucionfull'
					}
			]
		}),
		System: new Ext.data.JsonStore({
			autoLoad	:true,
			url			:'php/funcionesNewusa.php',
			baseParams 	: {
				option	:'getSystem',
			},
			root		:'data',
			fields	: [{
					name:	'id'
				},{
					name:	'name'
				}]
		})
    },
	models	:{

	},
	controllers	:{

		doEdit	: function (oGrid_Event) {
			var gid = oGrid_Event.record.data.actionid;
			var gfield = oGrid_Event.field;
			var gvaluenew = oGrid_Event.value;

			Ext.Ajax.request({
				url: 'php/funcionesNewusa.php',
				method :'POST',
				params: {
					gid : gid,
					option : 'updateCheck',
					gfield : gfield,
					gvaluenew : gvaluenew
				},
				waitTitle   :'Please wait!',
				waitMsg     :'Saving changes...',
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
				},
				success: function(respuesta){
					if(gfield=='valprom2')
					{
						oGrid_Event.record.data.valprom=((oGrid_Event.record.data.valpromcru*gvaluenew)/oGrid_Event.record.data.porc);
					}
					usaControl.stores.GridPrincipal.commitChanges();
					return true;
				}
			});
		},

		doEditActions	: function (oGrid_Event) {
			var gid = oGrid_Event.record.data.id;
			var gfield = oGrid_Event.field;
			var gvaluenew = oGrid_Event.value;
			Ext.Ajax.request({
				url: 'php/funcionesNewusa.php',
				method :'POST',
				params: {
					gid : gid,
					option : 'updateCheckOrigin',
					gfield : gfield,
					gvaluenew : gvaluenew
				},
				waitTitle   :'Please wait!',
				waitMsg     :'Saving changes...',
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
				},
				success: function(respuesta){
					usaControl.stores.actions.commitChanges();
					return true;
				}
			});
		},colorAndDateHandler: function(menuItem, choice) {
			console.debug(choice);
			console.debug(menuItem);
			Ext.getCmp('colortitle').setValue('#'+choice);
		},
		updateGridModel	:function (){
			usaControl.var.checksSel=new Array();
			/*
			 * if(usaControl.var.checksSel.length>0){ } else{
			 */
				usaControl.stores.Checks.each(function (e,i){
					if(i<5)
						usaControl.var.checksSel.push(e.id);
				})
				usaControl.var.checksSel.reverse();
			// }
			var fieldStore=usaControl.var.fieldsStore;
			var cm=[
				usaControl.view.SelectionModels.gridPrincipal,
				/*
				 * { header : "", width : 40, align : 'center', sortable : true,
				 * dataIndex : 'source', renderer :
				 * usaControl.controllers.renders.render5 },
				 */
				{
					header		: "ORDEN",
					width		: 70,
					align		: 'center',
					sortable	: true,
					dataIndex	: 'orden',
					editor		: usaControl.view.TextFields.anonimo
				},
				{
					header		: "ACTIONID",
					width		: 70,
					align		: 'center',
					hidden 		: true,
					sortable	: true,
					dataIndex	: 'actionid'
				},
				{
					header		: "TITULO",
					width		: 300,
					align		: 'left',
					sortable	: true,
					dataIndex	: 'titulo',
					renderer	: usaControl.controllers.renders.render4,
					editor		: usaControl.view.TextFields.anonimo
				},
				{
					header		: "Qx%",
					tooltip		:'Quantity by %',
					hidden		: false,
					width		: 70,
					align		: 'right',
					sortable	: true,
					renderer	: usaControl.controllers.renders.render1,
					dataIndex	: 'valprom2',
					editor		: usaControl.view.TextFields.anonimo
				},
				{
					header		: "Max Variation",
					tooltip		: 'Maximum Variation',
					hidden		: false,
					width		: 70,
					align		: 'right',
					sortable	: true,
					renderer	: usaControl.controllers.renders.render1,
					dataIndex	: 'valprom'
				}

			]
			var defaultData=	[
				{
					name	: 'actionid',
					type	: 'int'
				},
				{
					name	: 'orden',
					type	: 'int'
				},
				{					name	: 'titulo',
					type	: 'string'
				},
				'tipocampo',
				'source',
				'colortitle',
				'porc',
				'valporc' ,
				'valporcf',
				'valprom2',
				'valprom',
				'valpromcru',

				{
					name	: 'promedio0',
					type	: 'int'
				},
				{
					name	: 'promedio1',
					type	: 'int'
				},
				{
					name	: 'promedio2',
					type	: 'int'
				},
				{
					name	: 'promedio3',
					type	: 'int'
				},
				{
					name	: 'promedio4',
					type	: 'int'
				},
				{
					name	: 'promedio5',
					type	: 'int'
				},
				{
					name	: 'promedio6',
					type	: 'int'
				},
				{
					name	: 'promedio7',
					type	: 'int'
				},
				{
					name	: 'promedio8',
					type	: 'int'
				}

			];
			var aux=(usaControl.var.checksSel.length-2);
			var lastItemEval=null;
			Ext.each(usaControl.var.checksSel,function (e,i){
				var itemEval=usaControl.stores.Checks.getById( e );
				lastItemEval=itemEval;


				defaultData.push(itemEval.data['fecha']);

				fieldStore.push(itemEval.data['fecha']);
					cm.push({
						header		: itemEval.data['fecha'],
						width		: 90,
						sortable	: true,
						align		: 'right',
						dataIndex	: itemEval.data['fecha'],
						renderer	: usaControl.controllers.renders.render2
					});
				if(i<aux){
					cm.push({
						header		: 'Diff '+(usaControl.var.checksSel.length-(i+1)),
						width		: 90,
						sortable	: true,
						align		: 'right',
						dataIndex	: 'promedio'+(i+1),
						renderer	: usaControl.controllers.renders.changeColor
					});
				}
			});
			cm.push({
				header		: 'Diff 1',
				width		: 90,
				sortable	: true,
				align		: 'right',
				dataIndex	: 'promedio'+(usaControl.var.checksSel.length-1),
				renderer	: usaControl.controllers.renders.changeColor
			});


			usaControl.var.fieldsStore = defaultData;

		usaControl.utility.columnMode = new Ext.grid.ColumnModel( cm);




			usaControl.var.gridReader = new Ext.data.JsonReader({
					root: 'data',
					totalProperty: 'total',
					fields:usaControl.var.fieldsStore
				}
			);

			usaControl.stores.GridPrincipal = new Ext.data.Store({
				baseParams		:{
					option	:'getChecksValues'
				},
				proxy			:usaControl.utility.proxy,
				listeners : {
					beforeload :function (){
					},
					load	: function (self, records, options ){
					}
				},
				reader		: usaControl.var.gridReader
			});


			usaControl.view.grid.reconfigure( usaControl.stores.GridPrincipal, usaControl.utility.columnMode );
			// usaControl.utility.columnMode.setConfig(cm);

			usaControl.stores.GridPrincipal.load({
				params:{
					ejecutions: usaControl.var.checksSel.join(','),
					state	:usaControl.var.comoSearchState
				}
			});
		},

		obtenerSeleccionados:function (){
			var selec = usaControl.view.grid.selModel.getSelections();
			var i=0;
			var marcados='(';
			for(i=0; i<selec.length; i++){
				if(i>0) marcados+=',';
				marcados+=selec[i].json.actionid;
			}
			marcados+=')';
			if(i==0)marcados=false;
			return marcados;
		},

		doEditActionId 	: function (){
			var obtenidos=usaControl.controllers.obtenerSeleccionados();
			if(!obtenidos || obtenidos.split(',').length>1){
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'You must select a single row.',
					buttons: Ext.MessageBox.OK,
					icon:Ext.MessageBox.ERROR
				});
				return;
			}
			obtenidos=obtenidos.replace('(','');
			obtenidos=obtenidos.replace(')','');
			Ext.Ajax.request({
				waitMsg: 'Loading...',
				url: 'php/grid_dataaccesscontrol.php',
				method: 'POST',
				timeout: 100000,
				params: {
					tipo : 'getactionid',
					actionid : obtenidos
				},
				failure:function(response,options){
					// Ext.MessageBox.alert('Warning','Error Loading');
				},
				success:function(response){
				    Ext.QuickTips.init();
				    var variable = Ext.decode(response.responseText);

					var form = new Ext.form.FormPanel({
						baseCls: 'x-plain',
						labelWidth: 55,
						items: [{
							xtype:'textfield',
							fieldLabel: 'Action',
							name: 'action',
							id: 'action',
							width: 280,
							value: variable.titulo,
							allowBlank: false
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Color Act',
							msgTarget : 'side',
							defaults: {
								flex: 1
							},
							items: [
								{
									xtype : 'splitbutton',
									text : 'Set color',
									disabled: false,
									width: 70,
									itemId : 'splitbut',
									menu : [{
										text : 'Choose Color',
										menu : {
											xtype : 'colormenu',
											handler : function (){ return usaControl.controllers.colorAndDateHandler ()},
											listeners : {
												click: function() {
													return false;
												}
											}
										}
									}]
								},
								{
									xtype:'textfield',
									fieldLabel: 'Action',
									name: 'colortitle',
									id: 'colortitle',
									value: variable.colortitle,
									width: 60
								}
							]
						}

						,{
							xtype:'numberfield',
							fieldLabel: 'Orden',
							name: 'orden',
							id: 'orden',
							value: variable.orden,
							allowBlank: false
						},{
							xtype:'combo',
							store:new Ext.data.SimpleStore({
							fields: ['id','texto'],
							data  : [
								['int','Int'],
								['int','Float'],
								['date','Date']
								]
							}),
							fieldLabel:'DataType',
							id:'datatype',
							name:'datatype',
							hiddenName: 'datatype',
							valueField: 'id',
							displayField: 'texto',
							triggerAction: 'all',
							mode: 'local',
							value: variable.tipocampo,
							allowBlank: false
						},{
							xtype:'combo',
							store:new Ext.data.SimpleStore({
							fields: ['id','texto'],
							data  : [
								['Psummary','Psummary'],
								['Mlsresidential','Mlsresidential'],
								['Rtmaster','Rtmaster'],
								['Pendes','Pendes'],
								['Cantidades','Cantidades'],
								['Rental','Rental'],
								['Mortgage','Mortgage'],
								['Sales','Sales'],
								['Latlong-Marketvalue','Latlong-Marketvalue'],
								['Calulos','Calculos'],
								['Exray','Exray'],
								['Tiempos','Tiempos']
								]
							}),
							fieldLabel:'Table',
							id:'source',
							name:'source',
							hiddenName: 'source',
							valueField: 'id',
							displayField: 'texto',
							triggerAction: 'all',
							value:  variable.source,
							mode: 'local',
							allowBlank: false
						}],
						buttonAlign: 'center',
						buttons: [{
							text: '<b>Save</b>',
							cls: 'x-btn-text-icon',
							icon: 'images/disk.png',
							formBind: true,
							handler: function(b){
								var form = b.findParentByType('form');
								// form.getForm().fileUpload = true;
								if (form.getForm().isValid()) {
									form.getForm().submit({
										url: 'php/grid_dataaccesscontrol.php',
										waitTitle   :'Please wait!',
										waitMsg     :'Loading...',
										params: {
											tipo : 'editactionid',
											actionid : obtenidos
										},
										timeout: 100000,
										method :'POST',
										success: function(form, action) {
											w.close();
											store.reload();
											Ext.Msg.alert('Success', 'Update succesfully ');

										},
										failure: function(form, action) {
											Ext.Msg.alert('Error', action.result.msg);
										}
									});
								}
							}
						},'->',{
							text: 'Close',
							cls: 'x-btn-text-icon',
							icon: 'images/cross.gif',
							handler: function(b){
								w.close();
							}
						}]
					});

					var w = new Ext.Window({
						title: 'Edit Action Id',
						width: 400,
						height: 250,
						layout: 'fit',
						plain: true,
						bodyStyle: 'padding:5px;',
						items: form
					});
					w.show();
					w.addListener("beforeshow",function(w){
						form.getForm().reset();
					});

				}
			})


		},
		doDel: function (){
			Ext.MessageBox.confirm('Delete Action Id','Are you sure delete row?.',//
				function(btn,text)
				{
					if(btn=='yes')
					{
						var obtenidos=usaControl.controllers.obtenerSeleccionados();
						if(!obtenidos){
							Ext.MessageBox.show({
								title: 'Warning',
								msg: 'You must select a row, by clicking on it, for the delete to work.',
								buttons: Ext.MessageBox.OK,
								icon:Ext.MessageBox.ERROR
							});
							return;
						}
						obtenidos=obtenidos.replace('(','');
						obtenidos=obtenidos.replace(')','');

						Ext.Ajax.request({
							waitMsg: 'Saving changes...',
							url: 'php/funcionesNewusa.php',
							method: 'POST',
							params: {
								option : 'deleteCheck',
								actionid: obtenidos
							},

							failure:function(response,options){
								Ext.MessageBox.alert('Warning','Error editing');
								store.reload();
							},

							success:function(response,options){

								store.reload();
							}
						});
					}
				}
			)
		},
		initloadData	:function (){
			usaControl.var.comoSearchState=Ext.getCmp('comoSearchState').getValue();
			usaControl.stores.Checks.load({
				params	:{
					state	:usaControl.var.comoSearchState
				}
			});
		},
		renders	:{
			render1		: function (val,p,record){
				if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
				else if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
				else {
					if(usaControl.utility.isFloat(val)){
						val=Ext.util.Format.number(val, '0,000.00');
					}
					else{
						val=Ext.util.Format.number(0, '0,000.00');
					}
				}
				return String.format('<b>{0}</b>',val);
			},
			render1Prome: function (val,p,record){
				val=val*record.data.valprom2;
				if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
				if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
				return String.format('<b>{0}</b>',val);
			},
			checkRow	: function (val,p,record){
				return String.format('<input type="checkbox" name="del'+record.data.idcounty+'"  id="del'+record.data.idcounty+'">',record.data.idcounty);
			},
			cellCheck	: function (value, metaData, record, rowIndex, colIndex, store){
				console.debug(value, metaData, record, rowIndex, colIndex, store);
		        if(val){
		        	p.style+='background-color:#33CC33';
		        }
		        else{
		        	p.style+='background-color:#FF0000';

		        }
		        return val;
		   },
			changeColor	: function (val, p, record,a,b,c,d){
		        if(Math.abs(val) <= record.data.valprom )
				{
					if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
					if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
		            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
		        }
				else
				{
					if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
					if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
					return '<span style="color:red;font-weight: bold;">' + val + '</span>';
				}
		        return val;
		   	},
			render2		: function (val,p,record){
				if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
				else if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
				else {
					if(usaControl.utility.isFloat(val)){
						val=Ext.util.Format.number(val, '0,000.00');
					}
				}
				return val;
			},
			render3		: function (val,p,record){
				return String.format('<b>{0}</b>',val);
			},
			render4		: function (val, p, record){
		        if( record.data.colortitle!='' )
		        	p.style+='color:'+record.data.colortitle+';font-weight: bold;';
		           // return '<span style="">' + val + '</span>';
		        return val;
		   	},
			render5		: function (val,p,record){
				if(val == 'Psummary')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Mlsresidential')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Rtmaster')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Pendes')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Cantidades')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Rental')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Mortgage')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Sales')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Latlong-Marketvalue')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -160px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Calulos')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -180px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Exray')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -200px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
				if(val == 'Tiempos')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -220px transparent; width:20px; height: 20px;" title="'+val+'">&nbsp;</div>';
			}
		}
	},
	utility:{
		proxy	: new Ext.data.HttpProxy({
			url		: 'php/funcionesNewusa.php',
			type	: 'POST',
			timeout : 3600000
		}),

		isFloat	:function (n){
			if(n===Number(n)  && n%1!==0){
				return true;
			}else{
				console.info('---no se numero:');
				console.debug(n);
				return false;
			}
		},
		loadMask:new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."})
		
	},
	init:function (){
		console.info('init');
		/*
		 * inicializando grid principal
		 */
		usaControl.var.cmDefault=[
				usaControl.view.SelectionModels.gridPrincipal,
				{
					header		: "",
					width		: 40,
					align		: 'center',
					sortable	: true,
					dataIndex	: 'source',
					renderer	: usaControl.controllers.renders.render5
				},
				{
					header		: "ORDEN",
					width		: 70,
					align		: 'center',
					sortable	: true,
					dataIndex	: 'orden',
					editor		: usaControl.view.TextFields.anonimo
				},
				{
					header		: "ACTIONID",
					width		: 70,
					align		: 'center',
					sortable	: true,
					dataIndex	: 'actionid'
				},
				{
					id			: 'actionid',
					header		: "TITULO",
					width		: 300,
					align		: 'left',
					sortable	: true,
					dataIndex	: 'titulo',
					renderer	: usaControl.controllers.renders.render4,
					editor		: usaControl.view.TextFields.anonimo
				}];

		usaControl.utility.columnMode = new Ext.grid.ColumnModel( usaControl.var.cmDefault);


		// usaControl.var.fieldsStore=;

		// usaControl.var.record = new
		// Ext.data.Record.create(usaControl.var.fieldsStore);

		usaControl.var.gridReader = new Ext.data.JsonReader({
				root: 'data',
				totalProperty: 'total',
				fields:usaControl.var.fieldsStore
			}
		);

		usaControl.stores.GridPrincipal = new Ext.data.Store({
			baseParams		:{
				option	:'getChecksValues'
			},
			proxy			:usaControl.utility.proxy,
			listeners : {
				beforeload :function (){
				},
				load	: function (self, records, options ){
				}
			},
			reader		: usaControl.var.gridReader
		});
		/**
		 * barra del pagineo
		 */
		usaControl.view.pagingBar = new Ext.PagingToolbar({
	        pageSize: 5000,
	        store: usaControl.stores.GridPrincipal,
	        displayInfo: true,
	        displayMsg: '<b>Total: {2}</b>',
	        emptyMsg: "No topics to display" ,
			items:[{
				iconCls:'icon',
				cls: 'x-btn-text-icon',
				icon: 'images/chgstat.png',
				id: 'compareact_butt',
				text: 'Compare Actions',
				tooltip: 'Click to Compare Actions',
				// handler: doCompareActions
	        },'-',{
				iconCls:'icon',
				cls: 'x-btn-text-icon',
				icon: 'images/refresh.png',
				id: 'compare_butt',
				text: 'Compare Dates',
				tooltip: 'Click to Compare Date',
				// handler: doCompare
	        },
	        {
				xtype		: 'combo',
				store		: usaControl.stores.States,
				valueField	: 'idcounty',
				id			: 'comoSearchState',
				width		: 200,
				displayField: 'state',
				triggerAction	: 'all',
				emptyText	: 'Select a State',
				fieldLabel	: 'State'
			}/*,{
				xtype		:'combo',
				store		: usaControl.stores.System,
				valueField	: 'id',
				displayField: 'name',
				id			: 'comoSearchSystem',
				triggerAction	: 'all',
				emptyText	: 'Select a System',
				fieldLabel	: 'System'
			}*/,{
				iconCls:'icon',
				cls: 'x-btn-text-icon',
				// icon: 'images/refresh.png',
				// id: 'compare_butt',
				text: 'Show Statistics',
				handler: usaControl.controllers.initloadData
	        }]
		});

		usaControl.view.adminActions={};
		/*
		 * 
		 * store de aciones creadas en el sistema
		 */
		usaControl.stores.actions=new Ext.data.Store({
			baseParams		:{
				option	:'getAllActions'
			},
			autoLoad	: true,
			proxy		: usaControl.utility.proxy,
			listeners 	: {
				beforeload :function (){
				},
				load	: function (a,b,c,d){
				}
			},
			reader		: new Ext.data.JsonReader({
				root: 'data',
				totalProperty: 'total',
				fields:usaControl.var.fieldsStoreSystem
			})
		});
		usaControl.view.adminActions.gridActions= new Ext.grid.EditorGridPanel({
			viewConfig: {
				forceFit:true
			},
			tbar	:[{
				iconCls:'icon',
				cls: 'x-btn-text-icon',
				icon: 'images/add.png',
				text	: 'Add Check',
				handler	: function(){
					var panel = usaControl.view.adminActions.panel.getComponent('PanelOpt');
					if(panel.collapsed){
						panel.expand();
					}
					panel.doLayout(true);
					panel.findByType('form')[0].getForm().reset();
				}
			},{
				text	: 'Edit Action',
				iconCls:'icon',
				cls: 'x-btn-text-icon',
				icon: 'images/editar.png',
				handler	: function(){
					usaControl.utility.loadMask.show();
					var panel = usaControl.view.adminActions.panel.getComponent('PanelOpt');
					if(panel.collapsed){
						panel.expand();
						panel.doLayout(true);
					}
					var form=usaControl.view.adminActions.panel.findByType('form')[0].getForm( );
					form.reset();
					var selec = usaControl.view.adminActions.gridActions.selModel.getSelections();
					var i=0;
					console.debug(usaControl);
					console.debug(usaControl.view.adminActions.gridActions.selModel);
					console.debug(selec);
					$.ajax({
            			type: "GET",
						url:'http://52.10.196.213/mant/General/get_code_mant.php',
						data: 'procID='+selec[0].data.procid,
						dataType:'jsonp',
						scope: this,
						success: function (res){
							if(res.success){
								form.setValues({
									idCheck	: selec[0].data.actionid,
									codePhp	: res.phpContent,
									action	: selec[0].data.titulo,
									colortitle	: selec[0].data.colortitle,
									orden	: selec[0].data.orden
								});
								return true;
							}
							else{
								return true;
							}
						},
						complete:function (){
							usaControl.utility.loadMask.hide();
						}
					})
				}
			}],
			store	: usaControl.stores.actions,
			clicksToEdit:2,
			cm: usaControl.utility.cmAction,
			autoScroll: true,
			sm: usaControl.view.SelectionModels.actionGrid,
			height: Ext.getBody().getViewSize().height-130,
			loadMask:true,
			listeners :{
				afteredit : usaControl.controllers.doEditActions
			},
			// tbar: usaControl.view.pagingBar
		});
		usaControl.view.adminActions.panel=new Ext.Panel({
			title	: 'Proccess Administration',
			height	: Ext.getBody().getViewSize().height-100,
			layout	:'border',
			defaults: {
			    collapsible	: true,
			    split		: true
			},
			items: [{
			    title		: 'Process Assignation',
			    region		: 'center',
			    items		: [
			    	usaControl.view.adminActions.gridActions
			    ],
			    margins		: '5 0 0 0'
			},{
			    region		: 'west',
			    itemId		: 'PanelOpt',
			    margins		: '5 0 0 0',
			    cmargins	: '5 5 0 0',
			    title		: 'Process Config',
			    titleCollapse : true,
			    width		: Ext.getBody().getViewSize().width/4,
			    items		:[{
			    		xtype		:'form',
						baseCls		: 'x-plain',
						labelWidth	: 55,
						items	: [{
							xtype		:'textfield',
							fieldLabel	: 'Action',
							name		: 'action',
							width		: 150,
							itemId		: 'action',
							allowBlank	:	false
						},
						{
			                xtype		: 'compositefield',
			                fieldLabel	: 'Color Act',
			                msgTarget 	: 'side',
			               /*
							 * defaults: { flex: 1 },
							 */
			                items: [
			                    {
									xtype 	: 'splitbutton',
									text 	: 'Set color',
									disabled: false,
									width	: 70,
			  						itemId 	: 'splitbut',
									menu 	: [{
										text : 'Choose Color',
										menu : {
											xtype 		: 'colormenu',
											handler 	: function (){ return usaControl.controllers.colorAndDateHandler ()},
											listeners 	: {
												click: function() {
													return false;
												}
											}
										}
									}]
								},
			                    {
									xtype		: 'textfield',
									fieldLabel	: 'Action',
									name		: 'colortitle',
									itemId		: 'colortitle',
									width		: 60
								}
			                ]
			            }

						,{
							xtype		:'hidden',
							fieldLabel	: 'idCheck',
							name		: 'idCheck',
							itemId			: 'idCheck',
							allowBlank	: false
						}
						,{
							xtype		:'numberfield',
							fieldLabel	: 'Orden',
							name		: 'orden',
							itemId			: 'orden',
							allowBlank	: false
						}
						,{
							xtype		: 'textarea',
							itemId		: 'codePhp',
							fieldLabel	: 'Php Code',
							name		: 'codePhp',
							width 		: 250,
							height		: Ext.getBody().getViewSize().height/2,
							allowBlank: false
						}],
						buttonAlign: 'center',
						buttons: [{
							text		: 'Save',
							cls			: 'x-btn-text-icon',
							icon		: 'images/disk.png',
			                formBind	: true,
							handler		: function(b){
								usaControl.utility.loadMask.show();
								var form = b.findParentByType('form');
							    if (form.getForm().isValid()) {
							    	console.debug(form.getForm().getFieldValues());
							    	var data=form.getForm().getFieldValues();
							    	data.userid=userid;
									$.ajax({
				            			type: "GET",
										url:'http://52.10.196.213/mant/General/edit_code_mant.php',
										data: data,
										dataType:'jsonp',
										scope: this,
										success: function (res){
											
										},
										complete:function (){
											usaControl.utility.loadMask.hide();
										}
									});
							    	/*
									form.getForm().submit({
										url			: 'http://52.10.196.213/mant/General/get_code_mant.php',
										waitTitle   :'Please wait!',
										waitMsg     :'Loading...',
										params	: {
											option 	: 'addCheck'
										},
										timeout 	: 100000,
										method 		:'GET',
										success		: function(form, action) {
											usaControl.stores.actions.reload();
											usaControl.view.adminActions.panel.getComponent('PanelOpt').collapse();
											Ext.Msg.alert('Success', action.result.msg);

										},
										failure	: function(form, action) {
											Ext.Msg.alert('Error', action.result.msg);
										}
									});*/
								}
							}
						},{
							text	: 'Close',
							cls		: 'x-btn-text-icon',
							icon	: 'images/cross.gif',
							handler	: function(b){
								usaControl.view.adminActions.panel.getComponent('PanelOpt').collapse();

							}
						}]
			    }],
			    collapsed	:true
			}]
		});

		usaControl.view.grid = new Ext.grid.EditorGridPanel({
			viewConfig: {
				forceFit:true
			},
      layout: 'fit',
			store	: usaControl.stores.GridPrincipal,
			clicksToEdit:2,
			cm: usaControl.utility.columnMode,
			autoScroll: true,
			sm: usaControl.view.SelectionModels.gridPrincipal,
			height: Ext.getBody().getViewSize().height-50,
			frame:true,
			title:'Usa Control',
			listeners :{
				afteredit : usaControl.controllers.doEdit
			},
			loadMask:true,
			tbar: usaControl.view.pagingBar
		});
		
		usaControl.view.gridLogSave = new Ext.grid.EditorGridPanel({
			viewConfig: {
				forceFit:true
			},
      layout: 'fit',
			store	: usaControl.stores.GridPrincipal,
			clicksToEdit:2,
			cm: usaControl.utility.columnMode,
			autoScroll: true,
			sm: usaControl.view.SelectionModels.gridPrincipal,
			height: Ext.getBody().getViewSize().height-50,
			frame:true,
			title:'Save Log',
			listeners :{
				afteredit : usaControl.controllers.doEdit
			},
			loadMask:true,
			tbar: usaControl.view.pagingBar
		});


		/***********************************************************************
		 * 
		 * init viewport (marco principal)
		 */

		usaControl.view.pag = new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [{
				region: 'north',
				height: 25,
				items: Ext.getCmp('menu_page')
			},{
				xtype:'tabpanel',
				region:'center',
				autoHeight: true,
        		activeTab: 0,
				items: [
					usaControl.view.grid,
					usaControl.view.adminActions.panel,
					usaControl.view.gridLogSave
				]
			}]
		});
	},
	prepare:function (){

				var defaultData=	[
				{
					name	: 'actionid',
					type	: 'int'
				},
				{
					name	: 'orden',
					type	: 'int'
				},
				{					name	: 'titulo',
					type	: 'string'
				},
				'tipocampo',
				'source',
				'colortitle',
				'porc',
				'valporc' ,
				'valporcf',
				'valprom2',
				'valprom',
				'valpromcru',

				{
					name	: 'promedio0',
					type	: 'int'
				},
				{
					name	: 'promedio1',
					type	: 'int'
				},
				{
					name	: 'promedio2',
					type	: 'int'
				},
				{
					name	: 'promedio3',
					type	: 'int'
				},
				{
					name	: 'promedio4',
					type	: 'int'
				},
				{
					name	: 'promedio5',
					type	: 'int'
				},
				{
					name	: 'promedio6',
					type	: 'int'
				},
				{
					name	: 'promedio7',
					type	: 'int'
				},
				{
					name	: 'promedio8',
					type	: 'int'
				}

			];
				usaControl.var.fieldsStore = defaultData;
				usaControl.var.fieldsStoreSystem=[
					'id',
					{
						name	:'orden',
						type	:'int'

					},
					'titulo',
					'tipocampo',
					'source',
					'colortitle',
					'phpContent',
					'visible',
					'procid',
					'actionid',
					'php'
				];
				/*
				 * Ext.Ajax.request({ url : 'php/funcionesNewusa.php', type :
				 * 'POST', params : { option :'getSystem' }, scope:this, success :
				 * function(response) { var
				 * data=Ext.util.JSON.decode(response.responseText);
				 */
						var temCm=[
									new Ext.grid.RowNumberer(),
							usaControl.view.SelectionModels.actionGrid,
							{
								header		: "actionID",
								width		: 25,
								align		: 'center',
								sortable	: true,
								dataIndex	: 'actionid'
							},
							{
								header		: "proceso",
								width		: 25,
								dataIndex	: 'procid',
								align		: 'right',
								sortable	: true,
							},
							{
								header		: "orden",
								width		: 50,
								dataIndex	: 'orden',
								align		: 'right',
								sortable	: true,
							},
							{
								header		: "Title",
								dataIndex	: 'titulo',
								sortable	: true,
								width		: 150,
								editor		: usaControl.view.TextFields.anonimo
							},
							{
								header		: "type Field",
								dataIndex	: 'tipocampo',
								sortable	: true,
								width		: 25,
								editor		: usaControl.view.TextFields.anonimo
							},
							{
								header		: "Source",
								dataIndex	: 'source',
								sortable	: true,
								width		: 60,
								editor		: usaControl.view.TextFields.anonimo
							},
							{
								header		: "Style",
								dataIndex	: 'colortitle',
								sortable	: true,
								width		: 50,
								editor		: usaControl.view.TextFields.anonimo
							},
							{
								header		: "Path PHP",
								dataIndex	: 'php',
								sortable	: true,
								width		: 80,
								editor		: usaControl.view.TextFields.anonimo
							},
							{
								header		: "Visible",
								dataIndex	: 'visible',
								sortable	: true,
								xtype		: 'booleancolumn',
					            trueText	: 'Yes',
					            falseText	: 'No',
								width		: 25,
								editor		: {
					                xtype: 'checkbox'
					            }
							}
						];
						/*
						 * Ext.each(data.data,function(data,i){ temCm.push( {
						 * editor: { xtype: 'checkbox' }, header : data.name,
						 * width : 45, xtype : 'booleancolumn', tooltip :
						 * data.name, trueText : 'Yes', falseText : 'No', align :
						 * 'center', sortable : true, renderer :
						 * usaControl.controllers.renders.cellCheck, dataIndex :
						 * 'system_'+data.id
						 * 
						 * }); usaControl.var.fieldsStoreSystem.push({ name :
						 * 'system_'+data.id }); });
						 */
						usaControl.utility.cmAction=new Ext.grid.ColumnModel(temCm);
						usaControl.init();
					/*
					 * } });
					 */
	}
}
Ext.onReady(usaControl.prepare);
