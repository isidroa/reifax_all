// JavaScript Document
Ext.ns('scraping');
scraping = {
	loadMask : new Ext.LoadMask(Ext.getBody(), {
				msg : "Please wait..."
			}),
	records : {
		statitics : new Ext.data.Record.create([{
					name : 'id',
					type : 'int'
				}, {
					name : 'name',
					type : 'string'
				}, {
					name : 'state',
					type : 'string'
				}, {
					name : 'date_update',
					dateFormat : 'Y-m-d H:i:s'
				}, {
					name : 'system_1',
					type : 'int'
				}, {
					name : 'system_2',
					type : 'int'
				}, {
					name : 'system_3',
					type : 'int'
				}, {
					name : 'system_4',
					type : 'int'
				}, {
					name : 'system_5',
					type : 'int'
				}, {
					name : 'system_6',
					type : 'int'
				}, {
					name : 'system_7',
					type : 'int'
				}, {
					name : 'system_8',
					type : 'int'
				}]),
		recordsJob : new Ext.data.Record.create([{
					name : 'idstatistics_down'
				}, {
					name : 'idstate'
				}, {
					name : 'dateInit'
				}, {
					name : 'dateEnd'
				}, {
					name : 'total'
				}, {
					name : 'ejecutorS'
				}, {
					name : 'ejecutorE'
				}])

	},
	SelectionModels : {
		gridN2csv : new Ext.grid.CheckboxSelectionModel({
					singleSelect : false
				})
	},
	dataProxy : new Ext.data.HttpProxy({
				url : 'php/funcionesScraping.php',
				method : 'POST'
			}),
	exportTableToCSV : function($table, filename) {
		var $rows = $table.find('tr:has(td)'),
		// Temporary delimiter characters unlikely to be typed by keyboard
		// This is to avoid accidentally splitting the actual contents
		tmpColDelim = String.fromCharCode(11), // vertical tab character
		tmpRowDelim = String.fromCharCode(0), // null character

		// actual delimiter characters for CSV format
		colDelim = '","', rowDelim = '"\r\n"',
		// Grab text from table into CSV formatted string
		csv = '"'
				+ $rows.map(function(i, row) {
							var $row = $(row), $cols = $row.find('td');

							return $cols.map(function(j, col) {
								var $col = $(col), text = $col.text();

								return text.replace('"', '""');
									// escape double quotes

								}).get().join(tmpColDelim);

						}).get().join(tmpRowDelim).split(tmpRowDelim)
						.join(rowDelim).split(tmpColDelim).join(colDelim) + '"',
		// Data URI
		csvData = 'data:application/csv;charset=utf-8,'
				+ encodeURIComponent(csv);

		$('.exportCSV').attr({
					'download' : filename,
					'href' : csvData,
					'target' : '_blank'
				});
	},
	saveData : function(data) {
		data.option = 'RecorJson';
		$.ajax({
					url : 'php/funcionesScraping.php',
					dataType : 'json',
					type : 'post',
					data : data,
					success : function(res) {
						scraping.loadMask.hide();
					}
				})
	},
	combos : {},
	GStore : {
		States : new Ext.data.JsonStore({
					url : 'php/funcionesScraping.php',
					root : 'data',
					baseParams : {
						option : 'getState'
						,
					},
					fields : [{
								name : 'idstate'
							}, {
								name : 'name'
							}, {
								name : 'code'
							}]
				}),
		System : new Ext.data.JsonStore({
					url : 'php/funcionesScraping.php',
					baseParams : {
						option : 'getSystem'
					},
					root : 'data',
					fields : [{
								name : 'id'
							}, {
								name : 'name'
							}]
				}),
		diary : new Ext.data.Store({
					url : 'php/funcionesScraping.php',
					root : 'data',
					baseParams : {
						option : 'getFilesToS3'
					},
					autoLoad : true,
					totalProperty : 'total',
					reader : new Ext.data.JsonReader({
								root : 'data',
								totalProperty : 'total',
								fields : ['1', '2', '3', '4', 'ids3_directory',
										'state', 'system', 'ruta', 'date',
										'type', 'items', 'size_original',
										'server', 'size_compressed', {
											name : 'process',
											type : 'int'
										}]
							})
				}),
		systemByServer : new Ext.data.Store({
					url : 'php/funcionesScraping.php',
					root : 'data',
					baseParams : {
						option : 'getsystemByServer'
					},
					autoLoad : true,
					autoSave : true,
					totalProperty : 'total',
					reader : new Ext.data.JsonReader({
								root : 'data',
								totalProperty : 'total',
								fields : ['1', '21', '18', 'id', 'name']
							})
				})
	},

	controllers : {
		renders : {
			cellCheck : function(value, metaData, record, rowIndex, colIndex,store) {
				//console.debug(value, metaData, record, rowIndex, colIndex,store);
				if(value){
					metaData.style+='background-color:#A9F5A9';
				}
				else{
					metaData.style+='background-color:#F5A9A9';

				}
				var t = this.trueText, f = this.falseText, u = this.undefinedText;

				if(value === undefined){
						return u;
				}
				if(!value || value === 'false'){
						return f;
				}
				return t;
			},
			colorActionStatus : function(value, metaData, record, rowIndex,
					colIndex, store) {
				if (value == 1) {
					metaData.css = "cell-yellow";
				}
				if (value == 2) {
					metaData.css = "cell-blue";
				}
				if (value == 3) {
					metaData.css = "cell-green";
				}
			}
		}
	}
};

scraping.init = function() {
	scraping.combos.States = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});

	scraping.combos.States2 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	/*
	 * combo usado en el tab de generacion del nivel 2
	 */
	scraping.combos.States3 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	/*
	 * combo usado en el tab de stade n2 process s3
	 */
	scraping.combos.States4 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	scraping.combos.States5 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'name',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	scraping.combos.States6 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	/*
	 * combo state n1 process s3
	 */
	scraping.combos.States7 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});
	/*
	 * combo state tab diary
	 */
	scraping.combos.States8 = new Ext.form.ComboBox({
				store : scraping.GStore.States,
				valueField : 'idstate',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			});

	/*
	 * disparados ara la carga de los condados en nive 1 del generador de url
	 */
	scraping.combos.States.on('select', function(cmb, record, index) {
				scraping.combos.Countys.enable();
				scraping.combos.Countys.clearValue();
				scraping.combos.Countys.getStore().load({
							params : {
								id : record.get('idstate')
							}
						});
			}, this);

	/*
	 * disparados ara la carga de los condados en nive 2 del generador de url
	 */
	scraping.combos.States3.on('select', function(cmb, record, index) {
				scraping.combos.Countys3.enable();
				scraping.combos.Countys3.clearValue();
				scraping.combos.Countys3.getStore().load({
							params : {
								id : record.get('idstate')
							}
						});
			}, this);

	scraping.combos.States2.on('select', function(cmb, record, index) {
				scraping.dataStores.statitics.load({
							params : {
								id : record.get('idstate')
							}
						});
			}, this);

	scraping.combos.System = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});

	/*
	 * disparados para la carga de los chekpoint en nive 1 del generador de url
	 */
	scraping.combos.System.on('select', function(cmb, record, index) {
				scraping.combos.DateSelect.enable();
				scraping.combos.DateSelect.clearValue();
				scraping.combos.DateSelect.getStore().load({
							params : {
								id : record.get('id')
							}
						});
			}, this);
	scraping.combos.System2 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});

	/*
	 * combo usado en el tab de generacion del nivel 2
	 */
	scraping.combos.System3 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});
	/*
	 * combo usado en el tab de nivel 2 procces s3
	 */
	scraping.combos.System4 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});

	scraping.combos.System5 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'name',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});
	scraping.combos.System6 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});
	/*
	 * combo system n1 process s3
	 */
	scraping.combos.System7 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});

	/*
	 * combo system page
	 */
	scraping.combos.System8 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});

	/*
	 * combo system tab diary
	 */
	scraping.combos.System9 = new Ext.form.ComboBox({
				store : scraping.GStore.System,
				valueField : 'id',
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			});
	/*
	 * combo checkpoint n1 process s3
	 */
	scraping.combos.DateSelect = new Ext.form.ComboBox({
				store : new Ext.data.JsonStore({
							url : 'php/funcionesScraping.php',
							baseParams : {
								option : 'getCheckPoint'
								,
							},
							root : 'data',
							fields : [{
										name : 'date_update'
									}]
						}),
				disabled : true,
				valueField : 'date_update',
				displayField : 'date_update',
				triggerAction : 'all',
				mode : 'local',
				emptyText : 'Select a Date',
				fieldLabel : 'Date'
			});

	scraping.combos.Countys = new Ext.form.ComboBox({
				store : new Ext.data.JsonStore({
							url : 'php/funcionesScraping.php',
							baseParams : {
								option : 'getCounty'
								,
							},
							root : 'data',
							fields : [{
										name : 'idcounty'
									}, {
										name : 'idstate'
									}, {
										name : 'name'
									}, {
										name : 'code'
									}]
						}),
				disabled : true,
				valueField : 'idcounty',
				displayField : 'name',
				triggerAction : 'all',
				mode : 'local',
				emptyText : 'Select a County first',
				fieldLabel : 'County'
			});

	/*
	 * combo usado en el tab de generacion del nivel 2
	 */
	scraping.combos.Countys3 = new Ext.form.ComboBox({
				store : new Ext.data.JsonStore({
							url : 'php/funcionesScraping.php',
							baseParams : {
								option : 'getCounty'
								,
							},
							root : 'data',
							fields : [{
										name : 'idcounty'
									}, {
										name : 'idstate'
									}, {
										name : 'name'
									}, {
										name : 'code'
									}]
						}),
				disabled : true,
				valueField : 'idcounty',
				displayField : 'name',
				triggerAction : 'all',
				mode : 'local',
				emptyText : 'Select a County first',
				fieldLabel : 'County'
			});

	scraping.combos.Countys4 = new Ext.form.ComboBox({
				store : new Ext.data.JsonStore({
							url : 'php/funcionesScraping.php',
							baseParams : {
								option : 'getCounty'
								,
							},
							root : 'data',
							fields : [{
										name : 'idcounty'
									}, {
										name : 'idstate'
									}, {
										name : 'name'
									}, {
										name : 'code'
									}]
						}),
				disabled : true,
				valueField : 'idcounty4',
				displayField : 'name',
				triggerAction : 'all',
				mode : 'local',
				emptyText : 'Select a County first',
				fieldLabel : 'County'
			});

	$(".exportCSV").on('click', function(event) {
				scraping.exportTableToCSV.apply(this, [$('#tableCSV'),
								'export.csv']);
			});

	/***************************************************************************
	 * definiendo reader
	 */

	scraping.gridReader = {
		statitics : new Ext.data.JsonReader({
					root : 'data',
					totalProperty : 'total',
					id : 'readerScraping'
				}, scraping.records.statitics),
		recordsJob : new Ext.data.JsonReader({
					root : 'data',
					totalProperty : 'total',
					id : 'readerScraping'
				}, scraping.records.recordsJob)
	}

	/***************************************************************************
	 * definiendo stores de grid
	 */

	scraping.dataStores = {
		statitics : new Ext.data.Store({
					proxy : scraping.dataProxy,
					baseParams : {
						option : 'selectStat'
					},
					remoteSort : true,
					reader : scraping.gridReader.statitics
				}),
		recordsJob : new Ext.data.Store({
					proxy : scraping.dataProxy,
					baseParams : {
						option : 'selectRecordsJob'
					},
					remoteSort : true,
					reader : scraping.gridReader.recordsJob
				})
	};

	scraping.pagers = {
		diary : new Ext.PagingToolbar({
					store : scraping.GStore.diary,
					displayInfo : true,
					displayMsg : '{0} - {1} of {2} Registros',
					emptyMsg : 'No hay Registros Disponibles',
					pageSize : 100,
					items : [scraping.combos.System9, scraping.combos.States8,
							{
								text : 'Filter',
								handler : function() {
									var system = scraping.combos.System9
											.getValue();
									var state = scraping.combos.States8
											.getValue();
									scraping.GStore.diary.load({
												params : {
													system : system,
													state : state
												}
											});
								}
							}, {
								text : 'Reset Filter',
								handler : function() {
									scraping.combos.System9.reset();
									scraping.combos.States8.reset();
									scraping.GStore.diary.load();
								}
							}]
				}),
		statitics : new Ext.PagingToolbar({
			store : scraping.dataStores.statitics,
			displayInfo : true,
			displayMsg : '{0} - {1} of {2} Registros',
			emptyMsg : 'No hay Registros Disponibles',
			pageSize : 5000,
			items : [scraping.combos.States2, '|', {
						xtype : 'label',
						text : 'Show All: '

					}, {
						xtype : 'checkbox',
						id : 'ShowAllStateStat',
						listeners : {
							check : function(_this, checked) {
								if (checked) {
									scraping.combos.States2.disable();
									scraping.dataStores.statitics.load({
												params : {
													showAll : checked
												}
											});
								} else {
									scraping.combos.States2.enable();
								}
							}
						}
					}, '|', {
						xtype : 'button',
						icon : 'images/excel.png',
						handler : function() {
							var grid = scraping.grid;
							grid.el.mask('Exporting...', 'x-mask-loading');

							Ext.Ajax.request({
								method : 'POST',
								url : 'Excel/xlsReifaxNewPageSystem.php',
								success : function(result, request) {
									grid.el.unmask();
									window
											.open(
													'Excel/d.php?nombre='
															+ $
																	.trim(result.responseText),
													'', 'width=50,height=50');
									return (true);
								},
								failure : function() {
									grid.el.unmask();
								},
								params : {
									parametro : 'Export_grid',
									showAll : Ext.getCmp("ShowAllStateStat")
											.getValue(),
									state : Ext.getCmp("ShowAllStateStat")
											.getValue()
								}
							});
						}
					}]
		})
	}

	scraping.columnModes = {
		statitics : new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(), {
					header : 'State',
					sortable : true,
					dataIndex : 'state'
				}, {
					header : 'County',
					sortable : true,
					dataIndex : 'name'
				}, {
					header : 'trulia sold',
					sortable : true,
					width : 80,
					dataIndex : 'system_1'
				}, {
					header : 'trulia for sale',
					sortable : true,
					width : 80,
					dataIndex : 'system_2'
				}, {
					header : 'RT sold',
					sortable : true,
					width : 80,
					dataIndex : 'system_3'
				}, {
					header : 'RT for sale',
					sortable : true,
					width : 80,
					dataIndex : 'system_4'
				}, {
					header : 'RT aution',
					sortable : true,
					width : 80,
					dataIndex : 'system_8'
				}, {
					header : 'RT pre-foreclosure',
					sortable : true,
					width : 80,
					dataIndex : 'system_6'
				}, {
					header : 'RT Bank owned',
					sortable : true,
					width : 80,
					dataIndex : 'system_7'
				}]),
		recordsJob : new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(), {
					header : 'id',
					sortable : true,
					width : 80,
					dataIndex : 'idstatistics_down'
				}, {
					header : 'idstate',
					sortable : true,
					width : 80,
					dataIndex : 'idstate'
				}, {
					header : 'dateInit',
					sortable : true,
					width : 80,
					dataIndex : 'dateInit'
				}, {
					header : 'dateEnd',
					sortable : true,
					width : 80,
					dataIndex : 'dateEnd'
				}])
	}
	scraping.grid = {
		statitics : new Ext.grid.GridPanel({
					store : scraping.dataStores.statitics,
					tbar : scraping.pagers.statitics,
					cm : scraping.columnModes.statitics,
					viewConfig : {
						forceFit : true
					},
					enableColLock : false,
					view : new Ext.grid.GridView({
								forceFit : true,
								getRowClass : function(row, index) {
									/*
									 * var cls = ''; var col = row.data; var
									 * check=new Date(col.date_check); var
									 * hoy=new Date() if(col.msg_cliente!='') {
									 * cls = 'red-row' } else
									 * if(col.msg_system!='') { cls =
									 * 'green-row' } else
									 * if(check.getDate()==hoy.getDate() &&
									 * check.getMonth()==hoy.getMonth() &&
									 * check.getFullYear()==hoy.getFullYear() ) {
									 * cls = 'orange-row' }
									 *
									 * return cls;
									 */
								}
							}),
					loadMask : true,
					border : false,
					layout : 'fit',
					height : Ext.getBody().getViewSize().height - 100,
					selModel : new Ext.grid.RowSelectionModel({
								singleSelect : false
							})
				}),
		recordsJob : new Ext.grid.GridPanel({
			store : scraping.dataStores.recordsJob,
			// tbar : scraping.pagers.statitics,
			cm : scraping.columnModes.recordsJob,
			viewConfig : {
				forceFit : true
			},
			enableColLock : false,
			view : new Ext.grid.GridView({
						forceFit : true,
						getRowClass : function(row, index) {

							var cls = '';
							var col = row.data;
							var check = new Date(col.date_check);
							var hoy = new Date()
							if (col.msg_cliente != '') {
								cls = 'red-row'
							} else if (col.msg_system != '') {
								cls = 'green-row'
							} else if (check.getDate() == hoy.getDate()
									&& check.getMonth() == hoy.getMonth()
									&& check.getFullYear() == hoy.getFullYear()) {
								cls = 'orange-row'
							}

							return cls;
						}
					}),
			loadMask : true,
			border : false,
			layout : 'fit',
			height : Ext.getBody().getViewSize().height - 100,
			selModel : new Ext.grid.RowSelectionModel({
						singleSelect : false
					})
			,
		})
		,
	}
	scraping.dataStores.recordsJob.load();

	scraping.tabs = new Ext.TabPanel({
		activeTab : 0,
		items : [{
			title : 'Upload Page',
			xtype : 'panel',
			items : [{
				xtype : 'panel',
				fileUpload : true,
				height : 500,
				defaults : {
					anchor : '95%',
					allowBlank : false,
					msgTarget : 'side'
				},
				items : [{
					xtype : 'panel',
					autoScroll : true,
					tbar : ['System to proccess: ', scraping.combos.System8,
							'|', 'Origen data: ', {
								xtype : 'combo',
								fieldLabel : 'Server',
								displayField : 'name',
								value : '1',
								id : 'OrigenData',
								valueField : 'id',
								store : new Ext.data.SimpleStore({
									fields : ['id', 'name'],
									data : [
											['1', 'Upload'],
											['2', 'On Server (/var/xima/page/)']]
								}),
								width : 100,
								triggerAction : 'all',
								mode : 'local'
							}, {
								text : 'Add Other',
								handler : function() {
									console.debug($('#orginItemsResult :file'));
									var ind = $('#orginItemsResult :file').length;
									$('#orginItemsResult')
											.append('<div><input name="file['
													+ ind
													+ ']" type="file" /></div>');
								}
							}, {
								text : 'Save',
								handler : function() {
									scraping.loadMask.show();
									var OrigenData = Ext.getCmp('OrigenData')
											.getValue();
									if (OrigenData == 1) {
										var formData = new FormData($('#orginItemsResult')[0]);
										function progressHandlingFunction(e) {
											if (e.lengthComputable) {
												$('progress').attr({
															value : e.loaded,
															max : e.total
														});
											}
										}

									} else {
										var formData = new FormData();
									}

									formData.append("idOrigin",
											scraping.combos.System8.getValue());
									formData.append("OrigenData", OrigenData);
									$.ajax({
										url : OrigenData == 1
												? 'php/funcionesScraping.php'
												: 'https://extractormaster.ddns.net/mant/scraping/proccessPage.php',
										type : OrigenData == 1 ? 'POST' : 'get',
										dataType : OrigenData == 1
												? 'html'
												: 'jsonp',
										/*
										 * xhr: function() { // Custom
										 * XMLHttpRequest var myXhr =
										 * $.ajaxSettings.xhr();
										 * if(myXhr.upload){ // Check if upload
										 * property exists
										 * myXhr.upload.addEventListener('progress',progressHandlingFunction,
										 * false); // For handling the progress
										 * of the upload } return myXhr; },
										 */
										// Ajax events
										beforeSend : function() {
										},
										success : function(res) {
											scraping.loadMask.hide();
											$("#orginItemsResultHtml")
													.html(res);
										},
										error : function() {

										},
										data : OrigenData == 1
												? formData
												: 'idOrigin='
														+ scraping.combos.System8
																.getValue()
														+ '&OrigenData='
														+ OrigenData,
										cache : false,
										contentType : false,
										processData : false
									});

								}
							}, {
								text : 'Reset',
								handler : function() {
									fp.getForm().reset();
								}
							}],
					html : '<form enctype="multipart/form-data" id="orginItemsResult">'
							+ '<div><input name="file[0]" type="file" /></div>'
							+ '<div><input name="option" value="RecordPage" type="hidden" /></div>'
							+ '</form>'
							+ '<div id="orginItemsResultHtml"></div>'
				}]
			}]
		}/*
			 * , { title : 'Stadistica Total', xtype : 'panel', layout : 'fit',
			 * items : [{ xtype : 'panel', height :
			 * Ext.getBody().getViewSize().height - 100, items : [scraping.grid] }] }
			 */, {
			title : 'Generec Url',
			xtype : 'tabpanel',
			activeTab : 0,
			height : Ext.getBody().getViewSize().height - 100,
			items : [{
				title : 'Nivel 1',
				xtype : 'panel',
				height : Ext.getBody().getViewSize().height - 150,
				items : [{
					xtype : 'form',
					height : 120,
					border : false,
					frame : true,
					items : [{
						xtype : 'fieldset',
						title : 'Config',
						style : {
							background : '#FFF'
						},
						layout : 'column',
						items : [{
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.States]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.Countys]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.System]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.DateSelect]
								}],
						buttons : [{
							text : 'Generate',
							scope : this,
							handler : function() {
								scraping.loadMask.show();
								$.ajax({
									url : 'php/funcionesScraping.php',
									dataType : 'json',
									type : 'post',
									data : {
										option : 'getUrlScrapin',
										state : scraping.combos.States
												.getValue(),
										county : scraping.combos.Countys
												.getValue(),
										date : scraping.combos.DateSelect
												.getValue(),
										system : scraping.combos.System
												.getValue()
									},
									success : function(res) {
										scraping.loadMask.hide();
										var $textarea = $('<textarea cols="120" rows="6"></textarea>');
										var $containerUrls = $('#containerUrls');
										// var
										// NBloque=Ext.getCmp('NBloque').getValue();
										var total = 0;
										var urls = '';
										$containerUrls.empty();
										/*
										 * Ext.each(res.data,function (el,ind){
										 * total++; //$containerUrls.append('<p>'+e+'</p>');
										 * urls+='\n'+el; }) console.debug(res);
										 * console.debug(res.data);
										 */
										$.each(res.data, function(i, item) {
											total++;
											// $containerUrls.append('<p>'+e+'</p>');
											urls += '\n' + item;
												/*
												 * if(total>NBloque){
												 * $textarea.appendTo('#containerUrls');
												 * var $textarea=$('<textarea></textarea>');
												 * total=0; }
												 */
											});
										console.debug($textarea);
										$textarea.appendTo('#containerUrls');
										$textarea.val(urls);
										Ext.getCmp('containerUrlsExt')
												.doLayout();
									}
								});
							}
						}]
					}]
				}, {
					xtype : 'panel',
					height : 800,
					autoScroll : true,
					id : 'containerUrlsExt',
					html : '<div id="containerUrls"></div>'
				}]
			}, {
				title : 'Nivel 2',
				xtype : 'panel',
				items : [{
					xtype : 'form',
					height : 120,
					border : false,
					frame : true,
					items : [{
						xtype : 'fieldset',
						title : 'Config',
						style : {
							background : '#FFF'
						},
						layout : 'column',
						items : [{
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.States3]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.Countys3]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.System3]
								}/*
									 * ,{ layout: 'form', columnWidth: .25,
									 * items:[ { fieldLabel : 'bloque', xtype :
									 * 'textfield', id : 'NBloqueN2', width : 60 } ] }
									 */
						],
						buttons : [{
							text : 'Generate',
							scope : this,
							handler : function() {
								scraping.loadMask.show();
								$.ajax({
									url : 'https://extractormaster.ddns.net/mant/scraping/generecUrlN2.php',
									dataType : 'jsonp',
									type : 'get',
									data : {
										option : 'getUrlScrapin',
										state : scraping.combos.States3
												.getValue(),
										county : scraping.combos.Countys3
												.getValue(),
										system : scraping.combos.System3
												.getValue()
									},
									success : function(res) {
										scraping.loadMask.hide();
										var $textarea = $('<textarea cols="120" rows="6"></textarea>');
										var $containerUrls = $('#containerUrlsN2');
										// var
										// NBloque=Ext.getCmp('NBloque').getValue();
										var total = 0;
										var urls = '';
										$containerUrls.html(res.html);
										$.each(res.url, function(i, e) {
											total++;
											// $containerUrls.append('<p>'+e+'</p>');
											urls += '\n' + e;
												/*
												 * if(total>NBloque){
												 * $textarea.appendTo('#containerUrlsN2');
												 * var $textarea=$('<textarea></textarea>');
												 * total=0; }
												 */
											});
										$textarea.val(urls)
												.appendTo('#containerUrlsN2');
										Ext.getCmp('containerUrlsExtN2')
												.doLayout();
									}
								});
							}
						}]
					}]
				}, {
					xtype : 'panel',
					height : 500,
					autoScroll : true,
					id : 'containerUrlsExtN2',
					html : '<div id="containerUrlsN2"></div>'
				}]
			}]
		}, {
			title : 'S3 CSV process',
			xtype : 'tabpanel',
			activeTab : 0,
			height : Ext.getBody().getViewSize().height - 100,
			items : [{
				title : 'Nivel 1',
				xtype : 'panel',
				layout : 'anchor',
				items : [{
					xtype : 'form',
					height : 120,
					border : false,
					frame : true,
					items : [{
						xtype : 'fieldset',
						title : 'Config',
						style : {
							background : '#FFF'
						},
						layout : 'column',
						items : [{
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.States7]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.System7]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Server',
										displayField : 'name',
										value : 'https://extractormaster.ddns.net/mant/',
										id : 'ServerActionS3N1',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
											fields : ['id', 'name'],
											data : [
													[
															'https://extractormaster.ddns.net/mant/',
															'Extractor 1'],
													[
															'https://extractor3reifax.ddns.net/mant/',
															'Extractor 3']]
										}),
										width : 100,
										triggerAction : 'all',
										mode : 'local'
									}]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Action',
										displayField : 'name',
										value : '1',
										id : 'filterActionS3N1',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
													fields : ['id', 'name'],
													data : [
															['1', 'Up'],
															['2',
																	'Down & Proccess'],
															['3',
																	'Proccess Full']]
												}),
										width : 100,
										triggerAction : 'all',
										mode : 'local'
									}]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Old File',
										displayField : 'name',
										value : '2',
										id : 'filterFileS3N1',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
													fields : ['id', 'name'],
													data : [['1', 'keep'],
															['2', 'Delete']]
												}),
										width : 100,
										triggerAction : 'all',
										mode : 'local'
									}]
								}],
						buttons : [{
							text : 'Proccess',
							scope : this,
							handler : function() {
								scraping.loadMask.show();
								var action = Ext.getCmp('filterActionS3N1')
										.getValue();
								if (action == 3) {
									var forInit = 1;
									var forEnd = 3
								} else if (action == 2) {
									var forInit = 2;
									var forEnd = 3
								} else if (action == 1) {
									var forInit = 1;
									var forEnd = 2
								}
								$("#containerResponses3ProcesN1").empty();
								for (var i = forInit; i < forEnd; i++) {
									$.ajax({
										url : Ext.getCmp('ServerActionS3N1')
												.getValue()
												+ 'scraping/downUpsS3.php',
										timeout : 30000000,
										async : false,
										type : 'get',
										dataType : 'jsonp',
										data : {
											option : 'N1',
											state : scraping.combos.States7
													.getValue(),
											system : scraping.combos.System7
													.getValue(),
											oldFile : Ext
													.getCmp('filterFileS3N1')
													.getValue(),
											action : i
										},
										success : function(data) {
											scraping.loadMask.hide();
											$("#containerResponses3ProcesN1")
													.append(data.html);
											Ext
													.getCmp('ExcontainerResponses3ProcesN1')
													.doLayout();
										}
									});
								};
							}
						}]
					}]
				}, {
					xtype : 'panel',
					height : 800,
					anchor : '100%',
					auto : true,
					layout : 'hbox',
					layoutConfig : {
						padding : '5',
						align : 'top'
					},
					items : [{
								xtype : 'panel',
								flex : 1,
								items : []
							}, {
								xtype : 'panel',
								height : 800,
								flex : 1,
								autoScroll : true,
								id : 'ExcontainerResponses3ProcesN1',
								html : '<h3>Console</h3><div id="containerResponses3ProcesN1"></div>'
							}]
				}]

			}, {
				title : 'Nivel 2',
				xtype : 'panel',
				layout : 'anchor',
				items : [{
					xtype : 'form',
					height : 120,
					border : false,
					frame : true,
					items : [{
						xtype : 'fieldset',
						title : 'Config',
						style : {
							background : '#FFF'
						},
						layout : 'column',
						items : [{
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.States4]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [scraping.combos.System4]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Server',
										displayField : 'name',
										value : 'https://extractormaster.ddns.net/mant/',
										id : 'ServerActionS3N2',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
											fields : ['id', 'name'],
											data : [
													[
															'https://extractormaster.ddns.net/mant/',
															'Extractor 1'],
													[
															'https://extractor3reifax.ddns.net/mant/',
															'Extractor 3']]
										}),
										width : 100,
										triggerAction : 'all',
										mode : 'local'
									}]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Action',
										displayField : 'name',
										value : '1',
										id : 'filterActionS3N2',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
													fields : ['id', 'name'],
													data : [
															['1', 'Up'],
															['2', 'Down'],
															['3',
																	'Proccess Full']]
												}),
										width : 100,
										triggerAction : 'all',
										mode : 'local'
									}]
								}, {
									layout : 'form',
									columnWidth : .25,
									items : [{
										xtype : 'combo',
										fieldLabel : 'Old File',
										displayField : 'name',
										value : '2',
										id : 'filterFileS3',
										valueField : 'id',
										store : new Ext.data.SimpleStore({
													fields : ['id', 'name'],
													data : [['1', 'keep'],
															['2', 'Delete']]
												}),
										width : 70,
										triggerAction : 'all',
										mode : 'local'
									}]
								}],
						buttons : [{
							text : 'List',
							scope : this,
							handler : function() {
								var grid = Ext.getCmp('gridListCsvN2');
								var store = grid.getStore();
								store.load({
											params : {
												type : 2,
												state : scraping.combos.States4
														.getValue(),
												system : scraping.combos.System4
														.getValue(),
												action : Ext
														.getCmp('filterActionS3N2')
														.getValue()
											}

										});
							}
						}, {
							text : 'Proccess',
							scope : this,
							handler : function() {
								scraping.loadMask.show();

								scraping.loadMask.show();
								var action = Ext.getCmp('filterActionS3N2')
										.getValue();
								if (action == 3) {
									var forInit = 1;
									var forEnd = 3
								} else if (action == 2) {
									var forInit = 2;
									var forEnd = 3
								} else if (action == 1) {
									var forInit = 1;
									var forEnd = 2
								}
								$("#containerResponses3ProcesN2").empty();

								var selec = Ext.getCmp('gridListCsvN2').selModel
										.getSelections();
								var i = 0;
								var marcados = new Array();
								for (i = 0; i < selec.length; i++) {
									marcados.push(selec[i].data.ids3_directory);
								}
								marcados = marcados.join(',');
								console.debug(marcados);
								scraping.SelectionModels.gridN2csv
								for (var i = forInit; i < forEnd; i++) {
									$.ajax({
										url : Ext.getCmp('ServerActionS3N2')
												.getValue()
												+ '/scraping/downUpsS3.php',
										timeout : 30000000,
										async : false,
										type : 'get',
										dataType : 'jsonp',
										data : {
											option : 'N2',
											state : scraping.combos.States4
													.getValue(),
											system : scraping.combos.System4
													.getValue(),
											oldFile : Ext
													.getCmp('filterFileS3')
													.getValue(),
											files : marcados,
											action : action
										},
										success : function(data) {
											scraping.loadMask.hide();
											$("#containerResponses3ProcesN2")
													.html(data.html);
											Ext
													.getCmp('ExcontainerResponses3ProcesN2')
													.doLayout();
										}
									});
								}
							}
						}]
					}]
				}, {
					xtype : 'panel',
					height : Ext.getBody().getViewSize().height - 200,
					anchor : '100%',
					auto : true,
					layout : 'hbox',
					layoutConfig : {
						padding : '5',
						align : 'top'
					},
					items : [{
						xtype : 'grid',
						height : Ext.getBody().getViewSize().height - 200,
						id : 'gridListCsvN2',
						flex : 2,
						viewConfig : {
							forceFit : true
						},
						title : 'List',
						store : {
							url : 'php/funcionesScraping.php',
							root : 'data',
							baseParams : {
								option : 'getFiles'
							},
							totalProperty : 'total',
							reader : new Ext.data.JsonReader({
										root : 'data',
										totalProperty : 'total',
										fields : ['ids3_directory', 'ruta',
												'date', 'size_original',
												'size_compressed']
									})
						},
						listeners : {
							load : function(a, b, c, d, e) {
								console.debug(a, b, c, d, e);
							}
						},
						cm : new Ext.grid.ColumnModel([
								new Ext.grid.RowNumberer(),
								scraping.SelectionModels.gridN2csv, {
									header : 'File',
									dataIndex : 'ruta',
									width : 400,
									sortable : true
								}, {
									header : 'Date',
									dataIndex : 'date',
									sortable : true
								}, {
									header : 'Size Csv',
									dataIndex : 'size_original',
									sortable : true
								}, {
									header : 'Size Compressed',
									dataIndex : 'size_compressed',
									sortable : true
								}]),
						sm : scraping.SelectionModels.gridN2csv,
						border : false,
						stripeRows : true
					}, {
						xtype : 'panel',
						layout : 'accordion',
						height : Ext.getBody().getViewSize().height - 250,
						flex : 1,
						items : [{
							xtype : 'panel',
							autoScroll : true,
							title : 'Console',
							id : 'ExcontainerResponses3ProcesN2',
							html : '<div id="containerResponses3ProcesN2"></div>'
						}, {
							xtype : 'grid',
							title : 'Task',
							store : {
								root : 'data',
								totalProperty : 'total',
								fields : ['city', 'visits', 'pageVisits',
										'averageTime']
							},
							columns : [new Ext.grid.RowNumberer(), {
										header : 'State',
										dataIndex : 'state',
										sortable : true
									}, {
										header : 'File',
										dataIndex : 'file',
										sortable : true
									}, {
										header : 'Server',
										dataIndex : 'server',
										sortable : true
									}, {
										header : 'Date',
										dataIndex : 'averageTime',
										sortable : true
									}],
							border : false,
							stripeRows : true
						}]
					}]
				}]
			}]
		}/*
			 * , { title : 'Record Job', xtype : 'panel', height :
			 * Ext.getBody().getViewSize().height - 100, items : [{ xtype :
			 * 'form', height : 120, border : false, frame : true, items : [{
			 * xtype : 'fieldset', title : 'New', style : { background : '#FFF' },
			 * layout : 'column', items : [{ layout : 'form', columnWidth : .2,
			 * items : [scraping.combos.States6] }, { layout : 'form',
			 * columnWidth : .2, items : [scraping.combos.System6] }, { layout :
			 * 'form', columnWidth : .6, items : [{ xtype : 'textfield', id :
			 * 'countRecordJob', fieldLabel : 'cantidad', width : 60 }] }, {
			 * layout : 'form', columnWidth : .2, items : [{ xtype :
			 * 'datefield', format : 'Y-m-d', id : 'dateInitRecord', fieldLabel :
			 * 'Date Start', altFormats : 'Y-m-d', maxValue : new Date() }] }, {
			 * layout : 'form', columnWidth : .2, items : [{ fieldLabel : 'Hour
			 * Start', xtype : 'textfield', id : 'hstartJob', regex :
			 * /\d{2}:\d{2}:\d{2}/, width : 60 }] }, { layout : 'form',
			 * columnWidth : .2, items : [{ xtype : 'datefield', format :
			 * 'Y-m-d', id : 'dateEndRecord', altFormats : 'Y-m-d', fieldLabel :
			 * 'End' }] }, { layout : 'form', columnWidth : .25, items : [{
			 * fieldLabel : 'Hour End', xtype : 'textfield', regex :
			 * /\d{2}:\d{2}:\d{2}/, id : 'hendJob', width : 60 }] }] }], buttons : [{
			 * text : 'Record', scope : this, handler : function() {
			 * scraping.loadMask.show(); $.ajax({ url :
			 * 'php/funcionesScraping.php', dataType : 'json', type : 'post',
			 * data : { option : 'RecordJob', init :
			 * Ext.util.Format.date(Ext.getCmp('dateInitRecord').getValue(),
			 * 'Y-m-d'), end :
			 * Ext.util.Format.date(Ext.getCmp('dateEndRecord').getValue(),
			 * 'Y-m-d'), inith : Ext.getCmp('hstartJob').getValue(), endh :
			 * Ext.getCmp('hendJob').getValue(), count :
			 * Ext.getCmp('countRecordJob').getValue(), state :
			 * scraping.combos.States6.getValue(), system :
			 * scraping.combos.System6.getValue() }, success : function(res) {
			 * scraping.loadMask.hide(); if (res.success) { alert('registro
			 * cargado con exito'); } else { alert('error al procesar'); } } }); } }] },
			 * scraping.grid.recordsJob] }
			 */, {
			title : 'Diary ',
			xtype : 'panel',
			height : Ext.getBody().getViewSize().height - 100,
			items : [{
				xtype : 'editorgrid',
				height : Ext.getBody().getViewSize().height - 100,
				flex : 2,
				stripeRows : true,
				viewConfig : {
					forceFit : true
				},
				title : 'List',
				loadMask : true,
				tbar : scraping.pagers.diary,
				store : scraping.GStore.diary,
				listeners : {
					load : function(a, b, c, d, e) {
						console.debug(a, b, c, d, e);
					},
					afteredit : function(oGrid_Event) {
						console.debug(oGrid_Event);
						var gid = oGrid_Event.record.data.ids3_directory;
						var gfield = oGrid_Event.field;
						var gvaluenew = oGrid_Event.value;
						Ext.Ajax.request({
									url : 'php/funcionesScraping.php',
									method : 'POST',
									params : {
										id : gid,
										option : 'updateS3Directory',
										field : gfield,
										valuenew : gvaluenew
									},
									waitTitle : 'Please wait!',
									waitMsg : 'Saving changes...',
									failure : function(response, options) {
										Ext.MessageBox.alert('Warning',
												'Error editing');
									},
									success : function(respuesta) {
										scraping.GStore.diary.commitChanges();
										return true;
									}
								});
					}
				},
				cm : new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
						scraping.SelectionModels.gridN2csv, {
							header : 'Id File',
							dataIndex : 'ids3_directory',
							width : 40,
							sortable : true
						}, {
							header : 'File',
							dataIndex : 'ruta',
							width : 400,
							sortable : true
						}, {
							header : 'State',
							dataIndex : 'state',
							sortable : true
						}, {
							header : 'System',
							dataIndex : 'system',
							sortable : true
						}, {
							header : 'Date',
							dataIndex : 'date',
							sortable : true
						}, {
							header : 'Type',
							dataIndex : 'type',
							width : 40,
							sortable : true,
							renderer : function(val) {
								if (val == '1') {
									return 'Nivel 1';
								}
								if (val == '2') {
									return 'Nivel 2';
								}
								if (val == '3') {
									return 'Actual Nivel 1';
								}
								if (val == '4') {
									return 'Old Nivel 1';
								}
								if (val == '5') {
									return 'Backup Nivel 1';
								}
								if (val == '7') {
									return 'Nivel 2 Procesado';
								}
							}
						}, {
							header : 'Items',
							dataIndex : 'items',
							width : 40,
							align : 'right',
							sortable : true
						}, {
							header : 'Size Csv',
							dataIndex : 'size_original',
							width : 50,
							sortable : true,
							align : 'right',
							renderer : Ext.util.Format.fileSize
						}, {
							header : 'Size Compressed',
							dataIndex : 'size_compressed',
							width : 50,
							align : 'right',
							sortable : true,
							renderer : Ext.util.Format.fileSize
						}, {
							header : 'Server',
							dataIndex : 'server',
							width : 60,
							sortable : true
						}, {
							header : 'S3',
							dataIndex : '2',
							width : 30,
							sortable : true,
							renderer : scraping.controllers.renders.colorActionStatus
						}, {
							header : 'Spider',
							dataIndex : '3',
							width : 30,
							sortable : true,
							renderer : scraping.controllers.renders.colorActionStatus
						}, {
							header : 'CSV',
							dataIndex : '4',
							width : 30,
							sortable : true,
							renderer : scraping.controllers.renders.colorActionStatus
						}, {
							editor : {
								xtype : 'checkbox'
							},
							header : 'Process',
							width : 40,
							xtype : 'booleancolumn',
							tooltip : 'Process',
							trueText : 'Yes',
							falseText : 'No',
							align : 'center',
							sortable : true,
							dataIndex : 'process'
						}]),
				sm : scraping.SelectionModels.gridN2csv,
				border : false,
				stripeRows : true

			}]
		}, {
			title : 'Server Control',
			xtype : 'tabpanel',
			activeTab : 0,
			height : Ext.getBody().getViewSize().height - 100,
			items : [{
				title : 'System by Server',
				xtype : 'panel',
				items : [{
					xtype : 'editorgrid',
					stripeRows : true,
					store : scraping.GStore.systemByServer,
					height : Ext.getBody().getViewSize().height - 150,
					listeners : {
						load : function(a, b, c, d, e) {
							console.debug(a, b, c, d, e);
						},
						afteredit : function(oGrid_Event) {
							console.debug(oGrid_Event);
							var gid = oGrid_Event.record.data.id;
							var gfield = oGrid_Event.field;
							var gvaluenew = oGrid_Event.value;
							Ext.Ajax.request({
										url : 'php/funcionesScraping.php',
										method : 'POST',
										params : {
											origen : gid,
											option : 'updatesystemByServer',
											server : gfield,
											gvaluenew : gvaluenew
										},
										waitTitle : 'Please wait!',
										waitMsg : 'Saving changes...',
										failure : function(response, options) {
											Ext.MessageBox.alert('Warning',
													'Error editing');
										},
										success : function(respuesta) {
											scraping.GStore.systemByServer
													.commitChanges();
											return true;
										}
									});
						}
					},
					cm : new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
							{
								header : 'Origen',
								dataIndex : 'name',
								width : 300,
								sortable : true
							}, {
								editor : {
									xtype : 'checkbox'
								},
								header : 'Extractor 1',
								width : 100,
								//xtype : 'booleancolumn',
								renderer : scraping.controllers.renders.cellCheck,
								tooltip : 'Extractor 1',
								trueText : 'Active',
								falseText : 'No',
								align : 'center',
								sortable : true,
								dataIndex : '18'

							}, {
								editor : {
									xtype : 'checkbox'
								},
								header : 'Extractor 3',
								width : 100,
								//xtype : 'booleancolumn',
								renderer : scraping.controllers.renders.cellCheck,
								tooltip : 'Extractor 3',
								trueText : 'Active',
								falseText : 'No',
								align : 'center',
								sortable : true,
								dataIndex : '21'

							}])
				}]
			}, {
				title : 'State by System',
				xtype : 'panel',
				items : [{
					xtype : 'editorgrid',
					stripeRows : true,
					store : scraping.GStore.stateBySystem,
					height : Ext.getBody().getViewSize().height - 150,

					viewConfig : {
						forceFit : true
					},
					layout : 'fit',
					listeners : {
						load : function(a, b, c, d, e) {
							console.debug(a, b, c, d, e);
						},
						afteredit : function(oGrid_Event) {
							console.debug(oGrid_Event);
							var idstate = oGrid_Event.record.data.idstate;
							var gfield = oGrid_Event.field.split('_');
							var gvaluenew = oGrid_Event.value;
							Ext.Ajax.request({
										url : 'php/funcionesScraping.php',
										method : 'POST',
										params : {
											idstate : idstate,
											option : 'updatestateBySystem',
											origen : gfield[1],
											gvaluenew : gvaluenew
										},
										waitTitle : 'Please wait!',
										waitMsg : 'Saving changes...',
										failure : function(response, options) {
											Ext.MessageBox.alert('Warning',
													'Error editing');
										},
										success : function(respuesta) {
											scraping.GStore.stateBySystem
													.commitChanges();
											return true;
										}
									});
						}
					},
					cm : scraping.utility.stateBySystem
				}]
			}]
		}]
	});
	scraping.marco = new Ext.Viewport({
				layout : 'border',
				hideBorders : true,
				monitorResize : true,
				items : [{
							region : 'north',
							height : 25,
							items : Ext.getCmp('menu_page')
						}, {
							region : 'center',
							items : scraping.tabs,
							height : 500,
							autoHeight : true
						}]
			});
}
scraping.utility = {};
scraping.var = {
	fieldsStoreSystem : ['idstate', 'name']
};

scraping.prepare = function() {

	Ext.Ajax.request({
		url : 'php/funcionesScraping.php',
		type : 'POST',
		params : {
			option : 'getSystem'
		},
		scope : this,
		success : function(response) {
			var data = Ext.util.JSON.decode(response.responseText);
			var temCm = [new Ext.grid.RowNumberer(), {
						header : "IdState",
						width : 25,
						align : 'center',
						sortable : true,
						dataIndex : 'idstate'
					}, {
						header : "State",
						dataIndex : 'name',
						sortable : true
					}];

			Ext.each(data.data, function(data, i) {
						temCm.push({
									editor : {
										xtype : 'checkbox'
									},
									header : data.name,
									width : 45,
									//xtype : 'booleancolumn',
									tooltip : data.name,
									trueText : 'Yes',
									falseText : 'No',
									align : 'center',
									sortable : true,
									renderer : scraping.controllers.renders.cellCheck,
									dataIndex : 'system_' + data.id

								});
						scraping.var.fieldsStoreSystem.push({
									name : 'system_' + data.id
								});
					});
			scraping.utility.stateBySystem = new Ext.grid.ColumnModel(temCm);

			scraping.GStore.stateBySystem = new Ext.data.Store({
						url : 'php/funcionesScraping.php',
						root : 'data',
						baseParams : {
							option : 'getstateBySystem'
						},
						autoLoad : true,
						autoSave : true,
						totalProperty : 'total',
						reader : new Ext.data.JsonReader({
									root : 'data',
									totalProperty : 'total',
									fields : scraping.var.fieldsStoreSystem
								})
					});
			scraping.init();
		}
	});
}
Ext.onReady(scraping.prepare);
Ext.QuickTips.init();
