Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=contracts',
		fields: [
			{name: 'id', type: 'int'}
			,'name'
			,'filename'
			,'signature' 
			,{name: 'userid', type: 'int'} 
			,'name_user'
			,'date_upload'
			,'signature'	
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display"/*,
        
        items:[
			{
				id: 'add_butt',
                tooltip: 'Click to Add Row',
				iconCls:'icon',
				icon: 'includes/img/add.gif',
                handler: doAdd 
			}
		]*/
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	getSignature=function (idcontract){
		Ext.Ajax.request({  
			waitMsg		:'Wait please...',
			url		:'php/grid_search.php', 
			method		:'POST', 
			params		:{ 
							tipo	:"contracts",
							idvalue	: idcontract
						},					
			failure		:function(response,options){
							Ext.MessageBox.alert('Warning','Error user show');
						},
						
			success		:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);						
							if(rest.succes==false){
								Ext.MessageBox.alert('Warning',rest.msg); return;
							}							
							arrtask=rest.results;
							var url='http://reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+arrtask[0].filename;
							//alert(url);
							window.open(url);
						}                                
		});
		
	};
	uploadContract=function (idcontract){
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			frame:true,
			fileUpload : true,
			title: 'Upload Signed Contract',
			bodyStyle:'padding:5px 5px 0',
			items: [{
					xtype: 'combo',
					name: 'ctype',
					id: 'ctype',
					hiddenName: 'type',
					store: new Ext.data.SimpleStore({
							fields: ['idtype', 'name'],
							data : [
							['ContractPurchase', 'Contract Purchase'],
							['leadBasedPaint', 'Lead Based Paint'],
							['ResidentialContract', 'Residential Contract'],
							['Other', 'Other']
						]
					}),
					editable: false,
					displayField: 'name',
					valueField: 'idtype',
					typeAhead: true,
					fieldLabel: '(*) Select Type',
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Select ...',
					selectOnFocus:true,
					allowBlank:false,
					value:'ContractPurchase',
					width: 200
				},{
					xtype       :'fileuploadfield',
					id          :'idupload',
					emptyText   :'Select pdf',
					fieldLabel  :'Upload File',
					name        :'pdfupload',
					buttonText  : 'Browse...'
				},{
					xtype		:'hidden',
					name		:'idcontract',
					hiddenName	:'idcontract',
					value		: idcontract
				}
			],
	
			buttons: [{
				text: 'Upload',
				handler  : function(){
						if(simple.getForm().isValid()) {
							simple.getForm().submit({
								url     : 'php/uploadpdf.php',
								waitMsg : 'Saving...',
								success : function(f, a){ 
									var resp    = a.result;
									if (resp.mensaje == 'Contract saved successfully!') {
										wind.close();
										store.load({params:{start:0, limit:200}});
									}
									Ext.MessageBox.alert('', resp.mensaje);
									
								},
								failure : function(response, o){ }	
							});
						}	
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
						wind.close();
                    }
			}]
		});
		wind = new Ext.Window({
			title		: 'Template',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 400,
			height      : 220,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: simple
		});
				
		wind.show();
	};
	DeleteWindows=function (selected)
	{
		idgridsel=selected;
		Ext.MessageBox.confirm('Delete', 'Confirm deletion?', deleteRow);
	}
	function deleteRow(btn){
		if(btn=='no')
			return;
		Ext.Ajax.request({
			waitTitle	:'Please wait..',
          	waitMsg		:'Sending data...',
			url			:'php/grid_del.php', 
			method		:'POST', 
			params		:{
							tipo: "contracts", 
							ID: idgridsel,
							key: "id"
						},
			success		:function(response,options){
							store.reload();
							var d = Ext.util.JSON.decode(response.responseText);
							Ext.MessageBox.alert('Success',d.errors.reason);
						},
			failure		:function(response,options){
							var d = Ext.util.JSON.decode(response.responseText);
							Ext.Msg.alert('Failure', d.errors.reason);
						}
		});		
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
		var idc=record.data.id;
		var name = record.data.filename;
		return String.format(
				'<a href="javascript:void(0)" title="Click to get signed contract." onclick="getSignature({0})"><img src="images/notes.png"></a>'+
				'&nbsp;<a href="javascript:void(0)" title="Click to upload signed contract." onclick="uploadContract({0})"><img src="images/up.png"></a>'+
				'&nbsp;<a href="javascript:void(0)" title="Click to delete row." onclick="DeleteWindows({0})"><img src="images/cross.gif"></a>',value);
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,{id:'id',header: "ID", width: 50, align: 'center', sortable: true, dataIndex: 'id'}
			,{header: 'User ID', width: 80, sortable: true, align: 'center', dataIndex: 'userid'}
			,{header: "Name User", width: 270, align: 'left', sortable: true, dataIndex: 'name_user'}
			,{header: "Contract", width: 250, align: 'left', sortable: true, dataIndex: 'name'}
			,{header: "Filename", width: 250, align: 'left', sortable: true, dataIndex: 'filename'}
			,{header: 'Date', width: 80, sortable: true, align: 'center', dataIndex: 'date_upload'}
			,{header: 'Signed', width: 80, sortable: true, align: 'center', dataIndex: 'signature'}
			,{header: "Oper", width: 80,  sortable: false, align: 'center', dataIndex: 'id',renderer: renderTopic}			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,//'99.8%',
		frame:true,
		title:'Contracts',
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	//grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});