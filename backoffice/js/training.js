// JavaScript Document
Ext.ns('training');

training=
{
	init: function ()
	{
		training.gridReader = new Ext.data.JsonReader({  
			root: 'videos', 
			totalProperty: 'total',
			id:'readertraining'
			},
			training.record 
			);
			
		training.store = new Ext.data.Store({  
			id: 'training',  
			proxy: training.dataProxy,  
			baseParams: {
				accion	:'list',
				typeHelp:typeHelp
			},  
			reader: training.gridReader  
		});
		
		training.storeUserAdmin= new Ext.data.SimpleStore({
			fields: ['userid', 'name'],
			data : Ext.combos_selec.dataUsersAdmin
		});
		training.comboUser = new Ext.form.ComboBox({
			store: training.storeUserAdmin,
			hiddenName	: 'userPro',
			valueField: 'userid',
			displayField:'name',
			 style: {
                fontSize: '16px'
            },
			typeAhead: true,
			triggerAction: 'all',
			mode: 'local',
			selectOnFocus:true
		})
		
		training.pager = new Ext.PagingToolbar({  
			store: training.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 100,
			items:[ 
				{
					xtype	:'button',
					iconCls	:'x-icon-add',
					handler	: training.new.init,
					text	: 'New training'
				},
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: training.delete,
					text	: 'Delete'
				},
				'|'
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-save',
					handler	: training.save,
					text	: 'Save Changes'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: training.cancel,
					text	: 'Cancel Change'
				},
				'|'
				,
				{
					xtype:'form',
					id:'filterTrainig',
					width: 470,
					layout :'column',
					padding: 3,
					style	:{
						background:'none'
					},
					border: false,
					items:[{
							layout: 'form',
							columnWidth: .40,
							labelWidth: 45,
							border: false,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Status'
								,displayField:'name'
								,value :'in (1,2,3,4,5)'
								,name:'status'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['in (1,2,3,4,5)',	'All'],
										['in (1)',	'Pending'],
										['in (2)', 		'Created'],
										['in (3)',	'Redo'],
										['in (4)', 		'Checked'],
										['in (5)', 		'uploaded ']
									]
								})
								,width: 70
								,triggerAction:'all'
								,mode:'local'
							}]
					},{
							layout: 'form',
							columnWidth: .60,
							labelWidth: 45,
							border: false,
							items:[{
								name		: 'search',
								xtype		: 'textfield',
								fieldLabel	: 'Search',
								width	: 200
							}]
					}
					]
				},
				{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						training.store.load({
							params:{
								status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
								search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
							}
						})
					}
				}
			], 
			listeners: {
			}
		});
		
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {
				forceSelection:true,
				minValue: '8:00 AM',
				maxValue: '6:00 PM',
				increment: 30});
				
		var opcionesCombo= [
        ['1', 'Pending'],
        ['2', 'Created'] ,
        ['3', 'Redo'],
        ['4', 'Checked'] ,
        ['5', 'Uploaded ']];
		
		var opcionesStatus= {
			1	:	'Pending',
			2	:	'Created',
			3	:	'Redo',
			4	:	'Checked',
			5	:	'Uploaded'
		}
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		
		
		training.store.load();	
		training.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
				training.sm,
			{  
				header: 'Id training',  
				dataIndex: 'id', 
				sortable: true,  
				width: 25  
			},{  
				header: 'Made By',  
				dataIndex: 'madeBy',
				editor	: training.comboUser, 
				renderer:function (val){
					var record = training.comboUser.findRecord(training.comboUser.valueField, val);
					return record ? record.get(training.comboUser.displayField) : val;
				},
				sortable: true,
				width: 70
			},{
				xtype: 'actioncolumn',
				width: 30,
				items: [
					{
						icon   : 'http://www.archivalencia.org/documentos/ficheros_audios/icon-play.png',                // Use a URL in the icon config
						tooltip: 'View Video',
						handler: function(grid, rowIndex, colIndex) {
							var rec = training.store.getAt(rowIndex);
							viewtraining(rec.get('idYoutube'));
						}
					}
				]
			},{  
				header		: 'Name',  
				dataIndex	: 'name', 
				sortable	: true,
				editor		:textField,
				width		: 150
			},{  
				header: 'Url Youtube',  
				dataIndex: 'urlVideo', 
				sortable: true,
				editor		:textField,
				renderer:function (val){
					var regex=/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
					if(regex.test(val))
						return val;
					else if(val)
						return 'http://www.youtube.com/embed/'+val;
						//return '<a href="http://www.youtube.com/embed/'+val+'?rel=0" onClick="viewtraining(\''+val+'\')">http://www.youtube.com/embed/'+val+'?rel=0</a>';
					else
						return 'without creating';
				},
				width: 130
			},{
				header: 'status',  
				dataIndex: 'status',
				editor	: combo, 
				renderer:function (val){
					if(opcionesStatus[val]){
						return opcionesStatus[val];
					}
					else{
						return val;
					}
				},
				sortable: true,
				width: 30
			},{  
				header: 'Date',  
				dataIndex: 'fecha', 
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				sortable: true,
				width: 50
			},{  
				header		: 'Observations',  
				dataIndex	: 'observations', 
				sortable	: true,
				editor		:textField,
				width		: 180
			},{  
				header		: 'help',  
				dataIndex	: 'modules', 
				sortable	: true,
				width		: 180,
				renderer:function (val,element, record){
					if (val!='')
						return '<a href="#" onClick="managerOptions(\''+record.data.id+'\',1)">'+val+'</a>';
					else
						return '<a href="#" onClick="managerOptions(\''+record.data.id+'\',1)">unallocated</a>';
				},
			}/*,{  
				header		: 'Module',  
				dataIndex	: 'modulesSys', 
				sortable	: true,
				width		: 180,
				renderer:function (val,element, record){
					if (val!='')
						return '<a href="#" onClick="managerOptions(\''+record.data.id+'\',2)">'+val+'</a>';
					else
						return '<a href="#" onClick="managerOptions(\''+record.data.id+'\',2)">unallocated</a>';
				}
			}*/
			]  
		);
		
		training.pager.on('beforechange',function(bar,params){
				params.status=Ext.getCmp('filterTrainig').getForm().findField("status").getValue();
				params.search=Ext.getCmp('filterTrainig').getForm().findField("search").getValue();
		});
		training.grid = new Ext.grid.EditorGridPanel({
			height: 500,
            store: training.store,
			tbar: training.pager,   
			cm: training.columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true ,
			sm:training.sm,
			loadMask: true,
			border :false
        }); 
		
		/***********
		
		***********************/
		training.modules.loaderGroups = new Ext.tree.TreeLoader({		//Paso 1
					url			: 'php/funcionestraining.php',
					baseParams	: {
						'accion':'listMenu',
						typeHelp:typeHelp
					}
				});
		training.modules.treeGroups = new Ext.tree.TreePanel({
			useArrows: true,
			autoScroll: true,
			height: 500,
			animate: true,
			enableDD: true,
			containerScroll: true,
			border: false,
			enableDrag: true,
			rootVisible: false,
			listeners:{
				'click'	: function (node){
					if(node.attributes.isVideo){
						training.currentFolder=node.parentNode.id;
					}
					else{
						training.currentFolder=node.id;
					}
					training.currentNode=node.id;
					training.currentNodeText=node.text;
					training.currentNodeTextAssing=node.attributes.id_assing;
					training.currentNodeid_video=node.attributes.id_video;
					training.currentNodeisVideo=node.attributes.isVideo;
				},
				movenode: function( tree, node, oldParent, newParent, index ){
						console.debug(tree, node, oldParent, newParent, index );
						if(node.attributes.isVideo){
							var params={
								accion: 'updateModulePos',
								typeHelp:typeHelp,
								id	: node.attributes.id_assing,
								nodeFather:newParent.id,
								pos	:index+1,
								isVideo:true
							}
						}
						else{
							var params={
								typeHelp:typeHelp,
								accion: 'updateModulePos',
								id	: node.id,
								nodeFather:newParent.id,
								pos	:index+1,
								isVideo:false
							}
						};
						
						Ext.Ajax.request({
							method :'POST',
							url: 'php/funcionestraining.php',
							success: function (){
								training.store.load({
							params:{
									status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
									search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
								}
							})
							},
							failure:  function (){
							   
							},
							 params: params
						});
						
						
				}
			},
			//dataUrl: 'php/funcionestraining.php',
			loader:training.modules.loaderGroups,
			root: {
				nodeType: 'async',
				text: 'All',
				draggable: false,
				id: 'All'
			},
			tbar:{
				
				defaults:{scope:this},
				items:[
					{
						xtype	:'button',
						iconCls	:'x-icon-add',
						text	:'New module',
						handler	:training.newGroup.init
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-cancel',
						text	: 'Delete module / Remove video assignment',
						handler	: training.deleteGroup
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-edit-text',
						text	: 'Video / module rename',
						handler	: training.editGroup.init
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-addmovie',
						text	: 'Assign video',
						handler	: training.addMovie.init
					}
				]
			}
		});

		/**************************
		
		***************************/
		
		training.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region	:'center',
					items: {
						xtype	:'tabpanel',
						activeTab: 0,
						items: [{
							title	: 'Trainings',
							items	: [training.grid]
						},{
							title: 'Manage Structure',
							items	: [training.modules.treeGroups]
						}]
					},
					autoHeight: true
			}]
		});
		
	},
	sm:new Ext.grid.CheckboxSelectionModel()
	,
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'id_modulo', type: 'int'},
			{name: 'madeBy', type: 'string'},
			{name: 'show', type: 'bool'},
			{name: 'name', type: 'string'},    
			{name: 'idYoutube', type: 'string'},    
			{name: 'urlVideo', type: 'string'},  
			{name: 'modules', type: 'string'},
			{name: 'modulesSys', type: 'string'},
			{name: 'urlImg', type: 'date', dateFormat: 'string'},
			{name: 'status', type: 'int'},
			{name: 'fecha', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'observations', type: 'string'}
	]),
	recordModules : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'name', type: 'string'},    
			{name: 'description', type: 'string'}
	]),
	recordModulesAssing : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'id_modulo', type: 'int'},    
			{name: 'id_video', type: 'int'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionestraining.php',   // Servicio web  
			method: 'POST'                      // Método de envío  
	}),
	save	:function ()
	{
		var grid=training.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcionestraining.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update',
						typeHelp:typeHelp
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
			   		training.grid.getStore().reload();
					if(training.modules.treeGroups) 
						training.modules.treeGroups.getRootNode().reload();
				}  
			});  
		}  
			
	},
	reset: function (){
		var cols=training.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].set){
				var date=cols[i].set('date_training','');
				var date=cols[i].set('hourtraining','');
			}
		}
	},
	cancel: function ()
	{
		training.grid.getStore().rejectChanges();
	},
	modules:{
	},
	delete:function (){
		data=training.grid.getSelectionModel().getSelections();
		if(data.length){
			var ids='';
			for(i=0;i<data.length;i++){
				ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
			}
			Ext.Ajax.request({
				method :'POST',
				url: 'php/funcionestraining.php',
				success: function (){
				   training.grid.getStore().reload();
				},
				failure:  function (){
				   
				},
				 params: { 
					accion: 'delete',
					ids	: ids,
					typeHelp:typeHelp
				}
			});
		}
	}
	,
	deleteGroup:function (){
		Ext.Msg.show({
		   title:'Delete Module?',
		   msg: 'Sure you want to delete the Module "'+training.currentNodeText+'"?',
		   buttons: Ext.Msg.YESNO,
		   fn: function (btn){
			   if(btn == 'yes'){
					Ext.Ajax.request({
						method :'POST',
						url: 'php/funcionestraining.php',
						success: function (){
							training.modules.treeGroups.getRootNode().reload();
							if(training.newGroup.storeModule)
								training.newGroup.storeModule.load();
							training.store.load({
								params:{
									status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
									search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
								}
							})
						},
						failure:  function (){
						   
						},
						 params: { 
						 	typeHelp:typeHelp,
							accion: 'deleteModule',
							id	: training.currentNode,
							isVideo	: training.currentNodeisVideo,
							id_assing	:training.currentNodeTextAssing
						}
					});
			   }
			},
		   icon: Ext.MessageBox.QUESTION
		});
	},
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	new:{
		init:function (){
			if(training.new.win)
			{
				training.new.form.getForm().reset();
				training.new.win.show();
			}
			else{
				training.new.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					
					items : [{    
    					width	: 200,
						xtype	:	'textfield',
						name	:	'new',
				        fieldLabel: 'without creating'
					},{    
						height 	: 140,
    					width	: 200,
						xtype	:	'textarea',
						name	:	'url',
				        fieldLabel: 'From Url of Youtube'
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(training.new.form.getForm().isValid())
								{
									training.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new training...',
										url: 'php/funcionestraining.php',
										scope : this,
										params :{
											typeHelp:typeHelp,
											accion : 'recordtraining'
										},
										success	: function (form,action)
										{
											training.new.form.getForm().reset();
											training.new.win.hide();
											training.store.load();	
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								training.new.form.getForm().reset();
							}
						}]
				})
				
				
				training.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New training', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 360,
					height: 265,
					items	:[
						training.new.form
					]
				});
				training.new.win.show();
			}
		}
	},
	addMovie:{
		init:function (){
			if(training.addMovie.win)
			{
				training.addMovie.form.getForm().reset();
				training.addMovie.win.show();
			}
			else{
				var readerModule = new Ext.data.JsonReader({  
					root: 'videos', 
					totalProperty: 'total'
					},
					training.record
					);
					
				training.addMovie.storeModule = new Ext.data.Store({
					id: 'id',  
					proxy: training.dataProxy,  
					baseParams: {
						accion	:'listSingle',
						typeHelp:typeHelp
					},  
					reader: readerModule  
				});
				
				var comboModule = new Ext.form.ComboBox({
					forceSelection:true,
					store: training.addMovie.storeModule,
					displayField:'name',
					hiddenName	:'video',
					typeAhead	: true,
					mode		: 'remote',
					fieldLabel: 'Training',
					valueField:'id',
					triggerAction: 'all',
					emptyText:'',
					width: 400,
					selectOnFocus:true
				});
				training.addMovie.storeModule.load();
				
				training.addMovie.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					
					items : [
						comboModule
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(training.addMovie.form.getForm().isValid())
								{
									training.addMovie.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration...',
										url: 'php/funcionestraining.php',
										scope : this,
										params :{
											typeHelp:typeHelp,
											accion  : 'recordusertrainigroup',
											node	: training.currentFolder
										},
										success	: function (form,action)
										{
											training.addMovie.form.getForm().reset();
											training.addMovie.win.hide();
											training.modules.treeGroups.getRootNode().reload();
											training.store.load({
											params:{
													status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
													search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.newGroup.form.getForm().reset();
							}
						}]
				});
				
				training.addMovie.win= new Ext.Window({
					layout: 'fit', 
					title: 'Add Training', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 120,
					items	:[
						training.addMovie.form
					]
				});
				training.addMovie.win.show();	
			}
		}
	},
	editGroup:{
		init:function (){
			if(training.editGroup.win)
			{
				training.editGroup.form.getForm().setValues({
						name : training.currentNodeText
					});
				
				training.editGroup.win.show();
			}
			else{
				training.editGroup.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					
					items : [
						{
							xtype 	: 'textfield',
							name	: 'name',
							value	: training.currentNodeText,
							width	: 400,
							fieldLabel	: 'Name'
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Save',
							handler	: function ()
							{
								if(training.editGroup.form.getForm().isValid())
								{
									training.editGroup.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing...',
										url: 'php/funcionestraining.php',
										scope : this,
										params :{
											typeHelp:typeHelp,
											accion 	: 'editModule',
											id		: training.currentNode,
											isVideo	: training.currentNodeisVideo,
											video	: training.currentNodeid_video
										},
										success	: function (form,action)
										{
											training.editGroup.form.getForm().reset();
											training.editGroup.win.hide();
											training.modules.treeGroups.getRootNode().reload();
											training.store.load({
											params:{
													status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
													search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								training.editGroup.form.getForm().reset();
							}
						}]
				});
				
				training.editGroup.win= new Ext.Window({
					layout: 'fit', 
					title: 'Edit Module', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 120,
					items	:[
						training.editGroup.form
					]
				});
				training.editGroup.win.show();
			}
		}
	}
	,newGroup:{
		init:function (){
			if(training.newGroup.win)
			{
				training.newGroup.form.getForm().reset();
				training.newGroup.win.show();
			}
			else{
				
				var readerModule = new Ext.data.JsonReader({  
					root: 'modules', 
					totalProperty: 'total'
					},
					training.recordModules
					);
					
				training.newGroup.storeModule = new Ext.data.Store({
					id: 'id',  
					proxy: training.dataProxy,  
					baseParams: {
						accion	:'listModule',
						typeHelp:typeHelp
					},  
					reader: readerModule  
				});
				
				var comboModule = new Ext.form.ComboBox({
					forceSelection:true,
					store: training.newGroup.storeModule,
					displayField:'name',
					hiddenName	:'module',
					typeAhead	: true,
					mode		: 'remote',
					fieldLabel: 'Father Module',
					valueField:'id',
					editable	:false,
					triggerAction: 'all',
					emptyText:'',
					width: 400,
					selectOnFocus:true
				});
				training.newGroup.storeModule.load();
				
				training.newGroup.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					
					items : [
						comboModule,
						{
							xtype 	: 'textfield',
							name	: 'name',
							width	: 400,
							fieldLabel	: 'Name'
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(training.newGroup.form.getForm().isValid())
								{
									training.newGroup.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new Module...',
										url: 'php/funcionestraining.php',
										scope : this,
										params :{
											accion : 'recordModule',
											typeHelp:typeHelp
										},
										success	: function (form,action)
										{
											training.newGroup.form.getForm().reset();
											training.newGroup.win.hide();
											training.newGroup.storeModule.load();
											training.modules.treeGroups.getRootNode().reload();
											training.store.load({
												params:{
													status	:	Ext.getCmp('filterTrainig').getForm().findField("status").getValue(),
													search	:	Ext.getCmp('filterTrainig').getForm().findField("search").getValue()
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								training.newGroup.form.getForm().reset();
							}
						}]
				});
				
				training.newGroup.win= new Ext.Window({
					layout: 'fit', 
					title: 'New Module', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 155,
					items	:[
						training.newGroup.form
					]
				});
				training.newGroup.win.show();
			}
		}
	},
}
function viewtraining(id){
	window.open('http://www.youtube.com/embed/'+id+'?rel=0','View training','width=720,height=500');
	return false;
}
function managerOptions (val,type){
	var id_video=val;
	var change=false;
	var sm=new Ext.grid.CheckboxSelectionModel();
	if(type==1){
		var title='Manager Help';
	}
	else{
		var title='Manager Modules';
	}
	
	var readerModule = new Ext.data.JsonReader({  
		root: 'modules', 
		totalProperty: 'total',
		id:'readermodules'
		},
		training.recordModules
		);
		
	var storeModule = new Ext.data.Store({
		id: 'id',  
		proxy: training.dataProxy,  
		baseParams: {
			accion	:'listModule',
			typeHelp:typeHelp,
			type	: type
		},  
		reader: readerModule  
	});
	
	var comboModule = new Ext.form.ComboBox({
		forceSelection:true,
		store: storeModule,
		displayField:'name',
		typeAhead: true,
		mode: 'remote',
		valueField:'id',
		editable	:false,
		triggerAction: 'all',
		emptyText:'Select a Status...',
		selectOnFocus:true
	});
	storeModule.load();
	
	var columnMode = new Ext.grid.ColumnModel(  
		[
			sm,
		{  
			header: 'Module',  
			dataIndex: 'id_modulo', 
			sortable: true, 
			editor	:comboModule,
			width: 25  ,
            renderer: function(val){
					var record = comboModule.findRecord(comboModule.valueField, val);
					return record ? record.get(comboModule.displayField) : comboModule.valueNotFoundText;
			}
		}
		]  
	);
	
	var gridReader = new Ext.data.JsonReader({  
		root: 'modules', 
		totalProperty: 'total',
		id:'readertraining'
		},
		training.recordModulesAssing 
		);
		
		
	var storeAssing = new Ext.data.Store({  
		id: 'training',  
		proxy: training.dataProxy,  
		baseParams: {
			accion	:'listModulesAssi',
			video	: val,
			typeHelp:typeHelp,
			type	: type
		},  
		reader: gridReader  
	});
	
	storeAssing.load();
	var grid= new Ext.grid.EditorGridPanel({
			xtype:'editorgrid',
			height: 420,
            store: storeAssing,
			tbar: {
				defaults:{scope:this},
				items:[
					{
						xtype	:'button',
						iconCls	:'x-icon-add',
						handler	: function (){
							var position = storeAssing.getCount();
							var id = Ext.id();
							var defaultData = {	
								newRecordId: id+'_'+id_video
							};
							var Module = storeAssing.recordType; 
							var module = new Module(defaultData,id_video); 
							grid.stopEditing(); //step 3
							grid.getStore().insert(position, module);
							grid.startEditing(position, 1);	
						},
						text	: 'New modulo'
					},
					{
						xtype	:'button',
						iconCls	:'x-icon-cancel',
						handler	: function (){
							change=true;
							data=grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionestraining.php',
									success: function (){
										if(training.modules.treeGroups) 
											training.modules.treeGroups.getRootNode().reload();
									    grid.getStore().reload();
									},
									failure:  function (){
									   
									},
									 params: { 
									 	typeHelp:typeHelp,
										accion: 'deleteModulo',
										ids	: ids,
										type	: type
									}
								});
						},
						text	: 'Delete'
					},
					{
						xtype	:'button',
						iconCls	:'x-icon-save',
						handler	: function ()
						{
							change=true;
							var modified = grid.getStore().getModifiedRecords();
							
							if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
								var recordsToSend = [];  
								Ext.each(modified, function(record) {
									recordsToSend.push(Ext.apply({id:record.id},record.data));  
								});  
							  
								grid.el.mask('Saving...', 'x-mask-loading');
								grid.stopEditing();  
							  
								recordsToSend = Ext.encode(recordsToSend);
							  
								Ext.Ajax.request({ 
									url : 'php/funcionestraining.php',  
									params :
										{
											typeHelp:typeHelp,
											records : recordsToSend,
											accion	:'updateModulos',
											type	: type
										},  
									scope:this,  
									success : function(response) {
										grid.el.unmask();  
										grid.getStore().commitChanges(); 
										if(training.modules.treeGroups) 
											training.modules.treeGroups.getRootNode().reload();
										grid.getStore().reload();
									}  
								});  
							}  
								
						},
						text	: 'Save Changes'
					}
				]},   
			cm: columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true,
			sm:sm,
			loadMask: true,
			border :false
		});
	
	var win= new Ext.Window({
		layout: 'fit', 
		title: title,
		closable: true,
		closeAction: 'hide',
		resizable: false,
		maximizable: false,
		plain: true,
		border: false,
		width: 420,
		height: 265,
		items	:[
		grid
		],
		listeners:{
			'hide':function(win){
				if(change)
					training.store.load();
			}
		}
	});
	win.show();
}

Ext.onReady(training.init);
Ext.QuickTips.init()