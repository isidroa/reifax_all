		Ext.ns("instructivesall");

		instructivesall = {
				store : {
					userTopics : new Ext.data.JsonStore({
						url : 'php/instructives/funcionesInstructives.php',
						root : 'data',
						autoLoad : true,
						baseParams : {
							option : 'getUserTopics'
						},
						fields : [ {
							name : 'topics',
						}, {
							name : 'idItg'
						} ]
					}),
					usersGroups : new Ext.data.JsonStore({
						url : 'php/instructives/funcionesInstructives.php',
						root : 'data',
						autoLoad : true,
						baseParams : {
							option : 'getuserGroups',				
						},
						fields : [ {
							name : 'idgroup',
						}, {
							name : 'group'
						} ]
					})
				},

				utility:
				{
					proxy: new Ext.data.HttpProxy({
						url		: 'php/instructives/funcionesInstructives.php',
						type	: 'POST',
						timeout : 3600000
					})
				}
		}

		function viewHtmlToManage(instructive_id) {
			Ext.Ajax.request({
				waitMsg : 'Loading...',
				url : 'php/instructives/funcionesInstructives.php',
				method : 'POST',
				timeout : 100000,
				params : {
					option : 'instructiveView',
					instructive_id : instructive_id,
				},
				failure : function(response, options){
					Ext.MessageBox.alert('Warning', 'Error Loading');
				},
				success : function(response) {
					Ext.QuickTips.init();
					var variable = Ext.decode(response.responseText);
					var appro = variable.isStatus;
					var readdd = variable.isStatus;
					var aprueba = variable.aprueba;
					var edita = variable.edita;
					var showMsg = true;
					var mostrarCombo = true;
					if (appro == 1) {
						var aprobar = true;
					}
					if (readdd == 1) {
						var readonlys = true;
					}
					
					if (readdd == 1) {
						var showMsg = false;
					}

					if(aprueba==1)
                  	{
                      var mostrarCombo = false;
                  	}

					if(appro == 1 && aprueba == 1){
						var actions = [[ '1','Do Nothing' ],[ '2','All Users must read again' ]];
					}else{
						if(appro == 1 && aprueba == 0)
							var actions = [[ '1','Do Nothing' ],[ '2','All Users must read again' ],[ '3','It must be approved again']];
						else{
							var actions = [['0','Not yet been approved']]	
						}
					}
 					
					var btnEdit =
					[
						{
							text : '<b>Save </b>',
							cls : 'x-btn-text-icon',
							icon : 'images/disk.png',
							formBind : true,
							handler : function(b) {							      
						        Ext.MessageBox.confirm('Confirmation','Are you sure edit this instructive?',
						          function(btn,text)
						            {
						               if(btn=='yes')
						               {  
						               		var form = b.findParentByType('form');
												if (form.getForm().isValid()) {
													form.getForm().submit(
													{
														url : 'php/instructives/funcionesInstructives.php',
														waitTitle : 'Please wait!',
														waitMsg : 'Loading...',
														params : {
															option : 'instructiveEdit',
															instructive_id : instructive_id
														},
														timeout : 100000,
														method : 'POST',
														success : function(form,action) {
															w.close();
															var store1 = Ext.getCmp("gridpanel").getStore();
															store1.reload();
															Ext.Msg.alert('Success',action.result.msg);
														},
														failure : function(form,action) {
															Ext.Msg.alert('Error',action.result.msg);
														}
													});
												}
						               }
						            }
						        );							
							}
						}							
					]

					var form = new Ext.form.FormPanel({
						baseCls : 'x-plain',
						labelWidth : 55,
						width: 700,
						items : 
						[
							{
								xtype : 'textfield',
								name : 'mensaje',
								fieldLabel : 'Message',
								value : 'Instructive approved, it can not be modified',
								width : 610,
								hidden : showMsg,
								readOnly : true
							},
							{
								xtype : 'combo',
								id : 'istatus',
								name : 'istatus',
								hiddenName : 'istatus',
								fieldLabel : 'Status',
								allowBlank : true,
								width : 200,
								store : new Ext.data.SimpleStore({
									fields : [ 'id', 'type' ],
									data : [
										[ '1','Approved' ],
										[ '0','Not Approved' ] 
									]
								}),
								displayField : 'type',
								valueField : 'id',
								mode : 'local',
								triggerAction : 'all',
								emptyText : 'Select..',
								selectOnFocus : true,
								value : variable.isStatus,
								disabled : aprobar,
								hidden : mostrarCombo
							},
							{
								xtype : 'textfield',
								fieldLabel : 'Title',
								name : 'title',
								id : 'title',
								width : 610,
								value : variable.title,
								allowBlank : false,
								readOnly : readonlys
							},
							{
								xtype : 'combo',
								id : 'instructives_type_id',
								name : 'instructives_type_id',
								hiddenName : 'instructives_type_id',
								fieldLabel : 'Group',
								allowBlank : false,
								width : 200,
								store : new Ext.data.SimpleStore({
									fields : [ 'id', 'instructives_type_id' ],
									data : 
									[
										[ '1','Instructive - General' ],
										[ '2','Instructive - Download' ],
										[ '3','Instructive - Customer Service' ],
										[ '4','Instructive - Administrator' ],
										[ '5','Instructive - Programmer' ],
										[ '6','Instructive - Single' ],
										[ '7','Instructive - Mobile' ] 
									]
								}),
								displayField : 'instructives_type_id',
								valueField : 'id',
								mode : 'local',
								triggerAction : 'all',
								emptyText : 'Select...',
								selectOnFocus : true,
								readOnly : readonlys,
								value : variable.instructiveTypeId,
								listeners : {
									select : {
										fn : function(combo, value) {
											var idCombo = Ext.getCmp('instructives_type_id').getValue();
											var combo = Ext.getCmp('id_topics');
											combo.setReadOnly(false);
											combo.setDisabled(false);
											combo.clearValue();
											combo.store.filter('instructivesTypeId',idCombo);
										}
									}
								}
							},
							{
								xtype : 'combo',
								name : 'Select Topic',
								fieldLabel : 'Select Topic',
								width : 610,
								hiddenName : 'iditg',
								displayField : 'topics',
								valueField : 'iditg',
								value : variable.topics, // variable.instructive_type,//valueField:'iditg',
								allowBlank : false,
								readOnly : true,
								id : 'id_topics',
								store : new Ext.data.JsonStore(
								{
									autoLoad : true,
									proxy: instructivesall.utility.proxy,
									baseParams:{
										option:'getTopicsGroup'
									},
									remoteSort : false,
									idProperty : 'id',
									root : 'results',
									totalProperty : 'total',
									fields : [
									'idItg',
									'instructivesTypeId',
									'topics' 
									],

								}),
								triggerAction : 'all',
								mode : 'local',
								lastQuery : '',
								emptyText : 'Select..',
								selectOnFocus : true,
								listeners:{
									select : {
										fn : function(combo, value){														
											Ext.getCmp('idtinsa').setValue(value.data.idItg);
											Ext.getCmp('id_topics').setValue(value.data.topics);
											//document.getElementById('id_topics').value=value.data.topics;
										}
									}
								}
							},
							{
								xtype : 'hidden',
								id : 'idtinsa',
								name : 'idtinsa',
								value : variable.idTopic,
								hiddenName : 'idtinsa'
							},	
							{
								xtype : 'textfield',
								fieldLabel : 'Assigned to',
								name : 'asignadon',
								id : 'asignadon',
								width : 610,
								value : variable.asignado,
								allowBlank : false,
								readOnly : readonlys
							},							
							{
								xtype : 'combo',
								id : 'action',
								name : 'action',
								hiddenName : 'action',
								fieldLabel : 'Actions',
								allowBlank : true,
								width : 200,
								store : new Ext.data.SimpleStore({
									fields : [ 'id', 'action' ],
									data : actions 									
								}),
								displayField : 'action',
								valueField : 'id',
								mode : 'local',
								triggerAction : 'all',
								emptyText : 'Select...',
								allowBlank : false,
								selectOnFocus : true,
								listeners : {
									select : {
										fn : function(combo, value) {
											var comboAction = parseInt(Ext.getCmp('action').getValue());
											var instructiveGroup = Ext.getCmp('instructives_type_id');
											var topics = Ext.getCmp('id_topics');
											var asignado = Ext.getCmp('asignadon');
											var titulo = Ext.getCmp('title');
											//Acciones 
											if(comboAction == 1 || comboAction == 2){
												instructiveGroup.setReadOnly(true);
												asignado.setReadOnly(true);
												if(titulo.readOnly == true ){
                          							titulo.setReadOnly(false);
                        						}	
											}else{
												if(instructiveGroup.readOnly == true){
													instructiveGroup.setReadOnly(false);
												}
												if(asignado.readOnly == true){
													asignado.setReadOnly(false);
												}
											}											
										}
									}
								}
							},
							{
								xtype : 'htmleditor',
								id : 'description',
								id : 'description',
								hideLabel : true,
								height : 270,
								width : 670,
								value : variable.description,
								allowBlank : false,
							}
						],
						buttonAlign : 'left',
						buttons :
						[
							btnEdit, 
							'->', 
							{
								text : 'Close',
								cls : 'x-btn-text-icon',
								icon : 'images/cross.gif',
								handler : function(b) {
									w.close();
								}
							} 
						]
					});

					var w = new Ext.Window({
						title : 'Manage Instructive',
						width: 710,
						height : 550,
						layout : 'fit',
						plain : true,
						modal: true,
						bodyStyle : 'padding:5px;',
						items : [form]
					});
					w.show();

					if(appro == 0){
						Ext.getCmp('action').setValue('0');
						Ext.getCmp('action').hide();
					}

					w.addListener("beforeshow", function(w) {
						form.getForm().reset();
					});
				}
			});
		}		

		function previewHtmlEmail(idtmail) {
			Ext.Ajax.request({
				waitMsg : 'Loading...',
				url : 'php/grid_data.php',
				method : 'POST',
				timeout : 100000,
				params : {
					tipo : 'previewinstructions',
					idtmail : idtmail
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('Warning', 'Error Loading');
				},
				success : function(response) {
					Ext.QuickTips.init();
					var variable = Ext.decode(response.responseText);
					var form = new Ext.form.FormPanel(
					{
						baseCls : 'x-plain',
						labelWidth : 55,
						items : 
						[
							{
								xtype : 'textfield',
								fieldLabel : 'Titulo',
								name : 'titulo',
								id : 'titulo',
								width : 690,
								value : variable.titulo,
								allowBlank : false
							},
							{
								xtype : 'itemselector',
								name : 'asignadon',
								hiddenName : 'asignado',
								fieldLabel : 'Assigned to',
								imagePath : 'includes/ext/examples/ux/images/',
								multiselects :
								[
									{
										width : 250,
										height : 300,
										store : new Ext.data.SimpleStore(
										{
											fields : ['value','text' ],
											data : Ext.combos_selec.dataUsersProgramm,
										}),
										displayField : 'text',
										valueField : 'value',
										value : variable.todo
									},
									{
										width : 250,
										height : 300,
										store : [ [ "", "" ] ],
										valueField : 'value',
										displayField : 'text',
										tbar :
										[
											{
												text : 'clear',
												handler : function() { form.getForm().findField('asignadon').reset(); }
											} 
										]
									} 
								]
							}, 
							{
								xtype : 'htmleditor',
								id : 'descripcion',
								id : 'descripcion',
								hideLabel : true,
								height : 350,
								width : 750,
								value : variable.descripcion,
								allowBlank : false
							} 
						],
						buttonAlign : 'center',
						buttons : 
						[
							{
								text : '<b>Update Instructions</b>',
								cls : 'x-btn-text-icon',
								icon : 'images/disk.png',
								formBind : true,
								handler : function(b)
								{	
									var form = b.findParentByType('form');
									if (form.getForm().isValid()) {
										form.getForm().submit(
										{
											url : 'php/grid_edit.php?typeHelp'+ typeHelp,
											waitTitle : 'Please wait!',
											waitMsg : 'Loading...',
											params : {
												tipo : 'instructions',
												idtmail : idtmail
											},
											timeout : 100000,
											method : 'POST',
											success : function(form,action) {
												w.close();
												var store1 = Ext.getCmp("gridpanel").getStore();
												store1.reload();
												Ext.Msg.alert('Success', action.result.msg);
											},
											failure : function(form, action) {
												Ext.Msg.alert('Error', action.result.msg);
											}
										});
									}
								}
							},
							'->',
							{
								text : 'Close',
								cls : 'x-btn-text-icon',
								icon : 'images/cross.gif',
								handler : function(b) { w.close(); }
							},
							{
								text : 'View',
								handler : function()
								{
									Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'
										+ form.getForm("asignadon").getValues(true));

								}
							}
						]
					});

					var w = new Ext.Window({
						title : 'Update Instructions',
						width : 800,
						height : 620,
						layout : 'fit',
						plain : true,
						bodyStyle : 'padding:5px;',
						modal: true,
						items : form
					});
					w.show();
					w.addListener("beforeshow", function(w) {
						form.getForm().reset();
					});
				}
			})
		}

		function viewHtmlReadInstructive(instructive_id) {

			Ext.Ajax.request({
				waitMsg : 'Loading...',
				url : 'php/instructives/funcionesInstructives.php',
				method : 'POST',
				timeout : 100000,
				params : {
					option : 'instructiveView',
					instructive_id : instructive_id
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('Warning', 'Error Loading');
				},
				success : function(response) {
					Ext.QuickTips.init();
					var variable = Ext.decode(response.responseText);
					var leido = variable.dateReaded;
					var estatus = variable.isStatus;
					var dateReaded = variable.dateReaded;
					if (dateReaded == null) {
						var leido = [ {
							text : '<b>I Read</b>',
							cls : 'x-btn-text-icon',
							icon : 'images/check.png',
							formBind : true,
							handler : function(b) {
								var form = b.findParentByType('form');
								form.getForm().submit(
								{
									url : 'php/instructives/funcionesInstructives.php',
									waitTitle : 'Please wait!',
									waitMsg : 'Loading...',
									params : {
										option : 'instructiveRead',
										instructive_id : instructive_id
									},
									timeout : 100000,
									method : 'POST',
									success : function(form, action) {
										w.close();
										var store1 = Ext.getCmp("gridpanel")
										.getStore();
										store1.reload();
										Ext.Msg.alert('Success',
											action.result.msg);

									},
									failure : function(form, action) {
										Ext.Msg.alert('Error',
											action.result.msg);
									}
								});

							}
						} ];
					} else {
						var leido = [ {
							text : '<b>I Read</b>',
							cls : 'x-btn-text-icon',
							icon : 'images/check.png',
							disabled : true,
							hidden: true
						} ];
					}

					var form = new Ext.form.FormPanel({
						baseCls : 'x-plain',
						labelWidth : 55,
						autoScroll : true, 
						items : [ {
							xtype : 'textfield',
							fieldLabel : 'Title',
							name : 'titulo',
							id : 'titulo',
							width : 690,
							value : variable.title,
							allowBlank : false,
							readOnly : true
						}, {
							xtype : 'htmleditor',
							id : 'descripcion',
							id : 'descripcion',
							hideLabel : true,
							height : 350,
							width : 750,
							value : variable.description,
							allowBlank : false,
							readOnly : true
						} ],
						buttonAlign : 'left',
						buttons : [ leido, '->', {
							text : 'Close',
							cls : 'x-btn-text-icon',
							icon : 'images/cross.gif',
							handler : function(b) {
								w.close();
							}
						} ]
					});

					var w = new Ext.Window({
						title : 'Read instructive',
						draggable   : true,
						width : 800,
						height : 450,
						layout : 'fit',
						plain : true,
						modal: true,
						bodyStyle : 'padding:5px;',
						items : [form]
					});
					w.show();
					//EN ESTA LINEA SE ELIMINA EL TOOLBAR
					var padre = Ext.DomQuery.select("div[id^='ext-comp-'][class='x-toolbar x-small-editor x-toolbar-layout-ct']")[1].parentNode;
					padre.removeChild(padre.childNodes[0]);
					w.addListener("beforeshow", function(w) {
						form.getForm().reset();
					});
					}
				})
		}

		Ext.BLANK_IMAGE_URL = 'includes/ext/resources/images/default/s.gif';

		instructivesall.prepare = function() {
			instructivesall.store.principal = new Ext.data.JsonStore({
				totalProperty : 'total',
				root : 'results',
				proxy: instructivesall.utility.proxy,
				baseParams:{option:'getInstructives'},
				fields : [ {
					name : 'instructiveId',
					type : 'int'
				},
				{
					name: 'creatorUserId',
					type: 'int'
				},
				{
					name:'approverUserId',
					type: 'int'
				},
				{
					name: 'userSession',
					type: 'int'
				}, 
				{
					name: 'isStatus',
					type: 'int'
				}, 
				'instructiveType', 'cabecera', 'title', 'description', 'userCreator', 'creationDate',
				'dateReaded', 'edita', 'aprueba']
			});
			instructivesall.init();
		};

		instructivesall.init = function() {
			var win;
			Ext.QuickTips.init();
			Ext.form.Field.prototype.msgTarget = 'side';

				// /////////Cargas de data dinamica///////////////
				var storeUser = new Ext.data.JsonStore({
					url : 'php/comboUser.php',
					root : 'data',
					totalProperty : 'num',
					fields : [ 'idmen2', 'caption2' ]
				});
				// /////////Cargas de data dinamica///////////////
				var storeMenu2 = new Ext.data.JsonStore({
					url : 'php/grid_data_instructives.php?tipo=group_list',
					root : 'data',
					totalProperty : 'num',
					fields : [ 'idgroup', 'group_name' ]
				});

				var storeTypes = new Ext.data.JsonStore({
					totalProperty : 'total',
					root : 'results',
					url : 'php/grid_data.php?tipo=topics_type',
					fields : [ {
						name : 'instructive_type_id',
						type : 'int'
					}, 'instructive_type' ]
				});

				var storeGroups = new Ext.data.JsonStore({
					totalProperty : 'total',
					root : 'results',
					url : 'php/grid_data.php?tipo=topics_group',
					fields : [ {
						name : 'iditg',
						type : 'int'
					}, {
						name : 'instructives_type_id',
						type : 'int'
					}, 'topics' ]
				});

				// /////////FIN Cargas de data dinamica///////////////
				// //////////////barra de pagineo//////////////////////
				var pagingBar = new Ext.PagingToolbar({
					pageSize : 100,
					store : instructivesall.store.principal,
					displayInfo : true,
					displayMsg : '<b>Total: {2}</b>',
					emptyMsg : "No topics to display",
					items : [ {
						iconCls : 'icon',
						cls : 'x-btn-text-icon',
						icon : 'images/add.png',
						id : 'add_button',
						tooltip : 'Click to Add Instructive',
						handler : AddWindow
					// Abre Ventana Registrar Instructivo
				},
				{
					xtype : 'label',
					name : 'lblasig',
					text : 'Group'
				},
				{
					xtype : 'combo',
					name : 'asic',
					id : 'asic',
					listWidth : 300,
					store : instructivesall.store.usersGroups,
					valueField : 'idgroup',
					displayField : 'group',
					allowBlank : false,
					typeAhead : true,
					triggerAction : 'all',
					selectOnFocus : true,
					style : {
						fontSize : '12px'
					},
					value : 0,
					listeners : {
						'select' : logStatistic
					}
				},

				{
					xtype : 'label',
					name : 'lbltopic',
					text : 'Topic'
				},
				{
					xtype : 'combo',
					name : 'id_topic',
					id : 'id_topic',
					listWidth : 400,
					store : instructivesall.store.userTopics,
					valueField : 'idItg',
					displayField : 'topics',
					allowBlank : false,
					typeAhead : true,
					triggerAction : 'all',
					selectOnFocus : true,
					style : {
						fontSize : '12px'
					},
					value : 0,
					listeners : {
						'select' : filterTopic
					}
				}

				]
			});
			// //////////////FIN barra de pagineo//////////////////////
			// ////////////////Manejo de Eventos//////////////////////

		function obtenerSeleccionados() {
			var selec = grid.selModel.getSelections();
			var i = 0;
			var marcados = '(';
			for (i = 0; i < selec.length; i++) {
				if (i > 0)
					marcados += ',';
					marcados += selec[i].json.idtins;
			}
			marcados += ')';
			if (i == 0)
				marcados = false;
			return marcados;
		}
		
		//FILTROS DE BUSQUEDAS: TIPO
		function logStatistic() {
			instructivesall.store.principal.setBaseParam('idtype',Ext.getCmp("asic").getValue());
			instructivesall.store.principal.load({params : {
				start : 0,
				limit : 100
			}});
		}
		//FILTROS DE BUSQUEDAS: TOPICOS
		function filterTopic() {
			instructivesall.store.principal.setBaseParam('idtopic', Ext.getCmp("id_topic").getValue());
			instructivesall.store.principal.load({
				params : {
					start : 0,
					limit : 100
				}
			});
		}

		function AddWindow() {
			var usersProgrammers = new Ext.data.ArrayStore({ 
				// usersProgrammers para el Store
				fields : [ 'value', 'text' ],
				data : Ext.combos_selec.dataUsersProgramm,
				// Obtengo todos los usuarios con idusertype 7
				sortInfo : {
					field : 'value',
					direction : 'ASC'
				}
			});
			var Topics = new Ext.data.ArrayStore({ 
				// usersProgrammers para el Store
				fields : [ 'value', 'text' ],
				data : Ext.combos_selec.dataTopics,
				// Obtengo todos los usuarios con idusertype 7
				sortInfo : {
					field : 'value',
					direction : 'ASC'
				}
			});

			// Data Stores
			var storeTypes = new Ext.data.JsonStore({
				// autoLoad: true,
				totalProperty : 'num',
				root : 'data',
				url : 'php/grid_data.php?tipo=topics_type',
				fields : [ {
					name : 'instructive_type_id',
					type : 'int'
				}, 'instructive_type' ]
			});

			var storeGroups = new Ext.data.JsonStore({
				totalProperty : 'total',
				root : 'results',
				url : 'php/grid_data.php?tipo=topics_group',
				fields : [ {
					name : 'iditg',
					type : 'int'
				}, {
					name : 'instructives_type_id',
					type : 'int'
				}, 'topics' ]
			});

			function getComboID(combo, record, index) {
				var ValorSeleccionado = Ext.getCmp('instructives_type_id')
				.getValue();
				if (ValorSeleccionado == 6) {
					Ext.getCmp('users_single').show();
				} else {
					Ext.getCmp('users_single').hide();
				}

				Ext.getCmp('topics').show();
				Ext.getCmp('topics').enable();
				// storeMenu2.load({params:{instructives_type_id:ValorSeleccionado}});
				// alert(ValorSeleccionado);

			}// TERMINA GETCOMBOID

					var btnEdit = [ {
						text : '<b>Save </b>',
						cls : 'x-btn-text-icon',
						icon : 'images/disk.png',
						formBind : true,
						width : 100,
						handler : function(b) {
							var form = b.findParentByType('form');
							// form.getForm().fileUpload = true;
							if (form.getForm().isValid()) {
								form.getForm().submit({
									url : 'php/grid_edit.php',
									waitTitle : 'Please wait!',
									waitMsg : 'Loading...',
									params : {
										tipo : 'instructive_edit',
										instructive_id : instructive_id
									},
									timeout : 100000,
									method : 'POST',
									success : function(form, action) {
										w.close();
										var store1 = Ext.getCmp("gridpanel").getStore();
										store1.reload();
										Ext.Msg.alert('Success', action.result.msg);

									},
									failure : function(form, action) {
										Ext.Msg.alert('Error', action.result.msg);
									}
								});
							}
						}
					} ];

					var form = new Ext.form.FormPanel({
						baseCls : 'x-plain',
						labelWidth : 55,
						items : 
						[				
						{
							xtype:'hidden',
							name:'idTopic',
							id:'idTopic'   
						},
						{
							xtype : 'textfield',
							fieldLabel : 'Title',
							name : 'title',
							id : 'title',
							width : 640,
							allowBlank : false
						},
						{
							xtype : 'combo',
							id : 'instructives_type_id',
							name : 'instructives_type_id',
							hiddenName : 'instructives_type_id',
							fieldLabel : 'Group',
							allowBlank : false,
							width : 200,
							store : new Ext.data.SimpleStore({
								fields :
								[ 'id','instructives_type_id' ],
								data : 
								[
								[ '1', 'Instructive - General' ],
								[ '2', 'Instructive - Download' ],
								[ '3', 'Instructive - Customer Service' ],
								[ '4', 'Instructive - Administrator' ],
								[ '5', 'Instructive - Programmer' ],
								[ '6', 'Instructive - Single' ],
								[ '7', 'Instructive - Mobile' ] 
								]
							}),
							displayField : 'instructives_type_id',
							valueField : 'id',
							mode : 'local',
							triggerAction : 'all',
							emptyText : 'Select..',
							selectOnFocus : true,
								// listeners: {'select': getComboID}
								listeners:{
									select : {
										fn : function(combo, value){
											var idCombo = Ext.getCmp('instructives_type_id').getValue();
											var topics = Ext.getCmp('id_topics');
											if (idCombo == 6) {
												Ext.getCmp('users_single').show();
											} else {
												Ext.getCmp('users_single').hide();
											}								
											topics.setDisabled(false);
											topics.clearValue();
											topics.store.filter('instructivesTypeId',idCombo);
										}
									}
								}
							},
							{
								xtype : 'compositefield',
								fieldLabel : 'Topic',
								msgTarget : 'side',
								anchor : '-20',
								defaults : {flex : 1},
								items :
								[
								{
									xtype : 'combo',
									name : 'Topic',
									fieldLabel : 'Topic',
									width : 580,
									hiddenName : 'iditg',
									displayField : 'topics',
									allowBlank : false,
									valueField : 'iditg',
									disabled : true,
									id : 'id_topics',
									store : new Ext.data.JsonStore({
										autoLoad : true,
											//url: 'php/grid_data.php?tipo=topics_group',
											proxy: instructivesall.utility.proxy,
											baseParams:{
												option:'getTopicsGroup'
											},
											remoteSort : false,
											idProperty : 'id',
											root : 'results',
											totalProperty : 'total',
											fields : 
											[
											'idItg',
											'instructivesTypeId',
											'topics' 
											]
										}),
									triggerAction : 'all',
									mode : 'local',
									lastQuery : '',
									emptyText : 'Select..',
									selectOnFocus : true,
									listeners:{
										select : {
											fn : function(combo, value){
												Ext.getCmp('idTopic').setValue(value.data.idItg);
												document.getElementById('id_topics').value=value.data.topics;
											}
										}
									}
								},
								{
									xtype : 'button',
									text : 'Add',
									cls : 'x-btn-text-icon',
									icon : 'images/add.png',
									width : 40,
									handler : Adduser
										// Abre Ventana Registrar Instructivo
									}
									]

								},
								{
									text : 'View',
									handler : function() { Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+ form.getForm("asignadon").getValues(true))}
								},
								{
									xtype : 'itemselector',
									id : 'users_single',
									name : 'users_single',
									hiddenName : 'users_single',
									fieldLabel : 'Select users',
									hidden : true,
									imagePath : 'includes/ext/examples/ux/images/',
									multiselects : [ 
									{
										width : 250,
										height : 200,
										store : usersProgrammers,
										displayField : 'text',
										valueField : 'value'
									},
									{
										width : 250,
										height : 200,
										store : [ [ '', '' ] ],
										displayField : 'text',
										valueField : 'value',
										tbar : [ {text : 'clear', handler : function() { form.getForm().findField().reset();} } ]
									} 
									]
								},
								{
									xtype : 'htmleditor',
									id : 'description',
									id : 'description',
									hideLabel : true,
									height : 250,
									width : 700,
									allowBlank : false
								}
								],
								buttonAlign : 'left',
								buttons :
								[
								{
									text : '<b>Create Instructive</b>',
									cls : 'x-btn-text-icon',
									icon : 'images/disk.png',
									formBind : true,
									handler : function(b)
									{
										var form = b.findParentByType('form');
										if (form.getForm().isValid()) {
											form.getForm().submit(
											{
												url : 'php/instructives/funcionesInstructives.php',
												waitTitle : 'Please wait!',
												waitMsg : 'Loading...',
												params : {option : 'instructiveAdd'},
												timeout : 100000,
												method : 'POST',
												success : function(form,action) {
													w.close();
													Ext.Msg.alert('Success',action.result.msg);
													Ext.getCmp("gridpanel").getStore().reload();														
												},
												failure : function(form,action){
													Ext.Msg.alert('Error',action.result.msg);
												}
											});
										}
									}
								}, 
								'->', 
								{
									text : 'Close',
									cls : 'x-btn-text-icon',
									icon : 'images/cross.gif',
									handler : function(b) { w.close(); }
								} 
								]
							});

			var w = new Ext.Window({
				title : 'New Instructive',
				width : 750,
				height : 450,
				layout : 'fit',
				modal : true,
				plain : true,
				bodyStyle : 'padding:5px;',
				items : form
			});
			w.show();
			w.addListener("beforeshow", function(w) {form.getForm().reset();});
		}
		//AGREGAR USUARIO
		function Adduser(){
			var formula = new Ext.FormPanel({
			//url : 'php/instructives/funcionesInstructives.php',
			url : instructivesall.utility.proxy,
			frame : true,
			layout : 'form',
			border : false,
			items : 
			[
			{
				xtype : 'fieldset',
				autoHeight : true,
				border : false,
				items : 
				[ 
				{
					xtype : 'combo',
					store : storeMenu2,
					fieldLabel : 'Group',
					id : 'idgroup',
					name : 'idgroup',
					hiddenName : 'idgroup',
					valueField : 'idgroup',
					displayField : 'group_name',
					triggerAction : 'all',
					emptyText : 'Select a group',
					allowBlank : false,
					width : 200
							// listeners: {'select': showmenu}
						},
						{
							xtype : 'textfield',
							fieldLabel : 'Topic name',
							name : 'topics',
							id : 'topics',
							width : 450,
							allowBlank : false
						} 
						]
					}
					],
					buttons:
					[
					{
						text : 'Cancel',
						cls : 'x-btn-text-icon',
						icon : 'images/cross.gif',
						handler : function() { wind.close(); }
					},
					{
						text : 'Save',
						cls : 'x-btn-text-icon',
						icon : 'images/disk.png',
						formBind : true,
						handler : function()
						{
							if (formula.getForm().isValid()){
								formula.getForm().submit({
									method : 'POST',
									params : {
									//tipo : 'groups_topics_add'
									option : 'groupsTopic',
								},
								waitTitle : 'Please wait..',
								waitMsg : 'Sending data...',
								success : function(form,action)
								{
									// storeUsersapro.reload();
									var storeww = Ext.getCmp('id_topics').getStore();
									storeww.reload();
									var comboTopics = Ext.getCmp('id_topics');
									comboTopics.setDisabled(true);
									comboTopics.setValue('');
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Success",obj.msg);
									wind.close();
								},
								failure : function(form,action)
								{
									obj = Ext.util.JSON.decode(action.response.responseText);
									Ext.Msg.alert("Failure",obj.msg);
								}
							});
							}
						}
					}
					]
				});
				var wind = new Ext.Window({
					title : 'New topic',
					iconCls : 'x-icon-templates',
					layout : 'fit',
					width : 650,
					height : 150,
					resizable : false,
					modal : true,
					plain : true,
					items : formula
				});
				wind.show();
				wind.addListener("beforeshow", function(wind) {
					formula.getForm().reset();
				});
		}				
		//RENDERS
		function renderfemail(val, p, record) {
			return String.format('<a href="javascript:void(0)" onClick="previewHtmlEmail({0})" ><img src="images/editar.png" border="0" title="Preview email" alt="Preview email"></a>',
						record.data.idtins);
		}
		// RENDERIZADO A LA VISTA DE LECTURA DEL INSTRUCTIVO
		function renderReadInstructive(val, p, record) {
			var creador = record.data.creatorUserId;
			var sessionId = record.data.userSession;
			var approverUserId = record.data.approverUserId;
			//console.log(record);
			if (creador == sessionId) {
				return '';
			}
			else{
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',
				record.data.instructiveId);
			}

			if (approverUserId == sessionId) {
				return '';
			}else{
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlReadInstructive({0})" ><img src="images/read.png" border="0" width="16" title="Read Instructive" alt="Read Instructive"></a>',
				record.data.instructiveId);
			}
		}
		//RENDERIZADO A LA VISTA DE EDICION DEL INSTRUCTIVO
		function renderViewtoManage(val, p, record) {
			var creador = record.data.creatorUserId;
			var sesionid = record.data.userSession;
			var approverUserId = record.data.approverUserId;

			if (creador == sesionid) {
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',
					record.data.instructiveId);
			}
			if (approverUserId == sesionid) {
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',
					record.data.instructiveId);
			}

			if (record.data.isStatus == 0) {
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',
					record.data.instructiveId);
			}
			if (record.data.aprueba == 1) {
				return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',
					record.data.instructiveId);
			}
		}
		//RENDERIZADO A LAS ACCIONES
		function renderActions(val,p,record){
			var read = renderReadInstructive(val,p,record);
			var manage = renderViewtoManage(val,p,record);
			if(manage === undefined)
				manage='';
			return read+' '+manage;  
		}

		// GRILLA O INDEX
		/*var mySelectionModel = new Ext.grid.CheckboxSelectionModel({
			singleSelect : false
		});*/

		var myView2 = new Ext.grid.GridView();
		myView2.getRowClass = function(record, index, rowParams, store) {
			var creador = record.data.creatorUserId;
			var sesionid = record.data.userSession;
			var approverUserId = record.data.approverUserId;
			var read = record.data.dateReaded;
			if (approverUserId == sesionid) {
				if ( read == 'UnRead'){
					return '';
				}
			}
			if (creador == sesionid) {
				if (read == 'UnRead'){
					return '';
				}
			}
			if (read == 'UnRead'){
				return 'rowRed';
			}
		};
		myView2.forceFit = true;   				

		var grid = new Ext.grid.EditorGridPanel({
				title : wherepage,
				view : myView2,
				id : "gridpanel",
				store : instructivesall.store.principal,
				iconCls : 'icon-grid',
				columns : [ 
				new Ext.grid.RowNumberer(), /*mySelectionModel, */
				{
					header : 'Actions ',
					width : 60,
					align : 'right',
					dataIndex : 'view',
					sortable : false,
					renderer : renderActions
				},
				{
					header : 'ID',
					width : 30,
					align : 'left',
					sortable : true,
					dataIndex : 'instructiveId'
				}, {
					header : 'Group / Topic',
					width : 140,
					sortable : true,
					align : 'left',
					dataIndex : 'cabecera'
				}, {
					header : 'Titulo',
					width : 160,
					sortable : true,
					align : 'left',
					dataIndex : 'title'
				}, {
					header : 'Body',
					width : 320,
					sortable : true,
					align : 'left',
					dataIndex : 'description'
				}, {
					header : 'Created by',
					width : 150,
					sortable : true,
					align : 'left',
					dataIndex : 'userCreator'
				}, {
					header : 'Creation Date',
					width : 130,
					sortable : true,
					align : 'left',
					dataIndex : 'creationDate'
				}
				// ,{header: 'Asigned', width: 150, align: 'left', sortable:
				// true, dataIndex: 'userasigned'}
				, {
					header : 'Date Read',
					width : 130,
					align : 'left',
					sortable : true,
					dataIndex : 'dateReaded'
				}, /*{
					header : 'Read ',
					width : 40,
					sortable : true,
					align : 'center',
					dataIndex : 'instructiveId',
					renderer : renderReadInstructive
				}, {
					header : 'Manage',
					width : 50,
					sortable : true,
					align : 'center',
					dataIndex : 'instructiveId',
					renderer : renderViewtoManage
				}*/ 
				],
				//clicksToEdit : 2,
				height : Ext.getBody().getViewSize().height - 50,
				sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
				frame : true,
				loadMask : true,
				tbar : pagingBar,
   				layout: 'fit'
		});

		// VIEWPORT
		var pag = new Ext.Viewport({
			layout : 'border',
			hideBorders : true,
			monitorResize : true,

			items : [ {
				region : 'north',
				height : 25,
				items : Ext.getCmp('menu_page')
			}, {
				region : 'center',
				autoHeight : true,
				items : [grid]
			} ]
		});
		// RECARGAR GRILLA
		instructivesall.store.principal.load({
			params : {
				start : 0,
				limit : 100
			}
		});
	};

	Ext.onReady(instructivesall.prepare);
