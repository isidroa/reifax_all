      Ext.ns("privilegieInstructive");

      privilegieInstructive = {
         store :
         {
            nolaboralDetail : new Ext.data.JsonStore({
               totalProperty: 'total',
               root: 'results',
               url: 'php/instructives/funcionesPrivilegiesInstructives.php',
               baseParams:{option:'getInstructiveDetails'},
               fields: [
                  {name: 'instructiveId', type: 'int'},
                  {name: 'userId', type: 'int'},
                  'rDateRead',
                  'rStatus',
                  'group',
                  'usuarioInstructivo',
                  //,'instructivo'
                  'instructiveId']
            })
         },
         utility:
         {
            proxy1: new Ext.data.HttpProxy({
               url : 'php/instructives/funcionesInstructives.php',
               type : 'POST',
               timeout : 3600000
            }),
            
            proxy2: new Ext.data.HttpProxy({
               url : 'php/instructives/funcionesPrivilegiesInstructives.php',
               type : 'POST',
               timeout : 3600000
            })
         }
      }


      function actions(val,p, record){
         var id=record.data.instructiveId;
         var retorno = String.format('<a href="javascript:void(0)" onClick="marcaEmails({0})"><img src="images/view.gif" border="0" width="16px" title="View Details" alt="View Details" /></a>',id);
         retorno+= String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})"><img src="images/gestionar.png" border="0" width="16px" title="Manage Instructive" alt="Manage Instructive"></a>',id);
         retorno+= String.format('<a href="javascript:void(0)" onClick="deleteInstructive({0})"><img src="images/delete.gif" border="0" width="16px" title="Delete Instructive" alt="Delete Instructive"></a>',id);
         return retorno;
      }

      //ELIMINAR INSTRUCTIVO
      function deleteInstructive(id){
        Ext.MessageBox.confirm('Delete Instructive','Are you sure delete this instructive?',//
          function(btn,text)
            {
               if(btn=='yes')
               {               
                  Ext.Ajax.request({
                     waitMsg: 'Applying changes...',
                     url: 'php/instructives/funcionesPrivilegiesInstructives.php',
                     method: 'POST',
                     params: {
                        option : 'delete',
                        id: id
                     },
                     failure:function(response,options){
                        Ext.MessageBox.alert('Warning','Error editing');
                        privilegieInstructive.store.principal.reload();
                     },
                     success:function(response,options){
                        privilegieInstructive.store.principal.reload();
                     }
                  });
               }
            }
        );
      } 

      function marcaEmails(idcont)
      {
         var viewDetails = new Ext.grid.GridView();
            
         privilegieInstructive.store.nolaboralDetail.setBaseParam('instructive_id',idcont);
         privilegieInstructive.store.nolaboralDetail.load({
            params:
            { 
               instructive_id: idcont 
            }
         });

         viewDetails.getRowClass = function(record, index, rowParams) 
         {
            if(record.data.rDateRead == 'Not Read') return 'rowRed'; //Valida el estado del registro
         }
            
         //var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});

         var pagingBar2 = new Ext.PagingToolbar({
               store: privilegieInstructive.store.nolaboralDetail,
               displayInfo: true,
               displayMsg: '{0} - {1} of {2} Registros',
               emptyMsg: 'No records found',
               pageSize: 100
         });
         
         var gridNolaboralDetail = new Ext.grid.EditorGridPanel({
            view:viewDetails,
            store: privilegieInstructive.store.nolaboralDetail,
            id:'gridNolaboralDetail',
            iconCls: 'icon-grid',
            border:false,
            viewConfig: { forceFit:true },
            tbar: pagingBar2,
            columns: 
            [
               new Ext.grid.RowNumberer(), //grid2
               //mySelectionModel,
               //{header: 'ID', width: 40, sortable: true, align: 'left', dataIndex: 'instructiveId'},
               {header: 'ID User', width: 50, sortable: true, align: 'left', dataIndex: 'userId'},
               {header: 'User', width: 250, sortable: true, align: 'left', dataIndex: 'usuarioInstructivo'},
               {header: 'Group', width: 200, sortable: true, align: 'left', dataIndex: 'group'},
               {header: 'Date Read', width: 150, sortable: true, align: 'left', dataIndex: 'rDateRead'},
               {header: 'Read', width: 150, sortable: true, align: 'left', dataIndex: 'rStatus'},
               // {header: 'Eliminar', width: 150, sortable: true, align: 'left', dataIndex: 'rstatus'}  
               {
                  xtype: 'actioncolumn',
                  width: 30,
                  items:
                  [
                     {
                           icon    : 'images/delete-icon.png', // Use a URL in the icon config
                           tooltip : 'Exclude User',
                           scope   :   this,
                           handler : function(grid, rowIndex, columnIndex, e){
                           var rec = privilegieInstructive.store.nolaboralDetail.getAt(rowIndex);
                           var instructive_id = rec.get('instructiveId');
                           var userid = rec.get('userId');
                           Ext.MessageBox.alert('User Exclude','This user has been excluded');
                              // Revisar este comportamiento -wape
                           Ext.Ajax.request({
                                 waitMsg: 'Loading...',
                                 url: 'php/instructives/funcionesPrivilegiesInstructives.php',
                                 scope:this,
                                 method: 'POST',
                                 params: {
                                    instructive_id  : instructive_id,
                                    userid : userid,
                                    option :'excludeUser'
                                 },
                           success:function (){ grid.getStore().reload();}
                           });
                        }
                     }   
                  ]   
               }
            ],
            height:Ext.getBody().getViewSize().height-400,
            loadMask:true   
         });

      
         var ventana = new Ext.Window({
               title : 'List of Users',
               width: screen.width-200,   
               height: 500,
               layout : 'fit',
               modal : true,
               maximizable:true,
               collapsable: true,
               items : [gridNolaboralDetail]            
         });
         ventana.show();  
      }   

      /*Nueva ventana para Gestionar*/

     function viewHtmlToManage(instructive_id) {
      Ext.Ajax.request({
        waitMsg : 'Loading...',
        url : 'php/instructives/funcionesInstructives.php',
        method : 'POST',
        timeout : 100000,
        params : {
          option : 'instructiveView',
          instructive_id : instructive_id,
        },
        failure : function(response, options){
          Ext.MessageBox.alert('Warning', 'Error Loading');
        },
        success : function(response) {
          Ext.QuickTips.init();
          var variable = Ext.decode(response.responseText);
          var appro = variable.isStatus;
          var readdd = variable.isStatus;
          var aprueba = variable.aprueba;
          var edita = variable.edita;
          var showMsg = true;
          var mostrarCombo = true;
          if (appro == 1) {
            var aprobar = true;
          }
          if (readdd == 1) {
            var readonlys = true;
          }
          
          if (readdd == 1) {
            var showMsg = false;
          }

          if(aprueba==1)
          {
            var mostrarCombo = false;
          }

          if(appro == 1 && aprueba == 1){
            var actions = [[ '1','Do Nothing' ],[ '2','All Users must read again' ]];
          }else{
            if(appro == 1 && aprueba == 0)
              var actions = [[ '1','Do Nothing' ],[ '2','All Users must read again' ],[ '3','It must be approved again']];
            else{
              var actions = [['0','Not yet been approved']] 
            }
          }
          
          var btnEdit =
          [
            {
              text : '<b>Save </b>',
              cls : 'x-btn-text-icon',
              icon : 'images/disk.png',
              formBind : true,
              handler : function(b) {
                var form = b.findParentByType('form');
                  if (form.getForm().isValid()) {
                    form.getForm().submit(
                    {
                      url : 'php/instructives/funcionesInstructives.php',
                      waitTitle : 'Please wait!',
                      waitMsg : 'Loading...',
                      params : {
                        option : 'instructiveEdit',
                        instructive_id : instructive_id
                      },
                      timeout : 100000,
                      method : 'POST',
                      success : function(form,action) {
                        w.close();
                        var store1 = Ext.getCmp("gridpanel").getStore();
                        store1.reload();
                        Ext.Msg.alert('Success',action.result.msg);
                      },
                      failure : function(form,action) {
                        Ext.Msg.alert('Error',action.result.msg);
                      }
                    });
                  }
              }
            }             
          ]

          var form = new Ext.form.FormPanel({
            baseCls : 'x-plain',
            labelWidth : 55,
            width: 700,
            items : 
            [
              {
                xtype : 'textfield',
                name : 'mensaje',
                fieldLabel : 'Message',
                value : 'Instructive approved, it can not be modified',
                width : 610,
                hidden : showMsg,
                readOnly : true
              },
              {
                xtype : 'combo',
                id : 'istatus',
                name : 'istatus',
                hiddenName : 'istatus',
                fieldLabel : 'Status',
                allowBlank : true,
                width : 200,
                store : new Ext.data.SimpleStore({
                  fields : [ 'id', 'type' ],
                  data : [
                    [ '1','Approved' ],
                    [ '0','Not Approved' ] 
                  ]
                }),
                displayField : 'type',
                valueField : 'id',
                mode : 'local',
                triggerAction : 'all',
                emptyText : 'Select..',
                selectOnFocus : true,
                value : variable.isStatus,
                disabled : aprobar,
                hidden : mostrarCombo
              },
              {
                xtype : 'textfield',
                fieldLabel : 'Title',
                name : 'title',
                id : 'title',
                width : 610,
                value : variable.title,
                allowBlank : false,
                readOnly : readonlys
              },
              {
                xtype : 'combo',
                id : 'instructives_type_id',
                name : 'instructives_type_id',
                hiddenName : 'instructives_type_id',
                fieldLabel : 'Group',
                allowBlank : false,
                width : 200,
                store : new Ext.data.SimpleStore({
                  fields : [ 'id', 'instructives_type_id' ],
                  data : 
                  [
                    [ '1','Instructive - General' ],
                    [ '2','Instructive - Download' ],
                    [ '3','Instructive - Customer Service' ],
                    [ '4','Instructive - Administrator' ],
                    [ '5','Instructive - Programmer' ],
                    [ '6','Instructive - Single' ],
                    [ '7','Instructive - Mobile' ] 
                  ]
                }),
                displayField : 'instructives_type_id',
                valueField : 'id',
                mode : 'local',
                triggerAction : 'all',
                emptyText : 'Select...',
                selectOnFocus : true,
                readOnly : readonlys,
                value : variable.instructiveTypeId,
                listeners : {
                  select : {
                    fn : function(combo, value) {
                      var idCombo = Ext.getCmp('instructives_type_id').getValue();
                      var combo = Ext.getCmp('id_topics');
                      combo.setReadOnly(false);
                      combo.setDisabled(false);
                      combo.clearValue();
                      combo.store.filter('instructivesTypeId',idCombo);
                    }
                  }
                }
              },
              {
                xtype : 'combo',
                name : 'Select Topic',
                fieldLabel : 'Select Topic',
                width : 610,
                hiddenName : 'iditg',
                displayField : 'topics',
                valueField : 'iditg',
                value : variable.topics, // variable.instructive_type,//valueField:'iditg',
                allowBlank : false,
                readOnly : true,
                id : 'id_topics',
                store : new Ext.data.JsonStore(
                {
                  autoLoad : true,
                  proxy: privilegieInstructive.utility.proxy1,
                  baseParams:{
                    option:'getTopicsGroup'
                  },
                  remoteSort : false,
                  idProperty : 'id',
                  root : 'results',
                  totalProperty : 'total',
                  fields : [
                  'idItg',
                  'instructivesTypeId',
                  'topics' 
                  ],

                }),
                triggerAction : 'all',
                mode : 'local',
                lastQuery : '',
                emptyText : 'Select..',
                selectOnFocus : true,
                listeners:{
                  select : {
                    fn : function(combo, value){                            
                      Ext.getCmp('idtinsa').setValue(value.data.idItg);
                      Ext.getCmp('id_topics').setValue(value.data.topics);
                      //document.getElementById('id_topics').value=value.data.topics;
                    }
                  }
                }
              },
              {
                xtype : 'hidden',
                id : 'idtinsa',
                name : 'idtinsa',
                value : variable.idTopic,
                hiddenName : 'idtinsa'
              },  
              {
                xtype : 'textfield',
                fieldLabel : 'Assigned to',
                name : 'asignadon',
                id : 'asignadon',
                width : 610,
                value : variable.asignado,
                allowBlank : false,
                readOnly : readonlys
              },              
              {
                xtype : 'combo',
                id : 'action',
                name : 'action',
                hiddenName : 'action',
                fieldLabel : 'Actions',
                allowBlank : true,
                width : 200,
                store : new Ext.data.SimpleStore({
                  fields : [ 'id', 'action' ],
                  data : actions                  
                }),
                displayField : 'action',
                valueField : 'id',
                mode : 'local',
                triggerAction : 'all',
                emptyText : 'Select...',
                allowBlank : false,
                selectOnFocus : true,
                listeners : {
                  select : {
                    fn : function(combo, value) {
                      var comboAction = parseInt(Ext.getCmp('action').getValue());
                      var instructiveGroup = Ext.getCmp('instructives_type_id');
                      var topics = Ext.getCmp('id_topics');
                      var asignado = Ext.getCmp('asignadon');
                      var titulo = Ext.getCmp('title');
                      //Acciones 
                      if(comboAction == 1 || comboAction == 2){
                        instructiveGroup.setReadOnly(true);
                        asignado.setReadOnly(true);
                        if(titulo.readOnly == true ){
                          titulo.setReadOnly(false);
                        }
                      }else{
                        if(instructiveGroup.readOnly == true){
                          instructiveGroup.setReadOnly(false);
                        }
                        if(asignado.readOnly == true){
                          asignado.setReadOnly(false);
                        }
                      }                     
                    }
                  }
                }
              },
              {
                xtype : 'htmleditor',
                id : 'description',
                id : 'description',
                hideLabel : true,
                height : 270,
                width : 670,
                value : variable.description,
                allowBlank : false,
              }
            ],
            buttonAlign : 'left',
            buttons :
            [
              btnEdit, 
              '->', 
              {
                text : 'Close',
                cls : 'x-btn-text-icon',
                icon : 'images/cross.gif',
                handler : function(b) {
                  w.close();
                }
              } 
            ]
          });

          var w = new Ext.Window({
            title : 'Manage Instructive',
            width: 710,
            height : 550,
            layout : 'fit',
            plain : true,
            bodyStyle : 'padding:5px;',
              items : [form]
          });
          w.show();

          if(appro == 0){
            Ext.getCmp('action').setValue('0');
            Ext.getCmp('action').hide();
          }

          w.addListener("beforeshow", function(w) {
            form.getForm().reset();
          });
        }
      });
    }

      /*Termina nueva ventana para Gestionar*/
      function viewHtmlToEdit(instructive_id) //Ver ventana para Editar
          {
              Ext.Ajax.request({
                  waitMsg: 'Loading...',
                  url: 'php/grid_data.php',
                  method: 'POST',
                  timeout: 100000,
                  params: {
                      tipo : 'instructive_view',
                      instructive_id: instructive_id,
                  },
                  failure:function(response,options){
                      Ext.MessageBox.alert('Warning','Error Loading');
                  },
                  success:function(response){
                      Ext.QuickTips.init();
                      var variable = Ext.decode(response.responseText);
                      var appro= variable.isStatus;
                      if(appro != "1")
                      { 
                      var btnEdit= [{
                              text: '<b>Update Instructive </b>',
                              cls: 'x-btn-text-icon',
                              icon: 'images/disk.png',
                              formBind: true,
                              handler: function(b){
                                  var form = b.findParentByType('form');
                                  //form.getForm().fileUpload = true;
                                  if (form.getForm().isValid()) {
                                      form.getForm().submit({
                                          url: 'php/grid_edit.php',
                                          waitTitle   :'Please wait!',
                                          waitMsg     :'Loading...',
                                          params: {
                                              tipo : 'instructive_edit',
                                              instructive_id: instructive_id
                                          },
                                          timeout: 100000,
                                          method :'POST',
                                          success: function(form, action) {
                                              w.close();
                                              var store1 = Ext.getCmp("gridpanel").getStore();
                                              store1.reload();
                                              Ext.Msg.alert('Success', action.result.msg);

                                          },
                                          failure: function(form, action) {
                                              Ext.Msg.alert('Error', action.result.msg);
                                          }
                                      });
                                  }
                              }
                          }]
                          }
                          else
                          {
                              var btnEdit = [{
                              text: '<b>It was approved</b>',
                              cls: 'x-btn-text-icon',
                              icon: 'images/access_denied.png'
                              }]
                          }

                     var form = new Ext.form.FormPanel({
                        baseCls: 'x-plain',
                        labelWidth: 55,
                        items: 
                        [{
                              xtype:'textfield',
                              fieldLabel: 'Title',
                              name: 'title',
                              id: 'title',
                              width:690,
                              value: variable.title,
                              allowBlank: false
                          },{
                          xtype       :'combo',
                          id          :'instructives_type_id',
                          name        :'instructives_type_id',
                          hiddenName  :'instructives_type_id',
                          fieldLabel  :'Type',
                          allowBlank  :false,
                          width       :200,
                          store       :new Ext.data.SimpleStore({
                                          fields: ['id', 'type'],
                                          data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                      }),
                          displayField:'type',
                          valueField  :'id',
                          mode        :'local',
                          triggerAction:'all',
                          emptyText   :'Select..',
                          selectOnFocus:true,
                          value:variable.instructiveTypeId,

                        },
                        {
                              xtype:'textfield',
                              fieldLabel: 'Assigned to',
                              name: 'asignadon',
                              id: 'asignadon',
                              width:690,
                              value: variable.asignado,
                              allowBlank: false
                        },
                        {
                              xtype: 'htmleditor',
                              id: 'description',
                              id: 'description',
                              hideLabel: true,
                              height: 350,
                              width:750,
                              value: variable.description,
                              allowBlank: false
                          }],
                          buttonAlign: 'center',
                          buttons: [
                          btnEdit,'->',{
                              text: 'Close',
                              cls: 'x-btn-text-icon',
                              icon: 'images/cross.gif',
                              handler: function(b){ w.close(); }
                          }
                          /*,{
                              text: 'View',
                              handler: function(){
                                 Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                                 form.getForm("asignadon").getValues(true));
                           }
                        }*/]
                     });


                     var w = new Ext.Window({
                        title: 'Update Instructive',
                        width: 800,
                        height: 550,
                        layout: 'fit',
                        plain: true,
                        bodyStyle: 'padding:5px;',
                        items: form
                     });
                     w.show();
                     w.addListener("beforeshow",function(w){
                        form.getForm().reset();
                     });
                  }
              })
          }

          function viewHtmltoApprove(instructive_id)
          {

              Ext.Ajax.request({
                  waitMsg: 'Loading...',
                  url: 'php/grid_data.php',
                  method: 'POST',
                  timeout: 100000,
                  params: {
                      tipo : 'instructive_view',
                      instructive_id: instructive_id
                  },
                  failure:function(response,options){
                      Ext.MessageBox.alert('Warning','Error Loading');
                  },
                  success:function(response){
                     Ext.QuickTips.init();
                     var variable = Ext.decode(response.responseText);
                     var appro= variable.istatus;
                     var user_session = variable.user_session;
                     if(appro != "1")
                     { 
                              if(user_session==5 || user_session==20 || user_session==2342)
                              {
                                  var btnEdit= [{
                                      text: '<b>I Approve </b>',
                                      cls: 'x-btn-text-icon',
                                      icon: 'images/check.png',
                                      formBind: true,
                                      handler: function(b){
                                          var form = b.findParentByType('form');
                                          //form.getForm().fileUpload = true;
                                         // if (form.getForm().isValid()) {
                                              form.getForm().submit({
                                                  url: 'php/grid_edit.php',
                                                  waitTitle   :'Please wait!',
                                                  waitMsg     :'Loading...',
                                                  params: {
                                                      tipo : 'instructive_approve',
                                                      instructive_id: instructive_id
                                                  },
                                                  timeout: 100000,
                                                  method :'POST',
                                                  success: function(form, action) {
                                                      w.close();
                                                      var store1 = Ext.getCmp("gridpanel").getStore();
                                                      store1.reload();
                                                      Ext.Msg.alert('Success', action.result.msg);

                                                  },
                                                  failure: function(form, action) {
                                                      Ext.Msg.alert('Error', action.result.msg);
                                                  }
                                              });
                                          }
                                      }]//Termina Btn Edit    
                              }
                              else
                              {
                                  var btnEdit = [{
                                      text: '<b>Not Authorized  </b>',
                                      cls: 'x-btn-text-icon',
                                      icon: 'images/access_denied.png'
                                      }]
                              }   
                          }
                          else
                          {
                              var btnEdit = [{
                              text: '<b>It was approved </b>',
                              cls: 'x-btn-text-icon',
                              icon: 'images/access_denied.png'
                              }]
                          }


                      var form = new Ext.form.FormPanel({
                          baseCls: 'x-plain',
                          labelWidth: 55,
                          items: [{
                              xtype:'textfield',
                              fieldLabel: 'Title',
                              name: 'title',
                              id: 'title',
                              width:690,
                              value: variable.title,
                              allowBlank: false
                          },{
                              xtype:'textfield',
                              fieldLabel: 'Assigned to',
                              name: 'asignadon',
                              id: 'asignadon',
                              width:690,
                              value: variable.asignado,
                              allowBlank: false
                          },{
                              xtype: 'htmleditor',
                              id: 'description',
                              id: 'description',
                              hideLabel: true,
                              height: 350,
                              width:750,
                              value: variable.description,
                              allowBlank: false
                          }],
                          buttonAlign: 'center',
                          buttons: [
                          btnEdit,'->',{
                              text: 'Close',
                              cls: 'x-btn-text-icon',
                              icon: 'images/cross.gif',
                              handler: function(b){
                                  w.close();
                              }
                          }]
                      });


                      var w = new Ext.Window({
                          title: 'Read Instructive',
                          width: 800,
                          height: 620,
                          layout: 'fit',
                          plain: true,
                          bodyStyle: 'padding:5px;',
                          items: form
                      });
                      w.show();
                      w.addListener("beforeshow",function(w){
                          form.getForm().reset();
                      });
                  }
              })
          }


      Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
      
      privilegieInstructive.prepare = function(){
         privilegieInstructive.store.principal = new Ext.data.JsonStore({
            totalProperty: 'total',
            root: 'results',
            proxy: privilegieInstructive.utility.proxy2,
            baseParams:{option:'getPrivilegeInstructional'},
            fields: [
                     {name: 'instructiveId', type: 'int'}
                     ,{name: 'creatorUserId', type: 'int'}
                     ,'creationDate'
                     ,'cabecera'
                     ,'title'
                     ,'description'
                     ,'userCreator'
                     ,'bodyShort'
                     ,'userAsigned'
                     ,'userApproved'
                     ,'isStatus'
                     ,'approvalDate'
                 ] 
         });
         privilegieInstructive.init();     
      }


      privilegieInstructive.init = function(){
         /////////////////Variables////////////////////
         var win;
         Ext.QuickTips.init();
         Ext.form.Field.prototype.msgTarget = 'side';
         /////////////////FIN Variables////////////////////

         var storeUser= new Ext.data.JsonStore({
                             url:'php/comboUser.php',
                             root: 'data',
                             totalProperty: 'num',
                             fields: ['idmen2','caption2']
                     });
         ///////////Cargas de data dinamica///////////////
          var storeMenu2= new Ext.data.JsonStore({
                             url:'php/grid_data_instructives.php?tipo=group_list',
                             root: 'data',
                             totalProperty: 'num',
                             fields: ['idgroup','group_name']
                         });

         var storeTypes = new Ext.data.JsonStore({
                 totalProperty: 'total',
                 root: 'results',
                 url: 'php/grid_data.php?tipo=topics_type',
                 fields: [
                     {name: 'instructive_type_id', type: 'int'}
                     ,'instructive_type'
                 ]
             });

         var storeGroups = new Ext.data.JsonStore({
                 totalProperty: 'total',
                 root: 'results',
                 url: 'php/grid_data.php?tipo=topics_group',
                 fields: [
                     {name: 'iditg', type: 'int'}
                     ,{name: 'instructives_type_id', type: 'int'}
                     ,'topics'
                 ]
             });
         ///////////FIN Cargas de data dinamica///////////////
         
         function mostrarDetalleNolaboral(instructive_id){
             Ext.getCmp('gridNolaboralDetail').getStore().setBaseParam('instructive_id', instructive_id);
             Ext.getCmp('gridNolaboralDetail').getStore().load({
             params:{
                 instructive_id: instructive_id
             }
             });
         }
         ////////////////barra de pagineo - Botones Add - Delete//////////////////////
         var pagingBar = new Ext.PagingToolbar({
              pageSize: 100,
              store: privilegieInstructive.store.principal,
              displayInfo: true,
              displayMsg: '<b>Total: {2}</b>',
              emptyMsg: "No records found",
              items:
              [
                 {
                     iconCls:'icon',
                     cls: 'x-btn-text-icon',
                     icon: 'images/add.png',
                     id: 'add_button',
                     tooltip: 'Click to Add Instructions',
                     handler: AddWindow // Abre Ventana Registrar Instructivo
                 },
                  {
                     xtype:'label',
                     name: 'lblasig',
                     text: 'Filter Group'
                  },
                  {
                     xtype       :'combo',
                     id          :'typeins',
                     name        :'typeins',
                     hiddenName  :'typeins',
                     fieldLabel  :'type',
                     allowBlank  :false,
                     width       :200,
                     store: new Ext.data.SimpleStore({
                        fields: ['id', 'type'],
                        data : 
                        [
                           ['10', 'All Instructives'], 
                           ['1', 'Instructive - General'], 
                           ['2', 'Instructive - Download'], 
                           ['3', 'Instructive - Customer Service'], 
                           ['4', 'Instructive - Administrator'], 
                           ['5', 'Instructive - Programmer'],
                           ['6', 'Instructive - Single'],
                           ['7', 'Instructive - Mobile']
                        ]
                     }),
                     displayField:'type',
                     valueField  :'id',
                     value: '10',
                     mode        :'local',
                     triggerAction:'all',
                     emptyText   :'Select..',
                     selectOnFocus:true,
                     style : {
                        fontSize : '12px'
                     },
                     listeners: {'select': logStatistic}
                  },
               ]
         });

         ////////////////FIN barra de pagineo//////////////////////

         //////////////////Manejo de Eventos//////////////////////

         function obtenerSeleccionados(){
             var selec = grid.selModel.getSelections();
             var i=0;
             var marcados='(';
             for(i=0; i<selec.length; i++){
                 if(i>0) marcados+=',';
                     marcados+=selec[i].json.idtins;
                 }
             marcados+=')';
             if(i==0)marcados=false;
                 return marcados;
         }

         function logStatistic(){
            privilegieInstructive.store.principal.setBaseParam('idtype',Ext.getCmp("typeins").getValue());
            privilegieInstructive.store.principal.load({params:{start:0, limit:100}});
         }
        

            var ValorSeleccionado = 1;

         //Inicio Add user

             function Adduser()
             {
                 var formula = new Ext.FormPanel({
                     url:'php/grid_data_instructives.php',
                     frame:true,
                     layout: 'form',
                     border:false,
                     items:
                        [{
                           xtype: 'fieldset',
                           autoHeight: true,
                           border: false,
                           items:
                           [
                            {
                                 xtype:'combo',
                                 store:storeMenu2,
                                 fieldLabel:'Group',
                                 id:'idgroup',
                                 name:'idgroup',
                                 hiddenName: 'idgroup',
                                 valueField: 'idgroup',
                                 displayField: 'group_name',
                                 triggerAction: 'all',
                                 emptyText:'Select a group',
                                 allowBlank: false,
                                 width:200
                                 //listeners: {'select': showmenu}
                             },
                             {
                                xtype:'textfield',
                                 fieldLabel: 'Topic name',
                                 name: 'topics',
                                 id: 'topics',
                                 width:450,
                                 allowBlank: false
                             }
                             ]
                           //Assistant1Active
                        }]
                    ,
                     buttons: [{

                         text: 'Cancel',
                         cls: 'x-btn-text-icon',
                         icon: 'images/cross.gif',
                         handler  : function(){
                             wind.close();
                        }
                     },{
                         text: 'Save',
                         cls: 'x-btn-text-icon',
                         icon: 'images/disk.png',
                     formBind: true,
                         handler  : function(){
                             if(formula.getForm().isValid())
                             {
                                 formula.getForm().submit({
                                     method: 'POST',
                                     params: {
                                         tipo : 'groups_topics_add'
                                     },
                                     waitTitle: 'Please wait..',
                               waitMsg: 'Sending data...',
                                     success: function(form, action) {
                                         //storeUsersapro.reload();
                                         var storeww = Ext.getCmp("id_topics").getStore();
                                             storeww.reload();
                                         var comboTopics = Ext.getCmp('id_topics');
                                             comboTopics.setDisabled(true);
                                             comboTopics.setValue('');

                                          //comboGrupos.setDisabled();      


         /*
          var comboCity = Ext.getCmp('combo-city');
                                //set and disable cities
                                comboCity.setDisabled(true);
                                comboCity.setValue('');
                                comboCity.store.removeAll();
                                //reload city store and enable city combobox
                                comboCity.store.reload({
                                    params: { stateId: combo.getValue() }
                                });
                                comboCity.setDisabled(false);
         */                                        

                                       obj = Ext.util.JSON.decode(action.response.responseText);
                                         Ext.Msg.alert("Success", obj.msg);
                                         wind.close();
                                     },
                                     failure: function(form, action) {
                                 obj = Ext.util.JSON.decode(action.response.responseText);
                                         Ext.Msg.alert("Failure", obj.msg);
                                     }
                                 });
                             }
                         }
                     }]
                 });
                 var wind = new Ext.Window({
                         title: 'New topic',
                         iconCls: 'x-icon-templates',
                         layout      : 'fit',
                         width       : 650,
                         height      : 150,
                         resizable   : false,
                         modal       : true,
                         plain       : true,
                         items       : formula
                     });

                 wind.show();
                 wind.addListener("beforeshow",function(wind){
                         formula.getForm().reset();
                 });
                 
             }
         //Termina add user     
        function AddWindow() {
            var usersProgrammers = new Ext.data.ArrayStore({ 
              // usersProgrammers para el Store
              fields : [ 'value', 'text' ],
              data : Ext.combos_selec.dataUsersProgramm,
              // Obtengo todos los usuarios con idusertype 7
              sortInfo : {
                field : 'value',
                direction : 'ASC'
              }
            });
            var Topics = new Ext.data.ArrayStore({ 
              // usersProgrammers para el Store
              fields : [ 'value', 'text' ],
              data : Ext.combos_selec.dataTopics,
              // Obtengo todos los usuarios con idusertype 7
              sortInfo : {
                field : 'value',
                direction : 'ASC'
              }
            });

            // Data Stores
            var storeTypes = new Ext.data.JsonStore({
              // autoLoad: true,
              totalProperty : 'num',
              root : 'data',
              url : 'php/grid_data.php?tipo=topics_type',
              fields : [ {
                name : 'instructive_type_id',
                type : 'int'
              }, 'instructive_type' ]
            });

            var storeGroups = new Ext.data.JsonStore({
              totalProperty : 'total',
              root : 'results',
              url : 'php/grid_data.php?tipo=topics_group',
              fields : [ {
                name : 'iditg',
                type : 'int'
              }, {
                name : 'instructives_type_id',
                type : 'int'
              }, 'topics' ]
            });

            function getComboID(combo, record, index) {
              var ValorSeleccionado = Ext.getCmp('instructives_type_id')
              .getValue();
              if (ValorSeleccionado == 6) {
                Ext.getCmp('users_single').show();
              } else {
                Ext.getCmp('users_single').hide();
              }

              Ext.getCmp('topics').show();
              Ext.getCmp('topics').enable();
              // storeMenu2.load({params:{instructives_type_id:ValorSeleccionado}});
              // alert(ValorSeleccionado);

            }// TERMINA GETCOMBOID

            var btnEdit = [ {
              text : '<b>Save </b>',
              cls : 'x-btn-text-icon',
              icon : 'images/disk.png',
              formBind : true,
              width : 100,
              handler : function(b) {
                var form = b.findParentByType('form');
                // form.getForm().fileUpload = true;
                if (form.getForm().isValid()) {
                  form.getForm().submit({
                    url : 'php/grid_edit.php',
                    waitTitle : 'Please wait!',
                    waitMsg : 'Loading...',
                    params : {
                      tipo : 'instructive_edit',
                      instructive_id : instructive_id
                    },
                    timeout : 100000,
                    method : 'POST',
                    success : function(form, action) {
                      w.close();
                      var store1 = Ext.getCmp("gridpanel").getStore();
                      store1.reload();
                      Ext.Msg.alert('Success', action.result.msg);

                    },
                    failure : function(form, action) {
                      Ext.Msg.alert('Error', action.result.msg);
                    }
                  });
                }
              }
            } ];

            var form = new Ext.form.FormPanel({
              baseCls : 'x-plain',
              labelWidth : 55,
              items : 
              [       
              {
                xtype:'hidden',
                name:'idTopic',
                id:'idTopic'   
              },
              {
                xtype : 'textfield',
                fieldLabel : 'Title',
                name : 'title',
                id : 'title',
                width : 640,
                allowBlank : false
              },
              {
                xtype : 'combo',
                id : 'instructives_type_id',
                name : 'instructives_type_id',
                hiddenName : 'instructives_type_id',
                fieldLabel : 'Group',
                allowBlank : false,
                width : 200,
                store : new Ext.data.SimpleStore({
                  fields :
                  [ 'id','instructives_type_id' ],
                  data : 
                  [
                  [ '1', 'Instructive - General' ],
                  [ '2', 'Instructive - Download' ],
                  [ '3', 'Instructive - Customer Service' ],
                  [ '4', 'Instructive - Administrator' ],
                  [ '5', 'Instructive - Programmer' ],
                  [ '6', 'Instructive - Single' ],
                  [ '7', 'Instructive - Mobile' ] 
                  ]
                }),
                displayField : 'instructives_type_id',
                valueField : 'id',
                mode : 'local',
                triggerAction : 'all',
                emptyText : 'Select..',
                selectOnFocus : true,
                  // listeners: {'select': getComboID}
                  listeners:{
                    select : {
                      fn : function(combo, value){
                        var idCombo = Ext.getCmp('instructives_type_id').getValue();
                        var topics = Ext.getCmp('id_topics');
                        if (idCombo == 6) {
                          Ext.getCmp('users_single').show();
                        } else {
                          Ext.getCmp('users_single').hide();
                        }               
                        topics.setDisabled(false);
                        topics.clearValue();
                        topics.store.filter('instructivesTypeId',idCombo);
                      }
                    }
                  }
                },
                {
                  xtype : 'compositefield',
                  fieldLabel : 'Topic',
                  msgTarget : 'side',
                  anchor : '-20',
                  defaults : {flex : 1},
                  items :
                  [
                  {
                    xtype : 'combo',
                    name : 'Topic',
                    fieldLabel : 'Topic',
                    width : 580,
                    hiddenName : 'iditg',
                    displayField : 'topics',
                    allowBlank : false,
                    valueField : 'iditg',
                    disabled : true,
                    id : 'id_topics',
                    store : new Ext.data.JsonStore({
                      autoLoad : true,
                        //url: 'php/grid_data.php?tipo=topics_group',
                        proxy: privilegieInstructive.utility.proxy1,
                        baseParams:{
                          option:'getTopicsGroup'
                        },
                        remoteSort : false,
                        idProperty : 'id',
                        root : 'results',
                        totalProperty : 'total',
                        fields : 
                        [
                        'idItg',
                        'instructivesTypeId',
                        'topics' 
                        ]
                      }),
                    triggerAction : 'all',
                    mode : 'local',
                    lastQuery : '',
                    emptyText : 'Select..',
                    selectOnFocus : true,
                    listeners:{
                      select : {
                        fn : function(combo, value){
                          Ext.getCmp('idTopic').setValue(value.data.idItg);
                          document.getElementById('id_topics').value=value.data.topics;
                        }
                      }
                    }
                  },
                  {
                    xtype : 'button',
                    text : 'Add',
                    cls : 'x-btn-text-icon',
                    icon : 'images/add.png',
                    width : 40,
                    handler : Adduser
                      // Abre Ventana Registrar Instructivo
                    }
                    ]

                  },
                  {
                    text : 'View',
                    handler : function() { Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+ form.getForm("asignadon").getValues(true))}
                  },
                  {
                    xtype : 'itemselector',
                    id : 'users_single',
                    name : 'users_single',
                    hiddenName : 'users_single',
                    fieldLabel : 'Select users',
                    hidden : true,
                    imagePath : 'includes/ext/examples/ux/images/',
                    multiselects : [ 
                    {
                      width : 250,
                      height : 200,
                      store : usersProgrammers,
                      displayField : 'text',
                      valueField : 'value'
                    },
                    {
                      width : 250,
                      height : 200,
                      store : [ [ '', '' ] ],
                      displayField : 'text',
                      valueField : 'value',
                      tbar : [ {text : 'clear', handler : function() { form.getForm().findField().reset();} } ]
                    } 
                    ]
                  },
                  {
                    xtype : 'htmleditor',
                    id : 'description',
                    id : 'description',
                    hideLabel : true,
                    height : 250,
                    width : 700,
                    allowBlank : false
                  }
                  ],
                  buttonAlign : 'left',
                  buttons :
                  [
                  {
                    text : '<b>Create Instructive</b>',
                    cls : 'x-btn-text-icon',
                    icon : 'images/disk.png',
                    formBind : true,
                    handler : function(b)
                    {
                      var form = b.findParentByType('form');
                      if (form.getForm().isValid()) {
                        form.getForm().submit(
                        {
                          url : 'php/instructives/funcionesInstructives.php',
                          waitTitle : 'Please wait!',
                          waitMsg : 'Loading...',
                          params : {option : 'instructiveAdd'},
                          timeout : 100000,
                          method : 'POST',
                          success : function(form,action) {
                            w.close();
                            Ext.Msg.alert('Success',action.result.msg);
                            Ext.getCmp("gridpanel").getStore().reload();                            
                          },
                          failure : function(form,action){
                            Ext.Msg.alert('Error',action.result.msg);
                          }
                        });
                      }
                    }
                  }, 
                  '->', 
                  {
                    text : 'Close',
                    cls : 'x-btn-text-icon',
                    icon : 'images/cross.gif',
                    handler : function(b) { w.close(); }
                  } 
                  ]
                });

            var w = new Ext.Window({
              title : 'New Instructive',
              width : 750,
              height : 450,
              layout : 'fit',
              modal : true,
              plain : true,
              bodyStyle : 'padding:5px;',
              items : form
            });
            w.show();
            w.addListener("beforeshow", function(w) {form.getForm().reset();});
      }    

         function AddWindow2(){
            var ds = new Ext.data.ArrayStore(
            {
               fields: ['value', 'text'],
               data : Ext.combos_selec.dataUsersProgramm,
               sortInfo: {
                  field: 'value',
                  direction: 'ASC'
               }
            });
            var obtenidos=obtenerSeleccionados();
               if(!obtenidos){
                  Ext.MessageBox.show({
                     title: 'Warning',
                     msg: 'You must select a row, by clicking on it, for the delete to work.',
                     buttons: Ext.MessageBox.OK,
                     icon:Ext.MessageBox.ERROR
                  });
                  obtenerTotal();
                  return;
               }
               //alert(obtenidos);//return;
               obtenidos=obtenidos.replace('(','');
               obtenidos=obtenidos.replace(')','');
               var form = new Ext.form.FormPanel({
                     baseCls: 'x-plain',
                     labelWidth: 55,
                     items: 
                     [
                        {
                           xtype:'hidden',
                           id: 'idtinsa',
                           name: 'idtinsa',
                           value: obtenidos,
                           hiddenName: 'idtinsa',
                        },
                        {
                           xtype: 'itemselector',
                           name: 'asignadona',
                           hiddenName  : 'asignadoa',
                           fieldLabel: 'Assigned to',
                           imagePath: 'includes/ext/examples/ux/images/',
                           multiselects:
                           [
                              {
                                 width: 250,
                                 height: 300,
                                 store: ds,
                                 displayField: 'text',
                                 valueField: 'value'
                              },
                              {
                                 width: 250,
                                 height: 300,
                                 store: [['','']],
                                 displayField: 'text',
                                 valueField: 'value',
                                 tbar:
                                 [
                                    {
                                       text: 'clear',
                                       handler:function(){form.getForm().findField().reset();}
                                    }
                                 ]
                              }
                           ]
                        }
                     ],
                     buttonAlign: 'center',
                     buttons: [{
                         text: '<b>Add User</b>',
                         cls: 'x-btn-text-icon',
                         icon: 'images/disk.png',
                         formBind: true,
                         handler: function(b){
                             var form = b.findParentByType('form');
                             //form.getForm().fileUpload = true;
                             if (form.getForm().isValid()) {
                                 form.getForm().submit({
                                     url: 'php/grid_add.php',
                                     waitTitle   :'Please wait!',
                                     waitMsg     :'Loading...',
                                     params: {
                                         tipo : 'userinstructions'
                                     },
                                     timeout: 100000,
                                     method :'POST',
                                     success: function(form, action) {
                                         w.close();
                                         privilegieInstructive.store.nolaboralDetail.reload();
                                         Ext.Msg.alert('Success', action.result.msg);

                                     },
                                     failure: function(form, action) {
                                         Ext.Msg.alert('Error', action.result.msg);
                                     }
                                 });
                             }
                         }
                     },'->',{
                         text: 'Close',
                         cls: 'x-btn-text-icon',
                         icon: 'images/cross.gif',
                         handler: function(b){
                             w.close();
                         }
                     }]
               });
                 
            var w = new Ext.Window({
               title: 'New User Instructive',
               width: 800,
               height: 400,
               layout: 'fit',
               plain: true,
               bodyStyle: 'padding:5px;',
               items: form
            });
            w.show();
            w.addListener("beforeshow",function(w){
               form.getForm().reset();
            });   
         }//fin function doAdd()   

         //Termina add user 
         /*Nuevo Render to Manage*/
         function renderViewtoAddtopic(val, p, record){
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToAddTopic({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructiveId);
         }

         /*function renderViewtoManage(val, p, record){
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructiveId);
         }*/

         /*Termina nuevo render to manage*/
         function renderViewtoEdit(val, p, record)
         {
            if(record.data.istatus !='N')
            {
               return '';
            }
            else
            {
               return String.format('<a href="javascript:void(0)" onClick="viewHtmlToEdit({0})" ><img src="images/editar.png" border="0" title="Edit Instructive" alt="Edit Instructive"></a>',record.data.instructiveId);
            }
         }

         function renderViewtoApprove(val, p, record){
            return String.format('<a href="javascript:void(0)" onClick="viewHtmltoApprove({0})" ><img src="images/check.png" border="0" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructiveId);
         }
         
         var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
         var myView2 = new Ext.grid.GridView();
         myView2.getRowClass = function(record, index, rowParams, store) 
         {     
            if(record.data.isStatus == 'N') return 'rowRed'; //Valida el estado del registro
         };
         myView2.forceFit = true;
         var grid = new Ext.grid.EditorGridPanel({
                 view: myView2,
                 title:wherepage,
                 id:"gridpanel",
                 store: privilegieInstructive.store.principal,
                 iconCls: 'icon-grid',
                 columns: [
                     new Ext.grid.RowNumberer()
                     //,mySelectionModel
                     ,{header: 'Actions', width: 60, dataIndex: 'view', align: 'left', renderer: actions}
                     ,{header: 'ID', width: 30, sortable: true, align: 'left', dataIndex: 'instructiveId'}
                     ,{header: 'Group / Topic', width: 140, sortable: true, align: 'left', dataIndex: 'cabecera'}
                     ,{header: 'Title', width: 160, sortable: true, align: 'left', dataIndex: 'title'}
                     ,{header: 'Body', width: 320, sortable: true, align: 'left', dataIndex: 'description'}
                     ,{header: 'Created By', width: 130, sortable: true, align: 'left', dataIndex: 'userCreator'}
                     ,{header: 'Creation Date', width: 120, sortable: true, align: 'left', dataIndex: 'creationDate'}
                     //,{header: 'Asigned', width: 150, align: 'left', sortable: true, dataIndex: 'userasigned'}
                     ,{header: 'Approved', width: 130, align: 'left', sortable: true, dataIndex: 'userApproved'}
                     ,{header: 'Date Approved', width: 130, align: 'left', sortable: true, dataIndex: 'approvalDate'}
                 ],
                 //clicksToEdit:2,
                 height:Ext.getBody().getViewSize().height-50,
                 sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
                 //width: screen.width,//'99.8%',
                 frame:true,
                 loadMask:true,
                 tbar: pagingBar,
                 viewConfig : {
                   forceFit : true
                 },
                layout: 'fit'
         });     

         
         var pag = new Ext.Viewport({
            layout: 'border',
            hideBorders: true,
            monitorResize: true,
            items: 
            [{
               region: 'north',
               height: 25,
               items: Ext.getCmp('menu_page')
            },
            {
               region:'center',
               autoHeight: true,
               items:
               [
                  {
                     xtype   : 'panel',
                     // width   : 500,
                     items: [grid]               
                  }
               ]
            }]
         });

         privilegieInstructive.store.principal.load({
            params:
            {
               start:0, 
               limit:100
            }
         });
      }

      Ext.onReady(privilegieInstructive.prepare);
