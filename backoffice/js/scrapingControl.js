// JavaScript Document
Ext.ns('scrapingControl');

Ext.override(Ext.form.ComboBox, {
	onTypeAhead : function() {
	}
});

scrapingControl = {
	var : {
		checksSel : new Array()// ,
		// record: new Ext.data.Record.create()
	},
	view : {
		TextFields : {
			anonimo : new Ext.form.TextField({
				allowBlank : false
			})
		},
		combos : {
			evaluation : new Ext.form.ComboBox({
				store : new Ext.data.SimpleStore({
					fields : ['id', 'texto'],
					data : [['>', 'Mayor que'], ['<', 'Menor que'], ['><', 'Dentro del rango'], ['<>', 'Fuera de rango']]
				}),
				valueField : 'id',
				displayField : 'texto',
				listWidth : 150,
				triggerAction : 'all',
				mode : 'local',
				allowBlank : false
			})
		},
		SelectionModels : {
			gridPrincipal : new Ext.grid.CheckboxSelectionModel({
				singleSelect : false
			}),
			gridEjecution : new Ext.grid.CheckboxSelectionModel({
				singleSelect : false
			}),
			actionGrid : new Ext.grid.CheckboxSelectionModel({
				singleSelect : true
			})
		}
	},
	stores : {
		nivels : new Ext.data.SimpleStore({
			fields : ['id', 'desc'],
			// data : [{'id':'1', 'desc':'Nivel - 1'},{'id':'2', 'desc':'Nivel -
			// 2'}]
			data : [['1', 'Nivel - 1'], ['2', 'Nivel - 2']]
		}),
		States : new Ext.data.JsonStore({
			url : 'php/funcionesScraping.php',
			root : 'data',
			autoLoad : true,
			baseParams : {
				option : 'getState',
			},
			fields : [{
				name : 'idstate'
			}, {
				name : 'name'
			}, {
				name : 'code'
			}]
		}),
		Checks : new Ext.data.JsonStore({
			url : 'php/funcionesScraping.php',
			root : 'data',
			autoLoad : true,
			listeners : {
				load : function() {
					return scrapingControl.controllers.updateGridModel()
				}
			},
			baseParams : {
				option : 'getChecks',
			},
			fields : [{
				name : 'id'
			}, {
				name : 'id_check_system'
			}, {
				name : 'idState'
			}, {
				name : 'ejecution'
			}, {
				name : 'dateEjecution'
			}, {
				name : 'Records'
			}, {
				name : 'sampleRecords'
			}]
		}),
		System : new Ext.data.JsonStore({
			autoLoad : true,
			url : 'php/funcionesScraping.php',
			baseParams : {
				option : 'getSystem',
			},
			root : 'data',
			fields : [{
				name : 'id'
			}, {
				name : 'name'
			}]
		})
	},
	models : {

	},
	controllers : {

		doEdit : function(oGrid_Event) {
			var gid = oGrid_Event.record.data.actionid;
			var evaluation = oGrid_Event.record.data.evaluation;
			var gfield = oGrid_Event.field;
			var gvaluenew = oGrid_Event.value;

			Ext.Ajax.request({
				url : 'php/funcionesScraping.php',
				method : 'POST',
				params : {
					gid : gid,
					option : 'updateCheck',
					origin : scrapingControl.var.comoSearchSystem,
					gfield : gfield,
					evaluation : evaluation,
					gvaluenew : gvaluenew
				},
				waitTitle : 'Please wait!',
				waitMsg : 'Saving changes...',
				failure : function(response, options) {
					Ext.MessageBox.alert('Warning', 'Error editing');
				},
				success : function(respuesta) {
					/*if (gfield == 'valprom2') {
					 oGrid_Event.record.data.valprom = ((oGrid_Event.record.data.valpromcru * gvaluenew) / oGrid_Event.record.data.porc);
					 }*/
					scrapingControl.stores.GridPrincipal.commitChanges();
					return true;
				}
			});
		},
		doCompare : function() {

			if ( typeof scrapingControl.view.doCompare == 'undefined') {
				scrapingControl.view.doCompare = new Array();
				scrapingControl.view.doCompare.grid = new Ext.grid.GridPanel({
					store : scrapingControl.stores.Checks,
					columns : [new Ext.grid.RowNumberer(), scrapingControl.view.SelectionModels.gridEjecution, {
						header : 'Id',
						dataIndex : 'id',
						sortable : true
					}, {
						header : 'ejecution',
						dataIndex : 'ejecution',
						sortable : true
					}, {
						header : 'Date',
						dataIndex : 'dateEjecution',
						width : 150,
						sortable : true
					}, {
						header : 'Records',
						dataIndex : 'Records',
						sortable : true
					}, {
						xtype : 'actioncolumn',
						width : 30,
						items : [{
							icon : '/backoffice/images/delete.gif',
							tooltip : 'Remover',
							align : 'center',
							handler : function(grid, rowIndex, colIndex) {
								console.debug('hola mundo');
								var rec = scrapingControl.stores.Checks.getAt(rowIndex);
								var gid = rec.get('ejecution');
								Ext.Ajax.request({
									url : 'php/funcionesScraping.php',
									method : 'POST',
									params : {
										gid : gid,
										option : 'removeCheckValues'
									},
									waitTitle : 'Please wait!',
									waitMsg : 'Saving changes...',
									failure : function(response, options) {
										Ext.MessageBox.alert('Warning', 'Error editing');
									},
									success : function(respuesta) {
										Ext.MessageBox.alert('Success', 'Success true');
										scrapingControl.stores.Checks.reload();
										return true;
									}
								});
							}
						}]
					}],
					border : false,
					sm : scrapingControl.view.SelectionModels.gridEjecution,
					stripeRows : true
				});

				scrapingControl.view.doCompare.win = new Ext.Window({
					title : 'Grid ejecutions',
					closeAction : 'hide',
					bbar : [{
						text : 'Compare',
						handler : function() {
							var selec = scrapingControl.view.doCompare.grid.selModel.getSelections();
							var i = 0;
							scrapingControl.var.markToView = new Array();
							for ( i = 0; i < selec.length; i++) {
								scrapingControl.var.markToView.push(selec[i]);
							}
							console.debug(scrapingControl.var.markToView);
							scrapingControl.controllers.updateGridModel();
						}
					}],
					layout : 'fit',
					width : 610,
					height : 350,
					items : [scrapingControl.view.doCompare.grid]
				});

			}
			scrapingControl.view.doCompare.win.show();
		},

		doEditActions : function(oGrid_Event) {
			var gid = oGrid_Event.record.data.id;
			var gfield = oGrid_Event.field;
			var gvaluenew = oGrid_Event.value;
			Ext.Ajax.request({
				url : 'php/funcionesScraping.php',
				method : 'POST',
				params : {
					gid : gid,
					option : 'updateCheckOrigin',
					gfield : gfield,
					gvaluenew : gvaluenew
				},
				waitTitle : 'Please wait!',
				waitMsg : 'Saving changes...',
				failure : function(response, options) {
					Ext.MessageBox.alert('Warning', 'Error editing');
				},
				success : function(respuesta) {
					scrapingControl.stores.actions.commitChanges();
					return true;
				}
			});
		},
		colorAndDateHandler : function(menuItem, choice) {
			console.debug(choice);
			console.debug(menuItem);
			Ext.getCmp('colortitle').setValue('#' + choice);
		},
		updateGridModel : function() {
			scrapingControl.var.checksSel = new Array();
			/*
			 * if(scrapingControl.var.checksSel.length>0){ } else{
			 */
			if (scrapingControl.var.markToView) {
				Ext.each(scrapingControl.var.markToView, function(e, i) {
					scrapingControl.var.checksSel.push(e.id);
				});
			} else {
				scrapingControl.stores.Checks.each(function(e, i) {
					if (i < 5)
						scrapingControl.var.checksSel.push(e.id);
				});
			}
			scrapingControl.var.markToView = null;
			scrapingControl.var.checksSel.reverse();
			// }
			var fieldStore = scrapingControl.var.fieldsStore;
			var cm = [scrapingControl.view.SelectionModels.gridPrincipal,
			/*
			 * { header : "", width : 40, align : 'center', sortable : true,
			 * dataIndex : 'source', renderer :
			 * scrapingControl.controllers.renders.render5 },
			 */
			{
				header : "ORDEN",
				width : 70,
				align : 'center',
				sortable : true,
				dataIndex : 'orden',
				editor : scrapingControl.view.TextFields.anonimo
			}, {
				header : "ACTIONID",
				width : 70,
				align : 'center',
				hidden : true,
				sortable : true,
				dataIndex : 'actionid'
			}, {
				header : "TITULO",
				width : 300,
				align : 'left',
				sortable : true,
				dataIndex : 'titulo',
				renderer : scrapingControl.controllers.renders.render4,
				editor : scrapingControl.view.TextFields.anonimo
			}, {
				header : "Qx%",
				tooltip : 'Quantity by %',
				hidden : false,
				width : 70,
				align : 'right',
				sortable : true,
				//renderer : scrapingControl.controllers.renders.render1,
				dataIndex : 'valprom2',
				editor : scrapingControl.view.TextFields.anonimo
			}, {
				header : "Evaluation",
				tooltip : 'Evaluation',
				hidden : false,
				width : 75,
				align : 'center',
				sortable : true,
				dataIndex : 'evaluation',
				renderer : scrapingControl.controllers.renders.evaluation,
				editor : scrapingControl.view.combos.evaluation
			}, {
				header : "Max Variation",
				tooltip : 'Maximum Variation',
				hidden : false,
				width : 70,
				align : 'right',
				sortable : true,
				renderer : scrapingControl.controllers.renders.render1,
				dataIndex : 'valprom'
			}, {
				header : "Value Cal",
				tooltip : 'Value To Cal',
				hidden : false,
				width : 70,
				align : 'right',
				sortable : true,
				//renderer : scrapingControl.controllers.renders.render1,
				editor : scrapingControl.view.TextFields.anonimo,
				dataIndex : 'all_record'
			}]
			var defaultData = [{
				name : 'actionid',
				type : 'int'
			}, {
				name : 'orden',
				type : 'int'
			}, {
				name : 'titulo',
				type : 'string'
			}, 'tipocampo', 'source', 'colortitle', 'porc', 'valporc', 'valporcf', 'valprom2', 'valprom', 'valpromcru', 'evaluation', 'all_record', {
				name : 'promedio0',
				type : 'int'
			}, {
				name : 'promedio1',
				type : 'int'
			}, {
				name : 'promedio2',
				type : 'int'
			}, {
				name : 'promedio3',
				type : 'int'
			}, {
				name : 'promedio4',
				type : 'int'
			}, {
				name : 'promedio5',
				type : 'int'
			}, {
				name : 'promedio6',
				type : 'int'
			}, {
				name : 'promedio7',
				type : 'int'
			}, {
				name : 'promedio8',
				type : 'int'
			}, {
				name : 'promedio9',
				type : 'int'
			}, {
				name : 'promedio10',
				type : 'int'
			}, {
				name : 'promedio11',
				type : 'int'
			}, {
				name : 'promedio12',
				type : 'int'
			}];

			var aux = (scrapingControl.var.checksSel.length - 2);
			var lastItemEval = null;
			Ext.each(scrapingControl.var.checksSel, function(e, i) {
				var itemEval = scrapingControl.stores.Checks.getById(e);
				lastItemEval = itemEval;

				defaultData.push(itemEval.id);
				defaultData.push({
					name : 'promedio' + i,
					type : 'int'
				});

				fieldStore.push(itemEval.id);
				cm.push({
					header : itemEval.data.dateEjecution,
					width : 90,
					sortable : true,
					align : 'right',
					dataIndex : itemEval.id,
					renderer : scrapingControl.controllers.renders.render2
				});
				if (i < aux) {
					cm.push({
						header : 'Diff ' + (scrapingControl.var.checksSel.length - (i + 1)),
						width : 90,
						sortable : true,
						align : 'right',
						dataIndex : 'promedio' + (i + 1),
						renderer : scrapingControl.controllers.renders.changeColor
					});
				}
			});
			cm.push({
				header : 'Diff 1',
				width : 90,
				sortable : true,
				align : 'right',
				dataIndex : 'promedio' + (scrapingControl.var.checksSel.length - 1),
				renderer : scrapingControl.controllers.renders.changeColor
			});

			scrapingControl.var.fieldsStore = defaultData;

			scrapingControl.utility.columnMode = new Ext.grid.ColumnModel(cm);

			scrapingControl.var.gridReader = new Ext.data.JsonReader({
				root : 'data',
				totalProperty : 'total',
				fields : scrapingControl.var.fieldsStore
			});

			scrapingControl.stores.GridPrincipal = new Ext.data.Store({
				baseParams : {
					option : 'getChecksValues'
				},
				proxy : scrapingControl.utility.proxy,
				listeners : {
					beforeload : function() {
					},
					load : function(self, records, options) {
					}
				},
				reader : scrapingControl.var.gridReader
			});

			scrapingControl.view.grid.reconfigure(scrapingControl.stores.GridPrincipal, scrapingControl.utility.columnMode);
			// scrapingControl.utility.columnMode.setConfig(cm);

			scrapingControl.stores.GridPrincipal.load({
				params : {
					ejecutions : scrapingControl.var.checksSel.join(','),
					system : scrapingControl.var.comoSearchSystem,
					comoSearchIntervalCal : scrapingControl.var.comoSearchIntervalCal,
					state : scrapingControl.var.comoSearchState
				}
			});
		},

		obtenerSeleccionados : function() {
			var selec = scrapingControl.view.grid.selModel.getSelections();
			var i = 0;
			var marcados = '(';
			for ( i = 0; i < selec.length; i++) {
				if (i > 0)
					marcados += ',';
				marcados += selec[i].json.actionid;
			}
			marcados += ')';
			if (i == 0)
				marcados = false;
			return marcados;
		},

		doEditActionId : function() {
			var obtenidos = scrapingControl.controllers.obtenerSeleccionados();
			if (!obtenidos || obtenidos.split(',').length > 1) {
				Ext.MessageBox.show({
					title : 'Warning',
					msg : 'You must select a single row.',
					buttons : Ext.MessageBox.OK,
					icon : Ext.MessageBox.ERROR
				});
				return;
			}
			obtenidos = obtenidos.replace('(', '');
			obtenidos = obtenidos.replace(')', '');
			Ext.Ajax.request({
				waitMsg : 'Loading...',
				url : 'php/grid_dataaccesscontrol.php',
				method : 'POST',
				timeout : 100000,
				params : {
					tipo : 'getactionid',
					actionid : obtenidos
				},
				failure : function(response, options) {
					// Ext.MessageBox.alert('Warning','Error Loading');
				},
				success : function(response) {
					Ext.QuickTips.init();
					var variable = Ext.decode(response.responseText);

					var form = new Ext.form.FormPanel({
						baseCls : 'x-plain',
						labelWidth : 55,
						items : [{
							xtype : 'textfield',
							fieldLabel : 'Action',
							name : 'action',
							id : 'action',
							width : 280,
							value : variable.titulo,
							allowBlank : false
						}, {
							xtype : 'compositefield',
							fieldLabel : 'Color Act',
							msgTarget : 'side',
							defaults : {
								flex : 1
							},
							items : [{
								xtype : 'splitbutton',
								text : 'Set color',
								disabled : false,
								width : 70,
								itemId : 'splitbut',
								menu : [{
									text : 'Choose Color',
									menu : {
										xtype : 'colormenu',
										handler : function() {
											return scrapingControl.controllers.colorAndDateHandler()
										},
										listeners : {
											click : function() {
												return false;
											}
										}
									}
								}]
							}, {
								xtype : 'textfield',
								fieldLabel : 'Action',
								name : 'colortitle',
								id : 'colortitle',
								value : variable.colortitle,
								width : 60
							}]
						}, {
							xtype : 'numberfield',
							fieldLabel : 'Orden',
							name : 'orden',
							id : 'orden',
							value : variable.orden,
							allowBlank : false
						}, {
							xtype : 'combo',
							store : new Ext.data.SimpleStore({
								fields : ['id', 'texto'],
								data : [['int', 'Int'], ['int', 'Float'], ['date', 'Date']]
							}),
							fieldLabel : 'DataType',
							id : 'datatype',
							name : 'datatype',
							hiddenName : 'datatype',
							valueField : 'id',
							displayField : 'texto',
							triggerAction : 'all',
							mode : 'local',
							value : variable.tipocampo,
							allowBlank : false
						}, {
							xtype : 'combo',
							store : new Ext.data.SimpleStore({
								fields : ['id', 'texto'],
								data : [['Psummary', 'Psummary'], ['Mlsresidential', 'Mlsresidential'], ['Rtmaster', 'Rtmaster'], ['Pendes', 'Pendes'], ['Cantidades', 'Cantidades'], ['Rental', 'Rental'], ['Mortgage', 'Mortgage'], ['Sales', 'Sales'], ['Latlong-Marketvalue', 'Latlong-Marketvalue'], ['Calulos', 'Calculos'], ['Exray', 'Exray'], ['Tiempos', 'Tiempos']]
							}),
							fieldLabel : 'Table',
							id : 'source',
							name : 'source',
							hiddenName : 'source',
							valueField : 'id',
							displayField : 'texto',
							triggerAction : 'all',
							value : variable.source,
							mode : 'local',
							allowBlank : false
						}],
						buttonAlign : 'center',
						buttons : [{
							text : '<b>Save</b>',
							cls : 'x-btn-text-icon',
							icon : 'images/disk.png',
							formBind : true,
							handler : function(b) {
								var form = b.findParentByType('form');
								// form.getForm().fileUpload = true;
								if (form.getForm().isValid()) {
									form.getForm().submit({
										url : 'php/grid_dataaccesscontrol.php',
										waitTitle : 'Please wait!',
										waitMsg : 'Loading...',
										params : {
											tipo : 'editactionid',
											actionid : obtenidos
										},
										timeout : 100000,
										method : 'POST',
										success : function(form, action) {
											w.close();
											store.reload();
											Ext.Msg.alert('Success', 'Update succesfully ');

										},
										failure : function(form, action) {
											Ext.Msg.alert('Error', action.result.msg);
										}
									});
								}
							}
						}, '->', {
							text : 'Close',
							cls : 'x-btn-text-icon',
							icon : 'images/cross.gif',
							handler : function(b) {
								w.close();
							}
						}]
					});

					var w = new Ext.Window({
						title : 'Edit Action Id',
						width : 400,
						height : 250,
						layout : 'fit',
						plain : true,
						bodyStyle : 'padding:5px;',
						items : form
					});
					w.show();
					w.addListener("beforeshow", function(w) {
						form.getForm().reset();
					});

				}
			})

		},
		doDel : function() {
			Ext.MessageBox.confirm('Delete Action Id', 'Are you sure delete row?.', //
			function(btn, text) {
				if (btn == 'yes') {
					var obtenidos = scrapingControl.controllers.obtenerSeleccionados();
					if (!obtenidos) {
						Ext.MessageBox.show({
							title : 'Warning',
							msg : 'You must select a row, by clicking on it, for the delete to work.',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.ERROR
						});
						return;
					}
					obtenidos = obtenidos.replace('(', '');
					obtenidos = obtenidos.replace(')', '');

					Ext.Ajax.request({
						waitMsg : 'Saving changes...',
						url : 'php/funcionesScraping.php',
						method : 'POST',
						params : {
							option : 'deleteCheck',
							actionid : obtenidos
						},

						failure : function(response, options) {
							Ext.MessageBox.alert('Warning', 'Error editing');
							store.reload();
						},

						success : function(response, options) {

							store.reload();
						}
					});
				}
			})
		},
		initloadData : function() {
			scrapingControl.var.comoSearchSystem = Ext.getCmp('comoSearchSystem').getValue();
			scrapingControl.var.comoSearchState = Ext.getCmp('comoSearchState').getValue();
			scrapingControl.var.comoSearchIntervalCal = Ext.getCmp('comoSearchIntervalCal').getValue();
			scrapingControl.stores.Checks.load({
				params : {
					system : scrapingControl.var.comoSearchSystem,
					state : scrapingControl.var.comoSearchState,
					comoSearchIntervalCal : scrapingControl.var.comoSearchIntervalCal
				}
			});
		},
		renders : {
			render1 : function(val, p, record) {

				var promedio = Math.abs(record.data.valprom);
				var procentage = record.data.valprom2;
				var value = Math.abs(val);
				if (procentage != 0) {
					if (record.data.evaluation == '>' || record.data.evaluation == '<') {
						var valEval = ((promedio * (procentage / 100)) );
					} else if (record.data.evaluation == '<>' || record.data.evaluation == '><') {
						var valEval = ((promedio * ((-1 * procentage) / 100)) );
						var valEval2 = ((promedio * (procentage / 100)) );
					}
				} else {
					valEval = promedio;
				}

				if (record.data.evaluation == '<>' || record.data.evaluation == '><') {
					valEval = Ext.util.Format.number(valEval, '0,000.00');
					valEval2 = Ext.util.Format.number(valEval2, '0,000.00');
					return String.format('<b>{0} || {1}</b>', valEval, valEval2);

				} else {
					if (record.data.tipocampo == 'int')
						val = Ext.util.Format.number(valEval, '0,000.00');
					else if (record.data.tipocampo == 'porc')
						val = Ext.util.Format.number(valEval, '0,000.00');
					else {
						if (scrapingControl.utility.isFloat(valEval)) {
							val = Ext.util.Format.number(valEval, '0,000.00');
						} else {
							val = Ext.util.Format.number(0, '0,000.00');
						}
					}
					return String.format('<b>{0}</b>', val);
				}
			},
			render1Prome : function(val, p, record) {
				val = val * record.data.valprom2;
				if (record.data.tipocampo == 'int')
					val = Ext.util.Format.number(val, '0,000.00');
				if (record.data.tipocampo == 'porc')
					val = Ext.util.Format.number(val, '0,000.00');
				return String.format('<b>{0}</b>', val);
			},
			evaluation : function(val, p, record) {
				var store = scrapingControl.view.combos.evaluation.getStore();
				var val2 = store.getAt(store.find('id', val));
				if ( typeof val2 !== "undefined")
					return val2.data.texto;
			},
			checkRow : function(val, p, record) {
				return String.format('<input type="checkbox" name="del' + record.data.idcounty + '"  id="del' + record.data.idcounty + '">', record.data.idcounty);
			},
			cellCheck : function(value, metaData, record, rowIndex, colIndex, store) {
				if (value) {
					metaData.style += 'background-color:#A9F5A9';
				} else {
					metaData.style += 'background-color:#F5A9A9';

				}
				var t = this.trueText,
				    f = this.falseText,
				    u = this.undefinedText;

				if (value === undefined) {
					return u;
				}
				if (!value || value === 'false') {
					return f;
				}
				return t;
			},
			changeColor : function(val, p, record, a, b, c, d) {
				//console.debug(1);
				var promedio = Math.abs(record.data.valprom);
				var procentage = Math.abs(record.data.valprom2);
				var value = Math.abs(val);
				var status = true;
				if (record.data.evaluation == '<') {
					var valEval = ((promedio * (procentage / 100)));
					if (valEval < value) {
						status = false;
					}
				} else if (record.data.evaluation == '>') {
					var valEval = ((promedio * (procentage / 100)));
					console.info(valEval, value, '<', valEval > value);
					if (valEval > value) {
						status = false;
					}

				} else if (record.data.evaluation == '<>') {
					var valEval = ((promedio * ((-1 * procentage) / 100)) );
					var valEval2 = ((promedio * (procentage / 100)) );
					if (valEval > value || valEval2 < value) {
						status = false;
					}
				} else if (record.data.evaluation == '><') {
					var valEval = ((promedio * ((-1 * procentage) / 100)) );
					var valEval2 = ((promedio * (procentage / 100)) );
					if (valEval < value || valEval2 > value) {
						status = false;
					}
				}

				if (status) {
					if (record.data.tipocampo == 'int')
						val = Ext.util.Format.number(val, '0,000.00');
					if (record.data.tipocampo == 'porc')
						val = Ext.util.Format.number(val, '0,000.00');
					return val;//return '<span style="color:green;font-weight:  bold;">' + val + '</span>';
				} else {
					if (record.data.tipocampo == 'int')
						val = Ext.util.Format.number(val, '0,000.00');
					if (record.data.tipocampo == 'porc')
						val = Ext.util.Format.number(val, '0,000.00');
					return val;//return '<span style="color:red;font-weight:  bold;">' + val + '</span>';
				}

				return val;
			},
			render2 : function(val, p, record) {
				if (record.data.tipocampo == 'int')
					val = Ext.util.Format.number(val, '0,000.00');
				else if (record.data.tipocampo == 'porc')
					val = Ext.util.Format.number(val, '0,000.00');
				else {
					if (scrapingControl.utility.isFloat(val)) {
						val = Ext.util.Format.number(val, '0,000.00');
					}
				}
				return val;
			},
			render3 : function(val, p, record) {
				return String.format('<b>{0}</b>', val);
			},
			render4 : function(val, p, record) {
				if (record.data.colortitle != '')
					p.style += 'color:' + record.data.colortitle + ';font-weight: bold;';
				// return '<span style="">' + val + '</span>';
				return val;
			},
			render5 : function(val, p, record) {
				if (val == 'Psummary')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Mlsresidential')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Rtmaster')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Pendes')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Cantidades')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Rental')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Mortgage')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Sales')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Latlong-Marketvalue')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -160px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Calulos')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -180px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Exray')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -200px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
				if (val == 'Tiempos')
					return '<div style="background: url(\'images/semaforo.png\') no-repeat scroll 0px -220px transparent; width:20px; height: 20px;" title="' + val + '">&nbsp;</div>';
			}
		}
	},
	utility : {
		proxy : new Ext.data.HttpProxy({
			url : 'php/funcionesScraping.php',
			type : 'POST',
			timeout : 3600000
		}),

		isFloat : function(n) {
			if (n === Number(n) && n % 1 !== 0) {
				return true;
			} else {
				console.info('---no se numero:');
				console.debug(n);
				return false;
			}
		}
	},
	init : function() {
		console.info('init');
		/*
		 * inicializando grid principal
		 */
		scrapingControl.var.cmDefault = [scrapingControl.view.SelectionModels.gridPrincipal, {
			header : "",
			width : 40,
			align : 'center',
			sortable : true,
			dataIndex : 'source',
			renderer : scrapingControl.controllers.renders.render5
		}, {
			header : "ORDEN",
			width : 70,
			align : 'center',
			sortable : true,
			dataIndex : 'orden',
			editor : scrapingControl.view.TextFields.anonimo
		}, {
			header : "ACTIONID",
			width : 70,
			align : 'center',
			sortable : true,
			dataIndex : 'actionid'
		}, {
			id : 'actionid',
			header : "TITULO",
			width : 300,
			align : 'left',
			sortable : true,
			dataIndex : 'titulo',
			renderer : scrapingControl.controllers.renders.render4,
			editor : scrapingControl.view.TextFields.anonimo
		}];

		scrapingControl.utility.columnMode = new Ext.grid.ColumnModel(scrapingControl.var.cmDefault);

		// scrapingControl.var.fieldsStore=;

		// scrapingControl.var.record = new
		// Ext.data.Record.create(scrapingControl.var.fieldsStore);

		scrapingControl.var.gridReader = new Ext.data.JsonReader({
			root : 'data',
			totalProperty : 'total',
			fields : scrapingControl.var.fieldsStore
		});

		scrapingControl.stores.GridPrincipal = new Ext.data.Store({
			baseParams : {
				option : 'getChecksValues'
			},
			proxy : scrapingControl.utility.proxy,
			listeners : {
				beforeload : function() {
				},
				load : function(self, records, options) {
				}
			},
			reader : scrapingControl.var.gridReader
		});
		/**
		 * barra del pagineo
		 */
		scrapingControl.view.pagingBar = new Ext.PagingToolbar({
			pageSize : 5000,
			store : scrapingControl.stores.GridPrincipal,
			displayInfo : true,
			displayMsg : '<b>Total: {2}</b>',
			emptyMsg : "No topics to display",
			items : [{
				iconCls : 'icon',
				cls : 'x-btn-text-icon',
				icon : 'images/chgstat.png',
				id : 'compareact_butt',
				text : 'Compare Actions',
				tooltip : 'Click to Compare Actions',
				// handler: doCompareActions
			}, '-', {
				iconCls : 'icon',
				cls : 'x-btn-text-icon',
				icon : 'images/refresh.png',
				id : 'compare_butt',
				text : 'Compare Dates',
				tooltip : 'Click to Compare Date',
				handler : scrapingControl.controllers.doCompare
			}, {
				xtype : 'combo',
				store : scrapingControl.stores.States,
				valueField : 'idstate',
				id : 'comoSearchState',
				width : 200,
				displayField : 'name',
				triggerAction : 'all',
				emptyText : 'Select a State',
				fieldLabel : 'State'
			}, {
				xtype : 'combo',
				store : scrapingControl.stores.System,
				valueField : 'id',
				displayField : 'name',
				id : 'comoSearchSystem',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'System'
			}, {
				xtype : 'combo',
				store : new Ext.data.SimpleStore({
					fields : ['id', 'texto'],
					data : [['calc|6', '6 calculus'], ['calc|12', '12 calculus'], ['calc|18', '18 calculus'], ['time|1_week', '1 Week'], ['time|1_month', '1 month'], ['time|2_month', '2 month'], ['time|6_month', '6 month']]
				}),
				valueField : 'id',
				displayField : 'texto',
				id : 'comoSearchIntervalCal',
				value : 'calc|12',
				mode : 'local',
				triggerAction : 'all',
				emptyText : 'Select a System',
				fieldLabel : 'intervalCal'
			}, {
				iconCls : 'icon',
				cls : 'x-btn-text-icon',
				// icon: 'images/refresh.png',
				// id: 'compare_butt',
				text : 'Show Statistics',
				handler : scrapingControl.controllers.initloadData
			}]
		});

		scrapingControl.view.adminActions = {};
		/*
		 *
		 * store de aciones creadas en el sistema
		 */
		scrapingControl.stores.actions = new Ext.data.Store({
			baseParams : {
				option : 'getAllChecksBySystem'
			},
			autoLoad : true,
			proxy : scrapingControl.utility.proxy,
			listeners : {
				beforeload : function() {
				},
				load : function(a, b, c, d) {
				}
			},
			reader : new Ext.data.JsonReader({
				root : 'data',
				totalProperty : 'total',
				fields : scrapingControl.var.fieldsStoreSystem
			})
		});
		scrapingControl.view.adminActions.gridActions = new Ext.grid.EditorGridPanel({
			viewConfig : {
				forceFit : true
			},
			tbar : [{
				xtype : 'combo',
				store : scrapingControl.stores.nivels,
				displayField : 'desc',
				valueField : 'id',
				typeAhead : true,
				id : 'nivelToProcess',
				mode : 'local',
				value : '2',
				width : '100px',
				forceSelection : true,
				triggerAction : 'all',
				emptyText : 'Select a Nivel...',
				selectOnFocus : true,
				listeners : {
					select : function() {
						scrapingControl.stores.actions.reload({
							params : {
								nivel : Ext.getCmp('nivelToProcess').getValue()
							}
						});
					}
				}
			}, {
				iconCls : 'icon',
				cls : 'x-btn-text-icon',
				icon : 'images/add.png',
				text : 'Add Check',
				handler : function() {
					var panel = scrapingControl.view.adminActions.panel.getComponent('PanelOpt');
					if (panel.collapsed) {
						panel.expand();
					}
					panel.doLayout(true);
					panel.findByType('form')[0].getForm().reset();
				}
			}, {
				text : 'Edit Check',
				iconCls : 'icon',
				cls : 'x-btn-text-icon',
				icon : 'images/editar.png',
				handler : function() {
					var panel = scrapingControl.view.adminActions.panel.getComponent('PanelOpt');
					if (panel.collapsed) {
						panel.expand();
						panel.doLayout(true);
					}
					var form = scrapingControl.view.adminActions.panel.findByType('form')[0].getForm();
					form.reset();
					var selec = scrapingControl.view.adminActions.gridActions.selModel.getSelections();
					var i = 0;
					form.setValues({
						idCheck : selec[0].id,
						codePhp : selec[0].data.codePhp,
						action : selec[0].data.titulo,
						nivel : selec[0].data.nivel,
						colortitle : selec[0].data.colortitle,
						orden : selec[0].data.orden
					});
					return true;
				}
			}],
			store : scrapingControl.stores.actions,
			clicksToEdit : 2,
			cm : scrapingControl.utility.cmAction,
			autoScroll : true,
			sm : scrapingControl.view.SelectionModels.actionGrid,
			height : Ext.getBody().getViewSize().height - 130,
			loadMask : true,
			listeners : {
				afteredit : scrapingControl.controllers.doEditActions
			},
			// tbar: scrapingControl.view.pagingBar
		});
		scrapingControl.view.adminActions.panel = new Ext.Panel({
			title : 'Actions Administration',
			height : Ext.getBody().getViewSize().height - 100,
			layout : 'border',
			defaults : {
				collapsible : true,
				split : true
			},
			items : [{
				title : 'Actions Assignation',
				region : 'center',
				items : [scrapingControl.view.adminActions.gridActions],
				margins : '5 0 0 0'
			}, {
				region : 'west',
				itemId : 'PanelOpt',
				margins : '5 0 0 0',
				cmargins : '5 5 0 0',
				title : 'Actions Config',
				titleCollapse : true,
				width : Ext.getBody().getViewSize().width / 4,
				items : [{
					xtype : 'form',
					baseCls : 'x-plain',
					labelWidth : 55,
					items : [{
						xtype : 'textfield',
						fieldLabel : 'Action',
						name : 'action',
						itemId : 'action',
						allowBlank : false
					}, {
						xtype : 'combo',
						store : scrapingControl.stores.nivels,
						displayField : 'desc',
						fieldLabel : 'Nivel',
						valueField : 'id',
						typeAhead : true,
						name : 'nivel',
						itemId : 'nivel',
						hiddenName : 'nivel',
						hiddenValue : 'desc',
						mode : 'local',
						value : '2',
						forceSelection : true,
						triggerAction : 'all',
						emptyText : 'Select a Nivel...',
						selectOnFocus : true,
						listeners : {
							select : function() {
							}
						}
					}, {
						xtype : 'compositefield',
						fieldLabel : 'Color Act',
						msgTarget : 'side',
						/*
						 * defaults: { flex: 1 },
						 */
						items : [{
							xtype : 'splitbutton',
							text : 'Set color',
							disabled : false,
							width : 70,
							itemId : 'splitbut',
							menu : [{
								text : 'Choose Color',
								menu : {
									xtype : 'colormenu',
									handler : function() {
										return scrapingControl.controllers.colorAndDateHandler()
									},
									listeners : {
										click : function() {
											return false;
										}
									}
								}
							}]
						}, {
							xtype : 'textfield',
							fieldLabel : 'Action',
							name : 'colortitle',
							itemId : 'colortitle',
							width : 60
						}]
					}, {
						xtype : 'hidden',
						fieldLabel : 'idCheck',
						name : 'idCheck',
						itemId : 'idCheck',
						allowBlank : false
					}, {
						xtype : 'numberfield',
						fieldLabel : 'Orden',
						name : 'orden',
						itemId : 'orden',
						allowBlank : false
					}, {
						xtype : 'textarea',
						itemId : 'codePhp',
						fieldLabel : 'Php Code',
						name : 'codePhp',
						width : 250,
						height : Ext.getBody().getViewSize().height / 2,
						allowBlank : false
					}],
					buttonAlign : 'center',
					buttons : [{
						text : 'Save',
						cls : 'x-btn-text-icon',
						icon : 'images/disk.png',
						formBind : true,
						handler : function(b) {
							var form = b.findParentByType('form');
							if (form.getForm().isValid()) {
								form.getForm().submit({
									url : 'php/funcionesScraping.php',
									waitTitle : 'Please wait!',
									waitMsg : 'Loading...',
									params : {
										option : 'addCheck'
									},
									timeout : 100000,
									method : 'POST',
									success : function(form, action) {
										scrapingControl.stores.actions.reload();
										scrapingControl.view.adminActions.panel.getComponent('PanelOpt').collapse();
										Ext.Msg.alert('Success', action.result.msg);

									},
									failure : function(form, action) {
										Ext.Msg.alert('Error', action.result.msg);
									}
								});
							}
						}
					}, {
						text : 'Close',
						cls : 'x-btn-text-icon',
						icon : 'images/cross.gif',
						handler : function(b) {
							scrapingControl.view.adminActions.panel.getComponent('PanelOpt').collapse();

						}
					}]
				}],
				collapsed : true
			}]
		});

		scrapingControl.view.grid = new Ext.grid.EditorGridPanel({
			viewConfig : {
				forceFit : true
			},
			layout : 'fit',
			store : scrapingControl.stores.GridPrincipal,
			clicksToEdit : 2,
			cm : scrapingControl.utility.columnMode,
			autoScroll : true,
			sm : scrapingControl.view.SelectionModels.gridPrincipal,
			height : Ext.getBody().getViewSize().height - 50,
			frame : true,
			title : 'Scraping Control',
			listeners : {
				afteredit : scrapingControl.controllers.doEdit
			},
			loadMask : true,
			tbar : scrapingControl.view.pagingBar
		});

		/***********************************************************************
		 *
		 * init viewport (marco principal)
		 */

		scrapingControl.view.pag = new Ext.Viewport({
			layout : 'border',
			hideBorders : true,
			monitorResize : true,
			items : [{
				region : 'north',
				height : 25,
				items : Ext.getCmp('menu_page')
			}, {
				xtype : 'tabpanel',
				region : 'center',
				autoHeight : true,
				activeTab : 0,
				items : [scrapingControl.view.grid, scrapingControl.view.adminActions.panel]
			}]
		});
	},
	prepare : function() {

		var defaultData = [{
			name : 'actionid',
			type : 'int'
		}, {
			name : 'orden',
			type : 'int'
		}, {
			name : 'titulo',
			type : 'string'
		}, 'tipocampo', 'source', 'colortitle', 'porc', 'valporc', 'valporcf', 'valprom2', 'valprom', 'valpromcru', {
			name : 'promedio0',
			type : 'int'
		}, {
			name : 'promedio1',
			type : 'int'
		}, {
			name : 'promedio2',
			type : 'int'
		}, {
			name : 'promedio3',
			type : 'int'
		}, {
			name : 'promedio4',
			type : 'int'
		}, {
			name : 'promedio5',
			type : 'int'
		}, {
			name : 'promedio6',
			type : 'int'
		}, {
			name : 'promedio7',
			type : 'int'
		}, {
			name : 'promedio8',
			type : 'int'
		}];
		scrapingControl.var.fieldsStore = defaultData;
		scrapingControl.var.fieldsStoreSystem = ['id', {
			name : 'orden',
			type : 'int'

		}, 'nivel', 'titulo', 'tipocampo', 'source', 'colortitle', 'codePhp'];
		scrapingControl.var.fieldsStoreSystem.push({
			name : 'State'
		});

		Ext.Ajax.request({
			url : 'php/funcionesScraping.php',
			type : 'POST',
			params : {
				option : 'getSystem'
			},
			scope : this,
			success : function(response) {
				var data = Ext.util.JSON.decode(response.responseText);
				var temCm = [new Ext.grid.RowNumberer(), scrapingControl.view.SelectionModels.actionGrid, {
					header : "ACTIONID",
					width : 25,
					align : 'center',
					sortable : true,
					dataIndex : 'id'
				}, {
					header : "Orden",
					width : 25,
					editor : scrapingControl.view.TextFields.anonimo,
					dataIndex : 'orden',
					align : 'right',
					sortable : true,
				}, {
					header : "Name",
					dataIndex : 'titulo',
					renderer : scrapingControl.controllers.renders.render4,
					sortable : true,
					editor : scrapingControl.view.TextFields.anonimo
				}];

				Ext.each(data.data, function(data, i) {
					temCm.push({
						editor : {
							xtype : 'checkbox'
						},
						header : data.name,
						width : 45,
						//xtype		: 'booleancolumn',
						tooltip : data.name,
						trueText : 'Yes',
						falseText : 'No',
						align : 'center',
						sortable : true,
						renderer : scrapingControl.controllers.renders.cellCheck,
						/*renderer	: function(value, metaData, record, rowIndex, colIndex, store) {
						 console.debug(23);
						 },*/
						dataIndex : 'system_' + data.id

					});
					scrapingControl.var.fieldsStoreSystem.push({
						name : 'system_' + data.id
					});
				});
				scrapingControl.utility.cmAction = new Ext.grid.ColumnModel(temCm);
				scrapingControl.init();
			}
		});
	}
}
Ext.onReady(scrapingControl.prepare);
