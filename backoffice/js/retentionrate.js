
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		remoteSort : true,
		url: 'php/grid_data.php?tipo=retentionrate',
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'email'
			,'status'			
			,'hometelephone'
			,'affiliationdate'
			,'countactive'
			,'frec'
			,'avgcountactive'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.form.FormPanel({
		layout: 'form',
		items: [
				{
				fieldLabel:'Status',
				width:150,
				name:'cbStatusret',
				id:'cbStatusret',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.dataStatus3 
				}),
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: true,
				editable: false,
				value: '*',
				listeners: {
					'select': searchStatus
				} 
			}
		]
			
	});
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 50,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Displaying {0} - {1} of {2}</b>',
        emptyMsg: "No topics to display",
		items:[panelpag,'-',
		{
				width:200,
				xtype:'label', 
				id: 'total',
				text: 'PAYMENTS AVERAGE: 0.00',
				margins: '5 0 0 50',
				align: 'center'
		},'-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				text:'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
		}]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("parametro=retentionrate&idstat="+Ext.getCmp('cbStatusret').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}

	function searchStatus(){
		store.setBaseParam('idstat',Ext.getCmp('cbStatusret').getValue());
		store.load({params:{start:0, limit:50}});
	}
	
	function doDel(){
		var com;
		var j=0;
		var eliminar="(";
		var arrayRecords = store.getRange(0,store.getCount());
		for(i=0;i<arrayRecords.length;i++){
			com="del"+arrayRecords[i].get('idcont');
			
			if(document.getElementById(com.toString()).checked==true){
				if(j>0) eliminar+=',';
				
				eliminar+=arrayRecords[i].get('idcont');
				j++;
			}
		}
		eliminar+=")";
		if(j==0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return;
		}
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/eliminaremail.php', 
			method: 'POST',
			params: {
				cont: eliminar 
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},
			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
					
				store.commitChanges();
				store.reload();
			}
		}); 

	}
	
	
	function searchActive(){
		if(Ext.getCmp("cbActive").getValue()==1){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'If you activate this workshop, this is the one that will appear on the webpage',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;			
		}		
		return;			
	}

	function obtenerTotal(){

		var totales = store.getRange(0,store.getCount());
		Ext.getCmp('total').setText('PAYMENTS AVERAGE: '+totales[0].data.avgcountactive);	

	}	
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function viewnotes(val, p, record){
		var comment = record.data.comment;
		return  '<img src="images/notes.png" border="0" ext:qtip="'+comment+'">';
	}
	function viewnstatus(val, p, record){
		
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	function rendercountactive(value, p, record){
        if(value=='' || value==null)return 0;
		return value;
    }	
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		title:'Retention Rate',
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [
			new Ext.grid.RowNumberer()
			,{				header: 'Name', 		width: 90, sortable: true, align: 'left', dataIndex: 'name'}//,editor: new Ext.form.TextField({allowBlank: false})
			,{				header: 'Last Name', 	width: 90, align: 'left', sortable: true, dataIndex: 'surname'}
			,{id:'userid',	header: 'User ID', 		width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{				header: 'Email', 		width: 200, align: 'left', sortable: true, dataIndex: 'email'}
			,{				header: 'Status', 		width: 80, align: 'left', sortable: true, dataIndex: 'status'}
			,{				header: 'Phone', 		width: 100, align: 'right', sortable: true, dataIndex: 'hometelephone'}
			,{				header: 'Sign Up', 		width: 100, sortable: true, align: 'left', dataIndex: 'affiliationdate'}
			,{				header: 'Payments', 	width: 80, sortable: true, align: 'center', dataIndex: 'countactive',renderer: rendercountactive}
			,{				header: 'Frec', hidden:true,width: 80, sortable: true, align: 'center', dataIndex: 'frec'}

		],
		clicksToEdit:2,
		height:470,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:50}});
	store.addListener('load', obtenerTotal);	
/////////////FIN Inicializar Grid////////////////////
 
});
