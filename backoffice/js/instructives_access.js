    function mostrarDetalleNolaboral(userid) 
    {
        Ext.getCmp('gridNolaboralDetail').getStore().setBaseParam('userid', userid);
        Ext.getCmp('gridNolaboralDetail').getStore().load({
            params:{
                userid: userid
                }
        });
    }

    /*Nueva ventana para Gestionar*/
    function viewHtmlToManage(userid) //Ver ventana para gestionar
    {
    	var usersProgrammers = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.Groups,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }

        });
    	
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_add_access',
                userid: userid,
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                var user_session = variable.user_session;
                if(appro == 1)
                {
                    var aprobar = true;
                }
             
                var btnEdit= [{
                        text: '<b>Save </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_data.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_add_access',
                                        userid: userid
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }]
             //       }
                   // else
                   /* {
                        var btnEdit = [{
                        text: '<b>It was approved</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }
                */
                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [
	                    {
				            xtype: 'itemselector',
				            name: 'groupid',
				            hiddenName  : 'groupid',
				            fieldLabel: 'Select groups',
				            imagePath: 'includes/ext/examples/ux/images/',
				            multiselects: [{
				                width: 200,
				                height: 350,
				                store: usersProgrammers,
				                displayField: 'text',
				                valueField: 'value'
				            },{
				                width: 200,
				                height: 350,
				                store: [['','']],
				                displayField: 'text',
				                valueField: 'value',
				                tbar:[{
				                    text: 'clear',
				                    handler:function(){
				                        form.getForm().findField().reset();
				                    }
				                }]
				            }]
				        }
                  ],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Assign Access',
                    width: 600,
                    height: 350,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

/*Termina nueva ventana para Gestionar*/
function viewHtmlToEdit(userid) //Ver ventana para Editar
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_add_access',
                userid: userid,
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                if(appro != "1")
                { 
                var btnEdit= [{
                        text: '<b>Update Instructive </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }]
                    }
                    else
                    {
                        var btnEdit = [{
                        text: '<b>It was approved</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'title',
                        id: 'title',
                        width:690,
                        value: variable.title,
                        allowBlank: false
                    },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    value:variable.instructive_type_id,

                  },
                  {
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false
                  },
                  {
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.description,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
                });


                var w = new Ext.Window({
                    title: 'Update Instructive',
                    width: 800,
                    height: 550,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

    function viewHtmltoApprove(instructive_id)
    {

        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                var user_session = variable.user_session;
                if(appro != "1")
                { 
	                	if(user_session==5 || user_session==20 || user_session==2342)
	                	{
	                		var btnEdit= [{
		                        text: '<b>I Approve </b>',
		                        cls: 'x-btn-text-icon',
		                        icon: 'images/check.png',
		                        formBind: true,
		                        handler: function(b){
		                            var form = b.findParentByType('form');
		                            //form.getForm().fileUpload = true;
		                           // if (form.getForm().isValid()) {
		                                form.getForm().submit({
		                                    url: 'php/grid_edit.php',
		                                    waitTitle   :'Please wait!',
		                                    waitMsg     :'Loading...',
		                                    params: {
		                                        tipo : 'instructive_approve',
		                                        instructive_id: instructive_id
		                                    },
		                                    timeout: 100000,
		                                    method :'POST',
		                                    success: function(form, action) {
		                                        w.close();
		                                        var store1 = Ext.getCmp("gridpanel").getStore();
		                                        store1.reload();
		                                        Ext.Msg.alert('Success', action.result.msg);

		                                    },
		                                    failure: function(form, action) {
		                                        Ext.Msg.alert('Error', action.result.msg);
		                                    }
		                                });
		                        	}
		                    	}]//Termina Btn Edit	
	                	}
	                	else
	                	{
	                		var btnEdit = [{
		                        text: '<b>Not Authorized  </b>',
		                        cls: 'x-btn-text-icon',
		                        icon: 'images/access_denied.png'
		                        }]
	                	}	
                    }
                    else
                    {
                    	var btnEdit = [{
                        text: '<b>It was approved </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }


                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'title',
                        id: 'title',
                        width:690,
                        value: variable.title,
                        allowBlank: false
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false
                    },{
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.description,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Read Instructive',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
    var store = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=usersinstructives',
        fields: [
            {name: 'userid', type: 'int'}
            ,'usuariofullname'
        ]
    });

     var storeNolaboralDetail = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=instructive_accesuser',
        fields: [
           {name: 'instructive_id', type: 'int'}
            ,{name: 'userid', type: 'int'}
            ,'group'
            ,'idgroup'

        ]
    });


///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo - Botones Add - Delete//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[/*{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            tooltip: 'Click to Add Instructions',
            handler: AddWindow // Abre Ventana Registrar Instructivo
        },*//*{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/delete.gif',
            id: 'del_butt',
            tooltip: 'Click to Delete Instructions',
            handler: doDel // Abre Ventana para Borrar Instructivo
        },{
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Type Instructive'
                    },{
                    xtype       :'combo',
                    id          :'typeins',
                    name        :'typeins',
                    hiddenName  :'typeins',
                    fieldLabel  :'type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['0', 'NONE'],['7', 'All Instructive'], ['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    value: '0',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    listeners: {'select': logstatistic}

                  },/*{ //Muestro Select para filtrar **wape
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Created By'
                    },{
                        xtype:'combo',
                        name:'asic',
                        id:'asic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['userid', 'name'],
                            data : Ext.combos_selec.dataUsersProgramm
                        }),
                        valueField: 'userid',
                        displayField:'name',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        },
                        listeners: {'select': logstatistic}

                    } */]
    });

     var pagingBar2 = new Ext.PagingToolbar({
            store: storeNolaboralDetail,
            displayInfo: true,
            displayMsg: '{0} - {1} of {2} Registros',
            emptyMsg: 'No hay Registros Disponibles',
            pageSize: 100,
            items:[
            /*{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button2',
          //  tooltip: 'Click to Add Instructions',
           // handler: AddWindow2
        }*/
        ]
    });


////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////

    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idtins;
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }

    function obtenerSeleccionados(){
            var selec = grid.selModel.getSelections();
            var i=0;
            var marcados='(';
            for(i=0; i<selec.length; i++){
                if(i>0) marcados+=',';
                marcados+=selec[i].json.idtins;
            }
            marcados+=')';
            if(i==0)marcados=false;
            return marcados;
        }


function logstatistic(){
//store.setBaseParam('iduser',Ext.getCmp("asic").getValue());
store.setBaseParam('idtype',Ext.getCmp("typeins").getValue());

        store.load({params:{start:0, limit:100}});
    }
    //Revisar bien, borrar
    function doDel(){
        Ext.MessageBox.confirm('Delete Instructives','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();
                    if(!obtenidos){
                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    //alert(obtenidos);//return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');

                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_del.php',
                        method: 'POST',
                        params: {
                            tipo : 'instructions',
                            idtmail: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }


    function AddWindow()
    {

    var usersProgrammers = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.Groups,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }

        });

        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                xtype:'textfield',
                fieldLabel: 'Title',
                name: 'title',
                id: 'title',
                width:690,
                allowBlank: false
            },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'instructives_type_id'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single']]
                                }),
                    displayField:'instructives_type_id',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,


                  },
{
            xtype: 'itemselector',
            name: 'users_single',
            hiddenName  : 'users_single',
            fieldLabel: 'Select users',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: usersProgrammers,
                displayField: 'text',
                valueField: 'value'
            },{
                width: 250,
                height: 300,
                store: [['','']],
                displayField: 'text',
                valueField: 'value',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField().reset();
                    }
                }]
            }]
        },
            {
                xtype: 'htmleditor',
                id: 'description',
                id: 'description',
                hideLabel: true,
                height: 350,
                width:750,
                allowBlank: false
            }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Create Instructive</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php',
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'instructive_add'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                store.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
        });


        var w = new Ext.Window({
            title: 'New Instructive',
            width: 800,
            height: 620,
            layout: 'fit',
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();
        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()

     function AddWindow2()
    {

var ds = new Ext.data.ArrayStore({
        fields: ['value', 'text'],
                data : Ext.combos_selec.dataUsersProgramm,
        sortInfo: {
            field: 'value',
            direction: 'ASC'
        }

    });
    var obtenidos=obtenerSeleccionados();
        if(!obtenidos){
            Ext.MessageBox.show({
                title: 'Warning',
                msg: 'You must select a row, by clicking on it, for the delete to work.',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            obtenerTotal();
            return;
        }
        //alert(obtenidos);//return;
        obtenidos=obtenidos.replace('(','');
        obtenidos=obtenidos.replace(')','');

        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                            xtype:'hidden',
                            id: 'idtinsa',
                            name: 'idtinsa',
                            value: obtenidos,
                            hiddenName: 'idtinsa',
                        },
{
            xtype: 'itemselector',
            name: 'asignadona',
            hiddenName  : 'asignadoa',
            fieldLabel: 'Assigned to',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: ds,
                displayField: 'text',
                valueField: 'value'
            },{
                width: 250,
                height: 300,
                store: [['','']],
                displayField: 'text',
                valueField: 'value',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField().reset();
                    }
                }]
            }]
        }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Add User</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php',
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'userinstructions'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                storeNolaboralDetail.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
        });


        var w = new Ext.Window({
            title: 'New User Instructive',
            width: 800,
            height: 400,
            layout: 'fit',
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();
        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()



//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
    /*Nuevo Render to Manage*/
    function renderViewtoManage(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/icon_permission.png" border="0" width="16" title="Manage Access" alt="Manage Access"></a>',record.data.userid);
    }

    /*Termina nuevo render to manage*/

    function renderViewtoEdit(val, p, record)
    {
    	if(record.data.istatus !='N')
    	{
			return '';
    	}
    	else
    	{
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToEdit({0})" ><img src="images/editar.png" border="0" title="Edit Instructive" alt="Edit Instructive"></a>',record.data.instructive_id);

    	}
    }

    function renderViewtoApprove(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="viewHtmltoApprove({0})" ><img src="images/check.png" border="0" title="Approve Instructive" alt="Approve Instructive"></a>',record.data.instructive_id);
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});

    var myView2 = new Ext.grid.GridView();
    myView2.getRowClass = function(record, index, rowParams, store) 
    {
        if(record.data['istatus'] == 'N')return 'rowRed'; //Valida el estado del registro
        if(record.data['instructive_type_id'] == '6')return 'rowSingle'; //Valida el estado del registro

    };
    var grid = new Ext.grid.EditorGridPanel({
        view: myView2,
        title:wherepage,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            ,{header: 'ID', width: 40, align: 'left', sortable: true, dataIndex: 'userid',renderer:function (val)
            {
            return '<a href="javascript:void(0)" class="itemusers" title="Click to see the information." onclick="mostrarDetalleNolaboral('+val+')"><img src="images/view.gif" width="16" border="0" title="View access: '+val+' " alt="View access"></a>';
            }}
           // ,{header: 'Approve', width: 40, sortable: true, align: 'center', dataIndex: 'instructive_id', renderer: renderViewtoApprove}
            ,{header: 'User', width: 160, sortable: true, align: 'left', dataIndex: 'usuariofullname'}
            ,{header: ' ', width: 40, sortable: true, align: 'center', dataIndex: 'userid', renderer: renderViewtoManage}
        ],
        clicksToEdit:2,
        height:Ext.getBody().getViewSize().height-400,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });


 	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var gridNolaboralDetail = new Ext.grid.EditorGridPanel({
    store: storeNolaboralDetail,
    id:'gridNolaboralDetail',
    iconCls: 'icon-grid',
    border:false,
    viewConfig: {
        forceFit:true
    },
    tbar: pagingBar2,
    columns: [
        new Ext.grid.RowNumberer(), //grid2

        {header: 'Group', width: 200, sortable: true, align: 'left', dataIndex: 'group'},
		{
        xtype: 'actioncolumn',
        width: 30,
        items: [
            {
                icon    : 'images/delete-icon.png',                // Use a URL in the icon config
                tooltip : 'Remove Access',
                scope   :   this,
                handler : function(grid, rowIndex, columnIndex, e) {
                    var rec = storeNolaboralDetail.getAt(rowIndex);
                   Ext.MessageBox.alert('Access removed','This access was removed');
                    var idgroup = rec.get('idgroup');
                    var userid = rec.get('userid');
                   /*alert('User delete');
                    var rec = grid.getStore( ).getAt(rowIndex);
                    console.debug(rec);
                    */

// Revisar este comportamiento -wape
                    Ext.Ajax.request({
                        waitMsg: 'Loading...',
                        url: 'php/grid_del.php',
                        scope:this,
                        method: 'POST',
                        params: {
                            idgroup  : idgroup,
                            userid : userid,
                            tipo  :'instructive_removeaccess'
                        },
                        success:function (){
                            grid.getStore( ).load();
                        }
                    });

                  //  console.debug(rec);
                }
            }
        ]
        }
    ],
    height:Ext.getBody().getViewSize().height-400,
    //autoHeight:true,
    loadMask:true
});

/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },{
            region:'center',
            autoHeight: true,
            items:[{
                    xtype   : 'panel',
                   // width   : 500,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        grid
                    ]
                },{
                    xtype   : 'panel',
                    //flex    : 1,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        gridNolaboralDetail
                    ]
                }]
        }]
    });


//////////////FIN VIEWPORT////////////////////////////////


//          ***     Inicializar Grid      ***              //
    store.load({params:{start:0, limit:100}});
//          ***     Terminar Inicializar Grid   ***       //

});
