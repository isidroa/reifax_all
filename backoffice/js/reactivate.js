// JavaScript Document
Ext.ns('reactivate');

Ext.override(Ext.form.ComboBox, {
      onTypeAhead : function(){
   }
});
reactivate=
{
	init: function ()
	{
		reactivate.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total',
			id:'readerreactivate'
			},
			reactivate.record 
			);
			
		reactivate.store = new Ext.data.Store({  
			id: 'reactivate',  
			proxy: reactivate.dataProxy,  
			remoteSort :true,
			baseParams: {
				accion	:'consultar',
				start	: 0,
				limit	: 50
			},  
			reader: reactivate.gridReader  
		});
		
		var panelpag = new Ext.form.FormPanel({
			layout: 'table',
			id:'reactivateFilter',
			padding: 3,
			defaults: {
				bodyStyle:'margin-left:5px'
			},
			layoutConfig:{
				columns	:	2
			},
			style	:{
				background:'none'
			},
			border: false,
			items: [{
				layout	: 'table',
				border: false,
				items	: [{
						xtype:'label',
						id: 'lblstatus',
						text: 'Estatus'
					},{
					xtype	: 'combo'
					,fieldLabel:'Status'
					,displayField:'name'
					,value :'in (1,3)'
					,name:'status'
					,valueField:'id'
					,store: new Ext.data.SimpleStore({
						 fields:['id', 'name']
						,data:[
							['','All'],
							['in (1,3)', 'Active'],
							['in (1)', 'Pending'],
							['in (3)', 'Process'],
							['in (4,5)', 'Inactive'],
							['in (5)', 'Reactivated'],
							['in (4)', 'Declined'],
							['in (7)', 'Coaching'],
							['in (8)', 'Testing']
						]
					})
					,width: 70
					,triggerAction:'all'
					,mode:'local'
				},{
					xtype	: 'textfield',
					name	: 'name',
					emptyText	:'Email, Name, Userid'
				}]
			},{
				layout	: 'table',
				border: false
			}]
		});
		reactivate.pager = new Ext.PagingToolbar({  
			store: reactivate.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[ 
				'|',{
					xtype	:'button',
					iconCls	:'x-icon-add',
					handler	: reactivate.new.init,
					text	: 'New reactivate'
				},((userid==5 || userid==2544)?
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: reactivate.delete,
					text	: 'Delete'
				}:''),
				'|',
				{
					xtype	:'button',
					iconCls	:'x-icon-save',
					handler	: reactivate.save,
					text	: 'Save Changes'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: reactivate.cancel,
					text	: 'Cancel Change'
				}
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-reload',
					handler	: reactivate.reset,
					text	: 'Reset Date'
				},
				{
					xtype	:'button',
					icon: 'images/excel.png',
					handler	: reactivate.export_excel
				},
				'|',panelpag,
				{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						reactivate.store.load({
							params:{
								status:Ext.getCmp('reactivateFilter').getForm().findField("status").getValue(),
								q	:Ext.getCmp('reactivateFilter').getForm().findField("name").getValue()
							}
						})
					}
				},{
					xtype	: 'button',
					text	: 'Remove',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						Ext.getCmp('reactivateFilter').getForm().reset();
						reactivate.store.load({
							params:{
								status:Ext.getCmp('reactivateFilter').getForm().findField("status").getValue(),
								q	:Ext.getCmp('reactivateFilter').getForm().findField("name").getValue()
							}
						})
					}
				},
				{
					xtype 	: 'textfield',
					width	: 45,
					id		: 'relojCaracas'
				},
				{
					xtype 	: 'textfield',
					width	: 45,
					id		: 'relojUsa'
				}
			], 
			listeners: {
				afterrender : function(){
					startReloj();
				}
			}
		});
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {
				forceSelection:true,
				minValue: '8:00 AM',
				maxValue: '6:00 PM',
				increment: 30});
				
		var opcionesCombo= [
        ['1', 'Pending'],
        ['3', 'Process'],
        ['5', 'Reactivated'],
        ['7', 'Coaching'],
        ['8', 'Testing'],
        ['4', 'Declined'] ];
		
		var opcionesStatus= {
			1	:	'Pending',
			3	:	'Process',
			5	:	'reactivated',
			7	:	'Coaching',
			8	:	'Testing',
			4	:	'Declined'
		}
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		
		var storeUserAdmin= new Ext.data.SimpleStore({
					fields: ['userid', 'name'],
					data : Ext.combos_selec.dataUsers2
			});
		var comboUser = new Ext.form.ComboBox({
			store: storeUserAdmin,
			hiddenName	: 'userPro',
			valueField: 'userid',
			displayField:'name',
			 style: {
                fontSize: '16px'
            },
			typeAhead: true,
			triggerAction: 'all',
			mode: 'local',
			selectOnFocus:true
		})
		
		var textfield=new Ext.form.TextField({});
		
		
		reactivate.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
				reactivate.sm,
			{  
				header: 'Id reactivate',  
				dataIndex: 'id', 
				sortable: true,  
				width: 30  
			},{  
				header: 'P',  
				dataIndex: 'p', 
				sortable: true,
				width: 25,
				editor: textField
			},{  
				header: 'Userid',  
				dataIndex: 'userid', 
				sortable: true,
				width: 55,
				renderer :function (value, p, record){
					return String.format(
					'<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
				}	
			},{  
				header: 'User',  
				sortable: true, 
				dataIndex: 'usr_name'
			},{  
				header: 'Register',  
				dataIndex: 'date_register_u', 
				sortable: true,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				width: 70
			},{  
				header: 'Created',  
				dataIndex: 'date_register', 
				sortable: true,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				width: 70
			},{  
				header: 'Logs',  
				dataIndex: 'logs', 
				sortable: false,
				width: 40
			},{  
				header: 'Frecuency',  
				dataIndex: 'frecuency', 
				sortable: true,
				width: 60
			},{  
				header: 'Days',
				sortable: true, 
				dataIndex: 'dias', 
				renderer: function(value, metaData, record, rowIndex, colIndex, store) { 
						var now		= (record.get('status')=='Pending')?new Date():record.get('date_lastUpdate');
						var diferece= now - record.get('date_register');
						var dias 	= Math.floor(diferece / (1000 * 60 * 60 * 24));
						dias=dias<0?0:dias;
						if(record.get('dias')<11)
	      					metaData.attr ='style="background-color:#4BB148; color: #FFF; font-weight:bold;"'; 
						else if(record.get('dias')<21)
	      					metaData.attr ='style="background-color:#FCD209; font-weight:bold;"'; 
						else 
	      					metaData.attr ='style="background-color:#E04828; color: #FFF; font-weight:bold;"'; 
					return dias; 
   				} ,
				width: 35
			},{  
				header: 'Home Phone',
				sortable: true, 
				dataIndex: 'home',
				editor:textfield,
			},{  
				header: 'Mobile Phone',
				sortable: true, 
				dataIndex: 'movile',
				editor:textfield
			},{  
				header: 'Product',
				sortable: true, 
				dataIndex: 'producto'
			}/*,{  
				header: 'User Status',
				sortable: true, 
				width: 70,
				dataIndex: 'statusProduct'
			},{  
				header: 'Time',
				sortable: true, 
				dataIndex: 'periodo',
				width: 55
			}*/,{  
				header: 'reactivate Status',
				sortable: true, 
				width: 70,
				dataIndex: 'status',
				renderer:function (val){
					if(opcionesStatus[val]){
						return opcionesStatus[val];
					}
					else{
						return val;
					}
				},
				editor	: combo
			}/*,{ 
				header: 'Available time',  
				dataIndex: 'time_user', 
				sortable: false,
				renderer: reactivate.tooltip  ,
				width: 130
			}*/,{   
				header: 'Scheduled',
				sortable: true, 
				dataIndex: 'date_reactivate',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{   
				header: 'Hour',
				sortable: true, 
				dataIndex: 'hourreactivate',
				editor	:timeField,
				width: 65
			},{   
				header: 'Calling Date',
				sortable: true, 
				dataIndex: 'callingD',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{   
				header: 'Conversation Date',
				sortable: true, 
				dataIndex: 'conversationD',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{   
				header: '# of Calls',
				sortable: true, 
				dataIndex: 'numCalls',
				editor	:textfield,
				width: 20
			},{   
				header: 'Execution Date',
				sortable: true, 
				dataIndex: 'date_execute',
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor	:dateField,
				width: 70
			},{  
				header: 'Mandated',
				sortable: false, 
				dataIndex: 'mandated',
				editor	: comboUser,
				width: 120,
				renderer:function(id){
					id=parseInt(id);
					if(id){
						var index = storeUserAdmin.find('userid',id);  
						if(index>-1){  
								var record = storeUserAdmin.getAt(index);
								return record.get('name');  
							}  
						return value;
						}
					else{
						return '';
					}
				}
			},{  
				header: 'Note',
				sortable: false, 
				dataIndex: 'note',
				renderer: function (value, metaData, record, rowIndex, colIndex, store){
					var id=record.get('id');
					var userid=record.get('userid');
					var usr_name=record.get('usr_name');
					return '<a href="javascript:void(0)"  onclick="reactivate.coachinOpenNote('+id+','+userid+',\''+usr_name+'\')" style="display: block; float: left; height: 16px; margin-right: 5px; width: 16px;" class="x-icon-add"></a>'+value;
				}
			}
			]  
		);
		reactivate.pager.on('beforechange',function(bar,params){
				params.status=Ext.getCmp('reactivateFilter').getForm().findField("status").getValue();
		});
		reactivate.store.load({
			params:{
				status:Ext.getCmp('reactivateFilter').getForm().findField("status").getValue()
			}
		});	
		reactivate.grid = new Ext.grid.EditorGridPanel({
			height: 500,
            store: reactivate.store,
			tbar: reactivate.pager,   
			cm: reactivate.columnMode,
            border: false,
				viewConfig: {
					forceFit:true
				},
            stripeRows: true ,
			listeners:{
			},
			sm:reactivate.sm,
			loadMask: true,
			border :false
        }); 
		
		reactivate.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: reactivate.grid,
					autoHeight: true
			}]
		});
		
	},
	sm:new Ext.grid.CheckboxSelectionModel()
	,
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},
			{name: 'p', type: 'string'},    
			{name: 'userid', type: 'int'},    
			{name: 'mandated', type: 'string'},    
			{name: 'status', type: 'string'},
			{name: 'date_register_u', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_register', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_reactivate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_lastUpdate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_execute', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'callingD', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'conversationD', type: 'date', dateFormat: 'Y-m-d'},
			{name: 'hourreactivate', type: 'string'},
			{name: 'usr_name', type: 'string'},
			{name: 'statusProduct', type: 'string'},
			{name: 'time_user', type: 'string'},
			{name: 'email' , type: 'string'},
			{name: 'producto' , type: 'string'},
			{name: 'home' , type: 'string'},
			{name: 'numCalls' , type: 'int'},
			{name: 'logs' , type: 'int'},
			{name: 'movile' , type: 'string'},
			{name: 'note' , type: 'string'},
			{name: 'frecuency' , type: 'string'},
			{name: 'dias' , type: 'int'},
			{name: 'periodo' , type: 'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesreactivate.php',   // Servicio web  
			method: 'POST'                          // M�todo de env�o  
	}),
	save	:function ()
	{
		var grid=reactivate.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcionesreactivate.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
					grid.getStore().reload();
				}  
			});  
		}  
			
	},
	export_excel:function(){
		var grid=reactivate.grid;
		grid.el.mask('Exporting...', 'x-mask-loading');
		var ids='';
		var cols=reactivate.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].get){
				ids+=((ids=='')?'':',')+cols[i].get('id');
			}
		}
		Ext.Ajax.request({
			method :'POST',
		   	url: 'Excel/xlsreactivate.php',
		   	success: function ( result, request ){
			 	grid.el.unmask();  
				window.open('Excel/d.php?nombre='+result.responseText,'','width=50,height=50');
				return(true); 
			},
		   	failure:  function (){
			   grid.el.unmask();  
			},
		  	 params: { 
			 	parametro	: 'Export_grid',
			 	ids			: 	ids,
				status		:Ext.getCmp('reactivateFilter').getForm().findField("status").getValue()//,
				/*typeDate:Ext.getCmp('reactivateFilter').getForm().findField("type_date").getValue(),
				startDate: Ext.util.Format.date(
					Ext.getCmp('reactivateFilter').getForm().findField("startdate").getValue()
					,'Y-m-d'
				),
				dueDate: Ext.util.Format.date(
					Ext.getCmp('reactivateFilter').getForm().findField("duedate").getValue()
					,'Y-m-d'
				)*/
			}
		});
		

	},
	reset: function (){
		var cols=reactivate.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].set){
				var date=cols[i].set('date_reactivate','');
				var date=cols[i].set('hourreactivate','');
			}
		}
	},
	coachinOpenNote: function (id,userid,name){
		
		var grid = new Ext.grid.GridPanel({
			baseCls			: 'cellComplete',
			store: new Ext.data.Store({  
				id: 'reactivate', 
				autoLoad :true,
				proxy: reactivate.dataProxy,  
				baseParams: {
					accion	:'listarNotes',
					id	: id
				},  
				reader:new Ext.data.JsonReader({  
				root: 'coacing', 
				totalProperty: 'total',
				},
					 new Ext.data.Record.create([
						{name: 'id', type: 'id'},
						{name: 'note', type: 'string' },
						{name: 'user', type: 'string' },
						{name: 'date_note', type: 'date', dateFormat: 'Y-m-d'}
					])   
				)
			}),
			colModel: new Ext.grid.ColumnModel({
				columns: [{  
						header	: 'Note',  
						dataIndex	: 'note', 
						//height		:80,
						sortable	: false,
						style		: 'white-space:normal !important;',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							metaData.style = "line-height:18px;";
							var text = "<p style='cursor:pointer;'>"+value+"</p>";
							return text;
						},
						width: 357,
							
					},{  
						header	: 'Date',  
						sortable: false, 
						width	: 100,
						renderer: Ext.util.Format.dateRenderer('Y/m/d'),
						dataIndex	: 'date_note'
					},{  
						header	: 'User',  
						sortable: false, 
						width	: 100,
						dataIndex	: 'user'
					}
				]
			}),
			height	: 200,
			width	: 577,
			frame	: true
		});
		var form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [
						{
							xtype	:'hidden',
							name	:'id',
							value	: id
						},{
							xtype	:'hidden',
							name	:'accion',
							value	:'recordNote'
						},{
							xtype	: 'textarea',
							emptyText: 'New Note',
							name	: 'note',
							allowBlank :false,
							hideLabel:true,
							height: 80,
							width: 540
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Close',
							handler	: function ()
							{
								btn=this;
								var form=this.findParentByType('form');
								var win=this.findParentByType('window');
								if(form.getForm().isValid())
								{
									form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new nota...',
										url: 'php/funcionesreactivate.php',
										scope : this,
										params :{
											accion : 'recordNote'
										},
										success	: function (form,action)
										{
											win.close();
											win.destroy();
										}
									})
								}
								else{
									win.close();
									win.destroy();
								}
							}
						}]
				})
		var win=new Ext.Window({
			modal	:true,
			title: 'Notes of '+id, 
			closable: false,
			resizable: false,
			maximizable: false,
			plain: true,
			border: false,
			width: 590,
			height: 430,
			items	:[
				{
					xtype	:'panel',
					bodyStyle	:'font-size:14px;',
					pandding	:13,
					html	:'<strong>User:</strong> '+name+' (<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+userid+')">'+userid+'</a>)'
				},
				grid,
				form
			]
		});
		win.show();
	},
	cancel: function ()
	{
		reactivate.grid.getStore().rejectChanges();
	},
	delete:function (){
		data=reactivate.grid.getSelectionModel().getSelections();
		if(data.length){
			var ids='';
			for(i=0;i<data.length;i++){
				ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
			}
		}
		Ext.Ajax.request({
			method :'POST',
		   	url: 'php/funcionesreactivate.php',
		   	success: function (){
			   reactivate.grid.getStore().reload();
			},
		   	failure:  function (){
			   
			},
		  	 params: { 
			 	accion: 'delete',
				ids	: ids
			}
		});
	}
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	new:{
		init:function (){
			if(reactivate.new.win)
			{
				reactivate.new.form.getForm().reset();
				reactivate.new.win.show();
			}
			else{
				reactivate.new.storeProducst= new Ext.data.Store({
								autoLoad:true,
								url: 'php/funcionesreactivate.php', 
								reader: new Ext.data.JsonReader({  
										root: 'root', 
										totalProperty: 'total'
									},
									 new Ext.data.Record.create([ 
										{name: 'idproducto', type: 'int'},
										{name: 'name', type: 'string'}
									]) 
								)
							});
				reactivate.new.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [{
							xtype: 'combo',
							store: new Ext.data.Store({
								autoLoad:true,
								url: 'php/funcionesreactivate.php', 
								reader: new Ext.data.JsonReader({  
										root: 'root', 
										totalProperty: 'total'
									},
									 new Ext.data.Record.create([ 
										{name: 'userid', type: 'int'},
										{name: 'name', type: 'string'}
									]) 
								)
							}),
							hiddenName: 'client', 
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Client',
							allowBlank   : false,
							forceSelection: false,
							editable   : true,
							minChars   : 2,        
							queryDelay   : 1,
							autoSelect   : false,
							typeAhead    : true,
							typeAheadDelay: 250,
							listeners:{
								change:function ( that, newValue, oldValue ){
									reactivate.new.storeProducst.load({
										params:{
											userid:newValue,
											accion:'getProducs'
										}	
									});
								}
							},
							style: {
								fontSize: '16px'
							}
					},{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['val', 'text'],
								data : [['6','1 Hour'],['8','2 Hour'],['9','3 Hour'],['7','4 Hour']]
							}),
							hiddenName: 'time', 
							valueField: 'val',
							displayField:'text',
							fieldLabel:	'Time',
							allowBlank:false,
							value:6,
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},{
							xtype: 'combo',
							store: reactivate.new.storeProducst,
							hiddenName: 'product', 
							valueField: 'idproducto',
							displayField:'name',
							fieldLabel:	'Product',
							allowBlank:false,
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},new Ext.form.DateField({
						fieldLabel:	'reactivate Date',
						format: 'Y-m-d',
						name	:'datereactivate',
						 style: {
							fontSize: '16px'
						},})
					,new Ext.form.TimeField({
						fieldLabel:	'reactivate Hour',
						forceSelection:true,
						name	:'hour',
						minValue: '9:00 AM',
						maxValue: '6:00 PM',
						increment: 30,
						 style: {
							fontSize: '16px'
						},})
					,{
						xtype:'combo',
						store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsers2
						}),
						fieldLabel:	'Mandated',
						hiddenName	: 'userPro',
						valueField: 'userid',
						displayField:'name',
						 style: {
							fontSize: '16px'
						},
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(reactivate.new.form.getForm().isValid())
								{
									reactivate.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new reactivate...',
										url: 'php/funcionesreactivate.php',
										scope : this,
										params :{
											accion : 'recordreactivate'
										},
										success	: function (form,action)
										{
											reactivate.new.form.getForm().reset();
											reactivate.new.win.hide();
											reactivate.store.load({
																params:{
																	status:Ext.getCmp('reactivateFilter').getForm().findField("status").getValue()
																}
															});	
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								reactivate.new.form.getForm().reset();
							}
						}]
				})
				
				
				reactivate.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New reactivate', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 360,
					height: 265,
					items	:[
						reactivate.new.form
					]
				});
				reactivate.new.win.show();
			}
		}
	}
}


function displayDateTime (time,wrapper){
	permanencia=parseInt(permanencia)+1000;
	time=(parseInt(time)*1000)+permanencia;
    var now = new Date(time); 
    var date = now.getDate();
    var year = now.getFullYear();
    var month = now.getMonth();
    var day = now.getDay();
     
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
 
    hour = ( hour < 10 )? "0"+hour : hour;
    minute = ( minute < 10 )? "0"+minute : minute;
    second = ( second < 10 )? "0"+second : second;
 
 
    var datetime = hour +':'+ minute;
	document.getElementById(wrapper).value=datetime;
}

Ext.onReady(reactivate.init);
Ext.QuickTips.init()