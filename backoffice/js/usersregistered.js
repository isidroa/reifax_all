function mostrarDetalleEmail(idemail)
{

	win=new Ext.Window({
						 title: 'Email Detail',
						 width: 585,
						 height: 333,
						 bodyStyle:'padding: 11px',
						 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 autoLoad: 'emaildetail.php?cont='+idemail
				});
	win.show();			
}
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=usersregistered',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=usersregistered', timeout: 3600000 }),
		fields: [
			{name: 'id', type: 'int'}
			,'name'
			,'surname'
			,'email' 
			,'pass'
			,'hometelephone'
			,'mobiletelephone'
			,'procode'
			,'payamount'			
			,'idproduct'
			,'idfrecuency'
			,'idstate'
			,'idcounty'
			,'questionuser'
			,'pseudonimo'
			,'answeruser'
			,'idusertype'
			,'licensen'
			,'officenum'
			,'address'
			,'city'
			,'state'
			,'zipcode'
			,'billingaddress'
			,'billingcity'
			,'billingstate'
			,'billingzip'
			,'cardholdername'
			,'cardtype'
			,'cardnumber'
			,'expirationmonth'
			,'expirationyear'
			,'csvnumber'
			,'insertdate'
			,'source'
			,'try'
			,'fieldsfailures'
			,'color'
			,'userid'
			,'check'
			,'updaterow'
			,'updateuser'			
			,'updatenotes'		
			,'email2'
			,'pass2'			
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
////////////////Toolbar//////////////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Displaying {0} - {1} of {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			id: 'del_butt',
            tooltip: 'View detail',
			text: 'View Detail',			
			iconCls:'icon',
			icon: 'images/view.gif',
			handler: detailRegistered
		}]
    });
////////////////FIN barra de pagineo//////////////////////
	//////////////////Manejo de Eventos//////////////////////
	function doDel(){
		var com;
		var j=0;
		var eliminar="(";
		var arrayRecords = store.getRange(0,store.getCount());
		for(i=0;i<arrayRecords.length;i++){
			com="del"+arrayRecords[i].get('idcont');
			
			if(document.getElementById(com.toString()).checked==true){
				if(j>0) eliminar+=',';
				
				eliminar+=arrayRecords[i].get('idcont');
				j++;
			}
		}
		eliminar+=")";
		if(j==0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
			return;
		}
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/eliminaremail.php', 
			method: 'POST',
			params: {
				cont: eliminar 
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.rejectChanges();
			},
			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
					
				store.commitChanges();
				store.reload();
			}
		}); 

	}

//////////////////FIN Manejo de Eventos//////////////////////
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		var ID =	oGrid_Event.record.data.id;
		campo=oGrid_Event.field;
		info=fieldValue;
		type='text';
		if(campo!='updatenotes')return;
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/grid_edit.php', 
			method: 'POST',
			params: {
				tipo: 'notesnewsusers'
				,value: info 
				,keyID: ID 			
			},			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				store.reload();
			},			
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);				
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);				
				store.reload();
				//Ext.MessageBox.alert('Success','Operation successfully');				
			}
		}); 
		
	}; 
	
///////////////////renders/////////////////////////
	marcaEmails =function (idcont){
			var marca='N';
			if(document.getElementById("cb"+idcont).checked==true)marca='Y';
			Ext.Ajax.request( 
				{   
					waitMsg: 'Saving changes...',
					url: 'php/grid_edit.php', 
					method: 'POST',
					params: {
						tipo: "checkuserregistered", 
						key: 'idcont',
						keyID: idcont,
						field: 'check',
						value: marca
					},
					
					failure:function(response,options){
						store.reload();
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						//if(rest.succes==false)Ext.MessageBox.alert('Warning',rest.msg);
						store.reload();
					}
				 }
			);  
						
		}
	function check(val, p, record){
		var marcado='';
		if(val=='Y'){
			marcado='checked';
		}
		var idc=record.data.id;
		var retorno= String.format('<input type="checkbox" name="cb'+idc+'"  id="cb'+idc+'" onClick="marcaEmails('+idc+')" '+marcado+'>',idc);		
		return retorno;
	}
	
	function deleteImg(val, p, record){
		var retorno= String.format('<a href="javascript:void(0)" onClick="borraEmail({0})" >',val);
		retorno+= String.format('<img src="images/delete.gif" border="0" ext:qtip="Eliminar"></a>',val);
		return retorno;
	}
	function checkEmailDelete(val,p,record){
		return String.format('<input type="checkbox" name="del'+val+'"  id="del'+val+'">',val);
	}
    function change(val, p, record){
		//return '<span style="color:'+record.data.color+'; font-weight:bold;">' + val + '</span>';
		if(val=='Success')
			return '<span style="color:green; font-weight:bold;">' + val + '</span>';
		else if(val=='FailureNotBillPay')
			return '<span style="color:orange;  font-weight:bold;">' + val + '</span>';
		else 
			return '<span style="color:red; ">' + val + '</span>';
		
    }
	function renderTopic(value, p, record){
		if(value!='' && value!='null' && value!=null)
			return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
		return ;
    }		
	
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			new Ext.grid.RowNumberer()
			,{header: '', width: 25, align: 'center',dataIndex: 'check', renderer: check}
			,{header: 'Date', width: 115, sortable: true, align: 'left', dataIndex: 'insertdate',editor:new Ext.form.TextField()}
			,{header: 'Try', width:150, align: 'left', sortable: true, dataIndex: 'try',renderer: change}
			,{header: 'Source', width:100, align: 'left', sortable: true, dataIndex: 'source'}
			,{header: 'Userid', width: 45, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 80, align: 'left', sortable: true, dataIndex: 'name',editor:new Ext.form.TextField(),id: 'id'}
			,{header: 'Last Name', width: 80, align: 'left', sortable: true, dataIndex: 'surname',editor:new Ext.form.TextField()}
			,{header: 'Email', width: 150, align: 'left', sortable: true, dataIndex: 'email',editor:new Ext.form.TextField()}
			//,{header: 'Pass', width: 70, align: 'left', sortable: true, dataIndex: 'pass'}
			,{header: 'Phone', width: 80, sortable: true, align: 'left', dataIndex: 'hometelephone',editor:new Ext.form.TextField()}
			//,{header: 'Phone 2', width: 90, sortable: true, align: 'left', dataIndex: 'mobiletelephone',editor:new Ext.form.TextField()}
			,{header: 'Procode', width: 50, align: 'center', sortable: true, dataIndex: 'procode'}
			,{header: 'Payamount', width: 75, align: 'right', sortable: true, dataIndex: 'payamount', renderer: 'usMoney',editor:new Ext.form.TextField()}
			//,{header: 'Product', width: 120, align: 'left', sortable: true, dataIndex: 'idproduct'}
			//,{header: 'Frecuency', width: 80, align: 'left', sortable: true, dataIndex: 'idfrecuency'}
			//,{header: 'State', width: 80, align: 'left', sortable: true, dataIndex: 'idstate'}
			//,{header: 'County', width: 80, align: 'left', sortable: true, dataIndex: 'idcounty'}
			,{header: 'Update', width: 115, sortable: true, align: 'left', dataIndex: 'updaterow',editor:new Ext.form.TextField()}
			,{header: 'Update User', width: 120, align: 'left', sortable: true, dataIndex: 'updateuser'}
			,{header: 'Update Notes', width: 200, align: 'left', sortable: false, dataIndex: 'updatenotes',editor:new Ext.form.TextArea()}
			//,{header: 'Question', width: 70, align: 'left', sortable: true, dataIndex: 'questionuser'}
			//,{header: 'Answer', width: 60, align: 'left', sortable: true, dataIndex: 'answeruser'}
			//,{header: 'Nickname', width: 60, align: 'left', sortable: true, dataIndex: 'pseudonimo'}
			//,{header: 'Usertype', width: 70, align: 'left', sortable: true, dataIndex: 'idusertype'}
			//,{header: 'Licensen', width: 60, align: 'left', sortable: true, dataIndex: 'licensen'}
			//,{header: 'Officenum', width: 60, align: 'left', sortable: true, dataIndex: 'officenum'}
			//,{header: 'Address', width: 70, align: 'left', sortable: true, dataIndex: 'address'}
			//,{header: 'City', width: 70, align: 'left', sortable: true, dataIndex: 'city'}
			//,{header: 'State', width: 70, align: 'left', sortable: true, dataIndex: 'state'}
			//,{header: 'Zipcode', width: 50, align: 'left', sortable: true, dataIndex: 'zipcode'}
			//,{header: 'Holdername', width: 70, align: 'left', sortable: true, dataIndex: 'cardholdername'}
			//,{header: 'Cardtype', width: 70, align: 'left', sortable: true, dataIndex: 'cardtype'}
			//,{header: 'Number', width: 60, align: 'left', sortable: true, dataIndex: 'cardnumber'}
			//,{header: 'Month', width: 40, align: 'left', sortable: true, dataIndex: 'expirationmonth'}
			//,{header: 'Year', width: 40, align: 'left', sortable: true, dataIndex: 'expirationyear'}
			//,{header: 'CVS', width: 40, align: 'left', sortable: true, dataIndex: 'csvnumber'}
			//,{header: 'BillAddress', width: 70, align: 'left', sortable: true, dataIndex: 'billingaddress'}
			//,{header: 'BillCity', width: 70, align: 'left', sortable: true, dataIndex: 'billingcity'}
			//,{header: 'BillState', width: 70, align: 'left', sortable: true, dataIndex: 'billingstate'}
			//,{header: 'BillZip', width: 70, align: 'left', sortable: true, dataIndex: 'billingzip'}
			//,{header: 'Delete', width: 50, sortable: true, align: 'center', dataIndex: 'idcont', renderer: checkEmailDelete}
		],
		clicksToEdit:2,
		height:470,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		width: screen.width,//'99.8%',
		frame:true,
		title:'Users Registrered',
		loadMask:true,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////
	grid.addListener('afteredit', doEdit);

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
	function itemGridSelected()
	{
		var selec = grid.selModel.getSelections();
		var i=0;
		var selected='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) selected+=',';
			selected+=selec[i].json.id;
		}
		selected+=')';
		if(i==0)selected=false;		
		return selected;
	}	

	function detailRegistered()
	{

		var selected=itemGridSelected();
		var j=0;
		if(selected.length>2)j=1;
		if(j==0)
		{	Ext.MessageBox.alert('Warning','You must select a row.');
			return;
		}
		selected=selected.replace('(','');
		selected=selected.replace(')','');
		if(selected.split(',').length>1){
			Ext.MessageBox.alert('Warning','You must select a row.');
			return;
		}

		var selec = grid.selModel.getSelections();
		var i=0;
		var workshop='',numberclass=0;		
		for(i=0; i<selec.length; i++){
			if(selec[i].json.id==selected)
				break;	
		}
	
				
//************* Personal Information ********************************************************************************************
		var formul = new Ext.FormPanel({			
			autoScroll: true,
			bodyStyle:'padding: 2px',  
			items: [{
				xtype:'fieldset',
				title:'Personal Information',
				autoHeight:true,
				items: [{
					layout:'column',
					border:false,
					items:[{
						layout: 'form',
						columnWidth:.5,
						border:false,
						items:[{
							xtype:'textfield',
							fieldLabel:'Try',
							name:'UD_try',
							id:'UD_try',
							value:selec[i].json.try,
							width:250,
							readOnly:true 
						},{
							xtype:'textfield',
							fieldLabel:'Name',
							name:'UD_name',
							id:'UD_name',
							value:selec[i].json.name,
							width:250,
							readOnly:true 
						},{
							xtype:'textfield',
							fieldLabel:'Email',
							name:'UD_email',
							id:'UD_email',
							value:selec[i].json.email,
							width:250,
							readOnly:true 				
						},{
							xtype:'textfield',
							fieldLabel:'Passwd',
							name:'UD_passwd',
							id:'UD_passwd',
							value:selec[i].json.pass,
							width:250,
							readOnly:true
						},{
							
							xtype:'textfield',
							fieldLabel:'Pseudonimo',
							name:'UD_pseudonimo',
							id:'UD_pseudonimo',
							value:selec[i].json.pseudonimo,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Hometelephone',
							name:'UD_hometelephone',
							id:'UD_hometelephone',
							value:selec[i].json.hometelephone,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'State',
							name:'UD_state',
							id:'UD_state',
							value:selec[i].json.state,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Zipcode',
							name:'UD_zipcode',
							id:'UD_zipcode',
							value:selec[i].json.zipcode,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'LicenseN',
							name:'UD_brokern',
							id:'UD_brokern',
							value:selec[i].json.licensen,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Officenum',
							name:'UD_officenum',
							id:'UD_officenum',
							value:selec[i].json.officenum,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Question',
							name:'UD_questionuser',
							id:'UD_questionuser',
							value:selec[i].json.questionuser,
							width:250,
							readOnly:true			
						}]
					},{
						layout: 'form',
						border:false,columnWidth:.5,
						items:[{
							xtype:'textfield',
							fieldLabel:'Source',
							name:'UD_source',
							id:'UD_source',
							value:selec[i].json.source,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Surname',
							name:'UD_surname',
							id:'UD_surname',
							value:selec[i].json.surname,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Email2',
							name:'UD_email2',
							id:'UD_email2',
							value:selec[i].json.email2,
							width:250,
							readOnly:true 				
						},{
							xtype:'textfield',
							fieldLabel:'Passwd2',
							name:'UD_passwd2',
							id:'UD_passwd2',
							value:selec[i].json.pass2,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Affiliation Date',
							name:'UD_affiliationdate',
							id:'UD_affiliationdate',
							value:selec[i].json.insertdate,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Mobiletelephone',
							name:'UD_mobiletelephone',
							id:'UD_mobiletelephone',
							value:selec[i].json.mobiletelephone,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'City',
							name:'UD_city',
							id:'UD_city',
							value:selec[i].json.city,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Address',
							name:'UD_address',
							id:'UD_address',
							value:selec[i].json.address,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'User Type',
							name:'UD_idusertype',
							id:'UD_idusertype',
							value:selec[i].json.idusertype,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Answer User',
							name:'answeruser',
							id:'answeruser',
							value:selec[i].json.answeruser,
							width:250,
							readOnly:true							  
						}]
					}]
				}]
			},{
				xtype:'fieldset',
				title:'Billing',
				autoHeight:true,
				items: [{
					layout:'column',
					border:false,
					items:[{
						layout: 'form',
						columnWidth:.5,
						border:false,
						items:[{
							xtype:'textfield',
							fieldLabel:'Product',
							name:'UD_idproduct',
							id:'UD_idproduct',
							value:selec[i].json.idproduct,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'State',
							name:'UD_idstate',
							id:'UD_idstate',
							value:selec[i].json.idstate,
							width:250,
							readOnly:true
						},{
							
							xtype:'textfield',
							fieldLabel:'Pay Amount',
							name:'UD_payamount',
							id:'UD_payamount',
							value:selec[i].json.payamount,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Card Type',
							name:'UD_cardname',
							id:'UD_cardname',
							value:selec[i].json.cardtype,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Holder Name',
							name:'UD_cardholdername',
							id:'UD_cardholdername',
							value:selec[i].json.cardholdername,
							width:250,
							readOnly:true
						},{
							
							xtype:'textfield',
							fieldLabel:'Expiration Year',
							name:'UD_expirationyear',
							id:'UD_expirationyear',
							value:selec[i].json.expirationyear,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Billing State',
							name:'UD_billingstate',
							id:'UD_billingstate',
							value:selec[i].json.billingstate,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Billing Address',
							name:'UD_billingaddress',
							id:'UD_billingaddress',
							value:selec[i].json.billingaddress,
							width:250,
							readOnly:true
						}]
					},{
						layout: 'form',
						border:false,columnWidth:.5,
						items:[{
							xtype:'textfield',
							fieldLabel:'Frecuency',
							name:'UD_idfrecuency',
							id:'UD_idfrecuency',
							value:selec[i].json.idfrecuency,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'County',
							name:'UD_idcounty',
							id:'UD_idcounty',
							value:selec[i].json.idcounty,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Procode',
							name:'UD_procode',
							id:'UD_procode',
							value:selec[i].json.procode,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Card Number',
							name:'UD_cardnumber',
							id:'UD_cardnumber',
							value:selec[i].json.cardnumber,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'CVS Number',
							name:'UD_csvnumber',
							id:'UD_csvnumber',
							value:selec[i].json.csvnumber,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Expiration Month',
							name:'UD_expirationmonth',
							id:'UD_expirationmonth',
							value:selec[i].json.expirationmonth,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Billing City',
							name:'UD_billingcity',
							id:'UD_billingcity',
							value:selec[i].json.billingcity,
							width:250,
							readOnly:true
						},{
							xtype:'textfield',
							fieldLabel:'Billing Zip',
							name:'UD_billingzip',
							id:'UD_billingzip',
							value:selec[i].json.billingzip,
							width:250,
							readOnly:true
						}]
					}]
				}]
			},{
				bodyStyle:'padding: 4px',  
				xtype:'textarea',
				id: 'Fieldsfailure',
				name: 'Fieldsfailure',
				fieldLabel: 'Fields',
				value:selec[i].json.fieldsfailures,
				width: 700,
				height: 40,
				readOnly:true					
			}],
			buttons: [{
				handler:function(){ 
				   win.close();
				},
				scope:this,
				text:'Close'				
			}]
		});//Fin panel Personal Information
	
		var win = new Ext.Window({  
			title:'<H1 align="center">('+selec[i].json.try+') '+selec[i].json.name+' '+selec[i].json.surname+'<H1>',  
			width:910,  			
			layout: "fit",
			height:700,  
			border: false,
			autoScroll: true,
			modal: true,
			plain: true,
			//bodyStyle: 'background-color:#fff;',  
			items: [formul]
		});  
		win.show(); 								   
	
	
	
	}//function detailRegistered()

});
