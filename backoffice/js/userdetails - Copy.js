var ArrCountry=eval([{"value":"AF","text":"Afganistan"},{"value":"AL","text":"Albania"},{"value":"DZ","text":"Algeria"},{"value":"AS","text":"American Samoa"},{"value":"AD","text":"Andorra"},{"value":"AO","text":"Angola"},{"value":"AI","text":"Anguilla"},{"value":"AQ","text":"Antarctica"},{"value":"AG","text":"Antigua and Barbuda"},{"value":"AR","text":"Argentina"},{"value":"AM","text":"Armenia"},{"value":"AW","text":"Aruba"},{"value":"AC","text":"AscensionIsland"},{"value":"AU","text":"Australia"},{"value":"AT","text":"Austria"},{"value":"AZ","text":"Azerbaijan"},{"value":"BS","text":"Bahamas"},{"value":"BH","text":"Bahrain"},{"value":"BD","text":"Bangladesh"},{"value":"BB","text":"Barbados"},{"value":"BY","text":"Belarus"},{"value":"BE","text":"Belgium"},{"value":"BZ","text":"Belize"},{"value":"BJ","text":"Benin"},{"value":"BM","text":"Bermuda"},{"value":"BT","text":"Bhutan"},{"value":"BO","text":"Bolivia"},{"value":"BA","text":"Bosnia and Herzegovina"},{"value":"BW","text":"Botswana"},{"value":"BV","text":"Bouvet Island"},{"value":"BR","text":"Brazil"},{"value":"IO","text":"British Indian Ocean Territory"},{"value":"BN","text":"Brunei"},{"value":"BG","text":"Bulgaria"},{"value":"BF","text":"Burkina Faso"},{"value":"BI","text":"Burundi"},{"value":"KH","text":"Cambodia"},{"value":"CM","text":"Cameroon"},{"value":"CA","text":"Canada"},{"value":"CV","text":"Cape Verde"},{"value":"KY","text":"Cayman Islands"},{"value":"CF","text":"Central African Republic"},{"value":"TD","text":"Chad"},{"value":"CL","text":"Chile"},{"value":"CN","text":"China"},{"value":"CX","text":"Christmas Island"},{"value":"CC","text":"Cocos (Keeling) Islands"},{"value":"CO","text":"Colombia"},{"value":"KM","text":"Comoros"},{"value":"CG","text":"Congo"},{"value":"CD","text":"Congo (DRC)"},{"value":"CK","text":"Cook Islands"},{"value":"CR","text":"Costa Rica"},{"value":"CI","text":"C�te d Ivoire"},{"value":"HR","text":"Croatia"},{"value":"CU","text":"Cuba"},{"value":"CY","text":"Cyprus"},{"value":"CZ","text":"Czech Republic"},{"value":"DK","text":"Denmark"},{"value":"DJ","text":"Djibouti"},{"value":"DM","text":"Dominica"},{"value":"DO","text":"Dominican Republic"},{"value":"EC","text":"Ecuador"},{"value":"EG","text":"Egypt"},{"value":"SV","text":"El Salvador"},{"value":"GQ","text":"Equatorial Guinea"},{"value":"ER","text":"Eritrea"},{"value":"EE","text":"Estonia"},{"value":"ET","text":"Ethiopia"},{"value":"FK","text":"Falkland Islands"},{"value":"FO","text":"Faroe Islands"},{"value":"FJ","text":"Fiji Islands"},{"value":"FI","text":"Finland"},{"value":"FR","text":"France"},{"value":"FG","text":"French Guiana"},{"value":"PF","text":"French Polynesia"},{"value":"TF","text":"French South & Antarctic Lands"},{"value":"GA","text":"Gabon"},{"value":"GM","text":"Gambia,The"},{"value":"GE","text":"Georgia"},{"value":"DE","text":"Germany"},{"value":"GH","text":"Ghana"},{"value":"GI","text":"Gibraltar"},{"value":"GR","text":"Greece"},{"value":"GL","text":"Greenland"},{"value":"GD","text":"Grenada"},{"value":"GP","text":"Guadeloupe"},{"value":"GU","text":"Guam"},{"value":"GT","text":"Guatemala"},{"value":"GG","text":"Guernsey"},{"value":"GN","text":"Guinea"},{"value":"GW","text":"Guinea-Bissau"},{"value":"GY","text":"Guyana"},{"value":"HT","text":"Haiti"},{"value":"HM","text":"Heard Isl & McDonald Isl"},{"value":"HN","text":"Honduras"},{"value":"HK","text":"Hong Kong SAR"},{"value":"HU","text":"Hungary"},{"value":"IS","text":"Iceland"},{"value":"IN","text":"India"},{"value":"ID","text":"Indonesia"},{"value":"IR","text":"Iran"},{"value":"IQ","text":"Iraq"},{"value":"IE","text":"Ireland"},{"value":"IM","text":"Isle of Man"},{"value":"IL","text":"Israel"},{"value":"IT","text":"Italy"},{"value":"JM","text":"Jamaica"},{"value":"JP","text":"Japan"},{"value":"JE","text":"Jersey"},{"value":"JO","text":"Jordan"},{"value":"KZ","text":"Kazakhstan"},{"value":"KE","text":"Kenya"},{"value":"KI","text":"Kiribati"},{"value":"KR","text":"Korea"},{"value":"KW","text":"Kuwait"},{"value":"KG","text":"Kyrgyzstan"},{"value":"LA","text":"Laos"},{"value":"LV","text":"Latvia"},{"value":"LB","text":"Lebanon"},{"value":"LS","text":"Lesotho"},{"value":"LR","text":"Liberia"},{"value":"LY","text":"Libya"},{"value":"LI","text":"Liechtenstein"},{"value":"LT","text":"Lithuania"},{"value":"LU","text":"Luxembourg"},{"value":"MO","text":"Macao SAR"},{"value":"MK","text":"Macedonia, Yugoslav Republic"},{"value":"MG","text":"Madagascar"},{"value":"MW","text":"Malawi"},{"value":"MY","text":"Malaysia"},{"value":"MV","text":"Maldives"},{"value":"ML","text":"Mali"},{"value":"MT","text":"Malta"},{"value":"MH","text":"Marshall Islands"},{"value":"MQ","text":"Martinique"},{"value":"MR","text":"Mauritania"},{"value":"MU","text":"Mauritius"},{"value":"YT","text":"Mayotte"},{"value":"MX","text":"Mexico"},{"value":"FM","text":"Micronesia"},{"value":"MD","text":"Moldova"},{"value":"MC","text":"Monaco"},{"value":"MN","text":"Mongolia"},{"value":"MS","text":"Montserrat"},{"value":"MA","text":"Morocco"},{"value":"MZ","text":"Mozambique"},{"value":"MM","text":"Myanmar"},{"value":"NA","text":"Namibia"},{"value":"NR","text":"Nauru"},{"value":"NP","text":"Nepal"},{"value":"NL","text":"Netherlands"},{"value":"AN","text":"Netherlands Antilles"},{"value":"NC","text":"New Caledonia"},{"value":"NZ","text":"New Zealand"},{"value":"NI","text":"Nicaragua"},{"value":"NE","text":"Niger"},{"value":"NG","text":"Nigeria"},{"value":"NU","text":"Niue"},{"value":"NF","text":"Norfolk Island"},{"value":"KP","text":"North Korea"},{"value":"MP","text":"Northern Mariana Islands"},{"value":"NO","text":"Norway"},{"value":"OM","text":"Oman"},{"value":"PK","text":"Pakistan"},{"value":"PW","text":"Palau"},{"value":"PS","text":"Palestinian Authority"},{"value":"PA","text":"Panama"},{"value":"PG","text":"Papua New Guinea"},{"value":"PY","text":"Paraguay"},{"value":"PE","text":"Peru"},{"value":"PH","text":"Philippines"},{"value":"PN","text":"~Pitcairn Islands"},{"value":"PL","text":"Poland"},{"value":"PT","text":"Portugal"},{"value":"PR","text":"Puerto Rico"},{"value":"QA","text":"Qatar"},{"value":"RE","text":"Reunion"},{"value":"RO","text":"Romania"},{"value":"RU","text":"Russia"},{"value":"RW","text":"Rwanda"},{"value":"WS","text":"Samoa"},{"value":"SM","text":"San Marino"},{"value":"ST","text":"S�o Tom� and Pr�ncipe"},{"value":"SA","text":"Saudi Arabia"},{"value":"SN","text":"Senegal"},{"value":"YU","text":"Serbia, Montenegro"},{"value":"SC","text":"Seychellesv"},{"value":"SL","text":"Sierra Leone"},{"value":"SG","text":"Singapore"},{"value":"SK","text":"Slovakia"},{"value":"SI","text":"Slovenia"},{"value":"SB","text":"Solomon Islands"},{"value":"SO","text":"Somalia"},{"value":"ZA","text":"South Africa"},{"value":"GS","text":"South Georgia and the SSI"},{"value":"ES","text":"Spain"},{"value":"LK","text":"Sri Lanka"},{"value":"SH","text":"St. Helena"},{"value":"KN","text":"St. Kitts and Nevis"},{"value":"LC","text":"St. Lucia"},{"value":"PM","text":"St. Pierre and Miquelon"},{"value":"VC","text":"St. Vincent and the Grenadines"},{"value":"SD","text":"Sudan"},{"value":"SR","text":"Suriname"},{"value":"SJ","text":"Svalbard and Jan Mayen"},{"value":"SZ","text":"Swaziland"},{"value":"SE","text":"Sweden"},{"value":"CH","text":"Switzerland"},{"value":"SY","text":"Syria"},{"value":"TW","text":"Taiwan"},{"value":"TJ","text":"Tajikistan"},{"value":"TZ","text":"Tanzania"},{"value":"TH","text":"Thailand"},{"value":"TP","text":"Timor-Leste"},{"value":"TG","text":"Togo"},{"value":"TK","text":"Tokelau"},{"value":"TO","text":"Tonga"},{"value":"TT","text":"Trinidad and Tobago"},{"value":"TA","text":"Tristan da Cunha"},{"value":"TN","text":"Tunisia"},{"value":"TR","text":"Turkey"},{"value":"TM","text":"Turkmenistan"},{"value":"TC","text":"Turks and Caicos Islands"},{"value":"TV","text":"Tuvalu"},{"value":"UG","text":"Uganda"},{"value":"UA","text":"Ukraine"},{"value":"AE","text":"United Arab Emirates"},{"value":"UK","text":"United Kingdom"},{"value":"US","text":"United States"},{"value":"UM","text":"United States Min Out Isl"},{"value":"UY","text":"Uruguay"},{"value":"UZ","text":"Uzbekistan"},{"value":"VU","text":"Vanuatu"},{"value":"VA","text":"Vatican City"},{"value":"VE","text":"Venezuela"},{"value":"VN","text":"Vietnam"},{"value":"VI","text":"Virgin Islands"},{"value":"VG","text":"Virgin Islands, British"},{"value":"WF","text":"Wallis and Futuna"},{"value":"YE","text":"Yemen"},{"value":"ZM","text":"Zambia"},{"value":"ZW","text":"Zimbabwe"}]);

var ArrState=eval([{"value":"","text":"-Select A State-"},{"value":"Alabama","text":"Alabama"},{"value":"Alaska","text":"Alaska"},{"value":"Alberta","text":"Alberta"},{"value":"American Samoa","text":"American Samoa"},{"value":"Arizona","text":"Arizona"},{"value":"Arkansas","text":"Arkansas"},{"value":"British Columbia","text":"British Columbia"},{"value":"California","text":"California"},{"value":"Colorado","text":"Colorado"},{"value":"Connecticut","text":"Connecticut"},{"value":"Delaware","text":"Delaware"},{"value":"District of Columbia","text":"District of Columbia"},{"value":"Florida","text":"Florida"},{"value":"Georgia","text":"Georgia"},{"value":"Guam","text":"Guam"},{"value":"Hawaii","text":"Hawaii"},{"value":"Idaho","text":"Idaho"},{"value":"Illinois","text":"Illinois"},{"value":"Indiana","text":"Indiana"},{"value":"Iowa","text":"Iowa"},{"value":"Kansas","text":"Kansas"},{"value":"Kentucky","text":"Kentucky"},{"value":"Louisiana","text":"Louisiana"},{"value":"Maine","text":"Maine"},{"value":"Manitoba","text":"Manitoba"},{"value":"Marshall Islands","text":"Marshall Islands"},{"value":"Maryland","text":"Maryland"},{"value":"Massachusetts","text":"Massachusetts"},{"value":"Michigan","text":"Michigan"},{"value":"Minnesota","text":"Minnesota"},{"value":"Mississippi","text":"Mississippi"},{"value":"Missouri","text":"Missouri"},{"value":"Montana","text":"Montana"},{"value":"Nebraska","text":"Nebraska"},{"value":"Nevada","text":"Nevada"},{"value":"New Brunswick","text":"New Brunswick"},{"value":"New Hampshire","text":"New Hampshire"},{"value":"New Jersey","text":"New Jersey"},{"value":"New Mexico","text":"New Mexico"},{"value":"New York","text":"New York"},{"value":"Newfoundland","text":"Newfoundland"},{"value":"North Carolina","text":"North Carolina"},{"value":"North Dakota","text":"North Dakota"},{"value":"Northwest Territory","text":"Northwest Territory"},{"value":"Nova Scotia","text":"Nova Scotia"},{"value":"Ohio","text":"Ohio"},{"value":"Oklahoma","text":"Oklahoma"},{"value":"Ontario","text":"Ontario"},{"value":"Oregon","text":"Oregon"},{"value":"Palau","text":"Palau"},{"value":"Pennsylvania","text":"Pennsylvania"},{"value":"Prince Edward Island","text":"Prince Edward Island"},{"value":"Puerto Rico","text":"Puerto Rico"},{"value":"Quebec","text":"Quebec"},{"value":"Rhode Island","text":"Rhode Island"},{"value":"Saskatchewan","text":"Saskatchewan"},{"value":"South Carolina","text":"South Carolina"},{"value":"South Dakota","text":"South Dakota"},{"value":"Tennessee","text":"Tennessee"},{"value":"Texas","text":"Texas"},{"value":"Utah","text":"Utah"},{"value":"Vermont","text":"Vermont"},{"value":"Virgin Islands","text":"Virgin Islands"},{"value":"Virginia","text":"Virginia"},{"value":"Washington","text":"Washington"},{"value":"West Virginia","text":"West Virginia"},{"value":"Wisconsin","text":"Wisconsin"},{"value":"Wyoming","text":"Wyoming"},{"value":"Yukon Territory","text":"Yukon Territory"}]);

function formatMoneda(num,longEntera, decSep,thousandSep) //formatear numeros xx,xxx.xx
{
	var arg;
	var entero;
	if(typeof(num) == 'undefined') return;
	if(typeof(decSep) == 'undefined') decSep = ',';
	if(typeof(thousandSep) == 'undefined') thousandSep = '.';
	
	if(thousandSep == '.'){	arg=/\./g;}
	else if(thousandSep == ','){arg=/\,/g;}
	
	if(typeof(arg) != 'undefined'){	num = num.toString().replace(arg,'');	}
	
	num = num.toString().replace(/,/g,'.');	
	if (num.indexOf('.') != -1)	{	entero = num.substring(0, num.indexOf('.'));	}
	else entero = num;
	
	if (entero.length > longEntera)
	{	//alert("El n�mero introducido excede de " + longEntera + " digitos en su parte entera");	
		return "0,00";	
	}
	
	if(isNaN(num))	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10)	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+thousandSep+ num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + decSep + cents);
}

function validarCampoTexto(tipo)//validar campos de texto
{
	if(tipo==0)//solo numero entero
		if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
	if(tipo==1)//solo numero y slash --> Fecha de expiracion de tarjeta MM/YYYY
		if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 47) event.returnValue = false;
	if(tipo==2)//solo numero con decimales
		if ((event.keyCode < 48 || event.keyCode > 57)&& event.keyCode != 46) event.returnValue = false;
	if(tipo==3)//todo teclas menos el enter
//		if (event.keyCode == 13)event.returnValue = false;
		if(tecla==13) return false;
}

function validarSinEnter(e)
{
 	var tecla = (document.all) ? e.keyCode : e.which;
	if(tecla==13) return false;
}
function ComboCountry(valuesel)//carga el combo de Country
{
	var cad='<select name="sCountry" id="sCountry"  onChange="return selectCountry();">';
	for(var i=0;i<ArrCountry.length;i++)
	{ 
		cad+='<option '+SelectedCombo(valuesel,ArrCountry[i].value)+' value="'+ArrCountry[i].value+'">'+ArrCountry[i].text+'</option>';
	}
	cad+='</select>';
	return cad;								
}						

function ComboState()//carga el combo de State
{
	var cad='<div style="z-index:1;" id="State"><select name="sState"  id="sState">';
	for(var i=0;i<ArrState.length;i++)
	{ 
		cad+='<option value="'+ArrState[i].value+'">'+ArrState[i].text+'</option>';
	}
	cad+='</select></div>';
	cad+='<div style="display:none;" id="State2"><input class="cajatexto" type="text" size="20" id="sState2" name="sState2""/></div>';
	return cad;								
}

function selectCountry()//Si el Country es diferente de USA muestra un input sino muestra el combo para el State
{
	if(document.getElementById("sCountry").value!="US"){
		document.getElementById("State").style.display="none";
		document.getElementById("State2").style.display="inline";
	}else{
		document.getElementById("State").style.display="inline";
		document.getElementById("State2").style.display="none";
	}
}

function MarcaSelectedState(statesel)//Busca y Muestra(Marca) el estado del usuario a editar
{
	if(document.getElementById("State").style.display=="inline")//combo de state
	{
		for(var i=0;i<ArrState.length;i++)
			if(statesel==ArrState[i].value)
				document.getElementById("sState").options[i].selected = true;
	}
	else//campo de textode state
	{
		document.getElementById("sState2").value=statesel;
	}
}

function ComboPrivilege(valuesel)//carga el combo de Privilege
{
	var tooltippriv="00-09: Administrator<br>10-19: Programmer<br>20-29: Salesman<br>30-39: Realtor<br>40-49: RealtorWO<br>50-59: Broker<br>60-69: Investor<br>70-100: Other";
	var cad='<select name="cbChangePrivilege" id="cbChangePrivilege" onChange="javascript:MostrarPrivUser(); return false;" >'+
			  '<option value="*">Select</option>';
	var tex="";
	for(var i=0;i<100;i++)
	{ 
		tex="";
		if(i<10) tex+="0";
		tex+=i;
		cad+='<option '+SelectedCombo(valuesel,tex)+' value="'+tex+'">'+tex+'</option>';
	}
	cad+='</select>';
//	cad+='&nbsp;&nbsp;<img src="includes/img/sugerencia.png" width="16" height="16" onMouseover="ddrivetip(\''+tooltippriv+'\')"  onMouseout="hideddrivetip()">'
	return cad;								
}

function MostrarPrivUser()//Muestra en el campo de texto Usertype en funcion del combo de Privilege
{
	var sel=document.getElementById("cbChangePrivilege").selectedIndex;
	var texto="";
	if(sel>0)
	{
		if(sel>=1 && sel<10) texto="Administrator";
		if(sel>=10 && sel<20) texto="Programmer";
		if(sel>=20 && sel<30) texto="Salesman";
		if(sel>=30 && sel<40) texto="Realtor";
		if(sel>=40 && sel<50) texto="RealtorWO";
		if(sel>=50 && sel<60) texto="Broker";
		if(sel>=60 && sel<69) texto="Investor";
		if(sel>=70 && sel<100) texto="Other";
	}
	document.getElementById("txUsertypeDeudor").value=texto;
}

function ComboCreditCard(valuesel)//carga el combo de Card Type
{
	var cad='<select name="cbCardtype" id="cbCardtype" >'+
				'<option '+SelectedCombo(valuesel,"none")+' value="none">- Select -</option>'+
				'<option '+SelectedCombo(valuesel,"MasterCard")+' value="MasterCard">Master Card</option>'+
				'<option '+SelectedCombo(valuesel,"Visa")+' value="Visa">Visa</option>'+
				'<option '+SelectedCombo(valuesel,"AmericanExpress")+' value="AmericanExpress">American Express</option>'+
				'<option '+SelectedCombo(valuesel,"Discover")+' value="Discover">Discover</option>'+
			'</select>';
	return cad;								
}

function ComboStatus(valuesel)//carga el combo de Status
{
	var cad='<select name="cbChangeStatus" id="cbChangeStatus" >'+
				'<option value="*">Select</option>'+
				'<option '+SelectedCombo(valuesel,"trial")+' value="trial">Trial</option>'+
				'<option '+SelectedCombo(valuesel,"suspended")+' value="suspended">Suspended</option>'+
				'<option '+SelectedCombo(valuesel,"active")+' value="active">Active</option>'+
				'<option '+SelectedCombo(valuesel,"free")+' value="free">Free</option>'+
				'<option '+SelectedCombo(valuesel,"inactive")+' value="inactive">Inactive</option>'+
				'<option '+SelectedCombo(valuesel,"freeze")+' value="freeze">Freeze</option>'+
				'<option '+SelectedCombo(valuesel,"freezeinac")+' value="freezeinac">FreezeInac</option>'+
				'<option '+SelectedCombo(valuesel,"realtorweb")+' value="realtorweb">RealtorWeb</option>'+
				'<option '+SelectedCombo(valuesel,"investorweb")+' value="investorweb">InvestorWeb</option>'+
			'</select>';
	return cad;								
}

function SelectedCombo(valuesel,valuecomp)//Marca la opcion Selected en funcion del valor que tenga ese registro
{
	if (valuesel.toLowerCase()==valuecomp.toLowerCase()) return " selected ";
	return "";								
}

function VerTarjetaCheque(tarjeta,cheque)//mostrar las filas de pago tarjeta de credito o cheque
{
	if( (tarjeta=='' || tarjeta==null || tarjeta=='none') &&  (cheque=='' || cheque==null || cheque=='none') )//no tiene forma de pago ni tarjeta de credito ni cheque
	{
		document.getElementById("rowCard1").style.display="inline";
		document.getElementById("rowCard2").style.display="inline";
		document.getElementById("rowCard3").style.display="none";
	}
	else
	{
		if( (tarjeta!='' && tarjeta!=null && tarjeta!='none') && (cheque=='' || cheque==null || cheque=='none') )//paga con tarjeta de credito 
		{
			document.getElementById("rowCard1").style.display="inline";
			document.getElementById("rowCard2").style.display="inline";
			document.getElementById("rowCard3").style.display="none";
		}
		if((cheque!='' && cheque!=null && cheque!='none') && (tarjeta=='' || tarjeta==null || tarjeta=='none') )//paga con cheque
		{
			document.getElementById("rowCard1").style.display="none";
			document.getElementById("rowCard2").style.display="none";
			document.getElementById("rowCard3").style.display="inline";
		}
	}
}

function showCheck()//marcar el check de tipo de pago
{
	if(document.getElementById("useCheck").checked==true)//paga con cheque
	{
		document.getElementById("rowCard1").style.display="none";
		document.getElementById("rowCard2").style.display="none";
		document.getElementById("rowCard3").style.display="inline";
	}
	else//paga con tarjeta
	{
		document.getElementById("rowCard1").style.display="inline";
		document.getElementById("rowCard2").style.display="inline";
		document.getElementById("rowCard3").style.display="none";
	}
}

function ComboProduct(idprod)//carga el combo de Productos (Texto -- Precio)
{
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_grid.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("grid=propreltv");
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			var ArrProd=eval('['+aRes[0]+']');
			var sel=0;
			var sal='';
			document.formulario.cbProduct[0]=new Option('Select Product','*',false);
			var icomb=1;
			for(var i=0;i<ArrProd.length;i++)
			{
				if(ArrProd[i].activo=='Y')
				{
					if(idprod==ArrProd[i].id)sel=i+1;
	//				if(ArrProd[i].procode!='' && ArrProd[i].procode!=null)
						sal=ArrProd[i].procode+' - '+ArrProd[i].product+' -- $ '+formatMoneda(ArrProd[i].priceoffer,9,'.','');
	//				else sal=ArrProd[i].product+' -- $ '+formatMoneda(ArrProd[i].priceoffer,9,'.','');
					document.formulario.cbProduct[icomb]=new Option(sal,ArrProd[i].id,false);
					document.formulario.cbProduct[icomb].title=ArrProd[i].priceoffer;
					icomb++;
				}
			}
			document.formulario.cbProduct[sel].selected = true ;
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function ComboAccountExecutive(executive)//cargar combo de usuarios de Account Executive
{
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_grid.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("grid=accountexe");
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			var ArrExecutive=eval('['+aRes[0]+']');
			var sel=0;
			var sal='';
			document.getElementById("cbExecutive").options[0]=new Option('Select Account Executive','*',false);
			for(var i=0;i<ArrExecutive.length;i++)
			{
				if(executive==ArrExecutive[i].userid)sel=i+1;
				sal=ArrExecutive[i].userid+' -- '+ArrExecutive[i].name+' '+ArrExecutive[i].surname;
				document.getElementById("cbExecutive").options[i+1]=new Option(sal,ArrExecutive[i].userid,false);
//				document.formulario.cbProduct[i+1].title=ArrProd[i].priceoffer;
			}
			document.getElementById("cbExecutive").options[sel].selected = true ;
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function generarParametros()//Generar los parametros que se van a pasar para actualizar
{
	var parametros=""
	parametros+="name="+document.getElementById("txNameDeudor").value;
	parametros+="&surname="+document.getElementById("txSurnameDeudor").value;	
	parametros+="&email="+document.getElementById("txEmailDeudor").value;	
	parametros+="&pass="+document.getElementById("txPassDeudor").value;	
	parametros+="&address="+document.getElementById("txAddressDeudor").value;
	parametros+="&hometelephone="+document.getElementById("txHomephoneDeudor").value;	
	parametros+="&mobiletelephone="+document.getElementById("txMobilephoneDeudor").value;	
	parametros+="&country="+document.getElementById("sCountry").options[document.getElementById("sCountry").selectedIndex].value;

	var state="";
	if(document.getElementById("State").style.display=="inline")
		state=document.getElementById("sState").options[document.getElementById("sState").selectedIndex].value;
	if(document.getElementById("State2").style.display=="inline")
		state=document.getElementById("sState2").value;

	parametros+="&paydate="+document.getElementById("txPaydate").value;
	parametros+="&state="+state;
	parametros+="&city="+document.getElementById("txCityDeudor").value;
	parametros+="&pass="+document.getElementById("txPassDeudor").value;	
	parametros+="&zipcode="+document.getElementById("txZipcodeDeudor").value;
	parametros+="&licensen="+document.getElementById("txLicensenDeudor").value;
	parametros+="&officenum="+document.getElementById("txOfficenumDeudor").value;
	parametros+="&brokern="+document.getElementById("txBrokernDeudor").value;
	parametros+="&privilege="+document.getElementById("cbChangePrivilege").options[document.getElementById("cbChangePrivilege").selectedIndex].value;
	parametros+="&usertype="+document.getElementById("txUsertypeDeudor").value;
	parametros+="&status="+document.getElementById("cbChangeStatus").options[document.getElementById("cbChangeStatus").selectedIndex].value;
//	parametros+="&procode="+document.getElementById("txProcodeDeudor").value;
	parametros+="&executive="+document.getElementById("cbExecutive").options[document.getElementById("cbExecutive").selectedIndex].value;
	parametros+="&memouser="+document.getElementById("memouser").value;
	parametros+="&cardtype="+document.getElementById("cbCardtype").options[document.getElementById("cbCardtype").selectedIndex].value;
	parametros+="&cardholdername="+document.getElementById("txCardholdernameDeudor").value;
	parametros+="&cardnumber="+document.getElementById("txCardnumberDeudor").value;
	parametros+="&expirationdate="+document.getElementById("txExpirationdateDeudor").value;
	parametros+="&csvnumber="+document.getElementById("txCsvnumberDeudor").value;
	if(document.getElementById("useCheck").checked==true)//pago con cheque
		parametros+="&billingaddress="+document.getElementById("txBillingaddressDeudorCheque").value;
	else//pago con tarjeta de credito
		parametros+="&billingaddress="+document.getElementById("txBillingaddressDeudorCard").value;
	parametros+="&billingcity="+document.getElementById("txBillingcityDeudor").value;
	parametros+="&billingstate="+document.getElementById("txBillingstateDeudor").value;
	parametros+="&billingzip="+document.getElementById("txBillingzipDeudor").value;
	parametros+="&routingnumber="+document.getElementById("txRoutingnumberDeudor").value;
	parametros+="&bankaccount="+document.getElementById("txBankaccountDeudor").value;
	//parametros+="&idprod="+document.getElementById("cbProduct").options[document.getElementById("cbProduct").selectedIndex].value;
	parametros+="&priceprod="+document.getElementById("txPriceProd").value;
	parametros+="&pseudonimo="+document.getElementById("txPseudonimo").value;
	return parametros;
}

function GuardarUsuario(userid,itemid)//hace el UPDATE en la tabla
{
	document.getElementById("reloj").style.visibility="visible";
	parametros=generarParametros();
//alert(parametros);return;
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_users.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros+"&oper=modificar"+"&ID="+userid);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			document.getElementById("reloj").style.visibility="hidden";
			if(aRes[0]==1)
			{alert("Sorry, this Nickname is already registered ");document.getElementById("txPseudonimo").select();return;}
			FillGrid(1);
			MostrarDetalles(userid,itemid);
		}
	}
}

function ModificarUsuario(userid,itemid)//Mostrar campos de textos y combos editables en el detalle de usuarios
{
	//alert("userid="+userid+"&itemid="+itemid);return;
	document.getElementById("WinDetailsUser").innerHTML='';
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_tags.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("userid="+userid+"&itemid="+itemid);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);
			var VUsers=new Array();
			VUsers=eval('['+results+']');
			var htmlTag=new Array();
			htmlTag.push('<table class="tblDetailsUser" ><thead><tr><th colspan="5">Details Users</th><th><a href="#" onClick="javascript:visibleDeudor(\'WinDetailsUser\',\'ocultar\'); return false;" title="Close Details" ><div align="right"><img src="includes/img/cancel.png" border="0"></div></a></th></tr></thead><tbody>');
			htmlTag.push('<tr><td width="95" class="info">UserID: '+VUsers[0].itemid+'</td><td width="120" class="info"></td><td width="107">&nbsp;</td><td width="120">&nbsp;</td><td width="100">&nbsp;</td><td width="210" class="titulo"><div align="right"><a href=\"#\" onClick=\"HistoricoFull('+VUsers[0].itemid+');\"><img id=\"bothistfull\" alt=\"History Full\" src=\"includes/img/historicofull.png\" border=\"0\" ></a>&nbsp;&nbsp;<a href="#" onClick="histChangeStaus('+VUsers[0].userid+');" title="Ver Historial de Cambios de Status"><img  src="includes/img/chgstat.png"  border="0"></a>&nbsp;&nbsp;<a href=\"#\" onClick=\"HistoricoDelAddCounty('+VUsers[0].itemid+');\"><img id=\"bothistfull\" alt=\"History Add-Del County\" src=\"includes/img/deladdcounty.gif\" border=\"0\" ></a>&nbsp;&nbsp;<INPUT class="botonsml" id="botSave" type="button" value="Save"onclick="GuardarUsuario('+VUsers[0].userid+','+VUsers[0].itemid+')" >&nbsp;<INPUT class="botonsml" id="botCancel" type="button" value="Cancel"  onclick="MostrarDetalles('+VUsers[0].userid+','+VUsers[0].itemid+')" ></div></td></tr>');
			htmlTag.push('<tr class="header"><td colspan="6">Contact Information</td></tr>');
			htmlTag.push('<tr><td class="titulo">Name:</td><td class="info"><input class="cajatexto" name="txNameDeudor" type="text" id="txNameDeudor" size="20" maxlength="50" ></td><td class="titulo">Surname:</td><td class="info"><input class="cajatexto" name="txSurnameDeudor" type="text" id="txSurnameDeudor" size="20" maxlength="50" ></td><td class="titulo">Email:</td><td class="info"><input class="cajatexto" name="txEmailDeudor" type="text" id="txEmailDeudor" size="30" maxlength="50" ></td></tr>');
			htmlTag.push('<tr><td class="titulo">Password:</td><td class="info"><input class="cajatexto" name="txPassDeudor" type="text" id="txPassDeudor" size="20" maxlength="50" ></td class="titulo"><td class="titulo">Address:</td><td colspan="3"  class="info"><input class="cajatexto" name="txAddressDeudor" type="text" id="txAddressDeudor" size="70" maxlength="70" ></td></tr>');
			var country=ComboCountry(VUsers[0].country);
			htmlTag.push('<tr><td class="titulo">Phone:</td><td class="info"><input class="cajatexto" name="txHomephoneDeudor" type="text" id="txHomephoneDeudor" size="20" maxlength="50" ></td><td class="titulo">Cellphone:</td><td class="info"><input class="cajatexto" name="txMobilephoneDeudor" type="text" id="txMobilephoneDeudor" size="20" maxlength="50" ></td><td class="titulo">Country:</td><td class="info">'+country+'</td></tr>');
			var state=ComboState();
			htmlTag.push('<tr><td class="titulo">State:</td><td class="info">'+state+'</td><td class="titulo">City:</td><td class="info"><input class="cajatexto" name="txCityDeudor" type="text" id="txCityDeudor" size="20" maxlength="50" ></td><td class="titulo">Zip Code:</td><td class="info"><input class="cajatexto" name="txZipcodeDeudor" type="text" id="txZipcodeDeudor" size="20" maxlength="6" ></td></tr>');
			htmlTag.push('<tr><td class="titulo">Sing Up:</td><td class="info">'+VUsers[0].testdatebeguin+'</td><td class="titulo">Affiliation:</td><td class="info">'+VUsers[0].affiliationdate+'</td><td class="titulo">License:</td><td class="info" class="info"><input class="cajatexto" name="txLicensenDeudor" type="text" id="txLicensenDeudor" size="20" maxlength="50" ></td></tr>');
			var privilege=ComboPrivilege(VUsers[0].privilege);
			htmlTag.push('<tr><td class="titulo">Office#: </td><td class="info"><input class="cajatexto" name="txOfficenumDeudor" type="text" id="txOfficenumDeudor" size="20" maxlength="50" ></td><td class="titulo">BrokerN:</td><td class="info"><input class="cajatexto" name="txBrokernDeudor" type="text" id="txBrokernDeudor" size="20" maxlength="50" ></td><td class="titulo">Privilege:</td><td class="info">'+privilege+'</td></tr>');
			var status=ComboStatus(VUsers[0].status);
			htmlTag.push('<tr><td class="titulo">User Type: </td><td class="info"><input class="cajatexto" name="txUsertypeDeudor" type="text" id="txUsertypeDeudor" size="20" maxlength="50" readonly="true"></td><td class="titulo">Status:</td><td class="info">'+status+'</td><td class="titulo">Change Status: </td><td class="info">'+VUsers[0].changestatus+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Paydate:</td><td  class="info" ><input class="cajatexto" name="txPaydate" type="text" id="txPaydate" size="20" maxlength="50" ><td class="titulo">Accept:</td><td colspan="3" class="info">'+VUsers[0].accept+'</td></td>');
			htmlTag.push('<tr><td class="titulo">Promo Code:</td><td class="info">'+VUsers[0].procode.toUpperCase()+'</td><td class="titulo">A. Executive:</td><td class="titulo" ><select name="cbExecutive" id="cbExecutive"></select></td><td class="titulo">Nickname: </td><td class="info"><input class="cajatexto" name="txPseudonimo" type="text" id="txPseudonimo" size="20" maxlength="50" ></td></tr>');
			htmlTag.push('<tr><td class="titulo">Couching-Mentoring:</td><td colspan="5" class="info"><input type="checkbox" name="txCouchingM" id="txCouchingM" '+(VUsers[0].platinum_invest==1 ? 'checked="checked"' : '')+' /></td></tr>');
			
			htmlTag.push('<tr><td class="titulo">Notes:</td><td colspan="5" class="info"><textarea name="memouser" id="memouser" cols="110" rows="3" onKeypress="return validarSinEnter(event)"></textarea></td></tr>');
			htmlTag.push('<tr class="header"><td colspan="6">Billing Information</td></tr>');
			var checked=""; if((VUsers[0].cardtype=='' || VUsers[0].cardtype==null) && VUsers[0].routingnumber!='')checked=" checked ";
			htmlTag.push('<tr><td class="titulo" colspan="6"><input '+checked+' name="useCheck" type="checkbox" id="useCheck" onclick="showCheck();" />&nbsp;Use check instead of credit card</td></tr>');
			var cardtype=ComboCreditCard(VUsers[0].cardtype);
			htmlTag.push('<tr id="rowCard1" class="oculto"><td class="titulo">Card Type:</td><td class="info">'+cardtype+'</td><td class="titulo">Holder Name: </td><td class="info"><input class="cajatexto" name="txCardholdernameDeudor" type="text" id="txCardholdernameDeudor" size="20" maxlength="50" ></td><td class="titulo">Card Number:</td><td class="info"><input class="cajatexto" name="txCardnumberDeudor" type="text" id="txCardnumberDeudor" size="20" maxlength="50" ></td></tr>');
			var csvnumber="";
			if(VUsers[0].csvnumber!='' || VUsers[0].csvnumber>0)csvnumber=VUsers[0].csvnumber;
			htmlTag.push('<tr id="rowCard2" class="oculto"><td class="titulo">Exp. Date:</td><td class="titulo"><input class="cajatexto" name="txExpirationdateDeudor" type="text" id="txExpirationdateDeudor" size="10" maxlength="7" onKeypress="validarCampoTexto(1)"> MM/YYYY</td><td class="titulo">CSV Number: </td><td class="info"><input class="cajatexto" name="txCsvnumberDeudor" type="text" id="txCsvnumberDeudor" size="20" maxlength="50" onKeypress="validarCampoTexto(0)"></td><td class="titulo">Billing Address:</td><td class="info"><input class="cajatexto" name="txBillingaddressDeudorCard" type="text" id="txBillingaddressDeudorCard" size="20" maxlength="50" ></td></tr>');
			htmlTag.push('<tr id="rowCard3" class="oculto"><td class="titulo">Routing #:</td><td class="info"><input class="cajatexto" name="txRoutingnumberDeudor" type="text" id="txRoutingnumberDeudor" size="20" maxlength="50" ></td><td class="titulo">Bank Account:</td><td class="info"><input class="cajatexto" name="txBankaccountDeudor" type="text" id="txBankaccountDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing Address:</td><td class="info"><input class="cajatexto" name="txBillingaddressDeudorCheque" type="text" id="txBillingaddressDeudorCheque" size="20" maxlength="50" ></td></tr>');
			htmlTag.push('<tr><td class="titulo">Billing City:</td><td class="info"><input class="cajatexto" name="txBillingcityDeudor" type="text" id="txBillingcityDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing State: </td><td class="info"><input class="cajatexto" name="txBillingstateDeudor" type="text" id="txBillingstateDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing Zip:</td><td class="info"><input class="cajatexto" name="txBillingzipDeudor" type="text" id="txBillingzipDeudor" size="20" maxlength="6" ></td></tr>');
			var precio=""; if(VUsers[0].priceprod!='' || VUsers[0].priceprod>0) precio=formatMoneda(VUsers[0].priceprod,9,'.',',');	//		<select name="cbProduct" id="cbProduct"></select>
			htmlTag.push('<tr><td class="titulo">Product: </td><td class="info">'+VUsers[0].product+'</td><td class="titulo">Price $:</td><td class="info" colspan="3"><input class="cajatexto" name="txPriceProd" type="text" id="txPriceProd" size="10" maxlength="10" onKeypress="validarCampoTexto(2)" ></td></tr>');
			htmlTag.push('<tr><td class="titulo">County: </td><td class="info" colspan="5">'+VUsers[0].condados+'</td></tr>');
			
			htmlTag.push('<tr><td class="titulo"></td><td class="info" colspan="3"></td><td class="titulo"></td><td class="info"></td></tr>');
			htmlTag.push('</tbody></table>');
			document.getElementById("WinDetailsUser").innerHTML=htmlTag.join('');
			document.getElementById("txNameDeudor").value=VUsers[0].name;
			document.getElementById("txSurnameDeudor").value=VUsers[0].surname;
			document.getElementById("txEmailDeudor").value=VUsers[0].email;
			document.getElementById("txPassDeudor").value=VUsers[0].pass;
			document.getElementById("txPassDeudor").value=VUsers[0].pass;
			document.getElementById("txHomephoneDeudor").value=VUsers[0].hometelephone;
			document.getElementById("txMobilephoneDeudor").value=VUsers[0].mobiletelephone;			
			document.getElementById("txCityDeudor").value=VUsers[0].city;
			document.getElementById("txAddressDeudor").value=VUsers[0].address;
			document.getElementById("txZipcodeDeudor").value=VUsers[0].zipcode;
			document.getElementById("txLicensenDeudor").value=VUsers[0].licensen;
			document.getElementById("txOfficenumDeudor").value=VUsers[0].officenum;
			document.getElementById("txBrokernDeudor").value=VUsers[0].brokern;
			document.getElementById("txUsertypeDeudor").value=VUsers[0].usertype;
			//document.getElementById("txProcodeDeudor").value=VUsers[0].procode.toUpperCase();
			document.getElementById("txPaydate").value=VUsers[0].paydate;
			document.getElementById("memouser").value=VUsers[0].notes;
			document.getElementById("txCardholdernameDeudor").value=VUsers[0].cardholdername;
			document.getElementById("txCardnumberDeudor").value=VUsers[0].cardnumber;
			document.getElementById("txExpirationdateDeudor").value=VUsers[0].expirationdate;
			document.getElementById("txCsvnumberDeudor").value=csvnumber;
			document.getElementById("txBillingaddressDeudorCard").value=VUsers[0].billingaddress;
			document.getElementById("txBillingaddressDeudorCheque").value=VUsers[0].billingaddress;
			document.getElementById("txBillingcityDeudor").value=VUsers[0].billingcity;
			document.getElementById("txBillingstateDeudor").value=VUsers[0].billingstate;
			document.getElementById("txBillingzipDeudor").value=VUsers[0].billingzip;
			document.getElementById("txRoutingnumberDeudor").value=VUsers[0].routingnumber;
			document.getElementById("txBankaccountDeudor").value=VUsers[0].bankaccount;
			document.getElementById("txPseudonimo").value=VUsers[0].pseudonimo;
			selectCountry();//mostrar el combo o el campo de texto en el estado
			MarcaSelectedState(VUsers[0].state);//marcar con selected el combo de estado
			VerTarjetaCheque(VUsers[0].cardtype,VUsers[0].routingnumber);//mostrar las filas de pago tarjeta de credito o cheque
			//ComboProduct(VUsers[0].idprod);//cargar combo de productos y su precio
			ComboAccountExecutive(VUsers[0].executive);//cargar combo de usuarios de Account Executive
			document.getElementById("txPriceProd").value=precio;
			MostrarPrivUser();//Mostrar el Usertype en el campo de texto en funcion del privilegio
			visibleDeudor('WinDetailsUser','mostrar');
			//<input class="cajatexto" name="txNameDeudor" type="text" id="txNameDeudor" size="20" maxlength="50" >
			//<input class="cajatexto" name="txProcodeDeudor" type="text" id="txProcodeDeudor" size="20" maxlength="50" readonly="true">			
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function MostrarDetalles(userid,itemid)//Mostrar el detalle del usuario seleccionado
{		
//alert("userid="+userid+"&itemid="+itemid);//return;
	document.getElementById("WinDetailsUser").innerHTML='';
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_tags.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("userid="+userid+"&itemid="+itemid);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			//alert("Mostrar detalle: "+guarda);
//document.getElementById("WinDetailsUser").innerHTML=results;visibleDeudor('WinDetailsUser','mostrar');return;
			var VUsers=new Array();
			VUsers=eval('['+results+']');
			var commercial;
			var realtor;
			var investor;
			if(VUsers[0].pricecommercial==25 || VUsers[0].pricecommercial==250 || VUsers[0].commercial==1 || VUsers[0].commercial=='1')
			{
				commercial='YES';
			}else{
				commercial='NO';
			}
			if(VUsers[0].realtorweb==1){
				realtor='YES';
			}else{
				realtor='NO';
			}
			if(VUsers[0].investorweb==1){
				investor='YES';
			}else{
				investor='NO';
			}
			var htmlTag=new Array();
			htmlTag.push('<table class="tblDetailsUser" ><thead><tr><th colspan="5">Details Users</th><th><a href="#" onClick="javascript:visibleDeudor(\'WinDetailsUser\',\'ocultar\'); return false;" title="Close Details"><div align="right"><img  src="includes/img/cancel.png" border="0"></div></a></th></tr></thead><tbody>');
htmlTag.push('<tr><td width="95" class="info">UserID: '+VUsers[0].itemid+'</td><td width="120" class="info"></td><td width="107">&nbsp;</td><td width="120">&nbsp;</td><td width="100">&nbsp;</td><td width="180"><div align="right"><a href=\"#\" onClick=\"HistoricoFull('+VUsers[0].itemid+');\"><img id=\"bothistfull\" alt=\"History Full\" src=\"includes/img/historicofull.png\" border=\"0\" ></a>&nbsp;&nbsp;<a href="#" onClick="histChangeStaus('+VUsers[0].userid+');" title="Ver Historial de Cambios de Status"><img  src="includes/img/chgstat.png"  border="0"></a>&nbsp;&nbsp;<a href=\"#\" onClick=\"HistoricoDelAddCounty('+VUsers[0].itemid+');\"><img id=\"bothistfull\" alt=\"History Add-Del County\" src=\"includes/img/deladdcounty.gif\" border=\"0\" ></a>&nbsp;&nbsp;<INPUT class="botonsml" id="botEdit" type="button" value="Edit"  onclick="ModificarUsuario('+VUsers[0].userid+','+VUsers[0].itemid+')" ></div></td></tr>');
			htmlTag.push('<tr class="header"><td colspan="6">Contact Information</td></tr>');
			htmlTag.push('<tr><td class="titulo">Name:</td><td class="info">'+VUsers[0].name+'</td><td class="titulo">Surname:</td><td class="info">'+VUsers[0].surname+'</td><td class="titulo">Email:</td><td class="info">'+VUsers[0].email+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Password:</td><td class="info">'+VUsers[0].pass+'</td class="titulo"><td class="titulo">Address:</td><td colspan="3"  class="info">'+VUsers[0].address+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Phone:</td><td class="info">'+VUsers[0].hometelephone+'</td><td class="titulo">Phone_2:</td><td class="info">'+VUsers[0].mobiletelephone+'</td><td class="titulo">Country:</td><td class="info">'+VUsers[0].country+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">State:</td><td class="info">'+VUsers[0].state+'</td><td class="titulo">City:</td><td class="info">'+VUsers[0].city+'</td><td class="titulo">Zip Code:</td><td class="info">'+VUsers[0].zipcode+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Sing Up:</td><td class="info">'+VUsers[0].testdatebeguin+'</td><td class="titulo">Affiliation:</td><td class="info">'+VUsers[0].affiliationdate+'</td><td class="titulo">License:</td><td class="info" class="info">'+VUsers[0].licensen+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Office#: </td><td class="info">'+VUsers[0].officenum+'</td><td class="titulo">BrokerN:</td><td class="info">'+VUsers[0].brokern+'</td><td class="titulo">Privilege:</td><td class="info">'+VUsers[0].privilege+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">User Type: </td><td class="info">'+VUsers[0].usertype+'</td><td class="titulo">Status:</td><td class="info">'+VUsers[0].status+'</td><td class="titulo">Change Status: </td><td class="info">'+VUsers[0].changestatus+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Paydate:</td><td class="info">'+VUsers[0].paydate+'</td><td class="titulo">Accept:</td><td colspan="3" class="info">'+VUsers[0].accept+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Promo Code:</td><td class="info">'+VUsers[0].procode.toUpperCase()+'</td><td class="titulo">A. Executive:</td><td class="info">'+VUsers[0].nameexutive+'</td><td class="titulo">Nickname:</td><td class="info">'+VUsers[0].pseudonimo+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Commercial:</td><td class="info">'+commercial+'</td><td class="titulo">Couching-Mentoring:</td><td class="info">'+(VUsers[0].platinum_invest==1 ? 'YES' : 'NO')+'</td><td></td><td></td></tr>');
			htmlTag.push('<tr><td class="titulo">Realtor Web:</td><td class="info">'+realtor+'</td><td class="titulo">Investor Web:</td><td class="info">'+investor+'</td><td></td><td></td></tr>');
			htmlTag.push('<tr><td class="titulo">Notes:</td><td colspan="5" class="info">'+VUsers[0].notes+'</td></tr>');
			htmlTag.push('<tr class="header"><td colspan="6">Billing Information</td></tr>');
			htmlTag.push('<tr><td class="titulo">Card Type:</td><td class="info">'+VUsers[0].cardtype+'</td><td class="titulo">Holder Name: </td><td class="info">'+VUsers[0].cardholdername+'</td><td class="titulo">Card Number:</td><td class="info">'+VUsers[0].cardnumber+'</td></tr>');
			var csvnumber="";
			if(VUsers[0].csvnumber!='' || VUsers[0].csvnumber>0)csvnumber=VUsers[0].csvnumber;
			htmlTag.push('<tr><td class="titulo">Exp. Date:</td><td class="info">'+VUsers[0].expirationdate+'</td><td class="titulo">CSV Number: </td><td class="info">'+csvnumber+'</td><td class="titulo">Billing Address:</td><td class="info">'+VUsers[0].billingaddress+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">Billing City:</td><td class="info">'+VUsers[0].billingcity+'</td><td class="titulo">Billing State: </td><td class="info">'+VUsers[0].billingstate+'</td><td class="titulo">Billing Zip:</td><td class="info">'+VUsers[0].billingzip+'</td></tr>');
			var precio=""; if(VUsers[0].priceprod!='' || VUsers[0].priceprod>0) precio=formatMoneda(VUsers[0].priceprod,9,'.',',');			
			if(VUsers[0].status.toLowerCase()=='freeze' || VUsers[0].status.toLowerCase()=='freezeinac')precio=precio +' <span style="color:#FF0000; font-weight:bold">( 15.00 - '+VUsers[0].status+' )</span>';
			var saldo=""; if(VUsers[0].saldo!='' || VUsers[0].saldo>0) saldo=formatMoneda(VUsers[0].saldo,9,'.',',');			
			htmlTag.push('<tr><td class="titulo">Routing #: </td><td class="info">'+VUsers[0].routingnumber+'</td><td class="titulo">Bank Account:</td><td class="info">'+VUsers[0].bankaccount+'</td><td class="titulo"></td><td class="info"></td></tr>');
			htmlTag.push('<tr><td class="titulo">Product: </td><td class="info">'+VUsers[0].product+'</td><td class="titulo">Price $:</td><td class="info" >'+precio+'</td><td class="titulo">Balance:</td><td class="info" >'+saldo+'</td></tr>');
			htmlTag.push('<tr><td class="titulo">County: </td><td class="info" colspan="5">'+VUsers[0].condados+'</td></tr>');
			htmlTag.push('<tr><td class="titulo"></td><td class="info" colspan="3"></td><td class="titulo"></td><td class="info"></td></tr>');
			htmlTag.push('</tbody></table>');
			document.getElementById("WinDetailsUser").innerHTML=htmlTag.join('');
			visibleDeudor('WinDetailsUser','mostrar');
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function histChangeStaus(userid)
{
	window.open('usersChangeStatus.php?u='+userid,'','width=470,height=330,resizable=yes');
	/*win=new Ext.Window({
						 title: 'Users - Change Status',
						 width: 700,
						 height: 330,
					 	 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 autoLoad: 'usersChangeStatus.php?u='+userid
				});
	win.show();*/	
}

function HistoricoDelAddCounty(userid)
{
	window.open('usersDelAddCounty.php?u='+userid,'','width=700,height=330,resizable=yes');
	/*win=new Ext.Window({
						 title: 'Users - History Add Del Counties',
						 width: 700,
						 height: 330,
					 	 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 autoLoad: 'usersDelAddCounty.php?u='+userid
				});
	win.show();	*/		
}


//*******************************************************************************************************************************************************************************************************
//*******************************************************************************************************************************************************************************************************
//*******************************************************************************************************************************************************************************************************
//*******************************************************************************************************************************************************************************************************
//*******************************************************************************************************************************************************************************************************
var win;
	function mostrarDetalleUsuers(activo)
	{
var ruta = 'php/details_users.php';
		Ext.Ajax.request( 
		{  
			waitMsg: 'Loading...',
			url: ruta, 
			method: 'POST', 
			timeout: 100000,
			params: { 
				userid : activo,
				caso:'busca'
				},
			failure:function(response,options){
//					_loading_win.close();
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			  
			    Ext.QuickTips.init();
			  
			    var variable = Ext.decode(response.responseText);
/*
		var store = new Ext.data.JsonStore({
			root : 'records',
			fields : ['executive','name'],
			url: ruta+'?cas0=combo'
			
		});*/
                            var uno = new Ext.form.FormPanel({			
			title:'Personal Information',
			autoScroll: true,
			bodyStyle:'padding: 10px',  
			items: [{
			  layout:'column',border:false,
			items:[{layout: 'form',columnWidth:.5,border:false,
                items:[
		{xtype:'textfield',fieldLabel:'Name',name:'name',id:'name',value:variable.NAME,width:200},
				{xtype:'textfield',fieldLabel:'Email',name:'email',id:'email',value:variable.EMAIL,width:200},
				{xtype:'textfield',fieldLabel:'Pseudonimo',name:'pseudonimo',id:'pseudonimo',value:variable.pseudonimo,width:200},
			{xtype:'textfield',fieldLabel:'Hometelephone',name:'hometelephone',id:'hometelephone',value:variable.HOMETELEPHONE,width:200},
			{xtype:'textfield',fieldLabel:'Country',name:'country',id:'country',value:variable.COUNTRY,width:200},
			{xtype:'textfield',fieldLabel:'City',name:'city',id:'city',value:variable.CITY,width:200},			
			{xtype:'textfield',fieldLabel:'Zipcode',name:'zipcode',id:'zipcode',value:variable.zipcode,width:200},		
			{xtype:'numberfield',fieldLabel:'BrokerN',name:'brokern',id:'brokern',value:variable.BrokerN,width:200},
			{xtype:'combo',
			  store: new Ext.data.SimpleStore({
   fields: ['userid', 'nameuser'],
    data : Ext.combos_selec.dataUsers
   }),
			  fieldLabel:'Executive',
			  name:'executive',
			  id:'executive',
			  value:variable.nombres,
			  triggerAction: 'all',
			  valueField: 'userid',
			 displayField: 'nameuser',
			hiddenName: 'nameuser',
			hiddenValue :variable.executive
			//  mode: 'remote'			  
			},
	{xtype:'combo',store:new Ext.data.SimpleStore({
						fields: ['idstatus','texto'],
							data  : [
							  [1,'Trial'],
							  [2,'Suspended'],
							  [3,'Active'],
							  [4,'Free'],
							  [5,'Inactive'],
							  [6,'Freeze'],
							  [7,'FreezeInac']
							]
						}),fieldLabel:'Status',triggerAction: 'all',valueField: 'idstatus',hiddenName: 'idstatus',hiddenValue :variable.status,displayField: 'texto',name:'idstatus',id:'idstatus',value:variable.idstatus,mode: 'local',width:200},	
			{xtype:'combo',store:new Ext.data.SimpleStore({
						fields: ['accept','texto'],
							data  : [
							  ['Y','Yes'],
							  ['N','No']
							]
						}),triggerAction: 'all',valueField: 'accept',mode: 'local',displayField: 'texto',fieldLabel:'Accept',name:'accept',id:'accept',value:((variable.accept)=='N')?'No':'Yes',width:200,hiddenName: 'accept',hiddenValue :variable.accept},			
			]  
			},{layout: 'form',border:false,columnWidth:.5,
                items:[
			
			  {xtype:'textfield',fieldLabel:'Surname',name:'surname',id:'surname',value:variable.SURNAME,width:200},
			  {xtype:'textfield',fieldLabel:'Passwd',name:'passwd',id:'passwd',value:variable.PASS,width:200},
			  {xtype:'label',fieldLabel:'Affiliation Date',text:variable.AFFILIATIONDATE,width:200},
			{xtype:'textfield',fieldLabel:'Mobiletelephone',name:'mobiletelephone',id:'mobiletelephone',value:variable.MOBILETELEPHONE,width:200},
			{xtype:'textfield',fieldLabel:'State',name:'state',id:'state',value:variable.STATE,width:200},
			{xtype:'textfield',fieldLabel:'Address',name:'address',id:'address',value:variable.ADDRESS,width:200},
			{xtype:'label',fieldLabel:'Procode',text:variable.procode,width:200},
			{xtype:'numberfield',fieldLabel:'LicenseN',name:'licensen',id:'licensen',value:variable.LicenseN,width:200},
			{xtype:'textfield',fieldLabel:'Officenum',name:'officenum',id:'officenum',value:variable.officenum,width:200},
	{xtype:'combo',store: new Ext.data.SimpleStore({
						fields: ['idusertype','texto'],
							data  : [
							  [1,'Administrator'],
							  [2,'Bank'],
							  [3,'Broker'],
							  [4,'CustomerServices'],
							  [5,'Investor'],
							  [6,'Other'],
							  [7,'Programmer'],
							  [8,'Realtor'],
							  [9,'RealtorWO'],
							  [10,'Salesman']
							]
						}),fieldLabel:'User Type',triggerAction: 'all',valueField: 'idusertype',hiddenName: 'idusertype',hiddenValue :variable.tipo,displayField: 'texto',name:'idusertype',id:'idusertype',value:variable.idusertype,mode: 'local',width:200},			
			{xtype:'combo',store:new Ext.data.SimpleStore({
						fields: ['blackList','texto'],
							data  : [
							  ['Y','Yes'],
							  ['N','No']
							]
						}),triggerAction: 'all',valueField: 'blackList',mode: 'local',displayField: 'texto',fieldLabel:'Black List',name:'blackList',id:'blackList',value:((variable.blackList)=='N')?'No':'Yes',width:200,hiddenName: 'blackList',hiddenValue :variable.blackList},			
			]}]}],
buttons: [{
                                        handler:function(){    
                                                            uno.getForm().submit( {
							    url :ruta,
                                                            method :'POST',
                                                            params: {
								    userid : activo,
                                                                    caso : 'actualizar'},
                                                            waitTitle   :'Por Favor espere!',
                                                            waitMsg     :'Almacenando Cambios...',
                                                            success: function(uno,action){
                                                                      Ext.Msg.alert('Exito',action.result.msg);
                                                                      /*uno.body.load({url : ruta,
                                                                                      params : {
												userid : activo,
                                                                                 		caso :'busca'
												},
                                                                                                method: 'POST'});*/
                                                                    },
                                                            failure: function(uno,action){
                                                                      Ext.Msg.alert('Fallo',action.result.msg);                                                                      
                                                                    }      
                                                            });                                            
                                                        },
                                        scope:this,
					iconCls:'chgstat',
					text:'Save',
                                        id:'chgstat'}]
});
                           
                            var dos = new Ext.Panel({
                               title:'User Permissions',
			       autoScroll: true,
                               html: '<table id="rounded-corner">'+
			           '<thead>'+
    	'<tr>'+
        	'<th scope="col">id permission</th>'+
	        '<th scope="col">homefinder</th>'+
		'<th scope="col">buyerspro</th>'+
		'<th scope="col">leadsgenerator</th>'+
        '</tr>'+
    '</thead>'+

    '<tbody>'+
    	'<tr>'+
        	'<td>'+variable.idpermission+'</td>'+
        	'<td>'+variable.homefinder+'</td>'+
		'<td>'+variable.buyerspro+'</td>'+
        	'<td>'+variable.leadsgenerator+'</td>'+
        '</tr>'+
 '   </tbody>'+
    '<thead>'+
    	'<tr>'+
		'<th scope="col">residential</th>'+
		'<th scope="col">platinum</th>'+	
        	'<th scope="col">professional</th>'+
	        '<th scope="col">professional_esp</th>'+
        '</tr>'+
    '</thead>'+

    '<tbody>'+
    	'<tr>'+
	        '<td>'+variable.residential+'</td>'+
		'<td>'+variable.platinum+'</td>'+
        	'<td>'+variable.professional+'</td>'+
        	'<td>'+variable.professional_esp+'</td>'+
        '</tr>'+
 '   </tbody>'+
     '<thead>'+
    	'<tr>'+
		'<th scope="col">realtorweb</th>'+
		'<th scope="col">investorweb</th>'+	
        	'<th scope="col">masterweb</th>'+
	        '<th scope="col">advertisingweb</th>'+
        '</tr>'+
    '</thead>'+

    '<tbody>'+
    	'<tr>'+
	        '<td>'+variable.realtorweb+'</td>'+
		'<td>'+variable.investorweb+'</td>'+
        	'<td>'+variable.masterweb+'</td>'+
        	'<td>'+variable.advertisingweb+'</td>'+
        '</tr>'+
 '   </tbody>'+
'</table>',
			       border	: false
                              // height:346,
                           //    anchor:'100%'    
                              });
			    
			      var tres = new Ext.Panel({
                               title:'Billing',
			       autoScroll: true,
                               html: 'html-css',
			       border		: false
                              // height:346,
                            //   anchor:'100%'    
                              });
							
			      var cuatro = new Ext.Panel({
                               title:'Product',
                               autoScroll: true,
			       html: 'html-css',	
			       border		: false
                              // height:346,
                            //   anchor:'100%'    
                              });
                           
                            var tab = new Ext.TabPanel({
                               
                            activeTab: 0,
			    border : false,
                            items:[uno,
                                  dos,tres,cuatro] 
                                });  
			    
			            var win = new Ext.Window({  
            title:'<H1 align="center">('+variable.USERID+') '+variable.NAME+' '+variable.SURNAME+'<H1>',  
            width:900,  			
	    layout		: "fit",
            height:480,  
	    border		: false,
	    autoScroll: true,
	    modal   : true,
         //   bodyStyle: 'background-color:#fff;',  
            items: [tab]
        });  
        win.show(); 
                           
                        new Ext.ToolTip({
                                target: 'historico',
                                anchor: 'bottom',
                                html: 'Historico Full'
                              });
			                /*        new Ext.ToolTip({
                                target: 'editar',
                                anchor: 'bottom',
                                html: 'Edit'
                              });
						                        new Ext.ToolTip({
                                target: 'deladdcounty',
                                anchor: 'bottom',
                                html: 'History add-del County'
                              });*/
									                        new Ext.ToolTip({
                                target: 'chgstat',
                                anchor: 'bottom',
                                html: 'Updates the Data'
                              });
                       
                        }                                
		});		
	};

function modificarDetalleUser(activo,itemid)//Mostrar campos de textos y combos editables en el detalle de usuarios
{
		//_loading_win.show();
		Ext.Ajax.request( 
		{  
			waitMsg: 'Loading...',
			url: 'respuesta_tags.php', 
			method: 'POST', 
			timeout: 100000,
			params: { 
						where: 'customerservices',
						userid: activo,
						itemid: activo
					},
			failure:function(response,options){
//					_loading_win.close();
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response,options){
//						win2.close();
					
				var rest = Ext.util.JSON.decode(response.responseText);
										
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);
//Ext.MessageBox.alert('Warning',rest.results);return;
									
				var VUsers=rest.results;

						var htmlTag=new Array();
						htmlTag.push('<table class="tblDetailsUser" id="pagtag_table" ><tbody>');
						htmlTag.push('<tr><td width="95" class="info">UserID: '+VUsers[0].itemid+'</td><td width="120" class="info"></td><td width="107">&nbsp;</td><td width="120">&nbsp;</td><td width="100">&nbsp;</td><td width="210" class="titulo"><div align="right"><a href=\"#\" onClick=\"HistoricoFull('+VUsers[0].itemid+');\"><img id=\"bothistfull\" name=\"bothistfull\" src=\"includes/img/historicofull.png\" border=\"0\" ></a>&nbsp;&nbsp;<a href="#" onClick="histChangeStaus('+VUsers[0].userid+');" id=\"botchstatus\" name=\"botchstatus\" ><img  src="includes/img/chgstat.png"  border="0"></a>&nbsp;&nbsp;<a href=\"#\" onClick=\"HistoricoDelAddCounty('+VUsers[0].itemid+');\"><img id=\"bothistadddelcty\" name=\"bothistadddelcty\" src=\"includes/img/deladdcounty.gif\" border=\"0\" ></a>&nbsp;&nbsp;<INPUT class="botonsml" id="botSave" type="button" value="Save" onclick="guardarDetailsUser('+VUsers[0].userid+','+VUsers[0].userid+')" >&nbsp;<INPUT class="botonsml" id="botCancel" type="button" value="Cancel"  onclick="win.close();mostrarDetalleUsuers('+VUsers[0].userid+',\'customerservices\')" ></div></td></tr>');
						htmlTag.push('<tr class="header"><td colspan="6">Contact Information</td></tr>');
						htmlTag.push('<tr><td class="titulo">Name:</td><td class="info"><input class="cajatexto" name="txNameDeudor" type="text" id="txNameDeudor" size="20" maxlength="50" ></td><td class="titulo">Surname:</td><td class="info"><input class="cajatexto" name="txSurnameDeudor" type="text" id="txSurnameDeudor" size="20" maxlength="50" ></td><td class="titulo">Email:</td><td class="info"><input class="cajatexto" name="txEmailDeudor" type="text" id="txEmailDeudor" size="30" maxlength="50" ></td></tr>');
						htmlTag.push('<tr><td class="titulo">Password:</td><td class="info"><input class="cajatexto" name="txPassDeudor" type="text" id="txPassDeudor" size="20" maxlength="50" ></td class="titulo"><td class="titulo">Address:</td><td colspan="3"  class="info"><input class="cajatexto" name="txAddressDeudor" type="text" id="txAddressDeudor" size="70" maxlength="70" ></td></tr>');
						var country=ComboCountry(VUsers[0].country);
						htmlTag.push('<tr><td class="titulo">Phone:</td><td class="info"><input class="cajatexto" name="txHomephoneDeudor" type="text" id="txHomephoneDeudor" size="20" maxlength="50" ></td><td class="titulo">Cellphone:</td><td class="info"><input class="cajatexto" name="txMobilephoneDeudor" type="text" id="txMobilephoneDeudor" size="20" maxlength="50" ></td><td class="titulo">Country:</td><td class="info">'+country+'</td></tr>');
						var state=ComboState();
						htmlTag.push('<tr><td class="titulo">State:</td><td class="info">'+state+'</td><td class="titulo">City:</td><td class="info"><input class="cajatexto" name="txCityDeudor" type="text" id="txCityDeudor" size="20" maxlength="50" ></td><td class="titulo">Zip Code:</td><td class="info"><input class="cajatexto" name="txZipcodeDeudor" type="text" id="txZipcodeDeudor" size="20" maxlength="6" ></td></tr>');
						htmlTag.push('<tr><td class="titulo">Sing Up:</td><td class="info">'+VUsers[0].testdatebeguin+'</td><td class="titulo">Affiliation:</td><td class="info">'+VUsers[0].affiliationdate+'</td><td class="titulo">License:</td><td class="info" class="info"><input class="cajatexto" name="txLicensenDeudor" type="text" id="txLicensenDeudor" size="20" maxlength="50" ></td></tr>');
						var privilege=ComboPrivilege(VUsers[0].privilege);
						htmlTag.push('<tr><td class="titulo">Office#: </td><td class="info"><input class="cajatexto" name="txOfficenumDeudor" type="text" id="txOfficenumDeudor" size="20" maxlength="50" ></td><td class="titulo">BrokerN:</td><td class="info"><input class="cajatexto" name="txBrokernDeudor" type="text" id="txBrokernDeudor" size="20" maxlength="50" ></td><td class="titulo">Privilege:</td><td class="info">'+privilege+'</td></tr>');
						var status=ComboStatus(VUsers[0].status);
						htmlTag.push('<tr><td class="titulo">User Type: </td><td class="info"><input class="cajatexto" name="txUsertypeDeudor" type="text" id="txUsertypeDeudor" size="20" maxlength="50" readonly="true"></td><td class="titulo">Status:</td><td class="info">'+status+'</td><td class="titulo">Change Status: </td><td class="info">'+VUsers[0].changestatus+'</td></tr>');
						htmlTag.push('<tr><td class="titulo">Paydate:</td><td colspan="5" class="info" ><input class="cajatexto" name="txPaydate" type="text" id="txPaydate" size="20" maxlength="50" ></td>');
						htmlTag.push('<tr><td class="titulo">Promo Code:</td><td class="info">'+VUsers[0].procode.toUpperCase()+'</td><td class="titulo">A. Executive:</td><td class="titulo" ><select name="cbExecutive" id="cbExecutive"></select></td><td class="titulo">Nickname: </td><td class="info"><input class="cajatexto" name="txPseudonimo" type="text" id="txPseudonimo" size="20" maxlength="50" ></td></tr>');
						htmlTag.push('<tr><td class="titulo">Couching-Mentoring:</td><td colspan="5" class="info"><input type="checkbox" '+(VUsers[0].platinum_invest==1 ? 'checked="checked"' : '')+' name="txCouchingM" id="txCouchingM" /></td></tr>');
						
						htmlTag.push('<tr><td class="titulo">Notes:</td><td colspan="5" class="info"><textarea name="memouser" id="memouser" cols="110" rows="3" onKeypress="return validarSinEnter(event)"></textarea></td></tr>');
						htmlTag.push('<tr class="header"><td colspan="6">Billing Information</td></tr>');
						var checked=""; if((VUsers[0].cardtype=='' || VUsers[0].cardtype==null) && VUsers[0].routingnumber!='')checked=" checked ";
				//		htmlTag.push('<tr><td class="titulo" colspan="6"><input '+checked+' name="useCheck" type="checkbox" id="useCheck" onclick="showCheck();" />&nbsp;Use check instead of credit card</td></tr>');
						var cardtype=ComboCreditCard(VUsers[0].cardtype);
						htmlTag.push('<tr ><td class="titulo">Card Type:</td><td class="info">'+cardtype+'</td><td class="titulo">Holder Name: </td><td class="info"><input class="cajatexto" name="txCardholdernameDeudor" type="text" id="txCardholdernameDeudor" size="20" maxlength="50" ></td><td class="titulo">Card Number:</td><td class="info"><input class="cajatexto" name="txCardnumberDeudor" type="text" id="txCardnumberDeudor" size="20" maxlength="50" ></td></tr>');
						var csvnumber="";
						if(VUsers[0].csvnumber!='' || VUsers[0].csvnumber>0)csvnumber=VUsers[0].csvnumber;
						htmlTag.push('<tr ><td class="titulo">Exp. Date:</td><td class="titulo"><input class="cajatexto" name="txExpirationdateDeudor" type="text" id="txExpirationdateDeudor" size="10" maxlength="7" onKeypress="validarCampoTexto(1)"> MM/YYYY</td><td class="titulo">CSV Number: </td><td class="info"><input class="cajatexto" name="txCsvnumberDeudor" type="text" id="txCsvnumberDeudor" size="20" maxlength="50" onKeypress="validarCampoTexto(0)"></td><td class="titulo">Billing Address:</td><td class="info"><input class="cajatexto" name="txBillingaddressDeudorCard" type="text" id="txBillingaddressDeudorCard" size="20" maxlength="50" ></td></tr>');
//						htmlTag.push('<tr id="rowCard3" class="oculto"><td class="titulo">Routing #:</td><td class="info"><input class="cajatexto" name="txRoutingnumberDeudor" type="text" id="txRoutingnumberDeudor" size="20" maxlength="50" ></td><td class="titulo">Bank Account:</td><td class="info"><input class="cajatexto" name="txBankaccountDeudor" type="text" id="txBankaccountDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing Address:</td><td class="info"><input class="cajatexto" name="txBillingaddressDeudorCheque" type="text" id="txBillingaddressDeudorCheque" size="20" maxlength="50" ></td></tr>');
						
						htmlTag.push('<tr><td class="titulo">Billing City:</td><td class="info"><input class="cajatexto" name="txBillingcityDeudor" type="text" id="txBillingcityDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing State: </td><td class="info"><input class="cajatexto" name="txBillingstateDeudor" type="text" id="txBillingstateDeudor" size="20" maxlength="50" ></td><td class="titulo">Billing Zip:</td><td class="info"><input class="cajatexto" name="txBillingzipDeudor" type="text" id="txBillingzipDeudor" size="20" maxlength="6" ></td></tr>');
						var precio=""; if(VUsers[0].priceprod!='' || VUsers[0].priceprod>0) precio=formatMoneda(VUsers[0].priceprod,9,'.',',');	//		<select name="cbProduct" id="cbProduct"></select>
						htmlTag.push('<tr><td class="titulo">Product: </td><td class="info">'+VUsers[0].product+'</td><td class="titulo">Price $:</td><td class="info" colspan="3"><input class="cajatexto" name="txPriceProd" type="text" id="txPriceProd" size="10" maxlength="10" onKeypress="validarCampoTexto(2)" ></td></tr>');
						htmlTag.push('<tr><td class="titulo">County: </td><td class="info" colspan="5">'+VUsers[0].condados+'</td></tr>');
						
						htmlTag.push('<tr><td class="titulo"></td><td class="info" colspan="3"></td><td class="titulo"></td><td class="info"></td></tr>');
						htmlTag.push('</tbody></table>');
				html=htmlTag.join('');
			
//				var html="|"+VUsers[0].name+"|";
				//mostrar ventana con datos.
				win=new Ext.Window({
						 title: 'Details Users',
						 width: 950,
						 height: 530,
					 	 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 html: html
				});
//						_loading_win.hide();
				win.show();
//				var w=parseInt(document.getElementById('pagtag_table').clientWidth)+10;	win.setWidth(w); win.center();
						document.getElementById("txNameDeudor").value=VUsers[0].name;
						document.getElementById("txSurnameDeudor").value=VUsers[0].surname;
						document.getElementById("txEmailDeudor").value=VUsers[0].email;
						document.getElementById("txPassDeudor").value=VUsers[0].pass;
						document.getElementById("txPassDeudor").value=VUsers[0].pass;
						document.getElementById("txHomephoneDeudor").value=VUsers[0].hometelephone;
						document.getElementById("txMobilephoneDeudor").value=VUsers[0].mobiletelephone;			
						document.getElementById("txCityDeudor").value=VUsers[0].city;
						document.getElementById("txAddressDeudor").value=VUsers[0].address;
						document.getElementById("txZipcodeDeudor").value=VUsers[0].zipcode;
						document.getElementById("txLicensenDeudor").value=VUsers[0].licensen;
						document.getElementById("txOfficenumDeudor").value=VUsers[0].officenum;
						document.getElementById("txBrokernDeudor").value=VUsers[0].brokern;
						document.getElementById("txUsertypeDeudor").value=VUsers[0].usertype;
						document.getElementById("txPaydate").value=VUsers[0].paydate;
						document.getElementById("memouser").value=VUsers[0].notes;
						document.getElementById("txCardholdernameDeudor").value=VUsers[0].cardholdername;
						document.getElementById("txCardnumberDeudor").value=VUsers[0].cardnumber;
						document.getElementById("txExpirationdateDeudor").value=VUsers[0].expirationdate;
						document.getElementById("txCsvnumberDeudor").value=csvnumber;
						document.getElementById("txBillingaddressDeudorCard").value=VUsers[0].billingaddress;
						//document.getElementById("txBillingaddressDeudorCheque").value=VUsers[0].billingaddress;
						document.getElementById("txBillingcityDeudor").value=VUsers[0].billingcity;
						document.getElementById("txBillingstateDeudor").value=VUsers[0].billingstate;
						document.getElementById("txBillingzipDeudor").value=VUsers[0].billingzip;
						//document.getElementById("txRoutingnumberDeudor").value=VUsers[0].routingnumber;
						//document.getElementById("txBankaccountDeudor").value=VUsers[0].bankaccount;
						document.getElementById("txPseudonimo").value=VUsers[0].pseudonimo;
						document.getElementById("txPriceProd").value=precio;
						selectCountry();//mostrar el combo o el campo de texto en el estado
						MarcaSelectedState(VUsers[0].state);//marcar con selected el combo de estado
						//VerTarjetaCheque(VUsers[0].cardtype,VUsers[0].routingnumber);//mostrar las filas de pago tarjeta de credito o cheque
						//ComboProduct(VUsers[0].idprod);//cargar combo de productos y su precio
						ComboAccountExecutive(VUsers[0].executive);//cargar combo de usuarios de Account Executive
//						MostrarPrivUser();//Mostrar el Usertype en el campo de texto en funcion del privilegio
				mostrarQuicktip('cbChangePrivilege','00-09: Administrator<br>10-19: Programmer<br>20-29: Salesman<br>30-39: Realtor<br>40-49: RealtorWO<br>50-59: Broker<br>60-69: Investor<br>70-100: Other');	
				mostrarQuicktip('bothistfull','History Full');	
				mostrarQuicktip('botchstatus','Ver Historial de Cambios de Status');	
				mostrarQuicktip('bothistadddelcty','History Add-Del County');	
			}                                
		});		

}


function guardarDetailsUser(userid,itemid)//hace el UPDATE en la tabla
{
		Ext.Ajax.request( 
		{  
			waitMsg: 'Loading...',
			url: 'respuesta_users.php', 
			method: 'POST', 
			timeout: 100000,
			params: { 
						 name:document.getElementById("txNameDeudor").value
						,surname:document.getElementById("txSurnameDeudor").value	
						,email:document.getElementById("txEmailDeudor").value	
						,pass:document.getElementById("txPassDeudor").value	
						,address:document.getElementById("txAddressDeudor").value
						,hometelephone:document.getElementById("txHomephoneDeudor").value	
						,mobiletelephone:document.getElementById("txMobilephoneDeudor").value	
						,country:document.getElementById("sCountry").options[document.getElementById("sCountry").selectedIndex].value
						,paydate:document.getElementById("txPaydate").value
						,state:state=document.getElementById("sState").options[document.getElementById("sState").selectedIndex].value
						,city:document.getElementById("txCityDeudor").value
						,pass:document.getElementById("txPassDeudor").value	
						,zipcode:document.getElementById("txZipcodeDeudor").value
						,licensen:document.getElementById("txLicensenDeudor").value
						,officenum:document.getElementById("txOfficenumDeudor").value
						,brokern:document.getElementById("txBrokernDeudor").value
						,privilege:document.getElementById("cbChangePrivilege").options[document.getElementById("cbChangePrivilege").selectedIndex].value
						,usertype:document.getElementById("txUsertypeDeudor").value
						,status:document.getElementById("cbChangeStatus").options[document.getElementById("cbChangeStatus").selectedIndex].value
						,executive:document.getElementById("cbExecutive").options[document.getElementById("cbExecutive").selectedIndex].value
						,couchingM:document.getElementById("txCouchingM").checked
						,memouser:document.getElementById("memouser").value
						,cardtype:document.getElementById("cbCardtype").options[document.getElementById("cbCardtype").selectedIndex].value
						,cardholdername:document.getElementById("txCardholdernameDeudor").value
						,cardnumber:document.getElementById("txCardnumberDeudor").value
						,expirationdate:document.getElementById("txExpirationdateDeudor").value
						,csvnumber:document.getElementById("txCsvnumberDeudor").value
						,billingaddress:document.getElementById("txBillingaddressDeudorCard").value
						,billingcity:document.getElementById("txBillingcityDeudor").value
						,billingstate:document.getElementById("txBillingstateDeudor").value
						,billingzip:document.getElementById("txBillingzipDeudor").value
						,priceprod:document.getElementById("txPriceProd").value
						,pseudonimo:document.getElementById("txPseudonimo").value
						,oper:'modificar'
						,ID:userid			
						,where:'customerservices'
					},
			failure:function(response,options){
//					_loading_win.close();
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				if(rest.succes==false)
				{	Ext.MessageBox.alert('Warning',rest.msg);return;}
				//alert('pasa '+wherepage);
				if(wherepage!='users')
					location.href=wherepage+'.php';
				else	
					mostrarDetalleUsuers(userid,'customerservices');		
				win.close();
			}                                
	});		
}

function mostrarQuicktip(targ,text)
{
	new Ext.ToolTip({target: targ,html: text});
}
