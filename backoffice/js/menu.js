Ext.onReady(function(){

   var tb = new Ext.Toolbar({
		id:'menu_page',
		items: [{
				text:'Connection Control',
				menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Connection Control',
							href: 'conexionAdmin9.php'
						})
					]
				})  
			},
			{
				text:'Pendings',
				menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Pending Manager',
							href: 'adminTaskManager.php'
						}),
						new Ext.menu.Item({
							text: 'Log',
							href: 'adminTaskManager.php'
						})
					]
				})  
			}
			,{
		 	text:'Products',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Products',
							href: 'products.php'
						})
					]
				})  
			}/*,{
		 	text:'Trials',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Trials',
							href: 'deudores.php'
						})
					]
				})  
			}*/,{
		 	text:'Workshop',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Manage Workshop',
							href: 'manageWorkshop.php'
						}),
						new Ext.menu.Item({
							text: 'Users Workshop',
							href: 'usersWorkshop.php'
						})
					]
				})  
			},{
		 	text:'Users',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Search',
							href: 'search_users.php'
						}),
						'-',
						/*new Ext.menu.Item({
							text: 'Users New',
							href: 'users_new.php'
						}),*/
						new Ext.menu.Item({
							text: 'Retention Rate',
							href: 'retentionrate.php'
						}),
						new Ext.menu.Item({
							text: 'Users Suspended',
							href: 'usersSuspended.php'
						}),
						'-',
						new Ext.menu.Item({
							disabled: enableadmin2,	
							text: 'Delete Users',
							href: 'usersDelete.php'
						}),
						new Ext.menu.Item({
							disabled: enableadmin2,	
							text: 'Users Credit Card',
							href: 'userscc.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Groups Users',
							href: 'adminUserGroup.php'
						}),
						new Ext.menu.Item({
							text: 'Groups Logs',
							href: 'adminLogUserGroup.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Short Sale Users ',
							href: 'shortSale.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Dave Dinkel Users ',
							href: 'ddusers.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Advertising ',
							href: 'adminAdvertising.php'
						})
					]
				})  
			},{
		 	text:'Emails',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Emails',
							href: 'emails.php'
						}),
						'-',
						new Ext.menu.Item({
							text: 'Template Emails',
							href: 'templateEmails.php'
						}),
						new Ext.menu.Item({
							text: 'Users Emails',
							href: 'usersEmails.php'
						})
					]
				})  
			},{
		 	text:'Camptit',
			disabled: enableadmin,	
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Camptit',
							href: 'camptit.php'
						})
					]
				})  
			},{
		 	text:'Aff. Marketing',
			menu: new Ext.menu.Menu({
					items: [
						/*new Ext.menu.Item({
							text: 'Affiliate Info',
							href: 'affMarketingUserInfo.php'
						}),*/
						new Ext.menu.Item({
							text: 'Cobrar Actives',
							href: 'affMarketingDeudores.php'
						}),
						new Ext.menu.Item({
							text: 'Cobrar Inactives',
							href: 'affMarketingDeudoresInac.php'
						}),
						new Ext.menu.Item({
							text: 'Cobros',
							href: 'affMarketingCobros.php'
						}),
						new Ext.menu.Item({
							text: 'Refunds',
							href: 'affMarketingRefunds.php'
						}),
						new Ext.menu.Item({
							text: 'Commissions',
							href: 'affMarketingComisiones.php'
						}),
						new Ext.menu.Item({
							text: 'Referrals',
							href: 'affMarketingReferrals.php'
						}),
						new Ext.menu.Item({
							text: 'Cash Flow',
							//href: 'affMarketingCashFlow.php'
				            handler: onButtonClick
							
						}),
						/*new Ext.menu.Item({
							text: 'Debit/Credit/Refuns',
							href: 'affMarketingCredDeb.php'
						})
						,*/
						new Ext.menu.Item({
							text: 'PP Cobro Dia',
							href: 'affMarketingDeudoresFecha.php'
						})
						,
						new Ext.menu.Item({
							text: 'Status Users',
							href: 'statususers.php'
						}),'-',
						new Ext.menu.Item({
							text: 'Expenses',
							href: 'affMarketingExpenses.php'
						}),'-',
						new Ext.menu.Item({
							text: 'Specials Pays',
							href: 'specialspays.php'
						})
					]
				})  
			},{
		 	text:'New User',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'New User',
							href: 'NewUser.php'
						})
						,
						new Ext.menu.Item({
							text: 'Users Registered',
							href: 'usersregistered.php'
						})
					]
				})  
			},{
		 	text:'Admin BD',
			disabled: enableadmin,	
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Admin BD',
							href: 'http://www.ximausa.com/mant/administrador/adminbd.php'
						})
					]
				})  
			},{
		 	text:'Login',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'SessionLogs',
							href: 'sessionlogs.php'
						}),
						new Ext.menu.Item({
							text: 'Monitoring',
							href: 'monitoring.php'
						}),
						new Ext.menu.Item({
							text: 'Monitoring Control',
							href: 'monitoringControl.php'
						})
					]
				})  
			},{
		 	text:'Tickets',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Admin Tickets',
							href: 'adminTickets.php'
						})
					]
				})  
			},{
		 	text:'Videos',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Videos - Webinars',
							href: 'adminWebinar.php'
						}),
						new Ext.menu.Item({
							text: 'Videos - Help',
							href: 'adminTraining.php'
						}),
						new Ext.menu.Item({
							text: 'Videos - Customer',
							href: 'adminTrainingC.php'
						}),
						new Ext.menu.Item({
							text: 'Videos - Programmer',
							href: 'adminTrainingP.php'
						})
					]
				})  
			},{
		 	text:'Data',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Diario',
							href: 'diario.php'
						}),
						new Ext.menu.Item({
							text: 'Crtl Frank',
							href: 'ctrl_frank.php'
						}),
						new Ext.menu.Item({
							text: 'Backups 1',
							href: 'backups1.php'
						}),
						new Ext.menu.Item({
							text: 'Comparable XRay',
							href: 'xrayComparado.php'
						}),
						new Ext.menu.Item({
							text: 'Status A Daily',
							href: 'statusadiario.php'
						}),
						new Ext.menu.Item({
							text: 'Daily Complete',
							href: 'controldiario.php'
						}),
						new Ext.menu.Item({
							text: 'Access Control',
							href: 'accesscontrol.php'
						}),
						new Ext.menu.Item({
							text: 'PAS Control',
							href: 'passcontrol.php'
						}),
						new Ext.menu.Item({
							text: 'Update Psummary with PAS',
							href: 'updatePsummary.php'
						})
					]
				})  
			},{
			text:'Settings',
			menu: new Ext.menu.Menu({
					items: [
						new Ext.menu.Item({
							text: 'Advertisings',
							menu: new Ext.menu.Menu({
								items: [
									new Ext.menu.Item({
										text: 'Model',
										href: 'model.php'
									}),
									new Ext.menu.Item({
										text: 'Client',
										href: 'client.php'
									}),
									new Ext.menu.Item({
										text: 'Advertising',
										href: 'advertising.php'
									})
								]
							})
						}),
						new Ext.menu.Item({
							text: 'Clean Cache',
							href: 'cleancache.php'
						}),
						new Ext.menu.Item({
							text: 'Training Videos',
							href: 'trainingVideos.php'
						}),
						new Ext.menu.Item({
							text: 'Foreclosure Released',
							href: 'foreclosurereleased.php'
						})
					]
				})  
			},{
				text:'Contracts',
				menu: new Ext.menu.Menu({
						items: [
							new Ext.menu.Item({
								text: 'Contracts',
								href: 'contracts.php'
							}),
							new Ext.menu.Item({
								text: 'Statistic',
								href: 'contracts_statistic.php'
							}),
							new Ext.menu.Item({
								text: 'Recursives',
								href: 'contracts_recursive.php'
							})
						]
					})  
			},{
				text:'Coaching',
				menu: new Ext.menu.Menu({
						items: [
							new Ext.menu.Item({
								text: 'coaching',
								href: 'adminCoaching.php'
							})
						]
					})  
			},
			{
				text: 'Logout',
				//href: 'logout.php'
				handler: function(){document.location='logout.php';}
			}
		],
		autoShow: true
	});

	function onButtonClick(btn){
		if(!enableadmin)location.href='affMarketingCashFlow.php';
    }

});


function formatMoneda(num,longEntera, decSep,thousandSep) //formatear numeros xx,xxx.xx
{
	var arg;
	var entero;
	if(typeof(num) == 'undefined') return;
	if(typeof(decSep) == 'undefined') decSep = ',';
	if(typeof(thousandSep) == 'undefined') thousandSep = '.';
	
	if(thousandSep == '.'){	arg=/\./g;}
	else if(thousandSep == ','){arg=/\,/g;}
	
	if(typeof(arg) != 'undefined'){	num = num.toString().replace(arg,'');	}
	
	num = num.toString().replace(/,/g,'.');	
	if (num.indexOf('.') != -1)	{	entero = num.substring(0, num.indexOf('.'));	}
	else entero = num;
	
	if (entero.length > longEntera)
	{	//alert("El n�mero introducido excede de " + longEntera + " digitos en su parte entera");	
		return "0,00";	
	}
	
	if(isNaN(num))	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10)	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+thousandSep+ num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + decSep + cents);
}