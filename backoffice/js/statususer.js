function nuevoAjax()
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
	var fecha=new Date();
	var mes=fecha.getMonth() +1;
	var anio=fecha.getFullYear();
	function generateData(){
		var data = [];
		data.push([anio-2]);
		data.push([anio-1]);
		data.push([anio]);
		return data;
	}
	var dataYear =new Ext.data.SimpleStore({
					fields: ['id'],
					data: generateData()
				});
	var dataMonths=new Ext.data.SimpleStore({
						fields: ['id','name'],
							data  : [
							  ['1','January'],
							  ['2','February'],
							  ['3','March'],
							  ['4','April'],
							  ['5','May'],
							  ['6','June'],
							  ['7','July'],
							  ['8','August'],
							  ['9','September'],
							  ['10','October'],
							  ['11','November'],
							  ['12','December']
							]
						});
/////////////////FIN Variables////////////////////
    var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=statususers',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=statususers', timeout: 3600000 }),
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname' 
			,'status' 
			,'usertype'
			,'frecuency'
			,{name: 'paydate', type: 'date',dateFormat: 'Y-m-d'}
			,{name: 'ultimoupdate', type: 'date',dateFormat: 'Y-m-d'}
			,'notes'
			,{name: 'fechaupd', type: 'date',dateFormat: 'Y-m-d'}
			,'afterstat'
			,'statusmonth'
			,{name: 'ultimafecha', type: 'date',dateFormat: 'Y-m-d'}
			,{name: 'ultimomonto', type: 'float'}
		],
		remoteSort: true
	});
	
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
            '-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
			},'-'/*, {
            pressed: false,
            enableToggle:false,
            text: 'Historico Full',
            handler: showHistorico

        }*/]
    });
////////////////FIN barra de pagineo//////////////////////
	
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");		
		ajax.send("parametro=status&year="+Ext.getCmp('year').getValue()+"&month="+Ext.getCmp('month').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}	

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}
	function searchPay(){
		if(Number(Ext.getCmp("dayfrom").getValue())>Number(Ext.getCmp("dayto").getValue())){
			Ext.MessageBox.alert('Warning','Dia desde no puede ser mayor al dia hasta');
		}else{
			store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
			store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
			store.load();
			storeTotal.load({params:{start:0, limit:200, dayfrom: Ext.getCmp("dayfrom").getValue(), dayto: Ext.getCmp("dayto").getValue()}});
		}	
	}
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }			
	function obtenerTotal(){
		var totales = storeTotal.getRange(0,storeTotal.getCount());
		var i;
		var acum=0;
		for(i=0;i<storeTotal.getCount();i++){
			if(totales[i].data.status=='freeze'){
				acum = acum + 15;
			}else if(totales[i].data.status=='realtorweb'){
				acum = acum + parseFloat(totales[i].data.pricerealtorweb);
			}else{ 
				acum = acum + parseFloat(totales[i].data.priceprod);
			}	
		}
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
;	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
	function checkUser(val,p,record){
		return String.format('<input type="checkbox" name="del'+val+'"  id="del'+val+'">',val);
	}
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" qtip="{1}"></a>',note,val);
		}	
	}
	
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: "Surname", width: 100, align: 'left', sortable: true, dataIndex: 'surname'}
			,{header: "User ID", width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: "Actual Status", width: 100, align: 'center', sortable: true, dataIndex: 'status'}
			,{header: 'Last Status Date', width: 100, sortable: true, align: 'left', dataIndex: 'ultimoupdate', renderer: Ext.util.Format.dateRenderer('Y-m-d')}
			,{header: 'Paydate', width: 80, sortable: true, align: 'left', dataIndex: 'paydate', renderer: Ext.util.Format.dateRenderer('Y-m-d')}
			,{header: "FrecuencyPay", width: 100, align: 'left', sortable: true, dataIndex: 'frecuency'}
			//,{header: 'Date Status', width: 80, sortable: true, align: 'left', dataIndex: 'fechaupd', renderer: Ext.util.Format.dateRenderer('Y-m-d')}
			,{header: 'Month Status', width: 100, sortable: false, align: 'center', dataIndex: 'statusmonth'}
			,{header: 'Month Pay', width: 80, sortable: true, align: 'left', dataIndex: 'ultimafecha', renderer: Ext.util.Format.dateRenderer('Y-m-d')}
			,{header: 'Month Amount', width: 100, sortable: true, align: 'right', dataIndex: 'ultimomonto', renderer : function(v){return Ext.util.Format.usMoney(v)}}
		],
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,//'99.8%',
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////
	
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Control Mes',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{xtype: 'compositefield',
					labelWidth: 5,
					fieldLabel: 'Select Month',
					items:[{
							width:100,
							id:'month',
							xtype:'combo',
							store: dataMonths,
							mode: 'local',
							valueField: 'id',
							displayField: 'name',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: mes,
							listeners: {
											 'select': function(){
													searchStatus();
												}	
										}
						},{
							width:20,
							xtype:'label',
							text: '',
							align: 'center'
						},{
							width:30,
							xtype:'label',
							text: 'Year',
							align: 'center'
						},{
							width:80,
							id:'year',
							xtype:'combo',
							store: dataYear,
							mode: 'local',
							valueField: 'id',
							displayField: 'id',
							triggerAction: 'all',            
							selectOnFocus: true,
							allowBlank: false,
							value: anio,
							listeners: {
											 'select': function(){
													searchStatus();
												}
										}
						}
					]}, 
				grid ]
	})	   
//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
/////////////Inicializar Grid////////////////////////
	store.setBaseParam('month',Ext.getCmp("month").getValue());
	store.setBaseParam('year',Ext.getCmp("year").getValue());	
	store.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), year: Ext.getCmp("year").getValue()}});
	/////////FIN Inicializar Grid////////////////////
	function searchStatus(){
		var mensaje = Ext.MessageBox.show({
			msg: 'Searching...',
			progressText: 'Searching...',
			width:300,
			wait:true,
			waitConfig: {interval:100},
			icon:'ext-mb-download',
			animEl: 'samplebutton'
			});
		store.setBaseParam('month',Ext.getCmp("month").getValue());
		store.setBaseParam('year',Ext.getCmp("year").getValue());	
		store.load({params:{start:0, limit: 200, month: Ext.getCmp("month").getValue(), year: Ext.getCmp("year").getValue()}});
		mensaje.hide();	
    }
});