var arrmail;
var fechaInicial;
var fechaFinal;
var nombre;
var apellido;
var dirbkoffice='';
var wherepage='affMarketingHistFull';//variable que dice de que pagina se esta ejecutando
var VFilas2;
var VHeader2;
var VFields2;
//added by Jesus 10-12-2013
function time_sleep_until (timestamp) {
  while (new Date() < timestamp * 1000) {}
  return true;
}
function nuevoAjax()
{ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	//var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	
//			
///////////FIN Cargas de data dinamica///////////////
	
	

//////////////////Manejo de Eventos//////////////////////

	var aStruct=[
		{"name":"name",				"bd":"xima","tabla":"ximausrs","title":"Name","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"surname",			"bd":"xima","tabla":"ximausrs","title":"Surname","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"userid",			"bd":"xima","tabla":"ximausrs","title":"UserID","type":"int","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"email",			"bd":"xima","tabla":"ximausrs","title":"Email","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"executive",		"bd":"xima","tabla":"ximausrs","title":"Acc. Executive","type":"string","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"country",			"bd":"xima","tabla":"ximausrs","title":"Country","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"state",			"bd":"xima","tabla":"ximausrs","title":"State","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"city",				"bd":"xima","tabla":"ximausrs","title":"City","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"address",			"bd":"xima","tabla":"ximausrs","title":"Address","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"hometelephone",	"bd":"xima","tabla":"ximausrs","title":"Home Tlf","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"mobiletelephone",	"bd":"xima","tabla":"ximausrs","title":"Mobile Tlf","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"licensen",			"bd":"xima","tabla":"ximausrs","title":"License #","type":"int","size":"10","show":"true","value":"","value2":"","condicional":""},
		//{"name":"brokern",			"bd":"xima","tabla":"ximausrs","title":"Broker #","type":"int","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"cardname",			"bd":"xima","tabla":"creditcard","title":"Card Type","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"cardnumber",		"bd":"xima","tabla":"creditcard","title":"Card #","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechacobro", 		"bd":"xima","tabla":"usr_cobros","title":"Pay Date","type":"date","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechacobroMonth", 	"bd":"xima","tabla":"usr_cobros","title":"Pay Date","type":"int","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"pseudonimo",		"bd":"xima","tabla":"ximausrs","title":"Pseudonimo","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		//{"name":"fechaupd", 		"bd":"xima","tabla":"logsstatus","title":"Logs Status (suspended)","type":"date","size":"10","show":"true","value":"","value2":"","condicional":""},	    
		{"name":"idstatus",			"bd":"xima","tabla":"usr_cobros","title":"Status","type":"status","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"idusertype",		"bd":"xima","tabla":"ximausrs","title":"UserType","type":"utype","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"accept",			"bd":"xima","tabla":"userterms","title":"Accept","type":"boolean","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"affiliationdate",	"bd":"xima","tabla":"ximausrs","title":"Sing Up","type":"date","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"officenum",		"bd":"xima","tabla":"ximausrs","title":"Office #","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"notes",			"bd":"xima","tabla":"usernotes","title":"Notes","type":"string","size":"35","show":"true","value":"","value2":"","condicional":""},
		//{"name":"paypal",			"bd":"xima","tabla":"cobros","title":"Paypal Error","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"platinum",			"bd":"xima","tabla":"permission","title":"Platinum","type":"platin","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"buyerspro",		"bd":"xima","tabla":"permission","title":"Buyerspro","type":"buy","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"leadsgenerator",	"bd":"xima","tabla":"permission","title":"Leads Generator","type":"leads","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"homefinder",		"bd":"xima","tabla":"permission","title":"Home Finder","type":"homef","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"realtorweb",		"bd":"xima","tabla":"permission","title":"Realtorweb","type":"realweb","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"investorweb",		"bd":"xima","tabla":"permission","title":"Investorweb","type":"invesweb","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"professional",		"bd":"xima","tabla":"permission","title":"Professional","type":"profes","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"professional_esp",	"bd":"xima","tabla":"permission","title":"Professional Breia","type":"esp","size":"20","show":"true","value":"","value2":"","condicional":""},
    	{"name":"residential",		"bd":"xima","tabla":"permission","title":"Residential","type":"profes","size":"20","show":"true","value":"","value2":"","condicional":""},
	    {"name":"idfrecuency",		"bd":"xima","tabla":"f_frecuency","title":"Frecuency","type":"frecuen","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"idrtyp", 			"bd":"xima","tabla":"usr_registertype","title":"RegisterType","type":"registertype","size":"20","show":"true","value":"","value2":"","condicional":""},		
		{"name":"suspenddate",	"bd":"xima","tabla":"logsstatus","title":"Suspend Date","type":"date","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"veceslog",			"bd":"xima","tabla":"ximausrs","title":"Logs 30 Days","type":"int","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechaupd",		"bd":"xima","tabla":"logsusr_cobros","title":"Logs Date Prod","type":"date","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"idproducto",		"bd":"xima","tabla":"usr_productobase","title":"Logs Products","type":"cbproduct","size":"","show":"true","value":"","value2":"","condicional":""}
		];
		
	function submitSearch(){
		//form.getForm().reset();
		var cbo_target,txt_target1,txt_target2,county;
		var _url='';
		
		for (i=0;i<aStruct.length;i++)
		{
				var combo= "com_"+aStruct[i].name;
				//alert(combo)
				var nombre= aStruct[i].name;
				var nombre2= aStruct[i].name+"_b";
				if(aStruct[i].type !== "boolean" && aStruct[i].type !== "status" && aStruct[i].type !== "utype" && aStruct[i].type !== "registertype" && aStruct[i].type !== "pcid" && aStruct[i].type !== "frecuen" && aStruct[i].type !== "cbproduct" && aStruct[i].type !== "leads"
				  && aStruct[i].type !== "platin" && aStruct[i].type !== "buy" && aStruct[i].type !== "homef" && aStruct[i].type !== "realweb" && aStruct[i].type !== "invesweb" && aStruct[i].type !== "profes" && aStruct[i].type !== "esp" && aStruct[i].type !== "resid")
				{ 
					//Para asignar o no a txt_ los elementos de input [Maria Oliveira]
					cbo_target=Ext.getCmp(combo.toString()).getValue();
					txt_target1=Ext.getCmp(nombre.toString()).getValue();
					if(aStruct[i].type=='date' || aStruct[i].type=='int'){
						txt_target2=Ext.getCmp(nombre2.toString()).getValue();
					}else{
						txt_target2='';
					}
					//capturar parametros de busqueda
					aStruct[i].value=txt_target1;
					aStruct[i].value2=txt_target2;
					aStruct[i].condicional=cbo_target;		
				}else
				{ 	
					txt_target1=Ext.getCmp(combo.toString()).getValue();
					if(txt_target1=='-Select-'){
						txt_target1='';
					}
					aStruct[i].value=txt_target1;
					aStruct[i].value2='';
					aStruct[i].condicional=txt_target1;//'Exact'
				}//endif
				//======
				if (cbo_target+''!="undefined")
				{	
					_url+=aStruct[i].name+"='"+aStruct[i].bd+"'$'"+aStruct[i].tabla+"'$'"+aStruct[i].condicional+"'$'"+aStruct[i].value+"'$'"+aStruct[i].value2+"'$'"+aStruct[i].type+"'$'"+aStruct[i].size+"'$'"+aStruct[i].show+"'$'"+aStruct[i].title+"'&";			
				}//endif
		}//endfor
		//form.getForm().reset();
		_url=_url.substring(0,_url.length-1); //quitar el & de sobra al final
		var ajax=nuevoAjax();
		ajax.open("POST", "php/atrapalo_x.php",true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send(_url);
		var mensaje = Ext.MessageBox.show({
			msg: 'Searching...',
			progressText: 'Searching...',
			width:300,
			wait:true,
			waitConfig: {interval:100},
			icon:'ext-mb-download',
			animEl: 'samplebutton'
			});	
		ajax.onreadystatechange=function() 
		{
			if (ajax.readyState==4) 
			{
				//alert(ajax.responseText);return;
				//document.getElementById('divRows').innerHTML=ajax.responseText;return;
				//mostrar resultados en esta capa
				mensaje.hide();
				var arr_res=ajax.responseText.split('^');
				//updateTab('gridsearch','Resultado','users2.php');
				SetGrid('searchUser');
				form.doLayout();
			}//readyState
		}//onreadystatechange	}
	}
	
	function submitSearchMentoring(){
		
		//form.getForm().reset();
		var cbo_target,txt_target1,txt_target2,county;
		var _url='';
		
		for (i=0;i<aStruct.length;i++)
		{
				var combo= "com_"+aStruct[i].name;
				//alert(combo)
				var nombre= aStruct[i].name;
				var nombre2= aStruct[i].name+"_b";
				if(aStruct[i].type !== "boolean" && aStruct[i].type !== "status" && aStruct[i].type !== "utype" && aStruct[i].type !== "registertype" && aStruct[i].type !== "pcid" && aStruct[i].type !== "frecuen" && aStruct[i].type !== "cbproduct" && aStruct[i].type !== "leads"
				  && aStruct[i].type !== "platin" && aStruct[i].type !== "buy" && aStruct[i].type !== "homef" && aStruct[i].type !== "realweb" && aStruct[i].type !== "invesweb" && aStruct[i].type !== "profes" && aStruct[i].type !== "esp" && aStruct[i].type !== "resid")
				{ 
					//Para asignar o no a txt_ los elementos de input [Maria Oliveira]
					cbo_target=Ext.getCmp(combo.toString()).getValue();
					txt_target1=Ext.getCmp(nombre.toString()).getValue();
					if(aStruct[i].type=='date' || aStruct[i].type=='int'){
						txt_target2=Ext.getCmp(nombre2.toString()).getValue();
					}else{
						txt_target2='';
					}
					//capturar parametros de busqueda
					aStruct[i].value=txt_target1;
					aStruct[i].value2=txt_target2;
					aStruct[i].condicional=cbo_target;		
				}else
				{ 	
					txt_target1=Ext.getCmp(combo.toString()).getValue();
					if(txt_target1=='-Select-'){
						txt_target1='';
					}
					aStruct[i].value=txt_target1;
					aStruct[i].value2='';
					aStruct[i].condicional=txt_target1;//'Exact'
				}//endif
				//======
				if (cbo_target+''!="undefined")
				{	
					_url+=aStruct[i].name+"='"+aStruct[i].bd+"'$'"+aStruct[i].tabla+"'$'"+aStruct[i].condicional+"'$'"+aStruct[i].value+"'$'"+aStruct[i].value2+"'$'"+aStruct[i].type+"'$'"+aStruct[i].size+"'$'"+aStruct[i].show+"'$'"+aStruct[i].title+"'&";			
				}//endif
		}//endfor
		//form.getForm().reset();
		_url=_url.substring(0,_url.length-1); //quitar el & de sobra al final
		var ajax=nuevoAjax();
		ajax.open("POST", "php/atrapalo_x.php",true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send(_url);
		var mensaje = Ext.MessageBox.show({
			msg: 'Searching...',
			progressText: 'Searching...',
			width:300,
			wait:true,
			waitConfig: {interval:100},
			icon:'ext-mb-download',
			animEl: 'samplebutton'
			});	
		ajax.onreadystatechange=function() 
		{
			if (ajax.readyState==4) 
			{
				//mostrar resultados en esta capa
				mensaje.hide();
				var arr_res=ajax.responseText.split('^');
				SetGrid('searchMentoring');
				//updateTab('gridsearchMentoring','Resultado 2','users3.php');
				form.doLayout();
			}//readyState
		}//onreadystatechange	
	}
	function cambiaCombo(combo, record, index){
		var name = combo.getId().split('_');
		//alert(name)
		var txt1 = name[1];
		var txt2= name[1]+'_b';
			
		if(combo.getValue()=='Between'){
			Ext.getCmp(txt2.toString()).setVisible(true);
		}else{
			Ext.getCmp(txt2.toString()).setVisible(false);
		}
		form.doLayout();	
	}
	
	function cambiaComboUserID(combo, record, index){
		var name = combo.getId().split('_');
		//alert(name)
		var txt1 = name[1];
		var txt2= name[1]+'_b';
			
		if(combo.getValue()=='Multiple'){
			Ext.getCmp(txt1.toString()).setWidth(350);
			Ext.getCmp(txt2.toString()).setVisible(false);
		}else if(combo.getValue()=='Between'){
			Ext.getCmp(txt1.toString()).setWidth(100);
			Ext.getCmp(txt2.toString()).setVisible(true);
		}else{
			Ext.getCmp(txt1.toString()).setWidth(100);
			Ext.getCmp(txt2.toString()).setVisible(false);
		}
		form.doLayout();	
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Form//////////////////////////////////
   

	var dataString = new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [
		  ['Exact'],
		  ['Start With'],
		  ['Contains'],
		  ['Not Exact'],
		  ['Not Start With']
		]
	});
	var dataInt=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between']
		]
	});
	var dataIntUser=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between'],
		  ['Multiple']
		]
	});
	var dataDate=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between']
		]
	});
	var dataBoolean=new Ext.data.SimpleStore({
		fields: ['id','text'],
		data  : [
		  ['-1','-Select-'],
		  ['Y','Yes'],
		  ['N','No']
		]
	});
	var dataPcid=new Ext.data.SimpleStore({
		fields: ['id'],
		data  : [
		  ['-Select-'],
		  ['CFRI'],
		  ['ASO2'],
		  ['GENERAL'],
		  ['SD']
		]
	});

	var dataPlatin=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [99,'No']/*,
		  [2,'Por preguntar'],
		  [9,'No, confirmado']*/
		]
	});
	var dataBuy=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});
	var dataRealweb=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});
	var dataInvesweb=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});
	var dataProfes=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});
	
	var dataFrecuen=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Monthly'],
		  [2,'Annually']
		]
	});
	
	var dataEsp=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});	      
	var dataResid=new Ext.data.SimpleStore({
		fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
	});
	
	var form = new Ext.FormPanel({
		closable:false,
        frame:true,
        title: 'Search Users',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
		 items: [{
            layout:'column',
            items:[{
                columnWidth:.5,
                layout: 'form',
                items: [{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Name',
					items:[{
						width:120,
						id:'com_name',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'name'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'User ID',
					items:[{
						width:120,
						id: 'com_userid',
						xtype:'combo',
						store: dataIntUser,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaComboUserID
							 }
					},{
						width:100,
						xtype:'textfield',
						id: 'userid',
						enableKeyEvents:true,
						listeners:{
							keypress:function(button,e){
								var temp=button.getValue();
								temp=temp.split(' ').join(',');
								//temp=temp.replace(/\s/g, ",");
								button.setValue(temp);
							},
							keyup:function(button,e){
								var temp=button.getValue();
								temp=temp.split(' ').join(',');
								//temp=temp.replace(/\s/g, ",");
								button.setValue(temp);
							},
							blur:function(button){
								var temp=button.getValue();
								temp=temp.split(' ').join(',');
								//temp=temp.replace(/\s/g, ",");
								button.setValue(temp);
							},
							focus:function(button){
								var temp=button.getValue();
								temp=temp.split(' ').join(',');
								//temp=temp.replace(/\s/g, ",");
								button.setValue(temp);
							}
						}
					},{
						width:100,
						xtype:'textfield',
						id: 'userid_b'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Acc. Executive',
					items:[{
						width: 120,
						id:'com_executive',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width: 120,
						xtype:'textfield',
						id: 'executive'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'State',
					items:[{
						width:120,
						id:'com_state',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact',
						editable: false
					},{
						width: 120,
						xtype:'textfield',
						id: 'state'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Address',
					items:[{
						width: 120,
						id: 'com_address',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width: 120,
						xtype:'textfield',
						id: 'address'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Mobile Tlf',
					items:[{
						width: 120,
						id:'com_mobiletelephone',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width: 120,
						xtype:'textfield',
						id: 'mobiletelephone'
					}]
                }/*,{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Broker #',
					items:[{
						width: 120,
						id:'com_brokern',
						xtype:'combo',
						store: dataInt,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaCombo
							 }
					},{
						width: 100,
						xtype:'textfield',
						id: 'brokern'
					},{
						width: 100,
						xtype:'textfield',
						id: 'brokern_b'
					}]
                }*/,{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'NickName',
					items:[{
						width:120,
						id:'com_pseudonimo',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'pseudonimo'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Sing up',
					items:[{
						width:120,
						id:'com_affiliationdate',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'affiliationdate',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'affiliationdate_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(YYYYMMDD)'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Notes',
					items:[{
						width:120,
						id:'com_notes',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'notes'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Residential',
					items:[{
						width:120,
						id:'com_residential',
						xtype:'combo',
						store: dataResid,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Platinum',
					items:[{
						width:120,
						id:'com_platinum',
						xtype:'combo',
						store: dataPlatin,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Professional',
					items:[{
						width:120,
						id:'com_professional',
						xtype:'combo',
						store: dataProfes,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Mentoring',
					items:[{
						width:120,
						id:'com_professional_esp',
						xtype:'combo',
						store: dataEsp,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Buyerspro',
					items:[{
						width:120,
						id:'com_buyerspro',
						xtype:'combo',
						store: dataBuy,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Leads Generator',
					items:[{
						width:120,
						id:'com_leadsgenerator',
						xtype:'combo',
						store: dataResid,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Home Finder',
					items:[{
						width:120,
						id:'com_homefinder',
						xtype:'combo',
						store: dataResid,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Logs 30 Days',
					items:[{
						width:120,
						id: 'com_veceslog',
						xtype:'combo',
						store: dataInt,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:100,
						xtype:'textfield',
						id: 'veceslog'
					},{
						width:100,
						xtype:'textfield',
						id: 'veceslog_b'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Logs Date Prod',
					items:[{
						width:120,
						id:'com_fechaupd',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'fechaupd',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'fechaupd_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(YYYYMMDD)'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Logs Products',
					items:[{
						width:120,
						id:'com_idproducto',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['idproducto', 'prod'],
							data : Ext.combos_selec.dataProducts
						 }),
						editable: false,
						valueField: 'idproducto',
						displayField: 'prod',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }]
            },{
                columnWidth:.5,
                layout: 'form',
                items: [{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Surname',
					items:[{
						width:120,
						id:'com_surname',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'surname'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Email',
					items:[{
						width:120,
						id: 'com_email',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact',
						anchor:'95%'
					},{
						width:120,
						xtype:'textfield',
						id: 'email'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Country',
					items:[{
						width:120,
						id:'com_country',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'country',
						anchor:'95%'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'City',
					items:[{
						width:120,
						id:'com_city',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'city'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Home Tlf',
					items:[{
						width:120,
						id: 'com_hometelephone',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'hometelephone'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'License #',
					items:[{
						width:120,
						id:'com_licensen',
						xtype:'combo',
						store: dataInt,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
						listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:100,
						xtype:'textfield',
						id: 'licensen'
					},{
						width:100,
						xtype:'textfield',
						id: 'licensen_b'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Office #',
					items:[{
						width:120,
						id: 'com_officenum',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'officenum',
						anchor:'95%'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Card Type',
					items:[{
						width:120,
						id:'com_cardname',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'cardname',
						anchor:'95%'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Card #',
					items:[{
						width:120,
						id: 'com_cardnumber',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
						
					},{
						columnWidth: 100,
						xtype:'textfield',
						id: 'cardnumber'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Pay Date',
					items:[{
						width:120,
						id:'com_fechacobro',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
			      			listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'fechacobro',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'fechacobro_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(YYYYMMDD)'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Pay Date Month',
					items:[{
						width:120,
						id:'com_fechacobroMonth',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
			      			listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'fechacobroMonth',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'fechacobroMonth_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(MMDD)'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Status',
					items:[{
						width:120,
						id:'com_idstatus',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['idstatus', 'status'],
							data : Ext.combos_selec.dataStatus
						 }),
						editable: false,
						valueField: 'idstatus',
						displayField: 'status',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Suspend Date',
					items:[{
						width:120,
						id:'com_suspenddate',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
			      			listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'suspenddate',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'suspenddate_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(YYYYMMDD)'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'User Type',
					items:[{
						width:120,
						id: 'com_idusertype',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['idusertype', 'usertype'],
							data : Ext.combos_selec.dataUType
						 }),
						editable: false,
						valueField: 'idusertype',
						displayField: 'usertype',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Accept',
					items:[{
						width:120,
						id:'com_accept',
						xtype:'combo',
						store: dataBoolean,
						editable: false,
						valueField: 'id',
						displayField: 'text',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }/*,{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Paypal Error',
					items:[{
						width:120,
						id:'com_paypal',
						xtype:'combo',
						store: dataString,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Exact'
					},{
						width:120,
						xtype:'textfield',
						id: 'paypal'
					}]
                }*/	
				,{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Realtor Web',
					items:[{
						width:120,
						id:'com_realtorweb',
						xtype:'combo',
						store: dataRealweb,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Investor Web',
					items:[{
						width:120,
						id:'com_investorweb',
						xtype:'combo',
						store: dataInvesweb,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Register Type',
					items:[{
						width:120,
						id: 'com_idrtyp',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['idrtype', 'rtype'],
							data : Ext.combos_selec.dataRegisterType
						 }),
						editable: false,
						valueField: 'idrtype',
						displayField: 'rtype',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: ''
					}]
                },{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Frecuency',
					items:[{
						width:120,
						id:'com_idfrecuency',
						xtype:'combo',
						store: dataFrecuen,
						editable: false,
						valueField: 'id',
						displayField: 'texto',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }/*, {
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Logs Status (suspended)',
					items:[{
						width:120,
						id:'com_fechaupd',
						xtype:'combo',
						store: dataDate,
						editable: false,
						valueField: 'id',
						displayField: 'id',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: 'Equal',
			      			listeners: {
							 'select': cambiaCombo
							 }
					},{
						width:80,
						xtype:'textfield',
						id: 'fechaupd',
						anchor:'95%'
					},{
						width:80,
						xtype:'textfield',
						id: 'fechaupd_b',
						anchor:'95%'
					},{
						width:50,
						xtype:'label',
						text: '(YYYYMMDD)'
					}]
                }*/
                ]
            }]
        }],
		waitTitle   :'Please wait...',
		waitMsg     :'Sending data...',
		buttonAlign: 'center',
        buttons: [{
            text: 'Search',
			handler: submitSearch
	  },{
            text: 'Search Mentoring',
			handler: submitSearchMentoring
        },{
            text: 'Reset',
			handler: function(){
					form.getForm().reset();
					Ext.getCmp('userid').setWidth(100);
				}
        }],
		listeners : {
			afterlayout: {
			  fn: function(p) {
				Ext.getCmp('userid_b').setVisible(false);
				//Ext.getCmp('brokern_b').setVisible(false);
				Ext.getCmp('licensen_b').setVisible(false);
				Ext.getCmp('affiliationdate_b').setVisible(false);
				Ext.getCmp('fechacobro_b').setVisible(false);
				Ext.getCmp('fechacobroMonth_b').setVisible(false);
				Ext.getCmp('veceslog_b').setVisible(false);
				Ext.getCmp('fechaupd_b').setVisible(false);
				Ext.getCmp('suspenddate_b').setVisible(false);
			  },
			  single: true
			},
			activate:function(tabpanel){
				form.doLayout();
			}
		}

    });
	var tabPanel = new Ext.TabPanel({  
		border: false,
		id:'unicoPrincipalTabPanel',
		activeTab: 0,
		closable:true,
		enableTabScroll:true,
		height: Ext.getBody().getViewSize().height-25,
		items:[form]  
	 });
	 	
	
	function SetGrid(g){
		switch(g){
			case 'searchUser':
				tabPanel.setActiveTab(tabPanel.add(grid));
				store.load();
				pagingBar.bind(store);
			break;
			case 'searchMentoring':
				if(!document.getElementById(g)){
					Ext.getCmp('unicoPrincipalTabPanel').add(gridMen).show();
				}else{
					//var tab = tabPanel.getItem(g);
					//tabPanel.remove(g);
				}
				//pagingBarMen.bind(storeMen);
				//store.load();
				//pagingBar.bind(store);
			break;
			
		}

	}
	
	var store = new Ext.data.JsonStore({
		idProperty: 'userid',
		remoteSort : true,
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=users',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=users', timeout: 3600000 }),
		//baseParams  :{start:0, limit:100},
		reader: new Ext.data.JsonReader(),						
		fields: [
			{name: 'id', type: 'int'},
			{name: 'userid', type: 'int'},
			'name',
			'surname',
			'email',
			'status',
			'privilege',
			'usertype',
			'accept',
			'executive',
			'affiliation',
			'fechaupd',
			'officenum',
			'procode',
			'notes',
			'pricefull',
			'pricecommercial',
			'platinum',
			'buyerspro',
			'leadsgenerator',
			'homefinder',
			'realtorweb',
			'investorweb',
			'nombres',
			'PASS',
			'blacklist',
			'idfrec',
			'freedays',
			'marcalogin',
			'totalprice',
			{name: 'veceslog', type: 'int'},
			'senddateemail'
		]
	});
	 

	
	var panelpag = new Ext.form.FormPanel({
		width: 415,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			fieldLabel: 'Template Email',
			items:		
			[/*{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				disabled: true, 
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				xtype: 'label',
				text: 'Template Email',
				width: 100 
			},*/{
				width:300,
				fieldLabel:'Template Email',
				name:'cbTemplateEmailGrid',
				id:'cbTemplateEmailGrid',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: true,
				editable: false,
				emptyText : 'Select to view see email template',
				listeners: {
					'select': searchTemplatEmail
				} 
			}]
		}]
	});

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 50,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Displaying {0} - {1} of {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
            pressed: true,
            enableToggle:false,
            text: '<b>Send Email</b>',
            handler: enviaEmail
        },{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
		},'-',
			panelpag
		]
    });
	

	
////////////////FIN barra de pagineo//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);
		
	}

	function obtenerSeleccionadosNameEmail(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+='"'+selec[i].data.name+' '+ selec[i].data.surname+'" <'+selec[i].data.email+'>';
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function fillEmail(){
		//store.setBaseParam('idtmail',);
		//store.load({params:{start:0, limit:100}});
		Ext.getCmp("subject").setValue('Loading..');
		Ext.getCmp("bodyemail").setValue('Loading..');
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			waitTitle   :'Please wait!',
			waitMsg     :'Loading...',
			params: { 
				tipo : 'previewEmails',
				idtmail: Ext.getCmp("cbTemplateEmail").getValue()
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);
				Ext.getCmp("subject").setValue(variable.subject);
				Ext.getCmp("bodyemail").setValue(variable.body);
				storeVolumens.setBaseParam('idtmail', Ext.getCmp("cbTemplateEmail").getValue());
				storeVolumens.load(/*{params:{start:0, limit:100}}*/);
			}
		})
		
	}

	function sendTo(){
		if(Ext.getCmp("rbsendtoAll").getValue()==true)
		{
			Ext.getCmp("toids").setValue('.');
			Ext.getCmp("toids").setVisible(false);
		
		}
		if(Ext.getCmp("rbsendtoCustom").getValue()==true)
		{
			var aux=Ext.getCmp("toid").getValue();
			if(aux==false || aux=='false')aux='';
			Ext.getCmp("toids").setValue(aux);
			Ext.getCmp("toids").setVisible(true);
		
		}
	}

	var storeVolumens ='';
	function enviaEmail(){
	
		deleteVolumen=function  (val)  		
		{
			Ext.Ajax.request({  
					waitTitle: 'Please wait..',
					waitMsg: 'Sending data...',
					url: 'php/grid_del.php', 
					method: 'POST', 
					params: { 
						tipo: "fileslist", 
						ID: val
					},
					success:function(response,options){
						storeVolumens.reload();
						var d = Ext.util.JSON.decode(response.responseText);
						clickaddfile=1;
						//Ext.MessageBox.alert('Success',d.errors.reason);
					},
					failure:function(response,options){
						var d = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert('Failure', d.errors.reason);
					}
			});		
		}//function deleteVolumen(btn)			

		///////////Data store Volumens///////////////
		storeVolumens = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'results',
			url: 'php/grid_data.php?&tipo=fileslist',
			fields: [
				{name: 'idatt', type: 'int'}
				,'namefile'	
				,'sizefile'	
				,'pathsaved'	
			],
			remoteSort: true
		});

		var ToolbarVolumens = new Ext.Toolbar({
			items  : [{
				id: 'add_button9',
				tooltip: 'Click to Refresh Attachments Grid',
				iconCls:'icon',
				icon: 'images/view-refresh.png',
				text: 'Refresh',
				handler:function(){storeVolumens.reload();}
			}]			
		});
		
		function renderOperationFile(val, p, record){
			var arraux=record.data.pathsaved.split('.');
			var ext=arraux[arraux.length-1].toLowerCase();
			var textcad='';
			//var textcad='<a href="'+record.data.pathsaved+'" target="_blank"><img src="images/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
			//if(ext=='flv' || ext=='mp4' || ext=='gif' || ext=='jpeg' || ext=='jpg' || ext=='png' || ext=='bmp' || ext=='tif')
			//	textcad='<a href="javascript:void(0)" onClick="previewfile(\''+record.data.pathsaved+'\',\''+ext+'\')"><img src="images/view.gif" border="0" title="Click to view file _'+ext+'_ "></a>';
				
			var retorno= String.format(	'<a href="javascript:void(0)" onClick="deleteVolumen({0})" ><img src="images/delete.gif" border="0" title="Click to delete file"></a>&nbsp;'+
										textcad,val);
										
			return retorno;
		}
		
		var GridVolumens = new Ext.grid.EditorGridPanel({
			store: storeVolumens,
			tbar: ToolbarVolumens, 
			columns: [	
				 new Ext.grid.RowNumberer()
				,{header: 'File Name',		width: 300, sortable: true, align: 'left', dataIndex: 'namefile'}
				,{header: 'File Size',	width: 120, sortable: true, align: 'right', dataIndex: 'sizefile'}
				,{header: " ", 			width: 50, align: 'center', sortable: true, dataIndex: 'idatt',renderer: renderOperationFile}						
				
			],
			clicksToEdit:2,
			sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
			height:120,
			width: '99.8%',
			frame:true,
			loadMask:true
		});		
        var uploadForm = new Ext.FormPanel({
			fileUpload  :true,
			width: '99.8%',
            frame       :true,
            autoheight  :true,
            labelWidth  : 60,
            defaults    :{
				anchor  : '95%',
                msgTarget: 'side'
			},
            items       :[{
					layout:'column',
					items:[{
						columnWidth:.85,
						layout: 'form',
						items: [{
							xtype       :'fileuploadfield',
							name        : 'upfile',
							id          : 'form-file1',
							emptyText   : 'Select a file',
							fieldLabel  :'File',
							allowBlank: true,
							width: 480,
							buttonCfg   :{
								text    :'',
								cls: 'x-btn-text-icon',			
								icon: 'images/saveas.png'
							}
						}]
					},{
						columnWidth:.15,
						layout: 'form',
						items: [{
							xtype: 'button',
							text:'<b>Add File</b>',
							cls:'x-btn-text-icon',
							icon:'images/add.png',
							width: 95,
							height: 25,
							handler:function(){
								//alert('estamos en eso');return;
								var tempmail=Ext.getCmp("cbTemplateEmail").getValue();
								if(tempmail==0 || tempmail=='' ){Ext.Msg.alert("Warning","Please select a Template Email");	return;}	
								if(uploadForm.getForm().isValid()){
									if(Ext.getCmp('form-file1').getValue()==''){Ext.Msg.alert("Warning","Please select a File");	return;}	
									clickaddfile=1;						
									uploadForm.getForm().submit({
										url     :'includes/uploadExtjs/phpupload.php',
										waitMsg :'Uploading...',
										params:{
											'idtmail': tempmail
										},
										success :function(form,action){
											//Ext.Msg.alert("Succes",action.response.responseText);								
											//uploadForm.getForm().reset();
											Ext.getCmp('form-file1').setValue('');
											storeVolumens.setBaseParam('idtmail',tempmail);
											storeVolumens.load(/*{params:{start:0, limit:100}}*/);
										},
										failure :function(form,action){
											obj = Ext.util.JSON.decode(action.response.responseText);
											Ext.Msg.alert("Failure", obj.errors.reason);
											//Ext.Msg.alert("Failure",action.response.responseText);		
										}
									});
								}
							}
						}]
					}]
			}]
		});
	
		var obtenidos=selectionPaging.showSelectedRows();
		
		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype: 'textarea',
				fieldLabel:'Toid',
				id:'toids',
				name:'toids',
				value: obtenidos,
				allowBlank: false,
				readOnly:true,
				width:620,
				height:60  
			}/*,{
				xtype: 'hidden',
				id: 'toid',
				name: 'toid',
				value: obtenidos
			}*/,{
				width:640,
				fieldLabel:'Template',
				name:'cbTemplateEmail',
				id:'cbTemplateEmail',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				hiddenName: 'cbidtmail',
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				editable: false,
				//value: activews,
				listeners: {
					'select': fillEmail
				} 
			},{
				xtype:'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'subject',
				width:640,
				allowBlank: false  
			},{
				xtype: 'htmleditor',
				id: 'bodyemail',
				id: 'bodyemail',
				hideLabel: true,
				height: 190,
				width:700,
				allowBlank: false  
			},{
				xtype: 'fieldset',
				title: 'Attachments',
				autoHeight: true,
				items: [uploadForm
				,GridVolumens
				]
			}],
			buttonAlign: 'center',
			buttons: [{
				text: 'Update Template',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
				formBind: true,
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
					if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/grid_edit.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Loading...',
							params: {
								tipo : 'templateEmails',
								idtmail:  Ext.getCmp("cbTemplateEmail").getValue()										
							},	
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								Ext.Msg.alert('Success', 'Update succesfully');
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},{
				text: '<b>Send Email</b>',
				cls: 'x-btn-text-icon',			
				icon: 'images/sendemail.jpg',				
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/sendingEmail.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Sending email...',
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								selectionPaging.clearSelections();
								w.close();
								store.reload();
								//Ext.Msg.alert('Success', action.result.msg);
								
								var variable = Ext.decode(action.response.responseText);
								var resulthtml=	'<table  border="0" cellpadding="0" cellspacing="0" style=" font-size:8pt">'+
												'	  <tr bgcolor="#FFFFCC" align="center">'+
												'		<td width="40px">&nbsp;</td>'+
												'		<td width="70px"><b>Userid</b></td>'+
												'		<td width="150px"><b>Name</b></td>'+
												'		<td width="180px"><b>Email</b></td>'+
												'		<td width="60px"><b>Send</b></td>'+
												'		<td width="320px"><b>Msg</b></td>'+
												'	  </tr>'
												;
								
								for (var i=0;i<variable.data.length;i++)
								{
									
									
									resulthtml+= '	<tr bgcolor="'+((i%2)==0?'#ffffff':'#fefee9')+'" >'+
												'		<td align="center">'+(i+1)+'</td>'+
												'		<td align="center">'+variable.data[i]['userid']+'</td>'+
												'		<td>'+variable.data[i]['toname']+'</td>'+
												'		<td>'+variable.data[i]['toemail']+'</td>'+
												'		<td align="center">'+(variable.data[i]['sending']==1?'<img src="images/check.png" border="0" />':'<img src="images/cross.gif" border="0" />')+'</td>'+
												'		<td>'+(variable.data[i]['errorsending'].length>0?variable.data[i]['errorsending']:'&nbsp;')+'</td>'+
												'	</tr>'
											;
  
								}
								resulthtml+='</table>';
								
								
								var w1 = new Ext.Window({
									title: 'Response Email', 
									width: 750,
									height: 500,
									closable :true,
									modal: true,
									layout: 'fit',
									autoScroll : true,
									plain: true,
									bodyStyle: 'padding:5px;',
									html: resulthtml
								});
								w1.show();								
							},
							failure: function(form, action) {
								selectionPaging.clearSelections();
								store.reload();
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},
				'->'
			,{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'Send Email', 
			width: 750,
			height: 580,//550
			layout: 'fit',
			plain: true,
			modal: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
	}
	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.load();
	}

	function searchTemplatEmail(){
		store.setBaseParam('idtmail',Ext.getCmp("cbTemplateEmailGrid").getValue());
		store.load(/*{params:{start:0, limit:100}}*/);
	}

		
	function mostrarEmail()//Muestra lista de los email que se enviaron
	{
		var htmlTag="";
		var i;
		htmlTag+='<table cellspacing="0" class="tblEmail"><tbody>';
		htmlTag+='<tr class="subtitulo"><th width="235">Sending Email</th><th width="15"> <a href="#" onClick="javascript:visibleDeudor(\'WinEmail\',\'ocultar\'); return false;"><div align="right"><img alt="Close Window" src="includes/img/cerrar.png" border="0"></div></a></th></tr>';
		htmlTag+='<tr class="color1"><td colspan="2"><br>MESSAGE SENT: '+subject+' <br>To the following Users: <br>';
		for(i=0;i<arrmail.length;i++)
		{
			htmlTag+=arrmail[i].email+'<br>';
		}
		htmlTag+='</td></tr>';
		htmlTag+='<tr class="color1"><td colspan="2" ><div align="center"><INPUT class="botonEmail" id="botAceptaEmail" type="button" value="OK" onclick="javascript:visibleDeudor(\'WinEmail\',\'ocultar\'); return false;" ></div></td></tr>';
		htmlTag+='<tr><td></td></tr>';
		htmlTag+='</tbody></table>';
	//	alert(htmlTag);
		var html=htmlTag.join('');
		var winEmail=new Ext.Window({
						 title: 'Email',
						 width: 500,
						 height: 450,
					 	 autoScroll: true,
						 resizable: false,
						 modal: true,
						 border:false,
						 plain: false,
						 html: html
				});
		winEmail.show();
	}
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("parametro=users&adicional=1");
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		var ID =	oGrid_Event.record.data.userid;
		campo=oGrid_Event.field;
		info=fieldValue;
		type='text';
		if(fieldValue=='BEGIN')
		{Ext.MessageBox.alert('Warning','Error select BEGIN');store.reload();return;}
		var ajax=nuevoAjax();
		ajax.open("POST", "respuesta_users.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("campo="+campo+"&info="+info+"&type="+type+"&oper=modificar"+"&ID="+ID);
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{  
				store.commitChanges();
				store.reload();
			}
		}
	}; 
//////////////////Manejo de Eventos//////////////////////
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	function obtenerTotal(){
		//Ext.MessageBox.alert('Warningsmith',store.data.fullids);

		/*Ext.getCmp('total').setText('Loading...');//'Total Amount: '+(acum).toFixed(2));	
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			params: { 
				tipo : 'users',
				only:'getTotalPrice'
			},
			failure:function(response,options){
				//Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);
				Ext.getCmp('total').setText(variable.totalprice);//'Total Amount: '+(acum).toFixed(2));	
			}
		})
		*/
/*
		var totales = store.getRange(0,store.getCount());
		//var i;
		var acum=0;
		//for(i=0;i<store.getCount();i++)
		//acum = acum + parseFloat(totales[0].data.totalprice);
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
*/
	}	
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function checkCommercial(val,p,record){
		if(val>0){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function checkRealtor(val,p,record){
		if(val==1){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function checkInvestor(val,p,record){
		if(val==1){
			return 'Yes';
		}else{
			return 'No';
		}
		
	}
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{0}">',val);
		}	
	}

 
    function changeColor(val, p, record){
        if(record.data.blacklist == 'Y')
		{
            return '<span style="color:red;font-weight: bold;">' + val + '</span>';
        }
        if(record.data.idfrec == 2)
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
        return val;
    }
	
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
  var myView = new Ext.grid.GridView();
   myView.getRowClass = function(record, index, rowParams, store) {
		
        if(record.data['blacklist'] == 'Y')return 'rowRed';
        if(record.data['idfrec'] == 2)return 'rowGreen';
		if(record.data['marcalogin'] == 'Y')return 'orange-row';
   };
	var selectionPaging = new Ext.ux.grid.RowSelectionPaging();
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		view: myView,
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{				header: ' ', 			width: 30, sortable: true, align: 'center', dataIndex: 'notes', renderer: comment}
			,{				header: 'Name', 		width: 90, sortable: true, align: 'left', dataIndex: 'name',renderer: changeColor}//,editor: new Ext.form.TextField({allowBlank: false})
			,{				header: 'Last Name', 	width: 90, align: 'left', sortable: true, dataIndex: 'surname',renderer: changeColor}
			,{id:'userid',	header: 'User ID', 		width: 45, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{				header: 'Email', 		width: 160, align: 'left', sortable: true, dataIndex: 'email',editor: new Ext.form.TextField()}
			,{				header: 'Password', 	width: 100, align: 'left', sortable: true, dataIndex: 'PASS',editor: new Ext.form.TextField()}			
			,{				header: 'Status', 		width: 75, align: 'left', sortable: true, dataIndex: 'status'}
			,{				header: 'Price', 		width: 70, align: 'right', sortable: true, dataIndex: 'pricefull', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{				header: '30 logs', width: 50, sortable: true, align: 'center', tooltip: 'Last 30 logs', dataIndex: 'veceslog'}
			//,{				header: 'User Type', 	width:100, sortable: true, align: 'left', dataIndex: 'usertype'}
			//,{				header: 'Accept', 		width:50, sortable: true, align: 'center', dataIndex: 'accept'}
			,{				header: 'Executive', 	width:150, sortable: true, align: 'left', dataIndex: 'nombres',hidden :true}
			,{				header: 'Sing Up', 		width: 70, sortable: true, align: 'left', dataIndex: 'affiliation'}
			,{				header: 'Trial', 	width: 35, sortable: true, align: 'center', dataIndex: 'freedays'}
			,{				header: "Send Date Email", width: 115, align: 'left', sortable: true, dataIndex: 'senddateemail'}
			,{				header: "Suspended Date", width: 115, align: 'left', sortable: true, dataIndex: 'fechaupd'}
			/*,{				header: 'Product Price', width: 95, sortable: true, align: 'right', dataIndex: 'preci0'}
			,{				header: 'PayDate', width: 95, sortable: true, align: 'center', dataIndex: 'fechacobro'}*/
		],
		clicksToEdit:2,
		sm: mySelectionModel,
        plugins: [selectionPaging],
		height:470,
		frame:true,
		title:"Search Results",
		loadMask:true,
		tbar: pagingBar
		
	});	
/////////////////FIN Grid////////////////////////////

/////////////////Grid-Mentoring/////////////////////////
var storeScrowLetter = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=users-contract-scrowLetter',  
	root: 'data',  
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'},
		{name:'logo', type: 'string'},
		{name:'desc', type: 'string'}
	]
});
var storeContracts = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=users-contract-contracts',  
	root: 'data',  
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var storeGrouping = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=grouping-contract',  
	root: 'data',  
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var storeAddons = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=users-contract-addons',  
	root: 'data',  
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var storeTemplates = new Ext.data.JsonStore({  
	url:'/custom_contract/includes/php/functions.php',  
	root: 'results',
	baseParams:{
		idfunction:'6',
		userCustom:'3803'
	},
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var storeTemplatesAdvanceSearch = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=users-template-advance-search',  
	root: 'data',
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var storeTemplatesAdvanceResult = new Ext.data.JsonStore({  
	url:'php/grid_data.php?tipo=users-template-advance-result',  
	root: 'data',
	autoLoad:true,
	totalProperty: 'total',  
	fields: [  
		{name:'id', type: 'string'},  
		{name:'name', type: 'string'}
	]
});
var BK_data = {
	standar:{
		contract:0,
		addons:{}
	},
	custom:{
		contract:0,
		addons:{}
	},
	users:{},
	dataToSend:{
		contracts:{},
		tplAdvanceSearch:"",
		tplAdvanceResult:"",
		scrowLetter:{
			header:"",
			footer:""
		}
	}
};
var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
/*prueba*/
Array.prototype.sum = function(field){
	if(field)
		for(var i=0,sum=0;i<this.length;sum+=parseFloat(this[i++].data[field]));
	else
		for(var i=0,sum=0;i<this.length;sum+=this[i++]);
	return sum;
} 
	var gridMen = {
		closable:true,
		title:'Search Result Mentoring',
		autoScroll:true,
		tbar: [{
			text:'Contract Uploads',
			handler:function(){
				var usersGroupId = new Array();
				Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
					var aux=item.id.split("-");
					if(Ext.getCmp(item.id).getValue()){
						usersGroupId.push({
							userid:aux[1],
							groups:Ext.getCmp(item.id).groups
						});
					}
				});
			 	if(usersGroupId.length<=0)
					Ext.MessageBox.alert('Warning', 'Dosen\'t have selected nothing.');
				else{
					var simple = new Ext.TabPanel({
						activeTab: 0,
						autoScroll:true,
						title: 'New Tutorial',
						id:'principlaControlTab',
						items:[{
							title:"Contracts",
							items:[{
								xtype:'panel',
								autoScroll:true,
								height:Ext.getBody().getViewSize().height-125,
								id:'tabStandar'
							}]
						},{
							title:"Buyer 1 Info",
							items:[{
								xtype:'panel',
								autoScroll:true,
								height:Ext.getBody().getViewSize().height-125,
								id:'tabBuyer'
							}]
						},{
							title:"ScrowLetter",
							items:[{
								xtype:'panel',
								id:'tabScrow',
								items:[{
									xtype:'form',
									style:'padding:10 15',
									border:false,
									items:[{
										xtype:'combo',
										id:'comboHeader',
										store:storeScrowLetter,
										valueField: 'id',
										typeAhead: true,
										width:450,
										forceSelection: true,
										mode: 'local',
										value:'0000',
										itemSelector: 'div.search-item',
										tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="background-image:url({logo})"><div class="name">{name}</div><div class="desc">{desc}</div></div></tpl>'),
										triggerAction: 'all',
										editable:false,
										displayField: 'name',
										fieldLabel:'Header'
									}]
								},{
									xtype:'form',
									style:'padding:10 15',
									border:false,
									items:[{
										xtype:'combo',
										id:'comboFooter',
										store:storeScrowLetter,
										valueField: 'id',
										typeAhead: true,
										width:450,
										forceSelection: true,
										mode: 'local',
										value:'0000',
										itemSelector: 'div.search-item',
										tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="background-image:url({logo})"><div class="name">{name}</div><div class="desc">{desc}</div></div></tpl>'),
										triggerAction: 'all',
										editable:false,
										displayField: 'name',
										fieldLabel:'Footer'
									}]
								}]
							}]
						},{
							title:"Advence Seach",
							items:[{
								xtype:'panel',
								id:'tabAdvanceSearch',
								items:[{
									xtype:'form',
									bodyStyle: 'padding-left: 20px; padding-top: 10px;',
									items:[{
							            xtype: 'superboxselect',
										fieldLabel: 'Templates',
										emptyText: 'Select Template(s)', 
							            resizable: true,
							            name: 'user_attr[]',
										id:'tplAdvanceSearch',
							            store: storeTemplatesAdvanceSearch,
							            mode: 'local',
										width:410,
							            displayField: 'name',
							            valueField: 'id',
							            queryDelay: 0,
							            triggerAction: 'all',
							            forceSelection: true,
							            forceFormValue: true,
							            allowBlank: true,
										listeners:{
											additem:function(combo, value){
												if(value=="0000"){
													combo.removeAllItems();
													var temp="";var cont=0;
													Ext.each(combo.getStore().data.items, function(tempItems, index) {
														if(tempItems.id!="0000"){
															temp += (cont!=0?",":"")+tempItems.id;
															cont=cont+1;
														}
													});
													combo.setValue(temp);
												}
											}
										}
							        }]
								}]
							}]
						},{
							title:"Advence Result",
							items:[{
								xtype:'panel',
								id:'tabAdvanceResult',
								items:[{
									xtype:'form',
									bodyStyle: 'padding-left: 20px; padding-top: 10px;',
									items:[{
							            xtype: 'superboxselect',
										fieldLabel: 'Templates',
										emptyText: 'Select Template(s)', 
							            resizable: true,
							            name: 'user_attr[]',
										id:'tplAdvanceResult',
							            store: storeTemplatesAdvanceResult,
							            mode: 'local',
										width:410,
							            displayField: 'name',
							            valueField: 'id',
							            queryDelay: 0,
							            triggerAction: 'all',
							            forceSelection: true,
							            forceFormValue: true,
							            allowBlank: true,
										listeners:{
											additem:function(combo, value){
												if(value=="0000"){
													combo.removeAllItems();
													var temp="";var cont=0;
													Ext.each(combo.getStore().data.items, function(tempItems, index) {
														if(tempItems.id!="0000"){
															temp += (cont!=0?",":"")+tempItems.id;
															cont=cont+1;
														}
													});
													combo.setValue(temp);
												}
											}
										}
							        }]
								}]
							}]
						},{
							title:"Signatures",
							items:[{
								xtype:'panel',
								height: 475,
								autoScroll:true,
								id:'tabSignatures',
								items:[{
									xtype:'form',
									labelWidth: 150,
									border: false,
									id:'signatures-formPrincipal',
									
								}]
							}]
						}]
					});
					var PanelJ = new Ext.Panel({
						layout      : 'fit',
						width       : 770,
						height      : 550,
						title		: 'Upload Contracts',
						resizable	: false,
						modal	 	: true,
						autoScroll	: true,
						plain       : true,
						usersGroupId:usersGroupId,
						items		: simple,
						tbar		: {
							items: [{
								text:'Grouping',
								menu: new Ext.menu.Menu({
									items: [
										new Ext.menu.Item({
											text: 'Save as new Group',
											listeners:	{
												click: function(click,e){
													Ext.MessageBox.prompt(
														'Asign Name', 
														'Please enter name to save this configuration:', 
														function(btn,text){
															if(btn=="ok"){
																myMask.show();
																BK_data.dataToSend	=	{
																	contracts:{},
																	tplAdvanceSearch:"",
																	tplAdvanceResult:"",
																	scrowLetter:{
																		header:"",
																		footer:""
																	}
																}
																Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
																	var contractN		=	index;
																	var fileName		=	Ext.getCmp(item.id).contractFileName;
																	var contractId		=	Ext.getCmp(item.id).contractId;
																	var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
																	var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
																	var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
																	BK_data.dataToSend.contracts[contractN]={
																		'fileName'		:	fileName+'.pdf',
																		'realName'		:	realName,
																		'template'		:	template,
																		'inspectionDays':	inspectionDays,
																		'addons':{}
																	}
																	Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
																		var addonN			=	index;
																		var addonFileName	=	Ext.getCmp(item.id).addonFileName;
																		var addonContractId	=	Ext.getCmp(item.id).addonId;
																		var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
																		var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
																		BK_data.dataToSend.contracts[contractN].addons[addonN]={
																			'addonFileName':addonFileName+'.pdf',
																			'addonRealName':addonRealName,
																			'addonTemplate':addonTemplate
																		}
																	});
																});
																Ext.getCmp('principlaControlTab').setActiveTab(1);
																Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
																	BK_data.users[Ext.getCmp(item.id).userId]={
																		buyer	:	Ext.getCmp('buyerName-'+Ext.getCmp(item.id).userId).getValue(),
																		address1:	Ext.getCmp('address1-'+Ext.getCmp(item.id).userId).getValue(),
																		address2:	Ext.getCmp('address2-'+Ext.getCmp(item.id).userId).getValue(),
																		address3:	Ext.getCmp('address3-'+Ext.getCmp(item.id).userId).getValue()
																	}
																});
																Ext.getCmp('principlaControlTab').setActiveTab(2);
																BK_data.dataToSend.scrowLetter.header=Ext.getCmp('comboHeader').getValue();
																BK_data.dataToSend.scrowLetter.footer=Ext.getCmp('comboFooter').getValue();
																Ext.getCmp('principlaControlTab').setActiveTab(3);
																BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
																Ext.getCmp('principlaControlTab').setActiveTab(4);
																BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
																Ext.Ajax.request({
																	url: 'php/grid_data.php',
																	method: 'POST',
																	timeout:0,
																	success: function(response, request) {
																		myMask.hide();
																		var jsonData = Ext.util.JSON.decode(response.responseText);
																		if(jsonData.success){
																			Ext.Msg.show({
																				title:'Info',
																				msg: jsonData.msg,
																				icon: Ext.MessageBox.INFO,
																				buttons: Ext.MessageBox.OK,
																			});
																		}else{
																			Ext.Msg.show({
																				title:'Info',
																				msg: jsonData.msg,
																				icon: Ext.MessageBox.WARNING,
																				buttons: Ext.MessageBox.OK,
																			});
																		}
																	},
																	failure: function(response, request) {
																		myMask.hide();
																		var errMessage = 'Error en la petición ' + request.url + 
																						' status ' + response.status + 
																						' statusText ' + response.statusText + 
																						' responseText ' + response.responseText;
																					   
																		alert(errMessage);
																	},
																	params: {
																		tipo:	'save-contract-grouping',
																		data:	Ext.util.JSON.encode(BK_data.dataToSend),
																		name:	text
																	}
																});
															}
														}
													);
												}
											}
										}),
										new Ext.menu.Item({
											text: 'Load a Group',
											listeners:	{
												click: function(click,e){
													storeGrouping.reload();
													var miventana = new Ext.Window({
														title: 'Select grouping to be loaded',
														width: 364,
														height: 100,
														maximizable: false,
														modal: true,
														resizable: false,
														maximized: false,
														constrain: true,
														items:[{
															xtype:'combo',
															id:'combo-load-grouping',
															store:storeGrouping,
															valueField: 'id',
															typeAhead: true,
															width:350,
															forceSelection: true,
															mode: 'local',
															value:'0000',
															triggerAction: 'all',
															selectOnFocus: true,
															displayField: 'name',
															editable: false,
															fieldLabel:'Basic Additionals'
														}],
														buttons: [{
															text     : 'Load',
															handler  : function(){
																if(Ext.getCmp('combo-load-grouping').getValue()!="0000"){
																	myMask.show();
																	Ext.Ajax.request({
																		url: 'php/grid_data.php',
																		method: 'POST',
																		timeout:0,
																		success: function(response, request) {
																			myMask.hide();
																			miventana.close();
																			var jsonData = Ext.util.JSON.decode(response.responseText);
																			Ext.getCmp('principlaControlTab').setActiveTab(0);
																			//
																			Ext.each(Ext.query('*[id^=contract-delete]'),function(item,index){
																				if(typeof Ext.getCmp(item.id) != "undefined"){
																					Ext.getCmp(item.id).fireEvent('click');
																				}
																			});
																			//
																			var comboContracts	=	Ext.getCmp('contracsStableCombo');
																			for(var c in jsonData.contracts){
																				comboContracts.setValue(jsonData.contracts[c].fileName);
																				var indexC	=	comboContracts.getStore().find(
																					'id',
																					jsonData.contracts[c].fileName
																				);;
																				comboContracts.fireEvent('select',comboContracts,comboContracts.store.getAt(indexC));
																				time_sleep_until(1000);
																				//**************Begin Asign Contract Template
																				var comboContractTpl	=	Ext.getCmp('contract-FinalTemplate-'+BK_data.standar.contract);
																				comboContractTpl.setValue(jsonData.contracts[c].template);
																				var indexCT	=	comboContractTpl.getStore().find(
																					'id',
																					jsonData.contracts[c].template
																				);;
																				comboContractTpl.fireEvent('select',comboContractTpl,comboContractTpl.store.getAt(indexCT));
																				//**************End Asign Contract Template
																				//**************Asign Contract RealName
																				Ext.getCmp('contract-FinalName-'+BK_data.standar.contract).setValue(jsonData.contracts[c].realName);
																				//**************Asign Contract Inspection Days
																				if(typeof jsonData.contracts[c].inspectionDays != "undefined")
																					Ext.getCmp('contract-inspectionDays-'+BK_data.standar.contract).setValue(jsonData.contracts[c].inspectionDays);
																				for(var a in jsonData.contracts[c].addons){
																					time_sleep_until(1000);
																					var comboAddon	=	Ext.getCmp('comboAddonsAdded-'+BK_data.standar.contract);
																					comboAddon.setValue(jsonData.contracts[c].addons[a].addonFileName);
																					var indexA	=	comboAddon.getStore().find(
																						'id',
																						jsonData.contracts[c].addons[a].addonFileName
																					);;
																					comboAddon.fireEvent('select',comboAddon,comboAddon.store.getAt(indexA));
																					time_sleep_until(1000);
																					//**************Begin Asign Addon Template
																					var comboAddonTpl	=	Ext.getCmp('addon-FinalTemplate-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]);
																					comboAddonTpl.setValue(jsonData.contracts[c].addons[a].addonTemplate);
																					var indexAT	=	comboAddonTpl.getStore().find(
																						'id',
																						jsonData.contracts[c].addons[a].addonTemplate
																					);;
																					comboAddonTpl.fireEvent('select',comboAddonTpl,comboAddonTpl.store.getAt(indexAT));
																					//**************End Asign Addon Template
																					//**************Asign Addon RealName
																					Ext.getCmp('addon-FinalName-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]).setValue(jsonData.contracts[c].addons[a].addonRealName);
																				
																				}
																			}
																			time_sleep_until(1000);
																			Ext.getCmp('principlaControlTab').setActiveTab(2);
																			Ext.getCmp('comboHeader').setValue(jsonData.scrowLetter.header);
																			Ext.getCmp('comboFooter').setValue(jsonData.scrowLetter.footer);
																			time_sleep_until(1000);
																			Ext.getCmp('principlaControlTab').setActiveTab(3);
																			Ext.getCmp('tplAdvanceSearch').setValue(jsonData.tplAdvanceSearch);
																			time_sleep_until(1000);
																			Ext.getCmp('principlaControlTab').setActiveTab(4);
																			Ext.getCmp('tplAdvanceResult').setValue(jsonData.tplAdvanceResult);
																			time_sleep_until(1000);
																			Ext.getCmp('principlaControlTab').setActiveTab(0);
																		},
																		failure: function(response, request) {
																			myMask.hide();
																			var errMessage = 'Error en la petición ' + request.url + 
																							' status ' + response.status + 
																							' statusText ' + response.statusText + 
																							' responseText ' + response.responseText;
																						   
																			alert(errMessage);
																		},
																		params: {
																			tipo:	'load-contract-grouping',
																			id:		Ext.getCmp('combo-load-grouping').getValue()
																		}
																	});
																}else{
																	Ext.MessageBox.alert('Warning','Please, select a grouping template.');
																}
															}
														},{
															text     : 'Close',
															handler  : function(){
																miventana.close();
															}
														}]
													}).show();
												}
											}
										}),
										new Ext.menu.Item({
											text: 'Update Existent Group',
											listeners:	{
												click: function(click,e){
													storeGrouping.reload();
													var miventana = new Ext.Window({
														title: 'Select grouping to be updated',
														width: 364,
														height: 120,
														maximizable: false,
														modal: true,
														resizable: false,
														maximized: false,
														constrain: true,
														items:[{
															xtype:'combo',
															id:'combo-load-grouping',
															store:storeGrouping,
															valueField: 'id',
															typeAhead: true,
															width:350,
															forceSelection: true,
															mode: 'local',
															value:'0000',
															triggerAction: 'all',
															selectOnFocus: true,
															displayField: 'name',
															editable: false,
															fieldLabel:'Basic Additionals',
															listeners:{
																select:function(combo){
																	if(combo.getValue()!="0000"){
																		Ext.getCmp('combo-load-grouping-text').setValue(combo.getRawValue());
																	}
																}
															}
														},{
															xtype:	'textfield',
															width:	350,
															id:		'combo-load-grouping-text',
														}],
														buttons: [{
															text     : 'Update',
															handler  : function(){
																if(Ext.getCmp('combo-load-grouping').getValue()!="0000"){
																	myMask.show();
																	BK_data.dataToSend	=	{
																		contracts:{},
																		tplAdvanceSearch:"",
																		tplAdvanceResult:"",
																		scrowLetter:{
																			header:"",
																			footer:""
																		}
																	}
																	Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
																		var contractN		=	index;
																		var fileName		=	Ext.getCmp(item.id).contractFileName;
																		var contractId		=	Ext.getCmp(item.id).contractId;
																		var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
																		var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
																		var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
																		BK_data.dataToSend.contracts[contractN]={
																			'fileName'		:	fileName+'.pdf',
																			'realName'		:	realName,
																			'template'		:	template,
																			'inspectionDays':	inspectionDays,
																			'addons':{}
																		}
																		Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
																			var addonN			=	index;
																			var addonFileName	=	Ext.getCmp(item.id).addonFileName;
																			var addonContractId	=	Ext.getCmp(item.id).addonId;
																			var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
																			var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
																			BK_data.dataToSend.contracts[contractN].addons[addonN]={
																				'addonFileName':addonFileName+'.pdf',
																				'addonRealName':addonRealName,
																				'addonTemplate':addonTemplate
																			}
																		});
																	});
																	Ext.getCmp('principlaControlTab').setActiveTab(1);
																	Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
																		BK_data.users[Ext.getCmp(item.id).userId]={
																			buyer	:	Ext.getCmp('buyerName-'+Ext.getCmp(item.id).userId).getValue(),
																			address1:	Ext.getCmp('address1-'+Ext.getCmp(item.id).userId).getValue(),
																			address2:	Ext.getCmp('address2-'+Ext.getCmp(item.id).userId).getValue(),
																			address3:	Ext.getCmp('address3-'+Ext.getCmp(item.id).userId).getValue()
																		}
																	});
																	Ext.getCmp('principlaControlTab').setActiveTab(2);
																	BK_data.dataToSend.scrowLetter.header=Ext.getCmp('comboHeader').getValue();
																	BK_data.dataToSend.scrowLetter.footer=Ext.getCmp('comboFooter').getValue();
																	Ext.getCmp('principlaControlTab').setActiveTab(3);
																	BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
																	Ext.getCmp('principlaControlTab').setActiveTab(4);
																	BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
																	Ext.Ajax.request({
																		url: 'php/grid_data.php',
																		method: 'POST',
																		timeout:0,
																		success: function(response, request) {
																			myMask.hide();
																			var jsonData = Ext.util.JSON.decode(response.responseText);
																			if(jsonData.success){
																				Ext.Msg.show({
																					title:'Info',
																					msg: jsonData.msg,
																					icon: Ext.MessageBox.INFO,
																					buttons: Ext.MessageBox.OK,
																				});
																			}else{
																				Ext.Msg.show({
																					title:'Info',
																					msg: jsonData.msg,
																					icon: Ext.MessageBox.WARNING,
																					buttons: Ext.MessageBox.OK,
																				});
																			}
																		},
																		failure: function(response, request) {
																			myMask.hide();
																			var errMessage = 'Error en la petición ' + request.url + 
																							' status ' + response.status + 
																							' statusText ' + response.statusText + 
																							' responseText ' + response.responseText;
																						   
																			alert(errMessage);
																		},
																		params: {
																			tipo:	'update-contract-grouping',
																			data:	Ext.util.JSON.encode(BK_data.dataToSend),
																			name:	Ext.getCmp('combo-load-grouping-text').getValue(),
																			id:		Ext.getCmp('combo-load-grouping').getValue()
																		}
																	});
																	miventana.close();
																}else{
																	Ext.MessageBox.alert('Warning','Please, select a grouping template.');
																}
															}
														},{
															text     : 'Close',
															handler  : function(){
																miventana.close();
															}
														}]
													}).show();
												}
											}
										})
									]
								})  
							}]
						},
						listeners	:	{
							afterrender:function(){
								Ext.each(this.usersGroupId, function(tempUsers, index) {
									var allInfoUser	=Ext.getCmp('gridFiles-'+tempUsers.userid).allInfoUser;
									var temp = [{
										xtype:'fieldset',
										id:'contract-'+tempUsers.userid,
										userId:tempUsers.userid,
										height: 150,
										title:'User: '+tempUsers.groups,
										collapsible	:	true,
										autoScroll  :	true,
										collapsed	:	false,
										items:[{
											xtype:'textfield',
											id:'buyerName-'+tempUsers.userid,
											width:300,
											fieldLabel:'Buyer Name',
											value:allInfoUser.name.trim().toUpperCase()+" "+allInfoUser.surname.trim().toUpperCase()
										},{
											xtype:'textfield',
											id:'address1-'+tempUsers.userid,
											width:300,
											fieldLabel:'Addres Line 1',
											value:allInfoUser.address.trim().toUpperCase()
										},{
											xtype:'textfield',
											id:'address2-'+tempUsers.userid,
											width:300,
											fieldLabel:'Addres Line 2',
											value:allInfoUser.city.trim().toUpperCase()+", "+allInfoUser.state.trim()+" "+allInfoUser.zipcode.trim()
										},{
											xtype:'textfield',
											id:'address3-'+tempUsers.userid,
											width:300,
											fieldLabel:'Addres Line 3'
										}]
									}]
									Ext.getCmp('tabBuyer').add(temp);
									Ext.getCmp('tabBuyer').doLayout();
								});
								Ext.each(this.usersGroupId, function(tempUsers, index) {
									var temp = [{
										xtype		:	'fieldset',
										soon		:	'signatures-'+tempUsers.userid,
										userId		:	tempUsers.userid,
										title		:	'User: '+tempUsers.groups,
										height      :	150,
										collapsible	:	true,
										autoScroll  :	true,
										collapsed	:	true,
										items:[{
											xtype       : 'fileuploadfield',
											id          : 'signature-buyer1-sig-'+tempUsers.userid,
											fieldLabel	: 'Buyer Signature',
											emptyText   : 'Upload Image',
											name        : 'signature-buyer1-sig-'+tempUsers.userid,
											width       : 350,
											buttonText  : 'Browse...'
										},{
											xtype       : 'fileuploadfield',
											id          : 'signature-buyer1-ini-'+tempUsers.userid,
											fieldLabel	: 'Buyer Initial',
											emptyText   : 'Upload Image',
											name        : 'signature-buyer1-ini-'+tempUsers.userid,
											width       : 350,
											buttonText  : 'Browse...'
										},{
											xtype       : 'fileuploadfield',
											id          : 'signature-buyer2-sig-'+tempUsers.userid,
											fieldLabel	: 'Buyer Second Signature',
											emptyText   : 'Upload Image',
											name        : 'signature-buyer2-sig-'+tempUsers.userid,
											width       : 350,
											buttonText  : 'Browse...'
										},{
											xtype       : 'fileuploadfield',
											id          : 'signature-buyer2-ini-'+tempUsers.userid,
											fieldLabel	: 'Buyer Second Initial',
											emptyText   : 'Upload Image',
											name        : 'signature-buyer2-ini-'+tempUsers.userid,
											width       : 350,
											buttonText  : 'Browse...'
										}],
										listeners:{
											expand:function(panel){
												//console.debug(typeof window.eliminarFirma);
												//Ext.getCmp(panel.soon).load({
												//	url: '/mysetting_tabs/mycontracts_tabs/mysignatures.php', 
												//	scripts: true,
												//	params:{userBackOffice:panel.userId}
												//});
											}
										}
									}]
									Ext.getCmp('signatures-formPrincipal').add(temp);
									Ext.getCmp('tabSignatures').doLayout();
								});
								var temp = [{
									xtype:'fieldset',
									id:'contract-standar',
									//height:500,
									collapsible	:	false,
									//autoScroll  :	true,
									collapsed	:	false,
									items:[{
										xtype:'combo',
										store:storeContracts,
										id:'contracsStableCombo',
										valueField: 'id',
										typeAhead: true,
										width:350,
										forceSelection: true,
										mode: 'local',
										value:'0000',
										triggerAction: 'all',
										selectOnFocus: true,
										displayField: 'name',
										editable: false,
										fieldLabel:'Basic Contracts',
										father:'contract-standar',
										listeners:{
											select:function( combo, newValue, oldValue ){
												if(newValue.data.id!='0000'){
													BK_data.standar.contract	=	BK_data.standar.contract+1;
													var temp1=[{
														xtype:'fieldset',
														layout:'column',
														collapsible	:	true,
														contractFileName:newValue.data.name,
														title:'Contract: '+newValue.data.name,
														id:'contract-standar-'+BK_data.standar.contract,
														contractId:BK_data.standar.contract,
														items:[{
															layout: 'form',
															border:false,
															columnWidth:1,
															cls:'padding: 0 0 10px;',
															items:[{
																layout:'column',
																border:false,
																items:[{
																	layout: 'form',
																	border:false,
																	columnWidth:.45,
																	items:[{
																		xtype:'textfield',
																		//columnWidth:.9,
																		id:'contract-FinalName-'+BK_data.standar.contract,
																		width:300,
																		fieldLabel:'Contract Name',
																		value:newValue.data.name
																	}]
																},{
																	layout: 'form',
																	border:false,
																	columnWidth:.45,
																	items:[{
																		xtype:'combo',
																		//columnWidth:.5,
																		store:storeTemplates,
																		valueField: 'id',
																		id:'contract-FinalTemplate-'+BK_data.standar.contract,
																		typeAhead: true,
																		width:300,
																		forceSelection: true,
																		mode: 'local',
																		value:'0000',
																		triggerAction: 'all',
																		selectOnFocus: true,
																		displayField: 'name',
																		editable: false,
																		fieldLabel:'Asign Template',
																		listeners:{
																			select:function( combo, newValue, oldValue ){
																				
																			}
																		}
																	}]
																},{
																	layout: 'form',
																	border:false,
																	columnWidth:.10,
																	items:[{
																		xtype:'button',
																		text: 'Delete',
																		id:'contract-delete-'+BK_data.standar.contract,
																		father:'contract-standar-'+BK_data.standar.contract,
																		tooltip:'Delete this contract with all addons inside',
																	    listeners: {
																			click:function(){
																				myMask.show();
																				Ext.getCmp(this.father).destroy();
																				myMask.hide();
																			}
																	    }
																	}]
																},{
																	layout		:	'form',
																	border		:	false,
																	columnWidth	:	.45,
																	items		:	[{
																		xtype			:	'numberfield',
																		allowDecimals	:	false,
																		allowNegative	:	false,
																		id				:	'contract-inspectionDays-'+BK_data.standar.contract,
																		width			:	50,
																		fieldLabel		:	'Inspection Days',
																		value			:	15
																	}]
																}]
															}]
														},{
															layout: 'form',
															border:false,
															columnWidth:1,
															items:[{
																xtype:'combo',
																store:storeAddons,
																valueField: 'id',
																typeAhead: true,
																width:350,
																forceSelection: true,
																mode: 'local',
																value:'0000',
																triggerAction: 'all',
																selectOnFocus: true,
																displayField: 'name',
																editable: false,
																fieldLabel:'Basic Aditionals',
																id:'comboAddonsAdded-'+BK_data.standar.contract,
																father:'contract-standar-'+BK_data.standar.contract,
																fatherId:BK_data.standar.contract,
																listeners:{
																	select:function( combo, newValue, oldValue ){
																		BK_data.standar.addons[combo.fatherId]	=	typeof BK_data.standar.addons[combo.fatherId] == "undefined" ? 1 : BK_data.standar.addons[combo.fatherId]+1;
																		if(newValue.data.id!='0000'){
																			if(BK_data.standar.addons[combo.fatherId]<=6){
																				var temp1=[{
																					xtype:'fieldset',
																					id:'addons-standar-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																					style: 'margin-left:30px',
																					columnWidth:1,
																					addonFileName:newValue.data.name,
																					addonId:BK_data.standar.addons[combo.fatherId],
																					cls:'margin-left:20px',
																					layout:'column',
																					title:'Addon: '+newValue.data.name,
																					items:[{
																						layout: 'form',
																						border:false,
																						columnWidth:.45,
																						items:[{
																							xtype:'textfield',
																							width:500,
																							id:'addon-FinalName-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																							fieldLabel:'Addon Name',
																							value:newValue.data.name
																						}]
																					},{
																						layout: 'form',
																						border:false,
																						columnWidth:.45,
																						items:[{
																							xtype:'combo',
																							columnWidth:.5,
																							store:storeTemplates,
																							valueField: 'id',
																							typeAhead: true,
																							width:350,
																							id:'addon-FinalTemplate-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																							forceSelection: true,
																							mode: 'local',
																							value:'0000',
																							triggerAction: 'all',
																							selectOnFocus: true,
																							displayField: 'name',
																							editable: false,
																							fieldLabel:'Asign Template',
																							listeners:{
																								select:function( combo, newValue, oldValue ){
																									
																								}
																							}
																						}]
																					},{
																						layout: 'form',
																						border:false,
																						columnWidth:.10,
																						items:[{
																							xtype:'button',
																							text: 'Delete',
																							father:'addons-standar-'+BK_data.standar.addons[combo.fatherId],
																							fatherID:combo.fatherId,
																							tooltip:'Delete this addon',
																						    handler: function(){
																								BK_data.standar.addons[this.fatherId]=BK_data.standar.addons[this.fatherId]-1;
																						        Ext.getCmp(this.father).destroy();
																						    }
																						}]
																					}]
																				}]
																				Ext.getCmp(combo.father).add(temp1);
																				Ext.getCmp('contract-standar').doLayout();
																				combo.setValue('0000');
																			}else{
																				BK_data.standar.addons[combo.fatherId]=BK_data.standar.addons[combo.fatherId]-1;
																				Ext.MessageBox.alert('Warning', 'Only can be added 6 aditionals documents.');
																			}
																		}
																	}
																}
															}]
														}]
													}]
													Ext.getCmp(combo.father).add(temp1);
													Ext.getCmp(combo.father).doLayout();
													combo.setValue('0000');
												}
											}
										}
									}],
									listeners:{
										beforerender:	function(){
											myMask.show();
											setTimeout(function(){
												myMask.hide();
											},1000)
										}
									}
								}]
								Ext.getCmp('tabStandar').add(temp);
								Ext.getCmp('tabStandar').doLayout();
							}
						},
						buttons:[{
							text:'Process all Tabs',
							handler:function(){
								Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
									var contractN		=	index;
									var fileName		=	Ext.getCmp(item.id).contractFileName;
									var contractId		=	Ext.getCmp(item.id).contractId;
									var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
									var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
									var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
									BK_data.dataToSend.contracts[contractN]={
										'fileName'		:	fileName+'.pdf',
										'realName'		:	realName,
										'template'		:	template,
										'inspectionDays':	inspectionDays,
										'addons':{}
									}
									Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
										var addonN			=	index;
										var addonFileName	=	Ext.getCmp(item.id).addonFileName;
										var addonContractId	=	Ext.getCmp(item.id).addonId;
										var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
										var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
										BK_data.dataToSend.contracts[contractN].addons[addonN]={
											'addonFileName':addonFileName+'.pdf',
											'addonRealName':addonRealName,
											'addonTemplate':addonTemplate
										}
									});
								});
								Ext.getCmp('principlaControlTab').setActiveTab(1);
								Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
									BK_data.users[Ext.getCmp(item.id).userId]={
										buyer		:	Ext.getCmp('buyerName-'+Ext.getCmp(item.id).userId).getValue(),
										address1	:	Ext.getCmp('address1-'+Ext.getCmp(item.id).userId).getValue(),
										address2	:	Ext.getCmp('address2-'+Ext.getCmp(item.id).userId).getValue(),
										address3	:	Ext.getCmp('address3-'+Ext.getCmp(item.id).userId).getValue()
									}
								});
								Ext.getCmp('principlaControlTab').setActiveTab(2);
								BK_data.dataToSend.scrowLetter.header=Ext.getCmp('comboHeader').getValue();
								BK_data.dataToSend.scrowLetter.footer=Ext.getCmp('comboFooter').getValue();
								Ext.getCmp('principlaControlTab').setActiveTab(3);
								BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
								Ext.getCmp('principlaControlTab').setActiveTab(4);
								BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
								Ext.getCmp('principlaControlTab').setActiveTab(5);
								Ext.Ajax.request({
						            url: 'php/grid_data.php',
						            method: 'POST',
									//jsonData: BK_data.dataToSend,
									timeout:0,
									isUpload: true,
									headers: {'Content-type':'multipart/form-data'},
									form: Ext.getCmp('signatures-formPrincipal').getForm().getEl().dom, 
						            success: function(response, request) {
								    	var jsonData = Ext.util.JSON.decode(response.responseText);
										console.debug(jsonData);
										Ext.Msg.show({
											title:'Info',
											msg:'Result: Successfully',
											icon: Ext.MessageBox.INFO,
											buttons: Ext.MessageBox.OK,
										});
								    },
						            failure: function(response, request) {
								    	var errMessage = 'Error en la petición ' + request.url + 
								                        ' status ' + response.status + 
								                        ' statusText ' + response.statusText + 
								                        ' responseText ' + response.responseText;
								                       
								        alert(errMessage);
								    },
						            params: {
										tipo:	'uploadNewsContracts',
										data:	Ext.util.JSON.encode(BK_data.dataToSend),
										user:	Ext.util.JSON.encode(BK_data.users)
									}
						        });
							}
						}]
					});
					tabPanel.setActiveTab(tabPanel.add(PanelJ));
				}
			}
		},{
			text:'Replace additional documents',
			menu: new Ext.menu.Menu({
		        items: [{
		            text: 'Replacement by name',
					handler:function(){
						Ext.MessageBox.alert('Warning','UnderConstruction By Jesus.');
						return true;
						var usersGroupId = new Array();
						Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
							var aux=item.id.split("-");
							usersGroupId.push({
								userid:aux[1],
								groups:Ext.getCmp(item.id).groups
							});
						});
						if(usersGroupId.length>0){
							var miventana = new Ext.Window({
								title: 'Replace Additional Document(s) by Name',
								width: 364,
								height: 180,
								maximizable: false,
								modal: true,
								resizable: false,
								maximized: false,
								constrain: true,
								items:[{
									xtype:'textfield',
									id:'addons-name-to-search',
									width:350,
									emptyText:'Name to change'
								},{
									xtype:'combo',
									id:'replace-combo-original-files',
									store:storeAddons,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Basic Additionals'
								},{
									xtype:'combo',
									id:'replace-combo-original-template',
									store:storeTemplates,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Assign Template'
								}],
								buttons: [{
				                    text     : 'Submit',
									handler  : function(){
										if(Ext.getCmp('replace-combo-original-files').getValue()!="0000"){
											Ext.Msg.show({
										        title:'Replace All Additional Document Previously Selected',
										        msg:'Are you sure?',
												icon: Ext.MessageBox.WARNING,
										        buttons: Ext.MessageBox.YESNO,
												fn:function(btn){
													if (btn == 'yes'){
														myMask.show();
								                        Ext.Ajax.request({
												            url: 'php/grid_data.php',
												            method: 'POST',
															timeout:0,
												            success: function(response, request) {
																myMask.hide();
														    	var jsonData = Ext.util.JSON.decode(response.responseText);
														        if(jsonData.success){
																	Ext.Msg.show({
																        title:'Info',
																        msg:'Replace additional documents: Successfully',
																		icon: Ext.MessageBox.INFO,
																        buttons: Ext.MessageBox.OK,
																	});
																}else{
															        Ext.Msg.show({
																        title:'Info',
																        msg:'Replace additional documents: Error',
																		icon: Ext.MessageBox.WARNING,
																        buttons: Ext.MessageBox.OK,
																	});
																}
														    },
												            failure: function(response, request) {
																myMask.hide();
														    	var errMessage = 'Error en la petición ' + request.url + 
														                        ' status ' + response.status + 
														                        ' statusText ' + response.statusText + 
														                        ' responseText ' + response.responseText;
														        alert(errMessage);
														    },
												            params: {
																tipo:	'replace-addons-by-name',
																data:	Ext.util.JSON.encode(additionalData),
																name:	Ext.getCmp('addons-name-to-search').getValue(),
																file:	Ext.getCmp('replace-combo-original-files').getValue(),
																tpl :	Ext.getCmp('replace-combo-original-template').getValue()
															}
												        });
													}
												}
											});
										}else{
											Ext.MessageBox.alert('Warning','Please, select an additional document.');
										}
				                    }
				                },{
				                    text     : 'Close',
				                    handler  : function(){
				                        miventana.close();
				                    }
				                }]
							}).show();
						}else{
							Ext.MessageBox.alert('Warning','Please, select an additional document from some user.');
						}
					}
		        },{
		            text: 'Replacement by previously selected',
					handler:function(){
						var usersGroupId = new Array();
						var additionalData = new Array();
						Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
							var aux=item.id.split("-");
							usersGroupId.push({
								userid:aux[1],
								groups:Ext.getCmp(item.id).groups
							});
						});
						Ext.each(usersGroupId, function(tempUsers, indexUser) {
							Ext.each(Ext.getCmp('gridFiles-'+tempUsers.userid).getSelectionModel().getSelections(), function(tempRows, indexRows) {
								if(tempRows.data.addon_name!="Doesn't have Addons")
									additionalData.push(tempRows.data);
							});
						});
				        if(additionalData.length>0){
							var miventana = new Ext.Window({
								title: 'Replace Additional Document(s)',
								width: 364,
								height: 140,
								maximizable: false,
								modal: true,
								resizable: false,
								maximized: false,
								constrain: true,
								items:[{
									xtype:'combo',
									id:'replace-combo-original-files',
									store:storeAddons,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Basic Additionals'
								},{
									xtype:'combo',
									id:'replace-combo-original-template',
									store:storeTemplates,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Assign Template'
								}],
								buttons: [{
				                    text     : 'Submit',
									handler  : function(){
										if(Ext.getCmp('replace-combo-original-files').getValue()!="0000"){
											Ext.Msg.show({
										        title:'Replace All Additional Document Previously Selected',
										        msg:'Are you sure?',
												icon: Ext.MessageBox.WARNING,
										        buttons: Ext.MessageBox.YESNO,
												fn:function(btn){
													if (btn == 'yes'){
														Ext.MessageBox.show({
														   msg: 'Processing, please wait...',
														   progressText: 'Saving...',
														   width:300,
														   wait:true,
														   waitConfig: {interval:200},
														   icon:'ext-mb-download' //custom class in msg-box.html
														});
								                        Ext.Ajax.request({
															waitMsg: 'Processing...',
												            url: 'php/grid_data.php',
												            method: 'POST',
															timeout:0,
												            success: function(response, request) {
																Ext.MessageBox.hide();
														    	var jsonData=	Ext.util.JSON.decode(response.responseText);
																var result	=	jsonData.result;
														        if(jsonData.success){
																	miventana.close();
																	if(result.length<=0){
																		Ext.Msg.show({
																			title:'Info',
																			msg:'Replace additional documents: Successfully',
																			icon: Ext.MessageBox.INFO,
																			buttons: Ext.MessageBox.OK,
																		});
																	}else{
																		var info="";
																		$.each(result,function(index,data){
																			var temp	=	'Userid: '+index+' '+data[0].userName+'<br>';
																			$.each(data,function(i,detail){
																				temp	+=	'Error: '+detail.error+'<br>'+
																							'Detail: '+detail.detail+'<br>'
																			});
																			info	+=	temp;
																		});
																		var nav = new Ext.Panel({
																			split: true,
																			autoWidth: true,
																			autoScroll:true,
																			html: info,
																			margins:'3 0 3 3',
																			cmargins:'3 3 3 3'
																		});

																		var win = new Ext.Window({
																			title: 'Report',
																			closable:true,
																			width:1024,
																			height:700,
																			layout: 'fit',
																			items: [nav]
																		}).show();
																	}
																}else{
															        Ext.Msg.show({
																        title:'Info',
																        msg:'Replace additional documents: Error',
																		icon: Ext.MessageBox.WARNING,
																        buttons: Ext.MessageBox.OK,
																	});
																}
														    },
												            failure: function(response, request) {
																Ext.MessageBox.hide();
														    	var errMessage = 'Error en la petición ' + request.url + 
														                        ' status ' + response.status + 
														                        ' statusText ' + response.statusText + 
														                        ' responseText ' + response.responseText;
														        alert(errMessage);
														    },
												            params: {
																tipo:	'replace-addons',
																data:	Ext.util.JSON.encode(additionalData),
																file:	Ext.getCmp('replace-combo-original-files').getValue(),
																tpl :	Ext.getCmp('replace-combo-original-template').getValue()
															}
												        });
													}
												}
											});
										}else{
											Ext.MessageBox.alert('Warning','Please, select an additional document.');
										}
				                    }
				                },{
				                    text     : 'Close',
				                    handler  : function(){
				                        miventana.close();
				                    }
				                }]
							}).show();
						}else{
							Ext.MessageBox.alert('Warning','Please, select an additional document from some user.');
						}
					}
		        }]
			})
		},{
			text:'Replace Contracts',
			menu: new Ext.menu.Menu({
		        items: [{
		            text: 'Replacement by name',
					handler:function(){
						var usersGroupId = new Array();
						Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
							var aux=item.id.split("-");
							usersGroupId.push({
								userid:aux[1],
								groups:Ext.getCmp(item.id).groups
							});
						});
						if(usersGroupId.length>0){
							var miventana = new Ext.Window({
								title: 'Replace Contracts by Name',
								width: 364,
								height: 180,
								maximizable: false,
								modal: true,
								resizable: false,
								maximized: false,
								constrain: true,
								items:[{
									xtype:'textfield',
									id:'contract-name-to-search',
									width:350,
									emptyText:'Name to change'
								},{
									xtype:'combo',
									id:'replace-combo-original-files',
									store:storeContracts,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Basic Contracts'
								},{
									xtype:'combo',
									id:'replace-combo-original-template',
									store:storeTemplates,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Assign Template'
								}],
								buttons: [{
				                    text     : 'Submit',
									handler  : function(){
										if(Ext.getCmp('contract-name-to-search').getValue()==""){
											Ext.MessageBox.alert('Warning','Please, write a contract name to search.');
											return true;
										}
										if(Ext.getCmp('replace-combo-original-files').getValue()!="0000"){
											var usersGroupId = new Array();
											Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
												var aux=item.id.split("-");
												usersGroupId.push({
													userid:aux[1],
													groups:Ext.getCmp(item.id).groups
												});
											});
											console.debug(usersGroupId.length);
											if(usersGroupId.length>0){
												Ext.Msg.show({
													title:'Replace All Contracts Previously Selected',
													msg:'Are you sure?',
													icon: Ext.MessageBox.WARNING,
													buttons: Ext.MessageBox.YESNO,
													fn:function(btn){
														if (btn == 'yes'){
															myMask.show();
															Ext.Ajax.request({
																url: 'php/grid_data.php',
																method: 'POST',
																timeout:0,
																success: function(response, request) {
																	myMask.hide();
																	var jsonData = Ext.util.JSON.decode(response.responseText);
																	if(jsonData.success){
																		Ext.Msg.show({
																			title:'Info',
																			msg:'Replace additional documents: Successfully',
																			icon: Ext.MessageBox.INFO,
																			buttons: Ext.MessageBox.OK,
																		});
																	}else{
																		Ext.Msg.show({
																			title:'Info',
																			msg:'Replace additional documents: Error',
																			icon: Ext.MessageBox.WARNING,
																			buttons: Ext.MessageBox.OK,
																		});
																	}
																},
																failure: function(response, request) {
																	myMask.hide();
																	var errMessage = 'Error en la petición ' + request.url + 
																					' status ' + response.status + 
																					' statusText ' + response.statusText + 
																					' responseText ' + response.responseText;
																	alert(errMessage);
																},
																params: {
																	tipo:	'replace-contract-by-name',
																	data:	Ext.util.JSON.encode(usersGroupId),
																	name:	Ext.getCmp('contract-name-to-search').getValue(),
																	file:	Ext.getCmp('replace-combo-original-files').getValue(),
																	tpl :	Ext.getCmp('replace-combo-original-template').getValue()
																}
															});
														}
													}
												});
											}else{
												Ext.MessageBox.alert('Warning','Sorry, doesn\'t have users..');
											}
										}else{
											Ext.MessageBox.alert('Warning','Please, select an additional document.');
										}
				                    }
				                },{
				                    text     : 'Close',
				                    handler  : function(){
				                        miventana.close();
				                    }
				                }]
							}).show();
						}else{
							Ext.MessageBox.alert('Warning','Please, select a contract from some user.');
						}
					}
				},{
		            text: 'Replacement by previously selected',
					handler:function(){
						Ext.MessageBox.alert('Warning','UnderConstruction By Jesus.');
						return true;
						var usersGroupId = new Array();
						Ext.each(Ext.get("principalResultContractsSearch").select('input[class*=cbx]').elements,function(item,index){
							if(item.checked){
								usersGroupId.push({
									userid	:	item.dataset.userid,
									contract:	item.dataset.contract
								});
							}
						});
						if(usersGroupId.length>0){
							var miventana = new Ext.Window({
								title: 'Replace Contracts by Name',
								width: 364,
								height: 180,
								maximizable: false,
								modal: true,
								resizable: false,
								maximized: false,
								constrain: true,
								items:[{
									xtype:'textfield',
									id:'contract-name-to-search',
									width:350,
									emptyText:'Name to change'
								},{
									xtype:'combo',
									id:'replace-combo-original-files',
									store:storeContracts,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Basic Contracts'
								},{
									xtype:'combo',
									id:'replace-combo-original-template',
									store:storeTemplates,
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Assign Template'
								}],
								buttons: [{
				                    text     : 'Submit',
									handler  : function(){
										if(Ext.getCmp('contract-name-to-search').getValue()==""){
											Ext.MessageBox.alert('Warning','Please, write a contract name to search.');
											return true;
										}
										if(Ext.getCmp('replace-combo-original-files').getValue()!="0000"){
											var usersGroupId = new Array();
											Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
												var aux=item.id.split("-");
												usersGroupId.push({
													userid:aux[1],
													groups:Ext.getCmp(item.id).groups
												});
											});
											console.debug(usersGroupId.length);
											if(usersGroupId.length>0){
												Ext.Msg.show({
													title:'Replace All Contracts Previously Selected',
													msg:'Are you sure?',
													icon: Ext.MessageBox.WARNING,
													buttons: Ext.MessageBox.YESNO,
													fn:function(btn){
														if (btn == 'yes'){
															myMask.show();
															Ext.Ajax.request({
																url: 'php/grid_data.php',
																method: 'POST',
																timeout:0,
																success: function(response, request) {
																	myMask.hide();
																	var jsonData = Ext.util.JSON.decode(response.responseText);
																	if(jsonData.success){
																		Ext.Msg.show({
																			title:'Info',
																			msg:'Replace additional documents: Successfully',
																			icon: Ext.MessageBox.INFO,
																			buttons: Ext.MessageBox.OK,
																		});
																	}else{
																		Ext.Msg.show({
																			title:'Info',
																			msg:'Replace additional documents: Error',
																			icon: Ext.MessageBox.WARNING,
																			buttons: Ext.MessageBox.OK,
																		});
																	}
																},
																failure: function(response, request) {
																	myMask.hide();
																	var errMessage = 'Error en la petición ' + request.url + 
																					' status ' + response.status + 
																					' statusText ' + response.statusText + 
																					' responseText ' + response.responseText;
																	alert(errMessage);
																},
																params: {
																	tipo:	'replace-contract-by-name',
																	data:	Ext.util.JSON.encode(usersGroupId),
																	name:	Ext.getCmp('contract-name-to-search').getValue(),
																	file:	Ext.getCmp('replace-combo-original-files').getValue(),
																	tpl :	Ext.getCmp('replace-combo-original-template').getValue()
																}
															});
														}
													}
												});
											}else{
												Ext.MessageBox.alert('Warning','Sorry, doesn\'t have users..');
											}
										}else{
											Ext.MessageBox.alert('Warning','Please, select a contract.');
										}
				                    }
				                },{
				                    text     : 'Close',
				                    handler  : function(){
				                        miventana.close();
				                    }
				                }]
							}).show();
						}else{
							Ext.MessageBox.alert('Warning','Please, select a contract from some user.');
						}
					}
				}]
			})
		}],
		id:'principalResultContractsSearch',
		listeners:{
			afteredit: doEdit,
			afterrender:function(){
				myMask.show();
				Ext.Ajax.request({  
					waitMsg: 'Loading...',
					//url: 'php/grid_data.php?tipo=users-contract',
					url: 'php/grid_data.php?tipo=users',
					method: 'POST', 
					timeout: 0,
					waitTitle   :'Please wait!',
					waitMsg     :'Loading...',
					failure:function(response,options){
						Ext.MessageBox.alert('Warning','Error Loading');
						myMask.hide();
					},
					success:function(response){ 
					    var variable = Ext.decode(response.responseText);
						Ext.each(variable.results, function(tempUsers, index) {
							// Create RowActions Plugin
					        var action = new Ext.ux.grid.RowActions({
					             header:'Actions'
					//          ,autoWidth:false
					//          ,hideMode:'display'
					            ,keepSelection:true
					            ,actions:[
								//{
					                //iconIndex:'action1'
					                //,qtipIndex:'qtip1'
					                //,iconCls:'icon-open'
					                //,tooltip:'Open'
									//,style:'background-color:green'
					            //},
								{
					                iconCls:'x-icon-see-file',
					                tooltip:'See Additional Document',
					                //,qtip:'Configure'
					                //,qtipIndex:'qtip2'
					                //,iconIndex:'action2'
					                //,hideIndex:'seeAddon'
									//,style:'background-color:red'
									//,text:'Open'
									callback:function(grid, record, action, row, col) {
										if(record.data.addon_name!="Doesn't have Addons"){
											record.data.origen='seeAddon';
											Ext.Ajax.request({
									            url: 'php/grid_data.php',
									            method: 'POST',
												timeout:0,
									            success: function(response, request) {
											    	var jsonData = Ext.util.JSON.decode(response.responseText);
											        if(jsonData.success){
														var Digital=new Date()
														var hours=Digital.getHours()
														var minutes=Digital.getMinutes()
														var seconds=Digital.getSeconds()
														var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/addons/'+jsonData.aditional.pdf;
														window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
													}
											    },
									            failure: function(response, request) {
											    	var errMessage = 'Error en la petición ' + request.url + 
											                        ' status ' + response.status + 
											                        ' statusText ' + response.statusText + 
											                        ' responseText ' + response.responseText;
											        alert(errMessage);
											    },
									            params: {
													tipo:	'upload-Info-Contracts',
													data:	Ext.util.JSON.encode(record.data),
												}
									        });
										}
									}
					            },{
					                iconCls:'x-icon-download-file',
					                tooltip:'See Additional Document',
					                //,qtip:'Configure'
					                //,qtipIndex:'qtip2'
					                //,iconIndex:'action2'
					                //,hideIndex:'seeAddon'
									//,style:'background-color:red'
									//,text:'Open'
									callback:function(grid, record, action, row, col) {
										if(record.data.addon_name!="Doesn't have Addons"){
											record.data.origen='seeAddon';
											Ext.Ajax.request({
									            url: 'php/grid_data.php',
									            method: 'POST',
												timeout:0,
									            success: function(response, request) {
											    	var jsonData = Ext.util.JSON.decode(response.responseText);
											        if(jsonData.success){
														var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/addons/'+jsonData.aditional.pdf;
														window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
													}
											    },
									            failure: function(response, request) {
											    	var errMessage = 'Error en la petición ' + request.url + 
											                        ' status ' + response.status + 
											                        ' statusText ' + response.statusText + 
											                        ' responseText ' + response.responseText;
											        alert(errMessage);
											    },
									            params: {
													tipo:	'upload-Info-Contracts',
													data:	Ext.util.JSON.encode(record.data),
												}
									        });
										}
									}
					            },{
					                 iconCls:'x-icon-addon-delete'
					                //,qtipIndex:'qtip3'
									//,iconCls:'x-icon-cancel'
					                ,tooltip:'Delete Addon',
									callback:function(grid, record, action, row, col) {
										//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
										if(record.data.addon_name!="Doesn't have Addons"){
											Ext.Msg.show({
										        title:'Delete Additional Document',
										        msg:'Delete: '+record.data.addon_name+'. Are you sure?',
										        buttons: Ext.MessageBox.YESNO,
												fn:function(btn){
													if(btn=="yes"){
														record.data.origen='deleteAddon';
												        Ext.Ajax.request({
												            url: 'php/grid_data.php',
												            method: 'POST',
															timeout:0,
												            success: function(response, request) {
														    	var jsonData = Ext.util.JSON.decode(response.responseText);
														        if(jsonData.success){
																	if(jsonData.aditional.reload)
																		grid.getStore().reload();
																	grid.getStore().commitChanges();
																}else{
																	grid.getStore().rejectChanges();
															        alert('Error en la Edicion');
																}
														    },
												            failure: function(response, request) {
																Ext.getCmp(grid.idGrid).getStore().rejectChanges();
														    	var errMessage = 'Error en la petición ' + request.url + 
														                        ' status ' + response.status + 
														                        ' statusText ' + response.statusText + 
														                        ' responseText ' + response.responseText;
														        alert(errMessage);
														    },
												            params: {
																tipo:	'upload-Info-Contracts',
																data:	Ext.util.JSON.encode(record.data),
															}
												        });
													}
												}
									        });
										}
					                }
					            }]
					            ,groupActions:[{
					                 iconCls:'x-icon-document-delete'
					                ,qtip:'Remove Table'
									//,style:'background-color:orange'
									,callback:function(grid, records, action, groupId) {
										console.debug(grid, records, action, groupId);
										var dataToSend = new Array();
										dataToSend={
											origen	:	'deleteContract',
											data	:	{}
										};
										dataToSend.data=records[0].data;
										Ext.Msg.show({
										    title:'Delete Contrac',
									        msg:'Delete: '+records[0].data.namefile+' with all additional documents? Are you sure?',
									        buttons: Ext.MessageBox.YESNO,
											fn:function(btn){
												if(btn=="yes"){
											        Ext.Ajax.request({
											            url: 'php/grid_data.php',
											            method: 'POST',
														timeout:0,
											            success: function(response, request) {
													    	var jsonData = Ext.util.JSON.decode(response.responseText);
													        if(jsonData.success){
																grid.getStore().reload();
																Ext.Msg.show({
																	title:'Info',
																	msg:'Delete Contract: Success',
																	icon: Ext.MessageBox.INFO,
																	buttons: Ext.MessageBox.OK,
																});
															}else{
																Ext.Msg.show({
																	title:'Info',
																	msg:'Delete Contract: Error',
																	icon: Ext.MessageBox.WARNING,
																	buttons: Ext.MessageBox.OK,
																});
															}
													    },
											            failure: function(response, request) {
													    	var errMessage = 'Error en la petición ' + request.url + 
													                        ' status ' + response.status + 
													                        ' statusText ' + response.statusText + 
													                        ' responseText ' + response.responseText;
													        alert(errMessage);
													    },
											            params: {
															tipo:	'upload-Info-Contracts',
															data:	Ext.util.JSON.encode(dataToSend),
														}
											        });
										    	}
											}
										});
					                }
					            },{
					                iconCls:'x-icon-download-files',
					                qtip:'Add Table - with callback',
					                callback:function(grid, records, action, groupId) {
					                    //Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
										records[0].data.origen='seeContract';
										Ext.Ajax.request({
								            url: 'php/grid_data.php',
								            method: 'POST',
											timeout:0,
								            success: function(response, request) {
										    	var jsonData = Ext.util.JSON.decode(response.responseText);
										        if(jsonData.success){
													var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+jsonData.aditional.pdf;
													window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
												}
										    },
								            failure: function(response, request) {
										    	var errMessage = 'Error en la petición ' + request.url + 
										                        ' status ' + response.status + 
										                        ' statusText ' + response.statusText + 
										                        ' responseText ' + response.responseText;
										        alert(errMessage);
										    },
								            params: {
												tipo:	'upload-Info-Contracts',
												data:	Ext.util.JSON.encode(records[0].data),
											}
								        });
					                }
									//style:'background-color:blue'
					            },{
					                iconCls:'x-icon-see-files',
					                qtip:'Add Table - with callback',
					                callback:function(grid, records, action, groupId) {
					                    //Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
										records[0].data.origen='seeContract';
										Ext.Ajax.request({
											url: 'php/grid_data.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												if(jsonData.success){
													var Digital=new Date()
													var hours=Digital.getHours()
													var minutes=Digital.getMinutes()
													var seconds=Digital.getSeconds()
													var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+jsonData.aditional.pdf;
													window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
												}
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
												alert(errMessage);
											},
											params: {
												tipo:	'upload-Info-Contracts',
												data:	Ext.util.JSON.encode(records[0].data),
											}
										});
					                }
									//style:'background-color:blue'
					            },{
					                iconCls:'x-icon-add-addon',
					                qtip:'Add Additionals Documents',
					                callback:function(grid, records, action, groupId) {
					                    //Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
										BK_data.standar.contract=0;
										BK_data.standar.addons={};
										var limit=0;
										Ext.each(records,function(item,index){
											if(item.data.addon_name!="Doesn't have Addons")
												limit = limit + 1;
										});
										var temp = [{
											xtype:'fieldset',
											layout:'column',
											autoHeight:true,
											collapsible	:	false,
											contractFileName:records[0].data.namefile,
											idContract:records[0].data.idContract,
											userId:records[0].data.userid,
											id:'upload-contracts-new-addons',
											contractId:BK_data.standar.contract,
											items:[{
												layout: 'form',
												border:false,
												columnWidth:1,
												items:[{
													xtype:'combo',
													store:storeAddons,
													valueField: 'id',
													typeAhead: true,
													width:350,
													forceSelection: true,
													mode: 'local',
													value:'0000',
													triggerAction: 'all',
													selectOnFocus: true,
													displayField: 'name',
													editable: false,
													fieldLabel:'Basic Aditionals',
													limit:limit,
													father:'upload-contracts-new-addons',
													fatherId:BK_data.standar.contract,
													listeners:{
														select:function( combo, newValue, oldValue ){
															BK_data.standar.addons[combo.fatherId]	=	typeof BK_data.standar.addons[combo.fatherId] == "undefined" ? 1 : BK_data.standar.addons[combo.fatherId]+1;
															if(newValue.data.id!='0000'){
																if(BK_data.standar.addons[combo.fatherId]<=(6-combo.limit)){
																	var temp1=[{
																		xtype:'fieldset',
																		id:'upload-contracts-new-addons-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																		style: 'margin-left:30px',
																		columnWidth:1,
																		addonFileName:newValue.data.name,
																		addonId:BK_data.standar.addons[combo.fatherId],
																		cls:'margin-left:20px',
																		layout:'column',
																		title:'Addon: '+newValue.data.name,
																		items:[{
																			layout: 'form',
																			border:false,
																			columnWidth:.45,
																			items:[{
																				xtype:'textfield',
																				width:400,
																				id:'addon-FinalName-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																				fieldLabel:'Addon Name',
																				value:newValue.data.name
																			}]
																		},{
																			layout: 'form',
																			border:false,
																			columnWidth:.45,
																			items:[{
																				xtype:'combo',
																				columnWidth:.5,
																				store:storeTemplates,
																				valueField: 'id',
																				typeAhead: true,
																				width:350,
																				id:'addon-FinalTemplate-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																				forceSelection: true,
																				mode: 'local',
																				value:'0000',
																				triggerAction: 'all',
																				selectOnFocus: true,
																				displayField: 'name',
																				editable: false,
																				fieldLabel:'Asign Template',
																				listeners:{
																					select:function( combo, newValue, oldValue ){
																						
																					}
																				}
																			}]
																		},{
																			layout: 'form',
																			border:false,
																			columnWidth:.10,
																			items:[{
																				xtype:'button',
																				text: 'Delete',
																				father:'addons-standar-'+BK_data.standar.addons[combo.fatherId],
																				fatherID:combo.fatherId,
																				tooltip:'Delete this addon',
																				handler: function(){
																					BK_data.standar.addons[this.fatherId]=BK_data.standar.addons[this.fatherId]-1;
																					Ext.getCmp(this.father).destroy();
																				}
																			}]
																		}]
																	}]
																	Ext.getCmp(combo.father).add(temp1);
																	Ext.getCmp('upload-contracts-new-addons').doLayout();
																	combo.setValue('0000');
																}else{
																	BK_data.standar.addons[combo.fatherId]=BK_data.standar.addons[combo.fatherId]-1;
																	Ext.MessageBox.alert('Warning', 'Only can be added '+(6-combo.limit)+' aditionals documents.');
																}
															}
														}
													}
												}]
											}]
										}];
										var miventana = new Ext.Window({
											title: 'Upload New Additional Document(s) for Contract: '+records[0].data.namefile,
											width: 1300,
											autoScroll:	true,
											height: 400,
											maximizable: false,
											modal: true,
											resizable: false,
											maximized: false,
											constrain: true,
											items:[{
												xtype:'form',
												items:[temp]
											}],
											buttons: [{
												text     : 'Submit',
												handler  : function(){
													Ext.Msg.show({
														title:'Are you sure?',
														msg:'Upload All Additional Document Previously Selected',
														icon: Ext.MessageBox.WARNING,
														buttons: Ext.MessageBox.YESNO,
														fn:function(btn){
															if (btn == 'yes'){
																BK_data.dataToSend.contracts={};
																
																var contractId	=	0;
																BK_data.dataToSend.contracts[contractId]={
																	'idContract':Ext.getCmp('upload-contracts-new-addons').idContract,
																	'addons':{}
																}
																Ext.each(Ext.get("upload-contracts-new-addons").select('fieldset[id*=new]').elements,function(item,index){
																	var addonN			=	index;
																	var addonFileName	=	Ext.getCmp(item.id).addonFileName;
																	var addonContractId	=	Ext.getCmp(item.id).addonId;
																	var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
																	var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
																	BK_data.dataToSend.contracts[contractId].addons[addonN]={
																		'addonFileName':addonFileName+'.pdf',
																		'addonRealName':addonRealName,
																		'addonTemplate':addonTemplate
																	}
																});
																Ext.Ajax.request({
																	url: 'php/grid_data.php',
																	method: 'POST',
																	timeout:0,
																	success: function(response, request) {
																		var jsonData = Ext.util.JSON.decode(response.responseText);
																		if(jsonData.success){
																			Ext.Msg.show({
																				title:'Info',
																				msg:'Upload additional documents: Successfully',
																				icon: Ext.MessageBox.INFO,
																				buttons: Ext.MessageBox.OK,
																			});
																		}else{
																			Ext.Msg.show({
																				title:'Warning',
																				msg:'Upload additional documents: Failed',
																				icon: Ext.MessageBox.WARNING,
																				buttons: Ext.MessageBox.OK,
																			});
																		}
																		grid.getStore().reload();
																	},
																	failure: function(response, request) {
																		var errMessage = 'Error en la petición ' + request.url + 
																						' status ' + response.status + 
																						' statusText ' + response.statusText + 
																						' responseText ' + response.responseText;
																					   
																		alert(errMessage);
																		grid.getStore().reload();
																	},
																	params: {
																		tipo:	'uploadNewsAddons',
																		data:	Ext.util.JSON.encode(BK_data.dataToSend),
																		user:	Ext.getCmp('upload-contracts-new-addons').userId
																	}
																});
															}
														}
													});
												}
											},{
												text     : 'Close',
												handler  : function(){
													miventana.close();
												}
											}]
										}).show();
					                }
									//style:'background-color:blue'
					            },{
					                iconCls:'x-icon-edit-text',
					                qtip:'Change Contract Name',
					                align:'left',
									callback:function(grid, records, action, groupId) {
										var dlg = Ext.MessageBox.prompt('Change '+records[0].data.type+':', 'Please enter new Contract name:', function(btn, text){
										    if (btn == 'ok'){
												records[0].data.origen='changeContractName';
												records[0].data.newName=text;
										        Ext.Ajax.request({
										            url: 'php/grid_data.php',
										            method: 'POST',
													timeout:0,
										            success: function(response, request) {
														console.debug(grid);
												    	var jsonData = Ext.util.JSON.decode(response.responseText);
												        if(jsonData.success){
															if(jsonData.aditional.reload)
																grid.getStore().reload();
															grid.getStore().commitChanges();
														}else{
															grid.getStore().rejectChanges();
													        alert('Error en la Edicion');
														}
												    },
										            failure: function(response, request) {
														Ext.getCmp(grid.idGrid).getStore().rejectChanges();
												    	var errMessage = 'Error en la petición ' + request.url + 
												                        ' status ' + response.status + 
												                        ' statusText ' + response.statusText + 
												                        ' responseText ' + response.responseText;
												        alert(errMessage);
												    },
										            params: {
														tipo:	'upload-Info-Contracts',
														data:	Ext.util.JSON.encode(records[0].data),
													}
										        });
										    }
										});
					                }
									//,style:'background-color:purple'
					            }]
					            ,callbacks:{
					                'x-icon-see-file':function(grid, record, action, row, col) {
										//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
									},
					                'x-icon-addon-delete':function(grid, record, action, row, col) {
					                    
					                }
					            }
					        });
							// Create row expander
					        var expander = new Ext.grid.RowExpander({
					            tpl: new Ext.Template(
					                 '<p style="margin:0 0 4px 8px"><b>Company:</b> {company}</p>'
					                ,'<p style="margin:0 0 4px 8px"><b>Summary:</b> {desc}</p>'
					            )
					        });
							// dummy action event handler - just outputs some arguments to console
							action.on({
					            action:function(grid, record, action, row, col) {
					                //Ext.ux.Toast.msg('Event: action', 'You have clicked row: <b>{0}</b>, action: <b>{1}</b>', row, action);
					            }
					            ,beforeaction:function() {
					                //Ext.ux.Toast.msg('Event: beforeaction', 'You can cancel the action by returning false from this event handler.');
					            }
					            ,beforegroupaction:function() {
					                //Ext.ux.Toast.msg('Event: beforegroupaction', 'You can cancel the action by returning false from this event handler.');
					            }
					            ,groupaction:function(grid, records, action, groupId) {
					                //Ext.ux.Toast.msg('Event: groupaction', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
					            }
					        });
							var store = new Ext.data.GroupingStore({
				                reader:new Ext.data.JsonReader({
				                     id:'id'
				                    ,totalProperty:'total'
				                    ,root:'data'
				                    ,fields:[
				                       {name: 'namefile'}
				                       ,{name: 'desc'}
				                       ,{name: 'addon_name'}
				                       ,{name: 'place'}
				                       ,{name: 'type'}
				                       ,{name: 'userid', type: 'int'}
				                       ,{name: 'idContract', type: 'int'}
				                       ,{name: 'idAddon', type: 'int'}
				                       ,{name: 'origen', type: 'string'}
				                    ]
				                })
				                ,proxy:new Ext.data.HttpProxy({url:'php/grid_data.php?tipo=users-contract'})
				                ,groupField:'namefile'
				                ,sortInfo:{field:'idContract', direction:'ASC'}
								,idGrid:'gridFiles-'+tempUsers.userid
								,listeners:{
									update:function(e,record,operation){
										//console.debug(JSON.stringify(recordData));
										Ext.Ajax.request({
								            url: 'php/grid_data.php',
								            method: 'POST',
											timeout:0,
								            success: function(response, request) {
										    	var jsonData = Ext.util.JSON.decode(response.responseText);
												if(jsonData.success){
													if(jsonData.aditional.reload)
														Ext.getCmp(e.idGrid).getStore().reload();
													Ext.getCmp(e.idGrid).getStore().commitChanges();
												}else{
													Ext.getCmp(e.idGrid).getStore().rejectChanges();
											        alert('Error en la Edicion');
												}
										    },
								            failure: function(response, request) {
												Ext.getCmp(e.idGrid).getStore().rejectChanges();
										    	var errMessage = 'Error en la petición ' + request.url + 
										                        ' status ' + response.status + 
										                        ' statusText ' + response.statusText + 
										                        ' responseText ' + response.responseText;
										        alert(errMessage);
										    },
								            params: {
												tipo:	'upload-Info-Contracts',
												data:	Ext.util.JSON.encode(record.data),
											}
								        });
									}
								}
				            });
							var mySelectionModelMen = new Ext.grid.CheckboxSelectionModel({
								singleSelect: false
							});
							var originalFunction = Ext.grid.GroupingView.prototype.processEvent;
						    Ext.override(Ext.grid.GroupingView, {
						        processEvent : function(name, e){	
						    	     if (e.getTarget().tagName == 'INPUT'){
						    	         e.stopPropagation();
						    	     }else{
						    	         originalFunction.apply(this, arguments);
						    	     }
						    	}
						    });
							// create grid configured
							var Grid = new Ext.grid.EditorGridPanel({
							//var Grid = new Ext.grid.GridPanel({
								autoHeight:true
								,id:'gridFiles-'+tempUsers.userid
								,allInfoUser:tempUsers
								,store:store
								,loadMask: true
								,userid:tempUsers.userid
								,clicksToEdit: 2
					            ,columns:[
					                //expander,
					                {header: "Contract", hidden: true, sortable: true, dataIndex: 'namefile'}
									//,{id:'company',header: "Company", width: 40, sortable: true, dataIndex: 'company'}
					                ,{header: "Addon Name", width: 20, sortable: true, dataIndex: 'addon_name', editor: new Ext.form.TextField({allowBlank: false})}
					                ,mySelectionModelMen
					                ,action
					            ]
					            ,plugins:[
									action
									//,expander
								]
					            ,view: new Ext.grid.GroupingView({
					                 forceFit:true
					                ,groupTextTpl:'<span class="gridHeaderCheckbox"><input type="checkbox" class="cbx" data-contract="{[ values.rs[0].data["idContract"] ]}"'+
										'data-userid="{[ values.rs[0].data["userid"] ]}"></span>'+
										'{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
					            })
					            ,loadMask:true
								,listeners:{
									render:function(){
										this.store.load({params:{userForGrid:this.userid}});
									},
									beforeedit:function(e){
										e.record.data.origen='changeAddonName';
										//console.debug(e);
										if(e.value=="Doesn't have Addons")
											e.cancel=true;
									}
								}
								,sm:mySelectionModelMen
								//,bbar:bbar
							}); // eo extend
							 
							var temp = [(index==0)?{
								xtype:'checkbox',
								style:'margin-left: 10px',
								listeners:{
									check:function(check,checked){
										if(checked){
											Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
												Ext.getCmp(item.id).setValue(true);
											});
										}else{
											Ext.each(Ext.get("principalResultContractsSearch").select('input[id*=checkUserField]').elements,function(item,index){
												Ext.getCmp(item.id).setValue(false);
											});
										}
									}
								}
							}:{},{
								layout:'column',
								border:false,
								items:[{
									xtype:'checkbox',
									columnWidth:0.02,
									id:'checkUserField-'+tempUsers.userid,
									groups:tempUsers.name+" "+tempUsers.surname,
									style:'margin-left: 10px'
								},{
									xtype:'fieldset',
									columnWidth:0.98,
									id:'userField-'+tempUsers.userid,
									userid:tempUsers.userid,
									height: 550,
									title:'User: '+tempUsers.name+" "+tempUsers.surname,
									collapsible	:	true,
									autoScroll  :	true,
									collapsed	:	true,
									items:[Grid]
								}]
							}]
							Ext.getCmp('principalResultContractsSearch').add(temp);
							Ext.getCmp('principalResultContractsSearch').doLayout();
						});
						myMask.hide();
					}
				});
			}
		}
	};
	

////////////////FIN Grid Mentoring////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			height: Ext.getBody().getViewSize().height-25,
			items: tabPanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////


	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
	store.addListener('load', obtenerTotal);	
	
	//gridMen.addListener('afteredit', doEdit);
	
//////////////////FIN Listener///////////////////////
	
//////////*Codigo fuera de uso*//////////
/***************************************/

function addTab(tabTitle, targetUrl){
		tabPanel.add(grid);
		store.load(/*{params:{start:0, limit:100}}*/);
		/////////////FIN Inicializar Grid////////////////////
		pagingBar.bind(store);
    }
	function updateTab(tabId,title, url) {
    	
		var tab = tabPanel.getItem(tabId);
    	if(tab){
			console.log("if");
    		tab.getUpdater().update(url);
    		tab.setTitle(title);
    	}else{
			console.log("else");
    		tab = addTab(title,url);
    	}
		
    	tabPanel.setActiveTab(1);
    }

});



var documets = function(s,g,id){
	console.log(s)
	console.log(g)
	var store = s;
	var grids = g;
}// JavaScript Document