function nuevoAjax()
{
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false;
		try
		{
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			try
		{
			// Creacion del objeto AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); }

			return xmlhttp;
}
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    var dayfrom;
	var dayto;
	var fecha=new Date();
	var diahoy=fecha.getDate();
///////////Cargas de data dinamica///////////////
	var dias=new Ext.data.SimpleStore({
						fields: ['id'],
							data  : [
								['1'],['2'],['3'],['4'],['5'],['6'],['7'],['8'],['9'],['10'],
								['11'],['12'],['13'],['14'],['15'],['16'],['17'],['18'],['19'],['20'],
								['21'],['22'],['23'],['24'],['25'],['26'],['27'],['28'],['29'],['30'],
								['31']
							]
						});
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		//url: 'php/grid_data.php?tipo=deudoresFecha',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data.php?tipo=deudoresFecha', timeout: 3600000 }),
		reader: new Ext.data.JsonReader(),
		baseParams  :{dayfrom: diahoy, dayto: diahoy, start:0, limit:200},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status'
			,'usertype'
			,'paydated'
			,'priceprod'
			,'cardname'
			,'cardnumber'
			,'saldo'
			,'amount'
			,'amountsinformato'
			,'notes'
			,'freedays'
			,'nameprod'
			,'marcalogin'
			,{name: 'veceslog', type: 'int'}
		]
	});

///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.PagingToolbar({
		//width: 700,
		//layout: 'form',
		items: [{
				width:100,
				xtype:'label',
				id: 'empty',
				text: 'Search Day From'
			},{
				width:50,
				id:'dayfrom',
				xtype:'combo',
				store: dias,
				mode: 'local',
				valueField: 'id',
				displayField: 'id',
				triggerAction: 'all',
				selectOnFocus: true,
				allowBlank: false,
				value: diahoy
			},{
				width:20,
				xtype:'label',
				text: 'To',
				align: 'center'
			},{
				width:50,
				id:'dayto',
				xtype:'combo',
				store: dias,
				mode: 'local',
				valueField: 'id',
				displayField: 'id',
				triggerAction: 'all',
				selectOnFocus: true,
				allowBlank: false,
				value: diahoy
			},{
				width:80,
				id:'search',
				xtype:'button',
				text:'&nbsp;<b>Search</b>',
				pressed: true,
				enableToggle: true,
				handler: searchPay
			},{
				width:200,
				xtype:'label',
				text: '',
				align: 'center'
			},{
				width:200,
				xtype:'label',
				id: 'total',
				text: 'Total Amount:',
				align: 'center'
			}
		]
	});

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 20000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			'-',{
				id: 'exc_butt',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
			},'-',
			panelpag
		]
    });
////////////////FIN barra de pagineo//////////////////////
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//ajax.send("campo=45");
		ajax.send("parametro=fecha&dayfrom="+Ext.getCmp('dayfrom').getValue()+"&dayto="+Ext.getCmp('dayto').getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);
			}
		}
	}
//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		/*var com;
		var j=0;
		var marcados="(";
		var arrayRecords = store.getRange(0,store.getCount());
		for(i=0;i<arrayRecords.length;i++){
			com="del"+arrayRecords[i].get('userid');

			if(document.getElementById(com.toString()).checked==true){
				if(j>0) marcados+=',';

				//marcados+=arrayRecords[i].get('userid');
				marcados+=arrayRecords[i].get('userid')+'$'+arrayRecords[i].get('saldo');
				j++;
			}
		}
		marcados+=")";
		return marcados;*/
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;
		return marcados;
	}
	function showHistorico(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		if(obtenidos.split(',').length>1){
			Ext.MessageBox.alert('Warning','Debes seleccionar un usuario para generar su historico');
			return;
		}
/********************************************************************
********** historico() Se encuentra en historico_full.js *************
*********************************************************************/
				historico(obtenidos.split('$')[0]);

	}
	function searchPay(){
		if(Number(Ext.getCmp("dayfrom").getValue())>Number(Ext.getCmp("dayto").getValue())){
			Ext.MessageBox.alert('Warning','Dia desde no puede ser mayor al dia hasta');
		}else{
			store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
			store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
			store.load();
		}
	}
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		for(i=0;i<store.getCount();i++){
			acum = acum + parseFloat(totales[i].data.amountsinformato);
		}
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));
;	}
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
	function renderTopic(value, p, record){// onclick="mostrarDetalleUsuers({0},\'customerservices\')"
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}
	}
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var myView = new Ext.grid.GridView();
	myView.getRowClass = function(record, index, rowParams, store) {
		if(record.data['marcalogin'] == 'Y')return 'orange-row';
    };
	var grid = new Ext.grid.EditorGridPanel({
		view: myView,
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{id:'userid',header: "User ID", width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: "Surname", width: 100, align: 'left', sortable: true, dataIndex: 'surname'}
			,{header: "Status", width: 80, align: 'left', sortable: true, dataIndex: 'status'}
			//,{header: 'User Type', width: 80, sortable: true, align: 'left', dataIndex: 'usertype'}
			,{header: 'Paydate', width: 80, sortable: true, align: 'center', dataIndex: 'paydated'}
			,{header: 'Price', width: 60, sortable: true, align: 'right', dataIndex: 'priceprod'}
			,{header: 'TrialDays',width: 70, sortable: true, align: 'center', dataIndex: 'freedays'}
			,{header: 'Card Type', width: 100, sortable: true, align: 'left', dataIndex: 'cardname'}
			//,{header: 'Card Number', width: 130, sortable: true, align: 'left', dataIndex: 'cardnumber'}
			,{header: 'Last 30 logs', width: 80, sortable: true, align: 'center', tooltip: 'Last 30 days logs', dataIndex: 'veceslog'}
			,{header: 'Amount', width: 60, sortable: true, align: 'right', dataIndex: 'amount'}
			,{header: 'Product', width: 120, sortable: true, align: 'left', dataIndex: 'nameprod'}
		],
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,//'99.8%',
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar
	});
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'PP Cobros Dia',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [{
			xtype: 'compositefield',
			fieldLabel: '<b>Opciones</b>',
			items:[/*{
				width:120,
				id:'historico',
				xtype:'button',
				text:'Historico',
				handler: showHistorico
			},*/{
			width:120,
				id:'cobrarsel',
				xtype:'button',
				text:'Otro'
			},{
				width:120,
				xtype:'checkbox',
				boxLabel: '<font color="red">&nbsp;<b>.</b></font>',
				name: 'chpaypal'
			}]
		},
			grid ]
	});

	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	dayfrom= Ext.getCmp("dayfrom").getValue();
	dayto=Ext.getCmp("dayto").getValue();

	store.load();
	//storeTotal.load({params:{start:0, limit:200, dayfrom: Ext.getCmp("dayfrom").getValue(), dayto: Ext.getCmp("dayto").getValue()}});
/////////////FIN Inicializar Grid////////////////////
	//searchPay();
});
