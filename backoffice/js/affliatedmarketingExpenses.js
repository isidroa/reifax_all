Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=expenses',
		fields: [
			{name: 'id', type: 'int'}
			,'reason'
			,'date'
			,'amount' 
			,'notes'			
			,'usersource'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var hoy  = new Date();	
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'add_button',
			text: 'Add',
			tooltip: 'Click to Add Workshop',
			handler: AddWindow 
        },{
			id: 'del_butt',
			tooltip: 'Delete selected row',
			text: 'Delete',
			iconCls:'icon',
			icon: 'images/delete.gif',
        	handler: doDel
		},'-'
		,{
			xtype: 'datefield',
			width: 100,
			name: 'dayfrom',
			id: 'dayfrom',
			format: 'Y-m-d',
			editable: false,
			value:  new Date(hoy.getFullYear(), hoy.getMonth() , 1)	
		},{
			xtype: 'datefield',
			width: 100,
			name: 'dayto',
			id: 'dayto',
			format: 'Y-m-d',			
			editable: false,
			value: new Date()
		},{
			xtype: 'button',
			width:80,
			id:'searchc',
			pressed: true,
			enableToggle: true,
			name:'searchc',
			text:'&nbsp;<b>Search</b>',
			handler: searchCobros
		},{
			width:200,
			xtype:'label',
			id: 'total',
			text: 'Total Amount: 0.00',
	        margins: '5 0 0 50',
			align: 'center'
		}]
    });
////////////////FIN barra de pagineo//////////////////////
	//////////////////Manejo de Eventos//////////////////////

	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.id;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}
	function doDel(){
		
		var obtenidos=obtenerSeleccionados();
		//alert( var include_type = mypanel.getValues()['radiocobros'] );
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
//		Ext.MessageBox.alert('Message',obtenidos);return;

		Ext.MessageBox.confirm('Expenses','Are you sure you want to delete this row?',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo: 'expenses',
							ID:  obtenidos
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							
							if(rest.succes==false)
								Ext.MessageBox.alert('Warning',rest.msg);
								
							store.reload();
						}
					}); 
				}
			}
		)
	}

	function AddWindow(){
		var formul = new Ext.FormPanel({
			url:'php/grid_add.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'reason',
				name: 'reason',
				fieldLabel: '<span style="color:#F00">*</span> Reason',
				allowBlank:false,
				width: 250
			},{
				xtype:'numberfield',
				id: 'amount',
				name: 'amount',
				fieldLabel: '<span style="color:#F00">*</span> Amount',
				allowBlank:false,
				allowDecimals : true,
				width: 100
			},{
				xtype:'textarea',
				id: 'notes',
				name: 'notes',
				fieldLabel: ' Notes',
				width: 250,
				height: 60						
			}],
			buttons: [{
				text: 'Cancel',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler  : function(){
					wind.close();
			   }
			},{
				text: 'Save',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
                formBind: true,
				handler  : function(){
					if(formul.getForm().isValid())
					{
						formul.getForm().submit({
							method: 'POST',
							params: {
								tipo : 'expenses'										
							},								
							waitTitle: 'Please wait..',
	                        waitMsg: 'Sending data...',
							success: function(form, action) {
								store.reload();
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Success", obj.msg);
								wind.close();
							},
							failure: function(form, action) {
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.msg);
							}
						});
					}
				}
			}]				
		});
		var wind = new Ext.Window({
				title: 'New Expense',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 200,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul				
			});
	
		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function doAdd()

	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.load({params:{start:0, limit:100}});
	}
	function obtenerTotal(){
		var totales = store.getRange(0,store.getCount());
		var i;
		var acum=0;
		for(i=0;i<store.getCount();i++)
		{
			acum = acum + parseFloat(totales[i].data.amount);
		}
		Ext.getCmp('total').setText('Total Amount: '+(acum).toFixed(2));	
	}
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function viewnotes(val, p, record){
		if(record.data.edit=='N') return '';
		
		return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editWorkshop('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }
	function viewnstatus(val, p, record){
		
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
	function viewlink(val, p, record){
		
		return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Manage Expenses',
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			new Ext.grid.RowNumberer()
			,mySelectionModel
			,{id: 'id',header: 'IDU', width: 30, sortable: true, align: 'center', dataIndex: 'id'}
			,{header: 'Reason', width: 250, sortable: true, align: 'left', dataIndex: 'reason'}
			,{header: 'Date', width: 130, align: 'left', sortable: true, dataIndex: 'date'}
			,{header: 'Amount', width: 70, sortable: true, align: 'right', dataIndex: 'amount', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{header: 'Source', width: 150, align: 'left', sortable: true, dataIndex: 'usersource'}
			,{header: 'Notes', width: 200, align: 'left', sortable: true, dataIndex: 'notes'}
		],
		clicksToEdit:2,
		height: Ext.getBody().getViewSize().height-50,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	store.addListener('load', obtenerTotal);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
 
});
