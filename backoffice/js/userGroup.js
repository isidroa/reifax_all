// JavaScript Document
Ext.ns('userGroup');

var sm		=	new Ext.grid.CheckboxSelectionModel({
	listeners: {
        selectionchange: function(a) {
        }
    }
});
var userAdministra=[5,75,2,1,3,20,3811,2342,2482,3175];
var userEjecutor=[4399,2544,4345,4707,4733];
var myMask	=	new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
var BK_data	=	{
	standar	:	{
		contract:	0,
		addons	:	{}
	},
	custom	:	{
		contract:	0,
		addons	:	{}
	},
	users		:	{},
	limitGrid	:	30,
	dataToSend	:	{
		contracts		:	{},
		tplAdvanceSearch:	'',
		tplAdvanceResult:	'',
		scrowLetter		:	{
			images:{
				header	:	'',
				footer	:	''
			},
			data:{
				agent	:	"",
				address	:	"",
				email	:	"",
				phone	:	"",
				fax		:	""
			}
		}
	}
};
userGroup	=	{
	prepare:function (){
		Ext.Ajax.request({
			url : 'php/funcionesUserGroup.php',
			params :
				{
					id : userGroup.currentNode,
					accion	:'loadConfigFeacture'
				},
			scope:this,
			success : function(response) {
				var data=Ext.util.JSON.decode(response.responseText);
				var defaultData=	[
					{name: 'id', type: 'int'},
					{name: 'userid', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'surname', type: 'string'},
					{name: 'phone', type: 'string'},
					{name: 'address', type: 'string'},
					{name: 'city', type: 'string'},
					{name: 'state', type: 'string'},
					{name: 'zipcode', type: 'string'},
					{name: 'active', type: 'int'},
					{name: 'statusUsr', type: 'string'},
					{name: 'camptitCondition', type: 'string'},
					{name: 'group', type: 'string'},
					{name: 'email', type: 'string'},
					{name: 'dateRecord', type: 'date', dateFormat: 'Y-m-d'}
				];
				Ext.each(data.feature,function(data){ //step 2
					if(data.feature!=''){
						if(data.feature.camptit!="")
							defaultData.push({
								name:data.id, type: 'string'
							});
						else
							defaultData.push({
								name:data.id, type: 'int'
							});
					}
				});
				userGroup.record = new Ext.data.Record.create(defaultData);
				userGroup.init();
			}
		})
	},
	init: function ()
	{
		userGroup.ZeroClipboard='';
		userGroup.gridReader = new Ext.data.JsonReader({
			root: 'user',
			totalProperty: 'total',
			id:'readeruserGroup'
			},
			userGroup.record
			);

		userGroup.store = new Ext.data.Store({
			id		: 'userGroup',
			remoteSort	: true,
			proxy	: userGroup.dataProxy,
			baseParams	: {
				accion	:'getUsers',
				specialFilters:userGroup.filterToSend
			},
			reader		: userGroup.gridReader,
			listeners 	:{
				load	: function (){
				}
			}
		});
		userGroup.storeAddons = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=users-contract-addons',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});
		userGroup.storeFeaturesType=	new Ext.data.Store({
			id			:	'id',
			autoLoad	:	true,
			proxy		:	userGroup.dataProxy,
			baseParams	:	{
				accion	:	'listFeaturesType'
			},
			reader				:	new Ext.data.JsonReader({
				root			:	'featuresType',
				totalProperty	:	'total'
			},
				userGroup.recordFeaturesType
			)
		});
		userGroup.storeGrouping = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=grouping-contract',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});
		userGroup.storeTemplates = new Ext.data.JsonStore({
			url:'/custom_contract/includes/php/functions.php',
			root: 'results',
			baseParams:{
				idfunction:'6',
				userCustom:'3803'
			},
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});
		userGroup.storeContracts = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=users-contract-contracts',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});
		userGroup.storeScrowLetter = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=users-contract-scrowLetter',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'},
				{name:'logo', type: 'string'},
				{name:'desc', type: 'string'}
			]
		});
		userGroup.storeTemplatesAdvanceSearch = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=users-template-advance-search',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});
		userGroup.storeTemplatesAdvanceResult = new Ext.data.JsonStore({
			url:'php/grid_data.php?tipo=users-template-advance-result',
			root: 'data',
			autoLoad:true,
			totalProperty: 'total',
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'}
			]
		});

		userGroup.pager = new Ext.PagingToolbar({
			store: userGroup.store,
			displayInfo: true,
			displayMsg: '{0} - {1} of {2} Registros',
			emptyMsg: 'No hay Registros Disponibles',
			pageSize: BK_data.limitGrid,
			items:(userAdministra.indexOf(userid)!=-1)?[

			{
				xtype	:'button',
				iconCls	:'x-icon-save',
				handler	: userGroup.save
			},{
				xtype	:'button',
				iconCls	:'x-icon-cancel',
				handler	: userGroup.cancel
			},
			'|',
			{
				text	:	'Document Manager',
				iconCls	:	'x-icon-editGrid',
				menu	:	new Ext.menu.Menu({
					items	: [
						new Ext.menu.Item({
							text	:	'Upload Set',
							iconCls	:	'x-icon-upload-document',
							handler	:	userGroup.managerContracts.init
						}),
						new Ext.menu.Item({
							text		:	'Replace/Delete Contracts',
							iconCls		:	'x-icon-change-document',
							handler		:	function(){
								userGroup.replaceAdditionalAndContractDocuments('Replace Contracts Document(s)',userGroup.storeContracts,'replace-delete-contract-by-name','Contracts');
							}
						}),
						new Ext.menu.Item({
							text		:	'Replace/Delete Additional Documents',
							iconCls		:	'x-icon-change-document',
							handler		:	function(){
								userGroup.replaceAdditionalAndContractDocuments('Replace Additional Document(s)',userGroup.storeAddons,'replace-delete-addons-by-name','Additionals');
							}
						}),
						'-',
						new Ext.menu.Item({
							text		:	'Advance Search',
							iconCls		:	'x-icon-see-file',
							handler		:	function(){
								userGroup.replaceAdvanceSearchAndResult(userGroup.storeTemplatesAdvanceSearch,'XX-3','Advance Search','Manager Advance Search');
							}
						}),
						new Ext.menu.Item({
							text		:	'Advance Result',
							iconCls		:	'x-icon-see-file1',
							handler		:	function(){
								userGroup.replaceAdvanceSearchAndResult(userGroup.storeTemplatesAdvanceResult,'XX-4','Advance Result','Manager Advance Result');
							}
						}),'-',
						new Ext.menu.Item({
							text		:	'Special Filters',
							//iconCls		:	'x-icon-see-file1',
							menu		:	{
								items:	[
									new Ext.menu.Item({
										text		:	'By Additional Document',
										//iconCls		:	'x-icon-see-file1',
										handler		:	function(){
											userGroup.specialFilters.init("byAdditionalDocument");
										}
									})
								]
							}
						})
					]
				})
			},{
				text	:	'User Options',
				//iconCls	:	'x-icon-editGrid',
				icon : 'http://reifax.com/img/user.png',
				menu	:	new Ext.menu.Menu({
					items	: [
						new Ext.menu.Item({
							text	: 'Add',
							iconCls	:'x-icon-addUser',
							handler	: userGroup.newUser.init
						}),
						new Ext.menu.Item({
							text	: 'Remove from Group',
							iconCls	:'x-icon-deleteUser',
							handler	: userGroup.deleteUser.init
						}),
						new Ext.menu.Item({
							text	: 'Move',
							iconCls	:'x-icon-assignUser',
							handler	: userGroup.moveUser.init
						}),
						new Ext.menu.Item({
							text	: 'Assign',
							iconCls	:'x-icon-addUser',
							handler	: userGroup.copyUser.init
						}),
						new Ext.menu.Item({
							text	: 'Assign Group',
							//iconCls	:'x-icon-assignUser',
							icon : 'http://reifax.com/img/icons_jesus/Groups-Meeting-Light-16.png',
							handler	: userGroup.massAssignUser.init
						})
					]
				})
			},
			'|'
			,{
				xtype:'form',
				id:'filterUser',
				width: 240,
				layout :'column',
				padding: 3,
				style	:{
					background:'none'
				},
				border: false,
				items:[{
					layout: 'form',
					columnWidth: 1,
					labelWidth: 45,
					border: false,
					items:[{
						xtype	: 'checkbox',
						value	: 1,
						name	: 'global',
						fieldLabel	: 'Exclusive',
						width	: 30,
						listeners:{
							'check'	: function (check){
								userGroup.store.load({
									params:{
										//search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
										group	:	check.getValue(),
										node	:	userGroup.currentNode,
										limit	:	BK_data.limitGrid
									}
								});
							}
						}
					}]
				}/*,{
					layout: 'form',
					columnWidth: 0.75,
					labelWidth: 45,
					border: false,
					items:[{
						name		: 'search',
						xtype		: 'textfield',
						fieldLabel	: 'Search',
						width		: 190
					}]
				},{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						userGroup.store.load({
							params:{
								search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
								group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
								node	:	userGroup.currentNode,
								limit	:	BK_data.limitGrid
							}
						});
					}
				}*/,{
					xtype	: 'button',
					text	: 'Config filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						userGroup.filterPanel.show();
						//console.debug(userGroup.filterPanel.find('id', 'filterItems')[0]);
						/*
						userGroup.store.load({
							params:{
								search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
								group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
								node	:	userGroup.currentNode,
								limit	:	BK_data.limitGrid
							}
						});*/
					}
				},{
					xtype	: 'button',
					text	: 'Reset filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						Ext.getCmp('buttonResetFilter').handler(Ext.getCmp('buttonResetFilter'));
						Ext.getCmp('buttonApplyFilter').handler(Ext.getCmp('buttonApplyFilter'));
						//console.debug(userGroup.filterPanel.find('id', 'filterItems')[0]);
						/*
						userGroup.store.load({
							params:{
								search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
								group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
								node	:	userGroup.currentNode,
								limit	:	BK_data.limitGrid
							}
						});*/
					}
				}]
			}]:(userEjecutor.indexOf(userid)!=-1)?[
			'|',
			{
				text	:	'Document Manager',
				iconCls	:	'x-icon-editGrid',
				menu	:	new Ext.menu.Menu({
					items	: [
						new Ext.menu.Item({
							text	:	'Upload Set',
							iconCls	:	'x-icon-upload-document',
							handler	:	userGroup.managerContracts.init
						}),
						new Ext.menu.Item({
							text		:	'Replace/Delete Contracts',
							iconCls		:	'x-icon-change-document',
							handler		:	function(){
								userGroup.replaceAdditionalAndContractDocuments('Replace Contracts Document(s)',userGroup.storeContracts,'replace-delete-contract-by-name','Contracts');
							}
						}),
						new Ext.menu.Item({
							text		:	'Replace/Delete Additional Documents',
							iconCls		:	'x-icon-change-document',
							handler		:	function(){
								userGroup.replaceAdditionalAndContractDocuments('Replace Additional Document(s)',userGroup.storeAddons,'replace-delete-addons-by-name','Additionals');
							}
						}),
						'-',
						new Ext.menu.Item({
							text		:	'Advance Search',
							iconCls		:	'x-icon-see-file',
							handler		:	function(){
								userGroup.replaceAdvanceSearchAndResult(userGroup.storeTemplatesAdvanceSearch,'XX-3','Advance Search','Manager Advance Search');
							}
						}),
						new Ext.menu.Item({
							text		:	'Advance Result',
							iconCls		:	'x-icon-see-file1',
							handler		:	function(){
								userGroup.replaceAdvanceSearchAndResult(userGroup.storeTemplatesAdvanceResult,'XX-4','Advance Result','Manager Advance Result');
							}
						}),'-',
						new Ext.menu.Item({
							text		:	'Special Filters',
							//iconCls		:	'x-icon-see-file1',
							menu		:	{
								items:	[
									new Ext.menu.Item({
										text		:	'By Additional Document',
										//iconCls		:	'x-icon-see-file1',
										handler		:	function(){
											userGroup.specialFilters.init("byAdditionalDocument");
										}
									})
								]
							}
						})
					]
				})
			},'|',{
				text	:	'User Options',
				//iconCls	:	'x-icon-editGrid',
				icon : 'http://reifax.com/img/user.png',
				menu	:	new Ext.menu.Menu({
					items	: [
						new Ext.menu.Item({
							text	: 'Add',
							iconCls	:'x-icon-addUser',
							handler	: userGroup.newUser.init
						}),
						new Ext.menu.Item({
							text	: 'Remove from Group',
							iconCls	:'x-icon-deleteUser',
							handler	: userGroup.deleteUser.init
						}),
						new Ext.menu.Item({
							text	: 'Move',
							iconCls	:'x-icon-assignUser',
							handler	: userGroup.moveUser.init
						}),
						new Ext.menu.Item({
							text	: 'Assign',
							iconCls	:'x-icon-addUser',
							handler	: userGroup.copyUser.init
						}),
						new Ext.menu.Item({
							text	: 'Assign Group',
							//iconCls	:'x-icon-assignUser',
							icon : 'http://reifax.com/img/icons_jesus/Groups-Meeting-Light-16.png',
							handler	: userGroup.massAssignUser.init
						})
					]
				})
			},
			'|'
			,{
				xtype:'form',
				id:'filterUser',
				width: 340,
				layout :'column',
				padding: 3,
				style	:{
					background:'none'
				},
				border: false,
				items:[{
					layout: 'form',
					columnWidth: 0.25,
					labelWidth: 45,
					border: false,
					items:[{
						xtype	: 'checkbox',
						value	: 1,
						name	: 'global',
						fieldLabel	: 'Exclusive',
						width	: 30,
						listeners:{
							'check'	: function (check){
								userGroup.store.load({
									params:{
										search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
										group	:	check.getValue(),
										node	:	userGroup.currentNode,
										limit	:	BK_data.limitGrid
									}
								});
							}
						}
					}]
				},{
					layout: 'form',
					columnWidth: 0.75,
					labelWidth: 45,
					border: false,
					items:[{
						name		: 'search',
						xtype		: 'textfield',
						fieldLabel	: 'Search',
						width		: 190
					}]
				},{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						userGroup.store.load({
							params:{
								search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
								group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
								node	:	userGroup.currentNode,
								limit	:	BK_data.limitGrid
							}
						});
					}
				}]
			}
				]:[{
				xtype:'form',
				id:'filterUser',
				width: 340,
				layout :'column',
				padding: 3,
				style	:{
					background:'none'
				},
				border: false,
				items:[{
					layout: 'form',
					columnWidth: 0.25,
					labelWidth: 45,
					border: false,
					items:[{
						xtype	: 'checkbox',
						value	: 1,
						name	: 'global',
						fieldLabel	: 'Exclusive',
						width	: 30,
						listeners:{
							'check'	: function (check){
								userGroup.store.load({
									params:{
										search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
										group	:	check.getValue(),
										node	:	userGroup.currentNode,
										limit	:	BK_data.limitGrid
									}
								});
							}
						}
					}]
				},{
					layout: 'form',
					columnWidth: 0.75,
					labelWidth: 45,
					border: false,
					items:[{
						name		: 'search',
						xtype		: 'textfield',
						fieldLabel	: 'Search',
						width		: 190
					}]
				},{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						userGroup.store.load({
							params:{
								search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
								group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
								node	:	userGroup.currentNode,
								limit	:	BK_data.limitGrid
							}
						});
					}
				}]
			}]
		});

        var textField = new Ext.form.TextField();
        var numberField = new Ext.form.NumberField({allowBlank:false});
		var dateField=new Ext.form.DateField();
		var timeField =  new Ext.form.TimeField( {
				forceSelection:true,
				minValue: '8:00 AM',
				maxValue: '6:00 PM',
				increment: 30});

		var opcionesCombo= [
        ['1', 'Pending'],
        ['2', 'Created'] ,
        ['3', 'Redo'],
        ['4', 'Checked'] ,
        ['5', 'Uploaded ']];

		var opcionesStatus= {
			1	:	'Pending',
			2	:	'Created',
			3	:	'Redo',
			4	:	'Checked',
			5	:	'Uploaded'
		}

		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});

		userGroup.storeCamptit=	new Ext.data.Store({
			id			:	'id',
			autoLoad	:	true,
			proxy		:	userGroup.dataProxy,
			baseParams	:	{
				accion	:	'listCamptit'
			},
			reader				:	new Ext.data.JsonReader({
				root			:	'camptit',
				totalProperty	:	'total'
			},
				userGroup.recordCamptit
			)
		});

		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});


		userGroup.store.load({
			params:{
				limit	:	BK_data.limitGrid
			}
		});
		userGroup.columnMode = new Ext.grid.ColumnModel( userGroup.cmDefault);

		userGroup.pager.on('beforechange',function(bar,params){
				params.node=userGroup.currentNode;
				params.group=Ext.getCmp('filterUser').getForm().findField("global").getValue();
				//params.search=Ext.getCmp('filterUser').getForm().findField("search").getValue();
		});
		userGroup.grid = new Ext.grid.EditorGridPanel({
			//height: Ext.getBody().getViewSize().height-50,
			//width:Ext.getBody().getViewSize().width-200,
            store: userGroup.store,
			tbar: userGroup.pager,
			cm: userGroup.columnMode,
            border: false,
			viewConfig: {
				//forceFit:true
			},
			listeners: {
				afteredit:function (model){
					if(model.field=='name' || model.field=='surname' || model.field=='phone' || model.field=='email'){
						Ext.Ajax.request({
							url : 'php/funcionesUserGroup.php',
							params :
								{
									value	: model.value,
									field	: model.field,
									userid	: model.record.data.userid,
									accion	: 'updateFeaturesEach'
								},
							scope:this,
							success : function(response,json) {
								var temp	=	Ext.util.JSON.decode(response.responseText);
								if(temp.success)
									model.record.commit();
								else{
									Ext.Msg.show({
										title:'Info',
										msg: temp.error,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.WARNING
									});
								}
							}
						});
					}else if(model.field=='active'){
						/******

						*********/
						var form = new Ext.form.FormPanel({
								frame : true,
								border :false,
								bodyStyle :{
									background : '#FFF'
								},
								padding: 10,
								layout:'form',

								items : [
									{
										xtype: 'combo',
										store: new Ext.data.SimpleStore({
											fields: ['userid', 'name'],
											data : Ext.combos_selec.dataUsersAdmin
										}),
										hiddenName: 'authorized',
										valueField: 'userid',
										displayField:'name',
										fieldLabel:	'Authorized',
										allowBlank:false,
										value:'NULL',
										typeAhead: true,
										triggerAction: 'all',
										mode: 'local',
										selectOnFocus:true,
										 style: {
											fontSize: '16px'
										},
										width:400
									}
								],
								buttons:[{
										xtype 	: 'button',
										text	: 'Process',
										scope	: this,
										handler	: function ()
										{
											if(form.getForm().isValid())
											{
												form.getForm().submit({
													waitTitle:'Recording',
													waitMsg :'It is processing...',
													url: 'php/funcionesUserGroup.php',
													scope : this,
													params :{
														accion: 'updateActive',
														value : model.value,
														assing : model.record.data.id
													},
													success	: function (form,action)
													{
														win.destroy();
													}
												})
											}
										}
									}]
							});

							win= new Ext.Window({
								layout: 'fit',
								title: 'Authorized',
								closable: true,
								closeAction: 'hide',
								resizable: false,
								maximizable: false,
								plain: true,
								border: false,
								width: 550,
								height: 155,
								items	:[
									form
								]
							});
							win.show();
						/************

						*************/

					}else{
						Ext.Ajax.request({
							url : 'php/funcionesUserGroup.php',
							params :
								{
									value	: model.value,
									field	: model.field,
									userid	: model.record.data.userid,
									accion	: 'updateFeaturesEach'
								},
							scope:this,
							success : function(response,json) {
								var temp	=	Ext.util.JSON.decode(response.responseText);
								if(temp.success)
									model.record.commit();
								else{
									Ext.Msg.show({
										title:'Info',
										msg: temp.error,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.WARNING
									});
								}
							}
						});
					}
				}
			},
            stripeRows: true ,
			sm:userGroup.sm,
			loadMask: true,
			border :false
        });
		userGroup.loaderGroups = new Ext.tree.TreeLoader({		//Paso 1
			url			: 'php/funcionesUserGroup.php',
			baseParams	: {'accion':'getGroups'}
		});
		userGroup.comboFilter = function (id){
			return new Ext.form.ComboBox({
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				id:id,
				width:80,
				value:1,
				mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: [
						'myId',
						'displayText'
					],
					data: [[1, 'Exact'], [2, 'Contains']]
				}),
				valueField: 'myId',
				displayField: 'displayText'
			});
		}
		userGroup.customCombo = function (data){
			var id=data.id==null?Ext.id():data.id;
			return new Ext.form.ComboBox({
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				id:id,
				hiddenName:id,
				width:80,
				value:null,
				mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: [
						'myId',
						'displayText'
					],
					data: data.values
				}),
				valueField: 'myId',
				displayField: 'displayText'
			});
		}
		userGroup.comboYesOrNo = function (id){
			return new Ext.form.ComboBox({
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				id:id,
				hiddenName:id,
				width:50,
				mode: 'local',
				value: null,
				store: new Ext.data.ArrayStore({
					fields: [
						'myId',
						'displayText'
					],
					data: [[0, 'No'], [1, 'Yes'] ]
				}),
				valueField: 'myId',
				displayField: 'displayText'
			});
		}
		userGroup.filterToSend = '{}';
		userGroup.filterPanel = new Ext.Window({
			title: 'Configure Filters',
			width: 400,
			//autoWidth:true,
			//autoWidth:true,
			maximizable: false,
			closeAction: 'hide',
			modal: true,
			resizable: true,
			maximized: false,
			autoScroll: true,
			constrain: true,
			items:[{
				xtype:'form',
				//layout:'form',
				hideLabel:true,
				hideLabels:true,
				padding:'10 10 10 10',
				id:'filterItems'
			}],
			buttons:[{
				text:'Apply',
				id:'buttonApplyFilter',
				handler:function(){
					userGroup.filterToSend=Ext.encode(Ext.getCmp('filterItems').getForm().getValues());
					userGroup.filterPanel.hide();
					lastOptions = userGroup.store.lastOptions;
					Ext.apply(lastOptions.params, {
					    specialFilters:userGroup.filterToSend,
							group:Ext.getCmp('filterUser').getForm().findField("global").getValue()
					});
					userGroup.store.reload(lastOptions);
				}
			},{
				text:'Reset',
				id:'buttonResetFilter',
				handler:function(){
					Ext.getCmp('filterItems').getForm().reset();
				}
			}],
			listeners:{
				'afterrender':function(el){
					//el.doLayout();
					//el.render();
				}
			}
		});
		userGroup.treeGroups = new Ext.tree.TreePanel({
			useArrows: true,
			autoScroll: true,
			animate: true,
			enableDD: true,
			containerScroll: true,
			border: false,
			//dataUrl: 'php/funcionesUserGroup.php',
			loader:userGroup.loaderGroups,
			listeners:{
				'click'	: function (node){
					Ext.getCmp('filterItems').removeAll();
					userGroup.currentNode=node.id;
					userGroup.currentNodeText=node.text;
					if(node.id!='All'){
						Ext.Ajax.request({
							url : 'php/funcionesUserGroup.php',
							params :
								{
									id : userGroup.currentNode,
									accion	:'loadConfigGrid'
								},
							scope:this,
							success : function(response) {
								var combo = new Ext.form.ComboBox({
									typeAhead: true,
									triggerAction: 'all',
									lazyRender:true,
									mode: 'local',
									store: new Ext.data.ArrayStore({
										id: 0,
										fields: [
											'myId',
											'displayText'
										],
										data: [[1, 'Yes'], [0, 'No']]
									}),
									valueField: 'myId',
									displayField: 'displayText'
								});
								var comboActive = new Ext.form.ComboBox({
									typeAhead: true,
									triggerAction: 'all',
									lazyRender:true,
									mode: 'local',
									store: new Ext.data.ArrayStore({
										fields: [
											'myId',
											'displayText'
										],
										data: [[1, 'Yes'], [2, 'No']]
									}),
									valueField: 'myId',
									displayField: 'displayText'
								});
								var data=Ext.util.JSON.decode(response.responseText);
								var cm=[
										new Ext.grid.RowNumberer(),
										userGroup.sm,
									{
										header: 'USERID',
										dataIndex: 'userid',
										sortable: true,
										renderer:function (val){
											return '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+val+')">'+val+'</a>';
										},
										width: 50
									},{
										xtype: 'actioncolumn',
										width: 105,
										items: [{
											icon   : 'http://i2.esmas.com/canal30/img/img_icon_doc.gif',
											tooltip: 'View History',
											handler: function(grid, rowIndex, colIndex) {
												var rec = grid.getStore().getAt(rowIndex);
												userGroup.viewHistory(rec.data);
											}
										},{
											icon   : 'http://icons.iconarchive.com/icons/custom-icon-design/flatastic-8/16/Open-folder-search-icon.png',
											tooltip: 'View Documents',
											handler: function(grid, rowIndex, colIndex) {
												var rec = grid.getStore().getAt(rowIndex);
												userGroup.viewDocuments(rec.data);
											}
										},{
											icon   : 'http://rfmjournal.com/Content/Shared/Images/icon-search.gif',
											tooltip: 'View Advance Search\'s and Advance Result\'s',
											handler: function(grid, rowIndex, colIndex) {
												var rec = grid.getStore().getAt(rowIndex);
												userGroup.viewAdvanceSearchAndResult.init(rec.data);
											}
										},{
											icon   : '/backoffice/images/login_icon.png',                // Use a URL in the icon config
											tooltip: 'Log in',
											handler: function(grid, rowIndex, colIndex) {
												var rec = grid.getStore().getAt(rowIndex);
												// Basic request
												if(Ext.util.Cookies.get('login')){
													Ext.Ajax.request({
													   url: '/resources/php/properties_releaseSess.php',
													   success: function (){
															Ext.Ajax.request({
															   url: '/resources/php/properties_validacion.php',
															   success: function (){
																	window.open("/");
																},
																method:'POST',
															   failure: function (){

																},
															   params: { userid: rec.get('userid') }
															});
														},
														method:'POST',
													   failure: function (){

														},
													   params: { userid: rec.get('userid') }
													});
												}else{
													Ext.Ajax.request({
													   url: '/resources/php/properties_validacion.php',
													   success: function (){
															window.open("/");
														},
														method:'POST',
													   failure: function (){

														},
													   params: { userid: rec.get('userid') }
													});
												}
											}
										}]
									},{
										header		: 'Name',
										dataIndex	: 'name',
										sortable	: true,
										editor		: (userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
										width		: 100
									},{
										header		: 'SurName',
										dataIndex	: 'surname',
										sortable	: true,
										editor		:(userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
										width		: 110
									},{
										header		: 'Phone',
										dataIndex	: 'phone',
										sortable	: true,
										editor		: (userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
										width		: 100
									},
									{
										header		: 'Email',
										dataIndex	: 'email',
										sortable	: true,
										editor		: (userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
										width		: 200
									},
									{
										header: 'Status',
										dataIndex: 'statusUsr',
										sortable: true,
										width: 100
									},
									{
										header		: 'Active',
										dataIndex	: 'active',
										editor		: (userAdministra.indexOf(userid)!=-1)?comboActive:null,
										width		: 50,
										sortable	: true,
										renderer	: function(val){
												var record = comboActive.findRecord(comboActive.valueField, val);
												return record ? record.get(comboActive.displayField) : comboActive.valueNotFoundText;
										}
									}
								];
								var formPanelFilterItems=userGroup.filterPanel.find('id', 'filterItems')[0];
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Name'
									},userGroup.comboFilter('filter-name'),{
										xtype:'textfield',
										id:'value-name'
									}]
								});
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Surname'
									},userGroup.comboFilter('filter-surname'),{
										xtype:'textfield',
										id:'value-surname'
									}]
								});
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Phone'
									},userGroup.comboFilter('filter-phone'),{
										xtype:'textfield',
										id:'value-phone'
									}]
								});
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Email'
									},userGroup.comboFilter('filter-email'),{
										xtype:'textfield',
										id:'value-email'
									}]
								});
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Status'
									},userGroup.comboFilter('filter-statusUsr'),{
										xtype:'textfield',
										id:'value-statusUsr'
									}]
								});
								formPanelFilterItems.add({
									xtype:'compositefield',
									padding:'10 0 0 0',
									labelWidth: 0,
									items:[{
										xtype:'label',
										text:'Active'
									},{
										xtype:'hidden',
										id:'filter-active'
									},userGroup.comboYesOrNo('value-active')]
								});
								Ext.each(data.col,function(data){
									//console.debug(data);
									if(data.feature!=''){
										/*if(data.camptit=="XX-1" || data.camptit=="XX-3"){
											cm.push({
												header:data.feature,
												control:data.camptit,
												dataIndex:data.id_feature,
												editor:false,
												//width: userGroup.letterWidth(data.feature,80),
												width: 25,
												tooltip:data.feature,
												sortable:true,
												renderer: function(value, metaData, record, rowIndex, colIndex, store) {
													//cm.setColumnWidth(colIndex, 20, false);
													//var valueCompare	=	userGroup.columnMode.getColumnAt(colIndex).control;	//Example valueCompare=="XX-1"
													if(value<=0){
														var text="NO"
														//var colorT="red";
														var colorF="FFB0C4";
													}else if(value==1){
														var text="YES"
														//var colorT="green";
														var colorF="B0FFC5";
													}else if(value>1){
														var text="YES"
														//var colorT="yellow";
														var colorF="DFFF7B";
													}
													metaData.attr = 'ext:qtip="' + data.feature + ', Total: ' + value + '"';
													return	'<div class="x-grid3-cell-inner" style="background-color:#'+colorF+';">'+
																//'<span style="color:'+colorT+';">'+
																//	text+' Count('+value+')'+
																//'</span>'+
																'&nbsp;'+
															'</div>';
												}
											});
										}else if(data.camptit!==null){*/
										if(data.type==1){
											if(data.camptitType=='boolean'){
												if(data.camptit=='847'){
													console.debug(1,Ext.id());
													cm.push({
														header:data.feature,
														control:data.camptit,
														dataIndex:data.id_feature,
														editor: (userAdministra.indexOf(userid)!=-1)?userGroup.customCombo({
															id:null,
															values:[[1, 'Monthly'], [2, 'Annually']]
														}):null,
														width: userGroup.letterWidth(data.feature,80),
														sortable:true,
														renderer: function(value, metaData, record, rowIndex, colIndex, store) {
															/*if (value == 0) {
																metaData.attr = 'style="background-color:#FFB0C4;"';
															}else{
																metaData.attr = 'style="background-color:#B0FFC5;"';
															}*/
															var combo = userGroup.customCombo({
																id:null,
																values:[[1, 'Monthly'], [2, 'Annually']]
															});
															var record = combo.findRecord(combo.valueField, value);
															return record ? record.get(combo.displayField) : combo.valueNotFoundText;
														}
													});
													formPanelFilterItems.add({
														xtype:'compositefield',
														padding:'10 0 0 0',
														labelWidth: 0,
														items:[{
															xtype:'label',
															text:data.feature
														},{
															xtype:'hidden',
															id:'filter-'+data.id_feature
														},userGroup.customCombo({
															id:'value-'+data.id_feature,
															values:[[1, 'Monthly'], [2, 'Annually']]
														})]
													});
												}else{
													cm.push({
														header:data.feature,
														control:data.camptit,
														dataIndex:data.id_feature,
														editor: (userAdministra.indexOf(userid)!=-1)?combo:null,
														width: userGroup.letterWidth(data.feature,80),
														sortable:true,
														renderer: function(value, metaData, record, rowIndex, colIndex, store) {
															if(value==0){
																var colorF="FFB0C4";
															}else if(value==1){
																var colorF="B0FFC5";
															}
															console.debug(record.get(combo.displayField));
															var record = combo.findRecord(combo.valueField, value);
															return	'<div class="x-grid3-cell-inner" style="background-color:#'+colorF+';">'+
																		(record ? record.get(combo.displayField) : combo.valueNotFoundText) +
																	'</div>';
														}
													});
													formPanelFilterItems.add({
														xtype:'compositefield',
														padding:'10 0 0 0',
														labelWidth: 0,
														items:[{
															xtype:'label',
															text:data.feature
														},{
															xtype:'hidden',
															id:'filter-'+data.id_feature
														},userGroup.comboYesOrNo('value-'+data.id_feature)]
													});
												}
											}else{
												cm.push({
													header:data.feature,
													control:data.camptit,
													dataIndex:data.id_feature,
													editor: (userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
													width: userGroup.letterWidth(data.feature,80),
													sortable:true,
													renderer: function(value, metaData, record, rowIndex, colIndex, store) {
														return value;
													}
												});
												formPanelFilterItems.add({
													xtype:'compositefield',
													padding:'10 0 0 0',
													labelWidth: 0,
													items:[{
														xtype:'label',
														text:data.feature
													},userGroup.comboFilter('filter-'+data.id_feature),{
														xtype:'textfield',
														id:'value-'+data.id_feature
													}]
												});
											}
										}else if(data.type==2){
											cm.push({
												header:data.feature,
												tooltip:data.feature,
												control:data.camptit,
												dataIndex:data.id_feature,
												width: 25,
												sortable:true,
												renderer: function(value, metaData, record, rowIndex, colIndex, store) {
													metaData.attr = 'ext:qtip="' + record.data.camptitCondition + '"';
													if(value<=0){
														var colorF="FFB0C4";
													}else if(value==1){
														var colorF="B0FFC5";
													}
													return	'<div class="x-grid3-cell-inner" style="background-color:#'+colorF+';">'+
																'&nbsp;'+
															'</div>';
												}
											});
											formPanelFilterItems.add({
												xtype:'compositefield',
												padding:'10 0 0 0',
												labelWidth: 0,
												items:[{
													xtype:'label',
													text:data.feature
												},{
													xtype:'hidden',
													id:'filter-'+data.id_feature
												},userGroup.comboYesOrNo('value-'+data.id_feature)]
											});
										}else if(data.type==4){
											cm.push({
												header:data.feature,
												control:data.camptit,
												dataIndex:data.id_feature,
												editor: (userAdministra.indexOf(userid)!=-1)?new Ext.form.TextField({allowBlank: false}):null,
												width: userGroup.letterWidth(data.feature,80),
												sortable:true,
												renderer: function(value, metaData, record, rowIndex, colIndex, store) {
													return value;
												}
											});
											formPanelFilterItems.add({
												xtype:'compositefield',
												padding:'10 0 0 0',
												labelWidth: 0,
												items:[{
													xtype:'label',
													text:data.feature
												},userGroup.comboFilter('filter-'+data.id_feature),{
													xtype:'textfield',
													id:'value-'+data.id_feature
												}]
											});
										}else if(data.type==3){
											cm.push({
												header:data.feature,
												control:data.camptit,
												dataIndex:data.id_feature,
												editor: (userAdministra.indexOf(userid)!=-1)?combo:null,
												width: userGroup.letterWidth(data.feature,80),
												sortable:true,
												renderer: function(value, metaData, record, rowIndex, colIndex, store) {
													var record = combo.findRecord(combo.valueField, value);
													return record ? record.get(combo.displayField) : combo.valueNotFoundText;
												}
											});
											formPanelFilterItems.add({
												xtype:'compositefield',
												padding:'10 0 0 0',
												labelWidth: 0,
												items:[{
													xtype:'label',
													text:data.feature
												},{
													xtype:'hidden',
													id:'filter-'+data.id_feature
												},
													userGroup.comboYesOrNo('value-'+data.id_feature)
												]
											});
										}
									}
								});
								cm.push({
									header: 'Group',
									dataIndex: 'group',
									sortable: true,
									width: 80
								},{
									header: 'Group date added',
									dataIndex: 'dateRecord',
									renderer: Ext.util.Format.dateRenderer('Y/m/d'),
									sortable: true,
									width: 100
								});
								userGroup.columnMode.setConfig(cm);
								//formPanelFilterItems.render();
							}
						});
					}else{
						var cm=userGroup.cmDefault;
						userGroup.columnMode.setConfig(cm);
					}
					userGroup.store.load({
						params:{
							node	:	node.id,
							group	:	Ext.getCmp('filterUser').getForm().findField("global").getValue(),
							specialFilters:userGroup.filterToSend,
							limit	:	BK_data.limitGrid
						}
					});
				}
			},
			root: {
				nodeType: 'async',
				text: 'All',
				draggable: false,
				id: 'All'
			},
			tbar:{
				defaults:{scope:this},
				items: (userAdministra.indexOf(userid)!=-1)?[
					{
						xtype	:'button',
						iconCls	:'x-icon-add',
						tooltip	:'Add Group',
						handler	:userGroup.newGroup.init
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-cancel',
						tooltip	: 'Delete Group',
						handler	: userGroup.delete
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-config',
						tooltip	: 'Group Features',
						handler	: userGroup.configGroup.init
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-edit-text',
						tooltip	: 'Edit Group',
						handler	: userGroup.editGroup.init
					},
					{
						xtype	: 'button',
						iconCls	: 'x-icon-exportExcel',
						tooltip	: 'Export To Excel',
						handler	: userGroup.downloadExcel
					},
					'|'
					,
					{
						xtype	: 'button',
						iconCls	: 'x-icon-editGrid',
						tooltip	: 'Feature Manager',
						handler	: userGroup.maganerFeacture.init
					}
				]:(userEjecutor.indexOf(userid)!=-1)?[
					{
						xtype	: 'button',
						iconCls	: 'x-icon-exportExcel',
						tooltip	: 'Export To Excel',
						handler	: userGroup.downloadExcel
					}
				]:[]
			}
		});
		userGroup.footer	=	new Ext.Toolbar({
			id:"footerGrouping",
			items:['-']
		});
		userGroup.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [{
				region		:	'north',
				height		:	25,
				items		:	Ext.getCmp('menu_page')
			},{
				region		:	'west',
				width		:	200,
				maxSize		:	250,
				collapsible :	true,
				title		:	'User Groups',
				border		:	true,
				items		:	userGroup.treeGroups
			},{
				region			:	'center',
				deferredRender	:	false,
				layout			:	"fit",
				items			:	userGroup.grid,
				//height	:	Ext.getBody().getViewSize().height-50
			},{
				region		:	'south',
				items		:	userGroup.footer,
				height		:	25
			}]
		});

	},
		sm:sm
	,
	cmDefault:[
		new Ext.grid.RowNumberer(),
			sm,
		{
			header: 'USERID',
			dataIndex: 'userid',
			sortable: true,
			width: 50
		},{
			header		: 'Name',
			dataIndex	: 'name',
			sortable	: true,
			editor		: new Ext.form.TextField({allowBlank: false}),
			width		: 100
		},{
			header		: 'SurName',
			dataIndex	: 'surname',
			sortable	: true,
			editor		: new Ext.form.TextField({allowBlank: false}),
			width		: 100
		},{
			header		: 'Phone',
			dataIndex	: 'phone',
			sortable	: true,
			editor		: new Ext.form.TextField({allowBlank: false}),
			width		: 100
		},
		{
			header		: 'Email',
			dataIndex	: 'email',
			sortable	: true,
			editor		: new Ext.form.TextField({allowBlank: false}),
			width		: 200
		},{
		header: 'Group',
		dataIndex: 'group',
		sortable: true,
		width: 80
	},{
		header: 'Group date added',
		dataIndex: 'dateRecord',
		renderer: Ext.util.Format.dateRenderer('Y/m/d'),
		sortable: true,
		width: 80
	}],
	recordModules : new Ext.data.Record.create([
			{name: 'id', type: 'int'},
			{name: 'name', type: 'string'},
			{name: 'description', type: 'string'}
	]),
	recordFeaturesType : new Ext.data.Record.create([
			{name: 'id', type: 'int'},
			{name: 'name', type: 'string'},
			{name: 'desc', type: 'string'}
	]),
	recordCamptit : new Ext.data.Record.create([
			{name: 'id', type: 'string'},
			{name: 'tabla', type: 'string'},
			{name: 'campos', type: 'string'},
			{name: 'description', type: 'string'}
	]),
	recordFeaturesAssing : new Ext.data.Record.create([
			{name: 'id', type: 'int'},
			{name: 'id_assing', type: 'int'},
			{name: 'id_feature', type: 'int'},
			{name: 'order', type: 'int'}
	]),
	recordFeatures	:new Ext.data.Record.create([
			{name: 'id', type: 'int'},
			{name: 'camptit', type: 'string'},
			{name: 'type', type: 'string'},
			{name: 'feature', type: 'string'},
			{name: 'name', type: 'string'},
			{name: 'camptitCondition', type: 'string'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({
			url: 'php/funcionesUserGroup.php',   // Servicio web
			timeout: 0,
			method: 'POST'                      // Método de envío
	}),
	save	:function ()
	{
		var grid=userGroup.grid;
		var modified = grid.getStore().getModifiedRecords();

		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){
			var recordsToSend = [];
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));
			});

			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();

			recordsToSend = Ext.encode(recordsToSend);

			Ext.Ajax.request({
				url : 'php/funcionesUserGroup.php',
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},
				scope:this,
				success : function(response) {
					grid.el.unmask();
					grid.getStore().commitChanges();
			   		userGroup.grid.getStore().reload();
				}
			});
		}

	},
	reset: function (){
		var cols=userGroup.grid.getSelectionModel( ).getSelections( );
	},
	cancel: function ()
	{
		userGroup.grid.getStore().rejectChanges();
	},
	delete:function (){
		Ext.Msg.show({
		   title:'Delete Group?',
		   msg: 'Sure you want to delete the group "'+userGroup.currentNodeText+'"?',
		   buttons: Ext.Msg.YESNO,
		   fn: function (btn){
			   if(btn == 'yes'){
					Ext.Ajax.request({
						method :'POST',
						url: 'php/funcionesUserGroup.php',
						success: function (){
							userGroup.treeGroups.getRootNode().reload();
							if(userGroup.newGroup.storeModule)
								userGroup.newGroup.storeModule.load();
						},
						failure:  function (){

						},
						 params: {
							accion: 'deleteGroup',
							id	: userGroup.currentNode
						}
					});
			   }
			},
		   icon: Ext.MessageBox.QUESTION
		});
	},
	deleteUser:{
		init:	function (){
			if(userGroup.deleteUser.win)
				{
					userGroup.deleteUser.form.getForm().reset();
					userGroup.deleteUser.win.show();
				}
				else{

					userGroup.deleteUser.form = new Ext.form.FormPanel({
						frame : true,
						border :false,
						bodyStyle :{
							background : '#FFF'
						},
						padding: 10,
						layout:'form',

						items : [
							{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsersAdmin
								}),
								hiddenName: 'authorized',
								valueField: 'userid',
								displayField:'name',
								fieldLabel:	'Authorized',
								allowBlank:false,
								value:'NULL',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								 style: {
									fontSize: '16px'
								},
								width:400
							}
						],
						buttons:[{
								xtype 	: 'button',
								text	: 'Delete',
								handler	: function ()
								{
									if(userGroup.deleteUser.form.getForm().isValid())
									{

											data=userGroup.grid.getSelectionModel().getSelections();
											if(data.length){
												var ids='';
												for(i=0;i<data.length;i++){
													ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
												}
											}
											userGroup.deleteUser.form.getForm().submit({
											waitTitle:'Recording',
											waitMsg :'It is processing...',
											url: 'php/funcionesUserGroup.php',
											scope : this,
											params :{
												accion: 'deleteUser',
												current : ids
											},
											success	: function (form,action)
											{
												userGroup.deleteUser.form.getForm().reset();
												userGroup.deleteUser.win.hide();

											   userGroup.grid.getStore().reload();
											}
										})
									}
								}
							},
							{
								xtype 	: 'button',
								text	: 'Cancel',
								handler : function ()
								{
									userGroup.deleteUser.form.getForm().reset();
									userGroup.deleteUser.win.hide();
								}
							}]
					});

					userGroup.deleteUser.win= new Ext.Window({
						layout: 'fit',
						title: 'Delete Users',
						closable: true,
						closeAction: 'hide',
						resizable: false,
						maximizable: false,
						plain: true,
						border: false,
						width: 550,
						height: 155,
						items	:[
							userGroup.deleteUser.form
						]
					});
					userGroup.deleteUser.win.show();
				}

			}
		}
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	moveUser:{
		init : function (){
			if(userGroup.newGroup.win)
			{
				userGroup.moveUser.form.getForm().reset();
				userGroup.moveUser.win.show();
			}
			else{

				var readerModule = new Ext.data.JsonReader({
					root: 'modules',
					totalProperty: 'total'
					},
					userGroup.recordModules
					);

				userGroup.moveUser.storeModule = new Ext.data.Store({
					id: 'id',
					proxy: userGroup.dataProxy,
					baseParams: {
						accion	:'listModule'
					},
					reader: readerModule
				});

				var comboModule = new Ext.form.ComboBox({
					forceSelection:true,
					store: userGroup.moveUser.storeModule,
					displayField:'name',
					hiddenName	:'group',
					typeAhead	: true,
					mode		: 'remote',
					fieldLabel: 'Group',
					valueField:'id',
					editable	:false,
					triggerAction: 'all',
					emptyText:'',
					width: 400,
					selectOnFocus:true
				});
				userGroup.moveUser.storeModule.load();

				userGroup.moveUser.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',

					items : [
						comboModule,
						{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsersAdmin
							}),
							hiddenName: 'authorized',
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Authorized',
							allowBlank:false,
							value:'NULL',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
							 style: {
								fontSize: '16px'
							},
							width:400
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Move',
							handler	: function ()
							{
								if(userGroup.moveUser.form.getForm().isValid())
								{

										data=userGroup.grid.getSelectionModel().getSelections();
										if(data.length){
											var ids='';
											for(i=0;i<data.length;i++){
												ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
											}
										}
										userGroup.moveUser.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing...',
										url: 'php/funcionesUserGroup.php',
										scope : this,
										params :{
											accion : 'moveUsers',
											current : ids
										},
										success	: function (form,action)
										{
											userGroup.moveUser.form.getForm().reset();
											userGroup.moveUser.win.hide();

											userGroup.store.load({
												params:{
													//search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
													node	:	userGroup.currentNode,
													specialFilters:userGroup.filterToSend,
													limit	:	BK_data.limitGrid
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.moveUser.form.getForm().reset();
							}
						}]
				});

				userGroup.moveUser.win= new Ext.Window({
					layout: 'fit',
					title: 'Move Users',
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 155,
					items	:[
						userGroup.moveUser.form
					]
				});
				userGroup.moveUser.win.show();
			}
		}
	}
	,
	copyUser:{
		init : function (){
			if(userGroup.copyUser.win)
			{
				userGroup.copyUser.form.getForm().reset();
				userGroup.copyUser.win.show();
			}
			else{

				var readerModule = new Ext.data.JsonReader({
					root: 'modules',
					totalProperty: 'total'
					},
					userGroup.recordModules
					);

				userGroup.copyUser.storeModule = new Ext.data.Store({
					id: 'id',
					proxy: userGroup.dataProxy,
					baseParams: {
						accion	:'listModule'
					},
					reader: readerModule
				});

				var comboModule = new Ext.form.ComboBox({
					forceSelection:true,
					store: userGroup.copyUser.storeModule,
					displayField:'name',
					hiddenName	:'group',
					typeAhead	: true,
					mode		: 'remote',
					fieldLabel: 'Group',
					valueField:'id',
					editable	:false,
					triggerAction: 'all',
					emptyText:'',
					width: 400,
					selectOnFocus:true
				});
				userGroup.copyUser.storeModule.load();

				userGroup.copyUser.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',

					items : [
						comboModule,
						{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsersAdmin
							}),
							hiddenName: 'authorized',
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Authorized',
							allowBlank:false,
							value:'NULL',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
							 style: {
								fontSize: '16px'
							},
							width:400
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(userGroup.copyUser.form.getForm().isValid())
								{

										data=userGroup.grid.getSelectionModel().getSelections();
										if(data.length){
											var ids='';
											for(i=0;i<data.length;i++){
												ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
											}
										}
										userGroup.copyUser.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing...',
										url: 'php/funcionesUserGroup.php',
										scope : this,
										params :{
											accion : 'copyUsers',
											current : ids
										},
										success	: function (form,action)
										{
											userGroup.copyUser.form.getForm().reset();
											userGroup.copyUser.win.hide();

											userGroup.store.load({
												params:{
													//search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
													node	:	userGroup.currentNode,
													specialFilters:userGroup.filterToSend,
													limit	:	BK_data.limitGrid
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.copyUser.form.getForm().reset();
							}
						}]
				});

				userGroup.copyUser.win= new Ext.Window({
					layout: 'fit',
					title: 'Add Users',
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 155,
					items	:[
						userGroup.copyUser.form
					]
				});
				userGroup.copyUser.win.show();
			}
		}
	}
	,
	configGroup: {
		init : function (){

			var change=false;
			var sm=new Ext.grid.CheckboxSelectionModel();

			var readerModule = new Ext.data.JsonReader({
				root	: 'features',
				totalProperty	: 'total'
				},
				userGroup.recordFeatures
				);

			var storeModule = new Ext.data.Store({
				proxy: userGroup.dataProxy,
				baseParams: {
					accion	:'listFeatures'
				},
				reader: readerModule
			});

			var comboModule = new Ext.form.ComboBox({
				forceSelection:true,
				store: storeModule,
				displayField:'feature',
				typeAhead: true,
				mode: 'remote',
				valueField:'id',
				editable	:false,
				triggerAction: 'all',
				emptyText:'Select a Status...',
				selectOnFocus:true
			});
			storeModule.load();

			var columnMode = new Ext.grid.ColumnModel(
				[
					sm,
				{
					header: 'Features',
					dataIndex: 'id_feature',
					sortable: true,
					editor	:comboModule,
					width: 335  ,
					renderer: function(val){
							var record = comboModule.findRecord(comboModule.valueField, val);
							return record ? record.get(comboModule.displayField) : comboModule.valueNotFoundText;
					}
				},{
					header: 'Order',
					dataIndex: 'order',
					sortable: true,
					width: 50  ,
					editor: new Ext.form.TextField({allowBlank: false})
				}]
			);

			var gridReader = new Ext.data.JsonReader({
				root: 'features',
				totalProperty: 'total'
				},
				userGroup.recordFeaturesAssing
				);


			var storeAssing = new Ext.data.Store({
				proxy: userGroup.dataProxy,
				baseParams: {
					accion	: 'listFeaturesAssi',
					id		: userGroup.currentNode
				},
				reader: gridReader
			});

			storeAssing.load();
			var grid= new Ext.grid.EditorGridPanel({
					xtype:'editorgrid',
					height: 420,
					store: storeAssing,
					tbar: {
						defaults:{scope:this},
						items:[
							{
								xtype	:'button',
								iconCls	:'x-icon-add',
								handler	: function (){
									var position = storeAssing.getCount();
									var id = Ext.id();
									var defaultData = {
										newRecordId	: id,
										id_assing	: userGroup.currentNode
									};
									var Module = storeAssing.recordType;
									var module = new Module(defaultData,id);
									grid.stopEditing(); //step 3
									grid.getStore().insert(position, module);
									grid.startEditing(position, 1);
								},
								text	: 'New Feature'
							},
							{
								xtype	:'button',
								iconCls	:'x-icon-cancel',
								handler	: function (){
									change=true;
									data=grid.getSelectionModel().getSelections();
										if(data.length){
											var ids='';
											for(i=0;i<data.length;i++){
												ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
											}
										}
										Ext.Ajax.request({
											method :'POST',
											url: 'php/funcionesUserGroup.php',
											success: function (){
											   grid.getStore().reload();
											},
											failure:  function (){

											},
											 params: {
												accion: 'deleteFeactureAssing',
												ids	: ids
											}
										});
								},
								text	: 'Delete Feature'
							},
							{
								xtype	:'button',
								iconCls	:'x-icon-save',
								handler	: function ()
								{
									change=true;
									var modified = grid.getStore().getModifiedRecords();

									if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){
										var recordsToSend = [];
										Ext.each(modified, function(record) {
											recordsToSend.push(Ext.apply({id:record.id},record.data));
										});

										grid.el.mask('Saving...', 'x-mask-loading');
										grid.stopEditing();

										recordsToSend = Ext.encode(recordsToSend);

										Ext.Ajax.request({
											url : 'php/funcionesUserGroup.php',
											params :
												{
													records : recordsToSend,
													accion	:'updateFeactureAssing'
												},
											scope:this,
											success : function(response) {
												grid.el.unmask();
												grid.getStore().commitChanges();
												grid.getStore().reload();
											}
										});
									}

								},
								text	: 'Save Changes'
							}
						]},
					cm: columnMode,
					border: false,
						viewConfig: {
							//forceFit:true
						},
					stripeRows: true,
					sm:sm,
					loadMask: true,
					border :false
				});

			var win= new Ext.Window({
				layout: 'fit',
				title: 'Configuration of Group',
				closable: true,
				closeAction: 'hide',
				resizable: false,
				maximizable: false,
				plain: true,
				border: false,
				width: 420,
				height: 265,
				items	:[
				grid
				],
				listeners:{
					'hide':function(win){
						if(change){
							userGroup.store.load({
								params:{
									limit	:	BK_data.limitGrid
								}
							});
						}
					}
				}
			});
			win.show();
		}
	}
	,
	maganerFeacture:{
		init: function (){

			var comboCamptit	=	new Ext.form.ComboBox({
				store: userGroup.storeCamptit,
				displayField	:	'description',
				id				:	'combounico',
				typeAhead		:	true,
				mode			:	'local',
				valueField		:	'id',
				triggerAction	:	'all',
				width			:	400,
				lazyRender		:	true
			});

			var change=false;
			var sm=new Ext.grid.CheckboxSelectionModel();
			var textField = new Ext.form.TextField();
			var columnMode = new Ext.grid.ColumnModel(
				[sm,
				{
					header		: 'Feature',
					dataIndex	: 'feature',
					sortable	: true,
					renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						metaData.attr = 'ext:qtip="' + value + '"';
						return value;
					}
				},{
					header		: 'Type',
					dataIndex	: 'name',
					width:60,
					sortable	: true,
					renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						metaData.attr = 'ext:qtip="' + value + '"';
						return value;
					}
				},{
					header		: 'Relation with Camptit',
					dataIndex	: 'camptit',
					sortable	: true,
					scope		: this,
					renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						metaData.attr = 'ext:qtip="' + value + '"';
						return value;
					}
				},{
					header		: 'Condition',
					dataIndex	: 'camptitCondition',
					sortable	: true,
					renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						metaData.attr = 'ext:qtip="' + value + '"';
						return value;
					}
				}]
			);
			var gridReader = new Ext.data.JsonReader({
					root	: 'features',
					totalProperty	: 'total'
				},
				userGroup.recordFeatures
				);


			var storeAssing = new Ext.data.Store({
				proxy	: userGroup.dataProxy,
				baseParams	: {
					accion		:'listFeatures'
				},
				reader	: gridReader
			});

			storeAssing.load();
			var grid= new Ext.grid.EditorGridPanel({
				xtype:'editorgrid',
				height: 420,
				store: storeAssing,
				tbar: {
					defaults:{scope:this},
					items:[{
						text	: 'Add Feature',
						iconCls	:'x-icon-add',
						handler	:	function(){
							userGroup.insertCustomFeature(null,grid);
						}
						/*menu: new Ext.menu.Menu({
							items: [
								new Ext.menu.Item({
									text: 'Normal Feature',
									handler	: function (){
										var position = storeAssing.getCount();
										//var id = Ext.id();
										var id = 'NEW-ITEM';
										var defaultData = {
											newRecordId: id
										};
										var Module = storeAssing.recordType;
										var module = new Module(defaultData,id);
										grid.stopEditing(); //step 3
										grid.getStore().insert(position, module);
										grid.startEditing(position, 1);
									}
								}),
								new Ext.menu.Item({
									text	:	'Contract Feature',
									handler	:	function(){
										userGroup.insertCustomFeature(grid,userGroup.storeContracts,'XX-1','Basic Contract\'s','Add new Feature as Contract');
									}
								}),
								new Ext.menu.Item({
									text: 'Aditional Document Feature',
									handler	:	function(){
										userGroup.insertCustomFeature(grid,userGroup.storeAddons,'XX-2','Basic Additional\'s','Add new Feature as Additional Document');
									}
								}),
								new Ext.menu.Item({
									text: 'Advance Search Feature',
									handler	:	function(){
										userGroup.insertCustomFeature(grid,userGroup.storeTemplatesAdvanceSearch,'XX-3','Advance Search','Add new Feature as Advance Search');
									}
								}),
								new Ext.menu.Item({
									text: 'Advance Result Feature',
									handler	:	function(){
										userGroup.insertCustomFeature(grid,userGroup.storeTemplatesAdvanceResult,'XX-4','Advance Result','Add new Feature as Advance Result');
									}
								})
							]
						})*/
					},
					{
						xtype	:'button',
						iconCls	:'x-icon-cancel',
						handler	: function (){
							change=true;
							data=grid.getSelectionModel().getSelections();
								if(data.length){
									var ids='';
									for(i=0;i<data.length;i++){
										ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
									}
								}
								Ext.Ajax.request({
									method :'POST',
									url: 'php/funcionesUserGroup.php',
									success: function (){
									   grid.getStore().reload();
									},
									failure:  function (){

									},
									 params: {
										accion: 'deleteFeatures',
										ids	: ids
									}
								});
						},
						text	: 'Delete Feacture'
					}/*,
					{
						xtype	:'button',
						iconCls	:'x-icon-save',
						handler	: function ()
						{
							change=true;
							var modified = grid.getStore().getModifiedRecords();

							if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){
								var recordsToSend = [];
								Ext.each(modified, function(record) {
									recordsToSend.push(Ext.apply({id:record.id},record.data));
								});

								grid.el.mask('Saving...', 'x-mask-loading');
								grid.stopEditing();

								recordsToSend = Ext.encode(recordsToSend);

								Ext.Ajax.request({
									url : 'php/funcionesUserGroup.php',
									params :
										{
											records : recordsToSend,
											accion	:'updateFeatures'
										},
									scope:this,
									success : function(response) {
										grid.el.unmask();
										grid.getStore().commitChanges();
										grid.getStore().reload();
									}
								});
							}

						},
						text	: 'Save Changes'
					}*/
				]},
				listeners:{
					'afteredit':function(changes){
						var recordsToSend = [];
						recordsToSend.push(Ext.apply({id:changes.record.id},{changes:changes.record.getChanges()}));
						Ext.Ajax.request({
							url		:	'php/funcionesUserGroup.php',
							params	:	{
								records : Ext.encode(recordsToSend),
								accion	:'updateFeatures'
							},
							scope	:	this,
							success :	function(response) {
								grid.el.unmask();
								grid.getStore().commitChanges();
								grid.getStore().reload();
							}
						});
					},
					'rowdblclick':function( grid, rowIndex, e ){
						userGroup.insertCustomFeature(grid.getSelectionModel().getSelected().json,grid);
					}
				},
				cm			:	columnMode,
				border		:	false,
				viewConfig	:	{
					forceFit	:	true
				},
				stripeRows	:	true,
				sm			:	sm,
				loadMask	:	true,
				border		:	false
			});

			var win= new Ext.Window({
				layout: 'fit',
				title: 'Manager Features',
				closable: true,
				//closeAction: 'hide',
				resizable: true,
				maximizable: false,
				plain: true,
				border: false,
				modal:true,
				width: 800,
				height: 265,
				items	:[
				grid
				],
				listeners:{
					'hide':function(win){
						if(change){
							userGroup.store.load({
								params:{
									limit	:	BK_data.limitGrid
								}
							});
						}
					}
				}
			});
			win.show();
		}
	},
	newUser:{
		init:function (){
			userGroup.newUser.form = new Ext.form.FormPanel({
				frame : true,
				border :false,
				bodyStyle :{
					background : '#FFF'
				},
				padding: 10,
				layout:'form',

				items : [{
					xtype: 'combo',
					store: new Ext.data.SimpleStore({
						fields: ['userid', 'name'],
						data : Ext.combos_selec.dataUsers
					}),
					hiddenName: 'userid',
					valueField: 'userid',
					displayField:'name',
					fieldLabel:	'User',
					allowBlank:false,
					//name:'client',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true,
					 style: {
						fontSize: '16px'
					},
					width:400
				},{
					xtype: 'combo',
					store: new Ext.data.SimpleStore({
						fields: ['userid', 'name'],
						data : Ext.combos_selec.dataUsersAdmin
					}),
					hiddenName: 'authorized',
					valueField: 'userid',
					displayField:'name',
					fieldLabel:	'Authorized',
					allowBlank:false,
					value:'NULL',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true,
					 style: {
						fontSize: '16px'
					},
					width:400
				}],
				buttons:[{
					xtype 	: 'button',
					text	: 'Add',
					handler	: function (){
						if(userGroup.newUser.form.getForm().isValid()){
							userGroup.newUser.form.getForm().submit({
								waitTitle:'Recording',
								waitMsg :'It is processing the registration...',
								url: 'php/funcionesUserGroup.php',
								scope : this,
								params :{
									accion  : 'recorduserUserGroup',
									node	: userGroup.currentNode
								},
								success	: function (form,action){
									userGroup.newUser.form.getForm().reset();
									userGroup.newUser.win.hide();
									userGroup.store.load({
										params:{
											//search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
											node	:	userGroup.currentNode,
											specialFilters:userGroup.filterToSend,
											limit	:	BK_data.limitGrid
										}
									})
								}
							});
						}
					}
				},{
					xtype 	: 'button',
					text	: 'Reset',
					handler : function (){
						userGroup.newGroup.form.getForm().reset();
					}
				}]
			});

			userGroup.newUser.win= new Ext.Window({
				layout: 'fit',
				title: 'Add User',
				closable: true,
				closeAction: 'hide',
				resizable: false,
				maximizable: false,
				plain: true,
				border: false,
				width: 550,
				height: 150,
				items	:[
					userGroup.newUser.form
				]
			});
			userGroup.newUser.win.show();
		}
	},
	massAssignUser:{
		init: function (){
			if(userGroup.massAssignUser.win){
				userGroup.massAssignUser.win.show();
			}else{
				userGroup.massAssignUser.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',

					items : [
						{
							xtype: 'textarea',
							width:400,
							height:100,
							name:'users',
							fieldLabel:	'Userid\'s',
						},
						{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsersAdmin
							}),
							hiddenName: 'authorized',
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Authorized',
							allowBlank:false,
							value:'NULL',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
							 style: {
								fontSize: '16px'
							},
							width:400
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(userGroup.massAssignUser.form.getForm().isValid())
								{
									userGroup.massAssignUser.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration...',
										url: 'php/funcionesUserGroup.php',
										scope : this,
										params :{
											accion  : 'recorduserUserGroupMass',
											node	: userGroup.currentNode
										},
										success	: function (form,action)
										{
											userGroup.massAssignUser.form.getForm().reset();
											userGroup.massAssignUser.win.hide();
											userGroup.store.load({
												params:{
													//search	:	Ext.getCmp('filterUser').getForm().findField("search").getValue(),
													node	:	userGroup.currentNode,
													specialFilters:userGroup.filterToSend,
													limit	:	BK_data.limitGrid
												}
											})
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.massAssignUser.form.getForm().reset();
							}
						}]
				});

				userGroup.massAssignUser.win= new Ext.Window({
					layout: 'fit',
					title: 'Add User',
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 240,
					items	:[
						userGroup.massAssignUser.form
					]
				});
				userGroup.massAssignUser.win.show();
			}
		}
	},
	managerContracts:{
		init:function(){
			var usersGroupId = new Array();
			Ext.each(userGroup.grid.getSelectionModel().getSelections(),function(item,index){
				usersGroupId.push(Ext.apply(item.json));
			});
			if(usersGroupId.length<=0)
				Ext.MessageBox.alert('Warning', 'You have not selected any user.!');
			else{

				var simple = new Ext.TabPanel({
					activeTab	:	0,
					autoScroll	:	true,
					title		:	'New Tutorial',
					id			:	'principlaControlTab',
					items	:	[{
						title	:	"Contracts",
						layout	:	'fit',
						items	:	[{
							xtype		:	'panel',
							autoScroll	:	true,
							id			:	'tabStandar'
						}]
					},{
						title	:	"Buyer & Seller Info",
						layout	:	'fit',
						items	:	[{
							xtype		:	'panel',
							autoScroll	:	true,
							id			:	'tabBuyer'
						}]
					},{
						title	:	"ScrowLetter",
						layout	:	'fit',
						items	:	[{
							id			:	'tabScrow',
							layout		:	{
								type: 'table',
								columns: 2,
								tableAttrs: {
									style: {
										width: '100%'
									}
								}
							},
							items	:	[{
								xtype	:	'fieldset',
								columnWidth:.5,
								title	:	'Escrow 1',
								defaults:{
									width	:	450,
									xtype	:	'textfield'
								},
								items	:	[{
									id				:	'escrowAgent1',
									fieldLabel		:	'Escrow Agent'
								},{
									id				:	'escrowAddress1',
									fieldLabel		:	'Escrow Address'
								},{
									id				:	'escrowEmail1',
									fieldLabel		:	'Escrow Email'
								},{
									id				:	'escrowPhone1',
									fieldLabel		:	'Escrow Phone'
								},{
									id				:	'escrowFax1',
									fieldLabel		:	'Escrow Fax'
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'scrowOption1',
									name       : 'scrowOption1',
									onClick    : function() {
										if (Ext.getCmp('scrowOption2').getValue() == true)
											Ext.getCmp('scrowOption2').setValue(false);

										if (Ext.getCmp('scrowOption1').getValue() == false)
											Ext.getCmp('scrowOption1').setValue(true);
									}
								}]
							},{
								xtype	:	'fieldset',
								columnWidth:.5,
								title	:	'Escrow 2',
								defaults:{
									width	:	450,
									xtype	:	'textfield'
								},
								items	:	[{
									id				:	'escrowAgent2',
									fieldLabel		:	'Escrow Agent'
								},{
									id				:	'escrowAddress2',
									fieldLabel		:	'Escrow Address'
								},{
									id				:	'escrowEmail2',
									fieldLabel		:	'Escrow Email'
								},{
									id				:	'escrowPhone2',
									fieldLabel		:	'Escrow Phone'
								},{
									id				:	'escrowFax2',
									fieldLabel		:	'Escrow Fax'
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'scrowOption2',
									name       : 'scrowOption2',
									onClick    : function() {
										if (Ext.getCmp('scrowOption1').getValue() == true)
											Ext.getCmp('scrowOption1').setValue(false);

										if (Ext.getCmp('scrowOption2').getValue() == false)
											Ext.getCmp('scrowOption2').setValue(true);
									}
								}]
							}]
						}]
					},{
						title:"Advence Seach",
						layout	:	'fit',
						items:[{
							xtype:'panel',
							id:'tabAdvanceSearch',
							items:[{
								xtype:'form',
								bodyStyle: 'padding-left: 20px; padding-top: 10px;',
								items:[{
									xtype: 'superboxselect',
									fieldLabel: 'Templates',
									emptyText: 'Select Template(s)',
									resizable: true,
									name: 'user_attr[]',
									id:'tplAdvanceSearch',
									store: userGroup.storeTemplatesAdvanceSearch,
									mode: 'local',
									width:410,
									displayField: 'name',
									valueField: 'id',
									queryDelay: 0,
									triggerAction: 'all',
									forceSelection: true,
									forceFormValue: true,
									allowBlank: true,
									listeners:{
										additem:function(combo, value){
											if(value=="0000"){
												combo.removeAllItems();
												var temp="";var cont=0;
												Ext.each(combo.getStore().data.items, function(tempItems, index) {
													if(tempItems.id!="0000"){
														temp += (cont!=0?",":"")+tempItems.id;
														cont=cont+1;
													}
												});
												combo.setValue(temp);
											}
										}
									}
								}]
							}]
						}]
					},{
						title:"Advence Result",
						layout	:	'fit',
						items:[{
							xtype:'panel',
							id:'tabAdvanceResult',
							items:[{
								xtype:'form',
								bodyStyle: 'padding-left: 20px; padding-top: 10px;',
								items:[{
									xtype: 'superboxselect',
									fieldLabel: 'Templates',
									emptyText: 'Select Template(s)',
									resizable: true,
									name: 'user_attr[]',
									id:'tplAdvanceResult',
									store: userGroup.storeTemplatesAdvanceResult,
									mode: 'local',
									width:410,
									displayField: 'name',
									valueField: 'id',
									queryDelay: 0,
									triggerAction: 'all',
									forceSelection: true,
									forceFormValue: true,
									allowBlank: true,
									listeners:{
										additem:function(combo, value){
											if(value=="0000"){
												combo.removeAllItems();
												var temp="";var cont=0;
												Ext.each(combo.getStore().data.items, function(tempItems, index) {
													if(tempItems.id!="0000"){
														temp += (cont!=0?",":"")+tempItems.id;
														cont=cont+1;
													}
												});
												combo.setValue(temp);
											}
										}
									}
								}]
							}]
						}]
					},{
						title	:	"Signatures",
						layout	:	'fit',
						items	:	[{
							xtype		:	'panel',
							autoScroll	:	true,
							id			:	'tabSignatures',
							items		:	[{
								xtype		:	'form',
								labelWidth	:	150,
								border		:	false,
								id			:	'signatures-formPrincipal',

							}]
						}]
					}]
				});
				var PanelJ = new Ext.Panel({
					layout      : 'fit',
					//title		: 'Manager Contracts',
					resizable	: false,
					autoScroll	: true,
					plain       : true,
					usersGroupId:usersGroupId,
					items		: simple,
					tbar		: {
						items: [{
							text:'Grouping Contracts',
							menu: new Ext.menu.Menu({
								items: [
									new Ext.menu.Item({
										text: 'Save as new Group',
										listeners:	{
											click: function(click,e){
												Ext.MessageBox.prompt(
													'Asign Name',
													'Please enter name to save this configuration:',
													function(btn,text){
														if(btn=="ok"){
															myMask.show();
															BK_data.dataToSend	=	{
																contracts:{},
																tplAdvanceSearch:"",
																tplAdvanceResult:"",
																scrowLetter:{
																	images:{
																		header	:	"",
																		footer	:	""
																	},
																	data:{}
																}
															}
															BK_data.dataToSend.contracts	=	{};
															Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
																var contractN		=	index;
																var fileName		=	Ext.getCmp(item.id).contractFileName;
																var contractId		=	Ext.getCmp(item.id).contractId;
																var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
																var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
																var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
																BK_data.dataToSend.contracts[contractN]={
																	'fileName'		:	fileName+'.pdf',
																	'realName'		:	realName,
																	'template'		:	template,
																	'inspectionDays':	inspectionDays,
																	'addons':{}
																}
																Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
																	var addonN			=	index;
																	var addonFileName	=	Ext.getCmp(item.id).addonFileName;
																	var addonContractId	=	Ext.getCmp(item.id).addonId;
																	var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
																	var addonEmd		=	Ext.getCmp('addon-emd-'+contractId+'-'+addonContractId).getValue();
																	var addonPof		=	Ext.getCmp('addon-pof-'+contractId+'-'+addonContractId).getValue();
																	var addonAdd		=	Ext.getCmp('addon-add-'+contractId+'-'+addonContractId).getValue();
																	var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
																	BK_data.dataToSend.contracts[contractN].addons[addonN]={
																		'addonFileName'	:   addonFileName+'.pdf',
																		'addonRealName'	:   addonRealName,
																		'addonEmd'		:	addonEmd,
																		'addonPof'		:	addonPof,
																		'addonAdd'		:	addonAdd,
																		'addonTemplate'	:   addonTemplate
																	}
																});
															});
															Ext.getCmp('principlaControlTab').setActiveTab(1);
															BK_data.users	=	{};
															Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
																BK_data.users[Ext.getCmp(item.id).userId]={
																	type1		:	Ext.getCmp('cOptionType1-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
																	active		:	(Ext.getCmp('documentOption1'+Ext.getCmp(item.id).userId).getValue())	?	'1'	:	'2',
																	buyer		:	Ext.getCmp('buyerName1-'+Ext.getCmp(item.id).userId).getValue(),
																	address11	:	Ext.getCmp('address11-'+Ext.getCmp(item.id).userId).getValue(),
																	address12	:	Ext.getCmp('address12-'+Ext.getCmp(item.id).userId).getValue(),
																	address13	:	Ext.getCmp('address13-'+Ext.getCmp(item.id).userId).getValue(),
																	type2		:	Ext.getCmp('cOptionType2-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
																	seller		:	Ext.getCmp('buyerName2-'+Ext.getCmp(item.id).userId).getValue(),
																	address21	:	Ext.getCmp('address21-'+Ext.getCmp(item.id).userId).getValue(),
																	address22	:	Ext.getCmp('address22-'+Ext.getCmp(item.id).userId).getValue(),
																	address23	:	Ext.getCmp('address23-'+Ext.getCmp(item.id).userId).getValue()
																}
															});
															Ext.getCmp('principlaControlTab').setActiveTab(2);
															BK_data.dataToSend.scrowLetter.data[0]	=	{
																active	:	(Ext.getCmp('scrowOption1').getValue())	?	'1'	:	'0',
																agent	:	Ext.getCmp('escrowAgent1').getValue(),
																address	:	Ext.getCmp('escrowAddress1').getValue(),
																email	:	Ext.getCmp('escrowEmail1').getValue(),
																phone	:	Ext.getCmp('escrowPhone1').getValue(),
																fax		:	Ext.getCmp('escrowFax1').getValue()
															}
															BK_data.dataToSend.scrowLetter.data[1]	=	{
																active	:	(Ext.getCmp('scrowOption2').getValue())	?	'1'	:	'0',
																agent	:	Ext.getCmp('escrowAgent2').getValue(),
																address	:	Ext.getCmp('escrowAddress2').getValue(),
																email	:	Ext.getCmp('escrowEmail2').getValue(),
																phone	:	Ext.getCmp('escrowPhone2').getValue(),
																fax		:	Ext.getCmp('escrowFax2').getValue()
															}
															Ext.getCmp('principlaControlTab').setActiveTab(3);
															BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
															Ext.getCmp('principlaControlTab').setActiveTab(4);
															BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
															Ext.Ajax.request({
																url: 'php/grid_data.php',
																method: 'POST',
																timeout:0,
																success: function(response, request) {
																	myMask.hide();
																	var jsonData = Ext.util.JSON.decode(response.responseText);
																	if(jsonData.success){
																		Ext.Msg.show({
																			title:'Info',
																			msg: jsonData.msg,
																			icon: Ext.MessageBox.INFO,
																			buttons: Ext.MessageBox.OK,
																		});
																	}else{
																		Ext.Msg.show({
																			title:'Info',
																			msg: jsonData.msg,
																			icon: Ext.MessageBox.WARNING,
																			buttons: Ext.MessageBox.OK,
																		});
																	}
																},
																failure: function(response, request) {
																	myMask.hide();
																	var errMessage = 'Error en la petición ' + request.url +
																					' status ' + response.status +
																					' statusText ' + response.statusText +
																					' responseText ' + response.responseText;

																	alert(errMessage);
																},
																params: {
																	tipo:	'save-contract-grouping',
																	data:	Ext.util.JSON.encode(BK_data.dataToSend),
																	name:	text
																}
															});
														}
													}
												);
											}
										}
									}),
									new Ext.menu.Item({
										text: 'Load a Group',
										listeners:	{
											click: function(click,e){
												userGroup.storeGrouping.reload();
												var miventana = new Ext.Window({
													title: 'Select grouping to be loaded',
													width: 364,
													height: 100,
													maximizable: false,
													modal: true,
													resizable: false,
													maximized: false,
													constrain: true,
													items:[{
														xtype:'combo',
														id:'combo-load-grouping',
														store:userGroup.storeGrouping,
														valueField: 'id',
														typeAhead: true,
														width:350,
														forceSelection: true,
														mode: 'local',
														value:'0000',
														triggerAction: 'all',
														selectOnFocus: true,
														displayField: 'name',
														editable: false,
														fieldLabel:'Basic Additionals'
													}],
													buttons: [{
														text     : 'Load',
														handler  : function(){
															if(Ext.getCmp('combo-load-grouping').getValue()!="0000"){
																myMask.show();
																Ext.Ajax.request({
																	url: 'php/grid_data.php',
																	method: 'POST',
																	timeout:0,
																	success: function(response, request) {
																		myMask.hide();
																		miventana.close();
																		var jsonData = Ext.util.JSON.decode(response.responseText);
																		Ext.getCmp('principlaControlTab').setActiveTab(0);
																		//
																		Ext.each(Ext.query('*[id^=contract-delete]'),function(item,index){
																			if(typeof Ext.getCmp(item.id) != "undefined"){
																				Ext.getCmp(item.id).fireEvent('click');
																			}
																		});
																		//
																		var comboContracts	=	Ext.getCmp('contracsStableCombo');
																		for(var c in jsonData.contracts){
																			comboContracts.setValue(jsonData.contracts[c].fileName);
																			var indexC	=	comboContracts.getStore().find(
																				'id',
																				jsonData.contracts[c].fileName
																			);;
																			comboContracts.fireEvent('select',comboContracts,comboContracts.store.getAt(indexC));
																			userGroup.time_sleep_until(1000);
																			//**************Begin Asign Contract Template
																			var comboContractTpl	=	Ext.getCmp('contract-FinalTemplate-'+BK_data.standar.contract);
																			comboContractTpl.setValue(jsonData.contracts[c].template);
																			var indexCT	=	comboContractTpl.getStore().find(
																				'id',
																				jsonData.contracts[c].template
																			);;
																			comboContractTpl.fireEvent('select',comboContractTpl,comboContractTpl.store.getAt(indexCT));
																			//**************End Asign Contract Template
																			//**************Asign Contract RealName
																			Ext.getCmp('contract-FinalName-'+BK_data.standar.contract).setValue(jsonData.contracts[c].realName);
																			//**************Asign Contract Inspection Days
																			if(typeof jsonData.contracts[c].inspectionDays != "undefined")
																				Ext.getCmp('contract-inspectionDays-'+BK_data.standar.contract).setValue(jsonData.contracts[c].inspectionDays);
																			for(var a in jsonData.contracts[c].addons){
																				userGroup.time_sleep_until(1000);
																				var comboAddon	=	Ext.getCmp('comboAddonsAdded-'+BK_data.standar.contract);
																				comboAddon.setValue(jsonData.contracts[c].addons[a].addonFileName);
																				var indexA	=	comboAddon.getStore().find(
																					'id',
																					jsonData.contracts[c].addons[a].addonFileName
																				);;
																				comboAddon.fireEvent('select',comboAddon,comboAddon.store.getAt(indexA));
																				userGroup.time_sleep_until(1000);
																				//**************Begin Asign Addon Template
																				var comboAddonTpl	=	Ext.getCmp('addon-FinalTemplate-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]);
																				comboAddonTpl.setValue(jsonData.contracts[c].addons[a].addonTemplate);
																				var indexAT	=	comboAddonTpl.getStore().find(
																					'id',
																					jsonData.contracts[c].addons[a].addonTemplate
																				);;
																				comboAddonTpl.fireEvent('select',comboAddonTpl,comboAddonTpl.store.getAt(indexAT));
																				//**************End Asign Addon Template
																				//**************Asign Addon RealName
																				Ext.getCmp('addon-FinalName-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]).setValue(jsonData.contracts[c].addons[a].addonRealName);
																				if(jsonData.contracts[c].addons[a].addonEmd){
																					Ext.getCmp('addon-emd-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]).setValue(true);
																				}
																				if(jsonData.contracts[c].addons[a].addonPof){
																					Ext.getCmp('addon-pof-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]).setValue(true);
																				}
																				if(jsonData.contracts[c].addons[a].addonAdd){
																					Ext.getCmp('addon-add-'+BK_data.standar.contract+'-'+BK_data.standar.addons[BK_data.standar.contract]).setValue(true);
																				}

																			}
																		}
																		userGroup.time_sleep_until(1000);
																		Ext.getCmp('principlaControlTab').setActiveTab(2);
																		Ext.getCmp('escrowAgent1').setValue(jsonData.scrowLetter.data[0].agent);
																		Ext.getCmp('escrowAddress1').setValue(jsonData.scrowLetter.data[0].address);
																		Ext.getCmp('escrowEmail1').setValue(jsonData.scrowLetter.data[0].email);
																		Ext.getCmp('escrowPhone1').setValue(jsonData.scrowLetter.data[0].phone);
																		Ext.getCmp('escrowFax1').setValue(jsonData.scrowLetter.data[0].fax);
																		if(jsonData.scrowLetter.data[0].active=='1'){
																			Ext.getCmp('scrowOption1').setValue(true);
																		}

																		Ext.getCmp('escrowAgent2').setValue(jsonData.scrowLetter.data[1].agent);
																		Ext.getCmp('escrowAddress2').setValue(jsonData.scrowLetter.data[1].address);
																		Ext.getCmp('escrowEmail2').setValue(jsonData.scrowLetter.data[1].email);
																		Ext.getCmp('escrowPhone2').setValue(jsonData.scrowLetter.data[1].phone);
																		Ext.getCmp('escrowFax2').setValue(jsonData.scrowLetter.data[1].fax);
																		if(jsonData.scrowLetter.data[1].active=='1'){
																			Ext.getCmp('scrowOption2').setValue(true);
																		}
																		userGroup.time_sleep_until(1000);
																		Ext.getCmp('principlaControlTab').setActiveTab(3);
																		Ext.getCmp('tplAdvanceSearch').setValue(jsonData.tplAdvanceSearch);
																		userGroup.time_sleep_until(1000);
																		Ext.getCmp('principlaControlTab').setActiveTab(4);
																		Ext.getCmp('tplAdvanceResult').setValue(jsonData.tplAdvanceResult);
																		userGroup.time_sleep_until(1000);
																		Ext.getCmp('principlaControlTab').setActiveTab(0);
																	},
																	failure: function(response, request) {
																		myMask.hide();
																		var errMessage = 'Error en la petición ' + request.url +
																						' status ' + response.status +
																						' statusText ' + response.statusText +
																						' responseText ' + response.responseText;

																		alert(errMessage);
																	},
																	params: {
																		tipo:	'load-contract-grouping',
																		id:		Ext.getCmp('combo-load-grouping').getValue()
																	}
																});
															}else{
																Ext.MessageBox.alert('Warning','Please, select a grouping template.');
															}
														}
													},{
														text     : 'Close',
														handler  : function(){
															miventana.close();
														}
													}]
												}).show();
											}
										}
									}),
									new Ext.menu.Item({
										text: 'Update Existent Group',
										listeners:	{
											click: function(click,e){
												userGroup.storeGrouping.reload();
												var miventana = new Ext.Window({
													title: 'Select grouping to be updated',
													width: 364,
													height: 120,
													maximizable: false,
													modal: true,
													resizable: false,
													maximized: false,
													constrain: true,
													items:[{
														xtype:'combo',
														id:'combo-load-grouping',
														store:userGroup.storeGrouping,
														valueField: 'id',
														typeAhead: true,
														width:350,
														forceSelection: true,
														mode: 'local',
														value:'0000',
														triggerAction: 'all',
														selectOnFocus: true,
														displayField: 'name',
														editable: false,
														fieldLabel:'Basic Additionals',
														listeners:{
															select:function(combo){
																if(combo.getValue()!="0000"){
																	Ext.getCmp('combo-load-grouping-text').setValue(combo.getRawValue());
																}
															}
														}
													},{
														xtype:	'textfield',
														width:	350,
														id:		'combo-load-grouping-text',
													}],
													buttons: [{
														text     : 'Update',
														handler  : function(){
															if(Ext.getCmp('combo-load-grouping').getValue()!="0000"){
																myMask.show();
																BK_data.dataToSend	=	{
																	contracts:{},
																	tplAdvanceSearch:"",
																	tplAdvanceResult:"",
																	scrowLetter:{
																			images:{
																			header	:	"",
																			footer	:	""
																		},
																		data:{}
																	}
																}
																BK_data.dataToSend.contracts	=	{};
																Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
																	var contractN		=	index;
																	var fileName		=	Ext.getCmp(item.id).contractFileName;
																	var contractId		=	Ext.getCmp(item.id).contractId;
																	var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
																	var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
																	var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
																	BK_data.dataToSend.contracts[contractN]={
																		'fileName'		:	fileName+'.pdf',
																		'realName'		:	realName,
																		'template'		:	template,
																		'inspectionDays':	inspectionDays,
																		'addons':{}
																	}
																	Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
																		var addonN			=	index;
																		var addonFileName	=	Ext.getCmp(item.id).addonFileName;
																		var addonContractId	=	Ext.getCmp(item.id).addonId;
																		var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
																		var addonEmd		=	Ext.getCmp('addon-emd-'+contractId+'-'+addonContractId).getValue();
																		var addonPof		=	Ext.getCmp('addon-pof-'+contractId+'-'+addonContractId).getValue();
																		var addonAdd		=	Ext.getCmp('addon-add-'+contractId+'-'+addonContractId).getValue();
																		var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
																		BK_data.dataToSend.contracts[contractN].addons[addonN]={
																			'addonFileName'	:   addonFileName+'.pdf',
																			'addonRealName'	:   addonRealName,
																			'addonEmd'		:	addonEmd,
																			'addonPof'		:	addonPof,
																			'addonAdd'		:	addonAdd,
																			'addonTemplate'	:   addonTemplate
																		}
																	});
																});
																Ext.getCmp('principlaControlTab').setActiveTab(1);
																BK_data.users	=	{};
																Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
																	BK_data.users[Ext.getCmp(item.id).userId]={
																		type1		:	Ext.getCmp('cOptionType1-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
																		active		:	(Ext.getCmp('documentOption1'+Ext.getCmp(item.id).userId).getValue())	?	'1'	:	'2',
																		buyer		:	Ext.getCmp('buyerName1-'+Ext.getCmp(item.id).userId).getValue(),
																		address11	:	Ext.getCmp('address11-'+Ext.getCmp(item.id).userId).getValue(),
																		address12	:	Ext.getCmp('address12-'+Ext.getCmp(item.id).userId).getValue(),
																		address13	:	Ext.getCmp('address13-'+Ext.getCmp(item.id).userId).getValue(),
																		type2		:	Ext.getCmp('cOptionType2-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
																		seller		:	Ext.getCmp('buyerName2-'+Ext.getCmp(item.id).userId).getValue(),
																		address21	:	Ext.getCmp('address21-'+Ext.getCmp(item.id).userId).getValue(),
																		address22	:	Ext.getCmp('address22-'+Ext.getCmp(item.id).userId).getValue(),
																		address23	:	Ext.getCmp('address23-'+Ext.getCmp(item.id).userId).getValue()
																	}
																});
																Ext.getCmp('principlaControlTab').setActiveTab(2);
																BK_data.dataToSend.scrowLetter.data[0]	=	{
																	active	:	(Ext.getCmp('scrowOption1').getValue())	?	'1'	:	'0',
																	agent	:	Ext.getCmp('escrowAgent1').getValue(),
																	address	:	Ext.getCmp('escrowAddress1').getValue(),
																	email	:	Ext.getCmp('escrowEmail1').getValue(),
																	phone	:	Ext.getCmp('escrowPhone1').getValue(),
																	fax		:	Ext.getCmp('escrowFax1').getValue()
																}
																BK_data.dataToSend.scrowLetter.data[1]	=	{
																	active	:	(Ext.getCmp('scrowOption2').getValue())	?	'1'	:	'0',
																	agent	:	Ext.getCmp('escrowAgent2').getValue(),
																	address	:	Ext.getCmp('escrowAddress2').getValue(),
																	email	:	Ext.getCmp('escrowEmail2').getValue(),
																	phone	:	Ext.getCmp('escrowPhone2').getValue(),
																	fax		:	Ext.getCmp('escrowFax2').getValue()
																}
																Ext.getCmp('principlaControlTab').setActiveTab(3);
																BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
																Ext.getCmp('principlaControlTab').setActiveTab(4);
																BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
																Ext.Ajax.request({
																	url: 'php/grid_data.php',
																	method: 'POST',
																	timeout:0,
																	success: function(response, request) {
																		myMask.hide();
																		var jsonData = Ext.util.JSON.decode(response.responseText);
																		if(jsonData.success){
																			Ext.Msg.show({
																				title:'Info',
																				msg: jsonData.msg,
																				icon: Ext.MessageBox.INFO,
																				buttons: Ext.MessageBox.OK,
																			});
																		}else{
																			Ext.Msg.show({
																				title:'Info',
																				msg: jsonData.msg,
																				icon: Ext.MessageBox.WARNING,
																				buttons: Ext.MessageBox.OK,
																			});
																		}
																	},
																	failure: function(response, request) {
																		myMask.hide();
																		var errMessage = 'Error en la petición ' + request.url +
																						' status ' + response.status +
																						' statusText ' + response.statusText +
																						' responseText ' + response.responseText;

																		alert(errMessage);
																	},
																	params: {
																		tipo:	'update-contract-grouping',
																		data:	Ext.util.JSON.encode(BK_data.dataToSend),
																		name:	Ext.getCmp('combo-load-grouping-text').getValue(),
																		id:		Ext.getCmp('combo-load-grouping').getValue()
																	}
																});
																miventana.close();
															}else{
																Ext.MessageBox.alert('Warning','Please, select a grouping template.');
															}
														}
													},{
														text     : 'Close',
														handler  : function(){
															miventana.close();
														}
													}]
												}).show();
											}
										}
									})
								]
							})
						}]
					},
					listeners	:	{
						afterrender:function(){
							Ext.each(this.usersGroupId, function(tempUsers, index) {
								var temp = [{
									xtype:'fieldset',
									id:'contract-'+tempUsers.userid,
									userId:tempUsers.userid,
									height: 250,
									title:'User: '+tempUsers.name.trim().toUpperCase()+" "+tempUsers.surname.trim().toUpperCase(),
									collapsible	:	true,
									autoScroll  :	true,
									collapsed	:	false,
									items:[{
										xtype        : 'panel',
										layout       : 'table',
										layoutConfig : {
											columns	:	2
										},
										items:[{
											xtype:'fieldset',
											title:'Option 1',
											defaults:{
												width:300,
												xtype:'textfield'
											},
											items:[{
												xtype: 'radiogroup',
												id: 'cOptionType1-'+tempUsers.userid,
												fieldLabel: 'Type',
												items: [
													{boxLabel: 'Buyer', name: 'cOptionType1-'+tempUsers.userid, inputValue:'B',checked: true},
													{boxLabel: 'Seller', name: 'cOptionType1-'+tempUsers.userid, inputValue:'S'}
												]
											},{
												id:'buyerName1-'+tempUsers.userid,
												fieldLabel:'Name',
												value:tempUsers.name.trim().toUpperCase()+" "+tempUsers.surname.trim().toUpperCase()
											},{
												id:'address11-'+tempUsers.userid,
												fieldLabel:'Addres Line 1',
												value:tempUsers.address.trim().toUpperCase()
											},{
												id:'address12-'+tempUsers.userid,
												fieldLabel:'Addres Line 2',
												value:tempUsers.city.trim().toUpperCase()+", "+tempUsers.state.trim()+" "+tempUsers.zipcode.trim()
											},{
												id:'address13-'+tempUsers.userid,
												fieldLabel:'Addres Line 3'
											},{
												xtype		:	'checkbox',
												fieldLabel	:	'Active',
												id			:	'documentOption1'+tempUsers.userid,
												name		:	'documentOption1'+tempUsers.userid,
												checked		:	true,
												onClick		:	function() {
													if (Ext.getCmp('documentOption2'+tempUsers.userid).getValue() == true)
														Ext.getCmp('documentOption2'+tempUsers.userid).setValue(false);

													if (Ext.getCmp('documentOption1'+tempUsers.userid).getValue() == false)
														Ext.getCmp('documentOption1'+tempUsers.userid).setValue(true);
												}
											}]
										},{
											xtype:'fieldset',
											title:'Option 2',
											defaults:{
												width:300,
												xtype:'textfield'
											},
											items:[{
												xtype: 'radiogroup',
												id: 'cOptionType2-'+tempUsers.userid,
												fieldLabel: 'Type',
												items: [
													{boxLabel: 'Buyer', name: 'cOptionType2-'+tempUsers.userid, inputValue:'B', checked: true},
													{boxLabel: 'Seller', name: 'cOptionType2-'+tempUsers.userid, inputValue:'S'}
												]
											},{
												id:'buyerName2-'+tempUsers.userid,
												fieldLabel:'Name',
												value:tempUsers.name.trim().toUpperCase()+" "+tempUsers.surname.trim().toUpperCase()
											},{
												id:'address21-'+tempUsers.userid,
												fieldLabel:'Addres Line 1',
												value:tempUsers.address.trim().toUpperCase()
											},{
												id:'address22-'+tempUsers.userid,
												fieldLabel:'Addres Line 2',
												value:tempUsers.city.trim().toUpperCase()+", "+tempUsers.state.trim()+" "+tempUsers.zipcode.trim()
											},{
												id:'address23-'+tempUsers.userid,
												fieldLabel:'Addres Line 3'
											},{
												xtype      : 'checkbox',
												fieldLabel : 'Active',
												id         : 'documentOption2'+tempUsers.userid,
												name       : 'documentOption2'+tempUsers.userid,
												onClick    : function() {
													if (Ext.getCmp('documentOption1'+tempUsers.userid).getValue() == true)
														Ext.getCmp('documentOption1'+tempUsers.userid).setValue(false);

													if (Ext.getCmp('documentOption2'+tempUsers.userid).getValue() == false)
														Ext.getCmp('documentOption2'+tempUsers.userid).setValue(true);
												}
											}]
										}]
									}]
								}]
								Ext.getCmp('tabBuyer').add(temp);
								Ext.getCmp('tabBuyer').doLayout();
							});
							Ext.each(this.usersGroupId, function(tempUsers, index) {
								var temp = [{
									xtype		:	'fieldset',
									soon		:	'signatures-'+tempUsers.userid,
									userId		:	tempUsers.userid,
									title		:	'User: '+tempUsers.name+' '+tempUsers.surname,
									autoHeight	:	true,
									collapsible	:	true,
									autoScroll  :	true,
									collapsed	:	true,
									defaults	:	{
										xtype       : 'fileuploadfield',
										width       : 350,
										emptyText   : 'Upload Image',
										buttonText  : 'Browse...'
									},
									items:[{
										id          : 'signature-buyer1-sig-'+tempUsers.userid,
										fieldLabel	: '1st Signature',
										name        : 'signature-buyer1-sig-'+tempUsers.userid
									},{
										id          : 'signature-buyer1-ini-'+tempUsers.userid,
										fieldLabel	: '1st Initial',
										name        : 'signature-buyer1-ini-'+tempUsers.userid
									},{
										id          : 'signature-buyer2-sig-'+tempUsers.userid,
										fieldLabel	: '2nd Signature',
										name        : 'signature-buyer2-sig-'+tempUsers.userid
									},{
										id          : 'signature-buyer2-ini-'+tempUsers.userid,
										fieldLabel	: '2nd Initial',
										name        : 'signature-buyer2-ini-'+tempUsers.userid
									}],
									listeners:{
										expand:function(panel){
											//console.debug(typeof window.eliminarFirma);
											//Ext.getCmp(panel.soon).load({
											//	url: '/mysetting_tabs/mycontracts_tabs/mysignatures.php',
											//	scripts: true,
											//	params:{userBackOffice:panel.userId}
											//});
										}
									}
								}]
								Ext.getCmp('signatures-formPrincipal').add(temp);
								Ext.getCmp('tabSignatures').doLayout();
							});
							var temp = [{
								xtype:'fieldset',
								id:'contract-standar',
								//height:500,
								collapsible	:	false,
								//autoScroll  :	true,
								collapsed	:	false,
								items:[{
									xtype:'combo',
									store:userGroup.storeContracts,
									id:'contracsStableCombo',
									valueField: 'id',
									typeAhead: true,
									width:350,
									forceSelection: true,
									mode: 'local',
									value:'0000',
									triggerAction: 'all',
									selectOnFocus: true,
									displayField: 'name',
									editable: false,
									fieldLabel:'Basic Contracts',
									father:'contract-standar',
									listeners:{
										select:function( combo, newValue, oldValue ){
											if(newValue.data.id!='0000'){
												BK_data.standar.contract	=	BK_data.standar.contract+1;
												var temp1=[{
													xtype:'fieldset',
													layout:'column',
													collapsible	:	true,
													contractFileName:newValue.data.name,
													title:'Contract: '+newValue.data.name,
													id:'contract-standar-'+BK_data.standar.contract,
													contractId:BK_data.standar.contract,
													items:[{
														layout: 'form',
														border:false,
														columnWidth:1,
														cls:'padding: 0 0 10px;',
														items:[{
															layout:'column',
															border:false,
															items:[{
																layout: 'form',
																border:false,
																columnWidth:.45,
																items:[{
																	xtype:'textfield',
																	//columnWidth:.9,
																	id:'contract-FinalName-'+BK_data.standar.contract,
																	width:300,
																	fieldLabel:'Contract Name',
																	value:newValue.data.name
																}]
															},{
																layout: 'form',
																border:false,
																columnWidth:.45,
																items:[{
																	xtype:'combo',
																	//columnWidth:.5,
																	store:userGroup.storeTemplates,
																	valueField: 'id',
																	id:'contract-FinalTemplate-'+BK_data.standar.contract,
																	typeAhead: true,
																	width:300,
																	forceSelection: true,
																	mode: 'local',
																	value:'0000',
																	triggerAction: 'all',
																	selectOnFocus: true,
																	displayField: 'name',
																	editable: false,
																	fieldLabel:'Asign Template',
																	listeners:{
																		select:function( combo, newValue, oldValue ){

																		}
																	}
																}]
															},{
																layout: 'form',
																border:false,
																columnWidth:.10,
																items:[{
																	xtype:'button',
																	text: 'Delete',
																	id:'contract-delete-'+BK_data.standar.contract,
																	father:'contract-standar-'+BK_data.standar.contract,
																	tooltip:'Delete this contract with all addons inside',
																	listeners: {
																		click:function(){
																			myMask.show();
																			Ext.getCmp(this.father).destroy();
																			myMask.hide();
																		}
																	}
																}]
															},{
																layout		:	'form',
																border		:	false,
																columnWidth	:	.45,
																items		:	[{
																	xtype			:	'numberfield',
																	allowDecimals	:	false,
																	allowNegative	:	false,
																	id				:	'contract-inspectionDays-'+BK_data.standar.contract,
																	width			:	50,
																	fieldLabel		:	'Inspection Days',
																	value			:	15
																}]
															}]
														}]
													},{
														layout: 'form',
														border:false,
														columnWidth:1,
														items:[{
															xtype:'combo',
															store:userGroup.storeAddons,
															valueField: 'id',
															typeAhead: true,
															width:350,
															forceSelection: true,
															mode: 'local',
															value:'0000',
															triggerAction: 'all',
															selectOnFocus: true,
															displayField: 'name',
															editable: false,
															fieldLabel:'Basic Additionals',
															id:'comboAddonsAdded-'+BK_data.standar.contract,
															father:'contract-standar-'+BK_data.standar.contract,
															fatherId:BK_data.standar.contract,
															listeners:{
																select:function( combo, newValue, oldValue ){
																	BK_data.standar.addons[combo.fatherId]	=	typeof BK_data.standar.addons[combo.fatherId] == "undefined" ? 1 : BK_data.standar.addons[combo.fatherId]+1;
																	if(newValue.data.id!='0000'){
																		if(BK_data.standar.addons[combo.fatherId]<=6){
																			var temp1=[{
																				xtype:'fieldset',
																				id:'addons-standar-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																				style: 'margin-left:30px',
																				columnWidth:1,
																				addonFileName:newValue.data.name,
																				addonId:BK_data.standar.addons[combo.fatherId],
																				cls:'margin-left:20px',
																				layout:'table',
																				layoutConfig:{
																					columns:4
																				},
																				title:'Addon: '+newValue.data.name,
																				items:[{
																					layout: 'form',
																					border:false,
																					items:[{
																						xtype:'textfield',
																						width:300,
																						id:'addon-FinalName-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						fieldLabel:'Addon Name',
																						value:newValue.data.name
																					}]
																				},{
																					layout: 'form',
																					border:false,
																					items:[{
																						xtype:'combo',
																						store:userGroup.storeTemplates,
																						valueField: 'id',
																						typeAhead: true,
																						width:300,
																						id:'addon-FinalTemplate-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						forceSelection: true,
																						mode: 'local',
																						value:'0000',
																						triggerAction: 'all',
																						selectOnFocus: true,
																						displayField: 'name',
																						editable: false,
																						fieldLabel:'Asign Template',
																						listeners:{
																							select:function( combo, newValue, oldValue ){

																							}
																						}
																					}]
																				},{
																					xtype	:	'checkboxgroup',
																					width	:	300,
																					id		:	'addon_types'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																					items: [{
																						boxLabel: 'EMD &nbsp;',
																						labelStyle: 'top:10px;',
																						name: 'addon-emd-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						id: 'addon-emd-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
																					},{
																						boxLabel: 'POF &nbsp;',
																						name: 'addon-pof-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						id: 'addon-pof-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
																					},{
																						boxLabel: 'ADDITIONAL &nbsp;',
																						name: 'addon-add-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						id: 'addon-add-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
																					}]
																				},{
																					layout: 'form',
																					border:false,
																					items:[{
																						xtype:'button',
																						text: 'Delete',
																						father:'addons-standar-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
																						fatherID:combo.fatherId,
																						tooltip:'Delete this addon',
																						handler: function(){
																							BK_data.standar.addons[this.fatherId]=BK_data.standar.addons[this.fatherId]-1;
																							Ext.getCmp(this.father).destroy();
																						}
																					}]
																				}]
																			}]
																			Ext.getCmp(combo.father).add(temp1);
																			Ext.getCmp('contract-standar').doLayout();
																			combo.setValue('0000');
																		}else{
																			BK_data.standar.addons[combo.fatherId]=BK_data.standar.addons[combo.fatherId]-1;
																			Ext.MessageBox.alert('Warning', 'Only can be added 6 additionals documents.');
																		}
																	}
																}
															}
														}]
													}]
												}]
												Ext.getCmp(combo.father).add(temp1);
												Ext.getCmp(combo.father).doLayout();
												combo.setValue('0000');
											}
										}
									}
								}],
								listeners:{
									beforerender:	function(){
										myMask.show();
										setTimeout(function(){
											myMask.hide();
										},1000)
									}
								}
							}]
							Ext.getCmp('tabStandar').add(temp);
							Ext.getCmp('tabStandar').doLayout();
							Ext.getCmp('tabScrow').doLayout();
						}
					},
					buttons:[{
						text:'Process all Tabs',
						handler:function(){
							BK_data.dataToSend.contracts	=	{};
							Ext.each(Ext.get("contract-standar").select('fieldset[id*=contract]').elements,function(item,index){
								var contractN		=	index;
								var fileName		=	Ext.getCmp(item.id).contractFileName;
								var contractId		=	Ext.getCmp(item.id).contractId;
								var realName		=	Ext.getCmp('contract-FinalName-'+contractId).getValue();
								var template		=	Ext.getCmp('contract-FinalTemplate-'+contractId).getValue();
								var inspectionDays	=	Ext.getCmp('contract-inspectionDays-'+contractId).getValue();
								BK_data.dataToSend.contracts[contractN]={
									'fileName'		:	fileName+'.pdf',
									'realName'		:	realName,
									'template'		:	template,
									'inspectionDays':	inspectionDays,
									'addons':{}
								}
								Ext.each(Ext.get(item.id).select('fieldset').elements,function(item,index){
									var addonN			=	index;
									var addonFileName	=	Ext.getCmp(item.id).addonFileName;
									var addonContractId	=	Ext.getCmp(item.id).addonId;
									var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
									var addonEmd		=	Ext.getCmp('addon-emd-'+contractId+'-'+addonContractId).getValue();
									var addonPof		=	Ext.getCmp('addon-pof-'+contractId+'-'+addonContractId).getValue();
									var addonAdd		=	Ext.getCmp('addon-add-'+contractId+'-'+addonContractId).getValue();
									var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
									BK_data.dataToSend.contracts[contractN].addons[addonN]={
										'addonFileName'	:   addonFileName+'.pdf',
										'addonRealName'	:   addonRealName,
										'addonEmd'		:	addonEmd,
										'addonPof'		:	addonPof,
										'addonAdd'		:	addonAdd,
										'addonTemplate'	:   addonTemplate
									}
								});
							});
							Ext.getCmp('principlaControlTab').setActiveTab(1);
							BK_data.users	=	{};
							Ext.each(Ext.get("tabBuyer").select('fieldset[id*=contract]').elements,function(item,index){
								BK_data.users[Ext.getCmp(item.id).userId]={
									type1		:	Ext.getCmp('cOptionType1-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
									active		:	(Ext.getCmp('documentOption1'+Ext.getCmp(item.id).userId).getValue())	?	'1'	:	'2',
									buyer		:	Ext.getCmp('buyerName1-'+Ext.getCmp(item.id).userId).getValue(),
									address11	:	Ext.getCmp('address11-'+Ext.getCmp(item.id).userId).getValue(),
									address12	:	Ext.getCmp('address12-'+Ext.getCmp(item.id).userId).getValue(),
									address13	:	Ext.getCmp('address13-'+Ext.getCmp(item.id).userId).getValue(),
									type2		:	Ext.getCmp('cOptionType2-'+Ext.getCmp(item.id).userId).getValue().getGroupValue(),
									seller		:	Ext.getCmp('buyerName2-'+Ext.getCmp(item.id).userId).getValue(),
									address21	:	Ext.getCmp('address21-'+Ext.getCmp(item.id).userId).getValue(),
									address22	:	Ext.getCmp('address22-'+Ext.getCmp(item.id).userId).getValue(),
									address23	:	Ext.getCmp('address23-'+Ext.getCmp(item.id).userId).getValue()
								}
							});
							Ext.getCmp('principlaControlTab').setActiveTab(2);
							BK_data.dataToSend.scrowLetter.data[0]	=	{
								active	:	(Ext.getCmp('scrowOption1').getValue())	?	'1'	:	'0',
								agent	:	Ext.getCmp('escrowAgent1').getValue(),
								address	:	Ext.getCmp('escrowAddress1').getValue(),
								email	:	Ext.getCmp('escrowEmail1').getValue(),
								phone	:	Ext.getCmp('escrowPhone1').getValue(),
								fax		:	Ext.getCmp('escrowFax1').getValue()
							}
							BK_data.dataToSend.scrowLetter.data[1]	=	{
								active	:	(Ext.getCmp('scrowOption2').getValue())	?	'1'	:	'0',
								agent	:	Ext.getCmp('escrowAgent2').getValue(),
								address	:	Ext.getCmp('escrowAddress2').getValue(),
								email	:	Ext.getCmp('escrowEmail2').getValue(),
								phone	:	Ext.getCmp('escrowPhone2').getValue(),
								fax		:	Ext.getCmp('escrowFax2').getValue()
							}
							Ext.getCmp('principlaControlTab').setActiveTab(3);
							BK_data.dataToSend.tplAdvanceSearch=(Ext.getCmp('tplAdvanceSearch').getValue());
							Ext.getCmp('principlaControlTab').setActiveTab(4);
							BK_data.dataToSend.tplAdvanceResult=(Ext.getCmp('tplAdvanceResult').getValue());
							Ext.getCmp('principlaControlTab').setActiveTab(5);
							Ext.Ajax.request({
								url: 'php/grid_data.php',
								method: 'POST',
								//jsonData: BK_data.dataToSend,
								timeout:0,
								isUpload: true,
								headers: {'Content-type':'multipart/form-data'},
								form: Ext.getCmp('signatures-formPrincipal').getForm().getEl().dom,
								success: function(response, request) {
									var jsonData = Ext.util.JSON.decode(response.responseText);
									if(!jsonData.success){
										Ext.Msg.show({
											title:'Info',
											msg:'Result: Error '+jsonData.error,
											icon: Ext.MessageBox.WARNING,
											buttons: Ext.MessageBox.OK,
										});
									}else{
										Ext.Msg.show({
											title:'Info',
											msg:'Result: Successfully',
											icon: Ext.MessageBox.INFO,
											buttons: Ext.MessageBox.OK,
										});
									}
								},
								failure: function(response, request) {
									var errMessage = 'Error en la petición ' + request.url +
										' status ' + response.status +
										' statusText ' + response.statusText +
										' responseText ' + response.responseText;
									alert(errMessage);
								},
								params: {
									tipo:	'uploadNewsContracts',
									data:	Ext.util.JSON.encode(BK_data.dataToSend),
									user:	Ext.util.JSON.encode(BK_data.users)
								}
							});
						}
					}]
				});
				var temp = new Ext.Window({
					layout		:	'fit',
					title		:	'Manager Contracts',
					closable	:	true,
					resizable	:	false,
					maximizable	:	false,
					plain		:	true,
					border		:	false,
					modal		:	true,
					width		:	Ext.getBody().getViewSize().width-100,
					height		:	Ext.getBody().getViewSize().height-50,
					items		:	PanelJ
				}).show();
			}
		}
	},
	editGroup:{
		init:function (){
			if(userGroup.editGroup.win)
			{
				userGroup.editGroup.form.getForm().setValues({
						name : userGroup.currentNodeText
					});

				userGroup.editGroup.win.show();
			}
			else{
				userGroup.editGroup.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',

					items : [
						{
							xtype 	: 'textfield',
							name	: 'name',
							value	: userGroup.currentNodeText,
							width	: 400,
							fieldLabel	: 'Name'
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Save',
							handler	: function ()
							{
								if(userGroup.editGroup.form.getForm().isValid())
								{
									userGroup.editGroup.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing...',
										url: 'php/funcionesUserGroup.php',
										scope : this,
										params :{
											accion 	: 'editGroup',
											id		:userGroup.currentNode
										},
										success	: function (form,action)
										{
											userGroup.editGroup.form.getForm().reset();
											userGroup.editGroup.win.hide();
											userGroup.treeGroups.getRootNode().reload();
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.editGroup.form.getForm().reset();
							}
						}]
				});

				userGroup.editGroup.win= new Ext.Window({
					layout: 'fit',
					title: 'Edit Group',
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 120,
					items	:[
						userGroup.editGroup.form
					]
				});
				userGroup.editGroup.win.show();
			}
		}
	},
	newGroup:{
		init:function (){
			if(userGroup.newGroup.win){
				userGroup.newGroup.form.getForm().reset();
				userGroup.newGroup.win.show();
			}else{

				var readerModule = new Ext.data.JsonReader({
					root: 'modules',
					totalProperty: 'total'
				},
					userGroup.recordModules
				);

				userGroup.newGroup.storeModule = new Ext.data.Store({
					id: 'id',
					proxy: userGroup.dataProxy,
					baseParams: {
						accion	:'listModule'
					},
					reader: readerModule
				});

				var comboModule = new Ext.form.ComboBox({
					forceSelection:true,
					store: userGroup.newGroup.storeModule,
					displayField:'name',
					hiddenName	:'group',
					typeAhead	: true,
					mode		: 'remote',
					fieldLabel: 'Group',
					valueField:'id',
					editable	:false,
					triggerAction: 'all',
					emptyText:'',
					width: 400,
					selectOnFocus:true
				});
				userGroup.newGroup.storeModule.load();

				userGroup.newGroup.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',

					items : [
						comboModule,
						{
							xtype 	: 'textfield',
							name	: 'name',
							width	: 400,
							fieldLabel	: 'Name'
						}
					],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(userGroup.newGroup.form.getForm().isValid())
								{
									userGroup.newGroup.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new Group...',
										url: 'php/funcionesUserGroup.php',
										scope : this,
										params :{
											accion : 'recorduserGroup'
										},
										success	: function (form,action)
										{
											userGroup.newGroup.form.getForm().reset();
											userGroup.newGroup.win.hide();
											userGroup.newGroup.storeModule.load();
											userGroup.treeGroups.getRootNode().reload();
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								userGroup.newGroup.form.getForm().reset();
							}
						}]
				});

				userGroup.newGroup.win= new Ext.Window({
					layout: 'fit',
					title: 'New Group of Users',
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 550,
					height: 155,
					items	:[
						userGroup.newGroup.form
					]
				});
				userGroup.newGroup.win.show();
			}
		}
	},
	viewDocuments:function (tempData){
		var store	=	new Ext.data.GroupingStore({
			reader		:	new Ext.data.JsonReader({
				id				:	'id',
				totalProperty	:	'total',
				root			:	'data',
				fields			:	[
				   {name: 'namefile'},
				   {name: 'desc'},
				   {name: 'addon_name'},
				   {name: 'place'},
				   {name: 'type'},
				   {name: 'userid', type: 'int'},
				   {name: 'idContract', type: 'int'},
				   {name: 'idAddon', type: 'int'},
				   {name: 'origen', type: 'string'}
				]
			}),
			proxy		:	new Ext.data.HttpProxy({url:'php/grid_data.php?tipo=users-contract'}),
			groupField	:	'namefile',
			sortInfo	:	{field:'idContract', direction:'ASC'},
			idGrid		:	'gridFiles-'+tempData.userid,
			listeners	:	{
				update	:	function(e,record,operation){
					//console.debug(JSON.stringify(recordData));
					Ext.Ajax.request({
						url: 'php/grid_data.php',
						method: 'POST',
						timeout:0,
						success: function(response, request) {
							var jsonData = Ext.util.JSON.decode(response.responseText);
							if(jsonData.success){
								if(jsonData.aditional.reload)
									Ext.getCmp(e.idGrid).getStore().reload();
								Ext.getCmp(e.idGrid).getStore().commitChanges();
							}else{
								Ext.getCmp(e.idGrid).getStore().rejectChanges();
								alert('Error en la Edicion');
							}
						},
						failure: function(response, request) {
							Ext.getCmp(e.idGrid).getStore().rejectChanges();
							var errMessage = 'Error en la petición ' + request.url +
											' status ' + response.status +
											' statusText ' + response.statusText +
											' responseText ' + response.responseText;
							alert(errMessage);
						},
						params: {
							tipo:	'upload-Info-Contracts',
							data:	Ext.util.JSON.encode(record.data),
						}
					});
				}
			}
		});

		var action	=	new Ext.ux.grid.RowActions({
			header			:	'Actions',
			keepSelection	:	true,
			actions			:	[{
				iconCls	:	'x-icon-addon-seeVars',
				tooltip	:	'View Vars',
				callback:	function(grid, record, action, row, col) {
					//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
					console.debug(record);
					new Ext.util.Cookies.set("idmodifycontract", record.data.idAddon);
					new Ext.util.Cookies.set("typemodifycontract", 'addon');
					new Ext.util.Cookies.set("userCustomBackOffice", record.data.userid);
					new Ext.util.Cookies.set("placemodifycontract", record.data.place);

					if(record.data.addon_name!="Doesn't have Addons"){
						var miventana = new Ext.Window({
							title: 'View Document(s) Variables: '+record.data.idAddon+' - '+record.data.addon_name,
							width: 970,
							autoScroll:	false,
							height: Ext.getBody().getViewSize().height,
							maximizable: false,
							modal: true,
							resizable: false,
							maximized: false,
							constrain: true,
							items:[{
								//closable 	:	true,
								width		:	955,
								height		:	Ext.getBody().getViewSize().height-25,
								autoScroll	:	true,
								id			:	'idmodifycontract',
								autoLoad	:	{
									url		:	'/custom_contract/manager.php',
									scripts	:	true
								}
							}],
							listeners:{
								beforeclose:function(){
									Ext.util.Cookies.set('userCustomBackOffice', null, new Date("January 1, 1970"));
									Ext.util.Cookies.clear('userCustomBackOffice');
								}
							}
						}).show();
					}
				}
			},{
				iconCls	:	'x-icon-see-file',
				tooltip	:	'See Additional Document',
				callback:	function(grid, record, action, row, col) {
					if(record.data.addon_name!="Doesn't have Addons"){
						record.data.origen='seeAddon';
						Ext.Ajax.request({
							url: 'php/grid_data.php',
							method: 'POST',
							timeout:0,
							success: function(response, request) {
								var jsonData = Ext.util.JSON.decode(response.responseText);
								if(jsonData.success){
									var Digital=new Date()
									var hours=Digital.getHours()
									var minutes=Digital.getMinutes()
									var seconds=Digital.getSeconds()
									var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/addons/'+jsonData.aditional.pdf;
									window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
								}
							},
							failure: function(response, request) {
								var errMessage = 'Error en la petición ' + request.url +
												' status ' + response.status +
												' statusText ' + response.statusText +
												' responseText ' + response.responseText;
								alert(errMessage);
							},
							params: {
								tipo:	'upload-Info-Contracts',
								data:	Ext.util.JSON.encode(record.data),
							}
						});
					}
				}
			},{
				iconCls:'x-icon-download-file',
				tooltip:'See Additional Document',
				callback:function(grid, record, action, row, col) {
					if(record.data.addon_name!="Doesn't have Addons"){
						record.data.origen='seeAddon';
						Ext.Ajax.request({
							url: 'php/grid_data.php',
							method: 'POST',
							timeout:0,
							success: function(response, request) {
								var jsonData = Ext.util.JSON.decode(response.responseText);
								if(jsonData.success){
									var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/addons/'+jsonData.aditional.pdf;
									window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
								}
							},
							failure: function(response, request) {
								var errMessage = 'Error en la petición ' + request.url +
												' status ' + response.status +
												' statusText ' + response.statusText +
												' responseText ' + response.responseText;
								alert(errMessage);
							},
							params: {
								tipo:	'upload-Info-Contracts',
								data:	Ext.util.JSON.encode(record.data),
							}
						});
					}
				}
			},{
				iconCls	:	'x-icon-addon-delete',
				tooltip	:	'Delete Addon',
				callback:	function(grid, record, action, row, col) {
					//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
					if(record.data.addon_name!="Doesn't have Addons"){
						Ext.Msg.show({
							title:'Delete Additional Document',
							msg:'Delete: '+record.data.addon_name+'. Are you sure?',
							buttons: Ext.MessageBox.YESNO,
							fn:function(btn){
								if(btn=="yes"){
									record.data.origen='deleteAddon';
									Ext.Ajax.request({
										url: 'php/grid_data.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											var jsonData = Ext.util.JSON.decode(response.responseText);
											if(jsonData.success){
												if(jsonData.aditional.reload)
													grid.getStore().reload();
												grid.getStore().commitChanges();
											}else{
												grid.getStore().rejectChanges();
												alert('Error en la Edicion');
											}
										},
										failure: function(response, request) {
											Ext.getCmp(grid.idGrid).getStore().rejectChanges();
											var errMessage = 'Error en la petición ' + request.url +
															' status ' + response.status +
															' statusText ' + response.statusText +
															' responseText ' + response.responseText;
											alert(errMessage);
										},
										params: {
											tipo:	'upload-Info-Contracts',
											data:	Ext.util.JSON.encode(record.data),
										}
									});
								}
							}
						});
					}
				}
			}],
			groupActions:userAdministra.indexOf(userid)?[{
				iconCls	:	'x-icon-document-delete',
				qtip	:	'Remove Table',
				callback:	function(grid, records, action, groupId) {
					var dataToSend = new Array();
					dataToSend={
						origen	:	'deleteContract',
						data	:	{}
					};
					dataToSend.data=records[0].data;
					Ext.Msg.show({
						title:'Delete Contrac',
						msg:'Delete: '+records[0].data.namefile+' with all additional documents? Are you sure?',
						buttons: Ext.MessageBox.YESNO,
						fn:function(btn){
							if(btn=="yes"){
								Ext.Ajax.request({
									url: 'php/grid_data.php',
									method: 'POST',
									timeout:0,
									success: function(response, request) {
										var jsonData = Ext.util.JSON.decode(response.responseText);
										if(jsonData.success){
											grid.getStore().reload();
											Ext.Msg.show({
												title:'Info',
												msg:'Delete Contract: Success',
												icon: Ext.MessageBox.INFO,
												buttons: Ext.MessageBox.OK,
											});
										}else{
											Ext.Msg.show({
												title:'Info',
												msg:'Delete Contract: Error',
												icon: Ext.MessageBox.WARNING,
												buttons: Ext.MessageBox.OK,
											});
										}
									},
									failure: function(response, request) {
										var errMessage = 'Error en la petición ' + request.url +
														' status ' + response.status +
														' statusText ' + response.statusText +
														' responseText ' + response.responseText;
										alert(errMessage);
									},
									params: {
										tipo:	'upload-Info-Contracts',
										data:	Ext.util.JSON.encode(dataToSend),
									}
								});
							}
						}
					});
				}
			},{
				iconCls:'x-icon-download-files',
				qtip:'Add Table - with callback',
				callback:function(grid, records, action, groupId) {
					//Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
					records[0].data.origen='seeContract';
					Ext.Ajax.request({
						url: 'php/grid_data.php',
						method: 'POST',
						timeout:0,
						success: function(response, request) {
							var jsonData = Ext.util.JSON.decode(response.responseText);
							if(jsonData.success){
								var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+jsonData.aditional.pdf;
								window.open('/custom_contract/onlydownload.php?url='+url); //Agregado por Jesus
							}
						},
						failure: function(response, request) {
							var errMessage = 'Error en la petición ' + request.url +
											' status ' + response.status +
											' statusText ' + response.statusText +
											' responseText ' + response.responseText;
							alert(errMessage);
						},
						params: {
							tipo:	'upload-Info-Contracts',
							data:	Ext.util.JSON.encode(records[0].data),
						}
					});
				}
				//style:'background-color:blue'
			},{
				iconCls:'x-icon-see-files',
				qtip:'Add Table - with callback',
				callback:function(grid, records, action, groupId) {
					//Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
					records[0].data.origen='seeContract';
					Ext.Ajax.request({
						url: 'php/grid_data.php',
						method: 'POST',
						timeout:0,
						success: function(response, request) {
							var jsonData = Ext.util.JSON.decode(response.responseText);
							if(jsonData.success){
								var Digital=new Date()
								var hours=Digital.getHours()
								var minutes=Digital.getMinutes()
								var seconds=Digital.getSeconds()
								var url = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+jsonData.aditional.pdf;
								window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
							}
						},
						failure: function(response, request) {
							var errMessage = 'Error en la petición ' + request.url +
											' status ' + response.status +
											' statusText ' + response.statusText +
											' responseText ' + response.responseText;
							alert(errMessage);
						},
						params: {
							tipo:	'upload-Info-Contracts',
							data:	Ext.util.JSON.encode(records[0].data),
						}
					});
				}
				//style:'background-color:blue'
			},{
				iconCls	:	'x-icon-contract-seeVars',
				tooltip	:	'View Vars',
				callback:	function(grid, records, action, groupId) {
					//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
					console.debug(records);
					new Ext.util.Cookies.set("idmodifycontract", records[0].data.idContract);
					new Ext.util.Cookies.set("typemodifycontract", 'contract');
					new Ext.util.Cookies.set("userCustomBackOffice", records[0].data.userid);

					var miventana = new Ext.Window({
						title: 'View Document(s) Variables: '+records[0].data.namefile,
						width: 970,
						autoScroll:	false,
						height: Ext.getBody().getViewSize().height,
						maximizable: false,
						modal: true,
						resizable: false,
						maximized: false,
						constrain: true,
						items:[{
							xtype	:	'panel',
							id:'panelToiRenderDocsBackoffice'
						}],
						listeners:{
							beforerender:function(){
								Ext.getCmp('panelToiRenderDocsBackoffice').add({
									//closable 	:	true,
									width		:	955,
									height		:	Ext.getBody().getViewSize().height-25,
									autoScroll	:	true,
									id			:	'idmodifycontract',
									autoLoad	:	{
										url		:	'/custom_contract/manager.php',
										scripts	:	true
									}
								});
								setTimeout(function(){
									/*Ext.getCmp('idmodifycontract').doLayout();
									Ext.getCmp('formulario').doLayout();
									console.debug(Ext.getCmp('idfieldcontract'));
									console.debug(Ext.getCmp('idfieldcontract').getStore());
									Ext.getCmp('idfieldcontract').getStore().reload();*/
								},15000);
							},
							beforeclose:function(){
								Ext.util.Cookies.set('userCustomBackOffice', null, new Date("January 1, 1970"));
								Ext.util.Cookies.clear('userCustomBackOffice');
							}
						}
					}).show();
				}
			},{
				iconCls:'x-icon-add-addon',
				qtip:'Add Additionals Documents',
				callback:function(grid, records, action, groupId) {
					//Ext.ux.Toast.msg('Callback: icon-add-table', 'Group: <b>{0}</b>, action: <b>{1}</b>, records: <b>{2}</b>', groupId, action, records.length);
					BK_data.standar.contract=0;
					BK_data.standar.addons={};
					var limit=0;
					Ext.each(records,function(item,index){
						if(item.data.addon_name!="Doesn't have Addons")
							limit = limit + 1;
					});
					var temp = [{
						xtype:'fieldset',
						layout:'column',
						autoHeight:true,
						collapsible	:	false,
						contractFileName:records[0].data.namefile,
						idContract:records[0].data.idContract,
						userId:records[0].data.userid,
						id:'upload-contracts-new-addons',
						contractId:BK_data.standar.contract,
						items:[{
							layout: 'form',
							border:false,
							columnWidth:1,
							items:[{
								xtype:'combo',
								store:userGroup.storeAddons,
								valueField: 'id',
								typeAhead: true,
								width:350,
								forceSelection: true,
								mode: 'local',
								value:'0000',
								triggerAction: 'all',
								selectOnFocus: true,
								displayField: 'name',
								editable: false,
								fieldLabel:'Basic Additionals',
								limit:limit,
								father:'upload-contracts-new-addons',
								fatherId:BK_data.standar.contract,
								listeners:{
									select:function( combo, newValue, oldValue ){
										BK_data.standar.addons[combo.fatherId]	=	typeof BK_data.standar.addons[combo.fatherId] == "undefined" ? 1 : BK_data.standar.addons[combo.fatherId]+1;
										if(newValue.data.id!='0000'){
											if(BK_data.standar.addons[combo.fatherId]<=(6-combo.limit)){
												var temp1=[{
													xtype:'fieldset',
													id:'upload-contracts-new-addons-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
													style: 'margin-left:30px',
													columnWidth:1,
													addonFileName:newValue.data.name,
													addonId:BK_data.standar.addons[combo.fatherId],
													cls:'margin-left:20px',
													layout:'column',
													title:'Addon: '+newValue.data.name,
													items:[{
														layout: 'form',
														border:false,
														columnWidth:.45,
														items:[{
															xtype:'textfield',
															width:300,
															id:'addon-FinalName-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															fieldLabel:'Addon Name',
															value:newValue.data.name
														}]
													},{
														layout: 'form',
														border:false,
														columnWidth:.45,
														items:[{
															xtype:'combo',
															columnWidth:.5,
															store:userGroup.storeTemplates,
															valueField: 'id',
															typeAhead: true,
															width:300,
															id:'addon-FinalTemplate-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															forceSelection: true,
															mode: 'local',
															value:'0000',
															triggerAction: 'all',
															selectOnFocus: true,
															displayField: 'name',
															editable: false,
															fieldLabel:'Asign Template',
															listeners:{
																select:function( combo, newValue, oldValue ){

																}
															}
														}]
													},{
														xtype	:	'checkboxgroup',
														width	:	300,
														id		:	'addon_types'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
														items: [{
															boxLabel: 'EMD &nbsp;',
															labelStyle: 'top:10px;',
															name: 'addon-emd-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															id: 'addon-emd-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
														},{
															boxLabel: 'POF &nbsp;',
															name: 'addon-pof-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															id: 'addon-pof-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
														},{
															boxLabel: 'ADDITIONAL &nbsp;',
															name: 'addon-add-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															id: 'addon-add-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId]
														}]
													},{
														layout: 'form',
														border:false,
														columnWidth:.10,
														items:[{
															xtype:'button',
															text: 'Delete',
															father:'upload-contracts-new-addons-'+combo.fatherId+'-'+BK_data.standar.addons[combo.fatherId],
															//father:'addons-standar-'+BK_data.standar.addons[combo.fatherId],
															fatherID:combo.fatherId,
															tooltip:'Delete this addon',
															handler: function(){
																BK_data.standar.addons[this.fatherId]=BK_data.standar.addons[this.fatherId]-1;
																Ext.getCmp(this.father).destroy();
															}
														}]
													}]
												}]
												Ext.getCmp(combo.father).add(temp1);
												Ext.getCmp('upload-contracts-new-addons').doLayout();
												combo.setValue('0000');
											}else{
												BK_data.standar.addons[combo.fatherId]=BK_data.standar.addons[combo.fatherId]-1;
												Ext.MessageBox.alert('Warning', 'Only can be added '+(6-combo.limit)+' additionals documents.');
											}
										}
									}
								}
							}]
						}]
					}];
					var miventana = new Ext.Window({
						title: 'Upload New Additional Document(s) for Contract: '+records[0].data.namefile,
						width: 1300,
						autoScroll:	true,
						height: 400,
						maximizable: false,
						modal: true,
						resizable: false,
						maximized: false,
						constrain: true,
						items:[{
							xtype:'form',
							items:[temp]
						}],
						buttons: [{
							text     : 'Submit',
							handler  : function(){
								Ext.Msg.show({
									title:'Are you sure?',
									msg:'Upload All Additional Document Previously Selected',
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.MessageBox.YESNO,
									fn:function(btn){
										if (btn == 'yes'){
											BK_data.dataToSend.contracts={};

											var contractId	=	0;
											BK_data.dataToSend.contracts[contractId]={
												'idContract':Ext.getCmp('upload-contracts-new-addons').idContract,
												'addons':{}
											}
											Ext.each(Ext.get("upload-contracts-new-addons").select('fieldset[id*=new]').elements,function(item,index){
												var addonN			=	index;
												var addonFileName	=	Ext.getCmp(item.id).addonFileName;
												var addonContractId	=	Ext.getCmp(item.id).addonId;
												var addonRealName	=	Ext.getCmp('addon-FinalName-'+contractId+'-'+addonContractId).getValue();
												var addonEmd		=	Ext.getCmp('addon-emd-'+contractId+'-'+addonContractId).getValue();
												var addonPof		=	Ext.getCmp('addon-pof-'+contractId+'-'+addonContractId).getValue();
												var addonAdd		=	Ext.getCmp('addon-add-'+contractId+'-'+addonContractId).getValue();
												var addonTemplate	=	Ext.getCmp('addon-FinalTemplate-'+contractId+'-'+addonContractId).getValue();
												BK_data.dataToSend.contracts[contractId].addons[addonN]={
													'addonFileName'	:	addonFileName+'.pdf',
													'addonRealName'	:	addonRealName,
													'emd'		:	addonEmd,
													'pof'		:	addonPof,
													'add'		:	addonAdd,
													'addonTemplate'	:	addonTemplate
												}
											});
											Ext.Ajax.request({
												url: 'php/grid_data.php',
												method: 'POST',
												timeout:0,
												success: function(response, request) {
													var jsonData=	Ext.util.JSON.decode(response.responseText);
													var result	=	jsonData.result;
													if(jsonData.success){
														if(result.length<=0){
															Ext.Msg.show({
																title:'Info',
																msg:'Upload additional documents: Successfully',
																icon: Ext.MessageBox.INFO,
																buttons: Ext.MessageBox.OK,
															});
														}else{
															userGroup.reportHtml(result);
														}
														/*Ext.Msg.show({
															title:'Info',
															msg:'Upload additional documents: Successfully',
															icon: Ext.MessageBox.INFO,
															buttons: Ext.MessageBox.OK,
														});*/
													}else{
														Ext.Msg.show({
															title:'Warning',
															msg:'Upload additional documents: Failed',
															icon: Ext.MessageBox.WARNING,
															buttons: Ext.MessageBox.OK,
														});
													}
													grid.getStore().reload();
												},
												failure: function(response, request) {
													var errMessage = 'Error en la petición ' + request.url +
																	' status ' + response.status +
																	' statusText ' + response.statusText +
																	' responseText ' + response.responseText;

													alert(errMessage);
													grid.getStore().reload();
												},
												params: {
													tipo:	'uploadNewsAddons',
													data:	Ext.util.JSON.encode(BK_data.dataToSend),
													user:	Ext.getCmp('upload-contracts-new-addons').userId
												}
											});
										}
									}
								});
							}
						},{
							text     : 'Close',
							handler  : function(){
								miventana.close();
							}
						}]
					}).show();
				}
				//style:'background-color:blue'
			},{
				iconCls:'x-icon-edit-text',
				qtip:'Change Contract Name',
				align:'left',
				callback:function(grid, records, action, groupId) {
					var dlg = Ext.MessageBox.prompt('Change '+records[0].data.type+':', 'Please enter new Contract name:', function(btn, text){
						if (btn == 'ok'){
							records[0].data.origen='changeContractName';
							records[0].data.newName=text;
							Ext.Ajax.request({
								url: 'php/grid_data.php',
								method: 'POST',
								timeout:0,
								success: function(response, request) {
									var jsonData = Ext.util.JSON.decode(response.responseText);
									if(jsonData.success){
										if(jsonData.aditional.reload)
											grid.getStore().reload();
										grid.getStore().commitChanges();
									}else{
										grid.getStore().rejectChanges();
										alert('Error en la Edicion');
									}
								},
								failure: function(response, request) {
									Ext.getCmp(grid.idGrid).getStore().rejectChanges();
									var errMessage = 'Error en la petición ' + request.url +
													' status ' + response.status +
													' statusText ' + response.statusText +
													' responseText ' + response.responseText;
									alert(errMessage);
								},
								params: {
									tipo:	'upload-Info-Contracts',
									data:	Ext.util.JSON.encode(records[0].data),
								}
							});
						}
					});
				}
			}]:[],
			callbacks:{
				'x-icon-see-file':function(grid, record, action, row, col) {
					//Ext.ux.Toast.msg('Callback: icon-plus', 'You have clicked row: <b>{0}</b>, action: <b>{0}</b>', row, action);
				},
				'x-icon-addon-delete':function(grid, record, action, row, col) {

				}
			}
		});

		var mySelectionModelMen = new Ext.grid.CheckboxSelectionModel({
			singleSelect: false
		});
		var originalFunction = Ext.grid.GroupingView.prototype.processEvent;
		Ext.override(Ext.grid.GroupingView, {
			processEvent : function(name, e){
				 if (e.getTarget().tagName == 'INPUT'){
					 e.stopPropagation();
				 }else{
					 originalFunction.apply(this, arguments);
				 }
			}
		});
		Ext.override(Ext.grid.ColumnModel, {
			destroy : function(){
				for(var i = 0, len = this.config.length; i < len; i++){
					Ext.destroy(this.config[i]);
				}
				this.purgeListeners();
			}
		});
		var Grid	=	new Ext.grid.EditorGridPanel({
			autoHeight	:	true,
			id			:	'gridFiles-'+tempData.userid,
			allInfoUser	:	tempData,
			store		:	store,
			loadMask	:	true,
			userid		:	tempData.userid,
			clicksToEdit:	2,
			columns		:	[
				{header: "Contract", hidden: true, sortable: true, dataIndex: 'namefile'},
				{header: "Addon Name", width: 20, sortable: true, dataIndex: 'addon_name', editor: new Ext.form.TextField({allowBlank: false})},
				mySelectionModelMen,
				action
			],
			plugins	:	[
				action
			],
			view	:	new Ext.grid.GroupingView({
				forceFit	:	true,
				groupTextTpl:	'<span class="gridHeaderCheckbox"><input type="checkbox" class="cbx" data-contract="{[ values.rs[0].data["idContract"] ]}"'+
					'data-userid="{[ values.rs[0].data["userid"] ]}"></span>'+
					'{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),
			loadMask	:	true,
			sm			:	mySelectionModelMen,
			listeners	:	{
				render		:	function(){
					this.store.load({params:{userForGrid:this.userid}});
				},
				beforeedit	:	function(e){
					e.record.data.origen='changeAddonName';
					if(e.value=="Doesn't have Addons")
						e.cancel=true;
				}
			}
		});
		var nav = new Ext.Panel({
			split: true,
			layout: 'fit',
			autoWidth: true,
			autoScroll:true,
			items: Grid,
			margins:'3 0 3 3',
			cmargins:'3 3 3 3'
		});

		var win = new Ext.Window({
			title: 'Documents with Additionals for '+tempData.name+' '+tempData.surname,
			closable:true,
			width:1024,
			height:500,
			layout: 'fit',
			items: [nav]
		}).show();
	},
	viewAdvanceSearchAndResult:	{
		init	:	function (tempData){
			var tabPanel	=	new Ext.TabPanel({
				activeTab	:	0,
				autoScroll	:	true,
				items:[
					userGroup.viewAdvanceSearchAndResult.listView(userGroup.storeTemplatesAdvanceSearch,'listViewAdvanceSearch',tempData.userid,'Search\'s'),
					userGroup.viewAdvanceSearchAndResult.listView(userGroup.storeTemplatesAdvanceResult,'listViewAdvanceResult',tempData.userid,'Result\'s')
				]
			});

			var win = new Ext.Window({
				title		:	'Data for '+tempData.name+' '+tempData.surname,
				closable	:	true,
				width		:	500,
				height		:	300,
				modal		:	true,
				layout		:	'fit',
				items		:	[tabPanel],
				listeners	:	{
					close	:	function(){
						userGroup.storeTemplatesAdvanceSearch.load();
						userGroup.storeTemplatesAdvanceResult.load();
					}
				}
			}).show();
		},
		listView	:	function(store,id,userid,title){
			store.load({
				params	:	{
					userCustom:	userid
				}
			});
			var sModel =	new Ext.grid.CheckboxSelectionModel({
				singleSelect:false,
				controlPanel:id,
				listeners:{
					selectionchange:	function(item){
						var l = item.getSelections().length;
						var s = l != 1 ? 's' : '';
						Ext.getCmp(item.controlPanel).setTitle('Advance '+title.substring(0,(title.length-2))+' <i>('+l+' item'+s+' selected)</i>');
					}
				}
			});
			var gridView = new Ext.grid.GridPanel({
				layout:'fit',
				store: store,
				frame	: true,
				//hideHeaders:true,
				loadMask:true,
				emptyText: 'No Advance '+title+' to display',
				autoWidth:true,
				viewConfig: {
					forceFit:true
				},
				columns: [
					new Ext.grid.RowNumberer(),
					sModel,
					{
					header: 'Template Name',
					//autoWidth:true,
					sortable:true,
					dataIndex: 'name'
				}],
				sm:sModel,
				tbar:new Ext.Toolbar({
					items: [{
						iconCls	:	'x-icon-cancel',
						tooltip	:	'Delete ' + title,
						userid	:	userid,
						handler	:	function(){
							Ext.Msg.show({
								title:'Action Delete!',
								msg: 'Are you sure you want to delete these '+title+'?',
								buttons: Ext.Msg.YESNO,
								icon: Ext.MessageBox.QUESTION,
								fn: function (btn){
									if(btn == 'yes'){
										var tempUserid	=	this.userid;
										if(gridView.getSelectionModel().getCount()<=0){
											Ext.Msg.show({
												title:'Info',
												msg:'Please Select once '+title.substring(0,(title.length-2)),
												icon: Ext.MessageBox.INFO,
												buttons: Ext.MessageBox.OK,
											});
											return;
										}
										var idsToSend	=	new Array();
										Ext.each(gridView.getSelectionModel().getSelections(),function(data,index){
											idsToSend.push(Ext.apply(data.data.id));
										});
										Ext.Ajax.request({
											waitMsg: 'Processing...',
											url: 'php/funcionesUserGroup.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												Ext.MessageBox.hide();
												var jsonData=	Ext.util.JSON.decode(response.responseText);
												var result	=	jsonData.result;
												if(jsonData.success){
													if(result.length<=0){
														Ext.Msg.show({
															title:'Info',
															msg:'Action Result: Successfully',
															icon: Ext.MessageBox.INFO,
															buttons: Ext.MessageBox.OK,
														});
														gridView.getStore().load({
															params	:	{
																userCustom:	tempUserid
															}
														});
													}else{
														userGroup.reportHtml(result);
													}
												}else{
													Ext.Msg.show({
														title:'Info',
														msg:'Action Result: Error',
														icon: Ext.MessageBox.WARNING,
														buttons: Ext.MessageBox.OK,
													});
												}
											},
											failure: function(response, request) {
												Ext.MessageBox.hide();
												var errMessage = 'Error en la petición ' + request.url +
																' status ' + response.status +
																' statusText ' + response.statusText +
																' responseText ' + response.responseText;
												alert(errMessage);
											},
											params: {
												accion	:	id=='listViewAdvanceSearch'	?	'deleteSearch'	:	'deleteResult',
												ids		:	idsToSend.join(","),
												userid	:	tempUserid
											}
										});
									}
								},
							});
						}
					}]
				})
			});
			var nav = new Ext.Panel({
				id:id,
				layout:'fit',
				autoWidth:true,
				title:'Advance '+title.substring(0,(title.length-2))+' <i>(0 items selected)</i>',
				items: gridView
			});
			return nav;
		}
	},
	replaceAdditionalAndContractDocuments:function (windowTitle,storeCombo,idAjax,type){
		var usersGroupId = new Array();
		Ext.each(userGroup.grid.getSelectionModel().getSelections(),function(item,index){
			usersGroupId.push(Ext.apply(item.json));
		});
		if(usersGroupId.length>0){
			if(idAjax=="replace-delete-addons-by-name"){
				var storePersonal	=	[[0, 'Select.!'],[1, 'Replace'], [2, 'Delete'], [3, 'Add']];
			}else{
				var storePersonal	=	[[0, 'Select.!'],[1, 'Replace'], [2, 'Delete']];
			}
			var miventana = new Ext.Window({
				title: windowTitle,
				width: 550,
				//height: (type=='Additionals')?260:225,
				maximizable: false,
				modal: true,
				resizable: true,
				maximized: false,
				shadow:false,
				constrain: true,
				items:[{
					layout:'form',
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype:'combo',
						id:'replace-combo-original-action',
						store:new Ext.data.ArrayStore({
							id: 0,
							fields: [
								'id',
								'name'
							],
							data: storePersonal
						}),
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:0,
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Action',
						listeners:{
							select:function(combo, record, index){
								console.debug(record.data);
								if(type=='Additionals'){
									Ext.getCmp('comboPrincipalSeis').show();
								}
								if(record.data.id==3){
									Ext.getCmp('comboPrincipalUno').hide();
									Ext.getCmp('comboPrincipalDos').show();
									Ext.getCmp('comboPrincipalTres').show();
									if(type=='Additionals'){
										Ext.getCmp('comboPrincipalCuatro').show();
										Ext.getCmp('comboPrincipalSeis').hide();
									}
									Ext.getCmp('comboPrincipalCinco').show();
									Ext.getCmp('replace-combo-original-files').label.update(type.substring(0,(type.length-1))+' Doc to be Assigned');
								}else if(record.data.id==2){
									Ext.getCmp('comboPrincipalUno').show();
									Ext.getCmp('comboPrincipalTres').hide();
									Ext.getCmp('comboPrincipalDos').hide();
									if(type=='Additionals'){
										Ext.getCmp('comboPrincipalCuatro').hide();
									}
									Ext.getCmp('comboPrincipalCinco').hide();
									Ext.getCmp('name-to-search').label.update(type.substring(0,(type.length-1))+((type=='Additionals')? ' Doc to be Deleted' : ' to be Deleted'));
								}else{
									Ext.getCmp('comboPrincipalUno').show();
									Ext.getCmp('comboPrincipalDos').hide();
									Ext.getCmp('comboPrincipalTres').show();
									if(type=='Additionals'){
										Ext.getCmp('comboPrincipalCuatro').show();
									}
									Ext.getCmp('comboPrincipalCinco').show();
									Ext.getCmp('replace-combo-original-files').label.update('Replaced by '+type.substring(0,(type.length-1)));
									Ext.getCmp('name-to-search').label.update(type.substring(0,(type.length-1))+((type=='Additionals')? ' Doc to be Replaced' : ' to be Replaced'));
								}
							}
						}
					}]
				},{
					layout:'form',
					id:'comboPrincipalUno',
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype:'combo',
						id:'name-to-search',
						store:storeCombo,
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:'0000',
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:type+' Doc to be Replaced'
					}]
				},{
					layout:'form',
					id:'comboPrincipalDos',
					//hidden:true,
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype: 'superboxselect',
						fieldLabel: 'Contract To be Assigned',
						//emptyText: 'Select Template(s)',
						resizable: true,
						name: 'user_attr[]',
						id:'name-to-searchDos',
						store: userGroup.storeContracts,
						mode: 'local',
						width:350,
						displayField: 'name',
						valueField: 'id',
						queryDelay: 0,
						triggerAction: 'all',
						forceSelection: true,
						forceFormValue: true,
						allowBlank: true,
						listeners:{
							additem:function(combo, value){
								if(value=="0000"){
									combo.removeAllItems();
									var temp="";var cont=0;
									Ext.each(combo.getStore().data.items, function(tempItems, index) {
										if(tempItems.id!="0000"){
											temp += (cont!=0?",":"")+tempItems.id;
											cont=cont+1;
										}
									});
									combo.setValue(temp);
								}
							}
						}
						/*xtype:'combo',
						id:'name-to-searchDos',
						store:userGroup.storeContracts,
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:'0000',
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Contract To be Assigned'*/
					}]
				},{
					layout:'form',
					id:'comboPrincipalTres',
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype:'combo',
						id:'replace-combo-original-files',
						store:storeCombo,
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:'0000',
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Replaced by '+type.substring(0,(type.length-1))
					}]
				},(type=='Additionals')?{
					layout:'form',
					id:'comboPrincipalCuatro',
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype	:	'checkboxgroup',
						width	:	350,
						fieldLabel: 'Type of Document',
						id		:	'addon_types',
						items: [{
							boxLabel: 'EMD &nbsp;',
							labelStyle: 'top:10px;',
							name: 'addon-emd',
							id: 'addon-emd'
						},{
							boxLabel: 'POF &nbsp;',
							name: 'addon-pof',
							id: 'addon-pof'
						},{
							boxLabel: 'ADDITIONAL &nbsp;',
							name: 'addon-add',
							id: 'addon-add'
						}]
					}]
				}:{},
				(type=='Additionals')?{
					layout:'form',
					id:'comboPrincipalSeis',
					//hidden:true,
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype: 'superboxselect',
						fieldLabel: 'Document to be Applied',
						//emptyText: 'Select Template(s)',
						resizable: true,
						name: 'user_attr[]',
						id:'additionals_base_contract',
						store: userGroup.storeContracts,
						mode: 'local',
						width:350,
						displayField: 'name',
						valueField: 'id',
						queryDelay: 0,
						triggerAction: 'all',
						forceSelection: true,
						//value:'0000',
						forceFormValue: true,
						allowBlank: true,
						listeners:{
							additem:function(combo, value){
								if(value=='0000'){
									combo.removeAllItems();
									var temp="";var cont=0;
									Ext.each(combo.getStore().data.items, function(tempItems, index) {
										if(tempItems.id!="0000"){
											temp += (cont!=0?",":"")+tempItems.id;
											cont=cont+1;
										}
									});
									combo.setValue(temp);
								}
							}
						}
					}]
				}:{},{
					layout:'form',
					id:'comboPrincipalCinco',
					labelWidth:150,
					padding:'10 0 0 10',
					items:[{
						xtype:'combo',
						id:'replace-combo-original-template',
						store:userGroup.storeTemplates,
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:'0000',
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Assigned Template'
					}]
				}],
				buttons: [{
					text     : 'Submit',
					handler  : function(){
						if(Ext.getCmp('replace-combo-original-action').getValue()==0){
							Ext.MessageBox.alert('Warning','Please, select a Action');
							return true;
						}
						if(Ext.getCmp('replace-combo-original-action').getValue()!=3){
							if(Ext.getCmp('name-to-search').getValue()=="0000"){
								Ext.MessageBox.alert('Warning','Please, select To be replaced '+type.substring(0,(type.length-1))+'.');
								return true;
							}
						}else{
							if(Ext.getCmp('name-to-searchDos').getValue()=="0000"){
								Ext.MessageBox.alert('Warning','Please, select a Contract to be assigned');
								return true;
							}
						}
						if(Ext.getCmp('replace-combo-original-action').getValue()==1 || Ext.getCmp('replace-combo-original-action').getValue()==3){
							if(Ext.getCmp('replace-combo-original-files').getValue()=="0000"){
								Ext.MessageBox.alert('Warning','Please, select Replaced by '+type.substring(0,(type.length-1))+'.');
								return true;
							}
						}
						Ext.Msg.show({
							title:'Warning!',
							msg:Ext.getCmp('replace-combo-original-action').getRawValue()+' All '+type.substring(0,(type.length-1))+' Document?',
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.MessageBox.YESNO,
							fn:function(btn){
								if (btn == 'yes'){
									Ext.MessageBox.show({
									   msg: 'Processing, please wait...',
									   progressText: 'Saving...',
									   width:300,
									   wait:true,
									   waitConfig: {interval:200},
									   icon:'ext-mb-download' //custom class in msg-box.html
									});

									contractToSend=[];
									temp='';
									if(typeof Ext.getCmp('additionals_base_contract') == 'undefined'){
										temp='';
									}else{
										temp=Ext.getCmp('additionals_base_contract').getValue().split(',');
										Ext.each(temp,function(item,index){
											contractToSend.push(item.substr(0, item.lastIndexOf('.')));
										});
									}
									Ext.Ajax.request({
										waitMsg: 'Processing...',
										url: 'php/grid_data.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											Ext.MessageBox.hide();
											var jsonData=	Ext.util.JSON.decode(response.responseText);
											var result	=	jsonData.result;
											if(jsonData.success){
												if(result.length<=0){
													Ext.Msg.show({
														title:'Info',
														msg:Ext.getCmp('replace-combo-original-action').getRawValue()+' '+type.substring(0,(type.length-1))+' documents: Successfully',
														icon: Ext.MessageBox.INFO,
														buttons: Ext.MessageBox.OK,
													});
												}else{
													userGroup.reportHtml(result);
												}
												miventana.close();
											}else{
												Ext.Msg.show({
													title:'Info',
													msg:Ext.getCmp('replace-combo-original-action').getRawValue()+' '+type.substring(0,(type.length-1))+' documents: Error',
													icon: Ext.MessageBox.WARNING,
													buttons: Ext.MessageBox.OK,
												});
											}
										},
										failure: function(response, request) {
											Ext.MessageBox.hide();
											var errMessage = 'Error en la petición ' + request.url +
															' status ' + response.status +
															' statusText ' + response.statusText +
															' responseText ' + response.responseText;
											alert(errMessage);
										},
										params: {
											tipo	:	idAjax,
											data	:	Ext.util.JSON.encode(usersGroupId),
											name	:	Ext.getCmp('replace-combo-original-action').getValue()==3	?	Ext.getCmp('name-to-searchDos').getValue()	:	Ext.getCmp('name-to-search').getValue().substr(0, Ext.getCmp('name-to-search').getValue().lastIndexOf('.')),
											file	:	Ext.getCmp('replace-combo-original-files').getValue(),
											tpl 	:	Ext.getCmp('replace-combo-original-template').getValue(),
											action	:	Ext.getCmp('replace-combo-original-action').getValue(),
											emd		:	typeof Ext.getCmp('addon-emd')!= 'undefined' ? Ext.getCmp('addon-emd').getValue()	: '',
											pof		:	typeof Ext.getCmp('addon-pof')!= 'undefined' ? Ext.getCmp('addon-pof').getValue()	: '',
											add		:	typeof Ext.getCmp('addon-add')!= 'undefined' ? Ext.getCmp('addon-add').getValue()	: '',
											contract		: contractToSend.join("','")
										}
									});
								}
							}
						});
					}
				},{
					text     : 'Close',
					handler  : function(){
						miventana.close();
					}
				}],
				listeners:{
					afterrender:	function(){
						Ext.getCmp('comboPrincipalDos').hide();
					}
				}
			}).show();
		}else
			Ext.MessageBox.alert('Warning','Please, select some user.');
	},
	replaceAdvanceSearchAndResult:function (storeCombo,camptitId,fieldCombo,windowsTitle){
		var usersGroupId = new Array();
		Ext.each(userGroup.grid.getSelectionModel().getSelections(),function(item,index){
			usersGroupId.push(Ext.apply(item.json.userid));
		});
		if(usersGroupId.length<=0){
			Ext.MessageBox.alert('Warning','Please, select some user.');
			return;
		}
		var miventana = new Ext.Window({
			title: windowsTitle,
			width: 600,
			maximizable: false,
			modal: true,
			resizable: true,
			maximized: false,
			autoScroll: true,
			constrain: true,
			items:[{
				layout:'form',
				//labelWidth:150,
				bodyStyle: 'padding-left: 30px; padding-top: 10px;maxHeight: 600px;',
				//padding:'10 0 0 10',
				items:[{
					xtype:'combo',
					id:'functionToExcecute',
					store:new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'id',
							'name'
						],
						data: [[0, 'Select.!'],[1, 'Add only if it does not exist'], [2, 'Add if it does not exist and replace if it exists'], [3, 'Only Delete if it exists']]
					}),
					valueField: 'id',
					typeAhead: true,
					width:350,
					forceSelection: true,
					mode: 'local',
					value:0,
					triggerAction: 'all',
					selectOnFocus: true,
					displayField: 'name',
					editable: false,
					fieldLabel:'Action'
				}]
			},{
				layout:'form',
				bodyStyle: 'padding-left: 30px; padding-top: 10px;maxHeight: 600px;',
				items:[{
					xtype: 'superboxselect',
					fieldLabel: 'Templates',
					emptyText: 'Select Template(s)',
					resizable: true,
					name: 'user_attr[]',
					id:'valueFromCombo',
					store: storeCombo,
					mode: 'local',
					width:410,
					displayField: 'name',
					valueField: 'id',
					queryDelay: 0,
					triggerAction: 'all',
					forceSelection: true,
					forceFormValue: true,
					allowBlank: true,
					listeners:{
						additem:function(combo, value){
							if(value=="0000"){
								combo.removeAllItems();
								var temp="";var cont=0;
								Ext.each(combo.getStore().data.items, function(tempItems, index) {
									if(tempItems.id!="0000"){
										temp += (cont!=0?",":"")+tempItems.id;
										cont=cont+1;
									}
								});
								combo.setValue(temp);
							}
							miventana.center();
						},
						removeitem:function(combo,value){
							miventana.center();
						}
					}
				}]
			}],
			buttons: [{
				text     : 'Process',
				handler  : function(){
					if(Ext.getCmp('valueFromCombo').getValue()==""){
						Ext.MessageBox.alert('Warning','Please, select a template.');
						return;
					}
					if(Ext.getCmp('functionToExcecute').getValue()==0){
						Ext.MessageBox.alert('Warning','Please, select an action.');
						return;
					}
					Ext.Msg.show({
						title:'Be Careful!',
						msg: 'Are you sure you want to execute this Action?',
						buttons: Ext.Msg.YESNO,
						icon: Ext.MessageBox.QUESTION,
						fn: function (btn){
							if(btn == 'yes'){
								switch(Ext.getCmp('functionToExcecute').getValue()){
									case 1:
										var actionToSend	=	fieldCombo=='Advance Search'	?	'addSearch'		:	'addResult';
									break;
									case 2:
										var actionToSend	=	fieldCombo=='Advance Search'	?	'replaceSearch'	:	'replaceResult';
									break;
									case 3:
										var actionToSend	=	fieldCombo=='Advance Search'	?	'deleteSearch'	:	'deleteResult';
									break;
								}
								var recordsToSend	=	new Array();
								myMask.show();
								Ext.Ajax.request({
									//waitMsg: 'Processing...',
									url: 'php/funcionesUserGroup.php',
									method: 'POST',
									timeout:0,
									success: function(response, request) {
										miventana.close();
										myMask.hide();
										var jsonData=	Ext.util.JSON.decode(response.responseText);
										var result	=	jsonData.result;
										if(jsonData.success){
											if(result.length<=0){
												Ext.Msg.show({
													title:'Info',
													msg:'Action Result: Successfully',
													icon: Ext.MessageBox.INFO,
													buttons: Ext.MessageBox.OK,
												});
											}else{
												userGroup.reportHtml(result);
											}
										}else{
											Ext.Msg.show({
												title:'Info',
												msg:'Action Result: Error',
												icon: Ext.MessageBox.WARNING,
												buttons: Ext.MessageBox.OK,
											});
										}
									},
									failure: function(response, request) {
										miventana.close();
										myMask.hide();
										var errMessage = 'Error en la petición ' + request.url +
														' status ' + response.status +
														' statusText ' + response.statusText +
														' responseText ' + response.responseText;
										alert(errMessage);
									},
									params: {
										accion	:	actionToSend,
										ids		:	Ext.getCmp('valueFromCombo').getValue(),
										userid	:	usersGroupId.join(",")
									}
								});
							}
						},
					});
				}
			},{
				text     : 'Close',
				handler  : function(){
					miventana.close();
				}
			}]
		}).show();
	},
	specialFilters	:	{
		init	:	function(type){
			var miventana = new Ext.Window({
				title: "Filter By Additional Document Options",
				//id:Ext.id(),
				width:530,
				height: 210,
				maximizable: false,
				//minimizable:true,
				//modal: true,
				resizable: false,
				maximized: false,
				constrain: true,
				items:[{
					layout:'form',
					padding:'10 0 0 10',
					labelWidth:140,
					items:[{
						xtype:'combo',
						id:'search-combo-additional-files',
						store:userGroup.storeAddons,
						valueField: 'id',
						typeAhead: true,
						width:350,
						//forceSelection: true,
						mode: 'local',
						value:'0000',
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Additional Documents',
						listeners:{
							select:function(combo, record, index){
								//console.debug(combo, record, index);
							}
						}
					}]
				},{
					xtype: 'fieldset',
					autoHeight: true,
					items: [{
						xtype: "radiogroup",
						fieldLabel: "Type Search",
						id: "typeResultGroup",
						columns: 1,
						defaults: {xtype: "radio",name: "typeResult"},
						items: [{
							boxLabel: 'All the ones that have it',
							inputValue: "1"
						},{
							boxLabel: 'All the ones that don\'t have it',
							inputValue: "2"
						},{
							boxLabel: 'All no matter if it has it or not',
							inputValue: "3"
						}]
					},{
						xtype:'combo',
						id:'destination',
						store:new Ext.data.ArrayStore({
							id: 0,
							fields: [
								'id',
								'name'
							],
							data: [[0, 'Select.!'],[1, 'Previously Group Selected'], [2, 'Previously Check Selecction']]
						}),
						valueField: 'id',
						typeAhead: true,
						width:350,
						forceSelection: true,
						mode: 'local',
						value:0,
						triggerAction: 'all',
						selectOnFocus: true,
						displayField: 'name',
						editable: false,
						fieldLabel:'Excecute at'
					}]
				}],
				buttons:[{
					text:"Search",
					handler:function(){
						if(Ext.getCmp('search-combo-additional-files').getValue()=="0000"){
							Ext.Msg.show({
								title:'Warning',
								msg:'Select a Additional Document',
								icon: Ext.MessageBox.INFO,
								buttons: Ext.MessageBox.OK,
							});
							return;
						}
						if(Ext.getCmp('destination').getValue()=="0"){
							Ext.Msg.show({
								title:'Warning',
								msg:'Select Excecute at',
								icon: Ext.MessageBox.INFO,
								buttons: Ext.MessageBox.OK,
							});
							return;
						}
						if(!Ext.getCmp('typeResultGroup').getValue()){
							Ext.Msg.show({
								title:'Warning',
								msg:'Select a Result Type',
								icon: Ext.MessageBox.INFO,
								buttons: Ext.MessageBox.OK,
							});
							return;
						}
						userGroup.specialFilters.runSpecial({
							type		:	type,
							title		:	Ext.getCmp('search-combo-additional-files').getRawValue(),
							file		:	Ext.getCmp('search-combo-additional-files').getRawValue(),
							destination	:	Ext.getCmp('destination').getValue(),
							result		:	Ext.getCmp('typeResultGroup').getValue().inputValue,
							users		:	""
						});
					}
				},{
					text:"Cancel",
					handler:function(){
						miventana.close();
					}
				}]
			}).show();
		},
		runSpecial	:	function(params){
			if(params.destination==1){
				if(userGroup.currentNode=="All"){
					Ext.Msg.show({
						title:'Warning?',
						msg: 'Are you sure you would like to search for all users?',
						buttons: Ext.Msg.YESNO,
						fn: function (btn){
							if(btn == 'yes'){
								params.title	=	params.title+" For All Users";
								userGroup.specialFilters.bodySpecial(params);
							}
						}
					});
				}else{
					params.title	=	params.title+' only for "'+userGroup.currentNodeText+'" Group';
					userGroup.specialFilters.bodySpecial(params);
				}
			}else{
				var usersGroupId = new Array();
				Ext.each(userGroup.grid.getSelectionModel().getSelections(),function(item,index){
					usersGroupId.push(Ext.apply(item.json.userid));
				});
				if(usersGroupId.length<=0){
					Ext.MessageBox.alert('Warning', 'You have not selected any user.!');
					return;
				}
				params.users	=	usersGroupId.join(",");
				userGroup.specialFilters.bodySpecial(params);
			}
		},
		bodySpecial	:	function(params){
			myMask.show();
			Ext.Ajax.request({
				url : 'php/funcionesUserGroup.php',
				params :	{
					accion		:	'filterByAdditionalDocument',
					node		:	userGroup.currentNode,
					group		:	false,
					result		:	params.result,
					destination	:	params.destination,
					file		:	params.file,
					users		:	params.users
				},
				scope	:	this,
				success : function(response,json) {
					var jsonData=	Ext.util.JSON.decode(response.responseText);
					// create the data store
					var fields	=	[
						   {name: 'userid'},
						   {name: 'name'}
					];
					for (var i=0;i<jsonData.totalDocs;i++){
						fields.push({
							name:"col"+(i+1)
						});
					}
					var storeSpecial = new Ext.data.JsonStore({
						autoLoad:	true,
						fields	:	fields,
						data	:	jsonData.grid
					});
					var smSpecial	=	new Ext.grid.CheckboxSelectionModel({
						listeners: {
							selectionchange: function(a) {
							}
						}
					});
					var cmTemp=[
						//new Ext.grid.RowNumberer(),
						{
							header		:	'USERID',
							id			:	'userid',
							locked		:	true,
							dataIndex	:	'userid',
							sortable	:	true,
							width		:	50
						},{
							header		:	'Name',
							locked		:	true,
							dataIndex	:	'name',
							sortable	:	true,
							width		:	180
						}
					];
					for (var i=0;i<jsonData.totalDocs;i++){
						cmTemp.push({
							dataIndex:"col"+(i+1),
							sortable:true,
							renderer: function(value, meta, record, rowIndex, colIndex, store) {
								if(value!=""){
									console.debug(meta);
									var temp = value.split("|");
									if(temp[1]=="1")	colorF="colorVerde";	else	colorF="colorRojo";

									meta.attr 	=	'ext:qtip="' + temp[0] + '"';
									newWidth		=	userGroup.letterWidth(temp[0]);
									if(cmSpecial.getColumnWidth(colIndex)<newWidth)
										cmSpecial.setColumnWidth(colIndex,newWidth);

									meta.css	+=	colorF;
									return temp[0];
								}else
									return value;
							}
						});
					}
					var cmSpecial = new Ext.ux.grid.LockingColumnModel(cmTemp);
					var gridSpecial = new Ext.grid.GridPanel({
						store		:	storeSpecial,
						colModel	:	cmSpecial,
						view		:	new Ext.ux.grid.LockingGridView(),
						stripeRows	:	true,
						listeners	:	{
							afterrender:function(gridtemp){
								gridtemp.getView().refresh();
							}
						}
					});
					myMask.hide();
					var miventanaResult = new Ext.Window({
						title		:	"Filter By Additional Document: "+params.title,
						textTitle	:	params.title,
						layout		:	'fit',
						id			:	Ext.id(),
						width		:	Ext.getBody().getViewSize().width-300,
						height		:	Ext.getBody().getViewSize().height-300,
						maximizable	:	false,
						minimizable	:	true,
						tbar		:	{
							items	:	[{
								xtype	:	'button',
								//iconCls	:'x-icon-add',
								userids	:	jsonData.users,
								text	:	'Copy all UserID\'s',
								handler	:	function(button){
									window.prompt("Copy to clipboard: Ctrl+C, Enter", button.userids);
								}
							}]
						},
						resizable	:	false,
						maximized	:	false,
						constrain	:	true,
						items		:	gridSpecial,
						listeners	:	{
							minimize:	function(window){
								userGroup.minimisable(window.id,'Search: '+window.textTitle,window);
							}
						}
					}).show();
				},
				failure	:	function(response, request) {
					myMask.hide();
				}
			});
		}
	},
	insertCustomFeature	:	function (data,grid){
		var parameters	=	{
			title:'',
			mode:false
		}

		if(data != null){
			parameters.title='Edit Feature';
			parameters.mode=true;
		}else{
			parameters.title='Add Feature';
		}
		Ext.form.Field.prototype.msgTarget = 'side';
		var miventana = new Ext.Window({
			title: parameters.title,
			width: 450,
			maximizable: false,
			modal: true,
			resizable: false,
			maximized: false,
			constrain: true,
			items:[{
				xtype:'form',
				id:'formFeatures',
				padding:'10 0 0 10',
				defaults:{
					width:298
				},
				items:[{
		        	xtype:'hidden',
		        	name:'id',
		        	value:(parameters.mode==true ? data.id : null)
		        },{
					xtype:'textfield',
					allowBlank:false,
					vtypeText:'Jesus',
					name:'feature',
					value:(parameters.mode==true ? data.feature : ''),
					fieldLabel:'Feature Name'
				},{
					xtype:'combo',
					fieldLabel:'Feature type',
					store:userGroup.storeFeaturesType,
					hiddenName:'type',
					id:'camptitFieldsTypes',
			        displayField:'name',
			        typeAhead: true,
			        valueField:'id',
			        lazyRender:true,
			        mode: 'local',
			        editable:false,
			        forceSelection: true,
			        allowBlank:false,
			        triggerAction: 'all',
			        emptyText:'Select a Feature Type.',
			        selectOnFocus:true,
			        listeners:{
			        	'select':function(combo, newValue, oldValue){
							var record = combo.findRecord(combo.valueField || combo.displayField, combo.getValue());
							console.debug(record);
			        		Ext.getCmp('noteFeatureType').setText(record.data.desc);
			        		if(record.data.id=='1' || record.data.id=='2'){
			        			Ext.getCmp('camptitFields').show();
			        			Ext.getCmp('camptitFields').allowBlank=false;
			        		}else{
			        			Ext.getCmp('camptitFields').hide();
			        			Ext.getCmp('camptitFields').allowBlank=true;
			        		}
			        		if(record.data.id=='2'){
			        			Ext.getCmp('buttonGroupHelpFeatures').show();
			        			Ext.getCmp('featureCondition').show();
			        			Ext.getCmp('featureCondition').allowBlank=false;
			        		}else{
			        			Ext.getCmp('buttonGroupHelpFeatures').hide();
			        			Ext.getCmp('featureCondition').hide();
			        			Ext.getCmp('featureCondition').setValue('');
			        			Ext.getCmp('featureCondition').allowBlank=true;
			        		}
			        	},
			        	'afterrender' : function(){
		        			if(parameters.mode==true){
			        			var combo=Ext.getCmp('camptitFieldsTypes');
			        			var record=combo.store.getById(data.type);
			        			var index=combo.store.indexOf(record);
			        			combo.setValue(combo.store.getAt(index).get(combo.valueField));
			        			combo.fireEvent('select',combo,combo.store.getAt(index),index);

		        				if(data.type=='1' || data.type=='2'){
				        			//Camptit Combo
				        			var combo=Ext.getCmp('camptitFields');
				        			var record=combo.store.getById(data.camptit);
				        			var index=combo.store.indexOf(record);
				        			combo.setValue(combo.store.getAt(index).get(combo.valueField));
				        			combo.fireEvent('select',combo,combo.store.getAt(index),index);
				        		}
			        		}
						}
			        }
				},{
					xtype:'label',
					id:'noteFeatureType',
					text:'',
					fieldLabel:'About feature type'
				},{
					xtype:'combo',
					id:'camptitFields',
					store:userGroup.storeCamptit,
					emptyText:'Select a Camptit Field.',
					hiddenName:'camptit',
					lazyRender:true,
					valueField:'id',
					typeAhead:true,
					listWidth:400,
					hidden:true,
					forceSelection:true,
					mode:'local',
					triggerAction:'all',
					selectOnFocus:true,
					displayField:'description',
					editable:false,
					fieldLabel:'Camptit Fields'
				},{
					xtype:'textfield',
					id:'featureCondition',
					name:'camptitCondition',
					hidden:true,
					value:(parameters.mode==true ? data.camptitCondition : ''),
					allowBlank:false,
					emptyText:'Example =\'hire contract name\' or any condition',
					fieldLabel:'Condition'
				},{
					xtype: 'buttongroup',
			        columns: 3,
			        id:'buttonGroupHelpFeatures',
			        hidden:true,
			        width:403,
					title: 'Use this for special names, will be copied to clipboard',
			        items: [{
			            text: 'Document Names',
			            scale: 'large',
			            handler:function(){
			            	userGroup.insertCustomFeatureClipBoard(userGroup.storeContracts,'Basic Contract\'s','Add new Feature as Contract');
			            }
			        },{
			            text: 'Additional Document Names',
			            scale: 'large',
			            handler	:	function(){
							userGroup.insertCustomFeatureClipBoard(userGroup.storeAddons,'Basic Additional\'s','Add new Feature as Additional Document');
						}
			        },{
			            text: 'Escrow Agent Names',
			            scale: 'large',
			            handler	:	function(){
							userGroup.insertCustomFeatureClipBoard(userGroup.storeTemplatesAdvanceSearch,'Escrow Agent','Add new Feature as Escrow Agent Name');
            			}
			        },{
			            text: 'Advance Search',
			            scale: 'large',
			            handler	:	function(){
							userGroup.insertCustomFeatureClipBoard(userGroup.storeTemplatesAdvanceSearch,'Advance Search','Add new Feature as Advance Search');
            			}
			        }]
				}]
			}],
			buttons: [{
				text     : (parameters.mode==true ? 'Edit' : 'Add'),
				handler  : function(){
					var form=Ext.getCmp('formFeatures').getForm();
					if(form.isValid()){
						form.submit({
							waitMsg: 'Processing...',
							url: 'php/funcionesUserGroup2.php',
							method: 'POST',
							timeout:0,
							success: function(form, acton) {
								Ext.MessageBox.hide();
								if(acton.result.success){
									miventana.close();
									grid.getStore().reload();
									Ext.Msg.show({
										title:'Info',
										msg:(parameters.mode==true ? 'Edit' : 'Add')+' Feature: Successfully',
										icon: Ext.MessageBox.INFO,
										buttons: Ext.MessageBox.OK,
									});
								}else{
									miventana.close();
									Ext.Msg.show({
										title:'Info',
										msg:(parameters.mode==true ? 'Edit' : 'Add')+' Feature: Error',
										icon: Ext.MessageBox.WARNING,
										buttons: Ext.MessageBox.OK,
									});
								}
							},
							failure: function(response, request) {
								miventana.close();
								Ext.MessageBox.hide();
								var errMessage = 'Error en la petición ' + request.url +
												' status ' + response.status +
												' statusText ' + response.statusText +
												' responseText ' + response.responseText;
								alert(errMessage);
							},
							params: {
								accion	:	'updateFeatures'
							}
						});
					}
				}
			},{
				text     : 'Close',
				handler  : function(){
					miventana.close();
				}
			}]
		}).show();
	},
	insertCustomFeatureClipBoard	:	function (storeCombo,fieldCombo,windowsTitle){
		var miventana = new Ext.Window({
			title: windowsTitle,
			width: 500,
			height: 100,
			maximizable: false,
			modal: true,
			resizable: false,
			maximized: false,
			constrain: true,
			items:[{
				layout:'form',
				padding:'10 0 0 10',
				items:[{
					xtype			:	'combo',
					id				:	'special-name-to-add',
					store			:	storeCombo,
					valueField		:	'id',
					typeAhead		:	true,
					width			:	350,
					forceSelection	:	true,
					mode			:	'local',
					value			:	'0000',
					triggerAction	:	'all',
					selectOnFocus	:	true,
					displayField	:	'name',
					editable		:	false,
					fieldLabel		:	fieldCombo,
					listeners:{
						'select':function(){
							$('#copyToClipBoardButton').attr({'data-clipboard-text':Ext.getCmp('special-name-to-add').getRawValue()});
						}
					}
				}]
			}],
			buttons: [{
				text     : 'Copy to ClipBoard',
				id:'copyToClipBoardButton',
				handler  : function(){
					/*if(Ext.getCmp('special-name-to-add').getValue()!="0000"){
						var recordsToSend	=	new Array();
						var featureValue	=	"";
						if(camptitId=="XX-1" || camptitId=="XX-2")
							featureValue	=	Ext.getCmp('special-name-to-add').getValue().substr(0, Ext.getCmp('special-name-to-add').getValue().lastIndexOf('.'));
						if(camptitId=="XX-3" || camptitId=="XX-4")
							featureValue	=	Ext.getCmp('special-name-to-add').getRawValue();
						recordsToSend.push(Ext.apply({
							id:'NEW-OTHER-ITEM',
							changes:{
								feature:featureValue,
								camptit:camptitId
							}
						}));
						Ext.MessageBox.show({
						   msg: 'Processing, please wait...',
						   progressText: 'Saving...',
						   width:300,
						   wait:true,
						   waitConfig: {interval:200},
						   icon:'ext-mb-download' //custom class in msg-box.html
						});
						Ext.Ajax.request({
							waitMsg: 'Processing...',
							url: 'php/funcionesUserGroup2.php',
							method: 'POST',
							timeout:0,
							success: function(response, request) {
								Ext.MessageBox.hide();
								var jsonData=	Ext.util.JSON.decode(response.responseText);
								var result	=	jsonData.result;
								if(jsonData.success){
									miventana.close();
									grid.getStore().reload();
									Ext.Msg.show({
										title:'Info',
										msg:'Insert Feature: Successfully',
										icon: Ext.MessageBox.INFO,
										buttons: Ext.MessageBox.OK,
									});
								}else{
									miventana.close();
									Ext.Msg.show({
										title:'Info',
										msg:'Insert Feature: Error',
										icon: Ext.MessageBox.WARNING,
										buttons: Ext.MessageBox.OK,
									});
								}
							},
							failure: function(response, request) {
								miventana.close();
								Ext.MessageBox.hide();
								var errMessage = 'Error en la petición ' + request.url +
												' status ' + response.status +
												' statusText ' + response.statusText +
												' responseText ' + response.responseText;
								alert(errMessage);
							},
							params: {
								accion	:	'updateFeatures',
								records	:	Ext.util.JSON.encode(recordsToSend),
							}
						});
					}else{
						Ext.MessageBox.alert('Warning','Please, select a option.');
					}*/
				}
			},{
				text     : 'Close',
				handler  : function(){
					miventana.close();
				}
			}],
			listeners:{
				'afterrender':function(){
					//var el=Ext.getCmp('copyToClipBoardButton').getClickEl().select("button").elements[0];
					//userGroup.copyToClipBoard(el);
					userGroup.copyToClipBoard();
				}
			}
		}).show();
	},
	viewHistory:function (data){

		var grid = new Ext.grid.GridPanel({
			baseCls			: 'cellComplete',
            border: false,
				viewConfig: {
					forceFit:true
				},
			store: new Ext.data.Store({
				autoLoad :true,
				proxy: userGroup.dataProxy,
				baseParams: {
					accion	:'listHistory',
					id	: data.id
				},
				reader:new Ext.data.JsonReader({
				root: 'logs',
				totalProperty: 'total',
				},
					 new Ext.data.Record.create([
						{name: 'id_log', type: 'int'},
						{name: 'action', type: 'string'},
						{name: 'ejecutor', type: 'int'},
						{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
						{name: 'mando', type: 'int'},
						{name: 'userid', type: 'int'},
						{name: 'user_group' , type: 'string'},
						{name: 'usr_client' , type: 'string'},
						{name: 'usr_program' , type: 'string'},
						{name: 'usr_admin' , type: 'string'},
						{name: 'group', type :'string'}
					])
				)
			}),
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{
						header: 'Id log',
						dataIndex: 'id_log',
						sortable: true,
						width: 60
					},{
						header: 'Action',
						dataIndex: 'action',
						sortable: true,
						width: 350
					},{
						header: 'Apply',
						sortable: true,
						dataIndex: 'usr_program',
						width: 250
					},{
						header: 'Authorized',
						sortable: true,
						dataIndex: 'usr_admin',
						width: 250
					},{
						header: 'Date',
						dataIndex: 'date',
						sortable: true,
						width: 150,
						renderer: Ext.util.Format.dateRenderer('Y/m/d   H:i:s')
					}
				]
			}),
			height	: 400,
			width	: 835,
			frame	: true
		});
		var win=new Ext.Window({
			modal	:true,
			title: 'History of '+data.id,
			closable: true,
			resizable: false,
			maximizable: false,
			plain: true,
			border: false,
			width: 850,
			height: 450,
			items	:[
				{
					xtype	:'panel',
					bodyStyle	:'font-size:14px;',
					pandding	:13,
					html	:'<strong>User:</strong> '+data.name+' (<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+data.userid+')">'+data.userid+'</a>)'
				},
				grid
			]
		});
		win.show();
	},
	minimisable:function(itemId,title,window){
		var tempId	=	Ext.id();
		Ext.getCmp("footerGrouping").add({
			id:tempId,
            text:title,
			itemId:itemId,
            handler: function(element){
				Ext.getCmp(element.itemId).show();
				element.destroy();
				Ext.getCmp("footerGrouping").doLayout();
			}
		});
		Ext.getCmp("footerGrouping").doLayout();
		window.hide(Ext.getCmp(tempId).el);
	},
	downloadExcel:function (){

		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
		myMask.show();
		Ext.Ajax.request({
			url : 'Excel/xlsUserGroup2.php',
			params :
				{
					group : userGroup.currentNode
				},
			scope:this,
			success : function(response) {
				var res=Ext.util.JSON.decode(response.responseText);
				myMask.hide();
				downloadURL('http://reifax.com/backoffice/Excel/'+res.file);
			}
		});
	},
	reportHtml:function(result) {
		var info="";
		$.each(result,function(index,data){
			var temp	=	'<br>Userid: <span style="color:blue;font-weight:bold">'+index+' - '+data[0].userName+'</span><br>';
			$.each(data,function(i,detail){
				temp	+=	'Error: <span style="color:red">'+detail.error+'</span><br>'+
							'Detail: '+detail.detail+'<br>'+
							'other: '+detail.other+'<br>'
			});
			info	+=	temp;
		});
		var nav = new Ext.Panel({
			split: true,
			bodyStyle: 'maxHeight: 600px;',
			autoWidth: true,
			autoScroll:true,
			html: info,
			margins:'3 0 3 3',
			cmargins:'3 3 3 3'
		});

		var win = new Ext.Window({
			title: 'Report',
			closable:true,
			width:800,
			layout: 'fit',
			items: [nav]
		}).show();
	},
	letterWidth:function(letter, minimo,size, font) {
		letter || (letter = 'a');
		size || (size = 13); // px
		font || (font = 'Arial');

		var span = document.createElement('span');
		span.innerHTML = letter;
		span.style.fontFamily = font;
		span.style.fontSize = size+'px';
		span.style.fontWeight = 'bold';

		document.body.appendChild(span);
		var w = span.offsetWidth;
		document.body.removeChild(span); // don't forget to remove comment

		return w<minimo	?	minimo	:	w;
	},
	time_sleep_until:function(timestamp) {
		while (new Date() < timestamp * 1000) {}
		return true;
	},
	copyToClipBoard:function(){
		userGroup.ZeroClipboard = new ZeroClipboard($('#copyToClipBoardButton'));
		userGroup.ZeroClipboard.on( 'ready', function(event) {
			userGroup.ZeroClipboard.on( 'copy', function(event) {
				//event.clipboardData.setData('text/plain', event.target.innerHTML);
				event.clipboardData.setData('text/plain', $(event.target).attr('data-clipboard-text'));
			});

			userGroup.ZeroClipboard.on( 'aftercopy', function(event) {
				//alert('Copied text to clipboard: ' + event.data['text/plain']);
			});
		} );

		userGroup.ZeroClipboard.on( 'error', function(event) {
			ZeroClipboard.destroy();
		});
		//console.debug(client);
	},
	currentNode:'All'
}
var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};

Ext.onReady(userGroup.prepare);
Ext.QuickTips.init()
