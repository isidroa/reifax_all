Ext.namespace('Ext.searchUsers');


Ext.searchUsers.dataString = new Ext.data.SimpleStore({
	fields: ['id'],
		data  : [
		  ['Exact'],
		  ['Start With'],
		  ['Contains'],
		  ['Not Exact'],
		  ['Not Start With'],
		  ['Empty'],
		  ['Not Empty']
		]
});

Ext.searchUsers.dataInt=new Ext.data.SimpleStore({
	fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between']
		]
});

Ext.searchUsers.dataIntUser=new Ext.data.SimpleStore({
	fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between'],
		  ['Multiple']
		]
});

Ext.searchUsers.dataDate=new Ext.data.SimpleStore({
	fields: ['id'],
		data  : [
		  ['Equal'],
		  ['Greater Than'],
		  ['Less Than'],
		  ['Equal or Less'],
		  ['Equal or Greater'],
		  ['Between']
		]
});

Ext.searchUsers.dataBoolean=new Ext.data.SimpleStore({
	fields: ['id','text'],
		data  : [
		  ['-1','-Select-'],
		  ['Y','Yes'],
		  ['N','No']
		]
});

Ext.searchUsers.dataPcid=new Ext.data.SimpleStore({
	fields: ['id'],
		data  : [
		  ['-Select-'],
		  ['CFRI'],
		  ['ASO2'],
		  ['GENERAL'],
		  ['SD']
		]
});

Ext.searchUsers.dataPlatin=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [99,'No']
		]
});
Ext.searchUsers.dataBuy=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});
Ext.searchUsers.dataRealweb=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});

Ext.searchUsers.dataInvesweb=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});
				
Ext.searchUsers.dataProfes=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});

Ext.searchUsers.dataFrecuen=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Monthly'],
		  [2,'Annually']
		]
});

Ext.searchUsers.dataEsp=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});	      

Ext.searchUsers.dataResid=new Ext.data.SimpleStore({
	fields: ['id','texto'],
		data  : [
		  ['','-Select-'],
		  [1,'Yes'],
		  [9,'No']
		]
});	
Ext.searchUsers.cambiaCombo = function(combo, record, index){
	var name = combo.getId().split('_');
	var txt1 = name[1];
	var txt2= name[1]+'_b';
	//alert(txt1+' '+txt2+' '+combo.getValue());
		
	if(combo.getValue()=='Between'){
		Ext.getCmp(txt2.toString()).setVisible(true);
	}		
	else if(combo.getValue()=='Empty' || combo.getValue()=='Not Empty')
	{
		if(Ext.getCmp(txt1.toString()))Ext.getCmp(txt1.toString()).setVisible(false);
		if(Ext.getCmp(txt2.toString()))Ext.getCmp(txt2.toString()).setVisible(false);
	}
	else{
		Ext.getCmp(txt1.toString()).setVisible(true);
		Ext.getCmp(txt2.toString()).setVisible(false);
	}
	Ext.searchUsers.form.doLayout();	
}	

Ext.searchUsers.cambiaComboUserID = function(combo, record, index){
	var name = combo.getId().split('_');
	var txt1 = name[1];
	var txt2= name[1]+'_b';
		
	if(combo.getValue()=='Multiple'){
		Ext.getCmp(txt1.toString()).setWidth(350);
		Ext.getCmp(txt2.toString()).setVisible(false);
	}else if(combo.getValue()=='Between'){
		Ext.getCmp(txt1.toString()).setWidth(100);
		Ext.getCmp(txt2.toString()).setVisible(true);
	}else{
		Ext.getCmp(txt1.toString()).setWidth(100);
		Ext.getCmp(txt2.toString()).setVisible(false);
	}
	Ext.searchUsers.form.doLayout();	
}	

Ext.searchUsers.search = function(_callback){
	if( typeof _callback === "function")
		_callback()
	else
		console.log("vacio")
}

Ext.searchUsers.aStruct=[
		{"name":"name",				"bd":"xima","tabla":"ximausrs","title":"Name","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"surname",			"bd":"xima","tabla":"ximausrs","title":"Surname","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"userid",			"bd":"xima","tabla":"ximausrs","title":"UserID","type":"int","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"email",			"bd":"xima","tabla":"ximausrs","title":"Email","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"executive",		"bd":"xima","tabla":"ximausrs","title":"Acc. Executive","type":"string","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"country",			"bd":"xima","tabla":"ximausrs","title":"Country","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"state",			"bd":"xima","tabla":"ximausrs","title":"State","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"city",				"bd":"xima","tabla":"ximausrs","title":"City","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"address",			"bd":"xima","tabla":"ximausrs","title":"Address","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"hometelephone",	"bd":"xima","tabla":"ximausrs","title":"Home Tlf","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"mobiletelephone",	"bd":"xima","tabla":"ximausrs","title":"Mobile Tlf","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"licensen",			"bd":"xima","tabla":"ximausrs","title":"License #","type":"int","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"cardname",			"bd":"xima","tabla":"creditcard","title":"Card Type","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"cardnumber",		"bd":"xima","tabla":"creditcard","title":"Card #","type":"string","size":"30","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechacobro", 		"bd":"xima","tabla":"usr_cobros","title":"Pay Date","type":"date","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechacobroMonth", 	"bd":"xima","tabla":"usr_cobros","title":"Pay Date","type":"int","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"pseudonimo",		"bd":"xima","tabla":"ximausrs","title":"Pseudonimo","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},   
		{"name":"idstatus",			"bd":"xima","tabla":"usr_cobros","title":"Status","type":"status","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"idusertype",		"bd":"xima","tabla":"ximausrs","title":"UserType","type":"utype","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"accept",			"bd":"xima","tabla":"userterms","title":"Accept","type":"boolean","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"affiliationdate",	"bd":"xima","tabla":"ximausrs","title":"Sing Up","type":"date","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"officenum",		"bd":"xima","tabla":"ximausrs","title":"Office #","type":"string","size":"20","show":"true","value":"","value2":"","condicional":""},
		//{"name":"notes",			"bd":"xima","tabla":"usernotes","title":"Notes","type":"string","size":"35","show":"true","value":"","value2":"","condicional":""},
		{"name":"platinum",			"bd":"xima","tabla":"permission","title":"Platinum","type":"platin","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"buyerspro",		"bd":"xima","tabla":"permission","title":"Buyerspro","type":"buy","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"leadsgenerator",	"bd":"xima","tabla":"permission","title":"Leads Generator","type":"leads","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"homefinder",		"bd":"xima","tabla":"permission","title":"Home Finder","type":"homef","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"realtorweb",		"bd":"xima","tabla":"permission","title":"Realtorweb","type":"realweb","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"investorweb",		"bd":"xima","tabla":"permission","title":"Investorweb","type":"invesweb","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"professional",		"bd":"xima","tabla":"permission","title":"Professional","type":"profes","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"professional_esp",	"bd":"xima","tabla":"permission","title":"Professional Breia","type":"esp","size":"20","show":"true","value":"","value2":"","condicional":""},
    	{"name":"residential",		"bd":"xima","tabla":"permission","title":"Residential","type":"profes","size":"20","show":"true","value":"","value2":"","condicional":""},
	    {"name":"idfrecuency",		"bd":"xima","tabla":"f_frecuency","title":"Frecuency","type":"frecuen","size":"20","show":"true","value":"","value2":"","condicional":""},
		{"name":"idrtyp", 			"bd":"xima","tabla":"usr_registertype","title":"RegisterType","type":"registertype","size":"20","show":"true","value":"","value2":"","condicional":""},		
		{"name":"sendcontract",		"bd":"xima","tabla":"saveddocsent","title":"Send Contract","type":"boolean","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"suspenddate",	"bd":"xima","tabla":"logsstatus","title":"Suspend Date","type":"date","size":"10","show":"true","value":"","value2":"","condicional":""},
		{"name":"veceslog",			"bd":"xima","tabla":"ximausrs","title":"Logs 30 Days","type":"int","size":"4","show":"true","value":"","value2":"","condicional":""},
		{"name":"fechaupd",		"bd":"xima","tabla":"logsusr_cobros","title":"Logs Date Prod","type":"date","size":"","show":"true","value":"","value2":"","condicional":""},
		{"name":"idproducto",		"bd":"xima","tabla":"usr_productobase","title":"Logs Products","type":"cbproduct","size":"","show":"true","value":"","value2":"","condicional":""}
		];

Ext.searchUsers.nuevoAjax = function(){ 
		/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
		lo que se puede copiar tal como esta aqui */
		var xmlhttp=false; 
		try 
		{ 
			// Creacion del objeto AJAX para navegadores no IE
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e)
		{ 
			try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
			catch(E) { xmlhttp=false; }
		}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
		
			return xmlhttp; 
}
Ext.searchUsers.atrapalo = function(){
	var cbo_target,txt_target1,txt_target2,county;
	var _url='';
	var aStruct =Ext.searchUsers.aStruct ;
	
	for (i=0;i<aStruct.length;i++)
	{
			var combo= "com_"+aStruct[i].name;
			//alert(combo)
			var nombre= aStruct[i].name;
			var nombre2= aStruct[i].name+"_b";
			if(aStruct[i].type !== "boolean" && aStruct[i].type !== "status" && aStruct[i].type !== "utype" && aStruct[i].type !== "registertype" && aStruct[i].type !== "pcid" && aStruct[i].type !== "frecuen" && aStruct[i].type !== "cbproduct" && aStruct[i].type !== "leads"
			  && aStruct[i].type !== "platin" && aStruct[i].type !== "buy" && aStruct[i].type !== "homef" && aStruct[i].type !== "realweb" && aStruct[i].type !== "invesweb" && aStruct[i].type !== "profes" && aStruct[i].type !== "esp" && aStruct[i].type !== "resid")
			{ 
				//Para asignar o no a txt_ los elementos de input [Maria Oliveira]
				cbo_target=Ext.getCmp(combo.toString()).getValue();
				txt_target1=Ext.getCmp(nombre.toString()).getValue();
				if(aStruct[i].type=='date' || aStruct[i].type=='int'){
					txt_target2=Ext.getCmp(nombre2.toString()).getValue();
				}else{
					txt_target2='';
				}
				//capturar parametros de busqueda
				aStruct[i].value=txt_target1;
				aStruct[i].value2=txt_target2;
				aStruct[i].condicional=cbo_target;		
			}else
			{ 	
				txt_target1=Ext.getCmp(combo.toString()).getValue();
				if(txt_target1=='-Select-'){
					txt_target1='';
				}
				aStruct[i].value=txt_target1;
				aStruct[i].value2='';
				aStruct[i].condicional=txt_target1;//'Exact'
			}//endif
			//======
			if (cbo_target+''!="undefined")
			{	
				_url+=aStruct[i].name+"='"+aStruct[i].bd+"'$'"+aStruct[i].tabla+"'$'"+aStruct[i].condicional+"'$'"+aStruct[i].value+"'$'"+aStruct[i].value2+"'$'"+aStruct[i].type+"'$'"+aStruct[i].size+"'$'"+aStruct[i].show+"'$'"+aStruct[i].title+"'&";			
			}//endif
	}//endfor
	
	_url=_url.substring(0,_url.length-1);
	var ajax=Ext.searchUsers.nuevoAjax();
	ajax.open("POST", "php/atrapalo_x.php",true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send(_url);
	var mensaje = Ext.MessageBox.show({
		msg: 'Searching...',
		progressText: 'Searching...',
		width:300,
		wait:true,
		waitConfig: {interval:100},
		icon:'ext-mb-download',
		animEl: 'samplebutton'
		});	
	ajax.onreadystatechange=function() 
	{
		if (ajax.readyState==4) 
		{
			mensaje.hide();
			Ext.searchUsers.search();
			Ext.searchUsers.form.doLayout();
		}//readyState
	}//onreadystatechange	}
		
}

Ext.searchUsers.form = new Ext.FormPanel({
	frame:true,
	title: 'Search Users',
	bodyStyle:'padding:5px 5px 0',
	width: screen.width,
	items:[{		
		layout:'column',
		items:[{
			columnWidth:.5,
			layout: 'form',
			items: [{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Name',
				items:[{
					width:120,
					id:'com_name',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact' 
				},{
					width:120,
					xtype:'textfield',
					id: 'name'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'User ID',
				items:[{
					width:120,
					id: 'com_userid',
					xtype:'combo',
					store: Ext.searchUsers.dataIntUser,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
					listeners: {
						 'select': Ext.searchUsers.cambiaComboUserID
						 }
				},{
					width:100,
					xtype:'textfield',
					id: 'userid'
				},{
					width:100,
					xtype:'textfield',
					id: 'userid_b'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Acc. Executive',
				items:[{
					width: 120,
					id:'com_executive',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width: 120,
					xtype:'textfield',
					id: 'executive'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'State',
				items:[{
					width:120,
					id:'com_state',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					editable: false,
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width: 120,
					xtype:'textfield',
					id: 'state'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Address',
				items:[{
					width: 120,
					id: 'com_address',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width: 120,
					xtype:'textfield',
					id: 'address'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Mobile Tlf',
				items:[{
					width: 120,
					id:'com_mobiletelephone',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width: 120,
					xtype:'textfield',
					id: 'mobiletelephone'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'NickName',
				items:[{
					width:120,
					id:'com_pseudonimo',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact'
				},{
					width:120,
					xtype:'textfield',
					id: 'pseudonimo'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Sing up',
				items:[{
					width:120,
					id:'com_affiliationdate',
					xtype:'combo',
					store: Ext.searchUsers.dataDate,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:80,
					xtype:'textfield',
					id: 'affiliationdate',
					anchor:'95%'
				},{
					width:80,
					xtype:'textfield',
					id: 'affiliationdate_b',
					anchor:'95%'
				},{
					width:50,
					xtype:'label',
					text: '(YYYYMMDD)'
				}]
			}/*,{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Notes',
				items:[{
					width:120,
					id:'com_notes',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact'
				},{
					width:120,
					xtype:'textfield',
					id: 'notes'
				}]
			}*/,{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Residential',
				items:[{
					width:120,
					id:'com_residential',
					xtype:'combo',
					store: Ext.searchUsers.dataResid,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Platinum',
				items:[{
					width:120,
					id:'com_platinum',
					xtype:'combo',
					store: Ext.searchUsers.dataPlatin,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Professional',
				items:[{
					width:120,
					id:'com_professional',
					xtype:'combo',
					store: Ext.searchUsers.dataProfes,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Mentoring',
				items:[{
					width:120,
					id:'com_professional_esp',
					xtype:'combo',
					store: Ext.searchUsers.dataEsp,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Buyerspro',
				items:[{
					width:120,
					id:'com_buyerspro',
					xtype:'combo',
					store: Ext.searchUsers.dataBuy,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Leads Generator',
				items:[{
					width:120,
					id:'com_leadsgenerator',
					xtype:'combo',
					store: Ext.searchUsers.dataResid,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Home Finder',
				items:[{
					width:120,
					id:'com_homefinder',
					xtype:'combo',
					store: Ext.searchUsers.dataResid,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Logs 30 Days',
				items:[{
					width:120,
					id: 'com_veceslog',
					xtype:'combo',
					store: Ext.searchUsers.dataInt,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:100,
					xtype:'textfield',
					id: 'veceslog'
				},{
					width:100,
					xtype:'textfield',
					id: 'veceslog_b'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Logs Date Prod',
				items:[{
					width:120,
					id:'com_fechaupd',
					xtype:'combo',
					store: Ext.searchUsers.dataDate,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:80,
					xtype:'textfield',
					id: 'fechaupd',
					anchor:'95%'
				},{
					width:80,
					xtype:'textfield',
					id: 'fechaupd_b',
					anchor:'95%'
				},{
					width:50,
					xtype:'label',
					text: '(YYYYMMDD)'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Logs Products',
				items:[{
					width:120,
						id:'com_idproducto',
						xtype:'combo',
						store: new Ext.data.SimpleStore({
							fields: ['idproducto', 'prod'],
							data : []
						 }),
						editable: false,
						valueField: 'idproducto',
						displayField: 'prod',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
				}]
			},{
                    xtype: 'compositefield',
					labelWidth: 120,
					fieldLabel: 'Send Contract',
					items:[{
						width:120,
						id:'com_sendcontract',
						xtype:'combo',
						store: Ext.searchUsers.dataBoolean,
						editable: false,
						valueField: 'id',
						displayField: 'text',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',            
						selectOnFocus: true,
						allowBlank: false,
						value: '-Select-'
					}]
                }]
		},{
			columnWidth:.5,
			layout: 'form',
			items: [{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Surname',
				items:[{
					width:120,
					id:'com_surname',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact'
				},{
					width:120,
					xtype:'textfield',
					id: 'surname'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Email',
				items:[{
					width:120,
					id: 'com_email',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					anchor:'95%'
				},{
					width:120,
					xtype:'textfield',
					id: 'email'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Country',
				items:[{
					width:120,
					id:'com_country',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width:120,
					xtype:'textfield',
					id: 'country',
					anchor:'95%'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'City',
				items:[{
					width:120,
					id:'com_city',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width:120,
					xtype:'textfield',
					id: 'city'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Home Tlf',
				items:[{
					width:120,
					id: 'com_hometelephone',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width:120,
					xtype:'textfield',
					id: 'hometelephone'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'License #',
				items:[{
					width:120,
					id:'com_licensen',
					xtype:'combo',
					store: Ext.searchUsers.dataInt,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:100,
					xtype:'textfield',
					id: 'licensen'
				},{
					width:100,
					xtype:'textfield',
					id: 'licensen_b'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Office #',
				items:[{
					width:120,
					id: 'com_officenum',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width:120,
					xtype:'textfield',
					id: 'officenum',
					anchor:'95%'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Card Type',
				items:[{
					width:120,
					id:'com_cardname',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
				},{
					width:120,
					xtype:'textfield',
					id: 'cardname',
					anchor:'95%'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Card #',
				items:[{
					width:120,
					id: 'com_cardnumber',
					xtype:'combo',
					store: Ext.searchUsers.dataString,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Exact',
					listeners: {
						 'select': Ext.searchUsers.cambiaCombo 
					}
					
				},{
					columnWidth: 100,
					xtype:'textfield',
					id: 'cardnumber'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Pay Date',
				items:[{
					width:120,
					id:'com_fechacobro',
					xtype:'combo',
					store: Ext.searchUsers.dataDate,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
						listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:80,
					xtype:'textfield',
					id: 'fechacobro',
					anchor:'95%'
				},{
					width:80,
					xtype:'textfield',
					id: 'fechacobro_b',
					anchor:'95%'
				},{
					width:50,
					xtype:'label',
					text: '(YYYYMMDD)'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Pay Date Month',
				items:[{
					width:120,
					id:'com_fechacobroMonth',
					xtype:'combo',
					store: Ext.searchUsers.dataDate,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
						listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:80,
					xtype:'textfield',
					id: 'fechacobroMonth',
					anchor:'95%'
				},{
					width:80,
					xtype:'textfield',
					id: 'fechacobroMonth_b',
					anchor:'95%'
				},{
					width:50,
					xtype:'label',
					text: '(MMDD)'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Status',
				items:[{
					width:120,
					id:'com_idstatus',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['idstatus', 'status'],
						data : []
					 }),
					editable: false,
					valueField: 'idstatus',
					displayField: 'status',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Suspend Date',
				items:[{
					width:120,
					id:'com_suspenddate',
					xtype:'combo',
					store: Ext.searchUsers.dataDate,
					editable: false,
					valueField: 'id',
					displayField: 'id',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: 'Equal',
						listeners: {
						 'select': Ext.searchUsers.cambiaCombo
						 }
				},{
					width:80,
					xtype:'textfield',
					id: 'suspenddate',
					anchor:'95%'
				},{
					width:80,
					xtype:'textfield',
					id: 'suspenddate_b',
					anchor:'95%'
				},{
					width:50,
					xtype:'label',
					text: '(YYYYMMDD)'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'User Type',
				items:[{
					width:120,
					id: 'com_idusertype',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['idusertype', 'usertype'],
						data : []
					 }),
					editable: false,
					valueField: 'idusertype',
					displayField: 'usertype',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Accept',
				items:[{
					width:120,
					id:'com_accept',
					xtype:'combo',
					store: Ext.searchUsers.dataBoolean,
					editable: false,
					valueField: 'id',
					displayField: 'text',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Realtor Web',
				items:[{
					width:120,
					id:'com_realtorweb',
					xtype:'combo',
					store: Ext.searchUsers.dataRealweb,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Investor Web',
				items:[{
					width:120,
					id:'com_investorweb',
					xtype:'combo',
					store: Ext.searchUsers.dataInvesweb,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			}, {
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Register Type',
				items:[{
					width:120,
					id: 'com_idrtyp',
					xtype:'combo',
					store: new Ext.data.SimpleStore({
						fields: ['idrtype', 'rtype'],
						data :[]
					 }),
					editable: false,
					valueField: 'idrtype',
					displayField: 'rtype',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			},{
				xtype: 'compositefield',
				labelWidth: 120,
				fieldLabel: 'Frecuency',
				items:[{
					width:120,
					id:'com_idfrecuency',
					xtype:'combo',
					store: Ext.searchUsers.dataFrecuen,
					editable: false,
					valueField: 'id',
					displayField: 'texto',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',            
					selectOnFocus: true,
					allowBlank: false,
					value: '-Select-'
				}]
			}]
		}]
	}],
	waitTitle   :'Please wait...',
	waitMsg     :'Sending data...',
	buttonAlign: 'center',
	buttons: [{
		text: 'Search',
		handler: function(){
			Ext.searchUsers.atrapalo()
		}
	},{
		text: 'Reset',
		handler: function(){
				Ext.searchUsers.form.getForm().reset();
			}
	}],
	listeners : {
		afterlayout: {
		  fn: function(p) {
			Ext.getCmp('userid_b').setVisible(false);
			Ext.getCmp('licensen_b').setVisible(false);
			Ext.getCmp('affiliationdate_b').setVisible(false);
			Ext.getCmp('fechacobro_b').setVisible(false);
			Ext.getCmp('fechacobroMonth_b').setVisible(false);
			Ext.getCmp('veceslog_b').setVisible(false);
			Ext.getCmp('fechaupd_b').setVisible(false);
			Ext.getCmp('suspenddate_b').setVisible(false);
			Ext.getCmp('com_idstatus').store.loadData(Ext.combos_selec.dataStatus, false);
			Ext.getCmp('com_idusertype').store.loadData(Ext.combos_selec.dataUType, false);
			Ext.getCmp('com_idproducto').store.loadData(Ext.combos_selec.dataProducts, false);
			Ext.getCmp('com_idrtyp').store.loadData(Ext.combos_selec.dataRegisterType, false);
			
		  },
		  single: true
		},
		activate:function(tabpanel){
			Ext.searchUsers.form.doLayout();
		}
	}
});



