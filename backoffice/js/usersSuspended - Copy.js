Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=userssuspended',
		baseParams  :{start:0, limit:100},
		fields: [
			{name: 'userid', type: 'int'}
			,'name'
			,'surname'
			,'status' 
			,'notes'
			,'fechaupd',
			,'email',
			{name: 'veceslog', type: 'int'}	
			,'senddateemail'
//			,'cardholdername'
//			,'cardnumber'
//			,'fecha'
//			,'transactionid'
//			,'paypalid'
//			,'amount'
//			,'amountsinformato'
//			,'priceprod'
//			,'cobrador'
//			,'source'
		]
	});
	
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var panelpag = new Ext.form.FormPanel({
		width: 1000,
		layout: 'form',
		items: [{
			xtype: 'compositefield',
			fieldLabel: 'Range Date',
			items:		
			[{
				xtype: 'datefield',
				width: 100,
				name: 'dayfrom',
				id: 'dayfrom',
				format: 'Y-m-d',
				editable: false,
				value: new Date()			
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dayto',
				id: 'dayto',
				format: 'Y-m-d',			
				editable: false,
				value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: searchCobros
			},{
				width:400,
				fieldLabel:'Template Email',
				name:'cbTemplateEmailGrid',
				id:'cbTemplateEmailGrid',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: true,
				editable: false,
				emptyText : 'Select to view see email template',
				listeners: {
					'select': searchTemplatEmail
				} 
			}]
		}]
	});

	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
            pressed: true,
            enableToggle:false,
            text: '<b>Send Email TEST</b>',
            handler: enviaEmail
        },'-', 
			panelpag
		]
    });

	
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.userid;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function obtenerSeleccionadosNameEmail(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+='"'+selec[i].data.name+' '+ selec[i].data.surname+'" <'+selec[i].data.email+'>';
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;		
	}
	function enviaEmail(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Please select a row');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		obtenidos=obtenidos.replace(/\,/g,'^');
		///Ext.MessageBox.alert('Warning',obtenidos);
		
		var obtenidosall=obtenerSeleccionadosNameEmail();
		obtenidosall=obtenidosall.replace('(','');
		obtenidosall=obtenidosall.replace(')','');
		
		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype:'textarea',
				fieldLabel:'To',
				id:'totext',
				name:'totext',
				value:obtenidosall,
				readOnly:true,
				width:640,
				height:100  
			},{
				xtype: 'hidden',
				name: 'toid',
				name: 'toid',
				value: obtenidos
			},{
				xtype:'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'subject',
				width:640,
				allowBlank: false  
			},{
				xtype: 'htmleditor',
				id: 'bodyemail',
				id: 'bodyemail',
				hideLabel: true,
				height: 300,
				width:700,
				allowBlank: false  
			}],
			buttonAlign: 'center',
			buttons: [{
				text: 'Send',
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/sendingEmail.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Sending email...',
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								w.close();
								//Ext.Msg.alert('Success', action.result.msg);
								
								var variable = Ext.decode(action.response.responseText);
								var resulthtml=	'<table  border="0" cellpadding="0" cellspacing="0">'+
												'	  <tr bgcolor="#FFFFCC" align="center">'+
												'		<td width="40px">&nbsp;</td>'+
												'		<td width="200px"><b>Name</b></td>'+
												'		<td width="200px"><b>Email</b></td>'+
												'		<td width="60px"><b>Send</b></td>'+
												'		<td width="350px"><b>Msg</b></td>'+
												'	  </tr>'
												;
								
								for (var i=0;i<variable.data.length;i++)
								{
									
									
									resulthtml+= '	<tr bgcolor="'+((i%2)==0?'#ffffff':'#fefee9')+'" >'+
												'		<td align="center">'+(i+1)+'</td>'+
												'		<td>'+variable.data[i]['toname']+'</td>'+
												'		<td>'+variable.data[i]['toemail']+'</td>'+
												'		<td align="center">'+(variable.data[i]['sending']==1?'<img src="images/check.png" border="0" />':'<img src="images/cross.gif" border="0" />')+'</td>'+
												'		<td>'+(variable.data[i]['errorsending'].length>0?variable.data[i]['errorsending']:'&nbsp;')+'</td>'+
												'	</tr>'
											;

								}
								resulthtml+='</table>';
								
								
								var w1 = new Ext.Window({
									title: 'Response Email', 
									width: 750,
									height: 500,
									closable :true,
									layout: 'fit',
									autoScroll : true,
									plain: true,
									bodyStyle: 'padding:5px;',
									html: resulthtml
								});
								w1.show();								
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},{
				text: 'Cancel',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'Send Email', 
			width: 750,
			height: 500,
			minWidth: 300,
			minHeight: 200,
			layout: 'fit',
			plain: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
	}
	
	function fillEmail(){
		//store.setBaseParam('idtmail',);
		//store.load({params:{start:0, limit:100}});
		Ext.Ajax.request({  
			waitMsg: 'Loading...',
			url: 'php/grid_data.php',
			method: 'POST', 
			timeout: 100000,
			params: { 
				tipo : 'previewEmails',
				idtmail: Ext.getCmp("cbTemplateEmail").getValue()
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){ 
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);
				Ext.getCmp("subject").setValue(variable.subject);
				Ext.getCmp("bodyemail").setValue(variable.body);
			}
		})
		
	}

	function enviaEmail2(){
		var obtenidos=obtenerSeleccionados();
		var j=0;
		if(obtenidos.length>2){
			j=1;
		}
		if(j==0)
		{	Ext.MessageBox.alert('Warning','Please select a row');
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		obtenidos=obtenidos.replace(/\,/g,'^');
		///Ext.MessageBox.alert('Warning',obtenidos);
		
		var obtenidosall=obtenerSeleccionadosNameEmail();
		obtenidosall=obtenidosall.replace('(','');
		obtenidosall=obtenidosall.replace(')','');
		
		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype:'textarea',
				fieldLabel:'To',
				id:'totext',
				name:'totext',
				value:obtenidosall,
				readOnly:true,
				width:640,
				height:100  
			},{
				xtype: 'hidden',
				name: 'toid',
				name: 'toid',
				value: obtenidos
			},{
				width:640,
				fieldLabel:'Template',
				name:'cbTemplateEmail',
				id:'cbTemplateEmail',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'textcombo'],
					data : Ext.combos_selec.storeTempEmail 
				}),
				hiddenName: 'cbidtmail',
				mode: 'local',
				valueField: 'id',
				displayField: 'textcombo',
				triggerAction: 'all',            
				selectOnFocus: true,
				allowBlank: false,
				editable: false,
				//value: activews,
				listeners: {
					'select': fillEmail
				} 
			},{
				xtype:'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'subject',
				readOnly:true,
				width:640,
				allowBlank: false  
			},{
				xtype: 'htmleditor',
				id: 'bodyemail',
				id: 'bodyemail',
				hideLabel: true,
				readOnly:true,
				height: 300,
				width:700,
				allowBlank: false  
			}],
			buttonAlign: 'center',
			buttons: [{
				text: '<b>Send Email</b>',
				cls: 'x-btn-text-icon',			
				icon: 'images/sendemail.jpg',				
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/sendingEmail.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Sending email...',
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								w.close();
								store.reload();
								//Ext.Msg.alert('Success', action.result.msg);
								
								var variable = Ext.decode(action.response.responseText);
								var resulthtml=	'<table  border="0" cellpadding="0" cellspacing="0">'+
												'	  <tr bgcolor="#FFFFCC" align="center">'+
												'		<td width="40px">&nbsp;</td>'+
												'		<td width="200px"><b>Name</b></td>'+
												'		<td width="200px"><b>Email</b></td>'+
												'		<td width="60px"><b>Send</b></td>'+
												'		<td width="350px"><b>Msg</b></td>'+
												'	  </tr>'
												;
								
								for (var i=0;i<variable.data.length;i++)
								{
									
									
									resulthtml+= '	<tr bgcolor="'+((i%2)==0?'#ffffff':'#fefee9')+'" >'+
												'		<td align="center">'+(i+1)+'</td>'+
												'		<td>'+variable.data[i]['toname']+'</td>'+
												'		<td>'+variable.data[i]['toemail']+'</td>'+
												'		<td align="center">'+(variable.data[i]['sending']==1?'<img src="images/check.png" border="0" />':'<img src="images/cross.gif" border="0" />')+'</td>'+
												'		<td>'+(variable.data[i]['errorsending'].length>0?variable.data[i]['errorsending']:'&nbsp;')+'</td>'+
												'	</tr>'
											;

								}
								resulthtml+='</table>';
								
								
								var w1 = new Ext.Window({
									title: 'Response Email', 
									width: 750,
									height: 500,
									closable :true,
									layout: 'fit',
									autoScroll : true,
									plain: true,
									bodyStyle: 'padding:5px;',
									html: resulthtml
								});
								w1.show();								
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},
				'->'
			,{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'Send Email', 
			width: 750,
			height: 550,
			layout: 'fit',
			plain: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
	}

	function searchCobros(){
		store.setBaseParam('dayfrom',Ext.getCmp("dayfrom").getValue());
		store.setBaseParam('dayto',Ext.getCmp("dayto").getValue());
		store.load();
	}

	function searchTemplatEmail(){
		store.setBaseParam('idtmail',Ext.getCmp("cbTemplateEmailGrid").getValue());
		store.load({params:{start:0, limit:100}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){//onclick="mostrarDetalleUsuers({0},\'customerservices\')"
        return String.format('<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a> ',value);
    }		
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,19);
			return String.format('<img src="images/notes.png" border="0" ext:qtip="{1}"></a>',note,val);
		}	
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,mySelectionModel
			,{header: '', width: 30, sortable: true, align: 'left', dataIndex: 'notes', renderer: comment}
			,{header: "User ID", width: 60, align: 'center', sortable: true, dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 100, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: "Surname", width: 100, align: 'left', sortable: true, dataIndex: 'surname'}			
			,{header: "Status", width: 80, align: 'left', sortable: true, dataIndex: 'status'}
			,{header: "Suspended Date", width: 120, align: 'left', sortable: true, dataIndex: 'fechaupd'}
			,{header: 'Last 30 logs', width: 60, sortable: true, align: 'center', tooltip: 'Last 30 logs', dataIndex: 'veceslog'}
			,{header: "Send Date Email", width: 120, align: 'left', sortable: true, dataIndex: 'senddateemail'}
		],			
		clicksToEdit:2,
		sm: mySelectionModel,
		height:470,
		width: screen.width-50,
		frame:false,
		loadMask:true,
		border: false,
		tbar: pagingBar 
	});
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////
	var mypanel = new Ext.form.FormPanel({
        frame:true,
        title: 'Usuarios suspendidos',
        bodyStyle:'padding:5px 5px 0',
        width: screen.width,
        items: [grid ]
	});
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: mypanel
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
});
