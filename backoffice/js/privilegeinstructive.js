

function marcaEmails(idcont)
{
    var marca='N';
    if(document.getElementById("cb"+idcont).checked==true)
    {
        Ext.getCmp('gridNolaboralDetail').getStore().setBaseParam('instructive_id', idcont);
            Ext.getCmp('gridNolaboralDetail').getStore().load({
                params:{
                    instructive_id: idcont
                    }
            });
    }
    //var marca='N';
    //if(document.getElementById("cb"+idcont).checked==true)marca='Y';
   /* Ext.Ajax.request( 
        {   
            waitMsg: 'Saving changes...',
            url: 'php/grid_edit.php', 
            method: 'POST',
            params: {
                tipo: "marcaemail", 
                key: 'idcont',
                keyID: idcont,
                field: 'view',
                value: marca
            },
            
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error editing');
                var store1 = Ext.getCmp("gridpanel").getStore();
                store1.rejectChanges();
            },
            
            success:function(response,options){
                var rest = Ext.util.JSON.decode(response.responseText);
                
                if(rest.succes==false)
                    Ext.MessageBox.alert('Warning',rest.msg);
                    
                var store1 = Ext.getCmp("gridpanel").getStore();
                store1.commitChanges();
                store1.reload();
            }*/
         }

    
function check(val, p, record){
        var marcado='';
        if(val=='Y'){
            marcado='checked';
        }
        var idc=record.data.instructive_id;
        //var comment = record.data.comment;
        var retorno= String.format('<input type="checkbox" name="cb'+idc+'"  id="cb'+idc+'" onClick="marcaEmails('+idc+')" '+marcado+'>',idc);
        //retorno+= String.format('<a href="javascript:void(0)" onClick="mostrarDetalleEmail({0})" >',idc);
        retorno+= String.format('<img src="images/view.gif" border="0" title="View Details ID: " alt="View Details" ext:qtip=""></a>',idc);
        
        return retorno;
    }

   

    /*Nueva ventana para Gestionar*/
    function viewHtmlToManage(instructive_id) //Ver ventana para gestionar
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id,
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                var readdd = variable.istatus;
                var aprueba = variable.aprueba;

                //mostrarMensaje
                var showMsg = true;
                if(readdd==1)
                {
                    var showMsg = false;
                }
                if(appro == 1){ var aprobar = true;}
                if(readdd ==1)
                {
                    var readonlys = true;
                }
                //5 || user_session==20 || user_session==2342
                var mostrarCombo = true;
                if(aprueba==1)
                {
                    var mostrarCombo = false;
                }


 		//var idcombo = variable.idtopic;
      
       // idcombo.store.filter('instructives_type_id', idcombo);
              
                var btnEdit= [{
                        text: '<b>Save </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        disabled: aprobar,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }]
            

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [
                        {
                            xtype:'textfield',
                            name:'mensaje',
                            fieldLabel:'Message',
                            value: 'Instructive approved, it can not be modified',
                            width:600,
                            hidden: showMsg,
                            readOnly:true

                        },
                        {
                            xtype       :'combo',
                            id          :'istatus',
                            name        :'istatus',
                            hiddenName  :'istatus',
                            fieldLabel  :'Status',
                            allowBlank  :true,
                            width       :200,
                            store       :new Ext.data.SimpleStore({
                                            fields: ['id', 'type'],
                                            data : [['1', 'Approved'], ['0', 'Not Approved']]
                                        }),
                            displayField:'type',
                            valueField  :'id',
                            mode        :'local',
                            triggerAction:'all',
                            emptyText   :'Select..',
                            selectOnFocus:true,
                            value:variable.istatus,
                            disabled: aprobar,
                            hidden: mostrarCombo
                          },
	                    {
	                        xtype:'textfield',
	                        fieldLabel: 'Title',
	                        name: 'title',
	                        id: 'title',
	                        width:690,
	                        value: variable.title,
	                        allowBlank: false,
	                        readOnly: readonlys

	                    },{
	                    xtype       :'combo',
	                    id          :'instructives_type_id',
	                    name        :'instructives_type_id',
	                    hiddenName  :'instructives_type_id',
	                    fieldLabel  :'Group',
	                    allowBlank  :false,
	                    width       :200,
	                    store       :new Ext.data.SimpleStore({
	                                    fields: ['id', 'instructives_type_id'],
	                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
	                                }),
	                    displayField:'instructives_type_id',
	                    valueField  :'id',
	                    value: variable.instructive_type,//valueField:'iditg',
	                    mode        :'local',
	                    triggerAction:'all',
	                    emptyText   :'Select..',
	                    selectOnFocus:true,
	                    readOnly: readonlys,
	                   // listeners: {'select': getComboID}
	                    listeners: 
	                    {
                           select: 
                           {
                               fn:function(combo, value) 
                               {
			                        var idcombo = Ext.getCmp('instructives_type_id').getValue();
		                            var comboCity = Ext.getCmp('id_topics');
		                                comboCity.setReadOnly(false);
		                                comboCity.setDisabled(false);
		                                comboCity.clearValue();
		                                comboCity.store.filter('instructives_type_id', idcombo);
                              	}
                           }
                        }
                  	},
                  	{
                       xtype       :'combo',
                       name        :'Topic',
                       fieldLabel  :'Topic',
                       width       :610,
                       hiddenName  :'iditg',
                       displayField:'topics',
                       allowBlank  :false,
                       valueField  :'iditg',
                       value:variable.topics,  //variable.instructive_type,//valueField:'iditg',
                      // disabled:true,
                       readOnly: true,
                       id:'id_topics',
                       store: new Ext.data.JsonStore({
                            autoLoad:true,
                            url:'php/grid_data.php?tipo=topics_group',
                            remoteSort: false,
                            idProperty: 'iditg',
                            root: 'results',
                            totalProperty: 'total',
                            fields: ['iditg','instructives_type_id','topics']
                        }),
                        triggerAction:'all',
                        mode:'local',
                        lastQuery:'',
                    },
                    {
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false,
                        readOnly: readonlys
                   },
                   {
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 300,
                        width:750,
                        value: variable.description,
                        allowBlank: false,
                        readOnly: readonlys
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Manage Instructive',
                    width: 800,
                    height: 550,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

/*Termina nueva ventana para Gestionar*/
function viewHtmlToEdit(instructive_id) //Ver ventana para Editar
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id,
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                if(appro != "1")
                { 
                var btnEdit= [{
                        text: '<b>Update Instructive </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }]
                    }
                    else
                    {
                        var btnEdit = [{
                        text: '<b>It was approved</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'title',
                        id: 'title',
                        width:690,
                        value: variable.title,
                        allowBlank: false
                    },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    value:variable.instructive_type_id,

                  },
                  {
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false
                  },
                  {
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.description,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
                });


                var w = new Ext.Window({
                    title: 'Update Instructive',
                    width: 800,
                    height: 550,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

    function viewHtmltoApprove(instructive_id)
    {

        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'instructive_view',
                instructive_id: instructive_id
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.istatus;
                var user_session = variable.user_session;
                if(appro != "1")
                { 
                        if(user_session==5 || user_session==20 || user_session==2342)
                        {
                            var btnEdit= [{
                                text: '<b>I Approve </b>',
                                cls: 'x-btn-text-icon',
                                icon: 'images/check.png',
                                formBind: true,
                                handler: function(b){
                                    var form = b.findParentByType('form');
                                    //form.getForm().fileUpload = true;
                                   // if (form.getForm().isValid()) {
                                        form.getForm().submit({
                                            url: 'php/grid_edit.php',
                                            waitTitle   :'Please wait!',
                                            waitMsg     :'Loading...',
                                            params: {
                                                tipo : 'instructive_approve',
                                                instructive_id: instructive_id
                                            },
                                            timeout: 100000,
                                            method :'POST',
                                            success: function(form, action) {
                                                w.close();
                                                var store1 = Ext.getCmp("gridpanel").getStore();
                                                store1.reload();
                                                Ext.Msg.alert('Success', action.result.msg);

                                            },
                                            failure: function(form, action) {
                                                Ext.Msg.alert('Error', action.result.msg);
                                            }
                                        });
                                    }
                                }]//Termina Btn Edit    
                        }
                        else
                        {
                            var btnEdit = [{
                                text: '<b>Not Authorized  </b>',
                                cls: 'x-btn-text-icon',
                                icon: 'images/access_denied.png'
                                }]
                        }   
                    }
                    else
                    {
                        var btnEdit = [{
                        text: '<b>It was approved </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/access_denied.png'
                        }]
                    }


                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Title',
                        name: 'title',
                        id: 'title',
                        width:690,
                        value: variable.title,
                        allowBlank: false
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.asignado,
                        allowBlank: false
                    },{
                        xtype: 'htmleditor',
                        id: 'description',
                        id: 'description',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.description,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [
                    btnEdit,'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Read Instructive',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

var storeUser= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
            });
///////////Cargas de data dinamica///////////////
 var storeMenu2= new Ext.data.JsonStore({
                    url:'php/grid_data_instructives.php?tipo=group_list',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idgroup','group_name']
                });

var storeTypes = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_type',
        fields: [
            {name: 'instructive_type_id', type: 'int'}
            ,'instructive_type'
        ]
    });

var storeGroups = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_group',
        fields: [
            {name: 'iditg', type: 'int'}
            ,{name: 'instructives_type_id', type: 'int'}
            ,'topics'
        ]
    });

    var store = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=privilegeinstructional',
        fields: [
            {name: 'instructive_id', type: 'int'}
            ,{name: 'creator_userid', type: 'int'}
            ,'creation_date'
            ,'cabecera'
            ,'title'
            ,'description'
            ,'usercreator'
            ,'bodyshort'
            ,'userasigned'
            ,'user_approved'
            ,'istatus'
            ,'approval_date'
        ]
    });

     var storeNolaboralDetail = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=instructive_details',
        fields: [
           {name: 'instructive_id', type: 'int'}
            ,{name: 'userid', type: 'int'}
            ,'rdate_read'
            ,'rstatus'
            ,'group'
            ,'usuario_instructivo'
            //,'instructivo'
            ,'instructive_id'

        ]
    });

 function mostrarDetalleNolaboral(instructive_id) 
    {
        Ext.getCmp('gridNolaboralDetail').getStore().setBaseParam('instructive_id', instructive_id);
        Ext.getCmp('gridNolaboralDetail').getStore().load({
            params:{
                instructive_id: instructive_id
                }
        });
    }
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo - Botones Add - Delete//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No records found",
        items:[{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            tooltip: 'Click to Add Instructions',
            handler: AddWindow // Abre Ventana Registrar Instructivo
        },{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/delete.gif',
            id: 'del_butt',
            tooltip: 'Click to Delete Instructions',
            handler: doDel // Abre Ventana para Borrar Instructivo
        },{
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Filter Group'
                    },{
                    xtype       :'combo',
                    id          :'typeins',
                    name        :'typeins',
                    hiddenName  :'typeins',
                    fieldLabel  :'type',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'type'],
                                    data : [['10', 'All Instructive'], ['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                }),
                    displayField:'type',
                    valueField  :'id',
                    value: '10',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                    listeners: {'select': logstatistic}

                  },/*{ //Muestro Select para filtrar **wape
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Created By'
                    },{
                        xtype:'combo',
                        name:'asic',
                        id:'asic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['userid', 'name'],
                            data : Ext.combos_selec.dataUsersProgramm
                        }),
                        valueField: 'userid',
                        displayField:'name',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        },
                        listeners: {'select': logstatistic}

                    } */]
    });

     var pagingBar2 = new Ext.PagingToolbar({
            store: storeNolaboralDetail,
            displayInfo: true,
            displayMsg: '{0} - {1} of {2} Registros',
            emptyMsg: 'No records found',
            pageSize: 100,
            items:[/*{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button2',
            tooltip: 'Click to Add Instructions',
            handler: AddWindow2
        }*/
        ]
    });


////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////

    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idtins;
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }

    function obtenerSeleccionados(){
            var selec = grid.selModel.getSelections();
            var i=0;
            var marcados='(';
            for(i=0; i<selec.length; i++){
                if(i>0) marcados+=',';
                marcados+=selec[i].json.idtins;
            }
            marcados+=')';
            if(i==0)marcados=false;
            return marcados;
        }


function logstatistic(){
//store.setBaseParam('iduser',Ext.getCmp("asic").getValue());
store.setBaseParam('idtype',Ext.getCmp("typeins").getValue());

        store.load({params:{start:0, limit:100}});
    }
    //Revisar bien, borrar
    function doDel(){
        Ext.MessageBox.confirm('Delete Instructives','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();
                    if(!obtenidos){
                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    //alert(obtenidos);//return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');

                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_del.php',
                        method: 'POST',
                        params: {
                            tipo : 'instructions',
                            idtmail: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }

        var ValorSeleccionado = 1;

//Inicio Add user

    function Adduser()
    {
        var formula = new Ext.FormPanel({
            url:'php/grid_data_instructives.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [
                   {
                        xtype:'combo',
                        store:storeMenu2,
                        fieldLabel:'Group',
                        id:'idgroup',
                        name:'idgroup',
                        hiddenName: 'idgroup',
                        valueField: 'idgroup',
                        displayField: 'group_name',
                        triggerAction: 'all',
                        emptyText:'Select a group',
                        allowBlank: false,
                        width:200
                        //listeners: {'select': showmenu}
                    },
                    {
	                    xtype:'textfield',
                        fieldLabel: 'Topic name',
                        name: 'topics',
                        id: 'topics',
                        width:450,
                        allowBlank: false
                    }
                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formula.getForm().isValid())
                    {
                        formula.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'groups_topics_add'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                //storeUsersapro.reload();
                                var storeww = Ext.getCmp("id_topics").getStore();
                                    storeww.reload();
                                var comboTopics = Ext.getCmp('id_topics');
                                    comboTopics.setDisabled(true);
                                    comboTopics.setValue('');

                                	//comboGrupos.setDisabled();	    


/*
 var comboCity = Ext.getCmp('combo-city');
                       //set and disable cities
                       comboCity.setDisabled(true);
                       comboCity.setValue('');
                       comboCity.store.removeAll();
                       //reload city store and enable city combobox
                       comboCity.store.reload({
                           params: { stateId: combo.getValue() }
                       });
                       comboCity.setDisabled(false);
*/                                        

                        		obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New topic',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 650,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formula
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formula.getForm().reset();
        });
        
    }
//Termina add user     
    function AddWindow()
    {
    var usersProgrammers = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.dataUsersProgramm,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }
        });
    var Topics = new Ext.data.ArrayStore({ //usersProgrammers para el Store
            fields: ['value', 'text'],
                    data : Ext.combos_selec.dataTopics,//Obtengo todos los usuarios con idusertype 7
            sortInfo: {
                field: 'value',
                direction: 'ASC'
            }
        });
///
//
// Data Stores 
//
var storeTypes = new Ext.data.JsonStore({
		//autoLoad: true,
        totalProperty: 'num',
        root: 'data',
        url: 'php/grid_data.php?tipo=topics_type',
        fields: [
            {name: 'instructive_type_id', type: 'int'}
            ,'instructive_type'
        ]
    });

var storeGroups = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=topics_group',
        fields: [
            {name: 'iditg', type: 'int'}
            ,{name: 'instructives_type_id', type: 'int'}
            ,'topics'
        ]
    });



    function getComboID(combo,record,index)
    {
        var ValorSeleccionado = Ext.getCmp('instructives_type_id').getValue(); 
        if(ValorSeleccionado == 6)
        {
            Ext.getCmp('users_single').show();
        }
        else
        {
            Ext.getCmp('users_single').hide();
        }
        
         Ext.getCmp('topics').show();  
           Ext.getCmp('topics').enable();
          // storeMenu2.load({params:{instructives_type_id:ValorSeleccionado}});
           //alert(ValorSeleccionado);
           
    }//TERMINA GETCOMBOID

var btnEdit= [{
                        text: '<b>Save </b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                 		width: 100,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructive_edit',
                                        instructive_id: instructive_id
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    }];
        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                xtype:'textfield',
                fieldLabel: 'Title',
                name: 'title',
                id: 'title',
                width:640,
                allowBlank: false
            },{
                    xtype       :'combo',
                    id          :'instructives_type_id',
                    name        :'instructives_type_id',
                    hiddenName  :'instructives_type_id',
                    fieldLabel  :'Group',
                    allowBlank  :false,
                    width       :200,
                    store       :new Ext.data.SimpleStore({
                                    fields: ['id', 'instructives_type_id'],
                                    data : [['1', 'Instructive - General'], ['2', 'Instructive - Download'], ['3', 'Instructive - Customer Service'], ['4', 'Instructive - Administrator'], ['5', 'Instructive - Programmer'],['6', 'Instructive - Single'],['7', 'Instructive - Mobile']]
                                }),
                    displayField:'instructives_type_id',
                    valueField  :'id',
                    mode        :'local',
                    triggerAction:'all',
                    emptyText   :'Select..',
                    selectOnFocus:true,
                   // listeners: {'select': getComboID}
                    listeners: {
                           select: {
                               fn:function(combo, value) {
                        var idcombo = Ext.getCmp('instructives_type_id').getValue();
                        if(idcombo == 6)
                        {
                            Ext.getCmp('users_single').show();
                        }
                        else
                        {
                            Ext.getCmp('users_single').hide();
                        }
                        /*  
                       var comboCity = Ext.getCmp('combo-city');
                       //set and disable cities
                       comboCity.setDisabled(true);
                       comboCity.setValue('');
                       comboCity.store.removeAll();
                       //reload city store and enable city combobox
                       comboCity.store.reload({
                           params: { stateId: combo.getValue() }
                       });
                       comboCity.setDisabled(false);
                  }
               }
                        */

                                var comboCity = Ext.getCmp('id_topics');
                                comboCity.setDisabled(false);
                                comboCity.clearValue();
                                comboCity.store.filter('instructives_type_id', idcombo);

                                   
                              }
                           }
                    	}
                  },
                  {
                    xtype: 'compositefield',
	                fieldLabel: 'Topic',
	                msgTarget : 'side',
	                anchor    : '-20',
	                defaults: {
	                    flex: 1
	                },
	                items: [
	                    {
	                	   xtype       :'combo',
	                       name        :'Topic',
	                       fieldLabel  :'Topic',
	                       width       :580,
	                       hiddenName  :'iditg',
	                       displayField:'topics',
	                        allowBlank  :false,
	                       valueField:'iditg',
	                       disabled:true,
	                       id:'id_topics',
	                       store: new Ext.data.JsonStore({
	                            autoLoad:true,
	                            url:'php/grid_data.php?tipo=topics_group',
	                            remoteSort: false,
	                            idProperty: 'id',
	                            root: 'results',
	                            totalProperty: 'total',
	                            fields: ['iditg','instructives_type_id','topics']
	                        }),
	                        triggerAction:'all',
	                        mode:'local',
	                       lastQuery:''
	                    },
	                    {		xtype :'button',
					            text: 'Add',
					              cls: 'x-btn-text-icon',
		                        icon: 'images/add.png',
		                 		width: 40,
					           	handler: Adduser // Abre Ventana Registrar Instructivo
			            }
	                   
						

	                ]
            	},
            	{
            	text: 'View',
					            handler: function(){
				                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
			                        form.getForm("asignadon").getValues(true));

					            }
				},
                {
                 xtype: 'itemselector',
                    id   :'users_single',
                    name: 'users_single',
                    hiddenName  : 'users_single',
                    fieldLabel: 'Select users',
                    hidden: true,

                    imagePath: 'includes/ext/examples/ux/images/',
                    multiselects: [{
                        width: 250,
                        height: 200,
                        store: usersProgrammers,
                        displayField: 'text',
                        valueField: 'value'
                    },{
                        width: 250,
                        height: 200,
                        store: [['','']],
                        displayField: 'text',
                        valueField: 'value',
                        tbar:[{
                            text: 'clear',
                            handler:function(){
                                form.getForm().findField().reset();
                            }
                        }]
                    }] 
            },
            
            {
                xtype: 'htmleditor',
                id: 'description',
                id: 'description',
                hideLabel: true,
                height: 250,
                width:700,
                allowBlank: false
            }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Create Instructive</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php',
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'instructive_add'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                store.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            }/*,{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }*/]
        });


        var w = new Ext.Window({
            title: 'New Instructive',
            width: 750,
            height: 650,
            layout: 'fit',
            //resizable: true,
            modal: true,
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();

        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()

     function AddWindow2()
    {

var ds = new Ext.data.ArrayStore({
        fields: ['value', 'text'],
                data : Ext.combos_selec.dataUsersProgramm,
        sortInfo: {
            field: 'value',
            direction: 'ASC'
        }

    });
    var obtenidos=obtenerSeleccionados();
        if(!obtenidos){
            Ext.MessageBox.show({
                title: 'Warning',
                msg: 'You must select a row, by clicking on it, for the delete to work.',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            obtenerTotal();
            return;
        }
        //alert(obtenidos);//return;
        obtenidos=obtenidos.replace('(','');
        obtenidos=obtenidos.replace(')','');

        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                            xtype:'hidden',
                            id: 'idtinsa',
                            name: 'idtinsa',
                            value: obtenidos,
                            hiddenName: 'idtinsa',
                        },
{
            xtype: 'itemselector',
            name: 'asignadona',
            hiddenName  : 'asignadoa',
            fieldLabel: 'Assigned to',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: ds,
                displayField: 'text',
                valueField: 'value'
            },{
                width: 250,
                height: 300,
                store: [['','']],
                displayField: 'text',
                valueField: 'value',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField().reset();
                    }
                }]
            }]
        }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Add User</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php',
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'userinstructions'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                storeNolaboralDetail.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            }]
        });


        var w = new Ext.Window({
            title: 'New User Instructive',
            width: 800,
            height: 400,
            layout: 'fit',
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();
        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()


    

//Termina add user 
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
    /*Nuevo Render to Manage*/
     function renderViewtoAddtopic(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="viewHtmlToAddTopic({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
    }

    function renderViewtoManage(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="viewHtmlToManage({0})" ><img src="images/gestionar.png" border="0" width="16" title="Manage Instructive" alt="Manage Instructive"></a>',record.data.instructive_id);
    }


    /*Termina nuevo render to manage*/

    function renderViewtoEdit(val, p, record)
    {
        if(record.data.istatus !='N')
        {
            return '';
        }
        else
        {
            return String.format('<a href="javascript:void(0)" onClick="viewHtmlToEdit({0})" ><img src="images/editar.png" border="0" title="Edit Instructive" alt="Edit Instructive"></a>',record.data.instructive_id);

        }
    }

    function renderViewtoApprove(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="viewHtmltoApprove({0})" ><img src="images/check.png" border="0" title="Approve Instructive" alt="Approve Instructive"></a>',record.data.instructive_id);
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});

    var myView2 = new Ext.grid.GridView();
    myView2.getRowClass = function(record, index, rowParams, store) 
    {
        if(record.data['istatus'] == 'N')return 'rowRed'; //Valida el estado del registro
    };
    var grid = new Ext.grid.EditorGridPanel({
        view: myView2,
        title:wherepage,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            //,mySelectionModel
            ,{header: '', width: 40, dataIndex: 'view', renderer: check}
            //,{header: 'ID', width: 30, align: 'left', sortable: true, dataIndex: 'instructive_id',renderer:function (val)
            //{
            //return '<a href="javascript:void(0)" class="itemusers" title="Details Instructive. Click to see the information." onclick="mostrarDetalleNolaboral('+val+')"><img src="images/view.gif" border="0" title="View Details ID: '+val+' " alt="View Details"></a>';
            //}
            ,{header: 'ID', width: 30, sortable: true, align: 'left', dataIndex: 'instructive_id'}
            ,{header: 'Group / Topic', width: 140, sortable: true, align: 'left', dataIndex: 'cabecera'}
            ,{header: 'Title', width: 160, sortable: true, align: 'left', dataIndex: 'title'}
            ,{header: 'Body', width: 320, sortable: true, align: 'left', dataIndex: 'description'}
            ,{header: 'Created By', width: 130, sortable: true, align: 'left', dataIndex: 'usercreator'}
            ,{header: 'Creation Date', width: 120, sortable: true, align: 'left', dataIndex: 'creation_date'}
            //,{header: 'Asigned', width: 150, align: 'left', sortable: true, dataIndex: 'userasigned'}
            ,{header: 'Approved', width: 130, align: 'left', sortable: true, dataIndex: 'user_approved'}
            ,{header: 'Date Approved', width: 130, align: 'left', sortable: true, dataIndex: 'approval_date'}
            ,{header: 'Manage', width: 50, sortable: true, align: 'center', dataIndex: 'instructive_id', renderer: renderViewtoManage}
        ],
        clicksToEdit:2,
        height:Ext.getBody().getViewSize().height-400,
        sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
        //sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });


    var viewDetails = new Ext.grid.GridView();
    viewDetails.getRowClass = function(record, index, rowParams, storeNolaboralDetail) 
    {
        if(record.data['rdate_read'] == 'Not read') return 'rowRed'; //Valida el estado del registro
    };
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    
    var gridNolaboralDetail = new Ext.grid.EditorGridPanel({
    view:viewDetails,
    store: storeNolaboralDetail,
    id:'gridNolaboralDetail',
    iconCls: 'icon-grid',
    border:false,
    viewConfig: {
        forceFit:true
    },
    tbar: pagingBar2,
    columns: [
        new Ext.grid.RowNumberer(), //grid2
        mySelectionModel,
        {header: 'ID', width: 40, sortable: true, align: 'left', dataIndex: 'instructive_id'},
        //{header: 'ID User', width: 30, sortable: true, align: 'left', dataIndex: 'userid'},
        {header: 'User', width: 250, sortable: true, align: 'left', dataIndex: 'usuario_instructivo'},
        {header: 'Group', width: 200, sortable: true, align: 'left', dataIndex: 'group'},
        {header: 'Date Read', width: 150, sortable: true, align: 'left', dataIndex: 'rdate_read'},
        {header: 'Read', width: 150, sortable: true, align: 'left', dataIndex: 'rstatus'}
       // {header: 'Eliminar', width: 150, sortable: true, align: 'left', dataIndex: 'rstatus'}
    ,{
        xtype: 'actioncolumn',
        width: 30,
        items: [
            {
                icon    : 'images/delete-icon.png',                // Use a URL in the icon config
                tooltip : 'Exclude User',
                scope   :   this,
                handler : function(grid, rowIndex, columnIndex, e) {
                    var rec = storeNolaboralDetail.getAt(rowIndex);
                   Ext.MessageBox.alert('User Exclude','This user has been excluded');
                    var instructive_id = rec.get('instructive_id');
                    var userid = rec.get('userid');
                   /*alert('User delete');
                    var rec = grid.getStore( ).getAt(rowIndex);
                    console.debug(rec);
                    */

// Revisar este comportamiento -wape
                    Ext.Ajax.request({
                        waitMsg: 'Loading...',
                        url: 'php/grid_add.php',
                        scope:this,
                        method: 'POST',
                        params: {
                            instructive_id  : instructive_id,
                            userid : userid,
                            tipo  :'instructive_exclude_user'
                        },
                        success:function (){
                            grid.getStore( ).load();
                        }
                    });

                  //  console.debug(rec);
                }
            }
        ]
        }
    ],
    height:Ext.getBody().getViewSize().height-400,
    //autoHeight:true,
    loadMask:true
});

/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },{
            region:'center',
            autoHeight: true,
            items:[{
                    xtype   : 'panel',
                   // width   : 500,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        grid
                    ]
                },{
                    xtype   : 'panel',
                    //flex    : 1,
                    height:Ext.getBody().getViewSize().height-400,
                    items   : [
                        gridNolaboralDetail
                    ]
                }]
        }]
    });


//////////////FIN VIEWPORT////////////////////////////////


//          ***     Inicializar Grid      ***              //
    store.load({params:{start:0, limit:100}});
//          ***     Terminar Inicializar Grid   ***       //

});
