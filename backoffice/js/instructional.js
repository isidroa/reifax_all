function previewHtmlEmail(idtmail)
    {
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'previewinstructions',
                idtmail: idtmail
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Titulo',
                        name: 'titulo',
                        id: 'titulo',
                        width:690,
                        value: variable.titulo,
                        allowBlank: false
                    },
{
            xtype: 'itemselector',
            name: 'asignadon',
            hiddenName  : 'asignado',
            fieldLabel: 'Assigned to',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: new Ext.data.SimpleStore({
                fields: ['value', 'text'],
                data : Ext.combos_selec.dataUsersProgramm,

            }),
                displayField: 'text',
                valueField: 'value',
                value: variable.todo
            },{
                width: 250,
                height: 300,
                 store: [["",""]],
                 valueField: 'value',
                 displayField: 'text',

                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField('asignadon').reset();
                    }
                }]
            }]
        }
/*{
            xtype: 'multiselect',
            fieldLabel: 'Assigned to',
            name: 'asignadon',
            hiddenName  : 'asignado',
            width: 250,
            height: 300,
            allowBlank:false,
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            value: variable.to
        },{
            xtype       : 'combo',
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            fieldLabel  : 'Assigned to',
            name        : 'asignadon',
            hiddenName  : 'asignado',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            },
            value: variable.to
    }*/,
                    {
                        xtype: 'htmleditor',
                        id: 'descripcion',
                        id: 'descripcion',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.descripcion,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: [{
                        text: '<b>Update Instructions</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php?typeHelp'+typeHelp,
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'instructions',
                                        idtmail: idtmail
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    },'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    },{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }]
                });


                var w = new Ext.Window({
                    title: 'Update Instructions',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }

    function previewHtmlEmail2(idtmail)
    {

        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'previewinstructions',
                idtmail: idtmail
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
                var appro= variable.lei;
                if(appro != "1"){
                var prueba= [{
                        text: '<b>I Read</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/check.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                           // if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php?typeHelp='+typeHelp,
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'read',
                                        idtmail: idtmail
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });

                        }
                    }];}

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{
                        xtype:'textfield',
                        fieldLabel: 'Titulo',
                        name: 'titulo',
                        id: 'titulo',
                        width:690,
                        value: variable.titulo,
                        allowBlank: false
                    }/*,{
                        xtype:'textfield',
                        fieldLabel: 'Assigned to',
                        name: 'asignadon',
                        id: 'asignadon',
                        width:690,
                        value: variable.to,
                        allowBlank: false
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Readed by',
                        name: 'readed',
                        id: 'readed',
                        width:690,
                        value: variable.too,
                        //allowBlank: false
                    }{
            xtype: 'multiselect',
            fieldLabel: 'Assigned to',
            name: 'asignadon',
            hiddenName  : 'asignado',
            width: 250,
            height: 300,
            allowBlank:false,
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            value: variable.to
        }{
            xtype       : 'combo',
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            fieldLabel  : 'Assigned to',
            name        : 'asignadon',
            hiddenName  : 'asignado',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            },
            value: variable.to
    }*/,{
                        xtype: 'htmleditor',
                        id: 'descripcion',
                        id: 'descripcion',
                        hideLabel: true,
                        height: 350,
                        width:750,
                        value: variable.descripcion,
                        allowBlank: false
                    }],
                    buttonAlign: 'center',
                    buttons: prueba
                });


                var w = new Ext.Window({
                    title: 'Read instructions',
                    width: 800,
                    height: 620,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:5px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
    var store = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data.php?tipo=instructional&tip='+typeHelp,
        fields: [
            {name: 'idtins', type: 'int'}
            ,{name: 'requested', type: 'int'}
            ,'fechainsert'
            ,'titulo'
            ,'descripcion'
            ,'usercreator'
            ,'bodyshort'
            ,'userasigned'
            ,'dateread'
        ]
    });
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[/*{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            tooltip: 'Click to Add Instructions',
            handler: AddWindow
        },{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/delete.gif',
            id: 'del_butt',
            tooltip: 'Click to Delete Instructions',
            handler: doDel
        },*/{
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Created By'
                    },{
                        xtype:'combo',
                        name:'asic',
                        id:'asic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['userid', 'name'],
                            data : Ext.combos_selec.dataUsersProgramm
                        }),
                        valueField: 'userid',
                        displayField:'name',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        },
                        value: '0',
                        listeners: {'select': logstatistic}

                    }]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////

    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idtins;
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }
function logstatistic(){
store.setBaseParam('iduser',Ext.getCmp("asic").getValue());

        store.load({params:{start:0, limit:100}});
    }
    function doDel(){
        Ext.MessageBox.confirm('Delete Instructions','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();
                    if(!obtenidos){
                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    //alert(obtenidos);//return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');

                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_del.php',
                        method: 'POST',
                        params: {
                            tipo : 'instructions',
                            idtmail: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }

    function AddWindow()
    {

var ds = new Ext.data.ArrayStore({
        fields: ['value', 'text'],
                data : Ext.combos_selec.dataUsersProgramm,
        sortInfo: {
            field: 'value',
            direction: 'ASC'
        }

    });

        var form = new Ext.form.FormPanel({
            baseCls: 'x-plain',
            labelWidth: 55,
            items: [{
                xtype:'textfield',
                fieldLabel: 'Titulo',
                name: 'titulo',
                id: 'titulo',
                width:690,
                allowBlank: false
            },
{
            xtype: 'itemselector',
            name: 'asignadon',
            hiddenName  : 'asignado',
            fieldLabel: 'Assigned to',
            imagePath: 'includes/ext/examples/ux/images/',
            multiselects: [{
                width: 250,
                height: 300,
                store: ds,
                displayField: 'text',
                valueField: 'value'
            },{
                width: 250,
                height: 300,
                store: [['','']],
                displayField: 'text',
                valueField: 'value',
                tbar:[{
                    text: 'clear',
                    handler:function(){
                        form.getForm().findField().reset();
                    }
                }]
            }]
        },
            /*
            {
            xtype: 'multiselect',
            fieldLabel: 'Assigned to',
            name: 'asignadon',
            hiddenName  : 'asignado',
            width: 250,
            height: 300,
            allowBlank:false,
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name'
        },
{
            xtype       : 'combo',
            MultiSelect: true,
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            fieldLabel  : 'Assigned to',
            name        : 'asignadon',
            hiddenName  : 'asignado',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersProgramm
            }),
            valueField: 'userid',
            displayField:'name',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            }
    },*/

            {
                xtype: 'htmleditor',
                id: 'descripcion',
                id: 'descripcion',
                hideLabel: true,
                height: 350,
                width:750,
                allowBlank: false
            }],
            buttonAlign: 'center',
            buttons: [{
                text: '<b>Create Instructions</b>',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
                formBind: true,
                handler: function(b){
                    var form = b.findParentByType('form');
                    //form.getForm().fileUpload = true;
                    if (form.getForm().isValid()) {
                        form.getForm().submit({
                            url: 'php/grid_add.php?tip='+typeHelp,
                            waitTitle   :'Please wait!',
                            waitMsg     :'Loading...',
                            params: {
                                tipo : 'instructions'
                            },
                            timeout: 100000,
                            method :'POST',
                            success: function(form, action) {
                                w.close();
                                store.reload();
                                Ext.Msg.alert('Success', action.result.msg);

                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Error', action.result.msg);
                            }
                        });
                    }
                }
            },'->',{
                text: 'Close',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler: function(b){
                    w.close();
                }
            },{
            text: 'View',
            handler: function(){

                    Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
                        form.getForm("asignadon").getValues(true));

            }
        }]
        });


        var w = new Ext.Window({
            title: 'New Instructions',
            width: 800,
            height: 620,
            layout: 'fit',
            plain: true,
            bodyStyle: 'padding:5px;',
            items: form
        });
        w.show();
        w.addListener("beforeshow",function(w){
            form.getForm().reset();
        });
    }//fin function doAdd()

//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
    function renderpreviewemail(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlEmail({0})" ><img src="images/editar.png" border="0" title="Preview email" alt="Preview email"></a>',record.data.idtins);
    }

    function renderpreviewemail2(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlEmail2({0})" ><img src="images/historico.png" border="0" title="Preview email" alt="Preview email"></a>',record.data.idtins);
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    var myView2 = new Ext.grid.GridView();
    myView2.getRowClass = function(record, index, rowParams, store) {
    if(record.data['dateread'] == 'Unread')return 'rowRed';
    };
    var grid = new Ext.grid.EditorGridPanel({
        title:wherepage,
        view: myView2,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            ,{header: 'Read', width: 40, sortable: true, align: 'center', dataIndex: 'idtins', renderer: renderpreviewemail2}
            ,{header: 'ID', width: 40, align: 'left', sortable: true, dataIndex: 'idtins'}
            ,{header: 'Titulo', width: 160, sortable: true, align: 'left', dataIndex: 'titulo'}
            ,{header: 'Body', width: 350, sortable: true, align: 'left', dataIndex: 'bodyshort'}
            ,{header: 'Created By', width: 150, sortable: true, align: 'left', dataIndex: 'usercreator'}
            ,{header: 'Creation Date', width: 150, sortable: true, align: 'left', dataIndex: 'fechainsert'}
           // ,{header: 'Asigned', width: 150, align: 'left', sortable: true, dataIndex: 'userasigned'}
            ,{header: 'Readed', width: 150, align: 'left', sortable: true, dataIndex: 'dateread'}
           // ,{header: ' ', width: 40, sortable: true, align: 'center', dataIndex: 'idtins', renderer: renderpreviewemail}
        ],
        clicksToEdit:2,
        height:470,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },{
            region:'center',
            autoHeight: true,
            items: grid
        }]
    });
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
    store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
