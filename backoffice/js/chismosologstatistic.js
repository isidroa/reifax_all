Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica///////////////
	var storelogstatistic = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'data',
		url: 'php/funcionesControlConex.php?opcion=chismosologstatistic',
		fields: [
			,'usuario'
			,'reason'
			,'type'
			,'value'
			,'datecreate'
			,'creator'


		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
	var pagingBarLogStart = new Ext.PagingToolbar({
        pageSize: 100,
        store: storelogstatistic,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			width:200,
			fieldLabel:'logstatistic',
			name:'cbUser',
			id:'cbUser',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['userid', 'name'],
				data : Ext.combos_selec.logstatistic
			}),
			mode: 'local',
			valueField: 'userid',
			displayField:'name',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false,
			value: '*'
		},{
			width:200,
			fieldLabel:'logstatistic',
			name:'cbTIPO',
			id:'cbTIPO',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['value', 'texto'],
				data : [['*', 'ALL'],['ADD', 'ADD'],['HOLIDAY', 'HOLIDAY'],['LEAVE DAY', 'LEAVE DAY'],
						['SUBTRACTION', 'SUBTRACTION'],['VACATION', 'VACATION']]
			}),
			mode: 'local',
			valueField: 'value',
			displayField:'texto',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false,
			value: '*'
		},{
				xtype: 'datefield',
				width: 100,
				name: 'dateInit',
				id: 'dateInit',
				format: 'Y-m-d',
				editable: false,
				//value: new Date()
			},{
				xtype: 'datefield',
				width: 100,
				name: 'dateEnd',
				id: 'dateEnd',
				format: 'Y-m-d',
				editable: false,
				//value: new Date()
			},{
				xtype: 'button',
				width:80,
				id:'searchc',
				pressed: true,
				enableToggle: true,
				name:'searchc',
				text:'&nbsp;<b>Search</b>',
				handler: logstatistic
			}
			]
    });
////////////////FIN barra de pagineo//////////////////////
	//////////////////Manejo de Eventos//////////////////////


	function obtenerSeleccionados(){
		var selec = gridlogstart.selModel.getSelections();
		var i=0;
		var marcados='(';
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idworkshop;
			//alert(marcados)
		}
		marcados+=')';
		if(i==0)marcados=false;
		return marcados;
	}


	function copyworkshop(){
		var com;
		var j=0;
		var obtenidos=obtenerSeleccionados();
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');

		//alert(obtenidos);return;
		if(obtenidos.search(",")>0)
		{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it');
			return;
		}
		//Ext.MessageBox.alert('Warning','SIIIII VA by clicking on it');			return;
		Ext.Ajax.request({
			waitMsg: 'Saving changes...',
			url: 'php/grid_add.php',
			method: 'POST',
			params: {
				idw: obtenidos,
				tipo: 'copyworkshop'

			},

			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
				storelogstatistic.rejectChanges();
			},

			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);

				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);

				storelogstatistic.commitChanges();
				storelogstatistic.reload();
			}
		});

	}

	function AddWindow()
	{
		var formul = new Ext.FormPanel({
			url:'php/grid_add.php',
			frame:true,
			layout: 'form',
			border:false,
			items: [{
				xtype: 'textfield',
				id: 'title',
				name: 'title',
				fieldLabel: '<span style="color:#F00">*</span> Title',
				allowBlank:false,
				width: 250
			},{
				xtype:'textfield',
				id: 'speaker',
				name: 'speaker',
				fieldLabel: '<span style="color:#F00">*</span> Speaker',
				allowBlank:false,
				width: 250
			},{
				xtype:'numberfield',
				id: 'price',
				name: 'price',
				fieldLabel: '<span style="color:#F00">*</span> Price',
				allowBlank:false,
				allowDecimals : true,
				width: 100
			},{
				xtype: 'datefield',
				id: 'when',
				name: 'when',
				fieldLabel: '<span style="color:#F00">*</span> When',
				allowBlank:false,
				width: 250,
				//format: 'l, F dS Y',
				format: 'Y-m-d',
				editable: false,
				value: new Date()
			},{
               	xtype:'timefield',
				fieldLabel: '<span style="color:#F00">*</span> Hour',
                id: 'hour',
                name: 'hour',
				width: 100,
				allowBlank:false,
				editable :false,
				increment: 15,
				format: 'H:i',
                value: '10:00',
                minValue: '8:00',
                maxValue: '18:00'
            },{
				xtype:'textfield',
				id: 'whennote',
				name: 'whennote',
				fieldLabel: '<span style="color:#F00">&nbsp;&nbsp;</span> When Note',
				allowBlank:true,
				width: 250
			},{
				xtype:'textarea',
				id: 'linkmap',
				name: 'linkmap',
				fieldLabel: '<span style="color:#F00">*</span> Link Map',
				allowBlank:false,
				width: 250,
				height: 60
			},{
				xtype:'textarea',
				id: 'address',
				name: 'address',
				fieldLabel: '<span style="color:#F00">*</span> Address',
				allowBlank:false,
				width: 250,
				height: 60
			},{
				xtype:'textfield',
				id: 'city',
				name: 'city',
				fieldLabel: '<span style="color:#F00">*</span> City',
				allowBlank:false,
				width: 250
			},{
				width: 150,
				fieldLabel:'<span style="color:#F00">*</span> State',
				name:'cbState',
				id:'cbState',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'text'],
					data : Ext.combos_selec.storeState
				}),
				editable :false,
				mode: 'local',
				valueField: 'id',
				displayField: 'text',
				hiddenName: 'ocState',
				triggerAction: 'all',
				selectOnFocus: true,
				allowBlank: false,
				value: 'FL'
			},{
				xtype:'numberfield',
				id: 'zipcode',
				name: 'zipcode',
				fieldLabel: '<span style="color:#F00">*</span> Zipcode',
				allowBlank:false,
				maxLength: 5, // for validation
				width: 100
			},{
				xtype:'textfield',
				id: 'phone',
				name: 'phone',
				fieldLabel: '<span style="color:#F00">*</span> Phone',
				allowBlank:false,
				width: 250
			},{
				width: 150,
				fieldLabel:'<span style="color:#F00">*</span> Active',
				name:'cbActive',
				id:'cbActive',
				xtype:'combo',
				store: new Ext.data.SimpleStore({
					fields: ['id', 'text'],
					data : [
						['1', 'Active'],
						['0', 'No Active']
					]
				}),
				editable :false,
				mode: 'local',
				valueField: 'id',
				displayField: 'text',
				hiddenName: 'ocActive',
				triggerAction: 'all',
				selectOnFocus: true,
				allowBlank: false,
				value: '0',
				listeners: {
					'select': searchActive

				}
			}],
			buttons: [{
				text: 'Cancel',
				cls: 'x-btn-text-icon',
				icon: 'images/cross.gif',
				handler  : function(){
					wind.close();
			   }
			},{
				text: 'Save',
				cls: 'x-btn-text-icon',
				icon: 'images/disk.png',
                formBind: true,
				handler  : function(){
					if(formul.getForm().isValid())
					{
						formul.getForm().submit({
							method: 'POST',
							params: {
								tipo : 'manageworkshop'
							},
							waitTitle: 'Please wait..',
	                        waitMsg: 'Sending data...',
							success: function(form, action) {
								storelogstatistic.reload();
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Success", obj.msg);
								wind.close();
							},
							failure: function(form, action) {
                                obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.msg);
							}
						});
					}
				}
			}]
		});
		var wind = new Ext.Window({
				title: 'New Workshop',
				iconCls: 'x-icon-templates',
				layout      : 'fit',
				width       : 430,
				height      : 500,
				resizable   : false,
				modal	 	: true,
				plain       : true,
				items		: formul
			});

		wind.show();
		wind.addListener("beforeshow",function(wind){
				formul.getForm().reset();
		});
	}//fin function doAdd()


	function logstatistic(){
		storelogstatistic.setBaseParam('userid',Ext.getCmp("cbUser").getValue());
		storelogstatistic.setBaseParam('dateInit',Ext.getCmp("dateInit").getValue());
		storelogstatistic.setBaseParam('dateEnd',Ext.getCmp("dateEnd").getValue());
		storelogstatistic.setBaseParam('cbTIPO',Ext.getCmp("cbTIPO").getValue());
		storelogstatistic.load({params:{start:0, limit:100}});
	}
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
	function viewnotes(val, p, record){
		if(record.data.edit=='N') return '';

		return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editWorkshop('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }
	function viewnstatus(val, p, record){

		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
	function viewlink(val, p, record){

		return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
	}
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
	//var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var gridlogstart = new Ext.grid.EditorGridPanel({
		title:'Log Statistic',
		id:"gridpanel",
		store: storelogstatistic,
		iconCls: 'icon-grid',
		columns: [
			new Ext.grid.RowNumberer()
			//,mySelectionModel
			,{header: 'UserID', width: 150, sortable: true, align: 'center', dataIndex: 'usuario'}
			,{header: 'Reason', width: 250, sortable: true, align: 'left', dataIndex: 'reason'}
			,{header: 'Type', width: 150, align: 'left', sortable: true, dataIndex: 'type'}
			,{header: 'Value', width: 200, align: 'left', sortable: true, dataIndex: 'value'}
			,{header: 'Create date', width: 125, align: 'left', sortable: true, dataIndex: 'datecreate'}
			,{header: 'Creator', width: 150, sortable: true, align: 'left', dataIndex: 'creator'}

			//,{header: 'Hour', width: 80, sortable: true, align: 'left', dataIndex: 'hour'}
			//,{header: 'Price', width: 70, sortable: true, align: 'right', dataIndex: 'pricews', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			//,{header: 'Active', width: 50, sortable: true, align: 'center', dataIndex: 'status', renderer: viewnstatus}
		],
		clicksToEdit:2,
		height:470,
		//sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBarLogStart
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: gridlogstart
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	storelogstatistic.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
