function historico(obtenidos){
	
	var ruta = 'php/obtenerHist.php?u='+obtenidos;
			
	Ext.Ajax.request({
		url: ruta+'&caso=busca',
		success: function(response){
			var variable = Ext.decode(response.responseText);
			
			var reader = new Ext.data.JsonReader({
			successProperty	: 'success',
			messageProperty	: 'message',
			idProperty	: 'id',
			root		: 'data'
			},[
				{name: "fechgrup0", type: 'string'},
				{name: "fechatrans", type: "string"},
				{name: "titulo", type: "string"},
				{name: "userid", type: "string"},
				{name: "name", type: "string"},
				{name: "surname", type: "string"},
				{name: "email", type: "string"},
				{name: "phone", type: "string"},
				{name: "paydate", type: "string"},
				{name: "status", type: "string"},
				{name: "usertype", type: "string"},
				{name: "priceprod", type: "float"},						     
				{name: "monto", type: "float"},
				{name: "transactionid", type: "string"},
				{name: "nrodocumento", type: "string"},
				{name: "cobradordesc", type: "string"},
				{name: "notas", type: "string"}				
			]);
			
			this.gstorefullhist =new Ext.data.GroupingStore({
				url		: ruta+'&caso=arreglo',
				reader		: reader,
				sortInfo        : {field:'fechgrup0', direction:'DESC'},
				groupField	: 'fechgrup0'
			});			
			
			this.gstorefullhist.load();			
			
			var elemento = new Ext.ux.grid.GroupSummary();			
			var textField = new Ext.form.TextField();
			
			this.gridfullhist = new Ext.grid.EditorGridPanel({
				store	: this.gstorefullhist,
				plugins	: elemento,
				title:"<H1 align='center'>USER HISTORY: "+variable.nombre+" "+variable.apellido+" ("+obtenidos+")<H1>",
				columns : [new Ext.grid.RowNumberer(),
			{
			header:"Month",
			dataIndex: "fechgrup0",
			hideable: false,
			sortable : true,
			groupable: true,
			width: 100
			},
			{
			header		: "Date",
			width		: 125,
			dataIndex	: "fechatrans",
			sortable	: true,
			groupable	: true,
			summaryType	: "count",
			summaryRenderer: function(v, params){
					return ((v === 0 || v > 1) ? '(' + v +' Items)' : '(1 Item)');
				},
			editor:textField				
			},{
			  header	: "Paypal Trans",
			  dataIndex	: "transactionid",
			  width		: 175,
			  editor: textField
			}
			,{
			header		: "Transaction",
			dataIndex	: "titulo",
			width		: 295,
			sortable	: true,
			groupable	: true,
			editor:textField
			},{
			header		: "UserID",
			dataIndex	: "userid",
			sortable	: true,
			groupable	: true,
			editor:textField			
			},{
			header		: "Name",
			dataIndex	: "name",
			width		: 100,
			sortable	: true,
			groupable	: true,
			editor:textField			
			},{
			header		: "Surname",
			dataIndex	: "surname",
			width		: 100,
			sortable	: true,
			groupable	: true,
			editor:textField			
			} /*,
					  {
			header		: "Email",
			dataIndex	: "email",
			width		: 215,
			groupable	: true,
			sortable	: true
			},
					  {
			header		: "Phone",
			dataIndex	: "phone",
			width		: 125,
			groupable	: true,
			sortable	: true
			}*/ ,
					  {
			header		: "Document Number",
			dataIndex	: "nrodocumento",
			groupable	: true,
			sortable	: true,
			width		: 145,
			editor:textField			
			},{
			header		: "cobradordesc",
			dataIndex	: "cobradordesc",
			groupable	: true,
			sortable	: true,
			width		: 145,			
			editor:textField			
			},
					  {
			header		: "Pay Amount",
			width		: 115,
			dataIndex	: "priceprod",
			groupable	: true,
			sortable	: true,
			align: 'right',
			renderer: function(v, params, record){
					if(record.data.priceprod!='')
						return Ext.util.Format.usMoney(record.data.priceprod);
				},
			editor:textField			
			},{
			header		: "Balance",
			width		: 115,
			sortable	: true,
			align: 'right',
			groupable	: false,
			renderer: function(v, params, record){
					return Ext.util.Format.usMoney(record.data.monto);
				},
			editor:textField				
			},
			 {
			header		: "Notes",
			dataIndex	: "notas",
			groupable	: true,
			sortable	: true,
			align: 'center',
			renderer : comment
			}
		      ],
				
				view : new Ext.grid.GroupingView({
					//startCollapsed : true,
					forceFit:true,
					showGroupName: false,
					enableNoGroups:false,
					enableGroupingMenu:false,
					hideGroupedColumn: true
				}),
				sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
				clicksToEdit: 1,
				collapsible: true,
				animCollapse: false,
				trackMouseOver: false,
			});
			
	function comment(val, p, record){
		var note = '';
		if (val!='' && val!=null){
			note = val.substring(0,0);
			return String.format('{0}...<img src="images/notes.png" border="0" ext:qtip="{1}">',note,val);
		}	
	}

			var win = new Ext.Window({
				title	: "Users - Historico Full",
				layout	: "fit",
				width	: 1236,
				height	: 480,
				modal   : true, 
				items	: this.gridfullhist
			});
			win.show();

		}
	});	
}
