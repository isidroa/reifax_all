Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	//var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
	function addShortSaleUsers(userid,idProcessor){
		new Ext.Window({
			title	:	'Users to Assign',
			id		:	'window-'+userid,
			width	:	600,
			modal	:	true,
			items	:	[{
				xtype		:	'form',
				defaults:	{
					width		:	450,
					labelWidth	:	70
				},				
				items		:	[{
					xtype		:	'textfield',
					id			:	'tempUsersShortSale',
					allowBlank	:	false,
					fieldLabel	:	'Users'
				},{
					layout	:	'table',
					border	:	false,
					items	:	[{
						xtype		:	'button',
						text		:	'Search',
						width		:	100,
						listeners	:	{
							click	:	function(button){
								Ext.Msg.show({
								   title      : 'Warning!',
								   msg        : 'UPnder Contruction By Jesus XD',
								   buttons    : Ext.MessageBox.OK,
								   icon       : Ext.MessageBox.INFO
								});
							}
						}
					},{
						xtype		:	'button',
						text		:	'Assign',
						idProcessor	:	idProcessor,
						userid		:	userid,
						width		:	100,
						listeners	:	{
							click	:	function(button){
								if(Ext.getCmp(button.getEl().up('div.x-form-label-left').id).getForm().isValid()){
									Ext.Ajax.request({
										url     : '/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
										method  : 'POST',
										waitMsg : 'Assigning',
										success : function(r,o) {
											var resp	=	Ext.decode(r.responseText);
											if(resp.success==true){
												Ext.Msg.show({
												   title      : 'Info!',
												   msg        : resp.mensaje,
												   buttons    : Ext.MessageBox.OK,
												   icon       : Ext.MessageBox.INFO
												});
												Ext.getCmp('grid-'+button.userid).getStore().reload();
												Ext.getCmp('window-'+button.userid).close();
											}else{
												Ext.Msg.show({
												   title      : 'Warning!',
												   msg        : resp.mensaje,
												   buttons    : Ext.MessageBox.OK,
												   icon       : Ext.MessageBox.WARNING
												});
											}
										},
										failure:function(response,options){
											var d = Ext.util.JSON.decode(response.responseText);
											Ext.Msg.alert('Failure', d.errors.reason);
										},
										params	:	{
											idfunction	:	'assignShortSalesByUserid',
											idProcessor	:	button.idProcessor,
											users		:	Ext.getCmp('tempUsersShortSale').getValue()
										}
									});
								}
							}
						}
					}]
				}]
			}]
		}).show();
	}
	
	function removeShortSaleUsers(userid,idProcessor,tempUsers){
		var users	=	new Array();
		Ext.each(tempUsers,function(data,index){
			users.push(data.data.userid);
		});
		Ext.Ajax.request({
			url     : '/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
			method  : 'POST',
			waitMsg : 'Assigning',
			success : function(r,o) {
				var resp	=	Ext.decode(r.responseText);
				if(resp.success==true){
					Ext.Msg.show({
					   title      : 'Info!',
					   msg        : resp.mensaje,
					   buttons    : Ext.MessageBox.OK,
					   icon       : Ext.MessageBox.INFO
					});
					Ext.getCmp('grid-'+button.userid).getStore().reload();
					Ext.getCmp('window-'+button.userid).close();
				}else{
					Ext.Msg.show({
					   title      : 'Warning!',
					   msg        : resp.mensaje,
					   buttons    : Ext.MessageBox.OK,
					   icon       : Ext.MessageBox.WARNING
					});
				}
			},
			failure:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Failure', d.errors.reason);
			},
			params	:	{
				idfunction	:	'deleteShortSale',
				idProcessor	:	idProcessor,
				users		:	users.join()
			}
		});
	}
	function addTab(userid,name,idProcessor){
		var tempPanel	=	Ext.getCmp('tab-'+userid);
		if(tempPanel)
			tempPanel.show();
		else{
			var tempPanel	=	new Ext.Panel({  
				border			:	false,  
				enableTabScroll	:	true,  
				height			:	Ext.getBody().getViewSize().height-25,
				items			:	[]  
			});
			var tempMySelectionModelMen = new Ext.grid.CheckboxSelectionModel({
				singleSelect:	false
			});
			var tempStore	=	new Ext.data.JsonStore({
				root			:	'results',
				totalProperty	:	'total',
				idProperty		:	'userid',
				autoLoad		:	true,
				remoteSort		:	true,
				fields			:	[
					{name: 'userid', type: 'int'},
					{name: 'id_processor', type: 'int'},
					{name: 'id_user', type: 'int'},
					'name'
				],
				proxy			:	new Ext.data.HttpProxy({
					url		:	'/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
					timeout	:	0
				}),
				baseParams		:	{
					idfunction		:	'searchShortSales',
					useridProcessor	:	userid
				},
				reader			:	new Ext.data.JsonReader()
			});
			tabPanelCenter.add({
				title		:	'Processor configuration: ' + name,
				id			:	'tab-'+userid,
				iconCls		:	'tabs',
				bodyStyle	:	'padding: 0px;',
				closable	:	true,
				items		:	[{
					xtype			:	'panel',
					bodyStyle	:	'padding: 0px;',
					tbar			:	[{
						text	:	'Operations',
						iconCls	:	'bmenu',  // <-- icon
						menu	:	{
							xtype:'menu',
							style: {
								overflow: 'visible'     // For the Combo popup
							},
							items: [{
								text: 'Assign Users',
								userid:userid,
								idProcessor:idProcessor,
								listeners:{
									click	:	function(button, menuItem, e){
										addShortSaleUsers(button.userid,button.idProcessor);
									}
								}
								//checked: true//,       // when checked has a boolean value, it is assumed to be a CheckItem
								//checkHandler: onItemCheck
							},{
								text: 'Remove Users',
								userid:userid,
								idProcessor:idProcessor,
								listeners:{
									click	:	function(button, menuItem, e){
										var grid	=	Ext.getCmp('grid-'+button.userid);
										if (grid.getSelectionModel().hasSelection()) {
											Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(btn){
												if(btn=="yes")
													removeShortSaleUsers(button.userid,button.idProcessor,grid.getSelectionModel().getSelections());
											});
										}else{
											Ext.Msg.show({
											   title      : 'Info!',
											   msg        : "Please make a Selection.!",
											   buttons    : Ext.MessageBox.OK,
											   icon       : Ext.MessageBox.INFO
											});
										}
									}
								}
								//checked: true//,       // when checked has a boolean value, it is assumed to be a CheckItem
								//checkHandler: onItemCheck
							}]
						}  // assign menu by instance
					}],
					border			:	false,  
					enableTabScroll	:	true,  
					height			:	Ext.getBody().getViewSize().height-50,
					items:[{
						xtype		:	'grid',
						id			:	'grid-'+userid,
						bodyStyle	:	'padding: 0px;',
						title		:	'Short Sale User\'s',
						hideHeaders	:	true,
						loadMask	:	true,
						store		:	tempStore,
						height		:	Ext.getBody().getViewSize().height-80,
						sm			:	tempMySelectionModelMen,
						userid		:	userid,
						columns		:	[
							tempMySelectionModelMen,{
							//id			:	'userid', // id assigned so we can apply custom css (e.g. .x-grid-col-topic b { color:#333 })
							dataIndex	:	'userid',
							width		:	40,
							renderer	:	renderTopic,
							sortable	:	true
						},{
							//header: "Author",
							dataIndex	:	'name',
							width		:	183,
							//hidden	:	true,
							sortable	:	true
						},{
							xtype: 'actioncolumn',
							width: 50,
							items: [{
								icon   : '/img/del_doc.png',  // Use a URL in the icon config
								tooltip: 'Remove from this Processor',
								handler: function(grid, rowIndex, colIndex) {
									var record = grid.getStore().getAt(rowIndex).data;
									Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(btn){
										if(btn=="yes"){
											Ext.Ajax.request({
												url     : '/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
												method  : 'POST',
												waitMsg : 'Removing',
												success : function(r,o) {
													Ext.getCmp('grid-'+grid.userid).getStore().reload();
													var resp	=	Ext.decode(r.responseText);
													if(resp.success==true){
														Ext.Msg.show({
														   title      : 'Info!',
														   msg        : resp.mensaje,
														   buttons    : Ext.MessageBox.OK,
														   icon       : Ext.MessageBox.INFO
														});
														Ext.getCmp('grid-'+button.userid).getStore().reload();
														Ext.getCmp('window-'+button.userid).close();
													}else{
														Ext.Msg.show({
														   title      : 'Warning!',
														   msg        : resp.mensaje,
														   buttons    : Ext.MessageBox.OK,
														   icon       : Ext.MessageBox.WARNING
														});
													}
												},
												failure:function(response,options){
													var d = Ext.util.JSON.decode(response.responseText);
													Ext.Msg.alert('Failure', d.errors.reason);
												},
												params	:	{
													idfunction	:	'deleteShortSale',
													idProcessor	:	record.id_processor,
													users		:	record.userid
												}
											});
										}
									});
								}
							}]
						}],
						bbar		:	new Ext.PagingToolbar({
							pageSize: 25,
							store:  tempStore,
							displayInfo: true,
							displayMsg: 'Displaying topics {0} - {1} of {2}',
							emptyMsg: "No topics to display"
						})
					}]  
				}]
			}).show();
		}
    }
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	var mySelectionModelMen = new Ext.grid.CheckboxSelectionModel({
		singleSelect:	false,
		listeners	:	{
			rowselect	:	function( selectionModel, rowIndex, r ){
				if(selectionModel.getCount()==1){
					addTab(r.data.userid,r.data.name,r.data.id_processor);
				}
			}
		}
	});
	var processorStore	=	new Ext.data.JsonStore({
		root: 'data',
		totalProperty: 'totalCount',
		idProperty: 'userid',
		autoLoad:	true,
		remoteSort: true,
		fields: [
			{name: 'userid', type: 'int'},
			'name',
			'id_processor'
		],
		proxy: new Ext.data.HttpProxy({
			url		:	'/backoffice/php/grid_data.php?tipo=loadProcessors',
			timeout	:	0
		}),
		reader: new Ext.data.JsonReader(),
	});
	var tabPanelLeft = new Ext.Panel({  
		border: false,  
		enableTabScroll:true,  
		height:  Ext.getBody().getViewSize().height-25,
		items:[{
			xtype		:	'grid',
			hideHeaders	:	true,
			loadMask	:	true,
			store		:	processorStore,
			height		:	Ext.getBody().getViewSize().height-50,
			sm			:	mySelectionModelMen,
			columns		:	[
				mySelectionModelMen,{
				id			:	'userid', // id assigned so we can apply custom css (e.g. .x-grid-col-topic b { color:#333 })
				dataIndex	:	'userid',
				width		:	40,
				renderer	:	renderTopic,
				sortable	:	true
			},{
				//header: "Author",
				dataIndex	:	'name',
				width		:	183,
				//hidden	:	true,
				sortable	:	true
			}],
			bbar		:	new Ext.PagingToolbar({
				pageSize: 25,
				store:  processorStore,
				//displayInfo: true,
				//displayMsg: 'Displaying topics {0} - {1} of {2}',
				emptyMsg: "No topics to display"
			})
		}]  
	});
	var tabPanelCenter = new Ext.TabPanel({  
		border: false,  
		enableTabScroll:true,  
		height:  Ext.getBody().getViewSize().height-25//,
		//items:[form]  
	});

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		//hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'west',
			title:'Processors',
			width:250,
			autoHeight: true,
			items: tabPanelLeft
		},{
			region:'center',
			autoHeight: true,
			items: tabPanelCenter
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////	
});

