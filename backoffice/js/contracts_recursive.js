Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=contracts_recursive',
        remoteSort: true,
		fields: [
			'cant'
			,'status'
			,'parcelid'
			,'address' 
			,'state'
			,'county'
			,'city'	
			,'zip'	
			,'financial'	
			,'mindate'	
			,'maxdate'	
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[{
			width:120,
			fieldLabel:'County',
			name:'cbCounty',
			id:'cbCounty',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['id','text'],
				data : Ext.combos_selec.countiesfl 
			}),
			mode: 'local',
			valueField: 'id',
			displayField: 'text',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value:'*',
			listeners: {'select': searchActive} 
		},{
			xtype:'label',
			text: 'Status: '
		},{
			width:90,
			//fieldLabel:'County',
			name:'cbStatus',
			id:'cbStatus',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['id','text'],
				data : [['*','All Status'],['1','Active'],['2','Nonactive']]
			}),
			mode: 'local',
			valueField: 'id',
			displayField: 'text',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value:'*',
			listeners: {'select': searchActive} 
		},{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			//text: 'Search',			
			icon: 'images/setsearch.gif',
			id: 'ssdft_butt',
			tooltip: 'Click to Search ',
			handler: searchActive  
        },'-',{
			xtype:'label',
			text: 'Date Offer: '
		},{
			xtype: 'datefield',
            id: 'date1',
			name : 'date1',
            dateFormat: 'Y-m-d'			
		},{
			xtype: 'datefield',
            id: 'date2',
			name : 'date2',
            dateFormat: 'Y-m-d'
		},{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			//text: 'Search',			
			icon: 'images/setsearch.gif',
			id: 'sdat_butt',
			tooltip: 'Click to Search Data Between',
			handler: searchDateOffer  
        },'-',
		{
			xtype:'label',
			text: 'Parcelid: '
		},{
			xtype: 'textfield',
			id: 'sparcelid',
			name: 'sparcelid',
			width: 150 
		},{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			//text: 'Search',						
			icon: 'images/setsearch.gif',
			id: 'spid_butt',
			tooltip: 'Click to Search Parcelid',
			handler: searchParcelid  
        },'-',
		{
			xtype:'label',
			text: 'Address: '
		},{
			xtype: 'textfield',
			id: 'saddress',
			name: 'saddress',
			width: 150 
		},{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/setsearch.gif',
			//text: 'Search',			
			id: 'sadd_butt',
			tooltip: 'Click to Search Address',
			handler: searchAddress  
        }]
	
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////

	function searchActive(){
		store.setBaseParam('county',Ext.getCmp("cbCounty").getValue());
		store.setBaseParam('status',Ext.getCmp("cbStatus").getValue());
		store.setBaseParam('date1',Ext.getCmp("date1").getValue());
		store.setBaseParam('date2',Ext.getCmp("date2").getValue());
		store.load({params:{start:0, limit:100}});
	}
	
	function searchParcelid(){
		var sparcelid=Ext.getCmp("sparcelid").getValue();
		if(sparcelid!='')
			store.load({params:{start:0, limit:100, parcelid:sparcelid}});
	}
	
	function searchAddress(){
		var saddress=Ext.getCmp("saddress").getValue();
		if(saddress!='')
			store.load({params:{start:0, limit:100, address:saddress}});
	}
	
	
	function searchDateOffer(){
		var date1=Ext.getCmp("date1").getValue();
		var date2=Ext.getCmp("date2").getValue();
		if(date1!='' && date2!='')
		{
			store.setBaseParam('date1',Ext.getCmp("date1").getValue());
			store.setBaseParam('date2',Ext.getCmp("date2").getValue());
			
			store.load({params:{start:0, limit:100, date1:date1, date2:date2}});
		}
	}
	
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
		var idc=record.data.id;
		var name = record.data.filename;
		return String.format(
				'<a href="javascript:void(0)" title="Click to get signed contract." onclick="getSignature({0})"><img src="images/notes.png"></a>'+
				'&nbsp;<a href="javascript:void(0)" title="Click to upload signed contract." onclick="uploadContract({0})"><img src="images/up.png"></a>'+
				'&nbsp;<a href="javascript:void(0)" title="Click to delete row." onclick="DeleteWindows({0})"><img src="images/cross.gif"></a>',value);
	}
	
	function renderStatus(value, p, record){
		if(value=='A') return '<div style="background: url(\'images/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;" title="Active">&nbsp;</div>';
		if(value!='A') return '<div style="background: url(\'images/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;" title="Non-Active">&nbsp;</div>';
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			//,{id:'id',header: "ID", width: 50, align: 'center', sortable: true, dataIndex: 'id'}
			,{header: 'Sta', width: 40, sortable: true, align: 'center', dataIndex: 'status',renderer: renderStatus}
			,{header: 'Count', width: 60, sortable: true, align: 'center', dataIndex: 'cant'}
			,{header: "Parcelid", width: 150, align: 'left', sortable: true, dataIndex: 'parcelid', editor: new Ext.form.TextField({allowBlank: false})}
			,{header: "Financial", width: 140, align: 'left', sortable: true, dataIndex: 'financial'}
			,{header: "Address", width: 140, align: 'left', sortable: true, dataIndex: 'address', editor: new Ext.form.TextField({allowBlank: false})}
			//,{header: "State", width: 100, align: 'left', sortable: true, dataIndex: 'state'}
			,{header: 'County', width: 130, sortable: true, align: 'left', dataIndex: 'county'}
			,{header: 'City', width: 150, sortable: true, align: 'left', dataIndex: 'city'}
			,{header: "Zip", width: 80,  sortable: false, align: 'left', dataIndex: 'zip'}			
			,{header: "Last Offer", width: 80,  sortable: false, align: 'center', dataIndex: 'maxdate'}			
			,{header: "First Offer", width: 80,  sortable: false, align: 'center', dataIndex: 'mindate'}			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,//'99.8%',
		frame:true,
		title:'Recursives',
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	//grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});