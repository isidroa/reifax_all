
function borraEmail(idcont){
	var confirmar = false;
	Ext.Msg.confirm('Delete','Please confirm delection?', function(btn){
		if (btn == 'yes'){
			Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/eliminaremail.php', 
				method: 'POST',
				params: {
					cont: idcont 
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					var store1 = Ext.getCmp("gridpanel").getStore();
					store1.commitChanges();
					store1.reload();
				}
			 }
		); 
		}
	});				
}	
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=usersWorkshop',
		fields: [
			{name: 'userid', type: 'int'}
			,{name: 'idworkshop', type: 'int'}
			,'name'
			,'surname'
			,'email'
			,'hometelephone'
			,'transactionID' 
			,'amount'
			,'fecha'			
			,'cobradordesc'
			,'frecuency'
			,'idrecord_workshop'
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",			
		items:[{
			id: 'add_butt',
			tooltip: 'Add user free to workshop',
			text: 'Add',
			iconCls:'icon',
			icon: 'images/add.gif',
        	handler: doAdd
		},{
			id: 'del_butt',
			tooltip: 'Delete selected row',
			text: 'Delete',
			iconCls:'icon',
			icon: 'images/delete.gif',
        	handler: doDel
		},{
				id: 'exc_butt',
				text: 'Excel',
                tooltip: 'Export to Excel',
				iconCls:'icon',
				icon: 'images/excel.png',
                handler: ExportExcel
		},{
			width:400,
			fieldLabel:'Workshop',
			name:'cbWorkshop',
			id:'cbWorkshop',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['idworkshop', 'textcombo'],
				data : Ext.combos_selec.storeWS 
			}),
			mode: 'local',
			valueField: 'idworkshop',
			displayField: 'textcombo',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value: activews,
			listeners: {
				'select': searchWS
			} 
		}]
    });
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
	function ExportExcel()
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "Excel/xlsUsers.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("parametro=usersworkshop&idws="+Ext.getCmp("cbWorkshop").getValue());
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				//relojOFF();
				eval(ajax.responseText);
				window.open('Excel/d.php?nombre='+nombre,'','width=50,height=50');
				return(true);	
			}
		} 
	}


	function doAdd(){
	
		Ext.Ajax.request({   
			waitMsg: 'Saving changes...',
			url: 'php/grid_data.php', 
			method: 'POST',
			params: {
				tipo: 'textWorkshop',
				idws:  Ext.getCmp("cbWorkshop").getValue()
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
			},
			success:function(response,options){
				var rest = Ext.util.JSON.decode(response.responseText);
				if(rest.succes==false)
					Ext.MessageBox.alert('Warning',rest.msg);

				
					var formul = new Ext.FormPanel({
						url:'php/grid_add.php',
						frame:true,
						layout: 'form',
						border:false,
						items: [{
							xtype:'textfield',
							fieldLabel: '<span style="color:#F00">*</span> Workshop',
							name: 'ws',
							id: 'ws',
							width:450,
							readOnly: true,
							allowBlank: false,
							value: rest.textcombo
						},{
							width: 400,
							fieldLabel:'<span style="color:#F00">*</span> User',
							name:'cbuser',
							id:'cbuser',
							xtype:'combo',
							store: new Ext.data.SimpleStore({
								fields: ['id', 'text'],
								data : Ext.combos_selec.dataUsers2
							}),
							editable :false,
							mode: 'local',
							valueField: 'id',
							displayField: 'text',
							hiddenName: 'cbuser',
							triggerAction: 'all',            
							selectOnFocus: true,
							typeAhead: true,
							allowBlank: false,
							emptyText : 'Select user'
						}],
						buttonAlign: 'center',
						buttons: [{
							text: '<b>Add User To Workshop</b>',
							cls: 'x-btn-text-icon',			
							icon: 'images/disk.png',
							formBind: true,
							handler  : function(){
								if(formul.getForm().isValid())
								{
									formul.getForm().submit({
										method: 'POST',
										params: {
											tipo : 'userworkshop',
											idws:  Ext.getCmp("cbWorkshop").getValue()
										},								
										waitTitle: 'Please wait..',
										waitMsg: 'Sending data...',
										success: function(form, action) {
											store.reload();
											obj = Ext.util.JSON.decode(action.response.responseText);
											Ext.Msg.alert("Success", obj.msg);
											wind.close();
										},
										failure: function(form, action) {
											obj = Ext.util.JSON.decode(action.response.responseText);
											Ext.Msg.alert("Failure", obj.msg);
										}
									});
								}
							}
						},'->',{
							text: 'Cancel',
							cls: 'x-btn-text-icon',			
							icon: 'images/cross.gif',
							handler  : function(){
								wind.close();
						   }
						}]				
					});
					var wind = new Ext.Window({
							title: 'Add User To Workshop',
							iconCls: 'x-icon-templates',
							layout      : 'fit',
							width       : 600,
							height      : 150,
							resizable   : false,
							modal	 	: true,
							plain       : true,
							items		: formul				
						});
				
					wind.show();
					wind.addListener("beforeshow",function(wind){
							formul.getForm().reset();
					});
					
								
			}
		}); 

	}

	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		var i=0;
		var marcados='(';		
		for(i=0; i<selec.length; i++){
			if(i>0) marcados+=',';
			marcados+=selec[i].json.idrecord_workshop;
		}
		marcados+=')';
		if(i==0)marcados=false;		
		return marcados;
	}
	function doDel(){
		
		var obtenidos=obtenerSeleccionados();
		//alert( var include_type = mypanel.getValues()['radiocobros'] );
		if(!obtenidos){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'You must select a row, by clicking on it, for the delete to work.',
				buttons: Ext.MessageBox.OK,
				icon:Ext.MessageBox.ERROR
			});						
			return;
		}
		obtenidos=obtenidos.replace('(','');
		obtenidos=obtenidos.replace(')','');
		//Ext.MessageBox.alert('Message',obtenidos);return;

		Ext.MessageBox.confirm('Users Workshop','Are you sure you want to delete this row?',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo: 'usersWorkshop',
							ID:  obtenidos
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							
							if(rest.succes==false)
								Ext.MessageBox.alert('Warning',rest.msg);
								
							store.reload();
						}
					}); 
				}
			}
		)
	}
	function searchWS(){
		store.setBaseParam('ws',Ext.getCmp("cbWorkshop").getValue());
		store.load({params:{start:0, limit:100}});
	}
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
        return String.format(
                '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
    }	
	function viewnotes(val, p, record){
		var comment = record.data.comment;
		return  '<img src="images/notes.png" border="0" ext:qtip="'+comment+'">';
	}
	function viewnstatus(val, p, record){
		
		return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		title:'Users Workshop', 
		id:"gridpanel",
		store: store, 
		iconCls: 'icon-grid',  
		columns: [	
			new Ext.grid.RowNumberer()
			,mySelectionModel
			//,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: viewnotes}
			,{id: 'idrecord_workshop',header: 'Userid', width: 50, sortable: true, align: 'center', dataIndex: 'userid',renderer: renderTopic}
			,{header: 'Name', width: 120, sortable: true, align: 'left', dataIndex: 'name'}
			,{header: 'Surname', width: 120, align: 'left', sortable: true, dataIndex: 'surname'}
			,{header: 'Email', width: 200, align: 'left', sortable: true, dataIndex: 'email',editor: new Ext.form.TextField()}
			,{header: 'Phone', width: 90, align: 'left', sortable: true, dataIndex: 'hometelephone',editor: new Ext.form.TextField()}
			,{header: 'Frecuency', width: 80, align: 'left', sortable: true, dataIndex: 'frecuency'}
			,{header: 'TransactionID', width: 100, align: 'left', sortable: true, dataIndex: 'transactionID'}
			,{header: 'Date', width: 100, sortable: true, align: 'center', dataIndex: 'fecha'}
			,{header: 'Amount', width: 80, sortable: true, align: 'right', dataIndex: 'amount', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{header: 'Description', width: 150, sortable: true, align: 'left', dataIndex: 'cobradordesc'}//, renderer: cobradordesc}
		],
		clicksToEdit:2,
		height:470,
		sm: mySelectionModel,
		width: screen.width,//'99.8%',
		frame:true,
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.setBaseParam('ws',activews);
	store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////
 
});
