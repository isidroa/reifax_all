// JavaScript Document
Ext.ns('emailVal');

Ext.override(Ext.form.ComboBox, {
      onTypeAhead : function(){
   }
});
emailVal=
{
	init: function ()
	{
		emailVal.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total',
			id:'readeremailVal'
			},
			emailVal.record 
			);
			
		emailVal.store = new Ext.data.Store({  
			id: 'emailVal',  
			proxy: emailVal.dataProxy,  
			remoteSort :true,
			baseParams: {
				accion	:'list'
			},  
			reader: emailVal.gridReader  
		});
		
		emailVal.pager = new Ext.PagingToolbar({  
			store: emailVal.store, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 30,
			items:[
			], 
			listeners: {
				afterrender : function(){
				}
			}
		});
		emailVal.pager.on('beforechange',function(bar,params){
		});
        var textField = new Ext.form.TextField();  
        var numberField = new Ext.form.NumberField({allowBlank:false}); 
		var dateField=new Ext.form.DateField();

				
		var opcionesCombo= [
        ['1', 'Active'],
       // ['3', 'Completed'],
       // ['5', 'Expired'],
        ['4', 'Canceled'] ];
		
		var opcionesStatus= {
			1	:	'Active',
			3	:	'Completed',
			5	:	'Expired',
			4	:	'Canceled'
		}
		
		var store = new Ext.data.SimpleStore({
			fields: ['abbr', 'status'],
			data : opcionesCombo
		});
		var combo = new Ext.form.ComboBox({
			forceSelection:true,
			store: store,
			displayField:'status',
			typeAhead: true,
			mode: 'local',
			valueField:'abbr',
			editable	:false,
			triggerAction: 'all',
			emptyText:'Select a Status...',
			selectOnFocus:true
		});
		
		var storeUserAdmin= new Ext.data.SimpleStore({
					fields: ['userid', 'name'],
					data : Ext.combos_selec.dataUsers2
			});
		var comboUser = new Ext.form.ComboBox({
			store: storeUserAdmin,
			hiddenName	: 'userPro',
			valueField: 'userid',
			displayField:'name',
			 style: {
                fontSize: '16px'
            },
			typeAhead: true,
			triggerAction: 'all',
			mode: 'local',
			selectOnFocus:true
		})
		
		var textfield=new Ext.form.TextField({});
		
		
		emailVal.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
				//emailVal.sm,
			{  
				header: 'Id emailVal',  
				dataIndex: 'id', 
				sortable: true,  
				width: 30  
			},
			{
				header: 'Date',  
				dataIndex: 'date_register', 
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				sortable: true,  
				width: 80  
			},
			{
				header: 'Email',  
				dataIndex: 'email', 
				sortable: true,  
				width: 150  
			},
			{
				header: 'Status',  
				dataIndex: 'status', 
				sortable: true,  
				width: 30,
				renderer:function (val){
					if(val=='200' || val=='215' || val=='207' ){
						return 'invalid';
					}
					else{
						return 'valid';
					}
				} 
			},
			{
				header: 'Description Status',  
				dataIndex: 'status_text', 
				sortable: true,  
				width: 200  
			},
			{
				header: 'Limit Status',  
				dataIndex: 'limit_status', 
				sortable: true,  
				width: 30  
			},
			{
				header: 'Limit Des.',  
				dataIndex: 'limit_desc', 
				sortable: true,  
				width: 80  
			}
			]  
		);
		emailVal.pager.on('beforechange',function(bar,params){
				
		});
		emailVal.store.load();	
		emailVal.grid = new Ext.grid.EditorGridPanel({
			height: 500,
            store: emailVal.store,
			tbar: emailVal.pager,   
			cm: emailVal.columnMode,
            border: false,
				viewConfig: {
					forceFit:true, 
					getRowClass : function (row, index) { 
							var cls = ''; 
							
							var col = row.data;
							if(col.status=='200' || col.status=='215' || col.status=='207' )
							{
								cls = 'green-row'
							}
							else{
								cls = 'red-row'
							}
							
							 return cls; 
						  } 
				},
            stripeRows: true ,
			listeners:{
				
			},
			sm:emailVal.sm,
			loadMask: true,
			border :false
        }); 
		
		emailVal.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: emailVal.grid,
					autoHeight: true
			}]
		});
		
	},
	sm:new Ext.grid.CheckboxSelectionModel()
	,
	record : new Ext.data.Record.create([ 
			{name: 'id', type: 'int'},  
			{name: 'email', type: 'string'},
			{name: 'status', type: 'nt'},    
			{name: 'status_text', type: 'string'},  
			{name: 'limit_status', type: 'nt'},    
			{name: 'limit_desc', type: 'string'},
			{name: 'date_register', type: 'date', dateFormat: 'Y-m-d H:i:s'},
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesemailVal.php',   // Servicio web  
			method: 'POST'                          // M�todo de env�o  
	}),
	save	:function ()
	{
		var grid=emailVal.grid;
		var modified = grid.getStore().getModifiedRecords();
		
		if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
			var recordsToSend = [];  
			Ext.each(modified, function(record) {
				recordsToSend.push(Ext.apply({id:record.id},record.data));  
			});  
		  
			grid.el.mask('Saving...', 'x-mask-loading');
			grid.stopEditing();  
		  
			recordsToSend = Ext.encode(recordsToSend);
		  
			Ext.Ajax.request({ 
				url : 'php/funcionesemailVal.php',  
				params :
					{
						records : recordsToSend,
						accion	:'update'
					},  
				scope:this,  
				success : function(response) {
					grid.el.unmask();  
					grid.getStore().commitChanges();  
					grid.getStore().reload();
				}  
			});  
		}  
			
	},
	export_excel:function(){
		var grid=emailVal.grid;
		grid.el.mask('Exporting...', 'x-mask-loading');
		
		Ext.Ajax.request({
			method :'POST',
		   	url: 'Excel/xlsemailVal.php',
		   	success: function ( result, request ){
			 	grid.el.unmask();  
				window.open('Excel/d.php?nombre='+result.responseText,'','width=50,height=50');
				return(true); 
			},
		   	failure:  function (){
			   grid.el.unmask();  
			},
		  	 params: { 
			 	parametro: 'Export_grid',
				status:Ext.getCmp('emailValFilter').getForm().findField("status").getValue()//,
				/*typeDate:Ext.getCmp('emailValFilter').getForm().findField("type_date").getValue(),
				startDate: Ext.util.Format.date(
					Ext.getCmp('emailValFilter').getForm().findField("startdate").getValue()
					,'Y-m-d'
				),
				dueDate: Ext.util.Format.date(
					Ext.getCmp('emailValFilter').getForm().findField("duedate").getValue()
					,'Y-m-d'
				)*/
			}
		});
		

	},
	reset: function (){
		var cols=emailVal.grid.getSelectionModel( ).getSelections( );
		for(i in cols){
			if(cols[i].set){
				var date=cols[i].set('date_emailVal','');
				var date=cols[i].set('houremailVal','');
			}
		}
	},
	coachinOpenNote: function (id,userid,name){
		
		var grid = new Ext.grid.GridPanel({
			baseCls			: 'cellComplete',
			store: new Ext.data.Store({  
				id: 'emailVal', 
				autoLoad :true,
				proxy: emailVal.dataProxy,  
				baseParams: {
					accion	:'listarNotes',
					id	: id
				},  
				reader:new Ext.data.JsonReader({  
				root: 'coacing', 
				totalProperty: 'total',
				},
					 new Ext.data.Record.create([
						{name: 'id', type: 'id'},
						{name: 'note', type: 'string' },
						{name: 'user', type: 'string' },
						{name: 'date_note', type: 'date', dateFormat: 'Y-m-d'}
					])   
				)
			}),
			colModel: new Ext.grid.ColumnModel({
				columns: [{  
						header	: 'Note',  
						dataIndex	: 'note', 
						//height		:80,
						sortable	: false,
						style		: 'white-space:normal !important;',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							metaData.style = "line-height:18px;";
							var text = "<p style='cursor:pointer;'>"+value+"</p>";
							return text;
						},
						width: 357,
							
					},{  
						header	: 'Date',  
						sortable: false, 
						width	: 100,
						renderer: Ext.util.Format.dateRenderer('Y/m/d'),
						dataIndex	: 'date_note'
					},{  
						header	: 'User',  
						sortable: false, 
						width	: 100,
						dataIndex	: 'user'
					}
				]
			}),
			height	: 200,
			width	: 577,
			frame	: true
		});
		var form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [
						{
							xtype	:'hidden',
							name	:'id',
							value	: id
						},{
							xtype	:'hidden',
							name	:'accion',
							value	:'recordNote'
						},{
							xtype	: 'textarea',
							emptyText: 'New Note',
							name	: 'note',
							allowBlank :false,
							hideLabel:true,
							height: 80,
							width: 540
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Close',
							handler	: function ()
							{
								btn=this;
								var form=this.findParentByType('form');
								var win=this.findParentByType('window');
								if(form.getForm().isValid())
								{
									form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new nota...',
										url: 'php/funcionesemailVal.php',
										scope : this,
										params :{
											accion : 'recordNote'
										},
										success	: function (form,action)
										{
											//emailVal.store.load();
											win.close();
											win.destroy();
										}
									})
								}
								else{
									win.close();
									win.destroy();
								}
							}
						}]
				})
		var win=new Ext.Window({
			modal	:true,
			title: 'Notes of '+id, 
			closable: false,
			resizable: false,
			maximizable: false,
			plain: true,
			border: false,
			width: 590,
			height: 430,
			items	:[
				{
					xtype	:'panel',
					bodyStyle	:'font-size:14px;',
					pandding	:13,
					html	:'<strong>User:</strong> '+name+' (<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+userid+')">'+userid+'</a>)'
				},
				grid,
				form
			]
		});
		win.show();
	},
	cancel: function ()
	{
		emailVal.grid.getStore().rejectChanges();
	},
	delete:function (){
		data=emailVal.grid.getSelectionModel().getSelections();
		if(data.length){
			var ids='';
			for(i=0;i<data.length;i++){
				ids+=(ids=='')?data[i].data.id:','+data[i].data.id;
			}
		}
		Ext.Ajax.request({
			method :'POST',
		   	url: 'php/funcionesemailVal.php',
		   	success: function (){
			   emailVal.grid.getStore().reload();
			},
		   	failure:  function (){
			   
			},
		  	 params: { 
			 	accion: 'delete',
				ids	: ids
			}
		});
	}
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	},
	new:{
		init:function (){
			if(emailVal.new.win)
			{
				emailVal.new.form.getForm().reset();
				emailVal.new.win.show();
			}
			else{
				emailVal.new.storeProducst= new Ext.data.Store({
								autoLoad:true,
								url: 'php/funcionesemailVal.php', 
								reader: new Ext.data.JsonReader({  
										root: 'root', 
										totalProperty: 'total'
									},
									 new Ext.data.Record.create([ 
										{name: 'idproducto', type: 'int'},
										{name: 'name', type: 'string'}
									]) 
								)
							});
				emailVal.new.form = new Ext.form.FormPanel({
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'form',
					items : [{
							xtype: 'combo',
							store: new Ext.data.Store({
								autoLoad:true,
								url: 'php/funcionesemailVal.php', 
								reader: new Ext.data.JsonReader({  
										root: 'root', 
										totalProperty: 'total'
									},
									 new Ext.data.Record.create([ 
										{name: 'userid', type: 'int'},
										{name: 'name', type: 'string'}
									]) 
								)
							}),
							hiddenName: 'client', 
							valueField: 'userid',
							displayField:'name',
							fieldLabel:	'Client',
							allowBlank   : false,
							forceSelection: false,
							editable   : true,
							minChars   : 2,        
							queryDelay   : 1,
							autoSelect   : false,
							typeAhead    : true,
							typeAheadDelay: 250,
							listeners:{
								change:function ( that, newValue, oldValue ){
									emailVal.new.storeProducst.load({
										params:{
											userid:newValue,
											accion:'getProducs'
										}	
									});
								}
							},
							style: {
								fontSize: '16px'
							}
					},{
							xtype: 'combo',
							store: new Ext.data.SimpleStore({
								fields: ['val', 'text'],
								data : [['6','1 Hour'],['8','2 Hour'],['9','3 Hour'],['7','4 Hour']]
							}),
							hiddenName: 'time', 
							valueField: 'val',
							displayField:'text',
							fieldLabel:	'Time',
							allowBlank:false,
							value:6,
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},{
							xtype: 'combo',
							store: emailVal.new.storeProducst,
							hiddenName: 'product', 
							valueField: 'idproducto',
							displayField:'name',
							fieldLabel:	'Product',
							allowBlank:false,
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local',
							selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
					},new Ext.form.DateField({
						fieldLabel:	'emailVal Date',
						format: 'Y-m-d',
						name	:'dateemailVal',
						 style: {
							fontSize: '16px'
						},})
					,new Ext.form.TimeField({
						fieldLabel:	'emailVal Hour',
						forceSelection:true,
						name	:'hour',
						minValue: '9:00 AM',
						maxValue: '6:00 PM',
						increment: 30,
						 style: {
							fontSize: '16px'
						},})
					,{
						xtype:'combo',
						store: new Ext.data.SimpleStore({
								fields: ['userid', 'name'],
								data : Ext.combos_selec.dataUsers2
						}),
						fieldLabel:	'Mandated',
						hiddenName	: 'userPro',
						valueField: 'userid',
						displayField:'name',
						 style: {
							fontSize: '16px'
						},
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true
					}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Add',
							handler	: function ()
							{
								if(emailVal.new.form.getForm().isValid())
								{
									emailVal.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new emailVal...',
										url: 'php/funcionesemailVal.php',
										scope : this,
										params :{
											accion : 'recordemailVal'
										},
										success	: function (form,action)
										{
											emailVal.new.form.getForm().reset();
											emailVal.new.win.hide();
											emailVal.store.load({
																params:{
																	status:Ext.getCmp('emailValFilter').getForm().findField("status").getValue()
																}
															});	
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								emailVal.new.form.getForm().reset();
							}
						}]
				})
				
				
				emailVal.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New emailVal', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 360,
					height: 265,
					items	:[
						emailVal.new.form
					]
				});
				emailVal.new.win.show();
			}
		}
	}
}


Ext.onReady(emailVal.init);
Ext.QuickTips.init()