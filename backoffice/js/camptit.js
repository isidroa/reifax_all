Ext.ns("tareas");

tareas = {
    globalVar:{},
    stores:{},
    combos:{},
    init : function() {
        tareas.store= new Ext.data.JsonStore({
            id  : 'id',
            autoLoad: true,
            remoteSort :true,
            url: 'php/funcionesTaskManager.php', //donde busca la informacion
            baseParams:{
                    accion: 'consultarcamptit'
            },
            root: 'data',                       //el objeto donde esta la informacion
            fields: [                           //los campos que contendran la data
                    {name: 'IDTC'},
                    {name: 'Tabla'},
                    {name: 'Campos'},
                    {name: 'Titulos'},
                    {name: 'Titulos_a'},
                    {name: 'type'},
                    {name: 'Desc'}
            ]
        });

        tareas.globalVar.opcionespri= [
                ['A','A - High'],
                ['B','B - Medium'],
                ['C','C - Low']];

        tareas.stores.storepri = new Ext.data.ArrayStore({
            fields: ['prioridad','text'],
            data : tareas.globalVar.opcionespri,
            id        : 0
        });

        tareas.combos.combopri =
        {   xtype:'combo',
            store: tareas.stores.storepri,
            fieldLabel:'Priority',
            name: 'prioridadText',
            hiddenName: 'prioridad', //REFERENCIA UN VALOR DE LA DATA DEL COMBO
            valueField: 'prioridad',
            displayField:'text',
            triggerAction: 'all',
            width:80,
            mode: 'local',
            selectOnFocus:true,
            forceSelection:true, // obliga que seleccione solo valores de la lista
            //readOnly:true,
            allowBlank: false
        };
        tareas.globalVar.opcionessta=[['1','Created'],
        //['2','Assigned'],
        ['3','Reassigned'],
        ['5','Active'],
        ['6','Canceled'],
        ['4','Finished'],
        //['7','Hold'],
        //['8','Answer'],
        //['99','Not Finished']
        ]
        tareas.globalVar.opcionesstaFilter=[
        ['98','Active & Created'],
        ['1','Created'],
        ['3','Reassigned'],
        ['5','Active'],
        ['6','Canceled'],
        ['4','Finished']
        ];

        tareas.stores.storestaFilter= new Ext.data.SimpleStore({
            fields: ['idsta','status'],
            data: tareas.globalVar.opcionesstaFilter
        });

        tareas.stores.storesta= new Ext.data.SimpleStore({
            fields: ['idsta','status'],
            data: tareas.globalVar.opcionessta
        });

        tareas.storeUser= new Ext.data.JsonStore({
        url: 'php/combosTaskManager.php',
        root : 'dataUsers',
        autoLoad: true,
        fields: [
                    {name: 'userid'},
                    {name: 'name'}
                ]
        });

        /* tareas.storetype= new Ext.data.JsonStore({
        url: 'php/combosType.php',
        root : 'dataUsers',
        autoLoad: true,
        fields: [
                    {name: 'type'},
                    {name: 'type2'}
                ]
        });*/

        var panelpag = new Ext.form.FormPanel({
            //width: 400,
            layout: 'table',
            id:'tareasFilter',
            name: 'tareasFilter',
            padding: 3,
            defaults: {
                bodyStyle:'margin-left:5px'
            },
            layoutConfig:{
                columns :   2
            },
            style   :{
                background:'none'
            },
            border: false,
            items: [{
                layout  : 'table',
                border: false,
                items   : [
                    {
                        hidden:enabledate,
                        xtype:'datefield',
                        name: 'dateInit',/*,
                        text: 'Assigned'*/
                    },{
                        hidden:enabledate,
                        xtype:'datefield',
                        name: 'dateEnd'/*,
                        text: 'Assigned'*/
                   },{
                        xtype:'label',
                        name: 'lblasig',
                        text: 'Tabla'
                    },
                    {
                        xtype:'combo',
                        name:'asic',
                        id:'asic',
                        listWidth:300,
                        store: new Ext.data.SimpleStore({
                            fields: ['userid', 'Tabla'],
                            data : Ext.combos_selec.datacamptittype//Ext.combos_selec.dataUsersProgramm
                        }),
                        valueField: 'userid',
                        displayField:'Tabla',
                        allowBlank:false,
                        typeAhead: true,
                        triggerAction: 'all',
                        mode: 'local',
                        selectOnFocus:true,
                         style: {
                            fontSize: '16px'
                        }

                    },{
                        xtype:'hidden',
                        name: 'lblstatus',
                        text: 'Status'
                    },{
                    xtype   : 'hidden',
                     style: {
                        fontSize: '16px'
                    },
                    fieldLabel:'Status',
                    store: tareas.stores.storestaFilter,
                    displayField:'status',
                    name:'status',
                    valueField:'idsta',
                    hiddenvalue:'status',
                    width: 120,
                    triggerAction:'all',
                    mode:'local'
                }
                ]
            }]
        });

        tareas.combos.combosta ={
            xtype   :'combo',
            store   : tareas.stores.storesta,
            fieldLabel  :'Status',
            id      :'status',
            name    :'status',
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            value   :'1',
            hiddenValue     :'status',
            valueField      : 'idsta',
            displayField    :'status',
            triggerAction   : 'all',
            mode            : 'local',
            selectOnFocus   :true,
            forceSelection  :true,
            allowBlank      : false
        };


        tareas.combos.comboUser ={
            xtype           :'combo',
             style: {
                fontSize: '16px'
            },
            listWidth:300,
            fieldLabel      :'Type',
            name            :'solicitadon',
            hiddenName      :'solicitado',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'name'],
                data : Ext.combos_selec.dataUsersAdmin
            }),
            valueField: 'userid',
            displayField:'name',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            }

    };
        tareas.combos.comboUsera ={
            xtype       : 'combo',
             style: {
                fontSize: '16px'
            },
            listWidth: 200,
            Width: 200,
            fieldLabel  : 'Type',
            name        : 'type',
            hiddenName  : 'type',
            store: new Ext.data.SimpleStore({
                fields: ['userid', 'type'],
                data : Ext.combos_selec.datacamptittype2
            }),
            valueField: 'userid',
            displayField:'type',
            allowBlank:false,
            typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            selectOnFocus:true,
             style: {
                fontSize: '16px'
            }
    };

        tareas.store.load();                   //cargando el store
        tareas.tb = new Ext.PagingToolbar({
            store       : tareas.store,
            displayInfo : true,
            displayMsg  : '{0} - {1} of {2} Registros',
            emptyMsg    : 'No hay Registros Disponibles',
            pageSize    : 30,
            items       :[
                '|'
                ,
                {
                    xtype   :'button',
                    iconCls :'x-icon-add',
                    text    : 'New',
                    handler : function()
                    {

                        var assing =new Ext.form.ComboBox(tareas.combos.comboUser);
                        var request =new Ext.form.ComboBox(tareas.combos.comboUsera);
                        assing.setValue(userid);
                        request.setValue("NONE");
                        var formc   = new Ext.FormPanel({
                            url:'php/funcionesTaskManager',
                            width: 350,
                            name:'formuc',
                            bodyStyle:'margin/left:10px',
                            border:false,
                            labelWidth:80, // se hacen las etiquetas mas peque;as
                            defaults:{
                                xtype:'textfield',
                                width: 200
                            },
                            items:[
                                //{ fieldLabel:'Task',id:'id'},
                                /*{
                                    xtype       :'combo',
                                    fieldLabel  : 'Priority',
                                    store       : tareas.stores.storepri,
                                    hiddenName: 'prioridad', //REFERENCIA UN VALOR DE LA DATA DEL COMBO
                                    valueField  : 'prioridad',
                                    displayField:'text',
                                    triggerAction: 'all',
                                    width       :80,
                                    value       : 'A',
                                    mode        : 'local',
                                    forceSelection:true,
                                    selectOnFocus:true,
                                    allowBlank  : false
                                },
                                tareas.combos.combosta,
                                assing,*/
                                {
                                    xtype:'hidden',
                                    width:40,
                                    fieldLabel:'IDTC',
                                    name:'IDTC'
                                },
                                {
                                    fieldLabel:'Tabla',
                                    width:200,
                                    name:'Tabla'

                                },
                                {
                                    fieldLabel:'Campos',
                                    width:200,
                                    name:'Campos'
                                },{
                                    fieldLabel:'Titulos',
                                    width:200,
                                    name:'Titulos'
                                },{
                                    fieldLabel:'Titulos_a',
                                    width:200,
                                    name:'Titulos_a'
                                },request,/*{
                                    fieldLabel:'Type',
                                    width:200,
                                    name:'type'
                                },*/{
                                    fieldLabel:'Description',
                                    width:200,
                                    name:'Desc'
                                },
                               /* {
                                    xtype:'textarea',
                                    fieldLabel:'Description',
                                    name:'descripcion',
                                    autoScroll: true,
                                    allowBlank: false,
                                     style: {
                                        fontSize: '16px'
                                    },
                                    height: 170
                                },
                                request,*/
                            ]

                        });

                        var winc = new Ext.Window({
                        title:'New Camptit',
                        name:'wincre',
                        width: 350,
                        height:250,
                        modal: true,
                        bodyStyle: 'padding:10px;background-color:#fff',

                        items:formc,
                        buttonAlign: 'center',
                            buttons: [
                            {
                                text:'Save',
                                handler : function (){
                                    if(formc.getForm().isValid())
                                    {
                                        var values=formc.getForm().getValues();
                                        formc.getForm().submit({
                                            waitTitle:'Recording',
                                            waitMsg :'Saving...',
                                            url: 'php/funcionesTaskManager.php',
                                            scope : this,
                                            params :{
                                                accion : 'crearCamptit'
                                            },
                                            success : function (formx,action)
                                            {
                                                formc.getForm().reset();
                                                winc.close();
                                                tareas.store.load();
                                            }
                                        })
                                    }
                                }
                            },
                            {text:'Cancel', handler:function (){winc.close();}}
                            ]
                    });
                    winc.show();
                    }

                },
                '|'
                ,
                {
                    xtype   :'button',
                    iconCls :'x-icon-cancel',
                    text    : 'Delete',
                    handler : function()
                            {
                                data=tareas.grid.getSelectionModel().getSelections();
                                    if(data.length){
                                        var ids='';
                                        for(i=0;i<data.length;i++){
                                            ids+=(ids=='')?data[i].data.IDTC:','+data[i].data.IDTC;
                                        }
                                    }
                                   // alert(ids);
                                    Ext.Ajax.request({
                                        method :'POST',
                                        url: 'php/funcionesTaskManager.php',
                                        success: function (){
                                           tareas.grid.getStore().reload();
                                        },
                                        failure:  function (){

                                        },
                                         params: {
                                            accion: 'eliminarIDTC',
                                            ids : ids
                                        }
                                    });
                            }
                },
                '|'
                ,panelpag,
                '|',
                {
                    xtype   : 'button',
                    text    : 'Filter',
                    iconCls : 'x-icon-filter',
                    handler : function (){
                        var dateInit=Ext.getCmp('tareasFilter').getForm().findField("dateInit").getValue();
                        if(dateInit != ''){
                            dateInit=dateInit.format('Y-m-d').trim();
                        }
                        var dateEnd=Ext.getCmp('tareasFilter').getForm().findField("dateEnd").getValue();
                        if(dateEnd != ''){
                            dateEnd=dateEnd.format('Y-m-d').trim();
                        }
                        tareas.store.load({
                            params:{
                                status:Ext.getCmp('tareasFilter').getForm().findField("status").getValue(),
                                quien:Ext.getCmp('tareasFilter').getForm().findField("asic").getValue(),
                                dateInit:dateInit,
                                dateEnd:dateEnd
                            }
                        })
                    }
                           },
                {
                    xtype   : 'button',
                    text    : 'Reset',
                    iconCls : 'x-icon-filter',
                    handler : function (){
                        tareas.store.load();
                        Ext.getCmp('tareasFilter').getForm().reset();
                    }
                }
            ]
        });

                tareas.tb.on('beforechange',function(bar,params){
                    params.status=Ext.getCmp('tareasFilter').getForm().findField("status").getValue();
                    params.quien=Ext.getCmp('tareasFilter').getForm().findField("asic").getValue();
        });


        //crearemos el grid donde se mostrara la informacion cargada en el store
            sm = new Ext.grid.CheckboxSelectionModel();

        /**********
        *
            definicion de editores
        **********************/
        var combouser=new Ext.form.ComboBox(tareas.combos.comboUser);
        var comboUsera=new Ext.form.ComboBox(tareas.combos.comboUsera);
        var comboGridPrio=new Ext.form.ComboBox(tareas.combos.combopri);
        var comboGridStatus=new Ext.form.ComboBox({
                    xtype   : 'combo',
                    fieldLabel:'Status',
                    store: tareas.stores.storesta,
                    displayField:'status',
                    name:'status',
                    valueField:'idsta',
                    hiddenvalue:'status',
                    width: 90,
                    triggerAction:'all',
                    mode:'local'
                });
/*
          var combonuevo=new Ext.form.ComboBox({
                    xtype   : 'combo',
                    fieldLabel:'Status',
                    store: tareas.storetype,
                    displayField:'type',
                    name:'usuarios',
                    valueField:'type',
                    hiddenvalue:'type',
                    width: 90,
                    triggerAction:'all',
                    mode:'local'
                });*/


        var textEdit=new Ext.form.TextField();


            tareas.grid= new Ext.grid.EditorGridPanel({
            store: tareas.store,                             //asignamos el store
            tbar : tareas.tb,                                // le incluimos el toolbar
            columns: [
                sm,
                new Ext.grid.RowNumberer(),//agregamos una columna numerada
                 {  header:'IDTC',
                    dataIndex:'IDTC',
                    width:20
                },
                {
                    xtype: 'actioncolumn',
                    width: 20,
                    items: [
                        {
                            icon   : 'http://docs.oracle.com/cd/E11036_01/alsb30/consolehelp/wwimages/edit_icon.gif',
                            tooltip: 'Edit Camptit',
                            handler: function(grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                tareas.editTask(rec);
                            }
                        }
                    ]
                },
                {   header:'Tabla',
                    dataIndex:'Tabla',
                    width:70,
                    editor  :textEdit,//comboGridPrio,
                    sortable: true
                 },
                 {  header:'Campos',
                    dataIndex:'Campos',
                    editor  :textEdit,
                    width:70,
                    sortable: true
                },
                {
                    width:80,
                    header:'Titulos',
                    dataIndex:'Titulos',
                    editor  :textEdit,
                    sortable: true
                },
                {   header:'Titulos_a',
                    dataIndex:'Titulos_a',
                    editor  :textEdit,
                    width:80,
                    sortable: true
                },/*
                {   header:'Decimals',
                    hidden:enabledate,
                    dataIndex:'date_end',
                    editor  :textEdit,
                    width:50,
                    sortable: true
                }   ,
                {   header:'Align',
                    width:50,
                    editor:comboGridStatus,
                    dataIndex:'status',                    // le asignamos a cada columna una propiedad del record del store
                    sortable: true,
                    renderer: function(val){
                            var record = comboGridStatus.findRecord(comboGridStatus.valueField, val);
                            return record ? record.get(comboGridStatus.displayField) : val;
                    }
                },*/
                {   header:'Type',
                    dataIndex:'type',
                    editor  : comboUsera,//textEdit,//combonuevo,
                    sortable: true,
                    width:70
                    /*renderer: function(val){
                            var record = comboUsera.findRecord(comboUsera.valueField, val);
                            return record ? record.get(comboUsera.displayField) : val;
                    }*/},

                {   header:'Descripcion',
                    dataIndex:'Desc',
                    editor  :textEdit,
                    width:350,
                    sortable: true
                } /*  ,
                {   header:'Value',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'Value2',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'Condicional',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'Show',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'Orden',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'OrdenMLS',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'OrdenPS',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'OrdenPEN',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'OrdenMOR',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'NumFormatted',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                },
                {   header:'Grid_orden',
                    dataIndex:'solicitadoname',
                    editor  :combouser,
                    sortable: true,
                    renderer: function(val){
                            var record = combouser.findRecord(combouser.valueField, val);
                            return record ? record.get(combouser.displayField) : val;
                    }
                }*/
                ],
                sm: sm,
                border: false,
                    viewConfig: {
                        forceFit:true
                    },
                autoScroll: true,
                border: false,
                loadMask: true,
                stripeRows: true,
                height:Ext.getBody().getViewSize().height-55,           //debe tener un ancho para que muestre todas las filas
                listeners:{
                    afteredit: function (model){
                        var grid=model.grid;
                        var modified = grid.getStore().getModifiedRecords();

                        if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){
                            var recordsToSend = [];
                            Ext.each(modified, function(record) {
                                recordsToSend.push(Ext.apply({id:record.id},record.data));
                            });

                            grid.el.mask('Saving...', 'x-mask-loading');
                            grid.stopEditing();

                            recordsToSend = Ext.encode(recordsToSend);

                            Ext.Ajax.request({
                                url : 'php/funcionesTaskManager.php',
                                params :
                                    {
                                        records : recordsToSend,
                                        accion  :'updatecamptit'
                                    },
                                scope:this,
                                success : function(response) {
                                    grid.el.unmask();
                                    grid.getStore().commitChanges();
                                }
                            });
                        }
                    }
                }
            });

        tareas.marco= new Ext.Viewport({         // ahora mostramos el grid en un viewport
            layout: 'border',
            hideBorders: true,
            monitorResize: true,
            items: [
                {
                    region: 'north',
                    height: 25,
                    items: Ext.getCmp('menu_page')
                },
                {
                    region:'center',
                    items: tareas.grid,
                    autoHeight: true
            }]
        });

         // creamos el listener del dobleclick de la fila del grid
            /*tareas.grid.on('rowdblclick',function(grid,index,event){
                 var record= grid.getStore().getAt(index);                   //obtengo el record y el indice de la fila clikeada
                 //console.log(record);
                    //Ext.
                });*/
    },
    editTask:function (record){
        var form= new Ext.FormPanel({
                url:'php/funcionesTaskManager.php',
                name:'formua',
                width: 400,
                bodyStyle:'margin/left:10px',
                border:false,
                labelWidth:80, // se hacen las etiquetas mas peque;as
                defaults:{
                    xtype:'textfield',
                    width: 200
                },
                items:[
                    { xtype:'hidden',fieldLabel:'IDTC',name:'IDTC',readOnly:true},
                    {
                        fieldLabel:'Tabla',
                        width:200,
                        name:'Tabla'
                    },
                     {
                        fieldLabel:'Campos',
                        width:200,
                        name:'Campos'
                    },
                     {
                        fieldLabel:'Titulos',
                        width:200,
                        name:'Titulos'
                    },
                     {
                        fieldLabel:'Titulos_a',
                        width:200,
                        name:'Titulos_a'
                    },
                    /* {
                        fieldLabel:'Type',
                        width:200,
                        name:'type'
                    }*/tareas.combos.comboUsera,
                    {
                        fieldLabel:'Desc',
                        width:200,
                        name:'Desc'
                    }
                ]

            });
             var win = new Ext.Window({
                title:'Edit Camptit',
                layout      : 'fit',
                closeAction : 'close',
                width:400,
                height:250,
                modal: true,
                bodyStyle: 'padding:10px;background-color:#fff',

                items:form,
                buttonAlign: 'center',
                buttons: [
                    {
                        text:'Save',
                        handler : function ()
                    {
                        if(form.getForm().isValid())
                            {
                                //var values=form.getForm().getValues();
                                form.getForm().submit({
                                    waitTitle:'Recording',
                                    waitMsg :'Saving...',
                                    url: 'php/funcionesTaskManager.php',
                                    scope : this,
                                    params :{
                                        accion : 'actualizarcamptit'
                                    },
                                    success : function (formg,action)
                                    {
                                        form.getForm().reset();
                                        win.close();
                                        tareas.store.load();
                                    }
                                })
                            }
                    }
                    },
                    {text:'Cancel', handler:function (){win.close();}}
                    ]
            });
            //console.log('HOLA');
            win.show();
            form.getForm().loadRecord(record); //se llena el formulario con la data por tener el mismo id el form y el store
    }
}
Ext.onReady(tareas.init);
