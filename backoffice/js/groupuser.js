  function previewHtmlMenu(idtmail)
    {
       // alert(idtmail);
        Ext.Ajax.request({
            waitMsg: 'Loading...',
            url: 'php/grid_data.php',
            method: 'POST',
            timeout: 100000,
            params: {
                tipo : 'editgroup',
                idtmail: idtmail
            },
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Error Loading');
            },
            success:function(response){
                Ext.QuickTips.init();
                var variable = Ext.decode(response.responseText);
  var storeMenu2= new Ext.data.JsonStore({
                    url:'php/comboGrupo.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen','caption']
                });

    var storeUser2= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
                });

                var form = new Ext.form.FormPanel({
                    baseCls: 'x-plain',
                    labelWidth: 55,
                    items: [{ xtype:'hidden',
                            id: 'typemenu4c',
                            name: 'typemenu4c',
                            value: variable.menu,
                            hiddenName: 'typemenu4c',
                        },{ xtype:'hidden',
                            id: 'typeuser4c',
                            name: 'typeuser4c',
                            value: variable.usergrupo,
                            hiddenName: 'typeuser4c',
                        }, {
                                        xtype:'combo',
                                        store:storeMenu2,
                                        fieldLabel:'Type',
                                        id:'typemenu4',
                                        name:'typemenu4',
                                        hiddenName: 'typemenu4',
                                        valueField: 'idmen',
                                        displayField: 'caption',
                                        triggerAction: 'all',
                                        emptyText:'Select a menu',
                                        allowBlank: false,
                                        width:200,
                                        value: variable.grupo
                                    },

                   {
                                        xtype:'combo',
                                        store:storeUser2,
                                        fieldLabel:'User',
                                        id:'typeuser4',
                                        name:'typeuser4',
                                        hiddenName: 'typeuser4',
                                        valueField: 'idmen2',
                                        displayField: 'caption2',
                                        triggerAction: 'all',
                                        emptyText:'Select a user',
                                        allowBlank: false,
                                        width:200,
                                        value: variable.user

                                    }],
                    buttonAlign: 'center',
                    buttons: [{
                        text: '<b>Update User Group</b>',
                        cls: 'x-btn-text-icon',
                        icon: 'images/disk.png',
                        formBind: true,
                        handler: function(b){
                            var form = b.findParentByType('form');
                            //form.getForm().fileUpload = true;
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'php/grid_edit.php',
                                    waitTitle   :'Please wait!',
                                    waitMsg     :'Loading...',
                                    params: {
                                        tipo : 'editgrupo',
                                        idtmail: idtmail
                                    },
                                    timeout: 100000,
                                    method :'POST',
                                    success: function(form, action) {
                                        w.close();
                                        var store1 = Ext.getCmp("gridpanel").getStore();
                                        store1.reload();
                                        Ext.Msg.alert('Success', action.result.msg);

                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error', action.result.msg);
                                    }
                                });
                            }
                        }
                    },'->',{
                        text: 'Close',
                        cls: 'x-btn-text-icon',
                        icon: 'images/cross.gif',
                        handler: function(b){
                            w.close();
                        }
                    }]
                });


                var w = new Ext.Window({
                    title: 'Update User Group',
                    width: 300,
                    height: 150,
                    layout: 'fit',
                    plain: true,
                    bodyStyle: 'padding:10px;',
                    items: form
                });
                w.show();
                w.addListener("beforeshow",function(w){
                    form.getForm().reset();
                });
            }
        })
    }


    function execToggleCheck(node, isCheck){
      if(node) {
       //node.expand();
       node.cascade(function(){
         if (this.attributes.cls=="file") {
           this.ui.toggleCheck(isCheck);
           this.attributes.checked=isCheck;
         }
       });
      }
     }


//////////////FIN ADD CHILDS ///////////////////////////////


Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
    var win;
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////


///////////Cargas de data dinamica///////////////
    var store = new Ext.data.JsonStore({
        totalProperty: 'total',
        root: 'results',
        url: 'php/grid_data_yan_menu.php?tipo=groupuser',
        fields: [
            {name: 'idg', type: 'int'}
         //{name: 'idworkshop', type: 'int'}
            ,'userid'
            ,'nombre'
            ,'grupo'
            ,'gruponame'

        ]
    });
    var storeMenu= new Ext.data.JsonStore({
                    url:'php/comboGrupo.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen','caption']
                });

    var storeUser= new Ext.data.JsonStore({
                    url:'php/comboUser.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen2','caption2']
                });

     var storeMenu3= new Ext.data.JsonStore({
                    url:'php/comboMenu.php',
                    root: 'data',
                    totalProperty: 'num',
                    fields: ['idmen','caption']
                });
//          ,'procode'
///////////FIN Cargas de data dinamica///////////////

////////////////barra de pagineo//////////////////////
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 100,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        items:[{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/add.png',
            id: 'add_button',
            text: 'Add',
            tooltip: 'Click to Add User Group',
            handler: AddMenu,


        },{
            iconCls:'icon',
            cls: 'x-btn-text-icon',
            icon: 'images/delete.gif',
            id: 'del_butt',
            tooltip: 'Click to Delete User of Group',
            text: 'Delete',
            handler: doDel
        },{
                                        xtype:'combo',
                                        store:storeMenu,

                                        fieldLabel:'logstatistic',
                                        id:'typemenu2',
                                        name:'typemenu2',
                                        hiddenName: 'typemenu3',
                                        valueField: 'idmen',
                                        displayField: 'caption',
                                        triggerAction: 'all',
                                        emptyText:'Select a group',
                                        allowBlank: false,
                                        width:200,
                                        listeners: {'select': logstatistic}
                                    },{
                xtype: 'button',
                width:80,
                id:'searchc',
                pressed: true,
                enableToggle: true,
                name:'searchc',
                text:'&nbsp;<b>Search</b>',
                handler: logstatistic
            }]
   });


////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
    function obtenerSeleccionados(){
        var selec = grid.selModel.getSelections();
        var i=0;
        var marcados='(';
        for(i=0; i<selec.length; i++){
            if(i>0) marcados+=',';
            marcados+=selec[i].json.idg;
         //marcados+=selec[i].json.idworkshop;
            //alert(marcados)
        }
        marcados+=')';
        if(i==0)marcados=false;
        return marcados;
    }
function logstatistic(){
store.setBaseParam('idmen',Ext.getCmp("typemenu2").getValue());

        store.load({params:{start:0, limit:100}});
    }



        function doDel(){
        Ext.MessageBox.confirm('Delete User Group','Are you sure delete row?.',//
            function(btn,text)
            {
                if(btn=='yes')
                {
                    var obtenidos=obtenerSeleccionados();

                    if(!obtenidos){

                        Ext.MessageBox.show({
                            title: 'Warning',
                            msg: 'You must select a row, by clicking on it, for the delete to work.',
                            buttons: Ext.MessageBox.OK,
                            icon:Ext.MessageBox.ERROR
                        });
                        obtenerTotal();
                        return;
                    }
                    ////return;
                    obtenidos=obtenidos.replace('(','');
                    obtenidos=obtenidos.replace(')','');
                    //alert(obtenidos);
                    Ext.Ajax.request({
                        waitMsg: 'Saving changes...',
                        url: 'php/grid_del.php',
                        method: 'POST',
                        params: {
                            tipo : 'deletegroup',
                            idmen: obtenidos
                        },

                        failure:function(response,options){
                            Ext.MessageBox.alert('Warning','Error editing');
                            store.reload();
                        },

                        success:function(response,options){

                            store.reload();
                        }
                    });
                }
            }
        )
    }
//Inicio funcion Menu

    function AddMenu()
    {

        var formul = new Ext.FormPanel({
            url:'php/grid_add.php',
            frame:true,
            layout: 'form',
            border:false,
            items:
               [{
                  xtype: 'fieldset',
                  autoHeight: true,
                  border: false,
                  items:
                  [{ xtype:'hidden',
                            id: 'txtstatus',
                            name: 'txtstatus',
                            value: 'Assistant1Active'
                        }, {
                                        xtype:'combo',
                                        store:storeMenu,
                                        fieldLabel:'Type',
                                        id:'typemenu',
                                        name:'typemenu',
                                        hiddenName: 'typemenu',
                                        valueField: 'idmen',
                                        displayField: 'caption',
                                        triggerAction: 'all',
                                        emptyText:'Select a group',
                                        allowBlank: false,
                                        width:200
                                        //listeners: {'select': showmenu}
                                    },

                   {
                                        xtype:'combo',
                                        store:storeUser,
                                        fieldLabel:'User',
                                        id:'typeuser',
                                        name:'typeuser',
                                        hiddenName: 'typeuser',
                                        valueField: 'idmen2',
                                        displayField: 'caption2',
                                        triggerAction: 'all',
                                        emptyText:'Select a user',
                                        allowBlank: false,
                                        width:200
                                        //listeners: {'select': showmenu}
                                    }

                    ]
                  //Assistant1Active
               }]
           ,
            buttons: [{

                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'images/cross.gif',
                handler  : function(){
                    wind.close();
               }
            },{
                text: 'Save',
                cls: 'x-btn-text-icon',
                icon: 'images/disk.png',
            formBind: true,
                handler  : function(){
                    if(formul.getForm().isValid())
                    {
                        formul.getForm().submit({
                            method: 'POST',
                            params: {
                                tipo : 'managegrupo'
                            },
                            waitTitle: 'Please wait..',
                      waitMsg: 'Sending data...',
                            success: function(form, action) {
                                store.reload();
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Success", obj.msg);
                                wind.close();
                            },
                            failure: function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);
                                Ext.Msg.alert("Failure", obj.msg);
                            }
                        });
                    }
                }
            }]
        });
        var wind = new Ext.Window({
                title: 'New User Group',
                iconCls: 'x-icon-templates',
                layout      : 'fit',
                width       : 400,
                height      : 150,
                resizable   : false,
                modal       : true,
                plain       : true,
                items       : formul
            });

        wind.show();
        wind.addListener("beforeshow",function(wind){
                formul.getForm().reset();
        });
    }

//Fin Funcion Menu



    function searchActive(){
        if(Ext.getCmp("cbActive").getValue()==1){
            Ext.MessageBox.show({
                title: 'Warning',
                msg: 'If you activate this workshop, this is the one that will appear on the webpage',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            return;
        }
        return;
    }
//////////////////FIN Manejo de Eventos//////////////////////

///////////////////renders/////////////////////////
function renderpreviewmenu(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="previewHtmlMenu({0})" ><img src="images/editar.png" border="0" ></a>',record.data.idg);
    }
    /*function renderTopic(value, p, record){
        return String.format('<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="editParents({0})"><img src="images/editar.png" border="0" ></a>',value);
    }*/

    function viewchild(val, p, record){
        if(record.data.edit=='N') return '';

        return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the Menu." onclick="AddMenu2()"><img src="images/editar.png" border="0" ></a>';
        //return '<a href="javascript:void(0)" class="itemusers" title="Click to edit the information." onclick="editParents('+record.data.idworkshop+')"><img src="images/editar.png" border="0" ></a>';
    }

    function viewnstatus(val, p, record){
        return  (val==1?'<img src="images/check.png" border="0" title="Active workshop currently " alt="Active workshop currently "/>':'');
    }
    function viewlink(val, p, record){

        return  '<a title="View map" class="itemusers" href="'+record.data.linkmap+'" target="_blank">'+val+'</a>';
    }
///////////////////FIN renders//////////////////////

/////////////////Grid//////////////////////////////////
    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
    var grid = new Ext.grid.EditorGridPanel({
        title:wherepage,
        id:"gridpanel",
        store: store,
        iconCls: 'icon-grid',
        columns: [
            new Ext.grid.RowNumberer()
            ,mySelectionModel
            ,{header: '', width: 40, align: 'center', dataIndex: 'view', renderer: renderpreviewmenu}//
         ,{header: 'ID', width: 40, sortable: true, align: 'center', dataIndex: 'idg' }
            ,{header: 'Iduser', width: 70, sortable: true, align: 'left', dataIndex: 'userid'}
            ,{header: 'Nombre', width: 200, align: 'left', sortable: true, dataIndex: 'nombre'}
            ,{header: 'Group', width: 200, align: 'left', sortable: true, dataIndex: 'gruponame'}

        ],
        clicksToEdit:2,
        height:550,
        sm: mySelectionModel,
        width: screen.width,//'99.8%',
        frame:true,
        loadMask:true,
        tbar: pagingBar
    });
//          ,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
    var pag = new Ext.Viewport({
        layout: 'border',
        hideBorders: true,
        monitorResize: true,
        items: [{
            region: 'north',
            height: 25,
            items: Ext.getCmp('menu_page')
        },{
            region:'center',
            autoHeight: true,
            items: grid
        }]
    });
//////////////FIN VIEWPORT////////////////////////////////

//////////////////Listener///////////////////////////

//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
    store.load({params:{start:0, limit:100}});
/////////////FIN Inicializar Grid////////////////////

});
