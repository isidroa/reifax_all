// JavaScript Document
Ext.ns('monitorig');

monitorig={
	init: function ()
	{ 
	
		/******************************************
		********* CONFIGUARCIONES GENERALES DEL GRID
		*******************************************/
		// declaro el lector para el drid
		monitorig.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total', 
			id: 'id'},
			monitorig.Record 
			);
		// declaro el store del grid
		monitorig.dataStore = new Ext.data.Store({  
			id: 'reports',  
			proxy: monitorig.dataProxy,  
			baseParams: {
				accion	:'listadoConexiones'
			},
			remoteSort:true,
			reader: monitorig.gridReader  
		});
		// declaro la barra para el pagino y los filtros
		monitorig.pager = new Ext.PagingToolbar({  
			store: monitorig.dataStore, 
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Visitas',  
			emptyMsg: 'No hay Visitas Disponibles',  
			pageSize: 100  
		});
		
		
		
		// declaro las columnas del grid
		monitorig.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
			{  
				header: 'Id Session',  
				dataIndex: 'id_registerexternal', 
				sortable: true,  
				width: 60  
			},{  
				header: 'Ip',  
				dataIndex: 'ip_address', 
				sortable: true,  
				width: 70  
			},{  
				header: 'Procode',  
				sortable: true, 
				dataIndex: 'procode',  
				width: 50,
				renderer: function (value)
				{
					return (value=='null')?'':value;
				} 
			},{  
				header: 'Userid',
				sortable: true, 
				dataIndex: 'userid',  
				width: 50  
			},{  
				header: 'Country',
				sortable: true, 
				dataIndex: 'country',  
				width: 50  
			},{  
				header: 'State',
				sortable: true, 
				dataIndex: 'state',  
				width: 50  
			},{  
				header: 'City',
				sortable: true, 
				dataIndex: 'city',  
				width: 60  
			},{  
				header: 'Date',  
				dataIndex: 'date', 
				sortable: true,  
				width: 100,  
				renderer: Ext.util.Format.dateRenderer('Y/m/d   H:i:s')  
			},{   
				header: 'Page Views',
				sortable: true, 
				dataIndex: 'page',  
				width: 30  
			},{   
				header: 'Accessed',
				sortable: true, 
				dataIndex: 'accessed',  
				width: 300  
			},{   
				header: 'References',
				sortable: true, 
				dataIndex: 'references',  
				width: 300,
				renderer: function (value)
				{
					if(value=='')
						return 'direct entry';
					else
						return '<a href="'+value+'" target="_blank">'+value+'</a>'
				} 
			}
			]  
		);
		// declaro el grid
		monitorig.gridStatistics = new Ext.grid.GridPanel({  
			id: 'list_report',
			store: monitorig.dataStore,  
		    bbar: monitorig.pager,   
			cm: monitorig.columnMode,
				viewConfig: {
					forceFit:true
				},
			enableColLock:false,
			//autoHeight : true,
			loadMask: true,
			border :false,
			layout	: 'fit',
			height	: 430,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
			listeners: {
				'rowcontextmenu' : monitorig.menuText,
				'rowdblclick' : monitorig.showDeatils
			}
		});
		// cargo el store
		monitorig.dataStore.load();
		
		// declaro el toolbar
		monitorig.toolbar=new Ext.form.FormPanel({
        frame : true,
		border :false,
        items : [{
					xtype: 'fieldset',
					style:{
						background : '#FFF'
						},
					title: 'Search',
					layout:'column',
					items: [{
							layout: 'form',
							columnWidth: .33,
							items:[{  
								fieldLabel : 'Promocode',  
								xtype	: 'textfield',  
								name 	: 'procode',
								width 	: 60  
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[{
								fieldLabel : 'Userid',  
								xtype	: 'textfield',  
								name 	: 'userid', 
								width 	: 60  
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[{  
								fieldLabel : 'IP',  
								xtype	: 'textfield',  
								name 	: 'ip', 
								width 	: 120 
							}]
						},{ 
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel : 'Min Date',  
								xtype	: 'datefield',  
								name 	: 'mindate',
								format	: 'Y-m-d',
								width 	: 120
							}]
						},{
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel : 'Max Date',  
								xtype	: 'datefield',  
								format	: 'Y-m-d',
								name 	: 'maxdate', 
								width 	: 120 
							}]
						},
						{
							layout: 'form',
							columnWidth: .33,
							items:[{   
								fieldLabel	: 'Show user',
								xtype	: 'radiogroup',
								//name	: 'mostrar',
								width	: 250,
								columns	: 3, //muestra los radiobuttons en dos columnas  
								 items: [  
									  {boxLabel: 'All', width: 50, name: 'mostrar', inputValue: 'all', checked: true},  
									  {boxLabel: 'Registered', name: 'mostrar', inputValue: 'registrados'},  
									  {boxLabel: 'Other', name: 'mostrar', inputValue: 'otros'}
								 ]
							}]
						}]
		}],
		buttons:[{
				xtype 	: 'button',
				text	: 'filter',
				handler	: monitorig.filter
			},
			{
				xtype 	: 'button',
				text	: 'Reset',
				handler	: monitorig.reset
			}]
		})
		monitorig.panel=new Ext.Panel({
			items:[monitorig.toolbar,monitorig.gridStatistics]
			});
		// declaro la vista donde se va a mostrar el grid
		monitorig.pag = new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: monitorig.panel,
					autoHeight: true
			}]
		});
		
		monitorig.pager.on('beforechange',function(bar,params){  
				params.procode=monitorig.toolbar.getForm().findField("procode").getValue();
				params.userid=monitorig.toolbar.getForm().findField("userid").getValue();
				params.maxdate=monitorig.toolbar.getForm().findField("maxdate").getValue();
				params.mindate=monitorig.toolbar.getForm().findField("mindate").getValue();
				params.mostrar=monitorig.toolbar.getForm().getValues()['mostrar'];
		}); 
	},
	// declaro el record para los datos q se van a tomar de la consulta
	Record : new Ext.data.Record.create([ 
			{name: 'ip_address', type: 'string'},
			{name: 'procode', type: 'string'},    
			{name: 'userid', type: 'string'},
			{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'accessed', type: 'string'},
			{name: 'references', type: 'string'},
			{name: 'country' , type: 'string'},
			{name: 'state' , type: 'string'},
			{name: 'city' , type: 'string'},
			{name: 'page' , type: 'int'},
			{name: 'id_registerexternal', type :'int'}
	]),
	// declaro el proxy que voy a usar en la consulta
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesMonitoring.php',   // Servicio web  
			method: 'POST'                          // Método de envío  
	}),
	// filtrar las conexiones
	filter: function ()
	{
		monitorig.dataStore.load({
			params:{
				procode:monitorig.toolbar.getForm().findField("procode").getValue(),
				userid:monitorig.toolbar.getForm().findField("userid").getValue(),
				ip:monitorig.toolbar.getForm().findField("ip").getValue(),
				maxdate:monitorig.toolbar.getForm().findField("maxdate").getValue(),
				mindate:monitorig.toolbar.getForm().findField("mindate").getValue(),
				mostrar:monitorig.toolbar.getForm().getValues()['mostrar'],
				start:0,
				limit:100
			}
		});
	},
	reset:function ()
	{
		monitorig.toolbar.getForm().reset();
		monitorig.dataStore.load({
			params:{
				start:0,
				limit:100
			}
		});

	}
	,
	// menu emergente
	menuText: function (grid, index, event) {
      event.stopEvent();
      var record = grid.getStore().getAt(index);
      var menu = new Ext.menu.Menu({
            items: [{
                text: 'Show History Session',
                handler: function() {
					var detailWin=
					{
						title 	: 'History for the Session: '+record.get('id_registerexternal'),
						userid	: record.get('userid'),
						county	: record.get('County')
					}
					var config=
					{
						session:record.get('id_registerexternal'),
						listFull:true
					}
					monitorig.viewDetail(config,detailWin) 
                }
            },{
                text: 'Show History User',
                handler: function() {
					if(record.get('userid'))
					{
						var detailWin=
						{
							title 	: 'History for the user: '+record.get('userid'),
							userid	: record.get('userid'),
							county	: record.get('County')
						}
						var config=
						{
							userid:record.get('userid'),
							listFull:true
						}
						monitorig.viewDetail(config,detailWin) 
					}
					else
					{
						Ext.Msg.alert('Error', 'There is no user assigned to this connection');
					}
                }
            },{
                text: 'Show History IP Address',
                handler: function() {
					var detailWin=
						{
							title 	: 'History for the ip: '+record.get('ip_address'),
							userid	: record.get('userid'),
							county	: record.get('County')
						}
					var config=
					{
						ip:record.get('ip_address'),
						listFull:true
					}
					monitorig.viewDetail(config,detailWin) 
                }            
			}]
        }).showAt(event.xy);
	},
	// menu emergente
	showDeatils: function (grid,index, event) {
     	var record = grid.getStore().getAt(index);
		var detailWin=
			{
				title 	: 'History for the Session: '+record.get('id_registerexternal'),
				userid	: record.get('userid'),
				county	: record.get('County')
			}
		var config=
			{
				session	:record.get('id_registerexternal'),
				listFull:true
			}
		monitorig.viewDetail(config,detailWin)  
	},
	/******************
	*	funcion para ver los detalles de la vista
	*******************/
	viewDetail: function (config,detailWin)
	{
		
		config.accion='listadoConexiones';
		// declaro el lector para el grid
		var gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total', 
			id: 'id'},
			monitorig.Record 
			);
		// declaro el store del grid
		var dataStore = new Ext.data.Store({ 
			proxy: monitorig.dataProxy,  
			baseParams: config,  
			reader: gridReader,
			groupField:'id_registerexternal' 
		});
		// declaro las columnas del grid
		var columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
			{  
				header: 'Date',  
				dataIndex: 'date', 
				sortable: true,  
				width: 140,  
				renderer: Ext.util.Format.dateRenderer('Y/m/d  H:i:s')  
			},{   
				header: 'Accessed',
				sortable: true, 
				dataIndex: 'accessed',  
				width: 300  
			},{   
				header: 'References',
				sortable: true, 
				dataIndex: 'references',  
				width: 300
			}
			]  
		);
		
		var estadisticas = new Ext.grid.GridPanel({
			store: dataStore, 
			cm: columnMode,
				viewConfig: {
					forceFit:true
				},
			enableColLock:false,
			loadMask: true,
			border :false,
			layout	: 'fit',
			height	: 380,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false})

		});
		
		dataStore.load();
		var content = new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region:'center',
					items: estadisticas,
					autoHeight: true
			}]
		});
		
		var miventana = new Ext.Window({
			title: detailWin.title,
			width: 700,
			items	:[estadisticas],
			resizable: false
		}).show()
	}
/////////////
//	FIN DEL OBJETO
////////////
}



Ext.onReady(monitorig.init);
	
	
	
	