Ext.ns('tickets')
tickets={
	init :function (){
		tickets.gridReader = new Ext.data.JsonReader({  
			root: 'data', 
			totalProperty: 'total',
			id:'readerCoaching'
			},
			tickets.record 
		);
		tickets.dataStore = new Ext.data.Store({
			proxy: tickets.dataProxy,  
			baseParams: {
				opcion	:'listarTickets',
				typeBack :'Tickets'
			},
			remoteSort:true,
			reader: tickets.gridReader  
		});
		tickets.pager = new Ext.PagingToolbar({  
			store: tickets.dataStore,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[ 
				'|'
				,
				{
					xtype	:'button',
					iconCls	:'x-icon-add',
					handler	: tickets.new.init,
					text	: 'New Ticket'
				}
				, 
				{
					xtype	:'button',
					iconCls	:'x-icon-cancel',
					handler	: function (){
						ticket=tickets.grid.getSelectionModel().getSelected()
						Ext.Msg.confirm('Confirm Delete?',
						'Confirm deletion of Ticket: '+ticket.data.ticket,
						function (opt){
								if(opt == 'yes'){
									tickets.loadMask.show();
									Ext.Ajax.request({
									   url: 'php/funcionesTickets.php',
									   success: function (result){
											tickets.loadMask.hide();
											Ext.Msg.alert('Info', 'Deleted successfully'); 
											Ext.getCmp('list_Tickets').getStore().reload();
										},
									   failure: function (result){
											tickets.loadMask.hide();
											Ext.Msg.alert('Info', 'Error al borrar el ticket. intentelo nuevamente');
										},
									   params: 
										{ 
											id : ticket.data.idcs,
											opcion :'deleteTicket'
										}
									})
								}
							}
						);
						//////console.debug(tickets.grid.getSelectionModel().getSelected())
					},
					text	: 'Delete Ticket'
				},
				{
					xtype	:'button',
					icon: 'images/excel.png',
					text	: 'Export to Excel',
					handler	: function (){
						var grid=tickets.grid;
						grid.el.mask('Exporting...', 'x-mask-loading');
						
						Ext.Ajax.request({
							method :'POST',
							url: 'Excel/xlsTickets.php',
							success: function ( result, request ){
								grid.el.unmask();  
								window.open('Excel/d.php?nombre='+result.responseText,'','width=50,height=50');
								return(true); 
							},
							failure:  function (){
							   grid.el.unmask();  
							},
							 params: { 
								parametro: 'Export_grid',
								status:Ext.getCmp('filterTicketsStatus').getValue(),
								error:Ext.getCmp('filterTicketsError').getValue(),
								programer:Ext.getCmp('filterTicketsProgramer').getValue()
							}
						});
						
					}
				},
				'|',
				/*{
					xtype:'form',
					id:'filterTickets',
					width: 620,
					layout :'column',
					padding: 3,
					style	:{
						background:'none'
					},
					border: false,
					items:[{
							layout: 'form',
							columnWidth: .24,
							labelWidth: 45,
							border: false,
							items:[*/{
								xtype	: 'combo',
								fieldLabel:'Status'
								,displayField:'name'
								,value :'not in ("Close","Hold")'
								,id:'filterTicketsStatus'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['','All'],
										['not in ("Close","Hold")', 'Active'],
										['in ("Close")', 'Inactive'],
										['in ("Hold")', 'Hold']
									]
								})
								,width: 70
								,triggerAction:'all'
								,mode:'local'
							},{
					xtype	: 'textfield',
					name	: 'name',
					id		: 'filterTicketsProgramer',
					emptyText	:'Ticket, Email, Name, Userid'
				}/*]
					},{
							layout: 'form',
							columnWidth: .44,
							labelWidth: 58,
							border: false,
							items:[*/
								// tickets.combo.programmer('All','filterTicketsProgramer')
								/*]
					},{
							layout: 'form',
							columnWidth: .32,
							labelWidth: 45,
							border: false,
							items:[*/,{
								xtype	: 'combo',
								fieldLabel:'Issues'
								,displayField:'name'
								,id:'filterTicketsError'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['', 'All'],
										['Billing/ Credit Card', 'Billing/ Credit Card'],
										['Cancellation','Cancellation'],
										['Contracts', 'Contracts'],
										['Data', 'Data'],
										['Downgrading Account', 'Downgrading Account'],
										['General Question','General Question'],
										['Refund','Refund'],
										['REIFax Promotion Inquiry', 'Promotion Inquiry'],
										['REIFax Training','Training'],
										['Suggestions', 'Suggestions'],
										['Technical/ System Issue', 'Technical/ System Issue'],
										['Upgrading Account','Upgrading Account'],
										['Other','Other']
									]
								})
								,width: 130
								,triggerAction:'all'
								,mode:'local'
							}/*]
					}
					]
				}*/,
				{
					xtype	: 'button',
					text	: 'filter',
					iconCls	: 'x-icon-filter',
					handler	: function (){
						tickets.dataStore.load({
							params:{
								status:Ext.getCmp('filterTicketsStatus').getValue(),
								error:Ext.getCmp('filterTicketsError').getValue(),
								programer:Ext.getCmp('filterTicketsProgramer').getValue()
							}
						})
					}
				},
				{
					xtype	: 'button',
					text	: 'Remove filter',
					handler	: function (){
						Ext.getCmp('filterTicketsStatus').reset( );
						Ext.getCmp('filterTicketsError').reset( );
						Ext.getCmp('filterTicketsProgramer').reset( );
						tickets.dataStore.load({
							params:{
								status:Ext.getCmp('filterTicketsStatus').getValue(),
								error:Ext.getCmp('filterTicketsError').getValue(),
								programer:Ext.getCmp('filterTicketsProgramer').getValue()
							}
						})
					}
				}
			]  
		});
		// declaro las columnas del grid
		tickets.columnMode = new Ext.grid.ColumnModel(  
			[
				new Ext.grid.RowNumberer(),
			{  
				header: 'Ticket #',  
				dataIndex: 'ticket', 
				sortable: true,  
				width: 40  
			},{  
				header: 'Userid',  
				dataIndex: 'useridclient', 
				sortable: true,  
				width: 40,
				renderer :function (value, p, record){
					return String.format(
							'<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers({0})">{0}</a>',value);
				}	
			},{  
				header: 'Name',  
				sortable: true, 
				dataIndex: 'cliente'
			},
			/*
				conteo de los dias
			*/
			{  
				header: 'Days',
				sortable: true, 
				width: 35,
				renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						var now		= (record.get('status')=='Close' || record.get('status')=='Hold')?record.get('datec'):new Date();
						var diferece= now - record.get('datel');
						var dias 	= Math.floor(diferece / (1000 * 60 * 60 * 24));
						dias=dias<0?0:dias;
						if(dias<6 || (record.get('date_check') > new Date()))
	      					metaData.attr ='style="background-color:#4BB148; color: #FFF; font-weight:bold;"';
						else if(dias<11)
	      					metaData.attr ='style="background-color:#FCD209; font-weight:bold;"'; 
						else 
	      					metaData.attr ='style="background-color:#E04828; color: #FFF; font-weight:bold;"'; 
					return dias; 
   				},
				dataIndex: 'datel'
			},{  
				header: 'Customer',  
				sortable: true, 
				width: 80,
				dataIndex: 'customer'
			},{  
				header: 'Programmer',
				sortable: true,  
				width: 80,
				dataIndex: 'programador'
			},{  
				header: 'Status',
				sortable: true,  
				width: 50,
				dataIndex: 'status'
			},{  
				header: 'Issues',
				sortable: true, 
				width: 60,
				dataIndex: 'errortype'
			},{  
				header: 'Issues Description',
				sortable: true, 
				width: 150,
				renderer: tickets.tooltip  ,
				dataIndex: 'errordescription'
			},{  
				header: 'Product',
				sortable: true, 
				width: 100,
				dataIndex: 'productName'
			},{  
				header: 'Date added',
				sortable: true, 
				width: 70,
				/*renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						var now		= new Date();
						var diferece= now - record.get('datel');
						var dias 	= Math.floor(diferece / (1000 * 60 * 60 * 24));
						if(dias<6)
	      					metaData.attr ='style="background-color:#4BB148; color: #FFF; font-weight:bold;"'; 
						else if(dias<11)
	      					metaData.attr ='style="background-color:#FCD209; font-weight:bold;"'; 
						else 
	      					metaData.attr ='style="background-color:#E04828; color: #FFF; font-weight:bold;"'; 
					return value; 
   				},*/
				dataIndex: 'datel'
			},/*{  
				header: 'Process start date',
				sortable: true, 
				dataIndex: 'dated'
			},{  
				header: 'Fecha final',
				sortable: true, 
				dataIndex: 'datef'
			},*/{  
				header: 'Date Notified',
				sortable: true, 
				width: 70,
				dataIndex: 'datec'
			},{  
				header: 'Check',
				sortable: true, 
				width: 70,
				dataIndex: 'date_check'
			}/*,{  
				header: 'OS',
				sortable: true, 
				dataIndex: 'os',  
				width: 50  
			},{  
				header: 'Browser',
				sortable: true, 
				dataIndex: 'browser',  
				width: 50  
			},{  
				header: 'State',
				sortable: true, 
				dataIndex: 'state',  
				width: 60  
			},{  
				header: 'County',  
				dataIndex: 'idcounty', 
				sortable: true,  
				width: 100
			},{   
				header: 'Data',
				sortable: true, 
				dataIndex: 'menutab',  
				width: 30  
			},{   
				header: 'Sub Menutab',
				sortable: true, 
				dataIndex: 'submenutab',  
				width: 300  
			},{   
				header: 'Protype',
				sortable: true, 
				dataIndex: 'protype',  
				width: 300
			}*/
			]  
		);
		////console.debug(expander);
		tickets.grid = new Ext.grid.GridPanel({  
			id: 'list_Tickets',
			store: tickets.dataStore,  
		    tbar: tickets.pager,   
			cm: tickets.columnMode,
				viewConfig: {
					forceFit:true
				},
			enableColLock:false,
			view: new Ext.grid.GridView({ 
				forceFit: true, 
				getRowClass : function (row, index) { 
						var cls = ''; 
						var col = row.data;
						var check=new Date(col.date_check);
						var hoy=new Date()
						if(col.msg_cliente!='')
						{
							cls = 'red-row'
						}
						else if(col.msg_system!='')
						{
							cls = 'green-row'
						}
						else if(check.getDate()==hoy.getDate() && check.getMonth()==hoy.getMonth() && check.getFullYear()==hoy.getFullYear() )
						{
							cls = 'orange-row'
						}
						
						 return cls; 
					  } 
		   }),  //end gridView
			loadMask: true,
			border :false,
			layout	: 'fit',
			height:Ext.getBody().getViewSize().height-50,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
			listeners: {
			//	'rowcontextmenu' : monitorig.menuText,
				'rowdblclick' : tickets.showDeatils
			}
		});
		tickets.marco= new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			monitorResize: true,
			items: [
				{
					region: 'north',
					height: 25,
					items: Ext.getCmp('menu_page')
				},
				{
					region:'center',
					items: tickets.grid,
					autoHeight: true
			}]
		});
		
		tickets.pager.on('beforechange',function(bar,params){
				params.status=Ext.getCmp('filterTicketsStatus').getValue();
				params.error=Ext.getCmp('filterTicketsError').getValue();
				params.programer=Ext.getCmp('filterTicketsProgramer').getValue();
		});
		tickets.dataStore.load({params:{
				status:'not in ("Close","Hold")'
			}});
		
		
	},
	new:{
		init :function (user) {
			if(tickets.new.win)
			{
				tickets.new.form.getForm().reset();
				tickets.new.win.show();
			}
			else
			{
				tickets.new.form = new Ext.form.FormPanel({
					fileUpload: true,
					frame : true,
					border :false,
					bodyStyle :{
						background : '#FFF'
					},
					padding: 10,
					layout:'column',
					items : [{
								layout: 'form',
								columnWidth: .50,
								items:[tickets.combo.user(tickets.userActive)]
							},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Source'
								,displayField:'name'
								,name:'source'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['Live Support', 'Live Support'],
										['Phone','Phone'],
										['Email', 'Email'],
										['Other','Other']
									]
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[tickets.combo.customerService()]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[tickets.combo.programmer()]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Issues'
								,displayField:'name'
								,name: 'errortype'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									 ,data:[
												['Billing/ Credit Card', 'Billing/ Credit Card'],
												['Cancellation','Cancellation'],
												['Contracts', 'Contracts'],
												['Data', 'Data'],
												['Downgrading Account', 'Downgrading Account'],
												['General Question','General Question'],
												['Refund','Refund'],
												['Credit','Credit'],
												['REIFax Promotion Inquiry', 'Promotion Inquiry'],
												['REIFax Training','Training'],
												['Suggestions', 'Suggestions'],
												['Technical/ System Issue', 'Technical/ System Issue'],
												['Upgrading Account','Upgrading Account'],
												['Other','Other']
									]
									
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Issues About'
								,displayField:'name'
								,name:'errortype2'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									 ,data:[
										['General', 'General'],
										['Specific','Specific']
									]
									
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[new Ext.form.DateField({
									name :'date_check',
									fieldLabel : 'Date Check',
									format :'Y-m-d',
									minValue: new Date()
								})]
						},{
							layout: 'form',
							columnWidth: 1,
							items:[{
								fieldLabel:'Issues Description',
								xtype	: 'textarea',
								width	: '560',
								name :'errordescription'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{   
								fieldLabel	: 'OS',
								xtype	: 'radiogroup',
								//name	: 'mostrar',
								//width	: 250,
								columns	: 3, //muestra los radiobuttons en dos columnas  
								 items: [  
									  {boxLabel: 'NONE', width: 50, name: 'os', inputValue: 'NONE', checked: true},  
									  {boxLabel: 'Windows', width: 50, name: 'os', inputValue: 'Windows'},  
									  {boxLabel: 'Mac', name: 'os', inputValue: 'Mac'},  
									  {boxLabel: 'Linux', name: 'os', inputValue: 'Other'},  
									  {boxLabel: 'Android', name: 'os', inputValue: 'Android'},  
									  {boxLabel: 'Other', name: 'os', inputValue: 'Linux'}
								 ]
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'State'
								,displayField:'name'
								,valueField:'id'
								,value: 'FL'
								,name: 'state'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:Ext.combos_selec.states
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'County'
								,displayField:'name'
								,valueField:'id'
								,hiddenName: 'idcounty'
								//,name :'idcounty'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:Ext.combos_selec.cbCounty
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{   
								fieldLabel	: 'Browser',
								xtype	: 'radiogroup',
								//name	: 'mostrar',
								//width	: 250,
								columns	: 3, //muestra los radiobuttons en dos columnas  
								 items: [  
									  {boxLabel: 'NONE', width: 50, name: 'browser', inputValue: 'NONE', checked: true},  
									  {boxLabel: 'IE6', width: 50, name: 'browser', inputValue: 'IE6'},  
									  {boxLabel: 'IE7', name: 'browser', inputValue: 'IE7'},  
									  {boxLabel: 'IE8', name: 'browser', inputValue: 'IE8'},  
									  {boxLabel: 'IE9', name: 'browser', inputValue: 'IE9'},  
									  {boxLabel: 'Firefox', name: 'browser', inputValue: 'Firefox'},  
									  {boxLabel: 'Chrome', name: 'browser', inputValue: 'Chrome'},  
									  {boxLabel: 'Safari', name: 'browser', inputValue: 'Safari'},  
									  {boxLabel: 'Opera', name: 'browser', inputValue: 'Opera'},  
									  {boxLabel: 'Other', name: 'browser', inputValue: 'Other'}
								 ]
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Product'
								,displayField:'name'
								,name :'product'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
									['1', 'REIFax Residential'],
									['2', 'REIFax Platinum'],
									['3', 'REIFax Professional'],
									['7', 'Home Deal Finder'],
									['6', 'Buyer´s Pro'],
									['8', 'Leads Generator']
								]
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Data'
								,displayField:'name'
								,valueField:'id'
								,name:'menutab'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
									['Public Records', 'Public Records'],
									['For Sale', 'For Sale'],
									['Foreclosures', 'Foreclosures'],
									['By Owner', 'By Owner'],
									['By Owner Rent', 'By Owner Rent']
								]
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Submenu Tab'
								,displayField:'name'
								,name :'submenutab'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
									['SEARCH', 'SEARCH'],
									['RESULTS', 'RESULTS'],
									['COMPARABLES', 'COMPARABLES'],
									['COMPARABLES ACTIVES', 'COMPARABLES ACTIVES'],
									['DISTRESSED', 'DISTRESSED'],
									['COMPARABLES RENTAL', 'COMPARABLES RENTAL'],
									['STATISTICS', 'STATISTICS'],
									['MY SHORT SALES', 'MY SHORT SALES'],
									['MY SAVED DOCS', 'MY SAVED DOCS']
								]
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: .50,
							items:[{
								xtype	: 'combo',
								fieldLabel:'Property Type'
								,displayField:'name'
								,name:'proptype'
								,valueField:'id'
								,store: new Ext.data.SimpleStore({
									 fields:['id', 'name']
									,data:[
										['Any Type', 'Any Type'],
										['Single Family', 'Single Family'],
										['Condo/Town/Villa', 'Condo/Town/Villa'],
										['Multi Family +10', 'Multi Family +10'],
										['Multi Family -10', 'Multi Family -10'],
										['Commercial', 'Commercial'],
										['Vacant Land', 'Vacant Land'],
										['Mobile Home', 'Mobile Home'],
										['Other', 'Other']
									]
								})
								,triggerAction:'all'
								,mode:'local'
							}]
						},{
							layout: 'form',
							columnWidth: 1,
							items:[{
								fieldLabel:'Search',
								xtype	: 'textarea',
								width	: '560',
								name:'search'
							}]
						},{
							layout: 'form',
							columnWidth: 0.5,
							items:[{
								fieldLabel:'Image 1',
								xtype	: 'textfield',
								width	: 200,
								inputType :'file',
								name:'img1'
								
							}]
						},{
							layout: 'form',
							columnWidth: 0.5,
							items:[{
								fieldLabel:'Image 2',
								xtype	: 'textfield',
								width	: 200,
								inputType :'file',
								name:'img2'
								
							}]
						}],
					buttons:[{
							xtype 	: 'button',
							text	: 'Save',
							handler	: function ()
							{
								if(tickets.new.form.getForm().isValid())
								{
									tickets.new.form.getForm().submit({
										waitTitle:'Recording',
										waitMsg :'It is processing the registration of the new ticket...',
										url: 'php/funcionesTickets.php',
										scope : this,
										params :{
											opcion : 'recordTicket'
										},
										success	: function (form,action)
										{
											tickets.new.form.getForm().reset();
											tickets.new.win.hide();
											tickets.dataStore.reload();
										}
									})
								}
							}
						},
						{
							xtype 	: 'button',
							text	: 'Reset',
							handler : function ()
							{
								tickets.new.form.getForm().reset();
							}
						}]
				})
				
				
				
				
				tickets.new.win= new Ext.Window({
					layout: 'fit', 
					title: 'New Ticket', 
					closable: true,
					closeAction: 'hide',
					resizable: false,
					maximizable: false,
					plain: true,
					border: false,
					width: 800,
					height: 540,
					items	:[
						tickets.new.form
					]
				});
				tickets.new.win.show();
			}
		}
	},
	record : new Ext.data.Record.create([ 
			{name: 'idcs', type: 'int'},
			{name: 'useridclient', type: 'int'}, 
			{name: 'cliente', type: 'string'},    
			{name: 'useridprogrammer', type: 'int'}, 
			{name: 'programador', type: 'string'},    
			{name: 'useridcustomer', type: 'int'},
			{name: 'mobile', type: 'string'},
			{name: 'id_product', type: 'int'},
			{name: 'productName', type: 'string'},
			{name: 'email', type: 'string'},
			{name: 'phone', type: 'string'},
			{name: 'customer', type: 'string'},
			{name: 'browser', type: 'string'},
			{name: 'os', type: 'string'},
			{name: 'state' , type: 'string'},
			{name: 'idcounty' , type: 'string'},
			{name: 'menutab' , type: 'string'},
			{name: 'submenutab' , type: 'string'},
			{name: 'proptype' , type: 'string'},
			{name: 'errortype' , type: 'string'},
			{name: 'search' , type: 'string'},
			{name: 'errordescription' , type: 'string'},
			{name: 'status' , type: 'string'},
			{name: 'datel', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'dated', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'datef', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'datec', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'date_check', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'errortype2', type: 'string'},
			{name: 'soldescription' , type: 'string'},
			{name: 'msg_cliente' , type: 'int'},
			{name: 'msg_system' , type: 'int'},
			{name: 'ticket' , type: 'string'},
			{name: 'source' , type: 'string'}
	]),
	dataProxy : new Ext.data.HttpProxy({  
			url: 'php/funcionesTickets.php',   // Servicio web  
			method: 'POST'                          // Método de envío  
	}),
	combo:{
		user: function (user)
			{
				var aux= new Ext.form.ComboBox({
					store: new Ext.data.SimpleStore({
						fields: ['userid', 'name'],
						data : Ext.combos_selec.dataUsers
					}),
					hiddenName: 'client', 
					valueField: 'userid',
					displayField:'name',
					fieldLabel:	'Client',
					//name:'client',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					value	:user,
					selectOnFocus:true
				})
				return aux;
		},
		customerService: function (valDefault)
			{
				Ext.combos_selec.dataUsersAdmin[0]=(valDefault!='')? new Array('', valDefault): new Array('', 'NONE') ;
				var aux= new Ext.form.ComboBox({
					store: new Ext.data.SimpleStore({
						fields: ['userid', 'name'],
						data : Ext.combos_selec.dataUsersAdmin
					}),
					//hiddenName: 'UD_executive', 
					valueField: 'userid',
					displayField:'name',
					fieldLabel:	'Attend',
					typeAhead: true,
					hiddenName:'customer',
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true
				})
				return aux
		},
		programmer:function (valDefault,id)
			{
				
				Ext.combos_selec.dataUsersAdmin[0]=(valDefault!='')? new Array('', valDefault): new Array('', 'NONE') ;
				var aux=  new Ext.form.ComboBox({
					store: new Ext.data.SimpleStore({
						fields: ['userid', 'name'],
						data :Ext.combos_selec.dataUsersAdmin
					}),
					//hiddenName: 'UD_executive', 
					valueField: 'userid',
					id:((id!='')?id:NULL),
					displayField:'name',
					hiddenName:'programer',
					fieldLabel:	'Programmer',
					typeAhead: true,
					triggerAction: 'all',
					mode: 'local',
					selectOnFocus:true
				})
				return aux;
			}
	},
	loadMask : new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."})
	,
	tooltip: function (value, metadata, record, rowIndex, colIndex, store)
	{
		metadata.attr = 'ext:qtip="' + value + '"';
    	return value;
	}
	,
	showDeatils: function (grid, rowIndex, columnIndex, e)
	{
		
		if(typeof grid=="object"){
			var record = grid.getStore().getAt(rowIndex);
		}
		else{
			var record=ticketsG.getStore().getAt(grid);
		}
		
		var programmer=tickets.combo.programmer();
		programmer.setValue(record.get('useridprogrammer'));
		
		var customerService=tickets.combo.customerService();
		customerService.setValue(record.get('useridcustomer'));
		
		
		
		Ext.Ajax.request({
			url: 'php/funcionesTickets.php',
			scope : this,
			params :{
				opcion : 'getTicket',
				id	:record.get('idcs')
			},
			success: function (data){
				//console.debug(data);
				data=Ext.util.JSON.decode(data.responseText);
				//console.debug(data);
				if(data.success){
					//console.debug(data.data.locked);
					
					if(data.data.locked==null){
						var _thats=this;
						//console.debug(data.data.idcs);
						var storeMsg = new Ext.data.JsonStore({ 
							id : 'store'+data.data.idcs,
							proxy : tickets.dataProxy,  
							baseParams: {
								opcion	:'listarMsg',
								id	: data.data.idcs
							},
							root: 'data',
							fields: [
								{ name: 'user', type: 'string' },
								{ name: 'msg', type: 'string' },
								{ name: 'dateMsg', type: 'date', dateFormat: 'Y-m-d H:i:s'}
							]
						});		
						storeMsg.load();
						var templeteMsg=new Ext.XTemplate(
							'<tpl for=".">',
								'<div class="msgUser"> <p class="title">{user} Says:</p>',
								'<p class="msgContent">{msg}</p>',
								'<div class="dateMsg">{dateMsg}</div></div>',
							'</tpl>',
							'<div class="x-clear"></div>'
						);

						var Msg = new Ext.DataView({
							id:'view'+data.data.idcs,
							autoScroll: true,
							store: storeMsg,
							border :false,
							tpl: templeteMsg,
							autoHeight: false,
							height: 200,
							multiSelect: false,
							overClass: 'x-view-over',
							itemSelector: 'div.thumb-wrap',
							emptyText: 'No Msg to display'
						});	
						var storeImg = new Ext.data.JsonStore({ 
							id : 'storeImg'+data.data.idcs,
							proxy : tickets.dataProxy,  
							baseParams: {
								opcion	:'listarImg',
								id	: record.get('idcs')
							},
							root: 'data',
							fields: [
								{ name: 'img', type: 'string' },
								{ name: 'id', type: 'int' }
							]
						});
						storeImg.load();
						var templeteImg=new Ext.XTemplate(
							'<tpl for=".">',
								'<div class="imgUser">',
								'<img src="{img}">',
								'<a href="#" class="removeImg" onClick="removeImg({id},\'viewImg'+data.data.idcs+'\')">X</a>',
								'</div>',
							'</tpl>',
							'<div class="x-clear"></div>'
						);
						var Img = new Ext.DataView({
							id:'viewImg'+data.data.idcs,
							autoScroll: true,
							store: storeImg,
							tpl: templeteImg,
							autoHeight: false,
							height: 200,
							width:'100%',
							multiSelect: true,
							overClass: 'x-view-over',
							itemSelector: 'div.imgUser',
							emptyText: 'No Images to display'
						});
						Img.on('dblclick', function(dview, index, node, e) {
							window.open(dview.getSelectedRecords()[0].data.img);
						});
						
						/*
						var fp = new Ext.FormPanel({
							fileUpload: true,
							width:'100%',
							frame: true,
							title: 'Add Image',
							autoHeight: true,
							bodyStyle: 'padding: 10px 10px 0 10px;',
							labelWidth: 50,
							defaults: {
								anchor: '95%',
								allowBlank: false,
								msgTarget: 'side'
							},
							items: [{
								xtype: 'fileuploadfield',
								id: 'form-file',
								emptyText: 'Select an image',
								fieldLabel: 'New',
								name: 'photo-path',
								buttonText: '',
								buttonCfg: {
									iconCls: 'upload-icon'
								}
							}]
						});
						*/
						
/**********************************
*************
***********************************/

						var form = new Ext.form.FormPanel({
											fileUpload: true,
											frame : true,
											border :false,
											id:'form'+data.data.idcs,
											bodyStyle :{
												background : '#FFF'
											},
											padding: 10,
											layout:'column',
											items : [
												{
													xtype: 'fieldset',
													padding:10,
													style:{
														background : '#FFF',
														font: '14px tahoma,arial,helvetica,sans-serif'
														},
													title: 'Contact Infomation',
													layout:'column',
													columnWidth: 1,
													items: [
														{
															layout: 'form',
															columnWidth: .16,
															items:[{
																xtype	:'label',
																html	: '<b>Userid: </b><span id="winUserid'+data.data.idcs+'"><a href="javascript:void(0)" class="itemusers" style="font-size:13px" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+data.data.useridclient+')">'+data.data.useridclient+'</a></span>' 
															}]
														},
														{
															layout: 'form',
															columnWidth: .39,
															items:[
															{
																xtype	:'label',
																html	: '<b>Client: </b><span id="winName'+data.data.idcs+'">'+data.data.cliente+'</span>  <a href="javascript:void(0)" class="itemusers" style="font-size:13px" title="Details Users. Click to see the information." onclick="changeUser('+data.data.idcs+')">Change</a>' 
															}]
														},
														{
															layout: 'form',
															columnWidth: 0.25,
															items:[{
																xtype	:'label',
																html	: '<b>Source: </b>'+data.data.source
															}]
														},
																			
																		{
																			layout: 'form',
																			columnWidth: 0.2,
																			items:[
																			{
																				xtype	:'label',
																				html	: '<b>Ticket: </b>'+data.data.ticket 
																			}]},
														{
															layout: 'form',
															columnWidth: .55,
															items:[{
																xtype	:'label',
																html	: '<b>Home Phone: </b><span id="winHomePho'+data.data.idcs+'">'+data.data.phone+'</span>'
															}]
														},
														{
															layout: 'form',
															columnWidth: .45,
															items:[{
																xtype	:'label',
																html	: '<b>Email: </b><span id="winEmail'+data.data.idcs+'">'+data.data.email+'</span>'
															}]
														},
														{
															layout: 'form',
															columnWidth: .33,
															items:[{
																xtype	:'label',
																html	: '<b>Mobile Phone: </b><span id="winMobilePho'+data.data.idcs+'">'+data.data.mobile+'</span>'
															}]
														}]
												},{
													xtype: 'fieldset',
													padding:10,
													style:{
														background : '#FFF',
														font: '13px tahoma,arial,helvetica,sans-serif'
														},
													title: 'Issues Infomation',
													layout:'column',
													columnWidth: 1,
													items: [
														{
															layout: 'form',
															columnWidth: .33,
															items:[{
																	xtype	: 'combo',
																	labelStyle: 'font-weight:bold;',
																	fieldLabel:'Issues'
																	,displayField:'name'
																	,name: 'errortype'
																	,value:data.data.errortype
																	,valueField:'id'
																	,store: new Ext.data.SimpleStore({
																		 fields:['id', 'name']
																		 ,data:[
																			['Billing/ Credit Card', 'Billing/ Credit Card'],
																			['Cancellation','Cancellation'],
																			['Contracts', 'Contracts'],
																			['Data', 'Data'],
																			['Downgrading Account', 'Downgrading Account'],
																			['General Question','General Question'],
																			['Refund','Refund'],
																			['Credit','Credit'],
																			['REIFax Promotion Inquiry', 'Promotion Inquiry'],
																			['REIFax Training','Training'],
																			['Suggestions', 'Suggestions'],
																			['Technical/ System Issue', 'Technical/ System Issue'],
																			['Upgrading Account','Upgrading Account'],
																			['Other','Other']
																		]
																		
																	})
																	,triggerAction:'all'
																	,mode:'local'
																	,width: 140
															},{
																xtype	: 'combo',
																fieldLabel:'Issues About'
																,displayField:'name'
																,labelStyle: 'font-weight:bold;'
																,value	:data.data.errortype2
																,valueField:'id'
																,hiddenName:'errortype2'
																,store: new Ext.data.SimpleStore({
																	 fields:['id', 'name']
																	,data:[
																	['0', 'NONE'],
																	['Genaral', 'Genaral'],
																	['Specific', 'Specific']
																]
																})
																,triggerAction:'all'
																,mode:'local'
																,width: 140
															},{
																xtype	: 'combo',
																fieldLabel:'System Opera.'
																,displayField:'name'
																,labelStyle: 'font-weight:bold;'
																,value	:data.data.os
																,valueField:'id'
																,hiddenName:'os'
																,store: new Ext.data.SimpleStore({
																	 fields:['id', 'name']
																	,data:[
																	['0', 'NONE'],
																	['Windows', 'Windows'],
																	['Linux', 'Linux'],
																	['Android', 'Android'],
																	['Other', 'Other']
																]
																})
																,triggerAction:'all'
																,mode:'local'
																,width: 140
															},{
																xtype	: 'combo',
																fieldLabel:'Browser'
																,displayField:'name'
																,labelStyle: 'font-weight:bold;'
																,value	:data.data.browser
																,valueField:'id'
																,hiddenName:'browser'
																,store: new Ext.data.SimpleStore({
																	 fields:['id', 'name']
																	,data:[
																	['0', 'NONE'],
																	['Chrome', 'Chrome'],
																	['Firefox', 'Firefox'],
																	['IE9', 'IE9'],
																	['IE8', 'IE8'],
																	['IE7', 'IE7'],
																	['IE6', 'IE6'],
																	['Safari', 'Safari'],
																	['Opera', 'Opera'],
																	['Other', 'Other']
																]
																})
																,triggerAction:'all'
																,mode:'local'
																,width: 140
															},{
																xtype	: 'combo',
																fieldLabel:'Product'
																,displayField:'name'
																,labelStyle: 'font-weight:bold;'
																,value	:data.data.id_product
																,valueField:'id'
																,hiddenName:'id_product'
																,store: new Ext.data.SimpleStore({
																	 fields:['id', 'name']
																	,data:[
																	['0', 'NONE'],
																	['1', 'REIFax Residential'],
																	['2', 'REIFax Platinum'],
																	['3', 'REIFax Professional'],
																	['7', 'Home Deal Finder'],
																	['6', 'Buyer´s Pro'],
																	['8', 'Leads Generator']
																]
																})
																,triggerAction:'all'
																,mode:'local'
																,width: 140
															}]
														},
														{
															layout: 'form',
															columnWidth: .67,
															labelWidth: 80,
															items:[{
																fieldLabel:'Issues Description',
																xtype	: 'textarea',
																name	: 'errordescription',
																labelStyle: 'font-weight:bold;',
																value 	: data.data.errordescription,
																width	: '100%',
																height	: 130
															}
															]
														}]
												},
												{
													xtype: 'fieldset',
													border : false,
													columnWidth: 1,
													items : [
													new Ext.TabPanel({ 
														border : false,
														activeTab: 0,
														items: [new Ext.Panel({
															title :'Admin Control',
															border : false,
															items :
															[
																{
																	xtype: 'fieldset',
																	padding:10,
																	style:{
																		background : '#FFF',
																		font: '13px tahoma,arial,helvetica,sans-serif'
																		},
																	title: 'Control',
																	layout:'column',
																	columnWidth: 1,
																	items: [
																		{
																			layout: 'form',
																			columnWidth: 0.5,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'Status',
																				value : data.data.status
																				,name:'status'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:[
																					['Open', 'Open'],
																					['Client', 'Client'],
																					['Customer', 'Customer'],
																					['Process', 'Process'],
																					['Solved', 'Solved'],
																					['Hold', 'Hold'],
																					['Close', 'Close']
																				]
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}
																			]
																		}]
																},{
																	xtype: 'fieldset',
																	padding:10,
																	style:{
																		background : '#FFF',
																		font: '13px tahoma,arial,helvetica,sans-serif'
																		},
																	title: 'Staff',
																	layout:'column',
																	columnWidth: 1,
																	items: [
																		{
																			layout: 'form',
																			columnWidth: 0.5,
																			items:[customerService]
																		},
																		{
																			layout: 'form',
																			columnWidth: 0.5,
																			items:[programmer]
																		}]
																},{
																	xtype: 'fieldset',
																	padding:10,
																	style:{
																		background : '#FFF',
																		font: '13px tahoma,arial,helvetica,sans-serif'
																		},
																	title: 'Date Control',
																	layout:'column',
																	columnWidth : 1,
																	labelWidth : 80,
																	items: [
																		{
																			layout: 'form',
																			columnWidth: 0.25,
																			items:[
																				new Ext.form.DateField({
																					name :'datel',
																					value :(data.data.datel)? data.data.datel.split(' ')[0]:'',
																					fieldLabel : 'Date added',
																					format :'Y-m-d'
																				})
																			]
																		},{
																			layout: 'form',
																			columnWidth: 0.25,
																			items:[
																				new Ext.form.DateField({
																					name :'datef',
																					value : (data.data.datef)?data.data.datef.split(' ')[0]:'',
																					fieldLabel : 'Date Solved',
																					format :'Y-m-d'
																				})
																			]
																		},{
																			layout: 'form',
																			columnWidth: 0.25,
																			items:[
																				new Ext.form.DateField({
																					name :'datec',
																					value : (data.data.datec)?data.data.datec.split(' ')[0]:'',
																					fieldLabel : 'Date Notified',
																					format :'Y-m-d'
																				})
																			]
																		},{
																			layout: 'form',
																			columnWidth: 0.25,
																			items:[
																				new Ext.form.DateField({
																					name :'date_check',
																					value : (data.data.date_check)?data.data.date_check.split(' ')[0]:'',
																					fieldLabel : 'Date Check',
																					format :'Y-m-d'
																				})
																			]
																		}
																	]
																}
															
															]
														}),new Ext.Panel({
																title :'Search',
																layout:'column',
																border : false,
																padding: 10,
																height : 400,
																items :[
																		{
																			layout: 'form',
																			columnWidth: .50,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'State',
																				value : data.data.state
																				,hiddenName:'state'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:Ext.combos_selec.states
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}]
																		},{
																			layout: 'form',
																			columnWidth: .50,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'County',
																				value : data.data.idcounty
																				,hiddenName:'County'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:Ext.combos_selec.cbCounty
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}]
																		},{
																			layout: 'form',
																			columnWidth: .50,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'Data',
																				value : data.data.menutab
																				,hiddenName:'menutab'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:[
																					['Public Records', 'Public Records'],
																					['For Sale', 'For Sale'],
																					['Foreclosures', 'Foreclosures'],
																					['By Owner', 'By Owner'],
																					['By Owner Rent', 'By Owner Rent']
																				]
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}]
																		},{
																			layout: 'form',
																			columnWidth: .50,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'Property Type',
																				value : data.data.proptype
																				,hiddenName:'proptype'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:[
																						['Any Type', 'Any Type'],
																						['Single Family', 'Single Family'],
																						['Condo/Town/Villa', 'Condo/Town/Villa'],
																						['Multi Family +10', 'Multi Family +10'],
																						['Multi Family -10', 'Multi Family -10'],
																						['Commercial', 'Commercial'],
																						['Vacant Land', 'Vacant Land'],
																						['Mobile Home', 'Mobile Home'],
																						['Other', 'Other']
																					]
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}]
																		},{
																			layout: 'form',
																			columnWidth: .50,
																			items:[{
																				xtype	: 'combo',
																				fieldLabel:'Submenu Tab',
																				value : data.data.submenutab
																				,hiddenName:'submenutab'
																				,displayField:'name'
																				,valueField:'id'
																				,store: new Ext.data.SimpleStore({
																					 fields:['id', 'name']
																					,data:[
																					['SEARCH', 'SEARCH'],
																					['RESULTS', 'RESULTS'],
																					['COMPARABLES', 'COMPARABLES'],
																					['COMPARABLES ACTIVES', 'COMPARABLES ACTIVES'],
																					['DISTRESSED', 'DISTRESSED'],
																					['COMPARABLES RENTAL', 'COMPARABLES RENTAL'],
																					['STATISTICS', 'STATISTICS'],
																					['MY SHORT SALES', 'MY SHORT SALES'],
																					['MY SAVED DOCS', 'MY SAVED DOCS']
																				]
																				})
																				,triggerAction:'all'
																				,mode:'local'
																			}]
																		},{
																			layout: 'form',
																			columnWidth: 1,
																			items:[{
																				fieldLabel:'Search',
																				xtype	: 'textarea',
																				name:'search',
																				value : data.data.search,
																				width	: '560'
																			}]
																		}]
																})
															,new Ext.Panel({
																title :'communication',
																border : false,
																tbar:new Ext.Toolbar({
																	width: '100%',
																	items: [
																			{
																				xtype: 'button', // default for Toolbars, same as 'tbbutton'
																				text: 'Read',
																				handler:  function (a,b,c,d){
																				if(data.data.msg_cliente!='')
																					{
																						Ext.Ajax.request({
																						   url: 'php/funcionesTickets.php',
																						   success: function (a)
																						   {
																							   tickets.dataStore.reload();
																						   },
																						   failure: function (a)
																						   {
																							   
																						   },
																						   params: { 
																							opcion: 'marcarMsg',
																							id	: data.data.idcs 
																							}
																						});
																					}
																				}
																			}
																		]
																	}),
																height : 400,
																layout:'column',
																items : [{
																			layout: 'form',
																			columnWidth: .50,
																			items:[Msg]
																		},{
																		layout: 'form',
																		columnWidth: .50,
																		items:[
																		new Ext.Panel({
																			frame : true,
																			layout: 'form',
																			scope	:_thats,
																			border :false,
																			items:
																			[
																				{
																					xtype	: 'textarea',
																					width	: '100%',
																					id	: 'msg'+data.data.idcs,
																					height	: 200,
																					hideLabel	: true,
																					emptyText	: 'New Msg',
																					name	:'Msg'
																				}
																			],
																			buttons: 
																			[
																				{
																					text : 'Send Msg',
																					scope	:this,
																					handler: function ()
																					{
																						var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
																						myMask.show();
																						var form=Ext.getCmp('msg'+data.data.idcs).getValue();
																						Ext.Ajax.request({
																							url: 'php/funcionesTickets.php',
																							scope : this,
																							params :{
																									opcion : 'newMsg',
																									Msg : form,
																									id	:data.data.idcs
																								},
																							   success: function (a)
																							   {
																								   	var result=Ext.util.JSON.decode(a.responseText);
																									myMask.hide();
																									Ext.getCmp('view'+result.id).getStore().reload();
																									tickets.dataStore.reload();
																									Ext.getCmp('msg'+data.data.idcs).setValue('');
																							   },
																							   failure: function (a)
																							   {
																								   
																							   },
																						})
																					}
																				}
																			]
																		})]
																		}
																	]
																}),
																new Ext.Panel({
																	title :'Attached',
																	layout:'column',
																	border : false,
																	padding: 10,
																	tbar:[
																	{
																			xtype		: 'fileuploadfield',
																			buttonOnly	: true,
																			name		: data.data.idcs,
																			buttonText	: '',
																			buttonCfg	: {
																				iconCls: 'x-icon-add'
																			},
																			scope	:this,
																			listeners: {
																				'fileselected': function(fb, v){
																					var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
																					myMask.show();
																					var fd = new FormData();
																					fd.append("fileToUpload",fb.fileInput.dom.files[0]);
																					fd.append("ticket", fb.name);
																					fd.append("opcion", 'addImages');
																				
																					$.ajax({
																					   url: "php/funcionesTickets.php",
																					   type: "POST",
																					   data: fd,
																					   processData: false,
																					   contentType: false,
																					   success: function(response) {
																						   storeImg.load();
																						   myMask.hide();
																					   },
																					   error: function(jqXHR, textStatus, errorMessage) {
																						   console.log(errorMessage); // Optional
																					   }
																					});
																					
																				}
																			}
																	
																	}
																	],
																	height : 350,
																	items :[
																			Img
																	]
																}),
																new Ext.Panel({
																	title :'solution',
																	layout:'column',
																	border : false,
																	padding: 10,
																	height : 250,
																	items :[
																			/*new Ext.form.FormPanel({
																			frame : true,
																			id	: 'sol'+data.data.idcs,
																			scope	:this,
																			border :false,
																			items:
																			[ */
																				{
																					xtype	: 'textarea',
																					width	: '100%',
																					height : 240,
																					fieldLabel:'Solution',
																					emptyText	: 'Enter the solution',
																					value : data.data.soldescription,
																					name	:'solution'
																				}
																			/*],
																			buttons: 
																			[
																				{
																					text : 'Update',
																					scope	:this,
																					handler: function ()
																					{
																						var form=Ext.getCmp('sol'+data.data.idcs).getForm()
																						////console.debug(Ext.getCmp('msg'+data.data.idcs).getForm());
																						////console.debug(data.data.idcs)
																						form.submit({
																							waitTitle:'Processing',
																							waitMsg :'Update...',
																							url: 'php/funcionesTickets.php',
																							scope : this,
																							params :{
																									opcion : 'updateSolution',
																									id	:data.data.idcs
																								},
																							success	: function (form,action)
																							{
																								
																							}
																						})
																					}
																				}
																			]
																		})*/
																	]
																})
															]
														})
													]
												}
											],
											buttons:[{
													xtype 	: 'button',
													text	: 'Close',
													scope: this,
													handler: function (){
														if(Ext.getCmp('form'+data.data.idcs).getForm().isValid())
														{								
															Ext.getCmp('form'+data.data.idcs).getForm().submit({
																waitTitle:'Updating',
																waitMsg :'Updating the information of the ticket...',
																url: 'php/funcionesTickets.php',
																scope : this,
																params :{
																	opcion : 'updateTicket',
																	id	:data.data.idcs
																},
																success	: function (form,action)
																{
																	if(tickets.dataStore)
																		tickets.dataStore.reload();
																	Ext.getCmp('win'+data.data.idcs).close();
																},
																failure :function (form,action)
																{
																	//console.debug(action);
																	var aux=Ext.util.JSON.decode(action.response.responseText);
																	//console.debug(aux);
																	Ext.Msg.alert('Error','<strong>sorry. is presented to an error.</strong> <br><br> MySql Error: '+aux.mysqlError+'<br>Sql:'+((aux.sql)?aux.sql:'NULL'));
																}
															})
														}
													}
													
												}]
										})
								
								win= new Ext.Window({
											layout: 'fit',
											id:'win'+data.data.idcs, 
											title: 'Ticket #'+data.data.ticket, 
											closable: false,
											resizable: false,
											maximizable: false,
											plain: true,
											border: false,
											width: 880,
											height: 645,
											items	:[
												form
											]
										});
								win.show();







/**********************************************
***********
*************************************************/
					}
					else{
						Ext.Msg.alert('Info.','The ticket is being modified by user '+data.data.editing);
					}
				}
				else{
					Ext.Msg.alert('Error','apology. There was an error when loading the ticket');
				}
			}
		})
		

	}
}
function cleanSession (){
	Ext.Ajax.request({
			url: 'php/funcionesTickets.php',
			scope : this,
			params :{
				opcion : 'unlookerTickes'
			},
			success: function (data){
				return true;
			}
	});
}
function removeImg(id,view){
	tickets.loadMask.show();
	$.ajax({
		url	:'php/funcionesTickets.php',
		type:'POST',
		async:false,
		data:{
				opcion 	: 'removeImg',
				img		: id},
		success: function(){
				tickets.loadMask.hide();
		}
	});
	Ext.getCmp(view).getStore().load();
}
function changeUser(id){
	var id=id;
	var form=new Ext.FormPanel({
			padding : 5,
			name :'changeUser',
			items:[tickets.combo.user()]
		});
	var win=new Ext.Window({
		layout: 'fit',
		modal	:true,
		title: 'Change User ', 
		closable: true,
		resizable: false,
		maximizable: false,
		plain: true,
		border: false,
		width: 350,
		height: 100,
		items	:[
			form
		],
	    buttons:[{
			text	:'Save',
			scope	: this,
			handler : function (a,b,c,d){
				form.getForm().submit({  
					url : 'php/funcionesTickets.php',  
					params: {  
						ticket: id ,
						opcion : 'changeUser'
					},  
					waitMsg : 'Changing user...',  
					failure: function (form, action) {  
						Ext.MessageBox.show({  
							title: 'Error processing exchange',  
							msg: 'Error processing exchange.',  
							buttons: Ext.MessageBox.OK,  
							icon: Ext.MessageBox.ERROR  
						});  
					},  
					success: function (form, request) {  
						Ext.MessageBox.show({  
							title: 'Change successfully processed',  
							msg: 'Change successfully processed',  
							buttons: Ext.MessageBox.OK,  
							icon: Ext.MessageBox.INFO  
						});
						
						var data=Ext.util.JSON.decode(request.response.responseText);
						
						document.getElementById('winUserid'+id).innerHTML='<a href="javascript:void(0)" class="itemusers" style="font-size:13px" title="Details Users. Click to see the information." onclick="mostrarDetalleUsuers('+data.idCliente+')">'+data.idCliente+'</a>';
						
						document.getElementById('winHomePho'+id).innerHTML=data.home;
						
						document.getElementById('winName'+id).innerHTML=data.name;
						
						document.getElementById('winMobilePho'+id).innerHTML=data.mobile;
						
						document.getElementById('winEmail'+id).innerHTML=data.email;
						win.close(); 
					}  
				});  
			}
		},{text:'Cancel',scope:this,handler:function (){win.close()}}]
	});
	win.show();
}
Ext.QuickTips.init();