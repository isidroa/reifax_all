// JavaScript Document
var ACTIVATE=0;
var TRABAJANDO=0;
var icorte=0;var varcorte=15;
Ext.Ajax.timeout = 120000; 
Ext.ns('schedule');
schedule.details={};
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
	console.debug(Ext.getBody().getViewSize().width);
	schedule.detailsInitDate=initSemana().format('Y-m-d');
	schedule.detailsEndDate=new Date().format('Y-m-d');
	ACTIVATE=Ext.util.Cookies.get('activeConexion')=='true'?1:0;
/////////////////Variables////////////////////
	var win;
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

///////////Cargas de data dinamica/////////////////
var store = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'results',
	url: 'saveconnections.php',
	remoteSort : true,
	fields: [
		{name: 'userid', type: 'int'}
		,{name: 'device'}
		,'ipclient'
		,'nameuser'
		,'status' 
		,'lastdate'
	],
	listeners:{
		'beforeload':function (){
			TRABAJANDO=1;
		},
		'load':function (that, records, options){
			TRABAJANDO=0;
			for (var k in that.data.map){
				if(!that.data.map[k].json.processClient){
					ACTIVATE=0;
					var clientdate=new Date().format('Y-m-d H:i:s'); 
					Ext.getCmp('btnConnect').setText('<b>CONNECT</b>');
					Ext.getCmp('btnConnect').setIcon('images/play.png');
				}
				break;
			}
			if(ACTIVATE){
				var hoy=new Date();  
				var hourcuerrent=hoy.getHours();
				var clientdate=new Date().format('Y-m-d H:i:s'); //hoy.getFullYear()+'-'+hoy.getMonth()+'-'+hoy.getDay()+' '+hourcuerrent+':'+hoy.getMinutes()+':'+hoy.getSeconds();  
				var colorfa='148530';
				if( (hourcuerrent>=12 && hourcuerrent<14 ) || hourcuerrent>=18)
					colorfa='853114';
				var aux="("+(icorte+1)+"). <font color='"+colorfa+"'><strong>[OK]</strong></font>. HI, <strong>"+username+"</strong>. <br>Date:  <strong>"+clientdate+'</strong>. IP:  <strong>'+ipclient+'</strong>.<hr>';
				Ext.getCmp('logsconnections').setValue(aux+Ext.getCmp('logsconnections').getValue());
				icorte++;
			}
		},
		'loadexception':function (){
			TRABAJANDO=0;
			if(ACTIVATE){
				var hoy=new Date();  
				var hourcuerrent=hoy.getHours();
				var clientdate=new Date().format('Y-m-d H:i:s'); //hoy.getFullYear()+'-'+hoy.getMonth()+'-'+hoy.getDay()+' '+hourcuerrent+':'+hoy.getMinutes()+':'+hoy.getSeconds(); 
				var colorfa='FF0000';
				if( (hourcuerrent>=12 && hourcuerrent<14 ) || hourcuerrent>=18)
					colorfa='853114';
				var aux="("+(icorte+1)+"). <font color='"+colorfa+"'><strong>[FAIL]</strong></font>. HI, <strong>"+username+"</strong>.<br>Date:  <strong>"+clientdate+'</strong>. IP:  <strong>'+ipclient+'</strong>.<hr>';
				
				Ext.getCmp('logsconnections').setValue(aux+Ext.getCmp('logsconnections').getValue());
				icorte++;
			}
		}
	}
});
/***************************
 * STORE FOR SCHEDULES
 */

var storeShedules = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'data',
	baseParams:{
		opcion:'readSchedules'
	},
	autoLoad	:true,
	autoSave	:true,
	url: 'php/funcionesControlConex.php',
	remoteSort : false,
fields: [
		{name: 'userid', type: 'int'},
		{name: 'username'},
		{name: 'hour_init'},
		{name: 'hour_end'},
		{name: 'hour_break_init'},
		{name: 'hour_break_end'},
		{name: 'break'}
	]
});
/***************************
 * STORE FOR Stadistica
 */

var storeStat = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'data',
	baseParams:{
		opcion:'readStatold',
		dateInit:'2014-01-01',
		dateEnd:new Date().format('Y-m-d')
	},
	autoSave	:true,
	autoLoad	:true,
	url: 'php/funcionesControlConex.php',
	remoteSort : false,
	fields: [
		{name: 'userid', type: 'int'},
		{name: 'nameUser', type: 'string'},
		{name: 'horario'},
		{name: 'Fhorario'},
		
		{name: 'statusLaboral'},
		{name: 'total'},
		{name: 'status'}
	]
});
/***************************
 * STORE FOR Stadistica
 */

var storeVacation = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'data',
	baseParams:{
		opcion:'readVacation'
	},
	autoLoad	:true,
	autoSave	:true,
	url: 'php/funcionesControlConex.php',
	remoteSort : false,
	fields: [
		{name: 'userid', type: 'int'},
		{name: 'username', type: 'string'},
		{name: 'balance'}
	]
});
var storeVacation2 = new Ext.data.JsonStore({
	totalProperty: 'total',
	root: 'data',
	baseParams:{
		opcion:'readVacationDetails'
	},
	autoLoad	:true,
	autoSave	:true,
	url: 'php/funcionesControlConex.php',
	remoteSort : false,
	fields: [
		{name: 'date_record'},
		{name: 'reason'},
		{name: 'value'}
	]
});

///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
////////////////FIN barra de pagineo//////////////////////

//////////////////Manejo de Eventos//////////////////////
var updateClock = function(){  
	var texto='<div align="center"><font size="7" color="FF0000" face="courier new"><b>'+new Date().format('G:i:s')+'</b></font></div>';
    Ext.getCmp('optionspanel').setValue(texto);  
}   
  
var task = {  
    run: updateClock, //the function to run  
    interval: 1000 //every second  
}   
  
var runner = new Ext.util.TaskRunner();  
runner.start(task); //start runing the task every one second  


var task2 = {  
    run: function (){
		var hoy=new Date();  
		var hourcuerrent=hoy.getHours();
		var clientdate=new Date().format('Y-m-d H:i:s'); 
		
		store.load({
			params:{
				active		:ACTIVATE,
				userid 		: userid,
				username 	: username,
				ip 			: ipclient,
				timedateclient : clientdate,
				icorte		:icorte
			}
		});
	},   
    interval: refreshConex 
}   
  
//var runner2 = new Ext.util.TaskRunner();  
//runner2.start(task2); 

function changeColor(val, p, record){
	if(record.data.status == 'OFFLINE')
	{
		return '<span style="color:red;font-weight: bold;">' + val + '</span>';
	}
	if(record.data.status == 'ONLINE')
	{
		return '<span style="color:green;font-weight: bold;">' + val + '</span>';
	}
	return val;
}

//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
var grid = new Ext.grid.EditorGridPanel({
	id:"gridpanel",
	store: store,
	iconCls: 'icon-grid',
	tbar:[
			{
				text:'Reload',
				scope:this,
				xtype	:'button',
				handler:function (){
					
					var hoy=new Date();  
					var hourcuerrent=hoy.getHours();
					var clientdate=new Date().format('Y-m-d H:i:s'); 
					
					store.load({
						params:{
							active		:ACTIVATE,
							userid 		: userid,
							username 	: username,
							ip 			: ipclient,
							timedateclient : clientdate,
							icorte		:icorte
						}
					});
					
				}
			}]
	,
	border:false,
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'Userid', width: 50, sortable: true, align: 'left', dataIndex: 'userid'},
		{header: 'User', width: 200, sortable: true, align: 'left', dataIndex: 'nameuser'},
		{header: 'Status', width: 80, align: 'center', sortable: true, dataIndex: 'status',renderer: changeColor},
		{header: 'Last Conex', width: 130, align: 'left', sortable: true, dataIndex: 'lastdate'},
		{header: 'ip', width: 130, align: 'left', sortable: true, dataIndex: 'ipclient'},
		{header: 'Device', width: 90, align: 'right', sortable: true, dataIndex: 'device',renderer:function (val){
			if(val==1){
				return 'Mobil'
			}
			if(val==2){
				return 'Tablet'
			}
			if(val==3){
				return 'Computer'
			}
		}},{
			xtype: 'actioncolumn',
			width: 16,
			items: [
				{
					icon   	: 'https://cdn1.iconfinder.com/data/icons/duesseldorf/16/logout.png',                // Use a URL in the icon config
					tooltip	: 'Force logout',
					scope	:	this,
					handler	: function(grid, rowIndex, colIndex) {
						var rec = grid.getStore( ).getAt(rowIndex);
						console.debug(rec);
						Ext.Ajax.request({  
							waitMsg: 'Loading...',
							url: 'php/funcionesControlConex.php', 
							scope:this,
							method: 'POST', 
							params: { 
								userid 	: rec.get('userid'),
								opcion	:'forceLogout'
							},
							success:function (){
								
								var hoy=new Date();  
								var hourcuerrent=hoy.getHours();
								var clientdate=new Date().format('Y-m-d H:i:s'); 
								store.load({
									params:{
										active		:ACTIVATE,
										userid 		: userid,
										username 	: username,
										ip 			: ipclient,
										timedateclient : clientdate,
										icorte		:icorte
									}
								});
							}
						});
					}
				}
			]
		}
	],
	viewConfig: {
		forceFit:true, 
		getRowClass : function (row, index) { 
			var cls = ''; 
			var col = row.data;
			if(col.status == 'OFFLINE')
			{
				cls = 'red-row'
			}
			if(col.device == 1 || col.device == 2){
				cls = 'yellow-row';
				col.status='OFFLINE';
			}
				 return cls; 
		} 
	},
	frame:true,
	autoHeight:true,
	loadMask:true,
});
/***************************
 * grid SCHEDULER 
 */
var editorHour 	= new Ext.form.TimeField({
    minValue: '5:00 AM',
    maxValue: '9:00 PM',
    format: 'H:i:s',
    increment: 30
});
var comboBool	=  new Ext.form.ComboBox({
    typeAhead: true,
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    store: new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [[1, 'Yes'], [0, 'No']]
    }),
    valueField: 'myId',
    displayField: 'displayText'
})
var gridSchedules = new Ext.grid.EditorGridPanel({
	store: storeShedules,
	iconCls: 'icon-grid',
	border:false,
	tbar:new Ext.PagingToolbar({  
			store: storeShedules,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[
			{
				text:'Save',
				scope:this,
				xtype	:'button',
				iconCls	:'x-icon-save',
				handler:function (){
					var grid=gridSchedules;
					var modified = grid.getStore().getModifiedRecords();
					
					if(!Ext.isEmpty(grid.getStore().getModifiedRecords())){  
						var recordsToSend = [];  
						Ext.each(modified, function(record) {
							recordsToSend.push(Ext.apply({id:record.id},record.data));  
						});  
					  
						grid.el.mask('Saving...', 'x-mask-loading');
						grid.stopEditing();  
					  
						recordsToSend = Ext.encode(recordsToSend);
					  
						Ext.Ajax.request({ 
							url : 'php/funcionesControlConex.php',  
							params :
								{
									records : recordsToSend,
									opcion	:'updateSchedules'
								},
							method:'POST',
							scope:this,  
							success : function(response) {
								grid.el.unmask();  
								grid.getStore().commitChanges();  
							}  
						});  
					}  
				}
			},
			{
				text:'Add User',
				scope:this,
				xtype	:'button',
				iconCls	:'x-icon-add',
				handler:function (){
					//creamos un formulario 
					this.form= new Ext.FormPanel({ 
						border:false, // <-- Le quitamos el borde al formulario
						defaults:{xtype:'textfield'},	//componente por default del formulario 
						items:[
							{ 
								fieldLabel:'Userid', 
								name:'txt-userid'
							},
							{ 
								fieldLabel:'Name', 
								name:'txt-name'
							},
							{ 
								fieldLabel:'Surname', 
								name:'txt-surname'
							},
							{ 
								fieldLabel:'Email',
								name:'txt-email'
							},
							{ 
								fieldLabel:'Password',
								name:'txt-pass'
							}
						] 
					}); 
					this.win = new Ext.Window({ 
						title: 'New Developer', 
						width:300, 
						height:250, 
						bodyStyle:'background-color:#fff;padding: 10px', 
						items:this.form,
						buttonAlign: 'right', 
						buttons:[{
								text:'Save',
								scope:this,
								handler:function (){
									var grid=gridSchedules;
									var store = grid.getStore();
									var win=this.win;
									this.form.getForm().submit({
										method: 'POST',
										url : 'php/funcionesControlConex.php',  
										params :
											{
												opcion	:'recordNewUser'
											},
										 success: function(form, action) {
											Ext.Msg.alert('Success', action.result.msg);
											store.load();
											win.close();
										},
										failure: function(form, action) { 
											switch (action.failureType) { 
												case Ext.form.Action.CLIENT_INVALID: 
												Ext.Msg.alert('Failure', 'Form fields may  not be submitted with invalid values');
												break;
									
												case Ext.form.Action.CONNECT_FAILURE: 
												Ext.Msg.alert('Failure', 'Ajax  communication failed'); 
												break; 
									
												case Ext.form.Action.SERVER_INVALID: 
												Ext.Msg.alert('Failure', action.result.msg); 
											} 
										} 
									});
								}
							},{
								text:'Cancel',
								scope:this,
								handler:function (){
									this.win.close();
								}
							}]
					}); 
					
					this.win.show();

				}
			}
		]
	}),
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'User', width: 200, sortable: true, align: 'left', dataIndex: 'username'},
		{header: 'Entry', width: 80, sortable: true, align: 'left', dataIndex: 'hour_init', editor:editorHour},
		{header: 'Break', width: 80, sortable: true, align: 'center', dataIndex: 'break', editor:comboBool,renderer:function (val){
			if(val==1){
				return 'Yes';
			}
			else{
				return 'No';
			}
		}
		},
		{header: 'Break Init', width: 80, sortable: true, align: 'left', dataIndex: 'hour_break_init', editor:editorHour},
		{header: 'Break End', width: 80, sortable: true, align: 'left', dataIndex: 'hour_break_end', editor:editorHour},
		{header: 'Out', width: 80, sortable: true, align: 'left', dataIndex: 'hour_end', editor:editorHour},{
		xtype: 'actioncolumn',
		width: 16,
		items: [
			{
				icon   	: 'https://cdn1.iconfinder.com/data/icons/duesseldorf/16/logout.png',                // Use a URL in the icon config
				tooltip	: 'deleteUser',
				scope	:	this,
				handler	: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore( ).getAt(rowIndex);
					console.debug(rec);
					Ext.Ajax.request({  
						waitMsg: 'Loading...',
						url: 'php/funcionesControlConex.php', 
						scope:this,
						method: 'POST', 
						params: { 
							userid 	: rec.get('userid'),
							opcion	:'deleteUser'
						},
						success:function (){
							grid.getStore( ).load();
						}
					});
					
					console.debug(rec);
				}
			}
		]
	}
	],
	viewConfig: {
		forceFit:true
	},
	autoHeight:true,
	loadMask:true,
	frame:true
});

/***************************
 * grid Stadistica 
 */
 
 
var gridStat = new Ext.grid.EditorGridPanel({
	store: storeStat,
	iconCls: 'icon-grid',
	border:false,
	tbar:new Ext.PagingToolbar({  
			store: storeStat,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[{
						xtype	:'button',
						iconCls	:'x-icon-add',
						text	: 'Add notification',
						handler:function (){
							if(schedule.details.addRecord){
								schedule.details.addRecord.win.show();
							}
							else{
								schedule.details.addRecord={};
									schedule.details.addRecord.form = new Ext.form.FormPanel({
									frame : true,
									border :false,
									bodyStyle :{
										background : '#FFF'
									},
									padding: 10,
									layout:'form',
									
									items : [{
										xtype: 'combo',
										store: new Ext.data.SimpleStore({
											fields: ['userid', 'name'],
											data : Ext.combos_selec.dataUsersAdmin
										}),
										hiddenName: 'userid', 
										valueField: 'userid',
										displayField:'name',
										fieldLabel:	'User',
										allowBlank:false,
										typeAhead: true,
										triggerAction: 'all',
										mode: 'local',
										selectOnFocus:true,
										 style: {
											fontSize: '16px'
										},
										width:200
									},
									{
										xtype:'combo',
										typeAhead: true,
										hiddenName :'signo',
										triggerAction: 'all',
										lazyRender:true,
										mode: 'local',
										value	:'+',
										store: new Ext.data.ArrayStore({
											fields: [
												'myId',
												'displayText'
											],
											data: [['+', 'Add'], ['-', 'Subtraction'], ['vacations', 'Vacations']]
										}),
										valueField: 'myId',
										fieldLabel	:'Type',
										displayField: 'displayText',
										listeners:{
											change:function (that, newValue, oldValue){
												if(newValue=='vacations'){
													that.findParentByType('form').findByType('textarea')[0].setValue('assigned day holiday');
													that.findParentByType('form').findByType('numberfield')[0].setVisible( false );
													that.findParentByType('form').findByType('numberfield')[1].setVisible( false );
													that.findParentByType('form').findByType('datefield')[1].setVisible( true );
													
												}
												else{
													
													that.findParentByType('form').findByType('textarea')[0].setValue('');
													that.findParentByType('form').findByType('numberfield')[0].setVisible( true );
													that.findParentByType('form').findByType('numberfield')[1].setVisible( true );
													that.findParentByType('form').findByType('datefield')[1].setVisible( false );
												}
											}
										},
										width:100
									},
									{
										xtype	:'textarea',
										width	: 150,
										name	:'reason',
										fieldLabel	:'Reason'
									},
									{
										xtype	:'datefield',
										name	:'date',
										format 	:'Y-m-d',
										value	: new Date(),
										fieldLabel	:'Date'
									},
									{
										xtype	:'datefield',
										name	:'date2',
										format 	:'Y-m-d',
										value	: new Date(),
										hidden	: true,
										fieldLabel	:'Date End'
									},
									{
										xtype	:'numberfield',
										name	:'hours',
										fieldLabel	:'Hours'
									},
									{
										xtype	:'numberfield',
										name	:'min',
										fieldLabel	:'Minutes'
									}
									],
									buttons:[{
											xtype 	: 'button',
											text	: 'Add',
											handler	: function ()
											{
												if(schedule.details.addRecord.form.getForm().isValid())
												{
													schedule.details.addRecord.form.getForm().submit({
														waitTitle:'Recording',
														waitMsg :'It is processing the registration...',
														url: 'php/funcionesControlConex.php',
														scope : this,
														params :{
															opcion  : 'recorduserUser',
															userid	: schedule.details.userid
														},
														success	: function (form,action)
														{
															schedule.details.addRecord.form.getForm().reset();
															schedule.details.addRecord.win.hide();
															storeStat.load();
															/*
															schedule.details.storeStat.load({
																params:{
																	userid:schedule.details.userid,
																	status:combo[1].getValue( ),
																	dateInit:fechas[0].format('Y-m-d') ,
																	dateEnd:fechas[1].format('Y-m-d') 
																}
															});
															*/
														}
													})
												}
											}
										},
										{
											xtype 	: 'button',
											text	: 'cancel',
											handler : function ()
											{
												schedule.details.addRecord.form.getForm().reset();
												schedule.details.addRecord.win.hide();
											}
										}]
								});
								
								schedule.details.addRecord.win= new Ext.Window({
									layout: 'fit', 
									title: 'Add notification', 
									closable: true,
									closeAction: 'hide',
									resizable: false,
									maximizable: false,
									plain: true,
									border: false,
									width: 350,
									height: 300,
									items	:[
										schedule.details.addRecord.form
									]
								});
								schedule.details.addRecord.win.show();	
							}
						}
					},'|',/*
					{
						xtype	:'button',
						iconCls	:'',
						text	: 'Update Data',
						handler:function (){
						}
					}
					,'|',*/
				{
					xtype: 'checkbox', 
					fieldLabel: 'Active',
					name: 'chk-active', 
					id: 'id-active',
					listeners : {
						check	:	function ( that, checked ){
							var col=gridStat.getColumnModel( );
							if(checked){
								Ext.getCmp('id-BetweenI').setVisible(true);
								Ext.getCmp('id-BetweenE').setVisible(true);
								Ext.getCmp('id-btn-Between').setVisible(true);
								col.setHidden(7, false);
							}
							else{
								Ext.getCmp('id-BetweenI').setVisible(false);
								Ext.getCmp('id-BetweenE').setVisible(false);
								Ext.getCmp('id-btn-Between').setVisible(false);
								col.setHidden(7, true);
							}
						}
					}
				},
				'Between:',
				{
					xtype	: 'datefield',
					name	: 'BetweenI',
					hidden 	: true,
					id: 'id-BetweenI',
					format	: 'Y-m-d',
					value	: initSemana(),
					listeners	: {
					}
				},'To',
				{
					xtype	: 'datefield',
					name	: 'BetweenE',
					hidden 	: true,
					id: 'id-BetweenE',
					format	: 'Y-m-d',
					value	: new Date(),
					listeners	: {
					}
				},
				{
					text	: 'filter',
					scope	: this,
					hidden 	: true,
					id: 'id-btn-Between',
					handler	: function (){
						var fechas=gridStat.getTopToolbar( ).findByType('datefield');
						schedule.detailsInitDate=fechas[0].getValue( );
						schedule.detailsEndDate=fechas[1].getValue( );
						if(schedule.details){
							if (Ext.getCmp('scheduleDetailsGridStat')){
								var fechasDetails=schedule.details.detailsConexTab.getTopToolbar( ).findByType('datefield');
								fechasDetails[0].setValue(fechas[0].getValue( ));
								fechasDetails[1].setValue(fechas[1].getValue( ));
							}
						}
						fechas[0]= new Date(fechas[0].getValue( ));
						fechas[1]= new Date(fechas[1].getValue( ));
						storeStat.load({
							params:{
								dateInit:fechas[0].format('Y-m-d') ,
								dateEnd:fechas[1].format('Y-m-d') 
							}
						})
					}
				}
			]
	}),
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'Userid', width: 50, sortable: true, align: 'left', dataIndex: 'userid',renderer:function (val){
			return '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleConexion('+val+')">'+val+'</a>';
		}},
		{header: 'User', width: 300, sortable: true, align: 'left', dataIndex: 'nameUser'},
		{header: 'Worked Hours', width: 100, sortable: true, align: 'right', dataIndex: 'horario',renderer:conversionTiempoDias},
		{header: 'Absences', width: 100, sortable: true, align: 'right', dataIndex: 'statusLaboral',renderer:conversionTiempoDiasNegativos},
		{header: 'Extra Hours', width: 100, sortable: true, align: 'right', dataIndex: 'Fhorario',renderer:conversionTiempoDias},
		{header: 'Parcial', width: 100, sortable: true, align: 'right', dataIndex: 'status',renderer:conversionTiempoDias},
		{header: 'Balance', width: 100, sortable: true, align: 'right', dataIndex: 'total',renderer:conversionTiempoDias},{
			xtype: 'actioncolumn',
			width: 16,
			items: [
				{
					icon   	: 'http://www.autodesk.com/techpubs/aliasstudio/2010/images/ALIAS/TenFidy/English/Tools/AssignShader.png',                // Use a URL in the icon config
					tooltip	: 'hours off for vacations days',
					scope	:	this,
					handler	: function(grid, rowIndex, colIndex) {
						var rec = grid.getStore( ).getAt(rowIndex);
						console.debug(rec);
						Ext.Ajax.request({  
							waitMsg: 'Loading...',
							url: 'php/funcionesControlConex.php', 
							scope:this,
							method: 'POST', 
							params: { 
								userid 	: rec.get('userid'),
								value	: Math.abs(rec.get('statusLaboral')),
								opcion	:'assingVacations'
							},
							success:function (){
								grid.getStore( ).load();
							}
						});
						
						console.debug(rec);
					}
				}
			]
		}
	],
	viewConfig: {
		forceFit:true, 
		getRowClass : function (row, index) { 
			var cls = ''; 
			var col = row.data;
			if(col.status <0)
			{
				cls = 'red-row'
			}
				 return cls; 
		} 
	},
	autoHeight:true,
	loadMask:true,
});
gridStat.getColumnModel( ).setHidden(7, true);

gridStat.getTopToolbar( ).on('beforechange',function(bar,params){
	/*
	var fechas=gridStat.getTopToolbar( ).findByType('datefield');
		fechas[0]= new Date(fechas[0].getValue( ));
		fechas[1]= new Date(fechas[1].getValue( ));
		params.dateInit=fechas[0].format('Y-m-d') ;
		params.dateEnd=fechas[1].format('Y-m-d');
		*/
})
/***************************
 * grid vacaciones 
 */
 
 
var gridVacation = new Ext.grid.EditorGridPanel({
	store: storeVacation,
	iconCls: 'icon-grid',
	border:false,
	viewConfig: {
		forceFit:true
	},
	tbar:new Ext.PagingToolbar({  
			store: storeVacation,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50,
			items:[
				'|','Between:',
				{
					xtype	: 'datefield',
					name	: 'BetweenI',
					format	: 'Y-m-d',
					value	: initSemana(),
					listeners	: {
					}
				},'To',
				{
					xtype	: 'datefield',
					name	: 'BetweenE',
					format	: 'Y-m-d',
					value	: new Date(),
					listeners	: {
					}
				},
				{
					text	: 'filter',
					scope	: this,
					handler	: function (){
						var fechas=gridVacation.getTopToolbar( ).findByType('datefield');
						schedule.detailsInitDate=fechas[0].getValue( );
						schedule.detailsEndDate=fechas[1].getValue( );
						fechas[0]= new Date(fechas[0].getValue( ));
						fechas[1]= new Date(fechas[1].getValue( ));
						storeStat.load({
							params:{
								dateInit:fechas[0].format('Y-m-d') ,
								dateEnd:fechas[1].format('Y-m-d') 
							}
						})
					}
				}
			]
	}),
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'Userid', width: 50, sortable: true, align: 'left', dataIndex: 'userid',renderer:function (val){
			return '<a href="javascript:void(0)" class="itemusers" title="Details Users. Click to see the information." onclick="mostrarDetalleVacations('+val+')">'+val+'</a>';
		}},
		{header: 'User', width: 300, sortable: true, align: 'left', dataIndex: 'username'},
		{header: 'Balance', width: 300, sortable: true, align: 'left', dataIndex: 'balance',renderer:conversionTiempoDias}
	],
	autoHeight:true,
	loadMask:true,
});
/***************************
 * grid vacaciones detalles 
 */
 
 
var gridVacation2 = new Ext.grid.EditorGridPanel({
	store: storeVacation2,
	id:'gridVacation2',
	iconCls: 'icon-grid',
	border:false,
	viewConfig: {
		forceFit:true
	},
	tbar:new Ext.PagingToolbar({  
			store: storeVacation2,
			displayInfo: true,  
			displayMsg: '{0} - {1} of {2} Registros',  
			emptyMsg: 'No hay Registros Disponibles',  
			pageSize: 50
	}),
	columns: [	
		new Ext.grid.RowNumberer(),
		{header: 'reason', width: 300, sortable: true, align: 'left', dataIndex: 'reason'},
		{header: 'Date', width: 300, sortable: true, align: 'left', dataIndex: 'date_record'},
		{header: 'Cantidad', width: 300, sortable: true, align: 'left', dataIndex: 'value',renderer:conversionTiempo}
	],
	autoHeight:true,
	loadMask:true,
});
/*
var fechas=gridStat.getTopToolbar( ).findByType('datefield');
	fechas[0]= new Date(fechas[0].getValue( ));
	fechas[1]= new Date(fechas[1].getValue( ));
gridStat.getStore().load({
		params:{
			dateInit	: fechas[0].format('Y-m-d'),
			dateEnd		: fechas[1].format('Y-m-d')
		}
	});
*/
/////////////////FIN Grid////////////////////////////
	
//////////////VIEWPORT////////////////////////////////			
	
	schedule.pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			xtype:'tabpanel',
			region:'center',
		    activeTab: 0,
		    items: [{
		        title: 'Conexion',
				layout: 'border',
				hideBorders: true,
		        items:[
		        /*****************
		         * inicio del primer tab 
		         */
		        {
					region:'west',
					width:400,
					items:[
					{
						xtype:'fieldset',
						layout: 'form',
						border:false,
						labelWidth:150,
						items:[{
							xtype: 'htmleditor',
							width: 380,
							hideLabel :true,
							height: 70,
							border:false,
							readOnly: true,
							id: 'optionspanel',
							name: 'optionspanel',
							enableAlignments : false,
							enableColors : false,
							enableFont : false,
							enableFontSize : false,
							enableFormat : false,
							enableLinks : false,
							enableLists : false,
							enableSourceEdit : false
						},{
							xtype:'button',
							scale: 'large',
							id	:'btnConnect',
							width:380,
							text: ACTIVATE==1?'<b>DISCONNECT</b>':'<b>CONNECT</b>',
							icon: ACTIVATE==1?'images/stop.png':'images/play.png',
							handler  : function(){
								if(TRABAJANDO){
									return true;
								}
								icorte=0;
								if(ACTIVATE==0)
								{
									ACTIVATE=1;
									var clientdate=new Date().format('Y-m-d H:i:s'); 
									store.load({
										params:{
											active		:ACTIVATE,
											userid 		: userid,
											username 	: username,
											ip 			: ipclient,
											timedateclient : clientdate,
											icorte		:icorte
										}
									});
									this.setText('<b>DISCONNECT</b>');
									this.setIcon('images/stop.png');
									Ext.util.Cookies.set('activeConexion',true)
								}
								else
								{
									ACTIVATE=0;
									Ext.util.Cookies.set('activeConexion',false);
									Ext.Ajax.request({  
										waitMsg: 'Loading...',
										url: 'php/funcionesControlConex.php', 
										scope:this,
										method: 'POST', 
										//timeout: 100000,
										params: { 
											userid 	: userid,
											opcion	:'desconectUser'
										},
										success:function (){
											var clientdate=new Date().format('Y-m-d H:i:s'); 
											store.load({
												params:{
													active		:ACTIVATE,
													userid 		: userid,
													username 	: username,
													ip 			: ipclient,
													timedateclient : clientdate,
													icorte		:icorte
												}
											});
											this.setText('<b>CONNECT</b>');
											this.setIcon('images/play.png');
										}
									});
								}				
							}
		
						}]
					},
					{
						layout: 'form',
						title:'Connections Logs',
						border:false,
						items:[{
							xtype: 'htmleditor',
							width: 400,
							height:Ext.getBody().getViewSize().height-200,
							readOnly: true,
							id: 'logsconnections',
							name: 'logsconnections',
							enableAlignments : false,
							hideLabel :true,
							enableColors : false,
							enableFont : false,
							enableFontSize : false,
							enableFormat : false,
							enableLinks : false,
							enableLists : false,
							enableSourceEdit : false
						}]
					}
					]
				},{
					region:'center',
					id	:'containerConexCurrent',
					autoHeight: true,
					items: grid
				}
		        /**********
		         **fin del primer tab 
		         */
		        ]
		    },{
		        title: 'Schedules',
				layout: 'border',
				width:Ext.getBody().getViewSize().width,
				hideBorders: true,
		        items:[
		        {
					region:'west',
					width:500,
					items:[{
						xtype	:'panel',
						html	:'<div id="contenedorCalendarioFechas"></div>',
						listeners:{
							afterrender:function (){
								var calendar=$('#contenedorCalendarioFechas').fullCalendar({
									header: {
										center: 'title',
										left: '',
										right: 'prev,next'
									},
									selectable :true,  
									eventClick: function(calEvent, jsEvent, view) {
										var form = new Ext.form.FormPanel({
												frame : true,
												border :false,
												bodyStyle :{
													background : '#FFF'
												},
												padding: 10,
												layout:'form',
												
												items : [
												{
													xtype	:'textfield',
													width	: 150,
													name	:'title',
													value	: calEvent.title,
													fieldLabel	:'title',
												}
												],
												buttons:[{
														xtype 	: 'button',
														text	: 'Update',
														scope	: this,
														handler	: function ()
														{
															calEvent.title=form.getForm().getValues().title;
															form.getForm().submit({
																waitTitle:'Recording',
																waitMsg :'It is processing the registration...',
																url: 'php/funcionesControlConex.php',
																scope : this,
																params :{
																	opcion  : 'updateEvent',
																	id		: calEvent.id
																},
																success	: function (form,action)
																{
																	calendar.fullCalendar( 'refetchEvents' )
																	win.hide();
																}
															})
														}
													},
													{
														xtype 	: 'button',
														text	: 'Delete',
														scope	: this,
														handler : function ()
														{
															$.ajax({
																url		:'php/funcionesControlConex.php',
																type	: 'POST',
																data	:{
																	opcion 	: 'deleteEvent',
																	id		: calEvent.id
																},
																dataType:"json",
																success: function(data){
																	win.hide();
																	calendar.fullCalendar('removeEvents',	calEvent.id
																	).fullCalendar( 'refetchEvents' );
																}
															});
														}
													},
													{
														xtype 	: 'button',
														text	: 'cancel',
														scope	: this,
														handler : function ()
														{
															win.hide();
														}
													}]
											});
											
											win= new Ext.Window({
												layout: 'fit', 
												title: 'Modified', 
												closable: true,
												modal	:true,
												closeAction: 'hide',
												resizable: false,
												maximizable: false,
												plain: true,
												border: false,
												width: 330,
												height: 140,
												items	:[
													form
												]
											});
											win.show();	
								
									},
									select:function (startDate, endDate, allDay, jsEvent, view){
										
										Ext.Msg.prompt('New holding', 'holding name:', function(btn, text){
											if (btn == 'ok'){
												$.ajax({
													url		:'php/funcionesControlConex.php',
													type	: 'POST',
													data	:{
														opcion 	: 'recordDateNoLaboral',
														title	: text,
														date	: startDate.format('Y-m-d')
													},
													dataType:"json",
													success: function(data){
														calendar.fullCalendar('renderEvent',
															{
																title: text,
																start: startDate,
																id		: data.id,
																end: endDate,
																allDay: allDay
															},
															true // make the event "stick"
														);
													}
												});
											}
										});
									},
									editable: true
								});
								$.ajax({
									url		:'php/funcionesControlConex.php',
									type	: 'POST',
									data	:{
										opcion 	: 'readDateNoLaboral'
									},
									dataType:"json",
									success: function(data){
										$.each(data.data,function (i,e) {
											calendar.fullCalendar('renderEvent',
													{
														title	: e.title,
														start	: e.date,
														id		: e.id,
														end		: e.date,
														allDay	: true
													},
													true // make the event "stick"
												);
										})
									}
								})
							}
						}
					}]
				},{
					region:'center',
					autoHeight: true,
					width:500,
					hideBorders: true,
					items:[
		        		gridSchedules
					]
				}
		        ]
		    },{
		        title: 'Statistical',
		        items:[
		        	gridStat
		        ]
		    },{
		        title: 'Vacation',
				layout	: 'hbox',
				items:[{
					xtype	: 'panel',
					width	: 650, 
					height:Ext.getBody().getViewSize().height-85, 
					items	: [
						gridVacation
				]},{
					xtype	: 'panel',
					flex	: 1, 
					height:Ext.getBody().getViewSize().height-100,
					items	: [
						gridVacation2
				]}
				]
		    }]
		}]
	});
	
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
//setTimeout("SendConexion()",1000);  
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
/////////////FIN Inicializar Grid////////////////////
});
function initSemana(){
	var today = new Date();
	var initSemana= new Date(today.getTime() - ((today.format('N')-1) * 24 * 3600 * 1000));
	return new Date(initSemana);
}
function conversionTiempoDias (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				signo='';
			}
			var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini-(dias*28800))/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)+(dias*28800)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}
			else if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
		}
function conversionTiempoDiasNegativos (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				return  '0 m 0 s';
			}
			//var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini)/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			/*if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}*/
			if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
	
}
function conversionTiempo (seg_ini){
			if(seg_ini<0){
				seg_ini=Math.abs(seg_ini);
				signo='-';
			}
			else{
				signo='';
			}
			//var dias = Math.floor(seg_ini/(28800));
			var horas = Math.floor((seg_ini)/3600);
			var minutos = Math.floor((seg_ini-((horas*3600)))/60);
			var segundos = Math.round(seg_ini-(horas*3600)-(minutos*60));
			
			/*if (dias > 0){
				return signo+' '+dias+ 'd '+' '+horas+ 'h '+ minutos+ 'm ';
			}*/
			if (horas > 0){
				return signo+' '+horas+ 'h '+ minutos+ 'm ';
			} else {
				return signo+' '+minutos+ 'm '+ segundos+ 's';
			}
	
		}
function mostrarDetalleConexion(userid){
	if(Ext.getCmp('detailsConexion')){
		Ext.getCmp('detailsConexion').show();
	}
	else{
		/***************************
		 * STORE FOR Stadistica
		 */
		schedule.details={};
		schedule.details.storeStat = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'data',
			baseParams:{
				opcion:'readDetailsConex'
			},
			url: 'php/funcionesControlConex.php',
			remoteSort : false,
			fields: [
				{name: 'userid', type: 'int'},
				{name: 'nameUser', type: 'string'},
				{name: 'horario'},
				{name: 'Fhorario'},
				{name: 'statusLaboral'},
				{name: 'status'},
				{name: 'record'},
				{name: 'date'},
				{name: 'reason'},
				{name: 'useridExec'},
				{name: 'normal'},
				{name: 'execute'},
				{name: 'dateAssing'}
			],
				listeners:{
					'load':function (thats, records, options){
						var total=0;
						for(var k in records){
							if(records[k].data){
								var aux= parseInt(records[k].data.status);
								total+=((typeof aux === 'number' && aux % 1 == 0) ? aux: 0);
							}
						}
						Ext.getCmp('totalParcial').setValue(conversionTiempoDias(total));
					}
				}
		});
		schedule.details.storeStat2 = new Ext.data.JsonStore({
			totalProperty: 'total',
			root: 'data',
			baseParams:{
				opcion:'readDetailsConexMod'
			},
			url: 'php/funcionesControlConex.php',
			remoteSort : false,
			fields: [
				{name: 'userid', type: 'int'},
				{name: 'nameUser', type: 'string'},
				{name: 'horario'},
				{name: 'Fhorario'},
				{name: 'statusLaboral'},
				{name: 'status'},
				{name: 'record'},
				{name: 'date'},
				{name: 'reason'},
				{name: 'useridExec'},
				{name: 'normal'},
				{name: 'execute'},
				{name: 'dateAssing'}
			]
		});
		
		/******************************************
		 **		inicio del panl para las desconexiones 
		 */
		schedule.details.storeDesconect = new Ext.data.JsonStore({
				totalProperty: 'total',
				root: 'data',
				baseParams:{
					opcion:'readDetailsDesconect'
				},
				url: 'php/funcionesControlConex.php',
				remoteSort : false,
				fields: [
					{name: 'dateDesconect1'},
					{name: 'dateDesconect2'},
					{name: 'type'},
					{name: 'timeDesconect'}
				]
			});
			
			schedule.details.gridDesconect = new Ext.grid.EditorGridPanel({
				store: schedule.details.storeDesconect,
				height:Ext.getBody().getViewSize().height-100,
				iconCls: 'icon-grid',
				border:false,
			/*	tbar:new Ext.PagingToolbar({  
						store: schedule.details.storeDesconect,
						displayInfo: true,  
						displayMsg: '{0} - {1} of {2} Registros',  
						emptyMsg: 'No hay Registros Disponibles',  
						pageSize: 50
				}),*/
				columns: [	
					new Ext.grid.RowNumberer(),
					{header: 'Type', width: 200, sortable: true, align: 'left', dataIndex: 'type',renderer:function (val){
						if(val==1){
							return 'Offline';
						}
						else 
							return  'Online';
					}},
					{header: 'Start', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect1'},
					{header: 'End', width: 200, sortable: true, align: 'left', dataIndex: 'dateDesconect2'},
					{header: 'Time', width: 200,  align: 'left', dataIndex: 'timeDesconect', renderer:conversionTiempo}
				],
				viewConfig: {
					forceFit:true, 
					getRowClass : function (row, index) { 
						var cls = ''; 
						var col = row.data;
						if(col.type ==1)
						{
							cls = 'red-row'
						}
						else{
							cls = 'green-row'
						}
							 return cls; 
					} 
				},
				loadMask:true,
			});
			
		schedule.details.gridStat = new Ext.grid.EditorGridPanel({
			id:'scheduleDetailsGridStat',
			store: schedule.details.storeStat,
				height:(Ext.getBody().getViewSize().height-100)/2,
			iconCls: 'icon-grid',
			border:false,
			bbar	:[
				'->','Total: ',
				{
					xtype	: 'textfield',
					id		: 'totalParcial',
					width	: 120
				}
			],
			tbar:new Ext.PagingToolbar({  
					store: schedule.details.storeStat,
					displayInfo: true,  
					displayMsg: '{0} - {1} of {2} Registros',  
					emptyMsg: 'No hay Registros Disponibles',  
					pageSize: 50,
					items:[
					]
			}),
			columns: [	
				new Ext.grid.RowNumberer(),
				{header: 'Date', width: 100, sortable: true, align: 'right', dataIndex: 'dateAssing'},
				{header: 'Worked Hours', width: 100, sortable: true, align: 'right', dataIndex: 'horario',renderer:conversionTiempoDias},
				{header: 'Absences', width: 100, sortable: true, align: 'right', dataIndex: 'statusLaboral',renderer:conversionTiempoDiasNegativos},
				{header: 'Extra Hours', width: 100, sortable: true, align: 'right', dataIndex: 'Fhorario',renderer:conversionTiempoDias},
				{header: 'Balance', width: 100, sortable: true, align: 'right', dataIndex: 'status',renderer:conversionTiempoDias}/*,
				
				
				{header: 'Reason', width: 300, sortable: true, align: 'left', dataIndex: 'reason'},
				{header: 'Responsable', width: 80, sortable: true, align: 'left', dataIndex: 'execute'},
				{header: 'Date Assing', width: 100, sortable: true, align: 'right', dataIndex: 'dateAssing'},
				{header: 'type', width: 100, sortable: true, align: 'right', dataIndex: 'normal',renderer:function (val){
					if(val==1){
						return 'In Schedule';
					}
					else if(val==2){
						return 'Out Schedule';
					}
					else if(val==4){
						return 'By user';
					}
					else{
						return 'System';
					}
				}},
				{header: 'Value', width: 100, sortable: true, align: 'right', dataIndex: 'record',renderer:conversionTiempo},*/
			],
			viewConfig: {
				forceFit:true
			},
			loadMask:true,
		});
		schedule.details.gridStat2 = new Ext.grid.EditorGridPanel({
			id:'scheduleDetailsGridStat2',
			store: schedule.details.storeStat2,
				height:(Ext.getBody().getViewSize().height-100)/2,
			iconCls: 'icon-grid',
			border:false,
			tbar:new Ext.PagingToolbar({  
					store: schedule.details.storeStat2,
					displayInfo: true,  
					displayMsg: '{0} - {1} of {2} Registros',  
					emptyMsg: 'No hay Registros Disponibles',  
					pageSize: 50,
					items:[
					]
			}),
			columns: [	
				new Ext.grid.RowNumberer(),
				
				
				{header: 'Reason', width: 300, sortable: true, align: 'left', dataIndex: 'reason'},
				{header: 'Responsable', width: 80, sortable: true, align: 'left', dataIndex: 'execute'},
				{header: 'Date Assing', width: 100, sortable: true, align: 'right', dataIndex: 'dateAssing'},
				{header: 'type', width: 100, sortable: true, align: 'right', dataIndex: 'normal',renderer:function (val){
					if(val==1){
						return 'In Schedule';
					}
					else if(val==2){
						return 'Out Schedule';
					}
					else if(val==4){
						return 'By user';
					}
					else{
						return 'System';
					}
				}},
				{header: 'Value', width: 100, sortable: true, align: 'right', dataIndex: 'record',renderer:conversionTiempoDias},
			],
			viewConfig: {
				forceFit:true
			},
			loadMask:true,
		});
		
		schedule.details.detailsConexTab=new Ext.Panel({
										title	: 'Details', 
										closable: true,
										layout	: 'hbox',
										id		: 'detailsConexTab',
										tbar	:	[
					{
						xtype	:'button',
						iconCls	:'x-icon-add',
						text	: 'Add notification',
						handler:function (){
							if(schedule.details.addRecord){
								schedule.details.addRecord.win.show();
								var combo=schedule.details.detailsConexTab.getTopToolbar( ).findByType('combo');
								var combo2=schedule.details.addRecord.form.findByType('combo');
								combo2[0].setValue(combo[0].getValue());
							}
							else{
								schedule.details.addRecord={};
									schedule.details.addRecord.form = new Ext.form.FormPanel({
									frame : true,
									border :false,
									bodyStyle :{
										background : '#FFF'
									},
									padding: 10,
									layout:'form',
									
									items : [{
												xtype: 'combo',
												store: new Ext.data.SimpleStore({
													fields: ['userid', 'name'],
													data : Ext.combos_selec.dataUsersAdmin
												}),
												hiddenName: 'userid', 
												valueField: 'userid',
												displayField:'name',
												fieldLabel:	'User',
												allowBlank:false,
												value:schedule.details.userid,
												typeAhead: true,
												triggerAction: 'all',
												mode: 'local',
												selectOnFocus:true,
												 style: {
													fontSize: '16px'
												},
												width:200
											},
									{
										xtype	:'textarea',
										width	: 150,
										name	:'reason',
										fieldLabel	:'Reason',
									},
									{
										xtype:'combo',
										typeAhead: true,
										hiddenName :'signo',
										triggerAction: 'all',
										lazyRender:true,
										mode: 'local',
										value	:'+',
										store: new Ext.data.ArrayStore({
											fields: [
												'myId',
												'displayText'
											],
											data: [['+', 'Add'], ['-', 'Subtraction'], ['vacations', 'Vacations']]
										}),
										valueField: 'myId',
										fieldLabel	:'Type',
										displayField: 'displayText',
										listeners:{
											change:function (that, newValue, oldValue){
												if(newValue=='vacations'){
													that.findParentByType('form').findByType('textarea')[0].setValue('assigned day holiday');
													that.findParentByType('form').findByType('numberfield')[0].setVisible( false );
													that.findParentByType('form').findByType('numberfield')[1].setVisible( false );
													that.findParentByType('form').findByType('datefield')[1].setVisible( true );
													
												}
												else{
													that.findParentByType('form').findByType('textarea')[0].setValue('');
													that.findParentByType('form').findByType('numberfield')[0].setVisible( true );
													that.findParentByType('form').findByType('numberfield')[1].setVisible( true );
													that.findParentByType('form').findByType('datefield')[1].setVisible( false );
												}
											}
										},
										width:100
									},
									{
										xtype	:'datefield',
										name	:'date',
										format 	:'Y-m-d',
										value	: new Date(),
										fieldLabel	:'Date',
									},
									{
										xtype	:'datefield',
										name	:'date2',
										format 	:'Y-m-d',
										value	: new Date(),
										hidden	: true,
										fieldLabel	:'Date End'
									},
									{
										xtype	:'numberfield',
										name	:'hours',
										fieldLabel	:'Hours',
									},
									{
										xtype	:'numberfield',
										name	:'min',
										fieldLabel	:'Minutes',
									}
									],
									buttons:[{
											xtype 	: 'button',
											text	: 'Add',
											handler	: function ()
											{
												if(schedule.details.addRecord.form.getForm().isValid())
												{
													schedule.details.addRecord.form.getForm().submit({
														waitTitle:'Recording',
														waitMsg :'It is processing the registration...',
														url: 'php/funcionesControlConex.php',
														scope : this,
														params :{
															opcion  : 'recorduserUser',
															userid	: schedule.details.userid
														},
														success	: function (form,action)
														{
															schedule.details.addRecord.form.getForm().reset();
															schedule.details.addRecord.win.hide();
															var fechas=schedule.details.detailsConexTab.getTopToolbar( ).findByType('datefield');
															var combo=schedule.details.detailsConexTab.getTopToolbar( ).findByType('combo');
															fechas[0]= new Date(fechas[0].getValue( ));
															fechas[1]= new Date(fechas[1].getValue( ));
															schedule.details.storeStat.load({
																params:{
																	status:combo[1].getValue( ),
																	dateInit:fechas[0].format('Y-m-d') ,
																	dateEnd:fechas[1].format('Y-m-d') 
																}
															});
														}
													})
												}
											}
										},
										{
											xtype 	: 'button',
											text	: 'cancel',
											handler : function ()
											{
												schedule.details.addRecord.form.getForm().reset();
												schedule.details.addRecord.win.hide();
											}
										}]
								});
								
								schedule.details.addRecord.win= new Ext.Window({
									layout: 'fit', 
									title: 'Add notification', 
									closable: true,
									closeAction: 'hide',
									resizable: false,
									maximizable: false,
									plain: true,
									border: false,
									width: 350,
									height: 300,
									items	:[
										schedule.details.addRecord.form
									]
								});
								schedule.details.addRecord.win.show();	
							}
						}
					},'|',
					{xtype: 'tbspacer'},'User:',{
						xtype: 'combo',
						store: new Ext.data.SimpleStore({
							fields: ['userid', 'name'],
							data : Ext.combos_selec.dataUsersAdmin
						}),
						hiddenName: 'userid', 
						valueField: 'userid',
						displayField:'name',
						fieldLabel:	'User',
						allowBlank:false,
						value:userid,
						typeAhead: true,
						triggerAction: 'all',
						mode: 'local',
						selectOnFocus:true,
						 style: {
							fontSize: '16px'
						},
						width:200
					},{xtype: 'tbspacer'},'Between:',
					{
						xtype	: 'datefield',
						format	: 'Y-m-d',
						name	: 'BetweenI',
						value	: (schedule.detailsInitDate)?schedule.detailsInitDate:initSemana(),
					},'To',
					{
						xtype	: 'datefield',
						name	: 'BetweenE',
						format	: 'Y-m-d',
						value	: (schedule.detailsEndDate)?schedule.detailsEndDate:new Date()
					},{xtype: 'tbspacer'},'|','View:',
					{
						xtype:'combo',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						mode: 'local',
						value	:'1',
						store: new Ext.data.ArrayStore({
							fields: [
								'myId',
								'displayText'
							],
							data: [[1, 'All'], [2, 'Balance']]
						}),
						valueField: 'myId',
						displayField: 'displayText',
						width:50
					},
					{
						text	: 'filter',
						scope	: this,
						handler	: function (){
							var fechas=schedule.details.detailsConexTab.getTopToolbar( ).findByType('datefield');
							var combo=schedule.details.detailsConexTab.getTopToolbar( ).findByType('combo');
							fechas[0]= new Date(fechas[0].getValue( ));
							fechas[1]= new Date(fechas[1].getValue( ));
							schedule.details.userid=combo[0].getValue( );
							schedule.details.storeStat.load({
								params:{
									userid:schedule.details.userid,
									status:combo[1].getValue( ),
									dateInit:fechas[0].format('Y-m-d') ,
									dateEnd:fechas[1].format('Y-m-d') 
								}
							});
							schedule.details.storeStat2.load({
								params:{
									userid:schedule.details.userid,
									dateInit:fechas[0].format('Y-m-d') ,
									dateEnd:fechas[1].format('Y-m-d') 
								}
							});
							
							schedule.details.storeDesconect.load({
									params:{
										userid:schedule.details.userid,
										dateInit:fechas[0].format('Y-m-d') ,
										dateEnd:fechas[1].format('Y-m-d') 
									}
								});
						}
					}],
				id		: 'detailsConexion', 
				items:[{
					xtype	: 'panel',
					width	: 800, 
					height:Ext.getBody().getViewSize().height-100,
					items	: [
						schedule.details.gridStat,
						schedule.details.gridStat2
				]},{
					xtype	: 'panel',
					flex	: 1, 
					height:Ext.getBody().getViewSize().height-100,
					items	: [
						schedule.details.gridDesconect
				]}
				]
			});
	}
	schedule.details.userid=userid;
	schedule.details.detailsConexTab.getTopToolbar( ).findByType('combo')[0].setValue(userid);
	schedule.details.storeStat.load({
			params:{
				userid:schedule.details.userid,
				status:1,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		})
	schedule.details.storeStat2.load({
			params:{
				userid:schedule.details.userid,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		})
	schedule.details.storeDesconect.load({
			params:{
				userid:schedule.details.userid,
				dateInit:schedule.detailsInitDate ,
				dateEnd:schedule.detailsEndDate
			}
		});
	schedule.pag.findByType('tabpanel')[0].add(schedule.details.detailsConexTab).show();
}
function mostrarDetalleVacations(userid) {
	Ext.getCmp('gridVacation2').getStore().load({
		params:{
			userid: userid
			}
	});
}