var win;
	function mostrarDetalleUsuers(activo)
	{
		var url = 'php/obtenerHist.php?u='+activo;
		var ruta = 'php/details_users.php';
		Ext.Ajax.request(
		{
			waitMsg: 'Loading...',
			url: ruta,
			method: 'POST',
			timeout: 100000,
			params: {
				userid : activo,
				caso:'busca'
			},
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error Loading');
			},
			success:function(response){
			    Ext.QuickTips.init();
			    var variable = Ext.decode(response.responseText);

//************* Personal Information ********************************************************************************************
				this.personalinf = new Ext.FormPanel({
					title:'Personal Information',
					autoScroll: true,
					bodyStyle:'padding: 10px',
					items: [{
						layout:'column',
						border:false,
						items:[{
							layout: 'form',
							columnWidth:.5,
							border:false,
							items:[{
								xtype:'textfield',
								fieldLabel:'<font color="#FF0000"><b>BLACKLIST</b></font>',
								name:'bklist',
								id:'bklist',
						        style: 'font-weight:bold; color:#FF0000;background: #ffed7f;',
								value : 'BLACKLIST',
								hidden : true,
								width:200,
								readOnly:true
							},{
								xtype:'textfield',
								fieldLabel:'Name',
								name:'UD_name',
								id:'UD_name',
								value:variable.NAME,
								width:200,
								blankText: 'Required field, type your Name',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Email',
								name:'UD_email',
								id:'UD_email',
								value:variable.EMAIL,
								width:200,
								blankText: 'Required field, type your Email',
								allowBlank: false
							},{

								xtype:'textfield',
								fieldLabel:'Pseudonimo',
								name:'UD_pseudonimo',
								id:'UD_pseudonimo',
								value:variable.pseudonimo,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Hometelephone',
								name:'UD_hometelephone',
								id:'UD_hometelephone',
								value:variable.HOMETELEPHONE,
								width:200,
								blankText: 'Required field, type your Phone',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Country',
								name:'UD_country',
								id:'UD_country',
								value:variable.COUNTRY,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'City',
								name:'UD_city',
								id:'UD_city',
								value:variable.CITY,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Zipcode',
								name:'UD_zipcode',
								id:'UD_zipcode',
								value:variable.zipcode,
								width:200
							},{
								xtype:'numberfield',
								fieldLabel:'BrokerN',
								name:'UD_brokern',
								id:'UD_brokern',
								value:variable.BrokerN,
								width:200,
								maxLengthText: 'The field can not contain more than 10 digits',
								maxLength:10

							},{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsers
								}),
								fieldLabel:'Executive ',
								id: 'useridclients',
								name: 'useridclients',
								hiddenName: 'UD_executive',
								valueField: 'userid',
								displayField:'name',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								value:variable.executive,
								width: 200
							}/*,{
								xtype:'combo',
								store: new Ext.data.SimpleStore({
									fields: ['idstatus', 'texto'],
									data : Ext.combos_selec.dataStatus2
								}),
								fieldLabel:'Status',
								id:'idstatus',
								name:'idstatus',
								hiddenName: 'UD_idstatus',
								valueField: 'idstatus',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.status,
								width:200
							}*/,{
								xtype:'combo',
								store:new Ext.data.SimpleStore({
									fields: ['accept','texto'],
									data  : [
										['Y','Yes'],
										['N','No']
									]
								}),
								fieldLabel:'Accept',
								id:'accept',
								name:'accept',
								hiddenName: 'UD_accept',
								valueField: 'accept',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.accept,
								width:200
							}/*,{
								xtype:'combo',
								store:new Ext.data.SimpleStore({
									fields: ['mentoring','texto'],
									data  : [
										['Y','Yes'],
										['N','No']
									]
								}),
								fieldLabel:'Mentoring',
								id:'mentoring',
								name:'mentoring',
								hiddenName: 'UD_mentoring',
								valueField: 'mentoring',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value: variable.professional_esp,
								width:200
							}*/,{
								xtype:'combo',
								store:new Ext.data.SimpleStore({
									fields: ['questionuser','texto'],
									data  : [
										['What was the name of your elementary / primary school','What was the name of your elementary / primary school'],
										['What is the name of the company of your first job?','What is the name of the company of your first job?'],
										['What is your spouse\'s mother\'s maiden name?','What is your spouse\'s mother\'s maiden name?'],
										['To what city did you go on your honeymoon?','To what city did you go on your honeymoon?'],
										['Who was your childhood hero?','Who was your childhood hero?'],
										['What month and day is your anniversary? (e.g., January 2)','What month and day is your anniversary? (e.g., January 2)'],
										['What is your grandmother\'s first name?','What is your grandmother\'s first name?'],
										['What is your mother\'s middle name?','What is your mother\'s middle name?'],
										['What was the make and model of your first car?','What was the make and model of your first car?'],
										['In what city and country do you want to retire?','In what city and country do you want to retire?'],
										['What year did you graduate from High School?','What year did you graduate from High School?']
									]
								}),
								fieldLabel:'Question User',
								id:'questionuser',
								name:'questionuser',
								hiddenName: 'ud_questionuser',
								valueField: 'questionuser',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value: variable.questionuser,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Answer User',
								name:'answeruser',
								id:'answeruser',
								value:variable.answeruser,
								width:200//,
								//blankText: 'Required field, type your answer',
								//allowBlank: false
							}]
						},{
							layout: 'form',
							border:false,columnWidth:.5,
							items:[{
							   xtype:'textfield',
							   fieldLabel:'Surname',
							   name:'UD_surname',
							   id:'UD_surname',
							   value:variable.SURNAME,
							   width:200,
								blankText: 'Required field, type your Surname',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Passwd',
								name:'UD_passwd',
								id:'UD_passwd',
								value:variable.PASS,
								width:200,
								blankText: 'Required field, type your Password',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Affiliation Date',
								name:'UD_affiliationdate',
								id:'UD_affiliationdate',
								value:variable.AFFILIATIONDATE,
								width:200,
								readOnly:true
							},{
								xtype:'textfield',
								fieldLabel:'Mobiletelephone',
								name:'UD_mobiletelephone',
								id:'UD_mobiletelephone',
								value:variable.MOBILETELEPHONE,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'State',
								name:'UD_state',
								id:'UD_state',
								value:variable.STATE,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Address',
								name:'UD_address',
								id:'UD_address',
								value:variable.ADDRESS,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Procode',
								name:'UD_procode',
								id:'UD_procode',
								value:variable.procode,
								width:200,
								readOnly:true
							},{
								xtype:'numberfield',
								fieldLabel:'LicenseN',
								name:'UD_licensen',
								id:'UD_licensen',
								value:variable.LicenseN,
								width:200,
								maxLengthText: 'The field can not contain more than 10 digits',
								maxLength:10,

							},{
								xtype:'textfield',
								fieldLabel:'Officenum',
								name:'UD_officenum',
								id:'UD_officenum',
								value:variable.officenum,
								width:200,
								maxLength:45
							},{
								xtype:'combo',
								store: new Ext.data.SimpleStore({
									fields: ['idusertype', 'texto'],
									data : Ext.combos_selec.dataUType2
								}),
								fieldLabel:'User Type',
								id:'idusertype',
								name:'idusertype',
								hiddenName: 'UD_idusertype',
								valueField: 'idusertype',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.tipo,
								width:200
							},{
								xtype:'combo',
								store:new Ext.data.SimpleStore({
									fields: ['blackList','texto'],
									data  : [
										['Y','Yes'],
										['N','No']
									]
								}),
								fieldLabel:'Black List',
								id:'blackList',
								name:'blackList',
								hiddenName: 'UD_blackList',
								valueField: 'blackList',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.blackList,
								width:200
							} ,{
								xtype:'combo',
								store: new Ext.data.SimpleStore({
									fields: ['registertype', 'texto'],
									data :[
										[1,'Customer Register'],
										[2,'Partner Register'],
										[3,'Advertising Register'],
										[4,'Community Register'],
										[5,'Report Register'],
										[6,'Workshop Register']
									]
								}),
								fieldLabel:'Register Type',
								id:'registertype',
								name:'registertype',
								hiddenName: 'UD_registertype',
								valueField: 'registertype',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.idrtyp,
								width:200
							}]
						}]
					},{
						xtype:'textarea',
						fieldLabel:'Notes',
						id:'UD_notes',
						name:'UD_notes',
						value:variable.notes,
						anchor:'97%',
						height:100
					}],
					buttons: [{
						handler:function(){
						    if (this.personalinf.getForm().isValid()) {
								this.personalinf.getForm().submit({
									url :ruta,
									method :'POST',
									params: {
										userid : activo,
										caso : 'actualizar',
										UD_bandera: variable.status,
										original: variable.idrtyp
									},
									waitTitle   :'Please wait!',
									waitMsg     :'Loading...',
									success: function(form,action){
										Ext.Msg.alert('Success',action.result.msg);
										//store.reload();
										win.close();
									},
									failure: function(form,action){
										Ext.Msg.alert('Warning',action.result.msg);
									}
								});
						    }
						    else
							{
								Ext.Msg.show({
									title:'Warning',
                                    msg: 'Before proceeding you must complete the fields',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
								});
							}
						},
						scope:this,
						iconCls:'chgstat',
						text:'Update',
						id:'chgstat'
					}]
				});//Fin panel Personal Information
//************* Fin Personal Information ****************************************************************************************


//************* Permission ******************************************************************************************************
				this.permission = new Ext.FormPanel({
					title:'User Permissions',
					autoScroll: true,
					bodyStyle:'padding: 10px',
					items: [{
						layout:'column',
						border:false,
						items:[{
							layout: 'form',
							columnWidth:.5,
							border:false,
							items:[{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Residential',
								items:[{
									width:120,
									id:'det_residential',
									xtype:'combo',
									hiddenName: 'det_residential',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.residential == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Platinum',
								items:[{
									width:120,
									id:'det_platinum',
									xtype:'combo',
									hiddenName: 'det_platinum',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [99,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.platinum == 'Y') ? 1 : 99)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Professional',
								items:[{
									width:120,
									id:'det_professional',
									xtype:'combo',
									hiddenName: 'det_professional',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.professional == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Mentoring',
								items:[{
									width:120,
									id:'det_professional_esp',
									xtype:'combo',
									hiddenName: 'det_professional_esp',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.professional_esp == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: '<font color="#FF0000">Contact Premiun</font>',
								items:[{
									width:120,
									id:'det_contactpremiun',
									xtype:'combo',
									hiddenName: 'det_contactpremiun',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  [1,'Yes'],
										  [0,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: variable.contactpremiun
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: '<font color="#FF0000">Realtor Special</font>',
								items:[{
									width:120,
									id:'det_realtor_esp',
									xtype:'combo',
									hiddenName: 'det_realtor_esp',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  [1,'Yes'],
										  [0,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.realtor_esp == 'Y') ? 1 : 0)
								}]
							}]
						},{
							layout: 'form',
							border:false,columnWidth:.5,
							items:[{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Buyerspro',
								items:[{
									width:120,
									id:'det_buyerspro',
									xtype:'combo',
									hiddenName: 'det_buyerspro',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.buyerspro == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Leads Generator',
								items:[{
									width:120,
									id:'det_leadsgenerator',
									xtype:'combo',
									hiddenName: 'det_leadsgenerator',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.leadsgenerator == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Home Finder',
								items:[{
									width:120,
									id:'det_homefinder',
									xtype:'combo',
									hiddenName: 'det_homefinder',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.homefinder == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Realtor Web',
								items:[{
									width:120,
									id:'det_realtorweb',
									xtype:'combo',
									hiddenName: 'det_realtorweb',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.realtorweb == 'Y') ? 1 : 9)
								}]
							},{
								xtype: 'compositefield',
								labelWidth: 120,
								fieldLabel: 'Investor Web',
								items:[{
									width:120,
									id:'det_investorweb',
									xtype:'combo',
									hiddenName: 'det_investorweb',
									store: new Ext.data.SimpleStore({
									fields: ['id','texto'],
										data  : [
										  ['','-Select-'],
										  [1,'Yes'],
										  [9,'No']
										]
									}),
									editable: false,
									valueField: 'id',
									displayField: 'texto',
									typeAhead: true,
									mode: 'local',
									triggerAction: 'all',
									selectOnFocus: true,
									allowBlank: false,
									value: ((variable.investorweb == 'Y') ? 1 : 9)
								}]
							}]
						}]
					}],
					buttons: [{
						handler:function(){
						    if (this.permission.getForm().isValid()) {
								this.permission.getForm().submit({
									url :ruta,
									method :'POST',
									params: {
										userid : activo,
										caso : 'updatepermission'
									},
									waitTitle   :'Please wait!',
									waitMsg     :'Loading...',
									success: function(form,action){
										this.storelogsPermission.load();
										Ext.Msg.alert('Success',action.result.msg);
									},
									failure: function(form,action){
										Ext.Msg.alert('Warning',action.result.msg);
									}
								});
						    }
						},
						scope:this,
						iconCls:'chgstat',
						text:'Update'
					}]
				});////Fin panel Permission

				//--------Logs de Permission
				this.storelogsPermission = new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'userslogsPermission'},
					fields: [
						'userid',
						'detail',
						'fecha',
						'perm'
					]
				});
				this.storelogsPermission.load();

				this.gridlogsPermission = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					store: this.storelogsPermission,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Permission", width: 150, align: 'left', sortable: true, dataIndex: 'detail'},
						{header: "Operation", width: 100, align: 'Left', sortable: true, dataIndex: 'perm'},
						{header: "Update", width: 150, align: 'center', sortable: true, dataIndex: 'fecha'}
					],
					border: false,
					stripeRows: true
				});

				this.tablogsPermission  = new Ext.form.FormPanel({
					frame    : true,
					title    : 'Logs Permission',
					border: true,
					loadMask : true,
					layout:'column',
					autoScroll:true,
					items:[this.gridlogsPermission]
				});
				//-------- Fin Logs de Permission

				this.tabdetailPermission = new Ext.TabPanel({
					title    : 'Permission',
					activeTab: 0,
					enableTabScroll:true,
					height: 400,
					border : false,
					items:[this.permission,this.tablogsPermission]
				});

//************* Fin Permission **************************************************************************************************


//************* Credit Card *****************************************************************************************************
				this.creditcard  = new Ext.FormPanel({
					title:'Credit Card',
					autoScroll: true,
					bodyStyle:'padding: 10px',
					items: [{
						layout:'column',
						border:false,
						items:[{
							layout: 'form',
							columnWidth:.5,
							border:false,
							items:[{
								xtype:'combo',
								store:new Ext.data.SimpleStore({
									fields: ['nombrtarjeta','texto'],
									data  : [
										['none','-Select-'],
										['MasterCard','MasterCard'],
										['Visa','Visa'],
										['AmericanExpress','AmericanExpress'],
										['Discover','Discover']
									]
								}),
								fieldLabel:'Card Type',
								id:'UD_cardname',
								name:'UD_cardname',
								hiddenName: 'UD_cardname',
								valueField: 'nombrtarjeta',
								displayField: 'texto',
								triggerAction: 'all',
								mode: 'local',
								value:variable.cardname,
								width:200
							},{
								xtype:'textfield',
								fieldLabel:'Holder Name',
								name:'UD_cardholdername',
								id:'UD_cardholdername',
								value:variable.cardholdername,
								width:200,
								allowBlank: false
							},{

								xtype:'numberfield',
								fieldLabel:'Expiration Year',
								name:'UD_expirationyear',
								id:'UD_expirationyear',
								value:variable.expirationyear,
								width:200,
								blankText: 'Required field, type your Year',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Billing State',
								name:'UD_billingstate',
								id:'UD_billingstate',
								value:variable.billingstate,
								width:200,
								blankText: 'Required field, type your State',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Billing Address',
								name:'UD_billingaddress',
								id:'UD_billingaddress',
								value:variable.billingaddress,
								width:200,
								blankText: 'Required field, type your Address',
								allowBlank: false
							}]
						},{
							layout: 'form',
							border:false,columnWidth:.5,
							items:[{
							   xtype:'textfield',
							   fieldLabel:'Card Number',
							   name:'UD_cardnumber',
							   id:'UD_cardnumber',
							   value:variable.cardnumber,
							   width:200,
								blankText: 'Required field, type your Number',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'CVS Number',
								name:'UD_csvnumber',
								id:'UD_csvnumber',
								value:variable.csvnumber,
								width:200,
								allowBlank: false
							},{
								xtype:'numberfield',
								fieldLabel:'Expiration Month',
								name:'UD_expirationmonth',
								id:'UD_expirationmonth',
								value:variable.expirationmonth,
								width:200,
								blankText: 'Required field, type your Month',
								allowBlank: false
							},{
								xtype:'textfield',
								fieldLabel:'Billing City',
								name:'UD_billingcity',
								id:'UD_billingcity',
								value:variable.billingcity,
								width:200,
								blankText: 'Required field, type your City',
								allowBlank: false
							},{
								xtype:'numberfield',
								fieldLabel:'Billing Zip',
								name:'UD_billingzip',
								id:'UD_billingzip',
								value:variable.billingzip,
								width:200,
								allowBlank: false
							}]
						}]
					}],
					buttons: [{
						id:'update_tc',
						handler:function(){
						    if (this.creditcard .getForm().isValid()){
								this.creditcard .getForm().submit({
									url :ruta,
									method :'POST',
									params: {
										userid : activo,
										caso : 'tarjeta'
									},
									waitTitle   :'Please wait!',
									waitMsg     :'Loading...',
									success: function(form,action){
										Ext.Msg.alert('Success',action.result.msg);
										//store.reload();
										win.close();
									},
									failure: function(form,action){
										Ext.Msg.alert('Warning',action.result.msg);
									}
								});
						    }
						    else
							{
								Ext.Msg.show({
									title:'Warning',
                                    msg: 'Before proceeding you must complete the fields',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
								});
							}
						},
						scope:this,
						iconCls:'chgstat',
						tooltip:'Update the data in the Credit Card',
						text:'Update'
					}]
				});//Fin panel Credit Card
//************* Fin Credit Card *************************************************************************************************


//************* Notes ***********************************************************************************************************
				this.notes = new Ext.FormPanel({
					title:'Notes',
					autoScroll: true,
					border:false,
					bodyStyle:'padding: 10px',
					items: [{
						xtype:'textarea',
						fieldLabel:'Cancel',
						name:'UD_cancel',
						id:'UD_cancel',
						anchor:'100%',
						labelAlign: 'right',
						value:variable.cancel
					},{
						xtype:'textarea',
						fieldLabel:'Freeze',
						name:'UD_freeze',
						id:'UD_freeze',
						anchor:'100%',
						value:variable.freeze
					}],
					buttons: [{
						id:'boton',
						handler:function(){
							this.notes.getForm().submit({
								url :ruta,
								method :'POST',
								params: {
									userid : activo,
									caso : 'note'
								},
								waitTitle   :'Please Wait!',
								waitMsg     :'Loading...',
								success: function(form,action){
									Ext.Msg.alert('Success',action.result.msg);
									win.close();
								},
								failure: function(form,action){
									Ext.Msg.alert('Warning',action.result.msg);
								}
							});
						},
						scope:this,
						iconCls:'chgstat',
						tooltip:'Update the data in the Notes',
						text:'Update'
					}]
				});//Fin panel Notes
//************* Fin Notes *******************************************************************************************************


//************* Products ********************************************************************************************************
				this.storegridproducts = new Ext.data.JsonStore({
					url: ruta+'?userid='+activo+'&caso=usersproducts',
					root: 'data',
					fields: [
						'idcobros',
						'userid',
						'name',
						'surname',
						'idstatus',
						'status',
						'idproducto',
						'nameprod',
						'priceprod',
						{name: "fechacobro", type: "date",dateFormat: 'Y-m-d'},
						'freedays',
						'namefrec',
						'discount',
						'state',
						'counties'
					]
				});
				this.storegridproducts.load();

				this.storecounties = new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'userscounties',idproducto:0},
					fields: [
						'idcounty',
						'county',
						'editcounty',
						'idproducto'
					]
				});
				this.storecounties.load();

				function renderCounties(value, p, record){
					return String.format('<a href="javascript:void(0)" class="itemusers" title="Click to see the counties." >{0}</a> ',value);
				}
				function doEditProducts(oGrid_Event) {
					var gvaluenew = oGrid_Event.value;
					var gid = oGrid_Event.record.data.userid;
					var gidprod = oGrid_Event.record.data.idproducto;
					var gfield = oGrid_Event.field;
					var caso='changestatusprod';
					if(gfield=='fechacobro')
					{	gvaluenew = oGrid_Event.value.format('Y-m-d');
						caso='changepaydateprod';
					}
					//alert(gvaluenew+"|"+gid+"|"+gidprod+"|"+gfield+"|"+gstatusold);
					Ext.Ajax.request({
						url: ruta,
						method :'POST',
						params: {
							userid : gid,
							caso : caso,
							idproducto : gidprod,
							statusnew : gvaluenew
						},
						waitTitle   :'Please wait!',
						waitMsg     :'Saving changes...',
						success: function(respuesta){
							var resp = Ext.decode(respuesta.responseText);
							Ext.Msg.alert('Change Status',resp.msg);
							if(resp.success)
							{
								storecounties.setBaseParam('userid',activo);
								storecounties.setBaseParam('idproducto',0);
								storegridproducts.load();
								storecounties.load();
								this.panelprods.getForm().reset();
								Ext.getCmp('txprodamountp').setValue(Ext.util.Format.usMoney(resp.payamount));
								this.storelogsStatus.load();
							}
						}
					});
				};

				this.gridprods = new Ext.grid.EditorGridPanel({
					title:'Products',
					autoScroll: true,
					store: this.storegridproducts,
					bodyBorder: true,
					border: true,
					height   : 165,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Product", width: 110, align: 'left', sortable: true, dataIndex: 'nameprod'},
						{header: "Status", width: 70, align: 'left', sortable: true, dataIndex: 'status', editor: new Ext.form.ComboBox({
							store: new Ext.data.SimpleStore({
								fields: ['idstatus', 'texto'],
								data : Ext.combos_selec.dataStatus2
							}),
							fieldLabel:'Status',
							id:'idstatusprod',
							name:'idstatusprod',
							hiddenName: 'UD_idstatusprod',
							valueField: 'idstatus',
							displayField: 'texto',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local'
						})},
						{header: "Paydate", width: 70, align: 'left', sortable: true, dataIndex: 'fechacobro', renderer: Ext.util.Format.dateRenderer('Y-m-d'),editor: new Ext.form.DateField({ format: 'Y-m-d'})},
						{header: "Price", width: 60, align: 'right', sortable: true, dataIndex: 'priceprod', renderer : function(v){return Ext.util.Format.usMoney(v)}},
						{header: "Frecuency", width: 65, align: 'left', sortable: true, dataIndex: 'namefrec'},
						{header: "TrialDays", width: 55, align: 'center', sortable: true, dataIndex: 'freedays'},
						{header: "Discount", width: 60, align: 'center', sortable: true, dataIndex: 'discount'},
						{header: "State", width: 70, align: 'left', sortable: true, dataIndex: 'state'},
						{header: "Counties", width: 60, align: 'center', sortable: true, dataIndex: 'counties',renderer: renderCounties}
					],
					border: false,
					stripeRows: true,
					listeners : {
						'cellclick' : function(grid, rowIndex, cellIndex, e)
						{
							var store1 = grid.getStore().getAt(rowIndex);
							var columnName = grid.getColumnModel().getDataIndex(cellIndex);
							var cellValue = store1.get(columnName);
							///alert(columnName+"|"+cellValue+"|"+store1.data.userid+"|"+store1.data.idproducto);
							if(columnName=='counties')
							{
								storecounties.setBaseParam('userid',store1.data.userid);
								storecounties.setBaseParam('idproducto',store1.data.idproducto);
								storecounties.load();
							}
						}
					}
				});	//fin gridprods
				this.gridprods.addListener('afteredit', doEditProducts);

				this.gridcounties = new Ext.grid.EditorGridPanel({
					title:'Counties',
					autoScroll: true,
					store: this.storecounties,
					bodyBorder: true,
					border: true,
					height   : 165,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Id", width: 30, align: 'center', sortable: true, dataIndex: 'idcounty'},
						{header: "County", width: 90, align: 'left', sortable: true, dataIndex: 'county'},
						{header: "", width: 30, align: 'center', sortable: false, dataIndex: 'editcounty'}
					],
					border: false,
					stripeRows: true,
					listeners : {
						'cellclick' : function(grid, rowIndex, cellIndex, e)
						{
							var store1 = grid.getStore().getAt(rowIndex);
							var columnName = grid.getColumnModel().getDataIndex(cellIndex);
							var cellValue = store1.get(columnName);
							///alert(columnName+"|"+cellValue+"|"+store1.data.userid+"|"+store1.data.idproducto);
							if(columnName=='editcounty')
							{

								var formcountyedit = new Ext.form.FormPanel({
									autoScroll: true,
									bodyStyle:'padding: 10px',
									layout:'form',
									border:true,
									items: [{
										xtype: 'combo',
										store: new Ext.data.SimpleStore({
											fields: ['idcounty', 'name'],
											data : Ext.combos_selec.countiesshowed
										}),
										fieldLabel:'County',
										id: 'cbcountyedit',
										name: 'cbcountyedit',
										hiddenName: 'cbcountyedit',
										valueField: 'idcounty',
										displayField:'name',
										typeAhead: true,
										triggerAction: 'all',
										mode: 'local',
										selectOnFocus:true,
										emptyText : 'Select County',
										allowBlank : false,
										width: 200
									}],
									buttons: [{
										scope:this,
										id:'butchangeprod',
										iconCls:'chgstat',
										text:'<b>Change County</b>',
										handler:function(){
											if (formcountyedit .getForm().isValid()){
												formcountyedit .getForm().submit({
													url :ruta,
													method :'POST',
													params: {
														userid : activo,
														caso : 'changecounty',
														idproducto: store1.data.idproducto

													},
													waitTitle   :'Please Wait!',
													waitMsg     :'Loading...',
													success: function(form,action){
														Ext.Msg.alert('Success',action.result.msg);
														storecounties.setBaseParam('userid',activo);
														storecounties.setBaseParam('idproducto',store1.data.idproducto);
														storecounties.load();
														wincountyedit.close();
													},
													failure: function(form,action){
														Ext.Msg.alert('Warning',action.result.msg);
													}
												});
											}
										}
									}]
								});

								var wincountyedit = new Ext.Window({
									title:'Change County',
									width:380,
									layout: "fit",
									height: 120,
									border: false,
									autoScroll: true,
									modal: true,
									plain: true,
									items: formcountyedit
								});
								wincountyedit.show();
							}
						}
					}
				  });	//fin gridcounties

				var storeProducts= new Ext.data.JsonStore({
					url:'php/comboProducts.php',
					root: 'data',
					totalProperty: 'num',
					fields: ['idproducto','name']
				});
				var storeFrecuency= new Ext.data.JsonStore({
					url:'php/comboFrecuency.php',
					root: 'data',
					totalProperty: 'num',
					fields: ['idfrecuency','name']
				});
				var storeState= new Ext.data.JsonStore({
					url:'php/combostate.php',
					root: 'data',
					totalProperty: 'num',
					fields: ['IdState','State']
				});
				var storeCounty= new Ext.data.JsonStore({
					url:'php/combocounty.php',
					root: 'data',
					totalProperty: 'num',
					fields: ['IdCounty','County']
				});
				function selectCountryp(combo,record,index){
					Ext.getCmp('cbGrupoCountyp').clearValue();
					Ext.getCmp('cbGrupoCountyp').enable();
					storeCounty.load({params:{id:record.get('IdState')}});
					showamounttobepayp();
				}
				function showamounttobepayp(combo,record,index){
					var id=Ext.getCmp('cbGrupoProductsp').getValue();
					var frencuency=Ext.getCmp('cbGrupoFrencuencyp').getValue();
					var state=Ext.getCmp('cbGrupoStatep').getValue();
					var county=Ext.getCmp('cbGrupoCountyp').getValue();
					var promotionCode=Ext.getCmp('cbuseridclientsp').getValue();
					//alert(id+"|"+frencuency+"|"+state+"|"+county+"|"+promotionCode);
					if(id!='' && frencuency!='' && state!='' && county!='')
					{
						Ext.Ajax.request({
							url: 'php/properties.php',
							method :'POST',
							params: {
								id : id,
								frencuency : frencuency,
								state: state,
								county: county,
								promotionCode: promotionCode,
								caso: 'reactivateaccount'
							},
							waitTitle   :'Wait!',
							waitMsg     :'Please wait!..',
							success: function(respuesta){
								var resp = Ext.decode(respuesta.responseText);
								Ext.getCmp('prodamountp').setValue(Ext.util.Format.usMoney(resp.price));
							}
						});
					}
				}
				function actualizaSaldo(user){
					//alert(user);//return
					Ext.Ajax.request({
						url: ruta,
						method :'POST',
						params: {
							userid: user,
							caso: 'buscarSaldo'
						},
						waitTitle   :'Wait!',
						waitMsg     :'Please wait!..',
						success: function(respuesta){
							//alert("volvio: "+respuesta.responseText);//return

							var resp = Ext.decode(respuesta.responseText);
							Ext.getCmp('txaccbalance').setValue(Ext.util.Format.usMoney(resp.saldo));
						}
					});
				}

				/*
				this.panelprods = new Ext.FormPanel({
					frame:true,
					title: 'Products Change',
					bodyStyle:'padding:5px 5px 0',
					name: 'formdetprod',
					id: 'formdetprod',
					height: 160,
					items: [{
						layout:'column',
						border:false,
						items:[{
							layout: 'form',
							columnWidth:.5,
							border:false,
							items:[{
								xtype:'combo',
								store:storeProducts,
								fieldLabel:'Product',
								id:'cbGrupoProductsp',
								name:'cbGrupoProductsp',
								hiddenName: 'cbGrupoProductsp',
								valueField: 'idproducto',
								displayField: 'name',
								triggerAction: 'all',
								emptyText:'Select a product',
								allowBlank: false,
								width:250,
								listeners: {'select': showamounttobepayp}
							},{
								xtype:'combo',
								store:storeState,
								fieldLabel:'State',
								id:'cbGrupoStatep',
								name:'cbGrupoStatep',
								hiddenName: 'cbGrupoStatep',
								valueField: 'IdState',
								displayField: 'State',
								triggerAction: 'all',
								emptyText:'Select a state',
								allowBlank: false,
								width:250,
								listeners: {'select': selectCountryp}
							},{
								xtype: 'combo',
								store: new Ext.data.SimpleStore({
									fields: ['userid', 'name'],
									data : Ext.combos_selec.dataUsers
								}),
								fieldLabel:'Promotion Code',
								id: 'cbuseridclientsp',
								name: 'cbuseridclientsp',
								hiddenName: 'cbpexecutivep',
								valueField: 'userid',
								displayField:'name',
								typeAhead: true,
								triggerAction: 'all',
								mode: 'local',
								selectOnFocus:true,
								readOnly:true,
								value:variable.executive,
								width:250,
								listeners: {'select': showamounttobepayp}
							}]
						},{
							layout: 'form',
							border:false,columnWidth:.5,
							items:[{
								xtype:'combo',
								store:storeFrecuency,
								fieldLabel:'Frecuency',
								id:'cbGrupoFrencuencyp',
								name:'cbGrupoFrencuencyp',
								hiddenName: 'cbGrupoFrencuencyp',
								valueField: 'idfrecuency',
								displayField: 'name',
								triggerAction: 'all',
								emptyText:'Select a Frencuency',
								allowBlank: false,
								width:250,
								listeners: {'select': showamounttobepayp}
							},{
								xtype:'combo',
								store:storeCounty,
								fieldLabel:'County',
								id:'cbGrupoCountyp',
								name:'cbGrupoCountyp',
								hiddenName: 'cbGrupoCountyp',
								valueField: 'IdCounty',
								displayField: 'County',
								triggerAction: 'all',
								allowBlank: false,
								emptyText:'Select a county',
								allowBlank: false,
								mode: 'local',
								disabled: true,
								width:250,
								listeners: {'select': showamounttobepayp}
							},{
								xtype:'textfield',
								fieldLabel:'<font color="#FF0000"><b>Amount</b></font>',
								name:'prodamountp',
								id:'prodamountp',
								width:100,
								readOnly:true
							}]
						}]
					}],
					buttons: [{
						scope:this,
						text:'Suspend All Products',
						id:'butsuspendprod',
						handler:function(){
							Ext.Msg.prompt('CANCELLATION TOTAL OF PRODUCTS','Please enter the reason for the CANCELLATION of the user: <b>'+variable.NAME+' '+variable.SURNAME+'</b>', function(btn, text)
							{
								if (btn == 'ok'){
									Ext.Ajax.request({
										url: ruta,
										method :'POST',
										params: {
											userid : activo,
											caso : 'changestatusprod',
											msj: text
										},
										waitTitle   :'Please wait!',
										waitMsg     :'Saving changes...',
										success: function(respuesta){
											var resp = Ext.decode(respuesta.responseText);
											Ext.Msg.alert('Cancellation',resp.msg);
											if(resp.success)
											{
												storecounties.setBaseParam('userid',activo);
												storecounties.setBaseParam('idproducto',0);
												storegridproducts.load();
												storecounties.load();
												this.panelprods.getForm().reset();
												this.gstore.load();
											}
										}
									});
								}
							},this,true);
						}
					},{
						scope:this,
						iconCls:'chgstat',
						text:'<b>Update Product</b>',
						id:'chgstat',
						handler:function(){
							if (this.panelprods.getForm().isValid()) {
								this.panelprods.getForm().submit({
									url :ruta,
									method :'POST',
									params: {
										userid : activo,
										caso : 'updateproducts'
									},
									waitTitle   :'Please Wait!',
									waitMsg     :'Loading...',
									success: function(form,action){
										Ext.Msg.alert('Succes',action.result.msg);
										storecounties.setBaseParam('userid',activo);
										storecounties.setBaseParam('idproducto',0);
										storegridproducts.load();
										storecounties.load();
										this.panelprods.getForm().reset();
										Ext.getCmp('txprodamountp').setValue(Ext.util.Format.usMoney(action.result.payamount));
									},
									failure: function(form,action){
										Ext.Msg.alert('Warning',action.result.msg);
									}
								});
							}
							else {
								Ext.Msg.show({
									title:'Warning',
									msg: 'Before proceeding you must complete the fields',
									buttons: Ext.Msg.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
						}
					}]
				});
				*/

				var taballprods  = new Ext.form.FormPanel({
					frame    : true,
					title    : 'Products',
					border: true,
					loadMask : true,
					layout:'column',
					autoScroll:true,
					items:[{
						width: 870,
						baseCls:'x-plain',
						bodyStyle:'padding:5px 0 5px 5px',
						layout:'form',
						items:[{
							xtype: 'compositefield',
							fieldLabel:'<font color="#FF0000"><b>Pay Total</b></font>',
							items: [{
								xtype:'textfield',
								fieldLabel:'<font color="#FF0000"><b>Pay Total</b></font>',
								name:'txprodamountp',
								id:'txprodamountp',
								value: Ext.util.Format.usMoney(variable.precio_producto),
								width:100,
								readOnly:true
							},{
								xtype: 'displayfield',
								value:' ',
								width: 120
							},{
								xtype: 'displayfield',
								value:'<font color="#FF0000"><b>Account Balance:</b></font>',
								width: 120
							},{
								xtype:'textfield',
								name:'txaccbalance',
								id:'txaccbalance',
								value: Ext.util.Format.number(variable.saldo,'0.00'),
								width:100,
								readOnly:true
							},{
								xtype: 'button',
								icon:'images/editar.png',
								tooltip : 'Click to edit the Account Balance',
								listeners : {
									click : function()
									{
										var formbalanceedit = new Ext.form.FormPanel({
											autoScroll: true,
											bodyStyle:'padding: 10px',
											layout:'form',
											border:true,
											items: [{
												xtype:'numberfield',
												fieldLabel:'Account Balance',
												allowNegative:true,
												name:'txaccbalanceedit',
												id:'txaccbalanceedit',
												allowBlank: false,
												value: Ext.util.Format.number(variable.saldo,'0.00'),
												width:100
											}],
											buttons: [{
												scope:this,
												id:'butchangeprod',
												iconCls:'chgstat',
												text:'<b>Update</b>',
												handler:function(){
													if (formbalanceedit .getForm().isValid()){
														formbalanceedit .getForm().submit({
															url :ruta,
															method :'POST',
															params: {
																userid : activo,
																caso : 'changeaccbalance'
															},
															waitTitle   :'Please Wait!',
															waitMsg     :'Loading...',
															success: function(form,action){
																Ext.Msg.alert('Success',action.result.msg);
																winbalanceedit.close();
																actualizaSaldo(activo);
															},
															failure: function(form,action){
																Ext.Msg.alert('Warning',action.result.msg);
															}
														});
													}
												}
											}]
										});

										var winbalanceedit = new Ext.Window({
											title:'Change Account Balance',
											width:300,
											layout: "fit",
											height: 120,
											border: false,
											autoScroll: true,
											modal: true,
											plain: true,
											items: formbalanceedit
										});
										winbalanceedit.show();
									}
								}

							}]
						}]
					},{
						width: 660,
						baseCls:'x-plain',
						bodyStyle:'padding:5px 0 5px 5px',
						items:[this.gridprods]
					},{
						width: 200,
						baseCls:'x-plain',
						bodyStyle:'padding:5px 0 5px 5px',
						items:[this.gridcounties]
					},{
						width: 870,
						//baseCls:'x-plain',
						frame:true,
						title: '<strong>Products Change</strong>',
						bodyStyle:'padding:5px 0 5px 5px;',
						//items:[
						//this.panelprods


							items: [{
								layout:'column',
								border:false,
								items:[{
									layout: 'form',
									columnWidth:.5,
									border:false,
									items:[{
										xtype:'combo',
										store:storeProducts,
										fieldLabel:'Product',
										id:'cbGrupoProductsp',
										name:'cbGrupoProductsp',
										hiddenName: 'cbGrupoProductsp',
										valueField: 'idproducto',
										displayField: 'name',
										triggerAction: 'all',
										emptyText:'Select a product',
										allowBlank: false,
										width:250,
										listeners: {'select': showamounttobepayp}
									},{
										xtype:'combo',
										store:storeState,
										fieldLabel:'State',
										id:'cbGrupoStatep',
										name:'cbGrupoStatep',
										hiddenName: 'cbGrupoStatep',
										valueField: 'IdState',
										displayField: 'State',
										triggerAction: 'all',
										emptyText:'Select a state',
										allowBlank: false,
										width:250,
										listeners: {'select': selectCountryp}
									},{
										xtype: 'combo',
										store: new Ext.data.SimpleStore({
											fields: ['userid', 'name'],
											data : Ext.combos_selec.dataUsers
										}),
										fieldLabel:'Promotion Code',
										id: 'cbuseridclientsp',
										name: 'cbuseridclientsp',
										hiddenName: 'cbpexecutivep',
										valueField: 'userid',
										displayField:'name',
										typeAhead: true,
										triggerAction: 'all',
										mode: 'local',
										selectOnFocus:true,
										readOnly:true,
										value:variable.executive,
										width:250,
										listeners: {'select': showamounttobepayp}
									}]
								},{
									layout: 'form',
									border:false,columnWidth:.5,
									items:[{
										xtype:'combo',
										store:storeFrecuency,
										fieldLabel:'Frecuency',
										id:'cbGrupoFrencuencyp',
										name:'cbGrupoFrencuencyp',
										hiddenName: 'cbGrupoFrencuencyp',
										valueField: 'idfrecuency',
										displayField: 'name',
										triggerAction: 'all',
										emptyText:'Select a Frencuency',
										allowBlank: false,
										width:250,
										listeners: {'select': showamounttobepayp}
									},{
										xtype:'combo',
										store:storeCounty,
										fieldLabel:'County',
										id:'cbGrupoCountyp',
										name:'cbGrupoCountyp',
										hiddenName: 'cbGrupoCountyp',
										valueField: 'IdCounty',
										displayField: 'County',
										triggerAction: 'all',
										allowBlank: false,
										emptyText:'Select a county',
										allowBlank: false,
										mode: 'local',
										disabled: true,
										width:250,
										listeners: {'select': showamounttobepayp}
									},{
										xtype:'textfield',
										fieldLabel:'<font color="#FF0000"><b>Amount</b></font>',
										name:'prodamountp',
										id:'prodamountp',
										width:100,
										readOnly:true
									}]
								}]
							}],
							buttons: [{
								scope:this,
								text:'Suspend All Products',
								id:'butsuspendprod',
								handler:function(){
									Ext.Msg.prompt('CANCELLATION TOTAL OF PRODUCTS','Please enter the reason for the CANCELLATION of the user: <b>'+variable.NAME+' '+variable.SURNAME+'</b>', function(btn, text)
									{
										if (btn == 'ok'){
											Ext.Ajax.request({
												url: ruta,
												method :'POST',
												params: {
													userid : activo,
													caso : 'changestatusprod',
													msj: text
												},
												waitTitle   :'Please wait!',
												waitMsg     :'Saving changes...',
												success: function(respuesta){
													var resp = Ext.decode(respuesta.responseText);
													Ext.Msg.alert('Cancellation',resp.msg);
													if(resp.success)
													{
														storecounties.setBaseParam('userid',activo);
														storecounties.setBaseParam('idproducto',0);
														storegridproducts.load();
														storecounties.load();
														this.panelprods.getForm().reset();
														this.gstore.load();
													}
												}
											});
										}
									},this,true);
								}
							},{
								scope:this,
								iconCls:'chgstat',
								text:'<b>Update Product</b>',
								id:'chgstat',
								handler:function(){
									if (taballprods.getForm().isValid()) {
										taballprods.getForm().submit({
											url :ruta,
											method :'POST',
											params: {
												userid : activo,
												caso : 'updateproducts'
											},
											waitTitle   :'Please Wait!',
											waitMsg     :'Loading...',
											success: function(form,action){
												Ext.Msg.alert('Succes',action.result.msg);
												storecounties.setBaseParam('userid',activo);
												storecounties.setBaseParam('idproducto',0);
												storegridproducts.load();
												storecounties.load();
												taballprods.getForm().reset();
												Ext.getCmp('txprodamountp').setValue(Ext.util.Format.usMoney(action.result.payamount));
											},
											failure: function(form,action){
												Ext.Msg.alert('Warning',action.result.msg);
											}
										});
									}
									else {
										Ext.Msg.show({
											title:'Warning',
											msg: 'Before proceeding you must complete the fields',
											buttons: Ext.Msg.OK,
											icon: Ext.MessageBox.ERROR
										});
									}
								}
							}]


						//]
					}]
				});

				//--------Logs de products (usr_cobros)
				this.storelogsprod = new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'userslogsprod'},
					fields: [
						'userid',
						'name',
						'fechaupd',
						'fechacobro'
					]
				});
				this.storelogsprod.load();

				this.gridlogsprod = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					store: this.storelogsprod,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Product", width: 150, align: 'left', sortable: true, dataIndex: 'name'},
						{header: "Paydate", width: 100, align: 'center', sortable: true, dataIndex: 'fechacobro'},
						{header: "Update", width: 150, align: 'center', sortable: true, dataIndex: 'fechaupd'}
					],
					border: false,
					stripeRows: true
				});

				this.tablogsprods  = new Ext.form.FormPanel({
					frame    : true,
					title    : 'Logs Products',
					border: true,
					loadMask : true,
					layout:'column',
					autoScroll:true,
					items:[this.gridlogsprod]
				});
				//-------- Fin Logs de products (usr_cobros)

				//--------Logs de Status
				this.storelogsStatus = new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'userslogsStatus'},
					fields: [
						'userid',
						'fechaupd',
						'status',
						'userid_upd',
						'where'
					]
				});
				this.storelogsStatus.load();

				this.gridlogsStatus = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					store: this.storelogsStatus,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Status", width: 100, align: 'left', sortable: true, dataIndex: 'status'},
						{header: "Update", width: 130, align: 'center', sortable: true, dataIndex: 'fechaupd'},
						{header: "Update User", width: 150, align: 'left', sortable: true, dataIndex: 'userid_upd'},
						{header: "From", width: 250, align: 'left', sortable: true, dataIndex: 'where'}
					],
					border: false,
					stripeRows: true,
					clicksToEdit: 2
				});

				this.tablogsStatus  = new Ext.form.FormPanel({
					frame    : true,
					title    : 'Logs Status',
					border: true,
					loadMask : true,
					layout:'column',
					autoScroll:true,
					items:[this.gridlogsStatus]
				});
				//-------- Fin Logs de Status

				this.tabdetailProd = new Ext.TabPanel({
					title    : 'Products',
					activeTab: 0,
					enableTabScroll:true,
					height: 400,
					border : false,
					items:[taballprods,this.tablogsprods,this.tablogsStatus]
				});
//************* Find Products ***************************************************************************************************


//************* History *********************************************************************************************************

				this.reader = new Ext.data.JsonReader({
					successProperty	: 'success',
					messageProperty	: 'message',
					idProperty	: 'id',
					root		: 'data'
				},[
					{name: "fechgrup0", type: 'string'},
					{name: "fechatrans", type: "date",dateFormat: 'Y-m-d'},
					{name: "titulo", type: "string"},
					{name: "userid", type: "string"},
					{name: "name", type: "string"},
					{name: "surname", type: "string"},
					{name: "email", type: "string"},
					{name: "phone", type: "string"},
					{name: "paydate", type: "string"},
					{name: "status", type: "string"},
					{name: "usertype", type: "string"},
					{name: "priceprod", type: "float"},
					{name: "monto", type: "float"},
					{name: "transactionid", type: "string"},
					{name: "nrodocumento", type: "string"},
					{name: "cobradordesc", type: "string"},
					{name: "notas", type: "string"},
					{name: "idoper", type: "int"},
					{name: "paypalid", type: "int"},
					{name: "idtrans", type: "int"}
				]);

				this.gstore =new Ext.data.GroupingStore({
					url		: url+'&caso=arreglo',
					reader		: this.reader,
					sortInfo        : {field:'fechgrup0', direction:'ASC'},
					groupField	: 'fechgrup0'
				});
				this.gstore.load();

				var elemento = new Ext.ux.grid.GroupSummary();
				var textField = new Ext.form.TextField();

				function commenthistfull(val, p, record){
					var note = '';
					if (val!='' && val!=null){
						note = val.substring(0,0);
						return String.format('<img src="images/notes.png" border="0" ext:qtip="{0}">',val);
					}
				}

				function doEditStatement(oGrid_Event) {
					var gid = oGrid_Event.record.data.idtrans;
					var gidpaypal = oGrid_Event.record.data.paypalid;
					var gfield = oGrid_Event.field;
					var gvaluenew = oGrid_Event.value;
					if(gfield=='fechatrans')gvaluenew = oGrid_Event.value.format('Y-m-d');
					//if(oGrid_Event.record.data.idoper==5 || oGrid_Event.record.data.idoper==17)	{ Ext.MessageBox.alert('Warning',"Record can't edited"); return;}
					//alert(gvaluenew+"|"+gid+"|"+gfield);
					Ext.Ajax.request({
						url: ruta,
						method :'POST',
						params: {
							gid : gid,
							gidpaypal: gidpaypal,
							caso : 'changestatement',
							gfield : gfield,
							gvaluenew : gvaluenew
						},
						waitTitle   :'Please wait!',
						waitMsg     :'Saving changes...',
						success: function(respuesta){
							var resp = Ext.decode(respuesta.responseText);
							Ext.Msg.alert('Change Statement',resp.msg);
							if(resp.success)
								this.gstore.load();
						}
					});
				};

				this.historico= new Ext.grid.EditorGridPanel({
					store	: this.gstore,
					autoScroll: true,
					plugins	: elemento,
					title:"Statement of Account",
					defaults     : {
						width : 150
					},
					columns : [
						new Ext.grid.RowNumberer(),
						{
							header:"Month",
							dataIndex: "fechgrup0",
							hideable: false,
							sortable : true,
							groupable: true,
							width: 100
						},{
							id: 'idtrans',
							header		: "Date",
							width		: 80,
							dataIndex	: "fechatrans",
							sortable	: true,
							groupable	: true,
							summaryType	: "count",
							summaryRenderer: function(v, params){
								return ((v === 0 || v > 1) ? '(' + v +' Items)' : '(1 Item)');
							},
							renderer: Ext.util.Format.dateRenderer('Y-m-d'),
							editor: new Ext.form.DateField({ format: 'Y-m-d'})
						},{
							header	: "TransactionId",
							dataIndex	: "transactionid",
							width		: 130,
							editor:  new Ext.form.TextField()
						},{
							header		: "Transaction",
							dataIndex	: "titulo",
							width		: 200,
							sortable	: true,
							groupable	: true
						},{
							header		: "UserID",
							dataIndex	: "userid",
							sortable	: true,
							groupable	: true,
							hidden:true
						},{
							header		: "Name",
							dataIndex	: "name",
							width		: 100,
							sortable	: true,
							groupable	: true,
							hidden:true
						},{
							header		: "Surname",
							dataIndex	: "surname",
							width		: 100,
							sortable	: true,
							groupable	: true,
							hidden:true
						},{
							header		: "Docum. #",
							dataIndex	: "nrodocumento",
							groupable	: true,
							sortable	: true,
							width		: 130,
							editor: new Ext.form.TextField()
						},{
							header		: "Charged By",
							dataIndex	: "cobradordesc",
							groupable	: true,
							sortable	: true,
							width		: 165
						},{
							header		: "Pay Amount",
							width		: 115,
							dataIndex	: "priceprod",
							groupable	: true,
							sortable	: true,
							align: 'right',
							renderer: function(v, params, record){
								if(record.data.priceprod!='')
									return Ext.util.Format.usMoney(record.data.priceprod);
							},
							hidden:true
						},{
							header		: "Balance",
							width		: 115,
							dataIndex	: "monto",
							sortable	: true,
							align: 'right',
							groupable	: false,
							renderer: function(v, params, record){
								return Ext.util.Format.usMoney(record.data.monto);
							},
							editor:new Ext.form.NumberField()
						},{
							header		: " ",
							dataIndex	: "notas",
							groupable	: true,
							sortable	: true,
							align: 'center',
							width		: 40,
							renderer : commenthistfull
						}
					],
					view : new Ext.grid.GroupingView({
						//startCollapsed : true,
						forceFit:true,
						showGroupName: false,
						enableNoGroups:false,
						enableGroupingMenu:false,
						hideGroupedColumn: true
					}),
					sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
					clicksToEdit: 2,
					//collapsible: true,
					//animCollapse: false,
					trackMouseOver: false
				});		//Fin panel 7
				this.historico.addListener('afteredit', doEditStatement);

				//--------Logs de Histoico de cobros de paypa o authorize
				this.storehistorypay = new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'usershistorypay'},
					fields: [
						'userid',
						'estado',
						'transactionID',
						'amount',
						'fecha',
						'error',
						'description'
					]
				});
				this.storehistorypay.load();

				this.gridhistorypay = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					store: this.storehistorypay,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "Status", width: 100, align: 'left', sortable: true, dataIndex: 'estado'},
						{header: "TransactionID", width: 150, align: 'center', sortable: true, dataIndex: 'transactionID',editor:textField},
						{header: "Amount", width: 70, align: 'right', sortable: true, dataIndex: 'amount', renderer : function(v){return Ext.util.Format.usMoney(v)}},
						{header: "Date", width: 130, align: 'center', sortable: true, dataIndex: 'fecha'},
						{header: "Error Oper (Failure)", width: 300, align: 'left', sortable: true, dataIndex: 'error',editor:new Ext.form.TextArea()}
						,{header: "Parameter", width: 150, align: 'center', sortable: true, dataIndex: 'description',editor:new Ext.form.TextArea(),hidden:true}
					],
					border: false,
					stripeRows: true,
					clicksToEdit: 2
				});

				this.tabhistorypay  = new Ext.form.FormPanel({
					frame    : true,
					title    : 'History Pay',
					border: true,
					loadMask : true,
					layout:'column',
					autoScroll:true,
					items:[this.gridhistorypay]
				});
				//-------- Fin Logs de Histoico de cobros de paypa o authorize

				//--------Credits/Debits/Refund
				var storeComboCredits= new Ext.data.JsonStore({
					url:'php/cbcredrefdeb.php',
					root: 'data',
					totalProperty: 'num',
					fields: ['idoper', 'operation']
				});
				storeComboCredits.load();
				this.debcredref = new Ext.form.FormPanel({
					frame:true,
					title: 'Credits/Debits/Refund',
					bodyStyle:'padding:5px 5px 0',
					width: 500,
					defaults: {width: 250},
					items: [{
						id:'cbOperationdet',
						name:'cbOperationdet',
						hiddenName: 'cbOperationdet',
						xtype:'combo',
						fieldLabel: 'Operation',
						store: storeComboCredits,

						/*store: new Ext.data.SimpleStore({
							fields: ['idoper', 'operation'],
   							data :[
								['14','Debit'],
								['13','Credit'],
								['15','Refund'],
								['4','Bill Payment']
							]
						}),*/
						valueField: 'idoper',
						displayField: 'operation',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',
						selectOnFocus: true,
						allowBlank: false,
						emptyText:'Select'
					},{
						xtype:'datefield',
						fieldLabel: 'Date',
						id: 'txtFechaCobrodet',
						name: 'txtFechaCobrodet',
						allowBlank: false,
						format: 'Y-m-d',
						value: new Date()
					},{
						xtype:'numberfield',
						fieldLabel: 'Amount',
						allowBlank: false,
						allowDecimals: true,
						allowNegative: false,
						id: 'txtMontoCobrardet',
						name: 'txtMontoCobrardet'
					},{
						xtype:'textfield',
						fieldLabel: 'Document N�',
						id: 'txtNroDocumentodet',
						name: 'txtNroDocumentodet'
					},{
						xtype: 'textarea',
						fieldLabel: 'Notes',
						allowBlank: false,
						id: 'txtNotasdet',
						name: 'txtNotasdet'
					}],
					buttons: [{
						handler:function(){
						    if (this.debcredref.getForm().isValid()) {
								this.debcredref.getForm().submit({
									url :ruta,
									method :'POST',
									params: {
										userid : activo,
										caso : 'creddebrefund'
									},
									waitTitle   :'Please wait!',
									waitMsg     :'Loading...',
									success: function(form,action){
										Ext.Msg.alert('Success',action.result.msg);
										this.gstore.load();
										actualizaSaldo(activo);
										this.debcredref.getForm().reset();
									},
									failure: function(form,action){
										Ext.Msg.alert('Warning',action.result.msg);
									}
								});
						    }
						},
						scope:this,
						iconCls:'chgstat',
						text:'Update',
						id:'chgstat',
						disabled : enablebutcdr
					}]
				});
				//-------- Fin Credits/Debits/Refund

				this.tabdetailStatement = new Ext.TabPanel({
					title    : 'Statement',
					activeTab: 0,
					enableTabScroll:true,
					height: 400,
					border : false,
					items:[this.historico,this.tabhistorypay,this.debcredref]
				});

//************* Fin History *****************************************************************************************************


//************* Reactivate Account **********************************************************************************************
				function selectCountry(combo,record,index){
					Ext.getCmp('cbGrupoCounty').clearValue();
					Ext.getCmp('cbGrupoCounty').enable();
					storeCounty.load({params:{id:record.get('IdState')}});
					showamounttobepay();
				}
				function showamounttobepay(combo,record,index){
					var id=Ext.getCmp('cbGrupoProducts').getValue();
					var frencuency=Ext.getCmp('cbGrupoFrencuency').getValue();
					var state=Ext.getCmp('cbGrupoState').getValue();
					var county=Ext.getCmp('cbGrupoCounty').getValue();
					var promotionCode=Ext.getCmp('cbuseridclients').getValue();
					//alert(id+"|"+frencuency+"|"+state+"|"+county+"|"+promotionCode);
					if(id!='' && frencuency!='' && state!='' && county!='')
					{
						Ext.Ajax.request({
							url: 'php/properties.php',
							method :'POST',
							params: {
								id : id,
								frencuency : frencuency,
								state: state,
								county: county,
								promotionCode: promotionCode,
								caso: 'reactivateaccount'
							},
							waitTitle   :'Wait!',
							waitMsg     :'Please wait!..',
							success: function(respuesta){
								var resp = Ext.decode(respuesta.responseText);
								Ext.getCmp('prodamount').setValue(resp.price);
							}
						});
					}
				}

				this.reactivateaccount = new Ext.FormPanel({
					title:'Reactivate Acc.',
					autoScroll: true,
					border:false,
					bodyStyle:'padding: 10px',
					items: [{
						xtype: 'fieldset',
						title: 'Product Information',
						autoHeight: true,
						items: [{
							layout:'column',
							border:false,
							items:[{
								layout: 'form',
								columnWidth:.5,
								border:false,
								items:[{
									xtype:'combo',
									store:storeProducts,
									fieldLabel:'Product',
									id:'cbGrupoProducts',
									name:'cbGrupoProducts',
									hiddenName: 'cbGrupoProducts',
									valueField: 'idproducto',
									displayField: 'name',
									triggerAction: 'all',
									emptyText:'Select a product',
									allowBlank: false,
									width:200,
									listeners: {'select': showamounttobepay}
								},{
									xtype:'combo',
									store:storeState,
									fieldLabel:'State',
									id:'cbGrupoState',
									name:'cbGrupoState',
									hiddenName: 'cbGrupoState',
									valueField: 'IdState',
									displayField: 'State',
									triggerAction: 'all',
									emptyText:'Select a state',
									allowBlank: false,
									width:200,
									listeners: {'select': selectCountry}
								},{
									xtype: 'combo',
									store: new Ext.data.SimpleStore({
										fields: ['userid', 'name'],
										data : Ext.combos_selec.dataUsers
									}),
									fieldLabel:'Promotion Code',
									id: 'cbuseridclients',
									name: 'cbuseridclients',
									hiddenName: 'cbpexecutive',
									valueField: 'userid',
									displayField:'name',
									typeAhead: true,
									selectOnFocus:true,
									triggerAction: 'all',
									mode: 'local',
									value:variable.executive,
									width: 200,
									listeners: {'select': showamounttobepay}
								}]
							},{
								layout: 'form',
								border:false,columnWidth:.5,
								items:[{
									xtype:'combo',
									store:storeFrecuency,
									fieldLabel:'Frecuency',
									id:'cbGrupoFrencuency',
									name:'cbGrupoFrencuency',
									hiddenName: 'cbGrupoFrencuency',
									valueField: 'idfrecuency',
									displayField: 'name',
									triggerAction: 'all',
									emptyText:'Select a Frencuency',
									allowBlank: false,
									width:200,
									listeners: {'select': showamounttobepay}
								},{
									xtype:'combo',
									store:storeCounty,
									fieldLabel:'County',
									id:'cbGrupoCounty',
									name:'cbGrupoCounty',
									hiddenName: 'cbGrupoCounty',
									valueField: 'IdCounty',
									displayField: 'County',
									triggerAction: 'all',
									allowBlank: false,
									emptyText:'Select a county',
									allowBlank: false,
									mode: 'local',
									disabled: true,
									width:200,
									listeners: {'select': showamounttobepay}
								},{
									xtype:'textfield',
									fieldLabel:'<font color="#FF0000"><b>Amount</b></font>',
									name:'prodamount',
									id:'prodamount',
									width:100,
									readOnly:true
								}]
							}]
						}]

					},{
						xtype: 'fieldset',
						title: 'Credit Card',
						autoHeight: true,
						items: [{
							layout:'column',
							border:false,
							items:[{
								layout: 'form',
								columnWidth:.5,
								border:false,
								items:[{
									xtype:'combo',
									store:new Ext.data.SimpleStore({
										fields: ['cardname','texto'],
										data  : [
											['none','-Select-'],
											['MasterCard','MasterCard'],
											['Visa','Visa'],
											['AmericanExpress','AmericanExpress'],
											['Discover','Discover']
										]
									}),
									fieldLabel:'Card Type',
									id:'prodcardname',
									name:'prodcardname',
									hiddenName: 'prodcardname',
									valueField: 'cardname',
									displayField: 'texto',
									triggerAction: 'all',
									mode: 'local',
									value:variable.cardname,
									width:200
								},{
									xtype:'textfield',
									fieldLabel:'Holder Name',
									name:'prodcardholdername',
									id:'prodcardholdername',
									value:variable.cardholdername,
									width:200,
									allowBlank: false
								},{

									xtype:'numberfield',
									fieldLabel:'Expiration Year',
									name:'prodexpirationyear',
									id:'prodexpirationyear',
									value:variable.expirationyear,
									width:200,
									blankText: 'Required field, type your Year',
									allowBlank: false
								},{
									xtype:'textfield',
									fieldLabel:'Billing State',
									name:'prodbillingstate',
									id:'prodbillingstate',
									value:variable.billingstate,
									width:200,
									blankText: 'Required field, type your State',
									allowBlank: false
								},{
									xtype:'textfield',
									fieldLabel:'Billing Address',
									name:'prodbillingaddress',
									id:'prodbillingaddress',
									value:variable.billingaddress,
									width:200,
									blankText: 'Required field, type your Address',
									allowBlank: false
								}]
							},{
								layout: 'form',
								border:false,columnWidth:.5,
								items:[{
								   xtype:'textfield',
								   fieldLabel:'Card Number',
								   name:'prodcardnumber',
								   id:'prodcardnumber',
								   value:variable.cardnumber,
								   width:200,
									blankText: 'Required field, type your Number',
									allowBlank: false
								},{
									xtype:'numberfield',
									fieldLabel:'CVS Number',
									name:'prodcsvnumber',
									id:'prodcsvnumber',
									value:variable.csvnumber,
									width:200,
									allowBlank: false
								},{
									xtype:'numberfield',
									fieldLabel:'Expiration Month',
									name:'prodexpirationmonth',
									id:'prodexpirationmonth',
									value:variable.expirationmonth,
									width:200,
									blankText: 'Required field, type your Month',
									allowBlank: false
								},{
									xtype:'textfield',
									fieldLabel:'Billing City',
									name:'prodbillingcity',
									id:'prodbillingcity',
									value:variable.billingcity,
									width:200,
									blankText: 'Required field, type your City',
									allowBlank: false
								},{
									xtype:'numberfield',
									fieldLabel:'Billing Zip',
									name:'prodbillingzip',
									id:'prodbillingzip',
									value:variable.billingzip,
									width:200,
									allowBlank: false
								}]
							}]
						}]
					}],
					buttons: [{
						id:'botonreactiacco',
						disabled : true,
						handler:function(){
							Ext.Msg.confirm('Reactivate Account','Are you sure this is the user you want to reactivate??', function(btn){
								//alert(btn);return;
								if (btn == 'yes'){
									this.reactivateaccount.getForm().submit({
										url :ruta,
										method :'POST',
										params: {
											userid : activo,
											caso : 'reactivateaccount'
										},
										waitTitle   :'Please wait',
										waitMsg     :'Loading...!!!',
										success: function(form,action){
											Ext.Msg.alert('Success',action.result.msg);
											//store.reload();
											win.close();
										},
										failure: function(form,action){
											Ext.Msg.alert('Warning',action.result.msg);
										}
									});
								}
							});
						},
						scope:this,
						iconCls:'chgstat',
						text:'Reactivate Account'
					}]
				});//Fin panel  Reactivate Account
//************* Fin Reactivate Account ******************************************************************************************


//************* History Connections *********************************************************************************************
				this.element0 = new Ext.ux.grid.GroupSummary();

				this.lectura = new Ext.data.JsonReader({
				    successProperty	: 'success',
				    messageProperty	: 'message',
				    idProperty	: 'id',
				    root	: 'data',
				    fields: [
						{name: 'fech_grupo', type: 'int'},
						{name: 'sessiondate', type: 'string'},
						{name: 'login_ip', type: 'string'}
				    ]
				});
				this.gNstore =new Ext.data.GroupingStore({
					url: ruta+'?userid='+activo+'&caso=sesionip',
					reader: this.lectura,
					sortInfo: {field:'fech_grupo', direction:'ASC'},
					groupField	: 'fech_grupo'
				});
				this.gNstore.load();

				this.connections = new Ext.grid.EditorGridPanel({
					store	: this.gNstore,
					plugins	: this.element0,
					title:"Connections",
					columns : [
						new Ext.grid.RowNumberer(),
						{
							header:"Day",
							dataIndex: "fech_grupo",
							hideable: false,
							sortable : true,
							groupable: true,
							width: 100
						},{
							header: "Date Time",
							dataIndex	: "sessiondate",
							width: 295,
							sortable	: true,
							groupable	: true,
							summaryType	: "count",
							summaryRenderer: function(v, params){
								return ((v === 0 || v > 1) ? '(' + v +' Items)' : '(1 Item)');
							}
						},{
							header: "IP",
							dataIndex	: "login_ip",
							width: 295,
							sortable	: true,
							groupable	: true,
						}
					],
					view : new Ext.grid.GroupingView({
						forceFit:true,
						showGroupName: false,
						enableNoGroups:false,
						enableGroupingMenu:false,
						hideGroupedColumn: true
					}),
					sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
					clicksToEdit: 1,
					collapsible: true,
					animCollapse: false,
					trackMouseOver: false,
				});//Fin Panel 9
//************* Fin History Sessions ********************************************************************************************
//************* History Connections *********************************************************************************************
				this.recordTicket=new Ext.data.Record.create([
						{name: 'idcs', type: 'int'},
						{name: 'useridclient', type: 'int'},
						{name: 'cliente', type: 'string'},
						{name: 'useridprogrammer', type: 'int'},
						{name: 'programador', type: 'string'},
						{name: 'useridcustomer', type: 'int'},
						{name: 'mobile', type: 'string'},
						{name: 'id_product', type: 'int'},
						{name: 'productName', type: 'string'},
						{name: 'email', type: 'string'},
						{name: 'phone', type: 'string'},
						{name: 'customer', type: 'string'},
						{name: 'browser', type: 'string'},
						{name: 'os', type: 'string'},
						{name: 'state' , type: 'string'},
						{name: 'idcounty' , type: 'string'},
						{name: 'menutab' , type: 'string'},
						{name: 'submenutab' , type: 'string'},
						{name: 'protype' , type: 'string'},
						{name: 'errortype' , type: 'string'},
						{name: 'search' , type: 'string'},
						{name: 'errordescription' , type: 'string'},
						{name: 'status' , type: 'string'},
						{name: 'datel', type: 'date', dateFormat: 'Y-m-d H:i:s'},
						{name: 'dated', type: 'date', dateFormat: 'Y-m-d H:i:s'},
						{name: 'datef', type: 'date', dateFormat: 'Y-m-d H:i:s'},
						{name: 'datec', type: 'date', dateFormat: 'Y-m-d H:i:s'},
						{name: 'errortype2', type: 'string'},
						{name: 'soldescription' , type: 'string'},
						{name: 'msg_cliente' , type: 'int'},
						{name: 'msg_system' , type: 'int'},
						{name: 'ticket' , type: 'string'},
						{name: 'source' , type: 'string'}
				]);
				this.gridReaderTicket = new Ext.data.JsonReader({
					root: 'data',
					totalProperty: 'total',
					id: 'cad_id'},
					this.recordTicket
					);
				this.dataProxyTicket = new Ext.data.HttpProxy({
					url	: '../resources/php/funcionesMyTickets.php',   // Servicio web
					method	: 'POST'
				});

				this.dataStoreTicket = new Ext.data.Store({
					id: 'reports',
					proxy: this.dataProxyTicket,
					baseParams: {
						option: "listTickets",
						user:activo
					},
					reader: this.gridReaderTicket
				});
				tickets.userActive=activo;
				this.dataStoreTicket.load();
				this.columnModeTicket = new Ext.grid.ColumnModel(
					[
						new Ext.grid.RowNumberer(),
					{
						header: 'Ticket #',
						dataIndex: 'ticket',
						sortable: true,
						width: 80,
						renderer:function (val, metaData, record, rowIndex, colIndex, store){
							console.debug(val, metaData, record, rowIndex, colIndex, store);
							return '<a href="#" onClick="tickets.showDeatils('+rowIndex+')">'+val+'</a>';
						}
					},{
						header: 'Product',
						sortable: true,
						dataIndex: 'productName',
						width: 100
					},{
						header: 'Error',
						dataIndex: 'errordescription',
						width: 250
					},{
						header: 'Date',
						dataIndex: 'datel',
						width: 100,
						renderer: Ext.util.Format.dateRenderer('Y/m/d'),

					},{
						header: 'Status',
						sortable: true,
						dataIndex: 'status',
						width: 80
					}
					]
				);
				this.ticketsG = new Ext.grid.GridPanel({
					id	: 'gridTicketsGlobal',
					title	: 'Tickets',
					store	: dataStoreTicket,
					/*tbar: {
						defaults:{scope:this},
						items:[
							{
								xtype	: 'button',
								iconCls	: 'x-icon-add',
								handler	: tickets.new.init
							}
						]
					},*/
					cm	: columnModeTicket,
						viewConfig: {
							forceFit:true
						},
					enableColLock:false,
					layout: 'fit',
					view: new Ext.grid.GridView({
								forceFit: true,
								getRowClass : function (row, index) {
										var cls = '';
										var col = row.data;
										if(col.msg_system!='')
										{
											//cls = 'red-row'
										}

										 return cls;
									  }
						   }),
			listeners: {
			//	'rowcontextmenu' : monitorig.menuText,
				'rowdblclick' : tickets.showDeatils
			}
					/*
					bbar:new Ext.Toolbar({
						width: '100%',
						items: [
								'->', // same as {xtype: 'tbfill'}, // Ext.Toolbar.Fill

							]
						}),* /
					tbar:new Ext.Toolbar({
						width: '100%',
						items: [{
									xtype: 'button', // default for Toolbars, same as 'tbbutton'
									text: 'New Ticket',
									scale :'large',
									iconCls :'x-icon-add-large',
									handler: function (){
											var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=800, top=85, left=140";
											window.open('../register/recordTicket.php',"",opciones);
											//window.open(,'New Ticket');
										}
								},'|',{
												xtype	: 'combo',
												id:'comboStatus',
												fieldLabel:'Status'
												,displayField:'name'
												,value :''
												,name:'status'
												,valueField:'id'
												,store: new Ext.data.SimpleStore({
													 fields:['id', 'name']
													,data:[
														['','All'],
														['not in ("Close")', 'Active'],
														['in ("Close")', 'Inactive']
													]
												})
												,width: 70
												,triggerAction:'all'
												,mode:'local'
											},
								{
									xtype: 'button', // default for Toolbars, same as 'tbbutton'
									text: 'Filter',
									iconCls: 'x-icon-filter',
									handler: function (){

										dataStore.reload({params:{
												status:Ext.getCmp('comboStatus').getValue()
											}});
									}
								},'|',{
									xtype: 'button', // default for Toolbars, same as 'tbbutton'
									text: 'Help!',
									scale :'large',
									iconCls :'x-icon-help-large',
									handler: function (){
											var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=800, top=85, left=140";
											window.open('../resources/pdf/Tickets-Help.pdf',"",opciones);
										}
								}
							]
						}),
					applyTo:'gridReport',
					selModel: new Ext.grid.RowSelectionModel({singleSelect:false})  ,
							listeners: {
							//	'rowcontextmenu' : monitorig.menuText,
								'rowdblclick' : showDeatils
							}*/
				});

				//Fin Panel 9
//************* Fin History Sessions ********************************************************************************************

//************* Inicio Logs user full ********************************************************************************************
				this.storeloguser= new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'loguserfull'},
					fields: [
						'idlog',
						'source',
						'target',
						'query',
						'operation',
						'insertdate',
						'php'
					]
				});
				this.storeloguser.load();

				this.gridloguserfull = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					title	: 'Logs Full',
					store: this.storeloguser,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "UID", width: 60, align: 'center', sortable: true, dataIndex: 'idlog'},
						{header: "Source User", width: 170, align: 'left', sortable: true, dataIndex: 'source'},
						{header: "Operation", width: 100, align: 'left', sortable: true, dataIndex: 'operation'},
						{header: "Date", width: 120, align: 'left', sortable: true, dataIndex: 'insertdate'},
						{header: "PHP", width: 200, align: 'left', sortable: true, dataIndex: 'php'},
						{header: "Query", width: 200, align: 'left', sortable: true, dataIndex: 'query',editor:new Ext.form.TextArea()}
					],
					border: false,
					stripeRows: true,
					clicksToEdit: 2
				});

//************* Fin Logs user full ********************************************************************************************

//************* Inicio Assistant user ********************************************************************************************
				this.storeassistantuser= new Ext.data.JsonStore({
					url: ruta,
					root: 'data',
					baseParams  :{userid:activo, caso:'assistantuser'},
					fields: [
						'iduas',
						'name',
						'surname',
						'userid',
						'idstatus',
						'insertdate',
						'email'
					]
				});
				this.storeassistantuser.load();

				this.gridassistantuser = new Ext.grid.EditorGridPanel({
					autoScroll: true,
					title	: 'Assistant User',
					store: this.storeassistantuser,
					bodyBorder: true,
					border: true,
					height   : 370,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: "UID", width: 60, align: 'center', sortable: true, dataIndex: 'iduas'},
						{header: "Name", width: 100, align: 'left', sortable: true, dataIndex: 'name'},
						{header: "Last Name", width: 100, align: 'left', sortable: true, dataIndex: 'surname'},
						{header: "Status", width: 150, align: 'left', sortable: true, dataIndex: 'idstatus'},
						{header: "Date", width: 120, align: 'left', sortable: true, dataIndex: 'insertdate'},
						{header: "Email", width: 150, align: 'left', sortable: true, dataIndex: 'email'}
					],
					border: false,
					stripeRows: true,
					clicksToEdit: 2
				});

//************* Fin Logs user full ********************************************************************************************

				this.tabdetailUser = new Ext.TabPanel({
					activeTab: 0,
					enableTabScroll:true,
					height: 530,
					border : false,
					items:[this.personalinf,this.creditcard ,this.notes,this.tabdetailProd,this.tabdetailPermission,
						this.tabdetailStatement,this.reactivateaccount,this.ticketsG,this.gridloguserfull,this.gridassistantuser ]
				});

				this.win = new Ext.Window({
					title:'<H1 align="center">('+variable.USERID+') '+variable.NAME+' '+variable.SURNAME+'<H1>',
					width:900,
					layout: "fit",
					height:600,
					border: false,
					autoScroll: true,
					modal: true,
					plain: true,
					//bodyStyle: 'background-color:#fff;',
					items: this.tabdetailUser
				});
				this.win.show();

				new Ext.ToolTip({
					target: 'chgstat',
					anchor: 'bottom',
					html: 'Update the Data'
				});

				//activacion de componentes para la Ractivate Account
				if(variable.status==2 || variable.status==1 )
				{
					Ext.getCmp('cbGrupoProducts').setDisabled(false);
					Ext.getCmp('cbuseridclients').setDisabled(false);
					Ext.getCmp('cbGrupoState').setDisabled(false);
					Ext.getCmp('cbGrupoFrencuency').setDisabled(false);
					Ext.getCmp('prodcardname').setDisabled(false);
					Ext.getCmp('prodcardholdername').setDisabled(false);
					Ext.getCmp('prodexpirationyear').setDisabled(false);
					Ext.getCmp('prodbillingstate').setDisabled(false);
					Ext.getCmp('prodbillingaddress').setDisabled(false);
					Ext.getCmp('prodcardnumber').setDisabled(false);
					Ext.getCmp('prodcsvnumber').setDisabled(false);
					Ext.getCmp('prodexpirationmonth').setDisabled(false);
					Ext.getCmp('prodbillingcity').setDisabled(false);
					Ext.getCmp('prodbillingzip').setDisabled(false);
					Ext.getCmp('botonreactiacco').setDisabled(false);
				}
				else
				{
					Ext.getCmp('cbGrupoProducts').setDisabled(true);
					Ext.getCmp('cbuseridclients').setDisabled(true);
					Ext.getCmp('cbGrupoState').setDisabled(true);
					Ext.getCmp('cbGrupoFrencuency').setDisabled(true);
					Ext.getCmp('prodcardname').setDisabled(true);
					Ext.getCmp('prodcardholdername').setDisabled(true);
					Ext.getCmp('prodexpirationyear').setDisabled(true);
					Ext.getCmp('prodbillingstate').setDisabled(true);
					Ext.getCmp('prodbillingaddress').setDisabled(true);
					Ext.getCmp('prodcardnumber').setDisabled(true);
					Ext.getCmp('prodcsvnumber').setDisabled(true);
					Ext.getCmp('prodexpirationmonth').setDisabled(true);
					Ext.getCmp('prodbillingcity').setDisabled(true);
					Ext.getCmp('prodbillingzip').setDisabled(true);
				}

				if(variable.blackList=='Y')
				{
					Ext.getCmp('bklist').setVisible(true);
				}
			}
		})
	}//fin funcion mostrarDetalleUsuers(activo)
