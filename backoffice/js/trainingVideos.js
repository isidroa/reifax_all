Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data.php?tipo=training',
		fields: [
			{name: 'id', type: 'int'}
			,'nombre'
			,'url'
			,'activo'
			,'filename'
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
        
        items:[
			{
				id: 'add_butt',
                tooltip: 'Click to Add Video',
				iconCls:'icon',
				icon: 'images/add.png',
                handler: doAdd 
			},{
				id: 'del_butt',
                tooltip: 'Delete selected Videos',
				iconCls:'icon',
				icon: 'images/delete.gif',
                handler: doDel
			}
		]
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////
	function doAdd(){
		var simple = new Ext.FormPanel({
			labelWidth: 100, 
			frame:true,
			fileUpload : true,
			title: 'Upload Training Video',
			bodyStyle:'padding:5px 5px 0',
			items: [{
					xtype		:'textfield',
					id			:'name',
					name		:'name',
					fieldLabel	: 'Name',
					allowBlank	:false,
					width		: 250
				},{
					xtype       :'fileuploadfield',
					id          :'videoupload',
					emptyText   :'Select video',
					fieldLabel  :'Upload Video',
					name        :'videoupload',
					buttonText  : 'Browse...',
					allowBlank	:false
				}
			],
	
			buttons: [{
				text: 'Upload',
				handler  : function(){
						if(simple.getForm().isValid()) {
							simple.getForm().submit({
								url     : 'php/uploadvideo.php',
								waitMsg : 'Saving...',
								success : function(f, a){ 
									var resp    = a.result;
									if (resp.mensaje == 'Video saved successfully!') {
										wind.close();
										store.load({params:{start:0, limit:200}});
									}
									Ext.MessageBox.alert('', resp.mensaje);
								},
								failure : function(response, o){ 
								
								}	
							});
						}	
                    }
			},{
				text: 'Cancel',
				handler  : function(){
                        simple.getForm().reset();
						wind.close();
                    }
			}]
		});
		wind = new Ext.Window({
			title		: 'New Training Video',
			iconCls		: 'x-icon-settings',
			layout      : 'fit',
			width       : 400,
			height      : 220,
			resizable   : false,
			modal	 	: true,
			plain       : true,
			items		: simple
		});
				
		wind.show();
	}
	
	function doDel(){
		Ext.MessageBox.confirm('Delete', 'Confirm deletion?', function(btn){
			if (btn == 'no'){
				return false;
			}
			var com;
			var j=0;
			var eliminar="(";
			var arrayRecords = store.getRange(0,store.getCount());
			for(i=0;i<arrayRecords.length;i++){
				com="del"+arrayRecords[i].get('id');
				
				if(document.getElementById(com.toString()).checked==true){
					if(j>0) eliminar+=',';
					
					eliminar+=arrayRecords[i].get('id');
					j++;
				}
			}
			eliminar+=")";
			if(j==0)
			{	Ext.MessageBox.alert('Warning','You must select a row, by clicking on it, for the delete to work.');
				return;
			}
			Ext.Ajax.request({   
				waitMsg: 'Saving changes...',
				url: 'php/grid_del.php', 
				method: 'POST',
				params: {
					ID: eliminar,
					tipo: 'training',
					key: 'id'		
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error deleting');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
					store.reload();
				}
			}); 
		});
	}
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		if(fieldValue=='BEGIN')
		{Ext.MessageBox.alert('Warning','Error select BEGIN');store.reload();return;}
		//submit to server
		Ext.Ajax.request( 
			{   
				waitMsg: 'Saving changes...',
				url: 'php/grid_edit.php', 
				method: 'POST',
				params: {
					tipo: "training", 
					key: 'id',
					keyID: oGrid_Event.record.data.id,
					field: oGrid_Event.field,
					value: fieldValue,
					originalValue: oGrid_Event.record.modified
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','Error editing');
					store.rejectChanges();
				},
				
				success:function(response,options){
					var rest = Ext.util.JSON.decode(response.responseText);
					
					if(rest.succes==false)
						Ext.MessageBox.alert('Warning',rest.msg);
						
					store.commitChanges();
					store.reload();
				}
			 }
		);  
	}; 
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function renderTopic(value, p, record){
		var urlvideo = 'http://www.reifax.com/training/videoTraining.php/videoTraining.php?video='+value;
		return String.format(
				'<a href="{0}" title="Click to view video." target="_blank">View</a>',urlvideo);
	}
	function checkVideoDelete(val,p,record){
		return String.format('<input type="checkbox" name="del'+val+'"  id="del'+val+'">',val);
	}
	function renderActive(val,p,record){
		if(val==1){
			return 'Yes';
		}else{
			return 'No';
		}
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 new Ext.grid.RowNumberer()
			,{header: '', width: 50, sortable: true, align: 'center', dataIndex: 'id', renderer: checkVideoDelete}
			,{id:'id',header: "ID", width: 50, align: 'center', sortable: true, dataIndex: 'id'}
			,{header: "Name", width: 150, align: 'left', sortable: true, dataIndex: 'nombre',editor: new Ext.form.TextField({allowBlank: false})}
			,{header: "Filename", width: 150, align: 'left', sortable: true, dataIndex: 'filename'}
			,{header: 'Url server', width: 250, sortable: true, align: 'left', dataIndex: 'url'}
			,{header: 'Url Video', width: 80, sortable: true, align: 'center', dataIndex: 'id', renderer: renderTopic}
			,{header: 'Active', width: 80, sortable: true, align: 'center', dataIndex: 'activo',renderer: renderActive,editor: new Ext.form.ComboBox({
				   typeAhead: true,
				   triggerAction: 'all',
				   store: new Ext.data.SimpleStore({
								fields: ['id', 'tipo'],
								data : [
								['1', 'Yes'],
								['0', 'No']
							]
						}),
				   mode: 'local',
				   triggerAction: 'all',
				   editable: false,
				   selectOnFocus:true,
				   displayField:'tipo',
				   valueField: 'id',
				   listClass: 'x-combo-list-small'
				})}			
		],
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:470,
		width: screen.width,//'99.8%',
		frame:true,
		title:'Training Videos',
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
});