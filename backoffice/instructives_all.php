<?php
include ("php/checkuser.php");
include("php/conexion.php");
$conex=conectar("xima");
$userid = $_SESSION['bkouserid'];
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instructives </title>
<script>
var wherepage='Instructive General';//variable que dice de que pagina se esta ejecutando
</script>
<link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/gridstyles.css" rel="stylesheet" type="text/css" media="screen">
<?php include("php/enablebuttons.php");?>
<link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="includes/ext/examples/ux/css/MultiSelect.css"/>
<link rel="stylesheet" type="text/css" href="includes/ext/examples/shared/examples.css" />

<style type="text/css">

.cellComplete .x-grid3-cell-inner {
    white-space:normal !important;
}
.videoActive{
    background:#E4FFE1;
}
.videoInactive{
    background:#CCC;
}
.rowGrey{
    background-color: #F78181;

}
</style>
    <script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/GroupSummary.js"></script>
    <script type="text/javascript" src="js/userdetails.js"></script>
<!--<script type="text/javascript" src="js/menu.js"></script>-->
    <?php include("php/menubackoffice.php");?>
    <script type="text/javascript" src="js/instructives/instructives_all.js"></script>
    <script type="text/javascript" src="js/tickets.js"></script>

    <script type="text/javascript" src="includes/ext/examples/ux/MultiSelect.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/ItemSelector.js"></script>

    <script type="text/javascript" src="includes/ext/examples/multiselect/multiselect-demo.js"></script>
   <!-- <script type="text/javascript" src="includes/ext/examples/shared/examples.js"></script>-->
<script>

var userid=<?php  echo $_SESSION['bkouserid']; ?>;
Ext.namespace('Ext.combos_selec');
var typeHelp=1;


Ext.combos_selec.dataStatus = [
<?php
    $res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
    $i=0;
    echo "['','SELECT']";
    while($r=mysql_fetch_array($res))
        echo ",[".$r['idstatus'].",'".$r['status']."']";
?>];
Ext.combos_selec.dataStatus2 = [
<?php
    $res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
    $i=0;
    while($r=mysql_fetch_array($res))
    {
        if($i>0)echo ",";
        echo "[".$r['idstatus'].",'".$r['status']."']";
        $i++;
    }
?>];

Ext.combos_selec.dataUsersProgramm = [//Usuarios con idusertype 7
 <?php
    $res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` where (idusertype in (1,4,7) and idstatus in (3,4)) OR userid in (20,73) ORDER BY name ASC")or die(mysql_error());
    $i=0;
    while($r=mysql_fetch_array($res))
    {
        if($i>0)echo ",";
        echo "[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
        $i++;
    }
 ?>];

  Ext.combos_selec.dataTopics = [//Usuarios con idusertype 7
 <?php
    $res=mysql_query("SELECT * FROM `xima`.`instructives_topics_groups`")or die(mysql_error());
    $i=0;
    while($r=mysql_fetch_array($res))
    {
        if($i>0)echo ",";
        echo "[".$r['idtg'].",'".$r['topics']."']";
        $i++;
    }
 ?>];
 
Ext.combos_selec.dataUType = [
<?php
    $res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
    $i=0;
    echo "['','SELECT']";
    while($r=mysql_fetch_array($res))
        echo ",[".$r['idusertype'].",'".$r['usertype']."']";
?>];
Ext.combos_selec.dataUType2 = [
<?php
    $res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
    $i=0;
    while($r=mysql_fetch_array($res))
    {
        if($i>0)echo ",";
        echo "[".$r['idusertype'].",'".$r['usertype']."']";
        $i++;
    }
?>];
 Ext.combos_selec.dataUsers = [
 <?php
    $res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` ORDER BY name")or die(mysql_error());
    $i=0;
    echo "['','NONE']";
    while($r=mysql_fetch_array($res))
        echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
 Ext.combos_selec.dataUsers2 = [
 <?php
    $res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` ORDER BY userid")or die(mysql_error());
    $i=0;
    echo "['','NONE']";
    while($r=mysql_fetch_array($res))
        echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].") ']";
 ?>];
 Ext.combos_selec.dataUsersAdmin = [
 <?php
    $res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` where idusertype in (1,4,7) ORDER BY name")or die(mysql_error());
    $i=0;
    echo "['','NONE']";
    while($r=mysql_fetch_array($res))
        echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
   Ext.combos_selec.countiesshowed = [
 <?php
    $res=mysql_query("SELECT l.`IdCounty`, l.`County` FROM xima.lscounty l WHERE l.`is_showed`=1 ORDER BY l.`IdCounty`;")or die(mysql_error());
    $i=0;
    while($r=mysql_fetch_array($res))
    {
        if($i>0)echo ",";
        echo "[".$r['IdCounty'].",'".$r['County']."']";
        $i++;
    }
 ?>];


 </script>
</head>
<body>
</body>
</html>