<?php	

include ("php/checkuser.php");  
include("php/conexion.php");
include("php/conexionAWSRC.php");
$conex=conectar("xima");

?>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Access->MySQL Control</title>

    <link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
	<?php 	include("php/enablebuttons.php");?>
    <link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
    <script src="includes/exporter/Exporter-all.js" type="text/javascript"></script>
	
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
<style>
	.row0{ background: #f7f7f7; }
	.row1{ background:#cdf2ff; }
	.row2{ background:#d1d1ff; }
	.row3{ background:#ddffdd; }
	.row4{ background:#fffed1; }
	.row5{ background:#edeccd; }
	.row6{ background:#fed2ff; }
	.row7{ background:#e0c2c2; }
	.row8{ background:#fbbbbb; }
	.row9{ background:#ffd9b3; }
	.row10{ background:#c2e0e0; }
	.row11{ background:#ffecd9; }
	.row12{ background:#efefef; }
</style>
	
</head>
<body>
<?php

	$actions=$_GET['a'];
	
	$conexaux=conexionAWSRC("mantenimiento");
	$que="SELECT a.`orden`, a.`titulo`, a.`actionid`, a.`tipocampo`
				FROM mantenimiento.action a
				where  a.`actionid` in ($actions)
				order by a.`actionid`";
			$arractions = array();
			$result=mysql_query ($que,$conexaux) or die ($que.mysql_error ());
			while ($row=mysql_fetch_array($result)) $arractions[] = $row;	
//echo "<pre>";print_r($arrdata);	echo "</pre>";	
?>
<style type="text/css">
.x-grid3-row-alt{
	background-color:#e1e1e1;
}
</style>

</body>
</html>
<script>
 
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////

		
								
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_dataaccesscontrol.php?tipo=comact&a=<?php echo $actions ?>', timeout: 3600000 }),
		fields: [
			{name: 'idcounty', type: 'int'},
			{name: 'county'},
			{name: 'fecha'}
			<?php  
			for($i=0;$i<count($arractions);$i++)
				//echo ", 'act".($i+1)."'";
				echo ",{name: 'act".($i+1)."', type: 'float'}";
			?>
			//'act1',			'act2'
		]
	});
	
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 5000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display" ,
		items:[/*{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'add_butt',
			text: 'Add',
			tooltip: 'Click to Add Action Id',
			handler: doActionId
        }*/] 
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////

//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
	function render1(val,p,record){
		//if(val.indexOf('-') <0)	val=val.toFixed(2);
		//return String.format('{0}',val);
		return val.toFixed(2);
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
			store: store,
			iconCls: 'icon',
			columns: [	  
				 {header: "IdCty", width: 50, align: 'center', sortable: true, dataIndex: 'idcounty'}
				,{header: "County", width: 120, align: 'left', sortable: true, dataIndex: 'county'}
				,{header: "Date", width: 80, align: 'center', sortable: true, dataIndex: 'fecha'}
				<?php 
				for($i=0;$i<count($arractions);$i++)
					echo ",{header: '(".$arractions[$i]['actionid'].") ".str_replace("'",'',$arractions[$i]['titulo'])."', tooltip: '".str_replace("'",'',$arractions[$i]['titulo'])."', width: 100, align: 'right', sortable: true, dataIndex: 'act".($i+1)."',renderer: render1}";//
				?>
				//,{header: "Action1", width: 100, align: 'right', sortable: true, dataIndex: 'act1'}
				//,{header: "Action2", width: 100, align: 'right', sortable: true, dataIndex: 'act2'}				
			],
			clicksToEdit:2,
			autoScroll: true,
			height: Ext.getBody().getViewSize().height-50,
			frame:true,
			title:'Compare Actions',
			tbar   : [],
			loadMask:true
		});
		//tbar: pagingBar 
//Create the Download button and add it to the top toolbar
        var exportButton = new Ext.ux.Exporter.Button({
          component: grid,
          text     : "Download as .xls"
        });
        
        grid.getTopToolbar().add(exportButton);		
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	//grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
 
});
</script>