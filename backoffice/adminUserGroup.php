<?php
include ("php/checkuser.php");
include("php/conexion.php");
$conex=conectar("admin");



?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>User Group</title>
<script>
var wherepage='training';//variable que dice de que pagina se esta ejecutando
</script>
<link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" type="text/css" href="css/superboxselect.css" />
<link rel="stylesheet" type="text/css" href="includes/uploadExtjs/file-upload.css"/>
<?php 	include("php/enablebuttons.php");?>
<link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="css/Ext.ux.grid.RowActions.css" />
<link rel="stylesheet" type="text/css" href="includes/ext/examples/ux/css/GroupSummary.css" />

<style type="text/css">

.cellComplete .x-grid3-cell-inner {
	white-space:normal !important;
}
.colorVerde {
	background-color: #B0FFC5;
}
.colorRojo {
	background-color: #FFB0C4;
}
.x-grid3-locked, .x-grid3-unlocked {
    overflow: hidden;
    position: absolute;
}

.x-grid3-locked {
    border-right: 1px solid #99BBE8;
}

.x-grid3-locked .x-grid3-scroller {
    overflow: hidden;
}

.x-grid3-locked .x-grid3-row {
    border-right: 0;
}

.x-grid3-scroll-spacer {
    height: 19px;
}

.x-grid3-unlocked .x-grid3-header-offset {
    padding-left: 0;
}

.x-grid3-unlocked .x-grid3-row {
    border-left: 0;
}
</style>
 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
	<script type="text/javascript" src="includes/ext/examples/ux/LockingGridView.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/GroupSummary.js"></script>
	<script type="text/javascript" src="/includes/jquery.js"></script>
	<script type="text/javascript" src="js/ZeroClipboard.js"></script>
	<script type="text/javascript" src="js/SuperBoxSelect.js"></script>
	<script type="text/javascript" src="js/Ext.ux.grid.RowActions.js"></script>
	<script type="text/javascript" src="js/Ext.ux.Toast.js"></script>
	<script type="text/javascript" src="includes/uploadExtjs/FileUploadField.js"></script>
	<script type="text/javascript" src="js/userdetails.js"></script>
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
	<script type="text/javascript" src="js/userGroup.js"></script>
	<script type="text/javascript" src="js/tickets.js"></script>
	<!-- JQuery -->
        <script type="text/javascript" language="javascript" src="/includes/jquery.js"></script>

	<!--Functions Edit PDF's-->
		<script>
			if (typeof Range.prototype.createContextualFragment == "undefined") {
			    Range.prototype.createContextualFragment = function (html) {
			        var doc = window.document;
			        var container = doc.createElement("div");
			        container.innerHTML = html;
			        var frag = doc.createDocumentFragment(), n;
			        while ((n = container.firstChild)) {
				  frag.appendChild(n);
			        }
			        return frag;
			    };
			}
		</script>
		<!-- System Global Functions By Jesus-->
    <script type="text/javascript" src="/includes/globalFunctions.js?<?php echo filemtime(dirname(__FILE__).'/../includes/globalFunctions.js'); ?>"></script>

		<script src="/custom_contract/includes/js/kineticNew.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/includes/js/kineticNew.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/compatibility.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/compatibility.js'); ?>"></script>
		<!-- In production, only one script (pdf.js) is necessary -->
		<!-- In production, change the content of PDFJS.workerSrc below ->
		<script type="text/javascript" src="/custom_contract/src/core.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/core.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/util.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/util.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/api.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/api.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/canvas.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/canvas.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/obj.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/obj.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/function.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/function.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/charsets.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/charsets.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/cidmaps.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/cidmaps.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/colorspace.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/colorspace.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/crypto.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/crypto.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/evaluator.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/evaluator.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/fonts.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/fonts.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/glyphlist.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/glyphlist.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/image.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/image.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/metrics.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/metrics.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/parser.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/parser.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/pattern.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/pattern.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/stream.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/stream.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/worker.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/worker.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/external/jpgjs/jpg.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/external/jpgjs/jpg.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/jpx.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/jpx.js'); ?>"></script-->

		<script type="text/javascript" src="/custom_contract/src/shared/util.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/shared/util.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/api.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/api.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/metadata.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/metadata.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/canvas.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/canvas.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/webgl.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/webgl.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/pattern_helper.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/pattern_helper.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/font_loader.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/font_loader.js'); ?>"></script>
		<script type="text/javascript" src="/custom_contract/src/display/annotation_helper.js?<?php echo filemtime(dirname(__FILE__).'/../custom_contract/src/display/annotation_helper.js'); ?>"></script>

		<style type="text/css">
			.search-item{border:1px solid #fff;padding:3px;background-position:right bottom;background-repeat:no-repeat;}
			.desc{padding-right:10px;}
			.name{font-size:16px !important;color:#000022;}

			li:hover .subMenuHelp{
				visibility:visible;
			}
			li:hover .textHelpMenu{
				display:none;
			}
			.subMenuHelp{
				visibility:hidden;
				position:relative;
				border: 1px solid #888888;
				box-shadow: 0 0 2px;
				position: relative;
			}
			.subMenuHelp li{
				float:none;
				display:block;
				padding:3px 5px;
			}

			.subMenuHelp li:hover{
				background:#D0FF9D;
				color:#212121;
			}

			.subMenuHelp li:hover a{
				color:#212121;
			}
		</style>

		<script type="text/javascript">
			// Specify the main script used to create a new PDF.JS web worker.
			// In production, change this to point to the combined `pdf.js` file.
			PDFJS.workerSrc = '/custom_contract/src/worker_loader.js?v=2810';
		</script>

        <!--El Siguiente JS es para el Control de los Navegadores-->
        <script type="text/javascript" src="/custom_contract/includes/js/navegadores.js"></script>
	<!--END Functions Edit PDF's-->
<script>
var userid=<?php  echo $_SESSION['bkouserid']; ?>;
Ext.namespace('Ext.combos_selec');
Ext.combos_selec.dataStatus = [
<?php
	$res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
	$i=0;
	echo "['','SELECT']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['idstatus'].",'".$r['status']."']";
?>];
Ext.combos_selec.dataStatus2 = [
<?php
	$res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['idstatus'].",'".$r['status']."']";
		$i++;
	}
?>];
Ext.combos_selec.dataUType = [
<?php
	$res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
	$i=0;
	echo "['','SELECT']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['idusertype'].",'".$r['usertype']."']";
?>];
Ext.combos_selec.dataUType2 = [
<?php
	$res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['idusertype'].",'".$r['usertype']."']";
		$i++;
	}
?>];
 Ext.combos_selec.dataUsers = [
 <?php
	$res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` ORDER BY name")or die(mysql_error());
	$i=0;
	echo "['','NONE']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
 Ext.combos_selec.dataUsers2 = [
 <?php
	$res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` ORDER BY userid")or die(mysql_error());
	$i=0;
	echo "['','NONE']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].") ']";
 ?>];
 Ext.combos_selec.dataUsersAdmin = [
 <?php
	$res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` where idusertype in (1,4,7) or userid in (437,611) ORDER BY name")or die(mysql_error());
	$i=0;
	echo "['','NONE']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
 </script>
</head>
<body>
</body>
</html>
