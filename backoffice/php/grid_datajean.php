<?php
	session_start();	

	$bkouserid=$_SESSION['bkouserid'];
	if(strlen($bkouserid)==0)$bkouserid=$_COOKIE['bkouseridc'];
	
	
function GetDays($sStartDate, $sEndDate){
// Firstly, format the provided dates.
// This function works best with YYYY-MM-DD
// but other date formats will work thanks
// to strtotime().
$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

// Start the variable off with the start date
$aDays[] = $sStartDate;

// Set a 'temp' variable, sCurrentDate, with
// the start date - before beginning the loop
$sCurrentDate = $sStartDate;
$cont=0;
// While the current date is less than the end date
while($sCurrentDate < $sEndDate){
// Add a day to the current date
$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
$cont++;
}

// Once the loop has finished, return the
// array of days.
return $cont;
}
	
	include("../../properties_getprecio.php");
	include("conexion.php");
	$conex=conectar("xima");
	
	$modulo=$_GET['tipo'];
	if(strlen($modulo)==0)$modulo=$_POST['tipo'];
	$start=$_POST['start'];
	$limit=$_POST['limit'];
	switch($modulo){
		case 'foreclosurereleased':
			require("conexionS9.php");
			$cS9=conectarS9("xima");
			$bd=$_POST['bd'];
			if(strlen($bd)==0)$bd='flbroward';
			$que="	SELECT SQL_CALC_FOUND_ROWS 
						p.`idpen`, p.`parcelid`, p.`attorney`, p.`file_date`, p.`judgeamt`, 
						p.`judgedate`, p.`insertdate`, date(p.`checkdate`) checkdate, p.`compare`, p.`reinsertdate` 
					FROM $bd.pendesdelete p 
					ORDER BY p.`idpen` 
					LIMIT ".$start.",".$limit;
			$result=mysql_query ($que,$cS9) or die ($que.mysql_error ());
			$data = array();
			
			while ($row=mysql_fetch_object($result))
				$data [] = $row;
				
			echo '({"total":"'.mysql_num_rows($result).'","results":'.json_encode($data).'})';
			return;
		break;
		case 'getpendes':

			$bd=$_POST['bd'];
			$parcelid=$_POST['parcelid'];
			
			include("conexionS6.php");
			include("conexionS7.php");
			include("conexionS9.php");
			$conex	=	conectarS9("mantenimiento");	//Server Master
			$query	=	"SELECT Servidor FROM xima.lscounty WHERE bd='$bd'";
			$result	=	mysql_query($query,$conex) or die("{success:false, msg:'ERROR :".__LINE__."  --  ".mysql_error($conex)."'}");
			$result	=	mysql_fetch_assoc($result);
			switch($result['Servidor']){
				case "6":
					$conex=conectarS6("mantenimiento");
					break;
				case "7":
					$conex=conectarS7("mantenimiento");
					break;
				case "9":
					$conex=conectarS9("mantenimiento");
					break;
			}
			
			$que="	SELECT 
						p.`parcelid`, p.`attorney`, p.`file_date`, p.`judgeamt`, p.`judgedate`,DATE_ADD(CURDATE(),INTERVAL 4 MONTH) checkdate  
					FROM $bd.pendes p 
					WHERE  p.`parcelid`='$parcelid'";
			$result=mysql_query ($que,$conex) or die("{success:false, msg:'ERROR :".__LINE__."  --  ".mysql_error($conex)."'}");
			$valor = mysql_fetch_array($result);
					
			echo json_encode(array(
				success			=> true,
				msg			=> 'OK',
				attorney	=> $valor['attorney'].' ',
				filedate	=> $valor['file_date'].' ',
				judgeamt    => $valor['judgeamt'].' ',
				judgedate   => $valor['judgedate'].' ',
				checkdate   => $valor['checkdate']
				)
			);
			return;			
		break;
		case 'deudores':// deudores activos y freeze 			
			/*$que="SELECT x1.userid,x1.name,x1.surname,s.status,t.usertype,0 as priceprod, 0 amountsinformato,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					c.cardname,c.cardnumber,n.notes,d.saldo		
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (3,6,1) and d.saldo<0
					ORDER BY  day(paydate) desc,x1.userid";*/
					
			 $que="SELECT distinct u.userid,x1.name,x1.surname,
						(
							SELECT distinct s1.status
							FROM usr_cobros u1
							INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
							INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
							WHERE u1.`userid`=u.`userid` and pb1.idproducto not in (11,12,13)
							ORDER BY s1.status
							limit 1
						) status,
						(
							SELECT distinct u1.fechacobro
							FROM usr_cobros u1
							WHERE u1.`userid`=u.`userid` 
							limit 1
						) paydated,
						(SELECT f.`idfrecuency` FROM xima.f_frecuency f WHERE f.`userid`=u.`userid`  limit 1) idfrec,
						(
							SELECT concat(c.`cobradordesc`,' (',date(c.`fechatrans`),')') 
							FROM xima.cobro_estadocuenta c
							WHERE c.`userid`=u.userid AND c.`idoper`=5
							ORDER BY c.`idtrans` desc
							LIMIT 1
						) lastpay,
						x1.blacklist,
						0 as priceprod,
						c.cardname,
						d.saldo,
						0 amountsinformato,
						'' notes,
						(SELECT if(DATE_SUB(CURDATE(),INTERVAL 60 DAY)>=sessiondate,'Y','N')
						FROM logssession u where u.userid=x1.`userid` order by sessiondate desc limit 1) as marcalogin,
						(SELECT count(*) FROM logssession u where u.userid=x1.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate) as veceslog
				FROM usr_cobros u
				INNER JOIN xima.ximausrs x1 ON u.userid=x1.userid
				INNER JOIN xima.cobro_porcentajes d ON d.userid=u.userid
				INNER JOIN xima.creditcard c ON c.userid=u.userid
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				WHERE u.idstatus IN (3,6,1) and p.pu=0 and d.saldo<0
				ORDER BY  day(paydated) desc,x1.userid";
			$result=mysql_query ($que) or die ($que.mysql_error ());

			$data = array();			
			while ($row=mysql_fetch_object($result))
			{
				$que="SELECT distinct n.notes				
					FROM xima.usernotes n 
					WHERE n.notes<>'' and n.userid=".$row->userid;				
				$result2=mysql_query ($que) or die ($que.mysql_error ());
				$row2=mysql_fetch_object($result2);
				$row->notes=$row2->notes;

				if($row->status<>'Suspended')$row->priceprod=number_format(getpriceuser($row->userid,0,'yes'),2,'.',',');	
				$row->amountsinformato=$row->saldo;				
				$row->saldo=number_format($row->saldo,2,'.',',');				
				$data [] = $row;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
		break;
		
		case 'deudoresinactivos':// deudores inacactivos y freezeinac 	
			/* $que="SELECT x1.userid,x1.name,x1.surname,s.status,t.usertype,0 as priceprod, 0 amountsinformato,
					(SELECT DISTINCT DAYOFMONTH(fechacobro) FROM xima.usr_cobros u WHERE u.userid=x1.userid) dia,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					(SELECT concat(date(y.fecha),'. ',y.errornum,'. ',y.errormsj,'. ',y.errormsjl)  FROM xima.cobro_paypal y WHERE  y.userid=x1.userid and y.estado='Failure' oRDER BY paypalid desc LIMIT 1) paypalerror,
					c.cardname,c.cardnumber,n.notes,d.saldo		
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (5,7) and d.saldo<0
					ORDER BY  dia, x1.userid";*/
			 $que="SELECT distinct u.userid,x1.name,x1.surname,
						(
							SELECT distinct s1.status
							FROM usr_cobros u1
							INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
							INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
							WHERE u1.`userid`=u.`userid`  and pb1.idproducto not in (11,12,13)
							ORDER BY s1.status
							limit 1
						) status,
						(
							SELECT distinct u1.fechacobro
							FROM usr_cobros u1
							WHERE u1.`userid`=u.`userid` 
							limit 1
						) paydated,
						(SELECT f.`idfrecuency` FROM xima.f_frecuency f WHERE f.`userid`=u.`userid`  limit 1) idfrec,
						x1.blacklist,
						0 as priceprod,
						c.cardname,
						d.saldo,
						0 amountsinformato,
						'' notes,
						'' dia,
						(SELECT concat(date(y.fecha),'. ',y.errornum,'. ',y.errormsj,'. ',y.errormsjl)  FROM xima.cobro_paypal y WHERE  y.userid=x1.userid and y.estado='Failure' oRDER BY paypalid desc LIMIT 1) paypalerror,
						(SELECT count(*) FROM logssession u where u.userid=x1.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate) as veceslog
				FROM usr_cobros u
				INNER JOIN xima.ximausrs x1 ON u.userid=x1.userid
				INNER JOIN xima.cobro_porcentajes d ON d.userid=u.userid
				INNER JOIN xima.creditcard c ON c.userid=u.userid
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				WHERE u.idstatus IN (5,7) and p.pu=0 and d.saldo<0
				ORDER BY  day(paydated),x1.userid";
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data = array();			
			while ($row=mysql_fetch_object($result))
			{
				$que="SELECT distinct n.notes				
					FROM xima.usernotes n 
					WHERE n.notes<>'' and n.userid=".$row->userid;				
				$result2=mysql_query ($que) or die ($que.mysql_error ());
				$row2=mysql_fetch_object($result2);
				$row->notes=$row2->notes;

				if($row->status<>'Suspended')$row->priceprod=number_format(getpriceuser($row->userid,0,'yes'),2,'.',',');	
				$row->amountsinformato=$row->saldo;				
				$row->saldo=number_format($row->saldo,2,'.',',');	
				$row->dia=date('d',strtotime($row->paydated));					
				$data [] = $row;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
			
		break;
		
		case 'cobrosrealizados':// cobros realizados en el dia
			$dayfrom=$dayto=date('Y-m-d');
			
			$idtrasaction=$_POST['idtrasaction'];
			if(isset($_POST['dayfrom']) && $_POST['dayfrom']<>'' && strlen($_POST['dayfrom'])>0 )
			{ 
				$dayfrom=$_POST['dayfrom'];
				$dayto=$_POST['dayto'];
			}
			
			$qwhe='';						
			if($idtrasaction=='')
				$qwhe=" AND date(h.fechatrans) BETWEEN '$dayfrom' AND '$dayto' ";
			else
				$qwhe=" AND p.transactionid='$idtrasaction' ";

			 /*$que="SELECT distinct x1.userid,x1.name,x1.surname,0 as priceprod,0 amountsinformato,p.paypalid,
					c.cardholdername,c.cardnumber,n.notes,p.fecha,p.transactionid,p.amount,h.cobradordesc source,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador  ,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate	
					,s.status		
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					INNER JOIN xima.cobro_paypal p ON p.paypalid=h.paypalid
					where h.idoper in (5,17) $qwhe
					ORDER BY  p.fecha,x1.userid";*/
			$que="SELECT distinct x1.userid,x1.name,x1.surname,0 as priceprod,0 amountsinformato,p.paypalid,
					c.cardholdername,c.cardnumber,'' notes,p.fecha,p.transactionid,p.amount,h.cobradordesc source,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador  ,
					(
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=x1.`userid`  and pb1.idproducto not in (11,12,13)
						ORDER BY s1.status
						limit 1
					) status,
					(
						SELECT distinct u1.fechacobro
						FROM usr_cobros u1
						WHERE u1.`userid`=x1.`userid` 
						limit 1
					) paydated,
					(SELECT f.`idfrecuency` FROM xima.f_frecuency f WHERE f.`userid`=x1.userid   limit 1) idfrec,
					x1.blacklist,
					(select count(*) FROM xima.cobro_estadocuenta h1 
					WHERE h1.userid=x1.userid  and (h1.idoper=5 || h1.idoper=17)
					) cantpagos,
					(SELECT count(*) FROM logssession u where u.userid=x1.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate) as veceslog	
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid ";
					
				//$que.="INNER JOIN xima.usernotes n ON n.userid=x1.userid ";
				
				$que.="INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					INNER JOIN xima.cobro_paypal p ON p.paypalid=h.paypalid
					where x1.userid not in (1350,5,73,138,2544,3403,3402,2482,20,2342,1668,2846,3430) and h.idoper in (5,17) $qwhe
					ORDER BY  p.fecha,x1.userid";					
					//(select count(*) FROM admin.historico h WHERE h.useridcomparado=f.userid  and (h.idoper=4 || h.idoper=16)) cobros
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data = array();			
			while ($row=mysql_fetch_object($result))
			{
				$que="SELECT distinct n.notes				
					FROM xima.usernotes n 
					WHERE n.notes<>'' and n.userid=".$row->userid;				
				$result2=mysql_query ($que) or die ($que.mysql_error ());
				$row2=mysql_fetch_object($result2);
				$row->notes=$row2->notes;
						
				if($row->status<>'Suspended')$row->priceprod=number_format(getpriceuser($row->userid,0,'yes'),2,'.',',');	
				$row->amountsinformato=$row->amount;				
				$row->amount=number_format($row->amount,2,'.',',');		
				$data [] = $row;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';//,"que":"'.$que.'"
			return;			
		break;
		
		case 'refundrealizados':// refund realizados del mes 
			$dayfrom=date('Y-m-01');
			$dayto=date('Y-m-d');
			
			if(isset($_POST['dayfrom']) && $_POST['dayfrom']<>'' && strlen($_POST['dayfrom'])>0 )
			{ 
				$dayfrom=$_POST['dayfrom'];
				$dayto=$_POST['dayto'];
			}
			
			$qwhe=" AND date(h.fechatrans) BETWEEN '$dayfrom' AND '$dayto' ";

			/*$que="SELECT x1.userid,x1.name,x1.surname,s.status,0 as priceprod,0 amountsinformato,h.monto amount,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					c.cardholdername,c.cardnumber,n.notes,h.fechatrans fecha,h.cobradordesc source,h.notas notasrefund,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador  			
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					where h.idoper in (6) $qwhe
					ORDER BY  h.fechatrans,x1.userid";*/
			$que="SELECT distinct x1.userid,x1.name,x1.surname,0 as priceprod,0 amountsinformato,h.monto amount,					
					c.cardholdername,c.cardnumber,'' notes,h.fechatrans fecha,h.cobradordesc source,h.notas notasrefund,
						x1.blacklist,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador  ,
					(
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=x1.`userid`  and pb1.idproducto not in (11,12,13)
						ORDER BY s1.status
						limit 1
					) status,
					(
						SELECT distinct u1.fechacobro
						FROM usr_cobros u1
						WHERE u1.`userid`=x1.`userid` 
						limit 1
					) paydated				
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					where h.idoper in (6) $qwhe
					ORDER BY  h.fechatrans,x1.userid";					
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data = array();			
			while ($row=mysql_fetch_object($result))
			{
				$que="SELECT distinct n.notes				
					FROM xima.usernotes n 
					WHERE n.notes<>'' and n.userid=".$row->userid;				
				$result2=mysql_query ($que) or die ($que.mysql_error ());
				$row2=mysql_fetch_object($result2);
				$row->notes=$row2->notes;

				if($row->status<>'Suspended')$row->priceprod=number_format(getpriceuser($row->userid),2,'.',',');	
				$row->amountsinformato=$row->amount;				
				$row->amount=number_format($row->amount,2,'.',',');		
				$data [] = $row;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';//,"query":"'.$que.'"
			return;			
		break;
		
		case 'cobrarselactivos':// cobrar los usuarios seleccionados deudores activos
//echo "{success:false, msg:'NO Procesados los cobros hablar con smith.'}";		return;	

			$arrusers=explode(',',$_POST['users']);
			$cbPaypal=$_POST['cbPaypal'];
			$chauthorizenet=$_POST['chauthorizenet'];
			
			$cad='';
			$cobrosi=$cobrono=$cuenta=0;
			foreach($arrusers as $val)
			{
				$cuenta++;
				$userid=$val;				
				//$arrus=explode('$',$val);
				//$userid=$arrus[0];
				//$montoapagar=abs($arrus[1]);

				$que="SELECT x.email,x.idstatus,c.*,p.*  
						FROM xima.creditcard c  
						INNER JOIN xima.ximausrs x on x.userid=c.userid
						INNER JOIN xima.cobro_porcentajes p on p.userid=c.userid
						where c.userid=$userid";
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$row=mysql_fetch_object($result);
				$holder=$row->cardholdername;
				$cardtype=$row->cardname;
				$cardnumber=$row->cardnumber;
				$exdate1=$row->expirationmonth;
				$exdate2=$row->expirationyear;
				$csv=$row->csvnumber;
				$sameAdd=$row->billingaddress;
				$sameCity=$row->billingcity;
				$sameState=$row->billingstate;
				$sameZip=$row->billingzip;				
				$email=$row->email;				
				$idstatus=$row->idstatus;				
				$montoapagar=abs($row->saldo);
				
				$cobrador='cobActBko:'.$bkouserid;
				
				/*if($userid==3733)
				{// cuando no se marcha el check de paypal
					$resArray['AMT']=$montoapagar;
					$resArray['TRANSACTIONID']='TRANSACTION'.$userid.date('YmdHis');
					$resArray['ACK']=$paypal='SUCCESS';

					$queryPay='Insert into xima.cobro_paypal( `userid`, `estado`, `transactionID`, `amount`, `AVS`, `CVV`, `fecha`, `description`) 
					values ('.$userid.',"'.$resArray['ACK'].'","'.$resArray['TRANSACTIONID'].'",'.$resArray['AMT'].',"","",NOW(),"authorize|'.$modulo.'|sin paypal")';
					mysql_query($queryPay) or die($queryPay.mysql_error());
					$queryPay='select paypalid from xima.cobro_paypal where transactionID="'.$resArray['TRANSACTIONID'].'"';
					$resP = mysql_query($queryPay) or die($queryPay.mysql_error());
					$res=mysql_fetch_array($resP);
					$paypalid = $res['paypalid'];
					$paypal='SUCCESS';
				}
				else*/
				{
					if($cbPaypal=='true')
					{$cobrador='PAYPAL: '.$cobrador;	require('../paypal_cobro.php');}
					elseif($chauthorizenet=='true')
					{$cobrador='AUTHORIZE: '.$cobrador;	require('../authorize_cobro.php');}
					else
					{// cuando no se marcha el check de paypal
						$resArray['AMT']=$montoapagar;
						$resArray['TRANSACTIONID']='TRANSACTION'.$userid.date('YmdHis');
						$resArray['ACK']=$paypal='SUCCESS';

						$queryPay='Insert into xima.cobro_paypal( `userid`, `estado`, `transactionID`, `amount`, `AVS`, `CVV`, `fecha`, `description`) 
						values ('.$userid.',"'.$resArray['ACK'].'","'.$resArray['TRANSACTIONID'].'",'.$resArray['AMT'].',"","",NOW(),"authorize|'.$modulo.'|sin paypal")';
						mysql_query($queryPay) or die($queryPay.mysql_error());
						$queryPay='select paypalid from xima.cobro_paypal where transactionID="'.$resArray['TRANSACTIONID'].'"';
						$resP = mysql_query($queryPay) or die($queryPay.mysql_error());
						$res=mysql_fetch_array($resP);
						$paypalid = $res['paypalid'];
						$paypal='SUCCESS';
					}
				}

				//OJO IMPORTANTE -- NO ELIMINAR ESTE COMENTARIO
				//si esta activo(3)
				// 	pasa la cobranza => queda en activo(3)  y se le pone pago idoper(5) en estadocuenta
				// 	NO pasa cobranza => pasa a inactivo(5)  
				//si esta frezze(6)
				// 	pasa la cobranza => queda en freeze(6)  y se le pone pago idoper(17) en estadocuenta
				// 	NO pasa cobranza => pasa a frezzeinactivo(7)  
				if(	$idstatus==6){$pasastat=7;$idop=17;}
				else {$pasastat=5;$idop=5;}

				if(strtoupper($paypal)<>'FAILURE' || ($cbPaypal=='false' && $chauthorizenet=='false'))////paso el cobro del usuario, o no se marco el check en el grid
				{
					$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,paypalid,`usercobrador`,`cobradordesc`)
							VALUES(NOW(),$idop,$userid,".$montoapagar.",$paypalid,$bkouserid,'$cobrador')";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO EN EL HISTORICO

					$queryIns="	UPDATE xima.cobro_porcentajes SET saldo=0 WHERE userid=$userid";
					mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO

					$cobrosi++;
				}
				else
				{
					//si no pasa el cobro lo pasamos bien sea a inactivo o a freezeinc
					$update="update xima.ximausrs set idstatus=$pasastat where userid='$userid'";
					$res=mysql_query($update) or die(mysql_error());//INSERTANDO EL PAGO REALIZADO EN EL HISTORICO
					
					$update="UPDATE usr_cobros u
							INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
							INNER JOIN usr_producto p on p.idproducto=b.idproducto
							SET u.idstatus=$pasastat
							WHERE u.`userid`=$userid and p.pu=0";
					$res=mysql_query($update) or die(mysql_error());
					$cobrono++;

					$sql="SELECT idstatuslogs FROM `logsstatus` WHERE `userid`=$userid ORDER BY `idstatuslogs` desc limit 1";
					$resultado=mysql_query($sql) or die("ERROR :".mysql_error());
					$aux=mysql_fetch_array($resultado);      
					$idstatuslogs=$aux['idstatuslogs'];
					
					$update="UPDATE logsstatus 
						   SET `userid_upd`='$bkouserid', `where`='BKO Payments Actives' 
						   WHERE `idstatuslogs`=$idstatuslogs ";
					mysql_query($update) or  die("{success:false, msg:'ERROR :".mysql_error()."'}");
					
				}
			}
			echo "{success:true, msg:'Procesados: $cuenta. Satisfactorios: $cobrosi. Fallidos: $cobrono.'}";
			return;
		break;
		
		case 'cobrarselinactivos':// cobrar los usuarios seleccionados deudores inactivos
//echo "{success:false, msg:'NO Procesados los cobros hablar con smith.'}";		return;	
		
			$arrusers=explode(',',$_POST['users']);
			$cbPaypal=$_POST['cbPaypal'];
			$chauthorizenet=$_POST['chauthorizenet'];
			$cad='';
			$cobrosi=$cobrono=$cuenta=0;
			foreach($arrusers as $val)
			{
				$cuenta++;
				$userid=$val;				
				//$arrus=explode('$',$val);
				//$userid=$arrus[0];

				//echo 
				$que="SELECT x.email,x.idstatus,c.*,p.* 
						FROM xima.creditcard c  
						INNER JOIN xima.ximausrs x on x.userid=c.userid
						INNER JOIN xima.cobro_porcentajes p on p.userid=c.userid
						where c.userid=$userid";
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$row=mysql_fetch_object($result);
				$holder=$row->cardholdername;
				$cardtype=$row->cardname;
				$cardnumber=$row->cardnumber;
				$exdate1=$row->expirationmonth;
				$exdate2=$row->expirationyear;
				$csv=$row->csvnumber;
				$sameAdd=$row->billingaddress;
				$sameCity=$row->billingcity;
				$sameState=$row->billingstate;
				$sameZip=$row->billingzip;				
				$email=$row->email;				
				$idstatus=$row->idstatus;				
				$montoapagar=abs($row->saldo);

				$cobrador='cobInActBko:'.$bkouserid;
				if($cbPaypal=='true')
				{$cobrador='PAYPAL: '.$cobrador;		require('../paypal_cobro.php');}
				elseif($chauthorizenet=='true')
				{$cobrador='AUTHORIZE: '.$cobrador;		require('../authorize_cobro.php');}
				else
				{// cuando no se marcha el check de paypal
					$resArray['AMT']=$montoapagar;
					$resArray['TRANSACTIONID']='TRANSACTION'.$userid.date('YmdHis');
					$resArray['ACK']=$paypal='SUCCESS';

					$queryPay='Insert into xima.cobro_paypal( `userid`, `estado`, `transactionID`, `amount`, `AVS`, `CVV`, `fecha`, `description`) 
					values ('.$userid.',"'.$resArray['ACK'].'","'.$resArray['TRANSACTIONID'].'",'.$resArray['AMT'].',"","",NOW(),"'.$modulo.'|sin paypal")';
					mysql_query($queryPay) or die($queryPay.mysql_error());
					$queryPay='select paypalid from xima.cobro_paypal where transactionID="'.$resArray['TRANSACTIONID'].'"';
					$resP = mysql_query($queryPay) or die($queryPay.mysql_error());
					$res=mysql_fetch_array($resP);
					$paypalid = $res['paypalid'];
					$paypal='SUCCESS';
				}

				//OJO IMPORTANTE -- NO ELIMINAR ESTE COMENTARIO
				//si esta inactivo(5)
				// 	pasa la cobranza => pasa a activo(3)  y se le pone pago idoper(5) en estadocuenta
				// 	NO pasa cobranza => queda en inactivo(5)  
				//si esta frezzeinactivo(7)
				// 	pasa la cobranza => pasa a freeze(6)  y se le pone pago idoper(17) en estadocuenta
				// 	NO pasa cobranza => queda en frezzeinactivo(5)  
				if(	$idstatus==7){$pasastat=6;$idop=17;}
				else {$pasastat=3;$idop=5;}

				if(strtoupper($paypal)<>'FAILURE' || ($cbPaypal=='false' && $chauthorizenet=='false'))////paso el cobro del usuario, o no se marco el check en el grid
				{
					$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,paypalid,`usercobrador`,`cobradordesc`)
							VALUES(NOW(),$idop,$userid,".$montoapagar.",$paypalid,$bkouserid,'$cobrador')";
					mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO EL PAGO REALIZADO EN EL HISTORICO

					$queryIns="	UPDATE xima.cobro_porcentajes SET saldo=0 WHERE userid=$userid";
					mysql_query($queryIns) or die($queryIns.mysql_error());//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO

					//si pasa el cobro lo pasamos bien sea a activo o a freeze
					$update="update xima.ximausrs set idstatus=$pasastat where userid='$userid'";				
					$res=mysql_query($update) or die(mysql_error());//ESTO ES POR SI ACASO, EL QUE SIRVE ES EL SEIGUIENTE QUERY
					
					$update="UPDATE usr_cobros u
							INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
							INNER JOIN usr_producto p on p.idproducto=b.idproducto
							SET u.idstatus=$pasastat
							WHERE u.`userid`=$userid and p.pu=0";					
					$res=mysql_query($update) or die(mysql_error());				
					$cobrosi++;

					$sql="SELECT idstatuslogs FROM `logsstatus` WHERE `userid`=$userid ORDER BY `idstatuslogs` desc limit 1";
					$resultado=mysql_query($sql) or die("ERROR :".mysql_error());
					$aux=mysql_fetch_array($resultado);      
					$idstatuslogs=$aux['idstatuslogs'];
					
					$update="UPDATE logsstatus 
						   SET `userid_upd`='$bkouserid', `where`='BKO Payments Inactives' 
						   WHERE `idstatuslogs`=$idstatuslogs ";
					mysql_query($update) or  die("{success:false, msg:'ERROR :".mysql_error()."'}");
					
				}
				else
					$cobrono++;
			}
			//echo "{success:true, msg:'TOTAL: $cuenta. SUCCESS: $cobrosi. FAILURE: $cobrono.'}";
			echo "{success:true, msg:'Procesados: $cuenta. Satisfactorios: $cobrosi. Fallidos: $cobrono.'}";			
			return;
		break;
		
		case 'deudoresFecha':// todos los deudores del dia
			$dayfrom=$_POST['dayfrom'];
			$dayto=$_POST['dayto'];

			/*$que="SELECT x1.userid,x1.name,x1.surname,s.idstatus,s.status,t.usertype,0 as priceprod, 0 amountsinformato,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					c.cardname,c.cardnumber,n.notes,d.saldo,0 as amount ,d.freedays 	 
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (1,3,5,6,7,8,9) 
					";
			if($_POST['dayfrom']!='-Select-'){ 
				$que.=" and  DAYOFMONTH((SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid)) between ".$dayfrom." and ".$dayto;
				$que.=" and  ((SELECT `idfrecuency` f FROM `xima`.`f_frecuency` f WHERE f.`userid`=x1.userid limit 1)=1 or
							((SELECT `idfrecuency` f FROM `xima`.`f_frecuency` f WHERE f.`userid`=x1.userid limit 1)=2 
							and MONTH((SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid))=".date('m')."
							and DAY((SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid)) between ".$dayfrom." and ".$dayto."))";
			}SELECT distinct concat_ws(' ',x1.userid,x1.name,x1.surname) usergroup,x1.userid,x1.name,x1.surname,s.idstatus,s.status,p.name nameprod,
							0 as priceprod, 0 amountsinformato,u.fechacobro paydated,
							c.cardname,n.notes,d.saldo,0 as amount,u.freedays,f.idfrecuency,y.name*/
			//echo $que;	
								
			$que="	SELECT distinct x1.name,x1.surname,u.userid,
						s.idstatus,p.name nameprod,
						0 as priceprod, 0 amountsinformato,
						c.cardname,'' notes,d.saldo,0 as amount,u.freedays,y.name namefrec,p.idproducto,
						(	
							SELECT distinct s1.status
							FROM usr_cobros u1
							INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
							INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
							WHERE u1.`userid`=x1.`userid`  and pb1.idproducto not in (11,12,13)
							ORDER BY s1.status
							limit 1
						) status,
						(
							SELECT distinct u1.fechacobro
							FROM usr_cobros u1
							WHERE u1.`userid`=x1.`userid` 
							limit 1
						) paydated,
						(SELECT if(DATE_SUB(CURDATE(),INTERVAL 60 DAY)>=sessiondate,'Y','N')
						FROM logssession u where u.userid=x1.`userid` order by sessiondate desc limit 1) as marcalogin,
						(SELECT count(*) FROM logssession u where u.userid=x1.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate) as veceslog		
					FROM xima.usr_cobros u
					INNER JOIN xima.ximausrs x1 ON u.userid=x1.userid
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
					INNER JOIN usr_producto p on p.idproducto=b.idproducto
					INNER JOIN f_frecuency f on (f.idproducto=p.idproducto and u.userid=f.userid)
					INNER JOIN frecuency y on (y.idfrecuency=f.idfrecuency)
					where u.idstatus in (1,3,6,7,8,9) ";//and u.userid=312 
			if($_POST['dayfrom']!='-Select-'){ 
				$que.=" and  DAY(u.fechacobro) between ".$dayfrom." and ".$dayto;
				/*$que.=" and ( 	
								f.`idfrecuency`=1 or  
								(
								f.`idfrecuency`=2 
								and MONTH(u.fechacobro)=".date('m')."
								and DAY(u.fechacobro) between ".$dayfrom." and ".$dayto."
								)
							)";*/
				$que.=" and ( 	
								f.`idfrecuency`=1
							)";			
			}
			 
			 $que.=" ORDER BY  u.userid,day(paydated) desc,p.idproducto desc";	
			//$que.=" LIMIT ".$start.",".$limit;	

			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data = array();			
			unset($arraux);
			while ($row=mysql_fetch_object($result))
			{
				$que="SELECT distinct n.notes				
					FROM xima.usernotes n 
					WHERE n.notes<>'' and n.userid=".$row->userid;				
				$result2=mysql_query ($que) or die ($que.mysql_error ());
				$row2=mysql_fetch_object($result2);
				$row->notes=$row2->notes;

				if(!(($row->nameprod=="Webs" && $row->amountsinformato==0 && ($row->status=="RealtorWeb" || $row->status=="InvestorWeb")) || $row->nameprod=="Workshop"  ))
				{
					if($row->status<>'Suspended')$row->priceprod=number_format(getpriceuser($row->userid,0,'yes'),2,'.',',');
					$row->saldo=number_format($row->saldo,2,'.',',');	
					
					//echo "<pre>";print_r($row);echo "</pre>";				
					$arraux[]=$row;
				}
			}
			
			unset($arrcomparado);
			$productoanterior=0;
			foreach($arraux as $arrval )
			{
				$insertar=1;
				if($productoanterior==2 && $arrval->idproducto==1)
					$insertar=0;
				if($productoanterior==3 && $arrval->idproducto==2)
					$insertar=0;
					
				if($insertar==1)
				{
					$env=$arrval->idproducto;
					if($arrval->idproducto==2) $env='1,2';
					if($arrval->idproducto==3) $env='1,2,3';
				
					$precio=getpriceuser($arrval->userid,0,'yes',$env);
					$arrval->amountsinformato=$precio;
					$arrval->amount=number_format($precio,2,'.',',');
					if($arrval->idstatus==6  || $arrval->idstatus==7 )
					{
						$arrval->amountsinformato=15;
						$arrval->amount=number_format(15,2,'.',',');
					}				
					$data[]=$arrval;
				}	
				$productoanterior=$arrval->idproducto;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			//$data [] = $row;			
			//echo json_encode($data);
			return;
		break;

		case 'users':
			ini_set("session.gc_maxlifetime", 18000); 
			session_start(); 
			$start=0;
			$limit=50;
			
			if($_POST['start']<>'' && isset($_POST['start']))
			{
				$start=$_POST['start'];
				$limit=$_POST['limit'];
			}				
			$idtmail=$_POST['idtmail'];
			$selectsenddateemail=",'' senddateemail";
			$orderbysenddateemail="ORDER BY xima.ximausrs.userid";
			if($_POST['sort']<>'')	$orderbysenddateemail="ORDER BY ".$_POST['sort']." ".$_POST['dir'];
			if($idtmail<>'')
			{
				$selectsenddateemail=",(
											SELECT e.`senddate`
											FROM xima.em_usersemails e
											WHERE e.`userid`=xima.ximausrs.userid AND e.`idtmail`=$idtmail
											ORDER BY e.`idumail` desc
											LIMIT 1
										) senddateemail";
				//$orderbysenddateemail="ORDER BY xima.ximausrs.userid";
				//$orderbysenddateemail=" ORDER BY length(senddateemail),x1.userid";			
			
			}
			$selecfechaupd=" '' fechaupd ";			
			if(isset($_SESSION['filters'])){ 
				$pos = strpos($_SESSION['filters'], 'xima.logsstatus.statusafter');
				if($pos !== false) 
					$selecfechaupd="  xima.logsstatus.fechaupd ";/*"
									(SELECT l.fechaupd
									FROM xima.logsstatus l
									where ( l.statusafter=2 
									AND (
												SELECT distinct s1.idstatus
												FROM usr_cobros u1
												INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
												WHERE u1.`userid`=xima.ximausrs.userid 
												ORDER BY s1.status
												limit 1
											) =2 ) 
									LIMIT 1 )fechaupd"*/;
			}	

			$que="SELECT DISTINCT SQL_CALC_FOUND_ROWS
					xima.ximausrs.userid id,
					xima.ximausrs.userid,
					xima.ximausrs.name,
					xima.ximausrs.surname,
					(	
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=xima.ximausrs.`userid`  and pb1.idproducto not in (11,12,13)
						ORDER BY s1.status
						limit 1
					) status,
					(
						SELECT p.name nameprod
						FROM xima.usr_cobros u
						INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
						INNER JOIN usr_producto p on p.idproducto=b.idproducto
						where u.userid=xima.ximausrs.`userid` and b.idproducto not in (11,12,13)
						ORDER BY p.idproducto desc limit 1
					) product,
					(SELECT f.`idfrecuency` FROM xima.f_frecuency f WHERE f.`userid`=xima.ximausrs.`userid`   limit 1) idfrec,
					xima.usertype.usertype,
					xima.ximausrs.blacklist,
					xima.ximausrs.email,
					xima.userterms.accept,
					xima.ximausrs.PASS,
					date(xima.ximausrs.affiliationdate) AS affiliation,
					xima.usernotes.notes,
					(SELECT CONCAT(x.NAME,' ',x.SURNAME,' (',xima.ximausrs.executive,')' ) FROM ximausrs x where userid=xima.ximausrs.executive LIMIT 1) AS nombres,
					(
						SELECT distinct u1.freedays
						FROM usr_cobros u1
						WHERE u1.`userid`=xima.ximausrs.`userid` 
						limit 1
					)  freedays ,
					0 pricefull,
					(
						SELECT if(DATE_SUB(CURDATE(),INTERVAL 60 DAY)>=sessiondate,'Y','N')
						FROM logssession u 
						WHERE u.userid=xima.ximausrs.`userid` 
						ORDER BY sessiondate desc limit 1
					) as marcalogin,		
					(
						SELECT count(*) 
						FROM logssession u 
						WHERE u.userid=xima.ximausrs.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate
					) as veceslog,
					'' totalprice 
					$selectsenddateemail,
					 $selecfechaupd 
				FROM xima.ximausrs
				LEFT JOIN (xima.status) ON (xima.ximausrs.idstatus=xima.status.idstatus)
				LEFT JOIN (xima.usertype) ON (xima.ximausrs.idusertype=xima.usertype.idusertype)
				LEFT JOIN (xima.f_frecuency) ON (xima.ximausrs.userid=xima.f_frecuency.userid)
				LEFT JOIN (xima.usernotes) ON (xima.ximausrs.userid=xima.usernotes.userid)
				LEFT JOIN (xima.creditcard) ON (xima.ximausrs.userid=xima.creditcard.userid)
				LEFT JOIN (xima.userterms) ON (xima.ximausrs.userid=xima.userterms.userid)
				LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
				LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
				LEFT JOIN (xima.permission) ON (xima.ximausrs.userid=xima.permission.userid)
				LEFT JOIN (xima.cobro_porcentajes) ON (xima.ximausrs.userid=xima.cobro_porcentajes.userid)
				LEFT JOIN (xima.usr_registertype) ON (xima.ximausrs.userid=xima.usr_registertype.userid)
				LEFT JOIN (xima.logsstatus) ON (xima.ximausrs.userid=xima.logsstatus.userid)
				LEFT JOIN (xima.logsusr_cobros) ON (xima.ximausrs.userid=xima.logsusr_cobros.userid)				
				WHERE 1=1 ANd ";//((xima.ximausrs.userid in (4,5) or  xima.ximausrs.idusertype not in (1)) and (xima.ximausrs.userid in (2482))
			if(isset($_SESSION['filters'])){ 
				 //echo 
				 $que.=$_SESSION['filters']."  ";
			}	
					
			$que.=" $orderbysenddateemail ";
			$que.="LIMIT ".$start.",".$limit;

			//echo $que;
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$totalprice=0;
			while ($row=mysql_fetch_object($result))
			{
				if($row->status<>'Suspended')$row->pricefull=getpriceuser($row->userid,0,'yes');	
				$totalprice+=$row->pricefull;
				$data [] = $row;
			}
			
			$pos=strrpos(strtolower($que),"limit");
			$que2=substr($que,0,$pos);
			
			$result2 = mysql_query($que2) or die ($que2.mysql_error());
			$fullids='';
			$num_row=0;
			while ($row2=mysql_fetch_object($result2))
			{
				if($num_row>0)$fullids.=',';
				$fullids.=$row2->userid;
				$num_row++;
			}	
			//$_SESSION['fullids']=$fullids;
			//$num_row=mysql_num_rows($result2);
			echo '({"total":"'.$num_row.'","totalprice":"'.$totalprice.'","results":'.json_encode($data).'})';
			
			return;
		break;
		
		case 'uploadNewsContracts':
			$data=json_decode($_POST['data']);
			$user=json_decode($_POST['user']);
			$path='C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs';
			foreach($user as $userIndex => $buyerInfo){
				if(count($data->contracts)>0){
					foreach($data->contracts as $contractIndex => $contract){
						$newFileName = "$userIndex-".date("YmdHis")."-".preg_replace('/[^a-zA-Z0-9.]/','',$contract->fileName);
						
						if(!@copy($path.'/template/contracts/MContracts/'.$contract->fileName, $path."/temp_change_contract/".$newFileName))
							echo "<pre>".print_r(error_get_last())."</pre>";
							
						if(!@copy($path."/temp_change_contract/".$newFileName, $path.'/template_upload/'.$newFileName))
							echo "<pre>".print_r(error_get_last())."</pre>";
							
						if(!@unlink($path."/temp_change_contract/".$newFileName))
							echo "<pre>".print_r(error_get_last())."</pre>";

						$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios

						system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/template_upload/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/template_upload/$newFileName\"", $valid);

						if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						
							if(!@unlink($path.'/template_upload/'.$newFileName))
								echo "<pre>".print_r(error_get_last())."</pre>";
								
							if(!@rename($path.'/template_upload/temp_by_jesus'.$id_name.'.pdf', $path.'/template_upload/'.$newFileName))
								echo "<pre>".print_r(error_get_last())."</pre>";
							$query = "
								INSERT INTO xima.contracts_custom 
									(userid, name, filename, date_upload, type,tpl1_name,tpl1_addr,tpl1_addr2,tpl1_addr3) 
									VALUES 
									($userIndex,'{$contract->realName}','$newFileName',NOW(),'N','{$buyerInfo->buyer}','{$buyerInfo->address1}','{$buyerInfo->address2}','{$buyerInfo->address3}')";
							
							if(mysql_query($query)){
								$mensaje = "Contract saved successfully!";
								
								if($_SERVER['HTTP_HOST']=='developer.reifax.com')
									copy($path.'/template_upload/'.$newFileName, 'C:/inetpub/wwwroot/xima3/mysetting_tabs/mycontracts_tabs/template_upload/'.$newFileName);
								
								require_once('../../custom_contract/fpdf/fpdf.php');
								require_once('../../custom_contract/fpdi/fpdi.php');
								$pdf = new FPDI();
								$contractPages = $pdf->setSourceFile($path."/template_upload/$newFileName");
								
								$contractOriginalID=mysql_insert_id();
								
								//Asign Templates to Contract
								if($contract->template!='0000'){
									$_POST['idfunction']		=	'7';
									$_POST['userCustom']		=	$userIndex;
									$_POST['pages_new']			=	$contractPages;
									$_POST['template_original']	=	$contract->template;
									$_POST['idContractCustom']	=	$contractOriginalID;
									ob_start();
									include($_SERVER['DOCUMENT_ROOT'].'/custom_contract/includes/php/functions.php');
									$tempContent = json_decode(ob_get_contents());
									ob_end_clean();
									unset($_POST['idfunction']);
									unset($_POST['userCustom']);
									unset($_POST['pages_new']);
									unset($_POST['template_original']);
								}
								if($tempContent->success){
									$query	=	"
										INSERT INTO contracts_templates 
											SELECT null,'$userIndex',temp_name FROM contracts_templates 
											WHERE temp_id='{$contract->template}' AND temp_userid NOT IN ('9999999','0')";
									if(mysql_query($query)){
										if(mysql_affected_rows()>0){
											$query	=	"
												INSERT INTO contracts_templates_campos 
													SELECT null,LAST_INSERT_ID(),'$userIndex',temp_page,temp_type,temp_idtc,temp_text,temp_posx,temp_posy FROM contracts_templates_campos 
													WHERE temp_temp_id='{$contract->template}'";
											mysql_query($query);
										}
									}

								}
								foreach($contract->addons as $addonIndex => $addon){
									$placeAddon=($addonIndex+1);
									$newFileName = "$contractOriginalID-$userIndex-$placeAddon.pdf";
						
									if(!@copy($path.'/template/contracts/MDocuments/'.$addon->addonFileName, $path."/temp_change_contract/".$newFileName))
										echo "<pre>".print_r(error_get_last())."</pre>";
										
									if(!@copy($path."/temp_change_contract/".$newFileName, $path.'/addons/'.$newFileName))
										echo "<pre>".print_r(error_get_last())."</pre>";
										
									if(!@unlink($path."/temp_change_contract/".$newFileName))
										echo "<pre>".print_r(error_get_last())."</pre>";

									$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios

									system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/addons/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/addons/$newFileName\"", $valid);

									if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
									
										if(!@unlink($path.'/addons/'.$newFileName))
											echo "<pre>".print_r(error_get_last())."</pre>";
											
										if(!@rename($path.'/addons/temp_by_jesus'.$id_name.'.pdf', $path.'/addons/'.$newFileName))
											echo "<pre>".print_r(error_get_last())."</pre>";
										$query = "
											INSERT INTO xima.contracts_addonscustom 
												(userid, type, place, addon_act, addon_name, document)
												VALUES 
												('$userIndex','$contractOriginalID','$placeAddon','1','{$addon->addonRealName}','$newFileName')";
										
										if(mysql_query($query)){
											$mensaje = "Contract saved successfully!";
											
											if($_SERVER['HTTP_HOST']=='developer.reifax.com')
												copy($path.'/addons/'.$newFileName, 'C:/inetpub/wwwroot/xima3/mysetting_tabs/mycontracts_tabs/addons/'.$newFileName);
											
											require_once('../../custom_contract/fpdf/fpdf.php');
											require_once('../../custom_contract/fpdi/fpdi.php');
											$pdf = new FPDI();
											$addonPages = $pdf->setSourceFile($path."/addons/$newFileName");
											
											$addonOriginalID=mysql_insert_id();
											
											//Asign Templates to Contract
											if($addon->template!='0000'){
												$_POST['idfunction']		=	'7';
												$_POST['userCustom']		=	$userIndex;
												$_POST['pages_new']			=	$addonPages;
												$_POST['template_original']	=	$addon->template;
												$_POST['idContractCustom']	=	$addonOriginalID;
												ob_start();
												include($_SERVER['DOCUMENT_ROOT'].'/custom_contract/includes/php/functions.php');
												$tempContent = json_decode(ob_get_contents());
												ob_end_clean();
												unset($_POST['idfunction']);
												unset($_POST['userCustom']);
												unset($_POST['pages_new']);
												unset($_POST['template_original']);
											}
											if($tempContent->success){
												$query	=	"
													INSERT INTO contracts_templates 
														SELECT null,'$userIndex',temp_name FROM contracts_templates 
														WHERE temp_id='{$addon->template}' AND temp_userid NOT IN ('9999999','0')";
												if(mysql_query($query)){
													if(mysql_affected_rows()>0){
														$query	=	"
															INSERT INTO contracts_templates_campos 
																SELECT null,LAST_INSERT_ID(),'$userIndex',temp_page,temp_type,temp_idtc,temp_text,temp_posx,temp_posy FROM contracts_templates_campos 
																WHERE temp_temp_id='{$addon->template}'";
														mysql_query($query);
													}
												}
											}
										}
									}
								}
							}else{
								@unlink($path.'/template_upload/'.$newFileName);
								echo json_encode(array("success"=>false,"msg"=>"Contract Can't be Uploaded!","error" => mysql_error())); 
							}
						}else{
							@unlink($path.'/template_upload/'.$newFileName);
							echo json_encode(array("success"=>false,"msg"=>"Contract Can't be Uploaded!")); 
						}
						
					}
				}else{
					echo json_encode(array("success"=>false,"msg"=>'Dosen\'t have Contracts')); 
				}
			}
			//echo json_encode(array("success"=>true,"total"=>count($data),"data"=>$_POST)); 
			return;
		break;
		case 'users-contract-contracts':
			$data=array();
			array_push($data,array(
				'id'=>'0000',
				'name'=>'Select Contract'
			));
			foreach (glob("../../mysetting_tabs/mycontracts_tabs/template/contracts/MContracts/*.pdf") as $filename) {
			    $tempData=pathinfo($filename);
				array_push($data,array(
					'id'=>$tempData['basename'],
					'name'=>$tempData['filename']
				));
			}
			echo json_encode(array("success"=>true,"total"=>count($data),"data"=>$data)); 
			return;
		break;
		case 'users-contract-addons':
			$data=array();
			array_push($data,array(
				'id'=>'0000',
				'name'=>'Select Aditional Document'
			));
			foreach (glob("../../mysetting_tabs/mycontracts_tabs/template/contracts/MDocuments/*.pdf") as $filename) {
			    $tempData=pathinfo($filename);
				array_push($data,array(
					'id'=>$tempData['basename'],
					'name'=>$tempData['filename']
				));
			}
			echo json_encode(array("success"=>true,"total"=>count($data),"data"=>$data)); 
			return;
		break;
		case 'users-contract':
			ini_set("session.gc_maxlifetime", 18000); 
			session_start(); 
			$start=0;
			$limit=50;
			
			if($_POST['start']<>'' && isset($_POST['start']))
			{
				$start=$_POST['start'];
				$limit=$_POST['limit'];
			}				
			$idtmail=$_POST['idtmail'];
			$selectsenddateemail=",'' senddateemail";
			$orderbysenddateemail="ORDER BY xima.ximausrs.userid";
			if($_POST['sort']<>'')	$orderbysenddateemail="ORDER BY ".$_POST['sort']." ".$_POST['dir'];
			if($idtmail<>'')
			{
				$selectsenddateemail=",(
											SELECT e.`senddate`
											FROM xima.em_usersemails e
											WHERE e.`userid`=xima.ximausrs.userid AND e.`idtmail`=$idtmail
											ORDER BY e.`idumail` desc
											LIMIT 1
										) senddateemail";
				//$orderbysenddateemail="ORDER BY xima.ximausrs.userid";
				//$orderbysenddateemail=" ORDER BY length(senddateemail),x1.userid";			
			
			}
			$selecfechaupd=" '' fechaupd ";			
			if(isset($_SESSION['filters'])){ 
				$pos = strpos($_SESSION['filters'], 'xima.logsstatus.statusafter');
				if($pos !== false) 
					$selecfechaupd="  xima.logsstatus.fechaupd ";/*"
									(SELECT l.fechaupd
									FROM xima.logsstatus l
									where ( l.statusafter=2 
									AND (
												SELECT distinct s1.idstatus
												FROM usr_cobros u1
												INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
												WHERE u1.`userid`=xima.ximausrs.userid 
												ORDER BY s1.status
												limit 1
											) =2 ) 
									LIMIT 1 )fechaupd"*/;
			}	

			$que="
				SELECT 
					SQL_CALC_FOUND_ROWS temp.*,
					xima.contracts_addonscustom.id as idDoc, 
					xima.contracts_addonscustom.`type`, 
					xima.contracts_addonscustom.document, 
					xima.contracts_addonscustom.addon_name from (
						SELECT DISTINCT 
							xima.ximausrs.userid,
							concat(xima.ximausrs.name,' ',xima.ximausrs.surname) as groups,
							xima.contracts_custom.id as idfile,
							xima.contracts_custom.name as namefile,
							xima.contracts_custom.filename
						FROM xima.ximausrs
						LEFT JOIN (xima.status) ON (xima.ximausrs.idstatus=xima.status.idstatus)
						LEFT JOIN (xima.usertype) ON (xima.ximausrs.idusertype=xima.usertype.idusertype)
						LEFT JOIN (xima.f_frecuency) ON (xima.ximausrs.userid=xima.f_frecuency.userid)
						LEFT JOIN (xima.usernotes) ON (xima.ximausrs.userid=xima.usernotes.userid)
						LEFT JOIN (xima.creditcard) ON (xima.ximausrs.userid=xima.creditcard.userid)
						LEFT JOIN (xima.userterms) ON (xima.ximausrs.userid=xima.userterms.userid)
						LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
						LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
						LEFT JOIN (xima.permission) ON (xima.ximausrs.userid=xima.permission.userid)
						LEFT JOIN (xima.cobro_porcentajes) ON (xima.ximausrs.userid=xima.cobro_porcentajes.userid)
						LEFT JOIN (xima.usr_registertype) ON (xima.ximausrs.userid=xima.usr_registertype.userid)
						LEFT JOIN (xima.logsstatus) ON (xima.ximausrs.userid=xima.logsstatus.userid)
						LEFT JOIN (xima.logsusr_cobros) ON (xima.ximausrs.userid=xima.logsusr_cobros.userid)	
						LEFT JOIN (xima.contracts_custom) ON (xima.ximausrs.userid= xima.contracts_custom.userid)	
						WHERE 1=1 AND ";//((xima.ximausrs.userid in (4,5) or  xima.ximausrs.idusertype not in (1)) and (xima.ximausrs.userid in (2482))
			if(isset($_SESSION['filters'])){ 
				 //echo 
				 $que.=$_SESSION['filters']."  ";
			}	
					
			$que.=" $orderbysenddateemail ";
			$que.="LIMIT ".$start.",".$limit;

			$que=$que.") temp
				left join xima.contracts_addonscustom ON (temp.idfile= xima.contracts_addonscustom.`type`)";
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$totalprice=0;
			$totalCount=1;
			while ($row=mysql_fetch_array($result))
			{
				array_push($data,array(
					"id"		=> $totalCount++,
					"userid"		=> $row["userid"],
					"groups"	=> $row["groups"], 
					"namefile"	=> $row["namefile"],
					"filename"	=> $row["filename"],
					"idDoc"	=> $row["idDoc"],
					"type"	=> is_null($row["type"]) ? $row["namefile"].$row['idfile'] : $row["namefile"].": ".$row['type'],
					"document"	=> $row["document"],
					"addon_name"	=> is_null($row["addon_name"])	?	"Doesn't have Addons" : $row["addon_name"]
				));
			}
			
			$num_row=mysql_num_rows($result);
			echo json_encode(
				array(  
				"success"	=> true,
				"total"		=> $num_row,
				"data"		=> $data
			));			
			return;
		break;
		case 'contract-statitic-graphics':
			$month=$_POST['month'];
			$year=$_POST['year'];
			$type=$_POST['type'];
			
			$arregloDias=array(
							'1'=>'SUN',
							'2'=>'MON',
							'3'=>'TUE',
							'4'=>'WED',
							'5'=>'THU',
							'6'=>'FRI',
							'7'=>'SAT'
							);
			$arregloYear=array(
							'1'=>'JAN',
							'2'=>'FEB',
							'3'=>'MAR',
							'4'=>'APR',
							'5'=>'MAY',
							'6'=>'JUN',
							'7'=>'JUL',
							'8'=>'AUG',
							'9'=>'SEP',
							'10'=>'OCT',
							'11'=>'NOV',
							'12'=>'DEC'
							);
			$arregloContDias=array(
							'1'=>'31',
							'2'=>'28',
							'3'=>'31',
							'4'=>'30',
							'5'=>'31',
							'6'=>'30',
							'7'=>'31',
							'8'=>'31',
							'9'=>'30',
							'10'=>'31',
							'11'=>'30',
							'12'=>'31'
							);
			if($type==1){
				$que="SELECT DISTINCT SQL_CALC_FOUND_ROWS u.USERID,date(sdate) as fecha, dayofmonth(sdate) as daymonth, dayofweek(sdate) as dayweek, count(*) as total 
						FROM xima.ximausrs u inner join  xima.saveddoc s on s.usr=u.USERID
						where month(sdate)=".$month." and year(sdate)=".$year;
						
						
				$que.=" group by date(s.sdate) order by s.sdate";
			}else{
				$que="SELECT DISTINCT SQL_CALC_FOUND_ROWS u.USERID,date(sdate) as fecha, month(sdate) as mes, count(*) as total
						FROM xima.ximausrs u inner join  xima.saveddoc s on s.usr=u.USERID
						where year(sdate)=".$year;
						
				$que.=" group by month(sdate) order by sdate ";
			}
			//echo $que;
			$result=mysql_query ($que) or die ($que.mysql_error ());
 
			$data = array();
			
			while ($row=mysql_fetch_array($result)){
				if($type==1){
					$row['day']=$row['daymonth'].' '.$arregloDias[$row['dayweek']];
					$row['Avr']="";
				}else{
					$row['day']=$row['mes'].' '.$arregloYear[$row['mes']];
					$row['totalD'] =$row['total'];
					if($year==date("Y")){
						if($row['mes']==date("n")){
							$row['Avr']= number_format(($row['total']) / (date("j")),2);
						}else{
							$row['Avr']= number_format(($row['total']) / ($arregloContDias[$row['mes']]),2);
						}
						
					}else{
						$row['Avr']= number_format(($row['total']) / ($arregloContDias[$row['mes']]),2);
					}
					
				}
				$data [] = $row;
			}	
			
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
		break;
		case 'contract-statitic':
			ini_set("session.gc_maxlifetime", 18000); 
			session_start(); 
			
			$start=0;
			$limit=50;
			
			$selectDate = $_POST['select_date'];
			$dateA = $_POST['select_dateA'];
			$dateB = $_POST['select_dateB'];
			$sincontract = isset($_POST['sinContract']) ? $_POST['sinContract'] : 'false';
			$cuerpo_query ="";
			$countDay=1;

			
			if($_POST['start']<>'' && isset($_POST['start']))
			{
				$start=$_POST['start'];
				$limit=$_POST['limit'];
			}			
			
			if($sincontract=='false'){//sin ceros
				$sincontract="inner"; 
			}else{// con ceros
				$sincontract="left";
			}
			
			if($dateA!=''){
				switch($selectDate){
					case "Equal":
						$param.=" and '".$dateA."' = date_format(v.sdate,'%Y-%m-%d') ";
						$tipo=1; 
					break;
					case "Greater Than":
						$param.=" and '".$dateA."' < date_format(v.sdate,'%Y-%m-%d') "; 
						$auxDateA= date("Y-m-d");$auxDateB= $dateA;
						$tipo=3; 
					break;
					case "Less Than":
						$param.=" and '".$dateA."' > date_format(v.sdate,'%Y-%m-%d') "; 
						$auxDateB= $dateA;
						$tipo=2; 
					break;
					case "Equal or Less":
						$param.=" and '".$dateA."' >= date_format(v.sdate,'%Y-%m-%d') "; 
						$auxDateB= $dateA;
						$tipo=2; 
					break;
					case "Equal or Greater":
						$param.=" and '".$dateA."' <= date_format(v.sdate,'%Y-%m-%d') "; 
						$auxDateA= date("Y-m-d");$auxDateB= $dateA;
						$tipo=3; 
					break;
					case "Between":
						if(!empty($dateA) && !empty($dateB)) 
						$param.="and date_format(v.sdate,'%Y-%m-%d') BETWEEN '".$dateA."' and  '".$dateB."'";	
						$auxDateA= $dateA;$auxDateB=$dateB;
						$tipo=3; 			        		
					break;
				}
			}
			
			if(!empty($_SESSION['filters'])){ 
				 //echo 
				$filter.=" AND ".$_SESSION['filters']."  ";
				$query_Filter ="
					select DISTINCT SQL_CALC_FOUND_ROWS xima.ximausrs.USERID
					from xima.ximausrs
					LEFT JOIN (xima.status) ON (xima.ximausrs.idstatus=xima.status.idstatus)
					LEFT JOIN (xima.usertype) ON (xima.ximausrs.idusertype=xima.usertype.idusertype)
					LEFT JOIN (xima.f_frecuency) ON (xima.ximausrs.userid=xima.f_frecuency.userid)
					LEFT JOIN (xima.usernotes) ON (xima.ximausrs.userid=xima.usernotes.userid)
					LEFT JOIN (xima.creditcard) ON (xima.ximausrs.userid=xima.creditcard.userid)
					LEFT JOIN (xima.userterms) ON (xima.ximausrs.userid=xima.userterms.userid)
					LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
					LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
					LEFT JOIN (xima.permission) ON (xima.ximausrs.userid=xima.permission.userid)
					LEFT JOIN (xima.cobro_porcentajes) ON (xima.ximausrs.userid=xima.cobro_porcentajes.userid)
					LEFT JOIN (xima.usr_registertype) ON (xima.ximausrs.userid=xima.usr_registertype.userid)
					LEFT JOIN (xima.logsstatus) ON (xima.ximausrs.userid=xima.logsstatus.userid)
					LEFT JOIN (xima.logsusr_cobros) ON (xima.ximausrs.userid=xima.logsusr_cobros.userid)
					where 1=1  ".$filter;
					
					$resultFilter=mysql_query ($query_Filter) or die ($query_Filter.mysql_error ());
					if(mysql_num_rows($resultFilter)>0){
						while ($rowID=mysql_fetch_array($resultFilter)){
							$FilterId.= ",".$rowID['USERID'];
						}	
					}
					
					$param.=" and xima.ximausrs.userid in (".substr($FilterId,1).")";
					
			}	
			
			if(empty($_SESSION['filters']) && $dateA=="" && $dateA==""){
				$param.=" and '".date("Y-m-d")."' = date_format(v.sdate,'%Y-%m-%d') "; 
			}
			
			$query ="select DISTINCT SQL_CALC_FOUND_ROWS xima.ximausrs.USERID, concat(xima.ximausrs.name,' ',xima.ximausrs.surname) as NAME ,xima.ximausrs.AFFILIATIONDATE,(
						SELECT p.name nameprod
						FROM xima.usr_cobros c
						INNER JOIN usr_productobase b on b.idproductobase=c.idproductobase
						INNER JOIN usr_producto p on p.idproducto=b.idproducto
						where c.userid=xima.ximausrs.userid and b.idproducto not in (11,12,13)
						ORDER BY p.idproducto desc limit 1
					) PRODUCT,MAX(v.sdate) as MaxDate,count( distinct v.iddoc)  as ENV
					from xima.ximausrs  ".$sincontract." join saveddoc v on v.usr=xima.ximausrs.USERID 	
					where 1=1 ".$param." group by xima.ximausrs.userid ";

			$quecount=$query;	
			
			
			if (isset($_POST['sort'])){
				switch($_POST['sort']){
					case 'USERID':$sort="xima.ximausrs.userid"; break;
					case 'NAME':$sort="NAME"; break;
					case 'AFFILIATIONDATE':$sort="xima.ximausrs.AFFILIATIONDATE"; break;
					case 'PRODUCT':$sort="PRODUCT"; break;
					case 'ENV':$sort="ENV"; break;
				}
				$query.=' ORDER BY ' . $sort . ' ' . $_POST['dir'];
			}else{
				$query.=' order by ENV desc ';
			}
			
			$query.=" LIMIT ".$start.",".$limit;	
			
			$result=mysql_query ($query) or die ($query.mysql_error ());
			$result2=mysql_query ($quecount) or die ($query.mysql_error ());
 			
			$total=0;
			while ($row1=mysql_fetch_array($result2)){
				$total = ($total)+($row1['ENV']);
			}
			
			if($tipo==1){
				$countDay = 1;	
			}else if($tipo==3){
				$s = strtotime($auxDateA)-strtotime($auxDateB);
				$d = intval($s/86400);
				$countDay = $d ;
			}
			
			
			$data = array();
			


			while ($row=mysql_fetch_array($result)){
				$row['TotEnv'] = $total;
				if($tipo==2){
					$dateaux=date_create($row['MaxDate']);
					$s = strtotime(date_format($dateaux,"Y-m-d"))-strtotime($auxDateB);
					$d = intval($s/86400);
					$countDay=$d;
				}
				$row['countD'] = $countDay;
				$data[] = $row;
			}
			
			echo '({"total":"'.mysql_num_rows($result2).'","results":'.json_encode($data).'})';
			return;
		break;

		case 'emails':
			$que="SELECT DISTINCT SQL_CALC_FOUND_ROWS * FROM anexos.contactus
				ORDER BY anexos.contactus.emaildate DESC	
				LIMIT ".$start.",".$limit;
		break;
		
		case 'contracts':
			$que="SELECT x.userid as ud, concat(x.name,' ',x.surname) as name_user, c.* FROM xima.contracts_custom c 
				  inner join xima.ximausrs x on c.userid=x.userid  where signature='N' and date_upload<>'2011-08-01'
				  order by date_upload desc, signature asc
				  LIMIT ".$start.",".$limit;
		break;

		case 'sessionlogs':
			$month=$_POST['month'];
			$year=$_POST['year'];
			$iduser=$_POST['userid'];
			$type=$_POST['type'];
			$arregloDias=array(
							'1'=>'SUN',
							'2'=>'MON',
							'3'=>'TUE',
							'4'=>'WED',
							'5'=>'THU',
							'6'=>'FRI',
							'7'=>'SAT'
							);
			$arregloYear=array(
							'1'=>'JAN',
							'2'=>'FEB',
							'3'=>'MAR',
							'4'=>'APR',
							'5'=>'MAY',
							'6'=>'JUN',
							'7'=>'JUL',
							'8'=>'AUG',
							'9'=>'SEP',
							'10'=>'OCT',
							'11'=>'NOV',
							'12'=>'DEC'
							);
			$arregloContDias=array(
							'1'=>'31',
							'2'=>'28',
							'3'=>'31',
							'4'=>'30',
							'5'=>'31',
							'6'=>'30',
							'7'=>'31',
							'8'=>'31',
							'9'=>'30',
							'10'=>'31',
							'11'=>'30',
							'12'=>'31'
							);
			if($type==1){
				$que="SELECT date(sessiondate) as fecha, dayofmonth(sessiondate) as daymonth, dayofweek(sessiondate) as dayweek, count(*) as total 
						FROM xima.logssession s
						where month(sessiondate)=".$month." and year(sessiondate)=".$year;
						
				if(strlen($iduser)>0){
					$que.=" and userid=".$iduser;
				}
				$que.=" group by date(sessiondate) order by sessiondate limit 0, 5000";
			}else{
				$que="SELECT date(sessiondate) as fecha, month(sessiondate) as mes, count(*) as total
						FROM xima.logssession s
						where year(sessiondate)=".$year;
						
				if(strlen($iduser)>0){
					$que.=" and userid=".$iduser;
				}
				$que.=" group by month(sessiondate) order by sessiondate limit 0, 5000";
			}
			//echo $que;
			$result=mysql_query ($que) or die ($que.mysql_error ());
 
			$data = array();
			
			while ($row=mysql_fetch_array($result)){
				if($type==1){
					$row['day']=$row['daymonth'].' '.$arregloDias[$row['dayweek']];
					$row['Avr']="";
				}else{
					$row['day']=$row['mes'].' '.$arregloYear[$row['mes']];
					$row['totalD'] =$row['total'];
					if($year==date("Y")){
						if($row['mes']==date("n")){
							$row['Avr']= number_format(($row['total']) / (date("j")),2);
						}else{
							$row['Avr']= number_format(($row['total']) / ($arregloContDias[$row['mes']]),2);
						}
						
					}else{
						$row['Avr']= number_format(($row['total']) / ($arregloContDias[$row['mes']]),2);
					}
					
				}
				$data [] = $row;
			}	
			
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
		break;		
		
		case 'historylogs':
			$month=$_POST['month'];
			$year=$_POST['year'];
			$iduser=$_POST['userid'];
			$type=$_POST['type'];
			if(strlen($iduser)>0){
				$que="SELECT day(sessiondate) as dia, sessiondate, login_ip FROM logssession l";
				if($type==1){
					$que.=" where month(sessiondate)=".$month." and year(sessiondate)=".$year;
				}else{
					$que.=" where year(sessiondate)=".$year;
				}
				$que.=" and userid=".$iduser;
				
				$que." order by sessiondate";
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$data= array();
				while($row= mysql_fetch_array($result)){
					array_push($data,array(
						"fechgrup0"	=> $row["dia"],
						"sessiondate"	=> $row["sessiondate"], 
						"login_ip"	=> $row["login_ip"]
					));
				}
				echo json_encode(
					array(  
					"success"	=> true,
					"data"		=> $data
				));
			}
			return;
		break;

		case 'statususers':
			$month=$_POST['month'];
			$year=$_POST['year'];
			$order=$_POST['sort'];
			$dir=$_POST['dir'];
			/*$que="SELECT SQL_CALC_FOUND_ROWS DISTINCT x.userid, x.name, x.surname, x.`idstatus`, date(x.testdatebeguin) as signup,
				s.statusafter, date(s.fechaupd) as fechaupd,  
				(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x.userid) paydate, 
				n.notes, st.`status`,
				(SELECT date(fechatrans)  FROM cobro_estadocuenta where userid=x.userid and idoper=5 and month(fechatrans)=$month 
				and year(fechatrans)=$year limit 1) ultimafecha,
				(SELECT monto  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month 
				and year(fechatrans)=$year limit 1) ultimomonto
				FROM logsstatus s right join xima.ximausrs x on s.userid=x.userid
				inner join `status` st on x.idstatus=st.idstatus
				inner join f_frecuency f on x.userid=f.userid
				INNER JOIN xima.usernotes n ON n.userid=x.userid
				where (s.statusafter=3
				and month(s.fechaupd)=$month and year(s.fechaupd)=$year) or (x.`idstatus`=3) 
				and (month(x.affiliationdate)<=$month and year(x.affiliationdate)<=$year)
				and ((f.idfrecuency=1) or (f.idfrecuency=2 and month(paydate)=$month and year(paydate)=$year))
				group by x.userid";*/
			/*$que="select x.userid, x.name, x.surname, x.`idstatus`, date(x.testdatebeguin) as signup,
				s.statusafter, date(s.fechaupd) as fechaupd,e.fechatrans,
				(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x.userid limit 1) paydate,
				n.notes, st.`status`, '' statusmonth,
				(SELECT date(fechatrans)  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month
				and year(fechatrans)=$year limit 1) ultimafecha,
				(SELECT monto  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month
				and year(fechatrans)=$year limit 1) ultimomonto from
				cobro_estadocuenta e inner join ximausrs x on e.userid=x.userid
				inner join status st on x.idstatus=st.idstatus
				left join logsstatus s on x.userid=s.userid
				left join f_frecuency f on x.userid=f.userid
				left JOIN xima.usernotes n ON x.userid=n.userid
				where month(fechatrans)=$month and year(fechatrans)=$year
				and e.idoper in (5,17) and ((s.statusafter=3
				and month(s.fechaupd)=$month and year(s.fechaupd)=$year) or (x.`idstatus` in (3,6,1)))
				and (month(x.affiliationdate)<=$month and year(x.affiliationdate)<=$year)
				group by x.userid";
				*/
							
				$que="SELECT distinct x.userid, x.name, x.surname,  
						(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x.userid limit 1) paydate,
						(SELECT DISTINCT idstatus FROM xima.usr_cobros u WHERE u.userid=x.userid limit 1) idstatus,
						(SELECT fr.`name` FROM f_frecuency f INNER JOIN frecuency fr ON f.idfrecuency=fr.idfrecuency WHERE f.`userid`=v.userid limit 1) frecuency,
						(
							SELECT DISTINCT status 
							FROM xima.usr_cobros u 
							INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u.idproductobase
							inner join status st on u.idstatus=st.idstatus
							WHERE u.userid=x.userid  and pb1.idproducto not in (11,12,13)
							limit 1
						) status,
						n.notes, 
						(	select st.status 
							FROM vuserlogstat v1 
							inner join status st on st.idstatus=v1.statusafter 
							WHERE v1.userid=v.userid and v1.fechaupd<'$year-$month-01' 
							ORDER BY v1.fechaupd desc 
							limit 1
						) statusmonth,
						(
							SELECT date(l.`fechaupd`) date FROM xima.logsstatus l
							WHERE l.`userid`=x.userid 
							ORDER BY l.`idstatuslogs` desc
							limit 1
						) ultimoupdate,
						(SELECT date(fechatrans)  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month and year(fechatrans)=$year limit 1) ultimafecha,
						(SELECT monto  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month and year(fechatrans)=$year limit 1) ultimomonto
						
						FROM vuserlogstat v
						INNER JOIN ximausrs x  on x.userid=v.userid
						left JOIN xima.usernotes n ON x.userid=n.userid
						where (select v1.statusafter FROM vuserlogstat v1 WHERE v1.userid=v.userid and v1.fechaupd<'$year-$month-01' ORDER BY v1.fechaupd desc limit 1) in (3,6)
						and x.idusertype not in (1,4,7,44) 
						";				
			//echo $que;
			if(strlen($order)>0){
				$que.=" ORDER BY $order $dir";
			}
			
			$quecount=$que;	
			$que.=" LIMIT ".$start.",".$limit;	

			//echo $que;
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$ifor=0;
			while ($row=mysql_fetch_object($result))
			{				
				if(!($row->frecuency=='Annually' && $row->ultimomonto==0) )
					if(!(strlen($row->paydate)==0 && strlen($row->idstatus)==0))
					{
						$que2='SELECT idproductobase 
							FROM xima.usr_cobros u
							inner join status st on u.idstatus=st.idstatus
							WHERE u.idproductobase=14 and  u.userid='.$row->userid;
							
						$result2=mysql_query ($que2) or die ($que2.mysql_error ());
						$row2=mysql_fetch_object($result2);
						if($row2->idproductobase=='')
						{
							$data [] = $row;
							$ifor++;
						}
					}
			}
			
			$result2 = mysql_query($quecount) or die ($quecount.mysql_error());
			$num_row=count($data);//mysql_num_rows($result2);
			$num_row=mysql_num_rows($result2);
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;			
		break;
		
		case 'affMarketing':
			$paffmarkmonth=$_POST['paffmarkmonth'];
			$paffmarkyear=$_POST['paffmarkyear'];
			$userid=$_POST['userid'];
			$data = array();	
			if($userid<>'' && $userid<>'*')
			{
				$que="SELECT
							0 Posi,c.`Userid`,(SELECT x.affiliationdate FROM ximausrs x WHERE x.`userid`=c.`useridhijo`)  affiliationdate,
							(SELECT concat_ws(' ',name,surname) FROM ximausrs x WHERE x.`userid`=c.`userid`) Name,
							o.titulo Operation,date(c.fechatrans) `Date`, c.payamount ,c.monto Amount,
							c.`useridhijo` Aff_Userid,
							(SELECT concat_ws(' ',name,surname) FROM ximausrs x WHERE x.`userid`=c.`useridhijo`) Aff_Name,
							(SELECT s.status FROM ximausrs x Inner JOIN status s on s.idstatus=x.idstatus WHERE x.`userid`=c.`useridhijo`)  Aff_status
						FROM cobro_estadocuenta c
						inner join cobro_operaciones o on c.idoper=o.idoper
						WHERE c.`userid`=$userid  and c.idoper in (7,8,9,11)
						and fechatrans between '$paffmarkyear-$paffmarkmonth-01 01:00:00' and '$paffmarkyear-$paffmarkmonth-31 23:59:59'
						order by fechatrans,idtrans";
				$result=mysql_query ($que) or die ($que.mysql_error ());
				$i=0;			
				while ($row=mysql_fetch_object($result))
				{
					$i++;
					$row->Posi=$i;
					$data [] = $row;
				}
			}
			echo '({"total":"'.count($data).'","results":'.json_encode($data).'})';
			return;
		break;
		
		case 'affMarketingReferrals':
				$pafflevel=$_POST['pafflevel'];
				$userid=$_POST['userid'];
				$data = array();	
				$que="SELECT x.name,x.surname FROM ximausrs x WHERE x.userid=$userid";
				$result3=mysql_query ($que) or die ($que.mysql_error ());
				$row3=mysql_fetch_object($result3);
							
				if($userid<>'' && $userid<>'*')
				{
				
					$que="SELECT 0 posi,x.userid,x.name,x.surname,o.status,x.email,x.hometelephone,x.mobiletelephone,x.affiliationdate,' ' father
							FROM ximausrs x
							inner join status o on x.idstatus=o.idstatus
							WHERE x.executive=$userid and x.idstatus not in (2,8,9,1,6,7)
							order by userid";
					$result=mysql_query ($que) or die ($que.mysql_error ());
					$i=0;			
					while ($row=mysql_fetch_object($result))
					{
						if($pafflevel==1)
						{
							$i++;
							$row->posi=$i;
							$row->father= $row3->name.' '.$row3->surname.' ('.$_POST['userid'].') ';
							$data [] = $row;
						}
						if($pafflevel==2)
						{
							$que="SELECT 0 posi,x.userid,x.name,x.surname,o.status,x.email,x.hometelephone,x.mobiletelephone,x.affiliationdate,' ' father
									FROM ximausrs x
									inner join status o on x.idstatus=o.idstatus
									WHERE x.executive=".$row->userid." and x.idstatus not in (2,8,9,1,6,7)
									order by userid";
							$result2=mysql_query ($que) or die ($que.mysql_error ());
							while ($row2=mysql_fetch_object($result2))
							{
								$i++;
								$row2->posi=$i;
								$row2->father= $row->name.' '.$row->surname.' ('.$row->userid.') ';
								$data [] = $row2;
							}							
						}
						
					}
				}
				echo '({"total":"'.count($data).'","results":'.json_encode($data).'})';
				return;
		break;
		
		case 'userssuspended':// cobros realizados en el dia
			$idtmail=$_POST['idtmail'];
			
			$dayfrom=$dayto=date('Y-m-d');
			if(isset($_POST['dayfrom']) && $_POST['dayfrom']<>'' && strlen($_POST['dayfrom'])>0 )
			{ 
				$dayfrom=$_POST['dayfrom'];
				$dayto=$_POST['dayto'];
			}
			
			$qwhe='';						
			$qwhe=" AND date(l.fechaupd) BETWEEN '$dayfrom' AND '$dayto' ";
			
			$selectsenddateemail=",'' senddateemail";
			$orderbysenddateemail="ORDER BY x1.userid";
			if($idtmail<>'')
			{
				$selectsenddateemail=",(
											SELECT e.`senddate`
											FROM xima.em_usersemails e
											WHERE e.`userid`=x1.userid AND e.`idtmail`=$idtmail
											ORDER BY e.`idumail` desc
											LIMIT 1
										) senddateemail";
				$orderbysenddateemail=" ORDER BY length(senddateemail),x1.userid";			
			
			}
			
			$que="SELECT distinct SQL_CALC_FOUND_ROWS x1.userid,x1.name,x1.surname, x1.email, l.fechaupd, n.notes,
					(
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=x1.`userid`  and pb1.idproducto not in (11,12,13)
						ORDER BY s1.status
						limit 1
					)  curstatus,'Suspended' oldstatus,
					(SELECT count(*) FROM logssession u where u.userid=x1.`userid` and DATE_SUB(CURDATE(),INTERVAL 30 DAY)<=sessiondate) as veceslog
					$selectsenddateemail
					FROM xima.ximausrs x1
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					inner join xima.logsstatus l on l.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					where ( l.statusafter=2 $qwhe		
					 ) 
					group by x1.userid
					$orderbysenddateemail 
					LIMIT ".$start.",".$limit;//  or x1.userid in (4,73,20,5)
/*
AND (
						SELECT distinct s1.idstatus
						FROM usr_cobros u1
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=x1.`userid`
						ORDER BY s1.status
						limit 1
					) =2
*/					
		break;

		case 'usersregistered':
			$quemain="	SELECT SQL_CALC_FOUND_ROWS u.id, u.`name`, u.`surname`, u.`email`, u.`pass`, u.`email2`, u.`pass2`, u.`hometelephone`, u.`mobiletelephone`, u.`procode`, u.`payamount`, 
					(SELECT p.`name` FROM usr_producto p WHERE p.`idproducto`=u.`idproduct`) `idproduct`,
					(SELECT f.`name` FROM frecuency f WHERE f.`idfrecuency`=u.`idfrecuency`) `idfrecuency`,
					if(u.`idstate`='ALL','ALL',((SELECT l.`State` FROM lsstate l WHERE l.`IdState`=u.`idstate`))) `idstate`, 
					if(u.`idcounty`='ALL','ALL',((SELECT l.`County` FROM lscounty l WHERE l.`IdCounty`=u.`idcounty`))) `idcounty`, 
					u.`questionuser`, u.`pseudonimo`, u.`answeruser`, 
					(SELECT t.`usertype` FROM usertype t WHERE t.`idusertype`=u.`idusertype`) `idusertype`, u.`licensen`, u.`officenum`,
					u.`address`, u.`city`, u.`state`, u.`zipcode`, u.`billingaddress`, u.`billingcity`, u.`billingstate`, u.`billingzip`, u.`cardholdername`,
					u.`cardtype`, u.`cardnumber`, u.`expirationmonth`, u.`expirationyear`, u.`csvnumber`, u.`insertdate`, u.`id`, u.`source`, u.`try`, u.`fieldsfailures`,
					'' userid,u.check,
					(SELECT concat(x.`NAME`,' ',x.`SURNAME`) userupdate FROM `ximausrs` x WHERE x.`USERID`=u.`updateuser`) `updateuser`,
					u.updaterow,u.updatenotes  					
					FROM  xima.usersregistered u 					
					ORDER BY u.check asc,u.`id` desc 
					LIMIT ".$start.",".$limit;
					
			$result=mysql_query ($quemain) or die ($que.mysql_error ());
			$data = array();
			$num_row=0;
			while ($row=mysql_fetch_object($result))
			{
				$useridaux='';
				$urid=$row->id;
				if($row->email<>'')
				{
					//if(strlen($row->cardnumber)>0)$row->cardnumber=substr($row->cardnumber,0,3).'*********'.substr($row->cardnumber,12,4); 
					//echo "<br>".
					$que="	SELECT x1.userid FROM xima.ximausrs x1 where x1.email like '".$row->email."%'  
							and  trim(x1.name)=trim('".$row->name."') 
							and  trim(x1.pass)=trim('".$row->pass."') limit 1";
					$result3=mysql_query ($que) or die ($que.mysql_error ());
					$row3=mysql_fetch_object($result3);
					if(strlen($row3->userid)==0)
						$row->try='FailureNotFirstPage'; //fallo no paso de la primera pagina
					else if(strlen($row3->userid)>0) 
					{
						//echo "<br>".
						$que="	SELECT userid FROM cobro_paypal c WHERE c.`userid`=".$row3->userid." AND lcase(c.`estado`)='success' limit 1";
						$result1=mysql_query ($que) or die ($que.mysql_error ());
						$row1=mysql_fetch_object($result1);
						if(strlen($row1->userid)>0)
						{
							$row->cardnumber='Empty'; 
							$row->csvnumber='Empty'; 
							$que="	UPDATE  xima.usersregistered 
									SET `cardnumber`='Empty',  `csvnumber`='Empty'	
									WHERE id=$urid";
							mysql_query ($que) or die ($que.mysql_error ());							
							$row->try='Success'; //paso de la primera pagina y se cobro
						}
						else
							$row->try='FailureNotBillPay'; //paso de la primera pagina pero no paso el cobro
					}
					$useridaux=$row3->userid;
				}
				$row->userid=$useridaux;
				
				if(	$row->source=='RegisterWorkshop' and strlen($row->userid)>0 )
				{
					$que="	SELECT r.`userid` 
							FROM xima.record_workshop r
							WHERE r.`userid`=".$row->userid."  
							and r.`idworkshop`=(SELECT w.`idworkshop` FROM xima.workshop w WHERE w.`status`=1 limit 1)";
					$result5=mysql_query ($que) or die ($que.mysql_error ());
					$row5=mysql_fetch_object($result5);
					if(strlen($row5->userid)==0)
						$row->try='FailureNotFirstPage'; //fallo no paso de la primera pagina
				}
				if($row->try=='Success' && strlen($row->userid)>0)
				{
					$que="	SELECT u1.idstatus 
							FROM usr_cobros u1 
							WHERE u1.`userid`=".$row->userid." 
							LIMIT 1	";					
					$result5=mysql_query ($que) or die ($que.mysql_error ());
					$row5=mysql_fetch_object($result5);
					if($row5->idstatus==2)
						$row->try='FailureNotFirstPageSuspend'; //fallo no paso de la primera pagina
				}

				$data [] = $row;
				$num_row++;
			}
			$pos=strrpos(strtolower($quemain),"limit");
			$que2=substr($quemain,0,$pos);
			$result2 = mysql_query($que2) or die ($que2.mysql_error());
			$num_row=mysql_num_rows($result2);

			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;			
		break;
		
		case 'training': 
			$que="	SELECT SQL_CALC_FOUND_ROWS 
				v.id, v.url, v.filename, v.activo, v.nombre
				FROM xima.training_videos v
				LIMIT ".$start.",".$limit;
		break;

		case 'manageWorkshop':
			$que=" SELECT w.`idworkshop`, w.`title`, w.`speaker`, w.`when`, w.`hour`, w.`status`,
						concat_ws('<br>', w.`address`, w.`city`, w.`zipcode`, w.`phone`) `comment`,
						p.precio  pricews 					
				FROM xima.workshop w
				INNER JOIN xima.usr_productobase p ON p.idproductobase=w.idproductobase
				LIMIT ".$start.",".$limit;
		break;

		case 'usersWorkshop':
			$idws=$_POST['ws'];
			$que=" SELECT r.idrecord_workshop,x.userid,x.name,x.surname,trim(x.email) email,x.hometelephone, r.idworkshop, c.`transactionID`, c.`amount`, c.`fecha`, ce.`cobradordesc`,
					(SELECT fr.`name` FROM f_frecuency f INNER JOIN frecuency fr ON f.idfrecuency=fr.idfrecuency WHERE f.`userid`=x.userid limit 1) frecuency 
					FROM xima.record_workshop r
					INNER JOIN xima.ximausrs x ON r.userid=x.USERID
					LEFT JOIN xima.cobro_paypal c ON r.paypalid=c.paypalid
					LEFT JOIN xima.cobro_estadocuenta ce ON ce.paypalid=c.paypalid
					WHERE r.idworkshop=$idws
					LIMIT ".$start.",".$limit;
		break;

		case 'textWorkshop':
			$idws=$_POST['idws'];
			$que=" SELECT w.`idworkshop`,concat(w.`title`,' - ', w.`when`,' - ', w.`hour`,(if(w.`status`=1,' - (Active)',''))) textcombo 
					FROM xima.workshop w 
					WHERE w.`idworkshop`=".$idws;

			$result=mysql_query ($que) or die ($que.mysql_error ());
			$row=mysql_fetch_object($result);
			$info = array(
				'success' => true,
				'msg' => 'Operation succesfully ',
				'textcombo' => $row->textcombo
			);
			echo json_encode($info);		
			
			return;
					
		break;
		
		case 'templateEmails':
			$que=" SELECT e.`idtmail`, e.`userid`, e.`insertdate`, e.`name`, e.`subject`,  e.`body`, '' `bodyshort`, concat(x.`name`,' ', x.`surname`) usercreator 
					FROM em_templateemails e
					INNER JOIN ximausrs x ON e.userid=x.USERID 
				LIMIT ".$start.",".$limit;
				$result=mysql_query ($que) or die ($que.mysql_error ());
			 
				$data = array();
				
				while ($row=mysql_fetch_object($result))
				{
					$row->bodyshort=substr(str_replace('nbsp;',' ',stripcslashes(htmlentities(strip_tags($row->body)))),0,100).'..';
					$data [] = $row;
				}
				$pos=strrpos(strtolower($que),"limit");
				$que2=substr($que,0,$pos);
				
				$result2 = mysql_query($que2) or die ($que2.mysql_error());
				$num_row=mysql_num_rows($result2);
				echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
				return;
		break;

		case 'previewEmails':
			$idtmail=trim($_POST['idtmail']);
			
			$que=" SELECT e.`name`,e.`subject`,  e.`body` 
					FROM em_templateemails e
					WHERE e.`idtmail`=".$idtmail;
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$row=mysql_fetch_object($result);
			//echo "<div style='bgcolor:white'>".$row->body."</div>";

			$info = array(
				'success' => true,
				'msg' => 'Operation succesfully ',
				'name' => $row->name,
				'subject' => $row->subject,
				'body' => $row->body
			);
			echo json_encode($info);		
			
			return;
		break;
		
		case 'usersEmails':
			$idtmail=trim($_POST['idtmail']);
		
			$que=" 	SELECT x.userid,x.name,x.surname,concat(x1.name,' ',x1.surname) usersender,em.senddate,em.idtmail,em.idumail
					FROM `xima`.`em_usersemails` em
					INNER JOIN xima.ximausrs x ON em.userid=x.USERID
					INNER JOIN xima.ximausrs x1 ON em.useridsender=x1.USERID 
					WHERE em.idtmail=$idtmail
					LIMIT ".$start.",".$limit;
			$result=mysql_query ($que) or die ($que.mysql_error ());
			 
		break;
	
		case 'ddusers':
			ini_set("session.gc_maxlifetime", 18000); 
			session_start(); 
			$start=0;
			$limit=50;
			
			if($_POST['start']<>'' && isset($_POST['start']))
			{
				$start=$_POST['start'];
				$limit=$_POST['limit'];
			}				
			$idtmail=$_POST['idtmail'];
			$selectsenddateemail=",'' senddateemail";
			$orderbysenddateemail="ORDER BY xima.daveusers.userid";
			if($_POST['sort']<>'')	$orderbysenddateemail="ORDER BY ".$_POST['sort']." ".$_POST['dir'];
			if($idtmail<>'')
			{
				$selectsenddateemail=",(
											SELECT e.`senddate`
											FROM xima.daveusersemails e
											WHERE e.`userid`=xima.daveusers.userid AND e.`idtmail`=$idtmail
											ORDER BY e.`idumail` desc
											LIMIT 1
										) senddateemail";
				//$orderbysenddateemail="ORDER BY xima.ximausrs.userid";
				//$orderbysenddateemail=" ORDER BY length(senddateemail),x1.userid";			
			
			}

			$que="	SELECT DISTINCT SQL_CALC_FOUND_ROWS
					xima.daveusers.userid,
					xima.daveusers.name,
					xima.daveusers.surname,
					xima.daveusers.email,
					xima.daveusers.pass,
					xima.daveusers.hometelephone,
					xima.daveusers.affiliationdate singup
					$selectsenddateemail
					FROM xima.daveusers 
					WHERE 1=1 ANd ";//((xima.ximausrs.userid in (4,5) or  xima.ximausrs.idusertype not in (1)) and (xima.ximausrs.userid in (2482))
			if(isset($_SESSION['filters'])){ 
				 //echo 
				 $que.=$_SESSION['filters']."  ";
			}	
					
			$que.=" $orderbysenddateemail ";
			$que.="LIMIT ".$start.",".$limit;

			//echo $que;
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$totalprice=0;
			while ($row=mysql_fetch_object($result))
			{
				$data [] = $row;
			}
			
			$pos=strrpos(strtolower($que),"limit");
			$que2=substr($que,0,$pos);
			
			$result2 = mysql_query($que2) or die ($que2.mysql_error());
			$fullids='';
			$num_row=0;
			while ($row2=mysql_fetch_object($result2))
			{
				if($num_row>0)$fullids.=',';
				$fullids.=$row2->userid;
				$num_row++;
			}	
			//$_SESSION['fullids']=$fullids;
			//$num_row=mysql_num_rows($result2);
			echo '({"total":"'.$num_row.'","totalprice":"'.$totalprice.'","results":'.json_encode($data).'})';
			
			return;
		break;

		case 'fileslist':
			$idtmail=$_POST['idtmail'];

			$que="	SELECT `idatt`, `pathsaved`, `namefile`, `sizefile`, `idtmail` 
					FROM `em_attachments`
					WHERE idtmail=".$idtmail;
			$order=$_POST['sort'];
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			else
				$que.=" ORDER BY idatt";			
			
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$num_row=mysql_num_rows($result);
			$data = array();
			while ($row=mysql_fetch_object($result))
					$data [] = $row;
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
		break;
		case 'xrayCompData':			
			$que="SELECT * FROM xima.xray ";
			$result=mysql_query ($que) or die ($que.mysql_error ());
		 
			$data = array();
			$data2 = array();
			
			while ($row=mysql_fetch_object($result))
				$data [] = $row;
			
			$data2[]=array("accion"=>"Total Properties","actual"=>$data[0]->total,"nueva"=>$data[1]->total,"diff"=>$data[2]->total);
			$data2[]=array("accion"=>"Distress Properties","actual"=>$data[0]->distress,"nueva"=>$data[1]->distress,"diff"=>$data[2]->distress);
			$data2[]=array("accion"=>"Pct. Distress Properties","actual"=>$data[0]->pctdistress,"nueva"=>$data[1]->pctdistress,"diff"=>$data[2]->pctdistress);
			$data2[]=array("accion"=>"Pre-Foreclosure","actual"=>$data[0]->preforeclosure,"nueva"=>$data[1]->preforeclosure,"diff"=>$data[2]->preforeclosure);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosure","actual"=>$data[0]->pctpreforeclosure,"nueva"=>$data[1]->pctpreforeclosure,"diff"=>$data[2]->pctpreforeclosure);
			$data2[]=array("accion"=>"Foreclosure","actual"=>$data[0]->foreclosure,"nueva"=>$data[1]->foreclosure,"diff"=>$data[2]->foreclosure);
			$data2[]=array("accion"=>"Pct. Foreclosure","actual"=>$data[0]->pctforeclosure,"nueva"=>$data[1]->pctforeclosure,"diff"=>$data[2]->pctforeclosure);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure","actual"=>$data[0]->upsidedown,"nueva"=>$data[1]->upsidedown,"diff"=>$data[2]->upsidedown);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure","actual"=>$data[0]->pctupsidedown,"nueva"=>$data[1]->pctupsidedown,"diff"=>$data[2]->pctupsidedown);
			$data2[]=array("accion"=>"Active For Sale","actual"=>$data[0]->sale,"nueva"=>$data[1]->sale,"diff"=>$data[2]->sale);
			$data2[]=array("accion"=>"Pct. Active For Sale","actual"=>$data[0]->pctsale,"nueva"=>$data[1]->pctsale,"diff"=>$data[2]->pctsale);
			$data2[]=array("accion"=>"Owner Occupied","actual"=>$data[0]->ownerY,"nueva"=>$data[1]->ownerY,"diff"=>$data[2]->ownerY);
			$data2[]=array("accion"=>"Pct. Owner Occupied","actual"=>$data[0]->pctownerY,"nueva"=>$data[1]->pctownerY,"diff"=>$data[2]->pctownerY);
			$data2[]=array("accion"=>"Not Owner Occupied","actual"=>$data[0]->ownerN,"nueva"=>$data[1]->ownerN,"diff"=>$data[2]->ownerN);
			$data2[]=array("accion"=>"Pct. Not Owner Occupied","actual"=>$data[0]->pctownerN,"nueva"=>$data[1]->pctownerN,"diff"=>$data[2]->pctownerN);
			$data2[]=array("accion"=>"Pre-Foreclosures Active For Sale","actual"=>$data[0]->total_sold_P,"nueva"=>$data[1]->total_sold_P,"diff"=>$data[2]->total_sold_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures Active For Sale","actual"=>$data[0]->pcttotal_sold_P,"nueva"=>$data[1]->pcttotal_sold_P,"diff"=>$data[2]->pcttotal_sold_P);
			$data2[]=array("accion"=>"Pre-Foreclosures Not For Sale","actual"=>$data[0]->total_no_sold_P,"nueva"=>$data[1]->total_no_sold_P,"diff"=>$data[2]->total_no_sold_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures Not For Sale","actual"=>$data[0]->pcttotal_no_sold_P,"nueva"=>$data[1]->pcttotal_no_sold_P,"diff"=>$data[2]->pcttotal_no_sold_P);
			$data2[]=array("accion"=>"Pre-Foreclosures with less than 0% equity","actual"=>$data[0]->total_P_0,"nueva"=>$data[1]->total_P_0,"diff"=>$data[2]->total_P_0);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures with less than 0% equity","actual"=>$data[0]->pcttotal_P_0,"nueva"=>$data[1]->pcttotal_P_0,"diff"=>$data[2]->pcttotal_P_0);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 0% equity","actual"=>$data[0]->total_P_0_mas,"nueva"=>$data[1]->total_P_0_mas,"diff"=>$data[2]->total_P_0_mas);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 0% equity","actual"=>$data[0]->pcttotal_P_0_mas,"nueva"=>$data[1]->pcttotal_P_0_mas,"diff"=>$data[2]->pcttotal_P_0_mas);
			$data2[]=array("accion"=>"Pre-Foreclosures with more than 30% equity","actual"=>$data[0]->total_P_30,"nueva"=>$data[1]->total_P_30,"diff"=>$data[2]->total_P_30);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures with more than 30% equity","actual"=>$data[0]->pcttotal_P_30,"nueva"=>$data[1]->pcttotal_P_30,"diff"=>$data[2]->pcttotal_P_30);
			$data2[]=array("accion"=>"Foreclosures Active For Sale","actual"=>$data[0]->total_sold_F,"nueva"=>$data[1]->total_sold_F,"diff"=>$data[2]->total_sold_F);
			$data2[]=array("accion"=>"Pct. Foreclosures Active For Sale","actual"=>$data[0]->pcttotal_sold_F,"nueva"=>$data[1]->pcttotal_sold_F,"diff"=>$data[2]->pcttotal_sold_F);
			$data2[]=array("accion"=>"Foreclosures Not For Sale","actual"=>$data[0]->total_no_sold_F,"nueva"=>$data[1]->total_no_sold_F,"diff"=>$data[2]->total_no_sold_F);
			$data2[]=array("accion"=>"Pct. Foreclosures Not For Sale","actual"=>$data[0]->pcttotal_no_sold_F,"nueva"=>$data[1]->pcttotal_no_sold_F,"diff"=>$data[2]->pcttotal_no_sold_F);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure Active For Sale","actual"=>$data[0]->total_sold_UD,"nueva"=>$data[1]->total_sold_UD,"diff"=>$data[2]->total_sold_UD);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure Active For Sale","actual"=>$data[0]->pcttotal_sold_UD,"nueva"=>$data[1]->pcttotal_sold_UD,"diff"=>$data[2]->pcttotal_sold_UD);
			$data2[]=array("accion"=>"Upside Down not in Foreclosure Not For sale","actual"=>$data[0]->total_no_sold_UD,"nueva"=>$data[1]->total_no_sold_UD,"diff"=>$data[2]->total_no_sold_UD);
			$data2[]=array("accion"=>"Pct. Upside Down not in Foreclosure Not For sale","actual"=>$data[0]->pcttotal_no_sold_UD,"nueva"=>$data[1]->pcttotal_no_sold_UD,"diff"=>$data[2]->pcttotal_no_sold_UD);
			$data2[]=array("accion"=>"Properties For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE,"nueva"=>$data[1]->total_sold_PE,"diff"=>$data[2]->total_sold_PE);
			$data2[]=array("accion"=>"Pct. Properties For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE,"nueva"=>$data[1]->pcttotal_sold_PE,"diff"=>$data[2]->pcttotal_sold_PE);
			$data2[]=array("accion"=>"Pre-Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE_P,"nueva"=>$data[1]->total_sold_PE_P,"diff"=>$data[2]->total_sold_PE_P);
			$data2[]=array("accion"=>"Pct. Pre-Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE_P,"nueva"=>$data[1]->pcttotal_sold_PE_P,"diff"=>$data[2]->pcttotal_sold_PE_P);
			$data2[]=array("accion"=>"Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->total_sold_PE_F,"nueva"=>$data[1]->total_sold_PE_F,"diff"=>$data[2]->total_sold_PE_F);
			$data2[]=array("accion"=>"Pct. Foreclosures For Sale with 30% equity or more","actual"=>$data[0]->pcttotal_sold_PE_F,"nueva"=>$data[1]->pcttotal_sold_PE_F,"diff"=>$data[2]->pcttotal_sold_PE_F);
			
			$data2[]=array("accion"=>"Total Sold in the Last 6 months","actual"=>$data[0]->total_sold_3,"nueva"=>$data[1]->total_sold_3,"diff"=>$data[2]->total_sold_3);
			$data2[]=array("accion"=>"Average Sold in the last 6 moths","actual"=>$data[0]->a_sold_3,"nueva"=>$data[1]->a_sold_3,"diff"=>$data[2]->a_sold_3);
			$data2[]=array("accion"=>"Months of inventory","actual"=>$data[0]->invM,"nueva"=>$data[1]->invM,"diff"=>$data[2]->invM);
			$data2[]=array("accion"=>"Average Days on Market","actual"=>$data[0]->a_days,"nueva"=>$data[1]->a_days,"diff"=>$data[2]->a_days);
			$data2[]=array("accion"=>"Average Days on Market of 'A'","actual"=>$data[0]->a_days_a,"nueva"=>$data[1]->a_days_a,"diff"=>$data[2]->a_days_a);
			
			
			$data2[]=array("accion"=>"Average Sold Price 12 months","actual"=>$data[0]->a_asp12,"nueva"=>$data[1]->a_asp12,"diff"=>$data[2]->a_asp12);
			$data2[]=array("accion"=>"Median Sold Price 12 months","actual"=>$data[0]->m_asp12,"nueva"=>$data[1]->m_asp12,"diff"=>$data[2]->m_asp12);
			$data2[]=array("accion"=>"Average Sold Price 9 months","actual"=>$data[0]->a_asp9,"nueva"=>$data[1]->a_asp9,"diff"=>$data[2]->a_asp9);
			$data2[]=array("accion"=>"Median Sold Price 9 months","actual"=>$data[0]->m_asp9,"nueva"=>$data[1]->m_asp9,"diff"=>$data[2]->m_asp9);
			$data2[]=array("accion"=>"Average Sold Price 6 months","actual"=>$data[0]->a_asp6,"nueva"=>$data[1]->a_asp6,"diff"=>$data[2]->a_asp6);
			$data2[]=array("accion"=>"Median Sold Price 6 months","actual"=>$data[0]->m_asp6,"nueva"=>$data[1]->m_asp6,"diff"=>$data[2]->m_asp6);
			$data2[]=array("accion"=>"Average Sold Price 3 months","actual"=>$data[0]->a_asp3,"nueva"=>$data[1]->a_asp3,"diff"=>$data[2]->a_asp3);
			$data2[]=array("accion"=>"Median Sold Price 3 months","actual"=>$data[0]->m_asp3,"nueva"=>$data[1]->m_asp3,"diff"=>$data[2]->m_asp3);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes1,4,2)),1,intval(substr($data[0]->mes1,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes1CS,"nueva"=>$data[1]->mes1CS,"diff"=>$data[2]->mes1CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes1CC,"nueva"=>$data[1]->mes1CC,"diff"=>$data[2]->mes1CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes2,4,2)),1,intval(substr($data[0]->mes2,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes2CS,"nueva"=>$data[1]->mes2CS,"diff"=>$data[2]->mes2CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes2CC,"nueva"=>$data[1]->mes2CC,"diff"=>$data[2]->mes2CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes3,4,2)),1,intval(substr($data[0]->mes3,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes3CS,"nueva"=>$data[1]->mes3CS,"diff"=>$data[2]->mes3CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes3CC,"nueva"=>$data[1]->mes3CC,"diff"=>$data[2]->mes3CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes4,4,2)),1,intval(substr($data[0]->mes4,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes4CS,"nueva"=>$data[1]->mes4CS,"diff"=>$data[2]->mes4CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes4CC,"nueva"=>$data[1]->mes4CC,"diff"=>$data[2]->mes4CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes5,4,2)),1,intval(substr($data[0]->mes5,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes5CS,"nueva"=>$data[1]->mes5CS,"diff"=>$data[2]->mes5CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes5CC,"nueva"=>$data[1]->mes5CC,"diff"=>$data[2]->mes5CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes6,4,2)),1,intval(substr($data[0]->mes6,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes6CS,"nueva"=>$data[1]->mes6CS,"diff"=>$data[2]->mes6CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes6CC,"nueva"=>$data[1]->mes6CC,"diff"=>$data[2]->mes6CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes7,4,2)),1,intval(substr($data[0]->mes7,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes7CS,"nueva"=>$data[1]->mes7CS,"diff"=>$data[2]->mes7CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes7CC,"nueva"=>$data[1]->mes7CC,"diff"=>$data[2]->mes7CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes8,4,2)),1,intval(substr($data[0]->mes8,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes8CS,"nueva"=>$data[1]->mes8CS,"diff"=>$data[2]->mes8CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes8CC,"nueva"=>$data[1]->mes8CC,"diff"=>$data[2]->mes8CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes9,4,2)),1,intval(substr($data[0]->mes9,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes9CS,"nueva"=>$data[1]->mes9CS,"diff"=>$data[2]->mes9CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes9CC,"nueva"=>$data[1]->mes9CC,"diff"=>$data[2]->mes9CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes10,4,2)),1,intval(substr($data[0]->mes10,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes10CS,"nueva"=>$data[1]->mes10CS,"diff"=>$data[2]->mes10CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes10CC,"nueva"=>$data[1]->mes10CC,"diff"=>$data[2]->mes10CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes11,4,2)),1,intval(substr($data[0]->mes11,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes11CS,"nueva"=>$data[1]->mes11CS,"diff"=>$data[2]->mes11CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes11CC,"nueva"=>$data[1]->mes11CC,"diff"=>$data[2]->mes11CC);
			
			$fecha=date('M Y',mktime(0,0,0,intval(substr($data[0]->mes12,4,2)),1,intval(substr($data[0]->mes12,0,4))));
			$data2[]=array("accion"=>"Statistic CS $fecha","actual"=>$data[0]->mes12CS,"nueva"=>$data[1]->mes12CS,"diff"=>$data[2]->mes12CS);
			$data2[]=array("accion"=>"Statistic CC $fecha","actual"=>$data[0]->mes12CC,"nueva"=>$data[1]->mes12CC,"diff"=>$data[2]->mes12CC);
			
			echo '({"total":"'.count($data2).'","results":'.json_encode($data2).'})';
			
			return 0;
		break;
		case 'xrayComp':
			function porcentaje($val){
				return number_format($val, 2, '.', '');
			}
			
			$array=array();
			$array2=array();
			
			$county_xray = str_replace('/','',$_POST['filter']);
			$proptype_xray = $_POST['filter2']==-1?'':$_POST['filter2'];
			
			
			$que="SELECT l.bd from xima.lsprogram l inner join xima.lscounty c on (l.idcounty=c.idcounty) 
					WHERE l.idprogram=1 and c.BD='".strtolower($county_xray)."'";
			//echo $que;
			$rest=mysql_query ($que) or die ($que.mysql_error ());
			$r=mysql_fetch_array($rest);
			if(intval($r[0])<=0){ $actual=$county_xray; $nueva=$county_xray.'1';}
			else{ $actual=$county_xray.'1'; $nueva=$county_xray;}
			
			//echo $actual.' '.$nueva;
			$conex=conectar($actual);
			include("xray.php");
			
			$array=array("tipo"=>"BD1","data"=>"Actual","proptype"=>$proptype_xray,"total"=>$total,"distress"=>$total_D,"pctdistress"=>$pct_D,"preforeclosure"=>$total_P,"pctpreforeclosure"=>$pct_P,"foreclosure"=>$total_F,"pctforeclosure"=>$pct_F,"upsidedown"=>$total_UD,"pctupsidedown"=>$pct_UD,"sale"=>$total_sold,"pctsale"=>$pct_sold,"ownerY"=>$total_owner_Y,"pctownerY"=>$pct_owner_Y,"ownerN"=>$total_owner_N,"pctownerN"=>$pct_owner_N,"total_sold_P"=>$total_sold_P,"pcttotal_sold_P"=>$pct_sold_P,"total_no_sold_P"=>$total_no_sold_P,"pcttotal_no_sold_P"=>$pct_no_sold_P,"total_P_0"=>$total_P_0,"pcttotal_P_0"=>$pct_P_0,"total_P_0_mas"=>$total_P_0_mas,"pcttotal_P_0_mas"=>$pct_P_0_mas,"total_P_30"=>$total_P_30,"pcttotal_P_30"=>$pct_P_30,"total_sold_F"=>$total_sold_F,"pcttotal_sold_F"=>$pct_sold_F,"total_no_sold_F"=>$total_no_sold_F,"pcttotal_no_sold_F"=>$pct_no_sold_F,"total_sold_UD"=>$total_sold_UD,"pcttotal_sold_UD"=>$pct_sold_UD,"total_no_sold_UD"=>$total_no_sold_UD,"pcttotal_no_sold_UD"=>$pct_no_sold_UD,"total_sold_PE"=>$total_sold_PE,"pcttotal_sold_PE"=>$pct_sold_PE,"total_sold_PE_P"=>$total_sold_PE_P,"pcttotal_sold_PE_P"=>$pct_sold_PE_P,"total_sold_PE_F"=>$total_sold_PE_F,"pcttotal_sold_PE_F"=>$pct_sold_PE_F,"invM"=>$invM,"a_days"=>$a_days,"a_days_a"=>$a_days_a,"total_sold_3"=>$total_sold_3,"a_sold_3"=>$a_sold_3,"a_asp12"=>$a_asp12,"m_asp12"=>$m_asp12,"a_asp9"=>$a_asp9,"m_asp9"=>$m_asp9,"a_asp6"=>$a_asp6,"m_asp6"=>$m_asp6,"a_asp3"=>$a_asp3,"m_asp3"=>$m_asp3,"mes1"=>$fechames[0],"mes1CC"=>$qtCC1,"mes1CS"=>$qtCS1,"mes2"=>$fechames[1],"mes2CC"=>$qtCC2,"mes2CS"=>$qtCS2,"mes3"=>$fechames[2],"mes3CC"=>$qtCC3,"mes3CS"=>$qtCS3,"mes4"=>$fechames[3],"mes4CC"=>$qtCC4,"mes4CS"=>$qtCS4,"mes5"=>$fechames[4],"mes5CC"=>$qtCC5,"mes5CS"=>$qtCS5,"mes6"=>$fechames[5],"mes6CC"=>$qtCC6,"mes6CS"=>$qtCS6,"mes7"=>$fechames[6],"mes7CC"=>$qtCC7,"mes7CS"=>$qtCS7,"mes8"=>$fechames[7],"mes8CC"=>$qtCC8,"mes8CS"=>$qtCS8,"mes9"=>$fechames[8],"mes9CC"=>$qtCC9,"mes9CS"=>$qtCS9,"mes10"=>$fechames[9],"mes10CC"=>$qtCC10,"mes10CS"=>$qtCS10,"mes11"=>$fechames[10],"mes11CC"=>$qtCC11,"mes11CS"=>$qtCS11,"mes12"=>$fechames[11],"mes12CC"=>$qtCC12,"mes12CS"=>$qtCS12);
			
			$conex=conectar($nueva);
			include("xray.php");
			
			$array2=array("tipo"=>"BD2","data"=>"Nueva","proptype"=>$proptype_xray,"total"=>$total,"distress"=>$total_D,"pctdistress"=>$pct_D,"preforeclosure"=>$total_P,"pctpreforeclosure"=>$pct_P,"foreclosure"=>$total_F,"pctforeclosure"=>$pct_F,"upsidedown"=>$total_UD,"pctupsidedown"=>$pct_UD,"sale"=>$total_sold,"pctsale"=>$pct_sold,"ownerY"=>$total_owner_Y,"pctownerY"=>$pct_owner_Y,"ownerN"=>$total_owner_N,"pctownerN"=>$pct_owner_N,"total_sold_P"=>$total_sold_P,"pcttotal_sold_P"=>$pct_sold_P,"total_no_sold_P"=>$total_no_sold_P,"pcttotal_no_sold_P"=>$pct_no_sold_P,"total_P_0"=>$total_P_0,"pcttotal_P_0"=>$pct_P_0,"total_P_0_mas"=>$total_P_0_mas,"pcttotal_P_0_mas"=>$pct_P_0_mas,"total_P_30"=>$total_P_30,"pcttotal_P_30"=>$pct_P_30,"total_sold_F"=>$total_sold_F,"pcttotal_sold_F"=>$pct_sold_F,"total_no_sold_F"=>$total_no_sold_F,"pcttotal_no_sold_F"=>$pct_no_sold_F,"total_sold_UD"=>$total_sold_UD,"pcttotal_sold_UD"=>$pct_sold_UD,"total_no_sold_UD"=>$total_no_sold_UD,"pcttotal_no_sold_UD"=>$pct_no_sold_UD,"total_sold_PE"=>$total_sold_PE,"pcttotal_sold_PE"=>$pct_sold_PE,"total_sold_PE_P"=>$total_sold_PE_P,"pcttotal_sold_PE_P"=>$pct_sold_PE_P,"total_sold_PE_F"=>$total_sold_PE_F,"pcttotal_sold_PE_F"=>$pct_sold_PE_F,"invM"=>$invM,"a_days"=>$a_days,"a_days_a"=>$a_days_a,"total_sold_3"=>$total_sold_3,"a_sold_3"=>$a_sold_3,"a_asp12"=>$a_asp12,"m_asp12"=>$m_asp12,"a_asp9"=>$a_asp9,"m_asp9"=>$m_asp9,"a_asp6"=>$a_asp6,"m_asp6"=>$m_asp6,"a_asp3"=>$a_asp3,"m_asp3"=>$m_asp3,"mes1"=>$fechames[0],"mes1CC"=>$qtCC1,"mes1CS"=>$qtCS1,"mes2"=>$fechames[1],"mes2CC"=>$qtCC2,"mes2CS"=>$qtCS2,"mes3"=>$fechames[2],"mes3CC"=>$qtCC3,"mes3CS"=>$qtCS3,"mes4"=>$fechames[3],"mes4CC"=>$qtCC4,"mes4CS"=>$qtCS4,"mes5"=>$fechames[4],"mes5CC"=>$qtCC5,"mes5CS"=>$qtCS5,"mes6"=>$fechames[5],"mes6CC"=>$qtCC6,"mes6CS"=>$qtCS6,"mes7"=>$fechames[6],"mes7CC"=>$qtCC7,"mes7CS"=>$qtCS7,"mes8"=>$fechames[7],"mes8CC"=>$qtCC8,"mes8CS"=>$qtCS8,"mes9"=>$fechames[8],"mes9CC"=>$qtCC9,"mes9CS"=>$qtCS9,"mes10"=>$fechames[9],"mes10CC"=>$qtCC10,"mes10CS"=>$qtCS10,"mes11"=>$fechames[10],"mes11CC"=>$qtCC11,"mes11CS"=>$qtCS11,"mes12"=>$fechames[11],"mes12CC"=>$qtCC12,"mes12CS"=>$qtCS12);
			
			//echo '({"total":"2","results":['.json_encode($array).','.json_encode($array2).']})';
			mysql_query ('TRUNCATE TABLE xima.xray') or die (mysql_error ());
			$que='INSERT INTO xima.xray VALUES ("'.$array['tipo'].'","'.$array['data'].'","'.$array['proptype'].'",'.$array['total'].','.$array['distress'].','.$array['pctdistress'].','.$array['preforeclosure'].','.$array['pctpreforeclosure'].','.$array['foreclosure'].','.$array['pctforeclosure'].','.$array['upsidedown'].','.$array['pctupsidedown'].','.$array['sale'].','.$array['pctsale'].','.$array['ownerY'].','.$array['pctownerY'].','.$array['ownerN'].','.$array['pctownerN'].','.$array['total_sold_P'].','.$array['pcttotal_sold_P'].','.$array['total_no_sold_P'].','.$array['pcttotal_no_sold_P'].','.$array['total_P_0'].','.$array['pcttotal_P_0'].','.$array['total_P_0_mas'].','.$array['pcttotal_P_0_mas'].','.$array['total_P_30'].','.$array['pcttotal_P_30'].','.$array['total_sold_F'].','.$array['pcttotal_sold_F'].','.$array['total_no_sold_F'].','.$array['pcttotal_no_sold_F'].','.$array['total_sold_UD'].','.$array['pcttotal_sold_UD'].','.$array['total_no_sold_UD'].','.$array['pcttotal_no_sold_UD'].','.$array['total_sold_PE'].','.$array['pcttotal_sold_PE'].','.$array['total_sold_PE_P'].','.$array['pcttotal_sold_PE_P'].','.$array['total_sold_PE_F'].','.$array['pcttotal_sold_PE_F'].','.$array['invM'].','.$array['a_days'].','.$array['a_days_a'].','.$array['total_sold_3'].','.$array['a_sold_3'].','.$array['a_asp12'].','.$array['m_asp12'].','.$array['a_asp9'].','.$array['m_asp9'].','.$array['a_asp6'].','.$array['m_asp6'].','.$array['a_asp3'].','.$array['m_asp3'].','.$array['mes1'].','.$array['mes1CC'].','.$array['mes1CS'].','.$array['mes2'].','.$array['mes2CC'].','.$array['mes2CS'].','.$array['mes3'].','.$array['mes3CC'].','.$array['mes3CS'].','.$array['mes4'].','.$array['mes4CC'].','.$array['mes4CS'].','.$array['mes5'].','.$array['mes5CC'].','.$array['mes5CS'].','.$array['mes6'].','.$array['mes6CC'].','.$array['mes6CS'].','.$array['mes7'].','.$array['mes7CC'].','.$array['mes7CS'].','.$array['mes8'].','.$array['mes8CC'].','.$array['mes8CS'].','.$array['mes9'].','.$array['mes9CC'].','.$array['mes9CS'].','.$array['mes10'].','.$array['mes10CC'].','.$array['mes10CS'].','.$array['mes11'].','.$array['mes11CC'].','.$array['mes11CS'].','.$array['mes12'].','.$array['mes12CC'].','.$array['mes12CS'].'),("'.$array2['tipo'].'","'.$array2['data'].'","'.$array2['proptype'].'",'.$array2['total'].','.$array2['distress'].','.$array2['pctdistress'].','.$array2['preforeclosure'].','.$array2['pctpreforeclosure'].','.$array2['foreclosure'].','.$array2['pctforeclosure'].','.$array2['upsidedown'].','.$array2['pctupsidedown'].','.$array2['sale'].','.$array2['pctsale'].','.$array2['ownerY'].','.$array2['pctownerY'].','.$array2['ownerN'].','.$array2['pctownerN'].','.$array2['total_sold_P'].','.$array2['pcttotal_sold_P'].','.$array2['total_no_sold_P'].','.$array2['pcttotal_no_sold_P'].','.$array2['total_P_0'].','.$array2['pcttotal_P_0'].','.$array2['total_P_0_mas'].','.$array2['pcttotal_P_0_mas'].','.$array2['total_P_30'].','.$array2['pcttotal_P_30'].','.$array2['total_sold_F'].','.$array2['pcttotal_sold_F'].','.$array2['total_no_sold_F'].','.$array2['pcttotal_no_sold_F'].','.$array2['total_sold_UD'].','.$array2['pcttotal_sold_UD'].','.$array2['total_no_sold_UD'].','.$array2['pcttotal_no_sold_UD'].','.$array2['total_sold_PE'].','.$array2['pcttotal_sold_PE'].','.$array2['total_sold_PE_P'].','.$array2['pcttotal_sold_PE_P'].','.$array2['total_sold_PE_F'].','.$array2['pcttotal_sold_PE_F'].','.$array2['invM'].','.$array2['a_days'].','.$array2['a_days_a'].','.$array2['total_sold_3'].','.$array2['a_sold_3'].','.$array2['a_asp12'].','.$array2['m_asp12'].','.$array2['a_asp9'].','.$array2['m_asp9'].','.$array2['a_asp6'].','.$array2['m_asp6'].','.$array2['a_asp3'].','.$array2['m_asp3'].','.$array2['mes1'].','.$array2['mes1CC'].','.$array2['mes1CS'].','.$array2['mes2'].','.$array2['mes2CC'].','.$array2['mes2CS'].','.$array2['mes3'].','.$array2['mes3CC'].','.$array2['mes3CS'].','.$array2['mes4'].','.$array2['mes4CC'].','.$array2['mes4CS'].','.$array2['mes5'].','.$array2['mes5CC'].','.$array2['mes5CS'].','.$array2['mes6'].','.$array2['mes6CC'].','.$array2['mes6CS'].','.$array2['mes7'].','.$array2['mes7CC'].','.$array2['mes7CS'].','.$array2['mes8'].','.$array2['mes8CC'].','.$array2['mes8CS'].','.$array2['mes9'].','.$array2['mes9CC'].','.$array2['mes9CS'].','.$array2['mes10'].','.$array2['mes10CC'].','.$array2['mes10CS'].','.$array2['mes11'].','.$array2['mes11CC'].','.$array2['mes11CS'].','.$array2['mes12'].','.$array2['mes12CC'].','.$array2['mes12CS'].'),("DIFF","DIFF","'.$array2['proptype'].'",'.($array['total']-$array2['total']).','.($array['distress']-$array2['distress']).','.($array['pctdistress']-$array2['pctdistress']).','.($array['preforeclosure']-$array2['preforeclosure']).','.($array['pctpreforeclosure']-$array2['pctpreforeclosure']).','.($array['foreclosure']-$array2['foreclosure']).','.($array['pctforeclosure']-$array2['pctforeclosure']).','.($array['upsidedown']-$array2['upsidedown']).','.($array['pctupsidedown']-$array2['pctupsidedown']).','.($array['sale']-$array2['sale']).','.($array['pctsale']-$array2['pctsale']).','.($array['ownerY']-$array2['ownerY']).','.($array['pctownerY']-$array2['pctownerY']).','.($array['ownerN']-$array2['ownerN']).','.($array['pctownerN']-$array2['pctownerN']).','.($array['total_sold_P']-$array2['total_sold_P']).','.($array['pcttotal_sold_P']-$array2['pcttotal_sold_P']).','.($array['total_no_sold_P']-$array2['total_no_sold_P']).','.($array['pcttotal_no_sold_P']-$array2['pcttotal_no_sold_P']).','.($array['total_P_0']-$array2['total_P_0']).','.($array['pcttotal_P_0']-$array2['pcttotal_P_0']).','.($array['total_P_0_mas']-$array2['total_P_0_mas']).','.($array['pcttotal_P_0_mas']-$array2['pcttotal_P_0_mas']).','.($array['total_P_30']-$array2['total_P_30']).','.($array['pcttotal_P_30']-$array2['pcttotal_P_30']).','.($array['total_sold_F']-$array2['total_sold_F']).','.($array['pcttotal_sold_F']-$array2['pcttotal_sold_F']).','.($array['total_no_sold_F']-$array2['total_no_sold_F']).','.($array['pcttotal_no_sold_F']-$array2['pcttotal_no_sold_F']).','.($array['total_sold_UD']-$array2['total_sold_UD']).','.($array['pcttotal_sold_UD']-$array2['pcttotal_sold_UD']).','.($array['total_no_sold_UD']-$array2['total_no_sold_UD']).','.($array['pcttotal_no_sold_UD']-$array2['pcttotal_no_sold_UD']).','.($array2['total_sold_PE']-$array['total_sold_PE']).','.($array['pcttotal_sold_PE']-$array2['pcttotal_sold_PE']).','.($array['total_sold_PE_P']-$array2['total_sold_PE_P']).','.($array['pcttotal_sold_PE_P']-$array2['pcttotal_sold_PE_P']).','.($array['total_sold_PE_F']-$array2['total_sold_PE_F']).','.($array['pcttotal_sold_PE_F']-$array2['pcttotal_sold_PE_F']).','.($array['invM']-$array2['invM']).','.($array['a_days']-$array2['a_days']).','.($array['a_days_a']-$array2['a_days_a']).','.($array['total_sold_3']-$array2['total_sold_3']).','.($array['a_sold_3']-$array2['a_sold_3']).','.($array['a_asp12']-$array2['a_asp12']).','.($array['m_asp12']-$array2['m_asp12']).','.($array['a_asp9']-$array2['a_asp9']).','.($array['m_asp9']-$array2['m_asp9']).','.($array['a_asp6']-$array2['a_asp6']).','.($array['m_asp6']-$array2['m_asp6']).','.($array['a_asp3']-$array2['a_asp3']).','.($array['m_asp3']-$array2['m_asp3']).','.$array2['mes1'].','.($array['mes1CC']-$array2['mes1CC']).','.($array['mes1CS']-$array2['mes1CS']).','.$array2['mes2'].','.($array['mes2CC']-$array2['mes2CC']).','.($array['mes2CS']-$array2['mes2CS']).','.$array2['mes3'].','.($array['mes3CC']-$array2['mes3CC']).','.($array['mes3CS']-$array2['mes3CS']).','.$array2['mes4'].','.($array['mes4CC']-$array2['mes4CC']).','.($array['mes4CS']-$array2['mes4CS']).','.$array2['mes5'].','.($array['mes5CC']-$array2['mes5CC']).','.($array['mes5CS']-$array2['mes5CS']).','.$array2['mes6'].','.($array['mes6CC']-$array2['mes6CC']).','.($array['mes6CS']-$array2['mes6CS']).','.$array2['mes7'].','.($array['mes7CC']-$array2['mes7CC']).','.($array['mes7CS']-$array2['mes7CS']).','.$array2['mes8'].','.($array['mes8CC']-$array2['mes8CC']).','.($array['mes8CS']-$array2['mes8CS']).','.$array2['mes9'].','.($array['mes9CC']-$array2['mes9CC']).','.($array['mes9CS']-$array2['mes9CS']).','.$array2['mes10'].','.($array['mes10CC']-$array2['mes10CC']).','.($array['mes10CS']-$array2['mes10CS']).','.$array2['mes11'].','.($array['mes11CC']-$array2['mes11CC']).','.($array['mes11CS']-$array2['mes11CS']).','.$array2['mes12'].','.($array['mes12CC']-$array2['mes12CC']).','.($array['mes12CS']-$array2['mes12CS']).')';
			
			mysql_query ($que) or die ($que.mysql_error ());
			echo "{success:true}";
			return 0;
			
		break;
		
		
		
	}
	//echo $que;
	$result=mysql_query ($que) or die ($que.mysql_error ());
 
	$data = array();
	
	while ($row=mysql_fetch_object($result))
		$data [] = $row;
	
	$pos=strrpos(strtolower($que),"limit");
	$que2=substr($que,0,$pos);
	
	$result2 = mysql_query($que2) or die ($que2.mysql_error());
	$num_row=mysql_num_rows($result2);
	echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
?>
