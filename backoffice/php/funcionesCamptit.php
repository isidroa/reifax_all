<?php
    include($_SERVER['DOCUMENT_ROOT'].'/mant/classes/connection.class.php');
    include($_SERVER['DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
    $Errors =   new managerErrors(
        array(
            'downloadFiles' =>  FALSE
        )
    );

    include("conexion.php");
    conectar('xima');
    switch($_POST['accion']){
        case('consultar'):
            $start  = $_POST['start'] != ''?$_POST['start']:0;
            $limit  = $_POST['limit'] != ''?$_POST['limit']:100;
            if($_POST['status'] == 98)
                $var    = "Active','Created";
            if($_POST['status'] == 1)
                $var    = "Created";
            if($_POST['status'] == 2)
                $var    = "Assigned";
            if($_POST['status'] == 3)
                $var    = "Reassigned";
            if($_POST['status'] == 4)
                $var    = "Finished";
            if($_POST['status'] == 5)
                $var    = "Active";
            if($_POST['status'] == 6)
                $var    = "Canceled";
            if($_POST['status'] == 7)
                $var    = "Hold";
            if($_POST['status'] == 8)
                $var    = "Answer";
            $where  = '1=1';
            if($_POST['status'] ==99)
                $where.=" AND t.status<>'Finished'";
            else
                $where.=($_POST['status']!=0)?" AND t.status in ('{$var}')":'';

            $where.=(!empty($_POST['quien']))?" AND t.asignado='{$_POST['quien']}'":" ";

            if(!empty($_POST['dateInit']) or !empty($_POST['dateEnd'])){
                if(!empty($_POST['dateInit']) AND !empty($_POST['dateEnd'])){
                    $where.=" AND d.date_create>='{$_POST['dateInit']}' AND date_end<='{$_POST['dateEnd']}'";
                }elseif(!empty($_POST['dateInit']) AND empty($_POST['dateEnd'])){
                    $where.=" AND d.date_create>='{$_POST['dateInit']}'";
                }else{
                    $where.=" AND date_end<='{$_POST['dateEnd']}'";
                }
            }

            $sort=($_POST['sort'])?"order by {$_POST['sort']} {$_POST['dir']}":'order by t.id desc';


            $sql        = "SELECT t.*,concat(x.name ,' ',x.surname)as asignadoname,concat(x1.name ,' ',x1.surname)as solicitadoname,date_begin,date_end, l.fecha dateInit
                        FROM tareas t
                        inner join ximausrs x on t.asignado=x.userid
                        inner join ximausrs x1 on t.solicitado=x1.userid
                        inner join bodate_task d on t.id=d.id_task
                        inner join logtarea l on t.id=l.tarea_id
                        WHERE {$where}
                        group by t.id {$sort} ";
            //ECHO $sql;
            $resultado  = mysql_query ($sql)or die(mysql_error());
            $data       = array();
            while($s = mysql_fetch_assoc($resultado)) {
                $data[] = $s;
            }
            echo json_encode(array(
                    'success'   => true,
                    'total'     => count($data),
                    'sql'       => $sql,
                    'data'      => array_splice($data,$start,$limit)
                )
            );
        break;

        case('actualizar'):
            $prioridad = $_POST['prioridad'];
            if($_POST['status'] == 1)
                $status = "Created";
            if($_POST['status'] == 2)
                $status = "Assigned";
            if($_POST['status'] == 3)
                $status = "Reassigned";
            if($_POST['status'] == 4)
                $status = "Finished";
            if($_POST['status'] == 5)
                $status = "Active";
            if($_POST['status'] == 6)
                $status = "Canceled";
            if($_POST['status'] == 7)
                $status = "Hold";
            if($_POST['status'] == 8)
                $status = "Answer";

            $solicitado = $_POST['solicitado'];
            $sistema    = $_POST['sistema'];
            $descripcion= $_POST['descripcion'];
            $asignado   = $_POST['asignado'];
            $orden      = $_POST['orden'];
            $fechamo    = date("Y-m-d H:i:s");
            $id         = $_POST['id'];

            $sql        = sprintf("UPDATE  tareas SET prioridad= '%s',status = '%s', solicitado= '%d', sistema= '%s', descripcion= '%s', asignado = '%d',orden = '%d' WHERE id=%d",
            mysql_real_escape_string($prioridad),
            mysql_real_escape_string($status),
            $solicitado,
            mysql_real_escape_string($sistema),
            mysql_real_escape_string($descripcion),
            $asignado,
            $orden,
            mysql_real_escape_string($id));
            $rs         = mysql_query($sql);
            // INSERTAR EN EL LOG
            if ($rs){
                $sqllg = sprintf("INSERT INTO logtarea (tarea_id,pasignado,estado,fecha) values ('%d','%d','%s','%s')",
                $id,
                $asignado,
                mysql_real_escape_string($status),
                $fechamo);
                $rslg = mysql_query($sqllg);
                if($status == "Finished"){
                    $querylg = sprintf("update bodate_task set date_end='%s' where id_task='%d'",
                    $fechamo,$id);
                    $rslg   = mysql_query($querylg);
                }
                if($status == "Process"){
                    $querylg = sprintf("update bodate_task set date_begin='%s' where id_task='%d'",
                    $fechamo,$id);
                    $rslg   = mysql_query($querylg);
                }
            }
            echo json_encode(array(
                "success"   => mysql_errno() == 0,
                "msg"       => mysql_errno() == 0?"Registro inserted successfully":mysql_error()
            ));
        break;

        case('crear'):
            $prioridad  = empty($_POST['prioridad'])?'0':$_POST['prioridad'];
            $status     = $_POST['status'];

            $solicitado = $_POST['solicitado'];
            $sistema    = $_POST['sistema'];
            $descripcion= $_POST['descripcion'];
            $asignado   = $_POST['asignado'];
            $orden      = $_POST['orden'];
            $fechacre   = date("Y-m-d H:i:s");

            $query      = sprintf("INSERT INTO tareas (prioridad,status,solicitado,sistema,descripcion,asignado,orden) values ('%s','%s','%d','%s','%s','%d','%d')",
            mysql_real_escape_string($prioridad),
            mysql_real_escape_string($status),
            $solicitado,
            mysql_real_escape_string($sistema),
            mysql_real_escape_string($descripcion),
            $asignado,
            $orden);
            $rs         = mysql_query($query);
            $idcreado   = mysql_insert_id(); //id de la consulta crear tarea

            // INSERTAR EN EL LOG
            if($idcreado != 0){
                $querylg = sprintf("INSERT INTO logtarea (tarea_id,pasignado,estado,fecha) values ('%d','%d','%s','%s')",
                mysql_real_escape_string($idcreado),
                $asignado,
                mysql_real_escape_string($status),
                $fechacre);
                $rslg   = mysql_query($querylg);
            }
            if($idcreado != 0 && $status == 'Process'){
                $querylg = sprintf("INSERT INTO bodate_task (date_begin,id_task,status,date_create) values ('%s','%d','%s','%s')",
                $fechacre,
                $idcreado,
                'A',
                $fechacre);
                $rslg   = mysql_query($querylg);
            }else if($idcreado != 0){
                $querylg = sprintf("INSERT INTO bodate_task (id_task,status,date_create) values ('%d','%s','%s')",
                $idcreado,
                'A',
                $fechacre);
                $rslg   = mysql_query($querylg);
            }
            echo json_encode(array(
                "success"   => mysql_errno() == 0,
                "msg"       => mysql_errno() == 0?"Registro inserted successfully":mysql_error(),
                "data"      => array(
                    array(
                        "id"            => $idcreado,   // <--- importantisimo regresar el ID asignado al record, para que funcione correctamente el metodo update y delete
                        "prioridad"     => $prioridad,
                        "status"        => $status,
                        "solicitado"    => $solicitado,
                        "sistema"       => $sistema,
                        "descripcion"   => $descripcion,
                        "asignado"      => $asignado,
                        "orden"         => $orden
                    )
                )
            ));
        break;
        case ('update'):
            $records = json_decode(stripslashes($_POST['records']));
            foreach($records as $k){
                $prioridad  = $k->prioridad;
                $status     = $k->status;
                if($status==1)
                    $status="Created";
                if($status==2)
                    $status="Assigned";
                if($status==3)
                    $status="Reassigned";
                if($status==4)
                    $status="Finished";
                if($status==5)
                    $status="Active";
                if($status==7)
                    $status="Hold";
                if($status==8)
                    $status="Answer";
                $solicitado = (is_numeric($k->solicitadoname))?$k->solicitadoname:$k->solicitado;
                $sistema    = $k->sistema;
                $descripcion= $k->descripcion;
                $asignado   = (is_numeric($k->asignadoname))?$k->asignadoname:$k->asignado;
                $fechamo    = date("Y-m-d H:i:s");
                $id         = $k->id;
                $orden      = $k->orden;

                $sql = sprintf("UPDATE  tareas SET prioridad= '%s',status = '%s', solicitado= '%d', sistema= '%s', descripcion= '%s', asignado = '%d', orden = '%d' WHERE id=%d",
                        mysql_real_escape_string($prioridad),
                        mysql_real_escape_string($status),
                        $solicitado,
                        mysql_real_escape_string($sistema),
                        mysql_real_escape_string($descripcion),
                        $asignado,
                        $orden,
                        mysql_real_escape_string($id));

                $rs = mysql_query($sql);

                // INSERTAR EN EL LOG
                if ($rs)
                {
                    $sqllg = sprintf("INSERT INTO logtarea (tarea_id,pasignado,estado,fecha) values ('%d','%d','%s','%s')",
                    $id,
                    $asignado,
                    mysql_real_escape_string($status),
                    $fechamo);
                    $rslg = mysql_query($sqllg);
                    if($status == "Finished"){
                        $querylg = sprintf("update bodate_task set date_end='%s' where id_task='%d'",
                        $fechamo,$id);
                        $rslg   = mysql_query($querylg);
                    }
                    if($status == "Process"){
                        $querylg = sprintf("update bodate_task set date_begin='%s' where id_task='%d'",
                        $fechamo,$id);
                        $rslg   = mysql_query($querylg);
                    }
                }
            }
        break;
        case ('eliminar'):
            $claves=$_POST['ids'];
            $sql='DELETE FROM tareas where tareas.id IN ('.$_POST['ids'].')';
            $s= mysql_query($sql);
            echo json_encode(array('success'=>true ,'claves' => $claves));
        break;
    }
?>
