<?php
include ("conexion.php");
conectar('xima');

$status = array('Active' => 1, 'Inactive' => 2);

switch ($_POST['accion']) {
	case ('recordWebinar') :
		$url = $_POST['url'];
		if ($url != '') {
			if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
				$video_id = $match[1];
			}

			$feedURL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,player,recordingDetails,statistics,status&key=AIzaSyBTck83MItPMfxAZ51zmMt7CajVTxUqvlU&id=' . $video_id;
			$entry = json_decode(file_get_contents($feedURL));
			if (!is_object($entry)) {
				echo json_encode(array('success' => false, 'error' => "el video es privado"));
				die();
			} else {
				$videoEval = $entry -> items[0];
				$video = new stdClass();

				$video -> views = $videoEval -> statistics -> viewCount;

				$video -> views = $video -> views != '' ? $video -> views[0] : 0;
				$video -> title = mysql_real_escape_string($videoEval -> snippet -> localized -> title);
				$video -> description = mysql_real_escape_string(trim($videoEval -> snippet -> localized -> description));
				$video -> thumbnailURL = mysql_real_escape_string(trim($videoEval -> snippet -> thumbnails -> medium -> url));
				$video -> length = mysql_real_escape_string(covtime(trim($videoEval -> contentDetails -> duration)));

				$sql = "INSERT INTO `webinar`
						(`name`,`idYoutube`,`urlVideo`,`urlImg`,`status`,`fecha`,`length`,`views`)
					VALUES
						('{$video->title }','{$video_id}','http://www.youtube.com/embed/{$video_id}','{$video->thumbnailURL}',1,NOW(),{$video->length},{$video->views});
					";

				mysql_query($sql) or die($sql . mysql_error());

				echo json_encode(array('success' => true));
			}
		}

		break;
	case ('list') :
		$sql = "SELECT * FROM `webinar` order by id desc;";

		$sql;
		$result = mysql_query($sql) or die(mysql_error());
		$video = array();
		$i = 1;
		while ($tem = mysql_fetch_assoc($result)) {
			if ($i > 24 || $tem['status'] == 2) {
				$tem['show'] = false;
			} else {
				$tem['show'] = true;
				$i++;
			}
			array_push($video, $tem);
		}
		echo json_encode(array('success' => true, 'videos' => $video));
		break;
	case ('update') :
		$records = json_decode(stripslashes($_POST['records']));
		foreach ($records as $k) {

			$status = ((is_numeric($k -> status)) ? $k -> status : $status[$k -> status]);

			$sql = "UPDATE webinar SET name='{$k->name}', madeBy='{$k->madeBy}', status='{$k->status}', description='{$k->description}' where id={$k->id}; ";

			mysql_query($sql);
		}
		if (mysql_affected_rows() > 0) {
			echo json_encode(array('success' => true, 'sql' => $sql));
		} else {echo json_encode(array('success' => false, 'sql' => $sql, 'error' => mysql_error()));
		}

		break;
	case ('delete') :
		$sql = 'DELETE FROM `webinar` where id in (' . $_POST['ids'] . ')';
		mysql_query($sql);
		echo json_encode(array('success' => true, 'sql' => $sql));
		break;
}

function parseVideoEntry($entry) {
	$obj = new stdClass;

	$obj -> author = $entry -> author -> name;
	$obj -> authorURL = $entry -> author -> uri;

	$media = $entry -> children('http://search.yahoo.com/mrss/');
	$obj -> title = $media -> group -> title;
	$obj -> description = $media -> group -> description;

	$attrs = $media -> group -> player -> attributes();
	$obj -> watchURL = $attrs['url'];

	$attrs = $media -> group -> thumbnail[0] -> attributes();
	$obj -> thumbnailURL = $attrs['url'];

	// get <yt:duration> node for video length
	$yt = $media -> children('http://gdata.youtube.com/schemas/2007');
	$attrs = $yt -> duration -> attributes();
	$obj -> length = $attrs['seconds'];

	// get <yt:stats> node for viewer statistics
	$yt = $entry -> children('http://gdata.youtube.com/schemas/2007');
	$attrs = $yt -> statistics -> attributes();
	$obj -> viewCount = $attrs['viewCount'];

	$gd = $entry -> children('http://schemas.google.com/g/2005');
	if ($gd -> rating) {
		$attrs = $gd -> rating -> attributes();
		$obj -> rating = $attrs['average'];
	} else {
		$obj -> rating = 0;
	}

	// get <gd:comments> node for video comments
	$gd = $entry -> children('http://schemas.google.com/g/2005');
	if ($gd -> comments -> feedLink) {
		$attrs = $gd -> comments -> feedLink -> attributes();
		$obj -> commentsURL = $attrs['href'];
		$obj -> commentsCount = $attrs['countHint'];
	}

	// get feed URL for video responses
	$entry -> registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
	$nodeset = $entry -> xpath("feed:link[@rel='http://gdata.youtube.com/
  schemas/2007#video.responses']");
	if (count($nodeset) > 0) {
		$obj -> responsesURL = $nodeset[0]['href'];
	}

	// get feed URL for related videos
	$entry -> registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
	$nodeset = $entry -> xpath("feed:link[@rel='http://gdata.youtube.com/
  schemas/2007#video.related']");
	if (count($nodeset) > 0) {
		$obj -> relatedURL = $nodeset[0]['href'];
	}

	// return object to caller
	return $obj;
}

function getValueRaay($var) {
	if (is_object($var) || is_array($var)) {
		$var = (array)$var;
		foreach ($var as $key => $val) {
			$valueT = $val;
		}
		$value = getValueRaay($valueT);
	} else {
		$value = $var;
	}
	return $value;
};

function covtime($youtube_time) {
	preg_match_all('/(\d+)/', $youtube_time, $parts);

	// Put in zeros if we have less than 3 numbers.
	if (count($parts[0]) == 1) {
		array_unshift($parts[0], "0", "0");
	} elseif (count($parts[0]) == 2) {
		array_unshift($parts[0], "0");
	}

	$sec_init = $parts[0][2];
	$seconds = $sec_init % 60;
	$seconds_overflow = floor($sec_init / 60);

	$min_init = $parts[0][1] + $seconds_overflow;
	$minutes = ($min_init) % 60;
	$minutes_overflow = floor(($min_init) / 60);

	$hours = $parts[0][0] + $minutes_overflow;

	if ($hours != 0)
		return ((($hours * 60) + $minutes * 60) + $seconds);
	else
		return (($minutes * 60) + $seconds);
}
?>