<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

$SERVER = new stdClass();
$SERVER -> DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

require_once $SERVER -> DOCUMENT_ROOT . '/MANT/classes/connection.class.php';
require_once $SERVER -> DOCUMENT_ROOT . '/MANT/classes/globalReifax.class.php';
require_once $SERVER -> DOCUMENT_ROOT . '/MANT/classes/managerDownload.class.php';
require_once $SERVER -> DOCUMENT_ROOT . '/MANT/classes/managerErrors.class.php';
require_once $SERVER -> DOCUMENT_ROOT . '/MANT/classes/parseo.class.php';
require_once $SERVER -> DOCUMENT_ROOT . '/reifaxAll/properties_tabs/propertyImport/class.baseImport.php';
include ("conexionAWSEXT4.php");

$Connect = new ReiFax( array('downloadFiles' => false));
$conMaster = $Connect -> mysqlByServer(array('master' => true, 'typeIp' => 'WAN'));
$conExtractorMaster = $Connect -> mysqlByServer(array('serverNumber' => 'Extractor1', 'typeIp' => 'WAN', 'dataBase' => 'reifaxcounty'));
$conMathExtractor = $Connect -> mysqlByServer(array('serverNumber' => 'Extractor4', 'typeIp' => 'WAN', 'dataBase' => 'reifaxcounty'));

$POST = new stdClass();
$POST -> codePhp = $conExtractorMaster -> real_escape_string(filter_input(INPUT_POST, 'codePhp'));

switch ($_POST['option']) {

	case 'getAllActions' :
		$sql = sprintf("SELECT 
    a.`orden`, a.`titulo`, a.`actionid`, a.`tipocampo`, trim(a.`source`) source ,colortitle,p.php ,p.procid
FROM
    mantenimientousa.administracion ad
        LEFT JOIN
    mantenimientousa.procesos p ON ad.idprocMapeo = p.procid
        LEFT JOIN
    mantenimientousa.action a ON ad.actionid = a.actionid
    where a.actionid is not null
GROUP BY a.actionid
ORDER BY a.orden;");
		$res = $conMathExtractor -> query($sql) or die($conMathExtractor -> error);
		$respuesta = array();
		while ($data = $res -> fetch_assoc()) {
			array_push($respuesta, $data);
		}
		echo json_encode(array('data' => $respuesta, 'total' => count($respuesta)));
		break;

	case 'getCode' :
		break;

	case 'addCheck' :
		if (empty($_POST['idCheck'])) {
			$sql = sprintf("INSERT INTO `checks_control` (`orden`, `titulo`,`colortitle`, `codePhp`,`tipocampo`) VALUES (%d,'%s','%s','%s','int');
			", $_POST['orden'], $_POST['action'], $_POST['colortitle'], $POST -> codePhp);
			$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		} else {
			$sql = sprintf("UPDATE checks_control SET
				`orden` = %d,
			 	`titulo` ='%s',
			 	`colortitle` ='%s', updateCheckOrigin
			 	`codePhp` ='%s' WHERE id = %d", $_POST['orden'], $_POST['action'], $_POST['colortitle'], $POST -> codePhp, $_POST['idCheck']);
			$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
		}
		$success = $conExtractorMaster -> affected_rows > 0 ? true : false;
		$respuesta = $conExtractorMaster -> affected_rows > 0 ? 'Cambios realizados con exito' : 'Hubo un problema';
		echo json_encode(array('success' => $success, 'msg' => $respuesta));
		break;
	case 'getFilesToS3' :
		$sql = sprintf("SELECT d.*, s.name state, sy.name system FROM s3_directory d
inner join state s on s.idstate=d.idstate
inner join diary_control dc on dc.id_file=d.ids3_directory
inner join system_origins sy on sy.id=d.id_system group by d.ids3_directory order by ids3_directory desc");
		//, $_POST['type'], $_POST['system'], $_POST['state']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			$sql = sprintf("SELECT * FROM reifaxcounty.diary_control where id_file=%d", $dataState['ids3_directory']);
			$res2 = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
			while ($diary = $res2 -> fetch_object()) {
				$dataState[$diary -> action] = $diary -> status;
				$dataState['server'] = $diary -> server;
			}
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta, 'total' => count($respuesta)));
		break;
	case 'getFiles' :
		$sql = sprintf("SELECT * FROM s3_directory WHERE type=%d AND  id_system=%d AND idstate=%d;", $_POST['type'], $_POST['system'], $_POST['state']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta, 'total' => count($respuesta)));
		break;
	case 'getAllChecksBySystem' :
		/*	$sql = sprintf("SELECT id FROM reifaxcounty.system_origins where active=1 order by name");
		 $res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		 $system = array();
		 while ($dataSystem = $res -> fetch_assoc()) {
		 $system['system_' . $dataSystem['id']] = false;
		 }
		 $sql = sprintf("SELECT *, group_concat(o.id_system) systems, c.id FROM reifaxcounty.checks_control c left join reifaxcounty.checks_origin o on c.id=o.id_check group by c.id order by c.orden;");
		 $res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		 $respuesta = array();
		 while ($dataCheck = $res -> fetch_assoc()) {
		 $tem = explode(',', $dataCheck['systems']);
		 foreach ($tem as $key => $value) {
		 $dataCheck['system_' . $value] = true;
		 }
		 $temFinal = array_merge($system, $dataCheck);
		 array_push($respuesta, $dataCheck);
		 }
		 echo json_encode(array('data' => $respuesta, 'total' => count($respuesta)));*/
		$conMathExtractor = conexionAWSEXT4("mantenimientousa");

		/*	$sql = sprintf("SELECT id FROM reifaxcounty.system_origins where active=1 order by name");
		 $res = $conMathExtractor -> query($sql) or die($conMathExtractor -> error);
		 $system = array();
		 while ($dataSystem = $res -> fetch_assoc()) {
		 $system['system_' . $dataSystem['id']] = false;
		 }*/

		$sql = sprintf("SELECT * from mantenimientousa.administracion ad
LEFT JOIN mantenimientousa. procesos p ON ad.idprocMapeo = p.procid;");
		$res = $conMathExtractor -> query($sql) or die($conMathExtractor -> error);
		$respuesta = array();
		while ($dataCheck = $res -> fetch_assoc()) {
			$respuesta[] = $res;
		}
		echo json_encode(array('data' => $respuesta, 'total' => count($respuesta)));
		break;
	case 'getChecksFields' :
		$sql = sprintf("SELECT ad.*
FROM
    mantenimientousa.administracion ad
        LEFT JOIN
    mantenimientousa.procesos p ON ad.idprocMapeo = p.procid
        LEFT JOIN
    mantenimientousa.action a ON ad.actionid = a.actionid
    where a.actionid is not null
GROUP BY ad.fecha
ORDER BY a.orden;");
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'updateCheck' :
		$sql = sprintf("UPDATE checks_control c
		join checks_origin o on c.id=o.id_check SET %s='%s' WHERE c.`id` = %d and o.id_system=%d", $_POST['gfield'], $_POST['gvaluenew'], $_POST['gid'], $_POST['origin']);
		$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
		echo json_encode(array('success' => true));
		break;
	case 'updateCheckOrigin' :
		$control = '';
		if (preg_match('/system_\d/i', $_POST['gfield'])) {
			$field = explode('_', $_POST['gfield']);
			$sql = sprintf("SELECT c.*, c.id actionid, o.valprom2 FROM checks_origin o
			join checks_control c on c.id=o.id_check
			where o.id_system=%d and c.id=%d;", $field[1], $_POST['gid']);
			$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
			if ($res -> num_rows > 0) {
				$exists = true;
			} else {
				$exists = false;
			}
			if ($_POST['gvaluenew'] == 'true') {
				if ($exists) {
					$sql = sprintf("UPDATE checks_control c
					join checks_origin o on c.id=o.id_check SET active=1 WHERE c.`id` = %d and o.id_system=%d", $_POST['gid'], $field[1]);
					$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
					$control = __LINE__;
				} else {
					$sql = sprintf("INSERT INTO checks_origin (id_check,active,id_system,valprom2) VALUES(%d,1,%d,1)", $_POST['gid'], $field[1]);
					$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
					$control = __LINE__;
				}
			} else {
				$sql = sprintf("UPDATE checks_control c
				join checks_origin o on c.id=o.id_check SET active=0 WHERE c.`id` = %d and o.id_system=%d", $_POST['gid'], $field[1]);
				$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
				$control = __LINE__;
			}
		} else {
			$sql = sprintf("UPDATE checks_control c
			join checks_origin o on c.id=o.id_check SET %s='%s' WHERE c.`id` = %d", $_POST['gfield'], $_POST['gvaluenew'], $_POST['gid']);
			$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
			$control = __LINE__;
		}
		echo json_encode(array('success' => true, 'control' => $control, 'system' => $field[1]));
		break;
	case 'getChecksValues' :
		$sql = sprintf("SELECT c.*, c.id actionid, o.valprom2 FROM checks_origin o
			join checks_control c on c.id=o.id_check
			where o.id_system=%s group by c.id order by orden;", $_POST['system']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		$totalFields = $res -> num_rows;
		while ($data = $res -> fetch_assoc()) {
			$respuesta[$data['id']] = $data;
		}

		$sql = sprintf("SELECT r.*, c.id id FROM reifaxcounty.checks_records r
			join checks_origin o on r.id_check_system=o.id
			join checks_control c on c.id=o.id_check
			where ejecution in (%s) order by c.id, ejecution asc;", $_POST['ejecutions']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$ejecutionsActive = '';
		$ejecutionsIdActive = NULL;
		$ejecutionsRecordActive = '';
		$ejecutionsReValueActive = 0;
		$lastValue = 0;
		$ejecutionsTotal = explode(',', $_POST['ejecutions']);
		$countEjecutionsTotal = count($ejecutionsTotal);

		while ($data = $res -> fetch_assoc()) {
			if ($data['id'] !== $ejecutionsIdActive) {
				if (NULL !== $ejecutionsIdActive) {
					if ($ejecutionsIdActive == 771) {

					}
					if ($i < $countEjecutionsTotal) {
						$falto = abs($i - $countEjecutionsTotal);
						for ($k = 1; $k <= $i; $k++) {
							$respuesta[$ejecutionsIdActive]['promedio' . ((($i + 1) - $k) + $falto)] = $ejecutionsReValueActive = $respuesta[$ejecutionsIdActive]['promedio' . (($i + 1) - $k)];
						}

						for ($k = 1; $k <= $falto; $k++) {
							$respuesta[$ejecutionsIdActive]['promedio' . $k] = 0;
						}
					}
					for ($k = 1; $k < $countEjecutionsTotal; $k++) {

						$respuesta[$ejecutionsIdActive]['promedio' . $k] = abs($respuesta[$ejecutionsIdActive]['promedio' . $k] - $lastValue);
						$ejecutionsReValueActive += $respuesta[$ejecutionsIdActive]['promedio' . $k];
					}
					$auxT = abs($ejecutionsReValueActive / $countEjecutionsTotal);
					$respuesta[$ejecutionsIdActive]['valpromCrudoAne'] = $ejecutionsReValueActive;
					$respuesta[$ejecutionsIdActive]['valprom'] = ($auxT) * $respuesta[$ejecutionsIdActive]['valprom2'];
					$respuesta[$ejecutionsIdActive]['valpromcru'] = $respuesta[$ejecutionsIdActive]['valprom'];
					$respuesta[$ejecutionsIdActive]['porc'] = $respuesta[$ejecutionsIdActive]['valprom2'];
					//var_dump($respuesta[$ejecutionsIdActive]);
				}
				$ejecutionsReValueActive = 0;
				$ejecutionsIdActive = $data['id'];
				$lastValue = 0;
				$i = 0;
			}
			$i++;
			$respuesta[$data['id']]['promedio' . $i] = $data['Records'];
			$lastValue = $data['Records'];
			$respuesta[$data['id']][$data['ejecution']] = $data['Records'];
		}

		if ($i < $countEjecutionsTotal) {
			$falto = abs($i - $countEjecutionsTotal);
			for ($k = 1; $k <= $i; $k++) {
				//var_dump('reasiganan promedio'.$k.' al '.($k+$falto));
				$ejecutionsReValueActive = $respuesta[$ejecutionsIdActive]['promedio' . ($k + $falto)] = $ejecutionsReValueActive = $respuesta[$ejecutionsIdActive]['promedio' . $k];
			}

			for ($k = 1; $k <= $falto; $k++) {
				//var_dump('llenando vacio de promedio'.($k));
				$ejecutionsReValueActive = $respuesta[$ejecutionsIdActive]['promedio' . $k] = 0;
			}
		}
		for ($k = 1; $k <= $countEjecutionsTotal; $k++) {
			$ejecutionsReValueActive = $respuesta[$ejecutionsIdActive]['promedio' . $k] = abs($respuesta[$ejecutionsIdActive]['promedio' . $k] - $lastValue);
		}
		$respuesta[$ejecutionsIdActive]['valprom'] = ($ejecutionsReValueActive / $countEjecutionsTotal) * $respuesta[$ejecutionsIdActive]['valprom2'];
		$respuesta[$ejecutionsIdActive]['valpromcru'] = $respuesta[$ejecutionsIdActive]['valprom'];
		$respuesta[$ejecutionsIdActive]['porc'] = $respuesta[$ejecutionsIdActive]['valprom2'];

		$respuestaJson = array();
		foreach ($respuesta as $key => $value) {
			array_push($respuestaJson, $value);
		}
		echo json_encode(array('data' => $respuestaJson, 'total' => $totalFields));
		break;
	case 'getChecks' :
		/*$sql = sprintf("SELECT ad.*
FROM
    mantenimientousa.administracion ad
        LEFT JOIN
    mantenimientousa.procesos p ON ad.idprocMapeo = p.procid
        LEFT JOIN
    mantenimientousa.action a ON ad.actionid = a.actionid
    where a.actionid is not null
GROUP BY ad.fecha
ORDER BY a.orden;", $_POST['state']);*/

		$sql = sprintf("SELECT ad.*
FROM
    mantenimientousa.administracion ad
        LEFT JOIN
    mantenimientousa.procesos p ON ad.idprocMapeo = p.procid
        LEFT JOIN
    mantenimientousa.action a ON ad.actionid = a.actionid
    where a.actionid is not null
GROUP BY ad.fecha
ORDER BY a.orden;");
		$res = $conMathExtractor -> query($sql) or die($conMathExtractor -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'getState' :
		$sql = "SELECT * FROM mantenimientousa.usastate";
		$res = $conMathExtractor -> query($sql) or die($conMathExtractor -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'getCheckPoint' :
		$table = in_array($_POST['id'], array(2, 1)) ? 'pages_spider' : 'county_origin';
		$field = in_array($_POST['id'], array(2, 1)) ? 'id_system' : 'idsystem';
		$sql = sprintf("SELECT date_update FROM reifaxcounty.%s where %s=%d group by date_update;", $table, $field, $_POST['id']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'getCounty' :
		$sql = sprintf("SELECT * FROM reifaxcounty.county where idstate=%d", $_POST['id']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array( array('idcounty' => 'All', 'name' => 'All', 'idstate' => 'All', 'code' => 'All'));
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'RecordJob' :
		/*session_start();
		 $sql = sprintf("INSERT INTO `reifaxcounty`.`statistics_down`
		 ( `idstate`, `int_system`, `dateInit`, `dateEnd`, `total`, `ejecutorS`, `ejecutorE`)
		 VALUES
		 ( %d, %d, '%s %s', '%s %s', %d, %d, %d);
		 ", $_POST['state'], $_POST['system'], $_POST['init'], $_POST['inith'], $_POST['end'], $_POST['endh'], $_POST['count'], $_SESSION['bkouserid'], $_SESSION['bkouserid']);
		 $res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		 if ($conExtractorMaster -> affected_rows) {
		 echo json_encode(array('success' => true, 'sql' => $sql));
		 } else {
		 echo json_encode(array('success' => false, 'sql' => $sql));
		 }*/
		break;
	case 'getServerAdmin' :
		$sql = sprintf("SELECT * FROM reifaxcounty.system_origins where active=1 order by name");
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataSystem = $res -> fetch_assoc()) {
			$sql = "SELECT * FROM reifaxcounty.server_byOrigin where id_system={$dataSystem['id']};";
			$res2 = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
			while ($dataServer = $res2 -> fetch_assoc()) {
				$dataSystem[$dataServer['id_server']] = true;
			}
			array_push($respuesta, $dataSystem);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'updateServerAdmin' :
		if ($_POST['gvaluenew'] == 'false') {
			$sql = sprintf("DELETE FROM reifaxcounty.server_byOrigin where id_system=%d and id_server=%d", $_POST['origen'], $_POST['server']);
			$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
			if ($conExtractorMaster -> affected_rows > 0) {
				echo true;
			} else {
				echo false;
			}
		} else {
			$sql = sprintf("insert into  reifaxcounty.server_byOrigin (id_system,id_server) values (%d,%d)", $_POST['origen'], $_POST['server']);
			$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
			if ($conExtractorMaster -> affected_rows > 0) {
				echo true;
			} else {
				echo false;
			}
		}
		break;
	case 'getSystem' :
		$sql = sprintf("SELECT * FROM reifaxcounty.system_origins where active=1 order by name");
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$respuesta = array();
		while ($dataState = $res -> fetch_assoc()) {
			array_push($respuesta, $dataState);
		}
		echo json_encode(array('data' => $respuesta));
		break;
	case 'getUrlScrapin' :
		$respuesta = array();
		$sql = sprintf("SELECT * FROM reifaxcounty.system_origins where id=%d", $_POST['system']);
		$res = $conExtractorMaster -> query($sql) or die($conExtractorMaster -> error);
		$dataSys = $res -> fetch_assoc();
		if ($_POST['county'] == 'All' || empty($_POST['county'])) {
			$optionCounty = '';
		} else {
			$optionCounty = sprintf("AND c.idcounty=%d", $_POST['county']);
		}
		if (in_array($_POST['system'], array(2, 1))) {
			if (!empty($_POST['date'])) {
				$sqlC = sprintf("SELECT
						    ci.idcounty, ci.idstate, sp.result n_records,sp.pages n_page, sp.id_system,ci.zip codeCity, '' code,sp.idcity, '' stateCode, '' State, '' name
						FROM pages_spider sp
		                join city ci on ci.idcity=sp.idcity where ci.idstate=%d and sp.id_system=%d  AND date_update='%s' ", $_POST['state'], $_POST['system'], $_POST['date']);
			} else {
				$sqlC = sprintf("select *  from (SELECT
					    ci.idcounty, ci.idstate, sp.result n_records,sp.pages n_page, sp.id_system,ci.zip codeCity, '' code,sp.idcity, '' stateCode, '' State, '' name
					FROM pages_spider sp
	                join city ci on ci.idcity=sp.idcity where ci.idstate=%d and sp.id_system=%d order by date_update desc) t1 group by idcity;", $_POST['state'], $_POST['system']);
			}
		} else {
			if (!empty($_POST['date'])) {
				$sqlC = sprintf("SELECT
					    c.idcounty, s.idstate, o.n_records,o.n_page, o.idsystem,o.date_update, s.name State, s.code stateCode, c.name, c.code, '' codeCity
					FROM
					    reifaxcounty.county c
					        JOIN
					    `reifaxcounty`.`county_origin` o ON o.idcounty = c.idcounty
					        JOIN
					    reifaxcounty.state s ON s.idstate = c.idstate where s.idstate=%d and o.idsystem=%d AND date_update='%s' %s  ", $_POST['state'], $_POST['system'], $_POST['date'], $optionCounty);
			} else {
				$sqlC = sprintf("select *  from (SELECT
					    c.idcounty, s.idstate, o.n_records,o.n_page, o.idsystem,o.date_update, s.name State, s.code stateCode, c.name, c.code, '' codeCity
					FROM
					    reifaxcounty.county c
					        JOIN
					    `reifaxcounty`.`county_origin` o ON o.idcounty = c.idcounty
					        JOIN
					    reifaxcounty.state s ON s.idstate = c.idstate where s.idstate=%d and o.idsystem=%d %s order by date_update desc) t1 group by idcounty", $_POST['state'], $_POST['system'], $optionCounty);
			}
		}
		$resC = $conExtractorMaster -> query($sqlC) or die($sqlC . $conExtractorMaster -> error);
		while ($dataCounty1 = $resC -> fetch_assoc()) {
			$cant = 0;
			//$totalEjecutar=round($dataCounty1['n_page']*0.15)+1;//Comented By Jesus 21-04-2015

			$totalEjecutar = round($dataCounty1['n_page']);
			if ($totalEjecutar == 0 && $dataCounty1['n_records'] > 0) {
				$totalEjecutar = 1;
			}
			//$totalEjecutar=$dataCounty1['n_page'];
			for ($i = 0; $i < ($totalEjecutar); $i++) {
				$cant++;
				$codeState = $dataCounty1['stateCode'];
				$StateName = $dataCounty1['State'];
				$CountyGuion = strtolower(str_replace(' ', '-', $dataCounty1['name']));
				$urlFinal = $dataSys['url'];
				$urlFinal = str_replace("['codeState']", $codeState, $urlFinal);
				$urlFinal = str_replace("['codeCity']", $dataCounty1['codeCity'], $urlFinal);
				$urlFinal = str_replace("['CountyGuion']", $CountyGuion, $urlFinal);
				$urlFinal = str_replace("['codeCounty']", $dataCounty1['code'], $urlFinal);
				$urlFinal = str_replace("['nPage']", ($i + 1), $urlFinal);
				$urlFinal = explode(']*[', $urlFinal);
				foreach ($urlFinal as $key => $value) {
					array_push($respuesta, $value);
				}
			}
		}
		echo json_encode(array('sql' => $sqlC, 'data' => $respuesta));
		break;
	case 'RecordPage' :
		$chekponi = date('Y-m-d H:i:s');
		$data = array();
		$errors = '';
		$noId = 0;
		$reg = '/\d+ GO/i';
		$reg2 = '/\d+_rid/i';
		$reg3 = '/\d+ Result/i';
		$idOrigin = $_POST['idOrigin'];
		$totalRegister = 0;
		$countyReplace = array('miami dade' => 'Miami-Dade');
		$filesEval = array();
		if ($_POST['OrigenData'] == 1) {
			foreach ($_FILES['file']['tmp_name'] as $value) {
				array_push($filesEval, $value);
			}
		} else {
			chdir("/var/xima/page");
			foreach (glob("*.csv") as $value) {
				echo "<br>archivo a procesar:" . $value;
				array_push($filesEval, $value);
			}
		}
		/**
		 *  evaluar sistemas realtytrac o trulia
		 * /
		 $testTrulia = '/trulia/i';
		 $testRealtytrac = '/realtyt/i';

		 /**
		 *  evaluar tipos de data para trulia
		 * /
		 $testSold = '/\/sold\//i';
		 $testSold3 = '/\/for_rent\//i';
		 $testSold1 = '/\d_nl\/date;d_sort/i';
		 $testSold2 = '/\d_nl\/date;d_sort/i';

		 /**
		 *  evaluar tipos de data para realtytrac
		 * /
		 $testForeclosures = '/county\-foreclosures/i';
		 $testAuctions = '/\/auctions\//i';
		 $testPreFR = '/\/pre\-foreclosures\//i';
		 $testBank = '/\/bank\-owned\-properties\//i';
		 */
		$testRealtytracFore = '/county\-foreclosures/i';
		//chdir('c:/temprt');
		foreach ($filesEval as $value) {
			$fila = 1;
			if (($gestor = fopen($value, "r")) !== FALSE) {
				while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
					if ($fila > 1) {
						// var_dump($datos);

						$_urlActua = $datos[5];
						/*
						 if (preg_match($testTrulia, $_urlActua)) {
						 if (preg_match($testSold, $_urlActua)) {
						 $idOrigin = 1;
						 } else if (preg_match($testSold1, $_urlActua)) {
						 $idOrigin = 11;
						 } else if (preg_match($testSold2, $_urlActua)) {
						 $idOrigin = 10;
						 } else if (preg_match($testSold3, $_urlActua)) {
						 $idOrigin = 12;
						 } else {
						 $idOrigin = 2;
						 }
						 */
						if (in_array($idOrigin, array(1, 11, 10, 12, 2))) {
							$paginas = explode(' ', $datos[3]);
							$paginas = $paginas[count($paginas) - 1];

							$paginas = (is_numeric(trim($paginas))) ? $paginas : 0;
							$tem2 = explode('/', $datos[5]);
							$datos[4] = str_replace(',', '', $datos[4]);
							$result = 0;
							if (preg_match('/([\d,]+)\s*\w*$/i', $datos[4], $values)) {
								$result = str_replace(',', '', array_pop($values));
								if ($paginas == 0) {
									$paginas = ceil($result / 15);
								}
							}

							$verify = explode(',', $datos[6]);

							$county = preg_replace('/\-/i', '', preg_replace('/.*in /i', '', preg_replace('/County.*/i', '', $verify[0])));

							$code2 = explode('_', $tem2[4]);
							$code = $code2[0];
						}
						/*
						 if (preg_match($testRealtytrac, $_urlActua) && empty($code)) {
						 $code = '';
						 if (preg_match($testSold, $_urlActua)) {
						 $idOrigin = 3;
						 } else if (preg_match($testForeclosures, $_urlActua)) {
						 $idOrigin = 5;
						 } else if (preg_match($testAuctions, $_urlActua)) {
						 $idOrigin = 8;
						 } else if (preg_match($testPreFR, $_urlActua)) {
						 $idOrigin = 6;
						 } else if (preg_match($testBank, $_urlActua)) {
						 $idOrigin = 7;
						 } else {
						 $idOrigin = 4;
						 }
						 *
						 */
						if (in_array($idOrigin, array(3, 5, 8, 7, 4))) {
							$paginas = 0;
							if (preg_match('/(\d+) GO/i', $datos[4], $values)) {
								$paginas = array_pop($values);
							}
							if ($paginas == 0 && $datos[3] == 'Save') {
								$paginas = 1;
							}
							$result = $paginas * 10;
							if (preg_match('/\/(\w{2})\//i', $_urlActua, $values)) {
								$state = str_replace('/', '', array_pop($values));
							}
							preg_match("/[\w_\-\']*county[\w_\-\']*/i", $_urlActua, $temp);
							$county = str_replace('county', '', str_replace('-', ' ', $temp[0]));
						}
						if (!empty($code)) {
							if (in_array($idOrigin, array(2, 1))) {
								$sql = sprintf("SELECT * FROM city where zip='%s';", $code);
								$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
								if ($res -> num_rows) {
									$countyEval = $res -> fetch_assoc();
								} else {
									$errors .= "la ciudad zip code '{$code}' no coincidio<br>";
									unset($countyEval);
								}
							} else {
								$sql = sprintf("SELECT c.* FROM reifaxcounty.county c join reifaxcounty.state s on c.idstate=s.idstate where c.code='%s';", $code);
								$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
								if ($res -> num_rows) {
									$countyEval = $res -> fetch_assoc();
								} else {
									$errors .= "el condados code '{$code}' no coincidio<br>";
									unset($countyEval);
								}
							}
						} else {
							if (preg_match('/Recently Sold/', $county)) {
								$county = preg_replace('/\s*Recently Sold.*/', '', $county);
							}
							if (isset($countyReplace[$county])) {
								$county = $countyReplace[$county];
							}
							$sql = sprintf("SELECT c.* FROM reifaxcounty.county c join reifaxcounty.state s on c.idstate=s.idstate where c.name='%s' AND s.code='%s';", $conExtractorMaster -> real_escape_string(trim($county)), $state);
							$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
							if ($res -> num_rows > 0) {
								$countyEval = $res -> fetch_assoc();
							} else {
								echo "el condados '{$county}' [ {$datos[5]} ] | REsult: {$result}, paginas: {$paginas} no coincidio<br>";
								unset($countyEval);
							}
						}

						if (isset($countyEval)) {
							if (in_array($idOrigin, array(2, 1))) {
								array_push($data, "({$result},{$paginas},{$countyEval['idcity']},{$idOrigin},'{$chekponi}')");
							} else {
								array_push($data, "({$countyEval['idcounty']},{$idOrigin},{$result},{$paginas},'{$chekponi}')");
							}
						}
					}
					$fila++;
				}
				fclose($gestor);
			}
			$valuesAll = implode(',', $data);
			if (in_array($idOrigin, array(2, 1))) {
				$sql = "INSERT INTO `pages_spider` (`result`, `pages`,  `idcity`, `id_system`, `date_update`) VALUES " . $valuesAll;
			} else {
				$sql = "INSERT INTO `reifaxcounty`.`county_origin` (`idcounty`, `idsystem`, `n_records`, `n_page`, `date_update`) values " . $valuesAll;
			}
			$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
			$totalRegister += $conExtractorMaster -> affected_rows;
		}
		if ($totalRegister > 0) {
			echo "<h3>se registraron {$totalRegister} registros nuevos</h3>";
		}
		break;
	case 'RecorJson' :
		$chekponi = date('Y-m-d H:i:s');
		$errors = '';
		foreach ($_POST['data'] as $k => $v) {
			$tem1 = explode('|', $v);
			$dataEval = array();
			foreach ($tem1 as $key2 => $value2) {
				$tem1[$key2] = explode(':', $value2);
				$dataEval[trim($tem1[$key2][0])] = trim($tem1[$key2][1]);
			}
			if (isset($dataEval['code'])) {
				$sql = sprintf("SELECT c.* FROM reifaxcounty.county c join reifaxcounty.state s on c.idstate=s.idstate where c.code='%s';", $dataEval['code']);
				$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
				if ($res -> num_rows) {
					$countyEval = $res -> fetch_assoc();
				} else {
					$errors .= "el condados '{$dataEval['county']}, {$dataEval['state']}' no coincidio<br>";
					unset($countyEval);
				}
			} else {
				$arrayRpl = array('miami dade' => 'Miami-Dade');
				//$dataEval['county']=$arrayRpl[$dataEval['county']];
				$sql = sprintf("SELECT c.* FROM reifaxcounty.county c join reifaxcounty.state s on c.idstate=s.idstate where c.name='%s' AND s.code='%s';", $conExtractorMaster -> real_escape_string($dataEval['county']), $dataEval['state']);
				$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
				if ($res -> num_rows) {
					$countyEval = $res -> fetch_assoc();
				} else {
					$errors .= "el condados '{$dataEval['county']}, {$dataEval['state']}' no coincidio<br>";
					unset($countyEval);
				}
			}
			if (isset($countyEval)) {
				$sql = sprintf("INSERT INTO `reifaxcounty`.`county_origin` (`idcounty`, `idsystem`, `n_records`, `n_page`, `date_update`) VALUES (%d, %d, %d,%d, '%s');", $countyEval['idcounty'], $dataEval['oringin'], $dataEval['cant'], $dataEval['pages'], $chekponi);
				$res = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
			}
		}
		echo json_encode(array('success' => true, 'total' => count($_POST['data']), 'errors' => $errors));
		break;
	case 'selectRecordsJob' :
		$dateRespuesta = array();
		$sql = "SELECT * FROM reifaxcounty.statistics_down;";
		$resSystem = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);
		while ($dataSystem = $resSystem -> fetch_assoc()) {
			array_push($dateRespuesta, $dataSystem);
		}
		echo json_encode(array('success' => true, 'data' => $dateRespuesta));
		break;
	case 'selectStat' :
		$dateRespuesta = array();
		$indexState = array();
		if (isset($_POST['showAll']) && $_POST['showAll'] != 'false') {

			$sql2 = "SELECT * FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id group by o.date_update,s.name;";
			$resSystem = $conExtractorMaster -> query($sql2) or die($sql2 . $conExtractorMaster -> error);
			while ($dataSystem = $resSystem -> fetch_assoc()) {
				$i = 0;
				$sql3 = "SELECT
						    date_update,
						    c.*,
						    e.code state,
						    s.id,
						    SUM(n_records) n_records,
						    SUM(n_page) n_page
						FROM
						    reifaxcounty.system_origins s
						        JOIN
						    reifaxcounty.county_origin o ON o.idsystem = s.id
						        JOIN
						    reifaxcounty.county c ON o.idcounty = c.idcounty
						    	JOIN
						    reifaxcounty.state e on c.idstate=e.idstate
						WHERE
						    date_update = '{$dataSystem['date_update']}'
						GROUP BY c.idstate;";
				$resRecord = $conExtractorMaster -> query($sql3) or die($sql3 . $conExtractorMaster -> error);

				while ($dataRecord = $resRecord -> fetch_assoc()) {
					if (isset($indexState[$dataRecord['code']])) {
						$indice = $indexState[$dataRecord['code']];
					} else {
						$indice = $indexState[$dataRecord['code']] = $i;
						$dateRespuesta[$indice] = $dataRecord;
						$i++;
					}
					$dateRespuesta[$indice]['system_' . $dataRecord['id']] = $dataRecord['n_records'];
				}
			}
		} else {
			$sql = sprintf("SELECT c.*, s.code state FROM reifaxcounty.county c
					join reifaxcounty.state s on c.idstate=s.idstate where s.idstate=%d order by s.code desc", $_POST['id']);
			$resCounty = $conExtractorMaster -> query($sql) or die($sql . $conExtractorMaster -> error);

			while ($dataCounty = $resCounty -> fetch_assoc()) {
				array_push($dateRespuesta, $dataCounty);
				$lastRecord = count($dateRespuesta) - 1;

				$sql2 = "SELECT * FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id group by o.date_update,s.name;";
				$resSystem = $conExtractorMaster -> query($sql2) or die($sql2 . $conExtractorMaster -> error);

				while ($dataSystem = $resSystem -> fetch_assoc()) {
					$sql3 = "SELECT n_records,n_page,date_update,s.id FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id where date_update='{$dataSystem['date_update']}' AND idcounty={$dataCounty['idcounty']};";
					$resRecord = $conExtractorMaster -> query($sql3) or die($sql3 . $conExtractorMaster -> error);

					while ($dataRecord = $resRecord -> fetch_assoc()) {
						$dateRespuesta[$lastRecord]['system_' . $dataRecord['id']] = $dataRecord['n_records'];
					}
				}
			}
		}
		echo json_encode(array('success' => true, 'data' => $dateRespuesta));
		break;
}
