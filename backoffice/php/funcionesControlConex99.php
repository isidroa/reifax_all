<?php
	session_start();	

	$bkouserid=$_SESSION['bkouserid'];
	if(strlen($bkouserid)==0)$bkouserid=$_COOKIE['bkouseridc'];
	include($_SERVER["DOCUMENT_ROOT"]."/connection.php");
	$Connect	=	new connect();
	$Connect->connectInit();
	
	$conex=$Connect->mysqlByServer(array('serverNumber' => 'Automate'));

	class Fecha {
		var $fecha;
		  function Fecha($anio=0, $mes=0, $dia=0) 
		  {
			   if($anio==0) $anio = Date("Y");
			   if($mes==0) $mes = Date("m");
			   if($dia==0) $dia = Date("d");
			   $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
		  }
		  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
		  {
			   $array_date = explode("-", $this->fecha);
			   $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
		  }
		 
		  function getFecha() { return $this->fecha; }
	}

	
	$opcion=$_POST['opcion'];
	if(strlen($opcion)==0)$opcion=$_GET['opcion'];

	switch($opcion)
	{
		case('forceLogout'):
		{
			$sql="	UPDATE `xima`.`connectionhours` 
					SET `forceLogout`=1
					WHERE `userid`={$_POST['userid']} ";
			$conex->query ($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
		}
		break;

		case ('readSchedules'):
		{
			$sql="	SELECT h.*, CONCAT(u.name,' ',u.surname,' (',u.userid,')') username 
					FROM `xima`.connectionhours h
					INNER JOIN `xima`.`ximausrs` u on u.userid=h.userid order by username;
			";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data
			));
		}
		break;

		case('deleteUser'):
		{
			$sql="delete from `xima`.`connectionhours` where userid={$_POST['userid']}";
			$conex->query ($sql) or die ($sql.$conex->error);
		}
		break;
		
		case ('readVacation'):
		{
			$sql="	SELECT (count(*)*28800) balance, CONCAT(u.name,' ',u.surname ) username,v.userid
					FROM xima.connectionvacations v
					INNER JOIN xima.ximausrs u on u.userid=v.userid
					GROUP BY v.userid";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data
			));
		}
		break;

		case ('readVacationDetails'):
		{
			if(isset($_POST['userid']))
			{
				$sql="	SELECT c.id,c.`reason`,c.`daterecord`,c.`userid`,c.`year`,
						(SELECT concat(x.`NAME`,' ',x.`SURNAME`,' (',x.`USERID`, ')') FROM `xima`.`ximausrs` x  WHERE x.`USERID`=c.useridexec) creator 
						FROM xima.connectionvacations c
						WHERE c.`userid`={$_POST['userid']} 
						ORDER BY c.`daterecord` desc 
				";
				$res=$conex->query($sql) or die(json_encode(array(
					"success" 	=> FALSE,
					"error"		=> $conex->error,
					"sql"		=> $sql
				)));
				$data=array();
				while ($tem=$res->fetch_assoc()) {
					array_push($data,$tem);
				}
				echo json_encode(array(
					"success"	=> true,
					"total"		=> count($data),
					"data"		=> $data
				));
			}
		}
		break;

		case ('readStat'):
		{
			$where.=$_POST['dateInit']!=''? " AND daterecord >= '{$_POST['dateInit']}'":'';
			$where.=$_POST['dateEnd']!=''? " AND daterecord <= '{$_POST['dateEnd']}'":'';
			//$where.=$_POST['userid']!=''? " AND userid = '{$_POST['userid']}'":'';
			
							//(((sum(c.`minonline`)*60)+(sum(c.`minextra`)*60))-(sum(c.`minoffline`)*60)) minbalance 
			/*$sql="	SELECT c.`userid`,
							(SELECT concat(x.`NAME`,' ',x.`SURNAME`) FROM `xima`.`ximausrs` x  WHERE x.`USERID`=c.userid) nameUser, 
							(sum(c.`minonline`)*60) minonline, 
							(sum(c.`minoffline`)*60) minoffline, 
							(sum(c.`minextra`)*60) minextra,
							((sum(c.`minextra`)*60)-(sum(c.`minoffline`)*60)) minbalance 
					FROM xima.connectionlotes c
					GROUP BY c.`userid`
					ORDER BY nameUser";*/

			$sql="SELECT h.`userid`,
							(SELECT concat(x.`NAME`,' ',x.`SURNAME`) FROM `xima`.`ximausrs` x  WHERE x.`USERID`=h.userid) nameUser,
							(sum(c.`minonline`)*60) minonline,
							(sum(c.`minoffline`)*60) minoffline,
							(sum(c.`minextra`)*60) minextra,
							((sum(c.`minextra`)*60)-(sum(c.`minoffline`)*60)) minbalance
					FROM connectionhours h
					LEFT JOIN xima.connectionlotes c on c.`userid`=h.`userid`
					WHERE 1=1 $where 
					GROUP BY h.`userid`
					ORDER BY nameUser";
					
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}			
			
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data,
				"sql"		=> $sql
			));
		}
		break;

		case ('readDetailsConex'):
		{
			$where='';
			if($_POST['dateInit']<>'' && $_POST['dateEnd']<>'')
				$where.=" AND c.`daterecord` BETWEEN  '{$_POST['dateInit']}'  AND '{$_POST['dateEnd']}' ";
			else
				//$where.=" AND c.`daterecord` BETWEEN  SUBDATE(CURDATE(),WEEKDAY(CURDATE()))  AND CURDATE() ";
				$where.=" AND c.`daterecord` BETWEEN  '".date('Y-m')."-01'  AND CURDATE() ";
			
				//((((c.`minonline`)*60)+((c.`minextra`)*60))-((c.`minoffline`)*60)) minbalance 
			$sql="SELECT c.`userid`,
							c.`daterecord`, DATE_FORMAT(c.`daterecord`,'%W') weekday,
							((c.`minonline`)*60) minonline, 
							((c.`minoffline`)*60) minoffline, 
							((c.`minextra`)*60) minextra,
							(((c.`minextra`)*60)-((c.`minoffline`)*60)) minbalance 
					FROM xima.connectionlotes c
					where c.userid= {$_POST['userid']} $where 
					order by c.`daterecord` desc";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}

			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data,
				"sql"		=> $sql
			));
		}
		break;

		case ('recorduserUser'):
		{			
			//return;
			if($_POST['signo']=='vacations'){
				$event_date = $_POST['date'];
				$event_end_date = $_POST['date2'];
				$eventyear = $_POST['year'];
				
				$date_calculation = " +1 day";
				
				$dateArray[] =  $event_date;
				
				$day = strtotime($event_date);
				$to = strtotime($event_end_date);

				$entra=0;				
				while( $day <= $to ) 
				{				
					$fechaEval = date("Y-m-d" , $day);
					$day = strtotime(date("Y-m-d", $day) . $date_calculation);
					$i = getdate(strtotime ($fechaEval));
					
					//echo 
					$sql="SELECT `userid`
							FROM `xima`.`connectionvacations`
							WHERE `userid`={$_POST['userid']} AND `daterecord`='{$fechaEval}'";
					$res1=$conex->query($sql); 
					$row1=$res1->fetch_assoc();
					if($row1['userid']=='')
					{//SI ESA FECHA NO TIENE VACACIONES YA ANOTADAS
						//echo 
						$sql="SELECT title
								FROM xima.connectionnolaboral c
								INNER JOIN xima.connectionnolaboraluser cu ON cu.idnol=c.idnol
								where cu.userid='{$_POST['userid']}' AND c.`date`='{$fechaEval}'";
						$res2=$conex->query($sql); 
						$row2=$res2->fetch_assoc();
						if($row2['title']=='')
						{//SI ESA FECHA NO TIENE DIA NO LABORAL YA ANOTADAS
							//echo "entra".$i['wday'];
							//print_r($i);
							if(($i['wday']>= 1 && $i['wday']<=5)){//SI LA FECHA ES DE LUNES A VIERNES
								$reason="Assigned {$fechaEval} day vacation";
								if (strpos($_POST['reason'], "day vacation")=== false) $reason=$_POST['reason'];
								
								if($entra<>1)
								{
									$value=$event_date;
									if($event_date<>$event_end_date) $value="{$event_date} / {$event_end_date}";
									
									$sql="	INSERT INTO `xima`.`connectionhistory`(`reason`,`value`,`insertdate`,`type`,`userid`,`useridcreator`)
										VALUES('{$_POST['reason']}','{$value}',now(),'VACATION','{$_POST['userid']}','{$bkouserid}')";
									$conex->query($sql) or die($sql.$conex->error);
								}								
								//echo 
								$sql="	INSERT INTO `xima`.`connectionvacations`(`userid`, `daterecord`, `useridExec`, `reason` , `year`)
										VALUES('{$_POST['userid']}','{$fechaEval}','{$bkouserid}','{$reason}','{$eventyear}')";
								$conex->query($sql) or die($sql.$conex->error);								
								$entra=1;								
							}						
						}
					} 
				}
				
				array(
					"success"	=> true,
					"msg"		=> 'Operation successfully'
				);
			}
			else if($_POST['signo']=='holidays'){
				$userid = $_POST['useridhol'];
				$idholiday = $_POST['idholiday'];
									
				$sql="	INSERT INTO `xima`.`connectionnolaboraluser`( `userid`, `idnol`, `useridcreator`)
						VALUES('{$userid}','{$idholiday}','{$bkouserid}')";
				$conex->query($sql) or die($sql.$conex->error);
				
				array(
					"success"	=> true,
					"msg"		=> 'Operation successfully'
				);
			}
			else
			{
				if($_POST['signo']=='+'){
					$sqlRecord="UPDATE `xima`.`connectionlogs` c
								SET
									c.`timedateclient`=date_add(concat(c.`evaldate`,' ', c.`evaltime`), interval 30 minute),
									c.`timedateserver`=concat(c.`evaldate`,' ', c.`evaltime`),
									c.`timematriz`=date_add(concat(c.`evaldate`,' ', c.`evaltime`), interval 30 minute),
									c.`device`=3,
									c.`check`=1
								WHERE c.`userid`={$_POST['userid']} AND c.`evaldate`='{$_POST['date']}'
								AND c.`evaltime` between date_sub(time('{$_POST['initime']}:00'), interval 30 minute) and date_sub(time('{$_POST['endtime']}:00'), interval 30 minute)							
								";
					$conex->query($sqlRecord) or die($sqlRecord.$conex->error);
					$tipo='ADD';
				}
				if($_POST['signo']=='-'){
					$sqlRecord="UPDATE `xima`.`connectionlogs` c
								SET
									c.`timedateclient`=null,
									c.`timedateserver`=null,
									c.`timematriz`=null,
									c.`device`=0,
									c.`check`=0
								WHERE c.`userid`={$_POST['userid']} AND c.`evaldate`='{$_POST['date']}'
								AND c.`evaltime` between date_sub(time('{$_POST['initime']}:00'), interval 30 minute) and date_sub(time('{$_POST['endtime']}:00'), interval 30 minute)							
								";
					$conex->query($sqlRecord) or die($sqlRecord.$conex->error);
					$tipo='SUBTRACTION';
				}

				$event_date = $_POST['initime'];
				$event_end_date = $_POST['endtime'];
				$value=$event_date;
				if($event_date<>$event_end_date) $value="({$_POST['date']}) {$event_date}:00 / {$event_end_date}:00";
				
				$sql="	INSERT INTO `xima`.`connectionhistory`(`reason`,`value`,`insertdate`,`type`,`userid`,`useridcreator`)
						VALUES('{$_POST['reason']}','{$value}',now(),'{$tipo}','{$_POST['userid']}','{$bkouserid}')";
				$conex->query($sql) or die($sql.$conex->error);
				
				//echo $_POST['date'].'||'.date('Y-m-d');
				if($_POST['date']<>date('Y-m-d'))
				{//si es cualquier  fecha distinta a hoy , hay que calcular de nuevo `minonline`,`minoffline`,`minextra` de la tabla connectionlotes 

					$fechaeval=$_POST['date'];
					$userid=$_POST['userid'];

					//VERIFICANDO SI FECHA ES FIN DE SEMANA
					$dia=date('N',strtotime($fechaeval));
					$FINSEMANA=0;
					if($dia==6 || $dia==7)$FINSEMANA=1;

					//VERIFICANDO SI FECHA ES DIA DE VACACION DEL USUARIO
					//echo 
					$sql="SELECT userid
						FROM xima.connectionvacations c
						WHERE c.`userid`=$userid and c.daterecord='$fechaeval'";
					$result=$conex->query ($sql) or die ($sql.$conex->error);
					$row=$result->fetch_object();
					$VACACION=0;
					if($row->userid<>'')$VACACION=1;

					//VERIFICANDO SI FECHA ES DIA NO LABORABLE
					//echo 
					$sql="	SELECT c1.`title` 
							FROM xima.connectionnolaboraluser c
							INNER JOIN xima.connectionnolaboral c1 ON c.idnol=c1.idnol 
							WHERE c.`userid`=$userid and c1.`date`='$fechaeval'";
					$result=$conex->query ($sql) or die ($sql.$conex->error);
					$row=$result->fetch_object();
					$NOLABORAL=0;
					if($row->title<>'')$NOLABORAL=1;

					//BUSCANDO EL HORARIO DE TRABAJO DEL USUARIO
					//echo 
					$sql="SELECT `break`, `server_hour_init`, `server_hour_end`, `server_hour_break_init`, `server_hour_break_end`, `userid`
						FROM xima.connectionhours c
						WHERE c.`userid`=$userid";
					$result=$conex->query ($sql) or die ($sql.$conex->error);
					$hours=$result->fetch_object();
					$wherehours1=$wherehours2='';
					if($hours->break==1)
					{
						$wherehours1="	and ( (c.`evaltime` between '".$hours->server_hour_init."' and '".$hours->server_hour_break_init."') or
										(c.`evaltime` between '".$hours->server_hour_break_end."' and '".$hours->server_hour_end."' ))";
						$wherehours2="	and ( (c.`evaltime` NOT between '".$hours->server_hour_init."' and '".$hours->server_hour_break_init."') and
										(c.`evaltime` NOT between '".$hours->server_hour_break_end."' and '".$hours->server_hour_end."' ))";
					}
					else
					{
						$wherehours1=" and ( c.`evaltime` between '".$hours->server_hour_init."' and '".$hours->server_hour_end."' )";
						$wherehours2=" and ( c.`evaltime` NOT between '".$hours->server_hour_init."' and '".$hours->server_hour_end."' )";
					}

					if($FINSEMANA==0 && $VACACION==0 && $NOLABORAL==0)
					{//sino es fin de semana ni esta de vacaciones ni es dia no laboral
						//echo 
						$sql="	SELECT count(*) minonline 
								FROM connectionlogs c
								WHERE c.`userid`=$userid AND c.`evaldate`='$fechaeval'
								and `check`=1
								$wherehours1";
						$result=$conex->query ($sql) or die ($sql.$conex->error);
						$row=$result->fetch_object();
						$minonline=$row->minonline;
						
						//echo 
						$sql="	SELECT count(*) minoffline 
								FROM connectionlogs c
								WHERE c.`userid`=$userid AND c.`evaldate`='$fechaeval'
								and `check`=0
								$wherehours1";
						$result=$conex->query ($sql) or die ($sql.$conex->error);
						$row=$result->fetch_object();
						$minoffline=$row->minoffline;

						//echo 
						$sql="	SELECT count(*) minextra 
								FROM connectionlogs c
								WHERE c.`userid`=$userid AND c.`evaldate`='$fechaeval'
								and `check`=1
								$wherehours2";
						$result=$conex->query ($sql) or die ($sql.$conex->error);
						$row=$result->fetch_object();
						$minextra=$row->minextra;
					}
					else
					{//todos los online fueron minutos extras bien sea por vacaciones, fin de semana o dia no laboral
						//echo 
						$sql="	SELECT count(*) minonline 
								FROM xima.connectionlogs c
								WHERE c.`userid`=$userid AND c.`evaldate`='$fechaeval'
								and `check`=1";
						$result=$conex->query ($sql) or die ($sql.$conex->error);
						$row=$result->fetch_object();
						$minextra=$row->minonline;
						$minonline=0;
						$minoffline=0;	
					} 

					//echo 
					$sql="	UPDATE xima.connectionlotes
							SET `minonline`='$minonline', 
								`minoffline`='$minoffline', 
								`minextra`='$minextra'
							WHERE `userid`=$userid AND `daterecord`='$fechaeval'
						";
					$conex->query ($sql) or die ($sql.$conex->error);
				}
					
				array(
					"success"	=> true,
					"msg"		=> 'Operation successfully'
				);	
			}				
		}
		break;
		
		case ('readDetailsConexMod'):
		{
			$where.=$_POST['dateInit']!=''? " AND date(c.`insertdate`) >= '{$_POST['dateInit']}'":'';
			$where.=$_POST['dateEnd']!=''? " AND date(c.`insertdate`) <= '{$_POST['dateEnd']}'":'';			

			$sql="SELECT c.`reason`, c.`value`, c.`insertdate`, date(c.`insertdate`) datecreate, c.`type`,c.userid,
				(SELECT concat(x.`NAME`,' ',x.`SURNAME`,' (',x.`USERID`, ')') FROM `xima`.`ximausrs` x  WHERE x.`USERID`=c.useridcreator) creator
				FROM connectionhistory c
				where c.userid={$_POST['userid']}  {$where}
				order by insertdate;";

			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) $data[]=$tem;
			
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data
			));
		}
		break;
		
		case ('readNolaboral'):
		{
			$sql="	SELECT (count(*)*28800) balance, CONCAT(u.name,' ',u.surname ) username,v.userid
					FROM xima.connectionnolaboraluser v
					INNER JOIN xima.ximausrs u on u.userid=v.userid
					GROUP BY v.userid
					order by username";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data
			));
		}
		break;

		case ('readNolaboralDetails'):
		{
			if(isset($_POST['userid']))
			{
				$sql="	SELECT v.id,c.`title`, c.`date`,v.userid,
						(SELECT concat(x.`NAME`,' ',x.`SURNAME`,' (',x.`USERID`, ')') FROM `xima`.`ximausrs` x  WHERE x.`USERID`=v.useridcreator) creator  
						FROM xima.connectionnolaboraluser v
						INNER JOIN xima.connectionnolaboral c on v.idnol=c.idnol
						WHERE v.userid={$_POST['userid']}
						order by c.`date` desc
				";
				$res=$conex->query($sql) or die(json_encode(array(
					"success" 	=> FALSE,
					"error"		=> $conex->error,
					"sql"		=> $sql
				)));
				$data=array();
				while ($tem=$res->fetch_assoc()) {
					array_push($data,$tem);
				}
				echo json_encode(array(
					"success"	=> true,
					"total"		=> count($data),
					"data"		=> $data
				));
			}
		}
		break;

		case ('recordDateNoLaboral'):
		{
			$date=$_POST['date'];
			//$formfields=explode('&',$_POST['formfields']);
			//$title=str_replace('title=','',trim($formfields[0]));
			//$itemselector=str_replace('itemselector=','',trim($formfields[1]));
			$itemselector=$_POST['itemselector'];
			$title=$_POST['title'];

			$sql="INSERT INTO xima.`connectionnolaboral`(`title`,`date`)
				VALUES('$title','$date')";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			
			$sql="select idnol from xima.`connectionnolaboral` order by idnol desc limit 1";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$tem=$res->fetch_assoc();
			$idnolab=$tem['idnol'];					
				
			$arraux=explode(',',$itemselector);
			foreach($arraux as $v)
			{
				$sql="INSERT INTO xima.`connectionnolaboraluser`(`userid`, `idnol`, `useridcreator`)
					VALUES('$v','$idnolab','$bkouserid')";
				$conex->query($sql) or die(json_encode(array(
					"success" 	=> FALSE,
					"error"		=> $conex->error,
					"sql"		=> $sql
				)));

				$sql="	INSERT INTO `xima`.`connectionhistory`(`reason`,`value`,`insertdate`,`type`,`userid`,`useridcreator`)
						VALUES('$title','$date',now(),'NON BUSINESS DAY','$v','$bkouserid')";
				$conex->query($sql) or die(json_encode(array(
					"success" 	=> FALSE,
					"error"		=> $conex->error,
					"sql"		=> $sql
				)));				
			}
			
			echo json_encode(array(
				"success"	=> true,
				"id"		=> $idnolab
			));
		}
		break;

		case ('updateEvent'):
		{
			$sql="UPDATE xima.connectionnolaboral SET title='{$_POST['title']}' where idnol={$_POST['id']}";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			echo json_encode(array(
				"success"	=> true
			));
		}
		break;
		
		case ('readFullCalendar'):
		{
			$cbmonthCalendar=$_POST['cbmonthCalendar'];
			$cbyearCalendar=$_POST['cbyearCalendar'];
			if(strlen($cbmonthCalendar)==0)$cbmonthCalendar=$_GET['cbmonthCalendar'];
			if(strlen($cbyearCalendar)==0)$cbyearCalendar=$_GET['cbyearCalendar'];

			$sql="SELECT concat(x.name,'(',x.userid,')','-',c1.`title`)title, c1.`date`
				FROM connectionnolaboraluser c
				INNER JOIN connectionnolaboral c1 ON c.idnol=c1.idnol
				INNER JOIN ximausrs x ON c.userid=x.USERID
				WHERE MONTH(c1.`date`)=$cbmonthCalendar AND  YEAR(c1.`date`)=$cbyearCalendar
				UNION
				SELECT  concat(x.name,'(',x.userid,')','-','Holiday')title,  c.`daterecord` date
				FROM connectionvacations c
				INNER JOIN ximausrs x ON c.userid=x.USERID
				WHERE MONTH(c.`daterecord`)=$cbmonthCalendar AND  YEAR(c.`daterecord`)=$cbyearCalendar
			";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
			
			function findarray($data,$date)
			{
				$arrret=array();
				foreach($data as $v)
				{
					if($v['date']==$date)
						$arrret[]=$v;
				}
				return $arrret;
			}
			//echo "<pre>";print_r(findarray($data,'2014-12-19'));echo "</pre>";
			
			$month=$cbmonthCalendar;
			$year=$cbyearCalendar;
			$diaActual=date("j");

			$diaSemana=date("w",mktime(0,0,0,$month,1,$year))+7; 
			$ultimoDiaMes=date("d",(mktime(0,0,0,$month+1,1,$year)-1));

			$meses=array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');

			echo '<center>
			<table id="calendartable"> 
				<caption>'.$meses[$month]." ".$year.'</caption>
				<tr> 
					<th><b>Mon</b></th><th><b>Tue</b></th><th><b>Wed</b></th><th><b>Thu</b></th>
					<th><b>Fri</b></th><th><b>Sat</b></th><th><b>Sun</b></th>
				</tr>
				<tr bgcolor="silver"> 
			';
			$last_cell=$diaSemana+$ultimoDiaMes;
			for($i=1;$i<=42;$i++)
			{
				if($i==$diaSemana)
				{
					$day=1;
				}
				if($i<$diaSemana || $i>=$last_cell)
				{
					echo "<td width='14%' >&nbsp;</td>";
				}else{
					$fech=$cbyearCalendar.'-'.str_pad($cbmonthCalendar, 2, "0", STR_PAD_LEFT).'-'.str_pad($day, 2, "0", STR_PAD_LEFT);
					$arrenc=findarray($data,$fech);
					$texto='';
					foreach($arrenc as $v)
						$texto.='<br>* '.$v['title'];
					
					if($day==$diaActual && str_pad($cbmonthCalendar, 2, "0", STR_PAD_LEFT)==date('m'))
						echo "<td  valign='top' class='hoy' width='14%' ><p><b>$day</b></p><p>$texto</p></td>";
					else
						echo "<td valign='top' width='14%' ><p><b>$day</b></p><p>$texto</p></td>";
					$day++;
				}
				if($i%7==0)
				{
					echo "</tr><tr>\n";
				}
			}
			echo "
				</tr>
			</table></center>";
			
		}
		break;

		case ('readDateNoLaboral'):
		{
			$sql="SELECT *	FROM xima.connectionnolaboral";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data
			));
		}
		break;

		case ('deleteEvent'):
		{
			$sql="delete FROM xima.connectionnolaboral where idnol={$_POST['id']};
			";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			echo json_encode(array(
				"success"	=> true
			));
		}
		break;

		case('deleteHoliday'):
		{
			$sql="delete from `xima`.`connectionvacations` where userid={$_POST['userid']} and  id={$_POST['id']}";
			$conex->query ($sql) or die ($sql.$conex->error);
		}
		break;
		
		case('deleteNBD'):
		{
			$sql="delete from `xima`.`connectionnolaboraluser` where userid={$_POST['userid']} and  id={$_POST['id']}";
			$conex->query ($sql) or die ($sql.$conex->error);
		}
		break;

		case ('updateSchedules'):
		{
			$records = json_decode(stripslashes($_POST['records']));
	        foreach($records as $k){ 
	                $sql="UPDATE `xima`.`connectionhours`
						SET
						`hour_init` = '{$k->hour_init}',
						`server_hour_init` = date_sub(time('{$k->hour_init}'),INTERVAL 30 minute),
						`hour_end` = '{$k->hour_end}',
						`server_hour_end` = date_sub(time('{$k->hour_end}'),INTERVAL 30 minute),
						`hour_break_init` = '{$k->hour_break_init}',
						`server_hour_break_init` = date_sub(time('{$k->hour_break_init}'),INTERVAL 30 minute),
						`hour_break_end` = '{$k->hour_break_end}',
						`server_hour_break_end` = date_sub(time('{$k->hour_break_end}'),INTERVAL 30 minute),
						`break` = {$k->break}
						WHERE userid = {$k->userid}";
						
						
					$conex->query($sql) or die(json_encode(array(
						"success" 	=> FALSE,
						"error"		=> $conex->error,
						"sql"		=> $sql
					)));
	        }  
			if(mysql_affected_rows()>0)
			{
				echo json_encode(array(
					'success' => true,
					'sql'	=> $sql
				));
			}
			else
			{echo json_encode(array(
					'success' => false,
					'sql'	=> $sql,
					'error' => $conex->error
				));
			}
		}
		break;

		case('recordNewUser'):
		{
			$sql="select count(*) cant from `xima`.`ximausrs` where userid={$_POST['txt-userid']}";
			$res=$conex->query ($sql) or die ($sql.$conex->error);
			$cant=$res->fetch_assoc();
			if($cant['cant']==0){
				$sql="INSERT INTO `xima`.`ximausrs`
					(`USERID`,
					`PASS`,
					`NAME`,
					`SURNAME`,
					`EMAIL`,
					`idstatus`,
					`idusertype`)
					VALUES
					({$_POST['txt-userid']},
					'{$_POST['txt-pass']}',
					'{$_POST['txt-name']}',
					'{$_POST['txt-surname']}',
					'{$_POST['txt-email']}',
					3,
					7);
				";
				$conex->query ($sql) or die ($sql.$conex->error);
			}
			$sql="INSERT INTO `xima`.`connectionhours`
				(`userid`)
				VALUES
				({$_POST['txt-userid']});
				";
			$conex->query ($sql) or die ($sql.$conex->error);
			echo json_encode(array('success'=> true ,'msg' => 'Agregado con exito'));
		}
		break;
		
		case('createMatrizuser'):
		{

			echo "<br>inicio ".date('Y-m-d H:i:s');

			$userid=$_GET['u'];
			
			$sql="SELECT count(*) cant FROM connectionlogs c WHERE c.`userid`=$userid";
			$res=$conex->query ($sql) or die ($sql.$conex->error);
			$data=array();
			$tem=$res->fetch_assoc(); 
			array_push($data,$tem);
			$can=$data[0]['cant'];
			if($can>0)
			{	
				echo "<br><span style='color:#FF0000'><strong>***** USERID: $userid, YA TIENE CREADA LA TABLA MATRIZ</strong></span><br>";
				return;
			}
			
			$sql="SELECT * FROM connectionconfig where name='datematriz'";
			$res=$conex->query ($sql) or die ($sql.$conex->error);
			$data1=array();
			$tem=$res->fetch_assoc(); 
			array_push($data1,$tem);
			$datematriz=explode('-',$data1[0]['value']);

			$fecha=new Fecha($datematriz[0],$datematriz[1],$datematriz[2]);
			
			
			$i=0;	
			for($d=0;$d<30;$d++)
			{
				$fecha->SumarFecha(0,0,1); 
				$fechaeval=$fecha->getFecha();
				for($h=0;$h<24;$h++)
					for($m=0;$m<60;$m++)
					{
						//echo '<br>fechaeval: '.$fechaeval.' '.str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, "0", STR_PAD_LEFT).':00';
						$dateval=$fechaeval;
						$timeval=str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, "0", STR_PAD_LEFT).':00';
						$qbd="INSERT INTO xima.connectionlogs (`userid`, `evaldate`,`evaltime`) VALUES ";
						$qbd.="('$userid','$dateval','$timeval')";
						//$conex->query ($qbd) or die ($qbd.$conex->error);
						echo '<br>'.$qbd;
						return;
						$i++;
					}
			}
			echo "<br>termino ".date('Y-m-d H:i:s');
		
			return;
		}
		break;
		


		
		
		
		
		
		
		
		
		
		
		
		case ('assingVacations'):
		{
			$sql="INSERT INTO `xima`.`cobro_connections_vacation`
				(
					`userid`,
					`reason`,
					`value`,
					`date_record`)
				VALUES
				(
					{$_POST['userid']},
					'Discount for time not worked',
					{$_POST['value']},
					NOW());";
			$sqlRecord="INSERT INTO `cobro_conections_record` (`userid`,`record`,`date`,`normal`,`reason`,`dateAssing`,useridExec)
						VALUES
					(
						{$_POST['userid']},
						{$_POST['value']},
						NOW(),
						4,
						'Assigned on vacation',
						NOW(),
						$bkouserid);";
			$conex->query($sqlRecord) or die($sqlRecord.$conex->error);
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			echo json_encode(array(
				"success"	=> true,
				"id"		=> mysql_insert_id()
			));
		}
		break;
		case ('readDetailsDesconect'):
		{
			$user=($_POST['userid'])?$_POST['userid']:$bkouserid;
			$where.=$_POST['dateInit']!=''? " AND date_desconect >= '{$_POST['dateInit']}'":'';
			$where.=$_POST['dateEnd']!=''? " AND date_desconect <= DATE_add('{$_POST['dateEnd']}',INTERVAL 1 day) ":'';
			$sql="SELECT 
				    value timeDesconect,
				    type,
				    if(type = 1,
				        date_desconect,
				        if(type = 1,
				            DATE_SUB(date_desconect,
				                INTERVAL value SECOND),
				            DATE_add(date_desconect,
				                INTERVAL value SECOND))) dateDesconect2,
				    if(type = 1,
				        if(type = 1,
				            DATE_SUB(date_desconect,
				                INTERVAL value SECOND),
				            DATE_add(date_desconect,
				                INTERVAL value SECOND)),
				        date_desconect) dateDesconect1
				FROM
				    cobro_connections_logdesconect
				where
				    userid = {$user} {$where} AND value>0
				order by date_desconect desc;";
			$res=$conex->query($sql);
			$desconects=array();
			while ($data=$res->fetch_assoc()) {
				$timezoneG = new DateTimeZone('America/New_York');
				$timezoneC = new DateTimeZone('America/Caracas');
				$data['dateDesconect2'] = new DateTime($data['dateDesconect2'], $timezoneG);
				$data['dateDesconect2']->setTimezone($timezoneC);
				$data['dateDesconect1'] = new DateTime($data['dateDesconect1'], $timezoneG);
				$data['dateDesconect1']->setTimezone($timezoneC);
				$data['dateDesconect2']=$data['dateDesconect2']->format('Y-m-d H:i:s');
				$data['dateDesconect1']=$data['dateDesconect1']->format('Y-m-d H:i:s');
				array_push($desconects,$data);
			}
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($desconects),
				"data"		=> $desconects
			));
		}	
		break;
		case('desconectUser'):
		{
			$sql="UPDATE `cobro_connectios_status` set 
						`status` =2
					WHERE
						`userid`={$_POST['userid']}
					";
			$conex->query ($sql) or die ($sql.$conex->error);
		}
		break;
		case ('readDetailsConexold'):
		{
			$where.=$_POST['dateInit']!=''? " AND dateAssing >= '{$_POST['dateInit']}'":'';
			$where.=$_POST['dateEnd']!=''? " AND dateAssing <= '{$_POST['dateEnd']}'":'';
			$sql="select 
					glob.userid,
					glob.dateAssing,
					glob.glob status,
					laboral.glob horario,
					nlaboral.glob Fhorario,
					inlaboral.glob statusLaboral,
					concat(u.name, ' ', u.surname) nameUser
				from
					(select 
						userid, dateAssing, sum(record) glob
					FROM
						cobro_conections_record
					where
						1 = 1 AND userid = {$_POST['userid']} {$where}
					group by dateAssing) glob
						left join
					(select 
						userid, dateAssing, sum(record) glob
					FROM
						cobro_conections_record
					where
						normal in (1) AND userid = {$_POST['userid']} {$where}
					group by dateAssing) laboral ON laboral.dateAssing = glob.dateAssing
						left join
					(select 
						userid, dateAssing, sum(record) glob
					FROM
						cobro_conections_record
					where
						normal = 2 AND userid = {$_POST['userid']} {$where}
					group by dateAssing) nlaboral ON nlaboral.dateAssing = glob.dateAssing
						left join
					(select 
						userid, dateAssing, sum(record) glob
					FROM
						cobro_conections_record
					where
						normal in (4 , 3, 1) AND userid = {$_POST['userid']} {$where}
					group by dateAssing) inlaboral ON inlaboral.dateAssing = glob.dateAssing
						left join
					`ximausrs` u ON u.userid = glob.userid
				order by dateAssing;
			";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			while ($tem=$res->fetch_assoc()) {
				array_push($data,$tem);
			}
		
			$dateEnd=new DateTime($_POST['dateEnd']);
			$i = getdate();
			$nowDate="{$i['year']}-{$i['mon']}-{$i['mday']}";
			$nowDate=new DateTime($nowDate);
			$dateEnd=$dateEnd;
			$nowDate=$nowDate;
			
			if($dateEnd>=$nowDate){
				$sqlHours="SELECT *, date(NOW()) lastUpdate,NOW() dateUpdate FROM cobro_conections_hours where userid={$_POST['userid']} ;";
				$result=$conex->query($sqlHours) or die($sqlHours.$conex->error);
				$userH=$result->fetch_object();
				$init ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_init}";
				$out ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_end}";
				$initBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_init}";
				$outBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_end}";
				$timezoneG = new DateTimeZone('America/New_York');
				$timezoneC = new DateTimeZone('America/Caracas');
				$dateAux = new DateTime('NOW', $timezoneC);
				$dateAux->setTimezone($timezoneG);
				$initDate = new DateTime($init, $timezoneC);
				$initDate->setTimezone($timezoneG);
				$outDate = new DateTime($out, $timezoneC);
				$outDate->setTimezone($timezoneG);
				$initBreakDate = new DateTime($initBreak, $timezoneC);
				$initBreakDate->setTimezone($timezoneG);
				$outBreakDate = new DateTime($outBreak, $timezoneC);
				$outBreakDate->setTimezone($timezoneG);
				
				$dateRecort=$initDate->format('Y-m-d');
				$initDate=$initDate->format('Y-m-d H:i:s');
				$outDate=$outDate->format('Y-m-d H:i:s');
				$initBreakDate=$initBreakDate->format('Y-m-d H:i:s');
				$outBreakDate=$outBreakDate->format('Y-m-d H:i:s');
				$sqlHours="SELECT count(*) cant FROM cobro_conections_nolaboral where `date`='{$dateRecort}'";
				$res=$conex->query($sqlHours);
				$laboral=$res->fetch_assoc();
				
				$recordDiario=array();
				
				$recordDiario['userid']=$_POST['userid'];
				$recordDiario['dateAssing']= $dateRecort;
				if(($i['wday']> 0 && $i['wday']<6) && ($laboral['cant']==0)){
					
					
					$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$_POST['userid']} AND  ( timedateserver  
							between '{$initDate}' AND '{$initBreakDate}' OR
							timedateserver 
							between '{$outBreakDate}' AND '{$outDate}') AND timeDesconect<300";
							
					$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
					$dataTime=$result2->fetch_object();
					
					$recordDiario['horario']=$dataTime->seg;
					
					$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$_POST['userid']} AND  ( timedateserver  
							between '{$dateRecort} 00:00:00' AND '{$initDate}' OR
							timedateserver 
							between '{$initBreakDate}' AND '{$outBreakDate}' OR
							timedateserver 
							between '{$outDate}' AND '{$dateRecort} 23:59:59')AND timeDesconect<300";
					$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
					$dataTime=$result2->fetch_object();
					
					$recordDiario['Fhorario']=$dataTime->seg;
				}
				else{
					$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$_POST['userid']} AND  ( timedateserver  
							between '{$dateRecort} 00:00:00' AND '{$dateRecort} 23:59:59')AND timeDesconect<300";
					$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
					$dataTime=$result2->fetch_object();
					
					
					$recordDiario['Fhorario']=$dataTime->seg;
					
					
				}array_push($data,$recordDiario);
				
				
			}

			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data,
				"sql"		=> $sql
			));
		}
		break;
		case ('readStatold'):
		{
			$where.=$_POST['dateInit']!=''? " AND dateAssing >= '{$_POST['dateInit']}'":'';
			$where.=$_POST['dateEnd']!=''? " AND dateAssing <= '{$_POST['dateEnd']}'":'';
			$where.=$_POST['userid']!=''? " AND userid = '{$_POST['userid']}'":'';
			
			$sql="select glob.userid, glob.glob status,laboral.glob horario,nlaboral.glob Fhorario,inlaboral.glob statusLaboral,
	totalW.glob total, concat(u.name,' ',u.surname) nameUser  from 
(select userid,sum(record) glob FROM cobro_conections_record where 1=1 {$where} group by userid) glob
join  (select userid,sum(record) glob FROM cobro_conections_record where normal in (1) {$where} group by userid) laboral on laboral.userid=glob.userid
join (select userid,sum(record) glob FROM cobro_conections_record where normal=2 {$where} group by userid) nlaboral on nlaboral.userid=glob.userid
join (select userid,sum(record) glob FROM cobro_conections_record where normal in (4,3,1) {$where} group by userid) inlaboral on inlaboral.userid=glob.userid
					join
    (select 
        userid, sum(record) glob
    FROM
        cobro_conections_record
    where
        normal in (4 , 3, 1)
    group by userid) totalW ON totalW.userid = glob.userid
					join `ximausrs` u on u.userid=glob.userid order by nameUser;
			";
			$res=$conex->query($sql) or die(json_encode(array(
				"success" 	=> FALSE,
				"error"		=> $conex->error,
				"sql"		=> $sql
			)));
			$data=array();
			
			$dateEnd=new DateTime($_POST['dateEnd']);
			$dateInit=new DateTime($_POST['dateInit']);
			$i = getdate();
			$nowDate="{$i['year']}-{$i['mon']}-{$i['mday']}";
			$nowDate=new DateTime($nowDate);
			$dateEnd=$dateEnd;
			$nowDate=$nowDate;
			
			while ($tem=$res->fetch_assoc()) {
				if($dateEnd>=$nowDate){
					$sqlHours="SELECT *, date(NOW()) lastUpdate,NOW() dateUpdate FROM cobro_conections_hours  where userid={$tem['userid']} ;";
					$result=$conex->query($sqlHours) or die($sqlHours.$conex->error);
					$userH=$result->fetch_object();
					$init ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_init}";
					$out ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_end}";
					$initBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_init}";
					$outBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_end}";
					$timezoneG = new DateTimeZone('America/New_York');
					$timezoneC = new DateTimeZone('America/Caracas');
					$dateAux = new DateTime('NOW', $timezoneC);
					$dateAux->setTimezone($timezoneG);
					$initDate = new DateTime($init, $timezoneC);
					$initDate->setTimezone($timezoneG);
					$outDate = new DateTime($out, $timezoneC);
					$outDate->setTimezone($timezoneG);
					$initBreakDate = new DateTime($initBreak, $timezoneC);
					$initBreakDate->setTimezone($timezoneG);
					$outBreakDate = new DateTime($outBreak, $timezoneC);
					$outBreakDate->setTimezone($timezoneG);
					
					$dateRecort=$initDate->format('Y-m-d');
					$initDate=$initDate->format('Y-m-d H:i:s');
					$outDate=$outDate->format('Y-m-d H:i:s');
					$initBreakDate=$initBreakDate->format('Y-m-d H:i:s');
					$outBreakDate=$outBreakDate->format('Y-m-d H:i:s');
					$sqlHours="SELECT count(*) cant FROM cobro_conections_nolaboral where `date`='{$dateRecort}'";
					$res2=$conex->query($sqlHours);
					$laboral=$res2->fetch_assoc();
					
					$recordDiario=array();
					
					$recordDiario['userid']=$tem['userid'];
					$recordDiario['dateAssing']= $dateRecort;
					if(($i['wday']> 0 && $i['wday']<6) && ($laboral['cant']==0)){
						
						
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$tem['userid']} AND  ( timedateserver  
								between '{$initDate}' AND '{$initBreakDate}' OR
								timedateserver 
								between '{$outBreakDate}' AND '{$outDate}') AND timeDesconect<500";
								
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						$tem['horario']+=$dataTime->seg;
						
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$tem['userid']} AND  ( timedateserver  
								between '{$dateRecort} 00:00:00' AND '{$initDate}' OR
								timedateserver 
								between '{$initBreakDate}' AND '{$outBreakDate}' OR
								timedateserver 
								between '{$outDate}' AND '{$dateRecort} 23:59:59')AND timeDesconect<500";
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						$tem['Fhorario']+=$dataTime->seg;
					}
					else{
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$tem['userid']} AND  ( timedateserver  
								between '{$dateRecort} 00:00:00' AND '{$dateRecort} 23:59:59')AND timeDesconect<500";
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						
						$tem['Fhorario']+=$dataTime->seg;
						
						
					}
					//array_push($tem,$recordDiario);
				}
				
				array_push($data,$tem);
			}
			
			if($dateInit==$nowDate){
				if(isset($_POST['userid'])){
					$sqlHours="SELECT h.*, date(NOW()) lastUpdate,NOW() dateUpdate,concat(u.name,' ',u.surname) nameUser FROM cobro_conections_hours h join `ximausrs` u on u.userid=h.userid where userid={$_POST['userid']} ;";
				}
				else{
					$sqlHours="SELECT h.*, date(NOW()) lastUpdate,NOW() dateUpdate,concat(u.name,' ',u.surname) nameUser FROM cobro_conections_hours h join `ximausrs` u on u.userid=h.userid ;";
				}
				$result=$conex->query($sqlHours) or die($sqlHours.$conex->error);
				while($userH=$result->fetch_object()){
					$init ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_init}";
					$out ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_end}";
					$initBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_init}";
					$outBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userH->hour_break_end}";
					$timezoneG = new DateTimeZone('America/New_York');
					$timezoneC = new DateTimeZone('America/Caracas');
					$dateAux = new DateTime('NOW', $timezoneC);
					$dateAux->setTimezone($timezoneG);
					$initDate = new DateTime($init, $timezoneC);
					$initDate->setTimezone($timezoneG);
					$outDate = new DateTime($out, $timezoneC);
					$outDate->setTimezone($timezoneG);
					$initBreakDate = new DateTime($initBreak, $timezoneC);
					$initBreakDate->setTimezone($timezoneG);
					$outBreakDate = new DateTime($outBreak, $timezoneC);
					$outBreakDate->setTimezone($timezoneG);
					
					$dateRecort=$initDate->format('Y-m-d');
					$initDate=$initDate->format('Y-m-d H:i:s');
					$outDate=$outDate->format('Y-m-d H:i:s');
					$initBreakDate=$initBreakDate->format('Y-m-d H:i:s');
					$outBreakDate=$outBreakDate->format('Y-m-d H:i:s');
					$sqlHours="SELECT count(*) cant FROM cobro_conections_nolaboral where `date`='{$dateRecort}'";
					$res2=$conex->query($sqlHours);
					$laboral=$res2->fetch_assoc();
					
					$recordDiario=array();
					
					$recordDiario['nameUser']=$userH->nameUser;
					$recordDiario['userid']=$userH->userid;
					$recordDiario['dateAssing']= $dateRecort;
					
					if(($i['wday']> 0 && $i['wday']<6) && ($laboral['cant']==0)){
						
						
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$userH->userid} AND  ( timedateserver  
								between '{$initDate}' AND '{$initBreakDate}' OR
								timedateserver 
								between '{$outBreakDate}' AND '{$outDate}') AND timeDesconect<500";
								
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						$recordDiario['horario']=$dataTime->seg;
						
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$userH->userid} AND  ( timedateserver  
								between '{$dateRecort} 00:00:00' AND '{$initDate}' OR
								timedateserver 
								between '{$initBreakDate}' AND '{$outBreakDate}' OR
								timedateserver 
								between '{$outDate}' AND '{$dateRecort} 23:59:59')AND timeDesconect<500";
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						$recordDiario['Fhorario']+=$dataTime->seg;
					}
					else{
						$sqlSchedule="SELECT sum(timeDesconect) seg FROM cobro_connections where userid={$userH->userid} AND  ( timedateserver  
								between '{$dateRecort} 00:00:00' AND '{$dateRecort} 23:59:59')AND timeDesconect<500";
						$result2=$conex->query($sqlSchedule) or die($sqlSchedule.$conex->error);
						$dataTime=$result2->fetch_object();
						
						
						$recordDiario['Fhorario']+=$dataTime->seg;
						
						
					}
					array_push($data,$recordDiario);
				}
			}
			
			
			
			
			
			echo json_encode(array(
				"success"	=> true,
				"total"		=> count($data),
				"data"		=> $data,
				"sql"		=> $sql
			));
		}
		break;
		case ('recorduserUserOld'):
		{			
			$seg=($_POST['hours']*60*60)+($_POST['min']*60);
			if($_POST['signo']=='vacations'){
				$event_date = $_POST['date'];
				$event_end_date = $_POST['date2'];
				
				$date_calculation = " +1 day";
				
				$dateArray[] =  $event_date;
				
				$day = strtotime($event_date);
				$to = strtotime($event_end_date);
				
				while( $day <= $to ) 
				{				
					$day = strtotime(date("Y-m-d", $day) . $date_calculation);
					$fechaEval = date("Y-m-d" , $day);
					$i = getdate(strtotime ($fechaEval));
					$sql="SELECT count(*) cant FROM cobro_conections_nolaboral where `date`='{$fechaEval}'";
					$res=$conex->query($sql,$XB0YXJsYSB1); 
					$laboral=$res->fetch_assoc();
					if(($i['wday']>= 0 && $i['wday']<5) && ($laboral['cant']==0)){
						$sql="INSERT INTO `xima`.`cobro_connections_vacation`
							(
								`userid`,
								`reason`,
								`value`,
								`date_record`)
							VALUES
							(
								{$_POST['userid']},
								'assigned {$fechaEval} day holiday',
								28800,
								'{$fechaEval}');";
						$conex->query($sql) or die($sql.$conex->error);
						
						$sqlRecord="INSERT INTO `cobro_conections_record` (`userid`,`record`,`date`,`normal`,`reason`,`dateAssing`,useridExec)
										VALUES
									(
										{$_POST['userid']},
										+28800,
										NOW(),
										4,
										'{$_POST['reason']}',
										'{$fechaEval}',
										$bkouserid);";
						$conex->query($sqlRecord) or die($sqlRecord.$conex->error);
						array(
							"success"	=> true,
							"id"		=> mysql_insert_id()
						);
					}
				}
			}
			else{
				$sqlRecord="INSERT INTO `cobro_conections_record` (`userid`,`record`,`date`,`normal`,`reason`,`dateAssing`,useridExec)
								VALUES
							(
								{$_POST['userid']},
								{$_POST['signo']}$seg,
								NOW(),
								4,
								'{$_POST['reason']}',
								'{$_POST['date']}',
								$bkouserid);";
				$conex->query($sqlRecord) or die($sqlRecord.$conex->error);
				array(
					"success"	=> true,
					"id"		=> mysql_insert_id()
				);
			}
		}
		break;
	}
?>