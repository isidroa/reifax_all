<?php
error_reporting ( E_ALL );
ini_set ( 'display_errors', '1' );
require_once ("../checkuser.php");
require_once ("../conexion.php");
$conex = conectar ( "xima" );
$bkouserid = $_SESSION ['bkouserid'];
$sql = NULL;
$option = filter_input ( INPUT_POST, 'option' );
//ACCIONES
switch ($option) {	
	// OBTENER LOS T�PICOS DE LOS INSTRUCTIVOS
	case "getUserTopics" :
		$sql = "SELECT itg.idItg, CONCAT(itg.topics,' (',it.instructiveType,')') AS topics, (SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userId= {$bkouserid} and ir.instructiveId= i.instructiveId) as dateReaded
				FROM instructives i		
				INNER JOIN instructives_type it ON i.instructiveTypeId = it.instructivesTypeId
				INNER JOIN instructives_topics ito ON ito.instructiveId = i.instructiveId
				INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
				WHERE i.isStatus = 1				 
				GROUP BY itg.idItg
				ORDER BY topics ASC";
		$res = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$respuesta = array(array('idItg' => 0,'topics' => 'All'));
		while ( $r = mysql_fetch_object ( $res ) ) {
			array_push ( $respuesta, array('idItg' => $r->idItg,'topics' => ucwords(strtolower($r->topics))));
		}
		echo json_encode(array('data' => $respuesta));
	break;	
	// OBTENER GRUPOS
	case "getuserGroups" :
		$sql = "SELECT g.idgroup,g.group FROM groups_users gu INNER JOIN groups g ON gu.idgroup = g.idgroup
		WHERE userid ={$bkouserid}
		GROUP BY gu.idgroup";
		$res = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$respuesta = array(array('idgroup' => 0,'group' => 'All'));
		while ($r = mysql_fetch_object ( $res ) ) {
			array_push ($respuesta, array('idgroup' => $r->idgroup,'group' =>  ucwords(strtolower($r->group))));
		}
		echo json_encode (array('data' => $respuesta));
	break;	
	// OBTENER USUARIOS QUE APRUEBAN LOS INSTRUCTIVOS
	case "getUsersInstructive" :
		$sql = "SELECT xu.userid, concat(xu.NAME, ' ' ,xu.SURNAME,' (',xu.USERID,')') as username
				FROM instructional i
				INNER JOIN ximausrs xu
				ON  i.requested = xu.userid
				GROUP BY xu.userid";
		$res = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$respuesta = array(array ('userid' => 0, 'username' => 'ALL'));
		while ( $r = mysql_fetch_object ( $res ) ) {
			array_push($respuesta,array('userid' => $r->userid,'username' => $r->username));
		}
		echo json_encode(array('data' => $respuesta));
	break;	
	//T�PICOS POR GRUPOS
	case "getTopicsGroup":
		$data = array();
		$sql="SELECT * FROM instructives_topics_groups";
		$result=mysql_query ($sql) or die ($sql . mysql_error ());		
		while ($row=mysql_fetch_assoc($result)){
			array_push($data,$row);
		}
		echo '({"total":"'.count($data).'","results":'.json_encode($data).'})';
	break;
	// LISTADO DE INSTRUCTIVOS
	case "getInstructives" :
		//VARIABLES A UTILIZAR
		$idType = 0;
		$idTopic = 0;
		$mostrarArray = array();
		$excluirArray = array();
		$data = array ();
		$dataSingle = array ();
		$canidad = 0;
		// USUARIOS QUE APRUEBAN LOS INSTRUCTIVOS
		$sql = "SELECT userId FROM instructives_user_approver WHERE userId = {$bkouserid}";		
		$apruebaRow = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$rowAprueba = mysql_fetch_object ( $apruebaRow );
		
		$apruebaInstructivo = isset($rowAprueba->userId) ? $rowAprueba->userId : NULL ;
		if ($apruebaInstructivo != NULL) {
			$aprueba = 1;
		} else {
			$aprueba = 0;
		}
		// FILTROS DE BUSQUEDAS ENVIADOS DESDE LA VISTA
		if (isset ( $_POST ['idtype'] ) and $_POST ['idtype'] > 0) {
			$idType = ( int ) $_POST ['idtype'];
			$filtro = "AND i.instructiveTypeId = $idType ";
		}else{
			$filtro = "";
		}
		
		if (isset ( $_POST ['idtopic'] ) and $_POST ['idtopic'] > 0) {
			$idTopic = ( int ) $_POST ['idtopic'];
			$whereTopics = "AND itg.idItg = $idTopic ";
		}else{
			$whereTopics = "";
		}
		// GRUPOS
		$sql = "SELECT g.idgroup FROM groups_users gu INNER JOIN groups g ON gu.idgroup = g.idgroup
						  WHERE userid = {$bkouserid} GROUP BY gu.idgroup";
		$mostrarResult = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		
		while ( $row = mysql_fetch_object ( $mostrarResult ) ) {
			array_push($mostrarArray,$row->idgroup);
		}
		
		$mostrar = implode (',',$mostrarArray);
		if ($mostrar != NULL || !empty($mostrar)) {
			$where = " AND idgroup IN ($mostrar)";
		} else {
			$where = "";
		}		
		// OBTENER USUARIOS EXCLUIDOS
		$sql = "SELECT instructiveId FROM instructives_exclude ie WHERE ie.userId= {$bkouserid}
								group by instructiveExcludeId";		
		$excluirResult = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		while ( $row = mysql_fetch_object ( $excluirResult ) ) {
			array_push($excluirArray,$row->instructiveId);
		}
		
		$excluidos = implode ( ',', $excluirArray );
		if ($excluidos != NULL || !empty($excluidos)) {
			$excluir = " AND i.instructiveId NOT IN ($excluidos)";
		} else {
			$excluir = "";
		}
		
		// INSTRUCTIVOS
		$sql ="SELECT i.instructiveId, i.title, i.description, i.creatorUserId, i.creationDate, i.approverUserId,g.idGroup,g.group,ito.idItg,i.isStatus, itg.topics, it.instructiveType,
				(SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')')
				FROM ximausrs x1 where x1.userid=i.creatorUserId) as userCreator,
				(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userid= {$bkouserid} and ir.instructiveId= i.instructiveId) as dateReaded
				FROM instructives i
				INNER JOIN groups g ON i.instructiveTypeId = g.idGroup
				INNER JOIN instructives_type it ON i.instructiveTypeId = it.instructivesTypeId
				INNER JOIN instructives_topics ito ON ito.instructiveId = i.instructiveId
				INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
				WHERE i.isStatus = 1
				$where
				$excluir
				$filtro
				$whereTopics";
		$result = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		while ( $row = mysql_fetch_object ( $result ) ) {	
			$row->description = substr (( strip_tags ( $row->description ) ), 0, 100 ) . '...'; // Acortar cadena
			if ($row->dateReaded == NULL) {
				$row->dateReaded = 'UnRead';
			} else {
				$row->dateReaded = $row->dateReaded;
			}
			$row->userSession = $bkouserid;
			$row->cabecera = $row->instructiveType . "-" . $row->topics;
			$row->aprueba = $aprueba;
			array_push($data,$row);
		}
		//INSTRUCTIVOS SINGLES
		$sql="SELECT i.instructiveId, i.title, i.description, i.creatorUserId, i.creationDate,i.approverUserId,i.isStatus,i.instructiveTypeId, itg.topics, it.instructiveType,
				(SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')')  FROM ximausrs x1 where x1.userid=i.creatorUserId) as userCreator,
				(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userid= {$bkouserid} and ir.instructiveId= i.instructiveId) as dateReaded
				FROM instructives i
				INNER JOIN instructives_single isi ON i.instructiveId = isi.instructiveId
				INNER JOIN groups g ON i.instructiveTypeId = g.idgroup
				INNER JOIN instructives_type it ON i.instructiveTypeId = it.instructivesTypeId
				INNER JOIN instructives_topics ito ON ito.instructiveId = i.instructiveId
				INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
				WHERE i.isStatus = 1
				AND isi.userId= {$bkouserid}
				AND i.instructiveTypeId = 6
				$whereTopics ";
		$resultSingle = mysql_query ( $sql ) or die ( $sql . mysql_error () );		
		while ( $rowSingle = mysql_fetch_object ( $resultSingle ) ) {
			$rowSingle->description = substr (strip_tags ( $rowSingle->description ) , 0, 100 ) . '...'; // Acortar cadena
			if ($rowSingle->dateReaded == NULL) {
				$rowSingle->dateReaded = 'UnRead';				
			} else {
				$rowSingle->dateReaded = $rowSingle->dateReaded;
			}
			$rowSingle->grupo = "Single";
			$rowSingle->userSession = $bkouserid;
			$rowSingle->cabecera = $rowSingle->instructiveType . "-" . $rowSingle->topics;
			$rowSingle->aprueba = $aprueba;
			array_push($dataSingle,$rowSingle);
		}		
		//INSTRUCTIVOS PROPIOS		
		$sql = "SELECT i.instructiveId, i.title, i.description, i.creatorUserId, i.creationDate,i.approverUserId,i.isStatus,i.instructiveTypeId, itg.topics, it.instructiveType,
				(SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')')  FROM ximausrs x1 where x1.userid=i.creatorUserId) as userCreator,
				(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userid= {$bkouserid} and ir.instructiveId= i.instructiveId) as dateReaded
				FROM instructives i
				INNER JOIN groups g ON i.instructiveTypeId = g.idGroup
				INNER JOIN instructives_type it ON i.instructiveTypeId = it.instructivesTypeId
				INNER JOIN instructives_topics ito ON ito.instructiveId = i.instructiveId
				INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
				WHERE creatorUserId = {$bkouserid} and i.isStatus = 0";
		$resultPropios = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$dataPropios = array ();
		while ( $rowPropios = mysql_fetch_object ( $resultPropios ) ) {
			$rowPropios->description = substr (( strip_tags ( $rowPropios->description ) ), 0, 100 ) . '...'; // Acortar cadena
			$rowPropios->userSession = $bkouserid;
			$rowPropios->cabecera = $rowPropios->instructiveType . "-" . $rowPropios->topics;
			$rowPropios->aprueba = $aprueba;			
			array_push($dataPropios,$rowPropios);
		}		
		$dataAll = array_merge ( $data, $dataSingle, $dataPropios ); //
		$cantidad = count($dataAll);
		echo '({"total":"' . $cantidad . '","results":' . json_encode ( $dataAll ) . '})';		
	break;
	//VER DETALLE
	case "instructiveView":
		$destino = array();
		$instructive_id = (int)trim($_POST['instructive_id']);
		$sql= "SELECT e.instructiveId, e.title, e.description,e.instructiveTypeId, it.instructiveType, e.creatorUserId, 
					e.creationDate, e.approverUserId, e.approvalDate, e.isStatus, ito.idItg,itg.topics,
					(SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')') FROM ximausrs x1 where x1.userid=e.approverUserId) as userApproved,
 					(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userId= {$bkouserid} and ir.instructiveId= e.instructiveId) as dateReaded
					FROM instructives e
					INNER JOIN instructives_type it ON e.instructiveTypeId = it.instructivesTypeId
					INNER JOIN instructives_topics ito ON ito.instructiveId = e.instructiveId
               		INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
					WHERE e.instructiveId= $instructive_id ";

			$result=mysql_query ($sql) or die ($sql.mysql_error ());
			$row=mysql_fetch_object($result);
			$tipo_instructivo = $row->instructiveTypeId;
			//$usuarioc = $row->creator_userid;
			//if($usuarioc == $bkouserid){$edita =1; }else{ $edita = "NO"; }
			if(isset($tipo_instructivo) AND $tipo_instructivo == 6 )
			{ //consulto todos los instructive single where  instructive_id = $instructive_id
				
				$sql = "SELECT e.instructiveId, e.userId, e.isStatus, (SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')')
						FROM ximausrs x1 WHERE x1.userId=e.userId) as usuarioFullName
						FROM instructives_single e
						WHERE e.instructiveId= $instructive_id";				
				$res = mysql_query($sql) or die ($sql . mysql_error());
			    while($r=mysql_fetch_object($res))
			    {
			        array_push($destino, $r->usuarioFullName); // VARIOS USUARIOS
			    }
			}
			else
			{
				$sql = "SELECT * FROM instructives_type WHERE instructivesTypeId = $row->instructiveTypeId";
				$tiposResult = mysql_query ($sql) or die ($sql . mysql_error ());
				$rowTipos = mysql_fetch_object($tiposResult);
				$destino = "GRUPO: ".$rowTipos->instructiveType;
			}
			//OBTENER SI SE PUEDE APROVAR
			$sql = "SELECT userId FROM instructives_user_approver WHERE userId = {$bkouserid}";
			$apruebaRow = mysql_query($sql) or die ($sql . mysql_error ());
			$rowAprueba = mysql_fetch_object($apruebaRow);
			$apruebaInstructivo = (isset($rowAprueba->userId)) ? $rowAprueba->userId : NULL;
			if($apruebaInstructivo != NULL){ 
				$aprueba = 1;
			}
			else{
				$aprueba = 0;
			}
				$info = array(
				'success' => true,
				'msg' => 'Operation succesfully ',
				'title' => $row->title,
				'description' => $row->description,
				'asignado' => $destino,
				'isStatus' => $row->isStatus,
				'instructiveTypeId' => $row->instructiveTypeId,
				'userSession' => $bkouserid,
				'instructiveType' => $row->instructiveType,
				'idTopic' => $row->idItg,
				'topics' => $row->topics,
				'aprueba' => $aprueba,
				'dateReaded' => $row->dateReaded
			);
			echo json_encode($info);
	break;	

	//INSTRUCTIVO LEIDO
	case "instructiveRead":
		$instructive_id=(int)trim($_POST['instructive_id']);
		$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed) 
			    values ($instructive_id,{$bkouserid},NOW(),1)";
				mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
		echo "{success:true, msg:'Operation succesfully'}";
	break;
	//AGREGAR NUEVO TOPICO 
	case "groupsTopic":
		$topics = $_POST["topics"];
		$idGroup = (int)trim($_POST['idgroup']);
       	$sql="INSERT INTO instructives_topics_groups(instructivesTypeId,topics)
				VALUES ($idGroup,'$topics')";
		mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
	    echo "{success:true, msg:'Operation succesfully'}";
	break;
	//CREAR NUEVO
	case "instructiveAdd":
		$title = trim($_POST['title']);
		$instructiveTypeId = (int)trim($_POST['instructives_type_id']);
		$description = trim(str_replace('"','\"',str_replace("'","\'",$_POST['description'])));
		$arregloUsers = preg_split("/,/",$_POST['users_single']);
		$idItg =(int) trim($_POST["idTopic"]);
		$sql="INSERT INTO instructives (title, description, instructiveTypeId,creatorUserId,creationDate)
				VALUES ('$title','$description',$instructiveTypeId,$bkouserid,NOW())";		
		mysql_query($sql) or die("{success: false, msg: '".$sql.mysql_error()."' }");
		$ultimoId =  mysql_insert_id();
		//REGISTRAR TOPICOS POR PERMISO		
       	$sql="INSERT INTO instructives_topics (instructiveId,idItg)
			  VALUES ($ultimoId,$idItg)";
		mysql_query($sql) or die("{success: false, msg: \"".$sql.mysql_error()."\" }");
        //ENVIOS INDIVIDUALES
		if($instructiveTypeId == 6 AND !empty($arregloUsers))
		{
			array_shift($arregloUsers);
            foreach ($arregloUsers as $idUser)
            {
              	$query="INSERT INTO instructives_single (instructiveId,userId)
						VALUES ($ultimoId,$idUser)";
				mysql_query($query) or die("{success: false, msg: \"".$sql.mysql_error()."\" }");
            }
		}
		$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed)
				values ($ultimoId,{$bkouserid},NOW(),1)";
		mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");		
		echo "{success:true,msg: 'Insertion  successfully' }";
	break;
	//EDICION Y APROVACI�N DEL INSTRUCTIVO
	case "instructiveEdit":
		$instructiveId = (int) trim($_POST['instructive_id']);
		$accion = (int) $_POST['action'];
		$title = $_POST['title'];
		$instructiveTypeId=(int) trim($_POST['instructives_type_id']);
		$isStatus = (isset($_POST['istatus']))?(int) trim($_POST['istatus']): NULL;
		$idItg = (int) trim($_POST['idtinsa']);
		$description=trim(str_replace('"','\"',str_replace("'","\'",$_POST['description'])));
		$exito = false;
		
		if(isset($isStatus) AND $isStatus == 1)
		{
			$aprobar =", isStatus = $isStatus , approverUserId = $bkouserid , approvalDate = NOW()";
		}
		else{
			$aprobar ="";
		}
		if(is_numeric($instructiveTypeId))
		{
			$groupTopic = ",instructiveTypeId = $instructiveTypeId ";
		}else{
			$groupTopic = "";
		}		
		//ACCIONES A REALIZAR 
		switch($accion){
			//INSTRUCTIVO CREADO Y SIN APROBACI�N
			case 0:
				$sql="UPDATE instructives
				SET description = '$description',title = '$title' $groupTopic $aprobar
				WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				if(is_numeric($instructiveTypeId) and is_numeric($idItg))
				{
					$sql="UPDATE instructives_topics
					SET idItg = $idItg
					WHERE instructiveId = $instructiveId";
					$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				}				
				$sql="SELECT COUNT(instructivesReadedId) as resultado FROM instructives_readed 
					  WHERE instructiveId = $instructiveId and userId = $bkouserid";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				$row = mysql_fetch_object($result);
				$count = (int)$row->resultado;
				if($count < 1){
					$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed)
					values ($instructiveId,{$bkouserid},NOW(),1)";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}else{
					$sql = "UPDATE instructives_readed SET dateReaded=NOW(), readed=1 
							WHERE instructiveId = $instructiveId and userId = $bkouserid";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}				
			break;
			
			//SOLAMENTE ACTUALIZACI�N DEL CONTENIDO APROBADO
			case 1:
				$sql="UPDATE instructives
				SET description = '$description',title = '$title' $groupTopic 
				WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				$sql="SELECT COUNT(instructivesReadedId) as resultado FROM instructives_readed
				WHERE instructiveId = $instructiveId and userId = $bkouserid";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				$row = mysql_fetch_object($result);
				$count = (int)$row->resultado;
				if($count < 1){
					$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed)
					values ($instructiveId,{$bkouserid},NOW(),1)";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}else{
					$sql = "UPDATE instructives_readed SET dateReaded=NOW(), readed=1
					WHERE instructiveId = $instructiveId and userId = $bkouserid";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}
			break;
			
			//MANDAR A LEER A TODOS LOS USUARIOS DE NUEVO EL INSTRUCTIVO
			case 2:
				$sql="UPDATE instructives
				SET description = '$description',title = '$title' $groupTopic 
				WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");				
				//SE ESTABLECE QUE LOS USUARIOS LEAN DE NUEVO EL INSTRUCTIVO
				$sql="DELETE FROM instructives_readed WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				//SE MARCA COMO LEIDO EL CREADOR DEL INSTRUCTIVO
				$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed)
				values ($instructiveId,{$bkouserid},NOW(),1)";
				mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
				$exito = true;
			break;

			//REAPROBACION DEL INSTRUCTIVO			
			case 3:
				$sql="UPDATE instructives
				SET description = '$description',title = '$title' $groupTopic , approverUserId = NULL, approvalDate = NULL, isStatus = 0 
				WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				if(is_numeric($instructiveTypeId) and is_numeric($idItg))
				{
					$sql="UPDATE instructives_topics
					SET idItg = $idItg
					WHERE instructiveId = $instructiveId";
					$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				}				
				
				$sql="DELETE FROM instructives_readed WHERE instructiveId = $instructiveId";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				
				
				$sql="SELECT COUNT(instructivesReadedId) as resultado FROM instructives_readed
				WHERE instructiveId = $instructiveId and userId = $bkouserid";
				$result = mysql_query($sql) or die("{success:false, msg:\"".$sql."<br>".mysql_error()."\"}");
				$row = mysql_fetch_object($result);
				$count = (int)$row->resultado;
				if($count < 1){
					$sql = "INSERT INTO instructives_readed (instructiveId,userId,dateReaded,readed)
					values ($instructiveId,{$bkouserid},NOW(),1)";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}else{
					$sql = "UPDATE instructives_readed SET dateReaded=NOW(), readed=1
					WHERE instructiveId = $instructiveId and userId = $bkouserid";
					mysql_query($sql) or die("{success:false, msg:'".$sql."<br>".mysql_error()."'}");
					$exito = true;
				}			
				$exito = true;
			break;	
		}
		if($exito==true){
			echo "{success:true, msg:'Update successfully'}";
		}
	break;	
}
?>