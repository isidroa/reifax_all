<?php
error_reporting ( E_ALL );
ini_set ( 'display_errors', '1' );
require_once ("../checkuser.php");
require_once ("../conexion.php");
$conex = conectar ( "xima" );
$bkouserid = $_SESSION ['bkouserid'];
$sql = NULL;
$option = filter_input ( INPUT_POST, 'option' );
//ACCIONES
switch ($option) {
	//LISTADO DE TODOS LOS INSTRUCTIVOS ALMACENADOS
	case "getPrivilegeInstructional" :
		$start = isset ( $_POST ['start'] ) ? (int)$_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? (int)$_POST ['limit'] : 50;
		$data = array ();	
		
		if (isset ( $_POST ['idtype'] ) && $_POST ['idtype'] != 10) {
			$tip = (int) trim($_POST ['idtype']);
			$where = "WHERE e.instructiveTypeId = $tip";
		} else {
			$where = "";
		}
			
		 $sql = "SELECT e.instructiveId, e.title, e.description,e.instructiveTypeId, ito.idItg, itg.topics, it.instructiveType,
				e.creatorUserId, e.creationDate, e.approverUserId, e.approvalDate, e.isStatus,
				concat(x.name,' ', x.surname) as userCreator,(SELECT concat(x1.NAME, ' ',x1.SURNAME, ' (',x1.USERID,')')
				FROM ximausrs x1 where x1.userid=e.approverUserId) as userApproved
				FROM instructives e
				INNER JOIN ximausrs x ON e.creatorUserId=x.USERID
                INNER JOIN instructives_type it ON e.instructiveTypeId = it.instructivesTypeId
                INNER JOIN instructives_topics ito ON ito.instructiveId = e.instructiveId
                INNER JOIN instructives_topics_groups itg ON ito.idItg = itg.idItg
		 		$where
		 		LIMIT $start,$limit";		 	
		 $result = mysql_query ( $sql ) or die ( $sql . mysql_error () );		 	
		 while ( $row = mysql_fetch_object ( $result ) ) {
		 	$row->description = substr ( strip_tags ( $row->description ), 0, 100 ) . '...'; // Acortar cadena
		 	$estadoInstructivo = ( int ) $row->isStatus;
		 	if ($estadoInstructivo == 0) {
		 		$row->isStatus = "N";
		 		$row->userApproved = "Not approved";
		 		$row->approvalDate = "Not approved";
		 	}
		 	$row->cabecera = $row->instructiveType . "-" . $row->topics;
		 	array_push($data,$row);
		 }
		 echo '({"total":"' . count($data) . '","results":' . json_encode ( $data ) . '})';
	break;
	//VER DETALLE DEL INSTRUCTIVO
	case "getInstructiveDetails":
		$data = array ();		
		$excluidos = array();
		$instructiveId = ( int ) trim ( $_POST ["instructive_id"] ); 
		$sql = "SELECT instructiveId,instructiveTypeId FROM instructives e WHERE e.instructiveId=$instructiveId";
		$tipoInstructivoResult = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		$rowTipoInstructivo = mysql_fetch_object ( $tipoInstructivoResult );
		$tipoInstructivo = (int)$rowTipoInstructivo->instructiveTypeId;

		$sql = "SELECT excludeUserId FROM instructives_exclude WHERE  instructiveId = $instructiveId";
		$excluirResult = mysql_query ( $sql ) or die ( $sql . mysql_error () );
		
		while ( $row = mysql_fetch_object ( $excluirResult ) ) {
			array_push($excluidos,$row->excludeUserId);
		}
		
		$excluidosArray = implode ( ',', $excluidos );

		if ($excluidosArray != NULL) {
			$and = "AND gu.userid NOT IN($excluidosArray)";
		} else {
			$and = "";
		}		
		if ($tipoInstructivo == 6) {
			$sql = "SELECT isi.instructiveId, isi.userId,(SELECT concat(x1.NAME, ' ',x1.SURNAME) FROM ximausrs x1 where x1.userid=isi.userId) as usuarioInstructivo,
					(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userId= isi.userId and ir.instructiveId = $instructiveId) as dateReaded
					FROM instructives_single isi
					INNER JOIN instructives i
					ON i.instructiveId = isi.instructiveId
					WHERE isi.instructiveId = $instructiveId ORDER BY usuarioInstructivo ASC";
			$result = mysql_query ( $sql ) or die ( $sql . mysql_error () );
			
			while ( $row = mysql_fetch_object ( $result ) ) {
				$row->instructiveId = $instructiveId;
				$row->group = "Single";
				$date_read = $row->dateReaded;
				if ($date_read == 0) {
					$row->rStatus = "Not Read";
					$row->rDateRead = "Not Read";
				} else {
					$row->rDateRead = $row->dateReaded;
					$row->rStatus = "Read";
				}
				array_push($data,$row);
			}
		}else {
			$sql = "SELECT gu.idgroups_users,gu.userid,gu.idgroup,g.idgroup,g.group,
					(SELECT concat(x1.NAME, ' ',x1.SURNAME) FROM ximausrs x1 where x1.userid=gu.userid) as usuarioInstructivo,
					(SELECT ir.dateReaded FROM instructives_readed ir WHERE ir.userId= gu.userid and ir.instructiveId= $instructiveId) as dateReaded
					FROM groups_users gu
					INNER JOIN groups g ON gu.idgroup = g.idgroup
					WHERE g.idgroup =$tipoInstructivo
					$and
					ORDER BY usuarioInstructivo ASC";			
			$resultAll = mysql_query ( $sql ) or die ( $sql . mysql_error () );
			while ( $rowAll = mysql_fetch_object ( $resultAll ) ) {				
				$rowAll->userId = $rowAll->userid;
				$rowAll->instructiveId = $instructiveId;
				$dateRead = $rowAll->dateReaded;
				$rowAll->group = ucwords(strtolower($rowAll->group));			
				
				if ($dateRead == NULL) {
					$rowAll->rStatus = "Not Read";
					$rowAll->rDateRead = "Not Read";
				}else {
					$rowAll->rDateRead = $rowAll->dateReaded;
					$rowAll->rStatus = "Read";
				}				
				array_push($data,$rowAll);
			}
		}				
		echo '({"total":"' . count($data) . '","results":' . json_encode ( $data ) . '})';
	break;
	//EXCLUSION DEL USUARIO
	case "excludeUser":
		$instructiveId = (int)trim($_POST['instructive_id']);
		$userId = (int)trim($_POST['userid']);	
		$sql="INSERT INTO xima.instructives_exclude (instructiveId,userId,dateExclude,excludeUserId)
				VALUES ($instructiveId,$bkouserid,NOW(),$userId)";
		mysql_query($sql) or die("{success: false, msg: \"".$sql.mysql_error()."\" }");
		echo "{success:true, msg:'Exclude successfully'}";
	break;
	//ELIMINACION DEL INSTRUCTIVO
	case "delete":	
		$id = (int) trim($_POST['id']);	
		$sql = "DELETE FROM instructives WHERE instructiveId = $id ";
		$result = mysql_query($sql) or die("{success: false, errors: { reason: \"".$sql.mysql_error()."\" }}");		
		echo "{success: true, errors: { reason: \"Delete succesfully\"}}";	
	break;
}
?>