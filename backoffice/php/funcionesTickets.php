<?php
include("conexion.php");
conectar('xima');
switch($_POST['opcion'])
{
	/*********************************
	*** LISTAR LOS TICKETS
	************************************/
	case ('listarTickets'):
		$start = isset($_POST['start'])?$_POST['start']:0; //posición a iniciar  
		$limit = isset($_POST['limit'])?$_POST['limit']:50; //número de registros
		$where="where 1=1";
		$where.= ($_POST['status']!='')?' AND c.status '.$_POST['status']:'';
		if(!empty($_POST['programer'])){
			if(is_numeric($_POST['programer'])){
				$where.= " AND (c.useridprogrammer={$_POST['programer']} 
				OR c.useridclient={$_POST['programer']} 
				OR  c.useridprogrammer={$_POST['programer']}
				OR  c.ticket={$_POST['programer']})";
			}
			else{
				$where.= " AND (u.name like '%{$_POST['programer']}%' OR u.surname like '%{$_POST['programer']}%' OR
				cu.name like '%{$_POST['programer']}%' OR cu.surname like '%{$_POST['programer']}%'  OR
				p.name like '%{$_POST['programer']}%' OR p.surname like '%{$_POST['programer']}%' OR
				u.email like '%{$_POST['programer']}%')";
			}
		}
		if($_POST['error']!=''){
			$where.=' AND c.errortype="'.$_POST['error'].'"';
		}
		else {
			if ($_POST['typeBack']=='Suggestions') {
					$where.=' AND c.errortype="Suggestions"';
			} else {
					$where.=' AND c.errortype!="Suggestions"';
			}
			
		}
		$sort= ($_POST['sort']!='')?$_POST['sort']:'c.idcs';
		$dir= ($_POST['dir']!='')?$_POST['dir']:'desc';
		$sql='select  c.idcs,
			c.useridclient,concat( u.name ," " ,u.surname) cliente, c.useridcustomer, concat( cu.name ," " ,cu.surname) customer, c.useridprogrammer,concat(p.name ," " ,p.surname) programador,
			u.HOMETELEPHONE phone, u.MOBILETELEPHONE mobile, u.email ,
			c.id_product,pr.name as productName, c.browser, c.os, 
			(msg.id_msg) msg_cliente, (msg_sys.id_msg) msg_system, 
			c.state, co.County, c.idcounty, c.menutab, c.submenutab,c.proptype, c.errortype, c.search, c.errordescription,
			c.`status`, c.datel, c.dated, c.datef, c.datefc datec, c.errortype2, c.soldescription, c.ticket, c.source ,
			c.date_check
			from `xima`.`customerservices` c 
				LEFT JOIN xima.ximausrs u ON c.useridclient=u.userid 
				LEFT JOIN xima.usr_producto pr ON pr.idproducto=c.id_product 
				LEFT JOIN xima.ximausrs cu ON c.useridcustomer=cu.userid 
				LEFT JOIN xima.ximausrs p ON c.useridprogrammer=p.userid 
				LEFT JOIN xima.lscounty co ON c.idcounty=co.IdCounty 
    left JOIN (select id_ticket, status_msg_back,id_msg from xima.customerservice_msg where status_msg_back=1 group by id_ticket) msg ON msg.id_ticket=c.idcs AND msg.status_msg_back=1 
    left JOIN (select id_ticket, status_msg_system,id_msg from xima.customerservice_msg where status_msg_system=1 group by id_ticket) msg_sys ON msg_sys.id_ticket=c.idcs AND msg_sys.status_msg_system=1 

			'.$where.'
			order by '.$sort.' '.$dir
	;
		$result= mysql_query($sql);
		$data=array();
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> array_splice($data,$start,$limit),
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** OBTENER UN TICKET
	************************************/
	case ('getTicket'):
		session_start();
		$sql='select  c.idcs,
			c.useridclient,concat( u.name ," " ,u.surname) cliente, c.useridcustomer, concat( cu.name ," " ,cu.surname) customer, c.useridprogrammer,concat(p.name ," " ,p.surname) programador,
			u.HOMETELEPHONE phone, u.MOBILETELEPHONE mobile, u.email ,
			c.id_product,pr.name as productName, c.browser, c.os, 
			(msg.id_msg) msg_cliente, (msg_sys.id_msg) msg_system, 
			c.state, co.County, c.idcounty, c.menutab, c.submenutab,c.proptype, c.errortype, c.search, c.errordescription,
			c.`status`, c.datel, c.dated, c.datef, c.datefc datec, c.errortype2, c.soldescription, c.ticket, c.source, c.locked,
			c.date_check
			from `xima`.`customerservices` c 
				LEFT JOIN xima.ximausrs u ON c.useridclient=u.userid 
				LEFT JOIN xima.usr_producto pr ON pr.idproducto=c.id_product 
				LEFT JOIN xima.ximausrs cu ON c.useridcustomer=cu.userid 
				LEFT JOIN xima.ximausrs p ON c.useridprogrammer=p.userid 
				LEFT JOIN xima.lscounty co ON c.idcounty=co.IdCounty 
    left JOIN (select id_ticket, status_msg_back,id_msg from xima.customerservice_msg where status_msg_back=1 group by id_ticket) msg ON msg.id_ticket=c.idcs AND msg.status_msg_back=1 
    left JOIN (select id_ticket, status_msg_system,id_msg from xima.customerservice_msg where status_msg_system=1 group by id_ticket) msg_sys ON msg_sys.id_ticket=c.idcs AND msg_sys.status_msg_system=1 

			WHERE c.idcs='.$_POST['id'].' LIMIT 1'
	;
		$result=mysql_query($sql);
		$data=mysql_fetch_assoc($result);
		$errorP=mysql_error();
		
		if($data['locked']=='' || $data['locked']==NULL || $data['locked']==$_SESSION['bkouserid']){
			$sqllook='update customerservices set locked='.$_SESSION['bkouserid'].' WHERE idcs='.$_POST['id'];
			mysql_query($sqllook);
			$data['locked']=NULL;
		}
		else{
			$aux='SELECT concat(name ," " ,surname) customer FROM ximausrs WHERE userid='.$data['locked'];
			$aux2=mysql_query($aux);
			$aux3=mysql_fetch_assoc($aux2);
			$data['editing']=$aux3['customer'];
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> $data,
			'sql' => $sql,
			'sql2' => $sqllook,
			'mysql Error' => $errorP, 
			'total' => count($data)
		));
	break;
	/*********************************
	*** CAMBIAR USUARIO
	************************************/
	case ('changeUser'):
		$sql='UPDATE customerservices SET useridclient='.$_POST['client'].' where idcs='.$_POST['ticket'].';';
		$result= mysql_query($sql);
		$sql='SELECT concat(name ," " ,surname) customer, HOMETELEPHONE, MOBILETELEPHONE, email FROM ximausrs WHERE userid='.$_POST['client'];
		$result= mysql_query($sql);
		$data=mysql_fetch_assoc($result);
		echo json_encode(array(
			'success'	=> true,
			'idCliente' => $_POST['client'],
			'home' => $data['HOMETELEPHONE'],
			'mobile' => $data['MOBILETELEPHONE'],
			'email' => $data['email'],
			'name' => $data['customer'],
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** LISTAR IMAGENES
	************************************/
	case ('listarImg'):
		$sql='SELECT url_archivo img, id_customerservices_img id FROM `xima`.customerservices_img where id_ticket='.$_POST['id'].';';
		$result= mysql_query($sql);
		$data=array();
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> $data,
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** Delete IMAGENES
	************************************/
	case ('removeImg'):
		$sql='SELECT uri_archivo img FROM `xima`.customerservices_img where id_customerservices_img='.$_POST['img'].';';
		$result= mysql_query($sql);
		$aux=mysql_fetch_assoc($result);
		unlink($aux['img']);
		$sql='DELETE FROM customerservices_img where id_customerservices_img='.$_POST['img'].';';
		$result= mysql_query($sql);
		echo json_encode(array(
			'success'	=> true,
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** LISTAR MENSAJES
	************************************/
	case ('listarMsg'):
		$sql='SELECT concat( u.name ," " ,u.surname) `user`, date_msg dateMsg,texto_msg msg FROM `xima`.`customerservice_msg` m LEFT JOIN `xima`.`ximausrs` u on u.userid=m.remitiente_msg where m.id_ticket='.$_POST['id'].';';$result= mysql_query($sql);
		$data=array();
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> $data,
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** 	MARCAR MENSAJES COMO LEIDOS
	************************************/
	case ('marcarMsg'):
		$sql='UPDATE `xima`.`customerservice_msg`
			SET
			`status_msg_back` = 2
			WHERE id_ticket=
			'.$_POST['id'].';';
		$result= mysql_query($sql);
		echo json_encode(array(
			'success'	=> true,
			'sql' => $sql,
			'mysql Error' => mysql_error()
		));
	break;
	
	/*********************************
	*** AGREGAR UN NUEVO MENSAJES
	************************************/
	
	case ('newMsg'):
		session_start();
		$sql='INSERT INTO `customerservice_msg`
			(`remitiente_msg`,
			`date_msg`,
			`texto_msg`,
			`id_ticket`,
			`status_msg_back`) VALUES 
			('.$_SESSION['bkouserid'].', NOW(), "'.mysql_real_escape_string($_POST['Msg']).'", '.$_POST['id'].',2)
		';
		mysql_query($sql);
		if(mysql_affected_rows() > 0)
		{
			$aux=mysql_query('select u.email, concat( u.name ," " ,u.surname) cliente, concat( cu.name ," " ,cu.surname) customer from
xima.customerservices c left join xima.ximausrs u on c.useridclient=u.userid LEFT JOIN xima.ximausrs cu ON cu.userid='.$_SESSION['bkouserid'].' where c.idcs='.$_POST['id']);
			$data=mysql_fetch_assoc($aux);
			require("../../resources/php/phpmailer/class.phpmailer.php");
					$email = new PHPMailer();
					$email->Host = "www.reifax.com";
					$email->From = 'Update-Ticket-'.$_POST['id'].'-@reifax.com';
					$email->FromName = 'reifax.com';
					$email->Subject = 'New Msg for Ticket'.$_POST['id'];
					$email->AddBCC();
					$email->AddAddress($data['email']);
					$body ='
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
						<html xmlns="http://www.w3.org/1999/xhtml">
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>Email Support</title>
						</head>
						<body>
							<p>
								El usuario '.$data['cliente'].' ah realizado un nuevo comentario en el ticket:'.$_POST['id'].'.
							</p>
							<p>
								puedes leer el comentario en el mysettings de tu cuenta de <a href="http://www.reifax.com">www.reifax.com</a> en el area de My Tickets
							</p>
						</body>
						</html>';
					$email->Body = $body;
					$email->IsHTML(true);
					$email->Send();
			
			
			echo json_encode(array(
				'success'	=> true,
				'id' => $_POST['id'],
				'sql' => $sql,
				'mysql Error' => mysql_error()
			));
		}
		else
		{
			echo json_encode(array(
				'success'	=> false,
				'id' => $_POST['id'],
				'sql' => $sql,
				'mysql Error' => mysql_error()
			));
		}
		
	break;
	/*********************************
	*** ACTUALIZAR TICKET
	************************************/
	case ('updateTicket'):
	$error=mysql_real_escape_string($_POST['errordescription']);
	$solution=mysql_real_escape_string($_POST['solution']);
	$escape=array('\x95');
	$error=
	$sql='
		update customerservices set 
			status="'.$_POST['status'].'"
			'.(($_POST['customer']!=0)?', useridcustomer='.abs($_POST['customer']): '').'
			'.(($_POST['programer']!=0)?', useridprogrammer='.abs($_POST['programer']): '').'
			'.(($_POST['datel']!='')?', datel="'.$_POST['datel'].' 00:00:00"' : '') .'
			'.(($_POST['dated']!='')?', dated="'.$_POST['dated'].' 00:00:00"' : '') .'
			'.(($_POST['datef']!='')?', datef="'.$_POST['datef'].' 00:00:00"' : '') .'
			'.(($_POST['datec']!='')?', datefc="'.$_POST['datec'].' 00:00:00"' : '') .'
			'.(($_POST['date_check']!='')?', date_check="'.$_POST['date_check'].' 00:00:00"' : '') .'
			'.(($solution!='')?', soldescription="'.$solution.'"' : '') .'
			,errordescription="'.$error.'" 
			,errortype="'.$_POST['errortype'].'"  
			'.(($_POST['id_product']!=0)?', id_product='.abs($_POST['id_product']): '').'
			,state="'.$_POST['state'].'"
			'.(($_POST['County']!=0)?', idcounty='.abs($_POST['County']): '').'
			,menutab="'.$_POST['menutab'].'"
			,submenutab="'.$_POST['submenutab'].'"
			,proptype="'.$_POST['proptype'].'"
			,errortype="'.$_POST['errortype'].'"
			,`errortype2`="'.$_POST['errortype2'].'"
			
			,`browser`="'.$_POST['browser'].'"
			,`os`="'.$_POST['os'].'"
			,search="'.$_POST['search'].'"
		where idcs='.$_POST['id'].'
	';
	mysql_query($sql);
	$info =  mysql_info(); 
	$info = substr($info, strpos($info, 'Changed:'), strpos($info, 'W') - strpos($info, 'Changed:') - 2); 
	$info = substr($info, strpos($info, ' ') + 1); 
	if($info!='')
	{
		$sqllook='update customerservices set locked=NULL WHERE idcs='.$_POST['id'];
		mysql_query($sqllook);
		echo json_encode(array(
			'success' => true,
			'sql' => $sql,
			'mysqlError' => mysql_error()
		));
	}
	else
	{		
		echo json_encode(array(
			'success' => false,
			'sql' => $sql,
			'TextError' => $error,
			'mysqlError' => mysql_error()
		));
	}
	break;
	/*********************************
	*** GRABAR UN NUEVO TICKET
	************************************/
	case ('recordTicket'):
		$aux=mysql_query('select (ticket+1) as ticket from customerservices order by ticket desc limit 1');
		$aux2=mysql_fetch_assoc($aux);
		$sql='
		INSERT INTO `xima`.`customerservices`
			(
			
			'.(($_POST['client']!='NONE' && $_POST['client']!='')? '`useridclient`,' : '') .'
			'.(($_POST['customer']!='NONE' && $_POST['customer']!='')? '`useridcustomer`,'  : '') .'
			'.(($_POST['programer']!='NONE' && $_POST['programer']!='')? '`useridprogrammer`,' : '') .'
			`browser`,
			`os`,
			`state`,
			'.(($_POST['idcounty']!='')? '`idcounty`,': '') .'
			`menutab`,
			`submenutab`,
			`proptype`,
			`errortype`,
			`search`,
			`errordescription`,
			`status`,
			`datel`,
			`errortype2`,
			`date_check`,
			`ticket`,
			`source`)
			VALUES
			(
			'.(($_POST['client']!='NONE' && $_POST['client']!='')? abs($_POST['client']).',' : '') .'
			'.(($_POST['customer']!='NONE' && $_POST['customer']!='')? abs($_POST['customer']).','  : '') .'
			'.(($_POST['programer']!='NONE' && $_POST['programer']!='')? abs($_POST['programer']).',' : '') .'
			"'.$_POST['browser'].'",
			"'.$_POST['os'].'",
			"'.$_POST['state'].'",
			'.(($_POST['idcounty']!='') ? $_POST['idcounty'].',': '') .'
			"'.$_POST['menutab'].'",
			"'.$_POST['submenutab'].'",
			"'.$_POST['proptype'].'",
			"'.$_POST['errortype'].'",
			"'.$_POST['search'].'",
			"'.mysql_real_escape_string($_POST['errordescription']).'",
			"Open",
			NOW(),
			"'.$_POST['errortype2'].'",
			'.(($_POST['date_check']!='')?'"'.$_POST['date_check'].'"':'NULL').',
			'.$aux2['ticket'].',
			"'.$_POST['source'].'"
			);
		';
		mysql_query($sql);
		if(mysql_error()=='')
		{
			$ticket=mysql_insert_id ();
			foreach ($_FILES as $aux)
			{
				if(isset($aux))
				{
					$archivo=$aux['tmp_name'];
					$name=explode('.',$aux['name']);
					$arc=(rand(1,100).md5($_SERVER['REMOTE_ADDR'].date("Y m j h i s").$name[0])).'.'.$name[count($name)-1];
					$destinoF='../../imgTickets/'.$arc;
					$destinoL='/imgTickets/'.$arc;
					if (copy($archivo,$destinoF)) 
					{
						$sql= 'INSERT INTO `xima`.`customerservices_img`
							(
							`url_archivo`,
							`uri_archivo`,
							`id_ticket`)
							VALUES
							(
								"'.$destinoL.'",
								"'.$destinoF.'",
								'.$ticket.'
							)';
						$respuesta=mysql_query($sql);
						if(mysql_affected_rows()<1)
						{
							$errorImg=true;	
						}
						
					}
				}
			}
			echo json_encode(array(
				'success' => true,
				'sql' => $sql,
				'img' => $img,
				'insertId' => $aux2['ticket'],
				'imgError' => $errorImg
			));
		}
		else
		{		
			echo json_encode(array(
				'success' => false,
				'sql' => $sql,
				'mysqlError' => mysql_error()
			));
		}
	break;
	/*********************************
	*** BORRAR UN NUEVO TICKET
	************************************/
	case ('addImages'):
		$ticket=$_POST['ticket'];
		foreach ($_FILES as $aux)
		{
			if(isset($aux))
			{
				$archivo=$aux['tmp_name'];
				$name=explode('.',$aux['name']);
				$arc=(rand(1,100).md5($_SERVER['REMOTE_ADDR'].date("Y m j h i s").$name[0])).'.'.$name[count($name)-1];
				$destinoF='../../imgTickets/'.$arc;
				$destinoL='/imgTickets/'.$arc;
				if (copy($archivo,$destinoF)) 
				{
					$sql= 'INSERT INTO `xima`.`customerservices_img`
						(
						`url_archivo`,
						`uri_archivo`,
						`id_ticket`)
						VALUES
						(
							"'.$destinoL.'",
							"'.$destinoF.'",
							'.$ticket.'
						)';
					$respuesta=mysql_query($sql);
					if(mysql_affected_rows()<1)
					{
						$errorImg=true;	
					}
					
				}
			}
		}
		echo json_encode(array(
			'success' => true,
			'sql' => $sql,
			'img' => $img,
			'imgError' => $errorImg
		));
	break;
	/*********************************
	*** BORRAR UN NUEVO TICKET
	************************************/
	case ('deleteTicket'):
		$sql='delete from customerservices where idcs='.$_POST['id'];
		mysql_query($sql);
		if(mysql_affected_rows() > 0)
		{
			echo json_encode(array(
				'success'	=> true
			));
		}
		else
		{	
			echo json_encode(array(
				'success'	=> false
			));
		}
	break;
	/*********************************
	*** DESBLOQUER LO TICKES
	************************************/
	case ('unlookerTickes'):
		session_start();
		$sql='update customerservices set locked=NULL WHERE locked='.$_SESSION['bkouserid'];
		mysql_query($sql);
		echo $sql;
	break;
}
?>