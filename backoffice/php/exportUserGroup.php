<?php
include("conexion.php");
conectar('xima');
include("class.excel.writer.php");
$xls = new ExcelWriter();

$xls_int = array('type'=>'int');
$xls_date = array('type'=>'date');

function loadDataGrid($group){
	global $xls;
	$sql="SELECT * FROM xima.user_groups_featuresassing fa 
		JOIN
		 `user_groups_features` f ON fa.id_feature=f.id WHERE fa.id_assing={$group};";
	$result=mysql_query($sql);
	$xls->OpenRow();
	$xls->NewCell('userid',false,array('bold'=>true));
	$xls->NewCell('Name',false,array('bold'=>true));
	$xls->NewCell('Phone',false,array('bold'=>true));
	$xls->NewCell('Email',false,array('bold'=>true));
	$feature=array();
	while($dataCol=mysql_fetch_assoc($result)){
		array_push($feature,$dataCol['id_feature']);
		$xls->NewCell($dataCol['feature'],false,array('bold'=>true));
	}
	$xls->CloseRow();
	$sql="SELECT * FROM user_groups_assing ga JOIN `ximausrs` x ON x.userid=ga.userid WHERE ga.idgroup={$group} group by x.userid;";
	$result=mysql_query($sql);
	while($tem=mysql_fetch_assoc($result)){
		$xls->OpenRow();
		$xls->NewCell($tem['userid'],false);
		$xls->NewCell($tem['NAME'],false);
		$xls->NewCell($tem['HOMETELEPHONE'],false);
		$xls->NewCell($tem['EMAIL'],false);
		$sql="SELECT * FROM user_groups_featureuser WHERE id_user={$tem['id']}";
		$result2=mysql_query($sql) or die($sql.mysql_error());
		while($tem2 = mysql_fetch_assoc($result2)){
			$tem[$tem2['id_feature']]=1;
		}
		foreach($feature as $k => $v){
			if($tem[$v]){
				$xls->NewCell('Yes',false);
			}
			else{
				$xls->NewCell('No',false);
			}
		}
		$xls->CloseRow();
	}
}

$sql="SELECT * FROM user_groups;";
$result=mysql_query($sql);

while($group=mysql_fetch_assoc($result)){
	loadDataGrid($group['id']);
}
$xls->GetXLS();
?>