<?php
	require("conexion.php");
	$conex=conectar("xima");
	
	$query="SELECT DISTINCT SQL_CALC_FOUND_ROWS
					xima.ximausrs.userid id,
					xima.ximausrs.userid,
					concat(xima.ximausrs.name,' ',xima.ximausrs.surname) as groups,
					xima.contracts_custom.id as idfile,
					xima.contracts_custom.name as namefile,
					xima.contracts_custom.filename
				FROM xima.ximausrs
				LEFT JOIN (xima.status) ON (xima.ximausrs.idstatus=xima.status.idstatus)
				LEFT JOIN (xima.usertype) ON (xima.ximausrs.idusertype=xima.usertype.idusertype)
				LEFT JOIN (xima.f_frecuency) ON (xima.ximausrs.userid=xima.f_frecuency.userid)
				LEFT JOIN (xima.usernotes) ON (xima.ximausrs.userid=xima.usernotes.userid)
				LEFT JOIN (xima.creditcard) ON (xima.ximausrs.userid=xima.creditcard.userid)
				LEFT JOIN (xima.userterms) ON (xima.ximausrs.userid=xima.userterms.userid)
				LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
				LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
				LEFT JOIN (xima.permission) ON (xima.ximausrs.userid=xima.permission.userid)
				LEFT JOIN (xima.cobro_porcentajes) ON (xima.ximausrs.userid=xima.cobro_porcentajes.userid)
				LEFT JOIN (xima.usr_registertype) ON (xima.ximausrs.userid=xima.usr_registertype.userid)
				LEFT JOIN (xima.logsstatus) ON (xima.ximausrs.userid=xima.logsstatus.userid)
				LEFT JOIN (xima.logsusr_cobros) ON (xima.ximausrs.userid=xima.logsusr_cobros.userid)	
				LEFT JOIN (xima.contracts_custom) ON (xima.ximausrs.userid= xima.contracts_custom.userid)			
				WHERE 1=1 ANd xima.ximausrs.userid='2482'";
	
	$result	=	mysql_query($query) or die(mysql_error());
	
	$data= array();
	
	while($row=mysql_fetch_array($result)){
		array_push($data,array(
			"id"		=> $row["idfile"],
			"continent"	=> $row["groups"], 
			"country"	=> $row["namefile"],
			"hotel"	=> $row["filename"]
		));
	}
	
	//step 3
	echo json_encode(
		array(  
		"success"	=> true,
		"total"		=> mysql_affected_rows(),
		"data"		=> $data
	));
?>