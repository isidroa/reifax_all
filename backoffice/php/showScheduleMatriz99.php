<style type="text/css">
html {
	font-family: Arial, Helvetica, sans-serif
} 

table.horario-table, table.horario-legend {
	border-collapse:collapse;
	font-weight: bold;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif
} 

.horario-table td 
{	
	padding: 1px 3px;
}

.horario-table td.header 
{	
	background-color: #104E8B; 
	color: #FFF;
}
.horario-table td.normal1
{	
	background-color: #fff; 
	border: 1px solid #ccc;
	color: #000;
	
}
.horario-table td.normal2
{	
	background-color: #fcfcfc; 
	border: 1px solid #ccc;
	color: #000;
	
}
.horario-table td.online,
.horario-legend td.online
{	
	background-color: #97d897; 
	border: 1px solid #ccc;
	color: #000;
	
}
.horario-table td.offline,
.horario-legend td.offline
{	
	background-color: #ff4c4c; 
	border: 1px solid #ccc;
	color: #000;
	
}
.horario-table td.extrahours,
.horario-legend td.extrahours
{	
	background-color: #d897d8; 
	border: 1px solid #ccc;
	color: #000;
	
}
.horario-table td.workschedule,
.horario-legend td.workschedule
{	
	background-color: #cefef3; 
	border: 1px solid #ccc;
	color: #000;
	
}
</style>
<?php
session_start();	

function conversionTiempoDias ($seg_ini){
	$seg_ini=$seg_ini*60;
	if($seg_ini<0){
		$seg_ini=abs($seg_ini);
		$signo='-';
	}
	else{
		$signo='';
	}
	$dias = floor($seg_ini/(28800));
	$horas = floor(($seg_ini-($dias*28800))/3600);
	$minutos = floor(($seg_ini-(($horas*3600)+($dias*28800)))/60);
	$segundos = round($seg_ini-($horas*3600)-($minutos*60));
		
	if ($dias > 0){
		return $signo.' '.$dias. 'd '.' '.$horas. 'h '. $minutos. 'm ';
	}
	else if ($horas > 0){
		return $signo.' '.$horas. 'h '. $minutos. 'm ';
	} else {
		return $signo.' '.$minutos. 'm '. $segundos. 's';
	}
}

function findhourminute($arrdata,$hour,$min)
{
	foreach($arrdata as $v)
		if($v['h']==$hour && $v['m']==$min )
			return 1;
	return 0;
}
include($_SERVER["DOCUMENT_ROOT"]."/connection.php");
$Connect	=	new connect();
$Connect->connectInit();
$conex=$Connect->mysqlByServer(array('serverNumber' => 'Automate'));

$fechaeval=date('Y-m-d');
if(isset($_POST['f'])) $fechaeval=$_POST['f'];
$userid=$_POST['userid'];
//$userid=2342; //9:00 - 5:30
//$userid=4345; //9:00 - 6:30
//$userid=73; //8-12 2-6

if(isset($_GET['f']) && isset($_GET['u']))
{ $fechaeval=$_GET['f']; $userid=$_GET['u']; }

//$fechaeval='2014-11-21';

$dia=date('N',strtotime($fechaeval));
$titulodia=date('l, d F Y',strtotime($fechaeval));
$FINSEMANA=0;
if($dia==6 || $dia==7)$FINSEMANA=1;




$sql="SELECT userid
	FROM xima.connectionvacations c
	WHERE c.`userid`=$userid and c.daterecord='$fechaeval'";
$result=$conex->query ($sql) or die ($sql.$conex->error);
$row=$result->fetch_object();
$VACACION=0;
if($row->userid<>'')$VACACION=1;

$sql="	SELECT c1.`title` 
		FROM xima.connectionnolaboraluser c
		INNER JOIN xima.connectionnolaboral c1 ON c.idnol=c1.idnol 
		WHERE c.`userid`=$userid and c1.`date`='$fechaeval'";
$result=$conex->query ($sql) or die ($sql.$conex->error);
$row=$result->fetch_object();
$NOLABORAL=0;
if($row->title<>'')$NOLABORAL=1;


$que="SELECT SQL_NO_CACHE  `USERID`, `NAME`, `SURNAME` FROM `xima`.`ximausrs`
		WHERE `USERID`=$userid";
$result=$conex->query ($que) or die ($que.$conex->error);
$row=$result->fetch_object();
$username=strtoupper($row->NAME.' '.$row->SURNAME);

//evaltime       timematriz		evaltime
 $sql="	SELECT SQL_NO_CACHE  hour(timematriz) h,minute(timematriz) m
		FROM xima.connectionlogs c
		WHERE c.`userid`=$userid AND c.`evaldate`='$fechaeval' AND c.`check`=1";
$res=$conex->query($sql) or die(json_encode(array("success"=> FALSE,"error"=> $conex->error,"sql"=> $sql)));
$data=array();
while ($tem=$res->fetch_assoc()) array_push($data,$tem);

$sql="SELECT SQL_NO_CACHE 
	hour(date_add(now(),interval 30 minute)) hcur,minute(date_add(now(),interval 30 minute)) mcur,
	c.`hour_init`, concat(abs(replace(c.`hour_init`,':',''))) initime,
	c.`hour_end`, concat(abs(replace(c.`hour_end`,':',''))) endtime,
	c.`hour_break_init`,concat(abs(replace(c.`hour_break_init`,':',''))) breakinitime,
	c.`hour_break_end`, concat(abs(replace(c.`hour_break_end`,':',''))) breakendtime,
		c.`break`
	FROM xima.connectionhours c
	WHERE c.`userid`=$userid";
$result=$conex->query ($sql) or die ($sql.$conex->error);
$hours=$result->fetch_object();


$acumonline=$acumoffline=$acumextrahours=$acumworkschedule=0;	

echo "<br><span style='color:#104E8B;font-weight: bold;font-size: 16px;margin-left:400px'> $username ($userid)</span>";
echo "<br><span style='color:#104E8B;font-weight: bold;font-size: 12px;margin-left:400px'> $titulodia </span>";
echo '<table class="horario-table">';

	echo '<tr >';
	for($m=0;$m<61;$m++)
	{
		$v='';
		if($m>0)$v=str_pad(($m-1), 2, "0", STR_PAD_LEFT);
		echo '<td  class="header">'.$v.'</td>';
	}	
	echo '</tr >';

	for($h=0;$h<24;$h++)
	{
		echo '<tr>';
		for($m=0;$m<61;$m++)
		{
			$a='&nbsp;';
			//$a=$h.":".($m-1);
			$class='class="normal1"';
			if(($h%2)==0)$class='class="normal2"';
			if($m==0)
			{
				$a=str_pad($h, 2, "0", STR_PAD_LEFT);
				$class='class="header"';
			}
			
			$hm=abs($h.str_pad(($m-1), 2, "0", STR_PAD_LEFT).'00');
			$hmcur=abs($hours->hcur.str_pad(($hours->mcur-1), 2, "0", STR_PAD_LEFT).'00');
			
			if($FINSEMANA==0 && $VACACION==0 && $NOLABORAL==0)
			{
				if($hours->break==0)
				{//no tiene break al mediodia,trabaja corrido
					if( $hm>=$hours->initime && $hm<=$hours->endtime )
					{			
						$class='class="workschedule"';	
						$acumworkschedule++;					
						if( findhourminute($data,$h,($m-1))==1 ) 
						{
							$acumonline++;
							$class='class="online"';			
						}
						else
						{
							if($fechaeval==date('Y-m-d'))
							{
								if($hm<=$hmcur)
								{
									$acumoffline++;
									$class='class="offline"';
								}
							}
							else
							{
								$acumoffline++;
								$class='class="offline"';
								
							}
						}
					}
					else
					{
						if( findhourminute($data,$h,($m-1))==1 ) 
						{
							$acumextrahours++;
							$class='class="extrahours"';
						}
					}
				}
				else
				{//tiene break al mediodia
					if( ($hm>=$hours->initime &&  $hm<=$hours->breakinitime) ||($hm>=$hours->breakendtime && $hm<=$hours->endtime ))
					{			
						$class='class="workschedule"';
						$acumworkschedule++;					
						if( findhourminute($data,$h,($m-1))==1 ) 
						{
							$acumonline++;
							$class='class="online"';			
						}
						else
						{
							if($fechaeval==date('Y-m-d'))
							{
								if($hm<=$hmcur)
								{
									$acumoffline++;
									$class='class="offline"';
								}
							}
							else
							{
								$acumoffline++;
								$class='class="offline"';
								
							}
						}
					}
					else
					{
						if( findhourminute($data,$h,($m-1))==1 ) 
						{
							$acumextrahours++;
							$class='class="extrahours"';
						}
					}
				}
				
			}
			else
			{
				if( findhourminute($data,$h,($m-1))==1 ) 
				{
					$acumextrahours++;
					$class='class="extrahours"';
				}
			}
			echo '<td '.$class.' >'.$a.'</td>';
		}
		echo '</tr>';
	}		
echo '</table>';

echo '<br><TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 class="horario-legend">';
echo '<tr>';
echo '<td class="online">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td  > ONLINE ('.$acumonline.' min ~ '.conversionTiempoDias ($acumonline).')</td>';
echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
echo '<td class="offline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td  > OFFLINE ('.$acumoffline.' min ~ '.conversionTiempoDias ($acumoffline).')</td>';
echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
echo '<td class="extrahours">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td  > EXTRA MINUTES ('.$acumextrahours.' min ~ '.conversionTiempoDias ($acumextrahours).')</td>';
echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
echo '<td class="workschedule">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td  > WORK SCHEDULE ('.$acumworkschedule.' min ~ 8h 0m)</td>';
echo '</tr>';
echo '</table>';

?>