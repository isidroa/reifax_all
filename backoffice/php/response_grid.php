<?php
	include ("checkuser.php");  
	include("conexion.php");	
	$modulo=$_GET['oper'];
        if(!is_null($_POST['oper'])){
            $modulo=$_POST['oper'];
        }
	$start=$_POST['start'];
	$limit=$_POST['limit'];

	$switch=0;
	switch($modulo){
		case 'people':
			$order=$_POST['sort'];
			$que="	SELECT distinct SQL_CALC_FOUND_ROWS people.`iduse`,people.`nickname`, people.`name`, people.`lastname`, 
						people.`email`, people.`cellphone`,people.`insertdate`, usertype.`type`
					FROM people 
					LEFT JOIN enrollment  ON enrollment.idstu=people.iduse
					LEFT JOIN payments  ON payments.idenr=enrollment.idenr
					LEFT JOIN creditcard ON creditcard.iduse=people.iduse
					LEFT JOIN cardtype  ON cardtype.idcar=creditcard.idcar 
					LEFT JOIN usertype ON people.idtyp=usertype.idtyp
				";
				if(isset($_SESSION['filters'])){ 
				 //echo 
				 $que.=" WHERE  ".$_SESSION['filters']."  ";
				}					
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;

		case 'cardtype':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS c.`idcar`, c.`cardtype` FROM dbworkshops.cardtype c ";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;

		case 'usertype':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS u.`idtyp`, u.`type`, u.`description` FROM dbworkshops.usertype u ";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;

		case 'operationpay':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS  o.`idope`, o.`title`, o.`description` FROM dbworkshops.operationpay o ";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;

		case 'sourcepay':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS  s.`idsou`, s.`source`, s.`description` FROM dbworkshops.sourcepay s ";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;

		case 'generaltopic':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS g.`idtop`, g.`name`, g.`description` FROM dbworkshops.generaltopics g";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			$que.=" LIMIT ".$start.",".$limit; 
		break;		
		
		case 'workshop':
			$order=$_POST['sort'];
			$que="	SELECT w.`idwor`, w.`nameworkshop`,  w.`description`, w.`active`,
					(SELECT count(*) cant FROM dbworkshops.volumen c WHERE c.`idwor`=w.`idwor`) countvolumen,
					(SELECT count(*) cant FROM dbworkshops.subject c WHERE c.`idwor`=w.`idwor`) countsubject,
					f1.`frecuency` fpay,
					f2.`frecuency` fdeliver,
					w.price,
					w.deliveryvolume 
					FROM dbworkshops.workshop w
					INNER JOIN dbworkshops.frecuency f1 on f1.idfre=w.idfrepay
					INNER JOIN dbworkshops.frecuency f2 on f2.idfre=w.idfredeliver";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
		$que.=" LIMIT ".$start.",".$limit; 
		break;
		
		case 'products':
			$order=$_POST['sort'];
			$que="	SELECT SQL_CALC_FOUND_ROWS  d.idpro, d.nameproduct, d.`frecuencypay` ,d.`price` ,
					(SELECT count(*) cant FROM dbworkshops.productclass c1 WHERE c1.`idpro`=d.idpro) numberclass,
					(SELECT count(*) cant FROM dbworkshops.enrollment c1 WHERE c1.`idpro`=d.`idpro`) numberstudent
					FROM dbworkshops.products d";
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
		$que.=" LIMIT ".$start.",".$limit; 
		break;
		
		case 'volumens':
			$volaux=$_GET['volaux'];

			$que="	SELECT a.idaux,a.`idauxclass`, a.`nameresource`, a.`pathresource`, a.`idtop`,a.size,g.name
					FROM dbworkshops.auxvolumnes a
					INNER JOIN dbworkshops.generaltopics g on a.`idtop`=g.`idtop`
					where a.`idauxclass`='$volaux'";
			$order=$_POST['sort'];
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
		break;
		
		case 'fileslist':
			$idvol=$_GET['idvol'];
			$idsub=$_GET['idsub'];

			$que="	SELECT idvsub,`nameresource`, `sizefile`,pathresource 
					FROM volumensubject  
					WHERE `idvol`='$idvol' and `idsub`='$idsub'";
			$order=$_POST['sort'];
			if(trim($order!='')){
				$que.=" ORDER BY ".$order.' '.$_POST['dir'];
			}
			else
				$que.=" ORDER BY idvsub";			
		break;
		
		case 'onlyworkshop':
			$idwor=$_POST['idwor'];
			$que="	SELECT w.`idwor`, w.`nameworkshop`,  w.`description`, w.`active`,
					(SELECT count(*) cant FROM dbworkshops.volumen c WHERE c.`idwor`=w.`idwor`) countvolumen,
					(SELECT count(*) cant FROM dbworkshops.subject c WHERE c.`idwor`=w.`idwor`) countsubject,
					w.idfrepay,
					w.idfredeliver,
					f1.`frecuency` fpay,
					f2.`frecuency` fdeliver,
					w.price,
					w.deliveryvolume 
					FROM dbworkshops.workshop w
					INNER JOIN dbworkshops.frecuency f1 on f1.idfre=w.idfrepay
					INNER JOIN dbworkshops.frecuency f2 on f2.idfre=w.idfredeliver
					WHERE w.idwor=$idwor";
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$row=mysql_fetch_array($result);
			echo json_encode(array(
				idwor       		=> $row['idwor'],
				nameworkshop      	=> $row['nameworkshop'],
				description       	=> $row['description'],
				active       		=> $row['active'],
				countvolumen       	=> $row['countvolumen'],
				countsubject       	=> $row['countsubject'],
				idfrepay       		=> $row['idfrepay'],
				idfredeliver       	=> $row['idfredeliver'],
				fpay       			=> $row['fpay'],
				fdeliver       		=> $row['fdeliver'],
				price       		=> $row['price'],
				deliveryvolume      => $row['deliveryvolume']
			));
			return;

		break;
/*
		case 'deudores':// deudores activos y freeze 			
								
			$que="SELECT distinct u.userid,x1.name,x1.surname,
						(
							SELECT distinct s1.status
							FROM usr_cobros u1
							INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
							WHERE u1.`userid`=u.`userid` 
							ORDER BY s1.status
							limit 1
						) status,
						(
							SELECT distinct u1.fechacobro
							FROM usr_cobros u1
							WHERE u1.`userid`=u.`userid` 
							limit 1
						) paydated,
						0 as priceprod,
						c.cardname,
						d.saldo,
						0 amountsinformato,
						n.notes
				FROM usr_cobros u
				INNER JOIN xima.ximausrs x1 ON u.userid=x1.userid
				INNER JOIN xima.cobro_porcentajes d ON d.userid=u.userid
				INNER JOIN xima.creditcard c ON c.userid=u.userid
				INNER JOIN xima.usernotes n ON n.userid=u.userid
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				WHERE u.idstatus IN (3,6,1) and p.pu=0 and d.saldo<0
				ORDER BY  day(paydated) desc,x1.userid";
			$result=mysql_query ($que) or die ($que.mysql_error ());

			$data = array();			
			while ($row=mysql_fetch_object($result))
			{
				$row->priceprod=number_format(getpriceuser($row->userid,0,'yes'),2,'.',',');	
				$row->amountsinformato=$row->saldo;				
				$row->saldo=number_format($row->saldo,2,'.',',');				
				$data [] = $row;
			}
			echo '({"total":"'.$num_row.'","results":'.json_encode($data).'})';
			return;
		break;
*/		
	}


    if($switch==0){
        $result=mysql_query ($que) or die ($que.mysql_error ());

        $data = array();

        while ($row=mysql_fetch_object($result))
            $data [] = $row;

        $result2 = mysql_query("SELECT FOUND_ROWS();" ) or die (mysql_error());
        $num_row=mysql_fetch_row($result2);

        echo '({"total":"'.$num_row[0].'","results":'.json_encode($data).',"que":""})';//'.$que.'
    }

?>
