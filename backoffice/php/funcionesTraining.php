<?php
include("conexion.php");
conectar('xima');

$status=array(
	'Pending' 	=> 1,
	'Created' 	=> 2,
	'Redo' 	=> 3,
	'Checked' 	=> 4,
	'Uploaded' 	=> 5
);

switch ($_POST['accion'])
{
	case ('recordtraining'):
	
		$url = $_POST['url'];
		if($url!=''){
			if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
				$video_id = $match[1];
			}
			 
			$feedURL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,player,recordingDetails,statistics,status&key=AIzaSyBTck83MItPMfxAZ51zmMt7CajVTxUqvlU&id=' . $video_id;
			$entry=json_decode(file_get_contents($feedURL));
			if(!is_object($entry)){
				echo json_encode(array(
					'success' 	=> false,
					'error'		=> "el video es privado"
				));
				die();
			}
			else{
				$videoEval=$entry->items[0];
				$video= new stdClass();
				
				$video->views=$videoEval->statistics->viewCount;
				
				$video->views=$video->views!=''?$video->views[0]:0;
				$video->title=mysql_real_escape_string($videoEval->snippet->localized->title);
				$video->description=mysql_real_escape_string(trim($videoEval->snippet->localized->description));
				$video->thumbnailURL=mysql_real_escape_string(trim($videoEval->snippet->thumbnails->medium->url));
				$video->length=mysql_real_escape_string(covtime(trim($videoEval->contentDetails->duration)));
				$sql="INSERT INTO `videos_training`
						(`name`,`idYoutube`,`urlVideo`,`urlImg`,`status`,`fecha`,`description`,`length`,`views`,typeHelp)
					VALUES
						('{$video->title }','{$video_id}','http://www.youtube.com/embed/{$video_id}','{$video->thumbnailURL}',1,NOW(),'{$video->description}',{$video->length},{$video->views},{$_POST['typeHelp']});
					";
			}
		}
		else{
			$sql="INSERT INTO `videos_training`
					(`name`,`idYoutube`,`status`,`fecha`,`description`,typeHelp)
				VALUES
					('{$_POST['new']}','{$video->thumbnailURL}',1,NOW(),'{$_POST['new']}',{$_POST['typeHelp']});
				";
		}
		mysql_query($sql) or die($sql.mysql_error());
		
		
		
		echo json_encode(array(
			'success' => true
		));
	break;
	case ('listModulesAssi'):
		$table=($_POST[
		'type']==1)?'training_assignment_video':'training_assignment_videosys';
		$sql="SELECT a.* FROM {$table} a join training_modulo m on a.id_modulo=m.id where m.typeHelp={$_POST['typeHelp']} AND id_video={$_POST['video']};";
		$result=mysql_query($sql) or die($sql.mysql_error());
		$video=array();
		$i=1;
		while($tem = mysql_fetch_assoc($result)){
			array_push($video,$tem);
		}
		echo json_encode(array(
			'success' 	=> true,
			'modules'	=> $video
		));
	break;
	case('listMenu'):
		if($_POST['q']!=''){
			$sql='SELECT id_modulo FROM xima.videos_training t join training_assignment_video on id_video=t.id WHERE t.typeHelp='.$_POST['typeHelp'].' AND name LIKE "%'.$_POST['q'].'%" OR `description` LIKE "%'.$_POST['q'].'%"';
			$res=mysql_query($sql) or die ($sql.mysql_error());
			$busqueda=array();
			while($data=mysql_fetch_assoc($res)){
				array_push($busqueda,$data['id_modulo']);
			}
		}
		$id=$_POST['node'];
		if($id=='All')
			$sql='SELECT m.* FROM training_modulo m WHERE typeHelp='.$_POST['typeHelp'].' AND father IS NULL order by COALESCE(pos, 999999999) ASC, `name`';
		else
			$sql='SELECT m.* FROM training_modulo m WHERE typeHelp='.$_POST['typeHelp'].' AND father='.$id.' order by COALESCE(pos, 999999999) ASC, `name`';
		$res=mysql_query($sql) or die ($sql.mysql_error());
		$tree=array();
		while($data=mysql_fetch_assoc($res)){
			$childs=getChild($data['id']);
			$tem=array( 'text' => $data['name'], 'iconCls' => 'FOLDER_HOME', 'expanded' => false , 'id'=> $data['id'], 'children' => $childs,'idsSearch' => $busqueda);
			array_push($tree,$tem);
		}
		echo json_encode($tree);
	break;
	case ('listSingle'):
		if($_POST['query']!=''){
			$sql="select * from videos_training where typeHelp={$_POST['typeHelp']} AND `status`=5 AND name like '%{$_POST['query']}%' order by `name`";
		}
		else{
			$sql="select * from videos_training where typeHelp={$_POST['typeHelp']} order by `name` ";
		}
		$result=mysql_query($sql) or die ($sql.mysql_error());
		$video=array();
		while($tem = mysql_fetch_assoc($result)){
			array_push($video,$tem);
		}
		echo json_encode(array(
			'success' 	=> true,
			'videos'	=> $video
		));
	break;
	case ('recordusertrainigroup'):
		$sql="INSERT INTO `training_assignment_video`
			(`id_video`, `id_modulo`)
		VALUES
			({$_POST['video']},{$_POST['node']});
		";
		mysql_query($sql);
		if(mysql_affected_rows()>0)
		{
			echo json_encode(array(
				'success' => true,
				'sql'	=> $sql
			));
		}
		else{
		echo json_encode(array(
				'success' => false,
				'sql'	=> $sql,
				'error' => mysql_error()
			));
		}
	break;
	case ('list'):
	
		$where="where v.typeHelp={$_POST['typeHelp']} AND status {$_POST['status']}";
		
		if($_POST['search']){
			$_POST['search']=str_replace(' ','%',$_POST['search']);
			$where.=" AND (v.name LIKE '%{$_POST['search']}%' OR v.description LIKE '%{$_POST['search']}%') ";
		}
		$sql="select v.*, IF(v.idYoutube='','',CONCAT('http://www.youtube.com/embed/',v.idYoutube,'?rel=0&def=true')) videoUrl, 
				GROUP_CONCAT(m.father,'|') father, 
				GROUP_CONCAT(m.name,'|') modules from videos_training v 
				left join training_assignment_video a on v.id=a.id_video 
				left join training_modulo m on m.id=a.id_modulo 
				{$where} 
				group by v.id
		order by v.id";
		$result=mysql_query($sql) or die($sql.mysql_error());
		$video=array();
		$index1=array();
		$i=1;
		while($tem = mysql_fetch_assoc($result)){
			$tem['father']=explode('|',$tem['father']);
			$tem['modules']=explode('|',$tem['modules']);
			foreach($tem['father'] as $k => $v){
				$tem['father'][$k]=str_replace(',','',$tem['father'][$k]);
				$tem['padre'].=($tem['padre']!='' && $tem['father'][$k]!='')?' / ':'';
				$tem['modules'][$k]=str_replace(',','',$tem['modules'][$k]);
				if($tem['modules'][$k]!='')
					$tem['padre'].=getFatherModule($tem['father'][$k],$tem['modules'][$k]);	
			}
			if($tem['status']==2){
				$tem['show']=false;
			}else{
				$tem['show']=true;
			}
			$index1[$tem['id']]=count($video);
			array_push($video,$tem);
		}
		
		$sql="select v.id,
				GROUP_CONCAT(mSys.father,'|') fatherSys, 
				GROUP_CONCAT(mSys.name,'|') modulesSys from videos_training v 
				left join training_assignment_videosys aSys on v.id=aSys.id_video 
				left join training_modulosys mSys on mSys.id=aSys.id_modulo  
				{$where} 
				group by v.id
		order by v.id";
		$result=mysql_query($sql) or die($sql.mysql_error());
		$i=1;
		$aux=array();
		while($tem = mysql_fetch_assoc($result)){
			$tem['fatherSys']=explode('|',$tem['fatherSys']);
			$tem['modulesSys']=explode('|',$tem['modulesSys']);
			foreach($tem['fatherSys'] as $k => $v){
				$tem['fatherSys'][$k]=str_replace(',','',$tem['fatherSys'][$k]);
				$tem['padreSys'].=($tem['padre']!='' && $tem['father'][$k]!='')?' / ':'';
				$tem['modulesSys'][$k]=str_replace(',','',$tem['modulesSys'][$k]);
				if($tem['modulesSys'][$k]!=''){
					$tem['padreSys'].=getFatherModuleSys($tem['fatherSys'][$k],$tem['modulesSys'][$k]);	
					
				}
			}
			//print_r($video[$index1[$tem['id']]]);
			$video[$index1[$tem['id']]]=array_merge($video[$index1[$tem['id']]],$tem);
			//print_r($tem);
			//print_r($video[$index1[$tem['id']]]);
			/*echo '
			########
			';*/
		}
		
		
		
		
		
		echo json_encode(array(
			'success' 	=> true,
			'videos'	=> $video
		));
	break;
	case ('listModule'):
		$table=($_POST['type']==1)?'training_modulo':'training_modulosys';
		$sql="SELECT a.* FROM training_modulo a join training_modulo m on a.id=m.id where m.typeHelp={$_POST['typeHelp']} AND   a.father is NULL order by `name`;";
		$result=mysql_query($sql) or die(mysql_error());
		$modules=array();
		while($tem = mysql_fetch_assoc($result)){
			$childs=getChilModules($tem['id'],$tem['name']);
			array_push($modules,$tem);
			$modules=array_merge($modules,$childs);
		}
		echo json_encode(array(
			'success' 	=> true,
			'modules'	=> $modules
		));
	break;
	case ('getDes'):
		$sql="SELECT * FROM videos_training where typeHelp={$_POST['typeHelp']} AND id={$id};";
		$result=mysql_query($sql) or die(mysql_error());
		$modules=array();
		$tem = mysql_fetch_assoc($result);
		
		echo json_encode($tem);
	break;
	case ('recordModule'):
		$_POST['module']=$_POST['module']==''?'NULL':$_POST['module'];
		$sql="INSERT INTO `training_modulo`
				( `name`, `father`,typeHelp)
			VALUES
			('{$_POST['name']}',{$_POST['module']},{$_POST['typeHelp']});";
		$result=mysql_query($sql) or die($sql.mysql_error());
		echo json_encode(array('success' => true));
	break;
	case ('updateModulos'):
		$table=($_POST['type']==1)?'training_assignment_video':'training_assignment_videosys';
		$records = json_decode(stripslashes($_POST['records']));
        foreach($records as $k){ 
				if($k->newRecordId){
					$k->newRecordId=explode('_',$k->newRecordId);
					$sql="INSERT INTO `{$table}`
						(`id_video`, `id_modulo`)
					VALUES
						({$k->newRecordId[1]},{$k->id_modulo});
					";
				}
				else{
					$sql="UPDATE {$table} SET id_modulo={$k->id_modulo} where id={$k->id}; ";
				}
				mysql_query($sql);
        }  
		if(mysql_affected_rows()>0)
		{
			echo json_encode(array(
				'success' => true,
				'sql'	=> $sql
			));
		}
		else{
		echo json_encode(array(
				'success' => false,
				'sql'	=> $sql,
				'error' => mysql_error()
			));
		}
	break;
	case ('updateDes'):
		$description=mysql_escape_string($_POST['description']);
		$observations=mysql_escape_string($_POST['observations']);
		$sql="UPDATE videos_training SET observations='{$observations}', description='{$description}' where id={$_POST['id']}; ";
		mysql_query($sql) or die($sql.mysql_error());
		echo json_encode(array(
			'success' => true,
			'sql'	=> $sql
		));
	break;
	case ('updateModulePos'):
		$_POST['nodeFather']=($_POST['nodeFather']=='All')?'NULL':$_POST['nodeFather'];
		if($_POST['isVideo']=='true'){
			$select="SELECT count(id) cant FROM training_modulo where father={$_POST['nodeFather']};";
			$res=mysql_query($select) or die($select.mysql_error());
			$temporal=mysql_fetch_assoc($res);
			$select="SELECT pos,id_modulo FROM training_assignment_video where id={$_POST['id']};"; 
			$res=mysql_query($select) or die($select.mysql_error());
			$data=mysql_fetch_assoc($res);
			$_POST['pos']=$_POST['pos']-$temporal['cant'];
			$_POST['pos']=$_POST['pos']<1?1:$_POST['pos'];
			if($data['id_modulo']==$_POST['nodeFather']){
				if($data['pos']<$_POST['pos']){
					$sql="UPDATE training_assignment_video SET pos=(pos-1) where id_modulo={$_POST['nodeFather']} and pos <= {$_POST['pos']} AND pos > {$data['pos']} ; ";
					mysql_query($sql) or die($sql.mysql_error());
				}
				else{
					$sql="UPDATE training_assignment_video SET pos=(pos+1) where id_modulo={$_POST['nodeFather']} and pos >= {$_POST['pos']} AND pos < {$data['pos']}; ";
					mysql_query($sql) or die($sql.mysql_error());	
				}
			}
			else{
				$sql="UPDATE training_assignment_video SET pos=(pos-1) where id_modulo={$_POST['nodeFather']} and pos >= {$data['pos']}; ";
				mysql_query($sql) or die($sql.mysql_error());
				$sql="UPDATE training_assignment_video SET pos=(pos+1) where id_modulo={$_POST['nodeFather']} and pos >= {$_POST['pos']}; ";
				mysql_query($sql) or die($sql.mysql_error());
			}
			$sql="UPDATE training_assignment_video SET id_modulo={$_POST['nodeFather']}, pos={$_POST['pos']} where id={$_POST['id']}; ";
			mysql_query($sql) or die($sql.mysql_error());
		}
		else{
			$select="SELECT COALESCE(pos, 999999999) pos,father FROM training_modulo where id={$_POST['id']};"; 
			$res=mysql_query($select) or die($select.mysql_error());
			$data=mysql_fetch_assoc($res);
			
			
			$newFather=($_POST['nodeFather']=='NULL')?'is NULL':'= '.$_POST['nodeFather'];
			$father=($data['father']=='')?'is NULL':'= '.$data['father'];
			
			$data['father']=($data['father']=='')?'NULL':$data['father'];
			
			if($data['father']==$_POST['nodeFather']){
				if($data['pos']<$_POST['pos']){
					$sql="UPDATE training_modulo SET pos=(pos-1) where father {$newFather} and pos <= {$_POST['pos']} AND pos > {$data['pos']} ; ";
					mysql_query($sql) or die($sql.mysql_error());
				}
				else{
					$sql="UPDATE training_modulo SET pos=(pos+1) where father {$newFather} and (pos >= {$_POST['pos']}); ";	
					mysql_query($sql) or die($sql.mysql_error());
				}
			}
			else{
				$sql="UPDATE training_modulo SET pos=(pos-1) where father {$father} and pos > {$data['pos']}; ";
				mysql_query($sql) or die($sql.mysql_error());
				$sql="UPDATE training_modulo SET pos=(pos+1) where father {$newFather} and (pos >= {$_POST['pos']}); ";
				mysql_query($sql) or die($sql.mysql_error());
			}
			$sql="UPDATE training_modulo SET father = {$_POST['nodeFather']}, pos={$_POST['pos']} where id={$_POST['id']}; ";
			mysql_query($sql) or die($sql.mysql_error());
		}
		echo json_encode(array(
			'success' => true,
			'sql'	=> $sql
		));
	break;
	case ('deleteModule'):
		if($_POST['isVideo']=='true'){
			$sql='DELETE FROM `training_assignment_video` where id ='.$_POST['id_assing'];
		}
		else{
			$sql='DELETE FROM `training_modulo` where id ='.$_POST['id'];
		}
		mysql_query($sql);
		echo json_encode(array('success'=>true ,'sql' => $sql));
	break;
	case ('editModule'):
		if($_POST['isVideo']=='true'){
			$sql="UPDATE `videos_training` SET name='{$_POST['name']}'   where id={$_POST['video']};";
		}
		else{
			$sql="UPDATE `training_modulo` SET name='{$_POST['name']}'   where id={$_POST['id']};";
		}
		$result=mysql_query($sql) or die($sql.mysql_error());
		$features=array();
		$i=1;
		while($tem = mysql_fetch_assoc($result)){
			array_push($features,$tem);
		}
		echo json_encode(array(
			'success' 	=> true,
			'features'	=> $features
		));
	break;
	case ('update'):
		$records = json_decode(stripslashes($_POST['records']));
        foreach($records as $k){ 
		
				if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $k->urlVideo, $match)) {
					$k->idYoutube=$match[1];
					
					$feedURL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,player,recordingDetails,statistics,status&key=AIzaSyBTck83MItPMfxAZ51zmMt7CajVTxUqvlU&id=' . $video_id;
					$entry=json_decode(file_get_contents($feedURL));
					$videoEval=$entry->items[0];
					
					$k->thumbnailURL=mysql_real_escape_string(trim($videoEval->snippet->thumbnails->medium->url));
					$k->urlVideo="http://www.youtube.com/embed/{$k->idYoutube}";
					
				}
				$k->views=$video->views!=''?$video->views:0;
				$k->length=$video->length!=''?$video->length:0;
				$status=((is_numeric($k->status))?$k->status:$status[$k->status]);
				$observations=mysql_escape_string($k->observations);
				$k->name_sys=mysql_escape_string($k->name_sys);
				$k->name=mysql_escape_string($k->name);
                $sql="UPDATE videos_training SET urlImg='{$k->thumbnailURL}', madeBy='{$k->madeBy}', name='{$k->name}',urlVideo='{$k->urlVideo}',  observations='{$observations}', idYoutube='{$k->idYoutube}',length={$k->length},views={$k->views},name_sys='{$k->name_sys}', status='{$k->status}' where id={$k->id}; ";
				
				mysql_query($sql);
        }  
		if(mysql_affected_rows()>0)
		{
			echo json_encode(array(
				'success' => true,
				'sql'	=> $sql
			));
		}
		else
		{echo json_encode(array(
				'success' => false,
				'sql'	=> $sql,
				'error' => mysql_error()
			));
		}
		
	break;
	case ('delete'):
		$sql='DELETE FROM `videos_training` where id in ('.$_POST['ids'].')';
		mysql_query($sql);
		echo json_encode(array('success'=>true ,'sql' => $sql));
	break;
	case ('deleteModulo'):
		$table=($_POST['type']==1)?'training_assignment_video':'training_assignment_videosys';
		$sql="DELETE FROM `{$table}` where id in ({$_POST['ids']})";
		mysql_query($sql);
		echo json_encode(array('success'=>true ,'sql' => $sql));
	break;
}

function parseVideoEntry($entry) {      
  $obj= new stdClass;

  $obj->author = $entry->author->name;
  $obj->authorURL = $entry->author->uri;


  $media = $entry->children('http://search.yahoo.com/mrss/');
  $obj->title = $media->group->title;
  $obj->description = $media->group->description;


  $attrs = $media->group->player->attributes();
  $obj->watchURL = $attrs['url']; 


  $attrs = $media->group->thumbnail[0]->attributes();
  $obj->thumbnailURL = $attrs['url']; 

  // get <yt:duration> node for video length
  $yt = $media->children('http://gdata.youtube.com/schemas/2007');
  $attrs = $yt->duration->attributes();
  $obj->length = $attrs['seconds']; 

  // get <yt:stats> node for viewer statistics
  $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
  $attrs = $yt->statistics->attributes();
  $obj->viewCount = $attrs['viewCount']; 


  $gd = $entry->children('http://schemas.google.com/g/2005'); 
  if ($gd->rating) { 
    $attrs = $gd->rating->attributes();
    $obj->rating = $attrs['average']; 
  } else {
    $obj->rating = 0;         
  }

  // get <gd:comments> node for video comments
  $gd = $entry->children('http://schemas.google.com/g/2005');
  if ($gd->comments->feedLink) { 
    $attrs = $gd->comments->feedLink->attributes();
    $obj->commentsURL = $attrs['href']; 
    $obj->commentsCount = $attrs['countHint']; 
  }

  // get feed URL for video responses
  $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
  $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/
  schemas/2007#video.responses']"); 
  if (count($nodeset) > 0) {
    $obj->responsesURL = $nodeset[0]['href'];      
  }

  // get feed URL for related videos
  $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
  $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/
  schemas/2007#video.related']"); 
  if (count($nodeset) > 0) {
    $obj->relatedURL = $nodeset[0]['href'];      
  }

  // return object to caller  
  return $obj;      
} 
function getChilModules ($id, $name){
	$sql="SELECT * FROM training_modulo where father={$id} order by `name`;";
	$result=mysql_query($sql) or die(mysql_error());
	$modules=array();
	while($tem = mysql_fetch_assoc($result)){
		$tem['name']=$name.' -> '.$tem['name'];
		$childs=getChilModules($tem['id'],$tem['name']);
		array_push($modules,$tem);
		$modules=array_merge($modules,$childs);
	}
	return $modules;
}
function getFatherModule ($id,$name){
	if($id!=''){
		$table=($_POST['type']==1)?'training_modulo':'training_modulosys';
		$sql="SELECT * FROM $table where id={$id} limit 1;";
		$result=mysql_query($sql) or die($sql.mysql_error());
		$modules=array();
		while($tem = mysql_fetch_assoc($result)){
			$tem['name']=$tem['name'].' -> '.$name;
			$padre=getFatherModule($tem['father'],$tem['name']);
		}
		$name=$padre;
		
	}
	return $name;
}
function getFatherModuleSys ($id,$name){
	if($id!=''){
		$sql="SELECT * FROM training_modulosys where id={$id} limit 1;";
		$result=mysql_query($sql) or die($sql.mysql_error());
		$modules=array();
		while($tem = mysql_fetch_assoc($result)){
			$tem['name']=$tem['name'].' -> '.$name;
			$padre=getFatherModuleSys($tem['father'],$tem['name']);
		}
		$name=$padre;
		
	}
	return $name;
}

function getChild($id){
	global $busqueda;
	$menu=array();
	$sql='SELECT a.*, v.name, v.status, v.idYoutube FROM training_assignment_video a 
		join videos_training v on a.id_video=v.id where a.id_modulo='.$id.' AND status=5  order by COALESCE(pos, 999999999) ASC, `name`';
	$res2=mysql_query($sql) or die (mysql_error());
	while($data2=mysql_fetch_assoc($res2)){
			$respuesta=array(
				'text' => $data2['name'],
				'iconCls' => 'x-icon-movie',
				'isVideo'	=>	true,
				'id'=> $data2['id'].$data2['id_modulo'],
				'id_video'=>$data2['id_video'],
				'id_assing'=>$data2['id'] ,
				'leaf' => true, 
				'cls' => (($data2['status']==5)?'videoActive':'videoInactive')
			);
			array_push($menu, $respuesta);
	}
	$sql='SELECT m.* FROM training_modulo m WHERE father='.$id.'  order by COALESCE(pos, 999999999) ASC, `name`';
	$res=mysql_query($sql) or die (mysql_error());
	while($data=mysql_fetch_assoc($res)){
			$childs=getChild($data['id']);
			$respuesta=array(
				'text' => $data['name'] ,
				'expanded' => false,
				'children' => $childs ,
				'leaf' => false,
				'id'=> $data['id'],
				'cls' => (in_array($data['id'],$busqueda)?'resaltado':'')
			);
			array_push($menu, $respuesta);
	}
	return $menu;
}

function covtime($youtube_time) {
    preg_match_all('/(\d+)/',$youtube_time,$parts);

    // Put in zeros if we have less than 3 numbers.
    if (count($parts[0]) == 1) {
        array_unshift($parts[0], "0", "0");
    } elseif (count($parts[0]) == 2) {
        array_unshift($parts[0], "0");
    }

    $sec_init = $parts[0][2];
    $seconds = $sec_init%60;
    $seconds_overflow = floor($sec_init/60);

    $min_init = $parts[0][1] + $seconds_overflow;
    $minutes = ($min_init)%60;
    $minutes_overflow = floor(($min_init)/60);

    $hours = $parts[0][0] + $minutes_overflow;

    if($hours != 0)
        return ((($hours*60)+$minutes*60)+$seconds);
    else
        return (($minutes*60)+$seconds);
}

?>