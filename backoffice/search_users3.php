<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demo: Group Summary Example | Quizzpot</title>

<link rel="stylesheet" type="text/css" href="/mant/lib/extjs3/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="/mant/lib/extjs3/examples/ux/css/GroupSummary.css" />

<script type="text/javascript" src="/mant/lib/extjs3/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="/mant/lib/extjs3/ext-all-debug.js"> </script>
<script type="text/javascript">
	Ext.ns("com.quizzpot.tutorials");

	com.quizzpot.tutorials.Group = {
		init : function() {
			var reader = new Ext.data.JsonReader({  //step 1
				totalProperty	: 'total',
				successProperty	: 'success',
				//messageProperty	: 'message',
				idProperty	: 'id',
				type: 'json',
				root		: 'data'
				//,fields:["continent","country","hotel"]
				},[
				//step 2
					{name: "continent", type: "string"},
					{name: "country", type: "string"},
					{name: "hotel", type: "string"},
				]
			);
			this.gstore =new Ext.data.GroupingStore({ //step 1
				url		: "/backoffice/php/parideraJesus.php", //step 2
				reader		: reader,
				sortInfo		: {field:"continent", direction:"ASC"},
				groupField	: "continent",
				remoteSort: true,
            	remoteGroup: true,
				autoLoad: false
			});

			this.gstore.load();
			this.grid = new Ext.grid.GridPanel({  //step 2
				store	: this.gstore, // le pasamos el GroupStore
				columns : [  //configuraci�n de las columnas del Grid
					{header: "Continent",dataIndex: "continent",sortable: true,hideable: false,groupable: false,width: 100},
					{header: "Country",dataIndex: "country",groupable: false},
					{header: "hotel",dataIndex: "hotel",groupable: false}
				], //sterp 3
				bbar:{
			        xtype: 'paging',
			        store: this.gstore,
			        pageSize: 50,
			        displayInfo: true
			      },
				stripeRows: true,
				view : new Ext.grid.GroupingView({
					forceFit 			: true,
					ShowGroupName		: true,
					enableNoGroup		: false,
					enableGropingMenu	: false,
					hideGroupedColumn	: true,
					groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
				}),
				fbar: ['->', {
                  text: 'Clear Grouping',
				  scope:this,
                  handler: function() {
                    this.gstore.clearGrouping();
                  }
                }]
			});
			var win = new Ext.Window({
				title	: "Destinations Summary ",
				layout	: "fit",
				width	: 550,
				height	: 300,
				items	: this.grid
			});

			win.show();
		}
	}
	Ext.onReady(com.quizzpot.tutorials.Group.init,com.quizzpot.tutorials.Group );
</script>
</head>
<body>
</body>
</html>