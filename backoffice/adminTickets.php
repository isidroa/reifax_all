<?php
include ("php/checkuser.php");  
include("php/conexion.php");
$conex=conectar("xima");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Tickets</title>
<style>
	.msgUser
	{
		position:relative;
		padding-bottom:12px;
		border-bottom:solid 1px #567DA9; 
	}
	.msgUser .title
	{
		font-family:Arial, Helvetica, sans-serif;
		font-size:14px;
		font-weight:bold;
		color:#4E6D85;
		text-decoration:underline;
		margin: 3px 4px;
	}
	.msgUser .msgContent
	{
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;
		font-weight:bold;
		color:#000;
		text-decoration:none;
		margin: 3px 8px;
	}
	.msgUser .dateMsg
	{
		position: absolute;
		right: 10px;
		bottom:-2px;
	}
	.imgUser{
		float: left;
		margin-left: 10px;
		position: relative;
		width: 160px;
	}
	.imgUser img {
		background: none repeat scroll 0 0 #CCF8FD;
		border: 1px solid #9ED5FC;
		float: left;
		height: 150px;
		padding: 5px;
		width: 150px;
	}
	.imgUser img:hover {
		background:#6CB9DD;
		border:solid 1px #128AB8;
	}
	.red-row{
		background:#FE5656;
	}
	.red-row:hover{
		background:#F14B4B;
	}
	.green-row{
		background:#B6D0AE;
	}
	.green-row:hover{
		background:#62A857;
	}
	.orange-row{
		background:#FF6;
	}
	.orange-row:hover{
		background:#F93;
	}
	.removeImg{
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #333333;
		color: #FF0000;
		display: block;
		font-size: 16px;
		font-weight: bold;
		padding: 4px 6px;
		position: absolute;
		right: 2px;
		text-decoration: none;
		top: 2px;
	}
</style>
<script>
var wherepage='adminTickets';//variable que dice de que pagina se esta ejecutando
</script>
<link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/fileuploadfield.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<?php 	include("php/enablebuttons.php");?>

<link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />

 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all-debug.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    <script type="text/javascript" src="js/FileUploadField.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/GroupSummary.js"></script>
	<script type="text/javascript" src="js/userdetails.js"></script>
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
    <script type="text/javascript" src="js/tickets.js"></script>
<script>

Ext.namespace('Ext.combos_selec');
Ext.combos_selec.dataStatus = [
<?php 
	$res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
	$i=0;
	echo "['','SELECT']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['idstatus'].",'".$r['status']."']";
?>];
Ext.combos_selec.dataStatus2 = [
<?php 
	$res=mysql_query('SELECT `idstatus`, `status` FROM `xima`.`status` WHERE idstatus not in (8,9)')or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['idstatus'].",'".$r['status']."']";
		$i++;
	}
?>];
Ext.combos_selec.dataUType = [
<?php 
	$res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
	$i=0;
	echo "['','SELECT']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['idusertype'].",'".$r['usertype']."']";
?>];
Ext.combos_selec.dataUType2 = [
<?php 
	$res=mysql_query('SELECT u.`idusertype`, u.`usertype` FROM xima.usertype u;')or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['idusertype'].",'".$r['usertype']."']";
		$i++;
	}
?>];
 Ext.combos_selec.dataUsers = [
 <?php 
	$res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` ORDER BY name")or die(mysql_error());
	$i=0;
	echo "['','NONE']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];  
 Ext.combos_selec.dataUsersAdmin = [
 <?php 
	$res=mysql_query("SELECT `userid`, name ,surname FROM `xima`.`ximausrs` where idusertype in (1,4,7) ORDER BY name")or die(mysql_error());
	$i=0;
	echo "['','NONE']";
	while($r=mysql_fetch_array($res))
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
  
  Ext.combos_selec.countiesshowed = [
 <?php 
	$res=mysql_query("SELECT l.`IdCounty`, l.`County` FROM xima.lscounty l WHERE l.`is_showed`=1 ORDER BY l.`IdCounty`;")or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['IdCounty'].",'".$r['County']."']";
		$i++;
	}
 ?>]; 

 Ext.combos_selec.cbCounty= [
<?php 
	$res=mysql_query('Select idcounty,county,state FROM xima.lscounty ORDER BY county')or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ',';
		echo '[\''.$r['idcounty'].'\',\''.strtoupper($r['county']).'\',\''.strtoupper($r['state']).'\']';
		$i++;
	}
?>
    ];
 Ext.combos_selec.states = [
        ['AL', 'Alabama'],
        ['AK', 'Alaska'],
        ['AZ', 'Arizona'],
        ['AR', 'Arkansas'],
        ['CA', 'California'],
        ['CO', 'Colorado'],
        ['CT', 'Connecticut'],
        ['DE', 'Delaware'],
        ['DC', 'District of Columbia'],
        ['FL', 'Florida'],
        ['GA', 'Georgia'],
        ['HI', 'Hawaii'],
        ['ID', 'Idaho'],
        ['IL', 'Illinois'],
        ['IN', 'Indiana'],
        ['IA', 'Iowa'],
        ['KS', 'Kansas'],
        ['KY', 'Kentucky'],
        ['LA', 'Louisiana'],
        ['ME', 'Maine'],
        ['MD', 'Maryland'],
        ['MA', 'Massachusetts'],
        ['MI', 'Michigan'],
        ['MN', 'Minnesota'],
        ['MS', 'Mississippi'],
        ['MO', 'Missouri'],
        ['MT', 'Montana'],
        ['NE', 'Nebraska'],
        ['NV', 'Nevada'],
        ['NH', 'New Hampshire'],
        ['NJ', 'New Jersey'],
        ['NM', 'New Mexico'],
        ['NY', 'New York'],
        ['NC', 'North Carolina'],
        ['ND', 'North Dakota'],
        ['OH', 'Ohio'],
        ['OK', 'Oklahoma'],
        ['OR', 'Oregon'],
        ['PA', 'Pennsylvania'],
        ['RI', 'Rhode Island'],
        ['SC', 'South Carolina'],
        ['SD', 'South Dakota'],
        ['TN', 'Tennessee'],
        ['TX', 'Texas'],
        ['UT', 'Utah'],
        ['VT', 'Vermont'],
        ['VA', 'Virginia'],
        ['WA', 'Washington'],
        ['WV', 'West Virginia'],
        ['WI', 'Wisconsin'],
        ['WY', 'Wyoming']
    ];
 
 
 setTimeout(function (){
Ext.onReady(tickets.init);},1000);
 </script>

 
</head>
<body onUnload="cleanSession()">	
</body>
</html>