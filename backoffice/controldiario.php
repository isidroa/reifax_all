<?php
include ("php/checkuser.php");  
include("php/conexion.php");
$conex=conectar("xima");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>DAILY COMPLETE</title>
    <link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
	<?php 	include("php/enablebuttons.php");?>
    <link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/CheckColumn.js"></script>
	
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
</head>
<body>
</body>
</html>
<script>
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

	
/////////////////FIN Variables////////////////////
	var winLoad=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div id="resultupload" style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
	var winLoadw=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div  style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
		
<?php
	$query="SELECT distinct date(s.`insertdate`) fdate, 
				DATE_sub(CURDATE(), INTERVAL 1 MONTH) hace1mes, DATE_sub(CURDATE(), INTERVAL 2 MONTH) hace2mes,
				DATE_sub(CURDATE(), INTERVAL 2 DAY) ayer, DATE_sub(CURDATE(), INTERVAL 3 DAY) anteayer				
			FROM controldiario s
			ORDER BY s.`insertdate` desc
			LIMIT 2";
	$res=mysql_query($query)or die($query.' ºº '.mysql_error());
	$arrdata=array();

	$auxurl='tipo=controldiariocolor';
	$i=1;
	
	while($r=mysql_fetch_array($res))
	{
		$arrdata[]=$r;
		$auxurl.='&f'.$i.'='.$r['fdate'];
		$hace1mes=$r['hace1mes'];
		$hace2mes=$r['hace2mes'];
		$ayer=$r['ayer'];
		$anteayer=$r['anteayer'];
		$i++;
	}
	//$auxurl.='&f3=2013-08-01';
	$arrdata[]=array(0=>$ayer, 'fdate' => $ayer);
	$auxurl.='&f3='.$ayer;
	$arrdata[]=array(0=>$anteayer, 'fdate' => $anteayer);
	$auxurl.='&f4='.$anteayer;
/*	
	$arrdata[]=array(0=>$hace1mes, 'fdate' => $hace1mes);
	$auxurl.='&f3='.$hace1mes;
	$arrdata[]=array(0=>$hace2mes, 'fdate' => $hace2mes);
	$auxurl.='&f4='.$hace2mes;
*/	
	
	$arrdata = array_reverse($arrdata);


//echo "<pre>";print_r($arrdata);	echo "</pre>";	
?>

    
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/grid_data2.php?<?php echo $auxurl ?>',
		fields: [
			{name: 'source', type: "string"},
			{name: 'idaction', type: "string"},
			{name: 'action', type: "string"},
			{name: 'f1', type: "float"},
			{name: 'diff1', type: "float"},
			{name: 'f2', type: "float"},
			{name: 'diff2', type: "float"},
			{name: 'f3', type: "float"},
			{name: 'diff3', type: "float"},
			{name: 'f4', type: "float"},
			{name: 'colorrow', type: "string"} ,
			{name: 'porcdiario', type: "float"}  ,
			{name: 'valporc', type: "float"} 
		]
	});
//			,'procode'
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			{
				id: 'del_butt',
                tooltip: 'Delete selected Emails',
				iconCls:'icon',
				icon: 'images/delete.gif',
               // handler: doDel
			}
		]
    });
////////////////FIN barra de pagineo//////////////////////
//////////////////Manejo de Eventos//////////////////////
		
//////////////////FIN Manejo de Eventos//////////////////////
	
///////////////////renders/////////////////////////
	function checkRow(val,p,record){
		return String.format('<input type="checkbox" name="del'+record.data.idaction+'"  id="del'+record.data.idaction+'">',record.data.idaction);
	}

	function changeColor(val, p, record){
        if(Math.abs(val) <= record.data.valporc )
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
		else
			return '<span style="color:red;font-weight: bold;">' + val + '</span>';
			
        return val;
    }

	function render1(val,p,record){
		
		return String.format('<b>{0}</b>',val);
	}
	function render2(val,p,record){
		
		return String.format('<b>{0}</b>',record.data.bdxima.toUpperCase());
	}
///////////////////FIN renders//////////////////////
	
/////////////////Grid//////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		id:"gridpanel",
		store: store,
		iconCls: 'icon-grid',
		columns: [	
			 {header: "idaction", width: 60, align: 'center', sortable: false, dataIndex: 'idaction',renderer: render1}
			,{id:'action',header: "Action", width: 470, align: 'left', sortable: false, dataIndex: 'action',renderer: render1}

			<?php
			$i=4;
			foreach ($arrdata as $v)
			{
				echo ",{header: '".$v['fdate']."', width: 80, sortable: false, align: 'right', dataIndex: 'f".$i."'}";
				if($i>1) echo ",{header: 'Diff ".($i-1)."', width: 60, sortable: false, align: 'right', dataIndex: 'diff".($i-1)."',renderer: changeColor}";
				$i--;
			}
			?>	
			,{header: 'Count', width: 80, sortable: false, align: 'right', dataIndex: 'valporc'}			
		],
		clicksToEdit:2,
		height:470,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		width: screen.width,//'99.8%',
		frame:true,
		title:'Daily Complete',
		loadMask:true,
		tbar: pagingBar 
	});
//			,{header: "Promo. Code", width: 80, align: 'center', sortable: true, dataIndex: 'procode'}
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:200}});
/////////////FIN Inicializar Grid////////////////////
 
 
});
</script>