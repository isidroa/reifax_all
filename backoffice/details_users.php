<?php   
include("conexion.php");
include("../../properties_getprecio.php");

$id=$_POST['userid'];
if(strlen($id)==0)$id=$_GET['userid'];
$caso=$_POST['caso'];
if(strlen($caso)==0)$caso=$_GET['caso'];

$conex=conectar("xima");

switch ($caso) {
	case 'busca':
		$que="SELECT x.PASS, x.NAME, x.SURNAME, x.COUNTRY, x.STATE, x.CITY, x.ADDRESS, x.MOBILETELEPHONE, x.EMAIL, x.AFFILIATIONDATE, x.LicenseN, x.officenum, 
					x.BrokerN, x.procode, x.HOMETELEPHONE, x.executive,(SELECT NAME FROM ximausrs where userid=x.executive) AS nombre, 
					(SELECT SURNAME FROM ximausrs where userid=x.executive) AS apellido, x.zipcode, x.pseudonimo, x.blackList, p.*, a.accept, x.idstatus, x.idusertype, s.status, t.usertype,
					c.cardname, c.cardholdername, c.cardnumber, c.expirationyear, c.expirationmonth, c.csvnumber, c.billingaddress, c.billingcity, c.billingstate, c.billingzip,
				n.notes, n.cancel, n.freeze,(SELECT idrtyp FROM usr_registertype WHERE insertdate=(SELECT max(insertdate) FROM usr_registertype WHERE userid=".$id.") AND userid=x.userid limit 1) AS idrtyp,
				x.questionuser, x.answeruser,j.saldo
				FROM ximausrs x
				LEFT JOIN xima.cobro_porcentajes j on j.userid=x.userid
				LEFT JOIN permission p ON p.userid=x.userid
				LEFT JOIN status s ON x.idstatus=s.idstatus
				LEFT JOIN userterms a ON x.userid=a.userid
				LEFT JOIN usertype t ON x.idusertype=t.idusertype 
				LEFT JOIN creditcard c ON x.userid=c.userid
				LEFT JOIN usernotes n ON x.userid=n.userid
				LEFT JOIN usr_registertype rt ON x.userid=rt.userid
				WHERE x.userid=".$id;
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$valor = mysql_fetch_array($result);

     	$resultado = mysql_query("SELECT DISTINCT fechacobro FROM usr_cobros u, ximausrs x WHERE u.userid=x.userid AND x.userid=".$id);
    	$row = mysql_fetch_row($resultado);

		echo json_encode(array(
				success			=> true,
				fecha_cobro => $row[0],
				precio_producto => getpriceuser($id,0,'yes'),
				USERID			=> $id,
				PASS       		=> $valor['PASS'],
				NAME       		=> $valor['NAME'],
				SURNAME        	=> $valor['SURNAME'],
				COUNTRY        	=> $valor['COUNTRY'],
				STATE        	=> $valor['STATE'],
				CITY        	=> $valor['CITY'],
				ADDRESS        	=> $valor['ADDRESS'],
				HOMETELEPHONE  	=> $valor['HOMETELEPHONE'],
				MOBILETELEPHONE => $valor['MOBILETELEPHONE'],
				EMAIL        	=> $valor['EMAIL'],
				AFFILIATIONDATE => $valor['AFFILIATIONDATE'],
				LicenseN        => $valor['LicenseN'],
				BrokerN        	=> $valor['BrokerN'],
				officenum      	=> $valor['officenum'],
				procode        	=> $valor['procode'],
				nombres      	=> $valor['nombre']." ".$valor['apellido'],
				executive       => $valor['executive'],
				zipcode        	=> $valor['zipcode'],
				pseudonimo     	=> $valor['pseudonimo'],
				idstatus        => $valor['status'],
				idusertype      => $valor['usertype'],
				blackList       => $valor['blackList'],
				status       => $valor['idstatus'],
				tipo  =>  $valor['idusertype'],
				idpermission    => $valor['idpermission'],
				homefinder      => (($valor['homefinder'])==1) ? 'Y':'N',
				buyerspro       => (($valor['buyerspro'])==1) ? 'Y':'N',
				leadsgenerator  => (($valor['leadsgenerator'])==1) ? 'Y':'N',
				residential     => (($valor['residential'])==1) ? 'Y':'N',
				platinum        => (($valor['platinum'])==1) ? 'Y':'N',
				professional    => (($valor['professional'])==1) ? 'Y':'N',
				professional_esp => $valor['professional_esp'],
				realtorweb      => (($valor['realtorweb'])==1) ? 'Y':'N',
				investorweb     => (($valor['investorweb'])==1) ? 'Y':'N',
				masterweb       => (($valor['masterweb'])==1) ? 'Y':'N',
				advertisingweb  => (($valor['advertisingweb'])==1) ? 'Y':'N',
				accept       	=> $valor['accept'],
				cardname       	=> $valor['cardname'],
				cardnumber       => $valor['cardnumber'],
				cardholdername   => $valor['cardholdername'],
				expirationyear   => $valor['expirationyear'],
				expirationmonth  => $valor['expirationmonth'],
				csvnumber       => $valor['csvnumber'],
				billingstate    => $valor['billingstate'],
				billingcity     => $valor['billingcity'],
				billingaddress  => $valor['billingaddress'],
				billingzip      => $valor['billingzip'],
				notes      => $valor['notes'],
				cancel      => $valor['cancel'],
				freeze      => $valor['freeze'],
				idrtyp      => $valor['idrtyp'],
				questionuser      => $valor['questionuser'],
				answeruser      => $valor['answeruser'],
				saldo      => $valor['saldo']
				));
	break;

  	case 'actualizar':
		$PASS  = $_POST['UD_passwd'];
		$NAME       		= $_POST['UD_name'];
		$SURNAME        	= $_POST['UD_surname'];
		$COUNTRY        	= $_POST['UD_country'];
		$STATE        	= $_POST['UD_state'];
		$CITY        	= $_POST['UD_city'];
		$ADDRESS        	= $_POST['UD_address'];
		$HOMETELEPHONE  	= $_POST['UD_hometelephone'];
		$MOBILETELEPHONE = $_POST['UD_mobiletelephone'];
		$EMAIL        	= trim($_POST['UD_email']);
		$AFFILIATIONDATE = $_POST['UD_AFFILIATIONDATE'];
		$LicenseN        = $_POST['UD_licensen'];
		$BrokerN        	= $_POST['UD_brokern'];
		$officenum      	= $_POST['UD_officenum'];
		$procode        	= $_POST['UD_procode'];
		$executive      	= $_POST['UD_executive'];
		$zipcode        	= $_POST['UD_zipcode'];
		$pseudonimo     	= $_POST['UD_pseudonimo'];
		$idstatus        = $_POST['UD_idstatus'];
		$idusertype      = $_POST['UD_idusertype'];
		$blackList       = $_POST['UD_blackList'];
		$accept       	= $_POST['UD_accept'];  
		$mentoring     	= $_POST['UD_mentoring'];
		$bandera     	= $_POST['UD_bandera'];
		$idrtyp     	= $_POST['UD_registertype'];
		$original     	= $_POST['original'];
		$questionuser  	= $_POST['ud_questionuser'];
		$answeruser    = $_POST['answeruser'];

		$que="SELECT p.platinum FROM permission p WHERE p.userid=".$id;
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$plat = mysql_fetch_array($result);
		$platinum = $plat['platinum'];
		
		if($platinum<>1 && $mentoring==1)
		{
			$info = array(
					'success' => false,
					'msg' => "Platinum version is required to be changed to mentoring $platinum && $mentoring"
				);	
		    echo json_encode($info);
			return;
		}
		
		
		if(($idstatus != 2) || ($bandera == $idstatus))
		{
			$que="UPDATE ximausrs x
				INNER JOIN permission p ON x.userid=p.userid				
				INNER JOIN userterms t  ON x.userid=t.userid
				SET x.PASS='$PASS', x.NAME='$NAME', x.SURNAME='$SURNAME', x.COUNTRY='$COUNTRY', x.STATE='$STATE', x.CITY='$CITY', 
						x.ADDRESS='$ADDRESS', x.MOBILETELEPHONE='$MOBILETELEPHONE', x.EMAIL='$EMAIL', x.LicenseN='$LicenseN', 
						x.officenum='$officenum', x.BrokerN='$BrokerN', x.procode='$procode', x.HOMETELEPHONE='$HOMETELEPHONE', 
						x.executive='$executive', x.zipcode='$zipcode', x.pseudonimo='$pseudonimo', t.accept='$accept', x.blackList='$blackList',
						 x.idusertype=$idusertype, p.professional_esp=$mentoring, x.questionuser='$questionuser', x.answeruser='$answeruser'
				WHERE x.userid=$id";//INNER JOIN usr_registertype rt  ON x.userid=rt.userid, rt.idrtyp=$idrtyp x.idstatus=$idstatus,
			$result=mysql_query ($que) or die ($que.mysql_error ());
			if(!$resultado)
			{
				$info = array(
					'success' => true,
					'msg' => 'Se ha Actualizado correctamente'
				);//.$que
			}
			else
			{
				$info = array(
					'success' => false,
					'msg' => 'Se ha producido un error al actualizar el registro'
				);
			}

			/*if ($original != $idrtyp) 
			{
			  $sql="INSERT INTO usr_registertype VALUES ($id,$idrtyp,NOW())";
			  $resultado=mysql_query ($sql) or die ($sql.mysql_error ());
			}*/

		} 
		else 
		{
			$info = array(
				'success' => false,
				'msg' => 'Please suspend user using the button "Suspend Account"'
			);
		}
	    echo json_encode($info);
  	break;

  	case 'tarjeta':
		$cardname       = $_POST['UD_cardname'];
		$cardnumber       = $_POST['UD_cardnumber'];
		$cardholdername   = $_POST['UD_cardholdername'];
		$expirationyear   = $_POST['UD_expirationyear'];
		$expirationmonth  = $_POST['UD_expirationmonth'];
		$csvnumber       = $_POST['UD_csvnumber'];
		$billingstate    = $_POST['UD_billingstate'];
		$billingcity     = $_POST['UD_billingcity'];
		$billingaddress  = $_POST['UD_billingaddress'];
		$billingzip = $_POST['UD_billingzip'];

    	$que="UPDATE creditcard 
				SET cardname='$cardname', cardnumber='$cardnumber', cardholdername='$cardholdername', expirationyear='$expirationyear', 
				expirationmonth='$expirationmonth', csvnumber='$csvnumber', billingstate='$billingstate', billingcity='$billingcity', 
				billingaddress='$billingaddress', billingzip='$billingzip'
				WHERE userid=".$id;
		$result=mysql_query ($que) or die ($que.mysql_error ());

	    if(!$resultado)
		{
			$info = array(
				'success' => true,
				'msg' => 'Se ha Actualizado correctamente'
		    );
		}
		else
		{
			$info = array(
				'success' => false,
				'msg' => 'Se ha producido un error al actualizar el registro'
		    );
		}
	    echo json_encode($info);
  	break;
	
  	case 'note':   
		$notes    = $_POST['UD_notes'];
		$cancel   = $_POST['UD_cancel'];
		$freeze   = $_POST['UD_freeze'];

    	$que="UPDATE usernotes SET notes='$notes', cancel='$cancel', freeze='$freeze' WHERE userid=".$id;
		$result=mysql_query ($que) or die ($que.mysql_error ());

	    if(!$resultado)
		{
			$info = array(
				'success' => true,
				'msg' => 'Se ha Actualizado correctamente'
		    );
		}
		else
		{
			$info = array(
				'success' => false,
				'msg' => 'Se ha producido un error al actualizar el registro'
		    );
		}
	    echo json_encode($info);
	break;

  	case 'changestatusprod':	

		$idproducto=$_POST['idproducto'];
		$statusnew=$_POST['statusnew'];
							
		if($idproducto=='' && strlen($idproducto)==0)
		{
			$update="update xima.ximausrs set idstatus=2 where userid=".$id;
			$res=mysql_query($update) or die("{success:false, msg:'ERROR :".mysql_error()."'}");

			$update="UPDATE usr_cobros u
				   INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				   INNER JOIN usr_producto p on p.idproducto=b.idproducto
				   SET u.idstatus=2
				   WHERE u.`userid`=$id and p.pu=0";
			mysql_query($update) or  die("{success:false, msg:'ERROR :".mysql_error()."'}");
			
			$cancelacion=trim($_POST['msj']);
			$update="update xima.usernotes set cancel='$cancelacion' where userid=".$id;
			mysql_query($update)  or die("{success:false, msg:'ERROR :".mysql_error()."'}");
		
			$sql='SELECT NAME,SURNAME,EMAIL,HOMETELEPHONE FROM ximausrs WHERE userid='.$id;
			$resultado=mysql_query($sql) or die("ERROR :".mysql_error());
			$datos_usr=mysql_fetch_array($resultado);      
			
			$insert="insert into anexos.contactus (source,name,email,company,phone,city,comment,emaildate)  
					Values ('CANCELACCOUNT','".$datos_usr['NAME']." ".$datos_usr['SURNAME']."','".$datos_usr['EMAIL'].
					"','','".$datos_usr['HOMETELEPHONE']."','','".$cancelacion."','".date('Y-m-d H:i:s')."')";
			mysql_query($insert) or die("{success:false, msg:'ERROR :".mysql_error()."'}");
			  
			//Actualiza el historico del cliente para que su saldo quede en 0
			$query="SELECT * FROM xima.cobro_estadocuenta h
					WHERE h.`userid`=".$id."
					ORDER BY h.`fechatrans` desc, h.`idtrans` desc limit 1";
			$res=mysql_query($query)  or die("{success:false, msg:'ERROR :".mysql_error()."'}");
			$row=mysql_fetch_array($res);
			//Tiene el cobro del mes
				
			if($row['idoper']==4){
				$queryIns="INSERT INTO xima.cobro_estadocuenta (idtrans,fechatrans,idoper,userid,monto,notas)
					VALUES (null,now(),13,".$id.",".$row['monto']*(-1).",'Credit suspended user')";
				mysql_query($queryIns)  or die("{success:false, msg:'ERROR :".mysql_error()."'}");
			}
			elseif($row['idoper']==2)
			{
				if($row['monto']<>0) 
					$row['monto']=abs($row['monto']);
					
				$queryIns="INSERT INTO xima.cobro_estadocuenta (idtrans,fechatrans,idoper,userid,monto,notas)
							VALUES (null,now(),13,".$id.",".$row['monto'].",'Credit suspended user')";
				mysql_query($queryIns)  or die("{success:false, msg:'ERROR :".mysql_error()."'}");
			}
			$queryIns=" UPDATE xima.cobro_porcentajes SET saldo=0 WHERE userid=".$id;
			mysql_query($queryIns)  or die("{success:false, msg:'ERROR :".mysql_error()."'}");;//ACTUALIZANDO EL SALDO DEL USUARIO CON EL SALDO FINAL, COMO SU NUEVO SALDO	  		
		}		
		else
		{
			$update="update xima.ximausrs set idstatus=$statusnew where userid=".$id;
			$res=mysql_query($update) or die("{success:false, msg:'ERROR :".mysql_error()."'}");
			
			if( $idproducto==1 || $idproducto==2 || $idproducto==3 )$idproducto='1,2,3';

			$update="UPDATE usr_cobros u
				   INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				   INNER JOIN usr_producto p on p.idproducto=b.idproducto
				   SET u.idstatus=$statusnew 
				   WHERE u.`userid`=$id and p.pu=0 and p.idproducto in ($idproducto)";
			mysql_query($update) or  die("{success:false, msg:'ERROR :".mysql_error()."'}");		
		}
		
		echo "{success:true, msg:'It has been updated correctly', payamount:'".getpriceuser($id,0,'yes')."'}";			
	
		//echo json_encode($msg);
	break;
	
  	case 'reactivateaccount':   
//return;	
		$ID=$_POST['userid'];
		$producto=$_POST['cbGrupoProducts'];
		$frecuencia=$_POST['cbGrupoFrencuency'];
		$setstate=$_POST['cbGrupoState'];
		$setgroup=$_POST['cbGrupoCounty'];
		$promotioncode=$_POST['cbpexecutive'];
		
		$montoapagar=str_replace(',','',$_POST['prodamount']);
		$prodbillingaddress=$_POST['prodbillingaddress'];
		$prodbillingcity=$_POST['prodbillingcity'];
		$prodbillingstate=$_POST['prodbillingstate'];
		$prodbillingzip=$_POST['prodbillingzip'];
		$prodcardholdername=$_POST['prodcardholdername'];
		$prodcardname=$_POST['prodcardname'];
		$prodcardnumber=$_POST['prodcardnumber'];
		$prodcsvnumber=$_POST['prodcsvnumber'];
		$prodexpirationmonth=$_POST['prodexpirationmonth'];
		$prodexpirationyear=$_POST['prodexpirationyear'];

		$query="update xima.ximausrs set idstatus=3 where userid=$ID";	
		mysql_query($query) or die($query.mysql_error());
		
		$query="UPDATE xima.usernotes  
				SET notes=concat( notes,' ',CURDATE(),' Reactivate Account')
				WHERE userid=$ID";	
		mysql_query($query) or die($query.mysql_error());

		$query="UPDATE xima.creditcard 
				SET cardname='$prodcardname',
					cardholdername='$prodcardholdername',
					cardnumber='$prodcardnumber',
					expirationyear='$prodexpirationyear',
					expirationmonth='$prodexpirationmonth',
					csvnumber='$prodcsvnumber',
					billingaddress='$prodbillingaddress',
					billingcity='$prodbillingcity',
					billingstate='$prodbillingstate',
					billingzip='$prodbillingzip'
				WHERE userid=$ID";	
		mysql_query($query) or die($query.mysql_error());
			
		$queryIns="DELETE FROM xima.usr_registertype WHERE idrtyp=1 and userid=$ID";
		mysql_query($queryIns) or die($queryIns.mysql_error());
		$queryIns="INSERT INTO xima.usr_registertype(userid,idrtyp,insertdate)VALUES($ID,1,NOW())";//Customer Register
		mysql_query($queryIns) or die($queryIns.mysql_error());			
		$queryIns="DELETE FROM xima.userstate WHERE userid=$ID";
		mysql_query($queryIns) or die($queryIns.mysql_error());
		$queryIns="DELETE FROM xima.usercounty WHERE userid=$ID";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$queryIns="DELETE FROM xima.usr_cobros WHERE userid=$ID";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$queryIns="DELETE FROM xima.f_frecuency WHERE userid=$ID";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$query="DELETE FROM xima.permission WHERE userid=$ID";
		mysql_query($query) or die($query.mysql_error());
		$query="DELETE FROM xima.f_discount WHERE userid=$ID";
		mysql_query($query) or die($query.mysql_error());

		unset($idproducto,$idfrecuencia,$precioproducto);
		$platinum=$professional=$homefinder=$buyerspro=$leadsgenerator=$residential=$professional=$webreal=$investorweb=$adverrealtorweb=0;
		
		$producto=$_POST['cbGrupoProducts'];
		$frecuencia=$_POST['cbGrupoFrencuency'];
		switch($producto)
		{
			case 1://'Residential Pro'
				$residential=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1);
				$idfrecuencia=array($frecuencia);
			break;		
			case 2://'Platinum'
				$platinum=1;$residential=1;$homefinder=1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2);
				$idfrecuencia=array($frecuencia,$frecuencia);
			break;		
			case 3://'Professional'
				$professional=1;$platinum=1;$residential=1;$homefinder =1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2,3);
				$idfrecuencia=array($frecuencia,$frecuencia,$frecuencia);
			break;	
			case 6://'Buyers Pro'
				$buyerspro=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 4://'Webs'
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 7://'Home Finder'
				$homefinder=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 8://'Leads Generator'
				$leadsgenerator=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
		}
		$freedays=0;
		//------------ Registrando los productos del cliente y frecuencia de los mismos para la nueva modalidad de cobranza y productos
		foreach($idproducto as $k => $val)
		{
			if($k==0){
				$query='INSERT INTO xima.usr_cobros (userid,idproductobase,fechacobro,idstatus,freedays) VALUES ';
				$query2='INSERT INTO xima.f_frecuency (userid,idfrecuency,idproducto) VALUES ';
			}else{
				$query.=',';
				$query2.=',';
			}
			$query.='('.$ID.',(SELECT idproductobase FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val.'),NOW(),3,'.$freedays.')';
			$query2.='('.$ID.','.$idfrecuencia[$k].','.$val.')';
			
			$queryp = 'SELECT precio FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val;
			$resultp = mysql_query($queryp) OR die($queryp.mysql_error());
			$rp = mysql_fetch_array($resultp);
			$precioproducto[]=$rp[0];

			$setstate=$_POST['cbGrupoState'];
			$setgroup=$_POST['cbGrupoCounty'];
			//--------- TRANSFORMANDO EL SETSTATE para la nueva modalidad de cobranza y productos, Y LLEVARLO A LA NUEVA TABLA xima.userstate
			if($setstate=='ALL')
			{
				$queryp="SELECT l.`abrState` FROM xima.lsstate l WHERE l.`is_showed`='Y'";
				$result=mysql_query($queryp) or die($queryp.mysql_error());					
				$cad='';
				$p=0;
				while($row=mysql_fetch_array($result, MYSQL_ASSOC))
				{
					if($p>0)$cad.=',';
					$cad.=$row["abrState"];
					$p++;
				}
				$setstate=$cad;
			}
			unset($arrstate);
			$arrstate=explode(',',$setstate);
			foreach($arrstate as $val9)
			{
				$allcou='0';
				if($setgroup=='ALL')$allcou='1';
				
				$queryp="INSERT INTO xima.userstate (`userid`, `idstate`, `all`,defstate,idproducto)VALUES($ID,$val9,$allcou,1,$val)";
				mysql_query($queryp) or die($queryp.mysql_error());	
			}
			//--------- TRANSFORMANDO EL SETGROUP para la nueva modalidad de cobranza y productos, Y LLEVARLO A LA NUEVA TABLA xima.userstate
			if($setgroup=='ALL')
			{
				$wherecurcty=' f.`countCounty`=999';
				$queryp="SELECT l.`idstate` FROM xima.userstate l WHERE l.`userid`=$ID";
				$result=mysql_query($queryp) or die($queryp.mysql_error());					
				$cadsta='';
				$p=0;
				while($row=mysql_fetch_array($result, MYSQL_ASSOC))
				{
					if($p>0)$cadsta.=',';
					$cadsta.=$row["idstate"];
					$p++;
				}
				
				$queryp="SELECT l.`idcounty` FROM xima.lscounty l WHERE l.`idstate` in ($cadsta)";
				$result2=mysql_query($queryp) or die($queryp.mysql_error());							
				$cadcounty='';
				$p=0;
				while($row2=mysql_fetch_array($result2, MYSQL_ASSOC))
				{
					if($p>0)$cadcounty.=',';
					$cadcounty.=$row2["idcounty"];
					$p++;
				}
				$setgroup=$cadcounty;
			}
			else
				$wherecurcty=' f.`countCounty`=1';
			unset($arrcounty);
			$arrcounty=explode(',',$setgroup);
			foreach($arrcounty as $val8)
			{
				$queryp="INSERT INTO xima.usercounty (`userid`, `idcounty`,defcounty,idproducto)VALUES($ID,$val8,1,$val)";
				mysql_query($queryp) or die($queryp.mysql_error());
			}			
			
		}
		mysql_query($query) OR die($query.mysql_error());
		mysql_query($query2) OR die($query2.mysql_error());

		//--------- LLENADO DE LAS TABLAS permission 
		$query="INSERT INTO xima.permission (homefinder,buyerspro,leadsgenerator,residential,platinum,professional,professional_esp,realtorweb,investorweb, masterweb,advertisingweb,userid)
				VALUES ($homefinder,$buyerspro,$leadsgenerator,$residential,$platinum,$professional,0,$webreal,$investorweb,0,$adverrealtorweb,$ID);";
		mysql_query($query) or die($query.mysql_error());
		
		//------------------------------------------------------------------
		//Registrando los descuentos de los productos para la nueva modalidad de cobranza y productos
		unset($iddescuento,$estados);
		foreach($idproducto as $k => $val)
		{
			if($promotioncode<>'')
			{
				$query = "SELECT f.`descuento`
							FROM xima.f_countyprecio f
							INNER JOIN xima.usr_productobase u ON (f.`idproductobase`=u.`idproductobase`)
							WHERE u.`activo`=1 and u.`idproducto`=$val and f.`idstate`=$setstate and $wherecurcty
							LIMIT 1";
				$result = mysql_query($query) or die(mysql_error());
				$row=mysql_fetch_array($result, MYSQL_ASSOC);
				$descuento=$row["descuento"];
			}
			else
				$descuento=1;

			$query='INSERT INTO xima.f_discount (userid,idproducto,discount) VALUES ('.$ID.','.$val.','.$descuento.')';
			mysql_query($query) OR die($query.mysql_error());
		}		
		
		//----------------------------------------------------------------------------------
		$hora=date('H:i:s');
		$query="DELETE FROM xima.cobro_porcentajes WHERE userid=$ID";
		mysql_query($query) or die($query.mysql_error());
		$query="Insert into xima.cobro_porcentajes (userid,saldo,pctgu,pctgc,mesu,pctgr,pctgr2lc,pctgr2lv,freedays)
				VALUES($ID,".($montoapagar*-1).",50,0,2,20,10,100,0)";
		$res=mysql_query($query) or die($query.mysql_error());//INSERTANDO EN SALDOPORCENTAJE
		
		$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`)
				VALUES ($ID,NOW(),".($montoapagar*-1).",4)";
		mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO	
		echo "{success:true, msg:'Reactivation account successfully'}";			
	break;

  	case 'usersproducts':
	
		$que="	SELECT u.idcobros,u.userid,x1.name,x1.surname,s.idstatus,s.status,p.idproducto,p.name nameprod,
					0 as priceprod, u.fechacobro,u.freedays,y.name namefrec,d.discount,
					ls.state,
					(SELECT count(*) cant FROM usercounty c WHERE c.`userid`=u.USERID and c.idproducto=p.idproducto GROUP BY p.idproducto ) counties 
				FROM xima.usr_cobros u
				INNER JOIN xima.ximausrs x1 ON u.userid=x1.userid
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				INNER JOIN xima.status s ON s.idstatus=u.idstatus
				INNER JOIN f_frecuency f on (f.idproducto=p.idproducto and u.userid=f.userid)
				INNER JOIN frecuency y on (y.idfrecuency=f.idfrecuency)
				INNER JOIN f_discount d ON (d.idproducto=p.idproducto and d.userid=u.USERID)
				INNER JOIN userstate ue ON (ue.idproducto=p.idproducto and ue.userid=u.USERID)
				INNER JOIN lsstate ls ON (ls.idstate=ue.idstate)
				where u.userid=$id
				ORDER BY p.idproducto";
		$result=mysql_query ($que) or die ($que.mysql_error ());

		$data = array();  
		while($valor = mysql_fetch_array($result))
		{  
		    /*array_push($data, array(  
				name		=> $valor['name'],
				fechacobro		=> $valor['fechacobro']
			));  */
			$valor['priceprod']=getpriceuser($id,0,'no',$valor['idproducto']);
			$data[]=$valor;
		}  

		echo json_encode(array(
			'data'=>$data
		));
	break;

  	case 'userscounties':
		$idproducto=$_POST['idproducto'];
		if(strlen($idproducto)==0)$idproducto=$_GET['idproducto'];	

		$data = array();  
		if(strlen($idproducto)>0 && $idproducto>0)
		{
			$que="	SELECT ls.idcounty,ls.county, '' editcounty,p.idproducto  
					FROM xima.usr_cobros u
					INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
					INNER JOIN usr_producto p on p.idproducto=b.idproducto
					INNER JOIN usercounty ue ON (ue.idproducto=p.idproducto and ue.userid=u.userid)
					INNER JOIN lscounty ls ON (ls.idcounty=ue.idcounty)
					where u.userid=$id AND p.idproducto=$idproducto
					order by ls.county";
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$num_rows = mysql_num_rows($result);
			while($valor = mysql_fetch_array($result))
			{
				if($num_rows ==1)$valor['editcounty']='<a href="javascript:void(0)" class="itemusers" title="Click to edit the county." ><img src="images/editar.png" border="0"></a>';	
				$data[]=$valor;				
			}
		}
		echo json_encode(array(
			'data'=>$data
		));
	break;

  	case 'changecounty':
		$idproducto=$_POST['idproducto'];
		if(strlen($idproducto)==0)$idproducto=$_GET['idproducto'];	
		$cbcountyedit=$_POST['cbcountyedit'];

		$aux=$idproducto;
		if($aux==1 || $aux==2 || $aux==3 ) $aux='1,2,3';
		
		$que="	UPDATE usercounty u  SET u.`idcounty`=$cbcountyedit where u.userid=$id AND u.idproducto in ($aux)";
		$resultado=mysql_query ($que) or die ($que.mysql_error ());

		$info = array(
			'success' => true,
			'msg' => 'Change county succesfully '
		);
		echo json_encode($info);		
			
	break;
	
  	case 'updateproducts':
		$ID=$_POST['userid'];
		$promotioncode=$_POST['cbpexecutivep'];
		$producto=$_POST['cbGrupoProductsp'];
		$montoapagar=str_replace(',','',$_POST['prodamountp']);

		$aux=$producto;
		if($aux==1 || $aux==2 || $aux==3 ) $aux='1,2,3';

		$que="SELECT idstatus FROM usr_cobros u WHERE u.`userid`=$ID";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$valor = mysql_fetch_array($result);
		$idstat=$valor['idstatus'];		
		
		$queryIns="DELETE FROM xima.userstate WHERE userid=$ID and idproducto in ($aux)";
		mysql_query($queryIns) or die($queryIns.mysql_error());
		$queryIns="DELETE FROM xima.usercounty WHERE userid=$ID and idproducto in ($aux)";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$queryIns="DELETE usr_cobros u
					FROM usr_cobros u
					INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
					WHERE u.`userid`=$id and b.idproducto in ($aux)";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$queryIns="DELETE FROM xima.f_frecuency WHERE userid=$ID and idproducto in ($aux)";
		mysql_query($queryIns) or die($queryIns.mysql_error());		
		$query="DELETE FROM xima.f_discount WHERE userid=$ID and idproducto in ($aux)";
		mysql_query($query) or die($query.mysql_error());
		$query="DELETE FROM xima.permission WHERE userid=$ID";
		mysql_query($query) or die($query.mysql_error());

		unset($idproducto,$idfrecuencia,$precioproducto);
		$platinum=$professional=$homefinder=$buyerspro=$leadsgenerator=$residential=$professional=$webreal=$investorweb=$adverrealtorweb=0;

		///dejando los permisos de los productos que quedaron
		$que="	SELECT p.idproducto 
				FROM usr_cobros u
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				WHERE u.`userid`=$ID";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		while($valor = mysql_fetch_array($result))
		{
			$pr=$valor['idproducto'];
			if($pr==1)$residential=1;
			if($pr==6)$buyerspro=1;
			if($pr==7)$homefinder=1;
			if($pr==8)$leadsgenerator=1;
		}		
		
		$frecuencia=$_POST['cbGrupoFrencuencyp'];
		switch($producto)
		{
			case 1://'Residential Pro'
				$residential=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1);
				$idfrecuencia=array($frecuencia);
			break;		
			case 2://'Platinum'
				$platinum=1;$residential=1;$homefinder=1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2);
				$idfrecuencia=array($frecuencia,$frecuencia);
			break;		
			case 3://'Professional'
				$professional=1;$platinum=1;$residential=1;$homefinder =1;$buyerspro=1;$leadsgenerator=1;
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array(1,2,3);
				$idfrecuencia=array($frecuencia,$frecuencia,$frecuencia);
			break;	
			case 6://'Buyers Pro'
				$buyerspro=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 4://'Webs'
				if($idusertype==5)$investorweb=1; else $webreal=1;				
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 7://'Home Finder'
				$homefinder=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
			case 8://'Leads Generator'
				$leadsgenerator=1;
				$idproducto=array($producto);
				$idfrecuencia=array($frecuencia);
			break;	
		}
		$freedays=0;
		//------------ Registrando los productos del cliente y frecuencia de los mismos para la nueva modalidad de cobranza y productos
		foreach($idproducto as $k => $val)
		{
			if($k==0){
				$query='INSERT INTO xima.usr_cobros (userid,idproductobase,fechacobro,idstatus,freedays) VALUES ';
				$query2='INSERT INTO xima.f_frecuency (userid,idfrecuency,idproducto) VALUES ';
			}else{
				$query.=',';
				$query2.=',';
			}
			$query.='('.$ID.',(SELECT idproductobase FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val.'),NOW(),'.$idstat.','.$freedays.')';
			$query2.='('.$ID.','.$idfrecuencia[$k].','.$val.')';
			
			$queryp = 'SELECT precio FROM xima.usr_productobase WHERE activo=1 AND idproducto='.$val;
			$resultp = mysql_query($queryp) OR die($queryp.mysql_error());
			$rp = mysql_fetch_array($resultp);
			$precioproducto[]=$rp[0];

			$setstate=$_POST['cbGrupoStatep'];
			$setgroup=$_POST['cbGrupoCountyp'];
			//--------- TRANSFORMANDO EL SETSTATE para la nueva modalidad de cobranza y productos, Y LLEVARLO A LA NUEVA TABLA xima.userstate
			if($setstate=='ALL')
			{
				$queryp="SELECT l.`abrState` FROM xima.lsstate l WHERE l.`is_showed`='Y'";
				$result=mysql_query($queryp) or die($queryp.mysql_error());					
				$cad='';
				$p=0;
				while($row=mysql_fetch_array($result, MYSQL_ASSOC))
				{
					if($p>0)$cad.=',';
					$cad.=$row["abrState"];
					$p++;
				}
				$setstate=$cad;
			}
			unset($arrstate);
			$arrstate=explode(',',$setstate);
			foreach($arrstate as $val9)
			{
				$allcou='0';
				if($setgroup=='ALL')$allcou='1';
				
				$queryp="INSERT INTO xima.userstate (`userid`, `idstate`, `all`,defstate,idproducto)VALUES($ID,$val9,$allcou,1,$val)";
				mysql_query($queryp) or die($queryp.mysql_error());	
			}
			//--------- TRANSFORMANDO EL SETGROUP para la nueva modalidad de cobranza y productos, Y LLEVARLO A LA NUEVA TABLA xima.userstate
			if($setgroup=='ALL')
			{
				$wherecurcty=' f.`countCounty`=999';
				$queryp="SELECT l.`idstate` FROM xima.userstate l WHERE l.`userid`=$ID";
				$result=mysql_query($queryp) or die($queryp.mysql_error());					
				$cadsta='';
				$p=0;
				while($row=mysql_fetch_array($result, MYSQL_ASSOC))
				{
					if($p>0)$cadsta.=',';
					$cadsta.=$row["idstate"];
					$p++;
				}
				
				$queryp="SELECT l.`idcounty` FROM xima.lscounty l WHERE l.`idstate` in ($cadsta)";
				$result2=mysql_query($queryp) or die($queryp.mysql_error());							
				$cadcounty='';
				$p=0;
				while($row2=mysql_fetch_array($result2, MYSQL_ASSOC))
				{
					if($p>0)$cadcounty.=',';
					$cadcounty.=$row2["idcounty"];
					$p++;
				}
				$setgroup=$cadcounty;
			}
			else
				$wherecurcty=' f.`countCounty`=1';
			unset($arrcounty);
			$arrcounty=explode(',',$setgroup);
			foreach($arrcounty as $val8)
			{
				$queryp="INSERT INTO xima.usercounty (`userid`, `idcounty`,defcounty,idproducto)VALUES($ID,$val8,1,$val)";
				mysql_query($queryp) or die($queryp.mysql_error());
			}			
			
		}
		mysql_query($query) OR die($query.mysql_error());
		mysql_query($query2) OR die($query2.mysql_error());

		//--------- LLENADO DE LAS TABLAS permission 
		$query="INSERT INTO xima.permission (homefinder,buyerspro,leadsgenerator,residential,platinum,professional,professional_esp,realtorweb,investorweb, masterweb,advertisingweb,userid)
				VALUES ($homefinder,$buyerspro,$leadsgenerator,$residential,$platinum,$professional,0,$webreal,$investorweb,0,$adverrealtorweb,$ID);";
		mysql_query($query) or die($query.mysql_error());
		
		//------------------------------------------------------------------
		//Registrando los descuentos de los productos para la nueva modalidad de cobranza y productos
		unset($iddescuento,$estados);
		foreach($idproducto as $k => $val)
		{
			if($promotioncode<>'')
			{
				$query = "SELECT f.`descuento`
							FROM xima.f_countyprecio f
							INNER JOIN xima.usr_productobase u ON (f.`idproductobase`=u.`idproductobase`)
							WHERE u.`activo`=1 and u.`idproducto`=$val and f.`idstate`=$setstate and $wherecurcty
							LIMIT 1";
				$result = mysql_query($query) or die(mysql_error());
				$row=mysql_fetch_array($result, MYSQL_ASSOC);
				$descuento=$row["descuento"];
			}
			else
				$descuento=1;

			$query='INSERT INTO xima.f_discount (userid,idproducto,discount) VALUES ('.$ID.','.$val.','.$descuento.')';
			mysql_query($query) OR die($query.mysql_error());
		}		
		//----------------------------------------------------------------------------------
		echo "{success:true, msg:'Change products successfully', payamount:'".getpriceuser($ID,0,'yes')."'}";					
	break;

  	case 'userslogsprod':
		$data = array();  
		$que="	SELECT u.userid,p.name,u.fechaupd,u.fechacobro
				FROM logsusr_cobros u
				INNER JOIN usr_productobase b on b.idproductobase=u.idproductobase
				INNER JOIN usr_producto p on p.idproducto=b.idproducto
				WHERE u.`userid`=$id 
				order by u.idlogsusr_cobros";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$num_rows = mysql_num_rows($result);
		while($valor = mysql_fetch_array($result))
			$data[]=$valor;				
		echo json_encode(array(
			'data'=>$data
		));
	break;
	
  	case 'userslogsPermission':
		$data = array();  
		$que="	SELECT c.`userid`, ucase(c.`detail`) detail, if(c.`valor`=0,'Delete','Add') perm, c.`fecha`
				FROM logspermission c
				WHERE `userid`=$id 
				order by idlogspermission";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$num_rows = mysql_num_rows($result);
		while($valor = mysql_fetch_array($result))
			$data[]=$valor;				
		echo json_encode(array(
			'data'=>$data
		));
	break;
	
  	case 'userslogsStatus':
		$data = array();  
		$que="	SELECT l.`fechaupd`, l.`userid`,s.status
				FROM logsstatus l
				INNER JOIN `status` s ON l.statusafter=s.idstatus
				WHERE l.`userid`=$id 
				order by l.`idstatuslogs`";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$num_rows = mysql_num_rows($result);
		while($valor = mysql_fetch_array($result))
			$data[]=$valor;				
		echo json_encode(array(
			'data'=>$data
		));
	break;
	
  	case 'usershistorypay':
		$data = array();  
		$que="	SELECT `paypalid`, `userid`, ucase(`estado`) estado, `transactionID`, `amount`, `fecha`, concat_ws(' ',`errorNum`, `errorMsj`, `errorMsjL`) error, `description`
				FROM `cobro_paypal`
				WHERE `userid`=$id 
				order by paypalid";
		$result=mysql_query ($que) or die ($que.mysql_error ());
		$num_rows = mysql_num_rows($result);
		while($valor = mysql_fetch_array($result))
			$data[]=$valor;				
		echo json_encode(array(
			'data'=>$data
		));
	break;
}

$caso=$_GET['caso'];
if(isset($caso))
{
	$id=$_GET['userid'];
	if ($caso == 'sesionip')
    { 
			$que="SELECT day(sessiondate) as dia, sessiondate, login_ip FROM logssession l 
			 where sessiondate BETWEEN DATE_ADD(NOW(), INTERVAL -6 MONTH) AND NOW()
			 and userid=$id order by sessiondate";
			$result=mysql_query ($que) or die ($que.mysql_error ());
			$data= array();
			while($row= mysql_fetch_array($result)){
				array_push($data,array(
					"fechgrup0"	=> $row["dia"],
					"sessiondate"	=> $row["sessiondate"], 
					"login_ip"	=> $row["login_ip"]
				));
			}
			echo json_encode(
				array(  
				"data"	=> $data
			));     
	}
}

?>

