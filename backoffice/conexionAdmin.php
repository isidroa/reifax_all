<?php 
include ("php/checkuser.php");  
include($_SERVER["DOCUMENT_ROOT"]."/connection.php");
session_start();
/*
if(!in_array($bkouserid, array(20,73,5,2544,4,2482)))
{
	header('Location: controlmain.php');
}
*/
header('Location: conexionAdmin9.php');

$Connect	=	new connect();
$Connect->connectInit();

//echo '<br>line 7:'.date('Y-m-d H:i:s');
$conex=$Connect->mysqlByServer(array('serverNumber' => '8'));

if(strlen($bkouserid)==0)$bkouserid=$_COOKIE['bkouseridc'];
function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
       
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
    return $_SERVER['REMOTE_ADDR'];
}
$bkouserid=$_SESSION['bkouserid'];
$sql="SELECT * FROM xima.cobro_conections_hours where userid={$bkouserid}";
$result=$conex->query ($sql) or die ($sql.$conex->error);
$userHours=$result->fetch_object();

if($userHours->break){
	$i = getdate();
	$out ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userHours->hour_end}";
	$initBreak ="{$i['year']}-{$i['mon']}-{$i['mday']}T{$userHours->hour_break_init}";
	
	
	$timezoneG = new DateTimeZone('America/New_York');
	$timezoneC = new DateTimeZone('America/Caracas');
	$dateAux = new DateTime('NOW', $timezoneC);
	$dateAux->setTimezone($timezoneG);
	$initBreak = new DateTime($initBreak, $timezoneC);
	$initBreak->setTimezone($timezoneG);
	$dif=($initBreak->format('U'))-($dateAux->format('U'));
	if($dif<0){
		$initBreak = new DateTime($out, $timezoneC);
		$initBreak->setTimezone($timezoneG);
		$dif=($initBreak->format('U'))-($dateAux->format('U'));
	}
}
$que="SELECT `USERID`, `NAME`, `SURNAME` FROM `xima`.`ximausrs`
		WHERE `USERID`=$bkouserid";
$result=$conex->query ($que) or die ($que.$conex->error);
$row=$result->fetch_object();
$nameuser=$row->NAME.' '.$row->SURNAME;
$conex->query ($que);


$sql="SELECT * FROM cobro_connections_config where id=1";
$result=$conex->query($sql) or die($sql.$conex->error);
$config=$result->fetch_assoc();
	
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<TITLE>Control Connections</TITLE> 
<link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<link href="css/redmond/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.print.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
<style>
	.red-row{
		background:#FADBD3;
	}
	.red-row:hover{
		background:#F4DDDD;
	}
	.green-row{
		background:#E9FFCC;
	}
	.green-row:hover{
		background:#D1FE97;
	}
	.yellow-row{
		background:#E5BE47;
	}
	.yellow-row:hover{
		background:#F4C614;
	}
</style>
<?php 	include("php/enablebuttons.php");?>

<link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="includes/ext/ext-all.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.min.js"></script>
<script language="javascript">
    var ipclient='<?php echo getRealIP() ?>';	
    var userid='<?php echo $bkouserid ?>';	
    var username='<?php echo $nameuser ?>';	
	var refreshConex=<?php echo $config['value'] ?>*1000;
	
Ext.namespace('Ext.combos_selec');
	
	
 Ext.combos_selec.dataUsersAdmin = [
 <?php 
	$res=$conex->query("SELECT u.`userid`, u.name ,u.surname FROM cobro_conections_hours h
					join `ximausrs` u on u.userid=h.userid ORDER BY u.name")or die($conexerror());
	$i=0;
	echo "['','NONE']";
	while($r=$res->fetch_array())
		echo ",[".$r['userid'].",'".$r['name']." ".$r['surname']." (".$r['userid'].")']";
 ?>];
	<?php
		if($dif>0){
			$dif=($dif+300)*1000;
			echo 'var dif='.$dif.';';
			?>
				setTimeout(function (){
					window.focus();
					window.self.focus();
					if(ACTIVATE){
						var refreshPag=	setTimeout(function (){
							ACTIVATE=0;
							Ext.util.Cookies.set('activeConexion',false);
							location.reload();
						},60000);
						Ext.Msg.show({
						   scope:this,
						   msg: 'Desea Continuar trabajando?',
						   buttons: Ext.Msg.YESNO,
						   fn: function (btn){
						   		if(btn=='yes'){ 
									clearTimeout(refreshPag);
						   		}
						   		else{
						   			
						   			ACTIVATE=0;
									Ext.util.Cookies.set('activeConexion',false);
									Ext.Ajax.request({  
										waitMsg: 'Loading...',
										url: 'php/funcionesControlConex.php', 
										scope:this,
										method: 'POST', 
										//timeout: 100000,
										params: { 
											userid 	: userid,
											opcion	:'desconectUser'
										},
										success:function (){
											var clientdate=new Date().format('Y-m-d H:i:s');
											Ext.getCmp('btnConnect').setText('<b>CONNECT</b>');
											Ext.getCmp('btnConnect').setIcon('images/play.png');
										}
									});
						   			
						   		}
						   },
						   icon: Ext.MessageBox.QUESTION
						});
						
					}
				},dif)
			<?php
		}
	?>
	
	
</script>

<script type="text/javascript" src="js/menu.js"></script>

<script type="text/javascript" src="js/conexionAdmin.js"></script>
	
</head>

<body>
</body>
</html>