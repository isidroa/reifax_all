<?php
include ("php/checkuser.php");  
include("php/conexion.php");
$conex=conectar("xima");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>AUCTION DIARY</title>
    <link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
	<?php 	include("php/enablebuttons.php");?>
    <link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/CheckColumn.js"></script>
	
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
</head>
<body>
</body>
</html>
<script>
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

	
/////////////////FIN Variables////////////////////
	var winLoad=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div id="resultupload" style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
	var winLoadw=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div  style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
		
<?php
	$query="SELECT distinct date(s.`insertdate`) fdate, DATE_sub(CURDATE(), INTERVAL 1 MONTH) hace1mes, DATE_sub(CURDATE(), INTERVAL 2 MONTH) hace2mes 
			FROM statusadiarios s
			ORDER BY s.`insertdate` desc
			LIMIT 2";
	$res=mysql_query($query)or die($query.' ºº '.mysql_error());
	$arrdata=array();

	$auxurl='tipo=auctiondiariocolor';
	$i=1;
	
	while($r=mysql_fetch_array($res))
	{
		$arrdata[]=$r;
		$auxurl.='&f'.$i.'='.$r['fdate'];
		$hace1mes=$r['hace1mes'];
		$hace2mes=$r['hace2mes'];
		$i++;
	}
	//$auxurl.='&f3=2013-08-01';
	
	$arrdata[]=array(0=>$hace1mes, 'fdate' => $hace1mes);
	$auxurl.='&f3='.$hace1mes;
	$arrdata[]=array(0=>'2013-09-20', 'fdate' => '2013-09-20');
	$auxurl.='&f4=2013-09-20';	
	$arrdata = array_reverse($arrdata);


//echo "<pre>";print_r($arrdata);	echo "</pre>";	
?>
    

///////////Cargas de data dinamica///////////////


 
			
// shared reader
    var reader = new Ext.data.JsonReader({
					successProperty	: 'success',
					messageProperty	: 'message',
					idProperty	: 'idcounty',
					root		: 'data'
				}, [
			{name: 'servernum', type: "int"},
			{name: 'server', type: "string"},
			{name: 'idcounty', type: "string"},
			{name: 'county', type: "string"},
			{name: 'f1', type: "int"},
			{name: 'diff1', type: "int"},
			{name: 'f2', type: "int"},
			{name: 'diff2', type: "int"},
			{name: 'f3', type: "int"},
			{name: 'diff3', type: "int"},
			{name: 'f4', type: "int"},
			{name: 'valporc', type: "int"},
			{name: 'porcdiario', type: "float"},
			{name: 'checking', type: 'bool'},
			{name: 'bdxima3', type: "string"},
			{name: 'bdxima', type: "string"},
			{name: 'colorrow', type: "string"} 
    ]);

    var store = new Ext.data.GroupingStore({
            reader: reader,
			url: 'php/grid_data2.php?<?php echo $auxurl ?>',
            sortInfo:{field: 'servernum', direction: "ASC"},
            groupField:'server'
       });
		

///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 200,
        store: store,
        displayInfo: false,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display",
		items:[
			{
				id: 'del_butt',
                tooltip: 'Upload counties selected',
				iconCls:'icon',
				icon: 'images/chgstat.png',				
				text: 'Upload County',
                handler: doDel
			},{
				id: 'del2_butt',
                tooltip: 'Upload counties selected',
				iconCls:'icon',
				icon: 'images/view.gif',				
				text: 'Verify Tables',
                handler: verifytables
			}
		]
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////

	var arrdata;
	var contadorj;
	var marcados="";
	function doDel(){

		arrdata=new Array(67);
		contadorj=0;
	
		var com;
		var j=0;
		marcados="(";
		var arrays = store.getRange(0,store.getCount());
		for(i=0;i<arrays.length;i++){
			com="del"+arrays[i].get('idcounty');
			
			if(document.getElementById(com.toString()).checked==true){
				if(j>0) marcados+=',';
				marcados+=arrays[i].get('idcounty');

				arrdata[contadorj]=new Array(2);
				arrdata[contadorj][0]=arrays[i].data.idcounty;
				arrdata[contadorj][1]=arrays[i].data.bdxima;
				contadorj++;
				
				j++;
			}
		}
		marcados+=")";
		if(i==0)marcados=false;		
		
	
		var com;
		var j=0,k=0;
		var seleccionadosids="";
		var seleccionadosbds="";
		var auxsele='';
		var arrayRecords = store.getRange(0,store.getCount());

		if(contadorj==0)
		{	Ext.MessageBox.alert('Warning','You must select a row.');
			return;
		}
	//	alert(marcados);return;
	//	var arrdata='['+auxsele+']';
	//	alert( seleccionadosids+'$'+ seleccionadosbds+'$$$'+auxsele+'$$$'+arrdata[0]["bdxima"]);
		var i;
		if(document.getElementById('resultupload'))document.getElementById('resultupload').innerHTML='';
		var htmlTag='<div id="resultupload" style="background:#FFF">Please wait, loading... </div>';
	//	alert(htmlTag);
		var html=htmlTag;
		var winEmail=new Ext.Window({
						 title: 'Upload Result',
						 width: 700,
						 height: 250,
					 	 autoScroll: true,
						 resizable: false,
						 maximized: true,
						 modal: true,
						 border:false,
						 plain: false,
						 html: html,
 					buttonAlign: 'center',
					buttons: [{
						handler:function(){ 
							location.href='statusadiario.php';
						},
						scope:this,
						iconCls:'icon',
						icon: 'images/cross.gif',						
						text:'Close'
					}]
						 
				});
		winEmail.show();
		winLoad.show();
		uploaddata(arrdata[0][1],1)
	}
	
	function uploaddata(bd,ciclo){
		if(ciclo<=contadorj)
		{
			Ext.Ajax.request({   
				waitMsg: 'Saving changes...',
				url: 'php/grid_data2.php',				
				timeout :99000000, 
				method: 'POST',
				params: {
					bd: bd,
					tipo: 'uploadverify'
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning',response.responseText);
				},
				
				success:function(response,options){
					var resp = Ext.decode(response.responseText);

					if(ciclo<=1)document.getElementById('resultupload').innerHTML='';
					document.getElementById('resultupload').innerHTML += resp.msg;
					ciclo++;
					if(ciclo<=contadorj)
						uploaddata(arrdata[(ciclo-1)][1],ciclo);
					else
					{
						store.reload();
						winLoad.close();
						verifytables();
						//location.href='statusadiario.php';
					}	
				}
			}); 
		}	
		return;
	}

	function verifytables()	{
		winLoadw.show();
			Ext.Ajax.request({   
				waitMsg: 'Saving changes...',
				url: 'php/grid_data2.php',				
				timeout :99000000, 
				method: 'POST',
				params: {
					tipo: 'verifytables'
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning',response.responseText);
				},
				
				success:function(response,options){
					var resp = Ext.decode(response.responseText);
					winLoadw.hide();
					//Ext.MessageBox.alert('Warning', resp.msg);
					var htmlTag='<div  style="background:#FFF">'+ resp.msg+'</div>';

					var winVerify=new Ext.Window({
									 title: 'Verify Tables',
									 width: 400,
									 height: 250,
									 autoScroll: true,
									 resizable: false,
									 modal: true,
									 border:false,
									 plain: false,
									 html: htmlTag,
								buttonAlign: 'center',
								buttons: [{
									handler:function(){ 
										winVerify.close();
									},
									scope:this,
									iconCls:'icon',
									icon: 'images/cross.gif',						
									text:'Close'
								}]
									 
							});
					winVerify.show();
					
				}
			}); 

	}

					
	function doEdit(oGrid_Event) {
		var gid = oGrid_Event.record.data.idcounty;
		var gfield = oGrid_Event.field;
		var gvaluenew = oGrid_Event.value;

		Ext.Ajax.request({
			url: 'php/grid_data2.php',
			method :'POST',
			params: {
				gid : gid,
				tipo : 'changepercent',
				gfield : gfield,
				gvaluenew : gvaluenew							
			},
			waitTitle   :'Please wait!',
			waitMsg     :'Saving changes...',
			success: function(respuesta){
				var resp = Ext.decode(respuesta.responseText);
				Ext.Msg.alert('Change Percent',resp.msg);
				if(resp.success)
					store.load();									
				} 
			});
	}; 
				

//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
	function checkRow(val,p,record){
		return String.format('<input type="checkbox" name="del'+record.data.idcounty+'"  id="del'+record.data.idcounty+'">',record.data.idcounty);
	}

	function changeColor(val, p, record){
        if(Math.abs(val) <= record.data.valporc )
		{
            return '<span style="color:green;font-weight: bold;">' + val + '</span>';
        }
		else
		{
			if(val >= record.data.valporc )
			{
				return '<span style="color:green;font-weight: bold;">' + val + '</span>';
			}
			else
				return '<span style="color:red;font-weight: bold;">' + val + '</span>';
		}	
        return val;
    }

	function render1(val,p,record){
		
		return String.format('<b>{0}</b>',val);
	}
	function render2(val,p,record){
		
		return String.format('<b>{0}</b>',record.data.bdxima.toUpperCase());
	}
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////

        
	var myView=new Ext.grid.GroupingView({
			forceFit 			: true,
			ShowGroupName		: true,
			enableNoGroup		: true,
			enableGropingMenu	: false,
			hideGroupedColumn	: true,
			startCollapsed 		: true,
			getRowClass : function (record, index) {
				return record.data.colorrow;
			}
	});
		
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
        iconCls: 'icon-grid',
		view : myView,		
		columns: [	  
			{header: "Server", width: 60, align: 'center', sortable: false, dataIndex: 'server',groupable:true,hideable: false}
			,{header: "idcounty", width: 60, align: 'center', sortable: false, dataIndex: 'idcounty',renderer: render1, groupable:false}
			,{id:'County',header: "County", width: 130, align: 'left', sortable: false, dataIndex: 'county',renderer: render1, groupable:false}
			,{header: "%", width: 60, align: 'center', sortable: false, dataIndex: 'porcdiario', groupable:false, editor: new Ext.form.TextField({allowBlank: false})}
			,{header: "Cantidad", width: 80, align: 'center', sortable: false, dataIndex: 'valporc', groupable:false,						
				renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				  //You provide the logic depending on your needs to add a CSS class 
				  //name of your own choosing to manipulate the cell depending upon
				  //the data in the underlying Record object.
				   metaData.css = '';
				  return value;
			   }//end renderer
			}
			<?php
			$i=4;
			foreach ($arrdata as $v)
			{
				echo ",{header: '".$v['fdate']."', width: 80, sortable: false, align: 'center', dataIndex: 'f".$i."', groupable:false}";
				if($i>2) echo ",{header: 'Diff ".($i-1)."', width: 60, sortable: false, align: 'center', dataIndex: 'diff".($i-1)."',renderer: changeColor, groupable:false}";
				$i--;
			}
			?>			
			,{header: 'Diff 1', width: 60, sortable: false, align: 'center', dataIndex: 'diff1',renderer: changeColor, groupable:false}

			//,mySelectionModel
			,{header: ' ',width: 30,sortable: false,align: 'center', dataIndex: 'checking', renderer: checkRow, groupable:false}
			,{header: 'UPLOAD', width: 130, sortable: false, align: 'left', dataIndex: 'bdxima',renderer: render2, groupable:false}
			,{header: 'Xima3', width: 90, sortable: false, align: 'left', dataIndex: 'bdxima3', groupable:false}
			,{header: 'Xima', width: 90, sortable: false, align: 'left', dataIndex: 'bdxima', groupable:false}
			
		],
		clicksToEdit:2,
//		sm: mySelectionModel,
        collapsible: true,
        animCollapse: false,
		height:470,
		frame:true,
		title:'AUCTION DIARY',
		loadMask:true
		,tbar: pagingBar 
	});

/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
	grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load();
/////////////FIN Inicializar Grid////////////////////
 
});
</script>