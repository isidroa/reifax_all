<?php
include ("php/checkuser.php");
include("php/conexion.php");
include("php/template.php");
?>
<html>
<head>
	<?php METATAGS();?>
	<?php HEAD1();?>
	<style type="text/css"> 
		table.matriz {  
			border: 1px solid #ccc;
            width: auto;
            margin: 20px auto;
			white-space: nowrap;
			border-collapse:collapse;
			border: 1px solid #000;
		} 
		table.matriz caption {
			font: bold 1em/1.5em "Trebuchet MS", Tahoma, Arial, sans-serif;
            color: #000;
            text-align: center;
            margin: 2px auto;
		}               
        table.matriz thead tr.header th {
			font: bold .9em/1.5em "Trebuchet MS", Tahoma, Arial, sans-serif;
            color: #fff;
            background: #5FACF3;
            text-align: left;
            padding: 3px;
 			border: 1px solid #000;
		}
		table.matriz tbody th {
			font: bold .9em/1.5em "Trebuchet MS", Tahoma, Arial, sans-serif;
			color: #DC4503;
			//background: #FEFFF1;
			text-align: left;
			padding: 2px;
			border: 1px solid #000;
		}                         
        table.matriz tbody td {
			font: bold .9em/1.5em "Trebuchet MS", Tahoma, Arial, sans-serif;
            color: #666;
            //background: #fff;
            text-align: left;
            padding: 2px;
            border: 1px solid #000;
		}                        
</style>
	<link rel="stylesheet" type="text/css" href="includes/uploadExtjs/file-upload.css"/>
	<script type="text/javascript" src="includes/uploadExtjs/FileUploadField.js"></script>

	<script type="text/javascript" src="js/workshop.js"></script>
	
</head>
<body>
</body>
</html>
<script type="text/javascript">
var store;
var Paginator;
var Toolbar;
var Grid;
var tabPanel;
	
Ext.onReady(function() {
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
	
///////////Data store///////////////
	store = new Ext.data.JsonStore({
		totalProperty: 'total',
		root: 'results',
		url: 'php/response_grid.php?oper=workshop',
		fields: [
			{name: 'idwor', type: 'int'}
			,'nameworkshop'	
			,'description'	
			,'active'	
			,'countvolumen'
			,'countsubject'
			,'fpay'
			,'fdeliver'
			,'price'
			,'deliveryvolume'
		],
		remoteSort: true
	});

////////////////Paginator//////////////////////
	Paginator = new Ext.PagingToolbar({
        pageSize: PAGESIZEGRID,
        store: store,
        displayInfo: true
	});

////////////////Toolbar//////////////////////
	Toolbar = new Ext.Toolbar({
        items  : [
			'<b>WORKSHOP GRID</b>',
			'-',
		{
			text: 'Add Workshop',
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'img/add.gif',
			id: 'add_button',
			tooltip: 'Click to Add Workshop',
			handler: AddWorkshop 
        },{
			text: 'Copy Workshop',
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'img/copy.png',
			id: 'add_button4',
			tooltip: 'Click to Copy Workshop',
			handler: CopyWorkshop
        }]
    });

/////////////////Grid//////////////////////////////////,editor: new Ext.form.TextField({allowBlank: false})
	Grid = new Ext.grid.EditorGridPanel({
		store: store,
        iconCls: 'x-icon-statistics',
		icon: 'img/grid.png',
		tbar: Toolbar, 
		columns: [	
			 new Ext.grid.RowNumberer()
			,{id:'idwor',header: " ",	width: 50, 	sortable: true, align: 'center', dataIndex: 'idwor',renderer: renderTopic}
			,{header: 'Workshop', 		width: 170, sortable: true, align: 'left', dataIndex: 'nameworkshop',editor:  new Ext.form.TextField()}
			,{header: 'Volumes',		width: 80, sortable: true, align: 'center', dataIndex: 'countvolumen'}
			,{header: 'Subjects',		width: 80, sortable: true, align: 'center', dataIndex: 'countsubject'}
			,{header: 'Price',			width: 80, sortable: true, align: 'right', dataIndex: 'price', renderer : function(v){return Ext.util.Format.usMoney(v)}}
			,{header: 'Frequency Pay',		width: 120, sortable: true, align: 'left', dataIndex: 'fpay'}
			,{header: 'Frequency Delivery',	width: 120, sortable: true, align: 'left', dataIndex: 'fdeliver'}
			,{header: 'Delivery Volumen',	width: 120, sortable: true, align: 'center', dataIndex: 'deliveryvolume'}
			,{header: 'Active', 		width: 50, sortable: true, align: 'center', dataIndex: 'active'}
			,{header: 'Description',	width: 250, sortable: true, align: 'left', dataIndex: 'description',editor:  new Ext.form.TextArea()}
		],
		title: 'Workshops',
		clicksToEdit:2,
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}),
		height:450,
		width: '99.8%',
		frame:true,
		loadMask:true,
		bbar: Paginator 
	});
	
/////////////////FIN Grid////////////////////////////

//////////////////Listener///////////////////////////
	Grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
	store.load({params:{start:0, limit:PAGESIZEGRID}});
/////////////FIN Inicializar Grid////////////////////

//////////////VIEWPORT////////////////////////////////

	tabPanel = new Ext.TabPanel({  
		id: 'TabPanelMain',  
        border: false,  
        activeTab: 0,  
        hideBorders : true,
        //height      :Ext.isIE ? 2150 : 2050,
        enableTabScroll:true,
        defaults    : {autoScroll:true},
        plain       :true,
		height: 530,	
		items:[Grid]  
    });
	
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 100,
			items: Ext.getCmp('menumain')
		},{
			region:'center',
			autoHeight: true,
			items: tabPanel
		}]
	});
	
////////////////Functions grid//////////////////////
//'<a href="javascript:void(0)" title="Click to delete row." onclick="DeleteWindows({0})"><img src="img/cross.gif"></a>'
	function renderTopic(value, p, record)
	{
		return String.format(
		'<a href="javascript:void(0)" title="Click to edit row." onclick="EditWorkshop({0})"><img src="img/edit.gif"></a>&nbsp;'+
		'<a href="javascript:void(0)" title="Click to see detail row." onclick="EditWorkshopMatriz({0})"><img src="img/view.gif"></a>&nbsp;'
		,value);
	}
	
	function doEdit(oGrid_Event) {
		var fieldValue = oGrid_Event.value;
		var ID =	oGrid_Event.record.data.idwor;
		campo=oGrid_Event.field;
		info=fieldValue;
		type='text';

		Ext.Ajax.request({  
			waitTitle: 'Please wait..',
			waitMsg: 'Sending data...',
			url: 'php/response_edit.php', 
			method: 'POST', 
			params: { 
				oper: "workshop", 
				campo: campo,
				info:info,
				id:ID							
			},
			success:function(response,options){
				store.reload();
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.MessageBox.alert('Success',d.errors.reason);
			},
			failure:function(response,options){
				var d = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Failure', d.errors.reason);
			}
		});				
	}; 
});
</script>