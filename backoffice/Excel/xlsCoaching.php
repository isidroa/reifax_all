<?php
//echo "entra al php de excel";
	session_start();
	include("../php/conexion.php");

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   

	$opcion=$_POST['parametro'];  

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("Xima LLC Reports");
	$excel->getProperties()->setSubject("Xima LLC Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$excel->setActiveSheetIndex(0);   
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	$conex=conectar("xima");
	switch ($opcion){
		case 'Export_grid':
			$VHeader=array('Sel.','','Item',"ID","Userid","User","Registration Date","# Logs","# Coaching","Days","Home Phone","Mobile Phone","Product","Time","Status","Available time","Date","Hour","Mandated","# Calls","Date Call");
			$VFields=array("id","userid","usr_name","date_register","logs","coaching", "dias","home","movile","producto","periodo","status","time_user","date_coaching","hourCoaching","mandated","numCalls","callingD");
			
			$where=($_POST['status']!='')? '  and c.status '.$_POST['status']:'';
			
			if($_POST['typeDate']!='' && $_POST['startDate'] != ''){
			$startDate= $_POST['startDate'];
			$dueDate= $_POST['dueDate'];
			
			switch ($_POST['typeDate']) {
					case '1':
						$where.=" and '".$startDate."' = date_format(c.date_register,'%Y-%m-%d') ";
					break;	
					case '2':
						$where.=" and '".$startDate."' < date_format(c.date_register,'%Y-%m-%d') ";
					break;				
					case '3':
						$where.=" and '".$startDate."' > date_format(c.date_register,'%Y-%m-%d') ";
					break;
					case '4':
						$where.=" and '".$startDate."' >= date_format(c.date_register,'%Y-%m-%d') ";
					break;
					case '5':
						$where.=" and '".$startDate."' <= date_format(c.date_register,'%Y-%m-%d') ";
					break;
					case '6':
						$where.=" and date_format(c.date_register,'%Y-%m-%d') BETWEEN '".$startDate."' and  '".$dueDate."' ";
					break;
					
				}
			}
			
			$query='SELECT c.id_record_coaching as id ,c.numCalls,c.callingD, c.userid , concat( u.name ," " ,u.surname) as usr_name ,DATE_FORMAT(c.date_register,"%Y-%m-%d") as date_register,  DATEDIFF(NOW(),c.date_register) dias ,
u.HOMETELEPHONE home, u.MOBILETELEPHONE movile,  p.name as producto,  f.name as periodo, if(c.status=1,"Active",if(c.status=3,"Completed",if(c.status=4,"Canceled","Expired"))) as status ,c.time_user, DATE_FORMAT(c.date_coaching,"%Y-%m-%d") as date_coaching,
 c.hourCoaching,concat(m.name ," " ,m.surname," (", m.userid ,")") mandated FROM `xima`.`record_coaching` c
join xima.ximausrs u on c.userid=u.userid LEFT JOIN xima.ximausrs m on c.mandated=m.userid LEFT JOIN xima.frecuency f on f.idfrecuency=c.id_frecuency LEFT JOIN `xima`.`usr_producto` p on p.idproducto=c.producto WHERE 1=1 '.$where.'
order by c.id_record_coaching desc;';

		break;
	}   
	$col_count=count($VHeader);
	$col_inicial=2;
	$row_count=2; 
	for($i=$col_inicial;$i<($col_count);$i++)
	{
		// Put in the header row   
		$excel->getActiveSheet()->setCellValueByColumnAndRow($i-1,$row_count,$VHeader[$i]);   
		// Set header cells to have bold font   
		$excel->getActiveSheet()->getStyleByColumnAndRow($i, $row_count)->getFont()->setBold(true);   
		$excel->getActiveSheet()->getColumnDimension($i-1)->setAutoSize(true);
		
	}
	$row_count++; 
//echo $query;
//exit();

	//echo $query;
	$i=0;
	$Filas="";
	$res=mysql_query($query) or die($query.mysql_error());
	// Add the data to the spreadsheet   
	// Keep a count of the row we're on   
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{
		
		$sql2='SELECT count(*) logs FROM xima.logssession where userid='.$row['userid'];
		$respuesta2=mysql_query($sql2) or die (json_encode(array('success'=>false , 'error'=> mysql_error(), 'sql' => $sql2)));
		$note=mysql_fetch_assoc($respuesta2);
		$row['logs']=$note['logs'];		
		

		$sql2='SELECT count(*) logs FROM xima.record_coaching where userid='.$row['userid'];
		$respuesta2=mysql_query($sql2) or die (json_encode(array('success'=>false , 'error'=> mysql_error(), 'sql' => $sql2)));
		$note=mysql_fetch_assoc($respuesta2);
		$row['coaching']=$note['logs'];		
		//echo "<br>";
		$excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row_count, $row_count-2); //poniendo el item
		$excel->getActiveSheet()->getColumnDimension(1)->setAutoSize(true);
		for($j=($col_inicial+1);$j<$col_count;$j++)//poniendo todos los textos y campos
		{
			$excel->getActiveSheet()->setCellValueByColumnAndRow($j-1, $row_count,$row[$VFields[$j-3]]);   
			$excel->getActiveSheet()->getColumnDimension($abc[$j-1])->setAutoSize(true);
		}	
		$row_count++;   
	}
 
	
	$excel->getActiveSheet()->getStyle('B2')->applyFromarray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
				'top'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'bottom'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'right'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'left'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				)
			),
			'fill' => array(
	 			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
	 			'startcolor' => array(
	 				'argb' => 'FF87cefa'
	 			)
	 		)
		)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($col_count-$col_inicial)].'2' );
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nombre = 'archivos/'.gettimeofday(true).'.xls';
	$objWriter->save($nombre);
	echo $nombre;
	
?>
