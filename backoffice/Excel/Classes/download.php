<?php 
function eliminar(  ){ 
	$directorio = opendir('../archivos/'); 
	while ($archivo = readdir($directorio)){ 
		if( $archivo !='.' && $archivo !='..'){ 
			//si no es un directorio, lo borramos 
			unlink('../archivos/'.$archivo); 
		} 
	} 
	closedir($directorio); 
}

$filename = "../archivos/".$_GET['file']; 
$tipo = $_GET['tipo'];
// required for IE, otherwise Content-disposition is ignored 
if(ini_get('zlib.output_compression')) 
  ini_set('zlib.output_compression', 'Off'); 

// addition by Jorg Weske 
$file_extension = strtolower(substr(strrchr($filename,"."),1)); 

if( $filename == "" ) 
{ 
  echo "<html><title>eLouai's Download Script</title><body>ERROR: download file NOT SPECIFIED. USE force-download.php?file=filepath</body></html>"; 
  exit; 
} elseif ( ! file_exists( $filename ) ) 
{ 
  echo "<html><title>eLouai's Download Script</title><body>ERROR: File not found. USE force-download.php?file=filepath</body></html>"; 
  exit; 
}; 
switch( $file_extension ) 
{ 
  case "pdf": $ctype="application/pdf"; break; 
  case "exe": $ctype="application/octet-stream"; break; 
  case "zip": $ctype="application/zip"; break; 
  case "doc": $ctype="application/msword"; break; 
  case "xls": $ctype="application/vnd.ms-excel"; break; 
  case "ppt": $ctype="application/vnd.ms-powerpoint"; break; 
  case "gif": $ctype="image/gif"; break; 
  case "png": $ctype="image/png"; break; 
  case "jpeg": 
  case "jpg": $ctype="image/jpg"; break; 
  default: $ctype="application/force-download"; 
} 

header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" ); 
header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" ); 
header ( "Cache-Control: no-cache, must-revalidate" ); 
header ( "Pragma: no-cache" ); 
header ( "Content-type: application/x-msexcel" ); 
header ( "Content-Disposition: attachment; filename=$tipo.xls" ); 
header("Content-Transfer-Encoding: binary"); 
header("Content-Length: ".filesize($filename)); 
readfile("$filename"); 
eliminar();
exit(); 

?>