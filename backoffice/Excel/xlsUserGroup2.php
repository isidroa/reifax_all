<?php
//echo "entra al php de excel";

error_reporting(E_ALL); 
ini_set("display_errors", 1); 
	session_start();
	include("../php/conexion.php");

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   

	//$opcion=$_POST['parametro'];  

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$indiceSheet=0;
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("Xima LLC Reports");
	$excel->getProperties()->setSubject("Xima LLC Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	//$excel->setActiveSheetIndex(0);
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	$conex=conectar("xima");
	function loadPage($group,$iS,$nameSheet,$init=FALSE){
		global $excel;
		global $abc;
		
		if($init){
		    $objWorkSheet = $excel->getActiveSheet();
		}
		else{
		    $objWorkSheet = $excel->createSheet($iS);
		}
		
		$VHeader=array('Sel.','Item',"ID",'userid','Name','Surname','Phone','Email','Active');
		$VFields=array("id","userid","NAME","SURNAME","HOMETELEPHONE","EMAIL","active");
		$feature=array();
		$sql="SELECT * FROM xima.user_groups_featuresassing fa 
			JOIN
			 `user_groups_features` f ON fa.id_feature=f.id WHERE fa.id_assing={$group};";
		$result=mysql_query($sql) or die ($sql.mysql_error());
		while($dataCol=mysql_fetch_assoc($result)){
			array_push($feature,$dataCol['id_feature']);
			array_push($VHeader,$dataCol['feature']);
		}
		$sql="SELECT * FROM user_groups_assing ga JOIN `ximausrs` x ON x.userid=ga.userid WHERE ga.idgroup={$group};";
		
		$row_count=2;
		foreach($VHeader as $k => $v){
		// Put in the header row   
			$objWorkSheet->setCellValueByColumnAndRow($k,$row_count,$v);   
			// Set header cells to have bold font   
			$objWorkSheet->getStyleByColumnAndRow($k, $row_count)->getFont()->setBold(true);   
			$objWorkSheet->getColumnDimension($k)->setAutoSize(true);
		}
		
		$row_count++;
		$sql="SELECT * FROM user_groups_assing ga JOIN `ximausrs` x ON x.userid=ga.userid WHERE ga.idgroup={$group} group by x.userid;";
		$result=mysql_query($sql) or die ($sql.mysql_error());
		while($tem=mysql_fetch_assoc($result)){
			
		//echo "<br>";
			$objWorkSheet->setCellValueByColumnAndRow(1, $row_count, $row_count-2); //poniendo el item
			$objWorkSheet->getColumnDimension(1)->setAutoSize(true);
			foreach($VFields as $k => $v){
				$objWorkSheet->setCellValueByColumnAndRow($k+2, $row_count,$tem[$v]);   
				$objWorkSheet->getColumnDimension($abc[$k+2])->setAutoSize(true);
			}
			$sql="SELECT * FROM user_groups_featureuser WHERE id_user={$tem['userid']}";
			$result2=mysql_query($sql) or die($sql.mysql_error());
			while($tem2 = mysql_fetch_assoc($result2)){
				$tem[$tem2['id_feature']]=1;
			}
			foreach($feature as $k => $v){
				if($tem[$v]){
					$objWorkSheet->setCellValueByColumnAndRow($k+7, $row_count,'Yes');   
					$objWorkSheet->getColumnDimension($abc[$k+7])->setAutoSize(true);
				}
				else{
					$objWorkSheet->setCellValueByColumnAndRow($k+7, $row_count,'No');   
					$objWorkSheet->getColumnDimension($abc[$k+7])->setAutoSize(true);
				} 
			}
			
			$row_count++;  
		}
		$objWorkSheet->getStyle('A2')->applyFromArray(
			array(
				'font'    => array(
					'bold'      => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'top'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'bottom'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'right'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'left'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					)
				),
				'fill' => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						'argb' => 'FF87cefa'
					)
				)
			)
		);
		$objWorkSheet->duplicateStyle( $objWorkSheet->getStyle('A2'), 'A2:'.$abc[(count($VHeader)-1)].'2' );
		$nameSheet=preg_replace('/[^\w\s]/', '', $nameSheet);
		$nameSheet=substr($nameSheet, 0,30);
	    $objWorkSheet->setTitle("$nameSheet");
		
	}
	
	function getChild($group){
		global $indiceSheet;
		$sql="SELECT * FROM user_groups WHERE father={$group};";
		$result=mysql_query($sql) or die ($sql.mysql_error());
		while($group=mysql_fetch_assoc($result)){
			loadPage($group['id'],$indiceSheet,$group['name']);
			$indiceSheet++;
			getChild($group['id']);
		}
	}
	$groupSelect=$_POST['group'];
	if($_POST['group']=='All' || $_POST['group']==''){
		$sql="SELECT * FROM user_groups WHERE father is NULL";
	}
	else{
		$sql="SELECT * FROM user_groups WHERE id={$groupSelect};";		
	}
	$result=mysql_query($sql) or die ($sql.mysql_error());
	while($group=mysql_fetch_assoc($result)){
		if($indiceSheet>0)
			loadPage($group['id'],$indiceSheet,$group['name']);
		else
			loadPage($group['id'],$indiceSheet,$group['name'],true);
			
		$indiceSheet++;
		getChild($group['id']);
		if($_POST['group']=='All' || $_POST['group']==''){
			$nameGroup='All';
		}
		else{
			$nameGroup=$group['name'];	
		}
	}
	
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nameGroup=preg_replace('/[^\w\s]/', '', $nameGroup);
	$nameGroup='archivos/Users-'.$nameGroup.'-'.gettimeofday(true).'.xls';
	$nombre =str_replace(' ','_',$nameGroup);
	$objWriter->save($nombre);
	echo json_encode(
		array(
			'success' 	=> true,
			'file'		=> $nombre
		)
	);
	
?>
