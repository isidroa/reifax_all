<?php
//echo "entra al php de excel";
	session_start();
	include("../php/conexion.php");

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$indiceSheet=0;
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("Xima LLC Reports");
	$excel->getProperties()->setSubject("Xima LLC Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	//$excel->setActiveSheetIndex(0);
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	$conex=conectar('xima');
	
	$objWorkSheet = $excel->getActiveSheet($indiceSheet);
	
	$VHeader=array(
		'UserId','Name','Status','Email Reader','Email Delivery','Escrow One','Escrow Two','S1','I1','S2','I2','Initial Deposit','Additional Deposit','Freddy','Freddy Factor','Freddy Round','Freddy Type'
	);
	$VFields=array(
		'userid','name','status','username','imap_username','escrowAgent1','escrowAgent2','imagen1','imagen2','imagen3','imagen4','initialDeposit','additionalDeposit','professional_esp',
		'investor_offer_factor','investor_offer_round','investor_offer_type'
	);
	$feature=array();
	
	$row_count=2;
	
	foreach($VHeader as $k => $v){
	// Put in the header row   
		$objWorkSheet->setCellValueByColumnAndRow($k+2,$row_count,$v);   
		// Set header cells to have bold font   
		$objWorkSheet->getStyleByColumnAndRow($k, $row_count)->getFont()->setBold(true);   
		$objWorkSheet->getColumnDimension($k)->setAutoSize(true);
	}
	$row_count++;
	$sql="
		select 
			a.userid,CONCAT(a.`name`,' ',a.surname) `name`,l.`status`,IF(b.professional_esp=1,'1','0') professional_esp,c.username,c.imap_username,d.escrowAgent escrowAgent1,
			e.escrowAgent escrowAgent2,IF(LENGTH(TRIM(f.imagen))>0,1,0) imagen1,IF(LENGTH(TRIM(g.imagen))>0,1,0) imagen2,IF(LENGTH(TRIM(h.imagen))>0,1,0) imagen3,
			IF(LENGTH(TRIM(i.imagen))>0,1,0) imagen4,j.initialDeposit,j.additionalDeposit,j.investor_offer_factor,
			j.investor_offer_round,IF(j.investor_offer_type=1,'Low',IF(j.investor_offer_type=2,'Median','NONE')) investor_offer_type,k.documents
		from xima.ximausrs a 
		left join xima.permission b on a.userid=b.userid
		left join xima.contracts_mailsettings c on a.userid=c.userid
		left join xima.contracts_scrow d on a.userid=d.userid and d.place=1
		left join xima.contracts_scrow e on a.userid=e.userid and e.place=2
		left join xima.contracts_signature f on a.userid=f.userid and f.`type`=1
		left join xima.contracts_signature g on a.userid=g.userid and g.`type`=2
		left join xima.contracts_signature h on a.userid=h.userid and h.`type`=3
		left join xima.contracts_signature i on a.userid=i.userid and i.`type`=4
		left join xima.xima_system_var j on a.userid=j.userid
		left join (
			select group_concat(CONCAT('<ul class=\"special-report-bo-col\">',c.contract,c.documents,'</ul>') SEPARATOR '') documents, c.userid from 
			(select CONCAT('<li><b>',a.`name`,'</b></li>') contract,group_concat('<li>',b.addon_name,'</li>' SEPARATOR '') documents,a.userid from xima.contracts_custom a 
			left join xima.contracts_addonscustom b on a.id=b.type and a.userid=b.userid
			group by CONCAT(a.userid,a.`name`)) c group by c.userid) k on a.userid=k.userid
		left join xima.`status` l on a.idstatus=l.idstatus
		where a.executive='3456' and not a.idstatus in (5,6,7,2)
	";
	$result=mysql_query($sql) or die(mysql_error());
	while($tem=mysql_fetch_assoc($result)){
		
		foreach($VFields as $k => $v){
			if(in_array($v, array('imagen1','imagen2','imagen3','imagen4','professional_esp'))){
				if($tem[$v]==1){
					$color='FF00FF7F';
				}else{
					$color='FFCD5C5C';
				}
				$objWorkSheet->getStyle($abc[$k+2].$row_count)->applyFromArray(
					array(
						'borders' => array(
							'top'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN 
							),
							'bottom'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN 
							),
							'right'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN 
							),
							'left'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN 
							)
						),
						'fill' => array(
							'type'       => PHPExcel_Style_Fill::FILL_SOLID,
							'startcolor' => array(
								'argb' => $color
							)
						)
					)
				);
				$tem[$v]='';
			}
			$objWorkSheet->setCellValueByColumnAndRow($k+2, $row_count,$tem[$v]);   
			$objWorkSheet->getColumnDimension($abc[$k+2])->setAutoSize(true);
		}
		
		$row_count++;  
	}
	$objWorkSheet->getStyle('C2')->applyFromArray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
				'top'     => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK
				),
				'bottom'     => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK
				),
				'right'     => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK
				),
				'left'     => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK
				)
			),
			'fill' => array(
				'type'       => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'argb' => 'FF87cefa'
				)
			)
		)
	);
	$objWorkSheet->duplicateStyle( $objWorkSheet->getStyle('C2'), 'D2:'.$abc[(count($VHeader)+1)].'2' );
    $objWorkSheet->setTitle('Basic Configuration');
	
	
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nombre = 'archivos/specialReport-'.gettimeofday(true).'.xls';
	$objWriter->save($nombre);
	echo json_encode(
		array(
			'success' 	=> true,
			'file'		=> $nombre
		)
	);
	
?>
