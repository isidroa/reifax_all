<?php
//echo "entra al php de excel";
	session_start();
	include("../php/conexion.php");

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   

	$opcion=$_POST['parametro'];  

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("Xima LLC Reports");
	$excel->getProperties()->setSubject("Xima LLC Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$excel->setActiveSheetIndex(0);   
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	$conex=conectar("xima");
	switch ($opcion){
		case 'Export_grid':
		
			$VHeader=array('Sel.','','Item',"Ticket#","Userid","Name","Days","Customer","Programmer","Status","Issues","Issues Description","Product","Date added","Date Notified","Check");
			$VFields=array("ticket","useridclient","cliente","dias","customer","programador","status","errortype","errordescription","productName","datel","datec","date_check");
			
			
			$where="where 1=1";
			$where.= ($_POST['status']!='')?' AND c.status '.$_POST['status']:'';
			$where.= ($_POST['programer']!='')?' AND c.useridprogrammer='.$_POST['programer']:'';
			$where.= ($_POST['error']!='')?' AND c.errortype="'.$_POST['error'].'"':'';
			$sort= 'c.idcs';
			$dir= 'desc';
			
			$query='select  c.idcs,
			c.useridclient,concat( u.name ," " ,u.surname) cliente, c.useridcustomer, concat( cu.name ," " ,cu.surname) customer, c.useridprogrammer,concat(p.name ," " ,p.surname) programador,
			u.HOMETELEPHONE phone, u.MOBILETELEPHONE mobile, u.email ,
			c.id_product,pr.name as productName, c.browser, c.os, 
			(msg.id_msg) msg_cliente, (msg_sys.id_msg) msg_system, 
			c.state, co.County, c.idcounty, c.menutab, c.submenutab,c.proptype, c.errortype, c.search, c.errordescription,
			c.`status`,DATE_FORMAT(c.datel,"%a %b %d %Y") as datel, c.dated, c.datef, DATE_FORMAT(c.datefc,"%a %b %d %Y") as datec, c.errortype2, c.soldescription, c.ticket, c.source ,
			DATE_FORMAT(c.date_check,"%a %b %d %Y") as date_check,DATEDIFF(if(c.status="Close",c.datefc,now()),c.datel) dias
			from `xima`.`customerservices` c 
				LEFT JOIN xima.ximausrs u ON c.useridclient=u.userid 
				LEFT JOIN xima.usr_producto pr ON pr.idproducto=c.id_product 
				LEFT JOIN xima.ximausrs cu ON c.useridcustomer=cu.userid 
				LEFT JOIN xima.ximausrs p ON c.useridprogrammer=p.userid 
				LEFT JOIN xima.lscounty co ON c.idcounty=co.IdCounty 
    left JOIN (select id_ticket, status_msg_back,id_msg from xima.customerservice_msg where status_msg_back=1 group by id_ticket) msg ON msg.id_ticket=c.idcs AND msg.status_msg_back=1 
    left JOIN (select id_ticket, status_msg_system,id_msg from xima.customerservice_msg where status_msg_system=1 group by id_ticket) msg_sys ON msg_sys.id_ticket=c.idcs AND msg_sys.status_msg_system=1 

			'.$where.'
			order by '.$sort.' '.$dir;
			
		break;
	}   
	$col_count=count($VHeader);
	$col_inicial=2;
	$row_count=2; 
	for($i=$col_inicial;$i<($col_count);$i++)
	{
		// Put in the header row   
		$excel->getActiveSheet()->setCellValueByColumnAndRow($i-1,$row_count,$VHeader[$i]);   
		// Set header cells to have bold font   
		$excel->getActiveSheet()->getStyleByColumnAndRow($i, $row_count)->getFont()->setBold(true);   
		$excel->getActiveSheet()->getColumnDimension($i-1)->setAutoSize(true);
		
	}
	$row_count++; 
//echo $query;
//exit();

	//echo $query;
	$i=0;
	$Filas="";
	$res=mysql_query($query) or die($query.mysql_error());
	// Add the data to the spreadsheet   
	// Keep a count of the row we're on   
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{

		//echo "<br>";
		$excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row_count, $row_count-2); //poniendo el item
		$excel->getActiveSheet()->getColumnDimension(1)->setAutoSize(true);
		for($j=($col_inicial+1);$j<$col_count;$j++)//poniendo todos los textos y campos
		{
			$excel->getActiveSheet()->setCellValueByColumnAndRow($j-1, $row_count,$row[$VFields[$j-3]]);   
			$excel->getActiveSheet()->getColumnDimension($abc[$j-1])->setAutoSize(true);
		}	
		$row_count++;   
	}
 
	
	$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
				'top'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'bottom'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'right'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'left'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				)
			),
			'fill' => array(
	 			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
	 			'startcolor' => array(
	 				'argb' => 'FF87cefa'
	 			)
	 		)
		)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($col_count-$col_inicial)].'2' );
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nombre = 'archivos/'.gettimeofday(true).'.xls';
	$objWriter->save($nombre);
	echo $nombre;
	
?>
