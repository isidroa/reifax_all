<?php
//echo "entra al php de excel";
	session_start();
	include("../php/conexion.php");

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   
	include("../../properties_getprecio.php");

	$opcion=$_POST['parametro'];  

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("Xima LLC Reports");
	$excel->getProperties()->setSubject("Xima LLC Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$excel->setActiveSheetIndex(0);   
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	$conex=conectar("xima");
switch ($opcion){ 
  case 'users':
//echo $_SESSION['filters'];
	$VHeader=array('Sel.','','Item','Name','Last Name','User ID','Email','Status','Price','User Type','Executive','Sing Up','HomeTelephone','MobileTelephone');
	$VFields=array('name','surname','userid','email','status','price','usertype','executive','affiliation','hometelephone','mobiletelephone');
  
	$query="SELECT DISTINCT SQL_CALC_FOUND_ROWS
			xima.ximausrs.userid,
			xima.ximausrs.name,
			xima.ximausrs.surname,
			xima.ximausrs.email,
			xima.ximausrs.country,
			xima.ximausrs.state,
			xima.ximausrs.city,
			xima.status.status,
			xima.usertype.usertype,
			xima.ximausrs.address,
			xima.ximausrs.hometelephone,
			xima.ximausrs.mobiletelephone,
			xima.ximausrs.licensen,
			xima.ximausrs.brokern,
			xima.ximausrs.affiliationdate,
			date(xima.ximausrs.affiliationdate) affiliation,
			xima.ximausrs.officenum,
			xima.ximausrs.licensen,
			xima.ximausrs.executive,
			0 price			
			FROM xima.ximausrs
				LEFT JOIN (xima.status) ON (xima.ximausrs.idstatus=xima.status.idstatus)
				LEFT JOIN (xima.usertype) ON (xima.ximausrs.idusertype=xima.usertype.idusertype)
				LEFT JOIN (xima.f_frecuency) ON (xima.ximausrs.userid=xima.f_frecuency.userid)
				LEFT JOIN (xima.usernotes) ON (xima.ximausrs.userid=xima.usernotes.userid)
				LEFT JOIN (xima.creditcard) ON (xima.ximausrs.userid=xima.creditcard.userid)
				LEFT JOIN (xima.userterms) ON (xima.ximausrs.userid=xima.userterms.userid)
				LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
				LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
				LEFT JOIN (xima.permission) ON (xima.ximausrs.userid=xima.permission.userid)
				LEFT JOIN (xima.cobro_porcentajes) ON (xima.ximausrs.userid=xima.cobro_porcentajes.userid)
				LEFT JOIN (xima.usr_registertype) ON (xima.ximausrs.userid=xima.usr_registertype.userid)
				LEFT JOIN (xima.logsstatus) ON (xima.ximausrs.userid=xima.logsstatus.userid)
				LEFT JOIN (xima.logsusr_cobros) ON (xima.ximausrs.userid=xima.logsusr_cobros.userid)				
		";
	if(isset($_SESSION['filters'])) $query.=" WHERE ".$_SESSION['filters']." "; //echo $query;
	$order="";
	if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
	{
		if($_POST['campoOrdenSent']=='ASC')$campoSentido='ASC';
		else $campoSentido='ASC';
		$order=" ORDER BY ".$_POST['campoOrden']." ".$campoSentido." ";
	}
	else
	{
		$order="ORDER BY xima.ximausrs.userid ASC";
	}
	$query.=$order;
  break;  
  case 'inact':

	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','Status','Payday','PayDate','Price','Card Type','Balance','Paypal Error');
	$VFields=array('userid','name','surname','status','dia','paydate','priceprod','cardname','saldo','paypalerror');

	$query="SELECT x1.userid,x1.name,x1.surname,s.status,t.usertype, 0 amountsinformato,
					(SELECT DISTINCT DAYOFMONTH(fechacobro) FROM xima.usr_cobros u WHERE u.userid=x1.userid) dia,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					(SELECT concat(date(y.fecha),'. ',y.errornum,'. ',y.errormsj,'. ',y.errormsjl)  FROM xima.cobro_paypal y WHERE  y.userid=x1.userid and y.estado='Failure' oRDER BY paypalid desc LIMIT 1) paypalerror,
					c.cardname,c.cardnumber,n.notes,d.saldo, (SELECT GetPrice(x1.userid)) AS priceprod
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (5,7) and d.saldo<0
					ORDER BY  dia, x1.userid";
  break;
  case 'cobros': 
	$VHeader=array('Sel.','','Item','User ID','Name','Last Name','Status','Paydate','Price','Holder Name','Card Number','Date Trans.','Paypal Trans.','Amount','Source','For');
	$VFields=array('userid','name','surname','status','paydate','priceprod','cardholdername','cardnumber','fecha','transactionid','amount','source','cobrador');
			$dayfrom=$dayto=date('Y-m-d');
			
			if(isset($_POST['dayfrom']) && $_POST['dayfrom']<>'' && strlen($_POST['dayfrom'])>0 )
			{ 
				$dayfrom=$_POST['dayfrom'];
				$dayto=$_POST['dayto'];
			}
			
			$qwhe='';						
				$qwhe=" AND date(h.fechatrans) BETWEEN '$dayfrom' AND '$dayto' ";


			 $query="SELECT x1.userid,x1.name,x1.surname,s.status,0 amountsinformato,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,p.paypalid,
					c.cardholdername,c.cardnumber,n.notes,p.fecha,p.transactionid,p.amount,h.cobradordesc source,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador, (SELECT GetPrice(x1.userid)) AS priceprod  			
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					INNER JOIN xima.cobro_paypal p ON p.paypalid=h.paypalid
					where h.idoper in (5,17) $qwhe
					ORDER BY  p.fecha,x1.userid";
      //$query=$_SESSION['excel'];

  break;
  case 'activos':
			$query="SELECT x1.userid,x1.name,x1.surname,s.status,t.usertype, 0 amountsinformato,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					c.cardname,c.cardnumber,n.notes,d.saldo, (SELECT GetPrice(x1.userid)) AS priceprod	
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (3,6,1) and d.saldo<0
					ORDER BY  day(paydate) desc,x1.userid";
	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','Status','PayDate','Price','Card Type','Balance');
	$VFields=array('userid','name','surname','status','paydate','priceprod','cardname','saldo');
  break;

  case 'refunds':
	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','Status','PayDate','Price','Date Trans.','Amount','Source','For','Description');
	$VFields=array('userid','name','surname','status','paydate','priceprod','fecha','amount','source','cobrador','notasrefund');
			$dayfrom=date('Y-m-01');
			$dayto=date('Y-m-d');
			$operation_filtre = 15;
			if(isset($_POST['rfrom']) && $_POST['rfrom']<>'' && strlen($_POST['rfrom'])>0 )
			{ 
				$dayfrom=$_POST['rfrom'];
				$dayto=$_POST['rto'];
				$operation_filtre = $_POST['type'];
			}

			if($operation_filtre == 13)
			{
				//$whereCredit =" AND h.notas !='Credit suspended user'";
				$whereCredit= "AND h.cobradordesc != ''";
			}
			
			$qwhe=" AND date(h.fechatrans) BETWEEN '$dayfrom' AND '$dayto' ";

			$query="SELECT distinct x1.userid,x1.name,x1.surname,0 as priceprod,0 amountsinformato,h.monto amount,
					c.cardholdername,c.cardnumber,'' notes,h.fechatrans fecha,h.cobradordesc source,h.notas notasrefund,
						x1.blacklist,
					(SELECT concat_ws(' ',u1.name,u1.surname) FROM xima.ximausrs u1 WHERE u1.userid=h.usercobrador) cobrador  ,
					(
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=x1.`userid`  and pb1.idproducto not in (11,12,13)
						ORDER BY s1.status
						limit 1
					) status,
					(
						SELECT distinct u1.fechacobro
						FROM usr_cobros u1
						WHERE u1.`userid`=x1.`userid`
						limit 1
					) paydated
					FROM xima.ximausrs x1
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.cobro_estadocuenta h ON h.userid=x1.userid
					where h.idoper = $operation_filtre $qwhe
					$whereCredit

					ORDER BY  h.fechatrans,x1.userid";
  break;
  case 'fecha':
	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','TrialDays.',/*'Amount',*/'PayDate','Status','UserType','Card Type','Card Number','Price');
	$VFields=array('userid','name','surname','freedays',/*'amount',*/'paydate','status','usertype','cardname','cardnumber','priceprod');
			$dayfrom=$_POST['dayfrom'];
			$dayto=$_POST['dayto'];
			
			$que="SELECT x1.userid,x1.name,x1.surname,s.idstatus,s.status,t.usertype, 0 amountsinformato,
					(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid) paydate,
					c.cardname,c.cardnumber,n.notes,d.saldo,0 as amount, (SELECT GetPrice(x1.userid)) AS priceprod
					FROM xima.ximausrs x1 
					INNER JOIN xima.status s ON s.idstatus=x1.idstatus
					INNER JOIN xima.usertype t ON t.idusertype=x1.idusertype
					INNER JOIN xima.creditcard c ON c.userid=x1.userid
					INNER JOIN xima.usernotes n ON n.userid=x1.userid
					INNER JOIN xima.cobro_porcentajes d ON d.userid=x1.userid
					where x1.idstatus in (1,3,5,6,7,8,9) 
					";
			if($_POST['dayfrom']!='-Select-'){ 
				$que.=" and  DAYOFMONTH((SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid)) between ".$dayfrom." and ".$dayto;
				$que.=" and  ((SELECT `idfrecuency` f FROM `xima`.`f_frecuency` f WHERE f.`userid`=x1.userid limit 1)=1 or
							((SELECT `idfrecuency` f FROM `xima`.`f_frecuency` f WHERE f.`userid`=x1.userid limit 1)=2 
							and MONTH((SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x1.userid))=".date('m')."))";
			}
			//echo $que;	
			$que.=" ORDER BY  day(paydate) desc,x1.userid";	
			$query=$que;
  break;
  case 'status':
	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','Actual Status.','Date Status','PayDate','Last Pay','Last Amount');
	$VFields=array('userid','name','surname','status','fechaupd','paydate','ultimafecha','ultimomonto');
			$month=$_POST['month'];
			$year=$_POST['year'];

			$query="select x.userid, x.name, x.surname, x.`idstatus`, date(x.testdatebeguin) as signup,
				s.statusafter, date(s.fechaupd) as fechaupd,
				(SELECT DISTINCT fechacobro FROM xima.usr_cobros u WHERE u.userid=x.userid) paydate,
				n.notes, st.`status`,
				(SELECT date(fechatrans)  FROM cobro_estadocuenta where userid=x.userid and idoper=5 and month(fechatrans)=$month
				and year(fechatrans)=$year limit 1) ultimafecha,
				(SELECT monto  FROM cobro_estadocuenta where userid=x.userid and (idoper=5 or idoper=17) and month(fechatrans)=$month
				and year(fechatrans)=$year limit 1) ultimomonto from
				cobro_estadocuenta e inner join ximausrs x on e.userid=x.userid
				inner join status st on x.idstatus=st.idstatus
				left join logsstatus s on x.userid=s.userid
				left join f_frecuency f on x.userid=f.userid
				left JOIN xima.usernotes n ON x.userid=n.userid
				where month(fechatrans)=$month and year(fechatrans)=$year
				and e.idoper in (5,17) and ((s.statusafter=3
				and month(s.fechaupd)=$month and year(s.fechaupd)=$year) or (x.`idstatus` in (3,6,1)))
				and (month(x.affiliationdate)<=$month and year(x.affiliationdate)<=$year)
				group by x.userid";
  break;
  case 'affMarketing':
	$VHeader=array('Sel.','','Item','Date','Description','User ID','Customer Name.','Customer Payment','Customer Status','Comm. Amount');
	$VFields=array('Date','Operation','Aff_Userid','Aff_Name','payamount','Aff_status','Amount');
			$paffmarkmonth=$_POST['paffmarkmonth'];
			$paffmarkyear=$_POST['paffmarkyear'];
			$userid=$_POST['userid'];
			if($userid<>'' && $userid<>'*')
			{
				$query = "SELECT 0 Posi,c.`userid`, 
				(SELECT concat_ws(' ',name,surname) FROM ximausrs x WHERE x.`userid`=c.`userid`) Name, 
				o.titulo Operation,date(c.fechatrans) `Date`, c.payamount ,c.monto Amount, c.`useridhijo` Aff_Userid, (SELECT concat_ws(' ',name,surname)
				FROM ximausrs x 
				WHERE x.`userid`=c.`useridhijo`) Aff_Name, 
				(SELECT s.status FROM ximausrs x Inner JOIN status s on s.idstatus=x.idstatus WHERE x.`userid`=c.`useridhijo`)  Aff_status 
				FROM cobro_estadocuenta c 
				inner join cobro_operaciones o on c.idoper=o.idoper 
				WHERE c.`userid`=$userid  and c.idoper in (7,8,9,11) 
				and fechatrans between '$paffmarkyear-$paffmarkmonth-01 01:00:00' and '$paffmarkyear-$paffmarkmonth-31 23:59:59' 
				order by fechatrans, idtrans"; 
			}
  break;
  case 'affMarketingReferrals':
	$VHeader=array('Sel.','','Item','User ID','Name','LastName','Status.','Email','Phone 1','Phone 2','Referrer');
	$VFields=array('userid','name','surname','status','email','hometelephone','mobiletelephone','father');
				$pafflevel=$_POST['pafflevel'];
				$userid=$_POST['userid'];
				$que="SELECT x.name,x.surname FROM ximausrs x WHERE x.userid=$userid";
				$result3=mysql_query ($que) or die ($que.mysql_error ());
				$row3=mysql_fetch_object($result3);
				$data = array();	

				if($userid<>'' && $userid<>'*')
				{
				
					$query="SELECT 0 posi,x.userid,x.name,x.surname,o.status,x.email,x.hometelephone,x.mobiletelephone,
					      CONCAT('".$row3->name."',' ','".$row3->surname."',' ','(".$_POST['userid'].")') father
							FROM ximausrs x
							inner join status o on x.idstatus=o.idstatus
							WHERE x.executive=$userid and x.idstatus not in (2,8,9)
							order by userid";
					$result=mysql_query ($query) or die ($query.mysql_error ());
					$i=0;
					while ($row=mysql_fetch_object($result))
					{

						if($pafflevel==2)
						{

							$query="SELECT 0 posi,x.userid,x.name,x.surname,o.status,x.email,x.hometelephone,x.mobiletelephone,
							      /*CONCAT('".$row->name."',' ','".$row->surname."',' ','(".$row->userid.")')*/'' father
									FROM ximausrs x
									inner join status o on x.idstatus=o.idstatus
									WHERE x.executive=".$row->userid." and x.idstatus not in (2,8,9)
									order by userid";
							/*$result2=mysql_query ($que) or die ($que.mysql_error ());
							while ($row2=mysql_fetch_object($result2))
							{
								$i++;
								$row2->posi=$i;
								$row2->father= $row->name.' '.$row->surname.' ('.$row->userid.') ';
								$data [] = $row2;
							}*/

						}
						
					}



				}    
		//    exit;
  break;
  case 'ddusers':
//echo $_SESSION['filters'];
	$VHeader=array('Sel.','','Item','Name','Last Name','User ID','Email','Pass','Phone','Sing Up');
	$VFields=array('name','surname','userid','email','pass','hometelephone','singup');
  
	$query="	SELECT DISTINCT SQL_CALC_FOUND_ROWS
			xima.daveusers.userid,
			xima.daveusers.name,
			xima.daveusers.surname,
			xima.daveusers.email,
			xima.daveusers.pass,
			xima.daveusers.hometelephone,
			xima.daveusers.affiliationdate singup
		FROM xima.daveusers 
		WHERE 1=1 ";
	if(isset($_SESSION['filters'])) $query.=" ANd  ".$_SESSION['filters']." "; //echo $query;
	$order="";
	if($_POST['campoOrden']<>NULL && $_POST['campoOrden']<>'' )
	{
		if($_POST['campoOrdenSent']=='ASC')$campoSentido='ASC';
		else $campoSentido='ASC';
		$order=" ORDER BY ".$_POST['campoOrden']." ".$campoSentido." ";
	}
	else
	{
		$order="ORDER BY xima.daveusers.userid ASC";
	}
	$query.=$order;
  break;  
  case 'usersworkshop':
	$idws=$_POST['idws'];
	$query="SELECT  x.userid,x.name,x.surname,trim(x.email) email,x.hometelephone,
					(SELECT fr.`name` FROM f_frecuency f INNER JOIN frecuency fr ON f.idfrecuency=fr.idfrecuency WHERE f.`userid`=x.userid limit 1) frecuency
					FROM xima.record_workshop r
					INNER JOIN xima.ximausrs x ON r.userid=x.USERID
					LEFT JOIN xima.cobro_paypal c ON r.paypalid=c.paypalid
					LEFT JOIN xima.cobro_estadocuenta ce ON ce.paypalid=c.paypalid
					WHERE r.idworkshop=$idws 
			order by  r.idworkshop";
	$VHeader=array('Sel.','','Item','UserID','Name','Last Name','Email','Phone','Frecuency');
	$VFields=array('userid','name','surname','email','hometelephone','frecuency');
  break;

  case 'retentionrate':

	$VHeader=array('Sel.','','Item','Name','Last Name','User ID','Email','Status','Phone','Sign Up','Payments');
	$VFields=array('name','surname','userid','email','status','hometelephone','affiliationdate','countactive');
  

	$idstat=trim($_POST['idstat']);
	$where='';
	if($idstat<>'' && $idstat<>'*') 
	{
		//$where=' and x.idstatus='.$idstat;
		$where=' AND ( SELECT u1.idstatus FROM usr_cobros u1 WHERE u1.`userid`=xima.ximausrs.`userid` limit 1 )='.$idstat;
	}
		
	$query=" SELECT distinct 
					(	
						SELECT distinct s1.status
						FROM usr_cobros u1
						INNER JOIN xima.usr_productobase pb1 ON pb1.idproductobase=u1.idproductobase
						INNER JOIN xima.status s1 ON u1.idstatus=s1.idstatus
						WHERE u1.`userid`=xima.ximausrs.`userid`  
						ORDER BY s1.status
						limit 1
					) status,xima.ximausrs.userid,xima.ximausrs.name,xima.ximausrs.surname,xima.ximausrs.email,
					xima.ximausrs.hometelephone,date(xima.ximausrs.affiliationdate) affiliationdate,
					0 avgcountactive,
					(SELECT distinct f.`idfrecuency` FROM f_frecuency f WHERE f.`userid`=xima.ximausrs.userid LIMIT 1) frec,
					(SELECT c.paysrate FROM cobro_porcentajes c WHERE c.`userid`=xima.ximausrs.userid limit 1) countactive 
			FROM xima.ximausrs
					LEFT JOIN (xima.usr_cobros) ON (xima.ximausrs.userid=xima.usr_cobros.userid)
					LEFT JOIN (xima.usr_productobase) ON (xima.usr_cobros.idproductobase=xima.usr_productobase.idproductobase)
					WHERE 1=1 ANd  (SELECT  u2.idproducto FROM usr_productobase u2
						INNER JOIN usr_producto u1 ON u2.idproducto=u1.idproducto
						where u2.idproductobase=xima.usr_cobros.idproductobase)<>13
						and xima.ximausrs.idstatus not in (1,4,9,8)
						 $where 
			ORDER BY xima.ximausrs.userid
	";

	//$order=$_POST['sort'];
	//$dir=$_POST['dir'];
	//if(strlen($order)>0)$que.=" ORDER BY $order $dir "; else $que.=" ORDER BY x.userid ASC  ";					
			
  break;  

  case 'specialspays': 
  
  
	$VHeader=array('Sel.','','Item','User ID','Name','Last Name','Date Trans.','Paypal Trans.','Amount','Executive');
	$VFields=array('userid','name','surname','fecha','transactionid','amount','executive');
			$dayfrom=date('Y-m-01');
			$dayto=date('Y-m-d');
			
			$qwhe='';
			if(isset($_POST['dayfrom']) && $_POST['dayfrom']<>'' && strlen($_POST['dayfrom'])>0 )
			{ 
				$dayfrom=$_POST['dayfrom'];
				$dayto=$_POST['dayto'];
			}
			
			$qwhe=" AND date(c.fecha) BETWEEN '$dayfrom' AND '$dayto' ";
			
			if(isset($_POST['cbtype']) && $_POST['cbtype']<>'*')
			{ 
				$cbtype=$_POST['cbtype'];
				$qwhe.=" AND c.`transactionid`= '$cbtype' ";
			}


			$query="SELECT x.`name`, x.`surname`, x.`userid`, x.`hometelephone`, x.`email`, c.`transactionid`, c.`amount`, c.`fecha`,
				(SELECT concat(x1.`NAME`, ' ',x1.`SURNAME`, ' (',x1.`USERID`,')') from ximausrs x1 where x1.userid=x.executive) executive
				FROM `cobro_paypal` c
				INNER JOIN ximausrs x ON c.userid=x.USERID
				WHERE c.AVS='SPECIALPAY' $qwhe";
      //$query=$_SESSION['excel'];

  break;
  
  
}   
	$col_count=count($VHeader);
	$col_inicial=2;
	$row_count=2; 
	for($i=$col_inicial;$i<($col_count);$i++)
	{
		// Put in the header row   
		$excel->getActiveSheet()->setCellValueByColumnAndRow($i-1,$row_count,$VHeader[$i]);   
		// Set header cells to have bold font   
		$excel->getActiveSheet()->getStyleByColumnAndRow($i, $row_count)->getFont()->setBold(true);   
		$excel->getActiveSheet()->getColumnDimension($i-1)->setAutoSize(true);
		
	}
	$row_count++; 
//echo $query;
//exit();

	//echo $query;
	$i=0;
	$Filas="";
	$res=mysql_query($query) or die($query.mysql_error());
	// Add the data to the spreadsheet   
	// Keep a count of the row we're on   
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{
//echo "<br>".$row['userid'];
		if($opcion<>'retentionrate')$row['price']=getpriceuser($row['userid'],0,'yes');	

		//echo "<br>";
		$excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row_count, $row_count-2); //poniendo el item
		$excel->getActiveSheet()->getColumnDimension(1)->setAutoSize(true);
		for($j=($col_inicial+1);$j<$col_count;$j++)//poniendo todos los textos y campos
		{
			$excel->getActiveSheet()->setCellValueByColumnAndRow($j-1, $row_count,$row[$VFields[$j-3]]);   
			$excel->getActiveSheet()->getColumnDimension($abc[$j-1])->setAutoSize(true);
		}	
		$row_count++;   
	}
 
	
	$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
				'top'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'bottom'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'right'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'left'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				)
			),
			'fill' => array(
	 			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
	 			'startcolor' => array(
	 				'argb' => 'FF87cefa'
	 			)
	 		)
		)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($col_count-$col_inicial)].'2' );
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nombre = 'archivos/'.gettimeofday(true).'.xls';
	$objWriter->save($nombre);
	echo "var nombre='".$nombre."';";
	
?>
