<?php
//echo "entra al php de excel";
	session_start();
	
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	$_SERVER['DOCUMENT_ROOT']='C:/inetpub/wwwroot';
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/MANT/classes/connection.class.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/MANT/classes/globalReifax.class.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/MANT/classes/managerDownload.class.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/MANT/classes/managerErrors.class.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/MANT/classes/parseo.class.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/reifaxAll/properties_tabs/propertyImport/class.baseImport.php';
	
	
	$Connect=   new ReiFax(array('downloadFiles' => false)); 
	$conMaster  =   $Connect->mysqlByServer(array('master'=>true, 'typeIp'=>'WAN'));
	$conReifax  =   $Connect->mysqlByServer(array('serverNumber'=>'MyREIFAX','typeIp'=>'WAN'));

	set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
	// Include the PHPExcel classes   
	require("PHPExcel.php");   

	$opcion=$_POST['parametro'];  

	// Start to build the spreadsheet   
	$excel = new PHPExcel();   
	$excel->getProperties()->setCreator("Xima LLC");
	$excel->getProperties()->setTitle("New Reifax Reports");
	$excel->getProperties()->setSubject("New Reifax Reports");
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$excel->setActiveSheetIndex(0);   
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ');
  
// Get data from the database   
	switch ($opcion){
		case 'Export_grid':
			$VHeader=array('Sel.','',"state","name","date_update","trulia sold","trulia for sale","RT sold","RT for sale","RT pre-foreclosure","RT Bank owned","RT aution");
			$VFields=array("state","name","date_update","system_1","system_2","system_3","system_4","system_6","system_7","system_8");
			
		$dateRespuesta=array();
		$indexState=array();
		if(isset($_POST['showAll']) && $_POST['showAll']!='false'){
			
			$sql2="SELECT * FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id group by o.date_update,s.name;";
				$resSystem=$conReifax->query($sql2) or die ($sql2.$conReifax->error);
				while($dataSystem=$resSystem->fetch_assoc()){
					$i=0;
					$sql3="SELECT
						    e.code state,
						    e.name,
						    s.id,
						    date_update,
						    SUM(n_records) n_records,
						    SUM(n_page) n_page
						FROM
						    reifaxcounty.system_origins s
						        JOIN
						    reifaxcounty.county_origin o ON o.idsystem = s.id
						        JOIN
						    reifaxcounty.county c ON o.idcounty = c.idcounty
						    	JOIN
						    reifaxcounty.state e on c.idstate=e.idstate
						WHERE
						    date_update = '{$dataSystem['date_update']}'
						GROUP BY c.idstate;";
					$resRecord=$conReifax->query($sql3) or die ($sql3.$conReifax->error);
					
					while($dataRecord=$resRecord->fetch_assoc()){
						if(isset($indexState[$dataRecord['state']])){
							$indice=$indexState[$dataRecord['state']];
						}
						else{
							$indice=$indexState[$dataRecord['state']]=$i;
							$dateRespuesta[$indice]=$dataRecord;
							$i++;
						}
						$dateRespuesta[$indice]['system_'.$dataRecord['id']]=$dataRecord['n_records'];
					}
				}				
			
		}
		else{
			$sql=sprintf("SELECT c.*, s.code state FROM reifaxcounty.county c
					join reifaxcounty.state s on c.idstate=s.idstate where s.idstate=%d order by s.code desc",$_POST['id']);
			$resCounty=$conReifax->query($sql) or die ($sql.$conReifax->error);
						
			while($dataCounty=$resCounty->fetch_assoc()){
				array_push($dateRespuesta,$dataCounty);
				$lastRecord=count($dateRespuesta)-1;
				
				$sql2="SELECT * FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id group by o.date_update,s.name;";
				$resSystem=$conReifax->query($sql2) or die ($sql2.$conReifax->error);
				
				while($dataSystem=$resSystem->fetch_assoc()){
					$sql3="SELECT n_records,n_page,date_update,s.id FROM reifaxcounty.system_origins s
	join  reifaxcounty.county_origin o on o.idsystem=s.id where date_update='{$dataSystem['date_update']}' AND idcounty={$dataCounty['idcounty']};";
					$resRecord=$conReifax->query($sql3) or die ($sql3.$conReifax->error);
					
					while($dataRecord=$resRecord->fetch_assoc()){
						$dateRespuesta[$lastRecord]['system_'.$dataRecord['id']]=$dataRecord['n_records'];
					}
				}
			}
		}

/*
		echo json_encode(array('success' => true, 'data' => $dateRespuesta));
			
			
			
			
			
			$query='SELECT c.id_record_reactivate as id , c.userid , concat( u.name ," " ,u.surname) as usr_name ,DATE_FORMAT(c.date_register,"%Y-%m-%d") as date_register,  DATEDIFF(NOW(),c.date_register) dias ,
u.HOMETELEPHONE home, u.MOBILETELEPHONE movile,  p.name as producto,  f.name as periodo, if(c.status=1,"Pending",if(c.status=3,"Process",if(c.status=3,"Reactivated",if(c.status=7,"Coaching",if(c.status=8,"Testing","Declined"))))) as status ,c.time_user, DATE_FORMAT(c.date_reactivate,"%Y-%m-%d") as date_reactivate,
 c.hourreactivate,concat(m.name ," " ,m.surname," (", m.userid ,")") mandated, c.callingD, c.conversationD, c.numCalls FROM `xima`.`record_reactivate` c
join xima.ximausrs u on c.userid=u.userid LEFT JOIN xima.ximausrs m on c.mandated=m.userid LEFT JOIN xima.frecuency f on f.idfrecuency=c.id_frecuency LEFT JOIN `xima`.`usr_producto` p on p.idproducto=c.producto WHERE 1=1 '.$where.'
order by c.id_record_reactivate desc;';

		break;
 * 
 */
 
	}   
	$col_count=count($VHeader);
	$col_inicial=2;
	$row_count=2; 
	for($i=$col_inicial;$i<($col_count);$i++)
	{
		// Put in the header row   
		$excel->getActiveSheet()->setCellValueByColumnAndRow($i-1,$row_count,$VHeader[$i]);   
		// Set header cells to have bold font   
		$excel->getActiveSheet()->getStyleByColumnAndRow($i, $row_count)->getFont()->setBold(true);   
		$excel->getActiveSheet()->getColumnDimension($i-1)->setAutoSize(true);
		
	}
	$row_count++; 
//echo $query;
//exit();

	//echo $query;
	$i=0;
	$Filas="";
	// Add the data to the spreadsheet   
	// Keep a count of the row we're on
	$escap=array('id');   
	foreach ($dateRespuesta as $key => $value) {
		$excel->getActiveSheet()->setCellValueByColumnAndRow(1, $key+3, $key);
		$excel->getActiveSheet()->getColumnDimension(1)->setAutoSize(true);
		$j=1;
		foreach ($value as $key2 => $value2) {			
			if(in_array($key2, $VFields)){
				$excel->getActiveSheet()->setCellValueByColumnAndRow($j, $key+3,$value2);   
				$excel->getActiveSheet()->getColumnDimension($abc[$j])->setAutoSize(true);
				$j++;
			}
		}	
	}
	/*
	while ($row= mysql_fetch_array($res, MYSQL_ASSOC))
	{

		//echo "<br>";
		$excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row_count, $row_count-2); //poniendo el item
		$excel->getActiveSheet()->getColumnDimension(1)->setAutoSize(true);
		for($j=($col_inicial+1);$j<$col_count;$j++)//poniendo todos los textos y campos
		{
			$excel->getActiveSheet()->setCellValueByColumnAndRow($j-1, $row_count,$row[$VFields[$j-3]]);   
			$excel->getActiveSheet()->getColumnDimension($abc[$j-1])->setAutoSize(true);
		}	
		$row_count++;   
	}
 */
	
	$excel->getActiveSheet()->getStyle('B2')->applyFromarray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
				'top'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'bottom'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'right'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				),
				'left'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THICK
 				)
			),
			'fill' => array(
	 			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
	 			'startcolor' => array(
	 				'argb' => 'FF87cefa'
	 			)
	 		)
		)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($col_count-$col_inicial)].'2' );
  
// Output the spreadsheet in binary format   
	include 'PHPExcel/Writer/Excel5.php';
	$objWriter = new PHPExcel_Writer_Excel5($excel);   
	$nombre = 'archivos/'.gettimeofday(true).'.xls';
	$objWriter->save($nombre);
	echo $nombre;
	
?>
