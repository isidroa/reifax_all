<?php	

include ("php/checkuser.php");  
include("php/conexion.php");
include("php/conexionS7.php");
include("php/conexionS9.php");
include("php/conexionS10.php");
include("php/conexionS11.php");
$conex=conectar("xima");

?>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Pass Control</title>

    <link href="css/stylegeneral.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/layoutmenu.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/gridstyles.css" rel="stylesheet" TYPE="text/css" MEDIA="screen">
	<?php 	include("php/enablebuttons.php");?>
    <link rel="stylesheet" type="text/css" href="includes/ext/resources/css/ext-all.css" />
 	<script type="text/javascript" src="includes/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="includes/ext/ext-all.js"></script>
    <script type="text/javascript" src="includes/ext/examples/ux/CheckColumn.js"></script>
	
<!--<script type="text/javascript" src="js/menu.js"></script>-->
	<?php 	include("php/menubackoffice.php");?>
</head>
<body>
<?php

	if(!isset($_GET['c'])) 	$_GET['c']=1;
	if(!isset($_GET['server'])) 	$_GET['server']=11;
	
	$q="SELECT servidor FROM xima.lscounty WHERE idcounty=".$_GET['c'];
	$res=mysql_query($q,$conex)or die(mysql_error());
	$r=mysql_fetch_array($res);
	$serv=$r['servidor'];
	$control=date("YmdHi");
	
	$conexaux=conectarS11("mantenimiento");
	
	$query="SELECT fecha, indicador FROM mantenimiento.administracionpass where `CountyID`={$_GET['c']} and fecha>=date_sub(curdate(),interval 5 month) group by indicador
			order by `indicador` desc limit 4;";
	$res=mysql_query($query,$conexaux)or die($query.' ºº '.mysql_error());

	$auxurl='tipo=passcontrol&county='.$_GET['c'].'&server='.$_GET['server'];
	$i=1;
	$arr1=array();
	while($r=mysql_fetch_array($res))$arr1[]=$r;
	
	$fecha1=$arr1[0]['fecha'];
	$fecha2=$arr1[1]['fecha'];
	$nuevafecha = strtotime ( '-1 month' , strtotime ( $fecha2 ) ) ;
	$fecha2eval = date ( 'Ymd' , $nuevafecha );
	$nuevafecha = strtotime ( '-1 month' , strtotime ( $fecha2eval ) ) ;
	$fecha3eval=date ( 'Ymd' , $nuevafecha );
	$i=0;
	$arrdata=array();
	$checkpoints=array();
	$fechaEval='';
	foreach($arr1 as $t)
	{
		array_push($arrdata,array(
			'header'=> $t['fecha'],
			'dataIndex'=> $t['indicador']
		));
		array_push($checkpoints,$t['indicador']);
	}
	$temp=implode(',',$checkpoints);
	$auxurl.='&checkpoints=('.$temp.')';
	$arrdata = array_reverse($arrdata);
?>
<style type="text/css">
.x-grid3-row-alt{
	background-color:#e1e1e1;
}
</style>
</body>
</html>
<script>
Ext.namespace('Ext.combos_selec');

  Ext.combos_selec.countiesfl = [
 <?php 
	$q="SELECT l.`IdCounty`, l.`County`, l.`Servidor` FROM xima.lscounty l WHERE l.`State`='FL' ORDER BY l.`County`;";
	$res=mysql_query($q,$conex)or die(mysql_error());
	$i=0;
	while($r=mysql_fetch_array($res))
	{
		if($i>0)echo ",";
		echo "[".$r['IdCounty'].",'".$r['County']."']";
		$i++;
	}
 ?>]; 
 
Ext.BLANK_IMAGE_URL='includes/ext/resources/images/default/s.gif';
Ext.onReady(function(){
/////////////////Variables////////////////////
	var win,select_st='',select_pt='-1';
	Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
/////////////////FIN Variables////////////////////
	var winLoad=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div id="resultupload" style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
	var winLoadw=new Ext.Window({
		 title: 'Please wait..',
		 width: 200,
		 height: 80,
	 	 autoScroll: false,
		 resizable: false,
		 modal: true,
		 border:false,
		 plain: false,
		 html: '<div  style="background:#FFF"><img src="images/ac.gif" width="32" height="32" alt="Please wait..!!" />'+
				' <span style="color:red;font-weight: bold;">Please wait..!!! </span><br/></div>'						 
	});
		
								
///////////Cargas de data dinamica///////////////
	var store = new Ext.data.JsonStore({
		totalProperty: 'total',
		baseParams:{
			tipo:'passcontrol'
		},
		root: 'results',
//		url: 'php/grid_data3.php?<?php echo $auxurl ?>',
		proxy:new Ext.data.HttpProxy({ url: 'php/grid_data4.php?<?php echo $auxurl ?>', timeout: 3600000 }),
		fields: [
			{name: 'task', type: 'int'},
			{name: 'actionID', type: 'int'},
			{name: 'orden', type: 'int'},
			{name: 'titulo', type: 'string'},
			'current',
			'diffc',
			'tipocampo',
			<?php
			foreach ($arrdata as $k => $v)
			{
				echo "'i{$v['dataIndex']}',";
			}
			?>	
			
		]
	});
///////////FIN Cargas de data dinamica///////////////
	
////////////////barra de pagineo//////////////////////
	var pagingBar = new Ext.PagingToolbar({
        pageSize: 5000,
        store: store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
        emptyMsg: "No topics to display" ,
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'add_butt',
			text: 'Add Action',
			tooltip: 'Click to Add Action Id',
			handler: doActionId
        },{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/cross.gif',
			id: 'del_butt',
			text: 'Delete Action',
			tooltip: 'Click to Delete Action Id',
			handler: doDel
        },'-',{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/refresh.png',
			id: 'compare_butt',
			text: 'Compare Dates',
			tooltip: 'Click to Compare Date',
			handler: doCompare
        },'-',{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/editar.png',
			id: 'admintask_butt',
			text: 'Task',
			tooltip: 'Click to Administrar los task',
			handler: admintask
        },'-',{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/refresh.png',
			id: 'view_butt',
			text: 'View Data',
			tooltip: 'Click to View Data',
			handler: doView
        },{
			width:200,
			fieldLabel:'County',
			name:'cbCounty',
			id:'cbCounty',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['id', 'text'],
				data : Ext.combos_selec.countiesfl 
			}),
			mode: 'local',
			valueField: 'id',
			displayField: 'text',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value:<?php echo $_GET['c'] ?>,
			listeners: {'select': searchActive} 
		},'-',{
			width:200,
			fieldLabel:'Server',
			name:'cbServer',
			id:'cbServer',
			xtype:'combo',
			store: new Ext.data.SimpleStore({
				fields: ['id', 'text'],
				data : [['7','Server 7'],['8','Server 8'],['10','Server 10'],['11','Server 11'],['RT','Server RT']] 
			}),
			mode: 'local',
			valueField: 'id',
			displayField: 'text',
			triggerAction: 'all',            
			selectOnFocus: true,
			allowBlank: false,
			value:'<?php echo $_GET['server'] ?>',
			listeners: {'select': searchActive} 
		},'-',{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/refresh.png',
			id: 'saveCurrent_butt',
			text: 'Save Current',
			tooltip: 'Click to Save Current',
			handler: saveCurrent
        },{
			xtype: 'hidden',
			id: 'actionselec',	
			name: 'actionselec'
		}]
	});
////////////////FIN barra de pagineo//////////////////////
	

//////////////////Manejo de Eventos//////////////////////

	var arrdata;
	var contadorj;
	var marcados="";


	function searchActive(){
		location.href='passcontrol.php?c='+Ext.getCmp("cbCounty").getValue()+'&server='+Ext.getCmp("cbServer").getValue();			
	}
	
	function doView(){
		var marc=obtenerSeleccionados();
		var sel=Array();
		for(var k in marc){
			if(k != "remove" )
				sel.push(marc[k].data.actionID);
		}
		view.store.load({params:{
				c		:Ext.getCmp("cbCounty").getValue(),
				server	:Ext.getCmp("cbServer").getValue(),
				querys	:sel.join(',')
		}});
		
		
		view.win.show();
	}
	
	function doCompare(){
		winLoad.show();
		Ext.Ajax.request( 
		{  
			waitMsg: 'Processing...',
			url: 'php/grid_data3.php', 
			method: 'POST',
			timeout :600000,
			params: {
				tipo: 'getdatesPass',
				idcounty: Ext.getCmp("cbCounty").getValue()
			},	
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','ERROR');
			},
			success:function(response,options){
				winLoad.close();
				
				var datesc=Ext.decode(response.responseText);

				var formul2 = new Ext.FormPanel({
					frame:true,
					height      : 280,
					//bodyStyle:'padding:5px 5px 0',			
					items: [{
						// Use the default, automatic layout to distribute the controls evenly
						// across a single row
						xtype: 'radiogroup',
						fieldLabel: 'Dates',
						columns: 5,
						items: datesc	
					}]			
				});
				var wind2 = new Ext.Window({
						title: 'Compare dates',
						iconCls: 'x-icon-templates',
						//layout      : 'fit',
						width       : 800,
						height      : 300,
						resizable   : false,
						modal	 	: true,
						plain       : true,
						autoScroll  : true,
						items		: formul2,				
						buttonAlign: 'center',
						buttons: [{
								text: '<b>Compare</b>',
								cls: 'x-btn-text-icon',			
								icon: 'images/refresh.png',
								formBind: true,
								handler  : function(){
									if(formul2.getForm().isValid()){
										var texto=formul2.getForm().getValues(true);
										if(texto.indexOf("=")>=0)
											//Ext.Msg.alert('Submitted Values', 'tiene un solo check '+texto.replace("cbdates=",""));
											location.href='passcontrol.php?c='+Ext.getCmp("cbCounty").getValue()+'&fmas='+texto.replace("cbdates=","");
										else
											Ext.Msg.alert('Submitted Values', 'Debe seleccionar fecha a comparar');
										//Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+ formul2.getForm().getValues(true));//.replace(/&/g,', ')
									}							
								}
							},'->',{
								text: '<b>Cancel</b>',
								cls: 'x-btn-text-icon',			
								icon: 'images/cross.gif',
								handler  : function(){
									wind2.close();
							   }
						}]					
					});
			
				wind2.show();
				wind2.addListener("beforeshow",function(wind){
						formul2.getForm().reset();
				});
				
			}                                
		});
		

	}//fin function doCompare()
	
	function doActionId(){

		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 55,
			items: [{
				xtype:'textfield',
				fieldLabel: 'Action',
				name: 'action',
				id: 'action',
				width: 280,
				allowBlank: false  
			},{
				xtype:'numberfield',
				fieldLabel: 'Orden',
				name: 'orden',
				id: 'orden',
				allowBlank: false  
			},{
				xtype:'combo',
				store:new Ext.data.SimpleStore({
				fields: ['id','texto'],
				data  : [
					['int','Int'],
					['int','Float'],
					['date','Date']
					]
				}),
				fieldLabel:'DataType',
				id:'datatype',
				name:'datatype',
				hiddenName: 'datatype',
				valueField: 'id',
				displayField: 'texto',
				triggerAction: 'all',
				mode: 'local',
				value: 'int',
				allowBlank: false  
			}],
			buttonAlign: 'center',
			buttons: [{
				text: '<b>Save</b>',
				cls: 'x-btn-text-icon',			
				icon: 'images/disk.png',
                formBind: true,
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
				    if (form.getForm().isValid()) {
						form.getForm().submit({
							url: 'php/grid_add.php',
							waitTitle   :'Please wait!',
							waitMsg     :'Loading...',
							params: {
								tipo : 'actionid'										
							},	
							timeout: 100000,
							method :'POST',
							success: function(form, action) {
								w.close();
								store.reload();
								Ext.Msg.alert('Success', action.result.msg);
								
							},
							failure: function(form, action) {
								Ext.Msg.alert('Error', action.result.msg);
							}
						});
					}
				}
			},'->',{
				text: 'Close',
				cls: 'x-btn-text-icon',			
				icon: 'images/cross.gif',
				handler: function(b){
					w.close();
				}
			}]
		});
		
		
		var w = new Ext.Window({
			title: 'New Action Id', 
			width: 400,
			height: 170,
			layout: 'fit',
			plain: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
		w.addListener("beforeshow",function(w){
			form.getForm().reset();
		});

	}//fin function doCompare()

	function obtenerSeleccionados(){
		var selec = grid.selModel.getSelections();
		return selec;
	}

	function doDel(){
		Ext.MessageBox.confirm('Delete Action Id','Are you sure delete row?.',//
			function(btn,text)
			{
				if(btn=='yes')
				{
					var obtenidos=obtenerSeleccionados();
					if(!obtenidos){
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'You must select a row, by clicking on it, for the delete to work.',
							buttons: Ext.MessageBox.OK,
							icon:Ext.MessageBox.ERROR
						});						
						return;
					}
					//alert(obtenidos);//return;		
					obtenidos=obtenidos.replace('(','');
					obtenidos=obtenidos.replace(')','');

					Ext.Ajax.request({   
						waitMsg: 'Saving changes...',
						url: 'php/grid_del.php', 
						method: 'POST',
						params: {
							tipo : 'actionid',
							actionid: obtenidos			
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','Error editing');
							store.reload();
						},
						
						success:function(response,options){
						
							store.reload();
						}
					}); 
				}
			}
		)
	}

						
	function doEdit(oGrid_Event) {
		var gid = oGrid_Event.record.data.actionid;
		var gfield = oGrid_Event.field;
		var gvaluenew = oGrid_Event.value;

		Ext.Ajax.request({
			url: 'php/grid_data3.php',
			method :'POST',
			params: {
				gid : gid,
				tipo : 'actionid',
				gfield : gfield,
				gvaluenew : gvaluenew,
				idcounty: Ext.getCmp("cbCounty").getValue() 		
			},
			waitTitle   :'Please wait!',
			waitMsg     :'Saving changes...',
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
			},
			success: function(respuesta){
				if(gfield=='valprom2')
				{
					//store.load();
					oGrid_Event.record.data.valprom=((oGrid_Event.record.data.valpromcru*gvaluenew)/oGrid_Event.record.data.porc);//
					/*if(Math.abs(val) <= record.data.valpromcru )
						return '<span style="color:green;font-weight: bold;">' + val + '</span>';
					else
						return '<span style="color:red;font-weight: bold;">' + val + '</span>';
						*/
				}
				grid.getStore().commitChanges();  
				return true;					
			} 
		});
	}; 
				

	function uploaddata(bd,ciclo){
		if(ciclo<=contadorj)
		{
			Ext.Ajax.request({   
				waitMsg: 'Saving changes...',
				url: 'php/grid_data2.php',				
				timeout :99000000, 
				method: 'POST',
				params: {
					bd: bd,
					tipo: 'uploadverify'
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning',response.responseText);
				},
				
				success:function(response,options){
					var resp = Ext.decode(response.responseText);

					if(ciclo<=1)document.getElementById('resultupload').innerHTML='';
					document.getElementById('resultupload').innerHTML += resp.msg;
					ciclo++;
					if(ciclo<=contadorj)
						uploaddata(arrdata[(ciclo-1)][1],ciclo);
					else
					{
						store.reload();
						winLoad.close();
						verifytables();
					}	
				}
			}); 
		}	
		return;
	}

	function verifytables(){
		winLoadw.show();
			Ext.Ajax.request({   
				waitMsg: 'Saving changes...',
				url: 'php/grid_data2.php',				
				timeout :99000000, 
				method: 'POST',
				params: {
					tipo: 'verifytables'
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning',response.responseText);
				},
				
				success:function(response,options){
					var resp = Ext.decode(response.responseText);
					winLoadw.hide();
					//Ext.MessageBox.alert('Warning', resp.msg);
					var htmlTag='<div  style="background:#FFF">'+ resp.msg+'</div>';

					var winVerify=new Ext.Window({
									 title: 'Verify Tables',
									 width: 400,
									 height: 250,
									 autoScroll: true,
									 resizable: false,
									 modal: true,
									 border:false,
									 plain: false,
									 html: htmlTag,
								buttonAlign: 'center',
								buttons: [{
									handler:function(){ 
										winVerify.close();
									},
									scope:this,
									iconCls:'icon',
									icon: 'images/cross.gif',						
									text:'Close'
								}]
									 
							});
					winVerify.show();
					
				}
			}); 

	}

	function changeColor(val, p, record){
		if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
		if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
		if(record.data.tipocampo=='date'){
			val=Ext.util.Format.number(val, '000');
			val=val.substring(0,4)+'-'+val.substring(4,6)+'-'+val.substring(6,8);
		};
		return '<span style="color:green;font-weight: bold;">' + val + '</span>';
    }

	function render1(val,p,record){
		
		if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
		if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
		return String.format('<b>{0}</b>',val);
	}

	function render2(val,p,record){
		if(record.data.tipocampo=='int')val=Ext.util.Format.number(val, '0,000.00');
		if(record.data.tipocampo=='porc')val=Ext.util.Format.number(val, '0,000.00');
		return val;
	}
	
	function saveCurrent(){
		Ext.MessageBox.confirm('Save Current', 'Are you sure ?', function(btn){
		   if(btn === 'yes'){
				Ext.Ajax.request({
					waitMsg: 'Saving changes...',
				   	url: 'php/grid_data3.php?',
				   	success: function (){
						location.reload()
					},
				   	failure: function (){
						Ext.MessageBox.alert('Error', 'Ah ocurrido un error');
					},
					params: {
						tipo: 'saveCurrent',
						idcounty: Ext.getCmp("cbCounty").getValue(),
						idserver: Ext.getCmp("cbServer").getValue()
					}
				});
		   }
		   else{
		   }
		 });
	}

	var ACTIONSELEC='';
	function admintask(){
		var marc=obtenerSeleccionados();
		var actionid='';
		var can=0;
		for(var k in marc){
			if(k != "remove" )
			{
				actionid=marc[k].data.actionID;
				can++;	
			}
		}
		if(can!=1)
		{	Ext.MessageBox.alert('Warning','Debes seleccionar una accion para administrar la tarea');
			return;
		}
		viewtask.store.load({params:{
			c:Ext.getCmp("cbCounty").getValue(),
			actionid:actionid
		}});
		Ext.getCmp("actionselec").setValue(actionid);
		viewtask.win.show();
	}
	
//////////////////FIN Manejo de Eventos//////////////////////
	
	
///////////////////renders/////////////////////////
	function checkRow(val,p,record){
		return String.format('<input type="checkbox" name="del'+record.data.idcounty+'"  id="del'+record.data.idcounty+'">',record.data.idcounty);
	}
	function renderStatus(value, p, record){
		if(value=='1') return '<div style="background: url(\'images/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;" title="Terminada">&nbsp;</div>';
		else if(value=='0') return '<div style="background: url(\'images/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;" title="Por Revisar">&nbsp;</div>';
		else '<div style="background: url(\'images/semaforo_status.png\') no-repeat scroll 0px -0px transparent; width:0px; height: 0px;" ></div>';
	}
	function rendertask(val, p, record){
        return String.format('<a href="javascript:void(0)" onClick="admintask({0})" ><img src="images/editar.png" border="0" title="Administrar Tareas" alt="Administrar Tareas"></a>',val);
	}	
///////////////////FIN renders//////////////////////
 
/////////////////Grid//////////////////////////////////
	var mySelectionModel = new Ext.grid.CheckboxSelectionModel({singleSelect: false});
	var grid = new Ext.grid.EditorGridPanel({
		store: store,
		stripeRows :true,
		iconCls: 'icon',
		viewConfig: {
			forceFit:true
		},
		columns: [	  
			mySelectionModel
			//,{header: ' ', width: 30, sortable: true, align: 'center', dataIndex: 'actionID', renderer: rendertask}
			,{header: "", width: 30, align: 'center', sortable: true, dataIndex: 'task',renderer: renderStatus}
			,{header: "ORDEN", width: 70, align: 'center', sortable: true, dataIndex: 'orden', editor: new Ext.form.TextField({allowBlank: false})}
			,{header: "ACTIONID", width: 70, align: 'center', sortable: true, dataIndex: 'actionID'}
			,{id:'actionid',header: "TITULO", width: 300, align: 'left', sortable: true, dataIndex: 'titulo', editor: new Ext.form.TextField({allowBlank: false})}		
			,{id:'actionid',header: "PORCE", width: 80, align: 'left', sortable: true, dataIndex: 'porce', editor: new Ext.form.TextField({allowBlank: false})}			
			<?php
			$tFachas=count($arrdata)-1;
			foreach ($arrdata as $k => $v)
			{
				$tFachas = $tFachas-$k;
				echo ",{header: '{$v['header']}', width: 90, sortable: false, align: 'center', dataIndex: 'i{$v['dataIndex']}',renderer: changeColor}";
				if($tFachas>0){
					echo ",{header: 'Diff {$tFachas}', width: 90, sortable: false, align: 'center', dataIndex: 'i{$v['dataIndex']}',renderer: calcularDiff}";
				}
				else{
					$headF=",{header: 'Diff 1', width: 90, sortable: false, align: 'center', dataIndex: 'i{$v['dataIndex']}',renderer: calcularDiff}";
				}
			}
			echo $headF;
			
			?>	
			,{header: 'Current', width: 90, hidden:true, sortable: true, align: 'center', dataIndex: 'diffc',renderer: changeColor}
			,{header: 'Current Diff', width: 90, hidden:true, sortable: true, align: 'center', dataIndex: 'diffc',renderer: changeColor}
			
		],
		clicksToEdit:2,
		autoScroll: true,
		sm: mySelectionModel,
		height: Ext.getBody().getViewSize().height-50,
		frame:true,
		title:'Pass Control',
		loadMask:true,
		tbar: pagingBar 
	});
	
	function calcularDiff(val, metaData, record, rowIndex, colIndex, store){
		var EX=/^i\d+$/;
		if(record.data.tipocampo=='porc'){
			var anterior='';
			for( var k in record.data){
				if(EX.test(k)){
					if(anterior!=''){
						var diff=val-record.data[anterior];
						anterior=k;
					}
					else{
						anterior=k;
					}
				}
			}
			val=Ext.util.Format.number(diff, '0,000.00');
		}
		else{
			val='';
		};
		return val;
	}
	
/////////////////FIN Grid////////////////////////////

//////////////VIEWPORT////////////////////////////////
	var pag = new Ext.Viewport({
		layout: 'border',
		hideBorders: true,
		monitorResize: true,
		items: [{
			region: 'north',
			height: 25,
			items: Ext.getCmp('menu_page')
		},{
			region:'center',
			autoHeight: true,
			items: grid
		}]
	});
//////////////FIN VIEWPORT////////////////////////////////
	
//////////////////Listener///////////////////////////
grid.addListener('afteredit', doEdit);
//////////////////FIN Listener///////////////////////

/////////////Inicializar Grid////////////////////////
store.load();
/////////////FIN Inicializar Grid////////////////////

///VENTANA DEL BOTON VIEW
var view={};
view.store = new Ext.data.JsonStore({
	proxy:new Ext.data.HttpProxy({ url: 'php/grid_data3.php?tipo=viewData', timeout: 3600000 }),
	root: 'data',
	remoteSort :true,
	baseParams: {start:0,limit:500},
	totalProperty: 'total',
	fields: [
	'ParcelID',    'pid',    'Xcode',    'CCode',    'Address',    'AssesedV',    'Bath',    'Beds',    'Bheated',    'book',
	'BuildingV',    'CCodeD',    'City',    'ffc',    'Folio',    'LandV',    'Legal',    'Lsqft',    'Owner',    'Owner_A',    'Owner_C',
    'Owner_P',    'Owner_S',    'Owner_Z',    'OZip',    'page',    'SaleDate',    'SalePrice',    'TaxableV',    'TSqft',    'Units',    'YrBuilt',    'Zip'
	]
});
view.pager=new Ext.PagingToolbar({
        store: view.store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
		pageSize:500,
        emptyMsg: "No topics to display" });
view.grid = new Ext.grid.GridPanel({
	store: view.store,
	frame:true,
	stripeRows :true,
	loadMask:true,
	tbar: view.pager,
	columns: [
		new Ext.grid.RowNumberer(),
		{header:'ParcelID', dataIndex:'ParcelID',sortable: true},
		{header:'Address', dataIndex:'Address', sortable: true},
		{header:'book', dataIndex:'book', sortable: true},
		{header:'page', dataIndex:'page', sortable: true},
		{header:'SaleDate', dataIndex:'SaleDate', sortable: true},
		{header:'SalePrice', dataIndex:'SalePrice', sortable: true},
		{header:'Owner', dataIndex:'Owner', sortable: true},
		{header:'Owner_A', dataIndex:'Owner_A', sortable: true},
		{header:'Owner_C', dataIndex:'Owner_C', sortable: true},
		{header:'Owner_P', dataIndex:'Owner_P', sortable: true},
		{header:'Owner_S', dataIndex:'Owner_S', sortable: true},
		{header:'Owner_Z', dataIndex:'Owner_Z', sortable: true},
		{header:'pid', dataIndex:'pid',sortable: true},
		{header:'Xcode', dataIndex:'Xcode',sortable: true},
		{header:'CCode', dataIndex:'CCode', sortable: true},
		{header:'AssesedV', dataIndex:'AssesedV', sortable: true},
		{header:'Bath', dataIndex:'Bath', sortable: true},
		{header:'Beds', dataIndex:'Beds', sortable: true},
		{header:'BuildingV', dataIndex:'BuildingV', sortable: true},
		{header:'CCodeD', dataIndex:'CCodeD', sortable: true},
		{header:'City', dataIndex:'City', sortable: true},
		{header:'Folio', dataIndex:'Folio', sortable: true},
		{header:'LandV', dataIndex:'LandV', sortable: true},
		{header:'Legal', dataIndex:'Legal', sortable: true},
		{header:'Lsqft', dataIndex:'Lsqft', sortable: true},
		{header:'TaxableV', dataIndex:'TaxableV', sortable: true},
		{header:'TSqft', dataIndex:'TSqft', sortable: true},
		{header:'Units', dataIndex:'Units', sortable: true},
		{header:'YrBuilt', dataIndex:'YrBuilt', sortable: true},
		{header:'Zip', dataIndex:'Zip', sortable: true}
	],
	border: false,
	stripeRows: true
});
view.pager.on('beforechange',function(bar,params){
		var marc=obtenerSeleccionados();
		var sel=Array();
		for(var k in marc){
			if(k != "remove" )
				sel.push(marc[k].data.actionID);
		}
		sel=sel.join(',');
		params.c		=Ext.getCmp("cbCounty").getValue();
		params.server	=Ext.getCmp("cbServer").getValue();
		params.querys	=sel;
});
view.win = new Ext.Window({
	title: 'View Data',
	maximizable :true,
	closeAction :'hide',
	modal:true,
	layout: 'fit',
	width: 1100,
	height: Ext.getBody().getViewSize().height-80,
	items: view.grid
});
});

///VENTANA DEL BOTON  DE LOS TASK
var viewtask={};
viewtask.store = new Ext.data.JsonStore({
	proxy:new Ext.data.HttpProxy({ url: 'php/grid_data4.php?tipo=viewtask', timeout: 3600000 }),
	root: 'data',
	remoteSort :true,
	baseParams: {start:0,limit:500},
	totalProperty: 'total',
	fields: [
		'idtask',    'actionid', 'action',    'countyid',    'fechacreacion',    'tarea',
		'status',    'fechacambia',    'titulo',    'county'
	]
});
viewtask.pager=new Ext.PagingToolbar({
		store: viewtask.store,
        displayInfo: true,
        displayMsg: '<b>Total: {2}</b>',
		pageSize:500,
        emptyMsg: "No topics to display" ,
		items:[{
			iconCls:'icon',
			cls: 'x-btn-text-icon',			
			icon: 'images/add.png',
			id: 'addtask_butt',
			text: 'Adicionar Tarea',
			tooltip: 'Click para adicionar tareas',
			handler: adicionartask
        }]

});
		
function renderStatust(value, p, record){
	if(value=='1') return 'Terminada';
	if(value=='0') return 'Por revisar';
}			
		
function adicionartask(){
	var formcountyedit = new Ext.form.FormPanel({			
		autoScroll: true,
		bodyStyle:'padding: 10px',  
		layout:'form',
		border:true,										
		items: [{
			xtype:'textfield',
			fieldLabel:'Tarea',
			name:'tarea',
			id:'tarea',
			width:280,
			allowBlank: false
		}],
		buttons: [{
			scope:this,
			id:'buttarea',		
			text:'<b>Guardar Tarea</b>',										
			handler:function(){	
				if (formcountyedit .getForm().isValid()){ 
					formcountyedit .getForm().submit({
						url : 'php/grid_data4.php',
						method :'POST',
						params: {
							tipo : 'addtask',
							c:Ext.getCmp("cbCounty").getValue(),
							actionid: Ext.getCmp("actionselec").getValue() 
						},
						waitTitle   :'Please Wait!',
						waitMsg     :'Loading...',
						success: function(form,action){
							Ext.Msg.alert('Success',action.result.msg);
							wintaskmanage.close();
 							viewtask.store.load({params:{
								c:Ext.getCmp("cbCounty").getValue(),
								actionid:Ext.getCmp("actionselec").getValue()
							}});
						},
						failure: function(form,action){
							Ext.Msg.alert('Warning',action.result.msg);                                                                      
						}      
					});
				}										
			}
		}]
	});										
		
	var wintaskmanage = new Ext.Window({  
		title:'Administrar Tareas',
		width:450,  			
		layout: "fit",
		height: 120,  
		border: false,
		autoScroll: true,
		modal: true,
		plain: true,
		items: formcountyedit
	});  
	wintaskmanage.show(); 								
}			

viewtask.grid = new Ext.grid.EditorGridPanel({
	store: viewtask.store,
	frame:true,
	stripeRows :true,
	loadMask:true,
	tbar: viewtask.pager,
	columns: [
		new Ext.grid.RowNumberer()
		,{header: 'ActionID', width: 60, sortable: true, align: 'center', dataIndex: 'actionid'}
		,{header: 'Action', width: 180, sortable: true, align: 'left', dataIndex: 'action'}
		,{header: 'County', width: 120, sortable: true, align: 'left', dataIndex: 'county'}
		,{header: 'Tarea', width: 400, sortable: true, align: 'left', dataIndex: 'tarea', editor: new Ext.form.TextField({allowBlank: false})}
		,{header: 'Creacion', width: 120, sortable: true, align: 'left', dataIndex: 'fechacreacion'}
		,{header: 'Status ', width: 100, sortable: true, align: 'center', dataIndex: 'status',renderer: renderStatust, editor: new Ext.form.ComboBox({
							store: new Ext.data.SimpleStore({
								fields: ['idstatus', 'texto'],
								data : [['0', 'Por revisar'],['1', 'Terminada']]
							}),						
							fieldLabel:'Status',
							id:'idstatusprod',
							name:'idstatusprod',
							hiddenName: 'UD_idstatusprod',
							valueField: 'idstatus',
							displayField: 'texto',
							typeAhead: true,
							triggerAction: 'all',
							mode: 'local'
						})}
	],
	clicksToEdit:2,
	border: false
	//,stripeRows: true
});
						
	function doEditTask(oGrid_Event) {
		var gid = oGrid_Event.record.data.idtask;
		var gfield = oGrid_Event.field;
		var gvaluenew = oGrid_Event.value;

		Ext.Ajax.request({
			url: 'php/grid_data4.php',
			method :'POST',
			params: {
				gid : gid,
				tipo : 'editartask',
				gfield : gfield,
				gvaluenew : gvaluenew,
				idcounty: Ext.getCmp("cbCounty").getValue() 		
			},
			waitTitle   :'Please wait!',
			waitMsg     :'Saving changes...',
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','Error editing');
			},
			success: function(respuesta){
				viewtask.grid.getStore().commitChanges(); 
				return true;					
			} 
		});
	}; 

viewtask.grid.addListener('afteredit', doEditTask);

/*
viewtask.pager.on('beforechange',function(bar,params){
		var marc=obtenerSeleccionados();
		var sel=Array();
		for(var k in marc){
			if(k != "remove" )
				sel.push(marc[k].data.actionID);
		}
		sel=sel.join(',');
		params.c		=Ext.getCmp("cbCounty").getValue();
		params.server	=Ext.getCmp("cbServer").getValue();
		params.querys	=sel;
});
*/
viewtask.win = new Ext.Window({
	title: 'Administrar Task',
	maximizable :true,
	closeAction :'hide',
	modal:true,
	layout: 'fit',
	width: 1100,
	height: 350,
	items: viewtask.grid,
	listeners:{
		hide: {
			fn: function (el, e) {
				location.href='passcontrol2.php?c='+Ext.getCmp("cbCounty").getValue()+'&server='+Ext.getCmp("cbServer").getValue();
			}
		}		
	}
});
</script>