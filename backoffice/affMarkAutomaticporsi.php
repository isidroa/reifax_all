<?php 
class Fecha {
  var $fecha;
  function Fecha($anio=0, $mes=0, $dia=0) 
  {
       if($anio==0) $anio = Date("Y");
       if($mes==0) $mes = Date("m");
       if($dia==0) $dia = Date("d");
       $this -> fecha = Date("Y-m-d", mktime(0,0,0,$mes,$dia,$anio));
  }

  function SumarFecha($anio = 0, $mes = 0, $dia = 0) 
  {
       $array_date = explode("-", $this->fecha);
       $this->fecha = Date("Y-m-d", mktime(0, 0, 0, $array_date[1] + $mes, $array_date[2] + $dia, $array_date[0] + $anio));
  }
 
  function getFecha() { return $this->fecha; }
}

//compara dos fechas 
function compara_fechas($fecha1,$fecha2)
{
	if(strtotime($fecha1)>strtotime($fecha2)) return 1;
	if(strtotime($fecha1)==strtotime($fecha2)) return 0;
	if(strtotime($fecha1)<strtotime($fecha2)) return -1;
}

//Convierte fecha de normal a mysql 
function fechaNormal2Mysql($fecha)
{ 
    ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
    return $lafecha; 
} 

function arreglarFechasInvalidasresp($fecha)
{
//	$fecha=fechaNormal2Mysql($fecha);//convierte 30/02/2008 a 2008-02-28
	$timestamp=strtotime($fecha);
	$fecha=date('Y-m-d',$timestamp);
	return $fecha;
}

//arregla las fechas invalidas
function arreglarFechasInvalidas($fecha)
{
	$timestamp=strtotime($fecha);
	$fecha=date('Y-m-d',$timestamp);
	return $fecha;
}

//ultimo dia del mes actual
function UltimoDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

//ultimo dia del mes anterior
function UltimoDiaAnteriorMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m')-1; 
	if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 0, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

//primer dia del mes actual
function PrimerDiaActualMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m'); 
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}
//primer dia del mes anterior
function PrimerDiaAnteriorMes()
{
	$VL_Ano = date ('Y'); 
	$VL_Mes = date ('m')-1; 
	if(date('m')==1){$VL_Ano--;$VL_Mes=12; }
	if (strlen($VL_Mes)==1)$VL_Mes='0'.$VL_Mes; 
	$VL_UltDia=date('d', mktime (0, 0, 0, $VL_Mes + 1, 1, $VL_Ano)); 
	$FechaFin=$VL_Ano.'-'.$VL_Mes.'-'.$VL_UltDia;  
	return $FechaFin ;
}

	$ECHOS=1;//0-->NO EJECUTAR LOS ECHOS 1-->EJECUTAR LOS ECHOS 

	include("php/conexion.php");
	$conex=conectar("xima");
	include("../properties_getprecio.php");

/*	
	//VALIDACION PARA CUANDO EL ZCRON  O CUALQUIER PERSONA EJECUTE LA COBRANZA MAS DE DOS VECES AL DIA, SOLO LO HAGA UNA VEZ.
	$sl=rand(0, 1200);
	echo "<br>".date("Y-m-d H:i:s");	
	echo "<br>Sleep: $sl ";
	sleep($sl);
	echo "<br>".date("Y-m-d H:i:s");	
	echo "<br>".
	$query="SELECT valor FROM `parametros` where campo ='cobranzadiaria' and valor=curdate()";
	$res1=mysql_query($query) or die($query.mysql_error());
	$row1= mysql_fetch_array($res1, MYSQL_ASSOC);
	if($row1['valor']<>'')	die("<HR/>NO HAY COBRANZA YA SE EJECUTO EN LA OTRA SESION.. OJO!!!!!! <HR/>");
	echo "<HR/>PROCESAR COBRANZA <HR/>";
	$query="UPDATE parametros p SET valor =curdate() WHERE p.`campo`='cobranzadiaria'";
	mysql_query($query) or die($query.mysql_error());
*/	

/*
//SI ES DIA 1 DE MES HACER COBRAR LAS COMISIONES Y LOS CIERRES DE MES
	if(date('d')=='01' || date('d')=='1' )
	{
		//HACER INGRESO DE LAS COMISIONES 
		include("comisiones.php");

		//HACER INGRESO DE LAS APERTURAS Y CIERRES 
		include("aperturascierres.php");
	}

	//VERIFICAR LOS TRIALS SI SE PASAN O NO A ACTIVOS PARA COBRARARLES 
	echo "<HR/>";
	$querycuota="SELECT c.freedays,x.userid,b.fechacobro
				FROM xima.ximausrs x
				INNER JOIN xima.cobro_porcentajes c ON c.userid=x.userid
				INNER JOIN xima.usr_cobros b ON b.userid=x.userid
				WHERE x.idstatus=1 ; ";//AND X1.USERID=1413 
	if($ECHOS==1)echo $querycuota.'<br/>';
	$rescuota=mysql_query($querycuota) or die($querycuota.mysql_error());
	$i=1;
	$fechainsert=date("Y-m-d H:i:s");			
	while ($row= mysql_fetch_array($rescuota, MYSQL_ASSOC))
	{
		$userid=$row['userid'];
		$freedays=$row['freedays'];
		$fechacobro=$row['fechacobro'];

		$query="SELECT DATEDIFF(curdate(),'$fechacobro') diffe ";
		$res1=mysql_query($query) or die($query.mysql_error());
		$row1= mysql_fetch_array($res1, MYSQL_ASSOC);
		$diffe=$row1['diffe'];	
		
		if($diffe>=($freedays-5))
		{
			echo "<br/>TRIALS====> userid=$userid | freedays=$freedays | fechacobro=$fechacobro | diffe=$diffe".
			$queryIns="	UPDATE xima.ximausrs SET  idstatus=3 WHERE userid=$userid";
			mysql_query($queryIns) or die($queryIns.mysql_error());
		}
	}
*/
	echo "<HR/>";
	//return;

//HACIENDO EL COBRO DE LOS 'active','inactive','freeze','freezeinac',free(se les cobra 0)
	$primeravez=0;
	$diahoy=date('N');
	$fechahoy=' ';
	$querycuota="SELECT distinct DAY(NOW()) diahoy, MONTH(NOW()) meshoy, YEAR(NOW()) aniohoy,
						x1.userid,x1.name,x1.surname,u.fechacobro paydate, DAY(u.fechacobro) diausuario,
						MONTH(u.fechacobro) mesusuario, YEAR(u.fechacobro) aniousuario,
						t.status,p.name nameprod,u.idproductobase,p.idproducto, u.idstatus, u.freedays,sp.saldo
				FROM xima.usr_cobros u
				INNER JOIN (xima.ximausrs x1)ON(x1.userid=u.userid)
				INNER JOIN (xima.cobro_porcentajes sp)ON(sp.userid=u.userid)
				INNER JOIN xima.usr_productobase s on s.idproductobase= u.idproductobase
				INNER JOIN xima.usr_producto p on p.idproducto= s.idproducto
				INNER JOIN xima.status t on t.idstatus= u.idstatus
				WHERE p.pu=0 and u.idstatus in (3,5,6,7,4)  AND X1.USERID in (2468,2543,889,2542,2880,1284,958)
				order by 	x1.userid ,p.idproducto desc ";//AND X1.USERID=1413  (2468,2543,889,2542,2880,1284,958)
				
if($ECHOS==1)echo $querycuota.'<br/>';
	$rescuota=mysql_query($querycuota) or die($querycuota.mysql_error());
	$i=1;
	$comparadoanterior='';
	$productoanterior=0;
	while ($row= mysql_fetch_array($rescuota, MYSQL_ASSOC))
	{
		$insertacuota=0;
		$SaltarUsurario=0;
		$comparado=$row['userid'];
		$crrcomparado=$row['crr'];
		$namecomparado=$row['name'];
		$surnamecomparado=$row['surname'];
		$paydatecomparado=$row['paydate'];
		$pctgr2lccomparado=$row['pctgr2lc'];
		$comisioncomparado=$row['comision'];

		$query="SELECT c.saldo FROM xima.cobro_porcentajes c WHERE c.userid=$comparado";
		$ressaldo=mysql_query($query) or die($query.mysql_error());
		$rowsaldo= mysql_fetch_array($ressaldo, MYSQL_ASSOC);
		$saldocomparado=$rowsaldo['saldo'];
		
		$anualpagocomparado=$row['anualpago'];
		$diausuario=$row['diausuario'];
		$row['priceprod']=0;
		$saltarinsertoper4=0;
		if($comparado==$comparadoanterior)
		{	//el usuario tiene 2 o mas productos
			if($productoanterior==2 && $row['idproducto']==1)
				$saltarinsertoper4=1;
			if($productoanterior==3 && $row['idproducto']==2)
				$saltarinsertoper4=1;
		}
		else
		{
			if($ECHOS==1)echo '<br><br><br><hr><hr><hr>';
		}
		
if($ECHOS==1)echo '<br><br> '.$i.' Userid: '.$comparado.' Paydate: '.$row['paydate'];
//return;
		
		if($saltarinsertoper4==1)if($ECHOS==1)echo '<br><strong> REGISTRO SALTADO </strong>';

//INSERTANDO LAS CUOTAS MENSUALES, INICIALES Y SUBSIGUIENTES
//COBRANDO LAS CUOTAS MENSUALES A LOS USUARIOS QUE SE LES VENCE HOY Y A LOS QUE NOS DEBEN.
			if($row['meshoy']==1 || $row['meshoy']=='01')
			{
				//echo "entra if <br>";
				if($row['diahoy']>=$row['diausuario'])
					$fecha=$row['aniohoy']."-".$row['meshoy']."-".$row['diausuario'];
				else
					$fecha=($row['aniohoy']-1)."-12-".$row['diausuario'];
			}
			else
			{
				//echo "<br>$comparado $paydatecomparado ";			
				$fecultimodia=UltimoDiaActualMes();
				$timest2=strtotime($fecultimodia);
				$ultdia=date('d',$timest2);
				
				//caso del ultimo dia de mes, como 30 junio, que le cobre a los  del 31  tambien
				if($ultdia==date('d') && $row['diausuario']>$ultdia)
					$row['diausuario']=$ultdia;		

				$fecha=$row['aniohoy']."-".$row['meshoy']."-".$row['diausuario'];
			}

//			$SaltarUsurario=1;
//			if($row['diausuario']==date('d'))$SaltarUsurario=0;
		
if($ECHOS==1)echo "<br>$comparado // ".$fecha." // ".$SaltarUsurario;

		if($SaltarUsurario==0)
		{
			$row['priceprod']=getpriceuser($comparado,$diausuario,'yes',$row['idproducto']);//($userid,$dia=0,$status='no',$idproducto=0)

			if($ECHOS==1)echo 	'<br> Montocuota: '.$row['priceprod'].' Paydate: '.$row['paydate'].' Iproducto: '.$row['idproducto'].
								' producto: '.$row['nameprod'].' status: '.$row['status'];
//return;
			$fecha=arreglarFechasInvalidas($fecha);
			$fechaevalcomparado=$fecha;
			if($row['idstatus']==6 || $row['idstatus']==7)//' freeze' 'freezeinac'
			{
				$montocuota=15*-1;	
				$idoper=16;
			}
			else
			{
				$montocuota=$row['priceprod']*-1;
				$idoper=4;
			}
/*
	
			$fechainsert=date("Y-m-d H:i:s");			
			if($row['idstatus']==4)//si es status free  no se le cobra
			{
				$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`)
						VALUES ($comparado,'$fechainsert',0,4)";
				if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
				mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

				$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
							VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Free')";					
				if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$queryIns;
				mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO			
			}
			else//si NO es status free  
			{
				$query="SELECT * FROM xima.f_frecuency f WHERE userid=$comparado";
				if($ECHOS==1)echo $query.'<br/>';
				$resq=mysql_query($query) or die($query.mysql_error());
				$rowq= mysql_fetch_array($resq, MYSQL_ASSOC);
				$payfrecuencycomparado=$rowq['idfrecuency'];
				if($payfrecuencycomparado==2)//pagos anuales
				{
					$anualpagocomparado++;
					if(($anualpagocomparado%12)==0)//LE TOCA PAGAR EL PAGO ANUAL...
					{
						//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
						$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`) 
								VALUES ($comparado,'$fechainsert',$montocuota,$idoper)";
						if($ECHOS==1)	echo '<br>PAGO ANUAL-->'.$query;
						mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

						//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
						$newsaldo=$saldocomparado+$montocuota;
						$queryIns="	UPDATE admin.saldoporcentaje SET  saldo=$newsaldo,anualpago=$anualpagocomparado WHERE userid=$comparado";
						mysql_query($queryIns) or die($queryIns.mysql_error());
						if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga --> Userid: '.$comparado.' Fechaevaluar: '.$fechainsert.' Paydate: '.$row['paydate'].' Montocuota: '.$montocuota;

						//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
						//que dando la resta de la cuota con el saldo en cero, se le ingresa un pago de cero
						if($newsaldo>=0)
						{
							$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
								VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Saldo Favor')";					
							if($ECHOS==1)	echo '<br>EXONERO SALDO A FAVOR-->'.$query;
							mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
						}						
					}
					else//LE TOCA EXONERACION DE PAGO ESTE MES...
					{
						$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`)
								VALUES ($comparado,'$fechainsert',0,$idoper)";
						if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
						mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

						$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
							VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Anual')";					
						if($ECHOS==1)	echo '<br>EXONERO ANUAL-->'.$query;
						mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO

						$queryIns="	UPDATE xima.cobro_porcentajes SET  anualpago=$anualpagocomparado WHERE userid=$comparado";
						mysql_query($queryIns) or die($queryIns.mysql_error());
					}
				}
				elseif($payfrecuencycomparado==3)//pagos trimestrales
				{
					$anualpagocomparado++;
					if(($anualpagocomparado%3)==0)//LE TOCA PAGAR EL PAGO TRIMESTRAL...
					{
						//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
						$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`) 
								VALUES ($comparado,'$fechainsert',$montocuota,$idoper)";
						if($ECHOS==1)	echo '<br>PAGO TRIMESTRAL-->'.$query;
						mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

						//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
						$newsaldo=$saldocomparado+$montocuota;
						$queryIns="	UPDATE admin.saldoporcentaje SET  saldo=$newsaldo,anualpago=$anualpagocomparado WHERE userid=$comparado";
						mysql_query($queryIns) or die($queryIns.mysql_error());
						if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga --> Userid: '.$comparado.' Fechaevaluar: '.$fechainsert.' Paydate: '.$row['paydate'].' Montocuota: '.$montocuota;

						//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
						//que dando la resta de la cuota con el saldo en cero, se le ingresa un pago de cero
						if($newsaldo>=0)
						{
							$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
								VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Saldo Favor')";					
							if($ECHOS==1)	echo '<br>EXONERO SALDO A FAVOR-->'.$query;
							mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
						}						
					}
					else//LE TOCA EXONERACION DE PAGO ESTE MES...
					{
						$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`)
								VALUES ($comparado,'$fechainsert',0,$idoper)";
						if($ECHOS==1)	echo '<br>EXONERO TRIMESTRAL-->'.$query;
						mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO

						$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
							VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Trimestral')";					
						if($ECHOS==1)	echo '<br>EXONERO TRIMESTRAL -->'.$query;
						mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO

						$queryIns="	UPDATE xima.cobro_porcentajes SET  anualpago=$anualpagocomparado WHERE userid=$comparado";
						mysql_query($queryIns) or die($queryIns.mysql_error());
					}
				}
				else if($payfrecuencycomparado==1)//pagos mensuales
				{
					//HACIENDO EL COBRO DE LA CUOTA DE ESTE USUARIO ESTE MES
					$query="INSERT INTO xima.cobro_estadocuenta (`userid`, `fechatrans`, `monto`, `idoper`) 
							VALUES ($comparado,'$fechainsert',$montocuota,$idoper)";
					if($ECHOS==1)	echo '<br>'.$query;
					mysql_query($query) or die($query.mysql_error());//INSERTANDO COBRO DE CUOTA AL HISTORICO
					
					//ACTUALIZANDO EL SALDO DEL CLIENTE, CON LA SUMA DEL SALDO ACTUAL MAS EL MONTO DE LA CUOTA(NEGATIVA)
					$newsaldo=$saldocomparado+$montocuota;
					$queryIns="	UPDATE xima.cobro_porcentajes SET  saldo=$newsaldo WHERE userid=$comparado";
					mysql_query($queryIns) or die($queryIns.mysql_error());
					if($ECHOS==1)	echo '<br>&nbsp;&nbsp;&nbsp; Cuota Paga mensual --> Userid: '.$comparado.' newsaldo: '.$newsaldo.' saldocomparado: '.$saldocomparado.' Paydate: '.$row['paydate'].' Montocuota: '.$montocuota;
					
					//caso cuando el usuario es activo y se le cobra la cuota y tiene el mismo monto a favor en el saldo,
					//que dando la resta de la cuota con el saldo en cero, se le ingresa un pago de cero
					if($newsaldo>=0)
					{
						$queryIns="INSERT INTO  xima.cobro_estadocuenta (fechatrans,idoper,userid,monto,`usercobrador`,`cobradordesc`)
							VALUES('$fechainsert',5,$comparado,0,$comparado,'Pago Exonerado Saldo Favor')";					
						if($ECHOS==1)	echo '<br>EXONERO SALDO A FAVOR-->'.$query;
						mysql_query($queryIns) or die($queryIns.mysql_error());//INSERTANDO PAGO DE CUOTA AL HISTORICO EXONERADO
					}
					
				}
				
				//arreglando un bug que inserta la cuota idoper=4 doble
				$query="SELECT idtrans FROM xima.cobro_estadocuenta f 
						WHERE `userid`=$comparado and `fechatrans`='$fechainsert' and `monto`=$montocuota and `idoper`=4";
				$res5=mysql_query($query) or die($query.mysql_error());
				$row5= mysql_fetch_array($res5, MYSQL_ASSOC);
				if(mysql_num_rows($res5)>1)
				{
					echo  '<br>&nbsp;&nbsp;&nbsp; CUOTA COBLE --> '.
					$query="DELETE FROM xima.cobro_estadocuenta WHERE idtrans=".$row5['idtrans'];
					mysql_query($query) or die($query.mysql_error());
					
				}
			}
*/		
		}//Saltar usuario	
		$i++;
		$newsaldo=$saldocomparado+$montocuota;
		echo '<br>saldocomparado: '.$saldocomparado.'|'.
		$queryIns="	UPDATE admin.saldoporcentaje SET  saldo=$newsaldo WHERE userid=$comparado";
		//mysql_query($queryIns) or die($queryIns.mysql_error());
		
		$comparadoanterior=$comparado;
		$productoanterior=$row['idproducto'];		
	}//fin while comparado

				
	

?>
