<?php 
/**
LINEAS DE CODIGO QUE CAMBIAN CON EL CAMBIO DE HORARIO EN USA

		linea 98 - 99
		ANTES DEL 09/03/2015 --> $sql="SELECT SQL_NO_CACHE  curdate() fecha,now() fulldate,hour(curtime()) hora,minute(curtime()) minuto";
		DESPUES DEL 09/03/2015 --> $sql="SELECT SQL_NO_CACHE  curdate() fecha,date_sub(now(),interval 60 minute) fulldate,
				hour(subtime(curtime(),'01:00:00')) hora,
				minute(subtime(curtime(),'01:00:00')) minuto";

		linea 158 -159
		ANTES DEL 09/03/2015 --> //if((c.lastdateserver>=date_sub(now(),interval 61 minute)),'ONLINE','OFFLINE')  status,
		DESPUES DEL 09/03/2015 --> $que="SELECT   c.`userid`,concat(x.name,' ',x.surname) nameuser,
									if((c.lastdateserver>=date_sub(now(),interval 61 minute)),'ONLINE','OFFLINE')  status,
				


*/
 //ESTE CODIGO SERA MODIFICADO UNICA Y EXCLUSIVAMENTE POR SMITH 
include($_SERVER["DOCUMENT_ROOT"]."/connection.php");
$Connect	=	new connect();
$Connect->connectInit();
$conex=$Connect->mysqlByServer(array('serverNumber' => 'Automate'));

//RECIBIENDO LAS VARIABLES DEL POST
$user=$_POST["userid"];
$username=$_POST["username"];
$ip=$_POST["ip"];
$timedateclient=$_POST["timedateclient"];
$icorte=$_POST["icorte"]+1; 
$active=$_POST['active'];
$botonconnect=$_POST['botonconnect'];

if($botonconnect==1)
{//quitando la desconexion forzada que se hizo
	$sql="	UPDATE `connectionhours` 
			SET `forceLogout`=0
			WHERE `userid`=$user ";
	$conex->query ($sql) or die ($sql.$conex->error);
}


$que="	SELECT  SQL_NO_CACHE forcelogout 	FROM connectionhours WHERE `USERID`=$user";
$result=$conex->query ($que) or die ($que.$conex->error);
$row=$result->fetch_object();
$forcelogout=$row->forcelogout;

$data = array();

//SI SE LE DIO CLICK AL BOTON CONNECT, Y ESTA CONECTADO, Y NO ESTA EL FORCELOGOUT ACTIVADO
if($_POST['active']==1 ){

	if($forcelogout==0){
	//AVERIGUANDO EL TIPO DE DISPOSITIVO DESDE EL CUAL SE ESTA CONECTADO AL CHISMOSO
		$tablet_browser = 0;
		$mobile_browser = 0;
		$body_class = 'desktop';
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$tablet_browser++;
			$body_class = "tablet";
		}
		 
		if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
		$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda ','xda-');
		 
		if (in_array($mobile_ua,$mobile_agents)) {
			$mobile_browser++;
		}
		 
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
			$mobile_browser++;
			$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
			if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
			  $tablet_browser++;
			}
		}

		// Si es tablet has lo que necesites
		if ($tablet_browser > 0){	   $dispositivo='2'; }
		// Si es dispositivo mobil has lo que necesites
		else if ($mobile_browser > 0) {	    $dispositivo='1';	}
		// Si es ordenador de escritorio has lo que necesites
		else {	    $dispositivo='3';	} 

		//$sql="SELECT SQL_NO_CACHE  curdate() fecha,now() fulldate,hour(curtime()) hora,minute(curtime()) minuto";
		$sql="SELECT SQL_NO_CACHE  curdate() fecha,date_sub(now(),interval 60 minute) fulldate,
				hour(subtime(curtime(),'01:00:00')) hora,
				minute(subtime(curtime(),'01:00:00')) minuto";
		$result=$conex->query($sql) or die($sql.$conex->error);
		$rowdatetime=$result->fetch_object();
		$date=$rowdatetime->fecha;
		$fulldate=$rowdatetime->fulldate;
		$time=$rowdatetime->hora.':'.$rowdatetime->minuto.':00';

		$queupdate="UPDATE connectionlogs  
					SET `check`=1,
						`timedateserver`='$fulldate', 
						`timedateclient`='$timedateclient',
						`timematriz`=date_add('$fulldate', interval 30 minute),
						`ipclient`='$ip' ,
						`device`='$dispositivo' 
					WHERE userid=$user AND evaldate='$date' AND evaltime='$time'";
		$conex->query ($queupdate) or die($queupdate.$conex->error);

		
		$queupdate2="UPDATE connectionhours  
					SET `lastdateserver`='$fulldate', 
						`lastdateclient`='$timedateclient',
						`lastip`='$ip' ,
						`lastdevice`='$dispositivo' 
					WHERE userid=$user ";
		$conex->query ($queupdate2) or die($queupdate2.$conex->error);

		echo json_encode(array(
			'success'	=>	true,
			'total'		=>	0,
			'results'	=>	'',
			'forcelogout'	=>	0,
			'queupdate'	=>	$queupdate
		));
	}
	else
		echo json_encode(array(
			'success'	=>	true,
			'total'		=>	0,
			'results'	=>	'',
			'forcelogout'	=>	1,
			'queupdate'	=>	$queupdate
		));
	
	return;
}
else
{
	// SI ES SMITH,GUILLE O FREDDY SE LE MUESTRA EL GRID DE LA IZQUIERDA
	{
				//if((c.lastdateserver>=date_sub(now(),interval 61 minute)),'ONLINE','OFFLINE')  status,

			$que="SELECT   c.`userid`,concat(x.name,' ',x.surname) nameuser,
				if((c.lastdateserver>=date_sub(now(),interval 61 minute)),'ONLINE','OFFLINE')  status,
				c.lastdateclient lastdate,
				c.lastdevice  device,
				c.lastip ipclient,
				(SELECT if(c2.daterecord is null,0,1) FROM `xima`.`connectionvacations` c2 WHERE c2.userid=x.userid and c2.daterecord=curdate() limit 1) vacation,
				(SELECT if(c1.`date` is null,0,1) FROM connectionnolaboraluser c2 INNER JOIN connectionnolaboral c1 ON c2.idnol=c1.idnol WHERE c1.`date`=curdate() AND c2.`userid`=x.userid limit 1) nolaboral,
				(
					SELECT h1.`type`
					FROM connectionnolaboraluser c2
					INNER JOIN connectionnolaboral c1 ON c2.idnol=c1.idnol
					LEFT JOIN connectionhistory h1 ON c2.idhis=h1.idhis
					WHERE c1.`date`=curdate() AND c2.`userid`=x.userid
					limit 1
				 ) tipo
			FROM xima.connectionhours c
			INNER JOIN xima.ximausrs x ON x.userid=c.userid
			ORDER BY nameuser";
		$result=$conex->query ($que) or die ($que.$conex->error);
		while ($row=$result->fetch_object())
		{
			if($row->vacation=='')$row->vacation=0;
			if($row->nolaboral=='')$row->nolaboral=0;
			$data[]=$row;
		}
	}
	echo json_encode(array(
		'success'	=>	true,
		'total'		=>	count($data),
		'results'	=>	$data,
		'forcelogout'	=>	0
		//,'que'	=>	str_replace("'","\'",$que)
	));
	return;
}
echo json_encode(array(
		'success'	=>	true,
		'total'		=>	0,
		'results'	=>	'',
		'queupdate'	=>	'',
		'forcelogout'	=>	0
));
return;
?>