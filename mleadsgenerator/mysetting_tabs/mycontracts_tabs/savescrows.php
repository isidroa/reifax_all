<?php
/**
 * saveaddons.php
 *
 * Save the addons of a document.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 07.04.2011
 */

include "../../properties_conexion.php"; 
conectar();

// Allowed images
$error   = "";
$mime    = array('image/gif' => 'gif', 'image/jpeg' => 'jpeg', 'image/png' => 'png');
$userid  = $_COOKIE['datos_usr']["USERID"];

// Delete addons and end execution if the command its recieve
if (strlen($_GET['scrow']) and $_GET['cmd'] == 'del') {
	
	$query = "DELETE FROM xima.contracts_scrow
				WHERE userid = $userid AND place = {$_GET['scrow']}";
	@mysql_query($query);	
	
	echo "Image deleted";
	die();
	
} else {

	// Check the form fields
	for ($i == 1 ; $i <=2 ; $i ++) {
	
		if ($_FILES["scrow{$i}"]['name']!='') {

			$fileInfo = getimagesize($_FILES["scrow{$i}"]['tmp_name']);
		
			 // No Image?
			if ( empty($fileInfo) )
				$error .= "The uploaded file doesnt seem to be an image. ";
			else {
			// Check Image
				$fileMime = $fileInfo['mime'];
				$extension = ($mime[$fileMime] == 'jpeg') ? 'jpg' : $mime[$fileMime];
				
				if(!$extension) {
					$extension = '';
					$error     = "No valid image file!";
				}
			}
			// No errors were found?
			if($error == "") {
				
				$newFileName = "$userid-"."$i.".$extension;
				
				@unlink(getcwd().'/scrows/'.$newFileName);
				
				if (!move_uploaded_file($_FILES["scrow{$i}"]['tmp_name'], getcwd().'/scrows/'.$newFileName))
					$error .= "Fallo al copiar. $newFileName";
				else {
	
					// Delete (if) the previous image and copy the new
					@unlink($_FILES["scrow{$i}"]['tmp_name']);
							
					// Delete the row of the previous image and insert a new one
					$query = "DELETE FROM xima.contracts_scrow
								WHERE userid = $userid AND place = $i";
					@mysql_query($query);	
				
					$query = "INSERT INTO xima.contracts_scrow (userid, imagen, place) 
							  VALUES ($userid,'$newFileName',$i)";
				
					if (mysql_query($query))
						$resp = array('success'=>'true','mensaje'=>'The image has been uploaded');
					else
						$resp = array('success'=>'true','mensaje'=>mysql_error());
					
				}
			}
		}
	}
	
	if ($error != "") 
		$resp = array('success'=>'true','mensaje'=>$error);
	
	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);

}

?>	