<?php
/**
 * mailsettings.php
 *
 * Just a simple form to get the STMP information from the users.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 11.07.2011
 */

include "../../properties_conexion.php";
conectar();

$userid = $_COOKIE['datos_usr']["USERID"];
$signs  = null;

$query  = "SELECT * FROM xima.contracts_mailsettings
				WHERE userid = $userid";
$rs     = mysql_query($query);
$row    = mysql_fetch_array($rs);



?>

<div id="div_imapsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_smtpsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">

var imapStore = new Ext.data.SimpleStore({
	fields : ['id','name'],
	data   : [
		['1','IMAP'],
		['2','POP3'],
		['3','IMAP/SSL']
	]
});

var smtpsettForm = new Ext.FormPanel({
	title      : 'Email Delivery Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'imap_type',
			id            : 'imap_type',
			hiddenName    : 'server_type',
			fieldLabel    : 'Type',
			store         : imapStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			listeners     : {
				'select'  : function() {
					if (Ext.getCmp('imap_type').getValue() == '1')
						Ext.getCmp('imap_port').setValue(143);
						
					if (Ext.getCmp('imap_type').getValue() == '2')
						Ext.getCmp('imap_port').setValue(110);
						
					if (Ext.getCmp('imap_type').getValue() == '3')
						Ext.getCmp('imap_port').setValue(993);
				}
			}
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;">If you want to use gmail please select IMAP/SSL</div> '
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Address',
			name       : 'imap_server',
			width      : '200',
			value      : '<?=addslashes($row['server'])?>',
			allowBlank : false
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with imap.gmail.com )</div>'
		},{
			xtype      : 'textfield',
			fieldLabel : 'Port Number',
			id         : 'imap_port',
			name       : 'imap_port',
			width      : '200',
			value      : '<?=strlen($row['port'])>0 ? $row['port'] : '143'?>',
			allowBlank : false
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'imap_username',
			width      : '200',
			value      : '<?=$row['username']?>',
			allowBlank : false
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'imap_password',
			value      : '<?=$row['password']?>',
			width      : '200',
			allowBlank : true
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Incomming Settings</b></span>',
		handler : function(){
			if (mailsettForm.getForm().isValid()) {
				mailsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mailsettings.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});

//smtpsettForm.render('div_smtpsett');


var imapsettForm = new Ext.FormPanel({
	title      : 'Email Delivery Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Address',
			name       : 'server',
			width      : '200',
			value      : '<?=addslashes($row['server'])?>'
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
		},{
			xtype      : 'textfield',
			fieldLabel : 'Port Number',
			name       : 'port',
			width      : '200',
			value      : '<?=strlen($row['port'])>0 ? $row['port'] : '25'?>'
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'username',
			width      : '200',
			value      : '<?=$row['username']?>'
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'password',
			value      : '<?=$row['password']?>',
			width      : '200'
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
		handler : function(){
			if (imapsettForm.getForm().isValid()) {
				imapsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mailsettings.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});

imapsettForm.render('div_imapsett');

</script>
