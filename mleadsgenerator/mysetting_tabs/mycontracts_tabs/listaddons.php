<?php
/**
 * listaddons.php
 *
 * Return the list of addons associated to an user ID and contract 
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 07.04.2011
 */

include_once "../../properties_conexion.php"; 
conectar();

$userid  = $_COOKIE['datos_usr']["USERID"];
$type    = strlen($_POST['type']) > 0  ? $_POST['type'] : $_GET['type'];
 
$query   = "SELECT id,addon_name as name,place,addon_act as active FROM xima.contracts_addonscustom 
				WHERE userid = $userid AND type=$type ORDER BY place ASC";
$rs      = mysql_query($query);

if ($rs) {

	while ($row = mysql_fetch_array($rs)) {
		
		$row['active']  = $row['active'] == 1 ? true : false;
		
		$data[]         = $row;
		
	}
	
	$resp = array('success'=>'true','data'=>$data);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	