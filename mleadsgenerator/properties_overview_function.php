<?php 
	include('properties_conexion.php');
	include ("properties_getgridcamptit.php");
	include("simple_html_dom.php");
		
	function camptit($campo,$tabla,$tipo='Titulos'){
		global $Camptit_array; 
		foreach($Camptit_array as $k => $val){
			if(strtolower($val[0])==strtolower($tabla) && strtolower($val[1])==strtolower($campo)){
				if($tipo=='Titulos')
					return $val[2];
				else if($tipo=='titulos_a')
					return $val[4];
				else
					return $val[3];
			}
		}
	}
	
	function format_fecha($fecha){
		if($fecha==NULL || empty($fecha) || strlen($fecha)!=8)
			return '';
		else
			return substr($fecha,4,2).'/'.substr($fecha,6,2).'/'.substr($fecha,0,4);
	}
	
	function url_exists($url)
	{
		$url_info = parse_url($url);
		
		if (! isset($url_info['host']))
			return false;
		
		$port = (isset($url_info['post'])?$url_info['port']:80);
		
		if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr)) 
			return false;
		
		$uri = @$url_info['path'] . '?' . @$url_info['query'];
		
		$http = "HEAD $uri HTTP/1.1\r\n";
		$http .= "Host: {$url_info['host']}\r\n";
		$http .= "Connection: close\r\n\r\n";
		
		@fwrite($hwnd, $http);
		
		$response = fgets($hwnd);
		$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";
		
		fclose($hwnd);
		
		if (preg_match($response_match, $response, $matches)) {
			//print_r($matches);
			if ($matches[1] == 404)
				return false;
			else if ($matches[1] == 200)
				return true;
			else
				return false;
			 
		} else {
			return false;
		}
	}
	
	function listing_realtorcom($mlnumber,$address){
		$address = explode(' ',$address);
		$address = trim($address[0]);
		
		$url="http://www.realtor.com/realestateandhomes-search?mlslid=$mlnumber";
		$html = file_get_html($url);
		$pos=0;
		unset($arrret);
		$urldetail='';
		foreach($html->find("a[class=primaryAction]") as $d)
		{
		$aux="http://www.realtor.com".trim($d->href);
		foreach($d->find("em") as $d1)
		{
		$cad=trim($d1->innertext);
		$c=explode(' ',$cad);
		if($c[0]==$address)
		{
		$urldetail=$aux;
		break;
		}
		}
		if($urldetail=='' && $aux<>'')
		$urldetail=$aux;
		
		if($urldetail<>'')
		break;
		}
		return $urldetail;
	}
	
	function listing_remarks($url){
		 $html = file_get_html($url);
		 $enc=0;
		 foreach($html->find("table") as $d)
		 {
		  foreach($d->find("td.TDProfileHeader") as $d1)
		  {
		   $rtext=trim($d1->innertext);
		   if($rtext=='Remarks:')
		   {
			$enc=1;
			//echo "<br/>".$rtext;
			break;
		   }
		  }
		  if($enc==1)
		  {
		   foreach($d->find(".TDProfileText") as $d2)
		   {
			$remarktext=trim($d2->innertext);
			//echo "<br/>".$remarktext;
			break;
		   }  
		   break;
		  }
		 }
		 $remarktext=trim(strip_tags($remarktext));
		 $remarktext=str_replace('"','',$remarktext);
		 $remarktext=str_replace("'",'',$remarktext);
		 $remarktext=str_replace('#','',$remarktext);
		 echo "<br/>".$remarktext;
	}
	
	function compare_calculate($x, $y)
	{
	   if ($x == $y) {
			return 0;
		}
		return ($x < $y) ? -1 : 1; 
	}

	///////////////////PUBLIC RECORDS///////////////////////////////////
	function overviewDetails($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$realtor,$loged,$block){
		conectarPorBD($db_data);
		
		$sql_comparado="Select 
		p.address,p.unit,p.ozip,p.city,p.sdname,p.folio,p.ccode,p.stories,
		p.lsqft,p.ac,p.yrbuilt,p.bheated,p.beds,p.pool,p.units,p.bath,p.waterf,p.buildingv,p.sfp,p.homstead,
		p.landv,p.taxablev,p.saledate,p.saleprice,p.owner,p.owner_a,p.owner_s,p.owner_c,p.owner_p,p.owner_z,
		p.phonename,p.phonenumber1,p.phonenumber2,p.rng,p.sec,p.twn,p.legal,p.ccoded,p.lunit_type,p.lunits,p.tsqft,
		m.closingdt,m.mlnumber
		FROM psummary p LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
		Where p.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
		?>
		<div align="center" id="psummary_data_div"  class="fondo_realtor_result_tab">
		<div align="center" style="width:100%; margin:auto; margin-top:0px;">
		<div id="<?php echo $map; ?>" style="width:48%;height:240px; border:1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div>
        <div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;"></div><br clear="all" />
        
        <h1 align="left" class="overtab_realtor_titulo">PUBLIC RECORDS</h1>
        <?php if($status_pro=='A'){?>
           <div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
                For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
            </div>
        <?php }?>
        <br clear="all" />
        
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit('address','psummary','desc'); ?>"><span style="font-weight:bold;"><?php echo camptit('address','psummary','titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['address']; ?></td>
                <td title="<?php echo camptit("unit","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("unit","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['unit']; ?></td>
            </tr>
            <tr align="left">
                <td  title="<?php echo camptit("city","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("city","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['city']; ?></td>
                <td title="<?php echo camptit("sdname","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("sdname","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['sdname']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("ozip","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("ozip","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['ozip']; ?></td>
                <td title="<?php echo camptit("folio","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("folio","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['folio']; ?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("ac","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("ac","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['ac']; ?></td>
                <td title="<?php echo camptit("stories","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("stories","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['stories']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("yrbuilt","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['yrbuilt']; ?></td>
                <td title="<?php echo camptit("tsqft","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("tsqft","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['tsqft']; ?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("beds","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("beds","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['beds']; ?></td>
                <td title="<?php echo camptit("pool","mlsresidential",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("pool","mlsresidential",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['pool']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("bath","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("bath","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['bath']; ?></td>
                <td title="<?php echo camptit("waterf","mlsresidential",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("waterf","mlsresidential",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['waterf']; ?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("lsqft","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("lsqft","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['lsqft']; ?></td>
                <td title="<?php echo camptit("bheated","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("bheated","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['bheated']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("units","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("units","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['units']; ?></td>
                <td title="<?php echo camptit("ccoded","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("ccoded","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['ccoded']; ?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("buildingv","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("buildingv","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo number_format($myrow['buildingv'],2,'.',','); ?></td>
                <td title="<?php echo camptit("sfp","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("sfp","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['sfp']; ?></td>
                
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("landv","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("landv","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo number_format($myrow['landv'],2,'.',','); ?></td>
                <td title="<?php echo camptit("taxablev","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("taxablev","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo number_format($myrow['taxablev'],2,'.',','); ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("legal","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("legal","psummary",'titulos_a'); ?>:</span></td>
                <td colspan=3><?php echo $myrow['legal']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("saledate","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("saledate","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo format_fecha($myrow['saledate']); ?></td>
                <td title="<?php echo camptit("saleprice","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("saleprice","psummary",'titulos_a'); ?>:</span></td>
                <td><?php echo $myrow['saleprice']==0 ? 'NA' : number_format($myrow['saleprice'],2,'.',','); ?></td> 
            </tr>
        </table>
        
        <?php
            ////////////////////////sales////////////////////////////////
                $sql_comparado="SELECT  
                parcelid,date,type,price,book,page,grantor
                FROM sales WHERE parcelid='$pid' 
                ORDER BY date DESC;";	
                $res = mysql_query($sql_comparado) or die(mysql_error());
                $i=1;
                $sales=mysql_num_rows($res);
                while($myrow2= mysql_fetch_array($res)){
                    if($i==1){
        ?>
        <br />
        <h1 align="left" class="overtab_realtor_titulo">HISTORY SALES</h1><br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
        <?php 		}?>
            <tr style="background-color:#b5cedd;" align="left">
                <td colspan=6 align="center"><span style="font-weight:bold;">SALES <?php echo $i;?></span></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("date","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("date","sales",'titulos_a');?>:</span></td>
                <td><?php echo format_fecha($myrow2['date']);?></td>
                <td title="<?php echo camptit("price","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("price","sales",'titulos_a');?>:</span></td>
                <td class="mortcellpadright"><?php echo number_format($myrow2['price'],2,'.',',');?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("book","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("book","sales",'titulos_a');?>:</span></td>
                <td><?php echo $myrow2['book'];?></td>
                <td title="<?php echo camptit("page","sales",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("page","sales",'titulos_a');?>:</span></td>
                <td><?php echo $myrow2['page'];?></td>
            </tr>
        <?php 		$i++;
				}	
		?>
        </table>
        <br />
        <?php if(!$realtor && $loged && !$block){?>
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">OWNER INFORMATION</h1><br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr align="left">
                <td title="<?php echo camptit("owner","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner","psummary",'titulos_a') ; ?>:</span></td>
                <td colspan="3"><?php echo $myrow['owner']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("owner_a","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_a","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['owner_a']; ?></td>
                <td title="<?php echo camptit("owner_c","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_c","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['owner_c']; ?></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("owner_z","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_z","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['owner_z']; ?></td>
                <td title="<?php echo camptit("owner_s","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_s","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['owner_s']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td title="<?php echo camptit("owner_p","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("owner_p","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php if(strtolower($myrow['owner_p'])!='null') echo $myrow['owner_p']; ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr align="left">
                <td title="<?php echo camptit("phonenumber1","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("phonenumber1","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['phonenumber1']; ?></td>
                <td title="<?php echo camptit("phonenumber2","psummary",'desc'); ?>"><span style="font-weight:bold;"><?php echo camptit("phonenumber2","psummary",'titulos_a') ; ?>:</span></td>
                <td><?php echo $myrow['phonenumber2']; ?></td>
            </tr>    
        </table>
        <?php } ?>
        </div>
        </div>
        
        <script>
			SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
			var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
			<?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
			<?php echo $map; ?>.map.HideDashboard();
			var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map; ?>.map.AddShape(pin); 
						
			var <?php echo $map2; ?> = new XimaMap('<?php echo $map2; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
			<?php echo $map2; ?>.map = new VEMap('<?php echo $map2; ?>');
			<?php echo $map2; ?>.map.LoadMap(SpaceNeedle, 15);
			<?php echo $map2; ?>.map.HideDashboard();
			var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map2; ?>.map.AddShape(pinS);
		</script>
<?php
	}
	
	////////////////////////Listing////////////////////////////////
	function overviewListings($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$slider1,$slider2,$slider3,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		
		$sql_comparado="Select 
		state,mlnumber,parcelid,agent,county,status,folio,agentemail,
		address,dom,muncode,agentid,stno,entrydate,
		sbdname,agentlic,dir,lprice,subdno,agentph,
		street,ldate,sec,agentph2,un,listtype,twn,agentph3,
		zip,orgprice,proptype,brokerpctg,city,saleprice,
		type,abuyerpctg,area,closingdt,constype,officename,
		devlpmnt,penddate,parceln,officefax,geoarea,waterf,
		directns1,officeemail,last4dig,waterfd,directns2,occupinfo,
		lsqft,withdate,hassocfee,view1,apxtotsqft,yrbuilt,intrmrks1,
		parkd,tsqft,yrbuiltd,intrmrks2,carportd,bath,petallow,style,carportq,
		beds,pool,frontexp,garaged,hbath,coold,heatd,garageq,remark1,remark2,remark3,
		remark4,remark5, proptype,agentfax,agenttollfree,officephone,officeotherphone1,
		officeotherphone2,officetollfree,
		urldetail
		FROM mlsresidential m 
		Where m.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
		?>
		<div align="center" id="listing_data_div" class="fondo_realtor_result_tab"> 
		<div align="center" style="width:100%; margin:auto; margin-top:0px; color: #CCC; text-align: justify;">   
		<div id="<?php echo $map; ?>" style="width:48%;height:240px; border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" align="center" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left; background:#FFF;">
        
        <?php 
		$cant_imagen_listing=0;
		if($status_pro=='A'){
			$_parcelid=$pid;
			$sql_fields="Select
			*  
			FROM imagenes 
			Where parcelid='$_parcelid';";
			
			$result = mysql_query($sql_fields) or die(mysql_error());
			$fila= mysql_fetch_array($result, MYSQL_ASSOC);
			$cant_imagen_listing=mysql_num_rows($result);
			if($cant_imagen_listing>0){
				$arr_pics="";
		
				if($fila["letra"]=='Y')
					$mlnumber=$fila["mlnumber"];
				else
					$mlnumber=substr($fila["mlnumber"],1);
				
				if($myrow['state']=='FL'){
					if(strlen($fila["urlxima"])>5){
						$arr_pics.=$fila["urlxima"].$fila["parcelid"].".".$fila['tipo'];
						if($fila['nphotos']>1){	
							for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
								if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
									$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
								elseif(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
									$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
							}
						}
					}else{
						if(url_exists($fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'])){
							$arr_pics.=$fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
									if(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
								}
							}
						}else{
							$arr_pics.="img/nophoto.gif";
						}	
					}
				}elseif($myrow['state']=='GA'){
					if(strlen($fila["urlxima"])>5){
						$arr_pics.=$fila["urlxima"].$fila["parcelid"].$fila["sep"].$fila["inicount"].".".$fila['tipo'];
						if($fila['nphotos']>1){	
							for($i=$fila['inicount']+1; $i<$fila['nphotos']; $i++){
								if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
									$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
								elseif(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
									$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
							}
						}
					}else{
						if(url_exists($fila['url'].$fila['url2'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$fila['inicount'].".".$fila['tipo'])){
							$arr_pics.=$fila['url'].$fila['url2'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$fila['inicount'].".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']+1; $i<$fila['nphotos']; $i++){
									if(url_exists($fila["url"].$fila['url3'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$fila['url3'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$i.".".$fila['tipo'];
								}
							}
						}else{
							$arr_pics.="img/nophoto.gif";
						}	
					}
				}elseif($myrow['state']=='TX'){
					if(strlen($fila["urlxima"])>5){
						$arr_pics.=$fila["urlxima"].$fila["parcelid"].".".$fila['tipo'];
						if($fila['nphotos']>1){	
							for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
								if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
									$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
								elseif(url_exists($fila["url"].$i."/".substr($mlnumber,-4,4)."/".$mlnumber.".".$fila['tipo']))
									$arr_pics.=",".$fila["url"].$i."/".substr($mlnumber,-4,4)."/".$mlnumber.".".$fila['tipo'];
							}
						}
					}else{
						if(url_exists($fila['url'].'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'])){
							$arr_pics.=$fila['url'].'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
									if(url_exists($fila["url"].$i.'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$i.'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'];
								}
							}
						}else{
							$arr_pics.="img/nophoto.gif";
						}	
					}
				}
			}
			mysql_free_result($result);
		
			$arr_data=explode(",",$arr_pics);
		?>
		<input type="hidden" id="<?php echo $slider1;?>" value="<?php echo $arr_pics;?>" />
		<img id="<?php echo $slider2;?>" src="<?php echo $arr_data[0]; ?>" height="220"/>
	   
		<div id="<?php echo $slider3;?>" style="background:#FFF;"></div>
		<?php }?>
        
        </div><br clear="all" />
        <h1 align="left"  class="overtab_realtor_titulo">Listing</h1>
        <?php if($status_pro=='A'){?>
            <div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
                For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
            </div>
        <?php }?>
        <br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("address","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("address","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['address']; ?></td>
                <td valign=top title="<?php echo camptit("mlnumber","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mlnumber","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['mlnumber']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("state","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("state","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['state']; ?></td>
                <td valign=top title="<?php echo camptit("parcelid","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['parcelid']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("county","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("county","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['county']; ?></td>
                <td valign=top title="<?php echo camptit("status","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("status","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['status']; ?></td>
            </tr>
            <tr align="left">
            	<td valign=top title="<?php echo camptit("city","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("city","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['city']; ?></td>
                <td valign=top title=""><span style="font-weight:bold;">&nbsp;</span></td>
                <td valign=top>&nbsp;</td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
            	<td valign=top title="<?php echo camptit("zip","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("zip","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['zip']; ?></td>
                <td valign=top title="<?php echo camptit("lprice","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("lprice","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo number_format($myrow['lprice'],2,'.',','); ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("subdno","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("subdno","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['subdno']; ?></td>
                <td valign=top title="<?php echo camptit("entrydate","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("entrydate","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo format_fecha($myrow['entrydate']); ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("constype","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("constype","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['constype']; ?></td>
                <td valign=top title="<?php echo camptit("ldate","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("ldate","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo format_fecha($myrow['ldate']); ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("apxtotsqft","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("apxtotsqft","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['apxtotsqft']; ?></td>
                <td valign=top title="<?php echo camptit("dom","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("dom","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['dom']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("beds","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("beds","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['beds']; ?></td>
                <td valign=top title="<?php echo camptit("waterf","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("waterf","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['waterf']; ?></td> 
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("bath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("bath","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['bath']; ?></td>
                <td valign=top title="<?php echo camptit("yrbuilt","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['yrbuilt']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("hbath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("hbath","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['hbath']; ?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr align="left">
            	<td valign=top><span style="font-weight:bold;">REMARK:</span></td valign=top>
                <td colspan=3>
				<?php 
				if(strlen(str_replace(' ','',($myrow['remark1']." ".$myrow['remark2']." ".$myrow['remark3']." ".$myrow['remark4']." ".$myrow['remark5'])))>0)
					echo $myrow['remark1']." ".$myrow['remark2']." ".$myrow['remark3']." ".$myrow['remark4']." ".$myrow['remark5'];
				else
					listing_remarks($myrow['urldetail']);
				?></td>
            </tr>
        </table>
        <br clear="all" />
        <h1 align="left"  class="overtab_realtor_titulo">AGENT / OFFICE</h1>
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
           <tr>
           <td valign=top><span style="font-weight:bold;">Courtesy Of:</span></td>
           <td valign=top><?php echo $myrow['officename']; ?></td>
           </tr>  
           <?php if(!$realtor && $loged && !$block){?>
                 <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("agent","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agent","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['agent']; ?></td>
               
                <td valign=top title="<?php echo camptit("agentemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentemail","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentemail']; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top title="<?php echo camptit("agentph","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph']; ?></td>
                
                <td valign=top title="<?php echo camptit("agentph2","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph2","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph2']; ?></td>
                </tr>
                
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("agentph3","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph3","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph3']; ?></td>
                <td valign=top><span style="font-weight:bold;">Agent Fax:</span></td>
                <td valign=top><?php echo $myrow['agentfax']; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top ><span style="font-weight:bold;">Agent Tollfree:</span></td>
                <td valign=top><?php echo $myrow['agenttollfree']; ?></td>
                <td valign=top></td>
                <td valign=top></td>
                </tr>
                
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("officename","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officename","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['officename']; ?></td>
                
                <td valign=top title="<?php echo camptit("officefax","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officefax","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['officefax']; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top title="<?php echo camptit("officephone","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officephone","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top><?php echo $myrow['officephone']; ?></td>
                <td valign=top><span style="font-weight:bold;">OfficePhone_2:</span></td>
                <td valign=top><?php echo $myrow['officeotherphone1']; ?></td>
                </tr>
                
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top><span style="font-weight:bold;">OfficePhone_3:</span></td>
                <td valign=top><?php echo $myrow['officeotherphone2']; ?></td>
                <td valign=top><span style="font-weight:bold;">Office Tollfree:</span></td>
                <td valign=top><?php echo $myrow['officetollfree']; ?></td>
                </tr>
                
                 <tr align="left">
                <td valign=top title="<?php echo camptit("officeemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officeemail","mlsresidential",'titulos_a');?>:</span></td>
                <td valign=top><?php echo $myrow['officeemail']; ?></td>
                <td></td>
                <td></td>
            </tr>
                 <?php }?> 
                 
            <?php 
            if($realtor){ 
            $sql8 = "SELECT * FROM xima.ximausrs where userid =".$realtorid; 
            $num2= mysql_query($sql8);   
            $res= mysql_fetch_array($num2);
			$email_pri= $res['profemail']; 
            $email_sec= $res['EMAIL']; 
			
			if (empty($email_pri)) { $email=$email_sec;}else{$email=$email_pri;}
			
			//$email="alexjp18@gmail.com";
            
            }
            
            if($loged || !$loged){$email= $myrow['agentemail']; }
            
                
            if($realtor || $loged || !$loged ){ 
                 $address_email= 'Parcelid: '.$myrow['folio'].' \n<br> Address: '.$myrow['address'].' \n<br> Zip code:'.$myrow['zip'].' \n<br> City:'.$myrow['city'].'\n<br> County:'.$myrow['county'].'\n<br> State:'.$myrow['state'].'\n<br>';
			       ?> 
                      
                <tr align="left"> 
                <td valign=top title="<?php echo camptit("agentemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentemail","mlsresidential",'titulos_a') ;?>:</span></td>
                <td valign=top>
                  <input type="submit" name="contacto" id="contacto"  style="background:#FFF; color:#069; font-size:12px; font-weight:bold;" value="Inquire about this property" onclick="contacto('<?php echo $email; ?>','<?php echo $address_email; ?>');"/>
                 </td>
                </tr>
                <?php }?>
           
         
         
        </table>
        <br />
        <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
          <tr>
            <td valign="top"><span style="font-size: 10px; font-family: Arial, Helvetica, sans-serif; color: #999; text-align: justify;">The data relating to real estate for sale/lease on this web site come in part from a cooperative data exchange program of the multiple listing service (MLS) in which this real estate firm (Broker) participates. The properties displayed may not be all of the properties in the MLS's database, or all of the properties listed with Brokers participating in the cooperative data exchange program. Properties listed by Brokers other than this Broker are marked with either the listing Broker's logo or name or the MLS name or a logo provided by the MLS. Detailed information about such properties includes the name of the listing Brokers. Information provided is thought to be reliable but is not guaranteed to be accurate; you are advised to verify facts that are important to you. No warranties, expressed or implied, are provided for the data herein, or for their use or interpretation by the user. The Florida Association of REALTORS® and its cooperating MLSs do not create, control or review the property data displayed herein and take no responsibility for the content of such records. Federal law prohibits discrimination on the basis of race, color, religion, sex, handicap, familial status or national origin in the sale, rental or financing of housing.</span></td>
          </tr>
        </table>
        </div>
        </div>
        <script>
            var tiene_ima=<?php echo $cant_imagen_listing;?>;
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
            <?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid,true);
            <?php echo $map; ?>.map.HideDashboard();
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.map.AddShape(pin);
                        
            if(tiene_ima==0){
                var <?php echo $map2; ?> = new XimaMap('<?php echo $map2; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
                <?php echo $map2; ?>.map = new VEMap('<?php echo $map2; ?>');
                <?php echo $map2; ?>.map.LoadMap(SpaceNeedle, 15);
                <?php echo $map2; ?>.map.HideDashboard();
                var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
                pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                    "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
                <?php echo $map2; ?>.map.AddShape(pinS);
            }else{
                
                var data=document.getElementById('<?php echo $slider1;?>').value;
                img_src=data.split(',');
                
                new Ext.Slider({
                    id: 'slidercomp_img',					
                    renderTo: '<?php echo $slider3;?>',
                    width: 200,
                    value:0,
                    increment: 1,
                    minValue: 0,
                    maxValue: (img_src.length-1),
                    listeners: {
                        change: function (slider,val){
                            document.getElementById('<?php echo $slider2;?>').src=img_src[val];
                        }
                    },
					plugins: new Ext.slider.Tip({
						getText: function(thumb){
							return String.format('<b>{0} of {1} pictures.</b>', (thumb.value+1), img_src.length);
						}
					})
                });
            }
        </script>
<?php
	}
	
	////////////////////////Listing IFRAME//////////////////////////
	function overviewListingsIFrame($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$slider1,$slider2,$slider3,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		$query="SELECT urldetail,mlnumber,address FROM mlsresidential WHERE parcelid='$pid'";
		$result = mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		
		$realtorcom = listing_realtorcom($r['mlnumber'],$r['address']);
		?>
        <div style="width:100%; margin:auto; margin-top:10px;">
            <div style="color:#FF0000; font-size:13px; text-align:center;">The following websites are offered as information referral sites. This information is not maintained, collected, stored or used by REIFAX.com, nor is any of the content shown on these websites the responsibility of REIFAX.com</div>
            <a href="<?php echo $r['urldetail'];?>" target="_blank" class="overviewBotonCss3">IDX</a>
            <?php if(strlen($realtorcom)>5){?>
            	<a href="<?php echo $realtorcom;?>" target="_blank" class="overviewBotonCss3">REALTOR.COM</a>
            <?php }?>
       	</div>
	<?php }
	
	
	////////////////////////Mortgage////////////////////////////////
	function overviewMortgage($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude){
		conectarPorBD($db_data);
		
		$sql_comparado="Select 
		parcelid,doctype,countyid,saleppsf,docdesc,usecode,
		saleprice,recdate,usedesc,grantor,buyer2,grantor_st,buyeradd2,
		buyer1,buyeradd3,buyer1_st,buyeradd4,buyeradd1,buyercity,
		buyerstate,buyercount,buyerzip,mtg_bor1,mtg_doc_ty,
		mtg_bor2,mtg_doc_dc,mtg_orbk,mtg_rattyp,mtg_orpg,mtg_amount,
		mtg_recdat,mtg_term,mtg_insdat,mtg_intrst,mtg_len_ad,
		titleco,mtg_lender
		FROM mortgage 
		Where parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$i=1;
		
		$mortgage=mysql_num_rows($res);
		
		while($myrow= mysql_fetch_array($res)){
			if($i==1){
		?>
            <div align="center" id="mortgage_data_div">
            <div align="center" style="width:100%; margin:auto; margin-top:10px;">
            <div id="<?php echo $map; ?>" style="width:48%;height:240px; border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;"></div><br clear="all" />
            <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px; float:left;">MORTGAGES</h1>
            <?php if($status_pro=='A'){?>
                <div align="rigth" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;color:#139713;font-size:20px; float:right; margin-right:40px; font-weight:bold;">
                    For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
                </div>
            <?php }?>
            <br clear="all" />
            <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <?php 		}?>
                <tr style="background-color:#b5cedd;" align="left" >
                    <td colspan=4 align=center>MORTGAGE <?php echo $i;?></td>
                </tr>
                <tr align="left">
                    <td title="<?php echo  camptit("parcelid","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","mortgage");?>:</span></td>
                    <td><?php echo $myrow['parcelid'];?></font></td>
                    <td title="<?php echo  camptit("docdesc","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("docdesc","mortgage");?>:</span></td>
                    <td><?php echo $myrow['docdesc'];?></font></td>
                </tr>
                <tr style="background-color:#b5cedd;" align="left">
                    <td title="<?php echo  camptit("mtg_bor1","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_bor1","mortgage");?>:</span></td>
                    <td><?php echo $myrow['mtg_bor1'];?></font></td>
                    <td title="<?php echo  camptit("mtg_lender","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_lender","mortgage");?>:</span></td>
                    <td><?php echo $myrow['mtg_lender'];?></font></td>
                </tr>
                <tr align="left">
                    <td title="<?php echo  camptit("mtg_orbk","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_orbk","mortgage");?>:</span></td>
                    <td><?php echo $myrow['mtg_orbk'];?></font></td>
                    <td title="<?php echo  camptit("mtg_orpg","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_orpg","mortgage");?>:</span></td>
                    <td><?php echo $myrow['mtg_orpg'];?></font></td>
                </tr>
                <tr style="background-color:#b5cedd;" align="left">
                    <td title="<?php echo  camptit("mtg_recdat","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_recdat","mortgage");?>:</span></td>
                    <td><?php echo format_fecha($myrow['mtg_recdat']);?></font></td>
                    <td title="<?php echo  camptit("mtg_amount","mortgage",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg_amount","mortgage");?>:</span></td>
                    <td><?php echo number_format($myrow['mtg_amount'],2,'.',',');?></font></td>
                </tr>
            <?php if($i==$mortgage){?>
            </table>
            </div>
            </div>
		<?php
					}
					$i++;
				}
		?>
		
		<script>
			SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
			var <?php echo $map; ?> = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
			<?php echo $map; ?>.HideDashboard();
			var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map; ?>.AddShape(pin);
						
			var <?php echo $map2; ?> = new VEMap('<?php echo $map2; ?>');
			<?php echo $map2; ?>.LoadMap(SpaceNeedle, 15);
			<?php echo $map2; ?>.HideDashboard();
			var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map2; ?>.AddShape(pinS);
		
		</script>
<?php
	}
	
	
	////////////////////////FORECLOSURE INFORMATION////////////////////////////////
	function overviewForeclosures($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude){
		conectarPorBD($db_data);
		
		$sql_comparado="Select 
		p.parcelid,p.prop1samt,p.plaintiff1,p.case_numbe,p.prop1sdate,p.mtg1amt,
		p.foreclosur,p.assessedva,p.mtg1bal,p.file_date,p.loanvalrat,p.mtg1book,
		p.defowner1,p.marketvalu,p.mtg1date,p.defowner2,p.judgeamt,p.mtg1intrat,
		p.defsumaddr,p.judgedate,p.mtg1lastpa,p.defsumname,p.judgeeq,p.mtg1page,
		p.totaliens,p.judgesaler,p.mtg1paymen,p.totalpendes,p.attorney,p.mtg1positi,
		m.debttv,p.attorneyp,p.mtg1ratety,p.mtg1type,
		p.lien1amt,p.lien2amt,p.lien3amt,p.lien7amt,
		p.lien1book,p.lien2book,p.lien3book,p.lien7book,
		p.lien1holde,p.lien2holde,p.lien3holde,p.lien7holde,
		p.lien1page,p.lien2page,p.lien3page,p.lien7page,
		p.lien1type,p.lien2type,p.lien3type,p.lien7type,
		p.lien4amt,p.lien5amt,p.lien6amt,
		p.lien4book,p.lien5book,p.lien6book,
		p.lien4holde,p.lien5holde,p.lien6holde,
		p.lien4page,p.lien5page,p.lien6page,
		p.lien4type,p.lien5type,p.lien6type,
		p.entrydate, m.pendes, m.sold
		FROM pendes p LEFT JOIN marketvalue m ON (p.parcelid=m.parcelid)
		Where p.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
		?>
        <div align="center" id="pendes_data_div">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
        <div id="<?php echo $map; ?>" style="width:48%;height:240px; border:1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;"></div><br clear="all" />
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px; float:left;">FORECLOSURE INFORMATION</h1>
        <?php if($status_pro=='A'){?>
            <div align="rigth" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;color:#139713;font-size:20px; margin-right:40px; float:right; font-weight:bold;">
                For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
            </div>
        <?php }?>
        <br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("parcelid","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","pendes");?>:</span></td>
                <td><?php echo $myrow['parcelid'];?></font></td>
                <td valign=top title="<?php echo camptit("case_numbe","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("case_numbe","pendes");?>:</span></td>
                <td><?php echo $myrow['case_numbe'];?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("totalpendes","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("totalpendes","pendes");?>:</span></td>
                <td><?php echo number_format($myrow['totalpendes'],2,'.',',');?></font></td>
                <td valign=top title="<?php echo camptit("debttv","marketvalue",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("debttv","marketvalue");?>:</span></td>
                <td><?php echo $myrow['debttv'];?></font></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("defowner1","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("defowner1","pendes");?>:</span></td>
                <td><?php echo $myrow['defowner1'];?></font></td>
                <td valign=top title="<?php echo camptit("defowner2","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("defowner2","pendes");?>:</span></td>
                <td><?php echo $myrow['defowner2'];?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("entrydate","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("entrydate","pendes");?>:</span></td>
                <td><?php echo format_fecha($myrow['entrydate']);?></font></td>
                <td valign=top title="<?php echo camptit("plaintiff1","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("plaintiff1","pendes");?>:</span></td>
                <td><?php echo $myrow['plaintiff1'];?></font></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("mtg1amt","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1amt","pendes");?>:</span></td>
                <td><?php echo number_format($myrow['mtg1amt'],2,'.',',');?></font></td>
                <td valign=top title="<?php echo camptit("mtg1bal","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1bal","pendes");?>:</span></td>
                <td><?php echo number_format($myrow['mtg1bal'],2,'.',',');?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("mtg1book","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1book","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1book'];?></font></td>
                <td valign=top title="<?php echo camptit("mtg1page","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1page","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1page'];?></font></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("mtg1date","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1date","pendes");?>:</span></td>
                <td><?php echo format_fecha($myrow['mtg1date']);?></font></td>
                <td valign=top title="<?php echo camptit("mtg1paymen","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1paymen","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1paymen'];?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("judgeamt","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("judgeamt","pendes");?>:</span></td>
                <td><?php echo number_format($myrow['judgeamt'],2,'.',',');?></font></td>
                <td valign=top title="<?php echo camptit("mtg1intrat","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1intrat","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1intrat'];?></font></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("judgedate","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("judgedate","pendes");?>:</span></td>
                <td><?php echo format_fecha($myrow['judgedate']);?></font></td>
                <td valign=top title="<?php echo camptit("mtg1lastpa","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1lastpa","pendes");?>:</span></td>
                <td><?php echo format_fecha($myrow['mtg1lastpa']);?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("attorney","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("attorney","pendes");?>:</span></td>
                <td><?php echo $myrow['attorney'];?></font></td>
                <td valign=top title="<?php echo camptit("mtg1positi","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1positi","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1positi'];?></font></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("attorneyp","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("attorneyp","pendes");?>:</span></td>
                <td><?php echo substr($myrow['attorneyp'],0,strlen($myrow['attorneyp'])-3);?></font></td>
                <td valign=top title="<?php echo camptit("mtg1ratety","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1ratety","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1ratety'];?></font></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("mtg1type","pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mtg1type","pendes");?>:</span></td>
                <td><?php echo $myrow['mtg1type'];?></font></td>
                <td valign=top title="Foreclosed Status"><span style="font-weight:bold;">Status:</span></td>
                <td><?php echo ($myrow['pendes']=='P' ? 'Pre-Foreclosed' : 'Foreclosed').($myrow['sold']=='S' ? ' Sold' : '');?></td>
            </tr>
        </table>
        <?php
            ////////////////////////Liens////////////////////////////////
                $sql_comparado="Select 
                lien1amt,lien2amt,lien3amt,lien7amt,
                lien1book,lien2book,lien3book,lien7book,
                lien1holde,lien2holde,lien3holde,lien7holde,
                lien1page,lien2page,lien3page,lien7page,
                lien4amt,lien5amt,lien6amt,
                lien4book,lien5book,lien6book,
                lien4holde,lien5holde,lien6holde,
                lien4page,lien5page,lien6page
                FROM pendes 
                Where parcelid='$pid' and totaliens >0";	
                $res = mysql_query($sql_comparado) or die(mysql_error());
                $myrow= mysql_fetch_array($res);
                $liens=mysql_num_rows($res);
                if($liens>0){
        ?>
        <br />
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">LIENS INFORMATION</h1>
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <?php 
            $i=1;
            while($i<=7){
                $amt='lien'.$i.'amt';
                $holde='lien'.$i.'holde';
                $book='lien'.$i.'book';
                $page='lien'.$i.'page';
                
                if($myrow[$amt]!='0.00' || $myrow[$holde]!='' || $myrow[$book]!='' || $myrow[$page]!=''){?>
                <tr style="background-color:#b5cedd;">
                    <td colspan="4" align="center"><span style="font-weight:bold;">LIEN <?php echo $i;?></span></td>
                </tr>
                <tr align="left">
                    <td valign=top title="<?php camptit($amt,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($amt,"pendes");?>:</span></td>
                    <td><?php echo number_format($myrow[$amt],2,'.',',');?></font></td>
                    <td valign=top title="<?php camptit($holde,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($holde,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$holde];?></font></td>
                </tr>
                <tr align="left">
                    <td valign=top title="<?php camptit($book,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($book,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$book];?></font></td>
                    <td valign=top title="<?php camptit($page,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($page,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$page];?></font></td>
                </tr>
            <?php }
                $i++;
            }?>
            <tr style="background-color:#b5cedd;">
                <td colspan="4">&nbsp;</td>
            </tr>
        </table>
        <?php
            }
        ?>
        
        </div>
        </div>
        <script>
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
            <?php echo $map; ?>.HideDashboard();
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.AddShape(pin);
                        
            var <?php echo $map2; ?> = new VEMap('<?php echo $map2; ?>');
            <?php echo $map2; ?>.LoadMap(SpaceNeedle, 15);
            <?php echo $map2; ?>.HideDashboard();
            var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map2; ?>.AddShape(pinS);
        
        </script>
<?php
	}
	
	////////////////////////Rental////////////////////////////////
	function overviewRental($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		
		$sql_comparado="Select
		m.state, m.mlnumber, m.parcelid, m.agent, m.county, m.status, m.folio, m.agentemail,
		m.address, m.dom, m.muncode, m.stno, m.entrydate,
		m.agentlic, m.dir, m.lprice, m.subdno, m.agentph,
		m.street, m.ldate, m.sec, m.agentph2, m.listtype, m.twn,
		m.zip, m.orgprice, m.proptype, m.brokerpctg, m.city,
		m.type, m.area, m.closingdt, m.constype, m.officename,
		m.devlpmnt, m.parceln, m.officefax, m.geoarea, m.waterf,
		m.directns1, m.waterfd, m.occupinfo,
		m.lsqft, m.apxtotsqft, m.yrbuilt,
		m.bath, m.style,
		m.beds, m.pool, m.hbath, m.coold, m.remark1,
		p.ccoded
		FROM rental m
		INNER JOIN psummary p ON (m.parcelid=p.parcelid)
		Where m.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
?>
		<div align="center" id="rental_data_div" class="fondo_realtor_result_tab">
		<div align="center" style="width:100%; margin:auto; margin-top:0px;">
		<div id="<?php echo $map; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;">
		</div><br clear="all" />
		<h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px; float:left;"><?php echo $myrow['ccoded'].' Rental'; ?></h1>
		<?php if($status_pro=='A'){?>
			<div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
				For Rent: $<?php echo number_format($lprice_pro,0,'.',',');?>
			</div>
		<?php }?>
		<br clear="all" />
		<table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("state","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("state","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['state']; ?></td>
				<td valign=top title="<?php echo camptit("mlnumber","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mlnumber","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['mlnumber']; ?></td>
			</tr>
			<tr align="left">
				<td valign=top title="<?php echo camptit("county","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("county","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['county']; ?></td>
				<td valign=top title="<?php echo camptit("parcelid","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['parcelid']; ?></td>
			</tr>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("address","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("address","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['address']; ?></td>
				<td valign=top title="<?php echo camptit("subdno","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("subdno","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['subdno']; ?></td>
			</tr>
			<tr align="left">
				<td valign=top title="<?php echo camptit("dom","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("dom","rental");?></span></td>
				<td valign=top><?php echo $myrow['dom']; ?></td>
				<td valign=top title="<?php echo camptit("status","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("status","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['status']; ?></td>
			</tr>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("constype","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("constype","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['constype']; ?></td>
				<td valign=top title="<?php echo camptit("proptype","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("proptype","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['ccoded']; ?></td>
			</tr>
			<tr align="left">
				<td valign=top title="<?php echo camptit("zip","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("zip","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['zip']; ?></td>
				<td valign=top title="<?php echo camptit("entrydate","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("entrydate","rental");?>:</span></td>
				<td valign=top><?php echo format_fecha($myrow['entrydate']); ?></td>
			</tr>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("city","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("city","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['city']; ?></td>
				<td valign=top title="<?php echo camptit("lprice","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("lprice","rental");?>:</span></td>
				<td valign=top><?php echo number_format($myrow['lprice'],2,'.',','); ?></td>
				
			</tr>
			<tr align="left">
				<td valign=top title="<?php echo camptit("apxtotsqft","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("apxtotsqft","mlsresidential");?>:</span></td>
				<td valign=top><?php echo $myrow['apxtotsqft']; ?></td>
				<td valign=top title="<?php echo camptit("waterf","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("waterf","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['waterf']; ?></td>
			</tr>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("beds","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("beds","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['beds']; ?></td>
				<td valign=top title="<?php echo camptit("yrbuilt","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['yrbuilt']; ?></td> 
			</tr>
			<tr align="left">
				<td valign=top title="<?php echo camptit("bath","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("bath","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['bath']; ?></td>
				<td valign=top title="<?php echo camptit("hbath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("hbath","mlsresidential");?>:</span></td>
				<td valign=top><?php echo $myrow['hbath']; ?></td>
			</tr>
			<tr style="background-color:#b5cedd;" align="left">
				<td valign=top><span style="font-weight:bold;">REMARK:</span></td valign=top>
				<td colspan=3><?php echo $myrow['remark1'];?></td>
			</tr>
		</table>
		<br clear="all" />
		<h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">AGENT / OFFICE</h1>
		<table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
		 <tr>
		   <td valign=top><span style="font-weight:bold;">Courtesy Of:</span></td>
		   <td valign=top><?php echo $myrow['officename']; ?></td>
		   </tr>  
			
				<?php if($loged && !$block){?>
				<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("agent","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agent","mlsresidential") ;?>:</span></td>
				<td valign=top><?php echo $myrow['agent']; ?></td>
				<td valign=top title="<?php echo camptit("agentemail","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentemail","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['agentemail']; ?></td>
				</tr>
		  
				  <tr align="left">
				<td valign=top title="<?php echo camptit("agentph","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['agentph']; ?></td>
				<td valign=top title="<?php echo camptit("agentph2","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph2","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['agentph2']; ?></td>
				</tr>
			
				<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("agentph3","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph3","rental") ;?>:</span></td>
				<td valign=top><?php echo $myrow['agentph3']; ?></td>
				<td></td>
				<td></td>
				</tr>
		   
				 <tr align="left">
				<td valign=top title="<?php echo camptit("officename","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officename","mlsresidential");?>:</span></td>
				<td valign=top><?php echo $myrow['officename']; ?></td>
				<td valign=top title="<?php echo camptit("officefax","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officefax","rental");?>:</span></td>
				<td valign=top><?php echo $myrow['officefax']; ?></td>
				</tr>
		   
				<tr style="background-color:#b5cedd;" align="left">
				<td valign=top title="<?php echo camptit("officeemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officeemail","mlsresidential");?>:</span></td>
				<td valign=top><?php echo $myrow['officeemail']; ?></td>
				<td></td>
				<td></td>
				</tr>
			<?php }?>
			
			 <?php 
			if($realtor){ 
			$sql8 = "SELECT * FROM xima.ximausrs where userid =".$realtorid; 
			$num2= mysql_query($sql8);   
			$res= mysql_fetch_array($num2); 
			$email= $res['EMAIL'];
			
			}
			
			if($loged || !$loged){ $email= $myrow['agentemail']; }
			 
			 
			 if($realtor || $loged || !$loged){ 
			  $address_email= 'Parcelid: '.$myrow['folio'].' \n<br> Address: '.$myrow['address'].' \n<br> Zip code:'.$myrow['zip'].' \n<br> City:'.$myrow['city'].'\n<br> County:'.$myrow['county'].'\n<br> State:'.$myrow['state'].'\n<br>';?> 
				<tr align="left"> 
				<td valign=top title="<?php echo camptit("agentemail","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentemail","rental") ;?>:</span></td>
				<td valign=top>
				  <input type="submit" name="contacto" id="contacto"  style="background:#FFF; color:#069; font-size:12px; font-weight:bold;" value="Inquire about this property" onclick="contacto('<?php echo $email; ?>','<?php echo $address_email; ?>');"/>
				  
			 </td>
				</tr>
				<?php }?>
		   
		</table>
		
		<br />
		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
		  <tr>
			<td valign="top"><span style="font-size: 10px; font-family: Arial, Helvetica, sans-serif; color: #999; text-align: justify;">The data relating to real estate for sale/lease on this web site come in part from a cooperative data exchange program of the multiple listing service (MLS) in which this real estate firm (Broker) participates. The properties displayed may not be all of the properties in the MLS's database, or all of the properties listed with Brokers participating in the cooperative data exchange program. Properties listed by Brokers other than this Broker are marked with either the listing Broker's logo or name or the MLS name or a logo provided by the MLS. Detailed information about such properties includes the name of the listing Brokers. Information provided is thought to be reliable but is not guaranteed to be accurate; you are advised to verify facts that are important to you. No warranties, expressed or implied, are provided for the data herein, or for their use or interpretation by the user. The Florida Association of REALTORS® and its cooperating MLSs do not create, control or review the property data displayed herein and take no responsibility for the content of such records. Federal law prohibits discrimination on the basis of race, color, religion, sex, handicap, familial status or national origin in the sale, rental or financing of housing.</span></td>
		  </tr>
		</table>
		</div>
		</div>
		<script>
			SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
			var <?php echo $map; ?> = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
			<?php echo $map; ?>.HideDashboard();
			var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map; ?>.AddShape(pin);
						
			var <?php echo $map2; ?> = new VEMap('<?php echo $map2; ?>');
			<?php echo $map2; ?>.LoadMap(SpaceNeedle, 15);
			<?php echo $map2; ?>.HideDashboard();
			var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
			pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
				"<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
			<?php echo $map2; ?>.AddShape(pinS);
						
		</script>
<?php
	}
	
	////////////////////////Listing IFRAME//////////////////////////
	function overviewRentalIFrame($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$slider1,$slider2,$slider3,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		$query="SELECT urldetail FROM rental WHERE parcelid='$pid'";
		$result = mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		?>
		<div style="width:100%; margin:auto; margin-top:10px;">
            <a href="<?php echo $r['urldetail'];?>" target="_blank" class="overviewBotonCss3">IDX</a>
       	</div>
	<?php }
	
	////////////////////////By Owner Rental////////////////////////////////
	function overviewByOwnerRental($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		
		$sql_comparado="Select
		m.state, m.mlnumber, m.parcelid, m.agent, m.county, m.status, m.folio, m.agentemail,
		m.address, m.dom, m.muncode, m.stno, m.entrydate,
		m.agentlic, m.dir, m.lprice, m.subdno, m.agentph,
		m.street, m.ldate, m.sec, m.agentph2, m.listtype, m.twn,
		m.zip, m.orgprice, m.proptype, m.brokerpctg, m.city,
		m.type, m.area, m.closingdt, m.constype, m.officename,
		m.devlpmnt, m.parceln, m.officefax, m.geoarea, m.waterf,
		m.directns1, m.waterfd, m.occupinfo,
		m.lsqft, m.apxtotsqft, m.yrbuilt,
		m.bath, m.style,
		m.beds, m.pool, m.hbath, m.coold, m.remark1,
		p.ccoded
		FROM byowner_r m
		INNER JOIN psummary p ON (m.parcelid=p.parcelid)
		Where m.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
?>
		<div align="center" id="byownerrental_data_div" class="fondo_realtor_result_tab">
        <div align="center" style="width:100%; margin:auto; margin-top:0px;">
        <div id="<?php echo $map; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left;">
        </div><br clear="all" />
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px; float:left;"><?php echo $myrow['ccoded'].' byownerrental'; ?></h1>
        <?php if($status_pro=='A'){?>
            <div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
                For Rent: $<?php echo number_format($lprice_pro,0,'.',',');?>
            </div>
        <?php }?>
        <br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("state","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("state","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['state']; ?></td>
                <td valign=top title="<?php echo camptit("mlnumber","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mlnumber","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['mlnumber']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("county","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("county","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['county']; ?></td>
                <td valign=top title="<?php echo camptit("parcelid","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['parcelid']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("address","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("address","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['address']; ?></td>
                <td valign=top title="<?php echo camptit("subdno","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("subdno","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['subdno']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("dom","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("dom","rental");?></span></td>
                <td valign=top><?php echo $myrow['dom']; ?></td>
                <td valign=top title="<?php echo camptit("status","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("status","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['status']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("constype","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("constype","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['constype']; ?></td>
                <td valign=top title="<?php echo camptit("proptype","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("proptype","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['ccoded']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("zip","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("zip","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['zip']; ?></td>
                <td valign=top title="<?php echo camptit("entrydate","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("entrydate","rental");?>:</span></td>
                <td valign=top><?php echo format_fecha($myrow['entrydate']); ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("city","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("city","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['city']; ?></td>
                <td valign=top title="<?php echo camptit("lprice","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("lprice","rental");?>:</span></td>
                <td valign=top><?php echo number_format($myrow['lprice'],2,'.',','); ?></td>
                
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("apxtotsqft","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("apxtotsqft","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['apxtotsqft']; ?></td>
                <td valign=top title="<?php echo camptit("waterf","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("waterf","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['waterf']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("beds","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("beds","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['beds']; ?></td>
                <td valign=top title="<?php echo camptit("yrbuilt","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['yrbuilt']; ?></td> 
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("bath","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("bath","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['bath']; ?></td>
                <td valign=top title="<?php echo camptit("hbath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("hbath","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['hbath']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top><span style="font-weight:bold;">REMARK:</span></td valign=top>
                <td colspan=3><?php echo $myrow['remark1'];?></td>
            </tr>
        </table>
        <br clear="all" />
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">CONTACT INFORMATION</h1>
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
         <tr>
           <td valign=top><span style="font-weight:bold;">Courtesy Of:</span></td>
           <td valign=top><?php echo $myrow['officename']; ?></td>
           </tr>  
            
                <?php if($loged && !$block){?>
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="Contact"><span style="font-weight:bold;">Contact:</span></td>
                <td valign=top><?php echo $myrow['agent']; ?></td>
                <td valign=top title="ContactEmail"><span style="font-weight:bold;">ContactEmail:</span></td>
                <td valign=top><?php echo $myrow['agentemail']; ?></td>
                </tr>
          
                  <tr align="left">
                <td valign=top title="<?php echo camptit("agentph","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph']; ?></td>
                <td valign=top title="<?php echo camptit("agentph2","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph2","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph2']; ?></td>
                </tr>
            
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("agentph3","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph3","rental") ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph3']; ?></td>
                <td></td>
                <td></td>
                </tr>
           
                 <tr align="left">
                <td valign=top title="<?php echo camptit("officename","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officename","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['officename']; ?></td>
                <td valign=top title="<?php echo camptit("officefax","rental",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officefax","rental");?>:</span></td>
                <td valign=top><?php echo $myrow['officefax']; ?></td>
                </tr>
           
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("officeemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officeemail","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['officeemail']; ?></td>
                <td></td>
                <td></td>
                </tr>
            <?php }?>
            
             <?php 
            if($realtor){ 
            $sql8 = "SELECT * FROM xima.ximausrs where userid =".$realtorid; 
            $num2= mysql_query($sql8);   
            $res= mysql_fetch_array($num2); 
            $email_pri= $res['profemail']; 
            $email_sec= $res['EMAIL']; 
			
			if (empty($email_pri)) { $email=$email_sec;}else{$email=$email_pri;}
            
            }
            
            if($loged || !$loged){ $email= $myrow['agentemail']; }
             
             
             if($realtor || $loged || !$loged){ 
			  $address_email= 'Parcelid: '.$myrow['folio'].' \n<br> Address: '.$myrow['address'].' \n<br> Zip code:'.$myrow['zip'].' \n<br> City:'.$myrow['city'].'\n<br> County:'.$myrow['county'].'\n<br> State:'.$myrow['state'].'\n<br>';?> 
                <tr align="left"> 
                <td valign=top title="ContactEmail"><span style="font-weight:bold;">ContactEmail:</span></td>
                <td valign=top>
                  <input type="submit" name="contacto" id="contacto"  style="background:#FFF; color:#069; font-size:12px; font-weight:bold;" value="Inquire about this property" onclick="contacto('<?php echo $email; ?>','<?php echo $address_email; ?>');"/>
                  
             </td>
                </tr>
                <?php }?>
           
        </table>
        
        <br />
        </div>
        </div>
        <script>
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
            <?php echo $map; ?>.HideDashboard();
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.AddShape(pin);
                        
           var <?php echo $map2; ?> = new VEMap('<?php echo $map2; ?>');
            <?php echo $map2; ?>.LoadMap(SpaceNeedle, 15);
            <?php echo $map2; ?>.HideDashboard();
            var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map2; ?>.AddShape(pinS);
                        
        </script>
<?php      
	}
	
	////////////////////////By Owner////////////////////////////////
	function overviewByOwner($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$slider1,$slider2,$slider3,$realtor,$realtorid,$loged,$block){
		conectarPorBD($db_data);
		
		$sql_comparado="Select 
		state,mlnumber,parcelid,agent,county,status,folio,agentemail,
		address,dom,muncode,agentid,stno,EntryDate,
		sbdname,agentlic,dir,lprice,subdno,agentph,
		street,ldate,sec,agentph2,un,listtype,twn,agentph3,
		zip,orgprice,PropType,brokerpctg,city,saleprice,
		type,abuyerpctg,area,closingdt,constype,officename,
		devlpmnt,penddate,parceln,officefax,geoarea,waterf,
		directns1,officeemail,last4dig,waterfd,directns2,occupinfo,
		lsqft,withdate,hassocfee,view1,apxtotsqft,yrbuilt,intrmrks1,
		parkd,tsqft,yrbuiltd,intrmrks2,carportd,bath,petallow,style,carportq,
		beds,pool,frontexp,garaged,hbath,coold,heatd,garageq,remark1,remark2,remark3,
		remark4,remark5,agentfax,agenttollfree,officephone,officeotherphone1,
		officeotherphone2,officetollfree
		FROM byowner_s m 
		Where m.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		
?>
		<div align="center" id="byowner_data_div" class="fondo_realtor_result_tab">
        <div align="center" style="width:100%; margin:auto; margin-top:0px; color: #CCC; text-align: justify;">
        <div id="<?php echo $map; ?>" style="width:48%;height:240px; border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div><div id="<?php echo $map2; ?>" align="center" style="width:48%;height:240px;border: 1px solid #4E9494;position:relative;float:left; background:#FFF;">
        <?php 
            $cant_imagen_byowner=0;
            if($status_pro=='A'){
                $_parcelid=$pid;
                $sql_fields="Select
                *  
                FROM imagenes 
                Where parcelid='$_parcelid';";
                
                $result = mysql_query($sql_fields) or die(mysql_error());
                $fila= mysql_fetch_array($result, MYSQL_ASSOC);
                $cant_imagen_byowner=mysql_num_rows($result);
                if($cant_imagen_byowner>0){
                    $arr_pics="";
            
                    if($fila["letra"]=='Y')
                        $mlnumber=$fila["mlnumber"];
                    else
                        $mlnumber=substr($fila["mlnumber"],1);
                    
                    if($myrow['state']=='FL'){
						if(strlen($fila["urlxima"])>5){
							$arr_pics.=$fila["urlxima"].$fila["parcelid"].".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
									if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
									elseif(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
								}
							}
						}else{
							if(url_exists($fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'])){
								$arr_pics.=$fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'];
								if($fila['nphotos']>1){	
									for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
										if(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
											$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
									}
								}
							}else{
								$arr_pics.="img/nophoto.gif";
							}	
						}
					}elseif($myrow['state']=='GA'){
						if(strlen($fila["urlxima"])>5){
							$arr_pics.=$fila["urlxima"].$fila["parcelid"].$fila["sep"].$fila["inicount"].".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']+1; $i<$fila['nphotos']; $i++){
									if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
									elseif(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
								}
							}
						}else{
							if(url_exists($fila['url'].$fila['url2'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$fila['inicount'].".".$fila['tipo'])){
								$arr_pics.=$fila['url'].$fila['url2'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$fila['inicount'].".".$fila['tipo'];
								if($fila['nphotos']>1){	
									for($i=$fila['inicount']+1; $i<$fila['nphotos']; $i++){
										if(url_exists($fila["url"].$fila['url3'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$i.".".$fila['tipo']))
											$arr_pics.=",".$fila["url"].$fila['url3'].'0'.$mlnumber.'/0'.$mlnumber.$fila['sep']."0".$i.".".$fila['tipo'];
									}
								}
							}else{
								$arr_pics.="img/nophoto.gif";
							}	
						}
					}elseif($myrow['state']=='TX'){
						if(strlen($fila["urlxima"])>5){
							$arr_pics.=$fila["urlxima"].$fila["parcelid"].".".$fila['tipo'];
							if($fila['nphotos']>1){	
								for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
									if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
										$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
									elseif(url_exists($fila["url"].$i."/".substr($mlnumber,-4,4)."/".$mlnumber.".".$fila['tipo']))
										$arr_pics.=",".$fila["url"].$i."/".substr($mlnumber,-4,4)."/".$mlnumber.".".$fila['tipo'];
								}
							}
						}else{
							if(url_exists($fila['url'].'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'])){
								$arr_pics.=$fila['url'].'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'];
								if($fila['nphotos']>1){	
									for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
										if(url_exists($fila["url"].$i.'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo']))
											$arr_pics.=",".$fila["url"].$i.'/'.substr($mlnumber,-4,4).'/'.$mlnumber.".".$fila['tipo'];
									}
								}
							}else{
								$arr_pics.="img/nophoto.gif";
							}	
						}
					}	
                }
                mysql_free_result($result);
            
                $arr_data=explode(",",$arr_pics);
        ?>
            <input type="hidden" id="<?php echo $slider1;?>" value="<?php echo $arr_pics;?>" />
            <img id="<?php echo $slider2;?>" src="<?php echo $arr_data[0]; ?>" height="220"/>
           
            <div id="<?php echo $slider3;?>" style="background:#FFF;"></div>
        <?php }?>
        </div><br clear="all" />
        <h1 align="left"  class="overtab_realtor_titulo"><?php  echo $myrow['PropType']; ?></h1>
        <?php if($status_pro=='A'){?>
            <div align="rigth" style="float:right; margin-right:40px; font-weight:bold;" class="overtab_realtor_titulo" >
                For Sale: $<?php echo number_format($lprice_pro,0,'.',',');?>
            </div>
        <?php }?>
        <br clear="all" />
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("state","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("state","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['state']; ?></td>
                <td valign=top title="<?php echo camptit("mlnumber","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("mlnumber","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['mlnumber']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("county","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("county","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['county']; ?></td>
                <td valign=top title="<?php echo camptit("parcelid","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("parcelid","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['parcelid']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("address","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("address","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['address']; ?></td>
                <td valign=top title="<?php echo camptit("subdno","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("subdno","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['subdno']; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("dom","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("dom","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['dom']; ?></td>
                <td valign=top title="<?php echo camptit("status","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("status","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['status']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("constype","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("constype","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['constype']; ?></td>
                <td valign=top title="<?php echo camptit("proptype","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("proptype","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['PropType'] //if($myrow['proptype']=='RE1') echo 'Single Family'; else echo 'Condominium'; ?></td>
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("zip","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("zip","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['zip']; ?></td>
                <td valign=top title="<?php echo camptit("entrydate","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("entrydate","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['EntryDate']; //echo format_fecha($myrow['EntryDate']); ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("city","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("city","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['city']; ?></td>
                <td valign=top title="<?php echo camptit("lprice","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("lprice","mlsresidential");?>:</span></td>
                <td valign=top><?php echo number_format($myrow['lprice'],2,'.',','); ?></td>
                
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("apxtotsqft","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("apxtotsqft","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['apxtotsqft']; ?></td>
                <td valign=top title="<?php echo camptit("waterf","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("waterf","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['waterf']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("beds","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("beds","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['beds']; ?></td>
                <td valign=top title="<?php echo camptit("yrbuilt","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("yrbuilt","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['yrbuilt']; ?></td> 
            </tr>
            <tr align="left">
                <td valign=top title="<?php echo camptit("bath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("bath","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['bath']; ?></td>
                <td valign=top title="<?php echo camptit("hbath","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("hbath","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['hbath']; ?></td>
            </tr>
            <tr style="background-color:#b5cedd;" align="left">
                <td valign=top><span style="font-weight:bold;">REMARK:</span></td valign=top>
                <td colspan=3><?php echo $myrow['remark1']." ".$myrow['remark2']." ".$myrow['remark3']." ".$myrow['remark4']." ".$myrow['remark5'];?></td>
            </tr>
        </table>
        <br clear="all" />
        <h1 align="left"  class="overtab_realtor_titulo">CONTACT INFORMATION</h1>
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
           <tr>    <?   if($realtor){ 
            $sql8 = "SELECT * FROM xima.ximausrs where userid =".$realtorid; 
            $num2= mysql_query($sql8);   
            $res= mysql_fetch_array($num2); 
           	$name=$res['NAME'].' '.$res['SURNAME'];
			$telephone=$res['profphone'];
			$telephone2=$res['profphone'];
			$email_pri= $res['profemail']; 
            $email_sec= $res['EMAIL']; 
			
			if (empty($email_pri)) { $email=$email_sec;}else{$email=$email_pri;}
		
            }else{  $name=$myrow['agent'];
			        $email=$myrow['agentemail'];
					 $telephone=$myrow['agenteph']; 
					 $telephone2=$myrow['agenteph'];}    ?>
           <td valign=top><span style="font-weight:bold;">Courtesy Of:</span></td>
           <td valign=top><?php echo $myrow['officename']; ?></td>
           </tr>  
           <?php //if(!$realtor && $loged && !$block){?>
                 <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="Contact"><span style="font-weight:bold;">Contact:</span></td>
                <td valign=top><?php echo $name; ?></td>
               
                <td valign=top title="ContactEmail"><span style="font-weight:bold;">ContactEmail:</span></td>
                <td valign=top><?php echo $email; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top title="<?php echo camptit("agentph","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $telephone; ?></td>
                
                <td valign=top title="<?php echo camptit("agentph2","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph2","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $telephone2; ?></td>
                </tr>
                <? if (!$realtor){ ?>
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("agentph3","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("agentph3","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['agentph3']; ?></td>
                <td valign=top><span style="font-weight:bold;">Contact Fax:</span></td>
                <td valign=top><?php echo $myrow['agentfax']; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top ><span style="font-weight:bold;">Contact Tollfree:</span></td>
                <td valign=top><?php echo $myrow['agenttollfree']; ?></td>
                <td valign=top></td>
                <td valign=top></td>
                </tr>
                
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top title="<?php echo camptit("officename","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officename","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['officename']; ?></td>
                
                <td valign=top title="<?php echo camptit("officefax","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officefax","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['officefax']; ?></td>
                </tr>
                
                <tr align="left">
                <td valign=top title="<?php echo camptit("officephone","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officephone","mlsresidential") ;?>:</span></td>
                <td valign=top><?php echo $myrow['officephone']; ?></td>
                <td valign=top><span style="font-weight:bold;">OfficePhone_2:</span></td>
                <td valign=top><?php echo $myrow['officeotherphone1']; ?></td>
                </tr>
                
                <tr style="background-color:#b5cedd;" align="left">
                <td valign=top><span style="font-weight:bold;">OfficePhone_3:</span></td>
                <td valign=top><?php echo $myrow['officeotherphone2']; ?></td>
                <td valign=top><span style="font-weight:bold;">Office Tollfree:</span></td>
                <td valign=top><?php echo $myrow['officetollfree']; ?></td>
                </tr>
                
                 <tr align="left">
                <td valign=top title="<?php echo camptit("officeemail","mlsresidential",'desc');?>"><span style="font-weight:bold;"><?php echo camptit("officeemail","mlsresidential");?>:</span></td>
                <td valign=top><?php echo $myrow['officeemail']; ?></td>
                <td></td>
                <td></td>
            </tr>
                 <?php }?> 
                 
            <?php 
            if($realtor){ 
            $sql8 = "SELECT * FROM xima.ximausrs where userid =".$realtorid; 
            $num2= mysql_query($sql8);   
            $res= mysql_fetch_array($num2); 
            $email_pri= $res['profemail']; 
            $email_sec= $res['EMAIL']; 
			
			if (empty($email_pri)) { $email=$email_sec;}else{$email=$email_pri;}
            
            }
            
            if($loged || !$loged){ $email= $myrow['agentemail']; }
            
                
            if($realtor || $loged || !$loged){ 
                $address_email= 'Parcelid: '.$myrow['folio'].' \n<br> Address: '.$myrow['address'].' \n<br> Zip code:'.$myrow['zip'].' \n<br> City:'.$myrow['city'].'\n<br> County:'.$myrow['county'].'\n<br> State:'.$myrow['state'].'\n<br>';?> 
                   
                <tr align="left"> 
                <td valign=top title="ContactEmail"><span style="font-weight:bold;">ContactEmail:</span></td>
                <td valign=top>
                  <input type="submit" name="contacto" id="contacto"  style="background:#FFF; color:#069; font-size:12px; font-weight:bold;" value="Inquire about this property" onclick="contacto('<?php echo $email; ?>','<?php echo $address_email; ?>');"/>
                 </td>
                </tr>
                <?php }?>
           
         
         
        </table>
        </div>
        </div>
        <script>
            var tiene_ima=<?php echo $cant_imagen_byowner;?>;
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
            <?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
            <?php echo $map; ?>.map.HideDashboard();
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.map.AddShape(pin);
                        
            if(tiene_ima==0){
                var <?php echo $map2; ?> = new XimaMap('<?php echo $map2; ?>','mapa_search_latlong','control_mapa_div','_pan','_draw','_poly','_clear','_maxmin');
                <?php echo $map2; ?>.map = new VEMap('<?php echo $map2; ?>');
                <?php echo $map2; ?>.map.LoadMap(SpaceNeedle, 15);
                <?php echo $map2; ?>.map.HideDashboard();
                var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
                pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                    "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
                <?php echo $map2; ?>.map.AddShape(pinS);
            }else{
                
                var data=document.getElementById('<?php echo $slider1;?>').value;
                img_src=data.split(',');
                
                new Ext.Slider({
                    id: 'slidercomp_img',					
                    renderTo: '<?php echo $slider3;?>',
                    width: 200,
                    value:0,
                    increment: 1,
                    minValue: 0,
                    maxValue: (img_src.length-1),
                    listeners: {
                        change: function (slider,val){
                            document.getElementById('<?php echo $slider2;?>').src=img_src[val];
                        }
                    }
                });
            }
        </script>
<?php
	}
	
	////////////////////////Map////////////////////////////////
	function overviewMap($pid,$db_data,$map,$latitude,$longitude,$par){
		conectarPorBD($db_data);
		
?>
		<div align="center" class="fondo_realtor_result_tab">
        <div align="center" style="width:100%; margin:auto; margin-top:0px;">
        <div id="<?php echo $map; ?>" style="width:99%; height:350px; border: 1px solid #4E9494; position:relative; float:center; margin-right:0px;"></div>
        </div>
        </div>
        <script>
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','_pan<?php echo $map; ?>','_draw<?php echo $map; ?>','_poly<?php echo $map; ?>','_clear<?php echo $map; ?>','_maxmin<?php echo $map; ?>');
            <?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.map.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://www.reifax.com/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.map.AddShape(pin);
            if(document.getElementById('<?php echo $map; ?>_control_mapa_div'))
                <?php echo $map; ?>.map.DeleteControl(<?php echo $map; ?>._mapTool);
            <?php echo $map; ?>.curBoton="AVG";
			<?php if(!$par){?>
				<?php echo $map; ?>.setBarType('no_full');
				<?php echo $map; ?>.ins_toolbar("320px","overview");
			<?php }?>
                        
        </script>
<?php
	}
	
	////////////////////////Comparable Distress////////////////////////////////
	function overviewCompDistress($pid,$db_data,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir){
		conectarPorBD($db_data);
		
		$_mainTbl="psummary";
		$_zip="$_mainTbl.ozip,";
		$_larea="$_mainTbl.bheated,";
		$_unit="$_mainTbl.unit,";
		$pricesSel="$_mainTbl.saleprice";
		
	
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,Marketvalue.pendes,Marketvalue.marketvalue,
		Marketvalue.marketmedia,Marketvalue.marketpctg,Marketvalue.offertvalue,Marketvalue.offertmedia,Marketvalue.offertpctg, LATLONG.LATITUDE,LATLONG.LONGITUDE
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		
		$dt_comparado="";
		$myrow["xlat"]=$myrow["LATITUDE"];	$myrow["xlong"]=$myrow["LONGITUDE"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('distress','comparables',true);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('distress','comparables',false);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
?>
		<div align="center" id="todo_compdistress_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
            <div id="<?php echo $map; ?>" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>
            <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="distress_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>
		
        <script>
		var limitDistress=50;
		var orderDissField = '<?php echo $orderField;?>';
		var orderDissDir = '<?php echo $orderDir;?>';
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('DISS-MAP');
			if(document.getElementById('<?php echo $map; ?>_control_mapa_div'))
				<?php echo $map; ?>.map.DeleteControl(<?php echo $map; ?>._mapTool);
			<?php echo $map; ?>.curBoton="AVG";
			<?php echo $map; ?>.ins_toolbar("320px","overview");
			
			var arrLatLong= new Array();
			var pin = new VEPushpin(
				'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
				new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
				'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
				'  <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
				'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
				'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
				'    <td>Subject</td>'+
				'  </tr></table></div>',
				'',
				'',
				''
			);
			<?php echo $map; ?>.map.ClearInfoBoxStyles();
			<?php echo $map; ?>.map.AddPushpin(pin);
			arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
			<?php echo $map; ?>.map.SetMapView(arrLatLong);
		<?php }else{?>
			<?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.map.LoadMap(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>), 15);
		<?php }?>
			
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
			if(record.get('diff_larea').length>0 && record.get('diff_larea')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
			if(record.get('diff_lsqft').length>0 && record.get('diff_lsqft')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
			if(record.get('diff_beds').length>0 && record.get('diff_beds')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
			if(record.get('diff_bath').length>0 && record.get('diff_bath')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
			if(record.get('diff_zip').length>0 && record.get('diff_zip')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		var arrayDataSujetoDistress = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoDistress = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoDistress = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			width: 'auto',
			height: 65,
			store: storeSujetoDistress,
			columns: [
				<?php 
					echo "{header: 'Sta.', width: 30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			],
			listeners: {
				"rowclick": function(grid, row, e) {
					document.getElementById("<?php echo $pin;?>_0_<?php echo $pid;?>_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				}
			}
		});
		
		storeSujetoDistress.loadData(arrayDataSujetoDistress);
		if(Ext.isIE){
			gridSujetoDistress.getEl().focus();
		}
	
		var storeDistress = new Ext.data.Store({
			url: 'http://www.reifax.com/properties_1mile_distress.php',
			baseParams: {'no_func': true,'bd': '<?php echo $db_data;?>', 'id': '<?php echo $pid;?>','userid':<?php echo $_COOKIE['datos_usr']['USERID'];?>, 'array_taken': "<?php echo $array_taken;?>",'sort': orderDissField, 'dir': orderDissDir},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.map.Clear();
					var arrLatLong= new Array();
					var pin = new VEPushpin(
						'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
						new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
						'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
						'  <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
						'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
						'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
						'    <td>Subject</td>'+
						'  </tr></table></div>',
						'',
						'',
						''
					);
					<?php echo $map; ?>.map.ClearInfoBoxStyles();
					<?php echo $map; ?>.map.AddPushpin(pin);
					arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							getCasita(status,pendes,sold);
							pin = new VEPushpin(
								'<?php echo $pin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>',
								new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')),
								'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
								data[k].get('pin_address'),
								/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>'+'" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
								'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
								'  <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
								'    <td>'+data[k].get('pin_lsqft')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
								'    <td>'+data[k].get('pin_larea')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
								'    <td>'+data[k].get('pin_bed')+'/'+data[k].get('pin_bath')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
								'    <td>'+data[k].get('pin_saleprice')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
								'    <td>'+lsImgCss[indImgCss].explain+'</td>'+
								'  </tr> <tr> '+
								'    <td colspan=2 style=" font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" onclick="createOverview(<?php echo $county;?>,\''+data[k].get('pid')+'\',\''+status+'\','+user_web+',true);">Click here for Overview Comp</td>'+
								'  </tr></table></div>',
								'',
								'',
								''
							);
							<?php echo $map; ?>.map.ClearInfoBoxStyles();
							<?php echo $map; ?>.map.AddPushpin(pin);
							arrLatLong.push(new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')));
						}
					}
					
					<?php echo $map; ?>.map.SetMapView(arrLatLong);
					//repaint shape/poly on filter.
					try{
						<?php echo $map; ?>.slDrawing.DeleteShape(<?php echo $map; ?>.myCurrentShape);
					}catch (err){}
					if(<?php echo $map; ?>.filter_map && <?php echo $map; ?>._curLatLonShape.length>5){
						var _xpoints=new Array();
						for(x=0;x<<?php echo $map; ?>._curLatLonShape.split("/").length;x++)
						{								//latitude	longitude
							_xpoints.push(new VELatLong(<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[0],<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[1]));
						}
						<?php echo $map; ?>.points2shape(_xpoints);
						//<?php echo $map; ?>.map.SetMapView(<?php echo $map; ?>.myCurrentShape.GetPoints());
					}
					
					<?php if(!$print){?>
					if ( Ext.fly(Ext.fly(gridDistress.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckDistress=true;
						gridDistress.getSelectionModel().selectAll();
						selected_dataDistress=new Array();
					}else{
						AllCheckDistress=false;
						var sel = [];
						if(selected_dataDistress.length > 0){
							for(val in selected_dataDistress){
								var ind = gridDistress.getStore().find('pid',selected_dataDistress[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridDistress.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridDistress.setHeight(alto);
					<?php }?>
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataDistress = new Array();
		var AllCheckDistress=false;
		
		var smDistress = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataDistress.indexOf(record.get('pid'))==-1)
						selected_dataDistress.push(record.get('pid'));
					
					if(Ext.fly(gridDistress.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckDistress=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataDistress = selected_dataDistress.remove(record.get('pid'));
					AllCheckDistress=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridDistress = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			store: storeDistress,
			columns: [],
			<?php if(!$ocomp){?>
			sm: smDistress,
			<?php }?>
			listeners: {
				"rowclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var id = record.get('status').split('_');
					var numeroPin=id[0];
					
					document.getElementById("<?php echo $pin;?>_"+numeroPin+"_"+record.get('pid')+"_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				},
				"rowdblclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var pid = record.get('pid');
					var status = record.get('status').split('_')[1].split('-')[0];
					createOverview(<?php echo $county;?>,pid,status,user_web,true);
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderDissField = sortInfo.field;
					orderDissDir = sortInfo.direction;
				}
			},
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitDistress,
				store: storeDistress,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitDistress=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compDistress_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitDistress=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compDistress_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitDistress=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compDistress_group'
				})
				]
			})
		});
		<?php }?>
		
		storeDistress.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeDistress.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					columns.push(smDistress);
					<?php }?>
					Ext.each(storeDistress.reader.jsonData.columns, function(column){
						columns.push(column);
					});
					gridDistress.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeDistress.reader.jsonData.columns;
				var data = storeDistress.reader.jsonData.records;
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else
									{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
	
		<?php if(!$print){?>
			storeDistress.load({params:{start:0, limit:limitDistress}});
		<?php }else{?>
			storeDistress.load();
		<?php }?>
	
	
		<?php if(!$print){?>
		if(document.getElementById('tabs')){
			if(document.getElementById('todo_compdistress_panel').offsetHeight > tabs.getHeight()){
				tabs.setHeight(document.getElementById('todo_compdistress_panel').offsetHeight+100);
				tabs2.setHeight(tabs.getHeight());
				viewport.setHeight(tabs.getHeight());
			}
		}
		<?php }?>
	</script>
<?php
	}
	
	////////////////////////Comparable Rental////////////////////////////////
	function overviewCompRental($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir){
		conectarPorBD($db_data);
		
		$_mainTbl="psummary";
		$_zip="$_mainTbl.ozip,";
		$_larea="$_mainTbl.bheated,";
		$_unit="$_mainTbl.unit,";
		$pricesSel="$_mainTbl.saleprice";
		
	
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,Marketvalue.pendes,Marketvalue.marketvalue,
		Marketvalue.marketmedia,Marketvalue.marketpctg,Marketvalue.offertvalue,Marketvalue.offertmedia,Marketvalue.offertpctg, LATLONG.LATITUDE,LATLONG.LONGITUDE
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		
		$dt_comparado="";
		$myrow["xlat"]=$myrow["LATITUDE"];	$myrow["xlong"]=$myrow["LONGITUDE"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('rental','comparables',true);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('rental','comparables',false);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
?>
		<div align="center" id="todo_comprental_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
            <div id="<?php echo $map; ?>" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>
            <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comprental_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>
		
        <script>
		var limitRental=50;
		var orderRentField = '<?php echo $orderField;?>';
		var orderRentDir = '<?php echo $orderDir;?>';
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('RENT-MAP');
			if(document.getElementById('<?php echo $map; ?>_control_mapa_div'))
				<?php echo $map; ?>.map.DeleteControl(<?php echo $map; ?>._mapTool);
			<?php echo $map; ?>.curBoton="AVG";
			<?php echo $map; ?>.ins_toolbar("320px","overview");
			
			var arrLatLong= new Array();
			var pin = new VEPushpin(
				'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
				new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
				'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
				'  <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
				'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
				'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
				'    <td>Subject</td>'+
				'  </tr></table></div>',
				'',
				'',
				''
			);
			<?php echo $map; ?>.map.ClearInfoBoxStyles();
			<?php echo $map; ?>.map.AddPushpin(pin);
			arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
			<?php echo $map; ?>.map.SetMapView(arrLatLong);
		<?php }else{?>
			<?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.map.LoadMap(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>), 15);
		<?php }?>
			
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
			if(record.get('diff_larea').length>0 && record.get('diff_larea')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
			if(record.get('diff_lsqft').length>0 && record.get('diff_lsqft')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
			if(record.get('diff_beds').length>0 && record.get('diff_beds')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
			if(record.get('diff_bath').length>0 && record.get('diff_bath')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
			if(record.get('diff_zip').length>0 && record.get('diff_zip')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		var arrayDataSujetoRental = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoRental = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoRental = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			width: 'auto',
			height: 65,
			store: storeSujetoRental,
			columns: [
				<?php 
					echo "{header: 'Sta.', width: 30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			],
			listeners: {
				"rowclick": function(grid, row, e) {
					document.getElementById("<?php echo $pin;?>_0_<?php echo $pid;?>_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				}
			}
		});
		
		storeSujetoRental.loadData(arrayDataSujetoRental);
		if(Ext.isIE){
			gridSujetoRental.getEl().focus();
		}
	
		var storeRental = new Ext.data.Store({
			url: 'http://www.reifax.com/properties_look4rental.php',
			baseParams: {'no_func': true,'bd': '<?php echo $db_data;?>','prop': '<?php echo $xcode;?>', 'id': '<?php echo $pid;?>','userid':<?php echo $_COOKIE['datos_usr']['USERID'];?>, 'array_taken': "<?php echo $array_taken;?>",'sort': orderRentField, 'dir': orderRentDir},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.map.Clear();
					var arrLatLong= new Array();
					var pin = new VEPushpin(
						'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
						new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
						'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
						'  <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
						'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
						'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
						'    <td>Subject</td>'+
						'  </tr></table></div>',
						'',
						'',
						''
					);
					<?php echo $map; ?>.map.ClearInfoBoxStyles();
					<?php echo $map; ?>.map.AddPushpin(pin);
					arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							getCasita(status,pendes,sold);
							pin = new VEPushpin(
								'<?php echo $pin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>',
								new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')),
								'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
								data[k].get('pin_address'),
								/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>'+'" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
								'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
								'  <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
								'    <td>'+data[k].get('pin_lsqft')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
								'    <td>'+data[k].get('pin_larea')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
								'    <td>'+data[k].get('pin_bed')+'/'+data[k].get('pin_bath')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
								'    <td>'+data[k].get('pin_saleprice')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
								'    <td>'+lsImgCss[indImgCss].explain+'</td>'+
								'  </tr> <tr> '+
								'    <td colspan=2 style=" font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" onclick="createOverview(<?php echo $county;?>,\''+data[k].get('pid')+'\',\''+status+'\','+user_web+',true);">Click here for Overview Comp</td>'+
								'  </tr></table></div>',
								'',
								'',
								''
							);
							<?php echo $map; ?>.map.ClearInfoBoxStyles();
							<?php echo $map; ?>.map.AddPushpin(pin);
							arrLatLong.push(new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')));
						}
					}
					
					<?php echo $map; ?>.map.SetMapView(arrLatLong);
					//repaint shape/poly on filter.
					try{
						<?php echo $map; ?>.slDrawing.DeleteShape(<?php echo $map; ?>.myCurrentShape);
					}catch (err){}
					if(<?php echo $map; ?>.filter_map && <?php echo $map; ?>._curLatLonShape.length>5){
						var _xpoints=new Array();
						for(x=0;x<<?php echo $map; ?>._curLatLonShape.split("/").length;x++)
						{								//latitude	longitude
							_xpoints.push(new VELatLong(<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[0],<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[1]));
						}
						<?php echo $map; ?>.points2shape(_xpoints);
					}
					
					<?php if(!$print){?>
					if ( Ext.fly(Ext.fly(gridRental.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckRental=true;
						gridRental.getSelectionModel().selectAll();
						selected_dataRental=new Array();
					}else{
						AllCheckRental=false;
						var sel = [];
						if(selected_dataRental.length > 0){
							for(val in selected_dataRental){
								var ind = gridRental.getStore().find('pid',selected_dataRental[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridRental.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridRental.setHeight(alto);
					<?php }?>
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataRental = new Array();
		var AllCheckRental=false;
		
		var smRental = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataRental.indexOf(record.get('pid'))==-1)
						selected_dataRental.push(record.get('pid'));
					
					if(Ext.fly(gridRental.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckRental=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataRental = selected_dataRental.remove(record.get('pid'));
					AllCheckRental=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridRental = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			store: storeRental,
			columns: [],
			<?php if(!$ocomp){?>
			sm: smRental,
			<?php }?>
			listeners: {
				"rowclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var id = record.get('status').split('_');
					var numeroPin=id[0];
					
					document.getElementById("<?php echo $pin;?>_"+numeroPin+"_"+record.get('pid')+"_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				},
				"rowdblclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var pid = record.get('pid');
					var status = record.get('status').split('_')[1].split('-')[0];
					createOverview(<?php echo $county;?>,pid,status,user_web,true);
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderRentField = sortInfo.field;
					orderRentDir = sortInfo.direction;
				}
			},
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitRental,
				store: storeRental,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitRental=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compRental_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitRental=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compRental_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitRental=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compRental_group'
				})
				]
			})
		});
		<?php }?>
		
		storeRental.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeRental.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					columns.push(smRental);
					<?php }?>
					Ext.each(storeRental.reader.jsonData.columns, function(column){
						columns.push(column);
					});
					gridRental.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeRental.reader.jsonData.columns;
				var data = storeRental.reader.jsonData.records;
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else
									{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
	
		<?php if(!$print){?>
			storeRental.load({params:{start:0, limit:limitRental}});
		<?php }else{?>
			storeRental.load();
		<?php }?>
	
	
		<?php if(!$print){?>
		if(document.getElementById('tabs')){
			if(document.getElementById('todo_comprental_panel').offsetHeight > tabs.getHeight()){
				tabs.setHeight(document.getElementById('todo_comprental_panel').offsetHeight+100);
				tabs2.setHeight(tabs.getHeight());
				viewport.setHeight(tabs.getHeight());
			}
		}
		<?php }?>
	</script>
<?php
	}
	
	////////////////////////Comparable Active////////////////////////////////
	function overviewCompActive($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir,$investorLimit){
		conectarPorBD($db_data);
		
		$_mainTbl="psummary";
		$_zip="$_mainTbl.ozip,";
		$_larea="$_mainTbl.bheated,";
		$_unit="$_mainTbl.unit,";
		$pricesSel="$_mainTbl.saleprice";
	
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,Marketvalue.pendes,Marketvalue.marketvalue,
		Marketvalue.marketmedia,Marketvalue.marketpctg,Marketvalue.offertvalue,Marketvalue.offertmedia,Marketvalue.offertpctg, LATLONG.LATITUDE,LATLONG.LONGITUDE
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		
		$dt_comparado="";
		$myrow["xlat"]=$myrow["LATITUDE"];	$myrow["xlong"]=$myrow["LONGITUDE"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('active','comparables',true);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('active','comparables',false);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
?>
		<div align="center" id="todo_compactive_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
            <div id="<?php echo $map; ?>" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>
            <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="compactive_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>
		
        <script>
		var limitActive=<?php if($investorLimit=='L') echo 4; else echo 50;?>;
		var orderActiveField = '<?php echo $orderField;?>';
		var orderActiveDir = '<?php echo $orderDir;?>';
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('ACTI-MAP');
			if(document.getElementById('<?php echo $map; ?>_control_mapa_div'))
				<?php echo $map; ?>.map.DeleteControl(<?php echo $map; ?>._mapTool);
			<?php echo $map; ?>.curBoton="AVG";
			<?php echo $map; ?>.ins_toolbar("320px","overview");
			
			var arrLatLong= new Array();
			var pin = new VEPushpin(
				'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
				new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
				'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
				'  <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
				'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
				'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
				'    <td>Subject</td>'+
				'  </tr></table></div>',
				'',
				'',
				''
			);
			<?php echo $map; ?>.map.ClearInfoBoxStyles();
			<?php echo $map; ?>.map.AddPushpin(pin);
			arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
			<?php echo $map; ?>.map.SetMapView(arrLatLong);
		<?php }else{?>
			<?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.map.LoadMap(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>), 15);
		<?php }?>
			
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
			if(record.get('diff_larea').length>0 && record.get('diff_larea')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
			if(record.get('diff_lsqft').length>0 && record.get('diff_lsqft')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
			if(record.get('diff_beds').length>0 && record.get('diff_beds')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
			if(record.get('diff_bath').length>0 && record.get('diff_bath')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
			if(record.get('diff_zip').length>0 && record.get('diff_zip')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		<?php 
			if($print && $array_taken!=""){
				$_POST["comparables"]=$array_taken;
				$_POST["pid"]=$pid;
				$_POST["status"]='A';
				$_POST['no_func']=true;
				ob_start();
				include('properties_calculate.php');
				$content = ob_get_contents();
				ob_end_clean();
				$datos=explode("^",$content);
				
				$data_comparado[0]->offertvalue=str_replace(',','',$datos[0]);
				$data_comparado[0]->offertmedia=str_replace(',','',$datos[1]);
				$data_comparado[0]->offertpctg=str_replace(',','',$datos[2]);
			}
		?>
		
		var arrayDataSujetoActive = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		<?php if($investorLimit=='L' || $investorLimit=='M'){?>
			var comparable_cca='';
		<?php }?>
		
		var storeSujetoActive = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoActive = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			width: 'auto',
			height: 65,
			store: storeSujetoActive,
			columns: [
				<?php 
					echo "{header: 'Sta.', width: 30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			],
			listeners: {
				"rowclick": function(grid, row, e) {
					document.getElementById("<?php echo $pin;?>_0_<?php echo $pid;?>_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				}
			}
		});
		
		storeSujetoActive.loadData(arrayDataSujetoActive);
		if(Ext.isIE){
			gridSujetoActive.getEl().focus();
		}
	
		var storeActive = new Ext.data.Store({
			url: 'http://www.reifax.com/properties_look4comparables.php',
			baseParams: {'bd': '<?php echo $db_data;?>','prop': '<?php echo $xcode;?>','status':'A','reset':'true', 'id': '<?php echo $pid;?>','userid':<?php echo $_COOKIE['datos_usr']['USERID'];?>, 'type': 'nobpo', 'sort': orderActiveField, 'dir': orderActiveDir, 'array_taken': "<?php echo $array_taken;?>"},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.map.Clear();
					var arrLatLong= new Array();
					var pin = new VEPushpin(
						'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
						new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
						'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
						'  <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
						'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
						'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
						'    <td>Subject</td>'+
						'  </tr></table></div>',
						'',
						'',
						''
					);
					<?php echo $map; ?>.map.ClearInfoBoxStyles();
					<?php echo $map; ?>.map.AddPushpin(pin);
					arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							getCasita(status,pendes,sold);
							pin = new VEPushpin(
								'<?php echo $pin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>',
								new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')),
								'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
								data[k].get('pin_address'),
								/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>'+'" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
								'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
								'  <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
								'    <td>'+data[k].get('pin_lsqft')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
								'    <td>'+data[k].get('pin_larea')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
								'    <td>'+data[k].get('pin_bed')+'/'+data[k].get('pin_bath')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
								'    <td>'+data[k].get('pin_saleprice')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
								'    <td>'+lsImgCss[indImgCss].explain+'</td>'+
								'  </tr> <tr> '+
								'    <td colspan=2 style=" font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" onclick="createOverview(<?php echo $county;?>,\''+data[k].get('pid')+'\',\''+status+'\','+user_web+',true);">Click here for Overview Comp</td>'+
								'  </tr></table></div>',
								'',
								'',
								''
							);
							<?php echo $map; ?>.map.ClearInfoBoxStyles();
							<?php echo $map; ?>.map.AddPushpin(pin);
							arrLatLong.push(new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')));
							
							<?php if($investorLimit=='L' || $investorLimit=='M'){?>
								if(comparable_cca!='') comparable_cca+=",";
								comparable_cca+="'"+data[k].get('pid')+"'";
							<?php }?>
						}
					}
					
					<?php 
					if($investorLimit=='L' || $investorLimit=='M'){
					?>
						//alert(comparable_cco);
						Ext.Ajax.request( 
						{  
							waitMsg: 'Validating...',
							url: 'properties_calculate.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: '<?php echo $data_comparado[0]->parcelid;?>',
								bd: '<?php echo $db_data;?>',
								status: 'A',
								comparables: comparable_cca
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var results=response.responseText.split('^');
								
								activevalue = parseFloat((results[0]).replace(',',''));
								
								arrayDataSujetoActive[0][13]=activevalue;
								arrayDataSujetoActive[0][14]=results[1];
								if(document.getElementById('activevalue')) document.getElementById('activevalue').innerHTML=Ext.util.Format.usMoney(activevalue);
								storeSujetoActive.loadData(arrayDataSujetoActive);
								
								if(marketvalue!=99999999 && activevalue!=99999999){
									var selectvalue=reifaxvalue;
									if(selectvalue>marketvalue && marketvalue>0) selectvalue=marketvalue;
									if(selectvalue>activevalue && activevalue>0) selectvalue=activevalue;
									document.getElementById('selectvalue').innerHTML=Ext.util.Format.usMoney(selectvalue);
									document.getElementById('percentvalue').innerHTML=percentvalue+'%';
									document.getElementById('ioffertvalue').innerHTML=Ext.util.Format.usMoney((selectvalue*(percentvalue/100)));
									if(roundTo==500)
										finalvalue = parseFloat((Math.floor((selectvalue*(percentvalue/100))/1000)+'500.00'));
									else 
										finalvalue = parseFloat(((Math.floor((selectvalue*(percentvalue/100))/1000)+1)+'000.00'));
									
									document.getElementById('finalvalue').innerHTML=Ext.util.Format.usMoney(finalvalue);
									
									if(document.getElementById('offer')){ 
										Ext.getCmp('offer').setValue(finalvalue);
									}   
								}
								
								if (document.getElementById('tabs')) {
		
									if (document.getElementById('report_content').offsetHeight > tabs.getHeight()) {
										tabs.setHeight(document.getElementById('report_content').offsetHeight+800);
										viewport.setHeight(document.getElementById('report_content').offsetHeight+1100);
									}
									
								}
							}                                
						});
					<?php
					}
					?>
					
					<?php echo $map; ?>.map.SetMapView(arrLatLong);
					//repaint shape/poly on filter.
					try{
						<?php echo $map; ?>.slDrawing.DeleteShape(<?php echo $map; ?>.myCurrentShape);
					}catch (err){}
					if(<?php echo $map; ?>.filter_map && <?php echo $map; ?>._curLatLonShape.length>5){
						var _xpoints=new Array();
						for(x=0;x<<?php echo $map; ?>._curLatLonShape.split("/").length;x++)
						{								//latitude	longitude
							_xpoints.push(new VELatLong(<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[0],<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[1]));
						}
						<?php echo $map; ?>.points2shape(_xpoints);
					}
					
					<?php if(!$print){?>
					if ( Ext.fly(Ext.fly(gridActive.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckActive=true;
						gridActive.getSelectionModel().selectAll();
						selected_dataActive=new Array();
					}else{
						AllCheckActive=false;
						var sel = [];
						if(selected_dataActive.length > 0){
							for(val in selected_dataActive){
								var ind = gridActive.getStore().find('pid',selected_dataActive[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridActive.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridActive.setHeight(alto);
					<?php }?>
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataActive = new Array();
		var AllCheckActive=false;
		
		var smActive = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataActive.indexOf(record.get('pid'))==-1)
						selected_dataActive.push(record.get('pid'));
					
					if(Ext.fly(gridActive.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckActive=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataActive = selected_dataActive.remove(record.get('pid'));
					AllCheckActive=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridActive = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			store: storeActive,
			columns: [],
			<?php if(!$ocomp){?>
			sm: smActive,
			<?php }?>
			listeners: {
				"rowclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var id = record.get('status').split('_');
					var numeroPin=id[0];
					
					document.getElementById("<?php echo $pin;?>_"+numeroPin+"_"+record.get('pid')+"_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				},
				"rowdblclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var pid = record.get('pid');
					var status = record.get('status').split('_')[1].split('-')[0];
					createOverview(<?php echo $county;?>,pid,status,user_web,true);
				},
				"sortchange": function(grid, sortInfo){
					orderActiveField = sortInfo.field;
					orderActiveDir = sortInfo.direction;
				}
			},
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitActive,
				store: storeActive,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitActive=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compActive_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitActive=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compActive_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitActive=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compActive_group'
				})
				]
			})
		});
		<?php }?>
		
		storeActive.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeActive.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					columns.push(smActive);
					<?php }?>
					Ext.each(storeActive.reader.jsonData.columns, function(column){
						columns.push(column);
					});
					gridActive.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeActive.reader.jsonData.columns;
				var data = storeActive.reader.jsonData.records;
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else
									{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
	
		<?php if(!$print || $investorLimit=='L'){?>
			storeActive.load({params:{start:0, limit:limitActive}});
		<?php }else{?>
			storeActive.load();
		<?php }?>
	
	
		<?php if(!$print){?>
		if(document.getElementById('tabs')){
			if(document.getElementById('todo_compactive_panel').offsetHeight > tabs.getHeight()){
				tabs.setHeight(document.getElementById('todo_compactive_panel').offsetHeight+100);
				tabs2.setHeight(tabs.getHeight());
				viewport.setHeight(tabs.getHeight());
			}
		}
		<?php }?>
	</script>
<?php
	}
	
	////////////////////////Comparable////////////////////////////////
	function overviewComp($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$loged,$block,$array_taken,$orderField,$orderDir,$investorLimit){
		conectarPorBD($db_data);
		
		$_mainTbl	= "psummary";
		$_zip		= "$_mainTbl.ozip,";
		$_larea		= "$_mainTbl.bheated,";
		$_unit		= "$_mainTbl.unit,";
		$pricesSel	= "$_mainTbl.saleprice";
	
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,Marketvalue.pendes,Marketvalue.marketvalue,
		Marketvalue.marketmedia,Marketvalue.marketpctg,Marketvalue.offertvalue,Marketvalue.offertmedia,Marketvalue.offertpctg, LATLONG.LATITUDE,LATLONG.LONGITUDE
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (MARKETVALUE.PARCELID  = $_mainTbl.PARCELID and latlong.parcelid = $_mainTbl.PARCELID ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		
		$dt_comparado="";
		$myrow["xlat"]=$myrow["LATITUDE"];	$myrow["xlong"]=$myrow["LONGITUDE"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		if($loged && !$block)
			$ArIDCT = getArray('comp','comparables',true);
		else
			$ArIDCT = array(45,47,46,9,55,53);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		if($loged && !$block)
			$ArIDCT = getArray('comp','comparables',false);
		else
			$ArIDCT = array(45,47,46,9,55,53);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
?>
		<div align="center" id="todo_comp_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
            <div id="<?php echo $map; ?>" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>
        <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comp_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>
		
        <script>
		var limitComp= <?php if($investorLimit=='L') echo 4; else echo 50;?>;
		var orderCompField = '<?php echo $orderField;?>';
		var orderCompDir = '<?php echo $orderDir;?>';
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print && $loged && !$block){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('COMP-MAP');
			if(document.getElementById('<?php echo $map; ?>_control_mapa_div'))
				<?php echo $map; ?>.map.DeleteControl(<?php echo $map; ?>._mapTool);
			<?php echo $map; ?>.curBoton="AVG";
			<?php echo $map; ?>.ins_toolbar("320px","overview");

			var arrLatLong= new Array();
			var pin = new VEPushpin(
				'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
				new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
				'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
				'  <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
				'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
				'  </tr><tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
				'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
				'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
				'  </tr> <tr> '+
				'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
				'    <td>Subject</td>'+
				'  </tr></table></div>',
				'',
				'',
				''
			);
			<?php echo $map; ?>.map.ClearInfoBoxStyles();
			<?php echo $map; ?>.map.AddPushpin(pin);
			arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
			<?php echo $map; ?>.map.SetMapView(arrLatLong);
			
		<?php }else{?>
			<?php echo $map; ?>.map = new VEMap('<?php echo $map; ?>');
			<?php echo $map; ?>.map.LoadMap(new VELatLong(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>), 15);
		<?php }?>
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
			if(record.get('diff_larea').length>0 && record.get('diff_larea')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
			if(record.get('diff_lsqft').length>0 && record.get('diff_lsqft')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
			if(record.get('diff_beds').length>0 && record.get('diff_beds')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
			if(record.get('diff_bath').length>0 && record.get('diff_bath')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
			if(record.get('diff_zip').length>0 && record.get('diff_zip')>0)
				return '<a href="javascript:void();" ext:qtitle="Public Record Value" ext:qtip="'+showdiffvaluewin(record.get('diff_lsqft'),record.get('diff_larea'),record.get('diff_beds'),record.get('diff_bath'),record.get('diff_zip'))+'">'+value+'</a>';
			else
				return value;
		}
		
		<?php 
			if($print && $array_taken!=""){
				$_POST["comparables"]=$array_taken;
				$_POST["pid"]=$pid;
				$_POST["status"]='CC';
				$_POST['no_func']=true;
				ob_start();
				include('properties_calculate.php');
				$content = ob_get_contents();
				ob_end_clean();
				$datos=explode("^",$content);
				
				$data_comparado[0]->marketvalue=str_replace(',','',$datos[0]);
				$data_comparado[0]->marketmedia=str_replace(',','',$datos[1]);
				$data_comparado[0]->marketpctg=str_replace(',','',$datos[2]);
			}
		?>
		
		var arrayDataSujetoComp = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		<?php if($investorLimit=='L' || $investorLimit=='M'){?>
			var comparable_cco='';
		<?php }?>
		var storeSujetoComp = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoComp = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			width: 'auto',
			height: 65,
			store: storeSujetoComp,
			columns: [
				<?php 
					echo "{header: 'Sta.', width: 30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			],
			listeners: {
				"rowclick": function(grid, row, e) {
					document.getElementById("<?php echo $pin;?>_0_<?php echo $pid;?>_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				}
			}
		});

		storeSujetoComp.loadData(arrayDataSujetoComp);
		if(Ext.isIE){
			gridSujetoComp.getEl().focus();
		}
	
		var storeComp = new Ext.data.Store({
			url: 'http://www.reifax.com/properties_look4comparables.php',
			baseParams: {'bd': '<?php echo $db_data;?>','prop': '<?php echo $xcode;?>','status':'CS,CC','reset':'true', 'id': '<?php echo $pid;?>', 'type': 'nobpo','sort': orderCompField, 'dir': orderCompDir, 'array_taken': "<?php echo $array_taken;?>"<?php if($loged && !$block) echo ", 'userid': ".$_COOKIE['datos_usr']['USERID'];?>},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.map.Clear();
					var arrLatLong= new Array();
					var pin = new VEPushpin(
						'<?php echo $pin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>',
						new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>),
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_0_<?php echo $data_comparado[0]->parcelid;?>_<?php echo $db_data;?>" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
						'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
						'  <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->lsqft;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
						'    <td><?php echo (float)$data_comparado[0]->bheated;?></td>'+
						'  </tr><tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
						'    <td><?php echo ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath);?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
						'    <td><?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?></td>'+
						'  </tr> <tr> '+
						'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
						'    <td>Subject</td>'+
						'  </tr></table></div>',
						'',
						'',
						''
					);
					<?php echo $map; ?>.map.ClearInfoBoxStyles();
					<?php echo $map; ?>.map.AddPushpin(pin);
					arrLatLong.push(new VELatLong(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>));
					<?php echo $map; ?>.map.SetMapView(arrLatLong);
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){

							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							getCasita(status,pendes,sold);
							pin = new VEPushpin(
								'<?php echo $pin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>',
								new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')),
								'http://www.reifax.com/img/houses/'+lsImgCss[indImgCss].img,
								data[k].get('pin_address'),
								/*'<div align=center style="float:left; padding-left:10px;"><a href=javascript:void(0) title="Photos" onclick="tabs2.setActiveTab(\'picturesTab2\');"><img id="<?php echo $imgPin;?>_'+ind+'_'+data[k].get('pid')+'_<?php echo $db_data;?>'+'" height="105px" width="135px;" src="http://www.reifax.com/img/nophotocasa.jpg"></a></div>'+*/
								'<div align=center style="float:right; padding-left:10px;"><table style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;" border="0">'+
								'  <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Gross Area:</td>'+
								'    <td>'+data[k].get('pin_lsqft')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Living Area:</td>'+
								'    <td>'+data[k].get('pin_larea')+'</td>'+
								'  </tr><tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td>'+
								'    <td>'+data[k].get('pin_bed')+'/'+data[k].get('pin_bath')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Price</td>'+
								'    <td>'+data[k].get('pin_saleprice')+'</td>'+
								'  </tr> <tr> '+
								'    <td style=" font-weight:bold; margin-right:5px;">Status:</td>'+
								'    <td>'+lsImgCss[indImgCss].explain+'</td>'+
								'  </tr> <tr> '+
								'    <td colspan=2 style=" font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" onclick="createOverview(<?php echo $county;?>,\''+data[k].get('pid')+'\',\''+status+'\','+user_web+',true);">Click here for Overview Comp</td>'+
								'  </tr></table></div>',
								'',
								'',
								''
							);
							<?php echo $map; ?>.map.ClearInfoBoxStyles();
							<?php echo $map; ?>.map.AddPushpin(pin);
							arrLatLong.push(new VELatLong(data[k].get('pin_xlat'),data[k].get('pin_xlong')));
							
							<?php if($investorLimit=='L' || $investorLimit=='M'){?>
								if(comparable_cco!='') comparable_cco+=",";
								comparable_cco+="'"+data[k].get('pid')+"'";
							<?php }?>
							
						}					
					}
					
					<?php 
					if($investorLimit=='L' || $investorLimit=='M'){
					?>
						//alert(comparable_cco);
						Ext.Ajax.request( 
						{  
							waitMsg: 'Validating...',
							url: 'properties_calculate.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: '<?php echo $data_comparado[0]->parcelid;?>',
								bd: '<?php echo $db_data;?>',
								status: 'CC',
								comparables: comparable_cco
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var results=response.responseText.split('^');
								
								marketvalue = parseFloat((results[0]).replace(',',''));
								
								arrayDataSujetoComp[0][13]=marketvalue;
								arrayDataSujetoComp[0][14]=results[1];
								if(document.getElementById('marketvalue')) document.getElementById('marketvalue').innerHTML=Ext.util.Format.usMoney(marketvalue);
								storeSujetoComp.loadData(arrayDataSujetoComp);

								if(marketvalue!=99999999 && activevalue!=99999999){
									var selectvalue=reifaxvalue;
									if(selectvalue>marketvalue && marketvalue>0) selectvalue=marketvalue;
									if(selectvalue>activevalue && activevalue>0) selectvalue=activevalue;
									document.getElementById('selectvalue').innerHTML=Ext.util.Format.usMoney(selectvalue);
									document.getElementById('percentvalue').innerHTML=percentvalue+'%';
									document.getElementById('ioffertvalue').innerHTML=Ext.util.Format.usMoney((selectvalue*(percentvalue/100)));
									if(roundTo==500)
										finalvalue = parseFloat((Math.floor((selectvalue*(percentvalue/100))/1000)+'500.00'));
									else 
										finalvalue = parseFloat(((Math.floor((selectvalue*(percentvalue/100))/1000)+1)+'000.00'));
									
									document.getElementById('finalvalue').innerHTML=Ext.util.Format.usMoney(finalvalue);
									
									if(document.getElementById('offer')){ 
										Ext.getCmp('offer').setValue(finalvalue);
									}
								}
								
								if (document.getElementById('tabs')) {
		
									if (document.getElementById('report_content').offsetHeight > tabs.getHeight()) {
										tabs.setHeight(document.getElementById('report_content').offsetHeight+800);
										viewport.setHeight(document.getElementById('report_content').offsetHeight+1100);
									}
									
								}
							}                                
						});
					<?php 
					}
					?>
					
					//repaint shape/poly on filter.
					try{
						<?php echo $map; ?>.slDrawing.DeleteShape(<?php echo $map; ?>.myCurrentShape);
					}catch (err){}
					if(<?php echo $map; ?>.filter_map && <?php echo $map; ?>._curLatLonShape.length>5){
						var _xpoints=new Array();
						for(x=0;x<<?php echo $map; ?>._curLatLonShape.split("/").length;x++)
						{								//latitude	longitude
							_xpoints.push(new VELatLong(<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[0],<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[1]));
							arrLatLong.push(new VELatLong(<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[0],<?php echo $map; ?>._curLatLonShape.split("/")[x].split(",")[1]));
							
						}
						<?php echo $map; ?>.points2shape(_xpoints);
					}
					<?php echo $map; ?>.map.SetMapView(arrLatLong);
					
					<?php if(!$print){?>
					if ( Ext.fly(Ext.fly(gridComp.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckComp=true;
						gridComp.getSelectionModel().selectAll();
						selected_dataComp=new Array();
					}else{
						AllCheckComp=false;
						var sel = [];
						if(selected_dataComp.length > 0){
							for(val in selected_dataComp){
								var ind = gridComp.getStore().find('pid',selected_dataComp[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridComp.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridComp.setHeight(alto);
					<?php }?>
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataComp = new Array();
		var AllCheckComp=false;
		
		var smComp = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataComp.indexOf(record.get('pid'))==-1)
						selected_dataComp.push(record.get('pid'));
					
					if(Ext.fly(gridComp.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckComp=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataComp = selected_dataComp.remove(record.get('pid'));
					AllCheckComp=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridComp = new Ext.grid.GridPanel({
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			store: storeComp,
			columns: [],
			<?php if(!$ocomp){?>
			sm: smComp,
			<?php }?>
			listeners: {
				"rowclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var id = record.get('status').split('_');
					var numeroPin=id[0];
					document.getElementById("<?php echo $pin;?>_"+numeroPin+"_"+record.get('pid')+"_<?php echo $db_data;?>_"+<?php echo $map; ?>.map.GUID).onmouseover();
				},
				"mouseout": function(e) {
					VEPushpin.Hide();
				},
				"rowdblclick": function(grid, row, e) {
					var record = this.store.getAt(row);
					var pid = record.get('pid');
					var status = record.get('status').split('_')[1].split('-')[0];
					createOverview(<?php echo $county;?>,pid,status,user_web,true);
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderCompField = sortInfo.field;
					orderCompDir = sortInfo.direction;
				}
			},
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitComp,
				store: storeComp,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitComp=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitComp=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitComp=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				})
				]
			})
		});
		<?php }?>
		
		storeComp.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeComp.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					columns.push(smComp);
					<?php }?>
					Ext.each(storeComp.reader.jsonData.columns, function(column){
						columns.push(column);
					});
					gridComp.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeComp.reader.jsonData.columns;
				var data = storeComp.reader.jsonData.records;
				
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
	
		<?php if(!$print || $investorLimit=='L'){?>
			storeComp.load({params:{start:0, limit:limitComp}});
		<?php }else{?>
			storeComp.load();
		<?php }?>
	
	
		<?php if(!$print){?>
		if(document.getElementById('tabs')){
			if(document.getElementById('todo_comp_panel').offsetHeight > tabs.getHeight()){
				tabs.setHeight(document.getElementById('todo_comp_panel').offsetHeight+100);
				tabs2.setHeight(tabs.getHeight());
				viewport.setHeight(tabs.getHeight());
			}
		}
		<?php }?>
	</script>
<?php
	}
	
	function overviewStreetView($pid,$lat,$long,$map){
		?>
        <div align="center" class="fondo_realtor_result_tab">
        <div align="center" style="width:100%; margin:auto; margin-top:0px;">
        <div id="<?php echo $map; ?>" style="width:99%; height:700px; border: 1px solid #4E9494; position:relative; float:center; margin-right:0px;"></div>
        </div>
        </div>
        <script>
			var ubicacion = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>);
			var panoramaOptions = {
				position: ubicacion,
				pov: {
					heading: 180,
					pitch: 0,
					zoom: 0
			    }
			};
			var panorama = new  google.maps.StreetViewPanorama(document.getElementById("<?php echo $map; ?>"),panoramaOptions);
		</script>
        <?php
	}
?>