<?php
include('properties/conexion.php');

function url_exists($url)
{
	$url_info = parse_url($url);
	
	if (! isset($url_info['host']))
		return false;
	
	$port = (isset($url_info['post'])?$url_info['port']:80);
	
	if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr)) 
		return false;
	
	$uri = @$url_info['path'] . '?' . @$url_info['query'];
	
	$http = "HEAD $uri HTTP/1.1\r\n";
	$http .= "Host: {$url_info['host']}\r\n";
	$http .= "Connection: close\r\n\r\n";
	
	@fwrite($hwnd, $http);
	
	$response = fgets($hwnd);
	$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";
	
	fclose($hwnd);
	
	if (preg_match($response_match, $response, $matches)) {
		//print_r($matches);
		if ($matches[1] == 404)
			return false;
		else if ($matches[1] == 200)
			return true;
		else
			return false;
		 
	} else {
		return false;
	}
}

$county='Broward';
$bd_county='flbroward';
mysql_select_db($bd_county) or die('Could not select database');
$link='http://206.51.228.161/imagenes/';
$d_base='C:/servidor/htdocs/imagenes/';


$query='SELECT p.state,p.county,p.city,p.zip,p.address,i.*  
FROM imagenes i
INNER JOIN properties_php p ON (i.parcelid=p.parcelid)
INNER JOIN psummary ps ON (i.parcelid=ps.parcelid)
ORDER BY p.state,p.county,p.city,p.zip,p.address,i.mlnumber';
$result=mysql_query($query) or die($query.mysql_error());

$num=1;

echo 'Inicio: '.date('Ymd H:i:s');

while($r=mysql_fetch_array($result)){
	$state=str_replace(' ','_',trim($r['state']));
	$county=str_replace(' ','_',trim($r['county']));
	$city=str_replace(' ','_',trim($r['city']));
	$zip=str_replace(' ','_',trim($r['zip']));
	$address=str_replace(' ','_',trim($r['address']));
	$mln=str_replace(' ','_',trim($r['mlnumber']));
	$sub1Mlnumber=substr(str_replace(' ','_',trim($r['mlnumber'])),0,1);
	$subMlnumber=substr(str_replace(' ','_',trim($r['mlnumber'])),1,4);
	$parcelid=trim($r['ParcelID']);
	
	
	if(!file_exists($d_base)) mkdir($d_base);
	$dir=$d_base.$state;
	$url=$link.$state;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$county;
	$url.='/'.$county;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$city;
	$url.='/'.$city;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$zip;
	$url.='/'.$zip;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$sub1Mlnumber;
	$url.='/'.$sub1Mlnumber;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$subMlnumber;
	$url.='/'.$subMlnumber;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$address;
	$url.='/'.$address;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/'.$mln;
	$url.='/'.$mln;
	if(!file_exists($dir)) mkdir($dir);
	$dir.='/';
	$url.='/';
	
	
	if($r["letra"]=='Y')
		$mlnumber=$r["mlnumber"];
	else
		$mlnumber=substr($r["mlnumber"],1);
		
	if(url_exists($r['url'].$r['url2'].$mlnumber.".".$r['tipo'])){
		
		$imagen_data=file_get_contents($r['url'].$r['url2'].$mlnumber.".".$r['tipo']);
		$f = fopen($dir.$parcelid.'.jpg','w+');
		$url.=$parcelid.'.jpg';
		
		fwrite($f,$imagen_data);
		fclose($f);
		
		if($r['nphotos']>1){	
			for($i=$r['inicount']; $i<=$r['nphotos']; $i++){
				$imagen_data=file_get_contents($r["url"].$r['url3'].$mlnumber.$r['sep'].$i.".".$r['tipo']);
				$f = fopen($dir.$parcelid.'_'.$i.'.jpg','w+');
				fwrite($f,$imagen_data);
				fclose($f);
			}
		}
		echo $num.'.- Parcelid: '.$parcelid.' url: '.$url.'<br>';
	}else{
		echo $num.'.- Parcelid: '.$parcelid.' no existe.<br>';
	}
	$num++;
}

echo 'Fin: '.date('Ymd H:i:s');
?>