// JavaScript Document
var arrinpbetween=new Array();
var systemwhat;
var systemsearch='basic';
var ximausa_login_name= Ext.util.Cookies.get("ximausa_login_name");
var ximausa_login_pass= Ext.util.Cookies.get("ximausa_login_pass");
if(Ext.isEmpty(ximausa_login_name)){
	ximausa_login_name='';
	ximausa_login_pass=''; 
}

var login_win=new Ext.Window({
	 width:435,
	 height:245,
	 resizable: false,
	 modal: true,
	 border:false,
	 closable:false,
	 plain: true,
	 listeners: {beforeshow: function(win){document.getElementById('login_error_msg').style.display='none'; win.center();}},
	 html:'<div style="background: url(../../../../../img/bkgd_login.jpg) repeat-x #95bed4; width:425px; height:255px; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;">	<div style="color:#FFF;font-size:18px;font-weight:bold;padding:0 10px 5px; height:30px;text-align:left;"><div style="float:left; width:90%;">Log In</div><div align="right" style="float:right; width:10%; padding-top:3px;"><img src="../../../../../img/close.gif" style="cursor:pointer;" onClick="login_win.hide();"></div></div><div style="border: #237aa7 solid 1px; background:#fff;font-size:12px;line-height:14px;margin:0px 10px;padding:15px; color:#000; height:145px;"><form action="go.php" method="post" name="ximausa_login"><div style="padding-bottom:10px; font-size:11px; text-align:left;"><label style="float:left;font-weight:bold;padding:5px 0 0;width:120px;vertical-align:middle;">Email:</label><input value="'+ximausa_login_name+'" type="text" name="user" id="user" style="width:230px;font-size:11px;vertical-align:middle;"></div><div style="padding-bottom:10px; font-size:11px; text-align:left;"><label style="float:left;font-weight:bold;padding:5px 0 0;width:120px;">Password:</label><input type="password" name="pass" id="pass" value="'+ximausa_login_pass+'" style="width:230px;font-size:11px;vertical-align:middle;"></div><div id="login_error_msg" align="center" style="padding-bottom:0px; border:none; margin:0px; font-weight:bold; color:#F00; font-size:10px; display:none;"></div><div align="center" style="font-weight:bold;font-size:11px;"><input name="login_guardar" id="login_guardar" type="checkbox" value="true" checked style=" margin-bottom:0px; height: 12px;"> Remember Me</div><div align="center" style="padding-bottom:15px;border:none; margin:0px; font-weight:bold; color:#237aa7;font-size:11px;"><span style="color:#333;">Not registered?.</span> <a href="register.php">Register Now! </a><span style="color:#333;">||</span><a href="forgot_pass.php" target="_blank" onClick="window.open(this.href,this.target,\'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no resizable=no,copyhistory=no,height=250,width=350,left=505,top=380\'); return false;">Forgot Password</a></div><div align="right"> <a href="javascript:void(0);" onClick="systemwhat=1;check_contract(\'check\');return false;" ><img  border=0 src="../../../../../img/blogin3.png" value="Multifamiliar - Comercial"></a></div></form></div></div>'
});

var loading_win=new Ext.Window({
     width:170,
     autoHeight: true,
     resizable: false,
     modal: true,
     border:false,
     closable:false,
     plain: true,
     html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="../../../../../img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
});

var contract_win=new Ext.Window({
	 width:650,
	 height:400,
	 resizable: false,
	 modal: true,
	 border:false,
	 closable:true,
	 closeAction: 'hide',
	 plain: true,
	 listeners: { 
	 	'beforehide': function (panel){
			loading_win.hide();
		}
	 },
	 html:'<div style="background: url(../../../../../img/bkgd_login.jpg) repeat-x #95bed4; width:638px; height:500px; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;">	<div style="color:#FFF;font-size:18px;font-weight:bold;padding:0 10px 5px; height:30px;text-align:left;">    	<div style="float:left; width:90%;">Read Terms and Conditions</div></div><div style="border: #237aa7 solid 1px; background:#fff;font-size:12px;line-height:14px;margin:0px 10px;padding:15px; color:#000; height:260px;"><div style="padding-bottom:10px; font-size:11px; text-align: justify;"><label style="padding:0;width:230px;vertical-align:middle;">Because of recent upgrades to our system and the future nationwide coverage of our service, we decided to choose a more appropriate domain name for the system.  The new domain name <span style="color:#F00;">"REIFAX.com"</span> <span style="font-weight:bold;">(Real Estate Investment Property FAX)</span> better exemplifies the commitment we have to providing timely and factual information to help our users in making critical investment decisions.  The new name further shows our continued commitment to expanding services and the data delivery we are now providing to our clients.<br><br>Just as recent examples of some of these changes, our <span style="font-weight:bold; color:#F00;">Platinum Version</span> now includes the following:<br>1. New advanced search parameters to find prospects more quickly.<br>2. Probate listings &shy; no more going to the court and searching endless reams of documents.<br>3. FAR/BAR Contracts that auto-fill with the information from the public record.<br>4. E-signatures for these Contracts <span style="color:#F00;">(coming soon)</span>.<br>5. Sorting for <span style=" font-weight:bold;">"Cash Buyers"</span> to find these elusive buyers <span style="color:#F00;">(coming soon)</span>.<br>6. And many more tools to simplify finding prospective sellers and buyers.<br><br>This Domain Change Wont\' affect Your current price, plan, billing cycle or affiliated marketing program, all data will be updated only in the new domain <a href="index.php" target="_blank" >www.reifax.com</a><br><br>To be able to login you have to accept this<a href="http://www.reifax.com/TermsAndConditions.pdf" style="padding-left:10px;" target="_blank" >Read Terms and Conditions</a>. After accepting, please save the new page in your preferences.</label></div><br><div><a href="javascript:void(0);" onClick="check_contract(\'update\'); return false;" ><img  border=0 src="../../../../../img/accept.png" value="Accept"></a></div></div>'
});

var lsImgCss={
'A-F':{'img':'verdel.png','explain':'Active Foreclosed'},
'A-F-S':{'img':'verdel_s.png','explain':'Active Foreclosed Sold'},
'A-P':{'img':'verdel.png','explain':'Active Pre-Foreclosed'},
'A-P-S':{'img':'verdel_s.png','explain':'Active Pre-Foreclosed Sold'},	
'A-N':{'img':'verdeb.png','explain':'Active'},
'A-N-S':{'img':'verdeb_s.png','explain':'Active Sold'},
'P-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'P-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'P-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'P-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'P-N': {'img':'grisb.png','explain':'Non-Active'},
'P-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'CC-F': {'img':'cielol.png','explain':'By Owner Foreclosed'},
'CC-F-S': {'img':'cielol_s.png','explain':'By Owner Foreclosed Sold'},
'CC-P': {'img':'cielol.png','explain':'By Owner Pre-Foreclosed'},
'CC-P-S': {'img':'cielol_s.png','explain':'By Owner Pre-Foreclosed Sold'},
'CC-N': {'img':'cielo.png','explain':'By Owner'},
'CC-N-S': {'img':'cielo_s.png','explain':'By Owner Sold'},
'CS-P': {'img':'marronl.png','explain':'Closed Sale Pre-Foreclosed'},
'CS-P-S': {'img':'marronl_s.png','explain':'Closed Sale Pre-Foreclosed Sold'},
'CS-F': {'img':'marronl.png','explain':'Closed Sale Foreclosed'},
'CS-F-S': {'img':'marronl_s.png','explain':'Closed Sale Foreclosed Sold'},
'CS-N': {'img':'marronb.png','explain':'Closed Sale'},
'CS-N-S': {'img':'marronb_s.png','explain':'Closed Sale Sold'},
'PS-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'PS-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'PS-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'PS-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'PS-N': {'img':'grisb.png','explain':'Non-Active'},
'PS-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'SP-': {'img':'grisdiamante.png','explain':'Unknow'},
'T-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'T-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'T-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'T-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'T-N': {'img':'grisb.png','explain':'Non-Active'},
'T-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'R-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'R-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'R-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'R-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'R-N': {'img':'grisb.png','explain':'Non-Active'},
'R-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'E-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'E-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'E-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'E-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'E-N': {'img':'grisb.png','explain':'Non-Active'},
'E-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'C-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'C-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'C-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'C-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'C-N': {'img':'grisb.png','explain':'Non-Active'},
'C-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'Q-N': {'img':'grisb.png','explain':'Non-Active'},
'Q-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'Q-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'Q-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'Q-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'Q-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'W-N': {'img':'grisb.png','explain':'Non-Active'},
'W-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'W-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'W-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'W-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'W-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'X-N': {'img':'grisb.png','explain':'Non-Active'},
'X-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'X-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'X-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'X-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'X-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'SUBJECT': {'img':'verdetotal.png','explain':'Subject'},
'USER_CAR': {'img':'xxima.png','explain':'User'},
'B-N': {'img':'grisb.png','explain':'Non-Active'},
'B-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'B-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'B-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'B-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'B-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'},
'N-N': {'img':'grisb.png','explain':'Non-Active'},
'N-N-S': {'img':'grisb_s.png','explain':'Non-Active Sold'},
'N-P': {'img':'grisl.png','explain':'Non-Active Pre-Foreclosed'},
'N-P-S': {'img':'grisl_s.png','explain':'Non-Active Pre-Foreclosed Sold'},
'N-F': {'img':'grisl.png','explain':'Non-Active Foreclosed'},
'N-F-S': {'img':'grisl_s.png','explain':'Non-Active Foreclosed Sold'}};
var indImgCss='';

function getCasita(status,pendes,sold)
{
	if(pendes=='L') pendes='N';
	if(sold=='S') sold='-S'; else sold='';
	switch (status.toUpperCase())
	{
		case "A":
			indImgCss="A-"+pendes+sold;					
			break;
		case "P":
			indImgCss="P-"+pendes+sold;
			break;
		case "CC":				
			indImgCss="CC-"+pendes+sold;
			break;
		case "CS":				
			indImgCss="CS-"+pendes+sold;
			break;
		case "PS":
			indImgCss="PS-"+pendes+sold;
			break;
		case "SP":
			indImgCss="SP-"+pendes+sold;
			break;
		case "T":
			indImgCss="T-"+pendes+sold;
			break;
		case "R":
			indImgCss="R-"+pendes+sold;
			break;
		case "E":
			indImgCss="E-"+pendes+sold;
			break;
		case "C":
			indImgCss="C-"+pendes+sold;
			break;
		case "Q":
			indImgCss="Q-"+pendes+sold;
			break;
		case "R":
			indImgCss="R-"+pendes+sold;
			break;
		case "T":
			indImgCss="T-"+pendes+sold;
			break;
		case "W":
			indImgCss="W-"+pendes+sold;
			break;
		case "X":
			indImgCss="X-"+pendes+sold;
			break;
		case "B":
			indImgCss="B-"+pendes+sold;
			break;
		case "N":
			indImgCss="N-"+pendes+sold;
			break;
		case "SUBJECT":
			indImgCss="SUBJECT";break
	}
}

function formatCurrency(num,dec_if) 
{
	if(num || num==0 || num=='0')
	{
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		if(dec_if)
			return (((sign)?'':'-') + num + '.' + cents);
		else
			return (((sign)?'':'-') + num);
	}
}


function imagen_grid(status,pendes){
	if(status=='CS'){
		if(pendes!='N')
			return 'marronl.png';
		else
			return 'marronb.png';
	}else{
		if(pendes!='N')
			return 'cielol.png';
		else
			return 'cielo.png';
	}
}

function OnObliqueEnterHandler()
{
	if(mapOverview.IsBirdseyeAvailable()){
		mapOverview.SetBirdseyeScene(SpaceNeedle,VEOrientation.North,1);
	}else{
		mapOverview.SetMapStyle(VEMapStyle.Aerial);
	}
}
	 
function change_value_img(slider,val){
	document.getElementById('img_data').src=img_src[val];
}

function check_contract(action){
	loading_win.show();
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_contract.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			user: document.getElementById('user').value,
			pass: document.getElementById('pass').value,
			action: action
		},
		
		failure:function(response,options){
			loading_win.hide();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText;
			//alert(results);
			if(results=='go'){
				contract_win.hide();
				ajax_submit();
			}else{ 
				if(results=='error'){
					document.getElementById('login_error_msg').innerHTML='Your email or password is incorrect!';
					document.getElementById('login_error_msg').style.display='';
					loading_win.hide();
				}else{
					contract_win.show();
				}
			}
			
			return false;
		}                                
	});
}

function ajax_submit(){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_validacion.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			user: document.getElementById('user').value,
			pass: document.getElementById('pass').value,
			check: document.getElementById('login_guardar').checked, 
			systemwhat: systemwhat
		},
		
		failure:function(response,options){
			loading_win.hide();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText.split('^');
			//alert(results);
			if(results[0]==true || results[0]=='true')
			{
				if(results[3]==2 || results[3]==5 || results[3]==6 || results[3]==7)
					login_menu(results[1],results[2],false);
				else
				{
					login_menu(results[1],results[2],true);	
					document.getElementById('login_error_msg').style.display='none';					
				}
			}
			else
			{
				if(results[1]=='logeado'){
					if(confirm("You did not log off or have another sesion opened. To continue and close the other sesion click 'OK'.")){
						session_release_with_data(document.getElementById('user').value,document.getElementById('pass').value);
					}
				}else{
					alert(results[1]);
					document.location='http://www.reifax.com';
				}
				loading_win.hide();
			}
			
			
			return false;
		}                                
	});
}

function ajax_submit_loged(userid){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_validacion.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			userid: userid,
			systemwhat: systemwhat
		},
		
		failure:function(response,options){
			loading_win.hide();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText.split('^');
           //alert(results);  
            			  
			if(results[0]==true || results[0]=='true')
			{    
				if(results[3]==2 || results[3]==5 || results[3]==6 || results[3]==7)
					login_menu(results[1],results[2],false);
				else{
					login_menu(results[1],results[2],true);
					if(document.getElementById('login_error_msg')) document.getElementById('login_error_msg').style.display='none';
				}
				
			}
			else
			{
				if(results[1]=='logeado'){
					if(confirm("You did not log off or have another sesion opened. To continue and close the other sesion click 'OK'.")){
						session_release_with_data_id(userid);
					}
				}else{
					alert(results[1]);
					document.location='http://www.reifax.com';
				}
				loading_win.hide();
			}
			
			
			return false;
		}                                
	});
}

function login_menu(name,useridlog,block){
	user_loged=true;
	useridlogin=useridlog;
	user_name_menu=name;
	//alert('Welcome Buyer`s Pro'); 
	
	/*var west = Ext.getCmp('west_panel');
	var east = Ext.getCmp('east_panel');
	var south = Ext.getCmp('south_panel');
	
	west.collapse();
	east.collapse();
	south.collapse();

	tabs.setWidth(system_width);
	//tabs.setHeight(580);
	//viewport.setHeight(650);
	if(document.getElementById('image_carousel')) document.getElementById('image_carousel').style.display = 'none';
	document.getElementById('layout_center').style.width= system_width;*/
			
	if(document.getElementById('top_m_reg')) document.getElementById('top_m_reg').style.display='none';
	if(document.getElementById('top_m_login')) document.getElementById('top_m_login').style.display='none';
	
	if(document.getElementById('top_m_user_name')) document.getElementById('top_m_user_name').innerHTML=name+' ('+useridlog+')';
	if(document.getElementById('top_m_user')) document.getElementById('top_m_user').style.display='';
	if(document.getElementById('top_m_training')) document.getElementById('top_m_training').style.display='';
	var tab = tabs.getItem('myaccountTab');
	if(tab)
		if(document.getElementById('top_m_logout')) document.getElementById('top_m_logout').style.display='';
	
	loading_win.hide(); 
	login_win.hide();
	//restarTabs(block);
}

function restarTabs(block){
	tabs.removeAll();
	
	search_type='PR';
	search_by_type='LOCATION';
	master_web=false;
	
	if(block){
	tabs.add({
		title: ' Search ',
		width: 660,
		id: 'searchTab',
		autoLoad: {
			url: 'searchs_types/basic_search.php', 
			timeout: 10800, 
			scripts: true, 
			params: {
				userweb:user_web, 
				search_type: search_type, 
				search_by_type: search_by_type, 
				state_search: search_state, 
				county_search: search_county, 
				masterweb:master_web
			}
		},
		
		tbar: new Ext.Toolbar({
			cls: 'no-border-search',
			width: 'auto',
			items: [' ',{
					tooltip: 'Click to Search by Map',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/map.png',
					handler: function (){
						if(document.getElementById("mapSearch").style.display=='none'){
							doSearchByFilter('MAP');
							document.getElementById(search_type+'_combo_search_by').value = 'MAP';
						}else{
							doSearchByFilter('MAP_OFF');
							document.getElementById(search_type+'_combo_search_by').value = 'LOCATION';
						}
					}
				},{
					tooltip: 'Click to Manage Search',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
					handler: function(){
						ShowSavedSearch();
					}
				},{
					tooltip: 'Click to find your Location',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/location.png',
					handler: function(){
						if(search_by_type!='GPS'){
							var formlocation = new Ext.FormPanel({
								frame:true,
								title: 'Get Location',
								width: 400,
								waitMsgTarget : 'Saving Documents...',
								items: [{
									xtype		:'combo',
									name		:'distance',
									id			:'distance',
									hiddenName	:'idtype',
									store		:new Ext.data.SimpleStore({
										fields	:['iddisc'],
										data 	: [['0.1'],['0.2'],['0.3'],['0.4'],['0.5'],['0.6'],['0.7'],['0.8'],['0.9'],['1'],['1.1'],['1.2'],['1.3'],['1.4'],['1.5'],['1.6'],['1.7'],['1.8'],['1.9'],['2']]
									}),
									editable	: false,
									displayField:'iddisc',
									valueField	:'iddisc',
									typeAhead	:true,
									fieldLabel	:'Distance Miles',
									mode		:'local',
									triggerAction: 'all',
									emptyText	:'Select ...',
									selectOnFocus:true,
									allowBlank	:false,
									value		:'0.5',
									width		:100
								},{
									xtype: 'hidden',
									id: 'tipomap',
									value:'Basic'
								}],
								buttons: [{
									text: 'Get',
									handler: getLocationGps
								},{
									text: 'Cancel',
									handler  : function(){
										win.close();
									}
								}]
							});
							
							var win = new Ext.Window({
								layout      : 'fit',
								id: 'ventanalocation',
								width       : 400,
								height      : 170,
								modal	 	: true,
								plain       : true,
								items		: formlocation,
								closeAction : 'hide'
							});
							
							win.show();
						}else{
							doSearchByFilter('LOCATION');	
						}
					}
				},'->'
				,{
					iconCls:'icon',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/reset.png',
					text: 'Reset',
					handler  : function(){
						document.getElementById('pformul').reset();
						if(mapSearch!=null) mapSearch.cleaner();				
					}
				}/*,
				' '
				,{
					iconCls:'icon',
					iconAlign: 'top',
					icon: 'http://www.reifax.com/img/toolbar/search.png',
					scale: 'medium',
					text: 'Search',
					width: 60, 
					height: 70,
					handler  : function(){
						searchForm(search_type+'_search','search');
					}				
				}*/, 
				' '
				,{
					iconCls:'icon',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/mortgage.png',
					text: 'Mortgage',
					handler  : function(){
						searchForm(search_type+'_search','mortgage');
					}				
				}, 
				' '
				,{
					iconCls:'icon',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/foreclosure.png',
					text: 'Foreclosures',
					handler  : function(){
						searchForm(search_type+'_search','foreclosures');
					}				
				}, 
				' '
				,{
					iconCls:'icon',
					cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/bankowned.png',
					text: 'Bank<br>Owned',
					handler  : function(){
						searchForm(search_type+'_search','bank');
					}				
				}]
		})
		
	});	

	tabs.setActiveTab(0); 
	}else{
		tabs.setActiveTab(0);
	}
	 
}

function logout_menu(){
	user_loged=false;
	
	if(document.getElementById('top_m_reg')) document.getElementById('top_m_reg').style.display='';
	if(document.getElementById('top_m_login')) document.getElementById('top_m_login').style.display='';
	if(document.getElementById('top_m_user')) document.getElementById('top_m_user').style.display='none';
	if(document.getElementById('top_m_logout')) document.getElementById('top_m_logout').style.display='none';
	if(document.getElementById('image_carousel')) document.getElementById('image_carousel').style.display = '';
	
	/*var west = Ext.getCmp('west_panel');
	var east = Ext.getCmp('east_panel');
	var south = Ext.getCmp('south_panel');
	
	if(document.body.offsetWidth >= 1200){
		west.expand(false);
	}
	east.expand(false);
	south.expand(false);

	tabs.setWidth(660,2350);
	/*tabs.setHeight(1300);
	viewport.setHeight(1500);*/
}

function ajax_submit_advertising(){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_validacion.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			user: document.getElementById('advertising_user').value,
			pass: document.getElementById('advertising_pass').value	
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText;
			//alert(results);
			eval(results);
			return false;
		}                                
	});
}

function session_release(){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_releaseSess.php', 
		method: 'POST',
		timeout :600000,
		params: { 
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText;
			if(user_loged) 	location.href='properties_search.php';
			else logout_menu();
			return false;
		}                                
	});
}

function session_release_with_data_id(userid){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_releaseSess.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			userid: userid
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			ajax_submit_loged(userid);
		}                                
	});
}

function session_release_with_data(user,pass){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_releaseSess.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			user: user,
			pass: pass
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			ajax_submit();
		}                                
	});
}

function valid_session(){
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_validSess.php', 
		method: 'POST',
		timeout :600000,
		params: { 
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			var results=response.responseText;
			//alert(results);
			if(results=='invalid'){
				Ext.MessageBox.alert('Warning','This sesion was closed as another became active.');
				session_release();
			}
		}                                
	});
}

function PlansAndPricing(){
	window.scrollTo(0,0);
	
	if(document.getElementById('PlansAndPricing')){
		var tab = tabs.getItem('PlansAndPricing');
		tabs.remove(tab);
	}
	
	tabs.add({
		title: ' Plans &amp; Pricing ',
		id: 'PlansAndPricing',
		autoLoad: {
			url: 'http://www.reifax.com/Pricing/index.php',
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true
		},
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('PlansAndPricing');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

/*function Register(){
	window.scrollTo(0,0);
	
	if(document.getElementById('RegisterTab')){
		var tab = tabs.getItem('RegisterTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title: ' Register ',
		id: 'RegisterTab',
		autoLoad: {
			url: 'http://www.reifax.com/xima3/register_tabs/register.php',
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true
		},
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('RegisterTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}*/

function AboutUs(){
	window.scrollTo(0,0);
	
	if(document.getElementById('AboutUs')){
		var tab = tabs.getItem('AboutUs');
		tabs.remove(tab);
	}
	
	var cade=	'<table width="650px" border="0" cellspacing="0" cellpadding="0" style="margin:auto;"><tr><td colspan="2" align="center"><font style="font-family:Trebuchet MS,Arial,Helvetica,sans-serif"; color="#1E90FF" size="4">REIFAX - Your source for foreclosed homes, foreclosure listings, REO, bank owned properties<br /></font></td></tr><tr><td colspan="2" align="center"><div style="width:90%;"><br /><p class="style9" align="justify"><font face="Arial" color="#003366" size="2"><strong><a href="http://www.reifax.com/" style="text-decoration: none; font-weight:bold;">REIFAX</a></strong> provides the best and most accurate foreclosure and pre-foreclosure information, data, and statistics available, making it a valuable resource to invest wisely. This Real Estate tool was designed mainly for brokers and investors with the need to identify the best investment properties. </font> </p><p class="style9" align="justify"><font face="Arial" color="#003366" size="2"><strong><a href="http://www.reifax.com/" style="text-decoration: none; font-weight:bold;">REIFAX</a></strong> is your one-stop destination to search for properties with over 30% equity. Search for foreclosed homes, short sales, pre-foreclosures or distressed homes. You can easily create comprehensive property comparison reports in specific areas, giving you a very powerful tool when making or negotiating an offer. </font></p><p class="style9" align="justify"><font face="Arial" color="#003366" size="2">All this information is collected from many different sources, but it is presented to you in one place, and in a very easy format. This will give you the ability to get Multi-Listing information, public records, mortgage, pre-foreclosure and foreclosure details; you can get comparables of active listings, closed sales and rentals. Best of all, you can customize and print any of such reports, mailing lists, and labels for easy mailings. </font></p><p class="style9" align="center"><font face="Arial" color="#003366" size="2">We invite you to login at <a href="http://www.reifax.com/" style="text-decoration: none">www.reifax.com</a></font></p><p class="style9" align="justify"><font face="Arial" color="#003366" size="2">We will be glad to schedule a presentation for you and  your agents at your convenience. </font> </p><p class="style11" align="justify"></p>&nbsp;</font></p><h2 align="justify"><font face="Arial" color="#003366" size="2"><strong><a href="http://www.reifax.com/" style="text-decoration: none; font-weight:bold;">REIFAX</a> </strong>is the ultimate resource for:</font> </h2><h2 align="left"><span style="font-weight: 100"><font color="#003366" size="2">Foreclosures </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Bank-owned properties </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Short sales </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Pre-Foreclosures </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Distressed property </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Homes with more than 30%, 40% or 50% equity </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Condos with more than 30%, 40% or 50% equity </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Foreclosure listings </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Foreclosure information, foreclosure data, foreclosure statistics </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Property public records </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Active MLS Listings </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Closed sales information </font></span><br/><span style="font-weight: 100"><font color="#003366" size="2">Rental properties information </font></span></h2></font></div></td></tr></table>';
	
	var aboutus = new Ext.FormPanel({
		url :'contactussendmail.php',
		labelAlign: 'top',
		frame:true,
		bodyStyle:'padding:5px 5px 0;text-align: left;font-size: 16px;font-family: Trebuchet MS,Arial,Helvetica,sans-serif; margin: auto;  ',
		waitMsgTarget: true,
		items: [{
			xtype: 'box',
			autoEl:{
				html:  cade  
			}
		}]
	});
	
	tabs.add({
		title: ' About Us ',
		id: 'AboutUs',
		items: aboutus,
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('AboutUs');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function ContactUs(){
	window.scrollTo(0,0);
	
	if(document.getElementById('ContactUs')){
		var tab = tabs.getItem('ContactUs');
		tabs.remove(tab);
	}
	
	var cade=	'<h2 style="font-size: 10px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" color="black" size="3px">'+
	'<p>Please call us with your questions and/or concerns during our business hours</p></font></h2>'+
	'<h2 style="font-size: 10px;"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;"  color="#05587F" size="3px">1 (888) 349 5368</font></h2> '+
	'<h2 style="font-size: 10px;"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;"  color="#05587F" size="3px">Fax us at: 1 (888) 376 9274</font></h2>'+
	'<p>&nbsp;</p>'+
	'<h2 style="font-size: 10px;"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;"  color="#05587F" size="3px">We are located at:</font></h2>'+
	'<h2 style="font-size: 10px;"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;"  color="black" size="3px"><p>18459&nbsp; Pines Blvd </p>'+
	'<p>Pembroke Pines, FL 33029 Suite 139</p></font></h2>'+
	'<p>&nbsp;</p>';
	
	var contactus = new Ext.FormPanel({
       	url :'contactussendmail.php',
        labelAlign: 'top',
        frame:true,
        bodyStyle:'padding:5px 5px 0;text-align: left;font-size: 16px;font-family: Trebuchet MS,Arial,Helvetica,sans-serif;  ',
        waitMsgTarget: true,
        items: [{
			xtype: 'box',
			autoEl:{html: cade }
		},{
            layout:'column',
            items:[{
                columnWidth:.5,
                layout: 'form',
                items: [{
					xtype:'textfield',
					anchor:'95%',
					id:'nombre',
					fieldLabel:'<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">Name</font></span>',
					name:'nombre',
					allowBlank: false
				},{
					xtype:'textfield',
					anchor:'95%',
					id:'correo',
					fieldLabel: '<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">Email</font></span>',
					name:'correo',
					vtype:'email',
					allowBlank: false
			   },{
					xtype:'textfield',
					anchor:'95%',
					id:'empresa', 
					fieldLabel: '<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">Company</font></span>',
					name:'empresa'
				},{
					xtype:'textfield',
					anchor:'95%',
					xtype:'numberfield', 
					id:'telefono',
					fieldLabel: '<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">Phone</font></span>',
					name:'telefono'
				},{
					xtype:'textfield',
					anchor:'95%',
					id:'ciudad',
					fieldLabel: '<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">City / County</font></span>',
					name:'ciudad'
				}/*,{
					xtype: 'hidden',
					name: 'email_user',
					hiddenName: 'email_user',
					value: '<?php echo $email_user; ?>'
				}*/
				]
            },{
                columnWidth:.5,
                layout: 'form',
                items: [{
					id:'comentario',
					fieldLabel:'<span style="font-size: 16px;font-weight:bold"><font style="font-family: Trebuchet MS,Arial,Helvetica,sans-serif;" size="3px">Your Message</font></span>',
					xtype:'textarea',
					anchor:'95%',
					height: 250,
					name:'comentario',
					allowBlank: false
				}]
            }]
        }],
		buttons:[{
        	text:'Send', 
			handler  : function(){
				contactus.getForm().submit({
					waitMsg: "Sending email...",
					success     :function(form, action){ 
                        obj = Ext.util.JSON.decode(action.response.responseText);
                        Ext.Msg.alert("Success", obj.errors.reason);
                        contactus.getForm().reset();
					},
                   	failure     :function(form, action) {
                        obj = Ext.util.JSON.decode(action.response.responseText);                                                        
                  		Ext.Msg.alert("Failure", obj.errors.reason);
					}
				});
			}
		}]
    });
	
	tabs.add({
		title: ' Contact Us ',
		id: 'ContactUs',
		items: contactus,
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('ContactUs');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function createResult(user_web,realtor_block){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultTab')){
		var tab = tabs.getItem('resultTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title		: ' Result ',
		id			: 'resultTab',		
		autoLoad: {
			url			: 'result_tabs/properties_advance_result.php', 
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true, 
			params		: {
				systemsearch: systemsearch
			}
		},
		closable: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function createResultBuyers(){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultBuyersTab')){
		var tab = tabs.getItem('resultBuyersTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title		: ' Buyers ',
		id			: 'resultBuyersTab',
		autoLoad	: {
			url			: 'properties_result_buyers.php', 
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true, 
			params		: {
				systemsearch: systemsearch
			}
		},
		closable	: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultBuyersTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function createResultMortgage(){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultMortgageTab')){
		var tab = tabs.getItem('resultMortgageTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title		: ' Mortgage ',
		id			: 'resultMortgageTab',
		autoLoad	: {
			url			: 'properties_result_mortgage.php', 
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true, 
			params		: {
				systemsearch: systemsearch
			}
		},
		closable	: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultMortgageTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function createResultForeclosures(){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultForeclosuresTab')){
		var tab = tabs.getItem('resultForeclosuresTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title		: ' Foreclosures ',
		id			: 'resultForeclosuresTab',
		autoLoad	: {
			url			: 'properties_result_foreclosures.php', 
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true, 
			params		: {
				systemsearch: systemsearch
			}
		},
		closable	: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultForeclosuresTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function createResultBankOwned(){
	window.scrollTo(0,0);
	
	if(document.getElementById('resultBankOwnedTab')){
		var tab = tabs.getItem('resultBankOwnedTab');
		tabs.remove(tab);
	}
	
	tabs.add({
		title		: ' Bank Owned ',
		id			: 'resultBankOwnedTab',
		autoLoad	: {
			url			: 'properties_result_bank_owned.php', 
			timeout		: 10800, 
			scripts		: true, 
			discardUrl	: true, 
			nocache		: true, 
			params		: {
				systemsearch: systemsearch
			}
		},
		closable	: true
	}).show();
	
	if(Ext.isIE){
		var tab = tabs.getItem('resultBankOwnedTab');
		tabs.setActiveTab(tab);
		tab.getEl().repaint();
	}
}

function colocarDefault(id,val){
	if(val=='Address, City or Zip Code' || val=='Mlnumber' || val=='Parcelid'){
		document.getElementById(id).value='';
	}
}

function colocarDefault2(id,val){
	if(val==''){
		if(search_by_type=='LOCATION' || search_by_type=='MAP')
			document.getElementById(id).value='Address, City or Zip Code';
		if(search_by_type=='PARCELID')
			document.getElementById(id).value='Parcelid';
		if(search_by_type=='MLNUMBER')
			document.getElementById(id).value='Mlnumber';
		if(search_by_type=='CASE')
			document.getElementById(id).value='Case-Number';
	}
}


function cambiarDefault(id,val){
	document.getElementById(id).value=val;
	if(val=='Address, City or Zip Code')
		document.getElementById(search_type+'_tsearch_l').innerHTML='Location';
	if(val=='Mlnumber')
		document.getElementById(search_type+'_tsearch_l').innerHTML='Mlnumber';
	if(val=='Parcelid')
		document.getElementById(search_type+'_tsearch_l').innerHTML='Parcelid';
	if(val=='Case-Number')
		document.getElementById(search_type+'_tsearch_l').innerHTML='Case-Number';
	
}

function OcultarMostrar(val){
	var equity='Debt Equity';
	if(search_type=='FS') equity='Potential Equity';
	
	if(val==true){
		if(document.getElementById(search_type+'_tproptype'))document.getElementById(search_type+'_tproptype').innerHTML='Property Type';
		if(document.getElementById(search_type+'_tpropfore'))document.getElementById(search_type+'_tpropfore').innerHTML='Foreclosure Status';
		if(document.getElementById(search_type+'_proptype'))document.getElementById(search_type+'_proptype').style.display='';
		if(document.getElementById(search_type+'_pendes'))document.getElementById(search_type+'_pendes').style.display='';
		if(document.getElementById(search_type+'_tprice'))document.getElementById(search_type+'_tprice').innerHTML='Price Range';
		if(document.getElementById(search_type+'_tbeds'))document.getElementById(search_type+'_tbeds').innerHTML='Beds';
		if(document.getElementById(search_type+'_tbath'))document.getElementById(search_type+'_tbath').innerHTML='Baths';
		if(document.getElementById(search_type+'_tsqft'))document.getElementById(search_type+'_tsqft').innerHTML='Sqft';
		if(document.getElementById(search_type+'_toccupied'))document.getElementById(search_type+'_toccupied').innerHTML='O. Occupied';
		if(document.getElementById(search_type+'_tpequity')){
			document.getElementById(search_type+'_tpequity').innerHTML='Debt Equity';
			if(search_type=='FS') document.getElementById(search_type+'_tpequity').innerHTML='Potential Equity';
		}
		if(document.getElementById(search_type+'_price_dol1'))document.getElementById(search_type+'_price_dol1').style.display='';
		if(document.getElementById(search_type+'_price_to'))document.getElementById(search_type+'_price_to').innerHTML='to';
		if(document.getElementById(search_type+'_price_dol2'))document.getElementById(search_type+'_price_dol2').style.display='';
		if(document.getElementById(search_type+'_bed'))document.getElementById(search_type+'_bed').style.display='';
		if(document.getElementById(search_type+'_bath'))document.getElementById(search_type+'_bath').style.display='';
		if(document.getElementById(search_type+'_sqft'))document.getElementById(search_type+'_sqft').style.display='';
		if(document.getElementById(search_type+'_pequity'))document.getElementById(search_type+'_pequity').style.display='';
		if(document.getElementById(search_type+'_price_low'))document.getElementById(search_type+'_price_low').style.display='';
		if(document.getElementById(search_type+'_price_hi'))document.getElementById(search_type+'_price_hi').style.display='';
		if(document.getElementById(search_type+'_tentrydate'))document.getElementById(search_type+'_tentrydate').innerHTML='Entry Date';
		if(document.getElementById(search_type+'_entrydate'))document.getElementById(search_type+'_entrydate').style.display='';
		if(document.getElementById(search_type+'_occupied'))document.getElementById(search_type+'_occupied').style.display='';
	}else{
		if(document.getElementById(search_type+'_tproptype'))document.getElementById(search_type+'_tproptype').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_tpropfore'))document.getElementById(search_type+'_tpropfore').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_proptype'))document.getElementById(search_type+'_proptype').style.display='none';
		if(document.getElementById(search_type+'_pendes'))document.getElementById(search_type+'_pendes').style.display='none';
		if(document.getElementById(search_type+'_tprice'))document.getElementById(search_type+'_tprice').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_tbeds'))document.getElementById(search_type+'_tbeds').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_tbath'))document.getElementById(search_type+'_tbath').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_tsqft'))document.getElementById(search_type+'_tsqft').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_tpequity'))document.getElementById(search_type+'_tpequity').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_toccupied'))document.getElementById(search_type+'_toccupied').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_price_dol1'))document.getElementById(search_type+'_price_dol1').style.display='none';
		if(document.getElementById(search_type+'_price_to'))document.getElementById(search_type+'_price_to').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_price_dol2'))document.getElementById(search_type+'_price_dol2').style.display='none';
		if(document.getElementById(search_type+'_bed'))document.getElementById(search_type+'_bed').style.display='none';
		if(document.getElementById(search_type+'_bath'))document.getElementById(search_type+'_bath').style.display='none';
		if(document.getElementById(search_type+'_sqft'))document.getElementById(search_type+'_sqft').style.display='none';
		if(document.getElementById(search_type+'_pequity'))document.getElementById(search_type+'_pequity').style.display='none';
		if(document.getElementById(search_type+'_price_low'))document.getElementById(search_type+'_price_low').style.display='none';
		if(document.getElementById(search_type+'_price_hi'))document.getElementById(search_type+'_price_hi').style.display='none';
		if(document.getElementById(search_type+'_tentrydate'))document.getElementById(search_type+'_tentrydate').innerHTML='&nbsp;';
		if(document.getElementById(search_type+'_entrydate'))document.getElementById(search_type+'_entrydate').style.display='none';
		if(document.getElementById(search_type+'_occupied'))document.getElementById(search_type+'_occupied').style.display='none';
	}
}




function searchForm(id,tipo){
	var value=document.getElementById(id).value;
	var long=document.getElementById(id).value.length;
	var county=document.getElementById(search_type+'_county_search').value;

	if(long>0 && value!='Address, City or Zip Code' && value!='Parcelid' && value!='Mlnumber')
		GuardarSearch(tipo); 
	else{
		if(search_by_type=='MAP' && (value=='Address, City or Zip Code' || value=='Parcelid' || value=='Mlnumber'))
			GuardarSearch(tipo); 
		else 
			GuardarSearch(tipo);
	}
}

function GuardarSearch(tipo){
	var ty='location';
		
	var equity=-1;
	if(document.getElementById(search_type+'_pequity'))equity=document.getElementById(search_type+'_pequity').value;
	var entrydate='';
	if(document.getElementById(search_type+'_entrydate'))entrydate=document.getElementById(search_type+'_entrydate').value;
	var _search='';
	if(document.getElementById(search_type+'_search'))_search=document.getElementById(search_type+'_search').value;
	
	if(search_by_type=='PARCELID') ty='parcelid';
	if(search_by_type=='CASE-NUMBER') ty='case';
	if(search_by_type=='MLNUMBER') ty='mlnumber';
	
	if(document.getElementById(search_type+'_case-number') && document.getElementById(search_type+'_case-number').value!='Case-Number'){
		_search=document.getElementById(search_type+'_case-number').value;	
		ty='case';
	}
	if(document.getElementById(search_type+'_mlnumber') && document.getElementById(search_type+'_mlnumber').value!='Mlnumber'){
		_search=document.getElementById(search_type+'_mlnumber').value;	
		ty='mlnumber';
	}
	if(document.getElementById(search_type+'_parcelid') && document.getElementById(search_type+'_parcelid').value!='Parcelid'){
		_search=document.getElementById(search_type+'_parcelid').value;	
		ty='parcelid';
	}
	var proptype='';
	if(document.getElementById(search_type+'_proptype'))proptype=document.getElementById(search_type+'_proptype').value;	
	var price_low='';
	if(document.getElementById(search_type+'_price_low'))price_low=document.getElementById(search_type+'_price_low').value;	
	var price_hi='';
	if(document.getElementById(search_type+'_price_hi'))price_hi=document.getElementById(search_type+'_price_hi').value;	
	var bed=-1;
	if(document.getElementById(search_type+'_bed'))bed=document.getElementById(search_type+'_bed').value;	
	var bath=-1;
	if(document.getElementById(search_type+'_bath'))bath=document.getElementById(search_type+'_bath').value;	
	var sqft=-1;
	if(document.getElementById(search_type+'_sqft'))sqft=document.getElementById(search_type+'_sqft').value;	
	var pendes=-1;
	if(document.getElementById(search_type+'_pendes'))pendes=document.getElementById(search_type+'_pendes').value;	
	var occupied=-1;
	if(document.getElementById(search_type+'_occupied'))occupied=document.getElementById(search_type+'_occupied').value;
	var mapa_search_latlong='-1';
	if(document.getElementById('mapa_search_latlong'))mapa_search_latlong=document.getElementById('mapa_search_latlong').value;	
	var realtor='-1';
	if(document.getElementById(search_type+'_realtor'))realtor=document.getElementById(search_type+'_realtor').value;
	
	if(search_by_type=='MAP' && mapa_search_latlong=='-1' && _search==''){
		alert('Map shape/polygon is required to execute the search');
		return false;
	}
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_coresearch.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			search: _search,
			county: document.getElementById(search_type+'_county_search').value,
			tsearch: ty,
			proptype: proptype,
			price_low: price_low,
			price_hi: price_hi,
			bed: bed,
			bath: bath,
			sqft: sqft,
			pequity: equity,
			pendes: pendes,
			search_mapa: mapa_search_latlong,
			entrydate: entrydate,
			search_type: search_type,
			occupied: occupied
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			systemsearch= 'basic';
			if(tipo=='search') createResult();
			else if(tipo=='buyers') createResultBuyers();
			else if(tipo=='mortgage') createResultMortgage();
			else if(tipo=='foreclosures') createResultForeclosures();
			else if(tipo=='bank') createResultBankOwned();
		}                                
	});
}

function ShowManageTemplate(){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_manage_template.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'baseLoad',
			idGridTemplate: search_type
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			var val = response.responseText.split('^');
			var val1 = Ext.decode(val[0]);
			var val2 = Ext.decode(val[1]);
			
			var panel = new Ext.Panel({
				width: 750,
				layout: 'column',
				bodyStyle:'padding:1px;text-align:left;',
				height: 380,
				autoScroll: true,
				items: [
					{
						columnWidth:.55,
						id: 'multiselectall',
						height: 360,
						width: 400,
						autoScroll: true,
						bodyStyle:'text-align:left;',
						items: val1
					},{
						columnWidth:.45,
						items: new Ext.tree.TreePanel({
							id: 'multiselecttemplate',
							height: 360,
							width: 320,
							useArrows: false,
							autoScroll: true,
							animate: true,
							enableDD: true,
							containerScroll: true,
							border: false,
							loader: new Ext.tree.TreeLoader({clearOnLoad: false}),
							rootVisible: false,
							root: {
								nodeType: 'async',
								text: 'Ext JS',
								draggable: false,
								id: 'source'
							},
							listeners: {
								'render': function (tree){
									for(v in val2){
										if(Ext.isNumber(parseInt(v))){
											Ext.getCmp('multiselecttemplate').root.appendChild(new Ext.tree.TreeNode({id:val2[v].id, text:val2[v].text, leaf:val2[v].leaf, iconCls:'treePanel-no-image'}));
										}
									}
								}
							}
						})
					}
				]
			});
			
			var total = new Ext.FormPanel({
				labelWidth: 100, // label settings here cascade unless overridden
				url:'save-form.php',
				frame:true,
				bodyStyle:'padding:5px 5px 0; text-align:left;',
				width: 'auto',
				items: [{
					xtype:'fieldset',
					id: 'new_template',
					checkboxToggle:true,
					title: 'New/Update Template',
					bodyStyle:'text-align:left;',
					autoHeight:true,
					layout: 'form',
					collapsed: true,
					items :[new Ext.form.ComboBox({
						editable: true,
						allowBlank: false,
						emptyText: 'Write Template Name',
						fieldLabel: 'Template Name',
						triggerAction: 'all',
						mode: 'remote',
						store: new Ext.data.JsonStore({
							url: 'properties_manage_template.php',
							baseParams: {'default': 'N'},
							id: 0,
							fields: [
								'tID',
								'tname'
							]
						}),
						width: 130,
						valueField: 'tID',
						displayField: 'tname',
						listeners:{
							'select': function (combo,record,index){
								LoadManageTemplateChecks(record.data.tID);
							},
							'change': function (field,newVal,oldVal){
								for(i=0;i<this.store.getCount();i++){
									if(this.store.getAt(i).data.tID==newVal){
										LoadManageTemplateChecks(newVal);
										return true;
									}
								}
								LoadManageTemplateChecks(-1);
							}
						}
					}),panel],
					buttons: [{
						text: 'Save',
						handler: function (){
							loading_win.show();
							var multy = Ext.getCmp('multiselecttemplate').root;
							var combo = Ext.getCmp('new_template').findByType('combo');
							var campos='';
							var i=0;
							var node = multy.item(i);
							
							while(!node.isLast()){
								if(campos!='') campos+=',';
								campos+=node.id;
								i++;
								node = multy.item(i);
							}
							campos+=','+node.id;
							
							if(combo[0].getValue()!='' && multy.hasChildNodes()>0){
								SaveManageTemplate(combo[0].getValue(),campos);
								win.close();
							}else{
								loading_win.hide();
								Ext.MessageBox.alert('Warning','Please select a Template Name from List to Create or Update.');
							}
						}
					}
					],
					listeners: {
						'collapse': function(panel){
							var com = Ext.getCmp('del_template');
							com.expand(false);
						},
						'expand': function(panel){
							var com = Ext.getCmp('del_template');
							com.collapse(false);
						}
					}
				},{
					xtype:'fieldset',
					id: 'del_template',
					checkboxToggle:true,
					title: 'Delete Template',
					autoHeight:true,
					layout: 'form',
					collapsed: false,
					items :[new Ext.form.ComboBox({
						autoSelect: true,
						autoLoad: true,
						editable: false,
						forceSelection: true,
						fieldLabel: 'Template Name',
						triggerAction: 'all',
						mode: 'remote',
						store: new Ext.data.JsonStore({
							url: 'properties_manage_template.php',
							baseParams: {'default': 'N'},
							id: 0,
							fields: [
								'tID',
								'tname'
							]
						}),
						width: 130,
						valueField: 'tID',
						displayField: 'tname'
					})],
					buttons: [{
						text: 'Delete',
						handler: function (){
							loading_win.show();
							var combo = Ext.getCmp('del_template').findByType('combo');
							if(combo[0].getValue()!=''){
								DeleteManageTemplate(combo[0].getValue());
								win.close();
							}else{
								loading_win.hide();
								Ext.MessageBox.alert('Warning','Please select a Template Name from List to delete.');
							}
						}
					}
					],
					listeners: {
						'collapse': function(panel){
							var com = Ext.getCmp('new_template');
							com.expand(false);
						},
						'expand': function(panel){
							var com = Ext.getCmp('new_template');
							com.collapse(false);
						}
					}
				}]
			});

			var win = new Ext.Window({		
				layout      : 'fit',
				width       : 800,
				height      : 560,
				modal	 	: true,
				plain       : true,
				title: 'Manage Template',
				items		: total
			});
			win.show();
		}                                
	});
	
}

function LoadManageTemplateChecks(template){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_manage_template.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'load',
			idGridTemplate: search_type,
			temp: template
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			if(document.getElementById('new_template')){
				var ids = Ext.decode(response.responseText);
				var checks = Ext.getCmp('new_template').findByType('checkbox');
				var ar = new Array();
				for(i=0; i<checks.length; i++){
					if(checks[i].getValue())
						checks[i].setValue(false);
					for(j=0; j<ids.length; j++){
						if(checks[i].getName().split('.')[0]==ids[j])
							ar[j]=i;	
					}
				}
				for(i=0; i<ar.length; i++){
					checks[ar[i]].setValue(true);
				}
			}
		}
	});
}

function SaveManageTemplate(idTemp,campos){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_manage_template.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'save',
			idTemp: idTemp,
			campos: campos
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			loading_win.hide();
			if(response.responseText=='true')
				Ext.MessageBox.alert('Manage Template','Saved Successfully.');
			else
				Ext.MessageBox.alert('Manage Template',response.responseText);
			
			if(document.getElementById('templateCombo'))
				Ext.getCmp('templateCombo').getStore().reload();
			if(document.getElementById('templateComboFG'))
				Ext.getCmp('templateComboFG').getStore().reload();
		}
	});
}

function DeleteManageTemplate(idTemp){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_manage_template.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'delete',
			idTemp: idTemp
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			loading_win.hide();
			if(response.responseText=='true')
				Ext.MessageBox.alert('Manage Template','Deleted Successfully.');
			else
				Ext.MessageBox.alert('Manage Template',response.responseText);
			Ext.getCmp('templateCombo').getStore().reload();
		}
	});
}

function ShowSavedSearch(){
	var fsf = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        url:'save-form.php',
        frame:true,
        title: 'Manage Saved Search',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        items: [{
            xtype:'fieldset',
			id: 'new_saved_search',
            checkboxToggle:true,
            title: 'New Saved Search',
            autoHeight:true,
            defaults: {width: 210},
            defaultType: 'textfield',
            collapsed: true,
            items :[{
                    fieldLabel: 'Name',
                    name: 'name',
                    allowBlank:false
                },{
                    fieldLabel: 'Description',
                    name: 'desc',
					allowBlank: false
                }
            ],
			listeners: {
				'collapse': function(panel){
					var com = Ext.getCmp('list_saved_search');
					com.expand(false);
					Ext.getCmp('save_SS').hide();
					Ext.getCmp('select_SS').show();
					Ext.getCmp('overwrite_SS').show();
					Ext.getCmp('delete_SS').show();
				},
				'expand': function(panel){
					var com = Ext.getCmp('list_saved_search');
					com.collapse(false);
					Ext.getCmp('save_SS').show();
					Ext.getCmp('select_SS').hide();
					Ext.getCmp('overwrite_SS').hide();
					Ext.getCmp('delete_SS').hide();
				}
			}
        },{
            xtype:'fieldset',
			id: 'list_saved_search',
			checkboxToggle:true,
			collapsed: false,
            title: 'List Saved Searches',
            autoHeight:true,
            items :[
				new Ext.form.ComboBox({
					fieldLabel: 'List',
					triggerAction: 'all',
					mode: 'remote',
					forceSelection: true,
					editable: false,
					store: new Ext.data.JsonStore({
						url: 'properties_saved_search.php',
						
						id: 0,
						fields: [
							'ssID',
							'ssname'
						]
					}),
					valueField: 'ssID',
					displayField: 'ssname'
				})
            ],
			listeners: {
				'collapse': function(panel){
					var com = Ext.getCmp('new_saved_search');
					com.expand(false);
					Ext.getCmp('save_SS').show();
					Ext.getCmp('select_SS').hide();
					Ext.getCmp('overwrite_SS').hide();
					Ext.getCmp('delete_SS').hide();
				},
				'expand': function(panel){
					var com = Ext.getCmp('new_saved_search');
					com.collapse(false);
					Ext.getCmp('save_SS').hide();
					Ext.getCmp('select_SS').show();
					Ext.getCmp('overwrite_SS').show();
					Ext.getCmp('delete_SS').show();
				}
			}
        }],

        buttons: [{
            text: 'Save',
			id: 'save_SS',
			handler: function(){
				var name = Ext.getCmp('new_saved_search').getComponent(0);
				var desc = Ext.getCmp('new_saved_search').getComponent(1);
				if(name.isValid() && desc.isValid()){
					GuardarSearchSaved(name.getValue(),desc.getValue());
					win.close();
				}else
					Ext.Msg.alert('Manage Saved Searches', 'Please fill the name and description field to save the search.');
			}
        },{
            text: 'Select',
			id: 'select_SS',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Searches', 'Please select a Saved Search from List to load.');
				else{
					SelectSearchSaved(ssid);
					win.close();
				}
			}
        },{
            text: 'Overwrite',
			id: 'overwrite_SS',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Searches', 'Please select a Saved Search from List to overwrite.');
				else{
					OverwriteSearchSaved(ssid);
					win.close();
				}
			}
        },{
            text: 'Delete',
			id: 'delete_SS',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Searches', 'Please select a Saved Search from List to delete.');
				else{
					DeleteSearchSaved(ssid);
					win.close();
				}
			}
        }]
    });
	
	var win = new Ext.Window({		
		layout      : 'fit',
		width       : 370,
		height      : 300,
		modal	 	: true,
		plain       : true,
		items		: fsf
	});
	win.show();
}

function GuardarSearchSaved(name,desc){
	var ty='location';	
	var equity=-1;
	if(document.getElementById(search_type+'_pequity'))equity=document.getElementById(search_type+'_pequity').value;
	var entrydate='';
	if(document.getElementById(search_type+'_entrydate'))entrydate=document.getElementById(search_type+'_entrydate').value;
	var _search='';
	if(document.getElementById(search_type+'_search'))_search=document.getElementById(search_type+'_search').value;	
	if(document.getElementById(search_type+'_case-number') && document.getElementById(search_type+'_case-number').value!='Case-Number'){
		_search=document.getElementById(search_type+'_case-number').value;	
		ty='case';
	}
	if(document.getElementById(search_type+'_mlnumber') && document.getElementById(search_type+'_mlnumber').value!='Mlnumber'){
		_search=document.getElementById(search_type+'_mlnumber').value;	
		ty='mlnumber';
	}
	if(document.getElementById(search_type+'_parcelid') && document.getElementById(search_type+'_parcelid').value!='Parcelid'){
		_search=document.getElementById(search_type+'_parcelid').value;	
		ty='parcelid';
	}
	var proptype='';
	if(document.getElementById(search_type+'_proptype'))proptype=document.getElementById(search_type+'_proptype').value;	
	var price_low='';
	if(document.getElementById(search_type+'_price_low'))price_low=document.getElementById(search_type+'_price_low').value;	
	var price_hi='';
	if(document.getElementById(search_type+'_price_hi'))price_hi=document.getElementById(search_type+'_price_hi').value;	
	var bed=-1;
	if(document.getElementById(search_type+'_bed'))bed=document.getElementById(search_type+'_bed').value;	
	var bath=-1;
	if(document.getElementById(search_type+'_bath'))bath=document.getElementById(search_type+'_bath').value;	
	var sqft=-1;
	if(document.getElementById(search_type+'_sqft'))sqft=document.getElementById(search_type+'_sqft').value;	
	var pendes=-1;
	if(document.getElementById(search_type+'_pendes'))pendes=document.getElementById(search_type+'_pendes').value;	
	var occupied=-1;
	if(document.getElementById(search_type+'_occupied'))occupied=document.getElementById(search_type+'_occupied').value;
	var mapa_search_latlong='-1';
	if(document.getElementById('mapa_search_latlong'))mapa_search_latlong=document.getElementById('mapa_search_latlong').value;	
	
	if(search_by_type=='MAP' && mapa_search_latlong=='-1' && _search==''){
		Ext.Msg.alert('Search', 'Map shape/polygon is required to execute the search.');
		return false;
	}
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_search.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'save',
			name: name,
			desc: desc,
			search: _search,
			county: document.getElementById(search_type+'_county_search').value,
			state: document.getElementById(search_type+'_state_search').value,
			tsearch: ty,
			proptype: proptype,
			price_low: price_low,
			price_hi: price_hi,
			bed: bed,
			bath: bath,
			sqft: sqft,
			pequity: equity,
			pendes: pendes,
			search_mapa: mapa_search_latlong,
			entrydate: entrydate,
			search_type: search_type,
			occupied: occupied
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			if(response.responseText=='true')
				Ext.Msg.alert('Manage Saved Searches', 'Saved successfully.');
			else
				Ext.Msg.alert('Manage Saved Searches', response.responseText);
		}                                
	});
}
var intervalID=null;
var ss_data=null;
function SelectSearchSaved(ssid){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_search.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'select',
			ssid: ssid
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			var resp = response.responseText.split('^');
			if(resp[0]=='true'){
				ss_data = Ext.decode(resp[1]);		
				search_county = ss_data.basic.county;
				
				//validacion cuando no se guardo el state
				if(!ss_data.basic.state){
					ss_data.basic.state = 1;
					if(search_county==82) ss_data.basic.state = 2;
					if(search_county==83 || search_county==84) ss_data.basic.state = 3;
				}
				//------------------------------
				
				search_state = ss_data.basic.state;
				search_by_type = ss_data.basic.tsearch.toUpperCase();		
				doSearchTypeFilter(ss_data.basic.search_type);				
				LoadSearchSaved();
			}else
				Ext.Msg.alert('Manage Saved Searches', response.responseText);

		}                                
	});
}

function LoadSearchSaved(){
	if(ss_data.basic.tsearch.toUpperCase()=='LOCATION') doSearchByFilter(ss_data.basic.tsearch.toUpperCase());
	else if(ss_data.basic.tsearch.toUpperCase()=='CASE')
		if(document.getElementById(search_type+'_case-number'))document.getElementById(search_type+'_case-number').value = ss_data.basic.search;
	else if(ss_data.basic.tsearch.toUpperCase()=='PARCELID')
		if(document.getElementById(search_type+'_parcelid'))document.getElementById(search_type+'_parcelid').value = ss_data.basic.search;
	else if(ss_data.basic.tsearch.toUpperCase()=='MLNUMBER')
		if(document.getElementById(search_type+'_mlnumber'))document.getElementById(search_type+'_mlnumber').value = ss_data.basic.search;
	
	if(document.getElementById('mapa_search_latlong'))document.getElementById('mapa_search_latlong').value=ss_data.basic.search_mapa;

	if(ss_data.basic.search_mapa!='-1' && ss_data.basic.search_mapa!=null){
		if(document.getElementById('mapSearch') && document.getElementById('mapSearch').style.display=='') doSearchByFilter('MAP_OFF');
		doSearchByFilter('MAP');
		document.getElementById(search_type+'_combo_search_by').value = 'MAP';

		var _xpoints=new Array();
		var latlong = ss_data.basic.search_mapa.split('/');
		for(x=0;x<latlong.length;x++){
			_xpoints.push(getLatLon(latlong[x].split(",")[0],latlong[x].split(",")[1]));
		}

		mapSearch.points2shape(_xpoints);
		mapSearch.centerMapCounty(search_county,true);
	}
	if(document.getElementById(search_type+'_county_search'))document.getElementById(search_type+'_county_search').value = ss_data.basic.county;
	if(document.getElementById(search_type+'_pequity'))document.getElementById(search_type+'_pequity').value=ss_data.basic.pequity;
	if(document.getElementById(search_type+'_entrydate'))document.getElementById(search_type+'_entrydate').value=ss_data.basic.entrydate;
	if(document.getElementById(search_type+'_search') && ss_data.basic.tsearch.toUpperCase()=='LOCATION')document.getElementById(search_type+'_search').value=ss_data.basic.search;	
	if(document.getElementById(search_type+'_proptype'))document.getElementById(search_type+'_proptype').value=ss_data.basic.proptype;	
	if(document.getElementById(search_type+'_price_low'))document.getElementById(search_type+'_price_low').value=ss_data.basic.price_low;	
	if(document.getElementById(search_type+'_price_hi'))document.getElementById(search_type+'_price_hi').value=ss_data.basic.price_hi;	
	if(document.getElementById(search_type+'_bed'))document.getElementById(search_type+'_bed').value=ss_data.basic.bed;	
	if(document.getElementById(search_type+'_bath'))document.getElementById(search_type+'_bath').value=ss_data.basic.bath;	
	if(document.getElementById(search_type+'_sqft'))document.getElementById(search_type+'_sqft').value=ss_data.basic.sqft;	
	if(document.getElementById(search_type+'_pendes'))document.getElementById(search_type+'_pendes').value=ss_data.basic.pendes;
	if(document.getElementById(search_type+'_occupied'))document.getElementById(search_type+'_occupied').value=ss_data.basic.occupied ? ss_data.basic.occupied : '-1';			
}

function DeleteSearchSaved(ssid){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_search.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'delete',
			ssid: ssid
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			if(response.responseText=='true')
				Ext.Msg.alert('Manage Saved Searches', 'Delete successfully.');
			else
				Ext.Msg.alert('Manage Saved Searches', response.responseText);

		}                                
	});
}

function OverwriteSearchSaved(ssid){
	var ty='location';	
	if(search_by_type=='MLNUMBER') 
		ty='mlnumber';
	if(search_by_type=='PARCELID') 
		ty='parcelid';
	if(search_by_type=='CASE')
		ty='case';
	var equity=-1;
	if(document.getElementById(search_type+'_pequity'))equity=document.getElementById(search_type+'_pequity').value;
	var entrydate='';
	if(document.getElementById(search_type+'_entrydate'))entrydate=document.getElementById(search_type+'_entrydate').value;
	var _search='';
	if(document.getElementById(search_type+'_search'))_search=document.getElementById(search_type+'_search').value;	
	var proptype='';
	if(document.getElementById(search_type+'_proptype'))proptype=document.getElementById(search_type+'_proptype').value;	
	var price_low='';
	if(document.getElementById(search_type+'_price_low'))price_low=document.getElementById(search_type+'_price_low').value;	
	var price_hi='';
	if(document.getElementById(search_type+'_price_hi'))price_hi=document.getElementById(search_type+'_price_hi').value;	
	var bed=-1;
	if(document.getElementById(search_type+'_bed'))bed=document.getElementById(search_type+'_bed').value;	
	var bath=-1;
	if(document.getElementById(search_type+'_bath'))bath=document.getElementById(search_type+'_bath').value;	
	var sqft=-1;
	if(document.getElementById(search_type+'_sqft'))sqft=document.getElementById(search_type+'_sqft').value;	
	var pendes=-1;
	if(document.getElementById(search_type+'_pendes'))pendes=document.getElementById(search_type+'_pendes').value;	
	var occupied=-1;
	if(document.getElementById(search_type+'_occupied'))occupied=document.getElementById(search_type+'_occupied').value;
	var mapa_search_latlong='-1';
	if(document.getElementById('mapa_search_latlong'))mapa_search_latlong=document.getElementById('mapa_search_latlong').value;	
	
	if(search_by_type=='MAP' && mapa_search_latlong=='-1' && _search==''){
		//alert('Map shape/polygon is required to execute the search');
		Ext.Msg.alert('Search', 'Map shape/polygon is required to execute the search.');
		return false;
	}
	
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_search.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'overwrite',
			ssid: ssid,
			search: _search,
			county: document.getElementById(search_type+'_county_search').value,
			state: document.getElementById(search_type+'_state_search').value,
			tsearch: ty,
			proptype: proptype,
			price_low: price_low,
			price_hi: price_hi,
			bed: bed,
			bath: bath,
			sqft: sqft,
			pequity: equity,
			pendes: pendes,
			search_mapa: mapa_search_latlong,
			entrydate: entrydate,
			search_type: search_type,
			occupied: occupied
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			if(response.responseText=='true')
				Ext.Msg.alert('Manage Saved Searches', 'Overwrite successfully.');
			else
				Ext.Msg.alert('Manage Saved Searches', response.responseText);
		}                                
	});
}

function ShowSavedSearchParameter(){
	var psp = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        //url:'save-form.php',
        frame:true,
        //title: 'Manage Saved Parameters',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        items: [{
            xtype:'fieldset',
			id: 'new_saved_parameter',
            checkboxToggle:true,
            title: 'New Saved Parameter',
            autoHeight:true,
            defaults: {width: 210},
            defaultType: 'textfield',
            collapsed: true,
            items :[{
                    fieldLabel: 'Name',
                    name: 'name',
                    allowBlank:false
                },{
                    fieldLabel: 'Description',
                    name: 'desc',
					allowBlank: false
                }
            ],
			listeners: {
				'collapse': function(panel){
					var com = Ext.getCmp('list_saved_search');
					com.expand(false);
					Ext.getCmp('save_SP').hide();
					Ext.getCmp('select_SP').show();
					Ext.getCmp('overwrite_SP').show();
					Ext.getCmp('delete_SP').show();
				},
				'expand': function(panel){
					var com = Ext.getCmp('list_saved_search');
					com.collapse(false);
					Ext.getCmp('save_SP').show();
					Ext.getCmp('select_SP').hide();
					Ext.getCmp('overwrite_SP').hide();
					Ext.getCmp('delete_SP').hide();
				}
			}
        },{
            xtype:'fieldset',
			id: 'list_saved_search',
			checkboxToggle:true,
			collapsed: false,
            title: 'List Saved Parameters',
            autoHeight:true,
            items :[
				new Ext.form.ComboBox({
					fieldLabel: 'List',
					triggerAction: 'all',
					mode: 'remote',
					store: new Ext.data.JsonStore({
						url: 'properties_saved_searchAdv.php',
						forceSelection: true,
						id: 0,
						fields: ['ssID','ssname']
					}),
					valueField: 'ssID',
					displayField: 'ssname'
				})
            ],
			listeners: {
				'collapse': function(panel){
					var com = Ext.getCmp('new_saved_parameter');
					com.expand(false);
					Ext.getCmp('save_SP').show();
					Ext.getCmp('select_SP').hide();
					Ext.getCmp('overwrite_SP').hide();
					Ext.getCmp('delete_SP').hide();
				},
				'expand': function(panel){
					var com = Ext.getCmp('new_saved_parameter');
					com.collapse(false);
					Ext.getCmp('save_SP').hide();
					Ext.getCmp('select_SP').show();
					Ext.getCmp('overwrite_SP').show();
					Ext.getCmp('delete_SP').show();
				}
			}
        }],

        buttons: [{
            text: 'Save',
			id: 'save_SP',
			handler: function(){
				var name = Ext.getCmp('new_saved_parameter').getComponent(0);
				var desc = Ext.getCmp('new_saved_parameter').getComponent(1);
				if(name.isValid() && desc.isValid()){
					GuardarSearchSavedParameter(name.getValue(),desc.getValue());
					win.close();
				}else
					Ext.Msg.alert('Manage Saved Parameters', 'Please fill the name and description field to save the parameter.');
			}
        },{
            text: 'Select',
			id: 'select_SP',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Parameters', 'Please select a Saved Parameters from List to load.');
				else{
					SelectSearchSavedParameter(ssid);
					win.close();
				}
			}
        },{
            text: 'Overwrite',
			id: 'overwrite_SP',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Parameters', 'Please select a Saved Parameters from List to overwrite.');
				else{
					OverwriteSearchSavedParameter(ssid);
					win.close();
				}
			}
        },{
            text: 'Delete',
			id: 'delete_SP',
			handler: function(){
				var list = Ext.getCmp('list_saved_search').getComponent(0);
				var ssid = list.getValue();
				if(Ext.isEmpty(ssid))
					Ext.Msg.alert('Manage Saved Parameters', 'Please select a Saved Parameters from List to delete.');
				else{
					DeleteSearchSavedParameter(ssid);
					win.close();
				}
			}
        }]
    });
	
	var win = new Ext.Window({		
		title: 'Manage Saved Parameters',
		layout      : 'fit',
		width       : 370,
		height      : 300,
		modal	 	: true,
		plain       : true,
		items		: psp
	});
	win.show();
}

function GuardarSearchSavedParameter(name,desc){
	if(search_by_type=='MAP' && document.getElementById('mapa_search_latlongAdv').value=='-1'){
		Ext.Msg.alert('Search', 'Map shape/polygon is required to execute the search.');
		return false;
	}

	formulsearchadv.getForm().submit({
		url: 'properties_saved_searchAdv.php', 
		method: 'POST',
		params: { tipo: 'save', name: name, desc: desc },
		waitTitle: 'Please wait..',
		waitMsg: 'Sending data...',
		success: function(form, action) {
			if(action.response.responseText=='true')
				Ext.Msg.alert('Manage Saved Parameters', 'Saved Parameters successfully.');
			else
				Ext.Msg.alert('Manage Saved Parameters', action.response.responseText);
		},
		failure: function(form, action) {
			obj = Ext.util.JSON.decode(action.response.responseText);
			Ext.Msg.alert("Failure", obj.errors.reason);
		}
	});
}

function DeleteSearchSavedParameter(ssid){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_searchAdv.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			tipo: 'delete',
			ssid: ssid
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			if(response.responseText=='true')
				Ext.Msg.alert('Manage Saved Parameters', 'Delete successfully.');
			else
				Ext.Msg.alert('Manage Saved Parameters', response.responseText);

		}                                
	});
}

function OverwriteSearchSavedParameter(ssid){
	if(search_by_type=='MAP' && document.getElementById('mapa_search_latlongAdv').value=='-1'){
		Ext.Msg.alert('Search', 'Map shape/polygon is required to execute the search.');
		return false;
	}

	formulsearchadv.getForm().submit({
		url: 'properties_saved_searchAdv.php', 
		method: 'POST',
		params: { tipo: 'overwrite', ssid: ssid },
		waitTitle: 'Please wait..',
		waitMsg: 'Sending data...',
		success: function(form, action) {
			if(action.response.responseText=='true')
				Ext.Msg.alert('Manage Saved Parameters', 'Overwrite successfully.');
			else
				Ext.Msg.alert('Manage Saved Parameters', action.response.responseText);
		},
		failure: function(form, action) {
			obj = Ext.util.JSON.decode(action.response.responseText);
			Ext.Msg.alert("Failure", obj.errors.reason);
		}
	});

}

function SelectSearchSavedParameter(ssid){

	Ext.Ajax.request( 
	{  
		waitMsg: 'Validating...',
		url: 'properties_saved_searchAdv.php', 
		method: 'POST',
		params: { tipo: 'select',ssid: ssid, dataarr: 'no'},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			var results=response.responseText;//alert(results);return;
			var aRes=results.split("^");
			
			if(aRes[0]=='true')
			{
				var rest = Ext.util.JSON.decode(aRes[1]);
				var havemortgage = aRes[2];
				
				if(Ext.getCmp('PR'))Ext.getCmp('PR').collapse();
				if(Ext.getCmp('PRMORE'))Ext.getCmp('PRMORE').collapse();
				if(Ext.getCmp('FS'))Ext.getCmp('FS').collapse();
				if(Ext.getCmp('FSMORE'))Ext.getCmp('FSMORE').collapse();
				if(Ext.getCmp('BO'))Ext.getCmp('BO').collapse();
				if(Ext.getCmp('BOMORE'))Ext.getCmp('BOMORE').collapse();
				if(Ext.getCmp('FO'))Ext.getCmp('FO').collapse();

				if(Ext.getCmp('MO'))
				{
					if(havemortgage=='N')
					{
						Ext.getCmp('MO').disable();
						Ext.getCmp('MO').setVisible(false);
					}
					if(havemortgage=='Y')
					{
						Ext.getCmp('MO').enable();
						Ext.getCmp('MO').setVisible(true);
						Ext.getCmp('MO').collapse();
					}
				}
				
                for(var i=0;i<rest.length;i++)
				{
                    if(rest[i]=='PRMORE')Ext.getCmp('PR').expand();
                    if(rest[i]=='FSMORE')Ext.getCmp('FS').expand();
                    if(rest[i]=='BOMORE')Ext.getCmp('BO').expand();
					if(Ext.getCmp(rest[i]))Ext.getCmp(rest[i]).expand();
				}

				formulsearchadv.getForm().reset();
				for(x=0;x<arrinpbetween.length;x++)
				{	
					Ext.getCmp(arrinpbetween[x]).setVisible(false);
				}
				arrinpbetween=new Array();
				if(mapSearchAdv!=null)
				{
					search_by_typeAdv='MAP_OFF';
					mapSearchAdv.cleaner()
				}
						
				formulsearchadv.getForm().load({
					url: 'properties_saved_searchAdv.php', 
					params: {  tipo: 'select', ssid: ssid, dataarr: 'yes' },
					success: function(form, action) {
						//Ext.Msg.alert("Success", action.response.responseText);
						
						var val=Ext.getCmp('nproperty').getValue();
						var valcur=Ext.getCmp('nproptype').getValue();
						if(val=='FR')val='FS';
						if(val=='BOR')val='BO';
						var cprop = Ext.getCmp('nproptype');        
						cprop.clearValue();
						cprop.store.removeAll();
						
						switch (val)
						{
							case 'PR': case 'FO': case 'MO'://public records  foreclosure
								cprop.store.add(new cprop.store.recordType({valuec:'*',textc:'Any Type'}));
								cprop.store.add(new cprop.store.recordType({valuec:'01',textc:'Single Family'}));
								cprop.store.add(new cprop.store.recordType({valuec:'04',textc:'Condo/Town/Villa'}));
								cprop.store.add(new cprop.store.recordType({valuec:'03',textc:'Multi Family +10'}));
								cprop.store.add(new cprop.store.recordType({valuec:'08',textc:'Multi Family -10'}));
								cprop.store.add(new cprop.store.recordType({valuec:'11',textc:'Commercial'}));
								cprop.store.add(new cprop.store.recordType({valuec:'00',textc:'Vacant Land'}));
								cprop.store.add(new cprop.store.recordType({valuec:'02',textc:'Mobile Home'}));
								cprop.store.add(new cprop.store.recordType({valuec:'99',textc:'Other'}));
							break;
							case 'FS': case 'BO':  //listing
								cprop.store.add(new cprop.store.recordType({valuec:'*',textc:'Any Type'}));
								cprop.store.add(new cprop.store.recordType({valuec:'01',textc:'Single Family'}));
								cprop.store.add(new cprop.store.recordType({valuec:'04',textc:'Condo/Town/Villa'}));
								cprop.store.add(new cprop.store.recordType({valuec:'08',textc:'Multi Family'}));
								cprop.store.add(new cprop.store.recordType({valuec:'11',textc:'Commercial'}));
								cprop.store.add(new cprop.store.recordType({valuec:'00',textc:'Vacant Land'}));
								cprop.store.add(new cprop.store.recordType({valuec:'02',textc:'Mobile Home'}));
							break;
						}
						cprop.setValue(valcur);		
						
						
						var cfore = Ext.getCmp('nforeclosure');        
						valcur=Ext.getCmp('nforeclosure').getValue();
						cfore.clearValue();
						cfore.store.removeAll();
						if(val=='FO')//foreclosure
						{
								cfore.store.add(new cfore.store.recordType({valuec:'-1',textc:'Any'}));
								cfore.store.add(new cfore.store.recordType({valuec:'P',textc:'Pre-Foreclosed'}));
								cfore.store.add(new cfore.store.recordType({valuec:'F',textc:'Foreclosed'}));
						}
						else//other outpput NOT foreclosure
						{
								cfore.store.add(new cfore.store.recordType({valuec:'-1',textc:'Any'}));
								cfore.store.add(new cfore.store.recordType({valuec:'N',textc:'No'}));
								cfore.store.add(new cfore.store.recordType({valuec:'P',textc:'Pre-Foreclosed'}));
								cfore.store.add(new cfore.store.recordType({valuec:'F',textc:'Foreclosed'}));
						}
						cfore.setValue(valcur);	
						
						if(mapSearchAdv!=null && document.getElementById('mapa_search_latlongAdv').value!='-1' && document.getElementById('mapa_search_latlongAdv').value!=null)
						{
							search_by_typeAdv='MAP';
							if(document.getElementById('mapSearchAdv') && document.getElementById('mapSearchAdv').style.display=='none')
								mapSearchAdv.control_map();

							mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
							
							var _xpoints=new Array();
							var latlong = document.getElementById('mapa_search_latlongAdv').value.split('/');
							for(x=0;x<latlong.length;x++){
								_xpoints.push(getLatLon(latlong[x].split(",")[0],latlong[x].split(",")[1]));
							}
							mapSearchAdv.points2shape(_xpoints);
							mapSearchAdv.getCenterPins();
						}

						//ajax select between
						Ext.Ajax.request( 
						{  
							waitMsg: 'Validating...',
							url: 'properties_saved_searchAdv.php', 
							method: 'POST',
							params: { tipo: 'selectbetween',ssid: ssid, dataarr: 'yes'},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								var results=response.responseText;//alert(results);return;
								var rest = Ext.util.JSON.decode(results);

								for(i=0;i<rest.data.length;i++)
								{
									txfield=rest.data[i].inputn;
									valcur=rest.data[i].inputv;
									//alert(txfield+"#"+valcur);
									Ext.getCmp(txfield).setVisible(true);
									Ext.getCmp(txfield).setValue(valcur);
									arrinpbetween.push(txfield);
								}
								
//								var cfore = Ext.getCmp('nforeclosure');        
//								valcur=Ext.getCmp('nforeclosure').getValue();
//								cfore.clearValue();
//								cfore.setValue(valcur);	
							},
							failure: function(form, action) {
								Ext.Msg.alert("Failure", action.result.errorMessage);
							}
						});
						
						
					},
					failure: function(form, action) {
						Ext.Msg.alert("Failure", action.result.errorMessage);
					}
				});
				
			}
			else
				Ext.Msg.alert('Manage Saved Parameters', response.responseText);

		}                                
	});

}

function expandir(fila,imagen)
{
	var mfila=document.getElementById(fila).style.display;
	if(mfila==null || mfila=="none")
	{
		document.getElementById(fila).style.display="inline";
		document.getElementById(imagen).src="../../img/up.png";
		document.getElementById(imagen).alt="Ocultar";
	}
	else
	{
		document.getElementById(fila).style.display="none";
		document.getElementById(imagen).src="../../img/down.png";
		document.getElementById(imagen).alt="Mostrar";
	}

}

function contacto(agentemail,address){
	    var mylisting=true;
		var simple = new Ext.FormPanel
		({
			labelWidth: 100, 
			url:'contactussendmail.php?email_user='+agentemail+'&mylisting='+mylisting+'&address_email='+address,
			frame:true,
			title: 'Contact AGENT', // titulo de la ventana
			bodyStyle:'padding:5px 5px 0', // estilo del boton
			width: 200,  
			defaults: {width: 200},
			waitMsgTarget : 'Send Message...',
			
			items: [{
					fieldLabel: 'Name',
					xtype: 'textfield',
					name: 'nombre'
					
				    },{
					fieldLabel: 'E-mail',  // etiqueta  
					xtype: 'textfield',   // tipo de campo   
					name: 'correo',      // variable
					vtype: 'email'  //tipo de variable
					
					 },{
					fieldLabel: 'Company',  // etiqueta  
					xtype: 'textfield',   // tipo de campo   
					name: 'empresa'      // variable
				
					  },{
					fieldLabel: 'Phone',  // etiqueta  
					xtype: 'textfield',   // tipo de campo   
					name: 'telefono'      // variable
				
					  },{
					fieldLabel: 'City - County',  // etiqueta  
					xtype: 'textfield',   // tipo de campo   
					name: 'ciudad'      // variable
				
					  },{
					fieldLabel: 'Your Message',  // etiqueta  
					xtype: 'htmleditor',   // tipo de campo
					id:'COMENTARIO',
					name: 'comentario',      // variable
				    height:200,
                    anchor:'98%'
				    }],
	
			buttons: [{
				text: 'Send',  
				handler: function() 
				{   
				 simple.getForm().submit({
					 
					 success: function(form, action)
											 {          
											 win.close();
							                 Ext.Msg.alert('Mensaje', 'Message Sent');
											 }, 
											 failure: function(form, action) 
											 {
								             Ext.Msg.alert("Failure", 'Message No Sent');
							                 }
					                	     });												
								
				}

				},{
				text: 'Cancel',
				handler  : function(){
                        {win.close();}
                    }
			         }]
		});
		 ///////////////// Habilita la nueva ventana con sus propiedades
		  
		  win = new Ext.Window({
			
			layout      : 'fit',
			width       : 680,
			height      : 350,
			modal	 	: true,
			plain       : true,
			items		: simple

		                    });
        /////////////////////////////////////////////////
		
        win.show(); // apertura de la ventana
		win.addListener("beforeshow",function(win)
											  {
			                                   simple.getForm().reset();
			                                  });
	}	
	
function close_login_win_allcounty(user,close)
{
	//loading_win.show();
	var newprice=document.getElementById("newprice").value;
	var prorate=document.getElementById("proratecommercial").value;
	//if(close==0 && 	document.getElementById("chcommercial").checked==false)close=1;
	var parametros="oper=allcountyactivate&userid="+user+"&newprice="+newprice+"&prorate="+prorate+"&close="+close;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			//loading_win.hide();
			if(aRes[0]>0)
			{
				login_win_allcounty.hide()
				if(aRes[0]==1)
				{
					Ext.Msg.alert('All County Promotion', aRes[1]);//document.getElementById("msgerror5").innerHTML=aRes[1];
				}
				if(systemwhat==1)location.href='properties_search.php';
				else location.href='pagmenu1.php';
			}
			else
				document.getElementById("msgerror5").innerHTML=aRes[1];	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

}

function close_login_win_commercial(user,close)
{
	//loading_win.show();
	var newprice=document.getElementById("newprice").value;
	var proratecommercial=document.getElementById("proratecommercial").value;
	var pricecommercial=document.getElementById("pricecommercial").value;
	//if(close==0 && 	document.getElementById("chcommercial").checked==false)close=1;
	var parametros="oper=ActivateCommercialAskLogin&userid="+user+"&newprice="+newprice+"&proratecommercial="+proratecommercial+"&pricecommercial="+pricecommercial+"&close="+close;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			//loading_win.hide();
			if(aRes[0]>0)
			{
				login_win_commercial.hide()
				if(aRes[0]==1)
				{
					Ext.Msg.alert('New Residential and Commercial Version', aRes[1]);//document.getElementById("msgerror5").innerHTML=aRes[1];
				}
				if(systemwhat==1)location.href='properties_search.php';
				else location.href='pagmenu1.php';
			}
			else
				document.getElementById("msgerror5").innerHTML=aRes[1];	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

}
function getLocationGps(){
	if(geo_position_js.init()){
		loading_win.show(); 
		geo_position_js.getCurrentPosition(success_callbackLocation,error_callbackLocation,{enableHighAccuracy:true,options:5000});
	}
	else{
		Ext.Msg.alert("Error","Functionality not available");
	}
}
function success_callbackLocation(p){
	var lat=p.coords.latitude;
	var lon=p.coords.longitude;
	var distancia=Ext.getCmp('distance').getValue();
	Ext.Ajax.request({   
		waitMsg: 'Saving changes...',
		url: 'properties_coresearch.php', 
		method: 'POST',
		params: {
			latlong: 'Y',
			latitude: lat,
			longitude: lon,
			distancia: distancia 
		},
		
		failure:function(response,options){
			loading_win.hide();
			Ext.MessageBox.alert('Warning','Error saving'); 
		},
		
		success:function(response,options){
			var rest = Ext.util.JSON.decode(response.responseText);
			if(Ext.getCmp('mylocationgps')!=null){
				Ext.getCmp('mylocationgps').setValue('Y');
			}
			loading_win.hide();
						
			if(Ext.getCmp('tipomap').getValue()=='Basic'){
				if(document.getElementById('mapSearch').style.display == 'none')
					mapSearch.control_map();

				mapSearch.borrarTodoMap();
				
				mapSearch.addPushpin(lat,lon,'http://www.reifax.com/img/man_location.png','');
				
				var _xpoints=new Array();
				var factor=(distancia*0.015)/0.5;
				latMin=lat-(factor);
				latMax=lat+(factor);
				lonMin=lon-(factor);
				lonMax=lon+(factor);
				_xpoints.push(getLatLon(latMin,lonMin));
				_xpoints.push(getLatLon(latMax,lonMax));
				document.getElementById('mapa_search_latlong').value=latMin+','+lonMin+'/'+latMax+','+lonMax;
				
				mapSearch.points2shape(_xpoints);
				mapSearch.getCenterPins();				
			}else{
				if(document.getElementById('mapSearchAdv').style.display == 'none')
					mapSearchAdv.control_map();

				mapSearchAdv.borrarTodoMap();
				
				mapSearchAdv.addPushpin(lat,lon,'http://www.reifax.com/img/man_location.png','');
				
				var _xpoints=new Array();
				var factor=(distancia*0.015)/0.5;
				latMin=lat-(factor);
				latMax=lat+(factor);
				lonMin=lon-(factor);
				lonMax=lon+(factor);
				_xpoints.push(getLatLon(latMin,lonMin));
				_xpoints.push(getLatLon(latMax,lonMax));
				document.getElementById('mapa_search_latlongAdv').value=latMin+','+lonMin+'/'+latMax+','+lonMax;
				
				mapSearchAdv.points2shape(_xpoints);
				mapSearchAdv.getCenterPins();
			}
				//Ext.Msg.alert("Error",'latitude '+lat+' longitude '+lon);
			doSearchByFilter('GPS');
			Ext.getCmp('ventanalocation').close();
		}
	});
	
}

function error_callbackLocation(p){
	loading_win.hide();
	Ext.getCmp('ventanalocation').close();
	Ext.Msg.alert("Error",'error='+p.message);
}