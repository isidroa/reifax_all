<?php 
$_SERVERXIMA="http://leads.reifax.com/";
//if(isset($_GET['webuser'])){$r=true;}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES CON MAPA
	function tagHeadHeader($mapa=true)
	{
		$link = /*$secure ? 'https' : */'http';
	?>
    	<title>REIFAX | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
         <meta content='IE=8' http-equiv='X-UA-Compatible'/>
<?php
		if(isset($_COOKIE['datos_usr']['cleancache']) && $_COOKIE['datos_usr']['cleancache']=='Y')
		{
?>		
		<meta http-equiv="cache-control" content="no-cache"> 
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<?php
		}
?> 
        <meta http-equiv="Content-Language" content="EN-US"> 
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/img/toolbar/home.png">
		
        <?php if($mapa){?>
        <!--<script charset="UTF-8" type="text/javascript" src="<?php echo $link;?>://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>-->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4&libraries=drawing"></script>
        <?php }?>
		<!-- ExtJS -->
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
        
        <!-- JQuery -->
        <script type="text/javascript" language="javascript" src="http://www.reifax.com/includes/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="http://www.reifax.com/includes/jquery.carouFredSel-4.3.3-packed.js"></script>
        
        <!-- htmlEditor -->
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.Word.js"></script>
      	
        <!-- data view plugins -->
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/DataView-more.js"></script>
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/data-view.css"/>
        
        <!-- fileuploadinput -->
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/FileUploadField.js"></script>
        
        <!-- geolocalización -->
     	<!--<script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/geo.js"></script>
        
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/fileuploadfield.css"/>
    	<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/includes/css/layout2.css" />
		<link rel="stylesheet" type="text/css" href="/includes/css/properties_css.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/includes/css/advertising_css.css" />
<?php 	//handlingError();
	}
	
	function topLoginHeader($advertising=true, $aboutus=true, $login=true, $register=true, $usa=true, $contactus=true, $livesupport=true, $trainning=true,$tickets=true)
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		$link = /*$secure ? 'https' : */'http';
		
		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
			$result=mysql_query($que) or die($que.mysql_error());
			$r=mysql_fetch_array($result);
			$htmls=$r['master_htmls'];?>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
	                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
    	  	</table>
           	<p>&nbsp;</p>
                               
		<? }else{   ?>        
			<div align="center" class="productMenuReiFax">
            
            	<div class="logoProduct menuBlock">
                	<a href="properties_search.php<?php echo $realtor ? '?webuser='.$realtorid : ($investor ? '?webowner='.$investorid : '');?>" target="_self">
						<?php if ($realtor){ 
                            $query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
                            $result = mysql_query($query) or die($query.mysql_error());
                            $datos_usr=mysql_fetch_array($result);
                            $photo=$datos_usr['profimg'];
                            $photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?>://www.reifax.com/<?php echo "$photo2";?>" width="240" alt="Company Realtor Web">
                        <?php } elseif ($investor){ 
                            $query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
                            $result = mysql_query($query) or die($query.mysql_error());
                            $datos_usr=mysql_fetch_array($result);
                            $photo=$datos_usr['profimg'];
                            $photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?>://www.reifax.com/<?php echo "$photo2";?>" width="240" alt="Company Investor Web">
                        <?php } elseif ($webmaster){ 
                            $query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
                            $result = mysql_query($query) or die($query.mysql_error());
                            $datos_usr=mysql_fetch_array($result);
                            $photo=$datos_usr['profimg'];
                            $photo2=$datos_usr['companyimg'];?><img src="<?php echo $link;?>://www.reifax.com/<?php echo "$photo2";?>" width="240" alt="Company Webmaster Web">
                        <?php }else{?> 
                            <div class="headerContainer">
                            <?php 
								$imgUrl = $link.'://www.reifax.com/img/';
								$sufijo = explode('.',$_SERVER['SERVER_NAME']);
								$activeProduct = $sufijo[0]=='developer' || $sufijo[0]=='www' ? 'professional' : $sufijo[0];
								
								if($activeProduct=='professional' || $activeProduct=='professionalm') $imgUrl .= 'Professional-white.png';
								elseif($activeProduct=='platinum' || $activeProduct=='platinumm') $imgUrl .= 'Platinum-white.png';
								elseif($activeProduct=='residential' || $activeProduct=='residentialm') $imgUrl .= 'Residential-white.png';
								elseif($activeProduct=='buyerspro' || $activeProduct=='buyersprom') $imgUrl .= 'buyerspro-white.png';
								elseif($activeProduct=='dealfinder' || $activeProduct=='dealfinderm') $imgUrl .= 'homefinder-white.png';
								elseif($activeProduct=='leads' || $activeProduct=='leadsm') $imgUrl .= 'leadsgenerator-white.png';
								else $imgUrl .= 'foreclosures-foreclosed-homes1.png';
							?>
                            <img src="<?php echo $imgUrl;?>" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                            </div>
                        <?php } ?>
                    </a>
                </div>
                
                <div class="otherProductMenu menuBlock">
                	<?php         
						$query='SELECT * 
						FROM xima.permission 
						WHERE userid='.$_COOKIE['datos_usr']['USERID'];
						$result = mysql_query($query) or die($query.mysql_error());
						$r = mysql_fetch_assoc($result);    
						
						$listProduct = array();
						
						if($r['professional']==1 || $r['professional_esp']==1) {
							if($activeProduct!='professional' && $activeProduct!='professionalm') $listProduct[] = array($link.'://www.reifax.com/img/Professional.png','http://professionalm.reifax.com');
							if($activeProduct!='dealfinder' && $activeProduct!='dealfinderm') $listProduct[] = array($link.'://www.reifax.com/img/homefinder.png','http://dealfinderm.reifax.com');
							if($activeProduct!='buyerspro' && $activeProduct!='buyersprom') $listProduct[] = array($link.'://www.reifax.com/img/buyerspro.png','http://buyersprom.reifax.com');
							if($activeProduct!='leads' && $activeProduct!='leadsm') $listProduct[] = array($link.'://www.reifax.com/img/leadsgenerator.png','http://leadsm.reifax.com');
						}
						elseif($r['platinum']==1){ 
							if($activeProduct!='platinum' && $activeProduct!='platinumm') $listProduct[] = array($link.'://www.reifax.com/img/Platinum.png','http://platinumm.reifax.com');
							if($activeProduct!='dealfinder' && $activeProduct!='dealfinderm') $listProduct[] = array($link.'://www.reifax.com/img/homefinder.png','http://dealfinderm.reifax.com');
							if($activeProduct!='buyerspro' && $activeProduct!='buyersprom') $listProduct[] = array($link.'://www.reifax.com/img/buyerspro.png','http://buyersprom.reifax.com');
							if($activeProduct!='leads' && $activeProduct!='leadsm') $listProduct[] = array($link.'://www.reifax.com/img/leadsgenerator.png','http://leadsm.reifax.com');
						}
						else{
							if($r['residential']==1 && $activeProduct!='residential' && $activeProduct!='residentialm')
								$listProduct[] = array($link.'://www.reifax.com/img/Residential.png','http://residentialm.reifax.com');
							if($r['homefinder']==1 && $activeProduct!='dealfinder' && $activeProduct!='dealfinderm')
								$listProduct[] = array($link.'://www.reifax.com/img/homefinder.png','http://dealfinderm.reifax.com');
							if($r['buyerspro']==1 && $activeProduct!='buyerspro' && $activeProduct!='buyersprom')
								$listProduct[] = array($link.'://www.reifax.com/img/buyerspro.png','http://buersprom.reifax.com');
							if($r['leadsgenerator']==1 && $activeProduct!='leads' && $activeProduct!='leadsm')
								$listProduct[] = array($link.'://www.reifax.com/img/leadsgenerator.png','http://leadsm.reifax.com');
						}
						
						
						foreach($listProduct as $k => $val){
							
					?>
                    <div class="otherProductItem">
                    	<ul class="menu">
                        	<li class="menu-item">
                            	<a href="<?php echo $val[1];?>" target="_self">
                                	<img src="<?php echo $val[0];?>" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php }?>
                    
                </div>
                
                <div class="ticketMenu moreMenu menuBlock">
                    <a href="http://www.reifax.com/settings/tickets.php" target="_blank">
                        <img id="iconTicket" src="http://www.reifax.com/img/menu/new/ticket.png" height="65px">
                    </a>                
                </div>
                <script>
				$.ajax({
					url:'check_ticket.php',
					dataType: 'JSON',
					success:function (res){
						if(res.msg){
							$('#iconTicket').attr('src',"<?php echo $link;?>://www.reifax.com/img/menu/new/ticketRed.png");
						}
						else{
							$('#iconTicket').attr('src',"<?php echo $link;?>://www.reifax.com/img/menu/new/ticket.png");
						}
					}
				})
				</script>
                
                <div class="supportMenu moreMenu menuBlock"></div>
                <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat; z-index:200; position:absolute; top:0px; right:123px;"></div><div id="sdSlBh"></div> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <noscript><div style="display:inline"><a href="<?php echo $link;?>://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
                
                <div class="logoutMenu moreMenu menuBlock" onClick="session_release(); return false;" title="Log Out"></div>
                
                <div id="settingsMore" class="settingsMenu moreMenu menuBlock">
                    <div class="more-menu-container">
                        <div class="menu-block">
                        	<ul id="menu-more-3" class="menu">
                                <li class="menu-item"><a href="javascript:settingsTabsAdd('ReiFax Help','helpTabID','http://www.reifax.com/help/')">ReiFax Help</a></li>
                                <li class="menu-item"><a href="javascript:settingsTabsAdd('ReiFax Default','reifaxDefaultTabID','http://www.reifax.com/mysetting_tabs/myaccount_tabs/defaultcounty.php',true)">ReiFax Default</a></li>
                                <li class="menu-item"><a href="javascript:settingsTabsAdd('Personal Data','personalDataTabID','http://www.reifax.com/settings/personalData.php')">Personal Data</a></li>
                                <li class="menu-item"><a href="javascript:settingsTabsAdd('Profile Data','profileDataTabID','http://www.reifax.com/settings/profileData.php')">Profile Data</a></li>
                                <li class="menu-item"><a href="javascript:settingsTabsAdd('Credit Card','creditCardTabID','http://www.reifax.com/settings/creditCard.php')">Credit Card</a></li>
                        	</ul>
                       	</div>
                    </div>
                </div>
                
                <script>				
				$('#settingsMore').hover(function(){
					$('#settingsMore .more-menu-container').show();
				}, function(){
					$('#settingsMore .more-menu-container').hide();
				});
				
				function settingsTabsAdd(title, id, url,tabDirect){
					if(document.getElementById(id)){
						var tab = tabs.getItem(id);
						tabs.remove(tab);
					}
					if(tabDirect){
						tabs.add({
							title: title,
							id: id,
							autoLoad: { url: url, scripts:true},
							closable: true
						}).show();
					}else{
						tabs.add({
							title: title,
							id: id,
							html: '<iframe width="980px" height="900px" src="'+url+'?reifaxframemenu=true">',
							closable: true
						}).show();
					}
				}
				
				</script>
				<style>
					iframe{
						border: none;
					}
				</style>
                
            </div>
<?php 
 } }

	
	 //infromacion para el google-analytics
	function googleanalytics()
	{
?>
        <script type="text/javascript">		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-3129734-3']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
<?php 
	}
?>			