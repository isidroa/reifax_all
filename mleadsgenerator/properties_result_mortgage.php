<?php 
//print_r($_COOKIE['query_search']);
$_SERVERXIMA="http://www.reifax.com/";
$realtor=$_POST['userweb']=="false" ? false:true;	
$realtorid=$_POST['realtorid'];
?> 
<div align="left" style="height:100%">
	<div id="body_central" style="height:100%">
		<div id="tabsResultMortgage"></div>
	</div>
</div>

<script>
var discardLocation = null;
var analyze_type = 'null';
var analyze_ptype = 'null';
var tabsResult=null;
var ancho=980;
var selected_dataR = new Array();
var AllCheckR=false;
var selected_dataRFG = new Array();
var AllCheckRFG=false;
if(user_loged) ancho=system_width;

var mapResult = mapResultAdv = mapResultAdvFG = null;
var icon_result=icon_mylabel=false;
var ResultTemplate=-1;
var ResultTemplateFG=-1;
if(realtor_block!=false){
	var icon_result=true; 
	var icon_mylabel=true;
}
if(user_web!=false)	var icon_mylabel=true;

var tabMortgage = new Ext.TabPanel({
	title: 'Mortgage',
	renderTo: 'tabsResultMortgage',
	id: 'MortgageResult',
	activeTab: 0,
	width: ancho,
	height: tabs.getHeight(),
	plain:true,
	enableTabScroll:true,
	defaults:{	autoScroll: false},
	items: [
	{
		title: 'Upsidedown',
	
		autoLoad: {
			url: 'result_tabs/properties_advance_result.php?typeTab=mortgage&subTypeTab=upsidedown',  
			timeout: 10800, 
			scripts: true, 
			params: {
				userweb: '<?php echo $_POST['userweb'];?>', 
				realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>,
				systemsearch: '<?php echo $_POST['systemsearch'];?>',
				title: 'Upsidedown Details'
			}
		},
		
		tbar: new Ext.Toolbar({
			cls: 'no-border',
			width: 'auto',
			items: [' ',
			{
				 tooltip: 'Click to View Legend',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/legend.png',
				  hidden:icon_result,
				 handler: function(){
					var dataLegend = [
						['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
						['A-F','http://www.reifax.com/img/houses/verdel.png','Active Forclosed'],
						['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Forclosed Sold'],
						['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Forclosed'],
						['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
						['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
						['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Forclosed'],
						['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
						['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
						['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
						['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
						['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Forclosed'],
						['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
						['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
						['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
						['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
						['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Forclosed'],
						['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
						['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
						['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
						['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
					];
					 
					 var store = new Ext.data.ArrayStore({
						idIndex: 0,
						fields: [
							'status', 'url', 'description'
						]
					 });
					
					store.loadData(dataLegend);
					
					 var listView = new Ext.list.ListView({
						store: store,
						multiSelect: false,
						emptyText: 'No Legend to display',
						columnResize: false,
						columnSort: false,
						columns: [{
							header: 'Color',
							width: .15,
							dataIndex: 'url',
							tpl: '<img src="{url}">'
						},{
							header: 'Status',
							width: .2,
							dataIndex: 'status'
						},{
							header: 'Description',
							dataIndex: 'description'
						}]
					});
					
					var win = new Ext.Window({
						
						layout      : 'fit',
						width       : 370,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: listView,
			
						buttons: [{
							text     : 'Print',
							handler  : function(){
								var htmlTag = new Array();
								var i=0;
								
								htmlTag.push('<table>'+
									'<tr>'+
										'<td>Color</td>'+
										'<td>Status</td>'+
										'<td>Description</td>'+
									'</tr>');
								
								while(i<dataLegend.length){
									htmlTag.push(
									'<tr>'+
										'<td><img src="'+dataLegend[i][1]+'" /></td>'+
										'<td>'+dataLegend[i][0]+'</td>'+
										'<td>'+dataLegend[i][2]+'</td>'+
									'</tr>');
									i++;
								}
								htmlTag.push('</table>');

								var WindowObject = window.open('', "TrackHistoryData", 
													  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
								
								WindowObject.document.write(htmlTag);
								WindowObject.document.close();
								WindowObject.focus();
								WindowObject.print();
								WindowObject.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				 }
			},{
				tooltip: 'Click to Print Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					
					var parcelids_res='';
					if(!AllCheckRmortgageupsidedown){
						var results = selected_dataRmortgageupsidedown;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
						}
					}
					
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Printing Report...',
						url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgageupsidedown', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							parcelids_res:parcelids_res,
							template_res:-1
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','file can not be generated');
							loading_win.hide();
						},
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);
						
						}                                
					});
				}
		
			},{
				tooltip: 'Click to Excel Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					
					var ownerShow='false';
					var parcelids_res='';
					if(!AllCheckRmortgageupsidedown){
						var results = selected_dataRmortgageupsidedown;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
						}
					}
		
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Excel Report...',
						url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgageupsidedown', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							ownerShow: ownerShow,
							parcelids_res:parcelids_res,
							template_res:-1
						},
	
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','file can not be generated');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			},{
				 tooltip: 'Click to Print Labels',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 handler: function(){

					var parcelids_res='';
					if(!AllCheckRmortgageupsidedown){
						var results = selected_dataRmortgageupsidedown;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Labels','You must check-select the records to be printed.'); return false;
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgageupsidedown',
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 400,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: parcelids_res
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										//Ext.Msg.alert("Failure", action.result.pdf);
										var url='http://www.reifax.com/'+action.result.pdf;
										loading_win.hide();
										window.open(url);
										//window.open(url,'Print Labels',"fullscreen",'');
									},
									failure: function(form, action) {
										Ext.Msg.alert("Failure", action.result.msg);
										loading_win.hide();
									}
								});
							}
						},{
							text: 'Cancel',
							handler  : function(){
								simple.getForm().reset();
							}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 340,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			},'->',{
				tooltip: 'Click to Close Mortgage',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/cancel.png',
				
				handler: function(){
					var tab = tabs.getItem('resultMortgageTab');
					tabs.remove(tab);
				}
			}],
			autoShow: true
		})
	},{
		title: 'With Equity',
	
		autoLoad: {
			url: 'result_tabs/properties_advance_result.php?typeTab=mortgage&subTypeTab=withEquity', 
			timeout: 10800, 
			scripts: true, 
			params: {
				userweb: '<?php echo $_POST['userweb'];?>', 
				realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>,
				systemsearch: '<?php echo $_POST['systemsearch'];?>',
				title: 'With Equity Details'
			}
		},
		
		tbar: new Ext.Toolbar({
			cls: 'no-border',
			width: 'auto',
			items: [' ',
			{
				 tooltip: 'Click to View Legend',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/legend.png',
				  hidden:icon_result,
				 handler: function(){
					var dataLegend = [
						['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
						['A-F','http://www.reifax.com/img/houses/verdel.png','Active Forclosed'],
						['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Forclosed Sold'],
						['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Forclosed'],
						['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
						['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
						['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Forclosed'],
						['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
						['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
						['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
						['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
						['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Forclosed'],
						['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
						['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
						['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
						['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
						['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Forclosed'],
						['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
						['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
						['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
						['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
					];
					 
					 var store = new Ext.data.ArrayStore({
						idIndex: 0,
						fields: [
							'status', 'url', 'description'
						]
					 });
					
					store.loadData(dataLegend);
					
					 var listView = new Ext.list.ListView({
						store: store,
						multiSelect: false,
						emptyText: 'No Legend to display',
						columnResize: false,
						columnSort: false,
						columns: [{
							header: 'Color',
							width: .15,
							dataIndex: 'url',
							tpl: '<img src="{url}">'
						},{
							header: 'Status',
							width: .2,
							dataIndex: 'status'
						},{
							header: 'Description',
							dataIndex: 'description'
						}]
					});
					
					var win = new Ext.Window({
						
						layout      : 'fit',
						width       : 370,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: listView,
			
						buttons: [{
							text     : 'Print',
							handler  : function(){
								var htmlTag = new Array();
								var i=0;
								
								htmlTag.push('<table>'+
									'<tr>'+
										'<td>Color</td>'+
										'<td>Status</td>'+
										'<td>Description</td>'+
									'</tr>');
								
								while(i<dataLegend.length){
									htmlTag.push(
									'<tr>'+
										'<td><img src="'+dataLegend[i][1]+'" /></td>'+
										'<td>'+dataLegend[i][0]+'</td>'+
										'<td>'+dataLegend[i][2]+'</td>'+
									'</tr>');
									i++;
								}
								htmlTag.push('</table>');

								var WindowObject = window.open('', "TrackHistoryData", 
													  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
								
								WindowObject.document.write(htmlTag);
								WindowObject.document.close();
								WindowObject.focus();
								WindowObject.print();
								WindowObject.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				 }
			},{
				tooltip: 'Click to Print Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					
					var parcelids_res='';
					if(!AllCheckRmortgagewithEquity){
						var results = selected_dataRmortgagewithEquity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
						}
					}
					
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Printing Report...',
						url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewithEquity', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							parcelids_res:parcelids_res,
							template_res:-1
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','file can not be generated');
							loading_win.hide();
						},
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);
						
						}                                
					});
				}
		
			},{
				tooltip: 'Click to Excel Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					
					var ownerShow='false';
					var parcelids_res='';
					if(!AllCheckRmortgagewithEquity){
						var results = selected_dataRmortgagewithEquity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
						}
					}
		
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Excel Report...',
						url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewithEquity', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							ownerShow: ownerShow,
							parcelids_res:parcelids_res,
							template_res:-1
						},
	
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','file can not be generated');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			},{
				 tooltip: 'Click to Print Labels',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 handler: function(){

					var parcelids_res='';
					if(!AllCheckRmortgagewithEquity){
						var results = selected_dataRmortgagewithEquity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Labels','You must check-select the records to be printed.'); return false;
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewithEquity',
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 400,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: parcelids_res
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											//Ext.Msg.alert("Failure", action.result.pdf);
											var url='http://www.reifax.com/'+action.result.pdf;
											loading_win.hide();
											window.open(url);
											//window.open(url,'Print Labels',"fullscreen",'');
										},
										failure: function(form, action) {
											Ext.Msg.alert("Failure", action.result.msg);
											loading_win.hide();
										}
									});
								}
						},{
							text: 'Cancel',
							handler  : function(){
									simple.getForm().reset();
								}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 340,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			},'->',{
				tooltip: 'Click to Close Mortgage',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/cancel.png',
				
				handler: function(){
					var tab = tabs.getItem('resultMortgageTab');
					tabs.remove(tab);
				}
			}],
			autoShow: true
		})
	},{
		title: 'With 30%+ Equity',
	
		autoLoad: {
			url: 'result_tabs/properties_advance_result.php?typeTab=mortgage&subTypeTab=with30Equity', 
			timeout: 10800, 
			scripts: true, 
			params: {
				userweb: '<?php echo $_POST['userweb'];?>', 
				realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>,
				systemsearch: '<?php echo $_POST['systemsearch'];?>',
				title: 'With 30%+ Equity Details'
			}
		},
		
		tbar: new Ext.Toolbar({
			cls: 'no-border',
			width: 'auto',
			items: [' ',
			{
				 tooltip: 'Click to View Legend',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/legend.png',
				  hidden:icon_result,
				 handler: function(){
					var dataLegend = [
						['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
						['A-F','http://www.reifax.com/img/houses/verdel.png','Active Forclosed'],
						['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Forclosed Sold'],
						['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Forclosed'],
						['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
						['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
						['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Forclosed'],
						['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
						['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
						['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
						['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
						['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Forclosed'],
						['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
						['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
						['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
						['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
						['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Forclosed'],
						['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
						['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
						['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
						['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
					];
					 
					 var store = new Ext.data.ArrayStore({
						idIndex: 0,
						fields: [
							'status', 'url', 'description'
						]
					 });
					
					store.loadData(dataLegend);
					
					 var listView = new Ext.list.ListView({
						store: store,
						multiSelect: false,
						emptyText: 'No Legend to display',
						columnResize: false,
						columnSort: false,
						columns: [{
							header: 'Color',
							width: .15,
							dataIndex: 'url',
							tpl: '<img src="{url}">'
						},{
							header: 'Status',
							width: .2,
							dataIndex: 'status'
						},{
							header: 'Description',
							dataIndex: 'description'
						}]
					});
					
					var win = new Ext.Window({
						
						layout      : 'fit',
						width       : 370,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: listView,
			
						buttons: [{
							text     : 'Print',
							handler  : function(){
								var htmlTag = new Array();
								var i=0;
								
								htmlTag.push('<table>'+
									'<tr>'+
										'<td>Color</td>'+
										'<td>Status</td>'+
										'<td>Description</td>'+
									'</tr>');
								
								while(i<dataLegend.length){
									htmlTag.push(
									'<tr>'+
										'<td><img src="'+dataLegend[i][1]+'" /></td>'+
										'<td>'+dataLegend[i][0]+'</td>'+
										'<td>'+dataLegend[i][2]+'</td>'+
									'</tr>');
									i++;
								}
								htmlTag.push('</table>');

								var WindowObject = window.open('', "TrackHistoryData", 
													  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
								
								WindowObject.document.write(htmlTag);
								WindowObject.document.close();
								WindowObject.focus();
								WindowObject.print();
								WindowObject.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				 }
			},{
				tooltip: 'Click to Print Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					
					var parcelids_res='';
					if(!AllCheckRmortgagewith30Equity){
						var results = selected_dataRmortgagewith30Equity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
						}
					}
					
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Printing Report...',
						url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewith30Equity', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							parcelids_res:parcelids_res,
							template_res:-1
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','file can not be generated');
							loading_win.hide();
						},
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);
						
						}                                
					});
				}
		
			},{
				tooltip: 'Click to Excel Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					
					var ownerShow='false';
					var parcelids_res='';
					if(!AllCheckRmortgagewith30Equity){
						var results = selected_dataRmortgagewith30Equity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
						}
					}
		
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Excel Report...',
						url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewith30Equity', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							ownerShow: ownerShow,
							parcelids_res:parcelids_res,
							template_res:-1
						},
	
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','file can not be generated');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			},{
				 tooltip: 'Click to Print Labels',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 handler: function(){

					var parcelids_res='';
					if(!AllCheckRmortgagewith30Equity){
						var results = selected_dataRmortgagewith30Equity;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Labels','You must check-select the records to be printed.'); return false;
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>&search_filter_groupby_type=mortgagewith30Equity',
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 400,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: parcelids_res
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											//Ext.Msg.alert("Failure", action.result.pdf);
											var url='http://www.reifax.com/'+action.result.pdf;
											loading_win.hide();
											window.open(url);
											//window.open(url,'Print Labels',"fullscreen",'');
										},
										failure: function(form, action) {
											Ext.Msg.alert("Failure", action.result.msg);
											loading_win.hide();
										}
									});
								}
						},{
							text: 'Cancel',
							handler  : function(){
									simple.getForm().reset();
								}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 340,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			},'->',{
				tooltip: 'Click to Close Mortgage',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/cancel.png',
				
				handler: function(){
					var tab = tabs.getItem('resultMortgageTab');
					tabs.remove(tab);
				}
			}],
			autoShow: true
		})
	},{
		title: 'Lender',
	
		autoLoad: {
			url: 'result_tabs/properties_group_result.php?typeTab=mortgage&subTypeTab=lender', 
			timeout: 10800, 
			scripts: true, 
			params: {
				userweb: '<?php echo $_POST['userweb'];?>', 
				realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>,
				systemsearch: '<?php echo $_POST['systemsearch'];?>',
				title: 'Lender Details'
			}
		},
		
		tbar: new Ext.Toolbar({
			cls: 'no-border',
			width: 'auto',
			items: [' ',
			{
				 tooltip: 'Click to View Legend',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/legend.png',
				  hidden:icon_result,
				 handler: function(){
					var dataLegend = [
						['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
						['A-F','http://www.reifax.com/img/houses/verdel.png','Active Forclosed'],
						['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Forclosed Sold'],
						['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Forclosed'],
						['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
						['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
						['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Forclosed'],
						['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
						['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
						['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
						['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
						['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Forclosed'],
						['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
						['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
						['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
						['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
						['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Forclosed'],
						['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
						['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
						['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
						['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
					];
					 
					 var store = new Ext.data.ArrayStore({
						idIndex: 0,
						fields: [
							'status', 'url', 'description'
						]
					 });
					
					store.loadData(dataLegend);
					
					 var listView = new Ext.list.ListView({
						store: store,
						multiSelect: false,
						emptyText: 'No Legend to display',
						columnResize: false,
						columnSort: false,
						columns: [{
							header: 'Color',
							width: .15,
							dataIndex: 'url',
							tpl: '<img src="{url}">'
						},{
							header: 'Status',
							width: .2,
							dataIndex: 'status'
						},{
							header: 'Description',
							dataIndex: 'description'
						}]
					});
					
					var win = new Ext.Window({
						
						layout      : 'fit',
						width       : 370,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: listView,
			
						buttons: [{
							text     : 'Print',
							handler  : function(){
								var htmlTag = new Array();
								var i=0;
								
								htmlTag.push('<table>'+
									'<tr>'+
										'<td>Color</td>'+
										'<td>Status</td>'+
										'<td>Description</td>'+
									'</tr>');
								
								while(i<dataLegend.length){
									htmlTag.push(
									'<tr>'+
										'<td><img src="'+dataLegend[i][1]+'" /></td>'+
										'<td>'+dataLegend[i][0]+'</td>'+
										'<td>'+dataLegend[i][2]+'</td>'+
									'</tr>');
									i++;
								}
								htmlTag.push('</table>');

								var WindowObject = window.open('', "TrackHistoryData", 
													  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
								
								WindowObject.document.write(htmlTag);
								WindowObject.document.close();
								WindowObject.focus();
								WindowObject.print();
								WindowObject.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				 }
			},{
				tooltip: 'Click to Print Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					
					var parcelids_res='';
					if(!AllCheckRG_lender){
						var results = selected_dataRG_lender;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
						}
					}
					
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Printing Report...',
						url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>&resultby=lender&filter_buyer_owns=0&search_filter_groupby_type=mortgagelender', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							parcelids_res:parcelids_res,
							template_res:-1,
							groupbylevel: 1
						},
						
						failure:function(response,options){
							Ext.MessageBox.alert('Warning','file can not be generated');
							loading_win.hide();
						},
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);
						
						}                                
					});
				}
		
			},{
				tooltip: 'Click to Excel Report',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					
					var ownerShow='false';
					var parcelids_res='';
					if(!AllCheckRG_lender){
						var results = selected_dataRG_lender;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
						}
					}
		
					loading_win.show();
					Ext.Ajax.request({  
						waitMsg: 'Excel Report...',
						url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>&resultby=lender&filter_buyer_owns=0&search_filter_groupby_type=mortgagelender', 
						method: 'POST', 
						timeout :600000,
						params: {
							userweb:user_web,
							realtorid:user_webid,
							ownerShow: ownerShow,
							parcelids_res:parcelids_res,
							template_res:-1,
							groupbylevel: 1
						},
	
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','file can not be generated');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			},{
				 tooltip: 'Click to Print Labels',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 handler: function(){

					var parcelids_res='';
					if(!AllCheckRG_lender){
						var results = selected_dataRG_lender;
						if(results.length > 0){
							parcelids_res=results[0];
							for(i=1; i<results.length; i++){
								parcelids_res+=','+results[i];
							}
						}else{
							Ext.MessageBox.alert('Print Labels','You must check-select the records to be printed.'); return false;
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>&resultby=lender&filter_buyer_owns=0&search_filter_groupby_type=mortgagelender',
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 400,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: parcelids_res
							},{
								xtype: 'hidden',
								name: 'groupbylevel',
								value: 1
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											//Ext.Msg.alert("Failure", action.result.pdf);
											var url='http://www.reifax.com/'+action.result.pdf;
											loading_win.hide();
											window.open(url);
											//window.open(url,'Print Labels',"fullscreen",'');
										},
										failure: function(form, action) {
											Ext.Msg.alert("Failure", action.result.msg);
											loading_win.hide();
										}
									});
								}
						},{
							text: 'Cancel',
							handler  : function(){
									simple.getForm().reset();
								}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 340,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			},{
				 tooltip: 'Click to Filter',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/filter.png',
				 handler: function(){
					
					var simple = new Ext.FormPanel({
						labelWidth: 50, 
						url:'',
						frame: true,
						title: 'Filter Result',
						bodyStyle:'padding:5px 5px 0',
						width: 380,
						waitMsgTarget : 'Procesing Filter...',
						
						items: [{
								xtype: 'datefield',
								name : 'filterDate',
								fieldLabel: 'Date',
								emptyText: 'Filter Date Format yyyymmdd',
								format: 'Ymd',
								width: 200,
								value: filterDate_lender
							},{
								xtype: 'textfield',
								name: 'filterLender',
								fieldLabel: 'Lender',
								emptyText: 'Filter Lender',
								width: 200,
								value: filterName_lender
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
								var values = simple.getForm().getValues();
								filterDate_lender = values.filterDate;
								filterName_lender = values.filterLender;
								storeRG_lender.setBaseParam('search_filter_groupby_date', filterDate_lender);
								storeRG_lender.setBaseParam('search_filter_groupby_name', filterName_lender);
								win.close();
								Ext.getCmp('pagingRG_lender').doLoad(0); 
							}
						},{
							text: 'Cancel',
							handler  : function(){
								simple.getForm().reset();
								filterDate_lender = null;
								filterName_lender = null;
								storeRG_lender.setBaseParam('search_filter_groupby_date', filterDate_lender);
								storeRG_lender.setBaseParam('search_filter_groupby_name', filterName_lender);
								win.close();
								Ext.getCmp('pagingRG_lender').doLoad(0);
							}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 270,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			},'->',{
				tooltip: 'Click to Close Mortgage',
				cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				icon: 'http://www.reifax.com/img/cancel.png',
				
				handler: function(){
					var tab = tabs.getItem('resultMortgageTab');
					tabs.remove(tab);
				}
			}],
			autoShow: true
		})
	}
	]
});
</script>

