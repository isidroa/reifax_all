<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>

		<link type="text/css" rel="stylesheet" href="http://www.reifax.com/xima3/includes/galleryview-2.1.1/galleryview.css" />
		
        <script type="text/javascript" src="http://www.reifax.com/xima3/includes/jquery-1.4.4.min.js"></script>

		<script type="text/javascript" src="http://www.reifax.com/xima3/includes/galleryview-2.1.1/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/xima3/includes/galleryview-2.1.1/jquery.galleryview-2.1.1.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/xima3/includes/galleryview-2.1.1/jquery.timers-1.2.js"></script>
		

        <script type="text/javascript">

        $(document).ready(function(){
            $('#gallery').galleryView({
                gallery_width: 800,
                gallery_height: 600,
                frame_width: 120,
                frame_height: 90,
                pause_on_hover: true,
                border: '1px solid black',
                nav_theme:'dark'
            });
        });

    </script>
    </head>
    <?php
	$db_data=$_GET['db'];
	$pid=$_GET['pid'];
	if(isset($_GET['overview_comp'])) $overview_comp=true;

	include("../properties_conexion.php");
	conectarPorBD($db_data);

	function url_exists($url)
	{
		$url_info = parse_url($url);

		if (! isset($url_info['host']))
			return false;

		$port = (isset($url_info['post'])?$url_info['port']:80);

		if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr))
			return false;

		$uri = @$url_info['path'] . '?' . @$url_info['query'];

		$http = "HEAD $uri HTTP/1.1\r\n";
		$http .= "Host: {$url_info['host']}\r\n";
		$http .= "Connection: close\r\n\r\n";

		@fwrite($hwnd, $http);

		$response = fgets($hwnd);
		$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";

		fclose($hwnd);

		if (preg_match($response_match, $response, $matches)) {
			//print_r($matches);
			if ($matches[1] == 404)
				return false;
			else if ($matches[1] == 200)
				return true;
			else
				return false;

		} else {
			return false;
		}
	}

	$sql_fields="Select
	*
	FROM imagenes
	Where parcelid='$pid';";

	$result = mysql_query($sql_fields) or die(mysql_error());
	$fila= mysql_fetch_array($result, MYSQL_ASSOC);
	if(mysql_num_rows($result)>0){
		$arr_pics="";

		if($fila["letra"]=='Y')
			$mlnumber=$fila["mlnumber"];
		else
			$mlnumber=substr($fila["mlnumber"],1);

			if(url_exists($fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'])){
				$arr_pics.=$fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'];
				if($fila['nphotos']>1){
					for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
						if(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
							$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
					}
				}
			}else{
				$arr_pics.="img/nophoto.gif";
			}
		//}
	}
	mysql_free_result($result);

	$arr_data=explode(",",$arr_pics);
	//print_r($arr_data);
?>
    <body>	
        <div align="center">
			<div align="left" style="width:610px;">
				<ul id="gallery">
					<?php foreach($arr_data as $k => $img){?>
						<li><img src="<?php echo $img;?>" title="<?php echo 'Picture '.($k+1).' / '.count($arr_data);?>"></li>
					<?php }?>
				</ul>
			</div>
        </div>
    </body>
</html>
