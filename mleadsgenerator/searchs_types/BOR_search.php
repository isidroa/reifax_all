<div align="left" class="search_realtor_fondo" id="BOR_search_div" style="display:none;">
    <table border="0" cellpadding="0" cellspacing="0" style="font-size:12px; margin:auto;">		
        <tr class="search_realtor_titulo">    	    	
            <td colspan="4" width="120">Data</td>                
            <td colspan="6" width="240">Search By</td>                                
            <td colspan="2" width="130">State</td>         
            <td width="130">County</td>      	
        </tr>
        <tr>    	    	
            <td colspan="3" width="115"><select name="combo_search_types" id="BOR_combo_search_types" style="width:115px;" onchange="doSearchTypeFilter(this.value);">
           <? if (!$realtor && !$webmaster){ ?> 
            <option value="PR" >Public Records</option> 
               <?php }?>
               <option value="FS">For Sale</option>
            	<option value="FR" >For Rent</option>
              <? if (!$realtor && !$webmaster){ ?> 
           <option value="FO" >Foreclosures</option> 
            <?php }?>
            <option value="BO" >By Owner</option>
            <option value="BOR" selected="selected">By Owner Rent</option>
            </select></td>
            <td width="5">&nbsp;</td>
            <td colspan="5" width="235"><select name="combo_search_by" id="BOR_combo_search_by" style="width:105px;" onchange="doSearchByFilter(this.value);">
            <option value="LOCATION" <?php if($search_by_type=='LOCATION') echo 'selected="selected"';?>>Location</option>
			<!--<option value="MLNUMBER" <?php if($search_by_type=='MLNUMBER') echo 'selected="selected"';?>>Mlnumber</option>-->
            <option value="PARCELID" <?php if($search_by_type=='PARCELID') echo 'selected="selected"';?>>Parcelid</option>
            <option value="MAP" <?php if($search_by_type=='MAP') echo 'selected="selected"';?>>Map</option>
            </select></td>
            <td width="5">&nbsp;</td>
            <td width="125"><select name="state_search" id="BOR_state_search" style="width:120px;" onchange="doChangeState(this.value);">
            <?php
                $query='select idstate,state FROM xima.lsstate where is_showed="Y" order by state';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($state_search==$r['idstate'])
                        echo '<option value="'.$r['idstate'].'" selected="selected">'.$r['state'].'</option>';
                    else
                        echo '<option value="'.$r['idstate'].'">'.$r['state'].'</option>';
                }
            ?>
            </select></td>
            <td width="5">&nbsp;</td>
            <td width="130"><select name="county_search" id="BOR_county_search" style="width:125px;" onchange="if(search_by_type=='MAP') mapSearch.centerMapCounty(this.value,true); else mapSearch.centerMapCounty(this.value,false); search_county=this.value;">
            <?php
                $query='select idcounty,county FROM xima.lscounty where ximapro="Y" and idstate='.$state_search.' order by county';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($county_search==$r['idcounty'])
                        echo '<option value="'.$r['idcounty'].'" selected="selected">'.$r['county'].'</option>';
                    else
                        echo '<option value="'.$r['idcounty'].'">'.$r['county'].'</option>';
                }
            ?>
            </select></td>
        </tr>
        <tr class="search_realtor_titulo">    	    	
            <td colspan="10" width="360" >
                <span id="BOR_tsearch_l">Location</span>
            </td>                  
            <td colspan="2" width="130" id="BOR_tproptype" >Type</td>                
            <td width="130" id="BOR_tpropfore" >Foreclosure Status</td> 
                
        </tr>        
        <tr>             	
            <td colspan="9" width="355">
                <input type="text" name="search" id="BOR_search" size="40" maxlength="2048" style="font-size:17px;" value="Address, City or Zip Code" onfocus="colocarDefault('BOR_search',this.value);" onblur="colocarDefault2('BOR_search',this.value);">
            </td>                
            <td width="5">&nbsp;</td>                
            <td width="125">
                <select name="proptype" style="width:120px;" id="BOR_proptype"><option value="">Any Type</option><option value="01">Single Family</option><option value="04">Condo/Town/Villa</option><option value="08">Multi Family</option><option value="11">Commercial</option><option value="00">Vacant Land</option><option value="02">Mobile Home</option></select>
            </td>   	        
            <td width="5">&nbsp;</td>        
            <td width="130">
                
                 <select name="pendes" id="BOR_pendes" style="width:125px;"><option value="-1">Any</option><option value="N">No</option><option value="P">Pre-Foreclosed</option><option value="F">Foreclosed</option></select>
                
            </td>   	
        </tr>        
        <tr class="search_realtor_titulo">    	    	
            <td colspan="4" width="120" id="BOR_tprice" >Price Range</td>                
            <td colspan="2" width="70" id="BOR_tbeds" >Beds</td>                
            <td colspan="2" width="70" id="BOR_tbath" >Baths</td>                
            <td colspan="2" width="100" id="BOR_tsqft" >Sqft</td>          
            <td colspan="2" width="130"<?php echo"$estilo"; ?>>&nbsp;</td>               
            <td width="130" id="BOR_toccupied" style="color:#FFF; font-weight:bold;">&nbsp;</td>      	
        </tr>        
        <tr>        
            <td width="50">
                <span id="BOR_price_dol1" style="color:#FFF;vertical-align:top;">$</span><input id="BOR_price_low" style="width:40px;" type="text" name="price_low" value="min" size="6" onfocus="if(this.value=='min')this.value='';" onblur="if(this.value=='')this.value='min';">
            </td>        
            <td width="20" id="BOR_price_to" style="text-align:center;color:#FFF;">to</td>           
            <td width="50">
                <span id="BOR_price_dol2" style="color:#FFF;vertical-align:top;">$</span><input id="BOR_price_hi" style="width:40px;" type="text" name="price_hi" value="max" size="6" onfocus="if(this.value=='max')this.value='';" onblur="if(this.value=='')this.value='max';">
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="65">
                <select name="bed" id="BOR_bed"><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="65">
                <select name="bath" id="BOR_bath"><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="95">
                <select name="sqft" id="BOR_sqft"><option value="-1">Any</option><option value="250">250+</option><option value="500">500+</option><option value="1000">1,000+</option><option value="1250">1,250+</option><option value="1500">1,500+</option><option value="1750">1,750+</option><option value="2000">2,000+</option><option value="2250">2,250+</option><option value="2500">2,500+</option><option value="2750">2,750+</option><option value="3000">3,000+</option><option value="3250">3,250+</option><option value="3500">3,500+</option><option value="3750">3,750+</option><option value="4000">4,000+</option><option value="5000">5,000+</option><option value="10000">10,000+</option></select>
            </td>       	
            <td width="5">&nbsp;</td>            	
            <td width="125">&nbsp;</td>        
            <td width="5">&nbsp;</td>    	
            <td width="130" style="padding-top:0px;">
                	<img src="img/button_search.png" alt="Xima Usa Search" onClick="searchForm('BOR_search','xxx');">
            </td>   	
        </tr>    
    </table>
</div>