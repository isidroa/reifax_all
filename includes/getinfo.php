<?php
/**
 * getinfo.php
 *
 * Gets the info of a parcel ID
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 05.04.2011
 */

include "../properties_conexion.php"; 
//conectar();

$db     = strtolower($_GET['db']);
$pid    = $_GET['pid'];
$userid = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

conectarPorBD($db);

$query   = "SELECT * FROM fl{$db}.mlsresidential WHERE parcelid='$pid'";
$rs      = mysql_query($query);

if ($rs) {
	
	$row  = mysql_fetch_array($rs);
	
	$row['dateAccept'] = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")));
	$row['dateClose']  = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")));

	$query             = "SELECT platinum_invest FROM xima.ximausrs WHERE userid='$userid'";
	$rs                = mysql_query($query);
	$rpinv             = mysql_fetch_array($rs);
	
	$row['platinum']   = $rpinv[0];
	
	$query             = "SELECT owner from fl{$db}.psummary where parcelid='$pid'";
	$rs                = mysql_query($query);
	$rinfo             = mysql_fetch_array($rs);
	
	$row['sellname']   = $rinfo[0];

	$query              = "SELECT count(*) FROM xima.contracts_mailsettings WHERE userid = $userid";
	$rs                 = mysql_query($query);
	$mailInfo           = mysql_fetch_array($rs);

    $row['mailconf']    = 0;
	if ($mailInfo[0]>0)
		$row['mailconf'] = 1;
	

	$resp = array('success'=>'true','data'=>$row);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

if ($error != "") 
	$resp = array('success'=>'true','mensaje'=>$error);

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	