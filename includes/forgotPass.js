//**************************[ Maria Oliviera]
//FUNCION LLAMADA A TRAVES DE forgot_pass.php

function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
  		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
	
//Envia el email a forgorPass.php para revisar que exista, y si es asi, enviar la clave al usuario a traves del email ingresado
function sendMail(){
	var email;
	email= document.getElementById('usr_email').value; 
	
	ajax=new objetoAjax();
	ajax.open("POST", "includes/forgotPass.php", true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("email="+email);
	
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			var resultado=ajax.responseText;
			var msg=document.getElementById("errorMail");
			msg.innerHTML=resultado;
		}
	}
}

function set_codigo(){
	var codpro = document.getElementById('codpro').value;
	var codbank = document.getElementById('codbank').value;
	//alert(codpro+' '+codbank);
	var processor=true;
	var bank=true;
	
	if(codpro.length>0) processor=false;
	if(codbank.length>0) bank=false;
	
	var shortsaleid = document.getElementById('shortsaleid').value;
	
	var variables ='';
	
	ajax=new objetoAjax();
	ajax.open("POST", "master_ssale.php", true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	if(processor==false && bank==false)
		variables='_action=3&id='+shortsaleid+'&codpro='+codpro+'&codbank='+codbank;
	if(processor==true && bank==false)
		variables='_action=3&id='+shortsaleid+'&codbank='+codbank;
	if(processor==false && bank==true)
		variables='_action=3&id='+shortsaleid+'&codpro='+codpro;
	
	//alert(variables);
	ajax.send(variables);
	
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			alert(ajax.responseText);
			recargar();
		}
	}
}

function recargar(){
	document.location="ss_code.php?ssid="+document.getElementById('shortsaleid').value;
}

function add_shortsale(){
	var codigo = document.getElementById('code').value;
	var ss = document.getElementById('shortsaleid').value;
	var tipo = document.getElementById('tipo').value;
	
	if(codigo.length<=0 || ss.length<=0) alert('debe ingresar la data completa.');  

	ajax=new objetoAjax();
	ajax.open("POST", "master_ssale.php", true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send('_action=4&id='+ss+'&codigo='+codigo+'&tipo='+tipo);
	
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			alert(ajax.responseText);
			window.close();
			return true;
		}
	}
}