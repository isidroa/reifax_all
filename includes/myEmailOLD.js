	//Emails Functions
	function showAssignmentProperty(userid,agentid,idmail,nameContact,address){	
		var assignmentPropertyForm = new Ext.FormPanel({
			labelWidth: 75, 
			url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
			frame:true,
			bodyStyle:'padding:5px 5px 0',
			width: 350,
			defaults: {width: 230},
			defaultType: 'textfield', 
	
			items: [
				{
					inputType: 'hidden',
					name: 'userid',
					value: userid
				},
				{
					inputType: 'hidden',
					name: 'agentid',
					value: agentid
				},
				{
					inputType: 'hidden',
					name: 'idmail',
					value: idmail
				},
				{
					inputType: 'hidden',
					name: 'type',
					value: 'assignmentProperty'
				},
				new Ext.form.ComboBox({
					fieldLabel: 'Address',
					triggerAction: 'all',
					selectOnFocus: true,
					typeAhead: true,
					editable: true,
					typeAheadDelay: 10,
					minChars: 1,
					mode: 'remote',
					store: new Ext.data.JsonStore({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						forceSelection: true,
						root: 'records',
						baseParams		: {
							'userid'	:  userid,
							'agentid'	: agentid,
							'type'		: 'searchPropertyAgent'
						},
						id: 0,
						fields: [
							'parcelid',
							'address'
						]
					}),
					valueField: 'parcelid',
					displayField: 'address',
					name: 'address',
					hiddenName: 'parcelid',
					value: address
				})
			],
	
			buttons: [{
				text: 'Assign',
				handler: function(b){
					var form = b.findParentByType('form');
					form.getForm().submit({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						waitMsg: 'Assignment Property...',
						success: function(form, action) {
							//storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}}); 
							form.reset();
							b.findParentByType('window').close();
							
							if(document.getElementById('myfollowmail_propertiesInbox')) 
								storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}}); 
							if(document.getElementById('myfollowmail_propertiesOutbox')) 
								storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}}); 
							if(document.getElementById('myfollowmail_propertiesPProperty')) 
								storemyfollowmailPProperty.load({params:{start:0, limit:limitmyfollowmailPProperty}}); 
							
						},
						failure: function(form, action) {
							Ext.Msg.alert('Error', action.result.msg);
						}
					});
				}
			},{
				text: 'Cancel',
				handler: function(b){
					b.findParentByType('window').close();
				}
			}]
		});
		
		var assignmentPropertyWin=new Ext.Window({
			title: 'Assignment Property of '+nameContact,
			y: 255,
			width:360,
			resizable: false,
			modal: true,
			border:false,
			closable:true,
			//closeAction: 'hide',
			plain: true,
			items: [assignmentPropertyForm]
		});
		
		assignmentPropertyWin.show();
	}
	
	function showAssignmentOnlyProperty(userid,idmail,address){	
		var assignmentPropertyForm = new Ext.FormPanel({
			labelWidth: 75, 
			url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
			frame:true,
			bodyStyle:'padding:5px 5px 0',
			width: 350,
			defaults: {width: 230},
			defaultType: 'textfield', 
	
			items: [
				{
					inputType: 'hidden',
					name: 'userid',
					value: userid
				},
				{
					inputType: 'hidden',
					name: 'idmail',
					value: idmail
				},
				{
					inputType: 'hidden',
					name: 'type',
					value: 'assignmentProperty'
				},
				new Ext.form.ComboBox({
					fieldLabel: 'Address',
					triggerAction: 'all',
					selectOnFocus: true,
					typeAhead: true,
					editable: true,
					typeAheadDelay: 10,
					minChars: 1,
					mode: 'remote',
					store: new Ext.data.JsonStore({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						forceSelection: true,
						root: 'records',
						baseParams		: {
							'userid'	:  userid,
							'type'		: 'searchProperty'
						},
						id: 0,
						fields: [
							'parcelid',
							'address'
						]
					}),
					valueField: 'parcelid',
					displayField: 'address',
					name: 'address',
					hiddenName: 'parcelid',
					value: address
				})
			],
	
			buttons: [{
				text: 'Assign',
				handler: function(b){
					var form = b.findParentByType('form');
					form.getForm().submit({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						waitMsg: 'Assignment Property...',
						success: function(form, action) {
							//storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}}); 
							form.reset();
							b.findParentByType('window').close();
							
							selected_datamyfollowmailInbox 	= new Array();
							AllCheckmyfollowmailInbox = false;
							storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}}); 
							
						},
						failure: function(form, action) {
							Ext.Msg.alert('Error', action.result.msg);
						}
					});
				}
			},{
				text: 'Cancel',
				handler: function(b){
					b.findParentByType('window').close();
				}
			}]
		});
		
		var assignmentPropertyWin=new Ext.Window({
			title: 'Assignment Property',
			y: 255,
			width:360,
			resizable: false,
			modal: true,
			border:false,
			closable:true,
			//closeAction: 'hide',
			plain: true,
			items: [assignmentPropertyForm]
		});
		
		assignmentPropertyWin.show();
	}
	
	function getTemplateEmailResult(userid,idtemplate,pid,county,reply,forw,emailContentBefore){
		var emailContent = emailContentBefore ? emailContentBefore : '';
		
		Ext.Ajax.request( 
		{  
			waitMsg: 'Checking...',
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			method: 'POST',
			timeout :600000,
			params: { 
				type: 'getTemplateEmail',
				userid: userid,
				idtemplate: idtemplate,
				pid: pid,
				county: county
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','ERROR');
			},
			
			success:function(response,options){
				var e = Ext.decode(response.responseText);
				var subject = '';
				if(reply==true) subject += 'Re: ';
				if(forw==true) subject += 'Fwd: ';
				
				subject += e.subject;
				
				Ext.getCmp('emailContentArea').setValue(e.body+'<br><br><br><div style="width: 100%; height: 1px; border: 1px solid #000"></div><br>'+emailContent);
				
				Ext.getCmp('emailSubjectArea').setValue(subject);	
			}                                
		});
	}
	
	function mailCompose(userid,mailid,pid,reply,forwards,subject,email,typeFollow,county){
		var s = e = c = '';
		var title = 'Compose Email';
		var ind = 8;
		var att = {
			xtype: 'compositefield',
			fieldLabel: 'Attach',
			items: [
				{
					xtype: 'fileuploadfield',
					emptyText: 'Select a file',
					name: 'file'+ind,
					buttonText: 'Browse',
					width: 250
				},new Ext.Button({
					text: 'Another file',
					handler: function(){
						att.items[0].name = 'file'+(ind+1);
						
						ind++;
						form.insert(ind,att);
						form.doLayout();
					}
				})
			]
		};
		var emailContentBefore='';
		
		if(reply){
			s = 'Re: '+subject;
			e = email;
			title = 'Reply Email';
		}
		
		if(forwards){
			s = 'Fwd: '+subject;
			title = 'Forward Email';
		}
		
		if(reply || forwards){
			//Get Content of Email
			Ext.Ajax.request( 
			{  
				waitMsg: 'Checking...',
				url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
				method: 'POST',
				timeout :600000,
				params: { 
					type: 'getEmailContent',
					mailid: mailid
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','ERROR');
					console.log(response);
					console.log(options);
				},
				
				success:function(response,options){
					c = response.responseText;
					emailContentBefore = c;
					
					if(forwards) Ext.getCmp('emailContentArea').setValue('<br><br><br><div style="width: 100%; height: 1px; border: 1px solid #000"></div><br>'+c);
					else Ext.getCmp('emailContentArea').setValue('<br><br><br><br><div style="width: 100%; height: 1px; border: 1px solid #000"></div><br>'+email+' wrote:'+c);
				} 
			});
		}
		
		Ext.override(Ext.form.ComboBox, {
    		onTypeAhead : function(){
			}
		});
		
		var form = new Ext.form.FormPanel({
			fileUpload: true,
			baseCls: 'x-plain',
			labelWidth: 55,
			defaults: {
				xtype: 'textfield',
				anchor: '0'
			},
	
			items: [{
				xtype: 'hidden',
				name: 'type',
				value: 'composeEmail'
			},{
				xtype: 'hidden',
				name: 'typeFollow',
				value: typeFollow
			},{
				xtype: 'hidden',
				name: 'mailid',
				value: mailid
			},{
				xtype: 'hidden',
				name: 'pid',
				value: pid
			},{
				xtype: 'hidden',
				name:  'userid',
				value: userid
			},{
				xtype         : 'combo',
				mode          : 'remote',
				fieldLabel    : 'To',
				triggerAction : 'all',
				store         : new Ext.data.JsonStore({
					id:'email',
					root:'records',
					totalProperty:'total',
					baseParams: {
						'userid': userid,
						'type': 'ContactEmailsList'
					},
					fields:[
						{name:'agent', type:'string'},
						{name:'email', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
				}),
				displayField  : 'agent',
				valueField    : 'email',
				name          : 'toname',
				hiddenName    : 'to',
				allowBlank	  : false,
				forceSelection: false,
				editable	  : true,
				minChars	  : 2,								
				queryDelay	  : 1,
				autoSelect	  : false,
				typeAhead 	  : true,
				typeAheadDelay: 250,
				hiddenValue	  : e,
				value		  : e
			},
			
		/*	//Agregado por Luis R Castro Sugerencia 12254 20/05/2015
			{
				xtype         : 'combo',
				mode          : 'remote',
				fieldLabel    : 'CC',
				triggerAction : 'all',
				store         : new Ext.data.JsonStore({
					id:'email',
					root:'records',
					totalProperty:'total',
					baseParams: {
						'userid': userid,
						'type': 'ContactEmailsList'
					},
					fields:[
						{name:'agent', type:'string'},
						{name:'email', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
				}),
				displayField  : 'agent',
				valueField    : 'email',
				name          : 'ccname',
				hiddenName    : 'cc',
				allowBlank	  : true,
				forceSelection: false,
				editable	  : true,
				minChars	  : 2,								
				queryDelay	  : 1,
				autoSelect	  : false,
				typeAhead 	  : true,
				typeAheadDelay: 250,
				hiddenValue	  : e,
				value		  : e
			},
			{
				xtype         : 'combo',
				mode          : 'remote',
				fieldLabel    : 'BCC',
				triggerAction : 'all',
				store         : new Ext.data.JsonStore({
					id:'email',
					root:'records',
					totalProperty:'total',
					baseParams: {
						'userid': userid,
						'type': 'ContactEmailsList'
					},
					fields:[
						{name:'agent', type:'string'},
						{name:'email', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
				}),
				displayField  : 'agent',
				valueField    : 'email',
				name          : 'bccname',
				hiddenName    : 'bcc',
				allowBlank	  : true,
				forceSelection: false,
				editable	  : true,
				minChars	  : 2,								
				queryDelay	  : 1,
				autoSelect	  : false,
				typeAhead 	  : true,
				typeAheadDelay: 250,
				hiddenValue	  : e,
				value		  : e
			},
			////////////////////////////////////////////////////////
*/			
			{
				id: 'emailSubjectArea',
				fieldLabel: 'Subject',
				name: 'subject',
				value: s
			},
			{
				xtype: 'htmleditor',
				id: 'emailContentArea',
				fieldLabel: 'Message text',
				hideLabel: true,
				name: 'msg',
				height: 300,
				anchor: '0'
				
			}],
			
			buttonAlign: 'center',
			buttons: [{
				text: 'Send',
				handler: function(b){
					var form = b.findParentByType('form');
					//form.getForm().fileUpload = true;
					form.getForm().submit({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						waitMsg: 'Sending email, please wait...',
						success: function(form, action) {
							Ext.Msg.alert('Email', 'Email successfully sent.');
							b.findParentByType('window').close();
						},
						failure: function(form, action) {
							console.log(action);
							 var data = Ext.decode(action.response.responseText);
							Ext.Msg.alert('Error', data.msg);
						}
					});
				}
			},{
				text: 'Cancel',
				handler: function(b){
					b.findParentByType('window').close();
				}
			}],
			
			listeners: {
				afterlayout: function(form) { // ready 
					Ext.getCmp('emailContentArea').focus(true);
				}
			}
		});
		
		//add template
		if(forwards!==true && pid != '-1' && pid != ''){
			var template = {
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'emailtemplates',
						'userid': userid,
						'newTemplate': false
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							if(sel!=records[0].get('id'))
								getTemplateEmailResult(userid,sel,pid,county,reply,forwards,emailContentBefore);
								
							Ext.getCmp('ccontracttemplate').setValue(sel); 
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260,
				listeners     : {
					'select'  : function(combo,record,index){
						var idtemplate = record.get('id');
						if(idtemplate != 0)
							getTemplateEmailResult(userid,idtemplate,pid,county,reply,forwards,emailContentBefore);
						else{
							Ext.getCmp('emailContentArea').setValue('<br><br><br><div style="width: 100%; height: 1px; border: 1px solid #000"></div><br>'+emailContentBefore);
							Ext.getCmp('emailSubjectArea').setValue('Re: '+subject);
						}
					}
				}
			};
			
			ind++;
			att.items[0].name = 'file'+(ind);
			form.insert(1,template);
			form.doLayout();
		}
		//////////////
		
		if(forwards){
			//Get Attachment of Email
			Ext.Ajax.request( 
			{  
				waitMsg: 'Checking...',
				url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
				method: 'POST',
				timeout :600000,
				params: { 
					type: 'getEmailAttach',
					mailid: mailid
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','ERROR');
				},
				
				success:function(response,options){
					var a = Ext.decode(response.responseText);
					if(a.total>0){
						var i=0;
						while(i < a.total){
							var f = a.attach[i];
							if(f.url && f.url.length>0){
								att = {
									xtype: 'compositefield',
									fieldLabel: 'Attach',
									items: [
										{
											xtype: 'hidden',
											name: 'file'+ind,
											value: f.url
										},{
											xtype: 'label',
											html: '<a href="'+f.url+'" target="_blank">'+f.filename+'</a>'
										}
									]
								};
							}else{
								att = {
									xtype: 'compositefield',
									fieldLabel: 'Attach',
									items: [
										{
											xtype: 'hidden',
											name: 'file'+ind,
											value: f.filename
										},{
											xtype: 'label',
											html: '<a href="/docs/MailAttach/'+userid+'/'+f.filename+'" target="_blank">'+f.filename+'</a>'
										}
									]
								};
							}
							form.insert(ind,att);
							i++;
							ind++;
						}
					}
					if(a.total==0)
						form.insert(ind,att);
					form.doLayout();
				}                        
			});
		}else{
			form.insert(ind,att);
			form.doLayout();
		}
	
		var w = new Ext.Window({
			title: title, 
			width: 750,
			height: 500,
			minWidth: 300,
			minHeight: 200,
			layout: 'fit',
			plain: true,
			bodyStyle: 'padding:5px;',
			items: form
		});
		w.show();
	}
	// agregado por Luis R Castro Sugerencia 12053 15/05/2015
	function viewMailDetail(idmail,userid,pid,tabsMail,county,msg_date,typemail,from){
		
		
		if (from=='Mail'){
			var tabsMail = Ext.getCmp('tabsFollowEmailsId');
			var tab = tabsMail.getItem('viewMailTab');
			tabsMail.remove(tab);
		}else{
			if(document.getElementById('viewMailTab')){
			var tab = tabsMail.getItem('viewMailTab');
			tabsMail.remove(tab);
		}
			}
		
		tabsMail.add({
			title: ' View Email ',
			id: 'viewMailTab', 
			closable: true,
			autoLoad: {
				url		: 'mysetting_tabs/myfollowup_tabs/followMailBody.php', 
				scripts	: true, 
				params	:{
					mailid 	: idmail, 
					userid 	: userid,
					pid		: pid,
					tabs	: tabsMail.id,
					county  : county,
					msg_date  : msg_date,
					typemail : typemail,
					from : from
				}, 
				timeout	: 10800
			},
			enableTabScroll:false,
			defaults:{ autoScroll: false}
		});
		tabsMail.setActiveTab('viewMailTab');
		
		
		tabsMail.setHeight(8000);
		tabs.setHeight(8000);
		viewport.setHeight(8000);
	}
	
	var syncPendingEmail = '0', syncPendingContact = '0', syncPendingProperty = '0';
	function updatePendings(valEmail, valContact, valProperty){
		var ve = '0', vc = '0', vp = '0';
		
		if(valEmail > 0) ve = valEmail;
		if(valContact > 0) vc = valContact;
		if(valProperty > 0) vp = valProperty;
		
		if(valEmail < 0) ve = syncPendingEmail;
		if(valContact < 0) vc = syncPendingContact;
		if(valProperty < 0) vp = syncPendingProperty;
		
		syncPendingEmail = ve;
		syncPendingContact = vc;
		syncPendingProperty = vp;
		
		//updates Pendings Emails
		if(document.getElementById('syncPendingEmailB')) Ext.getCmp('syncPendingEmailB').setText(ve);
		if(document.getElementById('syncPendingEmailS')) Ext.getCmp('syncPendingEmailS').setText(ve);
		if(document.getElementById('syncPendingEmailSS')) Ext.getCmp('syncPendingEmailSS').setText(ve);
		if(document.getElementById('syncPendingEmailMEI')) Ext.getCmp('syncPendingEmailMEI').setText(ve);
		if(document.getElementById('syncPendingEmailLB')) Ext.getCmp('syncPendingEmailLB').setText(ve);
		if(document.getElementById('syncPendingEmailLS')) Ext.getCmp('syncPendingEmailLS').setText(ve);
		if(document.getElementById('syncPendingEmailLMEI')) Ext.getCmp('syncPendingEmailLMEI').setText(ve);
		
		//Updates PEndings Contacts
		if(document.getElementById('syncPendingContactB')) Ext.getCmp('syncPendingContactB').setText(vc);
		if(document.getElementById('syncPendingContactS')) Ext.getCmp('syncPendingContactS').setText(vc);
		if(document.getElementById('syncPendingContactSS')) Ext.getCmp('syncPendingContactSS').setText(vc);
		if(document.getElementById('syncPendingContactMEI')) Ext.getCmp('syncPendingContactMEI').setText(vc);
		if(document.getElementById('syncPendingContactLB')) Ext.getCmp('syncPendingContactLB').setText(vc);
		if(document.getElementById('syncPendingContactLS')) Ext.getCmp('syncPendingContactLS').setText(vc);
		if(document.getElementById('syncPendingContactLMEI')) Ext.getCmp('syncPendingContactLMEI').setText(vc);
		
		//Updates Pendings Properties
		if(document.getElementById('syncPendingPropertyB')) Ext.getCmp('syncPendingPropertyB').setText(vp);
		if(document.getElementById('syncPendingPropertyS')) Ext.getCmp('syncPendingPropertyS').setText(vp);
		if(document.getElementById('syncPendingPropertySS')) Ext.getCmp('syncPendingPropertySS').setText(vp);
		if(document.getElementById('syncPendingPropertyMEI')) Ext.getCmp('syncPendingPropertyMEI').setText(vp);
		if(document.getElementById('syncPendingPropertyLB')) Ext.getCmp('syncPendingPropertyLB').setText(vp);
		if(document.getElementById('syncPendingPropertyLS')) Ext.getCmp('syncPendingPropertyLS').setText(vp);
		if(document.getElementById('syncPendingPropertyLMEI')) Ext.getCmp('syncPendingPropertyLMEI').setText(vp);
	}
	
	function checkPendings(userid){
		Ext.Ajax.request( 
		{  
			waitMsg: 'Checking...',
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			method: 'POST',
			timeout :600000,
			params: { 
				checkmail: 2,
				userid: userid,
				typeEmail: 0,
				pendings: true
			},
			
			failure:function(response,options){
				Ext.Msg.alert('Sync Failed', 'Error');
			},
			
			success:function(response,options){
				var j = Ext.decode(response.responseText);
				
				if(j.success){
					updatePendings(j.cantEmail, j.cantContact, j.cantProperty)
				}else{
					Ext.Msg.alert('Sync Failed', j.msg);
				}
			}
		});
	}
	
	//Renders
	function attachments(val, p, record){ 
		if (val==1){
			return String.format('<div height="14px" style="background: url(http://www.reifax.com/img/myemail/assignmentMail.png) -45px 1px; width:18px;">&nbsp;</div>');
		}	
	}
	
	function assignmentContact(val,p,record){
		if (val>0){
			return String.format('<div title="'+record.get('agent')+'" height="14px" style="background: url(http://www.reifax.com/img/myemail/assignmentMail.png) scroll 0px 0px; width:13px;">&nbsp;</div>');
		}
	}//Agregado por Luis R Castro Sugerencia 11808 22/05/2015
	function assignmentHistory(val,p,record){
		if (val>0 && val!=null){
									
			return String.format('<div title="Follow History" height="15px" style="background: url(http://www.reifax.com/img/toolbar/icono_ojo_small.png) no-repeat  0px 0px; width:15px;">&nbsp;</div>');
		}
	}
	
	function assignmentProperty(val,p,record){
		var contact = record.get('ac');
		var nameContact = record.get('agent');
		if (val!='-1' && val!='0'){
			return String.format('<div title="'+record.get('address')+'" height="14px" style="background: url(http://www.reifax.com/img/myemail/assignmentMail.png) -13px 0px; width:18px;" onClick="showAssignmentProperty('+(record.get('userid'))+','+contact+','+(record.get('idmail'))+',\''+nameContact+'\',\''+record.get('address')+'\');">&nbsp;</div>');
		}else if(contact>0 && val=='0'){
			return String.format('<div height="14px" style="background: url(http://www.reifax.com/img/myemail/assignmentMail.png) -31px 1px; width:15px; cursor:pointer;" title="Property Assignment" onClick="showAssignmentProperty('+(record.get('userid'))+','+contact+','+(record.get('idmail'))+',\''+nameContact+'\');">&nbsp;</div>');
		}
	}
	
	function typeEmails(val, p, record){
		if(val==0){
			return String.format('<div height="14px" style="background: url(http://www.reifax.com/img/myemail/typeEmails.png) 3px 0px; width:17px;">&nbsp;</div>');
		}else if(val==1){
			return String.format('<div height="14px" style="background: url(http://www.reifax.com/img/myemail/typeEmails.png) -12px 0px; width:17px;">&nbsp;</div>');
		}else{
			return String.format('<div height="14px" style="background: url(http://www.reifax.com/img/myemail/typeEmails.png) -28px 0px; width:17px;">&nbsp;</div>');
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="http://www.reifax.com/img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="http://www.reifax.com/img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="http://www.reifax.com/img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="http://www.reifax.com/img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="http://www.reifax.com/img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="http://www.reifax.com/img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="http://www.reifax.com/img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="http://www.reifax.com/img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="http://www.reifax.com/img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive Call." src="http://www.reifax.com/img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="http://www.reifax.com/img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="http://www.reifax.com/img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="http://www.reifax.com/img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="http://www.reifax.com/img/notes/reci_other.png" />'; break;
			case 15: return '<img title="Send Voice Mail." src="http://www.reifax.com/img/notes/send_voice.png" />'; break;
			case 16: return '<img title="Receive Voice Mail." src="http://www.reifax.com/img/notes/reci_voice.png" />'; break;
			case 17: return '<img title="Make Note." src="http://www.reifax.com/img/notes/send_note.png" />'; break;
			case 18: return '<img title="Short Sale." src="http://www.reifax.com/img/notes/shortsale.png" />'; break;
			case 19: return '<img title="Short Sale Processor." src="http://www.reifax.com/img/notes/shortsale_processor.png" />'; break;
		}
	}
	
	function dateRender(value, metaData, record, rowIndex, colIndex, store) {
		var d = new Date();
		
		if(d.format('Y-m-d') == value.format('Y-m-d'))
			return '<span title="'+value.format('Y-m-d G:i')+'">'+value.format('G:i')+'</span>';
		else if(d.format('Y') > value.format('Y'))
			return '<span title="'+value.format('Y-m-d G:i')+'">'+value.format('j M y')+'</span>';
		else
			return '<span title="'+value.format('Y-m-d G:i')+'">'+value.format('j M')+'</span>';
	}