var system_width;
var pushPinActive;
XimaMap = function(id,idl,idbar,idpan,iddraw,idpoly,idclear,idmaxmin,idcircle){
	this.idDiv=id;
	this.idLatlong=idl;
	this.idBar=idbar;
	this.idPan=idpan;
	this.idDraw=iddraw;
	this.idPoly=idpoly;
	this.idCircle=idcircle;
	this.idClear=idclear;
	this.idMaxMin=idmaxmin;
	this.barType='full';
	this._curLatLonShape = 0;
	this.curBoton="OFF";
	this.search_map=true;
	this.mini_map=true;
	this._curTool='_pan';
	this._mapTool='';
	this.map = null;
	this.myGeomType = "polygon";
	this.centerCircle = null;
	this.myCurrentShape = null;
	this.myCurrentShapeOpt = {
		fillColor: new Microsoft.Maps.Color(100,0,138,255), 
		strokeColor: new Microsoft.Maps.Color(0,157,201,226),
		strokeThickness: 3
	};
	this.myPoints = new Array();
	this.myDistance = 0;
	this.tempShape = null;
	this.temShapeOpt = {
		fillColor: new Microsoft.Maps.Color(100,223,122,191), 
		strokeColor: new Microsoft.Maps.Color(200,157,201,226),
		strokeThickness: 3
	};
	this.tempPoints = null;
	this.tempDistance = 0;
	this.posx=0;
	this.posy=0;
	this.fun2=this.fun3=this.fun4=this.fun5=this.fun6=this.fun7=this.fun8=null;
	this.filter_map= false;
	this.filter_type= '';
	
	//Shape
	this.fun2='';
	this.fun3='';
	this.fun4='';
	
	//Poly
	this.fun5='';
	this.fun6='';
	this.fun7='';
	this.fun8='';
	
	this.filterONOFF = function (type){
		this.filter_map=!this.filter_map;
		this.filter_type= type;
	}
	
	this.mapFilter =function(mapa){
		var type = this.filter_type;
		Ext.Ajax.request( 
		{  
			waitMsg: 'Filtering...',
			url: 'properties_filter.php', 
			method: 'POST',
			timeout :600000,
			params: { 
				type: this.filter_type,
				mapa: mapa
			},
			
			failure:function(response,options){
				Ext.MessageBox.alert('Warning','ERROR');
			},
			success:function(response,options){
				if(type=="COMP-MAP") storeComp.load({params:{start:0, limit:limitComp}});
				else if(type=="ACTI-MAP") storeActive.load({params:{start:0, limit:limitActive}});
				else if(type=="DISS-MAP") storeDistress.load({params:{start:0, limit:limitDistress}});
				else if(type=="RENT-MAP") storeRental.load({params:{start:0, limit:limitRental}});
			}                                
		});
	}
	
	this.on_off = function ()
	{	
		var self = this;
		this.DetachALL();
		var mapa=document.getElementById(this.idDiv);
	
		if( this._curTool=="_draw" )
		{
			if (this.myCurrentShape) this.map.entities.remove(this.myCurrentShape);
			this.myCurrentShape=null;
	
			//this.map.vemapcontrol.EnableGeoCommunity(true);	
			this.fun3 = Microsoft.Maps.Events.addHandler(this.map, "mousemove", function (e){
				self.MouseMove(e,self);
			});
			this.fun4 = Microsoft.Maps.Events.addHandler(this.map, "click", function (e){
				self.DrawShapeMouseClick(e,self);
			});
			if(document.getElementById(this.idPan)) document.getElementById(this.idPan).className = 'd_pan';
			if(document.getElementById(this.idPoly)) document.getElementById(this.idPoly).className = 'd_poly';
			if(document.getElementById(this.idCircle)) document.getElementById(this.idCircle).className = 'd_circle';
			if(this.mini_map==true && document.getElementById(this.idMaxMin)){
				if(document.getElementById(this.idMaxMin).className == 'd_maxmin')
					document.getElementById(this.idMaxMin).className = 'd_maxmin';
				else
					document.getElementById(this.idMaxMin).className = 'a_maxmin';
			}
			if(document.getElementById(this.idDiv)) document.getElementById(this.idDiv).childNodes[0].style.cursor='crosshair';
			
		}
		
		if( this._curTool=="_poly" )
		{
			alert("Close the polygon by pressing the 'Escape' key.")
			if (this.myCurrentShape) this.map.entities.remove(this.myCurrentShape);
			this.myCurrentShape=null;
			
			this.fun5 = Microsoft.Maps.Events.addHandler(this.map, "click", function (e){
				self.DrawPolyMouseClick(e,self);
			});
			this.fun6 = Microsoft.Maps.Events.addHandler(this.map, "mousemove", function (e){
				self.MouseMove(e,self);
			});
			if(document.getElementById(this.idDiv)) document.getElementById(this.idDiv).childNodes[0].style.cursor='crosshair';
			if(document.getElementById(this.idPan)) document.getElementById(this.idPan).className = 'd_pan';
			if(document.getElementById(this.idDraw)) document.getElementById(this.idDraw).className = 'd_draw';
			if(document.getElementById(this.idCircle)) document.getElementById(this.idCircle).className = 'd_circle';
			if(this.mini_map==true && document.getElementById(this.idMaxMin)){
				if(document.getElementById(this.idMaxMin).className == 'd_maxmin')
					document.getElementById(this.idMaxMin).className = 'd_maxmin';
				else
					document.getElementById(this.idMaxMin).className = 'a_maxmin';
			}
			//this.map.vemapcontrol.EnableGeoCommunity(true);
	
		}
		
		if( this._curTool=="_circle" )
		{
			if (this.myCurrentShape) this.map.entities.remove(this.myCurrentShape);
			this.myCurrentShape=null;

			this.fun8 = Microsoft.Maps.Events.addHandler(this.map, "click", function (e){
				self.DrawCircleMouseClick(e,self);
			});
			if(document.getElementById(this.idPan)) document.getElementById(this.idPan).className = 'd_pan';
			if(document.getElementById(this.idDraw)) document.getElementById(this.idDraw).className = 'd_draw';
			if(document.getElementById(this.idPoly)) document.getElementById(this.idPoly).className = 'd_poly';
			if(this.mini_map==true && document.getElementById(this.idMaxMin)){
				if(document.getElementById(this.idMaxMin).className == 'd_maxmin')
					document.getElementById(this.idMaxMin).className = 'd_maxmin';
				else
					document.getElementById(this.idMaxMin).className = 'a_maxmin';
			}
			if(document.getElementById(this.idDiv)) document.getElementById(this.idDiv).childNodes[0].style.cursor='crosshair';
	
		}	
	
		if( this._curTool=="_clear" )
		{
			if (this.myCurrentShape) this.map.entities.remove(this.myCurrentShape);
			this.myCurrentShape=null;
			document.getElementById(this.idLatlong).value='-1';
			this._curLatLonShape='-1';
			this._curTool="_pan";
			if(document.getElementById(this.idPan)) document.getElementById(this.idPan).className = 'a_pan';
			if(this.mini_map==true){
				if(document.getElementById(this.idBar)){
					this.map.setOptions({height: 350});
					document.getElementById(this.idBar).style.margin ="320px 0px 0px 10px";
				}
				if(document.getElementById(this.idMaxMin))
				document.getElementById(this.idMaxMin).className = 'd_maxmin';
			}
			if(this.filter_map) this.mapFilter("0");
		}
	
		if( this._curTool=="_pan" )
		{	
			//this.map.vemapcontrol.EnableGeoCommunity(false);
			if(document.getElementById(this.idPan)) document.getElementById(this.idPan).className = 'a_pan';	
			if(document.getElementById(this.idDiv)) document.getElementById(this.idDiv).style.cursor='';
			if(document.getElementById(this.idDraw)) document.getElementById(this.idDraw).className = 'd_draw';
			if(document.getElementById(this.idPoly)) document.getElementById(this.idPoly).className = 'd_poly';
			if(document.getElementById(this.idCircle)) document.getElementById(this.idCircle).className = 'd_circle';
			
			if(this.mini_map==true && document.getElementById(this.idMaxMin)){
				if(document.getElementById(this.idMaxMin).className == 'd_maxmin') 
					document.getElementById(this.idMaxMin).className = 'd_maxmin';
				else
					document.getElementById(this.idMaxMin).className = 'a_maxmin';
			}
		}
		
		if( this._curTool=="_maxmin" )
		{
			if(this.search_map==true && this.barType=='full'){
				document.getElementById(this.idPan).className = 'a_pan';
				document.getElementById(this.idDraw).className = 'd_draw';
				document.getElementById(this.idPoly).className = 'd_poly';
				document.getElementById(this.idCircle).className = 'd_circle';
			}
			
			if(document.getElementById(this.idMaxMin).className == 'd_maxmin'){ 
				document.getElementById(this.idMaxMin).className = 'a_maxmin';
				
				this.map.setOptions({height: 600});
				document.getElementById(this.idBar).style.margin ="570px 0px 0px 10px";
	
			}else{
				document.getElementById(this.idMaxMin).className = 'd_maxmin';
				this.map.setOptions({height: 350});
				document.getElementById(this.idBar).style.margin ="320px 0px 0px 10px";
			
			}
		}
	}
	
	this.DrawPolyMouseClick = function (e, self)
	{
		if (e.targetType == "map") {
			pixel = new Microsoft.Maps.Point(e.getX()-6, e.getY()-6);
			var LL = e.target.tryPixelToLocation(pixel);   
			
			if (self.myPoints.length == 0 && self.myGeomType != "point") 
			{
				Microsoft.Maps.Events.removeHandler(self.fun6);
				self.fun7 = Microsoft.Maps.Events.addHandler(self.map, "mousemove", function (e){
					self.DrawPolyMouseMove(e,self);
				});
				self.fun8 = Microsoft.Maps.Events.addHandler(self.map, "keydown", function (e){
					self.KeyPressedMap(e,self);
				});
				if(document.getElementById("divDistance"))
					document.getElementById("divDistance").style.visibility = "visible";
			}    
		
			self.myPoints.push(LL);
			self.myDistance +=  self.tempDistance;    
	
			if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).childNodes[0].style.cursor='crosshair';
			try{
				self.map.entities.remove(self.myCurrentShape);
			}catch(err){}
		}
	}
	
	this.DrawShapeMouseClick = function (e, self)
	{
		if (e.targetType == "map") {
			var x = e.getX();
			var y = e.getY();    
			
			if (self.myPoints.length == 0) 
			{
				pixel = new Microsoft.Maps.Point(x-6, y-6);
				var LL = e.target.tryPixelToLocation(pixel);
				
				Microsoft.Maps.Events.removeHandler(self.fun3);
				self.fun2 = Microsoft.Maps.Events.addHandler(self.map, "mousemove", function (e){
					self.DrawShapeMouseMove(e, self);
				});
				
				if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).childNodes[0].style.cursor='crosshair';
				try{
					self.map.entities.remove(self.myCurrentShape);
				}catch(err){}
				
				self.myPoints.push(LL);
				self.myDistance +=  self.tempDistance;
			}else{
				pixel = new Microsoft.Maps.Point(x-6, y-6);
				var LL = e.target.tryPixelToLocation(pixel);
				
				self.myPoints.push(LL);
				try{
					Microsoft.Maps.Events.removeHandler(self.fun2);
					self.map.entities.remove(self.tempShape);
				}catch (err){}
				
				self.points2shape(self.myPoints);
				
				var s="",_i=0;
				for( var data in self.myPoints ) 
				{
					if(Ext.isNumber(parseInt(data))){
						if(_i>0) s+="/";
						s+=self.myPoints[data].latitude +",";
						s+=self.myPoints[data].longitude;
						_i++;
					}
				}
				self._curLatLonShape=s;
				document.getElementById(self.idLatlong).value=s;
						
				self.myGeomType = null;
				self.myPoints = new Array();
				self.myDistance = 0;
				self.tempShape = null;
				self.tempPoints = null;
				
				if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).childNodes[0].style.cursor='crosshair';
		
				if(self.filter_map) self.mapFilter(s);
			}
		}
	}
	
	this.DrawCircleMouseClick = function (e, self)
	{
		if (e.targetType == "map") {
			var x = e.getX();
			var y = e.getY();    
			
			if (!self.centerCircle) 
			{
				pixel = new Microsoft.Maps.Point(x, y);
				var LL = e.target.tryPixelToLocation(pixel);
				
				self.fun7 = Microsoft.Maps.Events.addHandler(self.map, "mousemove", function (e){
					self.DrawCircleMouseMove(e, self);
				});
				
				if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).childNodes[0].style.cursor='crosshair';
				try{
					self.map.entities.remove(self.myCurrentShape);
				}catch(err){}
				
				self.centerCircle = LL;
			}else{
				pixel = new Microsoft.Maps.Point(x-6, y-6);
				var LL = e.target.tryPixelToLocation(pixel);
				var LLc = self.centerCircle;
				var r = self.getDistanceKM(LLc, LL);
		
				self.tempPoints = self._circlePoints(LLc, r);
				
				self.myPoints.push(LL);
				try{
					Microsoft.Maps.Events.removeHandler(self.fun7);
					self.map.entities.remove(self.tempShape);
				}catch (err){}
				
				self.points2shape(self.tempPoints);
				
				var s="",_i=0,maxLat=0,maxLong=0,minLat=1000,minLong=1000;
				for( var data in self.tempPoints ) 
				{
					if(Ext.isNumber(parseInt(data))){
						
						
						if(_i==0){ 
							maxLat = self.tempPoints[data].latitude;
							minLat = self.tempPoints[data].latitude;
							
							maxLong = self.tempPoints[data].longitude;
							minLong = self.tempPoints[data].longitude;
						}else{
							maxLat = Math.max(maxLat,self.tempPoints[data].latitude);
							minLat = Math.min(minLat,self.tempPoints[data].latitude);
							
							maxLong = Math.max(maxLong,self.tempPoints[data].longitude);
							minLong = Math.min(minLong,self.tempPoints[data].longitude);
						}
						_i++;
					}
				}
				s=maxLat+","+minLong+"/"+maxLat+","+maxLong+"/"+minLat+","+maxLong+"/"+minLat+","+minLong;
				self._curLatLonShape=s;
				document.getElementById(self.idLatlong).value=s;
						
				self.myGeomType = null;
				self.myPoints = new Array();
				self.myDistance = 0;
				self.tempShape = null;
				self.tempPoints = null;
				
				if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).childNodes[0].style.cursor='crosshair';
		
				if(self.filter_map) self.mapFilter(s);
			}
		}
	}
	
	this.KeyPressedMap = function (e, self){
		if(e.keyCode==27 && self.myPoints.length>2){
			try
			{
				Microsoft.Maps.Events.removeHandler(self.fun7);
				self.map.entities.remove(self.tempShape);
	
			}
			catch (err)
			{
			}        
	
			self.myDistance = Math.round(self.myDistance * 1000) / 1000; //in KM
	
			self.points2shape(self.myPoints);
	
			//buscar en mysql esto
				var s="",_i=0;
				for( var data in self.myPoints ) 
				{
					if(Ext.isNumber(parseInt(data))){
						if(_i>0) s+="/"; //siguiente lote
						s+=self.myPoints[data].latitude +",";
						s+=self.myPoints[data].longitude;
						_i++;
					}
				}
				self._curLatLonShape=s;
				document.getElementById(self.idLatlong).value=s;
			//			
				self.myGeomType = null;
				self.myPoints = new Array();
				self.myDistance = 0;
				self.tempShape = null;
				self.tempPoints = null;
				//
	
			if(document.getElementById(self.idDiv)) document.getElementById(self.idDiv).style.cursor = 'http://maps.live.com/cursors/grab.cur';
			if(self.filter_map) self.mapFilter(s);
		}
	
	}
	
	this.DetachALL = function ()
	{				
		Microsoft.Maps.Events.removeHandler(this.fun2);	
		Microsoft.Maps.Events.removeHandler(this.fun3);
		Microsoft.Maps.Events.removeHandler(this.fun4);
		
		Microsoft.Maps.Events.removeHandler(this.fun5);
		Microsoft.Maps.Events.removeHandler(this.fun6);	
		Microsoft.Maps.Events.removeHandler(this.fun7);
		Microsoft.Maps.Events.removeHandler(this.fun8);
	}
}

XimaMap.prototype.points2shape = function (vector)
{
	var newPoints=vector;
	//alert(vector);
	var lat=0,long=0;
	
	if (vector.length==2)//si es de 2 dimensiones hablamos de un rectangulo, hay q buscarlos los 4 puntos
	{
		newPoints=[new Microsoft.Maps.Location(vector[0].latitude,vector[0].longitude),
				new Microsoft.Maps.Location(vector[1].latitude,vector[0].longitude),
				new Microsoft.Maps.Location(vector[1].latitude,vector[1].longitude),        
				new Microsoft.Maps.Location(vector[0].latitude,vector[1].longitude)]

	}
	
	try{
		this.map.entities.remove(this.tempShape);
		this.map.entities.remove(this.myCurrentShape);
	}catch(err){}
	
	this.myCurrentShape = new Microsoft.Maps.Polygon(newPoints,this.myCurrentShapeOpt);
	this.map.entities.push(this.myCurrentShape);
}

XimaMap.prototype.ins_toolbar = function (adjust,tipo)
{
	var botones='', self = this;

	this._mapTool = document.createElement("div"); 
	this._mapTool.id = this.idBar;
	this._mapTool.style.position = "absolute";
	this._mapTool.style.zIndex = 100;
	this._mapTool.style.margin =adjust+" 0px 0px 10px"; 
	
	botones = "<div id=\"pan_shape\">"+
	"<TABLE bgcolor=\"orange\" >  "+
	"<TR>"+
	"<TD id=\""+this.idPan+"\" class=\"a_pan\" onClick=\"this.className ='a_pan';"+this.idDiv+"._curTool='_pan';"+this.idDiv+".on_off(); return false;\" align=middle width=80> </TD>"+
	"<TD  align=middle>&nbsp;</TD>";
	
	if(this.barType=='full'){
		botones+="<TD id=\""+this.idDraw+"\" class=\"d_draw\" align=middle width=80></TD>"+
		"<TD  align=middle>&nbsp;</TD>"+
		"<TD id=\""+this.idPoly+"\" class=\"d_poly\" align=middle ></TD>"+
		"<TD  align=middle>&nbsp;</TD>"+
		"<TD id=\""+this.idCircle+"\" class=\"d_circle\" align=middle ></TD>"+
		"<TD  align=middle>&nbsp;</TD>"+
		"<TD id=\""+this.idClear+"\" style='width:80px;height:18px;cursor:pointer;cursor:hand;' align=middle ><img src='img/reset_map.jpg' ></TD>";
	}
	
	if(this.mini_map==true)
		botones+="<TD id=\""+this.idMaxMin+"\" class=\"d_maxmin\" align=middle ></TD>";

	botones+="</TR>"+
	"</TABLE>"+
	"</div>";

	this._mapTool.innerHTML = botones;
	this.map.getRootElement().appendChild(this._mapTool);
	
	if(this.barType=='full'){
		document.getElementById(this.idDraw).onclick = function (e){
			e.stopPropagation();
			this.className ='a_draw';
			self._curTool='_draw';
			self.on_off();
		};
		
		document.getElementById(this.idPoly).onclick = function (e){
			e.stopPropagation();
			this.className ='a_poly';
			self._curTool='_poly';
			self.on_off();
		};
		
		document.getElementById(this.idCircle).onclick = function (e){
			e.stopPropagation();
			this.className ='a_circle';
			self._curTool='_circle';
			self.on_off();
		};
		
		document.getElementById(this.idClear).onclick = function (e){
			e.stopPropagation();
			self._curTool='_clear';
			self.on_off();
		};
	}
	
	document.getElementById(this.idMaxMin).onclick = function (e){
		e.stopPropagation();
		self._curTool='_maxmin';
		self.on_off();
	};
	
}

XimaMap.prototype.MouseMove = function (e)
{
    if (e.targetType == "map") {
		pixel = new Microsoft.Maps.Point(e.getX(), e.getY());
		var LL = e.target.tryPixelToLocation(pixel);
		window.status="Latitude->"+LL.latitude+" | Longitude->"+LL.longitude;
	}
}
//==============================================================

XimaMap.prototype.DrawPolyMouseMove = function(e, map)
{
	if (e.targetType == "map") {
		pixel = new Microsoft.Maps.Point(e.getX()-6, e.getY()-6);
		var LL = e.target.tryPixelToLocation(pixel);
		
		
		map.tempPoints = map.myPoints.slice(0, map.myPoints.length);
		
		
		map.tempPoints.push(LL);
		map.tempDistance = map.getDistanceKM(map.tempPoints[map.myPoints.length-1], LL);
		
		try
		{
			map.map.entities.remove(map.tempShape);
		}
		catch (err)
		{
		}
		
		map.tempShape = new Microsoft.Maps.Polyline(map.tempPoints,map.temShapeOpt);
		map.map.entities.push(map.tempShape);
	}
}

XimaMap.prototype.DrawShapeMouseMove = function(e, map)
{
	if (e.targetType == "map") {
		pixel = new Microsoft.Maps.Point(e.getX()-6, e.getY()-6);
		var LL = e.target.tryPixelToLocation(pixel);
		var LLaux = map.myPoints[0];
	
		map.tempPoints = [new Microsoft.Maps.Location(LL.latitude,LL.longitude),
							new Microsoft.Maps.Location(LLaux.latitude,LL.longitude),
							new Microsoft.Maps.Location(LLaux.latitude,LLaux.longitude),        
							new Microsoft.Maps.Location(LL.latitude,LLaux.longitude)]
		
		map.tempDistance = map.getDistanceKM(map.tempPoints[map.myPoints.length-1], LL);
	
		try
		{
			map.map.entities.remove(map.tempShape);
		}
		catch (err)
		{
		}
		
		map.tempShape = new Microsoft.Maps.Polyline(map.tempPoints,map.temShapeOpt);
		map.map.entities.push(map.tempShape);
	}
}

XimaMap.prototype.DrawCircleMouseMove = function(e, map)
{
	if (e.targetType == "map") {
		pixel = new Microsoft.Maps.Point(e.getX()-6, e.getY()-6);
		var LL = e.target.tryPixelToLocation(pixel);
		var LLc = map.centerCircle;
		var r = map.getDistanceKM(LLc, LL);
		
		map.tempPoints = map._circlePoints(LLc, r); 

		try
		{
			map.map.entities.remove(map.tempShape);
		}
		catch (err)
		{
		}
		
		map.tempShape = new Microsoft.Maps.Polyline(map.tempPoints,map.temShapeOpt);
		map.map.entities.push(map.tempShape);
	}
}

XimaMap.prototype.DrawPolySaved = function ()
{     
	
	var mapa=document.getElementById(this.idDiv);
	warning.style.display="none";

	cntGps=0;
	cntSearch++;
	var county=curCounty;
	this._allocateCounty(county);
	this.map.entities.clear(); 

	if  (mapa.className!="mapon") {
		mapa.className="mapon"
		this.curBoton="AVG"
		this.ins_toolbar("320px","search");
	}

	if ( (this.curBoton=='AVG') && (this._curLatLonShape!=0) ) 
	{
		var _xpoints=new Array();
		for(x=0;x<this._curLatLonShape.split("/").length;x++)
		{								//latitude	longitude
			_xpoints.push(new Microsoft.Maps.Location(this._curLatLonShape.split("/")[x].split(",")[0],this._curLatLonShape.split("/")[x].split(",")[1]));
		}
		this.points2shape(_xpoints)
	}
}

XimaMap.prototype.borrarTodoMap = function (){
	this.map.entities.clear();
}

XimaMap.prototype.getDistanceKM = function (p1, p2) 
{
    this.p1Lat = this.latLonToRadians(p1.latitude);
	this.p1Lon = this.latLonToRadians(p1.longitude);
	
	this.p2Lat = this.latLonToRadians(p2.latitude);
	this.p2Lon = this.latLonToRadians(p2.longitude);	
	
	var R = 6371; // earth's mean radius in km
	var dLat  = this.p2Lat - this.p1Lat;
	var dLong = this.p2Lon - this.p1Lon;
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(this.p1Lat) * Math.cos(this.p2Lat) * Math.sin(dLong/2) * Math.sin(dLong/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var disKm = R * c;
	//var disMiles = disKm * 0.6214;	
	return (disKm);
}

XimaMap.prototype.MouseCatch = function (e) {
	this.posx =  e.clientX-2;			
	this.posy = e.clientY-2;
	window.status='X: '+this.posx+' Y: '+this.posy;
}

XimaMap.prototype._IniMAP = function (lat,lon,mtype,w,h)
{
	if (typeof (this.map) != 'undefined' && this.map != null){
		this.map.dispose();
		this.map = null;
		document.getElementById(this.idLatlong).value='-1';
	}

	this.map = new Microsoft.Maps.Map(
		document.getElementById(this.idDiv),
		{
			credentials:"AtHjGZaGqSAUZAUPSCQ3mHkmAWDeZKr_FbwX9-_JitI9N4vORaw0Ut6NMh3IgLDA",
			enableSearchLogo: false,
			enableClickableLogo: false,
			mapTypeId: (mtype=='birdseye' ? Microsoft.Maps.MapTypeId.birdseye : Microsoft.Maps.MapTypeId.road),
			height: (h ? h : 350),
            width: (w ? w : (system_width ? (system_width-5) : 650))
		}
	);
	
	this.map.entities.clear(); 
	
	if(typeof(lat)=='undefined') 
		this.centerMapCounty(document.getElementById(search_type+'_county_search').value,true);
	else{ 
		this.map.setView({
			center: new Microsoft.Maps.Location(lat, lon),
			zoom: (mtype=='birdseye' ? 19 : 7)
		});
	}
}

XimaMap.prototype._IniMAPResult = function (lat,lon)
{
	if (typeof (this.map) != 'undefined' && this.map != null){
		this.map.dispose();
		this.map = null;
	}

	this.map = new Microsoft.Maps.Map(
		document.getElementById(this.idDiv),
		{
			credentials:"AtHjGZaGqSAUZAUPSCQ3mHkmAWDeZKr_FbwX9-_JitI9N4vORaw0Ut6NMh3IgLDA",
			enableSearchLogo: false,
			enableClickableLogo: false,
			showDashboard: false,
			showScalebar: false,
			disablePanning: true,
			disableMouseInput: true,
			mapTypeId: Microsoft.Maps.MapTypeId.birdseye,
			center: new Microsoft.Maps.Location(lat, lon),
			labelOverlay: Microsoft.Maps.LabelOverlay.hidden,
			zoom: 19,
			showCopyright: false
		}
	);
	
	this.map.entities.clear(); 
}

XimaMap.prototype._IniMAPOverview = function (latlon,mtype,z)
{
	if (typeof (this.map) != 'undefined' && this.map != null){
		this.map.dispose();
		this.map = null;
	}

	this.map = new Microsoft.Maps.Map(
		document.getElementById(this.idDiv),
		{
			//credentials:"Aqe7wdL1F6tw36t8ksunKUfLunY2Iz5ma1xIdmm8zGSP67Wj2U5HHUbDGujYBWIg",
			credentials:"AtHjGZaGqSAUZAUPSCQ3mHkmAWDeZKr_FbwX9-_JitI9N4vORaw0Ut6NMh3IgLDA",
			enableSearchLogo: false,
			enableClickableLogo: false,
			showDashboard: false,
			showScalebar: false,
			disablePanning: true,
			disableMouseInput: true,
			mapTypeId: (mtype=='birdseye' ? Microsoft.Maps.MapTypeId.birdseye : Microsoft.Maps.MapTypeId.road),
			center: latlon,
			labelOverlay: Microsoft.Maps.LabelOverlay.hidden,
			zoom: (z ? z : 19)
		}
	);
	
	this.map.entities.clear(); 
}

XimaMap.prototype.latLonToRadians = function ( point ) {
	return point * Math.PI / 180;	
}

XimaMap.prototype._circlePoints = function(center, r){
	var R = 6371;	
	var lat = (center.latitude * Math.PI) / 180;     
	var lon = (center.longitude * Math.PI) / 180;
	var d = parseFloat(r) / R;
	var circlePoints = new Array();
	
	for (x = 0; x <= 360; x += 5) {
		var p2 = new Microsoft.Maps.Location(0, 0);
		brng = x * Math.PI / 180;
		p2.latitude = Math.asin(Math.sin(lat) * Math.cos(d) + Math.cos(lat) * Math.sin(d) * Math.cos(brng));
		p2.latitude = (p2.latitude * 180) / Math.PI;
		
		p2.longitude = (lon + Math.atan2(Math.sin(brng) * Math.sin(d) * Math.cos(lat), 
						 Math.cos(d) - Math.sin(lat) * Math.sin(p2.latitude)));
		p2.longitude = (p2.longitude * 180) / Math.PI;
		
		circlePoints.push(p2);
	}
	
	return circlePoints;
}

XimaMap.prototype._getDistance = function (p1, p2) 
{
    this.p1Lat = this.latLonToRadians(p1.latitude);
	this.p1Lon = this.latLonToRadians(p1.longitude);
	
	this.p2Lat = this.latLonToRadians(p2.latitude);
	this.p2Lon = this.latLonToRadians(p2.longitude);	
	
	var R = 6371; // earth's mean radius in km
	var dLat  = this.p2Lat - this.p1Lat;
	var dLong = this.p2Lon - this.p1Lon;
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(this.p1Lat) * Math.cos(this.p2Lat) * Math.sin(dLong/2) * Math.sin(dLong/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var disKm = R * c;
	//var disMiles = disKm * 0.6214;	
	return (disKm);
}

XimaMap.prototype.doNothing = function (){}

XimaMap.prototype.control_map = function (resete)
{
	var mapa=document.getElementById(this.idDiv);

	if(mapa.style.display=='' || resete==true){
		mapa.style.display='none';
		this.curBoton="OFF"
		this._curTool='_clear';
		this.on_off();
		if(this._mapTool!='') this.map.getRootElement().removeChild(this._mapTool);
	}else{
		mapa.style.display='';
		this.curBoton="AVG";
		this.ins_toolbar("320px","search");
	}     
}

XimaMap.prototype.cleaner = function ()
{
	var mapa=document.getElementById(this.idDiv);

	if(mapa.style.display!='none')
	{
		mapa.style.display='none';
		this.curBoton="OFF"
		this._curTool='_clear';
		this.on_off();
		if(this._mapTool!='') this.map.getRootElement().removeChild(this._mapTool);	     
	}
}

XimaMap.prototype.centerMapCounty = function (id,press){
	if(press && this.map!=null){
		var esto = this;
		if(id!=-1){
			Ext.Ajax.request( 
			{  
				waitMsg: 'Validating...',
				url: '../../../../../properties_averageCounty.php',
				method: 'POST',
				timeout :600000,
				params: { 
					idcounty: id
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','ERROR');
				},
				success:function(response,options){
					var latlong = response.responseText.split('^');
					esto.map.setView({
						center: new Microsoft.Maps.Location(latlong[0], latlong[1]),
						zoom: 9
					});
					
					if(esto.myCurrentShape!=null){ 
						esto.map.setView({
							bounds: Microsoft.Maps.LocationRect.fromLocations(esto.myCurrentShape.getLocations())
						});
					}
				}                                
			});
		}else
			this.map.setView({
				center: new Microsoft.Maps.Location(29.0, -80.0),
				zoom: 6
			});
	}
}
XimaMap.prototype.setBarType = function(type){
	this.barType=type;
}

XimaMap.prototype.getPushpin = function(ind){
	var pin = null, entities = this.map.entities;
	
	for(var i=entities.getLength()-1; i>=0; i--){
		pin = entities.get(i); 
		if (pin instanceof Microsoft.Maps.Pushpin){
			if(parseInt(pin.getText())==parseInt(ind))
				return pin;
		}
	}
	
	return false;
}

XimaMap.prototype.addPushpin = function(lat,lon,urlImg,text){
	var pin = new Microsoft.Maps.Pushpin(
		new Microsoft.Maps.Location(lat, lon),
		{
			icon: urlImg,
			text: text,
			textOffset: new Microsoft.Maps.Point(-2,1),
			width: 21,
			height: 18
		}
	);
	this.map.entities.push(pin);
}

XimaMap.prototype.addPushpinInfobox = function(i, lat, lon, urlImg, a, ga, la, bb, p, s, co, cot){	
	var infopin = new Microsoft.Maps.Infobox(
		new Microsoft.Maps.Location(lat, lon),
		{
			width :200, 
			height :150, 
			showCloseButton: false, 
			offset:new Microsoft.Maps.Point(-12,-2), 
			showPointer: false,
			visible: false,
			zIndex: 100,
			htmlContent: '<div id="infopin'+i+'" style="background-color: #FFF; background-image: url(\'http://www.reifax.com/img/bkgd_result.jpg\'); border: 3px solid #B8DAE3; min-height:135px; min-width:160px; padding: 5px 8px;"><div class="VE_Pushpin_Popup_Title">'+a+'</div><div class="VE_Pushpin_Popup_Body"><div align="center"><table border="0" style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;"><tbody><tr><td style=" font-weight:bold; margin-right:5px;">Gross Area:</td><td>'+ga+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Living Area:</td><td>'+la+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td><td>'+bb+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Price</td><td>'+p+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Status:</td><td>'+s+'</td></tr><tr><td onclick="'+co+'" style="font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" colspan="2">'+cot+'</td></tr></tbody></table></div></div></div>'
		}
	);

	var pin = new Microsoft.Maps.Pushpin(
		new Microsoft.Maps.Location(lat,lon),
		{
			icon: urlImg,
			text: i,
			textOffset: new Microsoft.Maps.Point(-2,1),
			width: 21,
			height: 18,
			infobox: infopin
		}
	);
	
	this.map.entities.push(pin);
	this.map.entities.push(infopin);
	
	Microsoft.Maps.Events.addHandler(pin, 'mouseover', pinMouseOver);
	Microsoft.Maps.Events.addHandler(infopin, 'mouseleave', infopinMouseOut);
	
	return pin;
}

XimaMap.prototype.addPushpinInfoboxImage = function(i, lat, lon, urlImg, a, ga, la, bb, p, s, co, cot, urlImgC){	
	var infopin = new Microsoft.Maps.Infobox(
		new Microsoft.Maps.Location(lat, lon),
		{
			width :200, 
			height :150, 
			showCloseButton: false, 
			offset:new Microsoft.Maps.Point(-12,-2), 
			showPointer: false,
			visible: false,
			zIndex: 100,
			htmlContent: '<div id="infopin'+i+'" style="background-color: #FFF; background-image: url(\'http://www.reifax.com/img/bkgd_result.jpg\'); border: 3px solid #B8DAE3; min-height:135px; min-width:160px; padding: 5px 8px;"><div class="VE_Pushpin_Popup_Title">'+a+'</div><div class="VE_Pushpin_Popup_Body"><div align="center"><table border="0" style="white-space: nowrap;font-family:\'Trebuchet MS\',Arial,Helvetica,sans-serif;font-size:12px;"><tbody><tr><td rowspan="6" colspan="2" style="overflow:hidden;"><img src="'+urlImgC+'" height="105px" width="135px;"></td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Gross Area:</td><td>'+ga+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Living Area:</td><td>'+la+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Beds/Baths:</td><td>'+bb+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Price</td><td>'+p+'</td></tr><tr><td style=" font-weight:bold; margin-right:5px;">Status:</td><td>'+s+'</td></tr><tr><td onclick="'+co+'" style="font-weight:bold; margin-right:5px;cursor:pointer;color:blue;font-size:13px;" colspan="2">'+cot+'</td></tr></tbody></table></div></div></div>'
		}
	);

	var pin = new Microsoft.Maps.Pushpin(
		new Microsoft.Maps.Location(lat,lon),
		{
			icon: urlImg,
			text: i,
			textOffset: new Microsoft.Maps.Point(-2,1),
			width: 21,
			height: 18,
			infobox: infopin
		}
	);
	
	this.map.entities.push(pin);
	this.map.entities.push(infopin);
	
	Microsoft.Maps.Events.addHandler(pin, 'mouseover', pinMouseOver);
	Microsoft.Maps.Events.addHandler(infopin, 'mouseleave', infopinMouseOut);
	
	return pin;
}

XimaMap.prototype.addPushpinInfoboxMini = function(i, lat, lon,a,p,be,ba,sqft, s,url,pointColor){	
	var price=abbreviaNumber(p);
	var infopin = new Microsoft.Maps.Infobox(
		new Microsoft.Maps.Location(lat, lon),
		{
			width :100, 
			height :30, 
			showCloseButton: false, 
			offset:new Microsoft.Maps.Point(-12,-2), 
			showPointer: false,
			visible: false,
			zIndex: 100,
			htmlContent: '<div class="arrow_box"><h5>'+a+'</h5><div id="miniMapInfobox'+i+'" style="width:60px;height:60px; margin-left:2px; position:relative; float:left; background:#ccc;"></div><div style="float:left;width:124px;"><p>'+s+'</p>'+((parseInt(p)!=0)?'<p>'+formatNumber.new(parseInt(p), "$")+'</p>':'')+'<p>'+be+' Beds/ '+ba+' Baths<p>'+sqft+' sqft</p></div><div class="clearEmpty"></div><div style="text-align:center;">'+((url!=null)?'<a href="'+url+'">View Property</a>':'')+'</div></div>'
		}
	);
	
	var pin = new Microsoft.Maps.Pushpin(
		new Microsoft.Maps.Location(lat,lon),
		{
			text: i,
			htmlContent:'<div ><i style="display: block; margin-left: 18%; background:'+pointColor.color+'; border: solid 3px '+pointColor.border+'; width:10px; height:10px; border-radius:10px;"></i>'+((parseInt(p)!=0)?'<p style="background: none repeat scroll 0 0 #fff;border: 2px solid #000;border-radius: 4px;color: #000;font-size: 11px;font-style: normal;font-weight: bold;left: -50%;margin-top: 0;padding: 0px 4px; position: relative;margin-top:-1px; width:150%;">'+price+'</p>':'')+'</div>',
			textOffset: new Microsoft.Maps.Point(-2,1),
			width: 21,
			height: 18,
			infobox: infopin
		}
	);
	
	this.map.entities.push(pin);
	this.map.entities.push(infopin);
	Microsoft.Maps.Events.addHandler(pin, 'mouseover', this.pinMouseOver);
	Microsoft.Maps.Events.addHandler(infopin, 'mouseleave', this.infopinMouseOut);
	Microsoft.Maps.Events.addHandler(pin, 'mouseover', function () {
		setTimeout(function (){
			if($('#miniMapInfobox'+i).length){
				var imgTemp = document.createElement('img');
				imgTemp.src = 'http://maps.googleapis.com/maps/api/staticmap?center='+lat+','+lon+'&size=60x60&zoom=19&maptype=satellite&key=AIzaSyA4fFt-uXtzzFAgbFpHHOUm7bSXE8fvCb4';
				$('#miniMapInfobox'+i).html(imgTemp);
			}
		},500);
	});	
	return pin;
}

XimaMap.prototype.pinMouseOver=function (e){
	var pin = e.target ? e.target : e;
	var infopin = pushPinActive;
	if (infopin != null){ 
		infopin.setOptions({ visible: false });
	}
	if (pin != null){ 
		var infopin = pin.getInfobox();
		pushPinActive=infopin;
		infopin.setOptions({ visible: true });
	}
}

XimaMap.prototype.infopinMouseOut=function (e){
	var infopin = pushPinActive;
	if(infopin==null){
		var infopin = e.target ? e.target : e;
	}
	if (infopin != null){ 
		infopin.setOptions({ visible: false });
	}
}


function pinMouseOver(e){
	var pin = e.target ? e.target : e;
	var infopin = pushPinActive;
	if (infopin != null){ 
		infopin.setOptions({ visible: false });
	}
	if (pin != null){ 
		var infopin = pin.getInfobox();
		pushPinActive=infopin;
		infopin.setOptions({ visible: true });
	}
}

function infopinMouseOut(e){
	var infopin = pushPinActive;
	if(infopin==null){
		var infopin = e.target ? e.target : e;
	}
	if (infopin != null){ 
		infopin.setOptions({ visible: false });
	}
}

function setFirstImage(pid,imgID,bdcounty){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Setting...',
		url: 'http://'+(document.domain)+'/properties_setFirstImage.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			pid: pid,
			bd: bdcounty
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		success:function(response,options){
			
			var results=response.responseText;
			if(document.getElementById(imgID))
				document.getElementById(imgID).src=results;
		}                                
	});
}

function abbreviaNumber($numero) {
         var $abreviaturas = new Array('K', 'M', 'B', 'mB', 'mB');
         var $ultima_abreviatura = $abreviaturas.length-1;
         var $divisor = 1000;

        	var $sufijo = -1;

         while ($numero > $divisor && $sufijo < $ultima_abreviatura) {
                 $numero /= $divisor;
                 $sufijo++;
         }

         return parseInt($numero)+($sufijo>-1?$abreviaturas[$sufijo]:'');
}
var formatNumber = {
 separador: ",", // separador para los miles
 sepDecimal: '.', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}