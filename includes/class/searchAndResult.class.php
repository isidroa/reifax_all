<?php
	class searchAndResult{
		function searchAndResult(){
			return true;
		}
		function add($type,$data){
			switch((string)$type){
				case "search":
					return $this->addSearch($data);
				break;
				case "result":
					return $this->addResult($data);
				break;
			}
		}
		function replace($type,$data){
			switch((string)$type){
				case "search":
					return $this->replaceSearch($data);
				break;
				case "result":
					return $this->replaceResult($data);
				break;
			}
		}
		function delete($type,$data){
			switch((string)$type){
				case "search":
					return $this->deleteSearch($data);
				break;
				case "result":
					return $this->deleteResult($data);
				break;
			}
		}
		private function addSearch($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM savedsrch WHERE sid='{$data->id}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			$search	=	mysql_fetch_object($result);
			$query	=	"SELECT * FROM savedsrch WHERE userid='{$data->userid}' AND BINARY nombre='{$search->nombre}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			if(mysql_affected_rows()<=0){
				$query	=	"INSERT INTO savedsrch (SELECT null,'{$data->userid}',nombre,description,plantilla,savedVals FROM savedsrch WHERE sid='{$data->id}')";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been added', "Original: id:".mysql_insert_id()." Name: {$search->nombre}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}else
				return json_decode(json_encode(array("result"=>true)),FALSE);
		}
		private function addResult($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM gridtemplate WHERE idGrid='{$data->id}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			$search	=	mysql_fetch_object($result);
			$query	=	"SELECT * FROM gridtemplate WHERE userid='{$data->userid}' AND BINARY name='{$search->name}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			if(mysql_affected_rows()<=0){
				$query	=	"INSERT INTO gridtemplate (SELECT null,'{$data->userid}',type,campos,name FROM gridtemplate WHERE idGrid='{$data->id}')";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been added', "Original: id:".mysql_insert_id()." Name: {$search->name}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}else
				return json_decode(json_encode(array("result"=>true)),FALSE);
		}
		private function replaceSearch($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM savedsrch WHERE sid='{$data->id}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			$search	=	mysql_fetch_object($result);
			$query	=	"SELECT * FROM savedsrch WHERE userid='{$data->userid}' AND BINARY nombre='{$search->nombre}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			if(mysql_affected_rows()<=0){
				$query	=	"INSERT INTO savedsrch (SELECT null,'{$data->userid}',nombre,description,plantilla,savedVals FROM savedsrch WHERE sid='{$data->id}')";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been added', "Original: id:".mysql_insert_id()." Name: {$search->nombre}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}else{
				$original	=	mysql_fetch_object($result);
				$query	=	"
					UPDATE savedsrch SET
						nombre='{$search->nombre}',
						description='{$search->description}',
						plantilla='{$search->plantilla}',
						savedVals='{$search->savedVals}'
					WHERE sid={$original->sid}";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Search has been updated', "Original: id:{$original->sid} Name: {$search->nombre}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}
		}
		private function replaceResult($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM gridtemplate WHERE idGrid='{$data->id}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			$search	=	mysql_fetch_object($result);
			$query	=	"SELECT * FROM gridtemplate WHERE userid='{$data->userid}' AND BINARY name='{$search->name}'";
			$result	=	mysql_query($query) or die(json_encode(array('success'=>false,'msg'=>$query." -- ".mysql_error())));
			if(mysql_affected_rows()<=0){
				$query	=	"INSERT INTO gridtemplate (SELECT null,'{$data->userid}',type,campos,name FROM gridtemplate WHERE idGrid='{$data->id}')";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been added', "Original: id:".mysql_insert_id()." Name: {$search->name}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}else{
				$original	=	mysql_fetch_object($result);
				$query	=	"
					UPDATE gridtemplate SET
						type='{$search->type}',
						campos='{$search->campos}',
						name='{$search->name}'
					WHERE idGrid={$original->idGrid}";
				if(!$result	=	mysql_query($query))
					return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
				else{
					insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been updated', "Original: id:{$original->idGrid} Name: {$search->name}");
					return json_decode(json_encode(array("result"=>true)),FALSE);
				}
			}
		}
		private function deleteSearch($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM savedsrch WHERE sid ={$data->id}";
			$result	=	mysql_query($query);
			$search	=	mysql_fetch_object($result);
			$query	=	"DELETE FROM savedsrch WHERE sid ={$data->id}";
			if(!$result	=	mysql_query($query))
				return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
			else
				insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Search has been deleted', "Original: id:{$data->id} Name: {$search->nombre}");
			if(mysql_affected_rows()>0)
				return json_decode(json_encode(array("result"=>true)),FALSE);
			else
				return json_decode(json_encode(array("result"=>false,"msg"=>"File not Exists")),FALSE);
		}
		private function deleteResult($data){
			$data	=	json_decode(json_encode($data),FALSE);
			$query	=	"SELECT * FROM gridtemplate WHERE idGrid ={$data->id}";
			$result	=	mysql_query($query);
			$search	=	mysql_fetch_object($result);
			$query	=	"DELETE FROM gridtemplate WHERE idGrid ={$data->id}";
			if(!$result	=	mysql_query($query))
				return json_decode(json_encode(array("result"=>false,"msg"=>mysql_error())),FALSE);
			else
				insertlogs($_SESSION['bkouserid'], $data->userid, 'Advance Result has been deleted', "Original: id:{$data->id} Name: {$search->name}");
			if(mysql_affected_rows()>0)
				return json_decode(json_encode(array("result"=>true)),FALSE);
			else
				return json_decode(json_encode(array("result"=>false,"msg"=>"File not Exists")),FALSE);
		}
	}
?>