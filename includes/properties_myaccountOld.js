//Metodos para myaccount.php 
var timedel=5000;
function nuevoAjax()
{ 
	var xmlhttp=false; 
	try 
	{ xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 	}
	catch(e)
	{ 	try	{ xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); } 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
	return xmlhttp; 
}
function formatCurrency(num) 
{
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + '.' + cents);
}


function validaPersonalData()
{
	var  txname=document.getElementById("txname").value;
	var  txsurname=document.getElementById("txsurname").value;
	var  txnickname=document.getElementById("txnickname").value;
	var  cbState=document.getElementById("cbState").options[document.getElementById("cbState").selectedIndex].value;
	var  txcity=document.getElementById("txcity").value;
	var  txaddress=document.getElementById("txaddress").value;
	var  txhomephone=document.getElementById("txhomephone").value;
	var  txmobilephone=document.getElementById("txmobilephone").value;
	var  txemail=document.getElementById("txemail").value;
	

	
	if(txname.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your name';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txsurname.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your last name';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(document.getElementById("cbState").selectedIndex==0)	{ document.getElementById("msgerror1").innerHTML='Please select a state';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txcity.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your city';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txaddress.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your address';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txhomephone.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your phone number';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txemail.length==0)	{ document.getElementById("msgerror1").innerHTML='Please enter your email';setTimeout('limpiarMsgError(1)',timedel);return;}

	loading_win.show();
	var parametros="oper=PersonalData&userid="+useridlogin+"&txname="+txname+"&txsurname="+txsurname
					+"&txnickname="+txnickname+"&cbState="+cbState+"&txcity="+txcity+"&txaddress="+txaddress
					+"&txhomephone="+txhomephone+"&txmobilephone="+txmobilephone+"&txemail="+txemail;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
				document.getElementById("msgerror1").innerHTML=aRes[1];
			else
				document.getElementById("msgerror1").innerHTML="Error update. "+aRes[1];	
			setTimeout('limpiarMsgError(1)',timedel);	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaProfilePicture()
{
	var  txprofname=document.getElementById("txprofname").value;
	var  txprofemail=document.getElementById("txprofemail").value;
	var  txprofphone=document.getElementById("txprofphone").value;
    var  txprofaboutus=document.getElementById("txprofaboutus").value;
	var  txprofcontactus=document.getElementById("txprofcontactus").value;
	
	if(txprofname.length==0)	{ document.getElementById("msgerror2").innerHTML='Please enter your profile name';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txprofemail.length==0)	{ document.getElementById("msgerror2").innerHTML='Please enter your profile email';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txprofphone.length==0)	{ document.getElementById("msgerror2").innerHTML='Please enter your profile phone';setTimeout('limpiarMsgError(1)',timedel);return;}

	loading_win.show();
	var parametros="oper=ProfilePicture&userid="+useridlogin+"&txprofname="+txprofname+"&txprofemail="+txprofemail+"&txprofphone="+txprofphone+"&txprofaboutus="+txprofaboutus+"&txprofcontactus="+txprofcontactus;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
				document.getElementById("msgerror2").innerHTML=aRes[1];
			else
				document.getElementById("msgerror2").innerHTML="Error update";		
			setTimeout('limpiarMsgError(1)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaProfilePicturemyweb()
{
	var  txprofname1=document.getElementById("txprofname1").value;
	var  txprofcorreo=document.getElementById("txprofcorreo").value;
	var  txprofphone1=document.getElementById("txprofphone1").value;
    var  txprofaboutus1=document.getElementById("txprofaboutus1").value;
	var  txprofcontactus1=document.getElementById("txprofcontactus1").value;
	
	if(txprofname1.length==0)	{ document.getElementById("msgerror12").innerHTML='Please enter your profile name';setTimeout('limpiarMsgError(6)',timedel);return;}
	if(txprofcorreo.length==0)	{ document.getElementById("msgerror12").innerHTML='Please enter your profile email';setTimeout('limpiarMsgError(6)',timedel);return;}
	if(txprofphone1.length==0)	{ document.getElementById("msgerror12").innerHTML='Please enter your profile phone';setTimeout('limpiarMsgError(6)',timedel);return;}
	
	loading_win.show();
	var parametros="oper=ProfilePictureweb&userid="+useridlogin+"&txprofname1="+txprofname1+"&txprofcorreo="+txprofcorreo+"&txprofphone1="+txprofphone1+"&txprofaboutus1="+txprofaboutus1+"&txprofcontactus1="+txprofcontactus1;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
				document.getElementById("msgerror12").innerHTML=aRes[1];
			else
				document.getElementById("msgerror12").innerHTML="Error update";		
			setTimeout('limpiarMsgError(6)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validacustommyweb()
{
	var  fondo_pagina=document.getElementById("fondo_pagina").value;
	var  fondo_search=document.getElementById("fondo_search").value;
	var  fondo_result=document.getElementById("fondo_result").value;
	var  fondo_tab=document.getElementById("fondo_tab").value;
	var  search_font=document.getElementById("search_font").value;
	var  search_color=document.getElementById("search_color").value;
	var  search_size=document.getElementById("search_size").value;
	var  over_font=document.getElementById("over_font").value;
	var  over_color=document.getElementById("over_color").value;
	var  over_size=document.getElementById("over_size").value;
	var  tab_font=document.getElementById("tab_font").value;
	var  tab_color=document.getElementById("tab_color").value;
	var  tab_size=document.getElementById("tab_size").value;

	
	if(fondo_pagina.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color page';setTimeout('limpiarMsgError(5)',timedel);return;}
	if(fondo_tab.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color tabs';setTimeout('limpiarMsgError(5)',timedel);return;}
	if(fondo_search.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color search';setTimeout('limpiarMsgError(5)',timedel);return;}
		if(fondo_result.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color result';setTimeout('limpiarMsgError(5)',timedel);return;}
	if(search_color.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color';setTimeout('limpiarMsgError(5)',timedel);return;}
	if(over_color.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color';setTimeout('limpiarMsgError(5)',timedel);return;}
	if(tab_color.length==0)	{ document.getElementById("msgerror7").innerHTML='Please enter your color';setTimeout('limpiarMsgError(5)',timedel);return;}
	
	loading_win.show();
	var parametros="oper=Profilecustom&userid="+useridlogin+"&fondo_pagina="+fondo_pagina+"&fondo_result="+fondo_result+"&fondo_search="+fondo_search+"&search_font="+search_font+"&search_color="+search_color+"&search_size="+search_size+"&over_font="+over_font+"&over_color="+over_color+"&over_size="+over_size+"&tab_font="+tab_font+"&tab_color="+tab_color+"&tab_size="+tab_size+"&fondo_tab="+fondo_tab;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1){
				document.getElementById("msgerror7").innerHTML=aRes[1];
				document.getElementById("msgerror8").innerHTML=aRes[1];
				document.getElementById("msgerror9").innerHTML=aRes[1];
				document.getElementById("msgerror10").innerHTML=aRes[1];
				document.getElementById("msgerror11").innerHTML=aRes[1];
			}else{
				document.getElementById("msgerror7").innerHTML="Error update";	
				document.getElementById("msgerror8").innerHTML="Error update";	
				document.getElementById("msgerror9").innerHTML="Error update";	
				document.getElementById("msgerror10").innerHTML="Error update";	
				document.getElementById("msgerror11").innerHTML="Error update";	}
			setTimeout('limpiarMsgError(5)',timedel);	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaUpgradeAccount(){
	loading_win.show();
	var typePlan=document.getElementById("typePlan").value;
	var idproducto=document.getElementById("idproducto").value;
	var idfrecuency=document.getElementById("idfrecuency").value;
	var countCounty=document.getElementById("countCounty").value;
	var userid=document.getElementById("userid").value;
	var promcode = document.getElementById("promcode").value;
	
	var prorate=document.getElementById("prorate").value;
	
	if(prorate.length==0){
		document.getElementById("msgerror5").innerHTML='Please Choose the Plan that’s best for your business from top panel.';
		setTimeout('limpiarMsgError(2)',timedel);
		return;
	}
	
	var parametros="oper=UpgradeAccount&userid="+userid+"&typePlan="+typePlan+"&idproducto="+idproducto
					+"&idfrecuency="+idfrecuency+"&countCounty="+countCounty+"&prorate="+prorate+"&promcode="+promcode;

	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			loading_win.hide();
			
			document.getElementById("msgerror5").innerHTML=results;
			document.getElementById('btsave').disabled = true;
			setTimeout('limpiarMsgError(2)',timedel);	
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
	
}

function validaCreditCard()
{
	var  cardname=document.getElementById("cardname").options[document.getElementById("cardname").selectedIndex].value;
	var  holder=document.getElementById("holder").value;
	var  cardnumber=document.getElementById("cardnumber").value;
	var  exdate1=document.getElementById("exdate1").value;
	var  exdate2=document.getElementById("exdate2").value;
	var  csv=document.getElementById("csv").value;
	var  sameAdd=document.getElementById("sameAdd").value;
	var  sameCity=document.getElementById("sameCity").value;
	var  sameState=document.getElementById("sameState").options[document.getElementById("sameState").selectedIndex].value;
	var  sameZip=document.getElementById("sameZip").value;

	
	if(document.getElementById("cardname").selectedIndex==0)	{ document.getElementById("msgerror3").innerHTML='Please select a card type';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(holder.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your holder name';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(cardnumber.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your card number';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(exdate1.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(exdate2.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(csv.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your csv number';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(sameAdd.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your address';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(sameCity.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your city';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(document.getElementById("sameState").selectedIndex==0)	{ document.getElementById("msgerror3").innerHTML='Please select a state';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(sameZip.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your zip';setTimeout('limpiarMsgError(1)',timedel);return;}
	
	loading_win.show();
	var parametros="oper=CreditCard&userid="+useridlogin+"&cardname="+cardname+"&holder="+holder
					+"&cardnumber="+cardnumber+"&sameState="+sameState+"&exdate1="+exdate1+"&exdate2="+exdate2+"&csv="+csv+"&sameAdd="+sameAdd
					+"&sameCity="+sameCity+"&sameState="+sameState+"&sameZip="+sameZip;					
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror3").innerHTML=aRes[1];
				document.getElementById("cardnumber").value=document.getElementById("cardnumber").value.substr(0,4)+"********"+document.getElementById("cardnumber").value.substr(12,4);
			}
			else
				document.getElementById("msgerror3").innerHTML="Error update";
			setTimeout('limpiarMsgError(1)',timedel);	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaPasswordData()
{
	var  txpasswordcur=document.getElementById("txpasswordcur").value;
	var  txpassword=document.getElementById("txpassword").value;
	var  txpasswordconf=document.getElementById("txpasswordconf").value;

	if(txpasswordcur.length==0)	{ document.getElementById("msgerror4").innerHTML='Please enter your current password';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txpassword.length==0)	{ document.getElementById("msgerror4").innerHTML='Please enter your new password';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txpasswordconf.length==0)	{ document.getElementById("msgerror4").innerHTML='Please enter your new password confirmation';setTimeout('limpiarMsgError(1)',timedel);return;}
	if(txpassword!=txpasswordconf)	{ document.getElementById("msgerror4").innerHTML="The Confirmation Password doesn't match the Password. Please enter the same Password";setTimeout('limpiarMsgError(1)',timedel);return;}


	loading_win.show();
	var parametros="oper=PasswordData&userid="+useridlogin+"&txpasswordcur="+txpasswordcur
					+"&txpassword="+txpassword+"&txpasswordconf="+txpasswordconf;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
				document.getElementById("msgerror4").innerHTML=aRes[1];
			else if(aRes[0]==0)
				document.getElementById("msgerror4").innerHTML=aRes[1];
			else
				document.getElementById("msgerror4").innerHTML="Error update";	
			setTimeout('limpiarMsgError(1)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}


function validaCancelAccount()
{
	var  msg_cancel=document.getElementById("msg_cancel").value;
	if(msg_cancel.length==0)	{ document.getElementById("msgerror5").innerHTML='Please inform the reason you want to cancel the system';setTimeout('limpiarMsgError(2)',timedel);return;}
	loading_win.show();
	var parametros="oper=CancelAccount&userid="+useridlogin+"&msg_cancel="+msg_cancel;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror5").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				setTimeout('session_release()',1000);
			}
			else
				document.getElementById("msgerror5").innerHTML="Error cancel account";	
			setTimeout('limpiarMsgError(2)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaFreezeAccount()
{
	var  msg_cancel=document.getElementById("msg_cancel").value;
	if(msg_cancel.length==0)	{ document.getElementById("msgerror6").innerHTML='Please inform the reason you want to freeze the system';setTimeout('limpiarMsgError(3)',timedel);return;}
	loading_win.show();
	var parametros="oper=FreezeAccount&userid="+useridlogin+"&msg_cancel="+msg_cancel;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror6").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror6").innerHTML=aRes[1];
				setTimeout('session_release()',1000);
			}
			else
				document.getElementById("msgerror6").innerHTML="Error freeze account";	
			setTimeout('limpiarMsgError(3)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaunFreezeAccount()
{
	limpiarMsgError(3);
	loading_win.show();
	var parametros="oper=unFreezeAccount&userid="+useridlogin;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror6").innerHTML=results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror6").innerHTML=aRes[1];
				location.href='properties_search.php';
				//setTimeout('session_release()',1000);
			}
			else if(aRes[0]==0)
				document.getElementById("msgerror6").innerHTML="Error account activation. "+aRes[1];
			else
				document.getElementById("msgerror6").innerHTML="Error account activation";	
			//setTimeout('limpiarMsgError(3)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}



function validaChangeCreditCard()
{
	var  cardname=document.getElementById("cardname").options[document.getElementById("cardname").selectedIndex].value;
	var  holder=document.getElementById("holder").value;
	var  cardnumber=document.getElementById("cardnumber").value;
	var  exdate1=document.getElementById("exdate1").value;
	var  exdate2=document.getElementById("exdate2").value;
	var  csv=document.getElementById("csv").value;
	var  sameAdd=document.getElementById("sameAdd").value;
	var  sameCity=document.getElementById("sameCity").value;
	var  sameState=document.getElementById("sameState").options[document.getElementById("sameState").selectedIndex].value;
	var  sameZip=document.getElementById("sameZip").value;

	
	if(document.getElementById("cardname").selectedIndex==0)	{ document.getElementById("msgerror3").innerHTML='Please select a card type';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(holder.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your holder name';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(cardnumber.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your card number';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(exdate1.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(exdate2.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(csv.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your csv number';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(sameAdd.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your address';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(sameCity.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your city';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(document.getElementById("sameState").selectedIndex==0)	{ document.getElementById("msgerror3").innerHTML='Please select a state';setTimeout('limpiarMsgError(4)',timedel);return;}
	if(sameZip.length==0)	{ document.getElementById("msgerror3").innerHTML='Please enter your zip';setTimeout('limpiarMsgError(4)',timedel);return;}
	
	var expdate=exdate1.toString()+'/'+exdate2.toString();

	loading_win.show();
	var parametros="oper=activeuserinactive&userid="+useridlogin+"&cardname="+cardname+"&holder="+holder+"&exdate1="+exdate1+"&exdate2="+exdate2
					+"&cardnumber="+cardnumber+"&sameState="+sameState+"&expdate="+expdate+"&csv="+csv+"&sameAdd="+sameAdd
					+"&sameCity="+sameCity+"&sameState="+sameState+"&sameZip="+sameZip;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror3").innerHTML=results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror3").innerHTML=aRes[1];
				location.href='properties_search.php';
				/*document.getElementById("cardnumber").value=document.getElementById("cardnumber").value.substr(0,4)+"********"+document.getElementById("cardnumber").value.substr(12,4);*/
			}
			else if(aRes[0]==0)
				document.getElementById("msgerror3").innerHTML=aRes[1];
			else
			{
				document.getElementById("msgerror3").innerHTML="Error account activation";
				setTimeout('limpiarMsgError(4)',timedel);	
			}
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaActivateCommercial()
{
	loading_win.show();
	var newprice=document.getElementById("newprice").value;
	var proratecommercial=document.getElementById("proratecommercial").value;
	var pricecommercial=document.getElementById("pricecommercial").value;
	var parametros="oper=ActivateCommercial&userid="+useridlogin+"&newprice="+newprice+"&proratecommercial="+proratecommercial+"&pricecommercial="+pricecommercial;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror6").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else if(aRes[0]==0)
				document.getElementById("msgerror5").innerHTML=aRes[1];
			else
				document.getElementById("msgerror5").innerHTML="Error the New Residential and Commercial Version activation";	
			setTimeout('limpiarMsgError(2)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function cleancheck()
{
	if(document.getElementById("chcommercial").checked==true)
	{
		if(document.getElementById("payfrec"))
		document.getElementById("payfrec2").value=document.getElementById("payfrec").value;
		if(document.getElementById("curpayment"))
		document.getElementById("curpayment2").value='$'+formatCurrency(document.getElementById("curpayment").value);
		if(document.getElementById("paydatecur"))
		document.getElementById("paydatecur2").value=document.getElementById("paydatecur").value;
		if(document.getElementById("diasfaltan"))
		document.getElementById("diasfaltan2").value=document.getElementById("diasfaltan").value;
		if(document.getElementById("newprice2"))
		document.getElementById("newprice2").value='$'+formatCurrency(document.getElementById("newprice").value);
		if(document.getElementById("proratecommercial"))
		document.getElementById("proratecommercial2").value='$'+formatCurrency(document.getElementById("proratecommercial").value);
		if(document.getElementById("pricecommercial"))
		document.getElementById("pricecommercial2").value='$'+formatCurrency(document.getElementById("pricecommercial").value);
		if(document.getElementById("btsave"))
		document.getElementById("btsave").disabled = false; 
	}
	else
	{
		if(document.getElementById("payfrec2"))
		document.getElementById("payfrec2").value="";
		if(document.getElementById("curpayment2"))
		document.getElementById("curpayment2").value="";
		if(document.getElementById("paydatecur2"))
		document.getElementById("paydatecur2").value="";
		if(document.getElementById("diasfaltan2"))
		document.getElementById("diasfaltan2").value="";
		if(document.getElementById("newprice2"))
		document.getElementById("newprice2").value="";
		if(document.getElementById("proratecommercial2"))
		document.getElementById("proratecommercial2").value="";
		if(document.getElementById("pricecommercial2"))
		document.getElementById("pricecommercial2").value="";
		if(document.getElementById("btsave"))
		document.getElementById("btsave").disabled = true; 
	}
}

function validaDisableCommercial()
{
	loading_win.show();
	var newprice=document.getElementById("newprice").value;
	var parametros="oper=DisableCommercial&userid="+useridlogin+"&newprice="+newprice;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror6").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				setTimeout('session_release()',1000);
			}
			else
				document.getElementById("msgerror5").innerHTML="Error the New Residential and Commercial Version disable";	
			setTimeout('limpiarMsgError(2)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaActivateRealtorweb(typeuser)
{
	loading_win.show();
	var parametros="oper=ActivateRealtorweb&userid="+useridlogin+'&typeuser='+typeuser;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror6").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else if(aRes[0]==0)
				document.getElementById("msgerror5").innerHTML=aRes[1];
			else
				document.getElementById("msgerror5").innerHTML="Error the Xima's Realtors Website activation";	
			setTimeout('limpiarMsgError(2)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaDisableRealtorweb(typeuser)
{
	loading_win.show();
	var parametros="oper=DisableRealtorweb&userid="+useridlogin+'&typeuser='+typeuser;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//document.getElementById("msgerror5").innerHTML=	results;return;
			var aRes=results.split("^");
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else
				document.getElementById("msgerror5").innerHTML="Error the Xima's Realtors Website disable";	
			setTimeout('limpiarMsgError(2)',timedel);
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange
}

function validaActivateAllCounties()
{
	loading_win.show();
	var newprice=document.getElementById("newprice").value;
	var prorate=document.getElementById("proratecommercial").value;
	var parametros="oper=allcountyactivate&userid="+useridlogin+"&newprice="+newprice+"&prorate="+prorate+"&close=0";
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else
				document.getElementById("msgerror5").innerHTML=aRes[1];//"Error the All Counties Promotion activation. "+	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

}

function validaAdvertisingRealtorweb()
{
	var  cardtype=document.getElementById("cardtype").options[document.getElementById("cardtype").selectedIndex].value;
	var  holder=document.getElementById("holder").value;
	var  cardnumber=document.getElementById("cardnumber").value;
	var  exdate1=document.getElementById("exdate1").value;
	var  exdate2=document.getElementById("exdate2").value;
	var  csv=document.getElementById("csv").value;
	var  sameAdd=document.getElementById("sameAdd").value;
	var  sameCity=document.getElementById("sameCity").value;
	var  sameState=document.getElementById("sameState").options[document.getElementById("sameState").selectedIndex].value;
	var  sameZip=document.getElementById("sameZip").value;

	
	if(document.getElementById("cardtype").selectedIndex==0)	{ document.getElementById("msgerror5").innerHTML='Please select a card type';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(holder.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your holder name';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(cardnumber.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your card number';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(exdate1.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(exdate2.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(csv.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your csv number';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameAdd.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your address';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameCity.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your city';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(document.getElementById("sameState").selectedIndex==0)	{ document.getElementById("msgerror5").innerHTML='Please select a state';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameZip.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your zip';setTimeout('limpiarMsgError(7)',timedel);return;}
	
	var expdate=exdate1.toString()+'/'+exdate2.toString();

	loading_win.show();
	var parametros="oper=advertisingrealtorweb&userid="+useridlogin+"&cardtype="+cardtype+"&holder="+holder
					+"&cardnumber="+cardnumber+"&sameState="+sameState+"&expdate="+expdate+"&csv="+csv+"&sameAdd="+sameAdd
					+"&sameCity="+sameCity+"&sameState="+sameState+"&sameZip="+sameZip;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else
				document.getElementById("msgerror5").innerHTML="Error the Advertising activation";	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

}

function validaXimausaActivate()
{
	var  cardtype=document.getElementById("cardtype").options[document.getElementById("cardtype").selectedIndex].value;
	var  holder=document.getElementById("holder").value;
	var  cardnumber=document.getElementById("cardnumber").value;
	var  exdate1=document.getElementById("exdate1").value;
	var  exdate2=document.getElementById("exdate2").value;
	var  csv=document.getElementById("csv").value;
	var  sameAdd=document.getElementById("sameAdd").value;
	var  sameCity=document.getElementById("sameCity").value;
	var  sameState=document.getElementById("sameState").options[document.getElementById("sameState").selectedIndex].value;
	var  sameZip=document.getElementById("sameZip").value;
	var  pricepay=document.getElementById("pricepay").value;
	var  pcid=document.getElementById("pcid").value;

	
	if(document.getElementById("cardtype").selectedIndex==0)	{ document.getElementById("msgerror5").innerHTML='Please select a card type';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(holder.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your holder name';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(cardnumber.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your card number';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(exdate1.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(exdate2.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your expiration date';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(csv.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your csv number';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameAdd.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your address';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameCity.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your city';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(document.getElementById("sameState").selectedIndex==0)	{ document.getElementById("msgerror5").innerHTML='Please select a state';setTimeout('limpiarMsgError(7)',timedel);return;}
	if(sameZip.length==0)	{ document.getElementById("msgerror5").innerHTML='Please enter your zip';setTimeout('limpiarMsgError(7)',timedel);return;}
	
	var expdate=exdate1.toString()+'/'+exdate2.toString();

	loading_win.show();
	var parametros="oper=ximausaactivate&userid="+useridlogin+"&cardtype="+cardtype+"&holder="+holder
					+"&cardnumber="+cardnumber+"&sameState="+sameState+"&expdate="+expdate+"&csv="+csv+"&sameAdd="+sameAdd
					+"&sameCity="+sameCity+"&sameState="+sameState+"&sameZip="+sameZip+"&pricepay="+pricepay+"&pcid="+pcid;
//alert(parametros);	return;		
	var ajax=nuevoAjax();
	ajax.open("POST", "includes/properties_updateuser.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);return;
			var aRes=results.split("^");
			
			loading_win.hide();
			if(aRes[0]==1)
			{
				document.getElementById("msgerror5").innerHTML=aRes[1];
				location.href='properties_search.php';
			}
			else
				document.getElementById("msgerror5").innerHTML="Error  Ximausa Account  activation";	
			//document.getElementById("waitGuarda").style.visibility="hidden";
		} //fin de ajax readyState
	}//fin de ajax.onreadystatechange

}

function limpiarMsgError(donde)
{
	if(donde==1)//personal data
	{
		document.getElementById("msgerror1").innerHTML='&nbsp;';
		document.getElementById("msgerror2").innerHTML='&nbsp;';
		document.getElementById("msgerror3").innerHTML='&nbsp;';
		document.getElementById("msgerror4").innerHTML='&nbsp;';
		
	}
	if(donde==2 || donde==7)//cancel account
		document.getElementById("msgerror5").innerHTML='&nbsp;';
	if(donde==3)//freze y un freze account
		document.getElementById("msgerror6").innerHTML='&nbsp;';
	if(donde==4)//activar de cuando esta inactive
		document.getElementById("msgerror3").innerHTML='&nbsp;';
	if(donde==5){
	document.getElementById("msgerror7").innerHTML='&nbsp;';
	document.getElementById("msgerror8").innerHTML='&nbsp;';
	document.getElementById("msgerror9").innerHTML='&nbsp;';
	document.getElementById("msgerror10").innerHTML='&nbsp;';
	document.getElementById("msgerror11").innerHTML='&nbsp;';
	}
	if(donde==6)//activar de cuando esta inactive
		document.getElementById("msgerror12").innerHTML='&nbsp;';	
}

