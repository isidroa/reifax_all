<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 1px; 
}
</style>
<div align="left" id="todo_myfollowagent_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowagent_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowagent_filters"></div><br />
        <div id="myfollowagent_properties" align="left"></div> 
	</div>
</div>
<script>
	var limitmyfollowagent = 50;
	var selected_datamyfollowagent = new Array();
	var AllCheckmyfollowagent = false;
	
	var filterfield='agent';
	var filterdirection='ASC';
	var filteragentnicknamemyfollowagentnickname ='';
	var filteragentmyfollowagent 	= '';
	var filteremailmyfollowagent 	= '';
	var filterphonemyfollowagent 	= '';
	var filterwebmyfollowagent 	= '';
	var filteraddressmyfollowagent 	= '';
	var filtercompanymyfollowagent 	= '';
	var filtertypemyfollowagent 	= 'ALL';
	
	var storemyfollowagent = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
		fields: [
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
				{name: 'agentnickname'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				storemyfollowagentAll.load();
				AllCheckmyfollowagent=false;
				selected_datamyfollowagent=new Array();
				smmyfollowagent.deselectRange(0,limitmyfollowagent);
				obj.params.agent = filteragentmyfollowagent;
				obj.params.agentnickname = filteragentnicknamemyfollowagentnickname;
				obj.params.email = filteremailmyfollowagent;
				obj.params.phone = filterphonemyfollowagent;
				obj.params.web = filterwebmyfollowagent;
				obj.params.address = filteraddressmyfollowagent;
				obj.params.company = filtercompanymyfollowagent;
				obj.params.typeagent = filtertypemyfollowagent;
			},
			'load' : function (store,data,obj){
				if (AllCheckmyfollowagent){
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowagent=true;
					gridmyfollowagent.getSelectionModel().selectAll();
					selected_datamyfollowagent=new Array();
				}else{
					AllCheckmyfollowagent=false;
					Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowagent.length > 0){
						for(val in selected_datamyfollowagent){
							var ind = gridmyfollowagent.getStore().find('agentid',selected_datamyfollowagent[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowagent.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storemyfollowagentAll = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
		fields: [
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'agentnickname'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.agent = filteragentmyfollowagent;
				obj.params.agentnickname = filteragentnicknamemyfollowagentnickname;
				obj.params.email = filteremailmyfollowagent;
				obj.params.phone = filterphonemyfollowagent;
				obj.params.web = filterwebmyfollowagent;
				obj.params.address = filteraddressmyfollowagent;
				obj.params.company = filtercompanymyfollowagent;
				obj.params.typeagent = filtertypemyfollowagent;
			}
		}
    });
	var smmyfollowagent = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowagent.indexOf(record.get('agentid'))==-1)
					selected_datamyfollowagent.push(record.get('agentid'));
				
				if(Ext.fly(gridmyfollowagent.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowagent=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowagent = selected_datamyfollowagent.remove(record.get('agentid'));
				AllCheckmyfollowagent=false;
				Ext.get(gridmyfollowagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtagents=new Ext.Toolbar({
		renderTo: 'myfollowagent_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Contacts to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var agentids=selected_datamyfollowagent[0];
					for(i=1; i<selected_datamyfollowagent.length; i++)
						agentids+=','+selected_datamyfollowagent[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
							Ext.Msg.alert("Follow Contact", 'Contact delete.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'New Contact',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						title: 'Follow Contact',
						width: 425,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 300},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'agent',
									fieldLabel: 'Contact',
									allowBlank: false
								}// Agregado por Lui R CAstro Sugerencia 11932 13/05/2015
								,{
									xtype     : 'textfield',
									name      : 'agentnickname',
									fieldLabel: 'Nick Name',
									allowBlank: true
									
								}/////////////////////////////////
								,{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'storetype',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'agenttype',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'idtype', type:'string'},
											{name:'name', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
									}),
									displayField  : 'name',
									valueField    : 'idtype',
									name          : 'fagenttype',
									value         : 'Agent',
									hiddenName    : 'agenttype',
									hiddenValue   : '1',
									allowBlank    : false,
									listeners	  : {
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Email',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyemail1',
										value         : '0',
										hiddenName    : 'typeemail1',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'email',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Email 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyemail2',
										value         : '0',
										hiddenName    : 'typeemail2',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'email2',
										width	  : 165
									}]
								},{
									xtype     : 'textfield',
									name      : 'company',
									width	  : 250,
									fieldLabel: 'Company'
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Website 1',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Personal'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyurl1',
										value         : '0',
										hiddenName    : 'typeurl1',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'urlsend',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Website 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Personal'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyurl2',
										value         : '0',
										hiddenName    : 'typeurl2',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'urlsend2',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname1',
										value         : '0',
										hiddenName    : 'typeph1',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone1',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname2',
										value         : '0',

										hiddenName    : 'typeph2',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone2',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 3',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname3',
										value         : '0',
										hiddenName    : 'typeph3',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone3',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 4',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname4',
										value         : '0',
										hiddenName    : 'typeph4',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'fax',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 5',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname5',
										value         : '0',
										hiddenName    : 'typeph5',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'tollfree',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 6',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname6',
										value         : '0',
										hiddenName    : 'typeph6',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone6',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Address 1',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyaddress1',
										value         : '0',
										hiddenName    : 'typeaddress1',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'address1',
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Address 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyaddress2',
										value         : '0',
										hiddenName    : 'typeaddress2',
										hiddenValue   : '0',
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'address2',
										width	  : 165
									}]
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'insert'
								}],
						
						buttons: [{
								text: 'Insert',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Contact", action.result.msg);
											storemyfollowagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", "ERROR");
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 460,
						height      : 600,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			}),new Ext.Button({
				tooltip: 'Edit Contact',
				id: 'editContactbutton',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/update.png',
				handler: function(){
					var agents = smmyfollowagent.getSelections();
					if(agents.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Contact to be edited.'); return false;
					}else if(agents.length > 1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one Contact to be edited.'); return false;
					}

					var agent = agents[0];
					
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						title: 'Follow Contact',
						width: 425,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 300},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'agent',
									fieldLabel: 'Contact',
									allowBlank: false,
									value	  : agent.get('agent')
								},//Agregado por Luis R CAstro Sugerencia 11932 13/05/2015
								{
									xtype     : 'textfield',
									name      : 'agentnickname',
									fieldLabel: 'Nick Name',
									allowBlank: true,
									value	  : agent.get('agentnickname')
								}///////////////////////////////////////////////////////////
								,{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'storetype',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'agenttype',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'idtype', type:'string'},
											{name:'name', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
									}),
									displayField  : 'name',
									valueField    : 'idtype',
									name          : 'fagenttype',
									value         : agent.get('agentype'),
									hiddenName    : 'agenttype',
									hiddenValue   : agent.get('agenttype'),
									allowBlank    : false,
									listeners	  : {
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Email',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyemail1',
										value	  : agent.get('typeemail1'),
										hiddenName    : 'typeemail1',
										hiddenValue   : agent.get('typeemail1'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'email',
										value	  : agent.get('email'),
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Email 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyemail2',
										value         : agent.get('typeemail2'),
										hiddenName    : 'typeemail2',
										hiddenValue   : agent.get('typeemail2'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'email2',
										value	  : agent.get('email2'),
										width	  : 165
									}]
								},{
									xtype     : 'textfield',
									name      : 'company',
									fieldLabel: 'Company',
									width	  : 250,	
									value	  : agent.get('company')
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Website 1',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Personal'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyurl1',
										value         : agent.get('typeurl1'),
										hiddenName    : 'typeurl1',
										hiddenValue   : agent.get('typeurl1'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'urlsend',
										value     : agent.get('urlsend'),
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Website 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Personal'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyurl2',
										value         : agent.get('typeurl2'),
										hiddenName    : 'typeurl2',
										hiddenValue   : agent.get('typeurl2'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'urlsend2',
										value     : agent.get('urlsend2'),
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname1',
										value         : agent.get('typeph1'),
										hiddenName    : 'typeph1',
										hiddenValue   : agent.get('typeph1'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone1',
										width	  : 165,
										value	  : agent.get('phone1')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname2',
										value         : agent.get('typeph2'),
										hiddenName    : 'typeph2',
										hiddenValue   : agent.get('typeph2'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone2',
										width	  : 165,
										value	  : agent.get('phone2')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 3',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname3',
										value         : agent.get('typeph3'),
										hiddenName    : 'typeph3',
										hiddenValue   : agent.get('typeph3'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone3',
										width	  : 165,
										value	  : agent.get('phone3')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 4',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname4',
										value         : agent.get('typeph4'),
										hiddenName    : 'typeph4',
										hiddenValue   : agent.get('typeph4'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'fax',
										width	  : 165,
										value	  : agent.get('fax')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 5',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname5',
										value         : agent.get('typeph5'),
										hiddenName    : 'typeph5',
										hiddenValue   : agent.get('typeph5'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'tollfree',
										width	  : 165,
										value	  : agent.get('tollfree')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Phone 6',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office'],
												['2','Cell'],
												['3','Home Fax'],
												['6','Office Fax'],
												['4','TollFree'],
												['5','O. TollFree']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'typephname6',
										value         : agent.get('typeph6'),
										hiddenName    : 'typeph6',
										hiddenValue   : agent.get('typeph6'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'phone6',
										width	  : 165,
										value	  : agent.get('phone6')
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Address 1',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyaddress1',
										value	  : agent.get('typeaddress1'),
										hiddenName    : 'typeaddress1',
										hiddenValue   : agent.get('typeaddress1'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'address1',
										value	  : agent.get('address1'),
										width	  : 165
									}]
								},{
									xtype	  : 'compositefield',
									fieldLabel: 'Address 2',
									items	  : [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										width		  : 120,
										editable	  : false,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['0','Home'],
												['1','Office']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'tyaddress2',
										value         : agent.get('typeaddress2'),
										hiddenName    : 'typeaddress2',
										hiddenValue   : agent.get('typeaddress2'),
										allowBlank    : false
									},{
										xtype     : 'textfield',
										name      : 'address2',
										value	  : agent.get('address2'),
										width	  : 165
									}]
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'update'
								},{
									xtype     : 'hidden',
									name      : 'agentid',
									value     : agent.get('agentid')
								}],
						
						buttons: [{
								text: 'Update',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Contact", 'Updated Contact.');
											storemyfollowagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
												Ext.Msg.alert('Error',
													'Status:'+action.response.status+': '+
													action.response.statusText);
											}
											if (action.failureType === Ext.form.Action.SERVER_INVALID){
												// server responded with success = false
												Ext.Msg.alert('Invalid', action.result.errormsg);
											}
											if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
												Ext.Msg.alert('Error',
													'Please check de red market field, before update Contact.');
											}
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 460,
						height      : 600,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			}),new Ext.Button({
				tooltip: 'Print Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyfollowagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to print.'); return false;
					}
					if(AllCheckmyfollowagent==true){
						var totales = storemyfollowagentAll.getRange(0,storemyfollowagentAll.getCount());
						var agentids='\''+totales[0].data.agentid+'\'';
						for(i=1;i<storemyfollowagentAll.getCount();i++){
							agentids+=',\''+totales[i].data.agentid+'\'';	
						}
					}else{	
						var agentids=selected_datamyfollowagent[0];
						for(i=1; i<selected_datamyfollowagent.length; i++)
							agentids+=','+selected_datamyfollowagent[i];
					}
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection,
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyfollowagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to export.'); return false;
					}
					if(AllCheckmyfollowagent==true){
						var totales = storemyfollowagentAll.getRange(0,storemyfollowagentAll.getCount());
						var agentids='\''+totales[0].data.agentid+'\'';
						for(i=1;i<storemyfollowagentAll.getCount();i++){
							agentids+=',\''+totales[i].data.agentid+'\'';	
						}
					}else{	
						var agentids=selected_datamyfollowagent[0];
						for(i=1; i<selected_datamyfollowagent.length; i++)
							agentids+=','+selected_datamyfollowagent[i];
					}
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection,
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){					
					var formmyfollowagent = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						collapsible: true,
						collapsed: false,
						title: 'Filters',
						//renderTo: 'myfollowagent_filters',
						id: 'formmyfollowagent',
						name: 'formmyfollowagent',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									value         : filteragentmyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteragentmyfollowagent=newvalue;
										}
									}
								},{
									xtype		  : 'textfield',
									fieldLabel	  : 'Nick Name',
									name		  : 'fagentnickname',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteragentnicknamemyfollowagentnickname=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftypeagent',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'storetype',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'agenttype',
											'userid': <?php echo $userid;?>,
											selectall: 'all'
										},
										fields:[
											{name:'idtype', type:'string'},
											{name:'name', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
									}),
									displayField  : 'name',
									valueField    : 'idtype',
									name          : 'fagenttype',
									hiddenName    : 'agenttype',
									hiddenValue   : filtertypemyfollowagent,
									allowBlank    : false,
									listeners	  : {
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filtertypemyfollowagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email',
									name		  : 'femail',
									value		  : filteremailmyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteremailmyfollowagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fphone',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Phone',
									name		  : 'fphone',
									value		  : filterphonemyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterphonemyfollowagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fweb',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Website',
									name		  : 'fweb',
									value		  : filterwebmyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterwebmyfollowagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filteraddressmyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcompany',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Company',
									name		  : 'fcompany',
									value		  : filtercompanymyfollowagent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtercompanymyfollowagent=newvalue;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteragentmyfollowagent 	= '';
									filteragentnicknamemyfollowagentnickname='';
									filteremailmyfollowagent 	= '';
									filterphonemyfollowagent 	= '';
									filterwebmyfollowagent 	= '';
									filteraddressmyfollowagent 	= '';
									filtercompanymyfollowagent 	= '';
									filtertypemyfollowagent 	= 'ALL';
									Ext.getCmp('formmyfollowagent').getForm().reset();
									
									storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 690,
								height      : 330,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowagent,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filteragentmyfollowagent 	= '';
					filteragentnicknamemyfollowagentnickname='';
					filteremailmyfollowagent 	= '';
					filterphonemyfollowagent 	= '';
					filterwebmyfollowagent 	= '';
					filteraddressmyfollowagent 	= '';
					filtercompanymyfollowagent 	= '';
					filtertypemyfollowagent 	= 'ALL';
					storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
					//storemyfollowupAll.load();
				}
			}),new Ext.Button({
				tooltip: 'Block Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
					if(selected_datamyfollowagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to block.'); return false;
					}
					if(AllCheckmyfollowagent==true){
						selected_datamyfollowagent=new Array();
						var totales = storemyfollowagentAll.getRange(0,storemyfollowagentAll.getCount());
						
						for(i=0;i<storemyfollowagentAll.getCount();i++){
							if(selected_datamyfollowagent.indexOf(totales[i].data.agentid)==-1)
								selected_datamyfollowagent.push(totales[i].data.agentid);
						}
					
					}
					var agentids=selected_datamyfollowagent[0];
					for(i=1; i<selected_datamyfollowagent.length; i++)
						agentids+=','+selected_datamyfollowagent[i];
					
					Ext.MessageBox.show({
					    title:    'Contacts',
					    msg:      'Block properties of the contacts?',
					    buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
					    fn: function(btn){
								if(btn=='cancel'){
									return;
								}
								loading_win.show();
					
								Ext.Ajax.request( 
								{  
									waitMsg: 'Checking...',
									url: 'mysetting_tabs/myfollowup_tabs/properties_blockagent.php', 
									method: 'POST',
									timeout :600000,
									params: { 
										type: 'block',
										agentids: agentids,
										blockproperties: btn
									},
									
									failure:function(response,options){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','ERROR');
									},
									
									success:function(response,options){
										var resp   = Ext.decode(response.responseText);
										loading_win.hide();
										
										Ext.Msg.alert("Follow Contacts", selected_datamyfollowagent.length+' Contacts added to Block.<br>'+resp.properties+' Properties blocked');
										storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
										/*if(storemyblockagent){
											storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
										}*/ 
									}                                
								});
							}
					});
					
				}
			}),/*new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('FollowupContact');
				}
			})	*/
		]
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var num = colIndex-4;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = 'H';
		else if(tyvalue == 1){ 
			classvalue = 'O';
		}else if(tyvalue == 2){ 
			classvalue = 'C';
		}else if(tyvalue == 3){ 
			classvalue = 'HF';
		}else if(tyvalue == 4){ 
			classvalue = 'TF';
		}else if(tyvalue == 5){ 
			classvalue = 'OTF';
		}else if(tyvalue == 6){ 
			classvalue = 'OF';
		}
		//console.debug(classvalue+' '+tyvalue);
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function emailRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-2;
		var type = 'typeemail'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function webRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-10;
		var type = 'typeurl'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function addressRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-13;
		var type = 'typeaddress'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(H)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}	
	var gridmyfollowagent = new Ext.grid.GridPanel({
		renderTo: 'myfollowagent_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowagent,
		stripeRows: true,
		sm: smmyfollowagent, 
		columns: [
			smmyfollowagent,
			{header: 'Contact', width: 150, sortable: true, tooltip: 'Contact name,', dataIndex: 'agent'}
			,{header: 'NickName', width: 180, sortable: true, tooltip: 'NickName,', dataIndex: 'agentnickname'}
			,{header: 'Type', width: 150, sortable: true, tooltip: 'Contact type,', dataIndex: 'agentype'}
			,{header: 'Email 1', width: 150, sortable: true, tooltip: 'Email 1', dataIndex: 'email', renderer: emailRender} 
			,{header: 'Email 2', width: 150, sortable: true, tooltip: 'Email 2', dataIndex: 'email2', renderer: emailRender}
			,{header: 'Phone 1', width: 100, sortable: true, tooltip: 'Phone 1.', dataIndex: 'phone1', renderer: phoneRender}
			,{header: 'Phone 2', width: 100, sortable: true, tooltip: 'Phone 2.', dataIndex: 'phone2', renderer: phoneRender}
			,{header: 'Phone 3', width: 100, sortable: true, tooltip: 'Phone 3.', dataIndex: 'phone3', renderer: phoneRender}
			,{header: 'Phone 4', width: 100, sortable: true, tooltip: 'Phone 4.', dataIndex: 'fax', renderer: phoneRender}
			,{header: 'Phone 5', width: 100, sortable: true, tooltip: 'Phone 5', dataIndex: 'tollfree', renderer: phoneRender}
			,{header: 'Phone 6', width: 100, sortable: true, tooltip: 'Phone 6', dataIndex: 'phone6', renderer: phoneRender}
			,{header: 'Website 1', width: 100, sortable: true, tooltip: 'Url of page to send documents.', dataIndex: 'urlsend', renderer: webRender}	
			,{header: 'Website 2', width: 100, sortable: true, tooltip: 'Url of page to send documents.', dataIndex: 'urlsend2', renderer: webRender}
			,{header: 'Company', width: 100, sortable: true, tooltip: 'Company', dataIndex: 'company'}
			,{header: 'Address 1', width: 150, sortable: true, tooltip: 'Address 1', dataIndex: 'address1', renderer: addressRender} 
			,{header: 'Address 2', width: 150, sortable: true, tooltip: 'Address 2', dataIndex: 'address2', renderer: addressRender}			 
		],
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = e ? grid.getView().findCellIndex(e.getTarget()) : -1;
				if(cell != 0){
					smmyfollowagent.clearSelections();
					smmyfollowagent.selectRow(rowIndex,true,true);
					var but = Ext.getCmp('editContactbutton');
					but.btnEl.dom.click();
				}
			}
		},
				
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowagent',
            pageSize: limitmyfollowagent,
            store: storemyfollowagent,
            displayInfo: true,
			displayMsg: 'Total: {2} Contacts.',
			emptyMsg: "No Agents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 Contacts per page.',
				text: 50,
				handler: function(){
					limitmyfollowagent=50;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 Contacts per page.',
				text: 80,
				handler: function(){
					limitmyfollowagent=80;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 Contacts per page.',
				text: 100,
				handler: function(){
					limitmyfollowagent=100;
					Ext.getCmp('pagingmyfollowagent').pageSize = limitmyfollowagent;
					Ext.getCmp('pagingmyfollowagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        })
	});
	
	storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowagent_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowagent_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>