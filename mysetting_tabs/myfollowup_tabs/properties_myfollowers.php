<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<div align="left" id="todo_myfollower_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollower_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
    	<div align="left" style="font-size:14px; color: #1D6AAA; font-size: 14px; font-weight: bold; margin-left: 140px;" >New Follower (User id): <input type="number" name="new_follower_id" id="new_follower_id" /> <input type="button" value="Save" title="Save New Follower." name="save" onclick="javascript:guardarFollower(document.getElementById('new_follower_id').value);" style="color:#1D6AAA; background-color: #FFFFFF; font-size: 14px; font-weight: bold;" /></div>
        <br clear="all" />
  		<div id="myfollower_properties"></div> 
	</div>
</div>
<script>
	var storemyfollower = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_follower.php',
		fields: [ 'userid','follower_id','follower','status','accept',{name: 'typefollower', type: 'int'} ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {'userid': <?php echo $userid;?> },
		remoteSort: true
    });
	
	function guardarFollower(follower_id){
		loading_win.show();
		Ext.Ajax.request( 
		{  
			waitMsg: 'Saving...',
			url: 'mysetting_tabs/myfollowup_tabs/properties_follower.php', 
			method: 'POST',
			timeout :600000,
			params: { 
				type: 'insert',
				follower_id: follower_id,
				userid: <?php echo $userid;?>
			},
			
			failure:function(response,options){
				loading_win.hide();
				Ext.MessageBox.alert('Warning','ERROR');
			},
			success:function(response,options){
				loading_win.hide();
				var rest=Ext.decode(response.responseText);
				if(rest.exito==1){
					storemyfollower.load();
				}else{
					Ext.MessageBox.alert('Followers',rest.msg);
				}
			}                                
		});
	}
	function guardarTypeFollower(follower_id){
		loading_win.show();
		Ext.Ajax.request( 
		{  
			waitMsg: 'Saving...',
			url: 'mysetting_tabs/myfollowup_tabs/properties_follower.php', 
			method: 'POST',
			timeout :600000,
			params: { 
				type: 'updatetype',
				follower_id: follower_id,
				userid: <?php echo $userid;?>
			},
			
			failure:function(response,options){
				loading_win.hide();
				Ext.MessageBox.alert('Warning','ERROR');
			},
			success:function(response,options){
				loading_win.hide();
				storemyfollower.load();
			}                                
		});
	}
	function botones(value, metaData, record, rowIndex, colIndex, store) {
		var follower = record.get('follower_id');
		if(value=='Active') return '<img src="../../img/play.gif" onclick="javascript:guardarFollower('+follower+');"/>';
		else return '<img src="../../img/drop-no.gif" onclick="javascript:guardarFollower('+follower+');"/>';
	}
	
	function accept(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'A': return 'Accept'; break;
			case 'D': return 'Denied'; break;
			case 'P': return 'Pending'; break;
		}		
	}
	function typeFollower(value, metaData, record, rowIndex, colIndex, store) {
		var follower = record.get('follower_id');
		if(value==0) return '<img src="../../img/toolbar/search.png" style="width:20px; height: 20px;" onclick="javascript:guardarTypeFollower('+follower+');"/>';
		else return '<img src="../../img/toolbar/update.png" style="width:20px; height: 20px;" onclick="javascript:guardarTypeFollower('+follower+');"/>';		
	}	
	var gridmyfollower = new Ext.grid.GridPanel({
		renderTo: 'myfollower_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 500,
		store: storemyfollower,
		stripeRows: true,
		columns: [
			<?php 
				echo "{header: 'Follower', width: 400, sortable: true, tooltip: 'Follower Name.', dataIndex: 'follower'}"; 
				echo ",{header: 'Status', width: 150, sortable: true, tooltip: 'Status.', dataIndex: 'status'}";
				echo ",{header: 'Start / Stop', width: 100, tooltip: 'Start or Stop follower.', dataIndex: 'status', renderer: botones}";
				echo ",{header: 'Type', width: 100, tooltip: 'Type follower.', dataIndex: 'typefollower', renderer: typeFollower}";
				echo ",{header: 'Accept Status', width: 100, tooltip: 'Status of followers response.', dataIndex: 'accept', renderer: accept}";
		   ?>
		]
	});
	
	storemyfollower.load();

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollower_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollower_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>