<?php  
	include('../../properties_conexion.php');
	conectar();
	$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	if(isset($_POST['type'])){
		if($_POST['type']=='block'){
			$query='UPDATE xima.followagent SET agentblock=1
					WHERE userid='.$userid.' AND agentid IN ('.$_POST['agentids'].')';
			mysql_query($query) or die($query.mysql_error());
			$blocks=0;
			if($_POST['blockproperties']=='yes'){
				//Selecciona properties del agente para bloquearlas
				$query='SELECT a.parcelid,a.principal,ag.*,t.name as agentype
						FROM `xima`.`follow_assignment` a
						INNER JOIN xima.followagent ag ON (a.agentid=ag.agentid)
						inner join followagent_type t on ag.agenttype=t.idtype
						inner join followup f on a.parcelid=f.parcelid and ag.agent=f.agent and ag.userid=f.userid
						WHERE ag.userid='.$userid.' AND ag.agentid IN ('.$_POST['agentids'].')
						AND f.type NOT IN ("LB","LF","LBM","LFM","LS")';
				//echo $query;
				$result=mysql_query($query) or die($query.mysql_error());
				
				while($r=mysql_fetch_array($result)){
					$update='UPDATE xima.followup SET type=concat("L",type) 
							WHERE userid='.$userid.' AND parcelid="'.$r['parcelid'].'"';
					mysql_query($update) or die($update.mysql_error());
					$blocks++;
				}
			}
			echo '{success: true, properties: '.$blocks.'}';		
		}elseif($_POST['type']=='unblock'){
			$query='UPDATE xima.followagent SET agentblock=0
					WHERE userid='.$userid.' AND agentid IN ('.$_POST['agentids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			if($_POST['blockproperties']=='yes'){
				//Selecciona properties del agente para desbloquearlas
				$query='SELECT a.parcelid,a.principal,ag.*,t.name as agentype
						FROM `xima`.`follow_assignment` a
						INNER JOIN xima.followagent ag ON (a.agentid=ag.agentid)
						inner join followagent_type t on ag.agenttype=t.idtype
						inner join followup f on a.parcelid=f.parcelid and ag.agent=f.agent and ag.userid=f.userid
						WHERE ag.userid='.$userid.' AND ag.agentid IN ('.$_POST['agentids'].')';
				$result=mysql_query($query) or die($query.mysql_error());
				
				while($r=mysql_fetch_array($result)){
					$update='UPDATE xima.followup SET type=replace(type,"L","") 
							WHERE userid='.$userid.' AND parcelid="'.$r['parcelid'].'"';
					mysql_query($update) or die($update.mysql_error());
				}
			}
			echo '{success: true}';		
		}elseif($_POST['type']=='delete'){
			$query='DELETE FROM xima.followagent 
			WHERE userid='.$userid.' AND agentid IN ('.$_POST['agentids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
	}else{
		$userid	= $_POST['userid'];
		
		$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
		$agent 	= isset($_POST['agent']) ? $_POST['agent'] 	: '';
		$email 	= isset($_POST['email']) ? $_POST['email'] : '';
		$phone 	= isset($_POST['phone']) ? $_POST['phone'] : '';
		$web 	= isset($_POST['web']) ? $_POST['web'] : '';
		$company 	= isset($_POST['company']) ? $_POST['company'] : '';
		$typeagent 	= isset($_POST['typeagent']) ? $_POST['typeagent'] : 'ALL';						  
		$query	= "SELECT f.*, t.name as agentype FROM followagent f 
					inner join followagent_type t on f.agenttype=t.idtype 
					where f.userid=".$userid." and f.agentblock=1";;
		
		//filters
		if(isset($_POST['query']) && strlen($_POST['query'])>0){ 
			$query.=" AND agent LIKE '".$_POST['query']."%' ";
			
		}
		if(isset($_POST['agentid'])){
			if(is_numeric($_POST['agentid']))
				$query.=' AND agentid = '.$_POST['agentid'].'';
			else
				$query.=' AND agentid = -1';
		}
		if(trim($address)!=''){
			$query.=' AND (address1 LIKE \'%'.trim($address).'%\'';
			$query.=' OR address2 LIKE \'%'.trim($address).'%\')';
		}
		if(trim($agent)!='')
			$query.=' AND agent LIKE \'%'.trim($agent).'%\'';

		if($email!=''){
			$query.=' AND (email LIKE \'%'.trim($email).'%\'';
			$query.=' OR email2 LIKE \'%'.trim($email).'%\')';
		}
			
		if($phone!=''){
			$query.=' AND (phone1 LIKE \'%'.trim($phone).'%\'';
			$query.=' OR phone2 LIKE \'%'.trim($phone).'%\'';
			$query.=' OR phone3 LIKE \'%'.trim($phone).'%\'';
			$query.=' OR fax LIKE \'%'.trim($phone).'%\'';
			$query.=' OR tollfree LIKE \'%'.trim($phone).'%\'';
			$query.=' OR phone6 LIKE \'%'.trim($phone).'%\')';
		}
		if($web!=''){
			$query.=' AND (urlsend LIKE \'%'.trim($web).'%\'';
			$query.=' OR urlsend2 LIKE \'%'.trim($web).'%\')';
		}
		if(trim($company)!='')
			$query.=' AND company LIKE \'%'.trim($company).'%\'';
		
		if($typeagent!='ALL')
			$query.=' AND f.agenttype=\''.$typeagent.'\'';
		//orders
		if(isset($_POST['sort'])) $query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		else $query.=' ORDER BY agent';
		
		//echo $query;
		$result=mysql_query($query) or die($query.mysql_error());
		$i=1;
		$vFilas = array();
		while($r=mysql_fetch_object($result))
			$vFilas[]=$r;
							 
		
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}	
?>