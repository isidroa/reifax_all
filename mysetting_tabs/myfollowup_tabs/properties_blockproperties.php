<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowblock_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowblock_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowblock_filters"></div><br />
        <div id="myfollowblock_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	Right Click -> Overview / Contract 
            <span style="margin-left:15px; margin-right:15px;">
                  -     
            </span>
            Double Click -> Follow History
       	</div>
	</div>
</div>
<script>

	var limitmyfollowblock 			= 50;
	var selected_datamyfollowblock 	= new Array();
	var selected_datamyfollowblockid 	= new Array();
	var AllCheckmyfollowblock 			= false;
	
	//filter variables
	var filteraddressmyfollowblock 	= '';
	var filtermlnumbermyfollowall 	= '';
	var filterstatusmyfollowall 	= 'ALL';
	var filtercampaignmyfollowmail 	= 'ALL';
	var filterfield					= 'address';
	var filterdirection				= 'ASC';

	var storemyfollowblock = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				storemyfollowblockall.load();
				AllCheckmyfollowblock=false;
				selected_datamyfollowblock=new Array(); 
				smmyfollowblock.deselectRange(0,limitmyfollowblock);
				obj.params.address=filteraddressmyfollowblock;
				obj.params.mlnumber=filtermlnumbermyfollowall;
				obj.params.status=filterstatusmyfollowall;
				obj.params.campaign=filtercampaignmyfollowmail;
			},
			'load' : function (store,data,obj){ 
				if (AllCheckmyfollowblock){
					Ext.get(gridmyfollowblock.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowblock=true;
					gridmyfollowblock.getSelectionModel().selectAll();
					selected_datamyfollowblock=new Array(); 
				}else{
					AllCheckmyfollowblock=false;
					Ext.get(gridmyfollowblock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowblock.length > 0){
						for(val in selected_datamyfollowblock){
							var ind = gridmyfollowblock.getStore().find('pid',selected_datamyfollowblock[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						
						if (sel.length > 0)
							gridmyfollowblock.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemyfollowblockall = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowblock;
				obj.params.mlnumber=filtermlnumbermyfollowall;
				obj.params.status=filterstatusmyfollowall;
				obj.params.campaign=filtercampaignmyfollowmail;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive Call." src="../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../img/notes/reci_other.png" />'; break;
		}
	}
	
	function followupRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'FM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			default: return ''; break;
		}
	}

	var smmyfollowblock = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowblock.indexOf(record.get('pid'))==-1)
					selected_datamyfollowblock.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowblock.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on')){
					AllCheckmyfollowblock=true;  
				}
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowblock = selected_datamyfollowblock.remove(record.get('pid'));
				AllCheckmyfollowblock=false;
				Ext.get(gridmyfollowblock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'myfollowblock_filters',
		items: [
			new Ext.Button({
				tooltip: 'Click to delete',
				iconCls:'icon',
				iconAlign: 'top',
				scale: 'medium',
				width: 40,
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be deleted.'); return false;
					}
					if(AllCheckmyfollowblock==true){
						var totales = storemyfollowblockall.getRange(0,storemyfollowblockall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowblockall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowblock[0]+'\'';
						for(i=1; i<selected_datamyfollowblock.length; i++){
							pids+=',\''+selected_datamyfollowblock[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
							Ext.Msg.alert("Block Properties", 'Properties deleted.');
							
						}                                
					});
				}
			}),new Ext.Button({
				 tooltip: 'Click to Print Report',
				 iconCls:'icon',
				 iconAlign: 'top',
				 width: 40,
				 icon: 'http://www.reifax.com/img/toolbar/printer.png',
				 scale: 'medium',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowblock==true){
						var totales = storemyfollowblockall.getRange(0,storemyfollowblockall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowblockall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowblock[0];
						for(i=1; i<selected_datamyfollowblock.length; i++){
							pids+=','+selected_datamyfollowblock[i];
						}
					}
					
					Ext.Ajax.request( 
						{  
							waitMsg: 'Printing Report...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_pdf.php', 
							method: 'POST', 
							timeout :600000,
							params: {
								userweb: 'false',
								parcelids_res: pids,
								template_res: 'Default',
								printType: 1
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','file can not be generated');
								loading_win.hide();
							},
							success:function(response,options){
								var rest = Ext.util.JSON.decode(response.responseText);
								//alert(rest.pdf);
								var url='http://www.reifax.com/'+rest.pdf;
								//alert(url);
								loading_win.hide();
								window.open(url);
								
							}                                
						 }
					);
				 }
			}),new Ext.Button({
				 tooltip: 'Click to Excel Report',
				 iconCls:'icon',
				 iconAlign: 'top',
				 width: 40,
				 icon: 'http://www.reifax.com/img/toolbar/excel.png',
				 scale: 'medium',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					
					
					if(AllCheckmyfollowblock==true){
						var totales = storemyfollowblockall.getRange(0,storemyfollowblockall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowblockall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowblock[0];
						for(i=1; i<selected_datamyfollowblock.length; i++){
							pids+=','+selected_datamyfollowblock[i];
						}
					}
					
					var ownerShow='false';
					Ext.Msg.show({
						title:'Excel Report',
						msg: 'Would you like to save Excel Report with Owner Data?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn, text){
							if (btn == 'yes'){
								ownerShow='true';
							}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',

								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_excel.php', 
								timeout: 106000,
								method: 'POST', 
								params: {
									userweb:'false',
									parcelids_res:pids,
									ownerShow: ownerShow,
									template_res: 'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
						}
					});
				 }
			}),new Ext.Button({
				tooltip: 'Click to filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				scale: 'medium',
				handler: function(){
					filteraddressmyfollowblock = '';
					filtermlnumbermyfollowall = '';
					filterstatusmyfollowall = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					var formmyfollowall = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'myfollowblock_filters',
						id: 'formmyfollowall',
						name: 'formmyfollowall',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'fbaddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'fbaddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowblock=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fbmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fbmlnumber',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtermlnumbermyfollowall=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fbstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fbstatusname',
									value         : 'ALL',
									hiddenName    : 'fbstatus',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterstatusmyfollowall = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
									//storemyfollowblockall.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteraddressmyfollowblock = '';
									filtermlnumbermyfollowall = '';
									filterstatusmyfollowall = 'ALL';
									filtercampaignmyfollowmail 	= 'ALL';
									
									Ext.getCmp('formmyfollowall').getForm().reset();
									
									storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
									//storemyfollowblockall.load();
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 250,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowall,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Click to remove filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				scale: 'medium',
				handler: function(){
					filteraddressmyfollowblock = '';
					filtermlnumbermyfollowall = '';
					filterstatusmyfollowall = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					
					storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
				}
			}),new Ext.Button({
				tooltip: 'Click to Unblock',
				iconCls:'icon',
				iconAlign: 'top',
				scale: 'medium',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/unblock.png',
				handler: function(){
					if(selected_datamyfollowblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be unblocked.'); return false;
					}
					if(AllCheckmyfollowblock==true){
						var totales = storemyfollowblockall.getRange(0,storemyfollowblockall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowblockall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowblock[0]+'\'';
						for(i=1; i<selected_datamyfollowblock.length; i++){
							pids+=',\''+selected_datamyfollowblock[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'unblock',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
							Ext.Msg.alert("Block properties", 'Properties unblocked.');
							
						}                                
					});
				}
			})		
		]
	});
	
	var gridmyfollowblock = new Ext.grid.GridPanel({
		renderTo: 'myfollowblock_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowblock,
		stripeRows: true,
		sm: smmyfollowblock, 
		columns: [
			smmyfollowblock,
			{header: '', hidden: true, editable: false, dataIndex: 'pid'}
			<?php 
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					elseif($val->name!='agent')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
		   ?>
		   ,{header: "City", dataIndex: 'city', tooltip: 'City.'}		
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowblock',
            pageSize: limitmyfollowblock,
            store: storemyfollowblock,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Block.',
			emptyMsg: "No follow Block to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows block per page.',
				text: 50,
				handler: function(){
					limitmyfollowblock=50;
					Ext.getCmp('pagingmyfollowblock').pageSize = limitmyfollowblock;
					Ext.getCmp('pagingmyfollowblock').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows block per page.',
				text: 80,
				handler: function(){
					limitmyfollowblock=80;
					Ext.getCmp('pagingmyfollowblock').pageSize = limitmyfollowblock;
					Ext.getCmp('pagingmyfollowblock').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows block per page.',
				text: 100,
				handler: function(){
					limitmyfollowblock=100;
					Ext.getCmp('pagingmyfollowblock').pageSize = limitmyfollowblock;
					Ext.getCmp('pagingmyfollowblock').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				
			}
		}
	});
	
	storemyfollowblock.load({params:{start:0, limit:limitmyfollowblock}});
	//storemyfollowblockall.load();
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowblock_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowblock_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>