<?php
	define('FPDF_FONTPATH','../../../FPDF/font/');	
	$_SERVERXIMA="http://www.reifax.com/";
	include('../../../FPDF/mysql_table.php');
	include("../../../FPDF/limpiar.php");
	include("../../../properties_conexion.php");
	conectar();
	
	limpiardirpdf2('../../../FPDF/generated/');	//Se borran los pdf viejos
	
	$printType = $_POST['printType'];
	$pids=$_POST['parcelids_res'];
	
	class PDF extends PDF_MySQL_Table
	{
		function Header()
		{
			global $pid,$printType,$county; 
			//Title
			$this->SetFont('Arial','',10);
			$this->Cell(0,6,'Properties REI FAX.',0,1,'L');
			switch($printType){
				case 0: $this->Cell(0,6,'My Follow Up - Mailing Campaign',0,1,'C'); break;
			}
			
			$this->Ln(2);
			//Ensure table header is output
			parent::Header();
		}
	}
	
	$pdf=new PDF('L');		//Landscape -- hoja horizontal
	$pdf->Open();
	$pdf->AddPage();
	
	include("../../../properties_getgridcamptit.php");
	
	$id = getArray('MailingCampaing','result');
			
	$ArTab=getCamptitTipo($id, "Tabla",'defa');	
	$ArCamp=getCamptitTipo($id, "Campos",'defa');
	$ArTit=getCamptitTipo($id, "Titulos",'defa');
	$ArSize=getCamptitTipo($id, "r_size",'defa');
	
	$pdf->AddCol('sendmail',5,'TS','C');
	$pdf->AddCol('campaign',30,'Campaign','C');
	$pdf->AddCol('lasthistorydate',30,'Last mail date','C');
	
	for($i=0; $i< count($ArCamp); $i++){
		$pdf->AddCol($ArCamp[$i],$ArSize[$i],$ArTit[$i],'C');
	}
	
	$query="SELECT distinct bd 
	FROM xima.followup 
	WHERE parcelid IN ('".str_replace(',',"','",$pids)."')";
	$result = mysql_query($query) or die($query.mysql_error());
	
	$datos=array();
	$datos_id=array();
	while($r=mysql_fetch_assoc($result)){
		conectarPorNameCounty($r['bd']);
		
		$xSql="SELECT f.parcelid as pid, f.campaign, f.userid, f.mlnumber,
		f.bd as county, f.address, f.unit, f.city, f.zip, f.sendmail, 
		f.lasthistorydate, f.type, p.beds, p.bath, p.lsqft, p.bheated as larea, 
		f.pendes, p.pool, p.waterf, p.yrbuilt 
		FROM psummary p 
		INNER JOIN xima.followup f ON (p.parcelid=f.parcelid) 
		WHERE f.userid =".$_COOKIE['datos_usr']['USERID']." and f.type in ('M','FM','BM') 
		and p.parcelid IN ('".str_replace(',',"','",$pids)."')
		ORDER BY f.campaign, f.address ASC"; 
		$result2 = mysql_query($xSql) or die($xSql.mysql_error());
		
		while($r2=mysql_fetch_assoc($result2)){
			$datos[]=$r2;
			$datos_id[]=$r2['pid'];
		}
	}

	
	$prop=array('HeaderColor'=>array(135,206,250),
            'color1'=>array(255,255,255),
            'color2'=>array(255,255,255),
            'padding'=>2,
			'align'=>'C');
		
	//Handle properties
	if(!isset($prop['width']))
		$prop['width']=0;
	if($prop['width']==0)
		$prop['width']=$pdf->w-$pdf->lMargin-$pdf->rMargin;
	if(!isset($prop['align']))
		$prop['align']='C';
	if(!isset($prop['padding']))
		$prop['padding']=$pdf->cMargin;
	$cMargin=$pdf->cMargin;
	$pdf->cMargin=$prop['padding'];
	if(!isset($prop['HeaderColor']))
		$prop['HeaderColor']=array();
	$pdf->HeaderColor=$prop['HeaderColor'];
	if(!isset($prop['color1']))
		$prop['color1']=array();
	if(!isset($prop['color2']))
		$prop['color2']=array();
	$pdf->RowColors=array($prop['color1'],$prop['color2']);
	//Compute column widths
	$pdf->CalcWidths($prop['width'],$prop['align']);
	//Print header
	$pdf->TableHeader();
	//Print rows
	$pdf->SetFont('Arial','',8);
	$pdf->ColorIndex=0;
	$pdf->ProcessingTable=true;

	foreach($datos as $k => $val){
		$pdf->Row($datos[$k]);
	}
	
	$pdf->ProcessingTable=false;
	$pdf->cMargin=$cMargin;
	$pdf->aCols=array();		

	$file='FPDF/generated/'.strtotime("now").'.pdf';
	$pdf->Output('C:/inetpub/wwwroot/'.$file, 'F');
	echo "{success:true, pdf:'$file'}";
?>