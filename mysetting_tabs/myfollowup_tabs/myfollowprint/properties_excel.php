<?php  
	define('FPDF_FONTPATH','../../../FPDF/font/');
	set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Excel/Classes/');
	include 'PHPExcel.php';
	include 'PHPExcel/Writer/Excel5.php';
	include("../../../properties_conexion.php");
	conectar();

	$excel = new PHPExcel();

	$excel->getProperties()->setCreator("REI Property Fax");
	$excel->getProperties()->setTitle("REI Property Fax Reports");
	$excel->getProperties()->setSubject("REI Property Fax Reports");
	$excel->setActiveSheetIndex(0);
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
	'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
	'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
	'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
	'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ');
	
	$rest_total=array();
	$take_comp=NULL;
	$printType = $_POST['printType'];
	$pid = $_POST['parcelid'];
	$county = $_POST['county'];
	$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
	if(isset($_POST['pendingtask']) && $_POST['pendingtask']=='yes') $printType = 9;
	//printType:
	//0-> Following
	//1-> Agents
	//2-> Follow List
	//3-> Details
	//4-> History
	//5-> Schedule
	//6-> Messages
	
	if($printType>=4 && $printType<=6){
		conectarPorNameCounty($county);
		$query="SELECT p.address,p.unit,p.county,p.city,p.zip FROM properties_php p WHERE parcelid='".$pid."'";
		$result = mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);

		$dir = $r['address'];
		if(strlen(trim($r['unit']))>0) $dir.=' '.$r['unit'];
		$dir.=', '.$r['county'];
		$dir.=', '.$r['city'];
		$dir.=', FLORIDA';
		$dir.=', '.$r['zip']; 
		conectar();
	}
	
	switch($printType){
		case 0: 
			include("../../../properties_getgridcamptit.php");
			$id = getArray('MYFollowExcel','result');
			$myArrF=getCamptitTipo($id, "Campos",'defa');
			$myArrTit=getCamptitTipo($id, "Titulos",'defa');
			array_push($myArrF, 'followdate', 'lprice', 'offerpercent', 'offer', 'coffer', 'ndate', 'ntask', 'lasthistorydate', 'contract', 'pof', 'emd', 'rademdums');
			array_push($myArrTit, 'Date', 'L. Price','Offer %', 'Offer', 'C. Offer', 'Next Date', 'Next Task', 'LU', 'C', 'P', 'E', 'A');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			} 
			
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
			$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
			$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
			$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
			$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
			$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
			$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
			$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
			$pt 		= isset($_POST['pt']) 		? $_POST['pt'] 			: '-1';
			$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
			if($typeFollow=='B'){
				$followType="('B','F','BM','FM')";
			}else{
				$followType="('S')";
			}
			$pendingtask = isset($_POST['pendingtask']) ? $_POST['pendingtask'] : '';
			if($pendingtask=='yes'){
				$xSql='SELECT f.parcelid as pid, f.userid, f.mlnumber, IF(f.status="A","Active",IF(f.status="NA","Non-Active",IF(f.status="NF","Not For Sale","Sold"))) as status, f.bd as county,
						f.address, f.city, f.lprice, f.marketvalue, f.activevalue, f.dom, f.unit, f.zip, f.agent,
						f.offer, f.coffer, IF(f.contract=1,"N","Y") as contract, IF(f.pof=1,"N","Y") as pof, IF(f.emd=1,"N","Y") as emd, IF(f.realtorsadem=1,"N","Y") as rademdums, f.msj,
						DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ROUND(((f.offer/f.lprice)*100),2) as offerpercent,
						f.statusalt, f.offerreceived, f.followdate, concat(x.name," ",x.surname) as name_follow ,
						f.userid_follow, f.pendes, s.odate as ndate, IF(s.task=0,"",IF(s.task=1,"Send SMS",IF(s.task=2,"Receive SMS",IF(s.task=3,"Send Fax",IF(s.task=4,"Receive Fax",IF(s.task=5,"Send Email",IF(s.task=6,"Receive Email",IF(s.task=7,"Send Document",IF(s.task=8,"Receive Document",IF(s.task=9,"Make Call",IF(s.task=10,"Receive Call",IF(s.task=11,"Send Regular Mail",IF(s.task=12,"Receive Regular Mail",IF(s.task=13,"Send Other","Receive Other")))))))))))))) as ntask, s.idfus,followagent.phone1 as phone1
						FROM xima.followup f
						inner join followup_schedule s on f.parcelid=s.parcelid and f.userid=s.userid
						left join xima.ximausrs x on f.userid_follow=x.userid
							LEFT JOIN follow_assignment fa ON f.parcelid = fa.parcelid AND f.userid = fa.userid
					LEFT JOIN followagent ON fa.agentid = followagent.agentid
				WHERE f.userid ='.$userid.'  AND f.type IN '.$followType.' AND f.parcelid IN ('.$_POST['pids'].')';
			}else if($pendingtask=='no'){
				$xSql='SELECT f.parcelid as pid, f.userid, f.mlnumber, IF(f.status="A","Active",IF(f.status="NA","Non-Active",IF(f.status="NF","Not For Sale","Sold"))) as status, f.bd as county,
						f.address, f.city, f.lprice, f.marketvalue, f.activevalue, f.dom, f.unit, f.zip, f.agent,
						f.offer, f.coffer, IF(f.contract=1,"N","Y") as contract, IF(f.pof=1,"N","Y") as pof, IF(f.emd=1,"N","Y") as emd, IF(f.realtorsadem=1,"N","Y") as rademdums, f.msj,
						DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ROUND(((f.offer/f.lprice)*100),2) as offerpercent,
						f.statusalt, f.offerreceived, f.followdate, concat(x.name," ",x.surname) as name_follow ,
						f.userid_follow, f.pendes, date(s.odate) as ndate, IF(s.task=0,"",IF(s.task=1,"Send SMS",IF(s.task=2,"Receive SMS",IF(s.task=3,"Send Fax",IF(s.task=4,"Receive Fax",IF(s.task=5,"Send Email",IF(s.task=6,"Receive Email",IF(s.task=7,"Send Document",IF(s.task=8,"Receive Document",IF(s.task=9,"Make Call",IF(s.task=10,"Receive Call",IF(s.task=11,"Send Regular Mail",IF(s.task=12,"Receive Regular Mail",IF(s.task=13,"Send Other","Receive Other")))))))))))))) as ntask, s.idfuh
						FROM xima.followup f
						inner join followup_history s on f.parcelid=s.parcelid and f.userid=s.userid
						left join xima.ximausrs x on f.userid_follow=x.userid
				WHERE f.userid ='.$userid.'  AND f.type IN '.$followType.' AND f.parcelid IN ('.$_POST['pids'].')';
			}else if($pendingtask==''){
				$xSql='SELECT IF(status="A","Active",IF(status="NA","Non-Active",IF(status="NF","Not For Sale","Sold"))) as status, lprice, followdate, mlnumber, address, unit, city, zip, agent, ROUND(((offer/lprice)*100),2) as offerpercent, offer, coffer,
				ndate, IF(ntask=0,"",IF(ntask=1,"Send SMS",IF(ntask=2,"Receive SMS",IF(ntask=3,"Send Fax",IF(ntask=4,"Receive Fax",IF(ntask=5,"Send Email",IF(ntask=6,"Receive Email",IF(ntask=7,"Send Document",IF(ntask=8,"Receive Document",IF(ntask=9,"Make Call",IF(ntask=10,"Receive Call",IF(ntask=11,"Send Regular Mail",IF(ntask=12,"Receive Regular Mail",IF(ntask=13,"Send Other","Receive Other")))))))))))))) as ntask, DATEDIFF(NOW(),lasthistorydate) as lasthistorydate, IF(contract=1,"N","Y") as contract, IF(pof=1,"N","Y") as pof, IF(emd=1,"N","Y") as emd, IF(realtorsadem=1,"N","Y") as rademdums, parcelid
				FROM xima.followup f 
				WHERE userid ='.$userid.'  AND type IN '.$followType.' AND parcelid IN ('.$_POST['pids'].')';
			}	
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 1:
			$myArrF=array('agent','email','tollfree','phone1','phone2','phone3','fax','urlsend');
			$myArrTit=array('Agent','Email','Toll Free','Phone 1','Phone 2','Phone 3','Fax','Send Page');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			} 
		
			$xSql='SELECT agentid, agent, email, tollfree, phone1, typeph1, phone2, typeph2, phone3, typeph3, fax, urlsend 
			FROM xima.followagent 
			WHERE userid ='.$_COOKIE['datos_usr']['USERID'].' AND agentid IN ('.$_POST['agentids'].')'; 
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir']; 
			
			$xSql.=' limit 1000';
		break;
		
		case 2: 
			$myArrF=array('name','total','status1','status2','status3','status4','msj','history');
			$myArrTit=array('Following','Total','Active','Non-Active','Not For Sale','Sold','Messages','History');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			}
			
			$fname 		= isset($_POST['fname']) 	? $_POST['fname'] 		: '';
			$fuserid 	= isset($_POST['fuserid']) 	? $_POST['fuserid']		: '-1';
			
			$xSql='SELECT f.userid, concat(x.name," ",x.surname) name,
			f.total, f.msj, f.history, f.status1, f.status2, f.status3, f.status4 
			FROM xima.follower f
			INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
			WHERE f.follower_id='.$_COOKIE['datos_usr']['USERID'].' AND f.status="Active" AND x.idstatus IN (1,3,4)';
			
			//filters
			if(trim($fname)!='')
				$xSql.=' AND concat(x.name," ",x.surname) LIKE \'%'.trim($fname).'%\'';
				
			if($fuserid!='-1')
				$xSql.=' AND f.userid='.$fuserid;
			
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 3: 
			include("../../../properties_getgridcamptit.php");
			$id = getArray('MYFollow','result');
			$myArrF=getCamptitTipo($id, "Campos",'defa');
			$myArrTit=getCamptitTipo($id, "Titulos",'defa');
			array_push($myArrF, 'offerpercent', 'offer', 'coffer', 'ndate', 'ntask', 'lasthistorydate', 'contract', 'pof', 'emd', 'rademdums');
			array_push($myArrTit, 'Offer %', 'Offer', 'C. Offer', 'Next Date', 'Next Task', 'LU', 'C', 'P', 'E', 'A');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			}
			
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
			$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
			$ndateb 	= isset($_POST['ndateb']) 	? $_POST['ndateb'] 		: '';
			$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
			$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
			$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
			$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
			$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
			$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
			$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
			
			$xSql='SELECT IF(f.status="A","Active",IF(f.status="NA","Non-Active",IF(f.status="NF","Not For Sale","Sold"))) as status,f.mlnumber, f.address, f.unit, f.zip, f.agent,
			f.offer, f.coffer, f.ndate, IF(f.ntask=0,"",IF(f.ntask=1,"Send SMS",IF(f.ntask=2,"Receive SMS",IF(f.ntask=3,"Send Fax",IF(f.ntask=4,"Receive Fax",IF(f.ntask=5,"Send Email",IF(f.ntask=6,"Receive Email",IF(f.ntask=7,"Send Document",IF(f.ntask=8,"Receive Document",IF(f.ntask=9,"Make Call",IF(f.ntask=10,"Receive Call",IF(f.ntask=11,"Send Regular Mail",IF(f.ntask=12,"Receive Regular Mail",IF(f.ntask=13,"Send Other","Receive Other")))))))))))))) as ntask, IF(f.contract=1,"N","Y") as contract, IF(f.pof=1,"N","Y") as pof, IF(f.emd=1,"N","Y") as emd, IF(f.realtorsadem=1,"N","Y") as rademdums, n.history, n.msj,
			DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ROUND(((f.offer/f.lprice)*100),2) as offerpercent 
			FROM xima.followup f
			LEFT JOIN xima.follow_notification n ON (f.parcelid=n.parcelid AND f.userid=n.userid)
			WHERE f.userid ='.$_POST['userid'].' AND n.followid='.$_COOKIE['datos_usr']['USERID']; 
			
			//filters
			if(trim($address)!='')
				$xSql.=' AND f.address LIKE \'%'.trim($address).'%\'';
				
			if(trim($mlnumber)!='')
				$xSql.=' AND f.mlnumber=\''.trim($mlnumber).'\'';
				
			if(trim($agent)!='')
				$xSql.=' AND f.agent LIKE \'%'.trim($agent).'%\'';
			
			if($status!='ALL')
				$xSql.=' AND f.status=\''.$status.'\'';
				
			if(trim($ndateb)!='' && trim($ndate)!='')
				$xSql.=' AND f.ndate BETWEEM \''.$ndate.'\' AND \''.$ndateb.'\'';
			elseif(trim($ndate)!='')
				$xSql.=' AND f.ndate=\''.$ndate.'\'';
				
			if(trim($ntask)!='-2'){
				if(trim($ntask)!='-1')
					$xSql.=' AND ntask='.$ntask;
				else
					$xSql.=' AND ntask<>0';
			}
				
			if($contract!='-1')
				$xSql.=' AND f.contract='.$contract;
			
			if($ademdums!='-1')
				$xSql.=' AND f.realtorsadem='.$ademdums;
			
			if($pof!='-1')
				$xSql.=' AND f.pof='.$pof;
			
			if($emd!='-1')
				$xSql.=' AND f.emd='.$emd;
				
			if($msj!='-1')
				$xSql.=' AND n.msj='.$msj;
				
			if($history!='-1')
				$xSql.=' AND n.history='.$history;
			
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 4: 
			$myArrF=array('odate','offer','coffer','task','contract','pof','emd','realtorsadem','detail');
			$myArrTit=array('Date','Offer','C. Offer','Task','C','P','E','A','Detail');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			} 
			
			$excel->getActiveSheet()->mergeCells('B1:'.$abc[count($myArrTit)].'1');
			$excel->getActiveSheet()->setCellValue('B1',$dir);
			
			$xSql='SELECT odate,offer,coffer, IF(task=0,"",IF(task=1,"Send SMS",IF(task=2,"Receive SMS",IF(task=3,"Send Fax",IF(task=4,"Receive Fax",IF(task=5,"Send Email",IF(task=6,"Receive Email",IF(task=7,"Send Document",IF(task=8,"Receive Document",IF(task=9,"Make Call",IF(task=10,"Receive Call",IF(task=11,"Send Regular Mail",IF(task=12,"Receive Regular Mail",IF(task=13,"Send Other","Receive Other")))))))))))))) as task, IF(contract=1,"N","Y") as contract, IF(pof=1,"N","Y") as pof, IF(emd=1,"N","Y") as emd, IF(realtorsadem=1,"N","Y") as realtorsadem, detail FROM xima.followup_history WHERE parcelid="'.$_POST['parcelid'].'" AND userid='.$_POST['userid'];
		
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		case 5: 
			$myArrF=array('odate','task','detail');
			$myArrTit=array('Date','Task','Detail');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			} 
			
			$excel->getActiveSheet()->mergeCells('B1:'.$abc[count($myArrTit)].'1');
			$excel->getActiveSheet()->setCellValue('B1',$dir);
			
			$xSql='SELECT odate, detail, IF(task=0,"",IF(task=1,"Send SMS",IF(task=2,"Receive SMS",IF(task=3,"Send Fax",IF(task=4,"Receive Fax",IF(task=5,"Send Email",IF(task=6,"Receive Email",IF(task=7,"Send Document",IF(task=8,"Receive Document",IF(task=9,"Make Call",IF(task=10,"Receive Call",IF(task=11,"Send Regular Mail",IF(task=12,"Receive Regular Mail",IF(task=13,"Send Other","Receive Other")))))))))))))) as task FROM xima.followup_schedule WHERE parcelid="'.$_POST['parcelid'].'" AND userid='.$_POST['userid'].' ORDER BY odate'; 
		break;
		case 6: 
			$myArrF=array('moment','name','message');
			$myArrTit=array('Date','Name','Message');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			}
			
			$excel->getActiveSheet()->mergeCells('B1:'.$abc[count($myArrTit)].'1');
			$excel->getActiveSheet()->setCellValue('B1',$dir);
			
			$xSql="SELECT m.moment,concat(x.name,' ',x.surname) as name, m.message 
			FROM xima.follow_messages m
			INNER JOIN xima.ximausrs x ON (m.msgid=x.userid)
			WHERE m.userid =".$_POST['userid']." AND m.parcelid='".$_POST['parcelid']."'"; 
		break;
		
		//Mailing Campaign
		case 7:
			$ownerShow=false;
			if(isset($_POST['ownerShow']) && $_POST['ownerShow']=="true") $ownerShow=true;
			
			include("../../../properties_getgridcamptit.php");
			
			$myArrF=$myArrTit=array();
			$id = getArray('MailingCampaing','result');
			array_push($myArrF, 'sendmail','campaign', 'lasthistorydate');
			array_push($myArrTit, 'TS', 'Campaign', 'Last mail date');
			
			if($ownerShow){
				array_push($id,592);
				array_push($id,591);
				array_push($id,25);
				array_push($id,24);
				array_push($id,23);
				array_push($id,22);
				array_push($id,21);
				array_push($id,20);
			}
			
			$myArrF = array_merge($myArrF, getCamptitTipo($id, "Campos",'defa'));
			//$myArrF=getCamptitTipo($id, "Campos",'defa');
			$myArrTit = array_merge($myArrTit, getCamptitTipo($id, "Titulos",'defa'));
			//$myArrTit=getCamptitTipo($id, "Titulos",'defa');
			//array_push($myArrF, 'sendmail','campaign', 'lasthistorydate');
			//array_push($myArrTit, 'TS', 'Campaign', 'Last mail date');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			}
			
			$query='SELECT distinct bd 
			FROM xima.followup 
			WHERE parcelid IN ("'.str_replace(',','","',$_POST['parcelids_res']).'")';
			$result = mysql_query($query) or die($query.mysql_error());
			
			$datos=array();
			$datos_id=array();
			while($r=mysql_fetch_assoc($result)){
				conectarPorNameCounty($r['bd']);
				
				$xSql="SELECT f.parcelid as pid, f.campaign, f.userid, f.mlnumber,
				f.bd as county, f.address, f.unit, f.city, f.zip, f.sendmail, 
				f.lasthistorydate, f.type, p.beds, p.bath, p.lsqft, p.bheated as larea, 
				f.pendes, p.pool, p.waterf, p.yrbuilt, p.phonenumber2, p.phonenumber1, 
				p.owner_z, p.owner_s, p.owner_p, p.owner_c, p.owner_a, p.owner 
				FROM psummary p 
				INNER JOIN xima.followup f ON (p.parcelid=f.parcelid) 
				WHERE f.userid =".$_COOKIE['datos_usr']['USERID']." and f.type in ('M','FM','BM') 
				and p.parcelid IN ('".str_replace(',',"','",$_POST['parcelids_res'])."')"; 
				$result2 = mysql_query($xSql) or die($xSql.mysql_error());
				
				while($r2=mysql_fetch_assoc($result2)){
					$datos[]=$r2;
					$datos_id[]=$r['pid'];
				}
			}
			
			$fila_tabla=3;
			$fila_tabla_inicio=3;
			
			foreach($datos as $k => $val1){
				$r=$datos[$k];
				foreach($myArrF as $i=>$val){
					if(strtolower($val)=='parcelid' || strtolower($val)=='folio')
					{
						$excel->getActiveSheet()->setCellValueExplicit($abc[$i+1].$fila_tabla ,sprintf('%s',$r[$val]),PHPExcel_Cell_DataType::TYPE_STRING);
					}else{						
						if(strtolower($val)=='pendes' && (is_null($r[$val]) || empty($r[$val]) || $r[$val]=='' || $r[$val]==' '))
							$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,'N');
						else
							$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,utf8_encode($r[$val]));
						if(!is_null($take_comp) && strtolower($val)=='parcelid'){
							$tak='NO';
							foreach($take_comp as $k=>$va){
								if($r[$val]==$va){
									$tak='YES';
								}
							}
							$excel->getActiveSheet()->setCellValue('A'.$fila_tabla ,$tak);
						}
					}
				}
				$fila_tabla++;
			}
			
			$excel->getActiveSheet()->getStyle('B'.$fila_tabla_inicio)->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				)
			);
			$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B'.$fila_tabla_inicio), 'B'.$fila_tabla_inicio.':'.$abc[($myArrNum)].$fila_tabla );
			
			$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
				array(
					'font'    => array(
						'bold'      => true
					),
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
						'top'     => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK
						),
						'bottom'     => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK
						),
						'right'     => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK
						),
						'left'     => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK
						)
					),
					'fill' => array(
						'type'       => PHPExcel_Style_Fill::FILL_SOLID,
						'startcolor' => array(
							'argb' => 'FF89b039'
						)
					)
				)
			);
			$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($myArrNum)].'2' );
					
			$excel->setActiveSheetIndex(0);
			$objWriter = new PHPExcel_Writer_Excel5($excel);
			$nombre = 'Excel/archivos/follow_'.strtotime("now").'.xls';
			$objWriter->save('C:/inetpub/wwwroot/'.$nombre);
			echo "{success:true, excel:'$nombre'}";
			return true;
		break;
		
		//Short Sale
		case 8: 
			$type = $_POST['typeFollow'];
			$set = $_POST['set'];
			$pids = $_POST['pids'];
			$idProcessor= isset($_POST['idProcessor'])	? $_POST['idProcessor']	: '-1';
			$userid 	= isset($_POST['userid']) 		? $_POST['userid'] 		: $_COOKIE['datos_usr']['USERID'];
			
			$myArrF=array('ss_status','address');
			$myArrTit=array('SS Status','Address');

			if($type=='SSP'){
				array_push($myArrF, 'usuario');
				array_push($myArrTit, 'User');
			}else{
				array_push($myArrF, 'processor');
				array_push($myArrTit, 'Negotiator');
			}
			array_push($myArrF, 'package');
			array_push($myArrTit, 'Package');	
			
			$query = "SELECT b.id_require,b.gridName,b.displayName,b.descName
			FROM xima.shortsale_require b 
			INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
			WHERE b.status=1 
			ORDER BY c.order,b.ordShow";
			$result = mysql_query($query);
			
			while($r=mysql_fetch_array($result)){
				array_push($myArrF, $r['gridName']);
				array_push($myArrTit, strtoupper($r['gridName']));
			}
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			}
			
			if($idProcessor!='-1'){
				$xSql="SELECT f.userid, f.ss_status, concat(f.address, ' ', f.unit) as address, 
				f.id_shortsale, concat(x.name,' ',x.surname) as usuario, 
				sp.packageName as package, 
				f.id_packages, f.id_processor 
				FROM xima.shortsale_followup f 
				INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
				INNER JOIN xima.shortsale_packages sp ON (f.id_packages=sp.id_packages)
				WHERE f.id_processor =".$idProcessor;
			}else{
				$xSql="SELECT f.userid, f.ss_status, concat(f.address, ' ', f.unit) as address,  
				f.id_shortsale, fa.agent as processor, sp.packageName as package, 
				f.id_packages, f.id_processor  
				FROM xima.shortsale_followup f 
				INNER JOIN xima.followagent fa ON (f.userid=fa.userid AND f.id_processor=fa.id_processor)
				INNER JOIN xima.shortsale_packages sp ON (f.id_packages=sp.id_packages)
				WHERE f.userid =".$userid; 
			}
			
			$xSql.=' AND f.id_shortsale IN ('.$pids.')';
			
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		//Pending Task
		case 9: 
			include("../../../properties_getgridcamptit.php");
			$id = getArray('PendingTask','result');
			$myArrF=getCamptitTipo($id, "Campos",'defa');
			$myArrTit=getCamptitTipo($id, "Titulos",'defa');
			array_push($myArrF, 'ntask', 'stime', 'etime', 'detail', 'statusschedule', 'typeExec', 'agent', 'phone1');
				array_push($myArrTit, 'Task', 'Sche. Time','Exec. Time', 'Sche. Detail', 'Sche. Status', 'Exec. Type', 'Agent', 'Phone 1');
			$myArrNum = count($myArrTit);
			
			foreach($myArrTit as $i=>$val){
				$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
				$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
			} 

			if($typeFollow=='B'){
				$followType="('B','F','BM','FM')";
			}else{
				$followType="('S')";
			}
			
			$xSql='SELECT s.parcelid as pid, s.userid, f.mlnumber, f.status, f.bd as county,
			f.address, f.lprice, f.marketvalue, f.activevalue, f.dom, f.unit, f.zip, f.agent,
			f.offer, f.coffer, f.contract, f.pof, f.emd, f.realtorsadem as rademdums, f.msj,
			DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ((f.offer/f.lprice)*100) as offerpercent,
			f.statusalt, f.offerreceived, f.followdate, concat(x.name," ",x.surname) as name_follow, 
			f.userid_follow, f.pendes, s.odate as ndate, s.ohour as nhour, concat(s.odate," ",s.ohour) as etime, s.sdate as stime, 
			IF(s.task=0,"",IF(s.task=1,"Send SMS",IF(s.task=2,"Receive SMS",IF(s.task=3,"Send Fax",IF(s.task=4,"Receive Fax",IF(s.task=5,"Send Email",IF(s.task=6,"Receive Email",IF(s.task=7,"Send Document",IF(s.task=8,"Receive Document",IF(s.task=9,"Make Call",IF(s.task=10,"Receive Call",IF(s.task=11,"Send Regular Mail",IF(s.task=12,"Receive Regular Mail",IF(s.task=13,"Send Other","Receive Other")))))))))))))) as ntask, s.idfus, s.status as statusschedule, IF(s.typeExec=1,"Manual","Automatic") as typeExec, s.detail , followagent.phone1 as phone1
			FROM xima.followup_schedule s 
			inner join xima.followup f on f.parcelid=s.parcelid and f.userid=s.userid 
			left join xima.ximausrs x on s.userid_follow=x.userid 
			LEFT  JOIN follow_assignment fa ON f.parcelid = fa.parcelid AND f.userid = fa.userid
			LEFT JOIN followagent ON fa.agentid = followagent.agentid
			WHERE f.userid ='.$userid.'  AND f.type IN '.$followType.' AND s.idfus IN ('.$_POST['pids'].')';	
						
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
	}
	//echo $xSql;

	$sql_tabla=$xSql;
	$fila_tabla=3;
	$fila_tabla_inicio=3;
	require("../../../Excel/tabla.php");
		
	$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			array(
				'font'    => array(
					'bold'      => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'top'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'bottom'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'right'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'left'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					)
				),
				'fill' => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						'argb' => 'FF89b039'
					)
				)
			)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($myArrNum)].'2' );
			
	$excel->setActiveSheetIndex(0);
	$objWriter = new PHPExcel_Writer_Excel5($excel);
	$nombre = 'Excel/archivos/follow_'.strtotime("now").'.xls';
	$objWriter->save('C:/inetpub/wwwroot/'.$nombre);
	echo "{success:true, excel:'$nombre'}";
?>