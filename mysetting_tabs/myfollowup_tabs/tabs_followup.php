<?php
	include('../../properties_conexion.php');
	conectar();
	
	$que="SELECT * FROM xima.ximausrs WHERE userid=".$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($que) or die($que.mysql_error());
	$USERCURRENT=mysql_fetch_array($result);
	
	$query="SELECT skype_enable FROM xima.`contracts_phonesettings` WHERE userid=".$_COOKIE['datos_usr']['USERID'];
	$rest = mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($rest)>0){
		$skype_enable = mysql_fetch_array($rest);
		$skypeCallEnabled = $skype_enable[0]==1 ? false : true;
	}else
		$skypeCallEnabled = false;
	
	$que="SELECT * FROM xima.shortsale_processor_user WHERE userid=".$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($que) or die($que.mysql_error());
	$shortsaleEnabled = mysql_num_rows($result) > 0 ? true : false;
?>
<div align="left" style="height:100%">
    <div id="body_central_1" style="height:100%">
        <div id="tabs4" style="padding-top:2px;"></div>
    </div>
</div>
<script>
	var ancho=640;
	var skypeCallEnabled = <?php echo $skypeCallEnabled===true ? 'true' : 'false';?>;
	var shortsaleEnabled = <?php echo $shortsaleEnabled===true ? 'true' : 'false';?>;
	<?php if($USERCURRENT['idstatus']!=8 && $USERCURRENT['idstatus']!=9){ ?>
		if(user_loged) ancho=system_width; 
	<?php } ?>
	tabs.setHeight(3200);
	viewport.setHeight(tabs.getHeight());
	/*
	var tabsFollowSettings = new Ext.TabPanel({
					id: 'tabsFollowSettings',
					activeTab: 0,
					width: ancho,
					height: tabs.getHeight(),
					plain:true,
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					items:[	
						{
							title: 'Mail Settings',
							id: 'mailsettingsTab5',
							autoLoad: {
								url: 'mysetting_tabs/mycontracts_tabs/mailsettings.php', 
								scripts: true,
								params	: {
									tabId: 'Follow'
								}
							}
						},{
							title: 'Phone Settings',
							id: 'phonesettTab5',
							autoLoad: {url: 'mysetting_tabs/mycontracts_tabs/phonesettings.php', scripts: true}
						},{
							title: 'Templates',
							id: 'templatesTab5',
							autoLoad: {url: 'mysetting_tabs/mycontracts_tabs/contracts_templates.php', scripts: true}
						}
					]
				});*/
	var tabsFollowEmails = new Ext.TabPanel({
			id: 'tabsFollowEmailsId',
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Inbox',
					id: 'followInbox',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowupInbox.php', scripts: true}
				  },
				  {
					title: 'Outbox',
					id: 'followOutbox',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowupOutbox.php', scripts: true}
				  },{// Agregado por Luis R Castro Sugerencia 12625
					title: 'Trash',
					id: 'followTrash',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowupTrash.php', scripts: true}
				  }
				 ]
		});
	var tabsFollow = new Ext.TabPanel({
			id: 'tabsFollowId',
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Following',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowup.php', scripts: true}
				  },
				  {
					title: 'Pending Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_mytasks.php', scripts: true}
				  },
				  {
					title: 'Completed Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_mycompletedtasks.php', scripts: true}
				  },
				  {
					title: 'Pending Contracts',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_mycontracts.php', scripts: true}
				  }
				 ]
		});
	var tabsFollowSelling = new Ext.TabPanel({
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Following',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_selling_myfollowup.php', scripts: true}
				  },
				  {
					title: 'Pending Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_selling_mytasks.php', scripts: true}
				  },
				  {
					title: 'Completed Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_selling_mycompletetasks.php', scripts: true}
				  },
				  {
					title: 'Pending Contracts',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_selling_mycontracts.php', scripts: true}
				  }
				 ]
		});
	var tabsFollowSS = new Ext.TabPanel({
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Following',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale_myfollowup.php', scripts: true}
				  }/*,
				  {
					title: 'Pending Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale_mytasks.php', scripts: true}
				  },
				  {
					title: 'Completed Task',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale_mycompletetasks.php', scripts: true}
				  }*/
				 ]
		});
	var tabsImportData = new Ext.TabPanel({
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Properties',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_importfollow.php', scripts: true}
				  }/*,
				  {
					title: 'Contacts',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_importcontacts.php', scripts: true}
				  }*/
				 ]
		});
	var tabsFollowList = new Ext.TabPanel({
			id: 'terceario',
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{ autoScroll: false},
			items:[	
				{
					title: 'Follow List',
					id: 'followListTab5',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/followlist.php', scripts: true}
				}
			]
		})
	var tabsBlock = new Ext.TabPanel({
			activeTab: 0,
			width: ancho,
			height: tabs.getHeight(),
			plain:true,
			enableTabScroll:true,
			defaults:{  autoScroll: false},
			items:[
				  {
					title: 'Properties',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myblockproperties.php', scripts: true}
				  },
				  {
					title: 'Contacts',
					autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/properties_myblockcontacts.php', scripts: true}
				  }
				 ]
		});
	var tabs4 = new Ext.TabPanel({
		renderTo: 'tabs4',
   		activeTab: 0,
		width: ancho,
		height: tabs.getHeight(),
		plain:true,
		enableTabScroll:true,
		defaults:{  autoScroll: false},
		items:[
			  {
					title: 'Dash Board', 
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/dashboard.php', 
						scripts: true,
						timeout	: 10800
					}
			  }/*,{
					title: 'Settings',
					items: [tabsFollowSettings]
			  }*/,{
					title: 'My Emails',
					items: [tabsFollowEmails]
			  },{
					title: 'Buying',
					items: [tabsFollow]
			  },{
					title: 'Selling',
					items: [tabsFollowSelling]
			  }
			  <?php if($shortsaleEnabled){?>
			  ,{
					title: 'Short Sale',
					items: [tabsFollowSS]
			  }
			  <?php }?>
			  ,{
					title: 'Contacts',
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowagent.php', 
						scripts: true,
						timeout	: 10800
					}
			  },{
					title: 'Mailing Campaigns',
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowmail.php', 
						scripts: true,
						timeout	: 10800
					}
			  },{
					title: 'Import',
					items: [tabsImportData]
			  },{
					title: 'Block',
					items: [tabsBlock]
			  },{
					title: 'Followers',
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/properties_myfollowers.php', 
						scripts: true,
						timeout	: 10800
					}
			  },{
					title: 'Follow List',
					items: [tabsFollowList]
			  }
		]
	});
</script>
