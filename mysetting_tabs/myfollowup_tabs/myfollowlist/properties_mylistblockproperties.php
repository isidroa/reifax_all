<?php
	include("../../../properties_conexion.php");
	conectar();
	include ("../../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_POST['userid']; 
	$typefollower=$_POST['typefollower']==0?'true':'false';
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowlistblock_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowlistblock_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowlistblock_filters"></div><br />
        <div id="myfollowlistblock_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	Right Click -> Overview / Contract 
            <span style="margin-left:15px; margin-right:15px;">
                  -     
            </span>
            Double Click -> Follow History
       	</div>
	</div>
</div>
<script>

	var limitmyfollowlistblock 			= 50;
	var selected_datamyfollowlistblock 	= new Array();
	var selected_datamyfollowlistblockid 	= new Array();
	var AllCheckmyfollowlistblock 			= false;
	var ocultarboton=<?php echo $typefollower; ?>;
	var useridspider = <?php echo $userid; ?>;
	//filter variables
	var filteraddressmyfollowblock 	= '';
	var filtermlnumbermyfollowall 	= '';
	var filterstatusmyfollowall 	= 'ALL';
	var filtercampaignmyfollowmail 	= 'ALL';
	var filterfield					= 'address';
	var filterdirection				= 'ASC';
	var filtercountymyfollowup = 'ALL';
	var filtercitymyfollowup = 'ALL';
	var filterxcodemyfollowup = 'ALL';
	var storemyfollowlistblock = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'float'},
			   {name: 'followdate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'type'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				storemyfollowlistblockall.load();
				AllCheckmyfollowlistblock=false;
				selected_datamyfollowlistblock=new Array(); 
				smmyfollowlistblock.deselectRange(0,limitmyfollowlistblock);
				obj.params.address=filteraddressmyfollowblock;
				obj.params.mlnumber=filtermlnumbermyfollowall;
				obj.params.status=filterstatusmyfollowall;
				obj.params.campaign=filtercampaignmyfollowmail;
				obj.params.county=filtercountymyfollowup;
				obj.params.city=filtercitymyfollowup;
				obj.params.xcode=filterxcodemyfollowup;
			},
			'load' : function (store,data,obj){ 
				if (AllCheckmyfollowlistblock){
					Ext.get(gridmyfollowlistblock.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowlistblock=true;
					gridmyfollowlistblock.getSelectionModel().selectAll();
					selected_datamyfollowlistblock=new Array(); 
				}else{
					AllCheckmyfollowlistblock=false;
					Ext.get(gridmyfollowlistblock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowlistblock.length > 0){
						for(val in selected_datamyfollowlistblock){
							var ind = gridmyfollowlistblock.getStore().find('pid',selected_datamyfollowlistblock[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						
						if (sel.length > 0)
							gridmyfollowlistblock.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemyfollowlistblockall = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'float'},
			   {name: 'followdate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'type'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowblock;
				obj.params.mlnumber=filtermlnumbermyfollowall;
				obj.params.status=filterstatusmyfollowall;
				obj.params.campaign=filtercampaignmyfollowmail;
				obj.params.county=filtercountymyfollowup;
				obj.params.city=filtercitymyfollowup;
				obj.params.xcode=filterxcodemyfollowup;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function followupRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'FM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			default: return ''; break;
		}
	}

	var smmyfollowlistblock = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowlistblock.indexOf(record.get('pid'))==-1)
					selected_datamyfollowlistblock.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowlistblock.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on')){
					AllCheckmyfollowlistblock=true;  
				}
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowlistblock = selected_datamyfollowlistblock.remove(record.get('pid'));
				AllCheckmyfollowlistblock=false;
				Ext.get(gridmyfollowlistblock.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'myfollowlistblock_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowlistblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be deleted.'); return false;
					}
					if(AllCheckmyfollowlistblock==true){
						var totales = storemyfollowlistblockall.getRange(0,storemyfollowlistblockall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowlistblockall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowlistblock[0]+'\'';
						for(i=1; i<selected_datamyfollowlistblock.length; i++){
							pids+=',\''+selected_datamyfollowlistblock[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: useridspider
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
							Ext.Msg.alert("Block Properties", 'Properties deleted.');
							
						}                                
					});
				}
			}),new Ext.Button({
				 tooltip: 'Print Properties',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/printer.png',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowlistblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowlistblock==true){
						var totales = storemyfollowlistblockall.getRange(0,storemyfollowlistblockall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowlistblockall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowlistblock[0];
						for(i=1; i<selected_datamyfollowlistblock.length; i++){
							pids+=','+selected_datamyfollowlistblock[i];
						}
					}
					
					Ext.Ajax.request( 
						{  
							waitMsg: 'Printing Report...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_pdf.php', 
							method: 'POST', 
							timeout :600000,
							params: {
								userweb: 'false',
								parcelids_res: pids,
								template_res: 'Default',
								printType: 1
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','file can not be generated');
								loading_win.hide();
							},
							success:function(response,options){
								var rest = Ext.util.JSON.decode(response.responseText);
								//alert(rest.pdf);
								var url='http://www.reifax.com/'+rest.pdf;
								//alert(url);
								loading_win.hide();
								window.open(url);
								
							}                                
						 }
					);
				 }
			}),new Ext.Button({
				 tooltip: 'Export Excel',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/excel.png',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowlistblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					
					
					if(AllCheckmyfollowlistblock==true){
						var totales = storemyfollowlistblockall.getRange(0,storemyfollowlistblockall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowlistblockall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowlistblock[0];
						for(i=1; i<selected_datamyfollowlistblock.length; i++){
							pids+=','+selected_datamyfollowlistblock[i];
						}
					}
					
					var ownerShow='false';
					Ext.Msg.show({
						title:'Excel Report',
						msg: 'Would you like to save Excel Report with Owner Data?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn, text){
							if (btn == 'yes'){
								ownerShow='true';
							}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',

								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_excel.php', 
								timeout: 106000,
								method: 'POST', 
								params: {
									userweb:'false',
									parcelids_res:pids,
									ownerShow: ownerShow,
									template_res: 'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
						}
					});
				 }
			}),new Ext.Button({
				tooltip: 'Filter Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					/*filteraddressmyfollowblock = '';
					filtermlnumbermyfollowall = '';
					filterstatusmyfollowall = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';*/
					var formmyfollowall = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followblock.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'myfollowlistblock_filters',
						id: 'formmyfollowall',
						name: 'formmyfollowall',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'fbaddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'fbaddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowblock=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fbmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fbmlnumber',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtermlnumbermyfollowall=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-countys',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtercountymyfollowup,
									hiddenName    : 'fcounty',
									hiddenValue   : filtercountymyfollowup,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtercountymyfollowup = record.get('valor');
											combo.findParentByType('form').getForm().findField('fcity').setValue("ALL");
											filtercitymyfollowup='ALL';
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcity',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'City',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-citys',
											'userid': <?php echo $userid;?>,
											county: filtercountymyfollowup
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcityname',
									value         : filtercitymyfollowup,
									hiddenName    : 'fcity',
									hiddenValue   : filtercitymyfollowup,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtercitymyfollowup = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('county',filtercountymyfollowup);
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fxcode',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Property Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-xcodes',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fxcodename',
									value         : filterxcodemyfollowup,
									hiddenName    : 'fxcode',
									hiddenValue   : filterxcodemyfollowup,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterxcodemyfollowup = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fbstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fbstatusname',
									value         : 'ALL',
									hiddenName    : 'fbstatus',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterstatusmyfollowall = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
									//storemyfollowlistblockall.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteraddressmyfollowblock = '';
									filtermlnumbermyfollowall = '';
									filterstatusmyfollowall = 'ALL';
									filtercampaignmyfollowmail 	= 'ALL';
									filtercountymyfollowup = 'ALL';
									filtercitymyfollowup = 'ALL';
									filterxcodemyfollowup = 'ALL';
									
									Ext.getCmp('formmyfollowall').getForm().reset();
									
									storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
									//storemyfollowlistblockall.load();
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 300,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowall,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filteraddressmyfollowblock = '';
					filtermlnumbermyfollowall = '';
					filterstatusmyfollowall = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					filtercountymyfollowup = 'ALL';
					filtercitymyfollowup = 'ALL';
					filterxcodemyfollowup = 'ALL';
					
					storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
				}
			}),new Ext.Button({
				tooltip: 'Unblock Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/unblock.png',
				handler: function(){
					if(selected_datamyfollowlistblock.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be unblocked.'); return false;
					}
					if(AllCheckmyfollowlistblock==true){
						var totales = storemyfollowlistblockall.getRange(0,storemyfollowlistblockall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowlistblockall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowlistblock[0]+'\'';
						for(i=1; i<selected_datamyfollowlistblock.length; i++){
							pids+=',\''+selected_datamyfollowlistblock[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'unblock',
							pids: pids,
							userid: useridspider
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							Ext.Msg.alert("Block properties", selected_datamyfollowlistblock.length+' Properties unblocked.');
							storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
							
							
						}                                
					});
				}
			})		
		]
	});
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/drop-no.gif" />'; 
		else return '<img src="../../img/drop-yes.gif" />';
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/notes/new_msj.png" />';
		else return '';
	}
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenu(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	function typeFollowRender(value, metaData, record, rowIndex, colIndex, store) {
		var aux="";
		var title="";
		if(value!='0'){
			if(value=='LF' || value=='LFM' || value=='LB' || value=='LBM'){
				aux='http://www.reifax.com/img/drop-yes.gif';
				title="Buying";
			}else if(value=='LS'){
				aux='http://www.reifax.com/img/check-blue.gif';
				title="Selling";
			}
			//return "<div><img src='http://www.reifax.com/img/"+aux+"' title='"+title+"'/></div>";
			return '<div title="'+title+'" style="background: url('+aux+') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>';
		}else{
			return "";
		}
	}
	var gridmyfollowlistblock = new Ext.grid.GridPanel({
		renderTo: 'myfollowlistblock_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowlistblock,
		stripeRows: true,
		sm: smmyfollowlistblock, 
		columns: [
			smmyfollowlistblock,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
				echo ",{header: 'E', width: 25, sortable: true, tooltip: 'New Email.', dataIndex: 'msj', renderer: msjRender}";
				echo ",{header: 'Type', width: 40, sortable: true, tooltip: 'Follow Type.', dataIndex: 'type', renderer: typeFollowRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender},{header: 'Follow Date', width: 80, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'followdate', align: 'right', sortable: true}";	
					else
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent', align: 'right'}";
				echo ",{header: 'L. Price', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Listing Price', dataIndex: 'lprice', align: 'right',editor: new Ext.form.TextField({allowBlank: false})}";
				echo ",{header: 'Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Offer.', dataIndex: 'offer', align: 'right',editor: new Ext.form.TextField({allowBlank: false})}";
				echo ",{header: 'C. Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer', align: 'right'}";
				echo ",{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate', align: 'right'}";
				echo ",{header: 'C', width: 25, sortable: true, tooltip: 'Contract.', dataIndex: 'contract', renderer: checkRender}";
				echo ",{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}";
				echo ",{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}";
				echo ",{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}";
				echo ",{header: 'O', width: 25, sortable: true, tooltip: 'Offer Received.', dataIndex: 'offerreceived', renderer: checkRender}";
		   ?>		
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowlistblock',
            pageSize: limitmyfollowlistblock,
            store: storemyfollowlistblock,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Block.',
			emptyMsg: "No follow Block to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows block per page.',
				text: 50,
				handler: function(){
					limitmyfollowlistblock=50;
					Ext.getCmp('pagingmyfollowlistblock').pageSize = limitmyfollowlistblock;
					Ext.getCmp('pagingmyfollowlistblock').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows block per page.',
				text: 80,
				handler: function(){
					limitmyfollowlistblock=80;
					Ext.getCmp('pagingmyfollowlistblock').pageSize = limitmyfollowlistblock;
					Ext.getCmp('pagingmyfollowlistblock').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows block per page.',
				text: 100,
				handler: function(){
					limitmyfollowlistblock=100;
					Ext.getCmp('pagingmyfollowlistblock').pageSize = limitmyfollowlistblock;
					Ext.getCmp('pagingmyfollowlistblock').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				
			}
		}
	});
	
	storemyfollowlistblock.load({params:{start:0, limit:limitmyfollowlistblock}});
	//storemyfollowlistblockall.load();
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowlistblock_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowlistblock_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>