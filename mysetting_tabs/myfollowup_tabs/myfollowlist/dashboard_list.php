<style>
.dashboard{
	font-size:14px;
	margin: auto;
}
.dashboard .contentText{
	float:left; 
	color:#FFF; 
	text-align:center;
	margin-right:15px;
}
.dashboard .title{
	background:#F8F8F8;
	padding:10px 8px;
	margin-bottom:10px;
	text-align:justify;
}

.dashboard label{
	color:#021500;
	font-weight:bold;
	line-height: 25px;
}
.dashboard .columns{
	width: 33%;
	float: left;
	margin-left: 1px;
	margin-right: 2px;
}
.dashboard .two {
  margin: 0;
  width: 50%;
}
.dashboard table{
	padding:5px;
	width:100%;
	border: solid 1px #E1E1E1;
	border-collapse:collapse;
}
.dashboard .cuote{
	margin-top:7px;
}
.dashboard table th .colCenter {
	width:100%;
	height:100%;
}
.dashboard table th{
	background:#FFF;
	border:none;
	border-bottom:solid 2px #1D6AAA;
	padding:7px 4px;
	text-align:right;
	width:12%;
}
.dashboard table th:first-child{
	text-align:left;
	width:78%;
}
.dashboard table th:last-child{
	width:8%;
}
.dashboard table td{	
	border:none;
	padding:7px 4px;
	text-align:right;
	width:12%;
	height: 35px;
}
.dashboard table td:first-child{
	text-align:left;
	width:78%;
}
.dashboard table td:last-child{
	width:8%;
}
.dashboard .cuote {
  margin-top: 7px;
}
.dashboard .loadingBlock {
	width: 100%;
	height: 100%;
	background: url(http://www.reifax.com/img/ajax-loader.gif) center center no-repeat #FFF;
	display:block;
	position:absolute;
}

.whitetext {
  color: #FFFFFF !important;
}
.greentext {
  color: #9CC55F !important;
}
.bluetext {
  color: #1D6AAA !important;
}
.shortBox {
  margin-top: 5px;
  width: 100px !important;
}
.par {
  background: none repeat scroll 0 0 #F8F8F8 !important;
}
.impar {
  background: none repeat scroll 0 0 #F3F3F3 !important;
}
.cuote {
  background-color: #D90461;
  float: left;
  height: 5px;
  margin-right: 5px;
  width: 5px;
}
.between {
  float: right;
  margin-right: 30px;
  display: none;
}
#from, #to, #from2, #to2 {
	width: 100px;
	margin-top: 5px;
}
#propertiesList, #newList, #statusList, #taskList, #pendingList {
	position:relative; 
}
</style>
<div class="dashboard">
    <table>
        <tr>
            <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                <div
                <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; text-align:center;">
                    <a id="buttonRefreshDashboardList" class="overviewBotonCss3" href="javascript:void(0);">Refresh</a>
                    <label class="bluetext">
                        Type: 
                    </label>
                    <select class="shortBox" id="TypeList" name="type">
                        <option value="B">Buying</option>
                        <option value="S">Selling</option>
                    </select>
                </div>
            </th>
        </tr>
    </table>
    
    <div id="propertiesList" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Properties</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>All
                </td>
                <td>
                    <span id="all">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Active For Sale
                </td>
                <td>
                    <span id="afs">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Non-Active
                </td>
                <td>
                    <span id="na">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Not For Sale
                </td>
                <td>
                    <span id="nfs">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Sold
                </td>
                <td>
                    <span id="s">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="newList" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">New</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="vm">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="sms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="fax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Emails
                </td>
                <td>
                    <span id="email">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="statusList" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Status</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Pending Contracts
                </td>
                <td>
                    <span id="pc">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Contracts Sent
                </td>
                <td>
                    <span id="cs">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Offers Received
                </td>
                <td>
                    <span id="or">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Proof of Funds
                </td>
                <td>
                    <span id="pof">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Earnes Money Deposit
                </td>
                <td>
                    <span id="emd">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Addemdums
                </td>
                <td>
                    <span id="ad">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div><span id="nameCO">Counter Offers</span>
                </td>
                <td>
                    <span id="co">--</span>
                </td>
            </tr>
        </table>
	</div>
    
    <div class="clear">&nbsp;</div>
    
    <table>
        <tr>
            <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; width:49%;">
                    <label class="bluetext">
                        Period: 
                    </label>
                    <select class="shortBox" id="Period1List" name="period1">
                        <option value="A">All</option>
                        <option value="T">Today</option>
                        <option value="LW">Last Week</option>
                        <option value="LM">Last Month</option>
                        <option value="B">Between</option>
                    </select>
                    <div class="between" id="betweenContainer1List">
                    	<label class="bluetext">
                            From: 
                        </label>
                        <input type="text" id="from" name="from" />
                        <label class="bluetext">
                            To: 
                        </label>
                        <input type="text" id="to" name="to" />
                    </div> 
                </div>
                <div style="float:left; margin-left:10px; margin-top:5px; color:#FFF; width:49%;">
                    <label class="bluetext">
                        Period: 
                    </label>
                    <select class="shortBox" id="Period2List" name="period2">
                        <option value="A">All</option>
                        <option value="D">Due</option>
                        <option value="T">Today</option>
                        <option value="1W">1 Week</option>
                        <option value="1M">1 Month</option>
                        <option value="B">Between</option>
                    </select>
                    <div class="between" id="betweenContainer2List">
                    	<label class="bluetext">
                            From: 
                        </label>
                        <input type="text" id="from2" name="from2" />
                        <label class="bluetext">
                            To: 
                        </label>
                        <input type="text" id="to2" name="to2" />
                    </div>
                </div>
            </th>
        </tr>
    </table>
    
    <div id="taskList" class="columns two">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Task</span>
                </th>
                <th>
                    <span class="bluetext">Sent</span>
                </th>
                <th>
                    <span class="bluetext">Received</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Call
                </td>
                <td>
                    <span id="scall">--</span>
                </td>
                <td>
                    <span id="rcall">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="ssms">--</span>
                </td>
                <td>
                    <span id="rsms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="sfax">--</span>
                </td>
                <td>
                    <span id="rfax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Email
                </td>
                <td>
                    <span id="semail">--</span>
                </td>
                <td>
                    <span id="remail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Documents
                </td>
                <td>
                    <span id="sdoc">--</span>
                </td>
                <td>
                    <span id="rdoc">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Mail
                </td>
                <td>
                    <span id="smail">--</span>
                </td>
                <td>
                    <span id="rmail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="svmail">--</span>
                </td>
                <td>
                    <span id="rvmail">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Other
                </td>
                <td>
                    <span id="sother">--</span>
                </td>
                <td>
                    <span id="rother">--</span>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="pendingList" class="columns two">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Pending Task</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Call
                </td>
                <td>
                    <span id="pcall">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="psms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="pfax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Email
                </td>
                <td>
                    <span id="pemail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Documents
                </td>
                <td>
                    <span id="pdoc">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Mail
                </td>
                <td>
                    <span id="pmail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="pvmail">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Other
                </td>
                <td>
                    <span id="pother">--</span>
                </td>
            </tr>
        </table>
    </div>
</div> 

<script>
	$(document).ready(function(){		
		loadAllDataList();
		$('#TypeList').bind('change',loadAllDataList);
		$('#Period1List').bind('change',loadPeriod1Data);
		$('#Period2List').bind('change',loadPeriod2Data);
		$('#buttonRefreshDashboardList').click(loadAllDataList);
		
		var dates1 = $( "#betweenContainer1List #from, #betweenContainer1List #to" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		var nowDate = new Date();
		var currMonth = nowDate.getMonth()+1;
		if(currMonth<10){
			currMonth='0'+currMonth;
		}
		var currDay = nowDate.getDate();
		if(currDay<10){
			currDay='0'+currDay;
		}
		var currYear = nowDate.getFullYear();
		dates1.val(currYear+'-'+currMonth+'-'+currDay);
		
		var dates2 = $( "#betweenContainer2List #from2, #betweenContainer2List #to2" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		dates2.val(currYear+'-'+currMonth+'-'+currDay);
		
		$('#betweenContainer1List #from').change(loadPeriod1Data);
		$('#betweenContainer1List #to').change(loadPeriod1Data);
		$('#betweenContainer2List #from2').change(loadPeriod2Data); 
		$('#betweenContainer2List #to2').change(loadPeriod2Data); 
	
		
		function loadProperties(type){
			var loading = $('#propertiesList .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=properties&userid=<?php echo $_POST['userid'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#propertiesList #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadNew(type){
			var loading = $('#newList .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=new&userid=<?php echo $_POST['userid'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#newList #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadStatus(type){
			var loading = $('#statusList .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=status&userid=<?php echo $_POST['userid'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#statusList #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadTask(type,period1){
			var period1 = $('#Period1List').val();
			var from = $('#betweenContainer1List #from').val();
			var to = $('#betweenContainer1List #to').val();
			
			var loading = $('#taskList .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=task&userid=<?php echo $_POST['userid'];?>&type="+type+"&period1="+period1+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$.each(v, function(i2,v2){
							$('#taskList #'+i2).html(v2);
						});
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadPendingTask(type){
			var period2 = $('#Period2List').val();
			var from = $('#betweenContainer2List #from2').val();
			var to = $('#betweenContainer2List #to2').val();
			
			var loading = $('#pendingList .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=pending&userid=<?php echo $_POST['userid'];?>&type="+type+"&period2="+period2+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#pendingList #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadAllDataList(){
			var type = $('#TypeList').val();
			
			if(type=='B')
				$('#nameCO').html('Counter Offers');
			else
				$('#nameCO').html('Offers');
			
			loadProperties(type);
			loadNew(type);
			loadStatus(type);
			
			loadTask(type);
			loadPendingTask(type);
		}
		
		function loadPeriod1Data(){
			var type = $('#TypeList').val();
			
			if($('#Period1List').val()=='B'){
				$( "#betweenContainer1List:hidden" ).fadeIn(300);
			}else{
				$( "#betweenContainer1List:visible" ).fadeOut(300);
			}
			loadTask(type);
		}
		
		function loadPeriod2Data(){
			var type = $('#TypeList').val();
			
			if($('#Period2List').val()=='B'){ 
				$( "#betweenContainer2List:hidden" ).fadeIn(300);
			}else{
				$( "#betweenContainer2List:visible" ).fadeOut(300);
			}
			loadPendingTask(type);
		}
	});
	
</script>