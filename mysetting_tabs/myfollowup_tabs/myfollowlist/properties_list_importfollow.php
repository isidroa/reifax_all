<?php
	include("../../../properties_conexion.php");
	conectar();
	
	$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	//llenar combo de estado con los estados que tiene el usuario
	if($_COOKIE['datos_usr']['idusertype']!=1 && $_COOKIE['datos_usr']['idusertype']!=4 && $_COOKIE['datos_usr']['idusertype']!=7){ 
		$query = "Select us.idstate, ls.State
		From xima.userstate us 
		INNER JOIN xima.lsstate ls ON (us.idstate=ls.idstate)
		WHERE us.userid=".$_COOKIE['datos_usr']["USERID"];
	}else{
		$query = "Select ls.idstate, ls.State
		From xima.lsstate ls 
		ORDER BY ls.State";	 
	}
	$result = mysql_query($query) or die($query." ".mysql_error());

	$states="['0','Select'],";
	while($row=mysql_fetch_array($result, MYSQL_ASSOC)){
		$states.="['".$row['idstate']."','".$row['State']."'],";
	}
	
	//determinar los defcounty y defstate
	$query = "Select uc.idcounty,lc.idstate 
	From xima.usercounty uc 
	INNER JOIN xima.lscounty lc ON (uc.idcounty=lc.idcounty)
	WHERE uc.defcounty=1 AND uc.userid=".$_COOKIE['datos_usr']["USERID"];
	$result = mysql_query($query) or die($query." ".mysql_error());
	$r=mysql_fetch_array($result);
	
	$importliststate  = $r['idstate'];
	$importlistcounty = $r['idcounty'];
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowlistimport_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowlistimport_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowlistimport_filters"></div><br />
        <div id="myfollowlistimport_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	
       	</div>
	</div>
</div>
<script>
	var importlistcounty=<?php echo strlen($defcounty)>0 ? $defcounty : 1;?>;
	var importliststate=<?php echo strlen($defstate)>0 ? $defstate : 1;?>;
	var states=new Ext.data.ArrayStore({
										fields: ['id','name'],
										data  : [<?php echo rtrim($states,','); ?>]
									});
	
	var condados=new Ext.data.JsonStore({  
        url:'mysetting_tabs/myaccount_tabs/getcountys.php',  
        root:'results',  
        fields: ['id','name'],
		listeners: {
			'load' : function (store,data,obj){
				Ext.getCmp('importlistcounty').setValue(importlistcounty);
			}
		}
    });
	
	condados.load({params:{selectstate:importliststate}});
	
	var formmyfollowcontract = new Ext.FormPanel({
		url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
		frame:true,
		renderTo: 'myfollowlistimport_filters',
		bodyStyle:'padding:5px 5px 0;',
		title: 'Import Options',
		id: 'formmyfollowimport',
		name: 'formmyfollowimport',
		layout: 'table',
		layoutConfig: {columns:4},
		defaults: {width: 200},
		items:[{
				layout	: 'form',
				id		: 'fstateimportlist',
				labelWidth: 50,
				items	: [{
					xtype         : 'combo',
					name		  : 'importliststate',
					id			  : 'importliststate',
					mode          : 'local',
					fieldLabel    : 'State',
					triggerAction : 'all',
					width: 120,	
					store         : states,
					displayField  : 'name',
					valueField    : 'id',
					hiddenName    : 'fstate',
					allowBlank    : false,
					editable	  : false,
					value: importliststate,			
					listeners	  : {
						'select'  : function(combo,record,index){
							importlistcounty=0;
							condados.load({params:{selectstate:Ext.getCmp('importliststate').getValue()}});
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fcountyimportlist',
				labelWidth: 50,
				items	: [
					{
						xtype: 'combo',
						name: 'importlistcounty',
						id: 'importlistcounty',
						hiddenName	:'condado',
						fieldLabel: 'County',
						typeAhead: true,
						store: condados,
						triggerAction: 'all',
						mode: 'local',
						editable: false,
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank: false,
						displayField:'name',
						valueField: 'id',
						width: 120,	
						allowBlank    : false,
						value: importlistcounty,
						listeners	  : {
							'select'  : function(combo,record,index){
								
							}
						}
					}
				]
			},{
				layout	: 'form',
				id		: 'ftypeimportlist',
				labelWidth: 50,
				items	: [{
					xtype         : 'combo',
					name		  : 'importlisttype',
					id			  : 'importlisttype',
					mode          : 'local',
					fieldLabel    : 'Type',
					triggerAction : 'all',
					width: 120,	
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Parcel Id'],
							['1','Property Address']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					hiddenName    : 'fstate',
					allowBlank    : false,
					editable	  : false,
					value: '0'
				}]
			},{
				layout	: 'form',
				id		: 'fsourceimportlist',
				labelWidth: 50,
				items	: [{
					xtype         : 'combo',
					name		  : 'importlistsource',
					id			  : 'importlistsource',
					mode          : 'local',
					fieldLabel    : 'To',
					triggerAction : 'all',
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['B','Buying'],
							['S','Selling'],
							['M','Mailing']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					allowBlank    : false,
					editable	  : false,
					width: 120,
					value: 'B',
					listeners	  	: {
						'select'  : function(combo,record,index){
							var val 	= record.get('valor');
							var camp 	= Ext.getCmp('fcampimportlist')
							if(val == 'M')
								camp.setVisible(true);
							else
								camp.setVisible(false);
							//Ext.getCmp('formmyfollowimport').doLayout();
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fcampimportlist',
				colspan	: 4,
				hidden	: true,
				width: 800,
				items	: [{
					xtype	  :	'textfield',
					width	  : 750,
					name	  : 'importlistcamp',
					id		  : 'importlistcamp',	
					fieldLabel: 'Campaigns'
				}]
			},{
				html: '<p style="text-align:center; color: #2B73E6;" >Each property in a different line.</p>',
				colspan: 4,
				width: 850,
			},{
				layout	: 'form',
				id		: 'fdatalistimportlist',
				colspan	: 4,
				width: 800,
				items	: [{
					xtype	  :	'textarea',
					height	  : 300,
					width	  : 750,
					name	  : 'datalistimport',
					id		  : 'datalistimport',	
					fieldLabel: 'Data to Import',
					enableKeyEvents: true,
					autoScroll: true
				}]
			}
		],
		buttons:[
			{
				text		  : 'Import',
				handler  	  : function(){
					countProgressBarImport=0;
					registros = [];
					registrosProblema = [];
					registrosExisten = [];
					conseguidos = 0;
					noconseguidos = 0;
					multiples = 0;
					importados = 0;
					var valor = Ext.getCmp('datalistimport').getValue();
					var split = valor.split('\n');
					for (var i = 0; i < split.length; i++)
						if (split[i] && split[i].trim()!='') registros.push(split[i]);
					
					totalProcesar = registros.length;
					if(totalProcesar>100){
						totalProcesar = 100;
					}
					if(totalProcesar>0){
						var source = Ext.getCmp('importlistsource').getValue();
						var camp = Ext.getCmp('importlistcamp').getValue();
						
						if(source != 'M' || (source == 'M' && camp.length > 0)){
							progressBarImportListFollow_win.show();
							progressBarImportListFollow.updateProgress(
								((countProgressBarImport+1)/totalProcesar),
								"Processing "+(countProgressBarImport+1)+" of "+totalProcesar
							);
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_getimport.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									dataimport: registros[countProgressBarImport],
									importtype: Ext.getCmp('importlisttype').getValue(),
									importcounty: Ext.getCmp('importlistcounty').getValue(),
									importstate: Ext.getCmp('importliststate').getValue(),
									importsource: Ext.getCmp('importlistsource').getValue(),
									importcamp: Ext.getCmp('importlistcamp').getValue(),
									userid: <?php echo $userid;?>,
									type: 'import'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success: ejecutarImport
							});
						}else
							Ext.MessageBox.alert('Warning','You need a campaign name to import properties to the mail campaign.');
					}else
						Ext.MessageBox.alert('Warning','At least you have a property to import.');
				}				
			},{
				text		  : 'Clear',
				handler  	  : function(){
					Ext.getCmp('datalistimport').setValue('');
				}
			}
		],
		buttonAlign: 'center' 
	})
	
	var countProgressBarImport=0;
	var totalProcesar=0;
	var registros = [];
	var conseguidos = 0;
	var noconseguidos = 0;
	var multiples = 0;
	var importados = 0;
	var registrosProblema = [];
	var registrosExisten = [];
	function ejecutarImport(response,options){
		
		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			if(resp.found>0)
				conseguidos++;
			if(resp.nofound>0)
				noconseguidos++;
			if(resp.multi>0)
				multiples++;
				
			if(resp.ingresados>0)
				importados++;
				
			if(resp.nofound>0 || resp.multi>0)
				registrosProblema.push(registros[countProgressBarImport]);
			
			if(resp.existeimport>0)
				registrosExisten.push(registros[countProgressBarImport]);	
									
			if((countProgressBarImport)==totalProcesar-1){
				progressBarImportListFollow_win.hide();
				loading_win.hide();
				var r=Ext.decode(response.responseText);
				var texto='Total Properties: '+totalProcesar+'<br>Properties Found: '+(totalProcesar - noconseguidos)+'<br>Properties not found: '+noconseguidos;
				texto+='<br><br>Total Properties: '+totalProcesar;
				if(multiples>0)
					texto+='<br>Properties with multiple Results: '+multiples;
				if(registrosExisten.length>0)
					texto+='<br>Properties already following: '+registrosExisten.length;	
				texto+='<br>Properties Imported: '+importados;
				
				if(registrosProblema.length>0){
					texto+='<br><br>Not found or multiple: ';
					for (var i = 0; i < registrosProblema.length; i++){
						texto+='<br>'+registrosProblema[i];
					}
				}
				if(registrosExisten.length>0){
					texto+='<br><br>Already following: ';
					for (var i = 0; i < registrosExisten.length; i++){
						texto+='<br>'+registrosExisten[i];
					}
				}

				
				Ext.getCmp('datalistimport').setValue('');
				Ext.MessageBox.alert('Follow up',texto);
			}else{
				countProgressBarImport++;
				progressBarImportListFollow.updateProgress(
					((countProgressBarImport+1)/totalProcesar),
					"Processing "+(countProgressBarImport+1)+" of "+totalProcesar
				);
				Ext.Ajax.request({  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_getimport.php', 
					method: 'POST',
					timeout :600000, 
					params: { 
						dataimport: registros[countProgressBarImport],
						importtype: Ext.getCmp('importlisttype').getValue(),
						importcounty: Ext.getCmp('importlistcounty').getValue(),
						importstate: Ext.getCmp('importliststate').getValue(),
						importsource: Ext.getCmp('importlistsource').getValue(),
						importcamp: Ext.getCmp('importlistcamp').getValue(),
						userid: <?php echo $userid;?>,
						type: 'import'
					},
					
					failure:function(response,options){
						progressBarImportListFollow_win.hide();
						var output = '';
						for (prop in response) {
						  output += prop + ': ' + response[prop]+'; ';
						}
						Ext.MessageBox.alert('Warning',output);
					},
					
					success: ejecutarImport                      
				});
			}
		}else{
			progressBarImportListFollow_win.hide();
		}	
	}
	var progressBarImportListFollow= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarImportListFollow_win=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarImportListFollow]
	});		
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowlistimport_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowlistimport_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>
