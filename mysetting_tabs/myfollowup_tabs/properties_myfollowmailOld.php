<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmail_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowmail_filters"></div><br />
        <div id="myfollowmail_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	Right Click -> Overview / Contract 
            <span style="margin-left:15px; margin-right:15px;">
                  -     
            </span>
            Double Click -> Follow History
       	</div>
	</div>
</div>
<script>

	var limitmyfollowmail 			= 50;
	var selected_datamyfollowmail 	= new Array();
	var selected_datamyfollowmailid 	= new Array();
	var AllCheckmyfollowmail 			= false;
	
	//filter variables
	var filteraddressmyfollowmail 	= '';
	var filtermlnumbermyfollowmail 	= '';
	var filterstatusmyfollowmail 	= 'ALL';
	var filtercampaignmyfollowmail 	= 'ALL';
	var filterfield					= 'address';
	var filterdirection				= 'ASC';

	var storemyfollowmail = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				AllCheckmyfollowmail=false;
				selected_datamyfollowmail=new Array(); 
				smmyfollowmail.deselectRange(0,limitmyfollowmail);
				obj.params.address=filteraddressmyfollowmail;
				obj.params.mlnumber=filtermlnumbermyfollowmail;
				obj.params.status=filterstatusmyfollowmail;
				obj.params.campaign=filtercampaignmyfollowmail;
			},
			'load' : function (store,data,obj){
				storemyfollowmailall.load(); 
				if (AllCheckmyfollowmail){
					Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowmail=true;
					gridmyfollowmail.getSelectionModel().selectAll();
					selected_datamyfollowmail=new Array(); 
				}else{
					AllCheckmyfollowmail=false;
					Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowmail.length > 0){
						for(val in selected_datamyfollowmail){
							var ind = gridmyfollowmail.getStore().find('pid',selected_datamyfollowmail[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						
						if (sel.length > 0)
							gridmyfollowmail.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemyfollowmailall = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowmail;
				obj.params.mlnumber=filtermlnumbermyfollowmail;
				obj.params.status=filterstatusmyfollowmail;
				obj.params.campaign=filtercampaignmyfollowmail;
				obj.params.sort=filterfield;
				obj.params.dir=filterdirection;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive Call." src="../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../img/notes/reci_other.png" />'; break;
		}
	}
	
	function followupRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'FM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			case 'BM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			case 'LM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			case 'LFM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			case 'LBM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			default: return ''; break;
		}
	}

	var smmyfollowmail = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmail.indexOf(record.get('pid'))==-1)
					selected_datamyfollowmail.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowmail.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on')){
					AllCheckmyfollowmail=true;  
				}
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmail = selected_datamyfollowmail.remove(record.get('pid'));
				AllCheckmyfollowmail=false;
				Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'myfollowmail_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Mailing Campaign',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be deleted.'); return false;
					}
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowmail[0]+'\'';
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=',\''+selected_datamyfollowmail[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							//storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Properties deleted.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Send Another Mail',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/ximaicon/mailcampaing.jpg',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be sent.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'resend',
							pid: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							//storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Mailing Campaings Properties sended again.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Follow Up Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/ximaicon/followup.jpg',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be followed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					} 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'followup',
							pid: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							//storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Properties followed.');  
							
						}                                
					});
				}
			}),new Ext.Button({
				 tooltip: 'Print Mailing',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/printer.png',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					Ext.Ajax.request( 
						{  
							waitMsg: 'Printing Report...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_pdf.php', 
							method: 'POST', 
							timeout :600000,
							params: {
								userweb: 'false',
								parcelids_res: pids,
								template_res: 'Default',
								printType: 0
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','file can not be generated');
								loading_win.hide();
							},
							success:function(response,options){
								var rest = Ext.util.JSON.decode(response.responseText);
								//alert(rest.pdf);
								var url='http://www.reifax.com/'+rest.pdf;
								//alert(url);
								loading_win.hide();
								window.open(url);
								
							}                                
						 }
					);
				 }
			}),new Ext.Button({
				 tooltip: 'Export Excel',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/excel.png',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					var ownerShow='false';
					Ext.Msg.show({
						title:'Excel Report',
						msg: 'Would you like to save Excel Report with Owner Data?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn, text){
							if (btn == 'yes'){
								ownerShow='true';
							}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',

								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_excel.php', 
								timeout: 106000,
								method: 'POST', 
								params: {
									userweb:'false',
									parcelids_res:pids,
									ownerShow: ownerShow,
									template_res: 'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
						}
					});
				 }
			}),new Ext.Button({
				 tooltip: 'Print Labels',
				 cls:'x-btn-text-icon',
				 iconAlign: 'left',
				 text: ' ',
				 width: 30,
				 height: 30,
				 scale: 'medium',
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_labels.php', 
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 430,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: pids
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											//Ext.Msg.alert("Failure", action.result.pdf);
											var url='http://www.reifax.com/'+action.result.pdf;
											loading_win.hide();
											window.open(url);
											//window.open(url,'Print Labels',"fullscreen",'');
										},
										failure: function(form, action) {
											Ext.Msg.alert("Failure", action.result.msg);
											loading_win.hide();
										}
									});
								}
						},{
							text: 'Cancel',
							handler  : function(){
									simple.getForm().reset();
								}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 400,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			}),new Ext.Button({
				tooltip: 'Filter Mailing Campaign',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					filteraddressmyfollowmail = '';
					filtermlnumbermyfollowmail = '';
					filterstatusmyfollowmail = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					var formmyfollowmail = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'myfollowmail_filters',
						id: 'formmyfollowmail',
						name: 'formmyfollowmail',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowmail=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fmlnumber',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtermlnumbermyfollowmail=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : 'ALL',
									hiddenName    : 'fstatus',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterstatusmyfollowmail = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcampaign',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Campaign',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'persID',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'campaigns',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'},
											{name:'persFirstName', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followmail.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcampaignname',
									value         : 'ALL',
									hiddenName    : 'fcampaign',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtercampaignmyfollowmail = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
									//storemyfollowmailall.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteraddressmyfollowmail = '';
									filtermlnumbermyfollowmail = '';
									filterstatusmyfollowmail = 'ALL';
									filtercampaignmyfollowmail 	= 'ALL';
									
									Ext.getCmp('formmyfollowmail').getForm().reset();
									
									storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
									//storemyfollowmailall.load();
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 250,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowmail,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filteraddressmyfollowmail = '';
					filtermlnumbermyfollowmail = '';
					filterstatusmyfollowmail = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					
					storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
				}
			}),new Ext.Button({
				tooltip: 'Mail Merge',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/mailmerge.jpg',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be generated.'); return false;
					}
					//var pids=new Array();
					var aviso=false;
					if(AllCheckmyfollowmail==true){
						var total=storemyfollowmailall.getCount();
						if(total>500){
							aviso=true;
							total=500;
						}
						var totales = storemyfollowmailall.getRange(0,total);
						var pids=totales[0].data.pid;
						for(i=0;i<total;i++){
							pids+=','+totales[i].data.pid;	
							//pids.push(totales[i].data.pid);
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=0; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
							//pids.push(selected_datamyfollowmail[i]);
						}
					}
					
					var formTemplate = new Ext.FormPanel({
						url           : 'mysetting_tabs/myfollowup_tabs/properties_followgetdoc.php', 
						frame         : true,
						monitorValid  : true,
						id            : 'formTemplate',
						title         : 'Select Template.',
						width         : 450,
						labelWidth    : 130,
						waitMsgTarget : 'Generating...',
						items : [
							{
								xtype         : 'combo',
								name          : 'ccontracttemplate',
								id            : 'ccontracttemplate',
								hiddenName    : 'contracttemplate',
								fieldLabel    : 'Template',
								typeAhead     : true,
								autoSelect    : true,
								mode          : 'local',
								store         : new Ext.data.JsonStore({
									root:'results',
									totalProperty:'total',
									autoLoad: true, 
									baseParams: {
										type: 'docstemplates',
										template_type: 2,
										'userid': <?php echo $userid;?>
									},
									fields:[
										{name:'id', type:'int'},
										{name:'name', type:'string'},
										{name:'default', type:'int'}
									],
									url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
									listeners     : {
										'load'  : function(store, records) {
											var sel=records[0].get('id');
											//Ext.getCmp('ccontracttemplate').setValue(sel); 
											for(i=1;i<records.length;i++){
												if(records[i].get('default')==1)
													sel=records[i].get('id');
											}
											Ext.getCmp('ccontracttemplate').setValue(sel); 
										}
									}
								}),
								triggerAction : 'all', 
								editable      : false,
								selectOnFocus : true,
								allowBlank    : false,
								displayField  : 'name',
								valueField    : 'id',
								value		  : 0,
								width         : 260
							},{
								xtype         : 'hidden',
								name          : 'userid',
								value         : <?php echo $userid;?>
							},{
								xtype         : 'hidden',
								name          : 'pid',
								id            : 'pid',
								value		  : pids	
							},{
							
							},{
								xtype         : 'hidden',
								name          : 'procesado',
								id            : 'procesado',
								value		  : ''	
							},{
								xtype         : 'hidden',
								name          : 'generafinal',
								id            : 'generafinal',
								value		  : ''	
							}
							
						],
						style       : "text-align:left",  
						formBind    : true,
						buttonAlign : 'center', 
						buttons     : [
							{
								text    : 'Generate',
								handler : function() {
									if(aviso==true){
										alert('Only the first 500 properties will be sent');
									}
									mailmerge_win.show();
									formTemplate.getForm().submit({
										timeout : 600,
										success : function(form, action) {
											var url='http://www.reifax.com/'+action.result.pdf;
											mailmerge_win.hide();
											window.open(url);
										},
										failure : function(form, action) {
											mailmerge_win.hide();
											winTemplate.close();
											Ext.MessageBox.alert('Warning', action.result.msg);
										}
									});
								},
								id      : 'generatePdf',
								hidden  : false
							},{
								text    : 'Cancel',
								handler : function(){
									winTemplate.close();
								}
							}
						]
					});
					var winTemplate = new Ext.Window({
						width       : 450,
						autoHeight  : true,
						modal       : true,
						plain       : true,
						listeners   : {
							'beforeshow' : function() {
								formTemplate.getForm().reset();
							}
						},
						items       : [
							formTemplate
						],
						buttons : [{
							text     : 'Close',
							handler  : function(){
								winTemplate.close();
							}
						}]
					});
					winTemplate.show(); 
				}
			}),new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('Mailingcampaing');
				}
			})		
		]
	});
	var mailmerge_win=new Ext.Window({
		 width:250, 
		 autoHeight: true,
		 resizable: false,
		 modal: true,
		 border:false,
		 closable:false,
		 plain: true,
		 html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="http://www.reifax.com/img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, Generating Mail Merge!...</div></div>'
	});
	var gridmyfollowmail = new Ext.grid.GridPanel({
		renderTo: 'myfollowmail_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemyfollowmail,
		stripeRows: true,
		sm: smmyfollowmail, 
		columns: [
			smmyfollowmail,
			{header: '', hidden: true, editable: false, dataIndex: 'pid'},
			{header: 'F', width: 25, renderer: followupRender, tooltip: 'Followed.', dataIndex: 'type'},
			{header: 'TS', width: 25, sortable: true, tooltip: 'Times Sent', dataIndex: 'sendmail'},
			{header: 'Campaign', width: 100, sortable: true, tooltip: 'Campaign', dataIndex: 'campaign'}
			<?php 
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					elseif($val->name!='agent')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
		   ?>
		   ,{header: "City", dataIndex: 'city', tooltip: 'City.'}	
		   ,{header: "Last mail date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'lasthistorydate', tooltip: 'Last Regular Mail Date.'}		
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowmail',
            pageSize: limitmyfollowmail,
            store: storemyfollowmail,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Mail.',
			emptyMsg: "No follow Mail to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows mail per page.',
				text: 50,
				handler: function(){
					limitmyfollowmail=50;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows mail per page.',
				text: 80,
				handler: function(){
					limitmyfollowmail=80;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows mail per page.',
				text: 100,
				handler: function(){
					limitmyfollowmail=100;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				
				if(document.getElementById('reportsTab')){
					var tab = tabs.getItem('reportsTab');
					tabs.remove(tab);
				}
				
				tabsFollow.add({
					title: ' Follow ',
					id: 'followTab', 
					closable: true,
					items: [	
						new Ext.TabPanel({
							id: 'historyTab',
							activeTab: 0,
							width: ancho,
							height: tabs.getHeight(),
							plain:true,
							listeners: {
								'tabchange': function( tabpanel, tab ){
									if(tab.id=='followhistoryInnerTab')
										if(document.getElementById('follow_history_grid')) followstore.load();
									else if(tab.id=='followsheduleInnerTab')
										if(document.getElementById('follow_schedule_grid')) followstore_schedule.load();
								}
							},
							items:[{
								title: 'History',
								id: 'followhistoryInnerTab',
								autoLoad: {
									url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', 
									scripts	: true, 
									params	:{
										parcelid 	: record.get('pid'), 
										userid 		: record.get('userid'), 
										county 		: record.get('county') 
									}, 
									timeout	: 10800
								},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
								//,tbar: tbarhistory
							},{
								title: 'Schedule',
								id: 'followsheduleInnerTab',
								autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
								//,tbar: tbarschedule
							},{
								title: 'Email',
								id: 'followemailInnerTab',
								autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followemail.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
							}]
						})
					]
					//,tbar: followTbar
				}).show();
			}
		}
	});
	
	storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
</script>