<?php	
	//$docsDir = 'F:/ReiFaxDocs/';
	$docsDir = 'C:/inetpub/wwwroot/';
	
	function completeTaskFollow($userid,$idinsert,$pid,$task=5,$followtype='B',$contract=1,$pof=1,$emd=1,$adem=1,$offer=0){
		$userid_follow = isset($_COOKIE['datos_usr']['USERID']) ? $_COOKIE['datos_usr']['USERID'] : $userid;
		$query='SELECT s.*, concat(x.name," ",x.surname) as name_follow  
			FROM xima.followup_schedule s left join xima.ximausrs x on s.userid_follow=x.userid
			WHERE s.parcelid="'.$pid.'" AND s.userid='.$userid.' 
			ORDER BY s.odate desc';
		$result=mysql_query($query) or die($query.mysql_error());
		/*while($r=mysql_fetch_array($result)){
			$tassch = $r['task'];
			if($tassch==$task){ 				
				$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$r['idfus'].')';
				mysql_query($query) or die($query.mysql_error());
			}
		}*/
	}
	
	function scheduleTaskFollow($userid,$parcelid,$userid_follow,$task=5,$detail='',$status='PENDING',$typeFollow='B',$typeExec=1,$subject='',$body='',$idtemplate=-1,$complete='',$attach=array()){		
		$query='INSERT INTO xima.followup_schedule (parcelid,userid,sdate,odate,ohour,task,detail,userid_follow,
		follow_type, status,typeExec,subject,body,idtemplate,complete_detail)
		VALUES ("'.$parcelid.'",'.$userid.', NOW(),NOW(),NOW(),"'.$task.'","'.$detail.'",'.$userid_follow.',
		"'.$typeFollow.'","'.$status.'",'.$typeExec.',"'.$subject.'","'.$body.'",'.$idtemplate.',"'.$complete.'")';
		mysql_query($query) or die($query.mysql_error());
		$idinsert = mysql_insert_id();
		
		if(count($attach)>0){
			$con=0;
			$query='INSERT INTO xima.followup_schedule_attach (idfus,dir,url) VALUES ';
			foreach($attach as $k => $v){
				if($con>0) {$query.=', ';}
				$query.='('.$idinsert.',"'.$v['dir'].'","'.$v['url'].'")';
				$con++;
			}
			mysql_query($query) or die($query.mysql_error());
		}
	}
	
	function mailDeliveryFailure($content,$userid){
		$regex = '/[A-Za-z0-9_-]+@[A-Za-z0-9_-]+\.([A-Za-z0-9_-][A-Za-z0-9_]+)/';
		$emails = array();
		
		preg_match_all($regex, $content,$emails);
		$email = $emails[0][0];
		
		$query='SELECT f.`idmail`, f.`msg_date`, 
		IF(fe.parcelid is null OR fu.address is null,
			IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1),
			fe.parcelid) ap, 
		f.`task`, fa.agent, fu.address, fu.type, fu.userid_follow 
		FROM xima.follow_emails f
		LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
		LEFT JOIN xima.followagent fa ON (f.userid=fa.userid AND fe.agentid=fa.agentid)
		LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
		WHERE f.`userid`='.$userid.' and f.`type`=1 AND f.to_msg="'.$email.'"
		ORDER BY msg_date DESC
		LIMIT 1';
		$result = mysql_query($query) or die($query.mysql_error());
		
		if(mysql_num_rows($result)>0){
			$r = mysql_fetch_array($result);
			$parcelid 		= $r['ap'];
			$odate    		= $r['msg_date'];
			$task			= $r['task'];
			$typeFollow		= ($r['type']=='S' || $r['type']=='LS') ? 'S' : 'B';
			$userid_follow	= abs($r['userid_follow']) > 0 ? $r['userid_follow'] : $_COOKIE['datos_usr']['USERID'];
			
			
			if($parcelid!=0 && $parcelid!=-1){						
				$query='DELETE FROM xima.followup_history
				WHERE userid='.$userid.' AND parcelid="'.$parcelid.'" AND follow_type="'.$typeFollow.'" 
				AND task='.$task.' AND odate like "'.substr($odate,0,-2).'%"
				ORDER BY odate DESC LIMIT 1';
				//echo $query;
				mysql_query($query);
				
				scheduleTaskFollow($userid,$parcelid,$userid_follow,$task,'This email '.$email.' does not exist.','ERROR: Mail delivery failure.',$typeFollow);
			}
		}
	}
	
	function assigmentProperty($userid,$idinsert,$pid,$task=5,$followtype='B',$contract=1,$pof=1,$emd=1,$adem=1,$offer=0,$county=''){
		//Insert assigment Email
		insertEmailAssingment($userid,$idinsert,$pid);
		$tabs = $followtype=='B' ? 'tabsFollow' : 'tabsFollowSelling';
		// agregado por Luis R Castro Ticket 12746 16/07/2015 --,0,0,0,'.$Fow.'
		$Fow='Follow';
		if($task==0){
			$detail='Contract and Documents Sent By Bulk Contract. ';
			$detail.='<a href=\"javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$pid.'\','.$tabs.',0,0,0,\''.$Fow.'\');\" >View Content.</a>';
			$task=7;
		}else{
			$detail='<a href=\"javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$pid.'\','.$tabs.',0,0,0,\''.$Fow.'\');\" >View Content.</a>';
		}
		
		$query='SELECT * FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$pid.'"';
		$result = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($result) <= 0){
			if(!empty($county)){
				conectarPorNameCounty($county);

				$query="SELECT c.address, c.unit, c.city, c.ozip as zip, IF(r.agent is null,'',r.agent) as agent, r.mlnumber, c.astatus as status, c.xcode,
					if(length(r.lprice)>0,r.lprice,0) as lprice
					FROM psummary c  
					LEFT JOIN mlsresidential r ON(c.parcelid=r.parcelid)  
					WHERE c.parcelid ='".$pid."'";	 
					
				$result=mysql_query($query) or die($query.mysql_error());
				
				conectar();

				$r=mysql_fetch_array($result);
				
				$query='INSERT INTO xima.followup (parcelid,userid,bd,address,unit,city,zip,agent,mlnumber,status,followdate,lprice,type,xcode,offer) 
					VALUES 
					("'.$pid.'",'.$userid.',"'.$county.'","'.mysql_real_escape_string($r['address']).'","'.$r['unit'].'","'.mysql_real_escape_string($r['city']).'","'.$r['zip'].'","'.mysql_real_escape_string($r['agent']).'","'.$r['mlnumber'].'","'.$r['status'].'",now(),'.$r['lprice'].',"B","'.$r['xcode'].'","'.$offer.'")';
				mysql_query($query) or die($query.mysql_error());
				
				$queryM="update `xima`.`followup` f 
					SET f.lprice=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0 or f.offer>0,f.lprice,(select lprice FROM mlsresidential WHERE parcelid=f.parcelid)),
					f.dom=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0,f.dom,(select dom FROM mlsresidential WHERE parcelid=f.parcelid)),
					f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid) is null,IF(length(f.mlnumber)>0,'NA','NF'),(Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid))),
					f.marketvalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.marketvalue,(select marketvalue FROM marketvalue WHERE parcelid=f.parcelid)),
					f.activevalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.activevalue,(select OffertValue FROM marketvalue WHERE parcelid=f.parcelid)),
					f.pendes=IF((select count(*) from pendes p where p.parcelid=f.parcelid)=0,'N',(select pof from pendes p where p.parcelid=f.parcelid))  
					WHERE f.userid=$userid and f.parcelid='$pid'";
				mysql_query($queryM);
				$queryM="update `xima`.`followup` f 
					SET f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((SELECT date(p.saledate) FROM psummary p WHERE p.parcelid=f.parcelid AND p.saleprice>10000)>f.followdate,'S',f.status))
					WHERE f.userid=$userid and f.parcelid='$pid'";
				mysql_query($queryM); 
			}
		}

		$query='SELECT * FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$pid.'"';
		$result = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($result) > 0){
			//Delete Exact History on other property.<br>
			$query='DELETE FROM xima.followup_history 
			WHERE detail="'.$detail.'" AND userid='.$userid;
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			
			//Insert Follow History
			$query='INSERT INTO xima.followup_history 
			(parcelid, userid, odate, offer, coffer, task, contract, pof, emd, 
			realtorsadem, offerreceived, detail, userid_follow, follow_type)
			VALUES ("'.$pid.'",'.$userid.',NOW(),'.$offer.',0,'.$task.','.$contract.','.$pof.','.$emd.','.$adem.',1,
			"'.$detail.'",
			'.$userid.',\''.$followtype.'\')';
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		}
	}
	
	function assingmentEmail($userid,$idmail,$from,$sub,$insertProperty=true){
		//Check phone in from email and subject of mail
		$regex = '/\(\d+\) \d+-\d+|\s\d+\s|\+\d+|\d+@|\d+.\d+/i';
		preg_match_all($regex,$from,$phoneFrom);
		preg_match_all($regex,$sub,$phoneSub);
		
		//Check phone in From Email
		$phone = preg_replace('/\D/','',$phoneFrom[0][0]);
		//phone from domain txt.voice.google.com
		if(strlen($phone)==22) $phone = substr($phone,12,10);
		//phone with +1
		if(strlen($phone)>=11) $phone = substr($phone,1,10);
		//its not any phone
		if(strlen($phone)!=10) $phone = 0;
		
		//Check phone in Subject if not in From Email
		if($phone==0){
			$phone = preg_replace('/\D/','',$phoneSub[0][0]);
			//phone from domain txt.voice.google.com
			if(strlen($phone)==22) $phone = substr($phone,12,10);
			//phone with +1
			if(strlen($phone)>=11) $phone = substr($phone,1,10);
			//its not any phone
			if(strlen($phone)!=10) $phone = 0;
		}
		
		//Check for agent email with from
		$query="SELECT agentid FROM xima.followagent
		WHERE userid=$userid AND (email='$from' OR email2='$from' OR email3='$from' OR 
		email4='$from' OR email5='$from')";
		$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($resAE)>=1){
			$rAE = mysql_fetch_array($resAE);
			
			insertEmailContact($userid,$idmail,$rAE[0],$insertProperty);
			
			return true;
		}elseif($phone!=0){
			//Check for agent phone
			$query="SELECT agentid 
			FROM `xima`.`followagent` 
			WHERE userid=$userid AND
			(concat_ws(',',
			trim(replace(replace(replace(replace(phone1,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone2,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone3,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone4,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone5,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone6,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(fax,'-',''),')',''),'(',''),' ',''))) 
			like '%$phone%') = 1";
			$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			
			if(mysql_num_rows($resAE)>=1){
				$rAE = mysql_fetch_array($resAE);
				
				insertEmailContact($userid,$idmail,$rAE[0],$insertProperty);
				
				return true;
			}
		}

		return false;
	}
	
	function insertEmailContact($userid,$idmail,$agentid,$insertProperty=true){
		$query="SELECT * FROM xima.follow_emails_assigment WHERE idmail=$idmail AND userid=$userid";
		$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($resAE)==0){
			$query="SELECT * 
			FROM `xima`.`follow_assignment` fa
			INNER JOIN xima.followup f ON (fa.userid=f.userid AND f.parcelid=fa.parcelid)
			WHERE type <> 'M' AND fa.userid=$userid AND fa.agentid=$agentid";
			$resAP = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			$numAP = mysql_num_rows($resAP);
			
			if($numAP>0){
				if($numAP==1){
					$r = mysql_fetch_array($resAP);
					$parcelid = $r['parcelid'];
					
					//Insert assigment Email
					$query="INSERT INTO xima.follow_emails_assigment 
					(idmail,userid,agentid) VALUES 
					($idmail,$userid,$agentid)";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
					//Only assign property if not have #MSG-ID#.
					if($insertProperty){
						//Find task of property
						$query="SELECT * FROM xima.follow_emails WHERE userid=$userid and idmail=$idmail";
						$rest = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
						$r = mysql_fetch_array($rest);
						$task = $r['task'];
						
						//Find type of property
						$query="SELECT * FROM xima.followup WHERE userid=$userid and parcelid='$parcelid'
						AND (type <> 'M')";
						$rest = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
						$r = mysql_fetch_array($rest);
						$typefollow = $r['type']=='S' || $r['type']=='LS' ? 'S' : 'B';
	
						assigmentProperty($userid,$idmail,$parcelid,$task,$typefollow);
					}
				}else{
					//Insert assigment Email
					$query="INSERT INTO xima.follow_emails_assigment 
					(idmail,userid,agentid) VALUES 
					($idmail,$userid,$agentid)";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
				}
			}else{
				//Insert assigment Email
				$query="INSERT INTO xima.follow_emails_assigment 
				(idmail,userid,agentid) VALUES 
				($idmail,$userid,$agentid)";
				mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			}
		}else{
			//Update assigment Email
			$query="UPDATE xima.follow_emails_assigment 
			SET agentid='$agentid' WHERE idmail=$idmail AND userid=$userid";
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			
			//Assing contact to follow up when contact and property exist in assingment.
			$r = mysql_fetch_array($resAE);
			$pid = $r['parcelid'];
			verifyEmailAssingment($userid,$pid,$agentid);
		}
	}
	
	function insertEmailAssingment($userid,$idmail,$pid){
		$query="SELECT * FROM xima.follow_emails_assigment WHERE idmail=$idmail AND userid=$userid";
		$resAE = mysql_query($query);
		
		if(mysql_num_rows($resAE)==0){
			//Insert assigment Email
			$query="INSERT INTO xima.follow_emails_assigment 
			(idmail,userid,parcelid) VALUES 
			($idmail,$userid,'$pid')";
			mysql_query($query);
		}else{
			//Update assigment Email
			$query="UPDATE xima.follow_emails_assigment 
			SET parcelid='$pid' WHERE idmail=$idmail AND userid=$userid";
			mysql_query($query);
			
			//Assign contact to follow up when contact exist in property assign.
			$r = mysql_fetch_array($resAE);
			$agentid = $r['agentid'];
			verifyEmailAssingment($userid,$pid,$agentid);
		}
	}
	
	function verifyEmailAssingment($userid,$pid=NULL,$agentid=NULL){
		if(strlen($agentid)>0 && strlen($pid)>0){
			$query="SELECT * FROM xima.follow_assignment WHERE userid=$userid AND parcelid='$pid' AND agentid=$agentid";
			$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			
			if(mysql_num_rows($resAE) == 0){
				$query="INSERT INTO xima.follow_assignment (parcelid,userid,agentid)
				VALUES ('$pid',$userid,$agentid)";
				mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			}
		}
	}
	
	function checkPendingContact($userid,$typeEmail){
		$query="SELECT count(*)
		FROM xima.follow_emails f
		LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
		WHERE f.`userid`=$userid and f.`type`=$typeEmail
		AND fe.agentid is null AND f.seen=0";
		$result = mysql_query($query) or die($query.mysql_error());
		$r = mysql_fetch_array($result);
		return $r[0];
	}
	
	function checkPendingProperty($userid,$typeEmail){
		$query="SELECT count(*)
		FROM xima.follow_emails f
		LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
		LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
		WHERE f.`userid`=$userid and f.`type`=$typeEmail
		AND fe.agentid is not null AND (fe.parcelid is null OR fu.address is null) AND IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1)=0";
		$result = mysql_query($query) or die($query.mysql_error());
		$r = mysql_fetch_array($result);
		return $r[0];
	}
	
	function checkPendingEmail($userid,$typeEmail){
		$query='SELECT * FROM xima.contracts_mailsettings WHERE userid='.$userid;
		$result = mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			
			if(strlen($r['imap_server'])>0){
				$user=$r['imap_username'];
				$pass=$r['imap_password'];
				$server=$r['imap_server'];
				$puerto=$r['imap_port'];
				$fTimeSinc=false;
				
				$rSearch=array('"',"'");
				$rReplace=array('','');
				
				if($r['imap_type']==1){ 
					$protocol='imap';
					$auth=false;
				}elseif($r['imap_type']==2){
					$protocol='pop3';
					$auth=false;
				}elseif($r['imap_type']==3){
					$protocol='imap';
					$auth=true;
				}elseif($r['imap_type']==4){
					$protocol='pop3';
					$auth=true;
				}
				
				$query="SELECT date(msg_date) lastdateupdate 
				FROM xima.follow_emails 
				WHERE userid=$userid and type=0 and date(msg_date) <= date(now())  
				ORDER BY msg_date desc limit 2";
				
				$result = mysql_query($query) or die($query.mysql_error());
				if(mysql_num_rows($result)>0){
					$lastdateupdate='2050-01-01';
					while($r=mysql_fetch_array($result)){						
					
						$lastdateupdate1=$r['lastdateupdate'];
						
						$lastdateupdate = $lastdateupdate1 <= $lastdateupdate ? $lastdateupdate1 : $lastdateupdate;
					}
				}else{
					$timestamp = strtotime('-1 month');
					$lastdateupdate=date('Y-m-d', $timestamp);
				}
				
				$carpeta = $typeEmail == 0 ? 'INBOX' : 'SENT';
				// Creating a object of reciveMail Class
				$obj = new receiveMail($user,$pass,$user,$server,$protocol,$puerto,$auth,$carpeta); 
				//Connect to the Mail Box
				$conE = $obj->connect();
				if($conE!==true){//If connection fails give error message and exit
					echo json_encode(array('success' => false, 'msg' => $conE));
					return false;
				}
				// Get total list of mails in order.
				$mails = $obj->getCountHeaderMail($fTimeSinc,$carpeta,$lastdateupdate);
				
				//Close connection.
				$obj->close_mailbox();
				
				if($mails !== false){ 
					$query='SELECT count(*) 
					FROM xima.follow_emails_read 
					WHERE userid='.$userid.' AND date(msg_date)="'.$lastdateupdate.'" AND `status` != 0';
					$result = mysql_query($query);
					
					if($result===false)  return mysql_error();
					
					$readedMails = 0;
					if(mysql_num_rows($result)>0){
						$r=mysql_fetch_array($result);
						$readedMails = intval($r[0]);
					}
					
					return intval(count($mails) - $readedMails);
				}else return 0;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	
	function composeEmail($userid, $mailID=NULL, $pid, $to, $sub, $msg, $task=5, $cc=false, $typeFollow='B', $complete = true, $attachEmail = array(),$offer=0,$contract=1,$pof=1,$emd=1,$rademdums=1,$county=''){
		global $docsDir;
		// Agregado por Luis R Castro Ticket 12762 23/07/2015
		$docsDir = 'C:/inetpub/wwwroot/';
		$dir = $docsDir."MailAttach/$userid/";
		
		$query='SELECT m.*,m.username AS email,x.name, x.surname 
		FROM xima.contracts_mailsettings m
		INNER JOIN xima.ximausrs x ON (m.userid=x.userid)
		WHERE m.userid='.$userid;
		$result = mysql_query($query);
		if($result===false){
			return json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error()));
		}
		
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			
			if(strlen($r['server'])>0){
				$user=$r['username'];
				$pass=$r['password'];
				$server=$r['server'];
				$puerto=$r['port'];
				$from = $r['email'];
				$name = $r['name'].' '.$r['surname'];
	
				$email 				= new PHPMailer();
				$email->PluginDir	= $_SERVER['DOCUMENT_ROOT'].'/mailer/';
				$email->Mailer 		= "smtp";
				$email->Host 		= $server;
				$email->Port 		= $puerto;
				$email->SMTPAuth 	= true;
				
				//if($userid==1690) $email->SMTPDebug	= 2;
				
				$email->Username 	= $user;
				$email->Password 	= $pass;
				
				foreach($attachEmail as $k => $file)
					$email->AddAttachment($dir.basename($file));
				
				
				$email->From = $from;
				$email->FromName = $name;
				$email->AddReplyTo($from, $name);
				$email->AddAddress($to);
				
				
				//SEND CC for user email
				if($cc && $to!=$from)
					$email->AddAddress($from);

				$rSearch=array('"',"'");
				$rReplace=array('','');
				
				$email->Subject = str_replace($rSearch,$rReplace,$sub);
				
				$attachSend = count($attachEmail)>0 ? 1 : 0;
				
				//Insert Email
				$query="INSERT INTO  xima.follow_emails 
				(userid, msg_id, from_msg,fromName_msg, to_msg, subject, msg_date, type, attachments,task,seen) 
				VALUES($userid,'".sha1($userid.$sub.date())."','".mysql_real_escape_string(str_replace($rSearch,$rReplace,$from))."',
				'".mysql_real_escape_string(str_replace($rSearch,$rReplace,$name))."', '".str_replace($rSearch,$rReplace,$to)."', 
				'".mysql_real_escape_string(str_replace($rSearch,$rReplace,$sub))."', NOW(), 1, $attachSend, $task, 1)";		
					 
				if(mysql_query($query)===false){
					return json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error()));
				}
				$idinsert = mysql_insert_id();

				//Insert body part
				$query="INSERT INTO  xima.follow_emails_body
				(idmail,body) VALUES
				($idinsert,'".mysql_real_escape_string(str_replace($rSearch,$rReplace,$msg))."')";
				if(mysql_query($query)===false){
					return json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error()));
				}

				//Insert attach part
				foreach($attachEmail as $k => $file){
					$filename = mysql_real_escape_string(str_replace($rSearch,$rReplace,basename($file)));
					
					$query="INSERT INTO  xima.follow_emails_attach
					(idmail,filename) VALUES
					($idinsert,'$filename')"; 
					if(mysql_query($query)===false){
						return json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error()));
					}
				}

				$email->Body = $msg.'<br ><h6 style="color:#FFF;">#MSG-ID#='.$idinsert.';</h6>';
				$email->Body = preg_replace('/[\x00-\x1F\x80-\xFF]/',"",str_replace(array("\\n","\n","\\r","\r","\'"),array('<br>','<br>','','',"'"),$email->Body));
				
				$email->IsHTML(true);

				if($email->Send()){
					if($pid != '-1' && $complete){
						assigmentProperty($userid,$idinsert,$pid,$task,$typeFollow,$contract,$pof,$emd,$rademdums,$offer);
					}
					elseif($complete===false) insertEmailAssingment($userid,$idinsert,$pid);
											
					return json_encode(array('success' => true, 'msg' => '', 'id' => $idinsert));
				}else{
					$query="DELETE FROM xima.follow_emails WHERE idmail=$idinsert AND userid=$userid";
					mysql_query($query);
					return json_encode(array('success' => false, 'error' => $email->ErrorInfo, 'msg'=>$email->ErrorInfo));
				}
			}else
				return json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
		}else
			return json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
	}
	
	function sendSMSGoogleVoice($userid,$parcelid,$username,$password,$agentcell,$agentname,$fromMail,$body,$typeFollow='B',$offer=0.00,$completetask='false'){
		include_once 'class.googlevoice.php';
		
		$gv = new GoogleVoice($username, $password);
		$gv->sms($agentcell,$body);
		//return;
		
		$sub = 'SMS sent to '.$agentname.' (+'.$agentcell.')';
		
		$rSearch=array('"',"'");
		$rReplace=array('','');	
		
		//Insert Email
		$query="INSERT INTO  xima.follow_emails 
		(userid, msg_id, from_msg,fromName_msg, to_msg, subject, msg_date, type, attachments,task) 
		VALUES($userid,'".sha1($userid.$sub.time())."','".str_replace($rSearch,$rReplace,$fromMail)."',
		'".str_replace($rSearch,$rReplace,$agentname)."', '".str_replace($rSearch,$rReplace,$agentcell)."', 
		'".str_replace($rSearch,$rReplace,$sub)."', NOW(), 1, 0, 1)";		
			 
		if(!mysql_query($query)) return false; 
		$idinsert = mysql_insert_id();
		
		//Insert body part
		$query="INSERT INTO  xima.follow_emails_body
		(idmail,body) VALUES
		($idinsert,'$body')";
		if(!mysql_query($query)) return false; 

		assigmentProperty($userid,$idinsert,$parcelid,1,$typeFollow,1,1,1,1,$offer);
		if($completetask=='true'){
			completeTaskFollow($userid,0,$parcelid,1,'',0,1,1,1,0);
		}
		
		return $idinsert; 
	}
	
	function sendFAX($userid,$parcelid,$idfus,$body,$agentfax,$typeFollow='B',$completetask='false'){
		//turn var body to pdf and join all pdf in one.
		$rutapdftk = $_SERVER['DOCUMENT_ROOT'].'/overview_contract';
		$ruta = getCwd().'/myfollowupload/';
		$archivo = $ruta.'template.pdf';
		$dompdf = new DOMPDF();
		
		$dompdf->load_html($body);
		$dompdf->render();
		
		$pdfoutput = $dompdf->output();
		$filename = $pdfoutput;
		
		$fp = fopen($archivo, "a");
		fwrite($fp, $pdfoutput);
		fclose($fp); 
		
		
		$docsDir = 'F:/ReiFaxDocs/';
		$dir = $docsDir."MailAttach/$userid/";
		//Crear Directorio de Attachment
		if(!is_dir($dir)){
			mkdir($dir);
		}
		
		$attach = scheduleAttach($idfus,$userid);
		$pdfs=' ';
		if(count($attach)>0){
			foreach($attach as $k => $val){
				@copy($dir.$val,$ruta.(str_replace(' ','',$val)));
				$pdfs .= $ruta.(str_replace(' ','',$val)).' ';
			}
		}
		$fecha= date('YmdHms');
		$attachpdf=$dir.'fax_'.sha1($fecha.$userid.$parcelid).'.pdf';

		passthru("{$rutapdftk}/pdftk/pdftk {$archivo} {$pdfs} cat output {$attachpdf}");
		//$_POST['file7'] = $attachpdf;
		
		$query    = "SELECT * 
		FROM xima.contracts_mailsettings m
		inner join xima.contracts_phonesettings p on m.userid=p.userid
		WHERE m.userid = $userid";
		$rs       = mysql_query($query);
		$mailInfo = mysql_fetch_array($rs);
		$sendTo = strtolower($agentfax.'@'.$mailInfo['fax_domain']);
		
		if(strlen(trim($mailInfo['fax_domain']))==0) return false;
		
		$content = composeEmail($userid, NULL, $parcelid, $sendTo, 'Sent FAX to '.$sendTo.' ('.$agentfax.')', '', '3', false, $typeFollow, false, array($attachpdf));
		
		$respuesta=json_decode($content,true);
		if($respuesta['msg']==''){
			$enviadoagente='1';
			if($completetask=='true'){
				completeTaskFollow($userid,0,$parcelid,3,'',0,1,1,1,0);	
			}
			return $respuesta['id'];
		}else{
			$enviadoagente='0';
			//Saved error in pending task.
			$query='UPDATE xima.followup_schedule 
			SET status="ERROR: '.$respuesta['msg'].'" 
			WHERE idfus='.$idfus;
			mysql_query($query);
			return false;
		}
	}
	
	function sendEMAIL($userid,$parcelid,$idfus,$subject,$body,$remail,$typeFollow='B',$completetask='false'){
		$content = composeEmail($userid, NULL, $parcelid, $remail, $subject, $body, '5', false, $typeFollow, false, scheduleAttach($idfus,$userid));
		
		$respuesta=json_decode($content,true);		
		if($respuesta['msg']==''){
			$enviadoagente='1';
			if($completetask=='true'){
				completeTaskFollow($userid,0,$parcelid,5,'',0,1,1,1,0);	
			}
			return $respuesta['id'];
		}else{
			$enviadoagente='0';
			//Saved error in pending task.
			$query='UPDATE xima.followup_schedule 
			SET status="ERROR: '.$respuesta['msg'].'" 
			WHERE idfus='.$idfus;
			mysql_query($query);
			return false;
		}
	}
	
	function sendDOCS($userid,$parcelid,$idfus,$subject,$body,$remail,$agentfax,$docsType,$typeFollow='B',$completetask='false',$POSTContract=''){		
		if(verifySendMails($userid,$parcelid)){
			$p = json_decode($POSTContract);
			
			foreach($p as $k => $val){
				$_POST[$k] = $val;
			}
			$_POST['subject'] = $subject;
			$_POST['body'] = $body;
			
			ob_start();
				include($_SERVER['DOCUMENT_ROOT']."/mysetting_tabs/myfollowup_tabs/properties_followgetcontractJ.php");
				$content = ob_get_contents();
			ob_end_clean();
			
			$respuesta=json_decode($content,true);
			
			if($respuesta['success']==true){
				return true;
			}else
				return false;
		}else{
			//Saved error in pending task.
			$query='UPDATE xima.followup_schedule 
			SET status="ERROR: You have reached the allowed quantity per day.\nPlease send again tomorrow.", statusExec=0 
			WHERE idfus='.$idfus;
			mysql_query($query);
			
			return false;
		}
	}
	
	function scheduleAttach($idfus,$userid){
		global $docsDir;
		
		$query='SELECT dir 
		FROM xima.followup_schedule_attach 
		WHERE idfus='.$idfus;
		$result = mysql_query($query);
		
		$attach = array();		
		if(mysql_num_rows($result)>0){
			
			$dir = $docsDir."MailAttach/$userid/";
			//Crear Directorio de Attachment
			if(!is_dir($dir)){
				mkdir($dir);
			}
			
			while($r=mysql_fetch_assoc($result)){
				$aux = basename($r['dir']);
				$attach[] = $aux;
				
				if(!file_exists($dir.$aux)){
					@copy($r['dir'],$dir.$aux);
				}
			}
		}
		
		return $attach;
	}
	
	function checkfollowagent($userid, $pids, $task){
		$contacts = array();
		$q="SELECT f.parcelid, fo.offer, a.* 
		FROM xima.follow_assignment f
		INNER JOIN xima.followagent a ON (f.userid=a.userid AND f.agentid=a.agentid)
		INNER JOIN xima.followup fo ON (f.userid=fo.userid AND f.parcelid=fo.parcelid)
		WHERE f.userid=$userid AND f.parcelid in ('".implode("','",$pids)."') AND f.principal=1";
		$result = mysql_query($q) or die($q.mysql_error());
		
		switch(intval($task)){
			case 1: 
				//contact Phone				
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if( ($r['typeph1']==2 && strlen($r['phone1']) > 0)
						 || ($r['typeph2']==2 && strlen($r['phone2']) > 0)
						 || ($r['typeph3']==2 && strlen($r['phone3']) > 0)
						 || ($r['typeph4']==2 && strlen($r['phone4']) > 0)
						 || ($r['typeph5']==2 && strlen($r['phone5']) > 0)
						 || ($r['typeph6']==2 && strlen($r['phone6']) > 0))
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Phone not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
			break;
			
			case 3: 
				//contact fax
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if( (($r['typeph1']==3 || $r['typeph1']==6) && strlen($r['phone1']) > 0)
						 || (($r['typeph2']==3 || $r['typeph2']==6) && strlen($r['phone2']) > 0)
						 || (($r['typeph3']==3 || $r['typeph3']==6) && strlen($r['phone3']) > 0)
						 || (($r['typeph4']==3 || $r['typeph4']==6) && strlen($r['phone4']) > 0)
						 || (($r['typeph5']==3 || $r['typeph5']==6) && strlen($r['phone5']) > 0)
						 || (($r['typeph6']==3 || $r['typeph6']==6) && strlen($r['phone6']) > 0)
						 || (strlen($r['fax']) > 0))
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Fax not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
			break;

			case 5: 
				//contact email	
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if(strlen($r['email']) > 0 
						|| strlen($r['email2']) > 0
						|| strlen($r['email3']) > 0
						|| strlen($r['email4']) > 0
						|| strlen($r['email5']) > 0)
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Email not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
				
			break;
			
			case 7: 
				//contact email, phone and price
				while($r=mysql_fetch_array($result)){
					if($r['offer'] > 0){
						if($r['agentid'] > 0 && $r['agentid'] != NULL ){
							$status = '';
							if( (($r['typeph1']==3 || $r['typeph1']==6) && strlen($r['phone1']) > 0)
							 || (($r['typeph2']==3 || $r['typeph2']==6) && strlen($r['phone2']) > 0)
							 || (($r['typeph3']==3 || $r['typeph3']==6) && strlen($r['phone3']) > 0)
							 || (($r['typeph4']==3 || $r['typeph4']==6) && strlen($r['phone4']) > 0)
							 || (($r['typeph5']==3 || $r['typeph5']==6) && strlen($r['phone5']) > 0)
							 || (($r['typeph6']==3 || $r['typeph6']==6) && strlen($r['phone6']) > 0)
							 || (strlen($r['fax']) > 0))
								$status = 'PENDING';
							else
								$status = 'ERROR: Fax not found.';
								
							if(strlen($r['email']) > 0 
							|| strlen($r['email2']) > 0
							|| strlen($r['email3']) > 0
							|| strlen($r['email4']) > 0
							|| strlen($r['email5']) > 0)
								$status .= '/PENDING';
							else
								$status .= '/ERROR: Email not found.';
								
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>$status);
						}else{
							//buscar el contacto por kristy
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
						}
					}else{
						return false;
					}
				}
			break;

			default:
				foreach($pids as $k => $val){
					$contacts[] = array('pid'=>$val, 'status'=>'');
				}
			break;
		}
		
		return $contacts;
		
	}
	
	function checktemplateagent($userid, $task){
		$contacts = array();
		$q="SELECT a.* 
		FROM xima.followagent a WHERE a.userid=$userid ";
		$result = mysql_query($q) or die($q.mysql_error());
		
		switch(intval($task)){
			case 1: 
				//contact Phone				
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if( ($r['typeph1']==2 && strlen($r['phone1']) > 0)
						 || ($r['typeph2']==2 && strlen($r['phone2']) > 0)
						 || ($r['typeph3']==2 && strlen($r['phone3']) > 0)
						 || ($r['typeph4']==2 && strlen($r['phone4']) > 0)
						 || ($r['typeph5']==2 && strlen($r['phone5']) > 0)
						 || ($r['typeph6']==2 && strlen($r['phone6']) > 0))
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Phone not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
			break;
			
			case 3: 
				//contact fax
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if( (($r['typeph1']==3 || $r['typeph1']==6) && strlen($r['phone1']) > 0)
						 || (($r['typeph2']==3 || $r['typeph2']==6) && strlen($r['phone2']) > 0)
						 || (($r['typeph3']==3 || $r['typeph3']==6) && strlen($r['phone3']) > 0)
						 || (($r['typeph4']==3 || $r['typeph4']==6) && strlen($r['phone4']) > 0)
						 || (($r['typeph5']==3 || $r['typeph5']==6) && strlen($r['phone5']) > 0)
						 || (($r['typeph6']==3 || $r['typeph6']==6) && strlen($r['phone6']) > 0)
						 || (strlen($r['fax']) > 0))
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Fax not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
			break;

			case 5: 
				//contact email	
				while($r=mysql_fetch_array($result)){
					if($r['agentid'] > 0 && $r['agentid'] != NULL){
						if(strlen($r['email']) > 0 
						|| strlen($r['email2']) > 0
						|| strlen($r['email3']) > 0
						|| strlen($r['email4']) > 0
						|| strlen($r['email5']) > 0)
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'');
						else
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Email not found.');
					}else{
						//buscar el contacto por kristy
						$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
					}
				}
				
			break;
			
			case 7: 
				//contact email, phone and price
				while($r=mysql_fetch_array($result)){
					if($r['offer'] > 0){
						if($r['agentid'] > 0 && $r['agentid'] != NULL ){
							$status = '';
							if( (($r['typeph1']==3 || $r['typeph1']==6) && strlen($r['phone1']) > 0)
							 || (($r['typeph2']==3 || $r['typeph2']==6) && strlen($r['phone2']) > 0)
							 || (($r['typeph3']==3 || $r['typeph3']==6) && strlen($r['phone3']) > 0)
							 || (($r['typeph4']==3 || $r['typeph4']==6) && strlen($r['phone4']) > 0)
							 || (($r['typeph5']==3 || $r['typeph5']==6) && strlen($r['phone5']) > 0)
							 || (($r['typeph6']==3 || $r['typeph6']==6) && strlen($r['phone6']) > 0)
							 || (strlen($r['fax']) > 0))
								$status = 'PENDING';
							else
								$status = 'ERROR: Fax not found.';
								
							if(strlen($r['email']) > 0 
							|| strlen($r['email2']) > 0
							|| strlen($r['email3']) > 0
							|| strlen($r['email4']) > 0
							|| strlen($r['email5']) > 0)
								$status .= '/PENDING';
							else
								$status .= '/ERROR: Email not found.';
								
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>$status);
						}else{
							//buscar el contacto por kristy
							$contacts[] = array('pid'=>$r['parcelid'], 'status'=>'ERROR: Contact not found.');
						}
					}else{
						return false;
					}
				}
			break;

			default:
				foreach($pids as $k => $val){
					$contacts[] = array('pid'=>$val, 'status'=>'');
				}
			break;
		}
		
		return $contacts;
		
	}
?>