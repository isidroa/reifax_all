<?php
	include($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	conectar();
	include ($_SERVER['DOCUMENT_ROOT']."/properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	///Actualizacion de lprice y status
	$query="SELECT distinct f.bd FROM `xima`.`followup` f WHERE f.userid=$userid order by f.bd";
	$result=mysql_query($query) or die($query.mysql_error());
	
	while($r=mysql_fetch_array($result)){
		conectarPorNameCounty($r['bd']);
		$queryM="update `xima`.`followup` f 
			SET f.lprice=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0 or f.offer>0,f.lprice,(select lprice FROM mlsresidential WHERE parcelid=f.parcelid)),
			f.dom=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0,f.dom,(select dom FROM mlsresidential WHERE parcelid=f.parcelid)),
			f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid) is null,IF(length(f.mlnumber)>0,'NA','NF'),(Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid))),
			f.marketvalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.marketvalue,(select marketvalue FROM marketvalue WHERE parcelid=f.parcelid)),
			f.activevalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.marketvalue,(select OffertValue FROM marketvalue WHERE parcelid=f.parcelid)),
			f.pendes=IF((select count(*) from pendes p where p.parcelid=f.parcelid)=0,'N',(select pof from pendes p where p.parcelid=f.parcelid))  
			WHERE f.userid=$userid and f.bd='".$r['bd']."'"; 
		mysql_query($queryM);
		
		$queryM="update `xima`.`followup` f 
			SET f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((SELECT date(p.saledate) FROM psummary p WHERE p.parcelid=f.parcelid AND p.saleprice>10000 AND length(p.saledate)=8)>f.followdate,'S',f.status))
			WHERE f.userid=$userid and f.bd='".$r['bd']."'"; 
		mysql_query($queryM);
	}
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mysellingcompletetasks_panel" style="background-color:#FFF;border-color:#FFF">
	<div id="mysellingcompletetasks_data_div" align="center" style=" background-color:#FFF; margin:auto; width:970px;">
  		<div id="mysellingcompletetasks_filters"></div><br />
        <div id="mysellingcompletetasks_properties" align="left"></div> 
	</div>
</div>
<script>
	var useridspider= <?php echo $userid;?>;
	//Progress Bar Variables
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	var limitmyfollowcompleteselling 			= 50;
	var selected_datamyfollowcompleteselling 	= new Array();
	var AllCheckmyfollowcompleteselling 			= false;
	var loadedSCT								= false;
	//filter variables
	var filtersSCT = {
		property		: {
			address		: '',
			mlnumber	: '',
			county		: 'ALL',
			city		: 'ALL',
			zip			: '',
			status		: 'ALL',
			xcode		: 'ALL'
		},
		agent			: '',
		follow			: {
			datec		: 'Equal',
			date		: '',
			dateb		: ''
		},
		task			: {
			datec		: 'Equal',
			date		: '',
			dateb		: '',
			type		: '-1',
			pending		: '-1',
			complete	: '-1'
		},
		contract		: '-1',
		pof				: '-1',
		emd				: '-1',
		ademdums		: '-1',
		msj				: '-1',
		lu				: '-1',
		pe				: '-1',
		history			: '-1',
		offerreceived	: '-1',
		order			: {
			field		: 'address',
			direction	: 'ASC'
		}
	};
	
	var storemyfollowcompleteselling = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000,
			autoAbort: true
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'int'},
			   {name: 'coffer', type: 'int'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'int'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'dom', type: 'int'},
			   {name: 'pendes'},
			   {name: 'idfuh', type: 'int'},
			   {name: 'typeExec', type: 'int'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			typeFollow: 'S',
			'pendingtask': 'no'
		},
		remoteSort: true,
		sortInfo: {
			field: 'ndate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				
				AllCheckmyfollowcompleteselling=false;
				selected_datamyfollowcompleteselling=new Array();
				smmyfollowcompleteselling.deselectRange(0,limitmyfollowcompleteselling);
				obj.params.address=filtersSCT.property.address;
				obj.params.mlnumber=filtersSCT.property.mlnumber;
				obj.params.agent=filtersSCT.agent;
				obj.params.status=filtersSCT.property.status;
				obj.params.ndate=filtersSCT.task.date;
				obj.params.ndateb=filtersSCT.task.dateb;
				obj.params.ndatec=filtersSCT.task.datec;
				obj.params.ntask=filtersSCT.task.type;
				obj.params.contract=filtersSCT.contract;
				obj.params.pof=filtersSCT.pof;
				obj.params.emd=filtersSCT.emd;
				obj.params.ademdums=filtersSCT.ademdums;
				obj.params.msj=filtersSCT.msj;
				obj.params.history=filtersSCT.history;
				obj.params.offerreceived=filtersSCT.offerreceived;
				obj.params.zip=filtersSCT.property.zip;
				obj.params.county=filtersSCT.property.county;
				obj.params.city=filtersSCT.property.city;
				obj.params.xcode=filtersSCT.property.xcode;
			},
			'load' : function (store,data,obj){
				storemyfollowcompletesellingAll.load();
				if (AllCheckmyfollowcompleteselling){
					Ext.get(gridmyfollowcompleteselling.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowcompleteselling=true;
					gridmyfollowcompleteselling.getSelectionModel().selectAll();
					selected_datamyfollowcompleteselling=new Array();
				}else{
					AllCheckmyfollowcompleteselling=false;
					Ext.get(gridmyfollowcompleteselling.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowcompleteselling.length > 0){
						for(val in selected_datamyfollowcompleteselling){
							var ind = gridmyfollowcompleteselling.getStore().find('pid',selected_datamyfollowcompleteselling[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowcompleteselling.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storemyfollowcompletesellingAll = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'float'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'typeExec', type: 'int'},
			   {name: 'name_follow'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			typeFollow: 'S',
			'pendingtask': 'no'
		},
		remoteSort: true,
		sortInfo: {
			field: 'ndate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersSCT.property.address;
				obj.params.mlnumber=filtersSCT.property.mlnumber;
				obj.params.agent=filtersSCT.agent;
				obj.params.status=filtersSCT.property.status;
				obj.params.ndate=filtersSCT.task.date;
				obj.params.ndateb=filtersSCT.task.dateb;
				obj.params.ndatec=filtersSCT.task.datec;
				obj.params.ntask=filtersSCT.task.type;
				obj.params.contract=filtersSCT.contract;
				obj.params.pof=filtersSCT.pof;
				obj.params.emd=filtersSCT.emd;
				obj.params.ademdums=filtersSCT.ademdums;
				obj.params.msj=filtersSCT.msj;
				obj.params.history=filtersSCT.history;
				obj.params.offerreceived=filtersSCT.offerreceived;
				obj.params.zip=filtersSCT.property.zip;
				obj.params.county=filtersSCT.property.county;
				obj.params.city=filtersSCT.property.city;
				obj.params.xcode=filtersSCT.property.xcode;
			}
		}
    });
// Ticket 12681 Agregado por Luis R Castro 17/06/2015
function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/drop-no.png" />'; 
		else  return '<img src="../../img/drop-yes.png" />';
	}//////////////////////////////7
	
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'A': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;			
			
			case 'NA': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF': return '<div title="By Owner" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S': return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/notes/new_msj.png" />';
		else return '';
	}
	 
	function creaMenuTaskC(e,rowIndex){ 
		x = e.clientX;
 		y = e.clientY; 
		var record = gridmyfollowcompleteselling.getStore().getAt(rowIndex);
		var pid = record.get('pid');
		var county = record.get('county');
		var status = record.get('status');
		
		var simple = new Ext.FormPanel({
			url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
			frame: true,
			width: 220,
			waitMsgTarget : 'Waiting...',
			labelWidth: 100,
			defaults: {width: 200},
			labelAlign: 'left',
			items: [
				new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contact',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
								type: 'assignment'
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var r=Ext.decode(response.responseText);
								creaVentana(0,r,pid,useridspider);
							}
						});
					}
				})/*,new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Change Status',
					handler: function(){
						win.close();
						var simple = new Ext.FormPanel({
							url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
							frame: true,
							title: 'Follow Up - Status Change',
							width: 350,
							waitMsgTarget : 'Waiting...',
							labelWidth: 75,
							defaults: {width: 230},
							labelAlign: 'left',
							items: [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										fieldLabel	  : 'Status',
										width		  : 130,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['N','None'],
												['UC','Under Contract'],
												['PS','Pending Sale']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'statusaltname',
										value         : 'N',
										hiddenName    : 'statusalt',
										hiddenValue   : 'N',
										allowBlank    : false
									},{
										xtype     : 'hidden',
										name      : 'pid',
										value     : pid
									},{
										xtype     : 'hidden',
										name      : 'type',
										value     : 'change-status'
									}],
							
							buttons: [{
									text: 'Change',
									handler: function(){
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												winStatus.close();
												Ext.Msg.alert("Follow Up", 'Status Changed.');
												storemyfollowcomplete.load();
											},
											failure: function(form, action) {
												loading_win.hide();
												Ext.Msg.alert("Failure", "ERROR");
											}
										});
									}
								},{
									text: 'Reset',
									handler  : function(){
										simple.getForm().reset();
										winStatus.close();
									}
								}]
							});
						 
						var winStatus = new Ext.Window({
							layout      : 'fit',
							width       : 240,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'close',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									winStatus.close();
									loading_win.hide();
								}
							}]
						});
						winStatus.show();
					}
				})*/,new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Overview',
					handler: function(){
						win.close();
						createOverview(county,pid,status,false,false);
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contract',
					handler: function(){
						win.close();
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Edit History',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
							method: 'POST',
							timeout :600000,
							params: {
								idfuh: record.get('idfuh'), 
								parcelid: pid,
								userid: <?php echo $userid;?>,
								typeFollow: 'S'
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var rsch=Ext.decode(response.responseText);
								var followfuh=rsch.records[0];
								var simple = new Ext.FormPanel({
									url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
									frame: true,
									title: 'Update History Task - '+followfuh.fulladdress,
									width: 490,
									waitMsgTarget : 'Waiting...',
									labelWidth: 100,
									defaults: {width: 350},
									labelAlign: 'left',
									items: [{
												xtype     : 'numberfield',
												name      : 'offer',
												fieldLabel: 'Offer',
												minValue  : 0,
												value	  : followfuh.offer
											},{
												xtype     : 'numberfield',
												name      : 'coffer',
												fieldLabel: 'C. Offer',
												minValue  : 0,
												value	  : followfuh.coffer
											},{
												xtype         : 'combo',
												mode          : 'local',
												fieldLabel    : 'Task',
												triggerAction : 'all',
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['1','Send SMS'],
														['2','Receive SMS'],
														['3','Send FAX'],
														['4','Receive FAX'],
														['5','Send EMAIL'],
														['6','Receive EMAIL'],
														['7','Send DOC'],
														['8','Receive DOC'],
														['9','Make CALL'],
														['10','Receive CALL'],
														['11','Send R. MAIL'],
														['12','Receive R. MAIL'],
														['13','Send OTHER'],
														['14','Receive OTHER'],
														['15','Send VOICE MAIL'],
														['16','Receive VOICE MAIL']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'ftaskname',
												value         : followfuh.task,
												hiddenName    : 'task',
												hiddenValue   : followfuh.task,
												allowBlank    : false
											},{
												xtype: 'button',
												tooltip: 'View Contacts',
												cls:'x-btn-text-icon',
												iconAlign: 'left',
												text: ' ',
												width: 30,
												height: 30,
												scale: 'medium',
												icon: 'http://www.reifax.com/img/agent.png',
												handler: function(){
													loading_win.show();
													Ext.Ajax.request({
														waitMsg: 'Seeking...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															pid: pid,
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															type: 'assignment'
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															loading_win.hide();
															var r=Ext.decode(response.responseText);
															creaVentana(0,r,pid,useridspider);
														}
													});
												}
											},{
												xtype	  :	'textarea',
												height	  : 100,
												name	  : 'sheduledetail',
												fieldLabel: 'Schedule Detail',
												value	  : followfuh.sheduledetail
											},{
												xtype	  :	'textarea',
												height	  : 100,
												name	  : 'detail',
												fieldLabel: 'Complete Detail',
												value	  : followfuh.detail
											},{
												xtype: 'checkboxgroup',
												fieldLabel: 'Document',
												columns: 3,
												itemCls: 'x-check-group-alt',
												items: [
													// Ticket 12681 Agregado por Luis R Castro 17/06/2015
													{boxLabel: 'Contract', name: 'contract', checked: followfuh.contract==1},
													{boxLabel: 'Proof of Funds', name: 'pof', checked: followfuh.pof==1},
													{boxLabel: 'EMD', name: 'emd', checked: followfuh.emd==1},
													{boxLabel: 'Addendums', name: 'rademdums', checked: followfuh.realtorsadem==1},
													{boxLabel: 'Offer Received', name: 'offerreceived', checked: followfuh.offerreceived==1}
												]
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'update'
											},{
												xtype     : 'hidden',
												name      : 'idfuh',
												value     : followfuh.idfuh
											}],
									
									buttons: [{
											text: 'Update',
											handler: function(){
												loading_win.show();
												simple.getForm().submit({
													success: function(form, action) {
														loading_win.hide();
														win.close();
														Ext.Msg.alert("Follow Up", 'Updated Follow History.');
														storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
													},
													failure: function(form, action) {
														loading_win.hide();
														Ext.Msg.alert("Failure", "ERROR");
													}
												});
											}
										},{
											text: 'Reset',
											handler  : function(){
												simple.getForm().reset();
												win.close();
											}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 490,
									height      : 500,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'close',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
							}
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Follow History',
					handler: function(){
						win.close();

						if(document.getElementById('followTab')){
							var tab = tabsFollow.getItem('followTab');
							tabsFollow.remove(tab);
						}
						
						createFollowHistory(tabsFollow, 'followTab', record, pid, 'S', 0, useridspider);
					}
				})
			]
		});
		var win = new Ext.Window({
			layout      : 'fit',
			width       : 230,
			height      : 330,
			modal	 	: true,
			plain       : true,
			items		: simple,
			closeAction : 'close',
			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
					loading_win.hide();
				}
			}]
		});
		win.show();
		return false;
	}
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenuTaskC(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}

	var smmyfollowcompleteselling = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowcompleteselling.indexOf(record.get('pid'))==-1)
					selected_datamyfollowcompleteselling.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowcompleteselling.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowcompleteselling=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowcompleteselling = selected_datamyfollowcompleteselling.remove(record.get('pid'));
				AllCheckmyfollowcompleteselling=false;
				Ext.get(gridmyfollowcompleteselling.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowcompleteselling=new Ext.Toolbar({
		renderTo: 'mysellingcompletetasks_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowcompleteselling.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be eliminated.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcompleteselling==true){
						var numTotales = storemyfollowcompletesellingAll.getCount();
						var totales = storemyfollowcompletesellingAll.getRange(0,numTotales);
						var idfuh='\''+totales[0].data.idfuh+'\'';
						for(i=1;i<numTotales;i++){
							idfuh+=',\''+totales[i].data.idfuh+'\'';	
						}
						
						//Confirmacion para eliminar todos
						Ext.MessageBox.show({
							title:    'Follow Up',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will delete all '+numTotales+' selected tasks from your Completed Task.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteFollow
						});
					}else{	
						var totales = gridmyfollowcompleteselling.getSelectionModel().getSelections();
						var numTotales = totales.length;
						var idfuh='\''+totales[0].data.idfuh+'\'';
						for(i=1; i<numTotales; i++)
							idfuh+=',\''+totales[i].data.idfuh+'\''; 
						
						deleteFollow('yes'); 
					}

					function deleteFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'delete-multi',
								idfuh: idfuh,
								userid: <?php echo $userid;?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
								Ext.Msg.alert("Follow Up Task", 'Tasks delete.');
								
							}                                
						});
					}
				}
			}),new Ext.Button({
				tooltip: 'Edit history',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/update.png',
				handler: function(){
					if(selected_datamyfollowcompleteselling.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the record to be edit.'); return false;
					}else if(selected_datamyfollowcompleteselling.length>1){
						Ext.Msg.alert('Warning', 'You must select(check) only one record to be edit.'); return false;
					}
					loading_win.show();
					Ext.Ajax.request({
						waitMsg: 'Seeking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
						method: 'POST',
						timeout :600000,
						params: {
							idfuh: gridmyfollowcompleteselling.getSelectionModel().getSelections()[0].data.idfuh, 
							parcelid: gridmyfollowcompleteselling.getSelectionModel().getSelections()[0].data.pid,
							userid: <?php echo $userid;?>,
							typeFollow: 'S'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var rsch=Ext.decode(response.responseText);
							var followfuh=rsch.records[0];
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Update History Task - '+followfuh.fulladdress,
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0,
											value	  : followfuh.offer
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0,
											value	  : followfuh.coffer
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER'],
													['15','Send VOICE MAIL'],
													['16','Receive VOICE MAIL'],
													['17','Make NOTE']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfuh.task,
											hiddenName    : 'task',
											hiddenValue   : followfuh.task,
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'View Contacts',
											cls:'x-btn-text-icon',
											iconAlign: 'left',
											text: ' ',
											width: 30,
											height: 30,
											scale: 'medium',
											icon: 'http://www.reifax.com/img/agent.png',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid,useridspider);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'sheduledetail',
											fieldLabel: 'Schedule Detail',
											value	  : followfuh.sheduledetail
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail',
											value	  : followfuh.detail
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 3,
											itemCls: 'x-check-group-alt',
											items: [
												// Ticket 12681 Agregado por Luis R Castro 17/06/2015
													{boxLabel: 'Contract', name: 'contract', checked: followfuh.contract==1},
													{boxLabel: 'Proof of Funds', name: 'pof', checked: followfuh.pof==1},
													{boxLabel: 'EMD', name: 'emd', checked: followfuh.emd==1},
													{boxLabel: 'Addendums', name: 'rademdums', checked: followfuh.realtorsadem==1},
													{boxLabel: 'Offer Received', name: 'offerreceived', checked: followfuh.offerreceived==1}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfuh',
											value     : followfuh.idfuh
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Up", 'Updated Follow History.');
													storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 500,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						}
					});
				}
			}),new Ext.Button({
				tooltip: 'Print Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyfollowcompleteselling.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcompleteselling==true){
						var totales = storemyfollowcompletesellingAll.getRange(0,storemyfollowcompletesellingAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowcompletesellingAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyfollowcompleteselling[0]+'\'';
						for(i=1; i<selected_datamyfollowcompleteselling.length; i++)
							pids+=',\''+selected_datamyfollowcompleteselling[i]+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersSCT.property.address,
							mlnumber: filtersSCT.property.mlnumber,
							agent: filtersSCT.agent,
							status: filtersSCT.property.status,
							ndate: filtersSCT.task.date,
							ndateb: filtersSCT.task.dateb,
							ndatec: filtersSCT.task.datec,
							ntask: filtersSCT.task.type,
							contract: filtersSCT.contract,
							pof: filtersSCT.pof,
							emd: filtersSCT.emd,
							ademdums: filtersSCT.ademdums,
							msj: filtersSCT.msj,
							history: filtersSCT.history,
							sort: filtersSCT.order.field,
							dir: filtersSCT.order.direction,
							pids: pids,
							pendingtask: 'no'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyfollowcompleteselling.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcompleteselling==true){
						var totales = storemyfollowcompletesellingAll.getRange(0,storemyfollowcompletesellingAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowcompletesellingAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyfollowcompleteselling[0]+'\'';
						for(i=1; i<selected_datamyfollowcompleteselling.length; i++)
							pids+=',\''+selected_datamyfollowcompleteselling[i]+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersSCT.property.address,
							mlnumber: filtersSCT.property.mlnumber,
							agent: filtersSCT.agent,
							status: filtersSCT.property.status,
							ndate: filtersSCT.task.date,
							ndateb: filtersSCT.task.dateb,
							ndatec: filtersSCT.task.datec,
							ntask: filtersSCT.task.type,
							contract: filtersSCT.contract,
							pof: filtersSCT.pof,
							emd: filtersSCT.emd,
							ademdums: filtersSCT.ademdums,
							msj: filtersSCT.msj,
							history: filtersSCT.history,
							sort: filtersSCT.order.field,
							dir: filtersSCT.order.direction,
							pids: pids,
							pendingtask: 'no'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmycompleteselling = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						id: 'formmytasks',
						name: 'formmytasks',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersSCT.property.address,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSCT.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fmlnumber',
									value		  : filtersSCT.property.mlnumber,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSCT.property.mlnumber=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-countys',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtersSCT.property.county,
									hiddenName    : 'fcounty',
									hiddenValue   : filtersSCT.property.county,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.property.county = record.get('valor');
											combo.findParentByType('form').getForm().findField('fcity').setValue("ALL");
											filtersSCT.property.city='ALL';
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcity',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'City',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-citys',
											'userid': <?php echo $userid;?>,
											county: filtersSCT.property.county
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcityname',
									value         : filtersSCT.property.city,
									hiddenName    : 'fcity',
									hiddenValue   : filtersSCT.property.city,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.property.city = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('county',filtersSCT.property.county);
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fzip',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Zip Code',
									name		  : 'fzip',
									value		  : filtersSCT.property.zip,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSCT.property.zip=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fxcode',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Property Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-xcodes',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fxcodename',
									value         : filtersSCT.property.xcode,
									hiddenName    : 'fxcode',
									hiddenValue   : filtersSCT.property.xcode,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.property.xcode = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									value		  : filtersSCT.agent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSCT.agent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fntask',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Task',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER'],
											['15','Send VOICE MAIL'],
											['16','Receive VOICE MAIL'],
											['17','Make NOTE']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fntaskname',
									value         : filtersSCT.task.type,
									hiddenName    : 'fntask',
									hiddenValue   : filtersSCT.task.type,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.task.type = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : filtersSCT.property.status,
									hiddenName    : 'fstatus',
									hiddenValue   : filtersSCT.property.status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.property.status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontract',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Contract',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcontractname',
									value         : filtersSCT.contract,
									hiddenName    : 'fcontract',
									hiddenValue   : filtersSCT.contract,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.contract = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fpof',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Proof of Funds',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpofname',
									value         : filtersSCT.pof,
									hiddenName    : 'fpof',
									hiddenValue   : filtersSCT.pof,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.pof = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'femd',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'EMD',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'femdname',
									value         : filtersSCT.emd,
									hiddenName    : 'femd',
									hiddenValue   : filtersSCT.emd,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.emd = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fademdums',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Addendums', 
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fademdumsname',
									value         : filtersSCT.ademdums,
									hiddenName    : 'fademdums',
									hiddenValue   : filtersSCT.ademdums,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.ademdums = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmsj',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Message',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Yes'],
											['0','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fmsjname',
									value         : filtersSCT.msj,
									hiddenName    : 'fmsj',
									hiddenValue   : filtersSCT.msj,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.msj = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fofferreceived',
								colspan: 2,
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Offer Received',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fofferreceived',
									value         : filtersSCT.offerreceived,
									hiddenName    : 'foffer',
									hiddenValue   : filtersSCT.offerreceived,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSCT.offerreceived = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fndate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Completed Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fndatecname',
										hiddenName: 'fndatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersSCT.task.datec,
										listeners: {
											'select': function (combo,record,index){
												filtersSCT.task.datec = record.get('id');
												var secondfield = Ext.getCmp('fndateb');
												secondfield.setValue('');
												filtersSCT.task.dateb='';
												
												if(filtersSCT.task.datec=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fndate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fndate',
											value		  : filtersSCT.task.date,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersSCT.task.date=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fndateb',
										name		  : 'fndateb',
										hidden		  : filtersSCT.task.datec!='Between',
										value		  : filtersSCT.task.dateb,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersSCT.task.dateb=newvalue;
											}
										}
									}]
								}]	
							}]
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								//icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Load&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'load-filters'
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning',output);
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											if(rest.total==0){
												Ext.MessageBox.alert('Warning','You don\'t have default filters saved');
											}else{
												formmycompleteselling.getForm().findField('faddress').setValue(rest.data.address);
												formmycompleteselling.getForm().findField('fmlnumber').setValue(rest.data.mlnumber);
												formmycompleteselling.getForm().findField('fzip').setValue(rest.data.zip);
												formmycompleteselling.getForm().findField('fagent').setValue(rest.data.agent);
												formmycompleteselling.getForm().findField('fstatus').setValue(rest.data.status);
												formmycompleteselling.getForm().findField('fcontract').setValue(rest.data.contract);
												formmycompleteselling.getForm().findField('fpof').setValue(rest.data.pof);
												formmycompleteselling.getForm().findField('femd').setValue(rest.data.emd);
												formmycompleteselling.getForm().findField('fademdums').setValue(rest.data.ademdums);
												formmycompleteselling.getForm().findField('foffer').setValue(rest.data.offerreceived);
												formmycompleteselling.getForm().findField('fndate').setValue(rest.data.ndate);
												formmycompleteselling.getForm().findField('fndateb').setValue(rest.data.ndateb);
												if(rest.data.ntask==-2){
													rest.data.ntask=-1;
												}
												formmycompleteselling.getForm().findField('fntask').setValue(rest.data.ntask);
												formmycompleteselling.getForm().findField('fmsj').setValue(rest.data.msj);
												formmycompleteselling.getForm().findField('fcounty').setValue(rest.data.county);
												formmycompleteselling.getForm().findField('fcity').setValue(rest.data.city);
												formmycompleteselling.getForm().findField('fxcode').setValue(rest.data.xcode);
											}
										}
									});
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/save.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Save&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'save-filters',
											address: formmycompleteselling.getForm().findField('faddress').getValue(),
											mlnumber: formmycompleteselling.getForm().findField('fmlnumber').getValue(),
											agent: formmycompleteselling.getForm().findField('fagent').getValue(),
											status: formmycompleteselling.getForm().findField('fstatus').getValue(),
											contract: formmycompleteselling.getForm().findField('fcontract').getValue(),
											pof: formmycompleteselling.getForm().findField('fpof').getValue(),
											emd: formmycompleteselling.getForm().findField('femd').getValue(),
											ademdums: formmycompleteselling.getForm().findField('fademdums').getValue(),
											msj: formmycompleteselling.getForm().findField('fmsj').getValue(),
											offerreceived: formmycompleteselling.getForm().findField('foffer').getValue(),
											zip: formmycompleteselling.getForm().findField('fzip').getValue(),
											ndate: formmycompleteselling.getForm().findField('fndate').getValue(),
											ndateb: formmycompleteselling.getForm().findField('fndateb').getValue(),
											ntask: formmycompleteselling.getForm().findField('fntask').getValue(),
											county: formmycompleteselling.getForm().findField('fcounty').getValue(),
											city: formmycompleteselling.getForm().findField('fcity').getValue(),
											xcode: formmycompleteselling.getForm().findField('fxcode').getValue()
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Error saving filters');
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											Ext.MessageBox.alert('Warning','Default search successfully saved');
										}
									});
								}				
							},'->',{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSCT.property.address = formmycompleteselling.getForm().findField('faddress').getValue();
									filtersSCT.property.mlnumber = formmycompleteselling.getForm().findField('fmlnumber').getValue();
									filtersSCT.agent = formmycompleteselling.getForm().findField('fagent').getValue();
									filtersSCT.property.status = formmycompleteselling.getForm().findField('fstatus').getValue();
									filtersSCT.contract = formmycompleteselling.getForm().findField('fcontract').getValue();
									filtersSCT.pof = formmycompleteselling.getForm().findField('fpof').getValue();
									filtersSCT.emd = formmycompleteselling.getForm().findField('femd').getValue();
									filtersSCT.ademdums = formmycompleteselling.getForm().findField('fademdums').getValue();
									filtersSCT.offerreceived = formmycompleteselling.getForm().findField('foffer').getValue();
									filtersSCT.property.zip = formmycompleteselling.getForm().findField('fzip').getValue();
									filtersSCT.task.date = formmycompleteselling.getForm().findField('fndate').getValue();
									filtersSCT.task.dateb = formmycompleteselling.getForm().findField('fndateb').getValue();
									filtersSCT.task.datec = formmycompleteselling.getForm().findField('fndatec').getValue();
									filtersSCT.task.type = formmycompleteselling.getForm().findField('fntask').getValue();
									filtersSCT.property.county = formmycompleteselling.getForm().findField('fcounty').getValue();
									filtersSCT.property.city = formmycompleteselling.getForm().findField('fcity').getValue();
									filtersSCT.property.xcode = formmycompleteselling.getForm().findField('fxcode').getValue();
									
									storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
									//storemyfollowcompletesellingAll.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSCT.property.address = '';
									filtersSCT.property.mlnumber = '';
									filtersSCT.agent = '';
									filtersSCT.property.status = 'ALL';
									filtersSCT.task.date = '';
									filtersSCT.task.dateb = '';
									filtersSCT.task.datec = 'Equal';
									filtersSCT.task.type = '-1';
									filtersSCT.contract = '-1';
									filtersSCT.pof = '-1';
									filtersSCT.emd = '-1';
									filtersSCT.ademdums = '-1';
									filtersSCT.msj = '-1';
									filtersSCT.history = '-1';	
									filtersSCT.offerreceived = '-1';
									filtersSCT.property.zip='';
									filtersSCT.property.county = 'ALL';
									filtersSCT.property.city = 'ALL';
									filtersSCT.property.xcode = 'ALL';
									//Ext.getCmp('formmytasks').getForm().reset();
									
									formmycompleteselling.getForm().findField('faddress').setValue('');
									formmycompleteselling.getForm().findField('fmlnumber').setValue('');
									formmycompleteselling.getForm().findField('fzip').setValue('');
									formmycompleteselling.getForm().findField('fagent').setValue('');
									formmycompleteselling.getForm().findField('fstatus').setValue('ALL');
									formmycompleteselling.getForm().findField('fcontract').setValue('-1');
									formmycompleteselling.getForm().findField('fpof').setValue('-1');
									formmycompleteselling.getForm().findField('femd').setValue('-1');
									formmycompleteselling.getForm().findField('fademdums').setValue('-1');
									formmycompleteselling.getForm().findField('foffer').setValue('-1');
									formmycompleteselling.getForm().findField('fndate').setValue('');
									formmycompleteselling.getForm().findField('fndateb').setValue('');
									formmycompleteselling.getForm().findField('fndatec').setValue('Equal');
									formmycompleteselling.getForm().findField('fntask').setValue('-1');
									formmycompleteselling.getForm().findField('fmsj').setValue('-1');
									formmycompleteselling.getForm().findField('fcounty').setValue('ALL');
									formmycompleteselling.getForm().findField('fcity').setValue('ALL');
									formmycompleteselling.getForm().findField('fxcode').setValue('ALL');
									//storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
									//win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 450,
								modal	 	: true,  
								plain       : true,
								items		: formmycompleteselling,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersSCT.property.address = '';
					filtersSCT.property.mlnumber = '';
					filtersSCT.agent = '';
					filtersSCT.property.status = 'ALL';
					filtersSCT.task.date = '';
					filtersSCT.task.dateb = '';
					filtersSCT.task.datec = 'Equal';
					filtersSCT.task.type = '-1';
					filtersSCT.contract = '-1';
					filtersSCT.pof = '-1';
					filtersSCT.emd = '-1';
					filtersSCT.ademdums = '-1';
					filtersSCT.msj = '-1';
					filtersSCT.history = '-1';	
					filtersSCT.offerreceived = '-1';
					filtersSCT.property.zip='';
					filtersSCT.property.county = 'ALL';
					filtersSCT.property.city = 'ALL';
					filtersSCT.property.xcode = 'ALL';
					
					storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
					//storemyfollowcompletesellingAll.load();
				}
			}),new Ext.Button({
				tooltip: 'View Following',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/icono_tablero_casita.png',
				handler: function(){
					if(selected_datamyfollowcompleteselling.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					if(AllCheckmyfollowcompleteselling==true){
						var totales = storemyfollowcompletesellingAll.getRange(0,storemyfollowcompletesellingAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowcompletesellingAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyfollowcompleteselling[0]+'\'';
						for(i=1; i<selected_datamyfollowcompleteselling.length; i++)
							pids+=',\''+selected_datamyfollowcompleteselling[i]+'\''; 
					}
					tabsFollowSelling .activate(0);
					storemysellingfollowup.load({params:{start:0,pids:pids}});
				}
			}),new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'BuyingPendingTask'} }
					});
					win.show();
				}
			})
		]
	});
	
	function checkExecType(value, metaData, record, rowIndex, colIndex, store) {
		if(value==2) return 'Automatic'; 
		else return 'Manual';
	}
	
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var gridmyfollowcompleteselling = new Ext.grid.EditorGridPanel({
		renderTo: 'mysellingcompletetasks_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemyfollowcompleteselling,
		stripeRows: true,
		sm: smmyfollowcompleteselling,
		clicksToEdit: 1, 
		columns: [
			smmyfollowcompleteselling,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'User', width: 40, renderer: userRender, dataIndex: 'userid_follow'},{header: 'V', width: 25, sortable: true, tooltip: 'View.', dataIndex: 'msj', renderer: viewRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender},{header: 'T', width: 25, renderer: taskRender, tooltip: 'Next Task.', dataIndex: 'ntask', sortable: true},{header: 'Comp. Date', width: 90, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate', align: 'right', sortable: true}";	
					else if($val->name=='zip'){
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
						echo ",{header: 'DOM', width: 40, dataIndex: 'dom', align: 'center', sortable: true, tooltip: 'Days on Market'}";
						echo ",{header: 'F', width: 40, dataIndex: 'pendes', align: 'center', sortable: true, tooltip: 'Foreclosure status'}";			
					}else
						if($val->name!='agent')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent', align: 'right'}
				,{header: 'L. Price', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Listing Price', dataIndex: 'lprice', align: 'right'}
				,{header: 'Offer', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Offer.', dataIndex: 'offer', align: 'right'}
				,{header: 'C. Offer', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer', align: 'right'}
				
				,{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate', align: 'right'}
				,{header: 'C', width: 25, sortable: true, tooltip: 'Contract.', dataIndex: 'contract', renderer: checkRender}
				,{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}
				,{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}
				,{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}
				,{header: 'O', width: 25, sortable: true, tooltip: 'Offer Received.', dataIndex: 'offerreceived', renderer: checkRender}
				,{header: 'Exec. Type', dataIndex: 'typeExec', renderer: checkExecType, tooltip: 'Execution Type.'}";
		   ?>			 
		], 
		tbar: new Ext.PagingToolbar({ 
			id: 'pagingmysellingcompletetasks',
            pageSize: limitmyfollowcompleteselling,
            store: storemyfollowcompleteselling,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowcompleteselling=50;
					Ext.getCmp('pagingmysellingcompletetasks').pageSize = limitmyfollowcompleteselling;
					Ext.getCmp('pagingmysellingcompletetasks').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupSCT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowcompleteselling=80;
					Ext.getCmp('pagingmysellingcompletetasks').pageSize = limitmyfollowcompleteselling;
					Ext.getCmp('pagingmysellingcompletetasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSCT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowcompleteselling=100;
					Ext.getCmp('pagingmysellingcompletetasks').pageSize = limitmyfollowcompleteselling;
					Ext.getCmp('pagingmysellingcompletetasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSCT'
			})]
        }),
		
		listeners: {
			'click': function(){
				if(document.getElementById('generate_contract')){
					var tab = tabs.getItem('generate_contract');
					tabs.remove(tab);
				}				
			},
			'afteredit': function (oGrid_Event) {
				var fieldValue = oGrid_Event.value;
				if(fieldValue=='BEGIN')
				{Ext.MessageBox.alert('Warning','Error select BEGIN'); return;}
				//alert(fieldValue+' '+oGrid_Event.record.data.pid+' '+oGrid_Event.field)
				Ext.Ajax.request({   
					waitMsg: 'Saving changes...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
					method: 'POST',
					params: {
						type: "change-lprice", 
						pid: oGrid_Event.record.data.pid,
						field: oGrid_Event.field,
						value: fieldValue,
						originalValue: oGrid_Event.record.modified,
						userid: <?php echo $userid;?>
					},
					
					failure:function(response,options){
						Ext.MessageBox.alert('Warning','Error editing');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						
						if(rest.succes==false)
							Ext.MessageBox.alert('Warning',rest.msg);
							
						storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
					}
				 });
			},
			'sortchange': function (grid, sorted){
				filtersSCT.order.field=sorted.field;
				filtersSCT.order.direction=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var status = new Ext.Action({
					text: 'Change Status',
					handler: function(){
						var simple = new Ext.FormPanel({
							url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
							frame: true,
							title: 'Follow Up - Status Change',
							width: 350,
							waitMsgTarget : 'Waiting...',
							labelWidth: 75,
							defaults: {width: 230},
							labelAlign: 'left',
							items: [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										fieldLabel	  : 'Status',
										width		  : 130,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['N','None'],
												['UC','Under Contract'],
												['PS','Pending Sale']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'statusaltname',
										value         : 'N',
										hiddenName    : 'statusalt',
										hiddenValue   : 'N',
										allowBlank    : false
									},{
										xtype     : 'hidden',
										name      : 'pid',
										value     : pid
									},{
										xtype     : 'hidden',
										name      : 'type',
										value     : 'change-status'
									}],
							
							buttons: [{
									text: 'Change',
									handler: function(){
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												win.close();
												Ext.Msg.alert("Follow Up", 'Status Changed.');
												storemyfollowcompleteselling.load();
												//storemyfollowcompletesellingAll.load();
											},
											failure: function(form, action) {
												loading_win.hide();
												Ext.Msg.alert("Failure", "ERROR");
											}
										});
									}
								},{
									text: 'Reset',
									handler  : function(){
										simple.getForm().reset();
										win.close();
									}
								}]
							});
						 
						var win = new Ext.Window({
							layout      : 'fit',
							width       : 240,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'close',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									win.close();
									loading_win.hide();
								}
							}]
						});
						win.show();
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			}
		}
	});
	
	loadedSCT=true;
	storemyfollowcompleteselling.load({params:{start:0, limit:limitmyfollowcompleteselling}});
</script>