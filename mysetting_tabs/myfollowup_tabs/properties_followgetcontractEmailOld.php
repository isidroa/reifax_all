<?php
// Send the document by email with and attach
function SendEMail($attach,$mln,$address,$userid,$name,$email,$send,$copy,$template,$bd,$pid,$typeSend,$agentfax,$offerFollow,$type,$typeFollow='B') {
	conectarPorNameCounty($bd);
	
	$query = "SELECT x.email, x.name, x.surname, x.hometelephone, x.mobiletelephone, x.address,
			p.proffax, p.companyname
			FROM xima.ximausrs x inner join xima.profile p on x.userid=p.userid
			WHERE x.userid='$userid'";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);

	$userPhone    = strlen($row[4]) == 0 ? $row[3] : $row[4];
	$userName = $row[1].' '.$row[2];
	$emailUser = $row[0];
	$addressUser = $row[5];
	$userFax = $row[6];
	$userCompany = $row[7];
	
	$query    = "SELECT * FROM xima.contracts_mailsettings m
				inner join xima.contracts_phonesettings p on m.userid=p.userid
				WHERE m.userid = $userid";
	$rs       = mysql_query($query);
	$mailInfo = mysql_fetch_array($rs);
	
	//Elige template de email a utilizar
	if($template!=0){
		$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$template";
		$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
		$mailTemplate = mysql_fetch_array($resultTemplate);
		
		$subject = $mailTemplate['subject'];
		$body = $mailTemplate['body'];
		
		include_once('../../mysetting_tabs/mycontracts_tabs/function_template.php'); 
		$subject=replaceTemplate($subject,$userid,$pid,$type,$name,$email);
		$body=replaceTemplate($body,$userid,$pid,$type,$name,$email);
	}else{
		if($userid==933 || $userid==2846 || $userid==2883){
			$subject  = "{$address}-Cash Offer, You Represent Us";
			
			$body   .= "Dear,\r\n\r\n";
			$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
			$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
			$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
			$body    .= "    \t* We close Cash. We are well funded.\r\n";
			$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
			$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
			$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
			$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
			$body   .="Thank you and have an amazing day!\r\n\r\n";
			$body    .= "Best Regards,\r\n";
			$body    .= "{$row[1]} {$row[2]}.\r\n";
			$body    .= "Summit Home Buyers, LLC\r\n";
			$body    .= "$phone\r\n";
		}else{
			$subject  = "Offer for the MLS number $mln, Address {$address}";
			if($userid==1719 || $userid==1641){
				$texto="Dear $name<br><br>";
			}else{
				$texto="Dear Mr/Mrs $name<br><br>";
			}
			$body     = $texto;
			$body   .= "Please find attached a contract with our offer regarding the ";
			$body   .= "property with the address: {$address}<br>";
			$body   .= "We wait for your prompt response<br><br>";
			$body   .= "Regards {$row[1]} {$row[2]}.<br>";
			$body   .= "$phone<br>";
			$body   .= "{$mailInfo['username']}<br><br>";
		}
	}
	$fecha= date('YmdHms');
	$docsDir = 'F:/ReiFaxDocs/';
	$dir = $docsDir."MailAttach/$userid";
	//Crear Directorio de Attachment
	if(!is_dir($dir)){
		if(!mkdir($dir)){
			die('error dir not created!!!');
		}	
	}
	$dir .='/';
	
	$_POST['userid'] = $userid;	
	$_POST['subject']=$subject; 
	$_POST['msg']=$body;
	$_POST['task']=7;
	$_POST['type']='composeEmail';
	
	$_POST['contract'] 	= 0;
	$_POST['pof']		= $_POST['pof'] == 'on' ? 0:1;
	$_POST['emd']		= $_POST['emd'] == 'on' ? 0:1;
	$_POST['rademdums'] = $_POST['rademdums'] == 'on' ? 0:1;
	$_POST['offer'] = $offerFollow;
	
	if($typeSend==5){
		if($send=='on' && $copy=='on'){
			$_POST['pid']=$pid;
			$_POST['to']=$email;
			$_POST['cc']=true;
		}else if($send=='on' && $copy!='on'){
			$_POST['pid']=$pid;
			$_POST['to']=$email;
			$_POST['cc']=false;
		}else if($send!='on' && $copy=='on'){
			$_POST['pid']='-1';
			$_POST['to']=$emailUser;
			$_POST['cc']=true;
		}
		copy($attach,$dir.'contract_'.sha1($fecha.$userid.$pid).'.pdf');
		$_POST['file7']='contract_'.sha1($fecha.$userid.$pid).'.pdf';
		$saved=$dir.'contract_'.sha1($fecha.$userid.$pid).'.pdf';
	}else if($typeSend==3){
		$sendTo = strtolower($agentfax.'@'.$mailInfo['fax_domain']);
		
		if($userid==1719){
			$sendTo='juandej18@gmail.com';
		}
		include_once '../../FPDF/dompdf/dompdf_config.inc.php';
		
		$rutapdftk = getCwd().'/../../overview_contract';
		$archivo = $dir.'template.pdf';
		$dompdf = new DOMPDF();
		
		$dompdf->load_html($body);
		//echo $body;
		//return;
		$dompdf->render();
		
		$pdfoutput = $dompdf->output();
		$filename = $pdfoutput;
		
		$fp = fopen($archivo, "a");
		fwrite($fp, $pdfoutput);
		fclose($fp); 
		$attachpdf=$dir.'contract_'.sha1($fecha.$userid.$pid).'.pdf';
		passthru("{$rutapdftk}/pdftk/pdftk {$archivo} {$attach}  cat output {$attachpdf}");
		$_POST['file7']='contract_'.sha1($fecha.$userid.$pid).'.pdf';
		$_POST['pid']=$pid;
		$_POST['to']=$sendTo;
		$saved=$attachpdf;
	}
	
	//Control de contratos
	$queryControl="insert into xima.control_contratos (userid, contrato, destinatario, fecha, parcelid, typeSend) 
				values ($userid,'{$_POST['file7']}','{$_POST['to']}',now(),'$pid',$typeSend)";
	mysql_query($queryControl) or die($queryControl.' '.mysql_error());

	$query = "SELECT p.`parcelid`, s.financial, p.`state`, p.`county`, p.`city`, p.`zip`, p.`address`, p.`unit`, p.`xcode`, 
				p.`beds`, p.`bath`, p.`sqft`, p.`price`, p.`pendes`, p.`equity`, p.`tieneImg`, p.`latitude`, p.`longitude`, p.`status`  
				FROM properties_php p
				LEFT JOIN mlsresidential s ON s.parcelid=p.parcelid
				WHERE p.`parcelid`='$pid'";
	$rsss    = mysql_query($query) or die($query.' '.mysql_error());
	$rowss   = mysql_fetch_array($rsss);
	
	$query="INSERT INTO xima.saveddocsent 
					(`usr`, `parcelid`, `pzip`, `sdate`, `state`, `financial`,`county`, `city`, `zip`, `address`, `unit`, `xcode`,
						`beds`, `bath`, `sqft`, `price`, `pendes`, `latitude`, `longitude`, `status`) 
			VALUES ($userid,'$pid','$pid".$rowss[zip]."',now(),'".$rowss[state]."','".$rowss[financial]."','".$rowss[county]."',
			'".$rowss[city]."','".$rowss[zip]."','".$rowss[address]."','".$rowss[unit]."','".$rowss[xcode]."','".$rowss[beds]."',
				'".$rowss[bath]."','".$rowss[sqft]."','".$rowss[price]."','".$rowss[pendes]."','".$rowss[latitude]."','".$rowss[longitude]."','".$rowss[status]."')";
	mysql_query($query) or die($query.' '.mysql_error()) ;
	
	ob_start();
	include_once('properties_followupEmail.php');
	$content = ob_get_contents();
	ob_end_clean();
	
	$respuesta=json_decode($content,true);
	$env='0';
	if($respuesta['msg']==''){
		$env='1';
		$completetask=$_POST['completetask'];
		if($send=='on' && $completetask=='true'){
			completeTaskFollow($userid,0,$pid,7,'',0,$_POST['pof'] == 'on' ? 0:1,$_POST['emd'] == 'on' ? 0:1,$_POST['rademdums'] == 'on' ? 0:1,$offerFollow);
				
		}
		if($send=='on'){
			$query  = "SELECT address FROM xima.followup WHERE parcelid='$pid' and userid=$userid";
			$rs     = mysql_query($query);
			$row    = mysql_fetch_array($rs);
	
			$dire    = 'saved_documents';
			$namedoc= "contract_".sha1($fecha.$userid.$pid).".pdf";
			$doc    = "C:/inetpub/wwwroot/$dire/".$namedoc;
			$url    = "http://www.reifax.com/$dire/".$namedoc;
			
			copy($saved,$doc);
	
			$query  = "INSERT INTO xima.saveddoc
						(usr,parcelid,url,directorio,sdate,address,name)
					VALUES
						($userid,'$pid','$url','$doc',NOW(),'{$row[0]}','$namedoc')";
			$rs     = mysql_query($query) or die($query.' '.mysql_error());
		}
	}else{
		//Saved error in pending task.
		scheduleTaskFollow($userid,$pid,$_COOKIE['datos_usr']['USERID'],'NOW()',7,'','ERROR: '.$respuesta['msg'],$typeFollow);
	}
	return $env;
}
?>