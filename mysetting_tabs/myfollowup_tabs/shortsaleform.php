<?php 
	include("../../templates/properties_template.php");
	include('../../properties_conexion.php');
	conectar();
	$parcelid=$_GET['pid'];
	$userid=$_COOKIE['datos_usr']['USERID'];
?>
<html>
	<head>
	<?php tagHeadHeader();?>
    <script type="text/javascript" src="http://www.reifax.com/includes/properties_generator.js"></script>
    </head>
    <script>
		if(document.URL.substr(0,5)=='http:')document.location=document.URL.substr(0,4)+'s'+document.URL.substr(4);
		Ext.onReady(function(){
		    Ext.QuickTips.init();  
		    Ext.form.Field.prototype.msgTarget = 'side';  
			Ext.apply(Ext.form.VTypes, {  
				'ssn': function(){  
						var re = /^([0-6]\d{2}|7[0-6]\d|77[0-2])([ \-]?)(\d{2})\2(\d{4})$/;  
						return function(v){  
							return re.test(v);  
						}  
				}(),  
				'ssnText' : 'SSN format: xxx-xx-xxxx'  
			}); 
			<?php 
				$queryc = 'SELECT * FROM xima.shortsale_company WHERE userid='.$_COOKIE['datos_usr']["USERID"];
				$resc=mysql_query($queryc)or die($queryc.mysql_error());
		
				$i=0; $defaultcompany='';
				//echo "['0','As is Residential Contract'],['1','Lead base Contract'],['2','Residential Contract']";
				
				while($rowcr=mysql_fetch_array($resc))
				{
					if($i==0) $defaultcompany=$rowcr['id'];
					$i++;
				}
			?>
			<?php if($defaultcompany==0){  ?>
				var defaultcompany='';
			<?php }else{ ?>
				var defaultcompany=<?php echo $defaultcompany;?>;
			<?php } ?>
			var storeshortsale 		= new Ext.data.JsonStore({
				autoLoad		: true,
				url				: '../mycontracts_tabs/myshortsales.php?type=getcompanys',
				fields			: [
					{name		: 'id', type: 'int'},
					{name		: 'userid', type: 'int'},
					{name		: 'name'},
					{name		: 'address'},
					{name		: 'phone'}
				],
				root			: 'records',
				totalProperty	: 'total'
			});
			//storeshortsale.load();
								 var simple = new Ext.FormPanel({
									frame:true,
									title: 'Short Sale Form',
									url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
									width: 950,
									labelAlign: 'left',
									items: [{
												xtype     : 'textfield',
												name      : 'address',
												id      : 'address',
												fieldLabel: 'Property Address',
												width: 200
											},{
												xtype     : 'fieldset',
												title: 'Borrower Information',
												autoHeight:true,
												layout:'column',
												items:[{
													columnWidth:.3,
													layout: 'form',
													items: [{
														xtype:'textfield',
														fieldLabel: 'Borrower Name',
														name: 'borrower',
														id: 'borrower',
														anchor:'95%'
													}, {
														xtype:'textfield',
														fieldLabel: 'Co-Borrower Name',
														name: 'coborrower',
														id: 'coborrower',
														anchor:'95%'
													},{
														xtype     : 'textfield',
														name      : 'paddress',
														id: 'paddress',
														fieldLabel: 'Address',
														anchor:'95%'
													}]
												},{
													columnWidth:.4,
													layout: 'form',
													labelWidth: 135,
													items: [{
														xtype:'textfield',
														fieldLabel: 'Social Security Number',
														name: 'ssnborrower',
														id: 'ssnborrower',
														value: '000-00-0000',
														vtype: 'ssn',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: 'Social Security Number',
														name: 'ssncoborrower',
														id: 'ssncoborrower',
														value: '000-00-0000',
														vtype: 'ssn',
														anchor:'95%'
													},{
														xtype     : 'textfield',
														name      : 'phoneborrower',
														id      : 'phoneborrower',
														fieldLabel: 'Phone',
														anchor:'95%'
													}]
												},{
													columnWidth:.3,
													layout: 'form',
													labelWidth: 60,
													items: [{
														xtype:'textfield',
														fieldLabel: 'Email',
														name: 'emailborrower',
														id: 'emailborrower',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: 'Email',
														name: 'emailcoborrower',
														id: 'emailcoborrower',
														anchor:'95%'
													}]
												}]
											},{
												xtype     : 'fieldset',
												title: 'Mortgage Information',
												autoHeight:true,
												layout:'column',
												items:[{
													columnWidth:.5,
													layout: 'form',
													labelWidth: 150,
													items: [{
														xtype:'textfield',
														fieldLabel: '1 Mortgage Company',
														name: 'mortcomp1',
														id: 'mortcomp1',
														anchor:'95%'
													}, {
														xtype:'textfield',
														fieldLabel: '1 Mortgage Balance',
														name: 'balance1',
														id: 'balance1',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: '2 Mortgage Company',
														name: 'mortcomp2',
														id: 'mortcomp2',
														anchor:'95%'
													}, {
														xtype:'textfield',
														fieldLabel: '2 Mortgage Balance',
														name: 'balance2',
														id: 'balance2',
														anchor:'95%'
													}]
												},{
													columnWidth:.5,
													layout: 'form',
													labelWidth: 120,
													items: [{
														xtype:'textfield',
														fieldLabel: '1 Loan #',
														name: 'loan1',
														id: 'loan1',
														anchor:'95%'
													}, {
														xtype:'textfield',
														fieldLabel: '1 Loan Type',
														name: 'loantype1',
														id: 'loantype1',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: '2 Loan #',
														name: 'loan2',
														id: 'loan2',
														anchor:'95%'
													}, {
														xtype:'textfield',
														fieldLabel: '2 Loan Type',
														name: 'loantype2',
														id: 'loantype2',
														anchor:'95%'
													}]
												}]
											},{
												xtype     : 'fieldset',
												title: 'Buyer Information',
												autoHeight:true,
												layout:'column',
												items:[{
													columnWidth:.3,
													layout: 'form',
													items: [{
														xtype:'textfield',
														fieldLabel: 'Buyer Name',
														name: 'buyer',
														id: 'buyer',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: 'Co Buyer Name',
														name: 'cobuyer',
														id: 'cobuyer',
														anchor:'95%'
													}]
												},{
													columnWidth:.4,
													layout: 'form',
													items: [{
														xtype:'textfield',
														fieldLabel: 'Address',
														name: 'buyeraddress',
														id: 'buyeraddress',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: 'Address',
														name: 'cobuyeraddress',
														id: 'cobuyeraddress',
														anchor:'95%'
													}]
												},{
													columnWidth:.3,
													layout: 'form',
													items: [{
														xtype:'textfield',
														fieldLabel: 'Email',
														name: 'emailbuyer',
														id: 'emailbuyer',
														anchor:'95%'
													},{
														xtype:'textfield',
														fieldLabel: 'Email',
														name: 'coemailbuyer',
														id: 'coemailbuyer',
														anchor:'95%'
													}]
												}]
											},{
												xtype     : 'fieldset',
												title: 'Short Sale Company Information',
												autoHeight:true,
												layout:'column',
												items:[{
													columnWidth:.4,
													labelWidth: 120,
													layout: 'form',
													items: [{
														xtype         : 'combo',
														name          : 'shorcomp',
														id            : 'shorcomp',
														hiddenName    : 'shortsale',
														fieldLabel    : 'Short Sale Company',
														typeAhead     : true,
														autoSelect    : true,
														mode          : 'local',
														store         : storeshortsale,
														triggerAction : 'all', 
														editable      : false,
														selectOnFocus : true,
														allowBlank    : false,
														displayField  : 'name',
														valueField    : 'id',
														value		  : defaultcompany,
														anchor:'95%'
													}]
												},{
													columnWidth:.2,
													layout: 'form',
													items: [
														new Ext.Button({
														tooltip: 'Click to add a new short sale company.',
														iconCls:'icon',
														iconAlign: 'top',
														width: 30,
														icon: 'http://www.reifax.com/img/add.gif',
														handler: function(){
															var simple = new Ext.FormPanel({
																url: '../mycontracts_tabs/myshortsales.php',
																frame: true,
																title: 'Add Short Sale',
																width: 350,
																waitMsgTarget : 'Waiting...',
																labelWidth: 75,
																defaults: {width: 230},
																labelAlign: 'left',
																items: [{
																			xtype     : 'textfield',
																			name      : 'name',
																			fieldLabel: 'Name',
																			allowBlank: false
																		},{
																			xtype     : 'textfield',
																			name      : 'address',
																			fieldLabel: 'Address'
																		},{
																			xtype     : 'textfield',
																			name      : 'phone',
																			fieldLabel: 'Phone'
																		},{
																			xtype     : 'hidden',
																			name      : 'type',
																			value     : 'insert'
																		}],
																
																buttons: [{
																		text: 'Insert',
																		handler: function(){
																			loading_win.show();
																			simple.getForm().submit({
																				success: function(form, action) {
																					loading_win.hide();
																					win.close();
																					Ext.Msg.alert("Short Sale", 'New Short Sale company');
																					storeshortsale.load();
																				},
																				failure: function(form, action) {
																					loading_win.hide();
																					Ext.Msg.alert("Failure", "ERROR");
																				}
																			});
																		}
																	},{
																		text: 'Reset',
																		handler  : function(){
																			simple.getForm().reset();
																			win.close();
																		}
																	}]
																});
															 
															var win = new Ext.Window({
																layout      : 'fit',
																width       : 340,
																height      : 250,
																modal	 	: true,
																plain       : true,
																items		: simple,
																closeAction : 'close',
																buttons: [{
																	text     : 'Close',
																	handler  : function(){
																		win.close();
																		loading_win.hide();
																	}
																}]
															});
															win.show();
														}
													})]
												},{
													columnWidth:.3,
													layout: 'form',
													items: [{
														xtype:'checkbox',
														fieldLabel: 'Dates',
														name: 'markdates',
														id: 'markdates',
														anchor:'95%'
													}]
												}]
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'insert'
											},{
												xtype     : 'hidden',
												name      : 'parcelid',
												value     : <?php echo $parcelid; ?> 
											}],
									
									buttons: [{
											text: 'Save',
											handler: function(){
												if (simple.getForm().isValid()) {
													simple.getForm().submit({
														url     : 'saveshortsale.php',
														waitMsg : 'Saving...',
														success : function(f, a){ 
															var resp = a.result;
															Ext.MessageBox.alert('', resp.mensaje);
														}
													});
												}
											}
										},{
											text: 'Generate Package',
											handler: function(){
												if (simple.getForm().isValid()) {
													simple.getForm().submit({
														url     : 'saveshortsale.php',
														waitMsg : 'Saving...',
														success : function(f, a){ 
															var resp = a.result;
															if(resp.savesett=='yes'){
																simple.getForm().submit({
																	url     : 'generateshortsale.php',
																	waitMsg : 'Saving...',
																	success : function(f, a){ 
																		var resp = a.result;
																		//Ext.MessageBox.alert('', resp.mensaje);
																		var url = 'http://www.reifax.com/'+resp.pdf;
																		window.open(url);
																		//winshortsale.close();
																	}
																});
															}
														}
													});
												}
											}
										}],
										buttonAlign: 'center'
									});
			loading_win.show();
			var primera='Y';
			storeshortsale.addListener('load',cargar);
			function cargar(){
				if(primera=='Y'){
					primera='N';
					Ext.Ajax.request({
						url     : 'shortsaleparams.php?&pid='+<?php echo $parcelid; ?>,
						method  : 'POST',
						waitMsg : 'Getting Info',
						success : function(r) {
							var resp   = Ext.decode(r.responseText);
							loading_win.hide(); 
							if(resp.nuevo=='Y'){
								Ext.getCmp('shorcomp').setValue(defaultcompany);
								Ext.getCmp('borrower').setValue(resp.data.borrower);
								Ext.getCmp('coborrower').setValue(resp.data.coborrower);
								Ext.getCmp('address').setValue(resp.data.address);
								Ext.getCmp('mortcomp1').setValue(resp.data.mortcomp1);
								Ext.getCmp('balance1').setValue(resp.data.mortbal1);
								Ext.getCmp('loan1').setValue(resp.data.loannum1);
								Ext.getCmp('loantype1').setValue(resp.data.loantype1);
								Ext.getCmp('mortcomp2').setValue(resp.data.mortcomp2);
								Ext.getCmp('balance2').setValue(resp.data.mortbal2);
								Ext.getCmp('loan2').setValue(resp.data.loannum2);
								Ext.getCmp('loantype2').setValue(resp.data.loantype2);
							}else{
								Ext.getCmp('address').setValue(resp.data.prop_address);
								Ext.getCmp('borrower').setValue(resp.data.borrower);
								Ext.getCmp('ssnborrower').setValue(resp.data.ssnborrower);
								Ext.getCmp('emailborrower').setValue(resp.data.emailborrower);
								Ext.getCmp('coborrower').setValue(resp.data.coborrower);
								Ext.getCmp('ssncoborrower').setValue(resp.data.ssncoborrower);
								Ext.getCmp('emailcoborrower').setValue(resp.data.emailcoborrower);
								Ext.getCmp('paddress').setValue(resp.data.addressborrower);
								Ext.getCmp('phoneborrower').setValue(resp.data.phoneborrower);
								Ext.getCmp('mortcomp1').setValue(resp.data.company1);
								Ext.getCmp('balance1').setValue(resp.data.balance1);
								Ext.getCmp('loan1').setValue(resp.data.loannum1);
								Ext.getCmp('loantype1').setValue(resp.data.loantype1); 
								Ext.getCmp('mortcomp2').setValue(resp.data.company2);
								Ext.getCmp('balance2').setValue(resp.data.balance2);
								Ext.getCmp('loan2').setValue(resp.data.loannum2);
								Ext.getCmp('loantype2').setValue(resp.data.loantype2);
								Ext.getCmp('buyer').setValue(resp.data.buyer);
								Ext.getCmp('buyeraddress').setValue(resp.data.buyeraddress);
								Ext.getCmp('emailbuyer').setValue(resp.data.buyermail);
								Ext.getCmp('shorcomp').setValue(resp.data.shortsalecompany);
								Ext.getCmp('cobuyer').setValue(resp.data.cobuyer);
								Ext.getCmp('cobuyeraddress').setValue(resp.data.cobuyeraddress);
								Ext.getCmp('coemailbuyer').setValue(resp.data.cobuyermail);
							}
							 simple.render(document.body);
						}
					 });
				}
			}
			 //simple.render(document.body);
		});
	</script>
</html>
