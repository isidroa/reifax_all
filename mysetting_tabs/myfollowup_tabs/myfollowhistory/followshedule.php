<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>
<style>
.x-grid3-row-expander-header {
  background-color: transparent;
  background-position: 1px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
.x-grid3-row-expander-header-collapse {
  background-color: transparent;
  background-position: -24px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
</style>
<div id="report_content_schedule" style="text-align:center !important;">
	<br clear="all">
	<h1 align="center" >FOLLOW SCHEDULE</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$city.', '.$county.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center><div id="follow_schedule_grid"></div></center>
	<br>
</div>

<script>
	var followstore_schedule = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
		fields: [ 'idfus','parcelid','userid',
		   {name: 'odate', type: 'date', dateFormat: 'Y-m-d'},
		   {name: 'ohour', type: 'date', dateFormat: 'h:i:s'},
		   {name: 'sdate', type: 'date', dateFormat: 'Y-m-d h:i:s'},
		   {name: 'status'},
		   {name: 'typeExec', type: 'int'},
		   {name: 'task', type: 'int'},
		   {name: 'detail'},
		   {name: 'userid_follow', type: 'int'},
		   {name: 'name_follow'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'parcelid': "<?php echo $_POST['parcelid'];?>",'typeFollow': "<?php echo $typeFollow; ?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'odate',
			direction: 'DESC'
		}
    });
	
	var followdetail_schedule = new Ext.ux.grid.RowExpander({
		header: '<div id="headerexpander_schedule" class="x-grid3-row-expander-header">&nbsp;</div>',
		id: 'followdetailID_schedule',
		tpl : new Ext.Template(
			'<br><p style="margin-left:50px;font-size:14px;text-align:left;"><b>Detail:</b> {detail}</p>' 
		)
	});
	var followsm_schedule = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	
	var progressBarTask_schedule= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarTask_win_schedule=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarTask_schedule]
	});
	
	function execRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 1: return ''; break; //'<img title="Manual" src="../../img/notes/manual.png" />'
			case 2: return '<img title="Automatic" src="../../img/notes/automatic.png" />'; break;
		}
	}
	
	var followgrid_schedule = new Ext.grid.GridPanel({
		store: followstore_schedule,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [
				followsm_schedule,
				followdetail_schedule,
				{header: "User", width: 50, renderer: userRender, dataIndex: 'userid_follow'},
				{header: "T", width: 30, dataIndex: 'task', renderer: taskRender, tooltip: 'Type of Task.'},
				{header: "Exec. Date", width: 80, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'odate', tooltip: 'Execution Date of task.', sortable: true},
				{header: "Exec. Time", width: 80, renderer: Ext.util.Format.dateRenderer('h:i A'), dataIndex: 'ohour', tooltip: 'Execution Time of task.', sortable: true},
				{header: "Sche. Time", width: 120, renderer: Ext.util.Format.dateRenderer('m/d/Y h:i A'), dataIndex: 'sdate', tooltip: 'Creation/Edit Time of task.', sortable: true},
				{header: 'Status', width: 530, align: 'left', tooltip: 'Status Schedule Task', dataIndex: 'status', sortable: true},
				{header: 'E', width: 30, renderer: execRender, tooltip: 'Execution Type', dataIndex: 'typeExec', sortable: true}
			]
		}),  
		sm: followsm_schedule,      
		width: 978,
		height: 3000,
		plugins: followdetail_schedule,
		iconCls: 'icon-grid',
		renderTo: 'follow_schedule_grid',
		listeners: {
			'headerclick': function (grid,column,e){
				var colmod = grid.getColumnModel();
				var headerID = colmod.getColumnId(column);
				var count = grid.getStore().getCount();
				
				if(headerID=='followdetailID_schedule'){
					var col = document.getElementById('headerexpander_schedule').className;
					if(col=='x-grid3-row-expander-header'){
						colmod.setColumnHeader(column,'<div id="headerexpander_schedule" class="x-grid3-row-expander-header-collapse">&nbsp;</div>');
						for(i=0;i<count;i++){
							followdetail_schedule.expandRow(grid.view.getRow(i));
						}
					}else{
						colmod.setColumnHeader(column,'<div id="headerexpander_schedule" class="x-grid3-row-expander-header">&nbsp;</div>');
						
						for(i=0;i<count;i++){
							followdetail_schedule.collapseRow(grid.view.getRow(i));
						}
					}
				}
			}
		}
	});
	
	followstore_schedule.load();

if(document.getElementById('tabs')){
	if(document.getElementById('report_content_schedule').offsetHeight > tabs.getHeight()){
		tabs.setHeight(document.getElementById('report_content_schedule').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content_schedule').offsetHeight+150);
	}
}
</script>