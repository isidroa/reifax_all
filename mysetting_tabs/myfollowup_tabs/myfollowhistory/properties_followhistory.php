<?php
	include("../../../properties_conexion.php");
	conectar();	
	
	if(isset($_POST['type'])){
		if($_POST['type']=='delete' || $_POST['type']=='delete-multi'){
			$query='DELETE FROM xima.followup_history WHERE idfuh IN ('.$_POST['idfuh'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='insert'){	
			$parcelid 	= $_POST['parcelid'];
			//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
			 $odate = strtotime($_POST['dateadd']);
			$odate = date('Y-m-d H:i:s',$odate);
			$dateadd 	= isset($_POST['dateadd']) ? $odate : date("Y-m-d H:i:s");
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= $_POST['task'];
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived, detail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.',"'.$dateadd.'",'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.mysql_real_escape_string($detail).'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='update'){	
			$idfuh 		= $_POST['idfuh'];
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= $_POST['task'];
			//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
			 $odate = strtotime($_POST['dateadd']);
			$odate = date('Y-m-d H:i:s',$odate);
			$dateadd 	= isset($_POST['dateadd']) ? $odate : date("Y-m-d H:i:s");
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$sheduledetail 	= strlen($_POST['sheduledetail'])>0 ? $_POST['sheduledetail'] : '';
			
			$query='UPDATE xima.followup_history SET offer='.$offer.',coffer='.$coffer.', task="'.$task.'", contract='.$contract.',pof='.$pof.',
			emd='.$emd.',realtorsadem='.$rademdums.',offerreceived='.$offerreceived.',detail="'.mysql_real_escape_string($detail).'"';
			if(!isset($_POST['dateadd']) || $_POST['dateadd']!='')
			$query.= ',odate="'.$dateadd.'"';
			
			$query.= ' ,sheduledetail="'.addslashes($sheduledetail).'", userid_follow='.$userid_follow.' WHERE idfuh='.$idfuh;
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
	}else{
		$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
		$idfuh 	= isset($_POST['idfuh']) ? $_POST['idfuh'] : '';
		$query='SELECT h.*, concat(x.name," ",x.surname) as name_follow,
				concat(f.address,trim(concat(" ",IF(f.unit is null,"",f.unit))),", ",IF(f.city is null, "",f.city),", ",IF(f.zip is null, "", f.zip)) fulladdress
				FROM xima.followup_history h 
				left join xima.ximausrs x ON (h.userid_follow=x.userid)
				LEFT JOIN xima.followup f ON (h.userid=f.userid AND h.parcelid=f.parcelid)
				WHERE h.parcelid="'.$_POST['parcelid'].'" AND h.userid='.$_POST['userid'];
		if($idfuh!=''){
			$query.=' and h.idfuh='.$idfuh;
		}
		//orders
		if(isset($_POST['sort'])) $query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		
		$result=mysql_query($query) or die($query.mysql_error());
		$vFila=array();
		while($r=mysql_fetch_array($result))
			$vFila[]=$r;
			
		echo '{success: true, total: '.count($vFila).', records:'.json_encode($vFila).'}';
	}
?>