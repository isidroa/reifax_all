<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
	if($_COOKIE['datos_usr']['USERID']!=$_POST['userid']){
		$query="UPDATE xima.follow_notification SET history=0 WHERE parcelid='".$_POST['parcelid']."' AND userid=".$_POST['userid']." AND followid=".$_COOKIE['datos_usr']['USERID'];
		mysql_query($query) or die($query.mysql_error());
	}
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>
<style>
.x-grid3-row-expander-header {
  background-color: transparent;
  background-position: 1px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
.x-grid3-row-expander-header-collapse {
  background-color: transparent;
  background-position: -24px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
</style>
<div id="report_content_selling" style="text-align:center !important;">
	<br clear="all">
	<h1 align="center" >FOLLOW HISTORY</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$city.', '.$county.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center><div id="follow_historyselling_grid" style="width:650px;"></div></center>
	<br>
</div>

<script>
	var followstoreselling = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
		fields: [ 'idfuh','parcelid','userid',
		   {name: 'odate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
		   {name: 'offer', type: 'float'},
		   {name: 'coffer', type: 'float'},
		   {name: 'contract', type: 'bool'},
		   {name: 'task', type: 'int'},
		   {name: 'pof', type: 'bool'},
		   {name: 'emd', type: 'bool'},
		   {name: 'realtorsadem', type: 'bool'},
		   {name: 'offerreceived', type: 'bool'},
		   {name: 'detail'},
		   {name: 'sheduledetail'},
		   {name: 'userid_follow', type: 'int'},
		   {name: 'typeExec', type: 'int'},
		   {name: 'name_follow'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'parcelid': "<?php echo $_POST['parcelid'];?>",'typeFollow': "<?php echo $typeFollow; ?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'odate',
			direction: 'DESC'
		}
    });
	
	var followdetailselling = new Ext.ux.grid.RowExpander({
		header: '<div id="headerexpander" class="x-grid3-row-expander-header">&nbsp;</div>',
		width: 25,
		id: 'followdetailsellingID',
		tpl : new Ext.XTemplate(
			'<tpl for=".">',
			'<tpl if="sheduledetail != null">',	
				'<br><p style="margin-left:50px;font-size:14px;"><b>Schedule Task Detail:</b> {sheduledetail}</p>',
			'</tpl>',
			'<br><p style="margin-left:50px;font-size:14px;"><b>Completed Task Detail:</b> {detail}</p>',
			'</tpl>'
		)
	});
	var followsmselling = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../img/drop-no.png" />'; 
		else return '<img src="../img/drop-yes.png" />';
	}
	
	function checkExecType(value, metaData, record, rowIndex, colIndex, store) {
		if(value==2) return 'Automatic'; 
		else return 'Manual';
	}
	
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var followgrid = new Ext.grid.GridPanel({
		store: followstoreselling,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [

				followsmselling,
				followdetailselling,
				{header: "Date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'odate', tooltip: 'Insert/Update Date.'},
				{header: "Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'offer', tooltip: 'Offer.'},
				{header: "C. Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'coffer', tooltip: 'Contra Offer.'},
				{header: "User", renderer: userRender, dataIndex: 'userid_follow'},
				{header: "T", width: 6, dataIndex: 'task', renderer: taskRender, tooltip: 'Type of Task.'},
				{header: "C", width: 6, dataIndex: 'contract', renderer: checkRender, tooltip: 'Contract Send.'},
				{header: "P", width: 6, dataIndex: 'pof', renderer: checkRender, tooltip: 'Prof of Funds.'},
				{header: "E", width: 6, dataIndex: 'emd', renderer: checkRender, tooltip: 'EMD.'},
				{header: "A", width: 6, dataIndex: 'realtorsadem', renderer: checkRender, tooltip: 'Addendums.'},
				{header: "O", width: 6, dataIndex: 'offerreceived', renderer: checkRender, tooltip: 'Offer Received.'},
				{header: "Exec. Type", dataIndex: 'typeExec', renderer: checkExecType, tooltip: 'Execution Type.'}
			]
		}),
		viewConfig: {
			forceFit:true,
			getRowClass: function(record, rowIndex, rp, ds){ // rp = rowParams
				if(true){
					return 'x-grid3-row-expanded';
				}
				return 'x-grid3-row-collapsed';
			}
		},  
		sm: followsmselling,      
		width: 600,
		height: tabs.getHeight()-200,
		plugins: followdetailselling,
		iconCls: 'icon-grid',
		renderTo: 'follow_historyselling_grid',
		listeners: {
			'headerclick': function (grid,column,e){
				var colmod = grid.getColumnModel();
				var headerID = colmod.getColumnId(column);
				var count = grid.getStore().getCount();
				
				if(headerID=='followdetailsellingID'){
					var col = document.getElementById('headerexpander').className;
					if(col=='x-grid3-row-expander-header'){
						colmod.setColumnHeader(column,'<div id="headerexpander" class="x-grid3-row-expander-header-collapse">&nbsp;</div>');
						for(i=0;i<count;i++){
							followdetailselling.expandRow(grid.view.getRow(i));
						}
					}else{
						colmod.setColumnHeader(column,'<div id="headerexpander" class="x-grid3-row-expander-header">&nbsp;</div>');
						
						for(i=0;i<count;i++){
							followdetailselling.collapseRow(grid.view.getRow(i));
						}
					}
				}
			}
		}
	});
	
	followstoreselling.load();

if(document.getElementById('tabsFollow')){
	if(document.getElementById('report_content_selling').offsetHeight > tabsFollow.getHeight()){
		tabs.setHeight(document.getElementById('report_content_selling').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content_selling').offsetHeight+150);
	}
}
</script>