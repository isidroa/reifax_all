<?php
	include "../../../properties_conexion.php";
	conectar();
	$userid=$_POST['userid'];
	$query  = "SELECT * FROM xima.templates
				WHERE userid = $userid and template_type=3 order by `default` desc";
	$rs     = mysql_query($query);
	$checkedTemplateid=0;
	while ($rowcr = mysql_fetch_array($rs)) {
		$checkedTemplate = $rowcr['name'];
		$checkedTemplateid = $rowcr['id'];
		
		$comboTemplates .= "['{$rowcr['id']}','{$rowcr['name']}'],";
	}
	$checked='';
	
?>
<style type="text/css">
.add-variable {
	background-image: url("http://www.reifax.com/img/add.gif") !important;
}
</style>
<div align="left" id="todo_listmytemplate_sms_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="listmytemplate_sms_data_div" align="center" style=" background-color:#FFF; margin:auto; width:900px;">
    	<div id="listmytemplate_sms_filters"></div><br />
        <div id="listmytemplate_sms_properties" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<script>
	var seleccionarTemplate= '<?php echo $checkedTemplate; ?>';
	var seleccionarTemplateId= <?php echo $checkedTemplateid; ?>;
	
	var storetemplates = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			<?php echo $comboTemplates;?>
			['0','New Template']
		]
	});
	var toolbarmydocstemplate=new Ext.Toolbar({
		renderTo: 'listmytemplate_sms_filters',
		items: [
			new Ext.Button({
				id: 'listnew_templatesms',
				tooltip: 'New Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					newListTemplateSms();
				}
			}),new Ext.Button({
				id: 'listdelete_templatesms',
				tooltip: 'Delete template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'sms',
							modo: 'eliminar',
							id : Ext.getCmp('listtemplate_idsms').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Template Deleted');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_sms.php', 
								cache : false,
								params	: {
									userid: <?php echo $userid; ?>
								}
							});
							
						}
					});	
				}
			}),new Ext.Button({
				id: 'listdefault_templatesms',
				tooltip: 'Set Default',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium', 
				icon: 'http://www.reifax.com/img/toolbar/arrow_green.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'sms',
							modo: 'setdefault',
							id : Ext.getCmp('listtemplate_idsms').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Operation Completed');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_sms.php', 
								cache : false,
								params	: {
									userid: <?php echo $userid; ?>
								}
							});
							
						}
					});	
				}
			}),{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 260,
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'loadtemplates',
						userid: <?php echo $userid; ?>,
						template_type: 3
					},
					fields:[
						{name:'id', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							Ext.getCmp('listcombotemplatessms').setValue(0); 
						}
					}
				}),
				displayField  : 'name',
				valueField    : 'id',
				name          : 'ftemplate',
				id			  : 'listcombotemplatessms',
				value         : seleccionarTemplate,
				hiddenName    : 'ftemplatedoc11',
				hiddenValue   : seleccionarTemplate,
				allowBlank    : false,
				editable	  : false,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(record.get('id')!=0){
							getListTemplateSms(record.get('id'));
						}else{
							newListTemplateSms();  
						}
					}
				}
			}	
		]
	});
	function newListTemplateSms(){
		Ext.getCmp('listdelete_templatesms').setVisible(false);
		Ext.getCmp('listdefault_templatesms').setVisible(false);
		Ext.getCmp('listnew_templatesms').setVisible(false);
		
		Ext.getCmp('listcombotemplatessms').setValue(0);
		Ext.getCmp('listtemplatenamesms').setValue('Template name');
		Ext.getCmp('listtemplatenamesms').setReadOnly(false);
		Ext.getCmp('listtemplateeditorsms').setValue('');
		Ext.getCmp('listtemplate_idsms').setValue(0);
		Ext.getCmp('listdefaulttemplatesms').setValue(0);
		Ext.getCmp('listbuttontemplatesms').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Template</b></span>');
	}
	
	function getListTemplateSms(id){
		loading_win.show();
		Ext.getCmp('listdelete_templatesms').setVisible(true);
		Ext.getCmp('listdefault_templatesms').setVisible(true);
		Ext.getCmp('listnew_templatesms').setVisible(true);
		Ext.getCmp('listtemplatenamesms').setReadOnly(true);
		Ext.getCmp('listbuttontemplatesms').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>');
		Ext.Ajax.request({
			url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
			method  : 'POST',
			params    : {
				type: 'docs',
				modo: 'obtener',
				id : id,
				'userid': <?php echo $userid;?>
			},
			waitMsg : 'Getting Info',
			success : function(r) {
				loading_win.hide();
				var resp   = Ext.decode(r.responseText);
	
				if (resp.data!=null) {
					Ext.getCmp('listtemplatenamesms').setValue(resp.data.name);
					Ext.getCmp('listtemplateeditorsms').setValue(resp.data.body);
					Ext.getCmp('listtemplate_idsms').setValue(resp.data.id);	
					Ext.getCmp('listdefaulttemplatesms').setValue(resp.data.defaulttemplate);
				} 
			}
		});
	}
	
	
	var variablesStoreSms=new Ext.data.ArrayStore({
		fields    : ['id', 'texto'],
		data      : [
			['','Select'],
			<?php echo $combo; ?>
		]
	});
	var variableListInsertarSms='<?php echo $checked; ?>';
	
	var template_sms_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		frame      : true,
		method     : 'POST',
		renderTo: 'listmytemplate_sms_properties', 
		labelWidth: 60,
		items      : [
			{
				xtype		  : 'textfield',
				fieldLabel	  : 'Name',
				name		  : 'name',
				id		  : 'listtemplatenamesms',
				readOnly  : true,
				width: 300,
				allowBlank    : false,
				value: 'Template name'
			},{
				xtype         : 'combo',
				fieldLabel	  : 'Default',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 60,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
						['0','No'],
						['1','Yes']
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				name          : 'defaulttemplatesms',
				hiddenName    : 'defaulttemplatesms1',
				id			  : 'listdefaulttemplatesms',	
				allowBlank    : false,
				readOnly  : true,
				editable	  : false
			},{
				xtype     : 'panel',
				autoHeight:true,
				layout:'column',
				items:[{
					columnWidth: .32,
					labelWidth: 60,
					layout: 'form',
					items: [{
						xtype         : 'combo',
						mode          : 'local',
						fieldLabel    : 'Vars',
						triggerAction : 'all',
						displayField  : 'name',
						valueField    : 'id',
						name          : 'variableListInsertarSms',
						id            : 'variableListInsertarSms',
						value         : variableListInsertarSms,
						allowBlank    : false,
						editable	  : false,
						width: 		    200,
						store         : new Ext.data.JsonStore({
							root:'results',
							totalProperty:'total',
							autoLoad: true, 
							baseParams: {
								type: 'template-variables'
							},
							fields:[
								{name:'id', type:'string'},
								{name:'name', type:'string'}
							],
							url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
							listeners     : {
								'load'  : function(store, records) {
									Ext.getCmp('variableListInsertarSms').setValue(variableListInsertarSms); 
								}
							}
						}), 
						listeners	  : {
							'select'  : function(combo,record,index){
								variableListInsertarSms = record.get('id');
							}
						}
					}]
				},{
					columnWidth: .2,
					layout: 'form',
					items: [
						new Ext.Button({
						tooltip: 'Click to add insert variable.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 30,
						icon: 'http://www.reifax.com/img/add.gif',
						handler: function(){
							/*var texto=Ext.getCmp('listtemplateeditorsms').getValue();
							Ext.getCmp('listtemplateeditorsms').setValue(texto+variableListInsertarSms);*/
							
							var text_field = document.getElementById('listtemplateeditorsms');
							var startPos = text_field.selectionStart;
							var endPos = text_field.selectionEnd;
							text_field.value = text_field.value.substring(0, startPos)
							+ variableListInsertarSms
							+ text_field.value.substring(endPos, text_field.value.length);
				
							//this.el.focus();
							text_field.setSelectionRange(endPos+v.length,endPos+v.length);
						}
					})]
				}]
			},{
				xtype	  :	'textarea',
				height	  : 200,
				width	  : 300,
				name	  : 'body',
				id		  : 'listtemplateeditorsms',	
				fieldLabel: 'Body',
				enableKeyEvents: true,
				autoScroll: true
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : <?php echo $userid; ?>
			},{
				xtype         : 'hidden',
				name          : 'template_id',
				id          : 'listtemplate_idsms'
			},{
				xtype         : 'hidden',
				name          : 'template_type',
				value         : 3
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>', 
			id		: 'listbuttontemplatesms',
			handler : function(){
				if (template_sms_form.getForm().isValid()) {
					template_sms_form.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savetemplate.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							if (resp.mensaje == 'Template saved!') {
								updater.update({
									url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_sms.php', 
									cache : false,
									params	: {
										userid: <?php echo $userid; ?>
									}
								});
							}
						}
					});
				}
			}
		},{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Clear</b></span>',
			handler : function(){
				Ext.getCmp('listtemplateeditorsms').setValue('');
			}
		}]
	});
	
	
	
	
	newListTemplateSms();
</script>