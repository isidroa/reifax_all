<?php
include "../../../properties_conexion.php";
conectar();

$userid =  $_POST['userid'];
$signs  = null;

$query  = "SELECT * FROM xima.contracts_phonesettings
				WHERE userid = $userid";
$rs     = mysql_query($query);
$row    = mysql_fetch_array($rs);

$sms_domain=$row['sms_domain'];

if($sms_domain!=''){
	if($sms_domain=='txt.voice.google.com'){
		$type_sms='1';
	}else{
		$type_sms='0';
	}
}else{
	$type_sms='0';
}

$fax_domain=$row['fax_domain'];
if($fax_domain!=''){
	if($fax_domain=='imap.gmail.com'){
		$type_fax='1';
	}else{
		$type_fax='0';
	}
}else{
	$type_fax='0';
}
?>
<div id="div_list_smssett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_list_voicesett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_list_faxsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">
var smsStore = new Ext.data.SimpleStore({
	fields : ['id','name','domain'],
	data   : [
		['1','Google','txt.voice.google.com'],
		['0','Other','']
	]
});
var faxStore = new Ext.data.SimpleStore({
	fields : ['id','name','domain'],
	data   : [
		//['1','Google','txt.voice.google.com'],
		['0','Other','']
	]
});

var smssettForm = new Ext.FormPanel({
	title      : 'Google voice SMS Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the SMS Delivery.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'sms_service',
			id            : 'list_sms_service',
			hiddenName    : 'himap_service',
			fieldLabel    : 'Mail Service',
			store         : smsStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_sms?>',
			listeners     : {
				'select'  : selectSms
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_sms=='0' ? 'false' : 'true';?>,
			id: 'sms_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Phone Number',
				name       : 'sms_phone',
				id         : 'list_sms_phone',
				width      : '200',
				value      : '<?=addslashes($row['sms_phone'])?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Domain',
				id         : 'list_sms_domain',
				name       : 'sms_domain',
				width      : '200',
				value      : '<?=$row['sms_domain']?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with txt.voice.yourcompany.com)</div>'
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save SMS Settings</b></span>',
		handler : function(){
			if (smssettForm.getForm().isValid()) {
				smssettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					params: {
						userid: <?=$userid?>
					},
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = Ext.getCmp("tabsListFollowSettings").getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_phonesettings.php', 
							cache : false,
							params: {
								userid: <?=$userid?>
							},
						});
					}
				});
			}
		}
	}]
});
var voicesettForm = new Ext.FormPanel({
	title      : 'Google Voice message Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the email information to enable the Voice mail.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_sms=='0' ? 'false' : 'true';?>,
			id: 'list_voice_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Email',
				name       : 'voice_email',
				id         : 'voice_email',
				width      : '200',
				value      : '<?=addslashes($row['voice_email'])?>',
				allowBlank : false
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Voice Settings</b></span>',
		handler : function(){
			if (voicesettForm.getForm().isValid()) {
				voicesettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					params: {
						userid: <?=$userid?>
					},
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = Ext.getCmp("tabsListFollowSettings").getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_phonesettings.php', 
							cache : false,
							params: {
								userid: <?=$userid?>
							},
						});
					}
				});
			}
		}
	}]
});
var faxsettForm = new Ext.FormPanel({
	title      : 'Google voice Fax Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Fax Delivery.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'fax_service',
			id            : 'list_fax_service',
			hiddenName    : 'hfax_service',
			fieldLabel    : 'Mail Service',
			store         : faxStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_fax?>',
			listeners     : {
				'select'  : selectFax
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_fax=='0' ? 'false' : 'true';?>,
			id: 'fax_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Sender Domain',
				id         : 'list_fax_domain',
				name       : 'fax_domain',
				width      : '200',
				value      : '<?=$row['fax_domain']?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Receiver Domain',
				id         : 'list_fax_domain_receiver',
				name       : 'fax_domain_receiver',
				width      : '200',
				value      : '<?=$row['fax_domain_receiver']?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your domain with yourcompany.com)</div>'
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Fax Settings</b></span>',
		handler : function(){
			if (faxsettForm.getForm().isValid()) {
				faxsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					params: {
						userid: <?=$userid?>
					},
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = Ext.getCmp("tabsListFollowSettings").getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_phonesettings.php', 
							cache : false,
							params: {
								userid: <?=$userid?>
							},
						});
					}
				});
			}
		}
	}]
});
function selectSms(combo,record,index){
	//var service=Ext.getCmp('delivery_service').getValue();
	Ext.getCmp('list_sms_domain').setValue(record.get('domain'));
}
function selectFax(combo,record,index){
	//var service=Ext.getCmp('delivery_service').getValue();
	Ext.getCmp('list_fax_domain').setValue(record.get('domain'));
}
smssettForm.render('div_list_smssett');
voicesettForm.render('div_list_voicesett');
faxsettForm.render('div_list_faxsett');
</script>