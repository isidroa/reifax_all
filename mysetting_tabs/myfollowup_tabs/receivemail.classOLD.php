<?php
// Main ReciveMail Class File - Version 1.1 (02-06-2009)
/*
 * File: recivemail.class.php
 * Description: Reciving mail With Attechment
 * Version: 1.1
 * Created: 01-03-2006
 * Modified: 02-06-2009
 * Author: Mitul Koradia
 * Email: mitulkoradia@gmail.com
 * Cell : +91 9825273322
 */
 
/***************** Changes *********************
*
* 1) Added feature to retrive embedded attachment - Changes provided by. Antti <anttiantti83@gmail.com>
* 2) Added SSL Supported mailbox.
*
**************************************************/

class receiveMail
{
	var $server='';
	var $username='';
	var $password='';
	
	var $marubox='';					
	
	var $email='';			
	
	function receiveMail($username,$password,$EmailAddress,$mailserver='localhost',$servertype='pop',$port='110',$ssl = false, $type='INBOX') //Constructure
	{
		if($servertype=='imap')
		{
			if($port=='') $port='143'; 
			$strConnect='{'.$mailserver.':'.$port. '/imap'.($ssl ? "/ssl/novalidate-cert" : "").'}'.$type;
		}
		else
		{
			$strConnect='{'.$mailserver.':'.$port. '/pop3'.($ssl ? "/ssl/novalidate-cert" : "").'}'.$type; 
		}
		$this->server			=	$strConnect;
		$this->username			=	$username;
		$this->password			=	$password;
		$this->email			=	$EmailAddress;
	}
	function connect() //Connect To the Mail Box
	{
		$this->marubox=@imap_open($this->server,$this->username,$this->password);
		
		if(!$this->marubox)
		{
			$error='Error: Connecting to mail server';
			foreach(imap_errors() as $k => $e){
				$error.=', '.$e;
			}
			return $error;
		}
		return true;		
	}
	//Obtener la cantidad total de mails ordenados.
	function getCountHeaderMail($fTime,$type='INBOX'){
		if(!$this->marubox)
			return false;
		
		if($fTime)
			$headers=imap_search($this->marubox, 'ALL SINCE "2012-03-15"');
		elseif($type=='SENT')
			$headers=imap_search($this->marubox, 'ALL SINCE "2012-03-15"');
		else
			$headers=imap_search($this->marubox, 'UNSEEN SINCE "2012-03-15"');
		
		if($headers){
			rsort($headers);
			
			return $headers;
		}else return false;
	}
	
	function getHeaders($mid) // Get Header info
	{
		if(!$this->marubox)
			return false;

		$mail_header=imap_header($this->marubox,$mid);
		$sender=$mail_header->from[0];
		$sender_replyto=$mail_header->reply_to[0];
		if((strtolower($sender->mailbox)!='mailer-daemon' && strtolower($sender->mailbox)!='postmaster') 
		|| (strtolower($sender->mailbox)=='mailer-daemon' && strtolower($sender->personal)=='mail delivery subsystem'))
		{
			$overview = imap_fetch_overview($this->marubox,$mid);
			$overview = $overview[0];
			
			/*print_r(imap_rfc822_parse_headers(imap_fetchheader($this->marubox,$mid)));
			print_r($overview);
			print_r($mail_header);*/
			
			$mail_details=array(
				'from'=>strtolower($sender->mailbox).'@'.$sender->host,
				'fromName'=>iconv_mime_decode($sender->personal,0,"UTF-8"),
				'toOth'=>strtolower($sender_replyto->mailbox).'@'.$sender_replyto->host,
				'toNameOth'=>$sender_replyto->personal,
				'to'=>strtolower($mail_header->toaddress),	
				'date'=>$mail_header->date,
				'subject'=>iconv_mime_decode($mail_header->subject,0,"UTF-8"),
				'in_reply_to'=>$mail_header->in_reply_to,
				'message_id'=>$mail_header->message_id,
				'newsgroups'=>$mail_header->newsgroups,
				'references'=>$mail_header->references,
				'followup_to'=>$mail_header->followup_to,
				'maildate'=>$mail_header->MailDate,
				'msgno'=>$mail_header->Msgno,
				'uid'=>$mail_header->Msgno,
				'recent'=>$mail_header->Recent, 	//recent   - R si es reciente y visto, N si es reciente y no visto, ' ' si no es reciente.
				'unseen'=>$mail_header->Unseen, 		//seen     - U si no est� visto Y no es reciente, ' ' si est� visto O no visto y es reciente
				'seen'=>$mail_header->Seen, 
				'flagged'=>$mail_header->Flagged, 	//flagged  - F si est� marcado, ' ' si no est� marcado
				'answered'=>$mail_header->Answered, //answered - A si est� respondido, ' ' si no est� respondido
				'deleted'=>$mail_header->Deleted,	//Deleted  - D si est� borrado, ' ' si no est� borrado
				'draft'=>$mail_header->Draft,	 	//Draft    - X si es un borrador, ' ' si no es un borrador
				'size'=>$mail_header->Size,
				'udate'=>$mail_header->udate,
				'datereifax'=>date('Y-m-d H:i:s',strtotime($mail_header->date))					
			);
		}
		//marcar como leidos los descargados inicialmente.
		@imap_setflag_full($this->marubox,$mid,'\\Seen');
		
		return $mail_details;
	}

	function get_mime_type(&$structure) //Get Mime type Internal Private Use
	{ 
		$primary_mime_type = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"); 
		
		if($structure->subtype) { 
			return $primary_mime_type[(int) $structure->type] . '/' . $structure->subtype; 
		} 
		return "TEXT/PLAIN"; 
	} 
	function get_part($stream, $msg_number, $mime_type, $structure = false, $part_number = false) //Get Part Of Message Internal Private Use
	{ 
		if(!$structure) { 
			$structure = imap_fetchstructure($stream, $msg_number); 
		} 
		if($structure) { 
			if($mime_type == $this->get_mime_type($structure))
			{ 
				if(!$part_number) 
				{ 
					$part_number = "1"; 
				} 
				$text = imap_fetchbody($stream, $msg_number, $part_number); 
				if($structure->encoding == 3) 
				{ 
					return imap_base64($text); 
				} 
				else if($structure->encoding == 4) 
				{ 
					return imap_qprint($text); 
				} 
				else
				{ 
					return $text; 
				} 
			} 
			if($structure->type == 1) /* multipart */ 
			{ 
				while(list($index, $sub_structure) = each($structure->parts))
				{ 
					if($part_number)
					{ 
						$prefix = $part_number . '.'; 
					} 
					$data = $this->get_part($stream, $msg_number, $mime_type, $sub_structure, $prefix . ($index + 1)); 
					if($data)
					{ 
						return $data; 
					} 
				} 
			} 
		} 
		return false; 
	} 
	
	function GetAttach($mid,$path) // Get Atteced File from Mail
	{
		if(!$this->marubox)
			return false;

		$struckture = imap_fetchstructure($this->marubox,$mid);
		//print_r($struckture);
		$ar="";
		if($struckture->parts)
        {
			foreach($struckture->parts as $key => $value)
			{
				$enc=$struckture->parts[$key]->encoding;
				if($struckture->parts[$key]->ifdparameters || $struckture->parts[$key]->ifparameters)
				{
					if($struckture->parts[$key]->ifdparameters){
						$name=$struckture->parts[$key]->dparameters[0]->value;
						$att=$struckture->parts[$key]->dparameters[0]->attribute;
					}else{
						$name=$struckture->parts[$key]->parameters[0]->value;
						$att=$struckture->parts[$key]->parameters[0]->attribute;
					}
						
					if($att=='NAME' || $att=='FILENAME'){
						$message = imap_fetchbody($this->marubox,$mid,$key+1);
						//print_r($message);
						if ($enc == 0)
							$message = imap_8bit($message);
						if ($enc == 1)
							$message = imap_8bit ($message);
						if ($enc == 2)
							$message = imap_binary ($message);
						if ($enc == 3)
							$message = imap_base64 ($message); 
						if ($enc == 4)
							$message = quoted_printable_decode($message);
						if ($enc == 5)
							$message = $message;
						//print_r($message);
						$fp=fopen($path.$name,"w");
						fwrite($fp,$message);
						fclose($fp);
						$ar=$ar.$name.",";
					}
				}
				// Support for embedded attachments starts here
				if($struckture->parts[$key]->parts)
				{
					foreach($struckture->parts[$key]->parts as $keyb => $valueb)
					{
						
						$enc=$struckture->parts[$key]->parts[$keyb]->encoding;
						if($struckture->parts[$key]->parts[$keyb]->ifdparameters || $struckture->parts[$key]->parts[$keyb]->ifparameters)
						{
							if($struckture->parts[$key]->parts[$keyb]->ifdparameters){
								$name=$struckture->parts[$key]->parts[$keyb]->dparameters[0]->value;
								$att=$struckture->parts[$key]->parts[$keyb]->dparameters[0]->attribute;
							}else{
								$name=$struckture->parts[$key]->parts[$keyb]->parameters[0]->value;
								$att=$struckture->parts[$key]->parts[$keyb]->parameters[0]->attribute;
							}
							
							if($att=='NAME' || $att=='FILENAME'){
								$partnro = ($key+1).".".($keyb+1);
								$message = imap_fetchbody($this->marubox,$mid,$partnro);
								if ($enc == 0)
									   $message = imap_8bit($message);
								if ($enc == 1)
									   $message = imap_8bit ($message);
								if ($enc == 2)
									   $message = imap_binary ($message);
								if ($enc == 3)
									   $message = imap_base64 ($message);
								if ($enc == 4)
									   $message = quoted_printable_decode($message);
								if ($enc == 5)
									   $message = $message;
								$fp=fopen($path.$name,"w");
								fwrite($fp,$message);
								fclose($fp);
								$ar=$ar.$name.",";
							}
						}
					}
				}				
			}
		}
		$ar=substr($ar,0,(strlen($ar)-1));
		return $ar;
	}
	function getBody($mid) // Get Message Body
	{
		if(!$this->marubox)
			return false;

		$body = $this->get_part($this->marubox, $mid, "TEXT/HTML");
		if ($body == "")
			$body = $this->get_part($this->marubox, $mid, "TEXT/PLAIN");
		if ($body == "") { 
			return "";
		}
		return $body;
		//return imap_body($this->marubox, $mid, FT_UID);
	}
	function deleteMails($mid) // Delete That Mail
	{
		if(!$this->marubox)
			return false;
	
		imap_delete($this->marubox,$mid);
	}
	function close_mailbox() //Close Mail Box
	{
		if(!$this->marubox)
			return false;

		imap_close($this->marubox,CL_EXPUNGE);
	}
}
?>