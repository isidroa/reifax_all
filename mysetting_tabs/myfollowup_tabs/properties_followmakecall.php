<?php
	include_once "../../properties_conexion.php"; 
	include_once 'class.googlevoice.php';
	conectar();
	$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	$phone = $_POST['phone'];
	
	$query='SELECT m.username, m.password, c.forward_phone 
			FROM xima.contracts_phonesettings c
			inner join xima.contracts_mailsettings m on c.userid=m.userid
			where m.userid='.$userid;
	$result = mysql_query($query) or die($query.mysql_error());
	
	if(mysql_num_rows($result)==0){
		$resp = array('success'=>'false','msg'=>'Not provided the configuration of phone or mail settings, please provide it in Follow Up - My Settings'); 
	}else{
		$r=mysql_fetch_array($result);
		if(trim($r['forward_phone'])==''){
			$resp = array('success'=>'false','msg'=>'Forward phone not provided, please provide it in Follow Up - My Settings - Phone Settings'); 
			echo json_encode($resp);
			return;
		}
		$gv = new GoogleVoice($r['username'], $r['password']);
		
		//Obtener id de la ultima llamda actual
		$xml = $gv->get_calls();
	
		$dom = new DOMDocument();
		 
		// load the "wrapper" xml (contains two elements, json and html)
		$dom->loadXML($xml);
		$json = $dom->documentElement->getElementsByTagName("json")->item(0)->nodeValue;
		$json = json_decode($json);
		 
		// now make a dom parser which can parse the contents of the HTML tag
		$html = $dom->documentElement->getElementsByTagName("html")->item(0)->nodeValue;
		// replace all "&" with "&" so it can be parsed
		$html = str_replace("&", "&", $html);
		$dom->loadHTML($html);
		$xpath = new DOMXPath($dom);
		 
		$calls = array();
		 
		// Call duration isn't included, so foreach $json->messages, getting gc-message-call-details for each element
		foreach( $json->messages as $mid=>$convo )
		{
			//find this conversation by message id
			$elements = $xpath->query("//div[@id='$mid']");
			if(!is_null($elements))
			{
				$element = $elements->item(0);
				//find the gc-message-call-details contents
				$XMsg = $xpath->query("//span[@class='gc-message-call-details']", $element);
				//add details to the convo
				$convo->details = $XMsg->item(0)->nodeValue;
			}
			//add $convo to results
			$calls[] = $convo;
		}
		$idLastCall=$calls[0]->id;
		
		$gv->phone($r['forward_phone'], $phone); 
		
		$respuesta = json_decode($gv->status,true);

		if($respuesta['ok']==true){
			$resp = array('success'=>'true','idLastCall'=>$idLastCall,'msg'=>''); 
		}else{
			$resp = array('success'=>'false','msg'=>'Error connecting the call', 'resp'=>json_encode($respuesta), 'error'=>$gv->error); 
		}
	}
		
	echo json_encode($resp);
?>