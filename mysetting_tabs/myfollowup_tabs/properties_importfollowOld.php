<?php
	include("../../properties_conexion.php");
	conectar();
	
	$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	//llenar combo de estado con los estados que tiene el usuario
	if($_COOKIE['datos_usr']['idusertype']!=1 && $_COOKIE['datos_usr']['idusertype']!=4 && $_COOKIE['datos_usr']['idusertype']!=7){ 
		$query = "Select us.idstate, ls.State
		From xima.userstate us 
		INNER JOIN xima.lsstate ls ON (us.idstate=ls.idstate)
		WHERE us.userid=".$_COOKIE['datos_usr']["USERID"];
	}else{
		$query = "Select ls.idstate, ls.State
		From xima.lsstate ls 
		ORDER BY ls.State";	 
	}
	$result = mysql_query($query) or die($query." ".mysql_error());

	$states="['0','Select'],";
	while($row=mysql_fetch_array($result, MYSQL_ASSOC)){
		$states.="['".$row['idstate']."','".$row['State']."'],";
	}
	
	//determinar los defcounty y defstate
	$query = "Select uc.idcounty,lc.idstate 
	From xima.usercounty uc 
	INNER JOIN xima.lscounty lc ON (uc.idcounty=lc.idcounty)
	WHERE uc.defcounty=1 AND uc.userid=".$_COOKIE['datos_usr']["USERID"];
	$result = mysql_query($query) or die($query." ".mysql_error());
	$r=mysql_fetch_array($result);
	
	$importstate  = $r['idstate'];
	$importcounty = $r['idcounty'];
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowimport_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowimport_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowimport_filters"></div><br />
        <div id="myfollowimport_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	
       	</div>
	</div>
</div>
<script>
	var useridspider= <?php echo $userid;?>;
	var importcounty=<?php echo strlen($defcounty)>0 ? $defcounty : 1;?>;
	var importstate=<?php echo strlen($defstate)>0 ? $defstate : 1;?>;
	var states=new Ext.data.ArrayStore({
										fields: ['id','name'],
										data  : [<?php echo rtrim($states,','); ?>]
									});
	
	var condados=new Ext.data.JsonStore({  
        url:'mysetting_tabs/myaccount_tabs/getcountys.php',  
        root:'results',  
        fields: ['id','name'],
		listeners: {
			'load' : function (store,data,obj){
				Ext.getCmp('importcounty').setValue(importcounty);
			}
		}
    });
	
	condados.load({params:{selectstate:importstate}});
	
	var formmyfollowcontract = new Ext.FormPanel({
		url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
		frame:true,
		renderTo: 'myfollowimport_filters',
		bodyStyle:'padding:5px 5px 0;',
		title: 'Import Options',
		id: 'formmyfollowimport',
		name: 'formmyfollowimport',
		layout: 'table',
		layoutConfig: {columns:4},
		defaults: {width: 200},
		items:[{
				//html: '<p style="text-align:left; color: #2B73E6;" >Each property in a different line.</p>',
				colspan: 4,
				width: 850,
				items:[
					new Ext.Button({
							tooltip: 'View Help',
							cls:'x-btn-text-icon',
							iconAlign: 'left',
							text: ' ',
							width: 30,
							height: 30,
							icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
							scale: 'medium',
							handler: function(){
								//alert(user_loged+"||"+user_block+"||"+user_web);
								if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								showVideoHelp('ImportData');
							}
					})
				]
			},{
				layout	: 'form',
				id		: 'fstateimport',
				labelWidth: 50,
				items	: [{
					xtype         : 'combo',
					name		  : 'importstate',
					id			  : 'importstate',
					mode          : 'local',
					fieldLabel    : 'State',
					triggerAction : 'all',
					store         : states,
					displayField  : 'name',
					valueField    : 'id',
					hiddenName    : 'fstate',
					allowBlank    : false,
					editable	  : false,
					value: importstate,
					width: 120,			
					listeners	  : {
						'select'  : function(combo,record,index){
							importcounty=0;
							condados.load({params:{selectstate:Ext.getCmp('importstate').getValue()}});
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fcountyimport',
				labelWidth: 50,
				items	: [
					{
						xtype: 'combo',
						name: 'importcounty',
						id: 'importcounty',
						hiddenName	:'condado',
						fieldLabel: 'County',
						typeAhead: true,
						store: condados,
						triggerAction: 'all',
						mode: 'local',
						editable: false,
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank: false,
						displayField:'name',
						valueField: 'id',
						allowBlank    : false,
						value: importcounty,
						width: 120,
						listeners	  : {
							'select'  : function(combo,record,index){
								
							}
						}
					}
				]
			},{
				layout	: 'form',
				id		: 'ftypeimport',
				labelWidth: 50,
				items	: [{
					xtype         : 'combo',
					name		  : 'importtype',
					id			  : 'importtype',
					mode          : 'local',
					fieldLabel    : 'Type',
					triggerAction : 'all',
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Parcel Id'],
							['1','Property Address']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					hiddenName    : 'fstate',
					allowBlank    : false,
					editable	  : false,
					width: 120,
					value: '0'
				}]
			},{
				layout	: 'form',
				id		: 'fsourceimport',
				labelWidth: 50,
				items	: [{
					xtype         	: 'combo',
					name		  	: 'importsource',
					id			  	: 'importsource',
					mode          	: 'local',
					fieldLabel    	: 'To',
					triggerAction 	: 'all',
					store         	: new Ext.data.ArrayStore({
						id        	: 0,
						fields    	: ['valor', 'texto'],
						data      	: [
							['B','Buying'],
							['S','Selling'],
							['M','Mailing']
						]
					}),
					displayField 	: 'texto',
					valueField   	: 'valor',
					allowBlank    	: false,
					editable	  	: false,
					width			: 120,
					value			: 'B',
					listeners	  	: {
						'select'  : function(combo,record,index){
							var val 	= record.get('valor');
							var camp 	= Ext.getCmp('fcampimport')
							if(val == 'M')
								camp.setVisible(true);
							else
								camp.setVisible(false);
							//Ext.getCmp('formmyfollowimport').doLayout();
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fcampimport',
				colspan	: 4,
				hidden	: true,
				width: 800,
				items	: [{
					xtype	  :	'textfield',
					width	  : 750,
					name	  : 'importcamp',
					id		  : 'importcamp',	
					fieldLabel: 'Campaigns'
				}]
			},{
				html: '<p style="text-align:center; color: #2B73E6;" >Each property in a different line.</p>',
				colspan: 4,
				width: 850,
			},{
				layout	: 'form',
				id		: 'fdataimport',
				colspan	: 4,
				width: 800,
				items	: [{
					xtype	  :	'textarea',
					height	  : 300,
					width	  : 750,
					name	  : 'dataimport',
					id		  : 'dataimport',	
					fieldLabel: 'Data to Import',
					enableKeyEvents: true,
					autoScroll: true
				}]
			}
		],
		buttons:[
			{
				text		  : 'Import',
				handler  	  : function(){
					countProgressBarImport=0;
					registros = [];
					registrosProblema = [];
					registrosExisten = [];
					conseguidos = 0;
					noconseguidos = 0;
					multiples = 0;
					importados = 0;
					var valor = Ext.getCmp('dataimport').getValue();
					var split = valor.split('\n');
					for (var i = 0; i < split.length; i++)
						if (split[i] && split[i].trim()!='') registros.push(split[i]);
					
					totalProcesar = registros.length;
					if(totalProcesar>100 && useridspider!=3264){
						totalProcesar = 100;
					}else if(totalProcesar>1000 && useridspider==3264){
						totalProcesar = 1000;
					}
					if(totalProcesar>0){
						var source = Ext.getCmp('importsource').getValue();
						var camp = Ext.getCmp('importcamp').getValue();
						
						if(source != 'M' || (source == 'M' && camp.length > 0)){							
							progressBarImportFollow_win.show();
							progressBarImportFollow.updateProgress(
								((countProgressBarImport+1)/totalProcesar),
								"Processing "+(countProgressBarImport+1)+" of "+totalProcesar
							);
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_getimport.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									dataimport: registros[countProgressBarImport],
									importtype: Ext.getCmp('importtype').getValue(),
									importcounty: Ext.getCmp('importcounty').getValue(),
									importstate: Ext.getCmp('importstate').getValue(),
									importsource: Ext.getCmp('importsource').getValue(),
									importcamp: Ext.getCmp('importcamp').getValue(),
									userid: <?php echo $userid;?>,
									type: 'import'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success: ejecutarImport
							});
						}else
							Ext.MessageBox.alert('Warning','You need a campaign name to import properties to the mail campaign.');
					}else
						Ext.MessageBox.alert('Warning','At least you have a property to import.');
				}				
			},{
				text		  : 'Clear',
				handler  	  : function(){
					Ext.getCmp('dataimport').setValue('');
				}
			}
		],
		buttonAlign: 'center' 
	})
	
	var countProgressBarImport=0;
	var totalProcesar=0;
	var registros = [];
	var conseguidos = 0;
	var noconseguidos = 0;
	var multiples = 0;
	var importados = 0;
	var registrosProblema = [];
	var registrosExisten = [];
	function ejecutarImport(response,options){
		
		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			if(resp.found>0)
				conseguidos++;
			if(resp.nofound>0)
				noconseguidos++;
			if(resp.multi>0)
				multiples++;
				
			if(resp.ingresados>0)
				importados++;
				
			if(resp.nofound>0 || resp.multi>0)
				registrosProblema.push(registros[countProgressBarImport]);
			
			if(resp.existeimport>0)
				registrosExisten.push(registros[countProgressBarImport]);	
								
			if((countProgressBarImport)==totalProcesar-1){
				progressBarImportFollow_win.hide();
				loading_win.hide();
				var r=Ext.decode(response.responseText);
				var texto='Total Properties: '+totalProcesar+'<br>Properties Found: '+(totalProcesar - noconseguidos)+'<br>Properties not found: '+noconseguidos;
				texto+='<br><br>Total Properties: '+totalProcesar;
				if(multiples>0)
					texto+='<br>Properties with multiple Results: '+multiples;
				if(registrosExisten.length>0)
					texto+='<br>Properties already following: '+registrosExisten.length;	
				texto+='<br>Properties Imported: '+importados;
				
				if(registrosProblema.length>0){
					texto+='<br><br>Not found or multiple: ';
					for (var i = 0; i < registrosProblema.length; i++){
						texto+='<br>'+registrosProblema[i];
					}
				}
				if(registrosExisten.length>0){
					texto+='<br><br>Already following: ';
					for (var i = 0; i < registrosExisten.length; i++){
						texto+='<br>'+registrosExisten[i];
					}
				}
				
				Ext.getCmp('dataimport').setValue('');
				Ext.MessageBox.alert('Follow up',texto);
			}else{
				countProgressBarImport++;
				progressBarImportFollow.updateProgress(
					((countProgressBarImport+1)/totalProcesar),
					"Processing "+(countProgressBarImport+1)+" of "+totalProcesar
				);
				Ext.Ajax.request({  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_getimport.php', 
					method: 'POST',
					timeout :600000, 
					params: { 
						dataimport: registros[countProgressBarImport],
						importtype: Ext.getCmp('importtype').getValue(),
						importcounty: Ext.getCmp('importcounty').getValue(),
						importstate: Ext.getCmp('importstate').getValue(),
						importsource: Ext.getCmp('importsource').getValue(),
						importcamp: Ext.getCmp('importcamp').getValue(),
						userid: <?php echo $userid;?>,
						type: 'import'
					},
					
					failure:function(response,options){
						progressBarImportFollow_win.hide();
						var output = '';
						for (prop in response) {
						  output += prop + ': ' + response[prop]+'; ';
						}
						Ext.MessageBox.alert('Warning',output);
					},
					
					success: ejecutarImport                      
				});
			}
		}else{
			progressBarImportFollow_win.hide();
		}	
	}
	var progressBarImportFollow= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarImportFollow_win=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarImportFollow]
	});		
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowimport_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowimport_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>
