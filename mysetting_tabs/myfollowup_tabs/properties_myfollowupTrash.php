<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
 
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmailTrash_data_div" align="center" style=" background-color:#FFF; margin:auto;">
        <div id="myfollowmail_progressBar"></div>
        <div id="myfollowmail_propertiesTrash" align="left"></div> 
	</div>
</div>
<script>
	var storemyfollowmailTrash = null;
	var limitmyfollowmailTrash = 50;
Ext.onReady(function() {
	var selected_datamyfollowmailTrash 	= new Array();
	var AllCheckmyfollowmailTrash 		= false;
	var totalMailTrash					= 0;
	var currentMailTrash				= 0;
	var sincMailTrash					= 0;
	
//filter variables
var filtersMEO = {
	address		: '',
	pending		: {
		contact		: -1,
		property	: -1
	},
	order		: {
		field		: 'fromName_msg',
		direction	: 'ASC'
	},
	email		: {
		from	: {
			name	: '',
			mail	: ''
		},
		to		: {
			mail	: ''
		},
		dates	: {
			after	: '',
			before	: '',
			type	: 'Equal'
		},
		type	: -1,
		attach	: -1,
		seen	: -1,
		content	: ''
	}
};

	storemyfollowmailTrash = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'to_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'agent', type: 'string'},
			{name: 'ap', type: 'string'},
			{name: 'address', type: 'string'},
			{name: 'task', type: 'int'},
			{name: 'bd', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'type_ot', type: 'string'}
	    ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': 		<?php echo $userid;?>,
			'typeEmail':	2,
			'checkmail': 	0,
			'currentMail': 	currentMailTrash,
			'totalMail':  	totalMailTrash
		},
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				if(sincMailTrash==1) progressBarTrashWin.show();
				
				obj.params.toemail 	= filtersMEO.email.to.mail;
				obj.params.content	= filtersMEO.email.content;
				obj.params.dateAf	= filtersMEO.email.dates.after;
				obj.params.dateBe	= filtersMEO.email.dates.before;
				obj.params.dateTy	= filtersMEO.email.dates.type;
				obj.params.etype	= filtersMEO.email.type;
				obj.params.attach	= filtersMEO.email.attach;
				obj.params.pproperty= filtersMEO.pending.property;
			}, 
			load: function(store,records,opt){
				currentMailTrash = store.reader.jsonData.currentMail;
				if(totalMailTrash == 0) totalMailTrash = store.reader.jsonData.totalMail;

				if(currentMailTrash<totalMailTrash){
					
					progressBarTrash.updateProgress(
						(currentMailTrash/totalMailTrash),
						"Downloading emails "+currentMailTrash+" of "+totalMailTrash
					);
					
					storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash, currentMail:currentMailTrash, totalMail:totalMailTrash,checkmail:sincMailTrash}});
				}else{
					progressBarTrashWin.hide();
					progressBarTrash.updateProgress(
						0,
						"Initializing emails download..."
					);
					currentMailTrash = 0;
					totalMailTrash = 0;
					sincMailTrash=0;
				}
			},
			exception: function(){
				progressBarTrashWin.hide();
				progressBarTrash.updateProgress(
					0,
					"Initializing emails download..."
				);
				currentMailTrash = 0;
				totalMailTrash = 0;
				sincMailTrash=0;
				Ext.Msg.alert('Sync Failed', 'You must set your email configuration in \'Follow up\' -> \'My settings\' -> \'Mail settings\' -> \'Email Reader Configuration\'.');
			}
		}
    });
	
	var progressBarTrash = new Ext.ProgressBar({
		text: "Initializing emails download..."
	});
	
	var progressBarTrashWin=new Ext.Window({
		title: 'Sync Email, please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarTrash]
	});

	var smmyfollowmailTrash = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmailTrash.indexOf(record.get('idmail'))==-1)
					selected_datamyfollowmailTrash.push(record.get('idmail'));
				
				if(Ext.fly(gridmyfollowmailTrash.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailTrash=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmailTrash = selected_datamyfollowmailTrash.remove(record.get('idmail'));
				AllCheckmyfollowmailTrash=false;
				Ext.get(gridmyfollowmailTrash.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}		
	});
	var gridmyfollowmailTrash = new Ext.grid.EditorGridPanel({
		renderTo: 'myfollowmail_propertiesTrash',
		cls: 'grid_comparables',
		height: 3000,
		store: storemyfollowmailTrash,
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowmailTrash
			,{header: 'T', width: 30, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
			,{header: 'A', width: 25, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
			,{header: 'P', width: 25, sortable: true, align: 'center', dataIndex: 'ap', renderer: assignmentProperty}
			,{header: 'H', width: 20, sortable: true, align: 'center', dataIndex: 'parcelid', renderer: assignmentHistory}
			,{id: 'idmail', header: "To Mail", width: 100, align: 'left', sortable: true, dataIndex: 'to_msg'}		
			,{header: 'Subject', width: 630, sortable: true, align: 'left', dataIndex: 'subject'}
			,{header: "Date", width: 60, sortable: true, align: 'center', renderer: dateRender, dataIndex: 'msg_date'}
			,{header: "From ", width: 60, sortable: true, align: 'center', dataIndex: 'type_ot',renderer: assignmentFrom}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowmailTrash',
            pageSize: 		limitmyfollowmailTrash,
            store: 			storemyfollowmailTrash,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display',
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowmailTrash=50;
					Ext.getCmp('pagingmyfollowmailTrash').pageSize = limitmyfollowmailTrash;
					Ext.getCmp('pagingmyfollowmailTrash').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupEmailInbox'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowmailTrash=80;
					Ext.getCmp('pagingmyfollowmailTrash').pageSize = limitmyfollowmailTrash;
					Ext.getCmp('pagingmyfollowmailTrash').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupEmailInbox'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowmailTrash=100;
					Ext.getCmp('pagingmyfollowmailTrash').pageSize = limitmyfollowmailTrash;
					Ext.getCmp('pagingmyfollowmailTrash').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupEmailInbox'
			})]
		}),
		listeners: {
			'sortchange': function (grid, sorted){
				filtersMEO.order.field		= sorted.field;
				filtersMEO.order.direction	= sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = grid.getView().findCellIndex(e.getTarget()); 
				if(cell !== 0 && cell !== 3 &&  cell !== 4){
					Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
					var record = grid.getStore().getAt(rowIndex);
					
					viewMailDetail(record.get('idmail'),record.get('userid'),record.get('ap'),tabsFollowEmails,'',record.get('msg_date'),1,'Mail');
				}
							if(cell === 4){
					
		//Agregado por Luis R Castro Sugerencia 11808 22/05/2015
		
	var IRecord = storemyfollowmailInbox.getAt(rowIndex);
	
		
	var record = grid.getStore().getAt(rowIndex);
	var county = IRecord.get('bd')[rowIndex]; 
	var userid = record.get('userid');
	var typeFollow='S';
	var status = IRecord.get('status')[rowIndex]; 
	var pid = record.get('parcelid');

	if(document.getElementById(id)){
		var tab = tabs.getItem(id);
		tabs.remove(tab);
	}

	//Top Bar principal
	var followTbar = new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'View Contacts',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/agent.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					method: 'POST',
					timeout :86400,
					params: {
						pid: pid,
						userid: userid,
						type: 'assignment'
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var r=Ext.decode(response.responseText);
						creaVentana(0,r,pid,useridspider);
					}
				});
			}
		},{
			 tooltip: 'Short Sale',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/shortsale.png',
			 hidden: true,
			 handler: function(){
				var option = 'width=930px,height=555px';
				 window.open('https://www.reifax.com/xima3/mysetting_tabs/myfollowup_tabs/shortsaleform.php?pid='+pid ,'Signature',option);

			 }
		},'->',{
			 tooltip: 'Close Follow History',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/cancel.png',
			 handler: function(){
				  var tabsMail = Ext.getCmp('tabsFollowEmailsId');
				var tab = tabsMail.getItem(2);
			
			tabsMail.remove(tab);
			 }
		}]
	});


	//Top Bar History Tab
	var tbarhistory =  new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				var idfuh = '';
				if(followfuh.length > 0){
					idfuh=followfuh[0].get('idfuh');
					for(i=1; i<followfuh.length; i++){
						idfuh+=','+followfuh[i].get('idfuh');
					}
				}
				Ext.Ajax.request(
				{
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					method: 'POST',
					timeout :86400,
					params: {
						type: 'delete',
						idfuh: idfuh
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
						followstore.reload();
					}
				});
			 }
		},{
			tooltip: 'New History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Add History',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : '1',
								hiddenName    : 'task',
								hiddenValue   : '1',
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											pid: pid,
											userid: userid,
											type: 'assignment'
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Detail'
							},//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
							{
											xtype		  : 'datefield',
											width		  : 90,
											fieldLabel    : 'Date',
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'dateadd',
											
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													dateadd=newvalue;
												}
											}
									},
							////////////////////////////////////////////////////////7
							{
								xtype	  : 'checkboxgroup',
								fieldLabel: 'Document',
								columns	  : 3,
								itemCls   : 'x-check-group-alt',
								items	  : [
									{boxLabel: 'Contract', name: 'contract'},
									{boxLabel: 'Proof of Funds', name: 'pof'},
									{boxLabel: 'EMD', name: 'emd'},
									{boxLabel: 'Addendums', name: 'rademdums'},
									{boxLabel: 'Offer Received', name: 'offerreceived'}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'insert'
							},{
								xtype     : 'hidden',
								name      : 'parcelid',
								value     : pid
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],

					buttons: [{
							text: 'Add',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow History", 'New Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});

				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 500,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},{
			tooltip: 'Edit History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				if(followfuh.length > 1){
					Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
					return false;
				}else if(followfuh.length == 0){
					Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
					return false;
				}

				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Update History',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0,
								value	  : followfuh[0].get('offer')
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0,
								value	  : followfuh[0].get('coffer')
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : followfuh[0].get('task'),
								hiddenName    : 'task',
								hiddenValue   : followfuh[0].get('task'),
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											pid: pid,
											userid: userid,
											type: 'assignment'
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},
							//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
							{
											xtype		  : 'datefield',
											width		  : 90,
											fieldLabel    : 'Date',
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'dateadd',
											value	  : followfuh[0].get('dateadd'),
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													dateadd=newvalue;
												}
											}
									},
							////////////////////////////////////////////////////////7
							{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'sheduledetail',
								fieldLabel: 'Schedule Detail',
								value	  : followfuh[0].get('sheduledetail')
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Complete Detail',
								value	  : followfuh[0].get('detail')
							},{
								xtype: 'checkboxgroup',
								fieldLabel: 'Document',
								columns: 3,
								itemCls: 'x-check-group-alt',
								items: [
									{boxLabel: 'Contract', name: 'contract', checked: !followfuh[0].get('contract')},
									{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh[0].get('pof')},
									{boxLabel: 'EMD', name: 'emd', checked: !followfuh[0].get('emd')},
									{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh[0].get('realtorsadem')},
									{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh[0].get('offerreceived')}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'update'
							},{
								xtype     : 'hidden',
								name      : 'idfuh',
								value     : followfuh[0].get('idfuh')
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],

					buttons: [{
							text: 'Update',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow Up", 'Updated Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});

				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 500,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},new Ext.Button({
			tooltip: 'Print History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 4,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 4,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore.reload();
			}
		}),
		
		// Agregado por Luis R Castro Sugerencia 11693 18/05/2015
		
		new Ext.Button({
				tooltip: 'Send Contracts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/guarantee.png',
				handler: function(){
					
					
					
					
					
					sendContractJ(pid,'','',useridspider, '', '',Ext.getCmp('mailenviadosfollow'),false);
				}
			}),new Ext.Button({
				tooltip: 'Send Email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/email.png',
				handler: function(){
					
					sendEmail(pid,'','',useridspider, '', '');
				}
			}),new Ext.Button({
				tooltip: 'Send SMS',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/phone.png',
				handler: function(){
					
					sendEmailSms(pid,'','',useridspider, '', '');
				}
			}),new Ext.Button({
				tooltip: 'Send Fax',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/fax.png',
				handler: function(){
										
					sendEmailFax(pid,'','',useridspider, '', '');
				}
			}),
		
		////////////////////////////////////////////////////
		
		
		// Agregado por Luis R Castro Sugerencias 11818 - 11884 12/05/2015
		new Ext.Button({
				tooltip: 'Block Property',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
				
					loading_win.show();
				blockFollow('yes');

					function blockFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'block',
								pids: pid
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
							Ext.Msg.alert("Follow Up", 'Added to Follow Block this property.');
						//storemyfollowup.load({params:{start:0, limit:limitmyfollowup}});
							}                                
						});
					}
				}
			})
		/////////////////////////////////////////////////////////
		]
	});

	//Top Bar Follow Schedule
	var tbarschedule = new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				var idfus = '';
				if(followfus.length > 0){
					idfus=followfus[0].get('idfus');
					for(i=1; i<followfus.length; i++){
						idfus+=','+followfus[i].get('idfus');
					}
				}
				Ext.Ajax.request(
				{
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
					method: 'POST',
					timeout :86400,
					params: {
						type: 'delete',
						idfus: idfus
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
						followstore_schedule.reload();
					}
				});
			 }
		},{
			tooltip: 'New Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var selected = new Array(), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule;
				selected.push(pid);

				createScheduleTask(selected, store, userid, progressBarTask_win_schedule, progressBarTask_schedule, selling);
			 }
		},{
			tooltip: 'Edit Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}

				var idfus = followfus[0].get('idfus'), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule;
				editScheduleTask(idfus, userid, selling, store);
			 }
		},{
			tooltip: 'Complete Task',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/refresh.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections(), selectedAux=new Array();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}

				for(i=0;i<followfus.length;i++){
					selectedAux.push({
						'pid': pid,
						'id': followfus[i].get('idfus'),
						'task': followfus[i].get('task')
					});
				}

				loading_win.show();
				completeMultiTask(selectedAux,followgrid_schedule,followstore_schedule,userid, progressBarTask_win_schedule, progressBarTask_schedule, false);
			 }
		},new Ext.Button({
			tooltip: 'Print Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 5,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 5,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore_schedule.reload();
			}
		}),
		// Agregado por Luis R Castro Sugerencias 11818 - 11884 12/05/2015
		new Ext.Button({
				tooltip: 'Block Property',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
			
					loading_win.show();
				blockFollow('yes');

					function blockFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'block',
								pids: pid
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'Added to Follow Block this property.');
								storemyfollowup.load({params:{start:0, limit:limitmyfollowup}});
							}                                
						});
					}
				}
			})
		/////////////////////////////////////////////////////////
		]
	});
	if(document.getElementById('tabsFollowId')){
		var tabsMail = Ext.getCmp('tabsFollowId');
				var tab = tabsMail.getItem(5);
					tabsMail.remove(tab);
					var tab = tabsMail.getItem(4);
					tabsMail.remove(tab);
	}
	// agregado por Luis R Castro Sugerencia 12053 02/06/2015
	var tabsTrash = Ext.getCmp('tabsFollowEmailsId');
	//Create Tab Follow History with Top Bars
	tabsTrash.add({
		title: ' Follow ',
		id: id,
		closable: true,
		items: [
			new Ext.TabPanel({
				id: 'historyTab'+typeFollow,
				activeTab: 0,
				width: ancho,
				height: tabs.getHeight(),
				plain:true,
				listeners: {
					'tabchange': function( tabpanel, tab ){
						if(tab.id=='followhistoryInnerTab'+typeFollow)
							if(document.getElementById('follow_history_grid')) followstore.reload();
						else if(tab.id=='followsheduleInnerTab'+typeFollow)
							if(document.getElementById('follow_schedule_grid')) followstore_schedule.reload();
					}
				},
				items:[{
					title: 'History',
					id: 'followhistoryInnerTab'+typeFollow,
					autoLoad: {
						url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php',
						scripts	: true,
						params	:{
							parcelid 	: pid,
							userid 		: userid,
							county 		: county,
							typeFollow	: typeFollow
						},
						timeout	: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarhistory
				},{
					title: 'Schedule',
					id: 'followsheduleInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarschedule
				},{
					title: 'Email',
					id: 'followemailInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followemail.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow,
							tab : 'tabsFollowEmailsId',
							from	: 'Follow'
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
				},{
					title: 'Dashboard',
					id: 'followdashboardInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followdashboard.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
			   }]
			})
		],
		tbar: followTbar
	}).show();

		
		
		//////////////////////////////////////////////////////////////////////////////
		
			}
				
			}
		},
		sm: smmyfollowmailTrash,
		frame:false,
		loadMask:true,
		border: false
	});
	
	var TrashTabBar = new Ext.Toolbar({
		id: 'followTrashTbar',
		items:[
			new Ext.Button({
				tooltip: 'Delete Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowmailTrash.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the emails to be eliminated.'); return false;
					}
					loading_win.show();
					
					var pids=selected_datamyfollowmailTrash[0];
					for(i=1; i<selected_datamyfollowmailTrash.length; i++)
						pids+=','+selected_datamyfollowmailTrash[i]; 


					Ext.MessageBox.show({
							title:    'Trash',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will delete all '+selected_datamyfollowmailInbox.length+' selected emails from your Trash.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteTrash
						});					
						
						function deleteTrash(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						/////////////////////////////////////////////////////////////////////////////
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							selected_datamyfollowmailTrash 	= new Array();
							storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash}});
							Ext.Msg.alert("My email - Trash", 'Emails deleted.');
							
						}                                
					});
					}
				}
			}),
			new Ext.Button({
				tooltip: 'Filter Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					
					var formmyfollowmailTrash = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						id: 'formmyfollowmailTrash',
						name: 'formmyfollowmailTrash',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'To mail',
									name		  : 'femail',
									value		  : filtersMEO.email.to.mail,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMEO.email.to.mail = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email Content',
									name		  : 'fcontent',
									value		  : filtersMEO.email.content,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMEO.email.content = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Email Type',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											[-1,'All'],
											[1,'SMS'],
											[3,'Fax'],
											[5,'Email'],
											[15,'Voice Mail']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'etypename',
									value         : filtersMEO.email.type,
									hiddenName    : 'etype',
									hiddenValue   : filtersMEO.email.type,
									allowBlank    : false,
									width		  : 150,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.email.type = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fattach',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Attachment',
									triggerAction : 'all',
									width		  : 150,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fattachname',
									value         : filtersMEO.email.attach,
									hiddenName    : 'fattach',
									hiddenValue   : filtersMEO.email.attach,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.email.attach = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fpendingproperty',
								colspan	: 2,
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Pending Property',
									triggerAction : 'all',
									width		  : 150,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpendingpropertyname',
									value         : filtersMEO.pending.property,
									hiddenName    : 'fpendingproperty',
									hiddenValue   : filtersMEO.pending.property,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.pending.property = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Email Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fdatecname',
										hiddenName: 'fdatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersMEO.email.dates.type,
										listeners: {
											'select': function (combo,record,index){
												filtersMEO.email.dates.type = record.get('id');
												var secondfield = Ext.getCmp('fdatea');
												secondfield.setValue('');
												filtersMEO.email.dates.after='';
												
												if(filtersMEO.email.dates.type=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fdate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fdateb',
											value		  : filtersMEO.email.dates.before,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersMEO.email.dates.before=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fdatea',
										name		  : 'fdatea',
										hidden		  : filtersMEO.email.dates.type!='Between',
										value		  : filtersMEO.email.dates.after,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersMEO.email.dates.after=newvalue;
											}
										}
									}]
								}]
							}]	
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(b){
									storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash}});
									b.findParentByType('window').close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(b){
									filtersMEO.email.to.mail 		= '';
									filtersMEO.email.content 		= '';
									filtersMEO.email.dates.after 	= '';
									filtersMEO.email.dates.before	= '';
									filtersMEO.email.dates.type		= 'Equal';
									filtersMEO.email.attach			= -1;
									filtersMEO.email.type			= -1;
									filtersMEO.pending.property		= -1;
									
									b.findParentByType('form').getForm().reset();
									
									storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash}});
									b.findParentByType('window').close();
								}
							},{
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Close&nbsp;&nbsp; ',
								handler  	  : function(b){
									b.findParentByType('window').close();
								}
							}
						]
					});
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 650,
						height      : 280,
						modal	 	: true,  
						plain       : true,
						items		: formmyfollowmailTrash,
						closeAction : 'close'
					});
					win.show();
				}
			}),
			new Ext.Button({
				tooltip: 'Reset Filter Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersMEO.email.to.mail 		= '';
					filtersMEO.email.content 		= '';
					filtersMEO.email.dates.after 	= '';
					filtersMEO.email.dates.before	= '';
					filtersMEO.email.dates.type		= 'Equal';
					filtersMEO.email.attach			= -1;
					filtersMEO.email.type			= -1;
					filtersMEO.pending.property		= -1;
					
					storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash}});
				}
			}),
			new Ext.Button({
				tooltip: 'Compose Email',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/composeemail.png',
				handler: function(){
					mailCompose(<?php echo $userid;?>,-1,'-1',false,false,'','');
				}
			}),// Agregado por Luis R Castro Sugerencia 12625 10/06/2015
			
			new Ext.Button({
				tooltip: 'Move Back Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/propertyAssign.png',
				handler: function(){
					if(selected_datamyfollowmailTrash.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the emails to be moved.'); return false;
					}
					loading_win.show();
					
					var pids=selected_datamyfollowmailTrash[0];
					for(i=1; i<selected_datamyfollowmailTrash.length; i++)
						pids+=','+selected_datamyfollowmailTrash[i]; 
						var typeEmail='0';
						
						
					Ext.MessageBox.show({
							title:    'Trash',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will move all '+selected_datamyfollowmailInbox.length+' selected emails from your Trash.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteTrash
						});					
						
						function deleteTrash(btn){
							
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
							
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'move-back',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							selected_datamyfollowmailTrash 	= new Array();
							storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash}});
							Ext.Msg.alert("My email - Trash", 'Emails moved.');
							
						}                                
					
					});
					
					}
				}
			})			
			////////////////////////////////////////////////////////////////
		]
	});
	Ext.getCmp('followTrash').add(TrashTabBar);
	Ext.getCmp('followTrash').doLayout();
	
		
	storemyfollowmailTrash.load({params:{start:0, limit:limitmyfollowmailTrash, currentMail:currentMailTrash, totalMail:totalMailTrash,checkmail:sincMailTrash}});	
});
</script>