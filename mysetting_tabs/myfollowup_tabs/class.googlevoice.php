<?PHP
/*
Version     0.2
License     This code is released under the MIT Open Source License. Feel free to do whatever you want with it.
Author      lostleon@gmail.com, http://www.lostleon.com/
LastUpdate  05/28/2010
*/
class GoogleVoice
{
    public $username;
    public $password;
    public $status;
	public $error;
    private $lastURL;
    private $login_auth;
    private $inboxURL = 'https://www.google.com/voice/m/';
    private $loginURL = 'https://www.google.com/accounts/ClientLogin';
    private $smsURL = 'https://www.google.com/voice/m/sendsms';
    private $voiceURL = 'https://www.google.com/voice/call/connect/';

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getLoginAuth()
    {
        $login_param = "accountType=GOOGLE&Email=".urlencode($this->username)."&Passwd=".urlencode($this->password)."&service=grandcentral&source=com.lostleon.GoogleVoiceTool";
        $ch = curl_init($this->loginURL);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11 Safari/525.20");
        curl_setopt($ch, CURLOPT_REFERER, $this->lastURL);
		
		//Prueba correct error "Moved Temporarily"
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
        
		curl_setopt($ch, CURLOPT_POST, "application/x-www-form-urlencoded");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $login_param);
        $html = curl_exec($ch);
        $this->lastURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        $this->login_auth = $this->match('/Auth=([A-z0-9_-]+)/', $html, 1);
        return $this->login_auth;
    }

    public function get_rnr_se()
    {
        $this->getLoginAuth();
        $ch = curl_init($this->inboxURL);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers = array("Authorization: GoogleLogin auth=".urlencode($this->login_auth), 'User-Agent: Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11 Safari/525.20');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $html = curl_exec($ch);
        $this->lastURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        $_rnr_se = $this->match('!<input.*?name="_rnr_se".*?value="(.*?)"!ms', $html, 1);
        return $_rnr_se;
    }
	
	public function get_calls()
    {
        $this->getLoginAuth();
        $ch = curl_init($this->inboxURL);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers = array("Authorization: GoogleLogin auth=".urlencode($this->login_auth), 'User-Agent: Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11 Safari/525.20');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
        $html = curl_exec($ch);
		//print_r($html);
        $this->lastURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        //curl_close($ch);
        $_rnr_se = $this->match('!<input.*?name="_rnr_se".*?value="(.*?)"!ms', $html, 1);
        //return $_rnr_se;
		curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/voice/inbox/recent/all/');
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$xml = curl_exec($ch);
		//echo $xml;
		return $xml;
    }
	
    private function GetData($url, $postdata){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers = array("Authorization: GoogleLogin auth=".urlencode($this->login_auth), 'User-Agent: Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11 Safari/525.20');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_REFERER, $this->lastURL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $this->status = curl_exec($ch);
		$this->error = curl_error($ch);
        $this->lastURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
    }

    public function sms($to_phonenumber, $smstxt)
    {
        $_rnr_se = $this->get_rnr_se();
        $sms_param = "id=&c=&number=".urlencode($to_phonenumber)."&smstext=".urlencode($smstxt)."&_rnr_se=".urlencode($_rnr_se);
        $this->GetData($this->smsURL, $sms_param);
        return $this->status;
    }

    public function phone($to_phonenumber,$outgoing_number){
        $_rnr_se = $this->get_rnr_se();
        $phone_param = "phoneType=1&subscriberNumber=undefined&remember=0&forwardingNumber=".urlencode('+1'.$to_phonenumber)."&outgoingNumber=".urlencode($outgoing_number)."&_rnr_se=".urlencode($_rnr_se);
        $this->GetData($this->voiceURL, $phone_param);
        return $this->status;
    }

    private function match($regex, $str, $out_ary = 0)
    {
        return preg_match($regex, $str, $match) == 1 ? $match[$out_ary] : false;
    }
}
?>
