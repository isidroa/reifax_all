<?php
	$userid=$_COOKIE['datos_usr']['USERID']; 
?>
<style>
.x-grid3-cell-inner {
  padding: 1px; 
}
</style>
<div align="left" id="todo_myblockagent_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myblockagent_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myblockagent_filters"></div><br />
        <div id="myblockagent_properties" align="left"></div> 
	</div>
</div>
<script>
	var limitmyblockagent = 50;
	var selected_datamyblockagent = new Array();
	var AllCheckmyblockagent = false;
	
	var filterfield='agent';
	var filterdirection='ASC';
	
	var filteragentmyblockagent 	= '';
	var filteremailmyblockagent 	= '';
	var filterphonemyblockagent 	= '';
	var filterwebmyblockagent 	= '';
	var filteraddressmyblockagent 	= '';
	var filtercompanymyblockagent 	= '';
	var filtertypemyblockagent 	= 'ALL';
	
	var storemyblockagent = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_blockagent.php',
		fields: [
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				storemyblockagentAll.load();
				AllCheckmyblockagent=false;
				selected_datamyblockagent=new Array();
				smmyblockagent.deselectRange(0,limitmyblockagent);
				obj.params.agent = filteragentmyblockagent;
				obj.params.email = filteremailmyblockagent;
				obj.params.phone = filterphonemyblockagent;
				obj.params.web = filterwebmyblockagent;
				obj.params.address = filteraddressmyblockagent;
				obj.params.company = filtercompanymyblockagent;
				obj.params.typeagent = filtertypemyblockagent;
			},
			'load' : function (store,data,obj){
				if (AllCheckmyblockagent){
					Ext.get(gridmyblockagent.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyblockagent=true;
					gridmyblockagent.getSelectionModel().selectAll();
					selected_datamyblockagent=new Array();
				}else{
					AllCheckmyblockagent=false;
					Ext.get(gridmyblockagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyblockagent.length > 0){
						for(val in selected_datamyblockagent){
							var ind = gridmyblockagent.getStore().find('agentid',selected_datamyblockagent[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyblockagent.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storemyblockagentAll = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_blockagent.php',
		fields: [
           	{name: 'agentid', type: 'int'},
			{name: 'agent'},
			{name: 'email'},
			{name: 'tollfree'},
			{name: 'phone1'},
			{name: 'typeph1', type: 'int'},
			{name: 'phone2'},
			{name: 'typeph2', type: 'int'},
			{name: 'phone3'},
			{name: 'typeph3', type: 'int'},
			{name: 'fax'},
			{name: 'typeph4', type: 'int'},
			{name: 'typeph5', type: 'int'},
			{name: 'phone6'},
			{name: 'typeph6', type: 'int'},
			{name: 'typeemail1', type: 'int'},
			{name: 'email2'},
			{name: 'typeemail2', type: 'int'},
			{name: 'urlsend'},
			{name: 'typeurl1', type: 'int'},
			{name: 'urlsend2'},
			{name: 'typeurl2', type: 'int'},
			{name: 'address1'},
			{name: 'typeaddress1', type: 'int'},
			{name: 'address2'},
			{name: 'typeaddress2', type: 'int'},
			{name: 'company'},
			{name: 'agentype'},
			{name: 'agenttype', type: 'int'}
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'agent',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.agent = filteragentmyblockagent;
				obj.params.email = filteremailmyblockagent;
				obj.params.phone = filterphonemyblockagent;
				obj.params.web = filterwebmyblockagent;
				obj.params.address = filteraddressmyblockagent;
				obj.params.company = filtercompanymyblockagent;
				obj.params.typeagent = filtertypemyblockagent;
			}
		}
    });
	var smmyblockagent = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyblockagent.indexOf(record.get('agentid'))==-1)
					selected_datamyblockagent.push(record.get('agentid'));
				
				if(Ext.fly(gridmyblockagent.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyblockagent=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyblockagent = selected_datamyblockagent.remove(record.get('agentid'));
				AllCheckmyblockagent=false;
				Ext.get(gridmyblockagent.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtagents=new Ext.Toolbar({
		renderTo: 'myblockagent_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyblockagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the Contacts to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var agentids=selected_datamyblockagent[0];
					for(i=1; i<selected_datamyblockagent.length; i++)
						agentids+=','+selected_datamyblockagent[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_blockagent.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
							Ext.Msg.alert("Follow Contact", 'Contact delete.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Print Report',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyblockagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to print.'); return false;
					}
					if(AllCheckmyblockagent==true){
						selected_datamyblockagent=new Array();
						var totales = storemyblockagentAll.getRange(0,storemyblockagentAll.getCount());
						
						for(i=0;i<storemyblockagentAll.getCount();i++){
							if(selected_datamyblockagent.indexOf(totales[i].data.agentid)==-1)
								selected_datamyblockagent.push(totales[i].data.agentid);
						}
					
					}
					var agentids=selected_datamyblockagent[0];
					for(i=1; i<selected_datamyblockagent.length; i++)
						agentids+=','+selected_datamyblockagent[i];
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection,
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Excel Report',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyblockagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to export.'); return false;
					}
					if(AllCheckmyblockagent==true){
						selected_datamyblockagent=new Array();
						var totales = storemyblockagentAll.getRange(0,storemyblockagentAll.getCount());
						
						for(i=0;i<storemyblockagentAll.getCount();i++){
							if(selected_datamyblockagent.indexOf(totales[i].data.agentid)==-1)
								selected_datamyblockagent.push(totales[i].data.agentid);
						}
					
					}
					var agentids=selected_datamyblockagent[0];
					for(i=1; i<selected_datamyblockagent.length; i++)
						agentids+=','+selected_datamyblockagent[i];
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 1,
							sort: filterfield,
							dir: filterdirection,
							agentids: agentids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					filteragentmyblockagent 	= '';
					filteremailmyblockagent 	= '';
					filterphonemyblockagent 	= '';
					filterwebmyblockagent 	= '';
					filteraddressmyblockagent 	= '';
					filtercompanymyblockagent 	= '';
					filtertypemyblockagent 	= 'ALL';
					
					var formmyfollowagent = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_blockagent.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						collapsible: true,
						collapsed: false,
						title: 'Filters',
						//renderTo: 'myblockagent_filters',
						id: 'formmyfollowagent',
						name: 'formmyfollowagent',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteragentmyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftypeagent',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'storetype',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'agenttype',
											'userid': <?php echo $userid;?>,
											selectall: 'all'
										},
										fields:[
											{name:'idtype', type:'string'},
											{name:'name', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_blockagent.php'
									}),
									displayField  : 'name',
									valueField    : 'idtype',
									name          : 'fagenttype',
									value         : 'ALL',
									hiddenName    : 'agenttype',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										},
										'change'  : function(field,newvalue,oldvalue){
											filtertypemyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email',
									name		  : 'femail',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteremailmyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fphone',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Phone',
									name		  : 'fphone',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterphonemyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fweb',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Website',
									name		  : 'fweb',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterwebmyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyblockagent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcompany',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Company',
									name		  : 'fcompany',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtercompanymyblockagent=newvalue;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteragentmyblockagent 	= '';
									filteremailmyblockagent 	= '';
									filterphonemyblockagent 	= '';
									filterwebmyblockagent 	= '';
									filteraddressmyblockagent 	= '';
									filtercompanymyblockagent 	= '';
									filtertypemyblockagent 	= 'ALL';
									Ext.getCmp('formmyfollowagent').getForm().reset();
									
									storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 300,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowagent,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Unblock Contacts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/unblock.png',
				handler: function(){
					if(selected_datamyblockagent.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to unblock.'); return false;
					}
					if(AllCheckmyblockagent==true){
						selected_datamyblockagent=new Array();
						var totales = storemyblockagentAll.getRange(0,storemyblockagentAll.getCount());
						
						for(i=0;i<storemyblockagentAll.getCount();i++){
							if(selected_datamyblockagent.indexOf(totales[i].data.agentid)==-1)
								selected_datamyblockagent.push(totales[i].data.agentid);
						}
					
					}
					var agentids=selected_datamyblockagent[0];
					for(i=1; i<selected_datamyblockagent.length; i++)
						agentids+=','+selected_datamyblockagent[i];
					
					Ext.MessageBox.show({
					    title:    'Contacts',
					    msg:      'Unblock properties of the contacts?',
					    buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
					    fn: function(btn){
								if(btn=='cancel'){
									return;
								}
								loading_win.show();
					
								Ext.Ajax.request( 
								{  
									waitMsg: 'Checking...',
									url: 'mysetting_tabs/myfollowup_tabs/properties_blockagent.php', 
									method: 'POST',
									timeout :600000,
									params: { 
										type: 'unblock',
										agentids: agentids,
										blockproperties: btn
									},
									
									failure:function(response,options){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','ERROR');
									},
									
									success:function(response,options){
										loading_win.hide();
										Ext.Msg.alert("Block contacts", selected_datamyblockagent.length+' Contacts unblocked.');
										storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});
										/*if(storemyfollowagent){
											storemyfollowagent.load({params:{start:0, limit:limitmyfollowagent}});
										}*/
									}                                
								});
							}
					});
					
				}
			}),new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('BlockContacts');
				}
			})	
		]
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var num = colIndex-3;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = 'H';
		else if(tyvalue == 1){ 
			classvalue = 'O';
		}else if(tyvalue == 2){ 
			classvalue = 'C';
		}else if(tyvalue == 3){ 
			classvalue = 'HF';
		}else if(tyvalue == 4){ 
			classvalue = 'TF';
		}else if(tyvalue == 5){ 
			classvalue = 'OTF';
		}else if(tyvalue == 6){ 
			classvalue = 'OF';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function emailRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-1;
		var type = 'typeemail'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function webRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-9;
		var type = 'typeurl'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(P)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}
	
	function addressRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-12;
		var type = 'typeaddress'+num;
		var tyvalue = rec.get(type);
		var classvalue = ''; 
		
		if(tyvalue == 0) 
			classvalue = '(H)';
		else if(tyvalue == 1){ 
			classvalue = '(O)';
		}
		if(value != null){
			if(value.length == 0) classvalue = '';
			return '<div>'+classvalue+' '+value+'</div>';
		}else{
			classvalue = '';
			return '<div></div>';
		}
		//alert('<div class="'+classvalue+'">'+value+'</div>');
		//return '<div class="'+classvalue+'">'+value+'</div>';
		
	}	
	var gridmyblockagent = new Ext.grid.GridPanel({
		renderTo: 'myblockagent_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyblockagent,
		stripeRows: true,
		sm: smmyblockagent, 
		columns: [
			smmyblockagent,
			{header: 'Contact', width: 150, sortable: true, tooltip: 'Contact name,', dataIndex: 'agent'}
			,{header: 'Type', width: 150, sortable: true, tooltip: 'Contact type,', dataIndex: 'agentype'}
			,{header: 'Email 1', width: 150, sortable: true, tooltip: 'Email 1', dataIndex: 'email', renderer: emailRender} 
			,{header: 'Email 2', width: 150, sortable: true, tooltip: 'Email 2', dataIndex: 'email2', renderer: emailRender}
			,{header: 'Phone 1', width: 100, sortable: true, tooltip: 'Phone 1.', dataIndex: 'phone1', renderer: phoneRender}
			,{header: 'Phone 2', width: 100, sortable: true, tooltip: 'Phone 2.', dataIndex: 'phone2', renderer: phoneRender}
			,{header: 'Phone 3', width: 100, sortable: true, tooltip: 'Phone 3.', dataIndex: 'phone3', renderer: phoneRender}
			,{header: 'Phone 4', width: 100, sortable: true, tooltip: 'Phone 4.', dataIndex: 'fax', renderer: phoneRender}
			,{header: 'Phone 5', width: 100, sortable: true, tooltip: 'Phone 5', dataIndex: 'tollfree', renderer: phoneRender}
			,{header: 'Phone 6', width: 100, sortable: true, tooltip: 'Phone 6', dataIndex: 'phone6', renderer: phoneRender}
			,{header: 'Website 1', width: 100, sortable: true, tooltip: 'Url of page to send documents.', dataIndex: 'urlsend', renderer: webRender}	
			,{header: 'Website 2', width: 100, sortable: true, tooltip: 'Url of page to send documents.', dataIndex: 'urlsend2', renderer: webRender}
			,{header: 'Company', width: 100, sortable: true, tooltip: 'Company', dataIndex: 'company'}
			,{header: 'Address 1', width: 150, sortable: true, tooltip: 'Address 1', dataIndex: 'address1', renderer: addressRender} 
			,{header: 'Address 2', width: 150, sortable: true, tooltip: 'Address 2', dataIndex: 'address2', renderer: addressRender}			 
		],
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			}
		},
				
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyblockagent',
            pageSize: limitmyblockagent,
            store: storemyblockagent,
            displayInfo: true,
			displayMsg: 'Total: {2} Contacts.',
			emptyMsg: "No Agents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 Contacts per page.',
				text: 50,
				handler: function(){
					limitmyblockagent=50;
					Ext.getCmp('pagingmyblockagent').pageSize = limitmyblockagent;
					Ext.getCmp('pagingmyblockagent').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 Contacts per page.',
				text: 80,
				handler: function(){
					limitmyblockagent=80;
					Ext.getCmp('pagingmyblockagent').pageSize = limitmyblockagent;
					Ext.getCmp('pagingmyblockagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 Contacts per page.',
				text: 100,
				handler: function(){
					limitmyblockagent=100;
					Ext.getCmp('pagingmyblockagent').pageSize = limitmyblockagent;
					Ext.getCmp('pagingmyblockagent').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        })
	});
	
	storemyblockagent.load({params:{start:0, limit:limitmyblockagent}});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myblockagent_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myblockagent_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>