<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	///Actualizacion de lprice y status
	$query="SELECT distinct f.bd FROM `xima`.`followup` f WHERE f.userid=$userid order by f.bd";
	$result=mysql_query($query) or die($query.mysql_error());
	
	while($r=mysql_fetch_array($result)){
		conectarPorNameCounty($r['bd']);
		$queryM="update `xima`.`followup` f 
			SET f.lprice=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0 or f.offer>0,f.lprice,(select lprice FROM mlsresidential WHERE parcelid=f.parcelid)),
			f.dom=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0,f.dom,(select dom FROM mlsresidential WHERE parcelid=f.parcelid)),
			f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid) is null,IF(length(f.mlnumber)>0,'NA','NF'),(Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid))),
			f.marketvalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.marketvalue,(select marketvalue FROM marketvalue WHERE parcelid=f.parcelid)),
			f.activevalue=IF((select count(*) FROM marketvalue WHERE parcelid=f.parcelid)=0,f.marketvalue,(select OffertValue FROM marketvalue WHERE parcelid=f.parcelid)),
			f.pendes=IF((select count(*) from pendes p where p.parcelid=f.parcelid)=0,'N',(select pof from pendes p where p.parcelid=f.parcelid))  
			WHERE f.userid=$userid and f.bd='".$r['bd']."'"; 
		mysql_query($queryM);
		
		$queryM="update `xima`.`followup` f 
			SET f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((SELECT date(p.saledate) FROM psummary p WHERE p.parcelid=f.parcelid AND p.saleprice>10000)>f.followdate,'S',f.status))
			WHERE f.userid=$userid and f.bd='".$r['bd']."'"; 
		mysql_query($queryM);
	}
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mycompletetasks_panel" style="background-color:#FFF;border-color:#FFF">
	<div id="mycompletetasks_data_div" align="center" style=" background-color:#FFF; margin:auto; width:970px;">
  		<div id="mycompletetasks_filters"></div><br />
        <div id="mycompletetasks_properties" align="left"></div> 
	</div>
</div>
<script>
	var useridspider= <?php echo $userid;?>;
	//Progress Bar Variables
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	var limitmyfollowcomplete 			= 50;
	var selected_datamyfollowcomplete 	= new Array();
	var AllCheckmyfollowcomplete 			= false;
	var loadedBCT						= false;
	
	//filter variables
	var filtersBCT = {
		property		: {
			address		: '',
			mlnumber	: '',
			county		: 'ALL',
			city		: 'ALL',
			zip			: '',
			status		: 'ALL',
			xcode		: 'ALL'
		},
		agent			: '',
		follow			: {
			datec		: 'Equal',
			date		: '',
			dateb		: ''
		},
		task			: {
			datec		: 'Equal',
			date		: '',
			dateb		: '',
			type		: '-2',
			pending		: '-1',
			complete	: '-1'
		},
		contract		: '-1',
		pof				: '-1',
		emd				: '-1',
		ademdums		: '-1',
		msj				: '-1',
		lu				: '-1',
		pe				: '-1',
		history			: '-1',
		offerreceived	: '-1',
		order			: {
			field		: 'address',
			direction	: 'ASC'
		}
	};
	
	var storemyfollowcomplete = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000,
			autoAbort: true
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'int'},
			   {name: 'coffer', type: 'int'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'int'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'dom', type: 'int'},
			   {name: 'pendes'},
			   {name: 'idfuh', type: 'int'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'pendingtask': 'no'
		},
		remoteSort: true,
		sortInfo: {
			field: 'ndate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				AllCheckmyfollowcomplete=false;
				selected_datamyfollowcomplete=new Array();
				smmyfollowcomplete.deselectRange(0,limitmyfollowcomplete);
				obj.params.address=filtersBCT.property.address;
				obj.params.mlnumber=filtersBCT.property.mlnumber;
				obj.params.agent=filtersBCT.agent;
				obj.params.status=filtersBCT.property.status;
				obj.params.ndate=filtersBCT.task.date;
				obj.params.ndateb=filtersBCT.task.dateb;
				obj.params.ndatec=filtersBCT.task.datec;
				obj.params.ntask=filtersBCT.task.type;
				obj.params.contract=filtersBCT.contract;
				obj.params.pof=filtersBCT.pof;
				obj.params.emd=filtersBCT.emd;
				obj.params.ademdums=filtersBCT.ademdums;
				obj.params.msj=filtersBCT.msj;
				obj.params.history=filtersBCT.history;
				obj.params.offerreceived=filtersBCT.offerreceived;
				obj.params.zip=filtersBCT.property.zip;
				obj.params.county=filtersBCT.property.county;
				obj.params.city=filtersBCT.property.city;
				obj.params.xcode=filtersBCT.property.xcode;
			},
			'load' : function (store,data,obj){
				storemyfollowcompleteAll.load();
				if (AllCheckmyfollowcomplete){
					Ext.get(gridmyfollowcomplete.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowcomplete=true;
					gridmyfollowcomplete.getSelectionModel().selectAll();
					selected_datamyfollowcomplete=new Array();
				}else{
					AllCheckmyfollowcomplete=false;
					Ext.get(gridmyfollowcomplete.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowcomplete.length > 0){
						for(val in selected_datamyfollowcomplete){
							var ind = gridmyfollowcomplete.getStore().find('pid',selected_datamyfollowcomplete[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowcomplete.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storemyfollowcompleteAll = new Ext.data.JsonStore({
       	proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'float'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'pendingtask': 'no'
		},
		remoteSort: true,
		sortInfo: {
			field: 'ndate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersBCT.property.address;
				obj.params.mlnumber=filtersBCT.property.mlnumber;
				obj.params.agent=filtersBCT.agent;
				obj.params.status=filtersBCT.property.status;
				obj.params.ndate=filtersBCT.task.date;
				obj.params.ndateb=filtersBCT.task.dateb;
				obj.params.ndatec=filtersBCT.task.datec;
				obj.params.ntask=filtersBCT.task.type;
				obj.params.contract=filtersBCT.contract;
				obj.params.pof=filtersBCT.pof;
				obj.params.emd=filtersBCT.emd;
				obj.params.ademdums=filtersBCT.ademdums;
				obj.params.msj=filtersBCT.msj;
				obj.params.history=filtersBCT.history;
				obj.params.offerreceived=filtersBCT.offerreceived;
				obj.params.zip=filtersBCT.property.zip;
				obj.params.county=filtersBCT.property.county;
				obj.params.city=filtersBCT.property.city;
				obj.params.xcode=filtersBCT.property.xcode;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/drop-no.gif" />'; 
		else return '<img src="../../img/drop-yes.gif" />';
	}
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'A': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;			
			
			case 'NA': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF': return '<div title="By Owner" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S': return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/notes/new_msj.png" />';
		else return '';
	}
	 
	function creaMenuTaskC(e,rowIndex){ 
		x = e.clientX;
 		y = e.clientY; 
		var record = gridmyfollowcomplete.getStore().getAt(rowIndex);
		var pid = record.get('pid');
		var county = record.get('county');
		var status = record.get('status');
		
		var simple = new Ext.FormPanel({
			url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
			frame: true,
			width: 220,
			waitMsgTarget : 'Waiting...',
			labelWidth: 100,
			defaults: {width: 200},
			labelAlign: 'left',
			items: [
				new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contact',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
								type: 'assignment'
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var r=Ext.decode(response.responseText);
								creaVentana(0,r,pid,useridspider);
							}
						});
					}
				})/*,new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Change Status',
					handler: function(){
						win.close();
						var simple = new Ext.FormPanel({
							url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
							frame: true,
							title: 'Follow Up - Status Change',
							width: 350,
							waitMsgTarget : 'Waiting...',
							labelWidth: 75,
							defaults: {width: 230},
							labelAlign: 'left',
							items: [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										fieldLabel	  : 'Status',
										width		  : 130,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['N','None'],
												['UC','Under Contract'],
												['PS','Pending Sale']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'statusaltname',
										value         : 'N',
										hiddenName    : 'statusalt',
										hiddenValue   : 'N',
										allowBlank    : false
									},{
										xtype     : 'hidden',
										name      : 'pid',
										value     : pid
									},{
										xtype     : 'hidden',
										name      : 'type',
										value     : 'change-status'
									}],
							
							buttons: [{
									text: 'Change',
									handler: function(){
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												winStatus.close();
												Ext.Msg.alert("Follow Up", 'Status Changed.');
												storemyfollowcomplete.load();
											},
											failure: function(form, action) {
												loading_win.hide();
												Ext.Msg.alert("Failure", "ERROR");
											}
										});
									}
								},{
									text: 'Reset',
									handler  : function(){
										simple.getForm().reset();
										winStatus.close();
									}
								}]
							});
						 
						var winStatus = new Ext.Window({
							layout      : 'fit',
							width       : 240,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'close',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									winStatus.close();
									loading_win.hide();
								}
							}]
						});
						winStatus.show();
					}
				})*/,new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Overview',
					handler: function(){
						win.close();
						createOverview(county,pid,status,false,false);
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contract',
					handler: function(){
						win.close();
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Edit History',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
							method: 'POST',
							timeout :600000,
							params: {
								idfuh: record.get('idfuh'), 
								parcelid: pid,
								userid: <?php echo $userid;?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var rsch=Ext.decode(response.responseText);
								var followfuh=rsch.records[0];
								var simple = new Ext.FormPanel({
									url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
									frame: true,
									title: 'Update History Task - '+followfuh.fulladdress,
									width: 490,
									waitMsgTarget : 'Waiting...',
									labelWidth: 100,
									defaults: {width: 350},
									labelAlign: 'left',
									items: [{
												xtype     : 'numberfield',
												name      : 'offer',
												fieldLabel: 'Offer',
												minValue  : 0,
												value	  : followfuh.offer
											},{
												xtype     : 'numberfield',
												name      : 'coffer',
												fieldLabel: 'C. Offer',
												minValue  : 0,
												value	  : followfuh.coffer
											},{
												xtype         : 'combo',
												mode          : 'local',
												fieldLabel    : 'Task',
												triggerAction : 'all',
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['1','Send SMS'],
														['2','Receive SMS'],
														['3','Send FAX'],
														['4','Receive FAX'],
														['5','Send EMAIL'],
														['6','Receive EMAIL'],
														['7','Send DOC'],
														['8','Receive DOC'],
														['9','Make CALL'],
														['10','Receive CALL'],
														['11','Send R. MAIL'],
														['12','Receive R. MAIL'],
														['13','Send OTHER'],
														['14','Receive OTHER'],
														['15','Send VOICE MAIL'],
														['16','Receive VOICE MAIL']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'ftaskname',
												value         : followfuh.task,
												hiddenName    : 'task',
												hiddenValue   : followfuh.task,
												allowBlank    : false
											},{
												xtype: 'button',
												tooltip: 'View Contacts',
												cls:'x-btn-text-icon',
												iconAlign: 'left',
												text: ' ',
												width: 30,
												height: 30,
												scale: 'medium',
												icon: 'http://www.reifax.com/img/agent.png',
												handler: function(){
													loading_win.show();
													Ext.Ajax.request({
														waitMsg: 'Seeking...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															pid: pid,
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															type: 'assignment'
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															loading_win.hide();
															var r=Ext.decode(response.responseText);
															creaVentana(0,r,pid,useridspider);
														}
													});
												}
											},{
												xtype	  :	'textarea',
												height	  : 100,
												name	  : 'sheduledetail',
												fieldLabel: 'Schedule Detail',
												value	  : followfuh.sheduledetail
											},{
												xtype	  :	'textarea',
												height	  : 100,
												name	  : 'detail',
												fieldLabel: 'Complete Detail',
												value	  : followfuh.detail
											},{
												xtype: 'checkboxgroup',
												fieldLabel: 'Document',
												columns: 3,
												itemCls: 'x-check-group-alt',
												items: [
													{boxLabel: 'Contract', name: 'contract', checked: !followfuh.contract},
													{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh.pof},
													{boxLabel: 'EMD', name: 'emd', checked: !followfuh.getemd},
													{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh.realtorsadem},
													{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh.offerreceived}
												]
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'update'
											},{
												xtype     : 'hidden',
												name      : 'idfuh',
												value     : followfuh.idfuh
											}],
									
									buttons: [{
											text: 'Update',
											handler: function(){
												loading_win.show();
												simple.getForm().submit({
													success: function(form, action) {
														loading_win.hide();
														win.close();
														Ext.Msg.alert("Follow Up", 'Updated Follow History.');
														storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
													},
													failure: function(form, action) {
														loading_win.hide();
														Ext.Msg.alert("Failure", "ERROR");
													}
												});
											}
										},{
											text: 'Reset',
											handler  : function(){
												simple.getForm().reset();
												win.close();
											}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 490,
									height      : 500,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'close',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
							}
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Follow History',
					handler: function(){
						win.close();
						var record = gridmyfollowcomplete.getStore().getAt(rowIndex);
						var pid = record.get('pid');
						var county = record.get('county');
						var status = record.get('status');
						if(document.getElementById('followTab')){
							var tab = tabsFollow.getItem('followTab');
							tabsFollow.remove(tab);
						}
						
						var followTbar = new Ext.Toolbar({
							cls: 'no-border', 
							width: 'auto',
							items: [' ',{
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											pid: pid,
											userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
											type: 'assignment'
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},{
								 tooltip: 'Short Sale',
								 cls:'x-btn-text-icon',
								 iconAlign: 'left',
								 text: ' ',
								 width: 30,
								 height: 30,
								 scale: 'medium',
								 icon: 'http://www.reifax.com/img/toolbar/shortsale.png', 
								 hidden: true,
								 handler: function(){
									var option = 'width=930px,height=555px';	
									 window.open('https://www.reifax.com/xima3/mysetting_tabs/myfollowup_tabs/shortsaleform.php?pid='+record.get('pid') ,'Signature',option);
									 
								 }
							},'->',{
								 tooltip: 'Close Follow History',
								 cls:'x-btn-text-icon',
								 iconAlign: 'left',
								 text: ' ',
								 width: 30,
								 height: 30,
								 scale: 'medium',
								 icon: 'http://www.reifax.com/img/cancel.png',
								 handler: function(){ 
									 var tab = tabsFollow.getItem('followTab');
									 tabsFollow.remove(tab);
								 }
							}]
						}); 
						
						
						var tbarhistory =  new Ext.Toolbar({
							cls: 'no-border',
							width: 'auto',
							items: [' ',{
								tooltip: 'Delete History',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
								 handler: function(){
									var followfuh = followsm.getSelections();
									var idfuh = '';
									if(followfuh.length > 0){
										idfuh=followfuh[0].get('idfuh');
										for(i=1; i<followfuh.length; i++){
											idfuh+=','+followfuh[i].get('idfuh');
										}
									}
									Ext.Ajax.request( 
									{  
										waitMsg: 'Deleting...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											type: 'delete',
											idfuh: idfuh
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
											followstore.load();
										}                                
									});
								 }
							},{
								tooltip: 'New History',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/add.gif',
								 handler: function(){
									var simple = new Ext.FormPanel({
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
										frame: true,
										title: 'Add History',
										width: 490,
										waitMsgTarget : 'Waiting...',
										labelWidth: 100,
										defaults: {width: 350},
										labelAlign: 'left',
										items: [{
													xtype     : 'numberfield',
													name      : 'offer',
													fieldLabel: 'Offer',
													minValue  : 0
												},{
													xtype     : 'numberfield',
													name      : 'coffer',
													fieldLabel: 'C. Offer',
													minValue  : 0
												},{
													xtype         : 'combo',
													mode          : 'local',
													fieldLabel    : 'Task',
													triggerAction : 'all',
													store         : new Ext.data.ArrayStore({
														id        : 0,
														fields    : ['valor', 'texto'],
														data      : [
															['1','Send SMS'],
															['2','Receive SMS'],
															['3','Send FAX'],
															['4','Receive FAX'],
															['5','Send EMAIL'],
															['6','Receive EMAIL'],
															['7','Send DOC'],
															['8','Receive DOC'],
															['9','Make CALL'],
															['10','Receive CALL'],
															['11','Send R. MAIL'],
															['12','Receive R. MAIL'],
															['13','Send OTHER'],
															['14','Receive OTHER'],
															['15','Send VOICE MAIL'],
															['16','Receive VOICE MAIL']
														]
													}),
													displayField  : 'texto',
													valueField    : 'valor',
													name          : 'ftaskname',
													value         : '1',
													hiddenName    : 'task',
													hiddenValue   : '1',
													allowBlank    : false
												},{
													xtype: 'button',
													tooltip: 'View Contacts',
													cls:'x-btn-text-icon',
													iconAlign: 'left',
													text: ' ',
													width: 30,
													height: 30,
													scale: 'medium',
													icon: 'http://www.reifax.com/img/agent.png',
													handler: function(){
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid,useridspider);
															}
														});
													}
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'detail',
													fieldLabel: 'Detail'
												},{
													xtype	  : 'checkboxgroup',
													fieldLabel: 'Document',
													columns	  : 3,
													itemCls   : 'x-check-group-alt',
													items	  : [
														{boxLabel: 'Contract', name: 'contract'},
														{boxLabel: 'Proof of Funds', name: 'pof'},
														{boxLabel: 'EMD', name: 'emd'},
														{boxLabel: 'Addendums', name: 'rademdums'},
														{boxLabel: 'Offer Received', name: 'offerreceived'}
													]
												},{
													xtype     : 'hidden',
													name      : 'type',
													value     : 'insert'
												},{
													xtype     : 'hidden',
													name      : 'parcelid',
													value     : record.get('pid') 
												}],
										
										buttons: [{
												text: 'Add',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Follow History", 'New Follow History.');
															followstore.load();
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", "ERROR");
														}
													});
												}
											},{
												text: 'Reset',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
									 
									var win = new Ext.Window({
										layout      : 'fit',
										width       : 490,
										height      : 400,
										modal	 	: true,
										plain       : true,
										items		: simple,
										closeAction : 'close',
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
								 }
							},{
								tooltip: 'Edit History',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/toolbar/update.png',
								 handler: function(){
									var followfuh = followsm.getSelections();
									if(followfuh.length > 1){
										Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
										return false;
									}else if(followfuh.length == 0){
										Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
										return false;
									}
									
									var simple = new Ext.FormPanel({
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
										frame: true,
										title: 'Update History Task - '+followfuh[0].get('fulladdress'),
										width: 490,
										waitMsgTarget : 'Waiting...',
										labelWidth: 100,
										defaults: {width: 350},
										labelAlign: 'left',
										items: [{
													xtype     : 'numberfield',
													name      : 'offer',
													fieldLabel: 'Offer',
													minValue  : 0,
													value	  : followfuh[0].get('offer')
												},{
													xtype     : 'numberfield',
													name      : 'coffer',
													fieldLabel: 'C. Offer',
													minValue  : 0,
													value	  : followfuh[0].get('coffer')
												},{
													xtype         : 'combo',
													mode          : 'local',
													fieldLabel    : 'Task',
													triggerAction : 'all',
													store         : new Ext.data.ArrayStore({
														id        : 0,
														fields    : ['valor', 'texto'],
														data      : [
															['1','Send SMS'],
															['2','Receive SMS'],
															['3','Send FAX'],
															['4','Receive FAX'],
															['5','Send EMAIL'],
															['6','Receive EMAIL'],
															['7','Send DOC'],
															['8','Receive DOC'],
															['9','Make CALL'],
															['10','Receive CALL'],
															['11','Send R. MAIL'],
															['12','Receive R. MAIL'],
															['13','Send OTHER'],
															['14','Receive OTHER'],
															['15','Send VOICE MAIL'],
															['16','Receive VOICE MAIL']
														]
													}),
													displayField  : 'texto',
													valueField    : 'valor',
													name          : 'ftaskname',
													value         : followfuh[0].get('task'),
													hiddenName    : 'task',
													hiddenValue   : followfuh[0].get('task'),
													allowBlank    : false
												},{
													xtype: 'button',
													tooltip: 'View Contacts',
													cls:'x-btn-text-icon',
													iconAlign: 'left',
													text: ' ',
													width: 30,
													height: 30,
													scale: 'medium',
													icon: 'http://www.reifax.com/img/agent.png',
													handler: function(){
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid,useridspider);
															}
														});
													}
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'sheduledetail',
													fieldLabel: 'Schedule Detail',
													value	  : followfuh[0].get('sheduledetail')
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'detail',
													fieldLabel: 'Complete Detail',
													value	  : followfuh[0].get('detail')
												},{
													xtype: 'checkboxgroup',
													fieldLabel: 'Document',
													columns: 3,
													itemCls: 'x-check-group-alt',
													items: [
														{boxLabel: 'Contract', name: 'contract', checked: !followfuh[0].get('contract')},
														{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh[0].get('pof')},
														{boxLabel: 'EMD', name: 'emd', checked: !followfuh[0].get('emd')},
														{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh[0].get('realtorsadem')},
														{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh[0].get('offerreceived')}
													]
												},{
													xtype     : 'hidden',
													name      : 'type',
													value     : 'update'
												},{
													xtype     : 'hidden',
													name      : 'idfuh',
													value     : followfuh[0].get('idfuh')
												}],
										
										buttons: [{
												text: 'Update',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Follow Up", 'Updated Follow History.');
															followstore.load();
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", "ERROR");
														}
													});
												}
											},{
												text: 'Reset',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
									 
									var win = new Ext.Window({
										layout      : 'fit',
										width       : 490,
										height      : 500,
										modal	 	: true,
										plain       : true,
										items		: simple,
										closeAction : 'close',
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
								 }
							},new Ext.Button({
								tooltip: 'Print History',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/toolbar/printer.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request( 
									{  
										waitMsg: 'Checking...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											printType: 4,
											parcelid: record.get('pid'),
											userid: record.get('userid'), 
											county: record.get('county')
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.pdf;
											loading_win.hide();
											window.open(url);							
										}                                
									});
								}
							}),new Ext.Button({
								tooltip: 'Export Excel',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/toolbar/excel.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request( 
									{  
										waitMsg: 'Checking...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											printType: 4,
											parcelid: record.get('pid'),
											userid: record.get('userid'), 
											county: record.get('county')
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.excel;
											loading_win.hide();
											location.href= url;								
										}                                
									});
								}
							}),new Ext.Button({
								tooltip: 'Refresh History',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/refresh.gif',
								handler: function(){
									followstore.load();
								},listeners: {
									mouseover: function(but,e){
										but.setText('Reload History');
										but.removeClass('x-btn-text-icon');
										but.addClass('x-btn-text-icon');
									},
									mouseout: function(but,e){
										but.setText(' ');
									}
								}
							})]
						});
						
						var tbarschedule = new Ext.Toolbar({
							cls: 'no-border',
							width: 'auto',
							items: [' ',{
								tooltip: 'Delete Schedule',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
								 handler: function(){
									var followfus = followsm_schedule.getSelections();
									var idfus = '';
									if(followfus.length > 0){
										idfus=followfus[0].get('idfus');
										for(i=1; i<followfus.length; i++){
											idfus+=','+followfus[i].get('idfus');
										}
									}
									Ext.Ajax.request( 
									{  
										waitMsg: 'Deleting...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											type: 'delete',
											idfus: idfus
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
											followstore_schedule.load();
										}                                
									});
								 }
							},{
								tooltip: 'New Schedule',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/add.gif',
								 handler: function(){
									var simple = new Ext.FormPanel({
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
										frame: true,
										title: 'Add Schedule',
										width: 490,
										waitMsgTarget : 'Waiting...',
										labelWidth: 100,
										defaults: {width: 350},
										labelAlign: 'left',
										items: [{
													xtype		  : 'datefield',
													allowBlank	  : false,
													name		  : 'odate',
													fieldLabel	  : 'Date',
													format		  : 'Y-m-d'
												},{
													xtype         : 'combo',
													mode          : 'local',
													fieldLabel    : 'Task',
													triggerAction : 'all',
													store         : new Ext.data.ArrayStore({
														id        : 0,
														fields    : ['valor', 'texto'],
														data      : [
															['1','Send SMS'],
															['2','Receive SMS'],
															['3','Send FAX'],
															['4','Receive FAX'],
															['5','Send EMAIL'],
															['6','Receive EMAIL'],
															['7','Send DOC'],
															['8','Receive DOC'],
															['9','Make CALL'],
															['10','Receive CALL'],
															['11','Send R. MAIL'],
															['12','Receive R. MAIL'],
															['13','Send OTHER'],
															['14','Receive OTHER'],
															['15','Send VOICE MAIL'],
															['16','Receive VOICE MAIL']
														]
													}),
													displayField  : 'texto',
													valueField    : 'valor',
													name          : 'ftaskname',
													value         : '1',
													hiddenName    : 'task',
													hiddenValue   : '1',
													allowBlank    : false
												},{
													xtype: 'button',
													tooltip: 'View Contacts',
													cls:'x-btn-text-icon',
													iconAlign: 'left',
													text: ' ',
													width: 30,
													height: 30,
													scale: 'medium',
													icon: 'http://www.reifax.com/img/agent.png',
													handler: function(){
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid,useridspider);
															}
														});
													}
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'detail',
													fieldLabel: 'Schedule Detail'
												},{
													xtype     : 'hidden',
													name      : 'type',
													value     : 'insert'
												},{
													xtype     : 'hidden',
													name      : 'parcelid',
													value     : record.get('pid') 
												}],
										
										buttons: [{
												text: 'Add',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Follow Schedule", 'New Follow Schedule.');
															followstore_schedule.load();
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", "ERROR");
														}
													});
												}
											},{
												text: 'Reset',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
									 
									var win = new Ext.Window({
										layout      : 'fit',
										width       : 490,
										height      : 400,
										modal	 	: true,
										plain       : true,
										items		: simple,
										closeAction : 'close',
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
								 }
							},{
								tooltip: 'Edit Schedule',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/toolbar/update.png',
								 handler: function(){
									var followfus = followsm_schedule.getSelections();
									if(followfus.length > 1){
										Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
										return false;
									}else if(followfus.length == 0){
										Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
										return false;
									}
									
									var simple = new Ext.FormPanel({
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
										frame: true,
										title: 'Update Schedule Task - '+followfus[0].get('fulladdress'),
										width: 490,
										waitMsgTarget : 'Waiting...',
										labelWidth: 100,
										defaults: {width: 350},
										labelAlign: 'left',
										items: [{
													xtype     	  : 'datefield',
													name      	  : 'odate',
													fieldLabel	  : 'Date',
													value	  	  : followfus[0].get('odate'),
													format	  	  : 'Y-m-d'
												},{
													xtype         : 'combo',
													mode          : 'local',
													fieldLabel    : 'Task',
													triggerAction : 'all',
													store         : new Ext.data.ArrayStore({
														id        : 0,
														fields    : ['valor', 'texto'],
														data      : [
															['1','Send SMS'],
															['2','Receive SMS'],
															['3','Send FAX'],
															['4','Receive FAX'],
															['5','Send EMAIL'],
															['6','Receive EMAIL'],
															['7','Send DOC'],
															['8','Receive DOC'],
															['9','Make CALL'],
															['10','Receive CALL'],
															['11','Send R. MAIL'],
															['12','Receive R. MAIL'],
															['13','Send OTHER'],
															['14','Receive OTHER'],
															['15','Send VOICE MAIL'],
															['16','Receive VOICE MAIL']
														]
													}),
													displayField  : 'texto',
													valueField    : 'valor',
													name          : 'ftaskname',
													value         : followfus[0].get('task'),
													hiddenName    : 'task',
													hiddenValue   : followfus[0].get('task'),
													allowBlank    : false
												},{
													xtype: 'button',
													tooltip: 'View Contacts',
													cls:'x-btn-text-icon',
													iconAlign: 'left',
													text: ' ',
													width: 30,
													height: 30,
													scale: 'medium',
													icon: 'http://www.reifax.com/img/agent.png',
													handler: function(){
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid,useridspider);
															}
														});
													}
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'detail',
													fieldLabel: 'Schedule Detail',
													value	  : followfus[0].get('detail')
												},{
													xtype     : 'hidden',
													name      : 'type',
													value     : 'update'
												},{
													xtype     : 'hidden',
													name      : 'idfus',
													value     : followfus[0].get('idfus')
												}],
										
										buttons: [{
												text: 'Update',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Follow Schedule", 'Updated Follow Schedule.');
															followstore_schedule.load();
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", "ERROR");
														}
													});
												}
											},{
												text: 'Reset',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
									 
									var win = new Ext.Window({
										layout      : 'fit',
										width       : 490,
										height      : 400,
										modal	 	: true,
										plain       : true,
										items		: simple,
										closeAction : 'close',
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
								 }
							},{
								tooltip: 'Complete Task',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								 icon: 'http://www.reifax.com/img/refresh25.png',
								 handler: function(){
									var followfus = followsm_schedule.getSelections();
									if(followfus.length > 1){
										Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
										return false;
									}else if(followfus.length == 0){
										Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
										return false;
									}
									
									var simple = new Ext.FormPanel({
										url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
										frame: true,
										title: 'Complete Schedule Task - '+followfus[0].get('fulladdress'),
										width: 490,
										waitMsgTarget : 'Waiting...',
										labelWidth: 100,
										defaults: {width: 350},
										labelAlign: 'left',
										items: [{
													xtype     : 'numberfield',
													name      : 'offer',
													fieldLabel: 'Offer',
													minValue  : 0
												},{
													xtype     : 'numberfield',
													name      : 'coffer',
													fieldLabel: 'C. Offer',
													minValue  : 0
												},{
													xtype         : 'combo',
													mode          : 'local',
													fieldLabel    : 'Task',
													triggerAction : 'all',
													store         : new Ext.data.ArrayStore({
														id        : 0,
														fields    : ['valor', 'texto'],
														data      : [
															['1','Send SMS'],
															['2','Receive SMS'],
															['3','Send FAX'],
															['4','Receive FAX'],
															['5','Send EMAIL'],
															['6','Receive EMAIL'],
															['7','Send DOC'],
															['8','Receive DOC'],
															['9','Make CALL'],
															['10','Receive CALL'],
															['11','Send R. MAIL'],
															['12','Receive R. MAIL'],
															['13','Send OTHER'],
															['14','Receive OTHER'],
															['15','Send VOICE MAIL'],
															['16','Receive VOICE MAIL']
														]
													}),
													displayField  : 'texto',
													valueField    : 'valor',
													name          : 'ftaskname',
													value         : followfus[0].get('task'),
													hiddenName    : 'task',
													hiddenValue   : followfus[0].get('task'),
													allowBlank    : false
												},{
													xtype: 'button',
													tooltip: 'View Contacts',
													cls:'x-btn-text-icon',
													iconAlign: 'left',
													text: ' ',
													width: 30,
													height: 30,
													scale: 'medium',
													icon: 'http://www.reifax.com/img/agent.png',
													handler: function(){
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid,useridspider);
															}
														});
													}
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'sheduledetail',
													fieldLabel: 'Schedule Detail',
													value	  : followfus[0].get('detail')
												},{
													xtype	  :	'textarea',
													height	  : 100,
													name	  : 'detail',
													fieldLabel: 'Complete Detail'
												},{
													xtype: 'checkboxgroup',
													fieldLabel: 'Document',
													columns: 3,
													itemCls: 'x-check-group-alt',
													items: [
														{boxLabel: 'Contract', name: 'contract', checked: false},
														{boxLabel: 'Proof of Funds', name: 'pof', checked: false},
														{boxLabel: 'EMD', name: 'emd', checked: false},
														{boxLabel: 'Addendums', name: 'rademdums', checked: false},
														{boxLabel: 'Offer Received', name: 'offerreceived'} 
													]
												},{
													xtype     : 'hidden',
													name      : 'type',
													value     : 'complete'
												},{
													xtype     : 'hidden',
													name      : 'idfus',
													value     : followfus[0].get('idfus')
												},{
													xtype     : 'hidden',
													name      : 'parcelid',
													value     : record.get('pid') 
												}],
										
										buttons: [{
												text: 'Complete',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Follow Schedule", 'Completed Follow Schedule Task.');
															followstore_schedule.load();
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", "ERROR");
														}
													});
												}
											},{
												text: 'Reset',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
									 
									var win = new Ext.Window({
										layout      : 'fit',
										width       : 490,
										height      : 530,
										modal	 	: true,
										plain       : true,
										items		: simple,
										closeAction : 'close',
										buttons: [{
											text     : 'Close',
											handler  : function(){
												win.close();
											}
										}]
									});
									win.show();
								 }
							},new Ext.Button({
								tooltip: 'Print Schedule',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/toolbar/printer.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request( 
									{  
										waitMsg: 'Checking...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											printType: 5,
											parcelid: record.get('pid'),
											userid: record.get('userid'), 
											county: record.get('county')
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.pdf;
											loading_win.hide();
											window.open(url);							
										}                                
									});
								}
							}),new Ext.Button({
								tooltip: 'Export Excel',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/toolbar/excel.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request( 
									{  
										waitMsg: 'Checking...',
										url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											printType: 5,
											parcelid: record.get('pid'),
											userid: record.get('userid'), 
											county: record.get('county')
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.excel;
											loading_win.hide();
											location.href= url;								
										}                                
									});
								}
							}),new Ext.Button({
								tooltip: 'Refresh Schedule',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/refresh.gif',
								handler: function(){
									followstore_schedule.load();
								}
							})]
						});
						
						tabsFollow.add({
							title: ' Follow ',
							id: 'followTab', 
							closable: true,
							items: [	
								new Ext.TabPanel({
									id: 'historyTab',
									activeTab: 0,
									width: ancho,
									height: tabsFollow.getHeight(),
									plain:true,
									listeners: {
										'tabchange': function( tabpanel, tab ){
											if(tab.id=='followhistoryInnerTab')
												if(document.getElementById('follow_history_grid')) followstore.load();
											else if(tab.id=='followsheduleInnerTab')
												if(document.getElementById('follow_schedule_grid')) followstore_schedule.load();
										}
									},
									items:[{
										title: 'Dashboard',
										id: 'followdashboardInnerTab',
										autoLoad: {
											url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followdashboard.php', 
											scripts: true, 
											params:{
												parcelid:record.get('pid'), 
												userid:record.get('userid'), 
												county:record.get('county') 
											}, 
											timeout: 10800
										},
										enableTabScroll:true,
										defaults:{ autoScroll: false}
								   },{
										title: 'History',
										id: 'followhistoryInnerTab',
										autoLoad: {
											url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', 
											scripts	: true, 
											params	:{
												parcelid 	: record.get('pid'), 
												userid 		: record.get('userid'), 
												county 		: record.get('county') 
											}, 
											timeout	: 10800
										},
										enableTabScroll:true,
										defaults:{ autoScroll: false},
										tbar: tbarhistory
									},{
										title: 'Schedule',
										id: 'followsheduleInnerTab',
										autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
										enableTabScroll:true,
										defaults:{ autoScroll: false},
										tbar: tbarschedule
									},{
										title: 'Email',
										id: 'followemailInnerTabB',
										autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followemail.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
										enableTabScroll:true,
										defaults:{ autoScroll: false}
									}]
								})
							],
							tbar: followTbar
						}).show();
					}
				})
			]
		});
		var win = new Ext.Window({
			layout      : 'fit',
			width       : 230,
			height      : 330,
			modal	 	: true,
			plain       : true,
			items		: simple,
			closeAction : 'close',
			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
					loading_win.hide();
				}
			}]
		});
		win.show();
		return false;
	}
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenuTaskC(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}

	var smmyfollowcomplete = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowcomplete.indexOf(record.get('pid'))==-1)
					selected_datamyfollowcomplete.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowcomplete.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowcomplete=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowcomplete = selected_datamyfollowcomplete.remove(record.get('pid'));
				AllCheckmyfollowcomplete=false;
				Ext.get(gridmyfollowcomplete.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowcomplete=new Ext.Toolbar({
		renderTo: 'mycompletetasks_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowcomplete.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be eliminated.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcomplete==true){
						var totales = storemyfollowcompleteAll.getRange(0,storemyfollowcompleteAll.getCount());
						var idfuh='\''+totales[0].data.idfuh+'\'';
						for(i=1;i<storemyfollowcompleteAll.getCount();i++){
							idfuh+=',\''+totales[i].data.idfuh+'\'';	
						}
						
						//Confirmacion para eliminar todos
						Ext.MessageBox.show({
							title:    'Follow Up',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will delete all the selected tasks from your Completed Task.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteFollow
						});
					}else{	
						var totales = gridmyfollowcomplete.getSelectionModel().getSelections();
						var idfuh='\''+totales[0].data.idfuh+'\'';
						for(i=1; i<totales.length; i++)
							idfuh+=',\''+totales[i].data.idfuh+'\'';
						
						deleteFollow('yes'); 
					}
					
					function deleteFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}

						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'delete-multi',
								idfuh: idfuh,
								userid: <?php echo $userid;?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
								//storemyfollowcompleteAll.load();
								Ext.Msg.alert("Follow Up Task", 'Tasks delete.');
								
							}                                
						});
					}
				}
			}),new Ext.Button({
				tooltip: 'Edit history',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/update.png',
				handler: function(){
					if(selected_datamyfollowcomplete.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the record to be edit.'); return false;
					}else if(selected_datamyfollowcomplete.length>1){
						Ext.Msg.alert('Warning', 'You must select(check) only one record to be edit.'); return false;
					}
					loading_win.show();
					Ext.Ajax.request({
						waitMsg: 'Seeking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
						method: 'POST',
						timeout :600000,
						params: {
							idfuh: gridmyfollowcomplete.getSelectionModel().getSelections()[0].data.idfuh, 
							parcelid: gridmyfollowcomplete.getSelectionModel().getSelections()[0].data.pid,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							var rsch=Ext.decode(response.responseText);
							var followfuh=rsch.records[0];
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Update History Task - '+followfuh.fulladdress,
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0,
											value	  : followfuh.offer
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0,
											value	  : followfuh.coffer
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER'],
													['15','Send VOICE MAIL'],
													['16','Receive VOICE MAIL'],
													['17','Make NOTE']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfuh.task,
											hiddenName    : 'task',
											hiddenValue   : followfuh.task,
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'View Contacts',
											cls:'x-btn-text-icon',
											iconAlign: 'left',
											text: ' ',
											width: 30,
											height: 30,
											scale: 'medium',
											icon: 'http://www.reifax.com/img/agent.png',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid,useridspider);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'sheduledetail',
											fieldLabel: 'Schedule Detail',
											value	  : followfuh.sheduledetail
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail',
											value	  : followfuh.detail
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 3,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: !followfuh.contract},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh.pof},
												{boxLabel: 'EMD', name: 'emd', checked: !followfuh.getemd},
												{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh.realtorsadem},
												{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh.offerreceived}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfuh',
											value     : followfuh.idfuh
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Up", 'Updated Follow History.');
													storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 500,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						}
					});
				}
			}),new Ext.Button({
				tooltip: 'Print Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyfollowcomplete.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcomplete==true){
						var totales = storemyfollowcompleteAll.getRange(0,storemyfollowcompleteAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowcompleteAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyfollowcomplete[0]+'\'';
						for(i=1; i<selected_datamyfollowcomplete.length; i++)
							pids+=',\''+selected_datamyfollowcomplete[i]+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersBCT.property.address,
							mlnumber: filtersBCT.property.mlnumber,
							agent: filtersBCT.agent,
							status: filtersBCT.property.status,
							ndate: filtersBCT.task.date,
							ndateb: filtersBCT.task.dateb,
							ndatec: filtersBCT.task.datec,
							ntask: filtersBCT.task.type,
							contract: filtersBCT.contract,
							pof: filtersBCT.pof,
							emd: filtersBCT.emd,
							ademdums: filtersBCT.ademdums,
							msj: filtersBCT.msj,
							history: filtersBCT.history,
							sort: filtersBCT.order.field,
							dir: filtersBCT.order.direction,
							pids: pids,
							pendingtask: 'no'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyfollowcomplete.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowcomplete==true){
						var totales = storemyfollowcompleteAll.getRange(0,storemyfollowcompleteAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowcompleteAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyfollowcomplete[0]+'\'';
						for(i=1; i<selected_datamyfollowcomplete.length; i++)
							pids+=',\''+selected_datamyfollowcomplete[i]+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersBCT.property.address,
							mlnumber: filtersBCT.property.mlnumber,
							agent: filtersBCT.agent,
							status: filtersBCT.property.status,
							ndate: filtersBCT.task.date,
							ndateb: filtersBCT.task.dateb,
							ndatec: filtersBCT.task.datec,
							ntask: filtersBCT.task.type,
							contract: filtersBCT.contract,
							pof: filtersBCT.pof,
							emd: filtersBCT.emd,
							ademdums: filtersBCT.ademdums,
							msj: filtersBCT.msj,
							history: filtersBCT.history,
							sort: filtersBCT.order.field,
							dir: filtersBCT.order.direction,
							pids: pids,
							pendingtask: 'no'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmycomplete = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						id: 'formmytasks',
						name: 'formmytasks',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersBCT.property.address,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBCT.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fmlnumber',
									value		  : filtersBCT.property.mlnumber,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBCT.property.mlnumber=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-countys',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtersBCT.property.county,
									hiddenName    : 'fcounty',
									hiddenValue   : filtersBCT.property.county,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.property.county = record.get('valor');
											combo.findParentByType('form').getForm().findField('fcity').setValue("ALL");
											filtersBCT.property.city='ALL';
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcity',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'City',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-citys',
											'userid': <?php echo $userid;?>,
											county: filtersBCT.property.county
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcityname',
									value         : filtersBCT.property.city,
									hiddenName    : 'fcity',
									hiddenValue   : filtersBCT.property.city,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.property.city = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('county',filtersBCT.property.county);
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fzip',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Zip Code',
									name		  : 'fzip',
									value		  : filtersBCT.property.zip,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBCT.property.zip=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fxcode',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Property Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-xcodes',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fxcodename',
									value         : filtersBCT.property.xcode,
									hiddenName    : 'fxcode',
									hiddenValue   : filtersBCT.property.xcode,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.property.xcode = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									value		  : filtersBCT.agent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBCT.agent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fntask',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Task',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER'],
											['15','Send VOICE MAIL'],
											['16','Receive VOICE MAIL'],
											['17','Make NOTE']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fntaskname',
									value         : filtersBCT.task.type,
									hiddenName    : 'fntask',
									hiddenValue   : filtersBCT.task.type,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.task.type = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : filtersBCT.property.status,
									hiddenName    : 'fstatus',
									hiddenValue   : filtersBCT.property.status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.property.status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontract',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Contract',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcontractname',
									value         : filtersBCT.contract,
									hiddenName    : 'fcontract',
									hiddenValue   : filtersBCT.contract,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.contract = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fpof',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Proof of Funds',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpofname',
									value         : filtersBCT.pof,
									hiddenName    : 'fpof',
									hiddenValue   : filtersBCT.pof,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.pof = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'femd',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'EMD',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'femdname',
									value         : filtersBCT.emd,
									hiddenName    : 'femd',
									hiddenValue   : filtersBCT.emd,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.emd = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fademdums',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Addendums', 
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fademdumsname',
									value         : filtersBCT.ademdums,
									hiddenName    : 'fademdums',
									hiddenValue   : filtersBCT.ademdums,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.ademdums = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmsj',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Message',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Yes'],
											['0','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fmsjname',
									value         : filtersBCT.msj,
									hiddenName    : 'fmsj',
									hiddenValue   : filtersBCT.msj,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.msj = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fofferreceived',
								colspan: 2,
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Offer Received',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fofferreceived',
									value         : filtersBCT.offerreceived,
									hiddenName    : 'foffer',
									hiddenValue   : filtersBCT.offerreceived,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBCT.offerreceived = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fndate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Completed Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fndatecname',
										hiddenName: 'fndatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersBCT.task.datec,
										listeners: {
											'select': function (combo,record,index){
												filtersBCT.task.datec = record.get('id');
												var secondfield = Ext.getCmp('fndateb');
												secondfield.setValue('');
												filtersBCT.task.dateb='';
												
												if(filtersBCT.task.datec=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fndate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fndate',
											value		  : filtersBCT.task.date,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersBCT.task.date=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fndateb',
										name		  : 'fndateb',
										hidden		  : filtersBCT.task.datec!='Between',
										value		  : filtersBCT.task.dateb,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersBCT.task.dateb=newvalue;
											}
										}
									}]
								}]	
							}]	
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								//icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Load&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'load-filters'
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning',output);
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											if(rest.total==0){
												Ext.MessageBox.alert('Warning','You don\'t have default filters saved');
											}else{
												formmycomplete.getForm().findField('faddress').setValue(rest.data.address);
												formmycomplete.getForm().findField('fmlnumber').setValue(rest.data.mlnumber);
												formmycomplete.getForm().findField('fzip').setValue(rest.data.zip);
												formmycomplete.getForm().findField('fagent').setValue(rest.data.agent);
												formmycomplete.getForm().findField('fstatus').setValue(rest.data.status);
												formmycomplete.getForm().findField('fcontract').setValue(rest.data.contract);
												formmycomplete.getForm().findField('fpof').setValue(rest.data.pof);
												formmycomplete.getForm().findField('femd').setValue(rest.data.emd);
												formmycomplete.getForm().findField('fademdums').setValue(rest.data.ademdums);
												formmycomplete.getForm().findField('foffer').setValue(rest.data.offerreceived);
												formmycomplete.getForm().findField('fndate').setValue(rest.data.ndate);
												formmycomplete.getForm().findField('fndateb').setValue(rest.data.ndateb);
												if(rest.data.ntask==-2){
													rest.data.ntask=-1;
												}
												formmycomplete.getForm().findField('fntask').setValue(rest.data.ntask);
												formmycomplete.getForm().findField('fmsj').setValue(rest.data.msj);
												formmycomplete.getForm().findField('fcounty').setValue(rest.data.county);
												formmycomplete.getForm().findField('fcity').setValue(rest.data.city);
												formmycomplete.getForm().findField('fxcode').setValue(rest.data.xcode);
											}
										}
									});
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/save.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Save&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'save-filters',
											address: formmycomplete.getForm().findField('faddress').getValue(),
											mlnumber: formmycomplete.getForm().findField('fmlnumber').getValue(),
											agent: formmycomplete.getForm().findField('fagent').getValue(),
											status: formmycomplete.getForm().findField('fstatus').getValue(),
											contract: formmycomplete.getForm().findField('fcontract').getValue(),
											pof: formmycomplete.getForm().findField('fpof').getValue(),
											emd: formmycomplete.getForm().findField('femd').getValue(),
											ademdums: formmycomplete.getForm().findField('fademdums').getValue(),
											msj: formmycomplete.getForm().findField('fmsj').getValue(),
											offerreceived: formmycomplete.getForm().findField('foffer').getValue(),
											zip: formmycomplete.getForm().findField('fzip').getValue(),
											ndate: formmycomplete.getForm().findField('fndate').getValue(),
											ndateb: formmycomplete.getForm().findField('fndateb').getValue(),
											ntask: formmycomplete.getForm().findField('fntask').getValue(),
											county: formmycomplete.getForm().findField('fcounty').getValue(),
											city: formmycomplete.getForm().findField('fcity').getValue(),
											xcode: formmycomplete.getForm().findField('fxcode').getValue()
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Error saving filters');
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											Ext.MessageBox.alert('Warning','Default search successfully saved');
										}
									});
								}				
							},'->',{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersBCT.property.address = formmycomplete.getForm().findField('faddress').getValue();
									filtersBCT.property.mlnumber = formmycomplete.getForm().findField('fmlnumber').getValue();
									filtersBCT.agent = formmycomplete.getForm().findField('fagent').getValue();
									filtersBCT.property.status = formmycomplete.getForm().findField('fstatus').getValue();
									filtersBCT.contract = formmycomplete.getForm().findField('fcontract').getValue();
									filtersBCT.pof = formmycomplete.getForm().findField('fpof').getValue();
									filtersBCT.emd = formmycomplete.getForm().findField('femd').getValue();
									filtersBCT.ademdums = formmycomplete.getForm().findField('fademdums').getValue();
									filtersBCT.offerreceived = formmycomplete.getForm().findField('foffer').getValue();
									filtersBCT.property.zip = formmycomplete.getForm().findField('fzip').getValue();
									filtersBCT.task.date = formmycomplete.getForm().findField('fndate').getValue();
									filtersBCT.task.dateb = formmycomplete.getForm().findField('fndateb').getValue();
									filtersBCT.task.datec = formmycomplete.getForm().findField('fndatec').getValue();
									filtersBCT.task.type = formmycomplete.getForm().findField('fntask').getValue();
									filtersBCT.property.county = formmycomplete.getForm().findField('fcounty').getValue();
									filtersBCT.property.city = formmycomplete.getForm().findField('fcity').getValue();
									filtersBCT.property.xcode = formmycomplete.getForm().findField('fxcode').getValue();
									
									storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
									//storemyfollowcompleteAll.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersBCT.property.address = '';
									filtersBCT.property.mlnumber = '';
									filtersBCT.agent = '';
									filtersBCT.property.status = 'ALL';
									filtersBCT.task.date = '';
									filtersBCT.task.dateb = '';
									filtersBCT.task.datec = 'Equal';
									filtersBCT.task.type = '-1';
									filtersBCT.contract = '-1';
									filtersBCT.pof = '-1';
									filtersBCT.emd = '-1';
									filtersBCT.ademdums = '-1';
									filtersBCT.msj = '-1';
									filtersBCT.history = '-1';	
									filtersBCT.offerreceived = '-1';
									filtersBCT.property.zip='';
									filtersBCT.property.county = 'ALL';
									filtersBCT.property.city = 'ALL';
									filtersBCT.property.xcode = 'ALL';
									//Ext.getCmp('formmytasks').getForm().reset();
									
									formmycomplete.getForm().findField('faddress').setValue('');
									formmycomplete.getForm().findField('fmlnumber').setValue('');
									formmycomplete.getForm().findField('fzip').setValue('');
									formmycomplete.getForm().findField('fagent').setValue('');
									formmycomplete.getForm().findField('fstatus').setValue('ALL');
									formmycomplete.getForm().findField('fcontract').setValue('-1');
									formmycomplete.getForm().findField('fpof').setValue('-1');
									formmycomplete.getForm().findField('femd').setValue('-1');
									formmycomplete.getForm().findField('fademdums').setValue('-1');
									formmycomplete.getForm().findField('foffer').setValue('-1');
									formmycomplete.getForm().findField('fndate').setValue('');
									formmycomplete.getForm().findField('fndateb').setValue('');
									formmycomplete.getForm().findField('fndatec').setValue('Equal');
									formmycomplete.getForm().findField('fntask').setValue('-1');
									formmycomplete.getForm().findField('fmsj').setValue('-1');
									formmycomplete.getForm().findField('fcounty').setValue('ALL');
									formmycomplete.getForm().findField('fcity').setValue('ALL');
									formmycomplete.getForm().findField('fxcode').setValue('ALL');
									//storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
									//win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 450,
								modal	 	: true,  
								plain       : true,
								items		: formmycomplete,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersBCT.property.address = '';
					filtersBCT.property.mlnumber = '';
					filtersBCT.agent = '';
					filtersBCT.property.status = 'ALL';
					filtersBCT.task.date = '';
					filtersBCT.task.dateb = '';
					filtersBCT.task.datec = 'Equal';
					filtersBCT.task.type = '-1';
					filtersBCT.contract = '-1';
					filtersBCT.pof = '-1';
					filtersBCT.emd = '-1';
					filtersBCT.ademdums = '-1';
					filtersBCT.msj = '-1';
					filtersBCT.history = '-1';	
					filtersBCT.offerreceived = '-1';
					filtersBCT.property.zip='';
					filtersBCT.property.county = 'ALL';
					filtersBCT.property.city = 'ALL';
					filtersBCT.property.xcode = 'ALL';
					
					storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
					//storemyfollowcompleteAll.load();
				}
			}),new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					var win = new Ext.Window({
						layout      : 'fit',
						title		: 'Video Help',
						width       : 1010,
						height      : 590,
						modal	 	: true,
						plain       : true,
						autoScroll:false,
						autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'BuyingPendingTask'} }
					});
					win.show();
				}
			})
		]
	});
	
	
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var gridmyfollowcomplete = new Ext.grid.EditorGridPanel({
		renderTo: 'mycompletetasks_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemyfollowcomplete,
		stripeRows: true,
		sm: smmyfollowcomplete,
		clicksToEdit: 1, 
		columns: [
			smmyfollowcomplete,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'User', width: 40, renderer: userRender, dataIndex: 'userid_follow'},{header: 'V', width: 25, sortable: true, tooltip: 'View.', dataIndex: 'msj', renderer: viewRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender},{header: 'T', width: 25, renderer: taskRender, tooltip: 'Next Task.', dataIndex: 'ntask', sortable: true},{header: 'Comp. Date', width: 90, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate', align: 'right', sortable: true}";	
					else if($val->name=='zip'){
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
						echo ",{header: 'DOM', width: 40, dataIndex: 'dom', align: 'center', sortable: true, tooltip: 'Days on Market'}";
						echo ",{header: 'F', width: 40, dataIndex: 'pendes', align: 'center', sortable: true, tooltip: 'Foreclosure status'}";			
					}else
						if($val->name!='agent')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent', align: 'right'}
				,{header: 'L. Price', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Listing Price', dataIndex: 'lprice', align: 'right'}
				,{header: 'Offer', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Offer.', dataIndex: 'offer', align: 'right'}
				,{header: 'C. Offer', renderer: renderNumeros, width: 60, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer', align: 'right'}
				
				,{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate', align: 'right'}
				,{header: 'C', width: 25, sortable: true, tooltip: 'Contract.', dataIndex: 'contract', renderer: checkRender}
				,{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}
				,{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}
				,{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}
				,{header: 'O', width: 25, sortable: true, tooltip: 'Offer Received.', dataIndex: 'offerreceived', renderer: checkRender}";
		   ?>			 
		], 
		tbar: new Ext.PagingToolbar({ 
			id: 'pagingmycompletetasks',
            pageSize: limitmyfollowcomplete,
            store: storemyfollowcomplete,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowcomplete=50;
					Ext.getCmp('pagingmycompletetasks').pageSize = limitmyfollowcomplete;
					Ext.getCmp('pagingmycompletetasks').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupBCT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowcomplete=80;
					Ext.getCmp('pagingmycompletetasks').pageSize = limitmyfollowcomplete;
					Ext.getCmp('pagingmycompletetasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupBCT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowcomplete=100;
					Ext.getCmp('pagingmycompletetasks').pageSize = limitmyfollowcomplete;
					Ext.getCmp('pagingmycompletetasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupBCT'
			})]
        }),
		
		listeners: {
			'click': function(){
				if(document.getElementById('generate_contract')){
					var tab = tabs.getItem('generate_contract');
					tabs.remove(tab);
				}				
			},
			'afteredit': function (oGrid_Event) {
				var fieldValue = oGrid_Event.value;
				if(fieldValue=='BEGIN')
				{Ext.MessageBox.alert('Warning','Error select BEGIN'); return;}
				//alert(fieldValue+' '+oGrid_Event.record.data.pid+' '+oGrid_Event.field)
				Ext.Ajax.request({   
					waitMsg: 'Saving changes...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
					method: 'POST',
					params: {
						type: "change-lprice", 
						pid: oGrid_Event.record.data.pid,
						field: oGrid_Event.field,
						value: fieldValue,
						originalValue: oGrid_Event.record.modified,
						userid: <?php echo $userid;?>
					},
					
					failure:function(response,options){
						Ext.MessageBox.alert('Warning','Error editing');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						
						if(rest.succes==false)
							Ext.MessageBox.alert('Warning',rest.msg);
							
						storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
					}
				 });
			},
			'sortchange': function (grid, sorted){
				filtersBCT.order.field=sorted.field;
				filtersBCT.order.direction=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var status = new Ext.Action({
					text: 'Change Status',
					handler: function(){
						var simple = new Ext.FormPanel({
							url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
							frame: true,
							title: 'Follow Up - Status Change',
							width: 350,
							waitMsgTarget : 'Waiting...',
							labelWidth: 75,
							defaults: {width: 230},
							labelAlign: 'left',
							items: [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										fieldLabel	  : 'Status',
										width		  : 130,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['N','None'],
												['UC','Under Contract'],
												['PS','Pending Sale']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'statusaltname',
										value         : 'N',
										hiddenName    : 'statusalt',
										hiddenValue   : 'N',
										allowBlank    : false
									},{
										xtype     : 'hidden',
										name      : 'pid',
										value     : pid
									},{
										xtype     : 'hidden',
										name      : 'type',
										value     : 'change-status'
									}],
							
							buttons: [{
									text: 'Change',
									handler: function(){
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												win.close();
												Ext.Msg.alert("Follow Up", 'Status Changed.');
												storemyfollowcomplete.load();
												//storemyfollowcompleteAll.load();
											},
											failure: function(form, action) {
												loading_win.hide();
												Ext.Msg.alert("Failure", "ERROR");
											}
										});
									}
								},{
									text: 'Reset',
									handler  : function(){
										simple.getForm().reset();
										win.close();
									}
								}]
							});
						 
						var win = new Ext.Window({
							layout      : 'fit',
							width       : 240,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'close',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									win.close();
									loading_win.hide();
								}
							}]
						});
						win.show();
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			}
		}
	});
	
	loadedBCT=true;
	storemyfollowcomplete.load({params:{start:0, limit:limitmyfollowcomplete}});
</script>