<?php
	include("../../properties_conexion.php");
	conectar();	

	$userid 	= $_POST['userid'];
	$pid		= isset($_POST['pid']) ? $_POST['pid'] : 0;
	$type 		= $_POST['type'];
	$period1 	= $_POST['period1'];
	$period2 	= $_POST['period2'];
	$from		= $_POST['from'];
	$to			= $_POST['to'];
	$loadType	= $_POST['loadType'];
	
	//Properties
	$all = $afs = $na = $nfs = $s = 0;
	//New
	$vm = $sms = $fax = $email = 0;
	//Status
	$pc = $cs = $or = $pof = $emd = $ad = $co = 0;
	
	//Task Sent
	$scall = $ssms = $sfax = $semail = $sdoc = $smail = $svmail = $sother = 0;
	//Task Received
	$rcall = $rsms = $rfax = $remail = $rdoc = $rmail = $rvmail = $rother = 0;
	//Pendind Task
	$pcall = $psms = $pfax = $pemail = $pdoc = $pmail = $pvmail = $pother = 0;
	
	switch($loadType){
		//Properties Define:
		case 'properties':
			//All
			$query="SELECT count(*) FROM xima.followup WHERE userid=$userid";
			if($type=='B')
				$query.=" AND (type='F' or type='FM' or type='B' or type='BM')";
			else
				$query.=" AND type='$type'";
				
			if($pid!=0){
				$query.=" AND parcelid='$pid'";
			}
			
			$result = mysql_query($query);
			$r = mysql_fetch_array($result);
			
			$all = $r[0];
			
			//Active For Sale
			$query2 = $query . " AND status='A'";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$afs = $r[0];
			
			//Non-Active
			$query2 = $query . " AND status='NA'";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$na = $r[0];
			
			//Not For Sale
			$query2 = $query . " AND status='NF'";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$nfs = $r[0];
			
			//Sold
			$query2 = $query . " AND status='S'";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$s = $r[0];
			
			echo json_encode(array(
				'all'	=> $all, 
				'afs'	=> $afs, 
				'na'	=> $na, 
				'nfs'	=> $nfs, 
				's'		=> $s));
		break;
		
		case 'new':
			$query="SELECT count(*) 
			FROM xima.follow_emails f 
			LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
			WHERE f.userid=$userid AND f.seen=0 AND f.type=0";
			
			if($pid!=0){
				$query.=" AND fe.parcelid='$pid'";
			}
			
			//Voice Mail
			$query2 = $query . " AND (task=15 OR task=16)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$vm = $r[0];
			
			//SMS
			$query2 = $query . " AND (task=1 OR task=2)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$sms = $r[0];
			
			//Fax
			$query2 = $query . " AND (task=3 OR task=4)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$fax = $r[0];
			
			//Email
			$query2 = $query . " AND (task=5 OR task=6)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$email = $r[0];
			
			echo json_encode(array(
				'vm' 	=> $vm,
				'sms' 	=> $sms,
				'fax' 	=> $fax,
				'email' => $email)
			);
		break;
		
		//Status Define:
		case 'status':
			$query="SELECT count(*) FROM xima.followup WHERE userid=$userid";
			if($type=='B')
				$query.=" AND (type='F' or type='FM' or type='B' or type='BM')";
			else
				$query.=" AND type='$type'";
			
			if($pid!=0){
				$query.=" AND parcelid='$pid'";
			}
				
			//Pending Contracts
			$query2 = $query . " AND contract=1";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pc = $r[0];
			
			//Contracts Sent
			$query2 = $query . " AND contract<=0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$cs = $r[0];
			
			//Offers Received 
			$query2 = $query . " AND offerreceived = 0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$or = $r[0];
			
			//Proof of Funds 
			$query2 = $query . " AND pof = 0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pof = $r[0];
			
			//Earnes Money Deposit 
			$query2 = $query . " AND emd = 0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$emd = $r[0];
			
			//Addemdums 
			$query2 = $query . " AND realtorsadem = 0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$ad = $r[0];
			
			//Counter Offers 
			$query2 = $query . " AND coffer > 0";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$co = $r[0];
			
			echo json_encode(array(
				'pc'	=> $pc,
				'cs'	=> $cs,
				'or'	=> $or,
				'pof'	=> $pof,
				'emd'	=> $emd,
				'ad'	=> $ad,
				'co'	=> $co));
		break;
		
		//Task
		case 'task':
			$query="SELECT count(*) FROM xima.followup_history WHERE userid=$userid AND follow_type='$type'";
			if($period1=='T'){
				$query.=" AND odate = NOW()";
			}elseif($period1=='LW'){
				$query.=" AND odate BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW()";
			}elseif($period1=='LM'){
				$query.=" AND odate BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW()";
			}elseif($period1=='B'){
				$date1 = $from;
				$date2 = $to;
				$query.=" AND odate BETWEEN '$date1' AND '$date2'";
			}
			
			if($pid!=0){
				$query.=" AND parcelid='$pid'";
			}
			
			//Call
			$query2 = $query . " AND task=9";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$scall = $r[0];
			
			$query2 = $query . " AND task=10";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rcall = $r[0];
			
			//SMS
			$query2 = $query . " AND task=1";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$ssms = $r[0];
			
			$query2 = $query . " AND task=2";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rsms = $r[0];
			
			//Fax
			$query2 = $query . " AND task=3";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$sfax = $r[0];
			
			$query2 = $query . " AND task=4";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rfax = $r[0];
			
			//Email
			$query2 = $query . " AND task=5";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$semail = $r[0];
			
			$query2 = $query . " AND task=6";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$remail = $r[0];
			
			//Documents
			$query2 = $query . " AND task=7";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$sdoc = $r[0];
			
			$query2 = $query . " AND task=8";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rdoc = $r[0];
			
			//Mail
			$query2 = $query . " AND task=11";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$smail = $r[0];
			
			$query2 = $query . " AND task=12";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rmail = $r[0];
			
			//Voice Mail
			$query2 = $query . " AND task=15";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$svmail = $r[0];
			
			$query2 = $query . " AND task=16";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rvmail = $r[0];
			
			//Other
			$query2 = $query . " AND task=13";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$sother = $r[0];
			
			$query2 = $query . " AND task=14";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$rother = $r[0];
			
			echo json_encode(array(
				'taskSent' => array(
					'scall'  => $scall,
					'ssms' 	 => $ssms,
					'sfax' 	 => $sfax,
					'semail' => $semail,
					'sdoc' 	 => $sdoc,
					'smail'  => $smail,
					'svmail' => $svmail,
					'sother' => $sother),
					
				'taskReceived' => array(
					'rcall'  => $rcall,
					'rsms' 	 => $rsms,
					'rfax' 	 => $rfax,
					'remail' => $remail,
					'rdoc' 	 => $rdoc,
					'rmail'  => $rmail,
					'rvmail' => $rvmail,
					'rother' => $rother)
				)
			);
		break;
		
		//Pending Task
		case 'pending':
			$query="SELECT count(*) FROM xima.followup_schedule WHERE userid=$userid AND follow_type='$type'";
			if($period2=='D'){
				$query.=" AND odate < NOW()";
			}elseif($period2=='T'){
				$query.=" AND odate = NOW()";
			}elseif($period2=='1W'){
				$query.=" AND odate BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 1 WEEK)";
			}elseif($period2=='1M'){
				$query.=" AND odate BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 1 MONTH)";
			}elseif($period2=='B'){
				$date1 = $from;
				$date2 = $to;
				$query.=" AND odate BETWEEN '$date1' AND '$date2'";
			}
			
			if($pid!=0){
				$query.=" AND parcelid='$pid'";
			}
			
			//Call
			$query2 = $query . " AND (task=9 OR task=10)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pcall = $r[0];
			
			//SMS
			$query2 = $query . " AND (task=1 OR task=2)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$psms = $r[0];
			
			//Fax
			$query2 = $query . " AND (task=3 OR task=4)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pfax = $r[0];
			
			//Email
			$query2 = $query . " AND (task=5 OR task=6)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pemail = $r[0];
			
			//Documents
			$query2 = $query . " AND (task=7 OR task=8)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pdoc = $r[0];
			
			//Mail
			$query2 = $query . " AND (task=11 OR task=12)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pmail = $r[0];
			
			//Voice Mail
			$query2 = $query . " AND (task=15 OR task=16)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pvmail = $r[0];
			
			//Other
			$query2 = $query . " AND (task=13 OR task=14)";
			$result = mysql_query($query2);
			$r = mysql_fetch_array($result);
			
			$pother = $r[0];
			
			echo json_encode(array(
				'pcall' => $pcall,
				'psms' 	=> $psms,
				'pfax' 	=> $pfax,
				'pemail'=> $pemail,
				'pdoc' 	=> $pdoc,
				'pmail' => $pmail,
				'pvmail'=> $pvmail,
				'pother'=> $pother));
		break;
	}
?>