<?php
	include("../../../properties_conexion.php");
	conectar();
	include ("../../../properties_getgridcamptit.php"); 	 
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	$typefollower=$_POST['typefollower']==0?'true':'false';
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowlistDetails_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowlistDetails_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowlistDetails_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
            <div id="myfollowlistDetails_filters"></div><br />
            <div id="myfollowlistDetails_properties" align="left"></div> 
            <div align="left" style="color:#F00; font-size:14px;">
                Right Click -> Overview / Contract 
                <span style="margin-left:15px; margin-right:15px;">
                      -     
                </span>
                Double Click -> Follow History
            </div>
        </div>
	</div>
</div>
<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/Printer-all.js"></script>
<script>
	var ocultarboton=<?php echo $typefollower; ?>;
	var limitmyfollowlistDetails = 50;
	
	//filter variables
	var filteraddressmyfollowlistDetails 	= '';
	var filtermlnumbermyfollowlistDetails 	= '';
	var filteragentmyfollowlistDetails 		= '';
	var filterstatusmyfollowlistDetails 	= 'ALL';
	var filterndatemyfollowlistDetails 		= '';
	var filterndatebmyfollowlistDetails 	= '';
	var filterntaskmyfollowlistDetails 		= '-2';
	var filtercontractmyfollowlistDetails 	= '-1';
	var filterpofmyfollowlistDetails 		= '-1';
	var filteremdmyfollowlistDetails 		= '-1';
	var filterademdumsmyfollowlistDetails 	= '-1';
	var filtermsjmyfollowlistDetails 		= '-1';
	var filterhistorymyfollowlistDetails 	= '-1';
	var filterfield							= 'address';
	var filterdirection						= 'ASC';
	
	var formmyfollowlistDetails = new Ext.FormPanel({
		url:'mysetting_tabs/myfollowup_tabs/properties_followlist.php',
		frame:true,
		bodyStyle:'padding:5px 5px 0;text-align:left;',
		collapsible: true,
		collapsed: false,
		title: 'Filters',
		renderTo: 'myfollowlistDetails_filters',
		id: 'formmyfollowlistDetails',
		name: 'formmyfollowlistDetails',
		items:[{
			xtype: 'fieldset',
			title: 'Filters',
			layout: 'table',
			layoutConfig: {columns:3},
			defaults: {width: 320},
			
			items: [{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Address',
					name		  : 'faddress',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filteraddressmyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Mlnumber',
					name		  : 'fmlnumber',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filtermlnumbermyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype		  : 'textfield',
					fieldLabel	  : 'Agent',
					name		  : 'fagent',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filteragentmyfollowlistDetails=newvalue;
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fndate',
				items	: [{
					xtype		  : 'datefield',
					fieldLabel	  : 'Task Date',
					width		  : 130,
					editable	  : false,
					format		  : 'Y-m-d',
					name		  : 'fndate',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filterndatemyfollowup=newvalue;
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fndateb',
				items	: [{
					xtype		  : 'datefield',
					fieldLabel	  : 'Between',
					width		  : 130,
					editable	  : false,
					format		  : 'Y-m-d',
					name		  : 'fndateb',
					listeners	  : {
						'change'  : function(field,newvalue,oldvalue){
							filterndatebmyfollowup=newvalue;
						}
					}
				}]
			},{
				layout	: 'form',
				id		: 'fntask',
				items	: [{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Task',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-2','-Select-'],
							['-1','All Task'],
							['0','No Task'],
							['1','Send SMS'],
							['2','Receive SMS'],
							['3','Send FAX'],
							['4','Receive FAX'],
							['5','Send EMAIL'],
							['6','Receive EMAIL'],
							['7','Send DOC'],
							['8','Receive DOC'],
							['9','Make CALL'],
							['10','Receive CALL'],
							['11','Send R. MAIL'],
							['12','Receive R. MAIL'],
							['13','Send OTHER'],
							['14','Receive OTHER']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fntaskname',
					value         : '-2',
					hiddenName    : 'fntask',
					hiddenValue   : '-2',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterntaskmyfollowlistDetails = record.get('valor');
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Status', 
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['ALL','All Status'],
							['A','Active'],
							['NA','Non-Active'],
							['NF','Not for Sale'],
							['S','Sold']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fstatusname',
					value         : 'ALL',
					hiddenName    : 'fstatus',
					hiddenValue   : 'ALL',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterstatusmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Contract',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fcontractname',
					value         : '-1',
					hiddenName    : 'fcontract',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filtercontractmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Proof of Funds',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fpofname',
					value         : '-1',
					hiddenName    : 'fpof',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterpofmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'EMD',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'femdname',
					value         : '-1',
					hiddenName    : 'femd',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filteremdmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Addendums',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['0','Yes'],
							['1','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fademdumsname',
					value         : '-1',
					hiddenName    : 'fademdums',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterademdumsmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Message',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['1','Yes'],
							['0','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fmsjname',
					value         : '-1',
					hiddenName    : 'fmsj',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filtermsjmyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			},{
				layout:'form',
				items:[{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'History',
					triggerAction : 'all',
					width		  : 130,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['-1','-Select-'],
							['1','Yes'],
							['0','No']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'fhistoryname',
					value         : '-1',
					hiddenName    : 'fhistory',
					hiddenValue   : '-1',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							filterhistorymyfollowlistDetails = record.get('valor');
							storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
						}
					}
				}]
			}]
		}],
		buttons:[
			{
				iconCls		  : 'icon',
				icon		  : 'http://www.reifax.com/img/toolbar/search.png',
				scale		  : 'medium',
				width		  : 120,
				text		  : 'Search&nbsp;&nbsp; ',
				handler  	  : function(){
					storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
				}				
			},{
				iconCls		  : 'icon',
				icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
				scale		  : 'medium',
				width		  : 120,
				text		  : 'Reset&nbsp;&nbsp; ',  
				handler  	  : function(){
					filteraddressmyfollowlistDetails = '';
					filtermlnumbermyfollowlistDetails = '';
					filteragentmyfollowlistDetails = '';
					filterstatusmyfollowlistDetails = 'ALL';
					filterndatemyfollowlistDetails = '';
					filterndatebmyfollowlistDetails = '';
					filterntaskmyfollowlistDetails = '-2';
					filtercontractmyfollowlistDetails = '-1';
					filterpofmyfollowlistDetails = '-1';
					filteremdmyfollowlistDetails = '-1';
					filterademdumsmyfollowlistDetails = '-1';
					filtermsjmyfollowlistDetails = '-1';
					filterhistorymyfollowlistDetails = '-1';	
					
					Ext.getCmp('formmyfollowlistDetails').getForm().reset();
					
					storemyfollowlistDetails.load({params:{start:0, limit:limitmyfollowlistDetails}});
				}
			}
		]
	});
	
	formmyfollowlistDetails.collapse();
	
	var storemyfollowlistDetails = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php',
		autoLoad: true,
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'history', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $_POST['userid'];?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowlistDetails;
				obj.params.mlnumber=filtermlnumbermyfollowlistDetails;
				obj.params.agent=filteragentmyfollowlistDetails;
				obj.params.status=filterstatusmyfollowlistDetails;
				obj.params.ndate=filterndatemyfollowlistDetails;
				obj.params.ndateb=filterndatebmyfollowlistDetails;
				obj.params.ntask=filterntaskmyfollowlistDetails;
				obj.params.contract=filtercontractmyfollowlistDetails;
				obj.params.pof=filterpofmyfollowlistDetails;
				obj.params.emd=filteremdmyfollowlistDetails;
				obj.params.ademdums=filterademdumsmyfollowlistDetails;
				obj.params.msj=filtermsjmyfollowlistDetails;
				obj.params.history=filterhistorymyfollowlistDetails;
			}
		}
    });
	function creaVentana(dato,r,pid){
							var ocultar=false;
							var princ='';
							var title='';
							if(r.records[dato]==null){
								r.records[dato]=new Array();
								ocultar=true;
								title='New Contact';
								r.records[dato].parcelid=pid;
							}else{
								title='Contact '+(dato+1)+' of '+r.total;
							}
							if(r.records[dato].principal=='1'){
								princ='(Default)';
								title='Contact '+(dato+1)+' of '+r.total+' '+princ;
							}
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
								frame: true,
								title: title,
								width: 400,
								waitMsgTarget : 'Waiting...',
								labelWidth: 75,
								defaults: {width: 230},
								labelAlign: 'left',
								items: [{
											xtype     : 'combo',
											//hidden: !ocultar,
											fieldLabel: 'Contact',
											triggerAction: 'all',
											//forceSelection: true, 
											selectOnFocus: true,
											typeAhead: true,
											typeAheadDelay: 10,
											minChars: 0,
											mode: 'remote',
											store: new Ext.data.JsonStore({
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
												forceSelection: true,
												root: 'records',
												baseParams		: {
													'userid'	:  <?php echo $userid;?>,
													'sort'		: 'agent',
													'dir'		: 'ASC'
												},
												id: 0,
												fields: [
													'agentid',
													'agent'
												]
											}),
											valueField: 'agentid',
											displayField: 'agent',
											name          : 'fagent',
											value         : r.records[dato].agent, //agent.get('agentype'),
											hiddenName    : 'agent',
											hiddenValue   : r.records[dato].agentid, //agent.get('agenttype'),
											listeners	  : {
												select: function(combo,record,index){
													var agentid=record.get('agentid');
													//return;
													Ext.Ajax.request({
														waitMsg: 'Seeking...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															agentid: agentid
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															loading_win.hide();
															var r1=Ext.decode(response.responseText);
															if(r1.total>0){
																var value=r1.records[0];
																//simple.getForm().findField('agent').setValue(value.agent);
																simple.getForm().findField('agenttype').setValue(value.agenttype);
																simple.getForm().findField('company').setValue(value.company);
																simple.getForm().findField('typeemail1').setValue(value.typeemail1);
																simple.getForm().findField('typeemail2').setValue(value.typeemail2);
																simple.getForm().findField('email').setValue(value.email);
																simple.getForm().findField('email2').setValue(value.email2);
																simple.getForm().findField('typeurl1').setValue(value.typeurl1);
																simple.getForm().findField('urlsend').setValue(value.urlsend);
																simple.getForm().findField('typeurl2').setValue(value.typeurl2);
																simple.getForm().findField('urlsend2').setValue(value.urlsend2);
																simple.getForm().findField('typeph1').setValue(value.typeph1);
																simple.getForm().findField('phone1').setValue(value.phone1);
																simple.getForm().findField('typeph2').setValue(value.typeph2);
																simple.getForm().findField('phone2').setValue(value.phone2);
																simple.getForm().findField('typeph3').setValue(value.typeph3);
																simple.getForm().findField('phone3').setValue(value.phone3);
																simple.getForm().findField('typeph4').setValue(value.typeph4);
																simple.getForm().findField('fax').setValue(value.fax);
																simple.getForm().findField('typeph5').setValue(value.typeph5);
																simple.getForm().findField('tollfree').setValue(value.tollfree);
																simple.getForm().findField('typeph6').setValue(value.typeph6);
																simple.getForm().findField('phone6').setValue(value.phone6);
																simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
																simple.getForm().findField('address1').setValue(value.address1);
																simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
																simple.getForm().findField('address2').setValue(value.address2);
																simple.getForm().findField('type').setValue('insert');
																simple.getForm().findField('agentid').setValue(value.agentid);
																//simple.getForm().findField('pid').setValue('');
															}
														}
													});
												},
												change: function(combo, newvalue,oldvalue){
													if(isNaN(newvalue)){
														simple.getForm().findField('agenttype').setValue('Agent');
														simple.getForm().findField('company').setValue('');
														simple.getForm().findField('typeemail1').setValue(0);
														simple.getForm().findField('typeemail2').setValue(0);
														simple.getForm().findField('email').setValue('');
														simple.getForm().findField('email2').setValue('');
														simple.getForm().findField('typeurl1').setValue(0);
														simple.getForm().findField('urlsend').setValue('');
														simple.getForm().findField('typeurl2').setValue(0);
														simple.getForm().findField('urlsend2').setValue('');
														simple.getForm().findField('typeph1').setValue(0);
														simple.getForm().findField('phone1').setValue('');
														simple.getForm().findField('typeph2').setValue(0);
														simple.getForm().findField('phone2').setValue('');
														simple.getForm().findField('typeph3').setValue(0);
														simple.getForm().findField('phone3').setValue('');
														simple.getForm().findField('typeph4').setValue(0);
														simple.getForm().findField('fax').setValue('');
														simple.getForm().findField('typeph5').setValue(0);
														simple.getForm().findField('tollfree').setValue('');
														simple.getForm().findField('typeph6').setValue(0);
														simple.getForm().findField('phone6').setValue('');
														simple.getForm().findField('typeaddress1').setValue(0);
														simple.getForm().findField('address1').setValue('');
														simple.getForm().findField('typeaddress2').setValue(0);
														simple.getForm().findField('address2').setValue('');
														simple.getForm().findField('agentid').setValue('')
													}
												}
											}
										},/*{
											xtype     : 'textfield', 
											name      : 'agent',
											fieldLabel: 'Contact',
											allowBlank: false,
											value	  : r.records[dato].agent
										},*/{
											xtype         : 'combo',
											mode          : 'remote',
											fieldLabel    : 'Type',
											triggerAction : 'all',
											width		  : 130,
											store         : new Ext.data.JsonStore({
												id:'storetype',
												root:'results',
												totalProperty:'total',
												autoLoad: true, 
												baseParams: {
													type: 'agenttype',
													'userid': <?php echo $userid;?>
												},
												fields:[
													{name:'idtype', type:'string'},
													{name:'name', type:'string'}
												],
												url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
											}),
											displayField  : 'name',
											valueField    : 'idtype',
											name          : 'fagenttype',
											value         : r.records[dato].agentype, //agent.get('agentype'),
											hiddenName    : 'agenttype',
											hiddenValue   : r.records[dato].agenttype, //agent.get('agenttype'),
											allowBlank    : false,
											listeners	  : {
												beforequery: function(qe){
													delete qe.combo.lastQuery;
												}
											}
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Email',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyemail1',
												value	  : r.records[dato].typeemail1,
												hiddenName    : 'typeemail1',
												hiddenValue   : r.records[dato].typeemail1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'email',
												value	  : r.records[dato].email,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Email 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyemail2',
												value         : r.records[dato].typeemail2,
												hiddenName    : 'typeemail2',
												hiddenValue   : r.records[dato].typeemail2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'email2',
												value	  : r.records[dato].email2,
												width	  : 165
											}]
										},{
											xtype     : 'textfield',
											name      : 'company',
											fieldLabel: 'Company',
											value	  : r.records[dato].company
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Website 1',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Personal'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyurl1',
												value         : r.records[dato].typeurl1,
												hiddenName    : 'typeurl1',
												hiddenValue   : r.records[dato].typeurl1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'urlsend',
												value     : r.records[dato].urlsend,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Website 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Personal'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyurl2',
												value         : r.records[dato].typeurl2,
												hiddenName    : 'typeurl2',
												hiddenValue   : r.records[dato].typeurl2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'urlsend2',
												value     : r.records[dato].urlsend2,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname1',
												value         : r.records[dato].typeph1,
												hiddenName    : 'typeph1',
												hiddenValue   : r.records[dato].typeph1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone1',
												width	  : 165,
												value	  : r.records[dato].phone1
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname2',
												value         : r.records[dato].typeph2,
												hiddenName    : 'typeph2',
												hiddenValue   : r.records[dato].typeph2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone2',
												width	  : 165,
												value	  : r.records[dato].phone2
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 3',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname3',
												value         : r.records[dato].typeph3,
												hiddenName    : 'typeph3',
												hiddenValue   : r.records[dato].typeph3,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone3',
												width	  : 165,
												value	  : r.records[dato].phone3
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 4',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname4',
												value         : r.records[dato].typeph4,
												hiddenName    : 'typeph4',
												hiddenValue   : r.records[dato].typeph4,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'fax',
												width	  : 165,
												value	  : r.records[dato].fax
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 5',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname5',
												value         : r.records[dato].typeph5,
												hiddenName    : 'typeph5',
												hiddenValue   : r.records[dato].typeph5,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'tollfree',
												width	  : 165,
												value	  : r.records[dato].tollfree
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 6',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname6',
												value         : r.records[dato].typeph6,
												hiddenName    : 'typeph6',
												hiddenValue   : r.records[dato].typeph6,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone6',
												width	  : 165,
												value	  : r.records[dato].phone6
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Address 1',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyaddress1',
												value	  : r.records[dato].typeaddress1,
												hiddenName    : 'typeaddress1',
												hiddenValue   : r.records[dato].typeaddress1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'address1',
												value	  : r.records[dato].address1,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Address 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyaddress2',
												value         : r.records[dato].typeaddress2,
												hiddenName    : 'typeaddress2',
												hiddenValue   : r.records[dato].typeaddress2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'address2',
												value	  : r.records[dato].address2,
												width	  : 165
											}]
										},{
											xtype     : 'hidden',  
											name      : 'type'
										},{
											xtype     : 'hidden',
											name      : 'agentid',
											value     : r.records[dato].agentid
										},{
											xtype     : 'hidden',
											name      : 'pid',
											value     : pid
										}],
										buttons: [{
											text     : 'Save',
											id: 'agentsave',
											hidden	 : !ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(-1,r);*/
												loading_win.show();
												if(simple.getForm().findField('agentid').getValue()==''){
													simple.getForm().findField('type').setValue('insert'); 
													simple.getForm().submit({
														success: function(form, action) {
																if(action.result.agentid!=''){
																	Ext.Ajax.request( 
																	{  
																		waitMsg: 'Adding...',
																		url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																		method: 'POST',
																		timeout :600000,
																		params: { 
																			type	: 'assignment-add',
																			agentid	: action.result.agentid, 
																			userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																			pid		: simple.getForm().findField('pid').getValue() 
																		},
																		
																		failure:function(response,options){
																			loading_win.hide();
																			Ext.MessageBox.alert('Warning','ERROR');
																		},
																		success:function(response,options){
																			var resp=Ext.decode(response.responseText);
																			if(resp.existe==0){
																				alert('Contact inserted.');
																			}else{
																				alert('Contact already assigned.');
																			}
																			pid=simple.getForm().findField('pid').getValue();
																			win.close();
																			loading_win.show();
																			Ext.Ajax.request({
																				waitMsg: 'Seeking...',
																				url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																				method: 'POST',
																				timeout :600000,
																				params: { 
																					pid: pid, 
																					userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																					type: 'assignment'
																				},
																				
																				failure:function(response,options){
																					loading_win.hide();
																					Ext.MessageBox.alert('Warning','ERROR');
																				},
																				success:function(response,options){
																					loading_win.hide();
																					var r=Ext.decode(response.responseText);
																					creaVentana(0,r,pid);
																				}
																			});
																		}                                
																	});
																}
															
														},
														failure: function(form, action) {
															loading_win.hide();
															if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
																Ext.Msg.alert('Error',
																	'Status:'+action.response.status+': '+
																	action.response.statusText);
															}
															if (action.failureType === Ext.form.Action.SERVER_INVALID){
																// server responded with success = false
																Ext.Msg.alert('Invalid', action.result.errormsg);
															}
															if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
																Ext.Msg.alert('Error',
																	'Please check de red market field, before insert contact.');
															}
														}
													});
												}else{
													Ext.Ajax.request( 
													{  
														waitMsg: 'Adding...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															type	: 'assignment-add',
															agentid	: simple.getForm().findField('agentid').getValue(), 
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															pid		: simple.getForm().findField('pid').getValue() 
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															var resp=Ext.decode(response.responseText);
															if(resp.existe==0){
																alert('Contact inserted.');
															}else{
																alert('Contact already assigned.');
															}
															pid=simple.getForm().findField('pid').getValue();
															win.close();
															loading_win.show();
															Ext.Ajax.request({
																waitMsg: 'Seeking...',
																url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																method: 'POST',
																timeout :600000,
																params: { 
																	pid: pid,
																	userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																	type: 'assignment'
																},
																
																failure:function(response,options){
																	loading_win.hide();
																	Ext.MessageBox.alert('Warning','ERROR');
																},
																success:function(response,options){
																	loading_win.hide();
																	var r=Ext.decode(response.responseText);
																	creaVentana(0,r,pid);
																}
															});
														}                                
													});
												}
											}
										},{
											text     : 'Cancel',
											id: 'agentcancel',
											hidden	 : !ocultar,
											handler  : function(){
												pid=simple.getForm().findField('pid').getValue();
												win.close();
												creaVentana(0,r,pid);
											}
										},{
											text     : 'Add',
											id: 'agentadd',
											hidden	 : ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(-1,r);*/
												Ext.getCmp('agentsave').setVisible(true);
												Ext.getCmp('agentcancel').setVisible(true);
												Ext.getCmp('agentadd').setVisible(false);
												Ext.getCmp('agentupdate').setVisible(false);
												Ext.getCmp('agentremove').setVisible(false);
												Ext.getCmp('agentset').setVisible(false);
												Ext.getCmp('agentfirst').setVisible(false);
												Ext.getCmp('agentprev').setVisible(false);
												Ext.getCmp('agentnext').setVisible(false);
												Ext.getCmp('agentlast').setVisible(false);
												simple.setTitle('New Contact');
												//simple.getForm().findField('selectagent').setVisible(true);
												simple.getForm().findField('agent').setValue('');
												simple.getForm().findField('agenttype').setValue('Agent');
												simple.getForm().findField('company').setValue('');
												simple.getForm().findField('typeemail1').setValue(0);
												simple.getForm().findField('typeemail2').setValue(0);
												simple.getForm().findField('email').setValue('');
												simple.getForm().findField('email2').setValue('');
												simple.getForm().findField('typeurl1').setValue(0);
												simple.getForm().findField('urlsend').setValue('');
												simple.getForm().findField('typeurl2').setValue(0);
												simple.getForm().findField('urlsend2').setValue('');
												simple.getForm().findField('typeph1').setValue(0);
												simple.getForm().findField('phone1').setValue('');
												simple.getForm().findField('typeph2').setValue(0);
												simple.getForm().findField('phone2').setValue('');
												simple.getForm().findField('typeph3').setValue(0);
												simple.getForm().findField('phone3').setValue('');
												simple.getForm().findField('typeph4').setValue(0);
												simple.getForm().findField('fax').setValue('');
												simple.getForm().findField('typeph5').setValue(0);
												simple.getForm().findField('tollfree').setValue('');
												simple.getForm().findField('typeph6').setValue(0);
												simple.getForm().findField('phone6').setValue('');
												simple.getForm().findField('typeaddress1').setValue(0);
												simple.getForm().findField('address1').setValue('');
												simple.getForm().findField('typeaddress2').setValue(0);
												simple.getForm().findField('address2').setValue('');
												simple.getForm().findField('type').setValue('insert');
												simple.getForm().findField('agentid').setValue('');
												//simple.getForm().findField('pid').setValue('');
											}
										},{
											text     : 'Update',
											id: 'agentupdate',
											hidden	 : ocultar,
											handler  : function(){
												simple.getForm().findField('type').setValue('update');
												loading_win.show();
												simple.getForm().submit({
													success: function(form, action) {
														loading_win.hide();
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														alert('Contact Successfully Updated.');
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(dato,r,pid);
															}
														});
													},
													failure: function(form, action) {
														loading_win.hide();
														if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
															Ext.Msg.alert('Error',
																'Status:'+action.response.status+': '+
																action.response.statusText);
														}
														if (action.failureType === Ext.form.Action.SERVER_INVALID){
															// server responded with success = false
															Ext.Msg.alert('Invalid', action.result.errormsg);
														}
														if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
															Ext.Msg.alert('Error',
																'Please check de red market field, before update contact.');
														}
													}
												});	
											}
										},{
											text     : 'Remove',
											id: 'agentremove',
											hidden	 : ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(r.total-1,r);*/
												//simple.getForm().findField('type').setValue('assignment-del');
												Ext.Ajax.request( 
												{  
													waitMsg: 'Removing...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														type	: 'assignment-del',
														agentid	: r.records[dato].agentid, //agentid,
														userid	: r.records[dato].userid, //record.get('userid'),
														pid		: r.records[dato].parcelid, //record.get('pid')
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														alert('Contact Successfully Removed.');
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: r.records[dato].parcelid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid);
															}
														});
													}                                
												});		
											}
										},{
											text     : 'Set Principal',
											id: 'agentset',
											hidden	 : ocultar,
											handler  : function(){
												var agentname = r.records[dato].agent;//agent.get('agent');
												loading_win.show();
												Ext.Ajax.request( 
												{  
													waitMsg: 'Assigning...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														type		: 'assignment-principal',
														agentname	: agentname,
														userid		: r.records[dato].userid,
														pid			: r.records[dato].parcelid
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														alert('Contact Successfully Updated.');
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: r.records[dato].parcelid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid);
															}
														});
													}                                
												});
											}
										}]
								});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 550,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'close',
									buttons: [{
											text     : '|<',
											id: 'agentfirst',
											width: 30, 
											hidden	 : (dato==0), 	
											handler  : function(){
												/*win.close();
												creaVentana(0,r);*/
												dato=0;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '<',
											id: 'agentprev',
											width: 30, 
											hidden	 : (dato-1<0), 	
											handler  : function(){
												/*win.close();
												creaVentana(dato-1,r);*/
												dato=dato-1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '>',
											id: 'agentnext',
											width: 30, 
											hidden	 : (dato+1>r.total-1),
											handler  : function(){
												/*win.close();
												creaVentana(dato+1,r);*/
												dato=dato+1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '>|',
											id: 'agentlast',
											width: 30, 
											hidden	 : (dato+1>r.total-1), 	
											handler  : function(){
												/*win.close();
												creaVentana(r.total-1,r);*/
												dato=r.total-1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1); 
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
										text     : 'Close',
										handler  : function(){
											win.close();
											loading_win.hide();
										}
									}]
								});
								win.show();
							} 
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/drop-no.gif" />';
		else return '<img src="../../../img/drop-yes.gif" />';
	}
	
	function historyRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/notes/new_history.png" />';
		else return '';
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../../img/notes/new_msj.png" />';
		else return '';
	}
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive a Call." src="../../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../../img/notes/reci_other.png" />'; break;
		}
	}
	
		
	var gridmyfollowlistDetails = new Ext.grid.GridPanel({
		renderTo: 'myfollowlistDetails_properties',
		cls: 'grid_comparables',
		width: 920,
		height: 400,
		store: storemyfollowlistDetails,
		stripeRows: true,
		columns: [  
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'M', width: 25, sortable: true, tooltip: 'New Messages Chat.', dataIndex: 'msj', renderer: msjRender},{header: 'H', width: 25, sortable: true, tooltip: 'New or Updated History.', dataIndex: 'history', renderer: historyRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					else
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent'}
				,{header: 'Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Offer.', dataIndex: 'offer'}
				,{header: 'C. Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer'}
				,{header: 'Next Date', width: 60, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate', sortable: true}
				,{header: 'T', width: 25, sortable: true, tooltip: 'Next Task.', dataIndex: 'ntask', renderer: taskRender}
				,{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate'}
				,{header: 'C', width: 25, sortable: true, tooltip: 'Contract Sent.', dataIndex: 'contract', renderer: checkRender}
				,{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}
				,{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}
				,{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}";
		   ?>
		], 
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowlistDetails',
            pageSize: limitmyfollowlistDetails,
            store: storemyfollowlistDetails,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowlistDetails=50;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowlistDetails=80;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowlistDetails=100;
					Ext.getCmp('pagingmyfollowlistDetails').pageSize = limitmyfollowlistDetails;
					Ext.getCmp('pagingmyfollowlistDetails').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),new Ext.Button({
				tooltip: 'Click to print details.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 3,
							userid: <?php echo $_POST['userid'];?>,
							address: filteraddressmyfollowlistDetails,
							mlnumber: filtermlnumbermyfollowlistDetails,
							agent: filteragentmyfollowlistDetails,
							status: filterstatusmyfollowlistDetails,
							ndate: filterndatemyfollowlistDetails,
							ndateb: filterndatebmyfollowlistDetails,
							ntask: filterntaskmyfollowlistDetails,
							contract: filtercontractmyfollowlistDetails,
							pof: filterpofmyfollowlistDetails,
							emd: filteremdmyfollowlistDetails,
							ademdums: filterademdumsmyfollowlistDetails,
							msj: filtermsjmyfollowlistDetails,
							history: filterhistorymyfollowlistDetails,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to export Excel.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 30,
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 3,
							userid: <?php echo $_POST['userid'];?>,
							address: filteraddressmyfollowlistDetails,
							mlnumber: filtermlnumbermyfollowlistDetails,
							agent: filteragentmyfollowlistDetails,
							status: filterstatusmyfollowlistDetails,
							ndate: filterndatemyfollowlistDetails,
							ndateb: filterndatebmyfollowlistDetails,
							ntask: filterntaskmyfollowlistDetails,
							contract: filtercontractmyfollowlistDetails,
							pof: filterpofmyfollowlistDetails,
							emd: filteremdmyfollowlistDetails,
							ademdums: filterademdumsmyfollowlistDetails,
							msj: filtermsjmyfollowlistDetails,
							history: filterhistorymyfollowlistDetails,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			})]
        }),
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_POST['userid'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'Contract its not generated or not in system.');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				var userid = record.get('userid');
				
				if(document.getElementById('reportsTab')){
					var tab = tabs.getItem('reportsTab');
					tabs.remove(tab);
				}
				var followTbar = new Ext.Toolbar({
					cls: 'no-border', 
					width: 'auto',
					items: [' ',{
						tooltip: 'Click to Follow Contacts.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Contacts',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/agent.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									pid: pid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(0,r,pid);
								}
							});
						}
					},{
						 tooltip: 'Click to Short Sale',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Short<br>Sale',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/toolbar/shortsale.png',
						 scale: 'medium',
						  
						 handler: function(){
							var option = 'width=930px,height=555px';	
							 window.open('https://www.reifax.com/xima3/mysetting_tabs/myfollowup_tabs/shortsaleform.php?pid='+record.get('pid') ,'Signature',option);
							 
						 }
					},'->',{
						 tooltip: 'Click to Close Follow',
						 iconCls:'icon',
						 iconAlign: 'top',
						 text: 'Close',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/cancel.png',
						 scale: 'medium',
						  
						 handler: function(){ 
							 var tab = tabs.getItem('reportsTab');
							 tabs.remove(tab);
						 }
					}]
				}); 
				var tbarhistory =  new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to Delete Selected Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Delete',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/del_doc.png',
						 scale: 'medium',
						 handler: function(){
							var followfuh = followsm.getSelections();
							var idfuh = '';
							if(followfuh.length > 0){
								idfuh=followfuh[0].get('idfuh');
								for(i=1; i<followfuh.length; i++){
									idfuh+=','+followfuh[i].get('idfuh');
								}
							}
							Ext.Ajax.request( 
							{  
								waitMsg: 'Deleting...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									type: 'delete',
									idfuh: idfuh
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
									followstore.load();
								}                                
							});
						 }
					},{
						 tooltip: 'Click to New Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'New',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Add History',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : '0',
											hiddenName    : 'task',
											hiddenValue   : '0',
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail'
										},{
											xtype	  : 'checkboxgroup',
											fieldLabel: 'Document',
											columns	  : 3,
											itemCls   : 'x-check-group-alt',
											items	  : [
												{boxLabel: 'Contract', name: 'contract'},
												{boxLabel: 'Proof of Funds', name: 'pof'},
												{boxLabel: 'EMD', name: 'emd'},
												{boxLabel: 'Addendums', name: 'rademdums'},
												{boxLabel: 'Offer Received', name: 'offerreceived'}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										},{
											xtype     : 'hidden',
											name      : 'userid',
											value     : record.get('userid') 
										}],
								
								buttons: [{
										text: 'Add',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow History", 'New Follow History.');
													followstore.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 400,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							
							message_win.show();
							setTimeout('message_win.hide()',4000);
						 }
					},{
						 tooltip: 'Click to Update Select Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Update',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/refresh.gif',
						 scale: 'medium',
						 handler: function(){
							var followfuh = followsm.getSelections();
							if(followfuh.length > 1){
								Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
								return false;
							}else if(followfuh.length == 0){
								Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Update History',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0,
											value	  : followfuh[0].get('offer')
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0,
											value	  : followfuh[0].get('coffer')
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfuh[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfuh[0].get('task'),
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'sheduledetail',
											fieldLabel: 'Schedule Detail',
											value	  : followfuh[0].get('sheduledetail')
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail',
											value	  : followfuh[0].get('detail')
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 3,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: !followfuh[0].get('contract')},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh[0].get('pof')},
												{boxLabel: 'EMD', name: 'emd', checked: !followfuh[0].get('emd')},
												{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh[0].get('realtorsadem')},
												{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh[0].get('offerreceived')}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfuh',
											value     : followfuh[0].get('idfuh')
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Up", 'Updated Follow History.');
													followstore.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 500,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							message_win.show();
							setTimeout('message_win.hide()',4000);
						 }
					},new Ext.Button({
						tooltip: 'Click to print history.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Print',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 4,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'export',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 4,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});
				
				var tbarschedule = new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to Delete Selected Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Delete',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/del_doc.png',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							var idfus = '';
							if(followfus.length > 0){
								idfus=followfus[0].get('idfus');
								for(i=1; i<followfus.length; i++){
									idfus+=','+followfus[i].get('idfus');
								}
							}
							Ext.Ajax.request( 
							{  
								waitMsg: 'Deleting...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									type: 'delete',
									idfus: idfus
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
									followstore_schedule.load();
								}                                
							});
						 }
					},{
						 tooltip: 'Click to New Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'New',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Add Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype		  : 'datefield',
											allowBlank	  : false,
											name		  : 'odate',
											fieldLabel	  : 'Date',
											format		  : 'Y-m-d'
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : '0',
											hiddenName    : 'task',
											hiddenValue   : '0',
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Schedule Detail'
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										},{
											xtype     : 'hidden',
											name      : 'userid',
											value     : record.get('userid') 
										}],
								
								buttons: [{
										text: 'Add',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'New Follow Schedule.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 400,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							message_win.show();
							setTimeout('message_win.hide()',4000);
						 }
					},{
						 tooltip: 'Click to Update Select Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Update',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/refresh.gif',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							if(followfus.length > 1){
								Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
								return false;
							}else if(followfus.length == 0){
								Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Update Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     	  : 'datefield',
											name      	  : 'odate',
											fieldLabel	  : 'Date',
											value	  	  : followfus[0].get('odate'),
											format	  	  : 'Y-m-d'
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfus[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfus[0].get('task'),
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Schedule Detail',
											value	  : followfus[0].get('detail')
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfus',
											value     : followfus[0].get('idfus')
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'Updated Follow Schedule.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 400,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							message_win.show();
							setTimeout('message_win.hide()',4000);
						 }
					},{
						 tooltip: 'Click to Complete Follow Schedule Task.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Complete<br>Task',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/refresh.png',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							if(followfus.length > 1){
								Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
								return false;
							}else if(followfus.length == 0){
								Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Complete Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfus[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfus[0].get('task'),
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'sheduledetail',
											fieldLabel: 'Schedule Detail',
											value	  : followfus[0].get('detail')
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail'
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 3,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: false},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: false},
												{boxLabel: 'EMD', name: 'emd', checked: false},
												{boxLabel: 'Addendums', name: 'rademdums', checked: false},
												{boxLabel: 'Offer Received', name: 'offerreceived'} 
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'complete'
										},{
											xtype     : 'hidden',
											name      : 'idfus',
											value     : followfus[0].get('idfus')
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										},{
											xtype     : 'hidden',
											name      : 'userid',
											value     : record.get('userid') 
										}],
								
								buttons: [{
										text: 'Complete',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'Completed Follow Schedule Task.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 530,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},new Ext.Button({
						tooltip: 'Click to print history.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Print',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 5,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Export',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 5,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});
				
				var tbarmessages = new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to post message.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 hidden: ocultarboton,
						 text: 'Post',
						 width: 60, 
						 height: 70,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followmessages.php',
								frame: true,
								title: 'Follow Messages',
								width: 450,
								waitMsgTarget : 'Waiting...',
								labelWidth: 75,
								defaults: {width: 330},
								labelAlign: 'left',
								items: [{
											xtype	  :	'textarea',
											name	  : 'sms',
											fieldLabel: 'Message',
											value	  : '',
											blankText : 'post a Message...',
											height	  : 200
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'userid',
											value     : record.get('userid')
										},{
											xtype     : 'hidden',
											name      : 'pid',
											value     : record.get('pid') 
										}],
								
								buttons: [{
										text: 'Post',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Messages", 'Posted messages sucesfully.');
													followstore_messages.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close(); 
										} 
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 440,
								height      : 350,
								modal	 	: true,  
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						}
					},new Ext.Button({
						tooltip: 'Click to print messages.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Print',
						width: 60, 
						height: 70,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 6,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						hidden: ocultarboton,
						text: 'Export',
						 width: 60, 
						 height: 70,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 6,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});
				tabs.add({
					title: ' Follow ',
					id: 'reportsTab', 
					closable: true,
					items: [
						new Ext.TabPanel({
							id: 'historyTab',
							activeTab: 0,
							width: ancho,
							height: tabs.getHeight(),
							plain:true,
							listeners: {
								'tabchange': function( tabpanel, tab ){
									if(tab.id=='followhistoryInnerTab')
										if(document.getElementById('follow_history_grid')) followstore.load();
									else if(tab.id=='followsheduleInnerTab')
										if(document.getElementById('follow_schedule_grid')) followstore_schedule.load();
								}
							},
							items:[	
								{
									title: 'History',
									id: 'followhistoryInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false},
									tbar: tbarhistory
								},{
									title: 'Schedule',
									id: 'followsheduleInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false},
									tbar: tbarschedule
								},{
									title: 'Messages',
									id: 'followmessagesInnerTab',
									autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followmessages.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
									enableTabScroll:true,
									defaults:{ autoScroll: false},
									tbar: tbarmessages
								}
							]
						})
					],
					tbar: followTbar
				}).show();
			}
		}
	});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowlistDetails_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowlistDetails_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>