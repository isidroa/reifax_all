<?php
	include("../../../properties_conexion.php");
	conectar();
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	$query="update `xima`.`follower` f SET
	f.total=(select count(*) FROM `xima`.`followup` WHERE userid=f.userid),
	f.status1=(select count(*) FROM `xima`.`followup` WHERE userid=f.userid AND status='A'),
	f.status2=(select count(*) FROM `xima`.`followup` WHERE userid=f.userid AND status='NA'),
	f.status3=(select count(*) FROM `xima`.`followup` WHERE userid=f.userid AND status='NF'),
	f.status4=(select count(*) FROM `xima`.`followup` WHERE userid=f.userid AND status='S'),
	f.msj=(select count(*) FROM `xima`.`follow_notification` WHERE userid=f.userid AND followid=f.follower_id AND msj=1),
	f.history=(select count(*) FROM `xima`.`follow_notification` WHERE userid=f.userid AND followid=f.follower_id AND history=1)
	WHERE f.follower_id=$userid";
	mysql_query($query);
?>
<div align="left" id="todo_myfollowlist_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowlist_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
    	<div id="myfollowlist_filters"></div><br />
  		<div id="myfollowlist_properties" align="left"></div> 
	</div>
</div>
<script>
	var limitmyfollowlist = 50;
	
	//filter variables
	var filternamemyfollowlist 		= '';
	var filteruseridmyfollowlist 	= '-1';
	var filteracceptmyfollowlist 	= '-1';
	var filterfield					= 'name';
	var filterdirection				= 'ASC';
	
	
	var storemyfollowlist = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php',
		fields: ['userid','name','total','msj','history','status1','status2','status3','status4','accept',{name: 'typefollower', type: 'int'}],
		root: 'records',
		totalProperty: 'total',
		baseParams: {'userid': <?php echo $userid;?>, 'followlist': true },
		remoteSort: true,
		sortInfo: {
			field: 'name',
			direction: 'ASC'
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.fname=filternamemyfollowlist;
				obj.params.fuserid=filteruseridmyfollowlist;
				obj.params.faccept=filteracceptmyfollowlist;
			}
		}
    });
	var toolbarmyfollowlist=new Ext.Toolbar({
		renderTo: 'myfollowlist_filters',
		items: [
			new Ext.Button({
				tooltip: 'Click to print follow list.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 2,
							fname: filternamemyfollowlist,
							fuserid: filteruseridmyfollowlist,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							//alert(rest.pdf);
							var url='http://www.reifax.com/'+rest.pdf;
							//alert(url);
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to export Excel.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 2,
							fname: filternamemyfollowlist,
							fuserid: filteruseridmyfollowlist,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				scale: 'medium',
				handler: function(){
					filternamemyfollowlist 		= '';
					filteruseridmyfollowlist 	= '-1';
					filteracceptmyfollowlist 	= '-1';
					var formmyfollowlist = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						collapsible: true,
						collapsed: false,
						title: 'Filters',
						id: 'formmyfollowlist',
						name: 'formmyfollowlist',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'fname',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Following',
									width		  : 130,
									name		  : 'fname',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filternamemyfollowlist=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fuserid',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'User ID#',
									width		  : 130,
									name		  : 'fuserid',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteruseridmyfollowlist=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'faccept',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['A','Accept'],
											['D','Denied'],
											['P','Pending']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'facceptame',
									value         : '-1',
									hiddenName    : 'faccept',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filteracceptmyfollowlist = record.get('valor');
										}
									}
								}]
							}]
						}],
						
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowlist.load({params:{start:0, limit:limitmyfollowlist}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filternamemyfollowlist 		= '';
									filteruseridmyfollowlist 	= '-1';
									filteracceptmyfollowlist 	= '-1';
									
									Ext.getCmp('formmyfollowlist').getForm().reset();
									
									storemyfollowlist.load({params:{start:0, limit:limitmyfollowlist}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 300,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowlist,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			})	
		]
	});
	function mentoring(follower_id,name){
		Ext.Msg.show({
			title:'Mentoring',
			msg:'Confirm acceptance to couch '+name,
			buttons: Ext.Msg.YESNOCANCEL,
			closable: false,
			
			fn: function(btn){
				var accept = 'N';
				if(btn=='yes'){ accept='A';}
				if(btn=='no') { accept='D';}
				
				if(accept!='N'){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Saving...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'accept',
							follower_id: follower_id,
							userid: <?php echo $userid;?>,
							accept: accept
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							storemyfollowlist.load();
						}                                
					});
				}
			}
		});
	}
	function typeFollower(value, metaData, record, rowIndex, colIndex, store) {
		var follower = record.get('follower_id');
		if(value==0) return '<img src="../../img/toolbar/search.png" style="width:20px; height: 20px;"/>';
		else return '<img src="../../img/toolbar/update.png" style="width:20px; height: 20px;"/>';		
	}
	function accept(value, metaData, record, rowIndex, colIndex, store) {
		var userid=record.get('userid');
		var name=record.get('name');
		switch(value){
			case 'A': return '<img src="../../../img/drop-yes.gif" title="Accept" onclick="javascript:mentoring('+userid+',\''+name+'\');"/>'; break;
			case 'D': return '<img src="../../../img/drop-no.gif" title="Denied" onclick="javascript:mentoring('+userid+',\''+name+'\');"/>'; break;
			case 'P': return '<img src="../../../img/refresh.gif" title="Pending" onclick="javascript:mentoring('+userid+',\''+name+'\');"/>'; break;
		}		
	}
		
	var gridmyfollowlist = new Ext.grid.GridPanel({
		renderTo: 'myfollowlist_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 500,
		store: storemyfollowlist,
		stripeRows: true,
		columns: [
			<?php 
				echo "{header: 'Following', width: 300, sortable: true, tooltip: 'Following Name.', dataIndex: 'name'}";
				echo ",{header: 'S', width: 30, tooltip: 'Status of Mentoring.', dataIndex: 'accept', renderer: accept}";
				echo ",{header: 'Type', width: 50, tooltip: 'Type follower.', dataIndex: 'typefollower', renderer: typeFollower}";
				echo ",{header: 'Total', width: 70, sortable: true, tooltip: 'Total of properties.', dataIndex: 'total'}";
				echo ",{header: 'Active', width: 70, sortable: true, tooltip: 'Total of properties Active.', dataIndex: 'status1'}";
				echo ",{header: 'Non-Active', width: 70, sortable: true, tooltip: 'Total of properties Non-Active.', dataIndex: 'status2'}";
				echo ",{header: 'Not For Sale', width: 70, sortable: true, tooltip: 'Total of properties Not Fot Sale.', dataIndex: 'status3'}";
				echo ",{header: 'Sold', width: 70, sortable: true, tooltip: 'Total of properties.', dataIndex: 'status4'}";
				echo ",{header: 'Messages', width: 70, sortable: true, tooltip: 'Total of properties with Messages.', dataIndex: 'msj'}";
				echo ",{header: 'History', width: 70, sortable: true, tooltip: 'Total of properties with History.', dataIndex: 'history'}";
		   ?>
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowlist',
            pageSize: limitmyfollowlist,
            store: storemyfollowlist,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowlist=50;
					Ext.getCmp('pagingmyfollowlist').pageSize = limitmyfollowlist;
					Ext.getCmp('pagingmyfollowlist').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowlist=80;
					Ext.getCmp('pagingmyfollowlist').pageSize = limitmyfollowlist;
					Ext.getCmp('pagingmyfollowlist').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowlist=100;
					Ext.getCmp('pagingmyfollowlist').pageSize = limitmyfollowlist;
					Ext.getCmp('pagingmyfollowlist').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }), 
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'cellclick': function(grid, rowIndex, colIndex, e){
				if(colIndex==1){
					return;
				}
				var record = grid.getStore().getAt(rowIndex);
				var tabs = Ext.getCmp('terceario');
				
				if(document.getElementById('detailsTab5')){
					var tab = tabs.getItem('detailsTab5');
					tabs.remove(tab);
				}
				
				var tabsFollowDetail = new Ext.TabPanel({
					activeTab: 0,
					id: 'tabsFollowDetail',
					width: ancho,
					height: tabs.getHeight(),
					plain:true,
					enableTabScroll:true,
					defaults:{  autoScroll: false},
					items:[
						  {
							title: 'Following',
							id: 'FollowingDetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_myfollowup.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  },
						  {
							title: 'Pending Tasks',
							id: 'PendingtasksDetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_mytasks.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  },
						  {
							title: 'Pending Contracts',
							id: 'Pendingcontractsdetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_mycontracts.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  },{
							title: 'Import',
							id: 'importfollowdetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_importfollow.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  },{
							title: 'Contacts',
							id: 'ContactsDetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_myfollowagent.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  },
						  {
							title: 'Mailing Campaigns',
							id: 'MailingDetail3',
							autoLoad: {
								url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_list_myfollowmail.php', 
								scripts: true,
								params	: {
									userid: record.get('userid'),
									typefollower: record.get('typefollower')
								}, 
								timeout	: 10800
							}
						  }
						 ]
				});
				tabs.add({
					title: 'Details',
					id: 'detailsTab5',
					closable: true,
					plain:true,
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					items: [tabsFollowDetail]
					/*autoLoad: {
						url		: 'mysetting_tabs/myfollowup_tabs/myfollowlist/details.php', 
						scripts	: true,
						params	: {
							userid: record.get('userid'),
							typefollower: record.get('typefollower')
						}, 
						timeout	: 10800
					}*/
				}).show();
			}
		}
	});
	
	storemyfollowlist.load();

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowlist_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowlist_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>