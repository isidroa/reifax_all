<?php
	include_once("../../properties_conexion.php");
	include_once("receivemail.class.php");
	conectar();	
	
	function completeTaskFollow($userid,$idinsert,$pid,$task=5,$followtype='B',$contract=1,$pof=1,$emd=1,$adem=1,$offer=0){
		$userid_follow = $_COOKIE['datos_usr']['USERID'];
		$query='SELECT s.*, concat(x.name," ",x.surname) as name_follow  
			FROM xima.followup_schedule s left join xima.ximausrs x on s.userid_follow=x.userid
			WHERE s.parcelid="'.$pid.'" AND s.userid='.$userid.' 
			ORDER BY s.odate desc';
		$result=mysql_query($query) or die($query.mysql_error());
		while($r=mysql_fetch_array($result)){
			$tassch = $r['task'];
			if($tassch==$task){ 
				/*$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type)
				VALUES ("'.$pid.'",'.$userid.',NOW(),'.$offer.',0, "'.$task.'",'.$contract.','.$pof.','.$emd.','.$adem.','.$offerreceived.',"'.$r['detail'].'","'.$r['detail'].'",'.$userid_follow.',"'.$r['follow_type'].'")';
				mysql_query($query) or die($query.mysql_error());*/
				
				$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$r['idfus'].')';
				mysql_query($query) or die($query.mysql_error());
			}
		}
	}
	
	function assigmentProperty($userid,$idinsert,$pid,$task=5,$followtype='B',$contract=1,$pof=1,$emd=1,$adem=1,$offer=0){
		//Insert assigment Email
		insertEmailAssingment($userid,$idinsert,$pid);
		$tabs = $followtype=='B' ? 'tabsFollow' : 'tabsFollowSelling';
		
		if($task==0){
			$detail='Contract and Documents Sent By Bulk Contract. ';
			$detail.='<a href=\"javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$pid.'\','.$tabs.');\" >View Content.</a>';
			$task=7;
		}else{
			$detail='<a href=\"javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$pid.'\','.$tabs.');\" >View Content.</a>';
		}
		//Insert Follow History
		$query='INSERT INTO xima.followup_history 
		(parcelid, userid, odate, offer, coffer, task, contract, pof, emd, 
		realtorsadem, offerreceived, detail, userid_follow, follow_type)
		VALUES ("'.$pid.'",'.$userid.',NOW(),'.$offer.',0,'.$task.','.$contract.','.$pof.','.$emd.','.$adem.',1,
		"'.$detail.'",
		'.$userid.',\''.$followtype.'\')';
		mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
	}
	
	function assingmentEmail($userid,$idmail,$from,$sub,$insertProperty=true){
		//Check phone in from email and subject of mail
		$regex = '/\(\d+\) \d+-\d+|\s\d+\s|\+\d+|\d+@|\d+.\d+/i';
		preg_match_all($regex,$from,$phoneFrom);
		preg_match_all($regex,$sub,$phoneSub);
		
		//Check phone in From Email
		$phone = preg_replace('/\D/','',$phoneFrom[0][0]);
		//phone from domain txt.voice.google.com
		if(strlen($phone)==22) $phone = substr($phone,12,10);
		//phone with +1
		if(strlen($phone)>=11) $phone = substr($phone,1,10);
		//its not any phone
		if(strlen($phone)!=10) $phone = 0;
		
		//Check phone in Subject if not in From Email
		if($phone==0){
			$phone = preg_replace('/\D/','',$phoneSub[0][0]);
			//phone from domain txt.voice.google.com
			if(strlen($phone)==22) $phone = substr($phone,12,10);
			//phone with +1
			if(strlen($phone)>=11) $phone = substr($phone,1,10);
			//its not any phone
			if(strlen($phone)!=10) $phone = 0;
		}
		
		//Check for agent email with from
		$query="SELECT agentid FROM xima.followagent
		WHERE userid=$userid AND (email='$from' OR email2='$from' OR email3='$from' OR 
		email4='$from' OR email5='$from')";
		$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($resAE)>=1){
			$rAE = mysql_fetch_array($resAE);
			
			insertEmailContact($userid,$idmail,$rAE[0],$insertProperty);
			
			return true;
		}elseif($phone!=0){
			//Check for agent phone
			$query="SELECT agentid 
			FROM `xima`.`followagent` 
			WHERE userid=$userid AND
			(concat_ws(',',
			trim(replace(replace(replace(replace(phone1,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone2,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone3,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone4,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone5,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(phone6,'-',''),')',''),'(',''),' ','')),
			trim(replace(replace(replace(replace(fax,'-',''),')',''),'(',''),' ',''))) 
			like '%$phone%') = 1";
			$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			
			if(mysql_num_rows($resAE)>=1){
				$rAE = mysql_fetch_array($resAE);
				
				insertEmailContact($userid,$idmail,$rAE[0],$insertProperty);
				
				return true;
			}
		}

		return false;
	}
	
	function insertEmailContact($userid,$idmail,$agentid,$insertProperty=true){
		$query="SELECT * FROM xima.follow_emails_assigment WHERE idmail=$idmail AND userid=$userid";
		$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($resAE)==0){
			$query="SELECT * 
			FROM `xima`.`follow_assignment` fa
			INNER JOIN xima.followup f ON (fa.userid=f.userid AND f.parcelid=fa.parcelid)
			WHERE type <> 'M' AND fa.userid=$userid AND fa.agentid=$agentid";
			$resAP = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			$numAP = mysql_num_rows($resAP);
			
			if($numAP>0){
				if($numAP==1){
					$r = mysql_fetch_array($resAP);
					$parcelid = $r['parcelid'];
					
					//Insert assigment Email
					$query="INSERT INTO xima.follow_emails_assigment 
					(idmail,userid,agentid) VALUES 
					($idmail,$userid,$agentid)";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
					//Only assign property if not have #MSG-ID#.
					if($insertProperty){
						//Find task of property
						$query="SELECT * FROM xima.follow_emails WHERE userid=$userid and idmail=$idmail";
						$rest = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
						$r = mysql_fetch_array($rest);
						$task = $r['task'];
						
						//Find type of property
						$query="SELECT * FROM xima.followup WHERE userid=$userid and parcelid='$parcelid'
						AND (type <> 'M')";
						$rest = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
						$r = mysql_fetch_array($rest);
						$typefollow = $r['type']=='S' || $r['type']=='LS' ? 'S' : 'B';
	
						assigmentProperty($userid,$idmail,$parcelid,$task,$typefollow);
					}
				}else{
					//Insert assigment Email
					$query="INSERT INTO xima.follow_emails_assigment 
					(idmail,userid,agentid) VALUES 
					($idmail,$userid,$agentid)";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
				}
			}else{
				//Insert assigment Email
				$query="INSERT INTO xima.follow_emails_assigment 
				(idmail,userid,agentid) VALUES 
				($idmail,$userid,$agentid)";
				mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			}
		}else{
			//Update assigment Email
			$query="UPDATE xima.follow_emails_assigment 
			SET agentid='$agentid' WHERE idmail=$idmail AND userid=$userid";
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		}
	}
	
	function insertEmailAssingment($userid,$idmail,$pid){
		$query="SELECT * FROM xima.follow_emails_assigment WHERE idmail=$idmail AND userid=$userid";
		$resAE = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		
		if(mysql_num_rows($resAE)==0){
			//Insert assigment Email
			$query="INSERT INTO xima.follow_emails_assigment 
			(idmail,userid,parcelid) VALUES 
			($idmail,$userid,'$pid')";
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		}else{
			//Update assigment Email
			$query="UPDATE xima.follow_emails_assigment 
			SET parcelid='$pid' WHERE idmail=$idmail AND userid=$userid";
			mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
		}
	}

	$userid = $_POST['userid'];
	$typeEmail = $_POST['typeEmail']; //0->INBOX, 1->OUTBOX
	
	if(isset($_POST['type'])){
		if($_POST['type']=='delete'){
			//Eliminación de los attachment en carpeta
			$query='Select * from xima.follow_emails_attach WHERE idmail IN ('.$_POST['pids'].')';
			$result = mysql_query($query) or die($query.mysql_error());
			
			while($r=mysql_fetch_array($result))
				@unlink("../../MailAttach/$userid/".$r['filename']);
			
			//Eliminacion de los attachment en tabla
			$query='DELETE FROM xima.follow_emails_attach WHERE 
			idmail IN ('.$_POST['pids'].')'; 
			mysql_query($query) or die($query.mysql_error());
			
			//Eliminacion de los bodys
			$query='DELETE FROM xima.follow_emails_body WHERE 
			idmail IN ('.$_POST['pids'].')'; 
			mysql_query($query) or die($query.mysql_error());
			
			//Eliminación de los mails
			$query='DELETE FROM xima.follow_emails WHERE 
			userid='.$userid.' AND idmail IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='searchProperty'){
			$query = "select parcelid, IF(type='S', concat('Selling: ',address), concat('Buying: ',address)) as address
			from xima.followup 
			WHERE userid=$userid 
			AND (type='F' or type='FM' or type='B' or type='BM' or type='S')
			ORDER BY address";

			$result=mysql_query($query) or die($query.mysql_error());

			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;
			
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
		}elseif($_POST['type']=='searchPropertyAgent'){
			$agentid = $_POST['agentid'];
			
			$query="SELECT agenttype from xima.followagent where userid=$userid and agentid=$agentid";
			$res = mysql_query($query) or die($query.mysql_error());
			$r = mysql_fetch_array($res);
			
			if($r[0]==12){
				$query = "select parcelid, address  
				from xima.followup 
				WHERE userid=$userid
				AND (type='F' or type='FM' or type='B' or type='BM' or type='S')";
			}else{				
				$query = "select fa.parcelid,f.address  
				from xima.follow_assignment fa 
				INNER JOIN xima.followup f ON (f.userid=fa.userid AND f.parcelid=fa.parcelid)
				WHERE fa.userid=$userid AND fa.agentid=$agentid
				AND (type='F' or type='FM' or type='B' or type='BM' or type='S')";
			}
			$result=mysql_query($query) or die($query.mysql_error());

			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;
			
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
		}elseif($_POST['type']=='assignmentProperty'){
			$agentid=$_POST['agentid'];
			$idmail=$_POST['idmail'];
			$pid=$_POST['parcelid'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			
			//Busqueda del task del Email
			$query="SELECT task FROM xima.follow_emails WHERE idmail=$idmail and userid=$userid";
			$result = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			$r = mysql_fetch_array($result);
			
			$task = $r['task'];
			
			assigmentProperty($userid,$idmail,$pid,$task,$typeFollow);
			
			echo '{success: true}';
		}elseif($_POST['type']=='assignmentContact'){
			$agentid=$_POST['contactname'];
			$email = $_POST['email'];
			
			$query="SELECT email,email2,email3,email4,email5 FROM xima.followagent WHERE userid=$userid AND agentid=$agentid";
			$result=mysql_query($query) or die($query.mysql_error());
			$r = mysql_fetch_array($result);
			
			if(strlen($r['email'])==0){
				$set = 'email="'.$email.'"';	
			}elseif(strlen($r['email2'])==0){
				$set = 'email2="'.$email.'"';
			}elseif(strlen($r['email3'])==0){
				$set = 'email3="'.$email.'"';
			}elseif(strlen($r['email4'])==0){
				$set = 'email4="'.$email.'"';
			}elseif(strlen($r['email5'])==0){
				$set = 'email5="'.$email.'"';
			}else{
				echo '{success: false, msg:"The contact does not have an email space for the upgrade, please go to \'My Contacts\' to make the assignment."}';
				return false;
			}
			
			$query="UPDATE xima.followagent SET $set WHERE userid=$userid AND agentid=$agentid";
			mysql_query($query) or die($query.mysql_error());
			
			//Assignment agent to Emails.
			$query='SELECT fe.idmail FROM xima.follow_emails fe 
			LEFT JOIN xima.follow_emails_assigment fea ON (fe.idmail=fea.idmail AND fe.userid=fea.userid)
			WHERE fe.userid='.$userid.' 
			AND fe.from_msg="'.$email.'"
			AND fea.agentid IS NULL';
			$result = mysql_query($query) or die($query.mysql_error());
			while($r=mysql_fetch_array($result)){
				$query='INSERT INTO xima.follow_emails_assigment (idmail,userid,agentid)
				VALUES ('.$r['idmail'].','.$userid.','.$agentid.')';
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo '{success: true}';
		}elseif($_POST['type']=='getEmailContent'){
			$mailID = $_POST['mailid'];
			$query  = "SELECT body from xima.follow_emails_body WHERE idmail=$mailID";
			$result = mysql_query($query) or die($query.mysql_error());
			$r 		= mysql_fetch_array($result);
			
			print_r(html_entity_decode($r['body']));
		}elseif($_POST['type']=='getEmailAttach'){ 
			$mailID = $_POST['mailid'];
			
			$query  = "SELECT * from xima.follow_emails_attach WHERE idmail=$mailID";
			$result = mysql_query($query) or die($query.mysql_error());
			
			$attach = array();
			while($r = mysql_fetch_array($result)){
				$attach[] = $r;
			}
			
			echo json_encode(array('success' => true, 'total' => count($attach), 'attach' => $attach));
		}elseif($_POST['type']=='composeEmail'){
			$mailID = is_numeric($_POST['mailid']) ? $_POST['mailid'] : NULL;
			$pid = $_POST['pid'];
			$to = $_POST['to'];
			$sub = addslashes($_POST['subject']);
			$msg = addslashes($_POST['msg']);
			$task = isset($_POST['task']) ? $_POST['task'] : 5;
			$cc = isset($_POST['cc']) && $_POST['cc']==true ? true : false;
			$typeFollow = (isset($_POST['typeFollow']) && strlen($_POST['typeFollow'])>0) ? $_POST['typeFollow'] : 'B';
			$contract = isset($_POST['contract']) ? $_POST['contract'] : 1;
			$pof = isset($_POST['pof']) ? $_POST['pof'] : 1;
			$emd = isset($_POST['emd']) ? $_POST['emd'] : 1;
			$adem = isset($_POST['rademdums']) ? $_POST['rademdums'] : 1;
			$offer = isset($_POST['offer']) ? $_POST['offer'] : 0;
			
			$query='SELECT m.*,x.email,x.name, x.surname 
			FROM xima.contracts_mailsettings m
			INNER JOIN xima.ximausrs x ON (m.userid=x.userid)
			WHERE m.userid='.$userid;
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				$r=mysql_fetch_array($result);
				
				if(strlen($r['server'])>0){
					$user=$r['username'];
					$pass=$r['password'];
					$server=$r['server'];
					$puerto=$r['port'];
					$from = $r['email'];
					$name = $r['name'].' '.$r['surname'];
					$attachEmail = array();
					
					if(isset($_FILES['file7']) && strlen(basename($_FILES['file7']['name']))>3){
						$i=7;
						$aux = 'file'.$i;
						$dir = "../../MailAttach/$userid/";
						//Crear Directorio de Attachment
						if(!is_dir($dir)){
							mkdir($dir);
						}
						
						while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
							//subir archivo
							@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
							$attachEmail[]=$dir.basename($_FILES[$aux]['name']);
														
							$i++;
							$aux = 'file'.$i;
						}
					}
					
					if(isset($_POST['file7']) && strlen($_POST['file7'])>3){
						$i=7;
						$aux = 'file'.$i;
						$dir = "../../MailAttach/$userid/";
								
						while(isset($_POST[$aux]) && strlen($_POST[$aux])>3){
							$attachEmail[]=$dir.$_POST[$aux];
														
							$i++;
							$aux = 'file'.$i;
						}
					}
					
					include_once('../../mailer/class.phpmailer.php');
					
					$email 				= new PHPMailer();
					$email->PluginDir	= '../../mailer/';
					$email->Mailer 		= "smtp";
					$email->Host 		= $server;
					$email->Port 		= $puerto;
					$email->SMTPAuth 	= true;
					$email->Username 	= $user;
					$email->Password 	= $pass;
					
					foreach($attachEmail as $k => $file)
						$email->AddAttachment($file);
					
					
					$email->From = $from;
					$email->FromName = $name;
					$email->AddAddress($to);
					
					//SEND CC for user email
					if($cc && $to!=$from)
						$email->AddAddress($from);
					
					$email->Subject = $sub;
					
										
					$rSearch=array('"',"'");
					$rReplace=array('','');
					
					$attachSend = count($attachEmail)>0 ? 1 : 0;

					//Insert Email
					$query="INSERT INTO  xima.follow_emails 
					(userid, msg_id, from_msg,fromName_msg, to_msg, subject, msg_date, type, attachments,task,seen) 
					VALUES($userid,'".sha1($userid.$sub.date())."','".str_replace($rSearch,$rReplace,$from)."',
					'".str_replace($rSearch,$rReplace,$name)."', '".str_replace($rSearch,$rReplace,$to)."', 
					'".str_replace($rSearch,$rReplace,$sub)."', NOW(), 1, $attachSend, $task, 1)";		
						 
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query))); 
					$idinsert = mysql_insert_id();
					
					//Insert body part
					$query="INSERT INTO  xima.follow_emails_body
					(idmail,body) VALUES
					($idinsert,'$msg')";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
					
					//Insert attach part
					foreach($attachEmail as $k => $file){
						$filename = basename($file);
						
						$query="INSERT INTO  xima.follow_emails_attach
						(idmail,filename) VALUES
						($idinsert,'$filename')";
						mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
					}
					

					$email->Body = stripslashes($msg).'<br>#MSG-ID#='.$idinsert.';';
					
					if($task!=1)
						$email->IsHTML(true);

					if($email->Send()){
						if($pid != '-1'){
							assigmentProperty($userid,$idinsert,$pid,$task,$typeFollow,$contract,$pof,$emd,$adem,$offer);
						}
						echo json_encode(array('success' => true,  'msg' => ''));
					}else
						echo json_encode(array('success' => false, 'msg' => $email->ErrorInfo));
					
				}else
					echo json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'Follow up\' -> \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
			}else
				echo json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'Follow up\' -> \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
		}
	}elseif($_POST['checkmail']==1){
		//echo "Inicio: ".date('Y-m-d H:i:s')."<br>";		
		$query='SELECT * FROM xima.contracts_mailsettings WHERE userid='.$userid;
		$result = mysql_query($query) or die($query.mysql_error());
		
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			
			if(strlen($r['imap_server'])>0){
				$user=$r['imap_username'];
				$pass=$r['imap_password'];
				$server=$r['imap_server'];
				$puerto=$r['imap_port'];
				$fTimeSinc=$r['imap_sinc_fTime']==1;
				
				$rSearch=array('"',"'");
				$rReplace=array('','');
				
				if($r['imap_type']==1){ 
					$protocol='imap';
					$auth=false;
				}elseif($r['imap_type']==2){
					$protocol='pop3';
					$auth=false;
				}elseif($r['imap_type']==3){
					$protocol='imap';
					$auth=true;
				}elseif($r['imap_type']==4){
					$protocol='pop3';
					$auth=true;
				}
				
				$carpeta = $typeEmail == 0 ? 'INBOX' : 'SENT';
				// Creating a object of reciveMail Class
				$obj = new receiveMail($user,$pass,$user,$server,$protocol,$puerto,$auth,$carpeta); 
				//Connect to the Mail Box
				$conE = $obj->connect();
				if($conE!==true){//If connection fails give error message and exit
					echo json_encode(array('success' => false, 'msg' => $conE));
					return false;
				}
				// Get total list of mails in order.
				$mails = $obj->getCountHeaderMail($fTimeSinc,$carpeta);
				//Get total Mails to downloading.
				
				//if($userid==73) print_r($mails);
				
				$cantMail = count($mails);
				$stepMail = $_POST['currentMail'];
				
				if($mails){
					$breakCont=0;
					while($breakCont<10){
												
						$numMail = $mails[($fTimeSinc ? $stepMail : $breakCont)];

						unset($head,$bodymsg);
						$head=$obj->getHeaders($numMail);
						$bodymsg=str_replace($rSearch,$rReplace,htmlentities($obj->getBody($numMail)));
						$head['to']='';

						$query="SELECT idmail FROM xima.follow_emails WHERE userid=$userid AND msg_id='".sha1($head['message_id'])."'";
						$resultCheck = mysql_query($query) or die($obj->close_mailbox()); 
						if(mysql_num_rows($resultCheck)==0 && strlen($head['message_id'])>0){
							$fromEmail = str_replace($rSearch,$rReplace,$head['from']);
							$nameEmail = str_replace($rSearch,$rReplace,$head['fromName']);
							$subEmail  = addslashes(str_replace($rSearch,$rReplace,$head['subject']));
							
							
							$query="INSERT INTO  xima.follow_emails 
							(userid, msg_id, from_msg,fromName_msg, subject, msg_date, size, type) 
							VALUES($userid,'".sha1($head['message_id'])."','".$fromEmail."',
							'".$nameEmail."',
							'".$subEmail."',
							'".$head['datereifax']."','".$head['size']."',$typeEmail)";		
								
							mysql_query($query) or die($obj->close_mailbox()); 
							$idinsert = mysql_insert_id();
							
							
							//Insert body part
							$query="INSERT INTO  xima.follow_emails_body
							(idmail,body) VALUES
							($idinsert,'".addslashes($bodymsg)."')";
							if(mysql_query($query)===false){
								print_r($query);
								print_r(addslashes($bodymsg));
								$obj->close_mailbox();
								return false;
							}
							
							// Get attached File from Mail Return name of file in comma separated string  args.	
							if(!is_dir("../../MailAttach/$userid/")){
								mkdir("../../MailAttach/$userid/");
							}
							
							$str=$obj->GetAttach($numMail,"../../MailAttach/$userid/"); 
							$ar=explode(",",$str);
							foreach($ar as $key=>$value)
							{
								if($value<>'')
								{
									$query="INSERT INTO  xima.follow_emails_attach (idmail, filename) VALUES($idinsert,'$value')";
									mysql_query($query) or die($obj->close_mailbox()); 
									
									$query="UPDATE  xima.follow_emails SET attachments=1 WHERE idmail=$idinsert";
									mysql_query($query) or die($obj->close_mailbox()); 					
								}
							}
							
							//Check for #MSG-ID# in reply email.
							preg_match('/#MSG-ID#=(.*);/', html_entity_decode($bodymsg), $msgid);
							$msgidCount = count($msgid)>0 ? true : false;
							
							//Email assingment
							assingmentEmail($userid,$idinsert,$fromEmail,$subEmail,!$msgidCount);
							
							//Email assingment for reply
							if($msgidCount){
								$val = $msgid[count($msgid)-1];

								$q = "select fea.parcelid, fe.task 
								from xima.follow_emails fe
								INNER JOIN xima.follow_emails_assigment fea ON (fe.userid=fea.userid AND fe.idmail=fea.idmail)
								WHERE fe.idmail='$val'";
								$res = mysql_query($q) or die($obj->close_mailbox());
								
								if(mysql_num_rows($res)>0){
									$r = mysql_fetch_array($res);									
									assigmentProperty($userid,$idinsert,$r[0],($r[1])+1);
								}
							}
						}
						
						$stepMail++;
						//echo "Fin: ".date('Y-m-d H:i:s')."<br>";
						$breakCont++;
						if($_POST['totalMail']>0 && $stepMail >= $_POST['totalMail']){
							$sql="UPDATE xima.contracts_mailsettings SET imap_sinc_fTime=0 WHERE userid=$userid";
							mysql_query($sql) or die($obj->close_mailbox());
							$breakCont=10;
						}
					}
					$obj->close_mailbox();   //Close Mail Box
					unset($obj);
					
				}else{
					$stepMail=0;
					$cantMail=0;
				}
		 	
				$vFilas=array();
				$sql="SELECT f.`idmail`, f.`userid`, f.`from_msg`,f.`fromName_msg`, f.`to_msg`, 
				f.`seen`, f.`subject`, f.`msg_date` , f.`attachments`, IF(fe.agentid is null,0,fe.agentid) ac,
				IF(fe.parcelid is null,
					IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1),
					fe.parcelid) ap, 
				f.`task`, fa.agent, fu.address 
				FROM xima.follow_emails f
				LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
				LEFT JOIN xima.followagent fa ON (f.userid=fa.userid AND fe.agentid=fa.agentid)
				LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
				WHERE f.`userid`=$userid and f.`type`=$typeEmail 
				order by msg_date DESC"; 
				$result2=mysql_query($sql) or die('{success: false, error: "'.str_replace($rSearch,$rReplace,$sql.mysql_error()).'"}');

				$num=mysql_num_rows($result2);
				while ($row=mysql_fetch_array($result2,MYSQL_ASSOC))
					$vFilas[]=$row;	
					
				if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
				else $vFilas2=$vFilas;			
				
				echo '{success: true, totalMail: '.$cantMail.', currentMail: '.$stepMail.', total: '.(($cantMail-$stepMail)+count($vFilas)).', records:'.json_encode($vFilas2).'}';
			}else
				echo json_encode(array('success' => false, 'msg' => 'Not provided the configuration of email reading, please provide it in My Contracts - Mail Settings'));
		} 
	}
	else
	{ 
		$email 	= 	isset($_POST['email']) 		? $_POST['email'] 	: '';
		$toemail =  isset($_POST['toemail']) 	? $_POST['toemail'] : '';
		$name 	= 	isset($_POST['name']) 		? $_POST['name'] 	: '';
		$content = 	isset($_POST['content']) 	? $_POST['content'] : '';
		$datebe = 	isset($_POST['dateBe']) 	? $_POST['dateBe'] 	: '';
		$dateaf = 	isset($_POST['dateAf']) 	? $_POST['dateAf'] 	: '';
		$etype	=	isset($_POST['etype'])		? $_POST['etype']	: -1;
		$ACNP	=	isset($_POST['contactAssignNotProperty']) ? true : false;
		
		$vFilas=array();
		$sql="SELECT f.`idmail`, f.`userid`, f.`from_msg`,f.`fromName_msg`, f.`to_msg`, 
		f.`seen`, f.`subject`, f.`msg_date` , f.`attachments`, IF(fe.agentid is null,0,fe.agentid) ac,
		IF(fe.parcelid is null,
			IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1),
			fe.parcelid) ap, 
		f.`task`, fa.agent, fu.address   
		FROM xima.follow_emails f
		LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
		LEFT JOIN xima.followagent fa ON (f.userid=fa.userid AND fe.agentid=fa.agentid)
		LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
		WHERE f.`userid`=$userid and f.`type`=$typeEmail"; 
		
		//filters
		if($ACNP)
			$sql.=' AND fe.agentid is not null AND fe.parcelid is null 
			AND IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1)=0';
		
		if(trim($name)!='')
			$sql.=' AND f.`fromName_msg` LIKE \'%'.trim($name).'%\'';
		
		if(trim($toemail)!='')
			$sql.=' AND f.`to_msg` LIKE \'%'.trim($toemail).'%\'';
		
		if(trim($email)!='')
			$sql.=' AND f.`from_msg` LIKE \'%'.trim($email).'%\'';
			
		if(trim($content)!='')
			$sql.=' AND f.`subject` LIKE \'%'.trim($content).'%\'';
		
		if($etype!=-1)
			$sql.=' AND (f.`task`='.$etype.' OR f.`task`='.($etype+1).')';
			
		if(trim($datebe)!='' && trim($dateaf)!='')
			$sql.=' AND date(f.`msg_date`) BETWEEM \''.$datebe.'\' AND \''.$dateaf.'\'';
		elseif(trim($datebe)!='')
			$sql.=' AND date(f.`msg_date`)=\''.$datebe.'\'';
		
		//orders
		if(isset($_POST['sort'])) $sql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		else $sql.="ORDER BY msg_date DESC";

		$result2=mysql_query($sql) or die('{success: false, error: "'.str_replace($rSearch,$rReplace,$sql.mysql_error()).'"}');
		$num=mysql_num_rows($result2);
		while ($row=mysql_fetch_array($result2,MYSQL_ASSOC))
			$vFilas[]=$row;		
			
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;	
		
		echo '{success: true, totalMail: 0, currentMail: 0, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}
?>