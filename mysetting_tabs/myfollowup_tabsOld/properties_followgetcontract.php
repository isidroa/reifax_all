<?php
/**
 * generatecontract.php
 *
 * Generate the contract for download.
 *
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *          Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
 * @version 15.04.2011
 */

include "../../properties_conexion.php";
include "../../FPDF/limpiar.php";

//limpiardirpdf2('../../FPDF/generated/');


// -------------------------------------------
//   GET FORM DATA
// -------------------------------------------
$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
$type       = $_POST['type'];
$county     = $_POST['county'];
$parcelid   = $_POST['pid'];
$sendme     = $_POST['sendme'];
conectar();
$q="select bd, offer, f.agent, email, email2 from followup f
	LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$parcelid."')";
$res = mysql_query($q) or die($q.mysql_error());
while($r=mysql_fetch_array($res)){
	$county=$r['bd'];
	$rname      = $r['agent'];
	$remail     = $r['email'];
	$offer      = $r['offer'];
	if($remail==''){
		$remail = $r['email2'];
	}
	if($remail=='' && $sendme!='on'){
		$resp = array('success'=>'true','enviado'=>'0');
		
		echo json_encode($resp);
		return;
	}
}

$templateemail = $_POST['contracttemplate']; 
$completetask=$_POST['completetask']; 
//echo $q.' '.$county; return;
$contrOpc   = $_POST['options'];


$dateAcc    = $_POST['dateAcc'];
$dateClo    = $_POST['dateClo'];

$mlnumber   = $_POST['mlnaux'];
$address    = $_POST['addr'];

$sendmail   = $_POST['sendmail'];
$addendum   = $_POST['addendum'];
$addendata  = $_POST['fieldAddeum'];

$addons     = $_POST['addons'];
$addondata  = $_POST['fieldAddonG'];
$deposit    = $_POST['deposit'];
$inspection = $_POST['inspection'];
$scrow      = $_POST['scrow'];

$chseller   = $_POST['csinfo'];
$sellname   = $_POST['sellname'];

$chbuyer    = $_POST['cbinfo'];
$buyername  = $_POST['buyername'];

$comma      = '';
//if ($contrOpc == '1')
    $comma  = ",";

$listingagent  = $_POST['listingagent'];
$listingbroker    = $_POST['listingbroker'];
$buyeragent = $_POST['buyeragent'];
$buyerbroker      = $_POST['buyerbroker'];
// -------------------------------------------
//   HELPING FUNCTIONS
// -------------------------------------------

// Load the fields from the file that has the
// data extracted with pdftk
function load_field_data( $field_report_fn ) {

    $ret_val = array();
    $fp      = fopen( $field_report_fn, "r" );

    if( $fp ) {
        $line = '';
        $rec  = array();

        while(($line= fgets($fp, 2048))!== FALSE) {

            $line = rtrim( $line );       // remove white spaces

            if( $line== '---' ) {
                if( 0 < count($rec) ) {   // end of the record
                    $ret_val[] = $rec;
                    $rec       = array();
                }
                continue;                 // next line
            }

            // Divide the line between name and value
            $data_pos = strpos( $line, ':' );
            $name     = substr( $line, 0, $data_pos+ 1 );
            $value    = substr( $line, $data_pos+ 2 );

            if( $name == 'FieldStateOption:' ) {
                // Pack FieldStateOption in his own array
                if( !array_key_exists('FieldStateOption:',$rec) )
                    $rec['FieldStateOption:']= array();

                $rec['FieldStateOption:'][]= $value;

            } else
                $rec[ $name ]= $value;

        }

        if( 0< count($rec))  // Pack the end record
            $ret_val[]= $rec;

        fclose( $fp );
    }

    return $ret_val;

}


// Fix to get the correct aspect ratio of a image in a PDF
/*function PixelToDPI($px) {

    return abs(round((($px/110)*72),0));
}*/


// Send the document by email with and attach
function SendEMail($attach,$mln,$address,$userid,$name,$email,$send,$copy,$template,$bd,$pid) {

	conectarPorNameCounty($bd);
	$boundary = md5(uniqid(time()));
	//$copy     = 0;


	$query = "SELECT email,name,surname,hometelephone,mobiletelephone, address
				FROM xima.ximausrs WHERE userid='$userid'";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);

	$phone    = strlen($row[4]) == 0 ? $row[3] : $row[4];
	$userName = $row[1].' '.$row[2];
	$emailUser = $row[0];
	$addressUser = $row[5];
	
	$query    = "SELECT * FROM xima.contracts_mailsettings 
					WHERE userid = $userid";
	$rs       = mysql_query($query);
	$mailInfo = mysql_fetch_array($rs);
	
	if (strlen($mailInfo['server'])>0) {
	
		
		// * --------------------
		
		require_once '../../mailer/class.phpmailer.php';
		
		//Elige template de email a utilizar
		if($template!=0){
			$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$template";
			$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
			$mailTemplate = mysql_fetch_array($resultTemplate);
			
			$usarHtml=true;
			$subject = $mailTemplate['subject'];
			$body = $mailTemplate['body'];
			
			$queryVar = "select p.bath ,p.beds ,p.address ,p.ccoded ,p.city ,p.folio ,p.landv ,p.legal ,p.lsqft ,p.owner,
								p.owner_a ,p.owner_c ,p.owner_p ,p.owner_s ,p.owner_z ,p.saledate ,p.saleprice ,p.sdname ,p.sfp,
								p.stories ,p.taxablev ,p.tsqft ,p.unit ,p.yrbuilt ,p.ozip ,p.waterf, m.lprice,
							    l.mtg_bor1, l.mtg_lender, l.mtg_recdat, l.mtg_amount,
							    f.case_numbe,f.totalpendes,f.defowner1,f.judgedate,f.plaintiff1,f.mtg1type
							from psummary p left join mlsresidential m on p.parcelid=m.parcelid
							left join mortgage l on p.parcelid=l.parcelid
							left join pendes f on p.parcelid=f.parcelid
							where p.parcelid='$pid'";
			$resultVar=mysql_query($queryVar) or die($queryVar.mysql_error());
			$arrayVar=mysql_fetch_assoc($resultVar);
			foreach($arrayVar as $key => $var){
				$subject=str_replace('{%'.$key.'%}',$var,$subject);
				$body=str_replace('{%'.$key.'%}',$var,$body);
			}
			$q="select a.* from xima.followup f
				LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$pid."')";
			$res = mysql_query($q) or die($q.mysql_error());
			$arrayVar=mysql_fetch_assoc($res);
			
			$subject=str_replace('{%contact%}',$arrayVar['agent'],$subject);
			$subject=str_replace('{%contactEmail1%}',$arrayVar['email'],$subject);
			$subject=str_replace('{%contactEmail2%}',$arrayVar['email2'],$subject);
			$subject=str_replace('{%contactPhone1%}',$arrayVar['phone1'],$subject);
			$subject=str_replace('{%contactPhone2%}',$arrayVar['phone2'],$subject);
			$subject=str_replace('{%contactPhone3%}',$arrayVar['phone3'],$subject);
			$subject=str_replace('{%contactPhone4%}',$arrayVar['fax'],$subject);
			$subject=str_replace('{%contactPhone5%}',$arrayVar['tollfree'],$subject);
			$subject=str_replace('{%contactPhone6%}',$arrayVar['phone6'],$subject);
			$subject=str_replace('{%contactWeb1%}',$arrayVar['urlsend'],$subject);
			$subject=str_replace('{%contactWeb2%}',$arrayVar['urlsend2'],$subject);
			$subject=str_replace('{%contactCompany%}',$arrayVar['company'],$subject);
			$subject=str_replace('{%contactAddress1%}',$arrayVar['address1'],$subject);
			$subject=str_replace('{%contactAddress2%}',$arrayVar['address12'],$subject);
			$subject=str_replace('{%userName%}',$userName,$subject);
			$subject=str_replace('{%userEmail%}',$emailUser,$subject);
			$subject=str_replace('{%userPhone%}',$phone ,$subject);
			$subject=str_replace('{%userAddress%}',$addressUser,$subject);
			$subject=strip_tags(htmlspecialchars_decode($subject));
			$subject=str_replace('&nbsp;',' ',$subject);
			
			$body=str_replace('{%contact%}',$arrayVar['agent'],$body);
			$body=str_replace('{%contactEmail1%}',$arrayVar['email'],$body);
			$body=str_replace('{%contactEmail2%}',$arrayVar['email2'],$body);
			$body=str_replace('{%contactPhone1%}',$arrayVar['phone1'],$body);
			$body=str_replace('{%contactPhone2%}',$arrayVar['phone2'],$body);
			$body=str_replace('{%contactPhone3%}',$arrayVar['phone3'],$body);
			$body=str_replace('{%contactPhone4%}',$arrayVar['fax'],$body);
			$body=str_replace('{%contactPhone5%}',$arrayVar['tollfree'],$body);
			$body=str_replace('{%contactPhone6%}',$arrayVar['phone6'],$body);
			$body=str_replace('{%contactWeb1%}',$arrayVar['urlsend'],$body);
			$body=str_replace('{%contactWeb2%}',$arrayVar['urlsend2'],$body);
			$body=str_replace('{%contactCompany%}',$arrayVar['company'],$body);
			$body=str_replace('{%contactAddress1%}',$arrayVar['address1'],$body);
			$body=str_replace('{%contactAddress2%}',$arrayVar['address12'],$body);
			$body=str_replace('{%userName%}',$userName,$body);
			$body=str_replace('{%userEmail%}',$emailUser,$body);
			$body=str_replace('{%userPhone%}',$phone ,$body);
			$body=str_replace('{%userAddress%}',$addressUser,$body);
		}else{
			$usarHtml=false;
			if($userid==933 || $userid==2846 || $userid==2883){
				$subject  = "{$address}-Cash Offer, You Represent Us";
				
				$body   .= "Dear,\r\n\r\n";
				$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
				$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
				$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
				$body    .= "    \t* We close Cash. We are well funded.\r\n";
				$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
				$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
				$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
				$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
				$body   .="Thank you and have an amazing day!\r\n\r\n";
				$body    .= "Best Regards,\r\n";
				$body    .= "{$row[1]} {$row[2]}.\r\n";
				$body    .= "Summit Home Buyers, LLC\r\n";
				$body    .= "$phone\r\n";
			}else{
				$subject  = "Offer for the MLS number $mln, Address {$address}";
				if($userid==1719 || $userid==1641){
					$texto="Dear $name\r\n\r\n";
				}else{
					$texto="Dear Mr/Mrs $name\r\n\r\n";
				}
				$body     = $texto;
				$body   .= "Please find attached a contract with our offer regarding the ";
				$body   .= "property with the address: {$address}\r\n";
				$body   .= "We wait for your prompt response\r\n\r\n";
				$body   .= "Regards {$row[1]} {$row[2]}.\r\n";
				$body   .= "$phone\r\n";
				$body   .= "{$row[0]}\r\n\r\n";
			}
		}
		if ($send == 'on') {
			$sendTo = strtolower($email);
			
			$mail            = new phpmailer();		
			$mail->PluginDir = "../../mailer/";
			$mail->Mailer    = "smtp";
			$mail->Host      = $mailInfo['server'];
			$mail->Port      = $mailInfo['port'];
			$mail->SMTPAuth  = true;	
			$mail->Username  = $mailInfo['username'];
			$mail->Password  = $mailInfo['password'];
			
			// * --------------------
			
			$mail->From     = $mail->Username;
			$mail->FromName = "{$row[1]} {$row[2]}";
		
			$mail->IsHTML($usarHtml);
			$mail->Subject  = $subject;
				
			$mail->Body    = $body;
			
			$mail->AddAttachment($attach);
			
			$mail->AddAddress($sendTo,"$name");
			$mail->Send();
		}
	
		if ($copy == 'on') {
			$sendTo = strtolower($row[0]);
			if($userid==1719){
				$sendTo = 'juandej18@gmail.com';
			}
			$mail            = new phpmailer();		
			$mail->PluginDir = "../../mailer/";
			$mail->Mailer    = "smtp";
			$mail->Host      = $mailInfo['server'];
			$mail->Port      = $mailInfo['port'];
			$mail->SMTPAuth  = true;	
			$mail->Username  = $mailInfo['username'];
			$mail->Password  = $mailInfo['password'];
			
			// * --------------------
			
			$mail->From     = $mail->Username;
			$mail->FromName = "{$row[1]} {$row[2]}";
		
			$mail->IsHTML($usarHtml);
			$mail->Subject  = $subject;
				
			$mail->Body    = $body;
		
			$mail->AddAttachment($attach);
			
			$mail->AddAddress($sendTo,"$name");
			$mail->Send();
		}
	} 
}


// -------------------------------------------
//   DEFAULT VALS
// -------------------------------------------

$userid    = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

$arrayCamp = Array('txtseller','txtbuyer','txtaddress',
                   'txtcounty','txtlegal1','txtlegal2',
                   'txtprice','txtlistingsales','txtbuyeraddress1',
                   'txtselleraddress1','txtpage','txtdatebuyer1',
                   'txtdatebuyer2','txtcollectedfunds','txtselleraddress2','txtselleraddress3',
				   'txtbroker','txtcooperatingsales','txtcooperatingbroker','txtparcelid','txtlegalotro','txtdate');

conectarPorNameCounty($county);


// -------------------------------------------
//   Obtain all the data to fill the document
// -------------------------------------------

$valores      = Array();
$balancevalue = 0;
/*$query        = "SELECT * FROM xima.contracts_default c
                    WHERE contract='".$type."' AND `defaultoption`='Y' AND userid=".$userid;*/
$query        = "SELECT * FROM xima.contracts_custom c
                    WHERE id=".$type." and userid=".$userid;

$result       = mysql_query($query) or die($query.mysql_error());
$total        = mysql_num_rows($result);


if ( $total>0 ) {
	$r              = mysql_fetch_array($result);
    $tplusar=$r['tplactive'];
	if($tplusar==1){
		$valores[1] = $r['tpl1_name'];
    	$valores[8] = $r['tpl1_addr'];
	}else{
		$valores[1] = $r['tpl2_name'];
    	$valores[8] = $r['tpl2_addr'];
	}
	$pdfusar=$r['filename'];
	$borrartemplate = 'N';
} else {

    $query  = "SELECT
                x.`HOMETELEPHONE`, x.`ADDRESS`, x.`STATE`, x.`CITY`, x.`NAME`, x.`SURNAME`
                FROM xima.ximausrs x WHERE userid=".$userid;

    $result         = mysql_query($query) or die($query.mysql_error());
    $r              = mysql_fetch_array($result);
    $valores[1]     = $r['NAME'].' '.$r['SURNAME'];
    $valores[8]     = $r['ADDRESS'];
    $pdfusar        = 'default';
    $borrartemplate = 'N';

}

$query        = "SELECT owner, address, ozip, city, owner_a, owner_c, owner_z, owner_s, phonenumber1 from psummary where parcelid='".$parcelid."'";
$result       = mysql_query($query) or die($query.mysql_error());
$r            = mysql_fetch_array($result);
$valores[0]   = $r['owner'];
$valores[3]   = $county;
$valores[9]   = $r['owner_a'];
$valores[14]   = $r['owner_c'].', '.$r['owner_s'].' '.$r['owner_z'];
$address      = $r['address'];
$city         = $r['city'];
$zip          = $r['ozip'];
$valores[4]   = "Folio Number: ".$parcelid."  Legal Description as Shown in Public Records.";

$query        = "SELECT lprice, mlnumber, agent, address, city, zip from mlsresidential where parcelid='".$parcelid."'";
$result       = mysql_query($query) or die($query.mysql_error());
$r            = mysql_fetch_array($result);

//if ($contrOpc == '1')
    $r['lprice'] = $offer;

$mlnumber = $r['mlnumber'];
$valores[6]   = number_format($r['lprice'],2,'.',"$comma");
$balancevalue = $balancevalue+$r['lprice'];
$valores[7]   = '';//$r['agent'];

if($address=='')
    $address  = $r['address'];

if($city=='')
    $city     = $r['city'];

if($zip=='')
    $zip      = $r['zip'];

$valores[2]   = $address.', '.$city.', '.$zip;
$txtaddress = $valores[2];
$valores[10]  = '1';

$fecha        = date('m-d-Y');
$valores[11]  = $fecha;
$verificafirma="select * from xima.contracts_signature c where type in(1,2) and userid=".$userid;
$result1= mysql_query($verificafirma) or die($verificafirma.mysql_error());
$cantidadf=mysql_num_rows($result1);
if($cantidadf>1)
	$valores[12]  = $fecha;

$valores[7]=$listingagent;
$valores[16]=$listingbroker;
$valores[17]=$buyeragent;
$valores[18]=$buyerbroker;
$valores[19]=$parcelid;
$valores[20]='As Shown in Public Records';
$valores[21]=date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
// -------------------------------------------
//   Generate the document and fill the data
// -------------------------------------------

$contratos       = Array('ContractPurchase','leadBasedPaint','ResidentialContract','TX_unimproved_property','TX_one_to_four_residential','TX_residential_condominium_contract_resale','TX_lead_based_paint');
$camposContratos = Array('contract','lead','residential','tx_unimproved','tx_one_to_four','tx_residential','tx_lead');

$ruta            = getCwd().'/../../overview_contract';
$s               = 0;

if ($pdfusar!='default') {

    //Tomar plantilla de cliente
    $templatePdf = $contratos[$type].$userid.$pdfusar;

    //$orig        = "C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/{$templatePdf}.pdf";
	$orig        = "C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/{$pdfusar}";
    //$dest        = "{$ruta}/{$templatePdf}.pdf";
	$dest        = "{$ruta}/{$pdfusar}";
    copy($orig,$dest);
	
	$nombre = $ruta.'/campos1.pdf.fields'; 

    if (file_exists($nombre))
        unlink($nombre);
	
    //passthru( $ruta.'/pdftk/pdftk '.$templatePdf.'.pdf dump_data_fields >> '.$nombre );
	passthru( $ruta.'/pdftk/pdftk '.$dest.' dump_data_fields >> '.$nombre );
	
    $fields      = $nombre;
	
} else {
    $fields      = $camposContratos[$type].'.pdf.fields';
    $templatePdf = $contratos[$type];
}


// -------------------------------------------
//   Check the data field loaded for the new
//   Document
// -------------------------------------------

$campos                 = Array();
$valoresPredeterminados = Array();
$field_arr              = load_field_data($fields);

foreach( $field_arr as $field ) { // itera en los campos

    $campos[]=$field['FieldName:'];

    if ( $field['FieldName:']=='txtdeposit' || $field['FieldName:']=='txtaditional' || $field['FieldName:']=='txtotherprice' ) {

        if (strlen($deposit)!=0 && $field['FieldName:']=='txtdeposit')
            $field['FieldValue:'] = $deposit;

        $balancevalue=$balancevalue-$field['FieldValue:'];

        $field['FieldValue:'] = number_format($field['FieldValue:'],2,'.',"$comma");

    }

    $valoresPredeterminados[]=$field['FieldValue:'];

}

$valores[13] = number_format($balancevalue,2,'.',"$comma");


// -------------------------------------------
//   Create a new template with the values
// -------------------------------------------

require_once( '../../overview_contract/forge_fdf.php' );

$fdf_data_strings = array();
$fdf_data_names   = array();



foreach( $campos as $key => $value ) {

    $encontrado = true;
    $count      = 0;
    while ($encontrado && $count<count($arrayCamp)) {

        if ($value==$arrayCamp[$count]) {
            $encontrado = false;
            $fdf_data_strings[ strtr($arrayCamp[$count], '~', '.') ] = $valores[$count];
        }

        $count++;

    }
    if($encontrado) {
        if (!stristr($value,'chk'))
            $fdf_data_strings[ strtr($value, '~', '.') ] = htmlspecialchars_decode($valoresPredeterminados[$key]);
    }
}

//if ($contrOpc == '1') {
    $fdf_data_strings[ strtr('txtdateacceptance1', '~', '.') ] = $dateAcc;
    $fdf_data_strings[ strtr('txtdateclosing', '~', '.') ]     = $dateClo;
    $fdf_data_strings[ strtr('txtdayscancel', '~', '.') ]      = $inspection;
//}
if ($addendum=='on')
    $fdf_data_strings[ strtr('txtaditionalterms1', '~', '.') ] = "See additional Addendum page";

if ($chseller=='on')
    $fdf_data_strings[ strtr('txtseller', '~', '.') ] = $sellname;
    
if ($chbuyer=='on')
    $fdf_data_strings[ strtr('txtbuyer', '~', '.') ]  = $buyername;
	
$txtbuyer   = $fdf_data_strings[ strtr('txtbuyer', '~', '.') ];
//$txtaddress = $fdf_data_strings[ strtr('txtaddress', '~', '.') ];
$offerPrice = number_format($fdf_data_strings[ strtr('txtdeposit', '~', '.') ],2,'.',',');


// -------------------------------------------
//   Generate the document and fill the data
// -------------------------------------------

$fields_hidden   = array();
$fields_readonly = array();

$fdf= forge_fdf( '', $fdf_data_strings,
                     $fdf_data_names,
                     $fields_hidden,
                     $fields_readonly );
$fdf_fn = tempnam( '.', 'fdf' );
$fp     = fopen( $fdf_fn, 'w' );

if( $fp ) {

    fwrite( $fp, $fdf );
    fclose( $fp );
	
    //$file    = 'FPDF/generated/'.$contratos[$type].$parcelid.'.pdf';
	$file    = 'FPDF/generated/'.$parcelid.'.pdf';
    $aux = "";
    if ($contrOpc == '1')
        $aux = "flatten";
	//echo $pdfusar; return;
    // FILL THE PDF WITH THE DATA
    //passthru("{$ruta}/pdftk/pdftk {$templatePdf}.pdf fill_form {$fdf_fn} output C:/inetpub/wwwroot/{$file} $aux");
	passthru("{$ruta}/pdftk/pdftk {$dest} fill_form {$fdf_fn} output C:/inetpub/wwwroot/{$file} $aux");


    // FINAL PDF FILE
    $archivo = 'C:/inetpub/wwwroot/'.$file;

	//pdf agregar firmas
	//adjuntaFirma('ContractPurchase', $userid, $archivo);
	
    // --------------
    // ADD NEW PAGES WITH THE ADDENDUMS IF SELECTED

    if ($addendum=='on') {

        require_once '../../FPDF/fpdf.php';

        $pdf = new FPDF('P', 'cm', 'A4');

        $pdf->Open();

        $pdf->SetAutoPageBreak(true);
        $pdf->SetTextColor(0,0,0);

        $y_axis_initial = 25;

        $arrEsp = explode('|', $addendata);
        $carr   = count($arrEsp);

        $pdf->AddPage('P','A4');

        $pdf->SetY(0.5); $pdf->SetX(1.3);

        $pdf->SetFont('Helvetica', 'B', 12);
        $pdf->Cell(0, 2, 'Addendums',0,0,'C');

        $pdf->SetY(2.3); $pdf->SetX(1.3);

        $text = '';

        for ($iK = 1; $iK < $carr; $iK++) {

            $query   = "SELECT content FROM xima.contracts_addendum
                            WHERE userid = $userid AND id=".$arrEsp[$iK]."
                            ORDER BY id ASC";
            $rs      = mysql_query($query);

            if ($rs) {
                $row = mysql_fetch_array($rs);

                $text .= utf8_encode(" - ".$row[0]."\n\n");

            }
        }

        $pdf->SetFont('Helvetica', '', 11);
        $pdf->MultiCell(18,0.5,$text);

        $pdf->Output("{$ruta}/{$userid}.pdf");

        passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/{$userid}.pdf cat output {$ruta}/addend_{$userid}.pdf");

        copy("{$ruta}/addend_{$userid}.pdf","{$archivo}");

    }

    // --------------
    // SCROW DEPOSIT LETTER

    if ($scrow=='on') {

        // Text of the letter
        $date  = date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $doc   = "\n\n$date\n\n";
        $doc  .= "Re: $txtbuyer\n\n";
        $doc  .= "Property Address: $txtaddress\n\n";
        $doc  .= "To Whom It May Concern:\n\n";
        $doc  .= "In connection with the above referenced real estate transaction, ";
        $doc  .= "please note that our title company, has received a deposit from the\n\n";
        $doc  .= "Buyer: $txtbuyer\n\n";
        $doc  .= "In the amount of $ ".number_format($deposit,2)." as earnest money towards the purchase ";
        $doc  .= "of the above captioned property on $date.\n\n";
        $doc  .= "If you have any questions, please do not hesitate to contact the undersigned.\n\n";

        // Get header and Footer
        $query   = "SELECT imagen,place FROM xima.contracts_scrow
                        WHERE userid = $userid ORDER BY place ASC";
        $rs      = mysql_query($query);
        $path   = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/scrows/';
        $imgSc  = null;
        if ($rs) {
            while ($row = mysql_fetch_array($rs)) {
                $imgSc["s{$row['place']}"] = $path.$row[0];
            }
        }

        require_once '../../FPDF/fpdf.php';

        $pdf = new FPDF('P', 'cm', 'A4');

        $pdf->Open();

        $pdf->SetTextColor(0,0,0);

        $pdf->SetAutoPageBreak(false);

        $pdf->AddPage('P','A4');

        $pdf->SetFont('Helvetica', '', 13);

        if ($imgSc['s1']!='') {
            $pdf->SetY(2); $pdf->SetX(1.3);
            $pdf->Image($imgSc['s1'],null,null,18);
        }

        $pdf->SetY(9); $pdf->SetX(1.3);
        $pdf->MultiCell(18,0.5,$doc);

        if ($imgSc['s2']!='') {
            $pdf->SetY(20); $pdf->SetX(1.3);
            $pdf->Image($imgSc['s2'],null,null,18);
        }

        $pdf->Output("{$ruta}/{$userid}.pdf");

        passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/{$userid}.pdf cat output {$ruta}/scrow_{$userid}.pdf");

        copy("{$ruta}/scrow_{$userid}.pdf","{$archivo}");

    }

    // --------------
    // ADD NEW PAGES WITH THE ADDONS IF SELECTED

    if ($addons=='on') {

        $arrEsp = explode('|', $addondata);
        $carr   = count($arrEsp);
		
        for ($iK = 1; $iK < $carr; $iK++) {


			$query   = "SELECT place,document FROM xima.contracts_addonscustom
							WHERE userid = $userid AND type = $type AND id=".$arrEsp[$iK]."
							ORDER BY place ASC";
			$rs      = mysql_query($query);
			
			if ($rs) {
				while ($row = mysql_fetch_array($rs)) {
					//echo 'aqui '.$row[1];
					$fK = 'C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/addons/'.$row[1];
					//echo $fK; 
					copy("$fK","{$ruta}/addon_{$userid}.pdf");

					passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/addon_{$userid}.pdf cat output {$ruta}/finaladdon_{$userid}.pdf");
					
					passthru("{$ruta}/pdftk/pdftk {$ruta}/finaladdon_{$userid}.pdf output {$archivo}");

				}
			}
		}
    }


    // -------------
    // SEND THE DOCUMENT BY EMAIL IF SELECTED

    if ($sendmail=='on' or $sendme=='on') {
        SendEMail($archivo,$mlnumber,$address,$userid,$rname,$remail,$sendmail,$sendme,$templateemail,$county,$parcelid);

        // Save to My Document

        $query  = "SELECT address FROM psummary WHERE parcelid={$parcelid}";
        $rs     = mysql_query($query);
        $row    = mysql_fetch_array($rs);

        $dir    = 'saved_documents';
        $moment = date('YmdHisu');

        $doc    = "C:/inetpub/wwwroot/$dir/{$contratos[$type]}$parcelid_$moment.pdf";
        $url    = "http://www.reifax.com/$dir/{$contratos[$type]}$parcelid_$moment.pdf";

        copy("$archivo","C:/inetpub/wwwroot/$dir/{$contratos[$type]}$parcelid_$moment.pdf");

        $query  = "INSERT INTO xima.saveddoc
                    (usr,parcelid,url,directorio,sdate,address,name)
                VALUES
                    ($userid,$parcelid,'$url','$doc',NOW(),'{$row[0]}','contract_{$contratos[$type]}$parcelid.pdf')";
        $rs     = mysql_query($query);


    }


    unlink( $fdf_fn ); // delete temp file
    @unlink("{$ruta}/finaladdon_{$userid}.pdf");
    @unlink("{$ruta}/addon_{$userid}.pdf");
    @unlink("{$ruta}/addend_{$userid}.pdf");
    @unlink("{$ruta}/scrow_{$userid}.pdf");
    @unlink("{$ruta}/{$userid}.pdf");

    if($borrartemplate=='Y')
        unlink($templatePdf.'.pdf');


	$coffer 	= 0; 
	$task 		= 7;
	$contract 	= 0;
	$pof 		= $_POST['pof']=='on' ? 0:1;
	$emd 		= $_POST['emd']=='on' ? 0:1;
	$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
	$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
	$detail 	= 'Contract and Documents Sent By Bulk Contract';
	$userid_follow = $_COOKIE['datos_usr']['USERID'];
	if ($sendmail=='on'){
		$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow)
		VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.')';
		mysql_query($query) or die($query.mysql_error());
		
		if($completetask=='on'){
			$query='SELECT s.*, concat(x.name," ",x.surname) as name_follow  
			FROM xima.followup_schedule s left join xima.ximausrs x on s.userid_follow=x.userid
			WHERE s.parcelid="'.$parcelid.'" AND s.userid='.$userid.' ORDER BY s.odate desc limit 1';
			$result=mysql_query($query) or die($query.mysql_error());
			$idfus=0;
			while($r=mysql_fetch_array($result))
				$idfus=$r['idfus'];
			
			if($idfus!=0){
				$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$idfus.')';
				mysql_query($query) or die($query.mysql_error());
			}
		}
	}
	if($sendmail=='on'){
		$update="update xima.follow_contracts_sent set cant=cant+1 where userid=$userid";
		mysql_query($update) or die($update.mysql_error());
		$enviadoagente='1';
	}else{
		$enviadoagente='0';
	}
	
    $resp = array('success'=>'true','enviado'=>'1', 'pdf'=>"$file", 'enviadoagente'=> $enviadoagente);
		
	echo json_encode($resp);

} else
   echo '{msg: unable to write temp fdf file}';

?>
