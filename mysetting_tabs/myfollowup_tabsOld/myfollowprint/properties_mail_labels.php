<?php  
define('FPDF_FONTPATH','../../../FPDF/font/');	

$_SERVERXIMA="http://www.reifax.com/";

if(isset($_POST['groupbylevel'])) $_GET['groupbylevel']=$_POST['groupbylevel'];
$_GET['resultType']='basic';
$_POST['ResultTemplate']=-1;

$rest_total=explode(',',$_POST['parcelids_res']);


require_once('../../../FPDF/PDF_Label.php');
include("../../../FPDF/limpiar.php");
include("../../../properties_conexion.php");
	conectar();
	
limpiardirpdf2('../../../FPDF/generated/');	//Se borran los pdf viejos

$type=$_POST['type']; 						
$label_type=intval($_POST['label_type']);	// Formato del Label (Convertido a entero)
$label_size=intval($_POST['label_size']);	// Tamaño de la letra del Label (Convertido a entero)
$label_address_type=intval($_POST['address_type']=='Owner' ? 0 : 1);	// Address Dueño o de la Propiedad (Convertido a entero)
$resident_type=$_POST['resident_type']=='Yes' ? true:false;
$align_type=$_POST['align_type']=='Left'?'L':'C';

$pdf = new PDF_Label($label_type, 'in', 1, 1); // $label_type posee el tipo de label seleccionado en la ventana ss_labels.php
$pdf->Open();
$pdf->Set_Font_Size($label_size); // $label_size posee el tamaño de la letra del label seleccionado en la ventana ss_labels.php.

// Asigna el Margen Superior de cada tipo de label de acuerdo al tamaño de la letra.
switch ($label_type) {
	case 5160: 
		$pdf->SetMargins(0,0);		
	break;
	case 5161:
		$pdf->SetMargins(7.62,2.54);		
	break;
	case 5162:
		$pdf->SetMargins(4.242,4.242);		
	break;
	case 5163:
		$pdf->SetMargins(7.62,2.54);
	break;
	case 5197:
		switch ($label_size){
			case 8: $pdf->Set_Margin_Top(36.3); break;
			case 9: $pdf->Set_Margin_Top(34.3); break;
			case 10: $pdf->Set_Margin_Top(32.3); break;
		} 		
	break;
}


$querybd="SELECT distinct f.bd FROM `xima`.`followup` f order by f.bd";
$result=mysql_query($querybd) or die($querybd.mysql_error());
	
while($r=mysql_fetch_array($result)){
	conectarPorNameCounty($r['bd']);
		
	$res='Current Resident Or';
	$query="SELECT address,ozip,city,'FL' state, owner_a,owner,owner_z,owner_c,owner_s FROM psummary where parcelid IN ('".implode("','",$rest_total)."')";
	
	
	$rest=mysql_query($query) or die($query.mysql_error());
	while($row=mysql_fetch_array($rest)){
		if(!is_null($row["owner"]))		$owner=$row["owner"];
			if($type == "rental")	$owner="Current Resident";
			
		if($label_address_type!==1){
			if(!is_null($row["owner_a"]))	$add=$row["owner_a"];
			if(!is_null($row["owner_z"]))	$zip=$row["owner_z"];
			if(!is_null($row["owner_c"]))	$city=$row["owner_c"];
			if(!is_null($row["owner_s"]))	$state=$row["owner_s"];
			
			if($resident_type)
				$pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s, %s - %s", "$res","$owner", "$add", "$city", "$state","$zip"),$align_type);
			else
				$pdf->Add_PDF_Label(sprintf("%s\n%s\n%s, %s - %s", "$owner", "$add", "$city", "$state","$zip"),$align_type);
		}else{
			if(!is_null($row["address"]))	$add=$row["address"];
			if(!is_null($row["ozip"]))		$zip=$row["ozip"];
			if(!is_null($row["city"]))		$city=$row["city"];
			if(!is_null($row["state"]))		$state=$row['state'];
			
			if($resident_type)
				$pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s, %s - %s", "$res","$owner", "$add", "$city", "$state","$zip"),$align_type);
			else
				$pdf->Add_PDF_Label(sprintf("%s\n%s\n%s, %s - %s", "$owner", "$add", "$city", "$state","$state"),$align_type);
		}			  
	}
}

 
$file='FPDF/generated/'.strtotime("now").'.pdf';
$pdf->Output('C:/inetpub/wwwroot/'.$file, 'F');
echo "{success:true, pdf:'$file'}";
?>