<?php  
	define('FPDF_FONTPATH','../../../FPDF/font/');	
	$_SERVERXIMA="http://www.reifax.com/";
	include('../../../FPDF/mysql_table.php');
	include("../../../FPDF/limpiar.php");
	include("../../../properties_conexion.php");
	conectar();
	
	limpiardirpdf2('../../../FPDF/generated/');	//Se borran los pdf viejos
	
	$printType = $_POST['printType'];
	$pid=$_POST['parcelid'];
	$county=$_POST['county'];
	//printType:
	//0-> Following
	//1-> Agents
	//2-> Follow List
	//3-> Details
	//4-> History
	//5-> Schedule
	//6-> Messages
	
	class PDF extends PDF_MySQL_Table
	{
		function Header()
		{
			global $pid,$printType,$county; 
			//Title
			$this->SetFont('Arial','',10);
			$this->Cell(0,6,'Properties REI FAX.',0,1,'L');
			switch($printType){
				case 0: $this->Cell(0,6,'My Follow Up - Following',0,1,'C'); break;
				case 1: $this->Cell(0,6,'My Follow Up - Agents',0,1,'C'); break;
				case 2: $this->Cell(0,6,'My Follow List - Follow List',0,1,'C'); break;
				case 3: $this->Cell(0,6,'My Follow List - Details',0,1,'C'); break;
				case 4: $this->Cell(0,6,'Follow - History',0,1,'C'); break;
				case 5: $this->Cell(0,6,'Follow - Schedule',0,1,'C'); break;
				case 6: $this->Cell(0,6,'Follow - Messages',0,1,'C'); break;
			}
			
			if($printType>=4 && $printType<=6){
				conectarPorNameCounty($county);
				$query="SELECT p.address,p.unit,p.county,p.city,p.zip FROM properties_php p WHERE parcelid='".$pid."'";
				$result = mysql_query($query) or die($query.mysql_error());
				$r=mysql_fetch_array($result);

				$dir = $r['address'];
				if(strlen(trim($r['unit']))>0) $dir.=' '.$r['unit'];
				$dir.=', '.$r['county'];
				$dir.=', '.$r['city'];
				$dir.=', FLORIDA';
				$dir.=', '.$r['zip']; 
                
				$this->Cell(0,6,$dir,0,1,'C');
				conectar();
			}
			$this->Ln(2);
			//Ensure table header is output
			parent::Header();
		}
	}
	
	$pdf=new PDF('L');		//Landscape -- hoja horizontal
	$pdf->Open();
	$pdf->AddPage();
	
	include("../../../properties_getgridcamptit.php");

	switch($printType){
		case 0: 
			$id = getArray('MYFollow','result');
			
			$ArTab=getCamptitTipo($id, "Tabla",'defa');	
			$ArCamp=getCamptitTipo($id, "Campos",'defa');
			$ArTit=getCamptitTipo($id, "Titulos",'defa');
			$ArSize=getCamptitTipo($id, "r_size",'defa');
			
			for($i=0; $i< count($ArCamp); $i++){
				$pdf->AddCol($ArCamp[$i],$ArSize[$i],$ArTit[$i],'C');
			}
			$pdf->AddCol('offerpercent',12,'Offer %','C');
			$pdf->AddCol('offer',15,'Offer','C');
			$pdf->AddCol('coffer',15,'C. Offer','C');
			$pdf->AddCol('ndate',15,'Next Date','C');
			$pdf->AddCol('ntask',15,'Next Task','C');
			$pdf->AddCol('lasthistorydate',10,'LU','C');
			$pdf->AddCol('contract',10,'C','C');
			$pdf->AddCol('pof',10,'P','C');
			$pdf->AddCol('emd',10,'E','C');
			$pdf->AddCol('rademdums',10,'A','C');  
			
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
			$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
			$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
			$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
			$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
			$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
			$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
			$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
			$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
			
			$xSql='SELECT IF(status="A","Active",IF(status="NA","Non-Active",IF(status="NF","Not For Sale","Sold"))) as status, mlnumber, address, unit, zip, agent, ROUND(((offer/lprice)*100),2) as offerpercent, offer, coffer,
			ndate, IF(ntask=0,"",IF(ntask=1,"Send SMS",IF(ntask=2,"Receive SMS",IF(ntask=3,"Send Fax",IF(ntask=4,"Receive Fax",IF(ntask=5,"Send Email",IF(ntask=6,"Receive Email",IF(ntask=7,"Send Document",IF(ntask=8,"Receive Document",IF(ntask=9,"Make Call",IF(ntask=10,"Receive Call",IF(ntask=11,"Send Regular Mail",IF(ntask=12,"Receive Regular Mail",IF(ntask=13,"Send Other","Receive Other")))))))))))))) as ntask, DATEDIFF(NOW(),lasthistorydate) as lasthistorydate, IF(contract=1,"N","Y") as contract, IF(pof=1,"N","Y") as pof, IF(emd=1,"N","Y") as emd, IF(realtorsadem=1,"N","Y") as rademdums 
			FROM xima.followup 
			WHERE userid ='.$_COOKIE['datos_usr']['USERID'].
			" and (type='F' OR type='FM')"; 
			
			//filters
			if(trim($address)!='')
				$xSql.=' AND address LIKE \'%'.trim($address).'%\'';
				
			if(trim($mlnumber)!='')
				$xSql.=' AND mlnumber=\''.trim($mlnumber).'\'';
				
			if(trim($agent)!='')
				$xSql.=' AND agent LIKE \'%'.trim($agent).'%\'';
			
			if($status!='ALL')
				$xSql.=' AND status=\''.$status.'\'';
				
			if(trim($ndateb)!='' && trim($ndate)!='')
				$xSql.=' AND ndate BETWEEM \''.$ndate.'\' AND \''.$ndateb.'\'';
			elseif(trim($ndate)!='')
				$xSql.=' AND ndate=\''.$ndate.'\'';
				
			if(trim($ntask)!='-2'){
				if(trim($ntask)!='-1')
					$xSql.=' AND ntask='.$ntask;
				else
					$xSql.=' AND ntask<>0';
			}
				
			if($contract!='-1')
				$xSql.=' AND contract='.$contract;
			
			if($ademdums!='-1')
				$xSql.=' AND realtorsadem='.$ademdums;
			
			if($pof!='-1')
				$xSql.=' AND pof='.$pof;
			
			if($emd!='-1')
				$xSql.=' AND emd='.$emd;
				
			if($msj!='-1')
				$xSql.=' AND msj='.$msj;
				
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 1:
			$pdf->AddCol('agent',40,'Agent','C'); 
			$pdf->AddCol('email',60,'Email','C'); 
			$pdf->AddCol('tollfree',35,'Toll Free','C'); 
			$pdf->AddCol('phone1',35,'Phone 1','C'); 
			$pdf->AddCol('phone2',35,'Phone 2','C'); 
			$pdf->AddCol('phone3',35,'Phone 3','C'); 
			$pdf->AddCol('fax',35,'Fax','C');
			$pdf->AddCol('urlsend',60,'Send Page','C'); 
		
			$xSql='SELECT agentid, agent, email, tollfree, phone1, typeph1, phone2, typeph2, phone3, typeph3, fax, urlsend 
			FROM xima.followagent 
			WHERE userid ='.$_COOKIE['datos_usr']['USERID']; 
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir']; 
		break;
		
		case 2: 
			$pdf->AddCol('name',40,'Following','C'); 
			$pdf->AddCol('total',25,'Total','C'); 
			$pdf->AddCol('status1',25,'Active','C'); 
			$pdf->AddCol('status2',25,'Non-Active','C'); 
			$pdf->AddCol('status3',25,'Not For Sale','C'); 
			$pdf->AddCol('status4',25,'Sold','C'); 
			$pdf->AddCol('msj',25,'Messages','C'); 
			$pdf->AddCol('history',25,'History','C'); 
			
			$fname 		= isset($_POST['fname']) 	? $_POST['fname'] 		: '';
			$fuserid 	= isset($_POST['fuserid']) 	? $_POST['fuserid']		: '-1';
			
			$xSql='SELECT f.userid, concat(x.name," ",x.surname) name,
			f.total, f.msj, f.history, f.status1, f.status2, f.status3, f.status4 
			FROM xima.follower f
			INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
			WHERE f.follower_id='.$_COOKIE['datos_usr']['USERID'].' AND f.status="Active"';
			
			//filters
			if(trim($fname)!='')
				$xSql.=' AND concat(x.name," ",x.surname) LIKE \'%'.trim($fname).'%\'';
				
			if($fuserid!='-1')
				$xSql.=' AND f.userid='.$fuserid;
			
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 3: 
			$id = getArray('MYFollow','result');
			
			$ArTab=getCamptitTipo($id, "Tabla",'defa');	
			$ArCamp=getCamptitTipo($id, "Campos",'defa');
			$ArTit=getCamptitTipo($id, "Titulos",'defa');
			$ArSize=getCamptitTipo($id, "r_size",'defa');
			
			for($i=0; $i< count($ArCamp); $i++){
				$pdf->AddCol($ArCamp[$i],$ArSize[$i],$ArTit[$i],'C');
			}
			$pdf->AddCol('offerpercent',12,'Offer %','C');
			$pdf->AddCol('offer',15,'Offer','C');
			$pdf->AddCol('coffer',15,'C. Offer','C');
			$pdf->AddCol('ndate',15,'Next Date','C');
			$pdf->AddCol('ntask',15,'Next Task','C');
			$pdf->AddCol('lasthistorydate',10,'LU','C');
			$pdf->AddCol('contract',10,'C','C');
			$pdf->AddCol('pof',10,'P','C');
			$pdf->AddCol('emd',10,'E','C');
			$pdf->AddCol('rademdums',10,'A','C');
			
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
			$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
			$ndateb 	= isset($_POST['ndateb']) 	? $_POST['ndateb'] 		: '';
			$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
			$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
			$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
			$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
			$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
			$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
			$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
			
			$xSql='SELECT IF(f.status="A","Active",IF(f.status="NA","Non-Active",IF(f.status="NF","Not For Sale","Sold"))) as status,f.mlnumber, f.address, f.unit, f.zip, f.agent,
			f.offer, f.coffer, f.ndate, IF(f.ntask=0,"",IF(f.ntask=1,"Send SMS",IF(f.ntask=2,"Receive SMS",IF(f.ntask=3,"Send Fax",IF(f.ntask=4,"Receive Fax",IF(f.ntask=5,"Send Email",IF(f.ntask=6,"Receive Email",IF(f.ntask=7,"Send Document",IF(f.ntask=8,"Receive Document",IF(f.ntask=9,"Make Call",IF(f.ntask=10,"Receive Call",IF(f.ntask=11,"Send Regular Mail",IF(f.ntask=12,"Receive Regular Mail",IF(f.ntask=13,"Send Other","Receive Other")))))))))))))) as ntask, IF(f.contract=1,"N","Y") as contract, IF(f.pof=1,"N","Y") as pof, IF(f.emd=1,"N","Y") as emd, IF(f.realtorsadem=1,"N","Y") as rademdums, n.history, n.msj,
			DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ROUND(((f.offer/f.lprice)*100),2) as offerpercent 
			FROM xima.followup f
			LEFT JOIN xima.follow_notification n ON (f.parcelid=n.parcelid AND f.userid=n.userid)
			WHERE f.userid ='.$_POST['userid'].' AND n.followid='.$_COOKIE['datos_usr']['USERID']; 
			
			//filters
			if(trim($address)!='')
				$xSql.=' AND f.address LIKE \'%'.trim($address).'%\'';
				
			if(trim($mlnumber)!='')
				$xSql.=' AND f.mlnumber=\''.trim($mlnumber).'\'';
				
			if(trim($agent)!='')
				$xSql.=' AND f.agent LIKE \'%'.trim($agent).'%\'';
			
			if($status!='ALL')
				$xSql.=' AND f.status=\''.$status.'\'';
				
			if(trim($ndateb)!='' && trim($ndate)!='')
				$xSql.=' AND f.ndate BETWEEM \''.$ndate.'\' AND \''.$ndateb.'\'';
			elseif(trim($ndate)!='')
				$xSql.=' AND f.ndate=\''.$ndate.'\'';
				
			if(trim($ntask)!='-2'){
				if(trim($ntask)!='-1')
					$xSql.=' AND ntask='.$ntask;
				else
					$xSql.=' AND ntask<>0';
			}
				
			if($contract!='-1')
				$xSql.=' AND f.contract='.$contract;
			
			if($ademdums!='-1')
				$xSql.=' AND f.realtorsadem='.$ademdums;
			
			if($pof!='-1')
				$xSql.=' AND f.pof='.$pof;
			
			if($emd!='-1')
				$xSql.=' AND f.emd='.$emd;
				
			if($msj!='-1')
				$xSql.=' AND n.msj='.$msj;
				
			if($history!='-1')
				$xSql.=' AND n.history='.$history;
			
			
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		break;
		
		case 4: 
			$pdf->AddCol('odate',30,'Date','C'); 
			$pdf->AddCol('offer',15,'Offer','C'); 
			$pdf->AddCol('coffer',15,'C. Offer','C'); 
			$pdf->AddCol('task',30,'Task','C');
			$pdf->AddCol('contract',10,'C','C'); 
			$pdf->AddCol('pof',10,'P','C'); 
			$pdf->AddCol('emd',10,'E','C'); 
			$pdf->AddCol('realtorsadem',10,'A','C'); 
			 
			
			$xSql='SELECT odate,offer,coffer, IF(task=0,"",IF(task=1,"Send SMS",IF(task=2,"Receive SMS",IF(task=3,"Send Fax",IF(task=4,"Receive Fax",IF(task=5,"Send Email",IF(task=6,"Receive Email",IF(task=7,"Send Document",IF(task=8,"Receive Document",IF(task=9,"Make Call",IF(task=10,"Receive Call",IF(task=11,"Send Regular Mail",IF(task=12,"Receive Regular Mail",IF(task=13,"Send Other","Receive Other")))))))))))))) as task, IF(contract=1,"N","Y") as contract, IF(pof=1,"N","Y") as pof, IF(emd=1,"N","Y") as emd, IF(realtorsadem=1,"N","Y") as realtorsadem FROM xima.followup_history WHERE parcelid="'.$_POST['parcelid'].'" AND userid='.$_POST['userid'];
		
			//orders
			if(isset($_POST['sort'])) $xSql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir']; 
		break;
		
		case 5: 
			$pdf->AddCol('odate',30,'Date','C'); 
			$pdf->AddCol('task',30,'Task','C');
			
			$xSql='SELECT odate, IF(task=0,"",IF(task=1,"Send SMS",IF(task=2,"Receive SMS",IF(task=3,"Send Fax",IF(task=4,"Receive Fax",IF(task=5,"Send Email",IF(task=6,"Receive Email",IF(task=7,"Send Document",IF(task=8,"Receive Document",IF(task=9,"Make Call",IF(task=10,"Receive Call",IF(task=11,"Send Regular Mail",IF(task=12,"Receive Regular Mail",IF(task=13,"Send Other","Receive Other")))))))))))))) as task FROM xima.followup_schedule WHERE parcelid="'.$_POST['parcelid'].'" AND userid='.$_POST['userid'].' ORDER BY odate';
		break;
		
		case 6: 
			$pdf->AddCol('moment',30,'Date','C'); 
			$pdf->AddCol('name',30,'Name','C');
			$pdf->AddCol('message',30,'Message','C'); 

			$xSql="SELECT m.moment,concat(x.name,' ',x.surname) as name, m.message 
			FROM xima.follow_messages m
			INNER JOIN xima.ximausrs x ON (m.msgid=x.userid)
			WHERE m.userid =".$_POST['userid']." AND m.parcelid='".$_POST['parcelid']."'";
		break;
	}

	$prop=array('HeaderColor'=>array(135,206,250),
            'color1'=>array(255,255,255),
            'color2'=>array(255,255,255),
            'padding'=>2,
			'align'=>'C');
		
	$pdf->Table($xSql,$prop,$rest_total);		

	$file='FPDF/generated/'.strtotime("now").'.pdf';
	$pdf->Output('C:/inetpub/wwwroot/'.$file, 'F');
	echo "{success:true, pdf:'$file'}";
?>