<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	if($_COOKIE['datos_usr']['USERID']!=$_POST['userid']){
		$query="UPDATE xima.follow_notification SET msj=0 WHERE parcelid='".$_POST['parcelid']."' AND userid=".$_POST['userid']." AND followid=".$_COOKIE['datos_usr']['USERID'];
		mysql_query($query) or die($query.mysql_error());
	}else{
		$query="UPDATE xima.followup SET msj=0 WHERE parcelid='".$_POST['parcelid']."' AND userid=".$_POST['userid'];
		mysql_query($query) or die($query.mysql_error());
	}
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>
<style> 
.main {
  padding: 8px 15px;
  border-color: #E9E9E9;
  border-style: solid;
  border-width: 0px;
  border-bottom-width:1px;
  font-size: 15px;
}

.clearfix {
  display: block;
}

.UIImageBlock_SMALL_Image {
  margin-right: 8px;
}

.UIImageBlock_Image {
  float: left;
  margin-right: 10px;
}

a {
  color: #3B5998;
  cursor: pointer;
  text-decoration: none;
  font-size: 16px;
  font-weight:bold;
}

.UIImageBlock_Image .img {
  display: block;
}

.uiProfilePhotoLarge {
  height: 70px;
}

img {
  border: 0 none;
}

.UIImageBlock_Content {
  display: table-cell;
  vertical-align: top;
  width: 1000px;
}

.rfloat {
  float: right;
}

.timestamp {
  color: #AAAAAA;
  cursor: default;
  display: inline-block;
  vertical-align: top;
}

abbr {
  border-bottom: medium none;
}
</style>
<div id="report_content_messages" style="text-align:center !important;">
	<br clear="all">
	<h1 align="center" >FOLLOW MESSAGES</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center><div id="follow_messages_grid" style="width:850px; padding-top:20px;"></div></center>
	<br>
</div>

<script>
	var followstore_messages = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followmessages.php',
		fields: [ 
		   {name: 'moment', type: 'date', dateFormat: 'Y-m-d h:i:s'},
		   {name: 'moment2', type: 'date', dateFormat: 'Y-m-d h:i:s'},
		   {name: 'name'},
		   {name: 'message'},
		   {name: 'img'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'pid': "<?php echo $_POST['parcelid'];?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true
    });
	
	var templateView_messages = new Ext.XTemplate(
		'<tpl for=".">',
			'<div class="clearfix main">',
				/*'<div class="UIImageBlock_Image UIImageBlock_SMALL_Image">',
					'<img alt="{name}" src="{img}" class="uiProfilePhoto uiProfilePhotoLarge img">',
				'</div>',*/
				
				'<div align="left" class="UIImageBlock_Content UIImageBlock_SMALL_Content">',
					'<div>',
						'<div class="rfloat">',
							'<abbr class="timestamp" title="{moment2}">{moment}</abbr>',
						'</div>',
						'<strong>',
							'<a href="javascript:void(0);">{name}</a>',
						'</strong>',
					'</div>',
					
					'<div>',
						'<p>{message}</p>',
					'</div>',
				'</div>',
			'</div>',
		'</tpl>'
	);

	
	var followgrid_messages = new Ext.DataView({
		renderTo: 'follow_messages_grid',
		store: followstore_messages,
		tpl: templateView_messages,
		multiSelect: false,      
		width: 800,
		height: tabs.getHeight()-200,
		itemSelector:'div.main',
		emptyText: '<strong style="font-size:20px;">No messages to display</strong>',
		prepareData: function(data){
			data.moment = data.moment.format("m/d/Y g:i a");
			data.moment2 = data.moment2.format("l, F j, Y, g:i a");
			return data;
		}
	});
	
	followstore_messages.load();

if(document.getElementById('tabsFollow')){
	if(document.getElementById('report_content_messages').offsetHeight > tabsFollow.getHeight()){
		tabsFollow.setHeight(document.getElementById('report_content_messages').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content_messages').offsetHeight+150);
	}
}
</script>