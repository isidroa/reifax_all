<?php
	include("../properties_conexion.php");
	conectar();
	
	include ("../properties_getgridcamptit.php");	
	
	$ancho=850;
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYDoc','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
?>
<div align="center" id="todo_mydocument_panel">
<br clear="all" />
<div id="mydocument_data_div" align="center" style=" margin:auto; width:<?php echo $ancho.'px;';?>">
    <div id="mydocument_properties"></div>
</div>
</div>
<script>
	function urldoc(value, metaData, record, rowIndex){
		
		var urlamazon = record.get('urlamazon');
		var url = urlamazon ? urlamazon : value;
		
		var testA=/^https:\/\/s3.+php$/;
		if(testA.test(url)){
			url= '/includes/downs3.php?file='+url;
		}
		url=url.replace('reifax.com/docs/','reifax.com/');
		return '<a href="'+url+'" target="_blank"><img src="http://www.reifax.com/img/doc.png"/></a>';
	}

	var storemydocument = new Ext.data.JsonStore({
        url: 'mysetting_tabs/mydocument_tabs/getdocuments.php',
		fields: [
           <?php 
		   		echo "'ind','pid','urlamazon'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'sdate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	var limitmydocuments = 50;
	
	var smmydocument = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	var gridmydocument = new Ext.grid.GridPanel({
		renderTo: 'mydocument_properties',
		cls: 'grid_comparables',
		//width: <?php echo ($ancho-5);?>, 
		height: <?php echo ((50)*24);?>,
		store: storemydocument,
		columns: [
				  smmydocument,
			<?php 
		   		echo "{id: 'ind' , header: 'Ind.', width: 30, sortable: true, tooltip: 'Index Property.', dataIndex: 'ind'},{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='url')
						echo ",{header: '".$val->title."', renderer: urldoc, width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					elseif($val->name!='urlamazon')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", renderer  :function (val){
							var testA=/.+php$/;
							if(testA.test(val)){
								return '/includes/downs3.php?file='+val;
							}
							else{
								return val;
							}

						}, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
				}
		   ?>	  
			
		],
		sm: smmydocument,
		tbar: new Ext.PagingToolbar({
			id: 'pagingmydocuments',
            pageSize: limitmydocuments,
            store: storemydocument,
            displayInfo: true,
			displayMsg: 'Total: {2} Documents.',
			emptyMsg: "No documents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 documents mail per page.',
				text: 50,
				handler: function(){
					limitmydocuments=50;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 documents mail per page.',
				text: 80,
				handler: function(){
					limitmydocuments=80;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 documents mail per page.',
				text: 100,
				handler: function(){
					limitmydocuments=100;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
	});
	
	//storemydocument.loadData(arrayDatamydocument);
	storemydocument.load({params:{start:0, limit:limitmydocuments}});
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_mydocument_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_mydocument_panel').offsetHeight+100);
			//tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>