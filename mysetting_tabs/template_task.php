<?php
	include "../../properties_conexion.php";
	conectar();
	
	$userid=$_COOKIE['datos_usr']['USERID'];  // Agregado por Luis R Castro 19/06/2015
	$query  = "SELECT * FROM xima.templates_schedule
				WHERE userid = $userid order by `default` desc";
	$rs     = mysql_query($query) or die($query.mysql_error());
	$checkedTemplateid=0;
	$checkedTemplate = 'New Template';
	if (isset($_POST['idtem'])){
		$idtem = $_POST['idtem'];
		}else{
		$idtem=0;
		}
	while ($rowcr = mysql_fetch_array($rs)) {
		$checkedTemplate = $rowcr['name'];
		$checkedTemplateid = $rowcr['iditem'];
		
		
		$comboTemplates .= "['{$rowcr['iditem']}','{$rowcr['name']}'],";
	}
	
	$checked='';
	
?>
<style type="text/css">
.add-variable {
	background-image: url("http://www.reifax.com/img/add.gif") !important;
}
</style>
<div align="left" id="todo_mytemplate_task_panel" style="background-color:#FFF;border-color:#FFF">
	<div id="mytemplate_task_data_div" align="center" style=" background-color:#FFF; margin:auto; width:980px;">
    	<div id="mytemplate_task_filters"></div><br />
        <div id="mytemplate_task_properties" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<script type="text/javascript" src="mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>
<script>
////////////////////////////////////////////////////
var selected_dataTasktemplatetask 	= new Array();
	var AllCheckTasktemplatetask			= false;
	var selected_dataTasktemplatetime 	= new Array();
	var AllCheckTasktemplatetime			= false;
	
var storeTasktemplatestask = new Ext.data.JsonStore({
        url: 'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
		fields: [
           <?php 
		   		echo "'iditemtask','iditem'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'task', type: 'int'},
			   {name: 'detail'},{name: 'typeExec'},{name: 'subject'},{name: 'body'},{name: 'ord'}";
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'idtem': <?php echo $idtem;?>,
			'mode':'obtener'
			
		},
		remoteSort: true,
		sortInfo: {
			field: 'ord',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		}
		
    });

///////////////////////////////////////////////////
	var seleccionarTemplate= '<?php echo $checkedTemplate; ?>';
	var seleccionarTemplateId= <?php echo $checkedTemplateid; ?>;
	
	var storeTasktemplates = new Ext.data.SimpleStore({
		fields : ['idtem','name'],
		data   : [
			<?php echo $comboTemplates;?>
			['0','New Template']
		]
	});
	var storeTasktemplatestime = new Ext.data.JsonStore({
        url: 'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
		fields: [
           <?php 
		   		echo "'idtem','idtemtask','idtemtasktime','day','hour','min','sec'";
		   		
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'idtem' : <?php echo $idtem;?>,
			'mode':'obtener-time'
		},
		remoteSort: true,
		
		listeners: {
			'beforeload': function(store,obj){
				if(obj.params.pids) storeTasktemplatestime.load({params:{start:0,pids:obj.params.pids}});
				else storeTasktemplatestime.load();
				
				AllCheckTasktemplatetime=false;
				selected_dataTasktemplatetime=new Array();
				smTasktemplatetime.deselectRange(0,limitTasktemplatetime);
				
			},
			'load' : function (store,data,obj){
				if (AllCheckTasktemplatetime){
					Ext.get(gridTasktemplatetime.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckTasktemplatetime=true;
					gridTasktemplatetime.getSelectionModel().selectAll();
					selected_dataTasktemplatetime=new Array();
				}else{
					AllCheckTasktemplatetime=false;
					Ext.get(gridTasktemplatetime.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_dataTasktemplatetime.length > 0){
						for(val in selected_dataTasktemplatetime){
							var ind = gridTasktemplatetime.getStore().find('pid',selected_dataTasktemplatetime[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridTasktemplatetime.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var toolbartemplatetask=new Ext.Toolbar({
		renderTo: 'mytemplate_task_filters',
		items: [
			new Ext.Button({
				id: 'new_template_task',
				tooltip: 'New Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'small',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					newTemplate();
				}
			}),new Ext.Button({
				id: 'delete_template_task',
				tooltip: 'Delete Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'small',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
						method  : 'POST',
						params    : {
							type: 'task',
							modo: 'eliminar',
							id : Ext.getCmp('template_id').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Template Deleted');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_task.php',
								cache : false
							});
							
						}
					});	
				}
			}),new Ext.Button({
				id: 'default_template_task',
				tooltip: 'Set Default',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'small',
				icon: 'http://www.reifax.com/img/toolbar/arrow_green.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
						method  : 'POST',
						params    : {
							type: 'task',
							modo: 'setdefault',
							id : Ext.getCmp('template_id').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Operation Completed');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_task.php',
								cache : false
							});
							
						}
					});	
				}
			}),{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 260,
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						mode: 'loadtemplates',
						userid: <?php echo $userid; ?>,
						
					},
					fields:[
						{name:'iditem', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
					listeners     : {
						'load'  : function(store, records) {
							Ext.getCmp('combotemplates_task').setValue(0); 
						}
					}
				}),
				displayField  : 'name',
				valueField    : 'iditem',
				name          : 'ftemplate_task',
				id			  : 'combotemplates_task',
				value         : seleccionarTemplate,
				hiddenName    : 'ftemplate1_task',
				hiddenValue   : seleccionarTemplate,
				allowBlank    : false,
				editable	  : false,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(record.get('iditem')!=0){
							getTemplate(record.get('iditem'));
						}else{
							newTemplate();  
						}
					}
				}
			}
		]
	});
	function newTemplate(){
		Ext.getCmp('delete_template_task').setVisible(false);
		Ext.getCmp('default_template_task').setVisible(false);
		Ext.getCmp('new_template_task').setVisible(false);
		
		Ext.getCmp('combotemplates_task').setValue(0);
		Ext.getCmp('templatename_task').setValue('Template name');
		Ext.getCmp('templatename_task').setReadOnly(false);
		Ext.getCmp('template_id_task').setValue(0);
		Ext.getCmp('defaulttemplate_task').setValue(0);
		Ext.getCmp('buttontemplate_task').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Template</b></span>');
	}
	
	function getTemplate(id){
		loading_win.show();
		Ext.getCmp('delete_template_task').setVisible(true);
		Ext.getCmp('default_template_task').setVisible(true);
		Ext.getCmp('new_template_task').setVisible(true);
		Ext.getCmp('templatename_task').setReadOnly(true);
		Ext.getCmp('buttontemplate_task').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>');
		Ext.Ajax.request({
			url     : 'mysetting_tabs/mycontracts_tabs/get_templates_task.php',
			method  : 'POST',
			params    : {
				type: 'task',
				modo: 'obtener',
				id : id,
				'userid': <?php echo $userid;?>
			},
			waitMsg : 'Getting Info',
			success : function(r) {
				loading_win.hide();
				var resp   = Ext.decode(r.responseText);
	
				if (resp.data!=null) {
					Ext.getCmp('templatename_task').setValue(resp.data.name);
					Ext.getCmp('templatesubject_task').setValue(resp.data.subject);
					Ext.getCmp('templateeditor_task').setValue(resp.data.body);
					Ext.getCmp('template_id_task').setValue(resp.data.iditem);	
					Ext.getCmp('defaulttemplate_task').setValue(resp.data.default);
				} 
			}
		});
	}
	
	
	var variablesStore=new Ext.data.ArrayStore({
		fields    : ['id', 'texto'],
		data      : [
			['','Select'],
			<?php echo $combo; ?>
		]
	});
	
	var smmyTasktemplatetask = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataTasktemplatetask.indexOf(record.get('pid'))==-1)
					selected_dataTasktemplatetask.push(record.get('pid'));
				
				if(Ext.fly(gridTasktemplatetask.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckTasktemplatetask=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataTasktemplatetask = selected_datatemplatetask.remove(record.get('pid'));
				AllCheckTasktemplatetask=false;
				Ext.get(gridTasktemplatetask.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var smmyTasktemplatetime = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataTasktemplatetime.indexOf(record.get('pid'))==-1)
					selected_dataTasktemplatetime.push(record.get('pid'));
				
				if(Ext.fly(gridTasktemplatetime.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckTasktemplatetime=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataTasktemplatetime = selected_datatemplatetasktime.remove(record.get('pid'));
				AllCheckTasktemplatetime=false;
				Ext.get(gridTasktemplatetime.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	////////////////
	var gridTasktemplatestime new Ext.grid.EditorGridPanel({
	       id: 'gridTasktemplatestime',
        name: 'gridTasktemplatestime',
        height: 500,
        width: 400,
        title:'',
        store: storeTasktemplatestime,
		stripeRows: true,
		forceFit: true, 
		sm: smmyTasktemplatetime, 
		clicksToEdit: 1,
        columns: [
		smmyTasktemplatetime,
         <?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'idtemtasktime'}";
				echo ",{header: 'Days', width: 25, sortable: true, tooltip: 'Days.', dataIndex: 'day'}";
				echo ",{header: 'Hours', width: 25, sortable: true, tooltip: 'Hours.', dataIndex: 'hour'}";
				echo ",{header: 'Minutes', width: 25, sortable: true, tooltip: 'Minutes.', dataIndex: 'min'}";
				echo ",{header: 'Seconds', width: 25, sortable: true, tooltip: 'Seconds.', dataIndex: 'sec'}";
				?>
       ]
    });
	var gridTasktemplatestask new Ext.grid.EditorGridPanel({
        id: 'gridTasktemplatestask',
        name: 'gridTasktemplatestask',
       height: 500,
        width: 400,
        title:'',
        store: storeTasktemplatestask,
		stripeRows: true,
		forceFit: true, 
		sm: smmyTasktemplatetask, 
		clicksToEdit: 1,
        columns: [
		smmyTasktemplatetask,
         <?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'idtemtask'}";
				echo ",{header: 'Task', width: 25, sortable: true, tooltip: 'Task.', dataIndex: 'task'}";
				echo ",{header: 'Detail', width: 75, sortable: true, tooltip: 'Task Details', dataIndex: 'detail'}";
				echo ",{header: 'Type Exec', width: 25, sortable: true, tooltip: 'Type Exec.', dataIndex: 'typeExec'}";
				echo ",{header: 'Ord', width: 25, sortable: true, tooltip: 'Order.', dataIndex: 'ord'}";
				?>
       ]
    });
	//////////////////
	var template_task_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		frame      : true,
		method     : 'POST',
		renderTo: 'mytemplate_task_properties', 
		labelWidth: 60,
		items      : [
			{
				xtype		  : 'textfield',
				fieldLabel	  : 'Name',
				name		  : 'name',
				id		  : 'templatename_task',
				readOnly  : true,
				width: 300,
				allowBlank    : false,
				value: 'Template name'
			},{
				xtype         : 'combo',
				fieldLabel	  : 'Default',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 60,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
						['0','No'],
						['1','Yes']
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				name          : 'defaulttemplate_task',
				hiddenName    : 'defaulttemplate1_task',
				id			  : 'defaulttemplate_task',	
				allowBlank    : false,
				readOnly  : true,
				editable	  : false
			},
			
	////////////////////////////////////////////////
	{
				xtype      : 'viewport',
				 layout:"column",
  				items:[gridTasktemplatestask,gridTasktemplatestime]
	},
	///////////////////////////////////////////////
	
	{
				xtype      : 'box',
				html       : ' <div style="color: #4B8A08; margin:10px;margin-left:90px; font-size: 14px;"><b>Click on the letter box to set the cursor, then click on subject to select the variable and the plus sign button to add it to the word box.</b></div> '
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : <?php echo $userid; ?>
			},{
				xtype         : 'hidden',
				name          : 'template_id_task',
				id          : 'template_id_task'
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>',
			id		: 'buttontemplate_task',
			handler : function(){
				if (template_task_form.getForm().isValid()) {
					template_task_form.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savetemplatetask.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							if (resp.mensaje == 'Template saved!') {
								updater.update({
									url   : 'mysetting_tabs/mycontracts_tabs/template_task.php',
									cache : false
								});
							}
						}
					});
				}
			}
		}]
	});
	
	
	
	
	newTemplate();
		storeTasktemplatestask.load({params:{start:0, limit:50}});
		storeTasktemplatestime.load({params:{start:0, limit:50}});

</script>