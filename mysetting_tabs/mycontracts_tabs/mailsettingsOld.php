<?php
/**
 * mailsettings.php
 *
 * Just a simple form to get the STMP information from the users.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 11.07.2011
 */

include "../../properties_conexion.php";
conectar();

$userid = $_COOKIE['datos_usr']["USERID"];
$signs  = null;

$query  = "SELECT * FROM xima.contracts_mailsettings
				WHERE userid = $userid";
$rs     = mysql_query($query);
$row    = mysql_fetch_array($rs);
$server_delivery=$row['server'];

if($server_delivery!=''){
	if($server_delivery=='ssl://smtp.gmail.com'){
		$type_email='1';
	}else if($server_delivery=='smtp.mail.yahoo.com'){
		$type_email='3';
	}else if($server_delivery=='smtp.live.com' or $server_delivery=='smtp-mail.outlook.com'){
		$type_email='2';
	}else if($server_delivery=='smtp.aol.com '){
		$type_email='4';
	}else{
		$type_email='0';
	}
}else{
	$type_email='1';
}
$server_incoming=$row['imap_server'];
if($server_incoming!=''){
	if($server_incoming=='imap.gmail.com'){
		$type_imap='1';
	}else if($server_incoming=='smtp.mail.yahoo.com'){
		$type_imap='3';
	}else if($server_incoming=='pop3.live.com' or $server_incoming=='imap-mail.outlook.com'){
		$type_imap='2';
	}else if($server_incoming=='imap.aol.com'){
		$type_imap='4';
	}else{
		$type_imap='0';
	}
}else{
	$type_email='1';
}

$tabId = isset($_POST['tabId']) ? $_POST['tabId'] : "";
?>

<div id="div_imapsett<?=$tabId?>" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_smtpsett<?=$tabId?>" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">

var imapStore = new Ext.data.SimpleStore({
	fields : ['id','name'],
	data   : [
		['1','IMAP'],
		['2','POP3'],
		['3','IMAP/SSL'],
		['4','POP3/SSL']
	]
});
var incomingStore = new Ext.data.SimpleStore({
	fields : ['id','name','server','port','imap'],
	data   : [
		['1','Gmail','imap.gmail.com','993','3'],
		['2','Hotmail','pop3.live.com','995','4'],
		//['2','Hotmail / Outlook','imap-mail.outlook.com','993','3'],
		['4','Aol','imap.aol.com','143','1'],
		['0','Other','','','2']
	]
});
var smtpsettForm = new Ext.FormPanel({
	title      : 'Email Reader Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Reader in the My Follow Up / My Emails.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'imap_service',
			id            : 'imap_service<?=$tabId?>',
			hiddenName    : 'himap_service',
			fieldLabel    : 'Mail Service',
			store         : incomingStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_imap?>',
			listeners     : {
				'select'  : selectImap<?=$tabId?>
			}
		},{
			xtype      : 'box',
			id		   : 'imap_warning_pop3<?=$tabId?>',
			hidden	   : <?=$type_imap!='1' ? 'false' : 'true';?>,
			html       : "<div class=\"alert alert-error\" style=\"margin:10px;margin-left:110px;\"><span style=\"font-weight: bold !important\">Using POP3 technology could cause you to loose the emails in your inbox mail.</span><br><p>Before continuing, please check your email configuration and allow, if available, the option to save a copy in your email inbox.</p><p>If that option is not available do not use your email with this configuration.</p></div><div class=\"alert alert-success\"  style=\"margin:10px;margin-left:110px;\"><p><span style=\"font-weight: bold !important\">Our advice is that you create a GMAIL account</span> that works with IMAP and does not have this problem</p></div>"
		},{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_imap=='0' ? 'false' : 'true';?>,
			id: 'imap_delivery<?=$tabId?>',	
            items: [{
				xtype         : 'combo',
				width		  : 100,
				name          : 'imap_type',
				id            : 'imap_type<?=$tabId?>',
				hiddenName    : 'server_type',
				fieldLabel    : 'Type',
				store         : imapStore,
				triggerAction : 'all',
				mode          : 'local',
				editable      : false,
				selectOnFocus : true,
				displayField  : 'name',
				valueField    : 'id',
				value		  : '<?=$row['imap_type']>0 ? $row['imap_type'] : 2;?>',
				listeners     : {
					'select'  : function() {
						if (Ext.getCmp('imap_type<?=$tabId?>').getValue() == '1')
							Ext.getCmp('imap_port<?=$tabId?>').setValue(143);
							
						if (Ext.getCmp('imap_type<?=$tabId?>').getValue() == '2')
							Ext.getCmp('imap_port<?=$tabId?>').setValue(110);
						
							
						if (Ext.getCmp('imap_type<?=$tabId?>').getValue() == '3')
							Ext.getCmp('imap_port<?=$tabId?>').setValue(993);
						
					}
				}
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">If you want to use gmail please select IMAP/SSL</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'imap_server',
				id         : 'imap_server<?=$tabId?>',
				width      : '200',
				value      : '<?=addslashes($row['imap_server'])?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with imap.gmail.com )</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				id         : 'imap_port<?=$tabId?>',
				name       : 'imap_port',
				width      : '200',
				value      : '<?=strlen($row['imap_port'])>0 ? $row['imap_port'] : '143'?>',
				allowBlank : false
			}]
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'imap_username',
			width      : '200',
			value      : '<?=$row['imap_username']?>',
			allowBlank : false
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'imap_password',
			value      : '<?=$row['imap_password']?>',
			width      : '200',
			allowBlank : false
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Incomming Settings</b></span>',
		handler : function(){
			if (smtpsettForm.getForm().isValid()) {
				smtpsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mailsettings.php',
							cache : false
						});
					}
				});
			}
		}
	},{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Test Connection</b></span>',
		handler : function(){
			if (smtpsettForm.getForm().isValid()) {
				smtpsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/testmailsett.php',
					waitMsg : 'Testing...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	},{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('MailSetting');
		}	
	}]
});
var serviceStore = new Ext.data.SimpleStore({
	fields : ['id','name','server','port'],
	data   : [
		['1','Gmail','ssl://smtp.gmail.com','465'],
		['2','Hotmail','smtp.live.com','25'],
		//['2','Hotmail / Outlook','ssl://smtp-mail.outlook.com','587'],
		['3','Yahoo','smtp.mail.yahoo.com','587'],
		['4','Aol','smtp.aol.com','587'],
		['0','Other','','']
	]
});
function selectImap<?=$tabId?>(combo,record,index){
	var service=Ext.getCmp('imap_service<?=$tabId?>').getValue();
	if(service==0){
		Ext.getCmp('imap_delivery<?=$tabId?>').setVisible(true);
	}else{
		Ext.getCmp('imap_delivery<?=$tabId?>').setVisible(false);
		
	}
	
	if(record.get('id')=='1')
		Ext.getCmp('imap_warning_pop3<?=$tabId?>').hide();
	else
		Ext.getCmp('imap_warning_pop3<?=$tabId?>').show();
	
	Ext.getCmp('imap_type<?=$tabId?>').setValue(record.get('imap'));
	Ext.getCmp('imap_server<?=$tabId?>').setValue(record.get('server'));
	Ext.getCmp('imap_port<?=$tabId?>').setValue(record.get('port'));
}
function selectService<?=$tabId?>(combo,record,index){
	var service=Ext.getCmp('delivery_service<?=$tabId?>').getValue();
	if(service==0){
		Ext.getCmp('server_delivery<?=$tabId?>').setVisible(true);
		Ext.getCmp('port_delivery<?=$tabId?>').setVisible(true);
	}else{
		Ext.getCmp('server_delivery<?=$tabId?>').setVisible(false);
		Ext.getCmp('port_delivery<?=$tabId?>').setVisible(false);
		
	}
	
	Ext.getCmp('server_deliv<?=$tabId?>').setValue(record.get('server'));
	Ext.getCmp('port_deliv<?=$tabId?>').setValue(record.get('port'));
}
var imapsettForm = new Ext.FormPanel({
	title      : 'Email Delivery Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'delivery_service',
			id            : 'delivery_service<?=$tabId?>',
			hiddenName    : 'hdelivery_service',
			fieldLabel    : 'Mail Service',
			store         : serviceStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_email?>',
			listeners     : {
				'select'  : selectService<?=$tabId?>
			}
		},{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_email=='0' ? 'false' : 'true';?>,
			id: 'server_delivery<?=$tabId?>',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				id		   : 'server_deliv<?=$tabId?>',	
				width      : '200',
				value      : '<?=addslashes($row['server'])?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with ssl://smtp.gmail.com)</div>'
			}]
        },{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_email=='0' ? 'false' : 'true';?>,
			id: 'port_delivery<?=$tabId?>',
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				id		   : 'port_deliv<?=$tabId?>',	
				width      : '200',
				value      : '<?=strlen($row['port'])>0 ? $row['port'] : '25'?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with ssl://smtp.gmail.com)</div>'
			}]
        },{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'username',
			id		   : 'user_delivery<?=$tabId?>',
			width      : '200',
			value      : '<?=$row['username']?>',
			allowBlank : false
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'password',
			id		   : 'password_delivery<?=$tabId?>',
			value      : '<?=$row['password']?>',
			width      : '200',
			allowBlank : false
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
		handler : function(){
			if (imapsettForm.getForm().isValid()) {
				imapsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mailsettings.php',
							cache : false
						});
					}
				});
			}
		}
	},{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Test Connection</b></span>',
		handler : function(){
			if (imapsettForm.getForm().isValid()) { 
				imapsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/testmailsett.php',
					waitMsg : 'Testing...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	},{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('MailSetting');
		}	
	}]
});

imapsettForm.render('div_imapsett<?=$tabId?>');
smtpsettForm.render('div_smtpsett<?=$tabId?>');
if(Ext.getCmp('server_deliv<?=$tabId?>').getValue()==""){
	var recordV = Ext.getCmp('delivery_service<?=$tabId?>').findRecord(Ext.getCmp('delivery_service<?=$tabId?>').getValue());
	selectService<?=$tabId?>(Ext.getCmp('delivery_service<?=$tabId?>'),recordV,1);
}
</script>
