<?php
	include "../../properties_conexion.php"; 
	conectar();
	
	$userid= $_POST['userid'];
	$type= $_POST['type'];
	$modo= $_POST['modo'];
	
	if($type=='email'){
		$type_template=1;
	}else if($type=='docs'){
		$type_template=2;
	}else if($type=='sms'){
		$type_template=3;
	}else if($type=='fax'){
		$type_template=4;
	}
		if($modo=='listar'){
			$query  = "SELECT id, name FROM xima.templates WHERE userid = $userid and template_type=1";
			$result=mysql_query($query) or die($query.mysql_error());
			
			$data = array();
			
			$row['id']=0;
			$row['name']='New Template';
			$data [] = $row;
			while($row=mysql_fetch_array($result)){
				$data[]=$row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}else if($modo=='obtener'){
			$idtemplate=$_POST['id'];
			
			if($idtemplate != 0){
				$query  = "SELECT id, userid, template_type, subject, body, name, `default` as defaulttemplate
							FROM xima.templates WHERE userid = $userid and id=$idtemplate";
				$result=mysql_query($query) or die($query.mysql_error());
				
				$data=Array();
				while($row=mysql_fetch_assoc($result)){
					$query     = "SELECT idtc, campos, titulos, `desc`, tabla, type FROM xima.camptit WHERE variable=1";  
					$rscr      = mysql_query($query) or die($query.mysql_error());
					$array_campos = Array();
					while($r=mysql_fetch_array($rscr)){
						$row['subject'] = str_replace('{%'.$r['idtc'].'%}','{%'.$r['desc'].'%}',$row['subject']);
						$row['body'] = str_replace('{%'.$r['idtc'].'%}','{%'.$r['desc'].'%}',$row['body']);
					}
					$data=$row;
				}
			}else{
				if($userid==933 || $userid==2846 || $userid==2883){
					$subject  = "{%Property Address%}-Cash Offer, You Represent Us";
					
					$body   .= "Dear,\r\n\r\n";
					$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
					$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
					$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
					$body    .= "    \t* We close Cash. We are well funded.\r\n";
					$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
					$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
					$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
					$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
					$body   .="Thank you and have an amazing day!\r\n\r\n";
					$body    .= "Best Regards,\r\n";
					$body    .= "{%Contact Name%}.\r\n";
					$body    .= "Summit Home Buyers, LLC\r\n";
					$body    .= "{%Follow up Phone%}\r\n";
				}else{
					$subject  = "Offer for the MLS number $mln, Address {%Property Address%}";
					if($userid==1719 || $userid==1641){
						$texto="Dear {%Contact Name%}\r\n\r\n";
					}else{
						$texto="Dear Mr/Mrs {%Contact Name%}\r\n\r\n";
					}
					$body     = $texto;
					$body   .= "Please find attached a contract with our offer regarding the ";
					$body   .= "property with the address: {%Property Address%}\r\n";
					$body   .= "We wait for your prompt response\r\n\r\n";
					$body   .= "Regards {%User Name%}.\r\n";
					$body   .= "{%Follow up Phone%}\r\n";
					$body   .= "{%Follow up Email%}";
				}
				
				$data["id"]=0;
				$data["userid"]=$userid;
				$data["template_type"]=$type_template;
				$data["subject"]=$subject;
				$data["body"]=$body;
				$data["name"]='Standard '.($template_type==1 ? 'Email' : ($template_type==3 ? 'SMS' : 'Fax'));
				$data["defaulttemplate"]=0;
			}
						
			echo "{success:true, msg:'Success', data:".json_encode($data)."}"; 
		}else if($modo=='eliminar'){
			$template_id=$_POST['id'];
			
			$query='delete from xima.templates where id='.$template_id.' and userid='.$userid;
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}else if($modo=='setdefault'){
			$template_id=$_POST['id'];
			
			$query='update xima.templates set `default`=0 where template_type='.$type_template.'  and userid='.$userid;
			mysql_query($query) or die($query.mysql_error());
			
			$query='update xima.templates set `default`=1 where id='.$template_id.' and userid='.$userid;
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
	
?>
