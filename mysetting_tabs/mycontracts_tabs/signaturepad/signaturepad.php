<?php
require_once 'signature-to-image.php';

if ($_POST['output']!='') {
    $img = sigJsonToImage($_POST['output']);
    imagepng($img, './signature.png');
    imagedestroy($img);
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Signature</title>
        <script type="text/javascript" src="jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="jquery.signaturepad.css">
        <!--[if lt IE 9]><script src="flashcanvas.js"></script><![endif]-->
        <script type="text/javascript" src="jquery.signaturepad.min.js"></script>
        <script type="text/javascript" src="json2.min.js"></script>

    </head>
    <body>
    <form method="post" action="#" class="sigPad">

        <p class="drawItDesc">Draw your signature</p>

        <ul class="sigNav">

            <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
            <li class="clearButton"><a href="#clear">Clear</a></li>

        </ul>

        <div class="sig sigWrapper">
            <div class="typed"></div>
            <canvas class="pad" width="198" height="55"></canvas>
            <input type="hidden" name="output" class="output">
        </div>

        <button type="submit">I accept the terms of this agreement.</button>

    </form>

    <script type="text/javascript">
    $(document).ready( function(){
      $('.sigPad').signaturePad({drawOnly:true})
    })
    </script>

    </body>
</html>
