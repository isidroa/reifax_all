<?php
	include "../../properties_conexion.php";
	conectar();
	$userid=$_COOKIE['datos_usr']['USERID']; 
	$query  = "SELECT * FROM xima.templates
				WHERE userid = $userid and template_type=1 order by `default` desc";
	$rs     = mysql_query($query) or die($query.mysql_error());
	$checkedTemplateid=0;
	while ($rowcr = mysql_fetch_array($rs)) {
		$checkedTemplate = $rowcr['name'];
		$checkedTemplateid = $rowcr['id'];
		
		$comboTemplates .= "['{$rowcr['id']}','{$rowcr['name']}'],";
	}
	$checked='';
	
?>
<style type="text/css">
.add-variable {
	background-image: url("http://www.reifax.com/img/add.gif") !important;
}
</style>
<div align="left" id="todo_mytemplate_email_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="mytemplate_email_data_div" align="center" style=" background-color:#FFF; margin:auto; width:900px;">
    	<div id="mytemplate_email_filters"></div><br />
        <div id="mytemplate_email_properties" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<script>
	var seleccionarTemplate= '<?php echo $checkedTemplate; ?>';
	var seleccionarTemplateId= <?php echo $checkedTemplateid; ?>;
	
	var storetemplates = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			<?php echo $comboTemplates;?>
			['0','New Template']
		]
	});
	var toolbarmyfollowlist=new Ext.Toolbar({
		renderTo: 'mytemplate_email_filters',
		items: [
			new Ext.Button({
				id: 'new_template',
				tooltip: 'New Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					newTemplate();
				}
			}),new Ext.Button({
				id: 'delete_template',
				tooltip: 'Delete Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'email',
							modo: 'eliminar',
							id : Ext.getCmp('template_id').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Template Deleted');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_email.php',
								cache : false
							});
							
						}
					});	
				}
			}),new Ext.Button({
				id: 'default_template',
				tooltip: 'Set Default',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/arrow_green.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'email',
							modo: 'setdefault',
							id : Ext.getCmp('template_id').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Operation Completed');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_email.php',
								cache : false
							});
							
						}
					});	
				}
			}),{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 260,
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'loadtemplates',
						userid: <?php echo $userid; ?>,
						template_type: 1
					},
					fields:[
						{name:'id', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							Ext.getCmp('combotemplates').setValue(0); 
						}
					}
				}),
				displayField  : 'name',
				valueField    : 'id',
				name          : 'ftemplate',
				id			  : 'combotemplates',
				value         : seleccionarTemplate,
				hiddenName    : 'ftemplate1',
				hiddenValue   : seleccionarTemplate,
				allowBlank    : false,
				editable	  : false,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(record.get('id')!=0){
							getTemplate(record.get('id'));
						}else{
							newTemplate();  
						}
					}
				}
			},new Ext.Button({
				cls:'x-btn-text-icon',
				tooltip: 'View Help',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				tooltip: 'Click to view help',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('TemplateEmail');
				}
			})		
		]
	});
	function newTemplate(){
		Ext.getCmp('delete_template').setVisible(false);
		Ext.getCmp('default_template').setVisible(false);
		Ext.getCmp('new_template').setVisible(false);
		
		Ext.getCmp('combotemplates').setValue(0);
		Ext.getCmp('templatename').setValue('Template name');
		Ext.getCmp('templatename').setReadOnly(false);
		Ext.getCmp('templatesubject').setValue('');
		Ext.getCmp('templateeditor').setValue('');
		Ext.getCmp('template_id').setValue(0);
		Ext.getCmp('defaulttemplate').setValue(0);
		Ext.getCmp('buttontemplate').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Template</b></span>');
	}
	
	function getTemplate(id){
		loading_win.show();
		Ext.getCmp('delete_template').setVisible(true);
		Ext.getCmp('default_template').setVisible(true);
		Ext.getCmp('new_template').setVisible(true);
		Ext.getCmp('templatename').setReadOnly(true);
		Ext.getCmp('buttontemplate').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>');
		Ext.Ajax.request({
			url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
			method  : 'POST',
			params    : {
				type: 'email',
				modo: 'obtener',
				id : id,
				'userid': <?php echo $userid;?>
			},
			waitMsg : 'Getting Info',
			success : function(r) {
				loading_win.hide();
				var resp   = Ext.decode(r.responseText);
	
				if (resp.data!=null) {
					Ext.getCmp('templatename').setValue(resp.data.name);
					Ext.getCmp('templatesubject').setValue(resp.data.subject);
					Ext.getCmp('templateeditor').setValue(resp.data.body);
					Ext.getCmp('template_id').setValue(resp.data.id);	
					Ext.getCmp('defaulttemplate').setValue(resp.data.defaulttemplate);
				} 
			}
		});
	}
	
	
	var variablesStore=new Ext.data.ArrayStore({
		fields    : ['id', 'texto'],
		data      : [
			['','Select'],
			<?php echo $combo; ?>
		]
	});
	var variableInsertar='<?php echo $checked; ?>';
	var variableInsertarSub='<?php echo $checked; ?>';
	
	var template_email_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		frame      : true,
		method     : 'POST',
		renderTo: 'mytemplate_email_properties', 
		labelWidth: 60,
		items      : [
			{
				xtype		  : 'textfield',
				fieldLabel	  : 'Name',
				name		  : 'name',
				id		  : 'templatename',
				readOnly  : true,
				width: 300,
				allowBlank    : false,
				value: 'Template name'
			},{
				xtype         : 'combo',
				fieldLabel	  : 'Default',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 60,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
						['0','No'],
						['1','Yes']
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				name          : 'defaulttemplate',
				hiddenName    : 'defaulttemplate1',
				id			  : 'defaulttemplate',	
				allowBlank    : false,
				readOnly  : true,
				editable	  : false
			},{
				xtype:'htmleditor',
				id: 'templatesubject',
				fieldLabel : 'Subject',
				autoScroll: true,
				height:50,
				width: 800,
				name: 'subject',    
				enableSourceEdit : false,
				enableFont: false,
				enableAlignments: false,
				enableLinks: false,
				enableLists: false,
				enableColors: false,
				enableFontSize: false,
				enableFormat: false
			},{
				xtype:'htmleditor',
				id: 'templateeditor',
				fieldLabel : 'Body',
				autoScroll: true,
				height:150,
				width: 800,
				plugins: [
						  new Ext.ux.form.HtmlEditor.Word()  
						 ],
				name: 'body',    
				enableSourceEdit : false
			},{
				xtype      : 'box',
				html       : ' <div style="color: #4B8A08; margin:10px;margin-left:90px; font-size: 14px;"><b>Click in the Subject or the Body to be able to add variables.</b></div> '
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : <?php echo $userid; ?>
			},{
				xtype         : 'hidden',
				name          : 'template_id',
				id          : 'template_id'
			},{
				xtype         : 'hidden',
				name          : 'template_type',
				value         : 1
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>',
			id		: 'buttontemplate',
			handler : function(){
				if (template_email_form.getForm().isValid()) {
					template_email_form.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savetemplate.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							if (resp.mensaje == 'Template saved!') {
								updater.update({
									url   : 'mysetting_tabs/mycontracts_tabs/template_email.php',
									cache : false
								});
							}
						}
					});
				}
			}
		},{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Clear</b></span>',
			handler : function(){
				Ext.getCmp('templatesubject').setValue('');
				Ext.getCmp('templateeditor').setValue('');
			}
		}]
	});
	
	Ext.getCmp("templatesubject").getToolbar().addItem([{
		xtype         : 'combo',
		mode          : 'local',
		fieldLabel    : 'Vars',
		triggerAction : 'all',
		displayField  : 'name',
		valueField    : 'id',
		name          : 'variableInsertar',
		id			  : 'variableInsertarSub',
		value         : variableInsertarSub,
		allowBlank    : false,
		editable	  : false,
		width: 		    200,
		store         : new Ext.data.JsonStore({
			root:'results',
			totalProperty:'total',
			autoLoad: true, 
			baseParams: {
				type: 'template-variables'
			},
			fields:[
				{name:'id', type:'string'},
				{name:'name', type:'string'}
			],
			url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
			listeners     : {
				'load'  : function(store, records) {
					Ext.getCmp('variableInsertarSub').setValue(variableInsertarSub); 
				}
			}
		}),
		listeners	  : {
			'select'  : function(combo,record,index){
				variableInsertarSub = record.get('id');
			}
		}
	}]);	
	Ext.getCmp("templatesubject").getToolbar().addButton([{
		iconCls: 'icon', 
		icon: 'http://www.reifax.com/img/add.gif',
		handler: function () {
			Ext.getCmp("templatesubject").insertAtCursor(variableInsertarSub);
		},
		tooltip: 'Insert Variable'
	}]);
	Ext.getCmp("templateeditor").getToolbar().addItem([{
		xtype: 'tbseparator'
	}]);
	
	Ext.getCmp("templateeditor").getToolbar().addItem([{
		xtype         : 'combo',
		mode          : 'local',
		fieldLabel    : 'Vars',
		triggerAction : 'all',
		displayField  : 'name',
		valueField    : 'id',
		name          : 'variableInsertar',
		id          : 'variableInsertar',
		value         : variableInsertar,
		allowBlank    : false,
		editable	  : false,
		width: 		    200,
		store         : new Ext.data.JsonStore({
			root:'results',
			totalProperty:'total',
			autoLoad: true, 
			baseParams: {
				type: 'template-variables'
			},
			fields:[
				{name:'id', type:'string'},
				{name:'name', type:'string'}
			],
			url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
			listeners     : {
				'load'  : function(store, records) {
					Ext.getCmp('variableInsertar').setValue(variableInsertar); 
				}
			}
		}),
		listeners	  : {
			'select'  : function(combo,record,index){
				variableInsertar = record.get('id');
			}
		}
	}]);	
	Ext.getCmp("templateeditor").getToolbar().addButton([{
		iconCls: 'icon', 
		icon: 'http://www.reifax.com/img/add.gif',
		handler: function () {
			Ext.getCmp("templateeditor").insertAtCursor(variableInsertar);
		},
		tooltip: 'Insert Variable'
	}]);
	
	newTemplate();
</script>