<?php
/**
 * listascrows.php
 *
 * Return the list of images for scrow with an user ID and contract 
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 18.04.2011
 */

include "../../properties_conexion.php"; 
conectar();

$userid  = $_COOKIE['datos_usr']["USERID"];


$query   = "SELECT place,imagen,escrowAgent,escrowAddress,escrowEmail,escrowPhone,escrowFax FROM xima.contracts_scrow
				WHERE userid = $userid 
				ORDER BY place ASC";
$rs      = mysql_query($query);

if ($rs) {

	
	while ($row = mysql_fetch_assoc($rs)) {
		
		$data[]         = $row;
		
	}
	
	$resp = array('success'=>'true',
				  'scrow1'=>$data[0],
				  'scrow2'=>$data[1]);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	