<?php
/**
 * generatecontract.php
 *
 * Generate the contract for download.
 * 
 * @autor   Juan Vargas  <?@?.com>             Original Code
 *			Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
 * @version 07.04.2011
 */

include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];

$query    = "SELECT professional FROM xima.permission WHERE userid='$userid'";
$rs       = mysql_query($query);
$rp       = mysql_fetch_array($rs);
$platinum = $rp[0];
//$usrstate = explode(',',$rp[1]);

$query     = 'SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result    = mysql_query($query) or die($query.mysql_error());
$datos_usr = mysql_fetch_array($result);

$query     = 'SELECT * FROM xima.contracts_custom WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$rscr      = mysql_query($query) or die($query.mysql_error());
$countcr   = mysql_num_rows($rscr);

$checked   = '';
$combo     = '';
$contratos = '';
$i         = 0;

while ($rowcr = mysql_fetch_array($rscr)) {
	if ($i == 0){
		$checked = $rowcr['id']; $checked_name = $rowcr['name'];
	}

	$combo     .= "['{$rowcr['id']}','{$rowcr['name']}','{$rowcr['type']}'],";
	$contratos .= "arrayContratos[{$rowcr['id']}]='{$rowcr['filename']}';";
	$i++;
}
?>
<style type="text/css">
.x-form-file-wrap {
	position     : relative;
	height       : 22px;
	width        : 210px;
}
.x-form-file-wrap .x-form-file {
	position     : absolute;
	right        : 0;
	-moz-opacity : 0;
	filter       : alpha(opacity: 0);
	opacity      : 0;
	z-index      : 2;
	height       : 22px;
}
.x-form-file-wrap .x-form-file-btn {
	position     : absolute;
	right        : 0;
	z-index      : 1;
}
.x-form-file-wrap .x-form-file-text {
	position     : absolute;
	left         : 0;
	z-index      : 3;
	color        : #777;
}
.formulario {
	padding-top  : 2px; 
	color        : #274F7B; 
	font-weight  : bold; 
	font-size    : 14px;"
}
</style>

<div id="div_formulario" class="formulario"></div>

<div id="div_addons" class="formulario"></div>

<div id="div_scrow" class="formulario"></div>

<script type="text/javascript">

// ----------------------------------------------------
function getContract(){
	
	var arrayContratos = new Array(); 
	<?php echo $contratos; ?>
	
	var idcontract     = Ext.getCmp('ctype1').getValue();		
	var url            = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/template_upload/'+arrayContratos[idcontract];

	window.open(url);
}

function deleteContract(){
	var idcontract     = Ext.getCmp('ctype1').getValue();		
	if(idcontract && idcontract!=0)
	{
		Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(opt){
			if(opt == 'yes')																
			{
				
				Ext.Ajax.request({
					url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php?cmd=del&id='+idcontract,
					waitMsg : 'Deleting...',
					success : function(r) {
						Ext.MessageBox.alert('',r.responseText);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();

						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});					
					}
				});
			}
		});
	}

}

function modifyContract(typecontract,placecontract){
	if(typecontract=="addon"){
		var idcontract = Ext.getCmp('addon_id'+placecontract).getValue();
	}else{
		var idcontract = Ext.getCmp('ctype1').getValue();			
	}
	
	if(idcontract && idcontract!=0)
	{
		//Ext.MessageBox.alert('',idcontract);
		new Ext.util.Cookies.set("idmodifycontract", idcontract);
		new Ext.util.Cookies.set("typemodifycontract", typecontract);
		if(typecontract=="addon"){
			new Ext.util.Cookies.set("placemodifycontract", placecontract);
		}

		if(document.getElementById('idmodifycontract')){
			var tab = tabs.getItem('idmodifycontract');
			tabs.remove(tab);
		}
			
		Ext.getCmp('principal').add({
			//xtype:	'component', 
			title:		'Modify Contract', 
			closable:	true,
			autoScroll:	true,
			id:			'idmodifycontract', 
			autoLoad:	{url:'/custom_contract/manager.php',scripts:true}
		}).show();
	}

}

// ----------------------------------------------------------------
/*var contracts = new Ext.data.SimpleStore({
	fields : ['id','name','type'],
	data   : [
		<?php //echo $combo;?>
		['0','Upload new contract','O']
	]
});*/

var contracts = new Ext.data.JsonStore({
	url:		'/custom_contract/includes/php/functions.php',
	method:		'post',
	totalProperty:	'total',
	root:		'results',
	baseParams:{
		idfunction:	"contratos"
	},
	fields: [
		{name:'id', type:'string'},
		{name:'name', type:'string'},
		{name:'type', type:'string'},
		{name:'inspectionDays', type:'int'}
	],
	autoLoad: true
});
contracts.on('load',function(ds,records,o){
	Ext.getCmp('ctype1').setValue(records[0].data.id);
	Ext.getCmp('inspectionDays').setValue(records[0].data.inspectionDays);
	/////////////////////////////////Ejecutamos el Select del Combo de los Contratos////////////////////////////////
	Ext.getCmp('ctype1').fireEvent('select',Ext.getCmp('ctype1'),Ext.getCmp('ctype1').store.getAt(Ext.getCmp('ctype1').selectedIndex+1));
	//////////////Le Incremente 1 al indice para que no tomara el espacio en blanco desde un principio//////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});
//contracts.load();

var contractsAdd = new Ext.data.SimpleStore({
	fields : ['id','name','type'],
	data   : [
		<?php echo $combo;?>
		['0','Upload new document','O']
	]
});

function buscarcontrato() {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listcustomcontr.php',
		method  : 'POST',
		params    : {
			id : Ext.getCmp('ctype1').getValue()
		},
		waitMsg : 'Getting Info',
		success : function(r) {

			var resp   = Ext.decode(r.responseText)

			Ext.getCmp('option2').setValue(false);				
			Ext.getCmp('option1').setValue(true);

			if (resp.data!=null) {
				
				Ext.getCmp('name1').setValue(resp.data.tpl1_name);
				Ext.getCmp('address11').setValue(resp.data.tpl1_addr);
				Ext.getCmp('address12').setValue(resp.data.tpl1_addr2);
				Ext.getCmp('address13').setValue(resp.data.tpl1_addr3);
				if(resp.data.tpl2_type != null && resp.data.tpl2_type != ''){
					Ext.getCmp('cOptionType2').setValue('cOptionType2',resp.data.tpl2_type);
				}else{
					Ext.getCmp('cOptionType2').setValue([false,false]);
				}
				if(resp.data.tpl1_type != null && resp.data.tpl1_type != ''){
					Ext.getCmp('cOptionType1').setValue('cOptionType1',resp.data.tpl1_type);
				}else{
					Ext.getCmp('cOptionType1').setValue('cOptionType1',false);
				}

				Ext.getCmp('name2').setValue(resp.data.tpl2_name);
				Ext.getCmp('address21').setValue(resp.data.tpl2_addr);
				Ext.getCmp('address22').setValue(resp.data.tpl2_addr2);
				Ext.getCmp('address23').setValue(resp.data.tpl2_addr3);
								
				if (resp.data.tplactive == '1'){
					Ext.getCmp('option1').setValue(true);
					Ext.getCmp('option2').setValue(false);
				}
				if (resp.data.tplactive == '2'){
					Ext.getCmp('option1').setValue(false);
					Ext.getCmp('option2').setValue(true);
				}					
			} 
		}
	});
	
}
// ----------------------------------------------------------------

var formulario = new Ext.FormPanel({
	title      : 'User Options ',
	border     : true,
	bodyStyle  : 'padding: 10px;text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formul1',
	name       : 'formul1',
	items      : [
		{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 4 },
			rowspan      : 3,
			items        : [
				/*{
					xtype    : 'button',
					text     : 'Download Contract',
					handler  : getContract
				},*/{
					xtype         : 'combo',
					name          : 'ctype1',
					id            : 'ctype1',
					hiddenName    :'type',
					fieldLabel    : '<b>Contract Name</b>', 
					typeAhead     : true,
					store         : contracts,
					triggerAction : 'all',
					mode          : 'local',
					editable      : false,
					autoLoad	  : true,
					emptyText     : 'Select ...',
					selectOnFocus : true,
					allowBlank    : false,
					displayField  : 'name',
					valueField    : 'id',
					//value         : '0',
					width         : 300,
					listeners     : {
						'select'  : function(combo, record, index) {
							seleccionadorComboContratos();
							//////////record.data.type Validar si se muestra o no el Boton de UPDATE de los ADDONS
							listarAddons(record.data.type);
							///////////////////////////VALIDAR QUE SE MUESTRE EL BOTON DE EDITAR LOS DOCUMENTOS///////////////////////////
							if(record.data.type=="O"){
								Ext.getCmp('buttonmoddifycontract').hide();
								Ext.getCmp('address11').maxLength = 100;
								Ext.getCmp('controladdress12').hide();
								Ext.getCmp('controladdress13').hide();
								Ext.getCmp('address21').maxLength = 100;
								Ext.getCmp('controladdress22').hide();
								Ext.getCmp('controladdress23').hide();
							}else{
								Ext.getCmp('buttonmoddifycontract').show();
								Ext.getCmp('address11').maxLength = 45;
								Ext.getCmp('controladdress12').show();
								Ext.getCmp('controladdress13').show();
								Ext.getCmp('address21').maxLength = 45;
								Ext.getCmp('controladdress22').show();
								Ext.getCmp('controladdress23').show();
							}
							Ext.getCmp('inspectionDays').setValue(record.data.inspectionDays);
							//////////////////////////////////////////////////////FIN/////////////////////////////////////////////////////
						}
					}
				},{
					// Button added by alexbariv
					xtype    : 'button',
					id		 : 'buttonmoddifycontract',
					icon	 : '/img/update.png',
					tooltip  : 'Modify contract',
					hidden	 : true,
					cls		 : 'x-btn-text-icon',
					iconAlign: 'left',
					text     : ' ',
					scale: 'medium',
					style    : {
						marginRight : 10,
						marginLeft  : 10
					},
					handler  : function (){
						var navegador=navigator.userAgent;
						if(navegador.indexOf('MSIE') != -1) {
							if(parseInt(BrowserDetect.version)<9){
								Ext.Msg.show({
								   title      : '',
								   //msg        : 'This option works only with Firefox and Chrome. Internet Explorer users please download and use any of the mentioned browsers.',
								   msg		  : 'This option only works with IE9 or above.',
								   width      : 400,
								   buttons    : Ext.MessageBox.OK,
								   icon       : Ext.MessageBox.INFO
								})
							}else{
								modifyContract("contract","0");
							}
						}else{
							modifyContract("contract","0");
						}
					}					
					//handler  : deleteContract
				},{
					// Button added by alexbariv
					xtype    : 'button',
					id		 : 'buttondeletecontracts',
					icon	 : '/img/del_doc.png',
					tooltip: 'Delete Document',
					cls:'x-btn-text-icon',
					//iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
					style    : {
						marginRight : 10
					},
					handler  : deleteContract
				},{
					xtype    : 'button',
					tooltip: 'View Help',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
					style    : {
						marginRight : 10
					},
					handler: function(){
						//alert(user_loged+"||"+user_block+"||"+user_web);
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
						showVideoHelp('Mycontracts');
					}
				}
			]
		},{
			xtype         : 'panel', 
			id            : 'panelContractUpload',
			layout       : 'table',
			layoutConfig : { columns : 2 },
			
			items         : [
				{
					layout: 'form',
					labelWidth	: 60,
					items:[{					
						xtype      : 'textfield',
						id         : 'pdfname',
						fieldLabel : 'PDF Name',
						name       : 'pdfname',
						//allowBlank : false,
						width      : '200'
					}]
				},{
					layout: 'form',
					labelWidth	: 70,
					items:[{											
						xtype       : 'fileuploadfield',
						id          : 'idupload',
						emptyText   : 'Select pdf',
						fieldLabel  : 'Upload File',
						name        : 'pdfupload',
						width		: 300,
						buttonText  : 'Browse...'
					}]
				}
			]
		},{
			xtype         : 'panel',
			id            : 'panelContractEdit',
			layout        : 'form',
			items         : [
				{
					xtype        :'spacer',
					height       : 10
				},{
					xtype        :'panel',
					layout       :'table',
					<?php
					if ($countcr == 0)
						echo "hidden : true,";
					?>
					defaults     : {
						bodyStyle  : 'padding:5px; marginLeft: 10px;marginTop:10px'
					},
					layoutConfig : {
						columns  : 4
					},
					items: [
						{
							xtype          : 'fieldset',
							checkboxToggle : false,
							title          : 'Option 1',
							id             : 'idtemplate1',
							border         : 'false',
							layout         : 'form',
							labelWidth     : 90,
							colspan        : 2,
							style          : {
								marginLeft : 40
							},
							items          : [
								{
									xtype: 'radiogroup',
									id: 'cOptionType1',
									fieldLabel: 'Type',
									items: [
										{boxLabel: 'Buyer', name: 'cOptionType1', inputValue:'B', checked: true},
										{boxLabel: 'Seller', name: 'cOptionType1', inputValue:'S'}
									]
								},{
									xtype      : 'textfield',
									id         : 'name1',
									fieldLabel : 'Name',
									name       : 'name1',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address11',
									fieldLabel : 'Address Line 1',
									name       : 'address11',
									width      : '280',
									maxLength  : 45,
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype        : 'panel',
									layout       : 'table',
									id: 'controladdress12',
									items: [
									{
										layout: 'form',
										labelWidth	: 90,
										items:[{
											xtype      : 'textfield',
											id         : 'address12',
											fieldLabel : 'Address Line 2',
											name       : 'address12',
											width      : '280',
											maxLength  : 45,
											allowBlank : true,
											disabled   : false,
											style      : {
												width        : '95%',
												marginBottom : '10px'
											}
										}]
									}]
								},{
									xtype        : 'panel',
									layout       : 'table',
									id: 'controladdress13',
									items: [
									{
										layout: 'form',
										labelWidth	: 90,
										items:[{
											xtype      : 'textfield',
											id         : 'address13',
											fieldLabel : 'Address Line 3',
											name       : 'address13',
											width      : '280',
											maxLength  : 45,
											allowBlank : true,
											disabled   : false,
											style      : {
												width        : '95%',
												marginBottom : '10px'
											}
										}]
									}]
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'option1',
									name       : 'option1',
									onClick    : function() {
										
										if (Ext.getCmp('option2').getValue() == true) 
											Ext.getCmp('option2').setValue(false);
											
										if (Ext.getCmp('option1').getValue() == false) 
											Ext.getCmp('option1').setValue(true);
											
									}
								}
							]
						},{
							xtype          : 'fieldset',
							checkboxToggle : false,
							title          : 'Option 2',
							id             : 'idtemplate2',
							border         : 'false',
							layout         : 'form',
							labelWidth     : 90,
							colspan        : 2,
							style          : {
								marginLeft : 40
							},
							items          : [
								{
									xtype: 'radiogroup',
									id: 'cOptionType2',
									fieldLabel: 'Type',
									items: [
										{boxLabel: 'Buyer', name: 'cOptionType2', inputValue:'B', checked: true},
										{boxLabel: 'Seller', name: 'cOptionType2', inputValue:'S'}
									]
								},{
									xtype      : 'textfield',
									id         : 'name2',
									fieldLabel : 'Name',
									name       : 'name2',
									width      : '200',
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype      : 'textfield',
									id         : 'address21',
									fieldLabel : 'Address Line 1',
									name       : 'address21',
									width      : '280',
									maxLength  : 45,
									allowBlank : true,
									disabled   : false,
									style      : {
										width        : '95%',
										marginBottom : '10px'
									}
								},{
									xtype        : 'panel',
									layout       : 'table',
									id: 'controladdress22',
									items: [
									{
										layout: 'form',
										labelWidth	: 90,
										items:[{
											xtype      : 'textfield',
											id         : 'address22',
											fieldLabel : 'Address Line 2',
											name       : 'address22',
											width      : '280',
											maxLength  : 45,
											allowBlank : true,
											disabled   : false,
											style      : {
												width        : '95%',
												marginBottom : '10px'
											}
										}]
									}]
								},{
									xtype        : 'panel',
									layout       : 'table',
									id: 'controladdress23',
									items: [
									{
										layout: 'form',
										labelWidth	: 90,
										items:[{
											xtype      : 'textfield',
											id         : 'address23',
											fieldLabel : 'Address Line 3',
											name       : 'address23',
											width      : '280',
											maxLength  : 45,
											allowBlank : true,
											disabled   : false,
											style      : {
												width        : '95%',
												marginBottom : '10px'
											}
										}]
									}]
								},{
									xtype      : 'checkbox',
									fieldLabel : 'Active',
									id         : 'option2',
									name       : 'option2',
									onClick    : function() {
										
										if (Ext.getCmp('option1').getValue() == true) 
											Ext.getCmp('option1').setValue(false);
											
										if (Ext.getCmp('option2').getValue() == false) 
											Ext.getCmp('option2').setValue(true);
											
									}
								}
							]
						},{
							xtype          : 'fieldset',
							title          : 'Additional Info',
							border         : 'false',
							labelWidth     : 90,
							colspan        : 4,
							style          : {
								marginLeft : 40
							},
							items          : [{
								layout: 'form',
								labelWidth	: 90,
								items:[{
									xtype      		:	'numberfield',
									allowDecimals	:	false,
									allowNegative	:	false,
									id         		:	'inspectionDays',
									fieldLabel 		:	'Inspection Days',
									name       		:	'inspectionDays',
									width      		:	'280',
									maxLength  		:	45,
									allowBlank 		:	false,
									style      		:	{
										//width        : '95%',
										marginBottom : '10px'
									}
								}]
							}]
						}
					]
				}
			]
		}
	],
	buttonAlign : 'center',
	buttons     : [{
		text        : '<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save</b></span>',
		handler     : function() {
			if(Ext.getCmp('ctype1').getValue()=='0' || Ext.getCmp('ctype1').getValue()==0 ){
				Ext.getCmp('pdfname').allowBlank=false;
				if(formulario.getForm().isValid()) {
					Ext.Msg.show({
					   title      : 'Warning!',
					   msg        : 'The following Document Generator feature of REIFAX.com (herein after known as REIFAX) is for the sole purpose of modifying existing documents that the user uploads to the system.<br/>No documents will be supplied by REIFAX.<br/>The user MUST HAVE legal authority to upload, modify, change, fill-in and use any document he intends to modify and submit to another buyer or seller.<br/>Legal authority means that if a copyrighted document is uploaded, the user MUST HAVE written permission of the person or entity who drafted the document, or have paid an authorized re-seller for the use of that document, i.e. FAR/BAR forms.<br/>By uploading any document, the user hereby agrees to these terms and conditions for using the REIFAX Document Generator.<br/>Violation of these terms and conditions could result in the user violating copyright laws.<br/>The only information that will be filled in by REIFAX, on any form, if applicable is information that is available in the public record.<br/>Any and all other information will have to be supplied by the user.',
					   width      : 550,
					   buttons    : Ext.MessageBox.OKCANCEL,
					   fn         : myCallbackDisclaimer,
					   icon       : Ext.MessageBox.INFO
					})
				}
			}else{
				Ext.getCmp('pdfname').allowBlank=true;
				if(formulario.getForm().isValid()) {
					formulario.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp    = a.result;
							var tab     = tabs3.getActiveTab();
							var updater = tab.getUpdater();
							
							if(typeof contractsFL != 'undefined'){
								contractsFL.reload();
							}
							if (resp.mensaje == 'Changes Successfully Applied!') {
								updater.update({
									url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
									cache : false
								});
							}
							Ext.MessageBox.alert('', resp.mensaje);
						},
						failure : function(response, o){ }	
					});
				}
			}
		}			 
	}]
});


var myCallbackDisclaimer = function(btn, text) {
	if(btn == 'ok')		
	{
		if(formulario.getForm().isValid()) {
			formulario.getForm().submit({
				url     : 'mysetting_tabs/mycontracts_tabs/savecontracts.php',
				waitMsg : 'Saving...',
				timeout	: 0,
				success : function(f, a){ 
					var resp    = a.result;
					var tab     = tabs3.getActiveTab();
					var updater = tab.getUpdater();
					
					if (resp.mensaje == 'Changes Successfully Applied!') {
						if(typeof contractsFL != 'undefined'){
							contractsFL.reload();
						}
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
						Ext.Msg.show({
						   title      : '',
						   msg        : '<p align="center">Document saved successfully!</p>The document you just uploaded can be edited by yourself. Go to the Contract Manager by clicking in the icon to generate the contract; in there, below the contract selection combo, there will be a pencil with the text \'Modify contract\' if your contract is editable',
						   width      : 480,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.INFO
						});
					}else{
						Ext.Msg.show({
						   title      : 'Info.',
						   msg        : resp.mensaje,
						   //width      : 100,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.WARNING
						})														
					}
					//Ext.MessageBox.alert('', resp.mensaje);
				},
				failure : function(response, o){ }
			});
		}
	}
}


var seleccionadorComboContratos = function() {
	if (Ext.getCmp('ctype1').getValue() == 0 ) {
		Ext.getCmp('panelContractUpload').show();
		Ext.getCmp('panelContractEdit').hide();
		Ext.getCmp('buttondeletecontracts').hide();
		Ext.getCmp('formul2').hide();
	} else  {
		Ext.getCmp('panelContractUpload').hide();
		Ext.getCmp('panelContractEdit').show();
		Ext.getCmp('buttondeletecontracts').show();
		Ext.getCmp('formul2').show();
		buscarcontrato();
		Ext.getCmp('ctype2').setValue(Ext.getCmp('ctype1').getValue());		
		//listarAddons();
	}
}
   
var seleccionadorComboContratosAdicionales = function() {

}
    
formulario.render('div_formulario');
//seleccionadorComboContratos();
<?php
if ($countcr > 0){
	//echo "Ext.getCmp('ctype1').setValue('$checked');";	//Original de Juan
	//echo "Ext.getCmp('ctype1').setValue('$checked_name');";	//Original de Jesus
	echo "Ext.getCmp('panelContractUpload').hide();";
}
?>

var addonsForm = new Ext.FormPanel({
	title      : 'Additional Documents',
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	<?php
	if ($countcr == 0)
		echo "hidden : true,";
	?>
	method     : 'POST',
	id         : 'formul2',
	name       : 'formul2',
	items      : [
		{
			xtype         : 'spacer',
			height        : 10
		},{
			xtype         : 'box',
			html          : 'All the additional documents will be placed at the end, with a new page for each one.',
			cls           : 'x-form-item'
		},{
			xtype         : 'combo',
			name          : 'ctype2',
			id            : 'ctype2',
			hiddenName    : 'type_a',
			typeAhead     : true,
			store         : contracts,
			fieldLabel    : '<b>Document Type</b>',
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			emptyText     : 'Select ...',
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value         : 0,
			width         : 300,
			listeners     : {
				'select'  : function(combo, record, index) {
					//////////record.data.type Validar si se muestra o no el Boton de UPDATE de los ADDONS
					listarAddons(record.data.type);
					Ext.getCmp('ctype1').setValue(Ext.getCmp('ctype2').getValue());		
					seleccionadorComboContratos();
					///////////////////////////VALIDAR QUE SE MUESTRE EL BOTON DE EDITAR LOS DOCUMENTOS///////////////////////////
					if(record.data.type=="O")
						Ext.getCmp('buttonmoddifycontract').hide();
					else
						Ext.getCmp('buttonmoddifycontract').show();
					//////////////////////////////////////////////////////FIN/////////////////////////////////////////////////////
					Ext.getCmp('inspectionDays').setValue(record.data.inspectionDays);
				}
			}
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			width        : 800,
			layoutConfig : {
				columns  : 10
			},
			items       : [
				<?php
				for ($aI = 1 ; $aI <= 6; $aI ++) {
				?>
				{
					xtype       : 'checkbox',
					id          : 'addon_act<?=$aI?>',
					style       : {
						marginLeft : '30px'
					}
				},{
					html        : '&nbsp;Name:&nbsp;',
					cls         : 'x-form-item'
				},{
					xtype       : 'hidden',
					id          : 'addon_id<?=$aI?>'
				},{
					xtype       : 'textfield',
					width       : 250,
					id          : 'addon_name<?=$aI?>'
				}/*,{
					html        : 'Select a PDF File',
					cls         : 'x-form-item',
					style       : { marginRight : '10px',marginLeft : '10px' }
				}*/,{
					xtype       : 'fileuploadfield',
					id          : 'form-aon<?=$aI?>',
					//emptyText   : 'Upload Document',
					name        : 'addo<?=$aI?>',
					buttonOnly	: true,
					//hideLabel: true,
					//width       : 20,
					buttonCfg : {
						//width       : 20,
						icon : '/img/contract_upload.png',
						cls: 'x-btn-text-icon',
						text : ''
					},
					listeners:{
						'afterrender':function(element){
							//Ext.get(element.id+'-file').setWidth(element.button.getEl().getWidth());
							//Ext.get(element.id).setWidth('10');
							//Ext.get(element.id).setStyle('display','none');
							//Ext.getCmp('formul2').doLayout();
						}
					}
					//buttonText  : 'Browse...'
				},{
					xtype       : 'button',
					//text        : 'Delete',
					icon	 : '/img/del_doc.png',
					tooltip: 'Delete',
					/*cls:'x-btn-text-icon',
					//iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',*/
					handler     : function() {eliminarAddon(<?=$aI?>);},
					id          : 'addon_del<?=$aI?>',
				},{
					xtype       : 'button',
					//text        : 'Update',
					tooltip		: 'Update',
					icon	 	: '/img/contract_update16.png',
					handler     : function() {
						var navegador=navigator.userAgent;
						if(navegador.indexOf('MSIE') != -1) {
							if(parseInt(BrowserDetect.version)<9){
								Ext.Msg.show({
								   title      : '',
								   //msg        : 'This option works only with Firefox and Chrome. Internet Explorer users please download and use any of the mentioned browsers.',
								   msg		  : 'This option only works with IE9 or above.',
								   width      : 400,
								   buttons    : Ext.MessageBox.OK,
								   icon       : Ext.MessageBox.INFO
								});
							}else{
								modifyContract('addon',<?=$aI?>);
							}
						}else{
							modifyContract('addon',<?=$aI?>);
						}
					},
					id          : 'addon_mod<?=$aI?>',
					hidden      : true
				},{
					xtype	: 'checkboxgroup',
					id		: 'addon_types<?=$aI?>',
					width   : 300,
					labelStyle: 'top:10px;',
					style       : { marginRight : '10px',marginLeft : '20px',marginTop:'10px' },
					//style: 'top:10px;',
					items: [
						{boxLabel: 'EMD &nbsp;', labelStyle: 'top:10px;', name: 'emd-<?=$aI?>'},
						{boxLabel: 'POF &nbsp;', name: 'pof-<?=$aI?>'},
						{boxLabel: 'ADDITIONAL &nbsp;', name: 'add-<?=$aI?>'}
					],
					hidden	: true
				},{
					html        : '&nbsp;', colspan : 10, style : { height : '10px' }
				}
				<?php
				if ($aI<6){
				?>
				,
				<?php
				}
				}
				?>
			]
		},{
			xtype       : 'box',
			html        : 'Use the checkbox field to select the active documents.',
			cls         : 'x-form-item'
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Addons</b></span>',
		handler : function(){
			if (addonsForm.getForm().isValid()) {
				addonsForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php',
					waitMsg : 'Saving...',
					timeout	: 0,
					success : function(f, a){ 
						var resp = a.result;
						Ext.Msg.show({
						   title      : 'Info.',
						   msg        : resp.mensaje,
						   //width      : 100,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.INFO
						});
						//Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						
						if(typeof contractsFL != 'undefined'){
							contractsFL.reload();
						}
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					},
					failure : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	}]
});
addonsForm.render('div_addons');
<?php
if ($countcr > 0)
	echo "Ext.getCmp('ctype2').setValue($checked);";
?>

var scrowForm = new Ext.FormPanel({
	title      : 'Escrow Agent Information',
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	<?php
	if ($countcr == 0)
		echo "hidden : true,";
	?>
	method     : 'POST',
	id         : 'formul3',
	name       : 'formul3',
	items      : [
		{
			xtype:'spacer',
			height: 10
		},{
			xtype        : 'panel',
			layout       : 'table',
			border       : true,
			bodyStyle    : 'padding-left: 20px;',
			autoWidth        : true,
			layoutConfig : {
				columns  : 4
			},
			items       : [{
				xtype          : 'fieldset',
				checkboxToggle : false,
				title          : 'Option 1',
				border         : 'false',
				layout         : 'form',
				labelWidth     : 90,
				colspan        : 2,
				style          : {
					marginLeft : 40
				},
				items          : [{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowAgent1',
						id			: 'Contract-escrowAgent1',
						width		: 300,
						fieldLabel	: 'Escrow Agent'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowAddress1',
						id			: 'Contract-escrowAddress1',
						width		: 300,
						fieldLabel	: 'Address'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowEmail1',
						id			: 'Contract-escrowEmail1',
						width		: 300,
						vtype		: 'email',
						fieldLabel	: 'Email'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowPhone1',
						id			: 'Contract-escrowPhone1',
						width		: 300,
						fieldLabel	: 'Phone'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowFax1',
						id			: 'Contract-escrowFax1',
						width		: 300,
						fieldLabel	: 'Fax'
				},{
					xtype      : 'checkbox',
					fieldLabel : 'Active',
					id         : 'scrowOption1',
					name       : 'scrowOption1',
					onClick    : function() {

						if (Ext.getCmp('scrowOption2').getValue() == true)
							Ext.getCmp('scrowOption2').setValue(false);

						if (Ext.getCmp('scrowOption1').getValue() == false)
							Ext.getCmp('scrowOption1').setValue(true);

					}
				}]
			},{
				xtype          : 'fieldset',
				checkboxToggle : false,
				title          : 'Option 2',
				border         : 'false',
				layout         : 'form',
				labelWidth     : 90,
				colspan        : 2,
				style          : {
					marginLeft : 40
				},
				items          : [{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowAgent2',
						id			: 'Contract-escrowAgent2',
						width		: 300,
						fieldLabel	: 'Escrow Agent'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowAddress2',
						id			: 'Contract-escrowAddress2',
						width		: 300,
						fieldLabel	: 'Address'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowEmail2',
						id			: 'Contract-escrowEmail2',
						width		: 300,
						vtype		: 'email',
						fieldLabel	: 'Email'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowPhone2',
						id			: 'Contract-escrowPhone2',
						width		: 300,
						fieldLabel	: 'Phone'
				},{
						xtype       : 'textfield',
						allowBlank	: true,
						name		: 'Contract-escrowFax2',
						id			: 'Contract-escrowFax2',
						width		: 300,
						fieldLabel	: 'Fax'
				},{
					xtype      : 'checkbox',
					fieldLabel : 'Active',
					id         : 'scrowOption2',
					name       : 'scrowOption2',
					onClick    : function() {

						if (Ext.getCmp('scrowOption1').getValue() == true)
							Ext.getCmp('scrowOption1').setValue(false);

						if (Ext.getCmp('scrowOption2').getValue() == false)
							Ext.getCmp('scrowOption2').setValue(true);

					}
				}]
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Escrow</b></span>',
		handler : function(){
			if (scrowForm.getForm().isValid()) {
				scrowForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});
var platinum = '<?php echo $platinum;?>';
//if (platinum == '1')
scrowForm.render('div_scrow');


// ----------------------------------------------------
buscarcontrato();

// ----------------------------------------------------

function listarAddons(type) { 
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listaddons.php',

		method  : 'POST',
		params    : {
			type  : Ext.getCmp('ctype2').getValue()
		},
		waitMsg : 'Getting Info',
		success : function(r) {

			var resp   = Ext.decode(r.responseText)
			for (w=1; w<=6; w++){
				Ext.getCmp('addon_name'+w).setValue('');
				Ext.getCmp('addon_act'+w).setValue(false);
				Ext.getCmp('addon_del'+w).hide();
				Ext.getCmp('addon_mod'+w).hide();
				Ext.getCmp('addon_types'+w).hide();
			}
			for (i=0; i<6; i++){
				if (resp.data!=null) {
	
					if (resp.data[i]!=null) {
						
						k = resp.data[i].place;
							
						Ext.getCmp('addon_name'+k).setValue(resp.data[i].name);
						Ext.getCmp('addon_id'+k).setValue(resp.data[i].id);
						
						if(resp.data[i].active=='1'){
							Ext.getCmp('addon_act'+k).setValue(true);
						}else{
							Ext.getCmp('addon_act'+k).setValue(false);
						}
							
						Ext.getCmp('addon_del'+k).show();
						if(type=="N"){ /////////Validacion si el contrato es nuevo o viejo
							Ext.getCmp('addon_mod'+k).show();
							Ext.getCmp('addon_types'+k).show();
						}
						if(resp.data[i].emd=='1'){
							Ext.getCmp('addon_types'+k).setValue('emd-'+k,true);
						}else{
							Ext.getCmp('addon_types'+k).setValue('emd-'+k,false);
						}
						if(resp.data[i].pof=='1'){
							Ext.getCmp('addon_types'+k).setValue('pof-'+k,true);
						}else{
							Ext.getCmp('addon_types'+k).setValue('pof-'+k,false);
						}
						if(resp.data[i].add=='1'){
							Ext.getCmp('addon_types'+k).setValue('add-'+k,true);
						}else{
							Ext.getCmp('addon_types'+k).setValue('add-'+k,false);
						}
					}
					
				} else {

					w = i + 1;

					Ext.getCmp('addon_name'+w).setValue('');
					Ext.getCmp('addon_act'+w).setValue(false);
					Ext.getCmp('addon_del'+w).hide();
					Ext.getCmp('addon_mod'+w).hide();
					Ext.getCmp('addon_types'+w).hide();
				
				}
			}
		}
	});
	
}

listarAddons();

function listarScrows() {
	Ext.getCmp('scrowOption1').setValue(true);
	Ext.getCmp('scrowOption2').setValue(false);

	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/listascrows.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)
			if(resp.scrow1 != null){
				Ext.getCmp('Contract-escrowAgent1').setValue(resp.scrow1.escrowAgent);
				Ext.getCmp('Contract-escrowAddress1').setValue(resp.scrow1.escrowAddress);
				Ext.getCmp('Contract-escrowEmail1').setValue(resp.scrow1.escrowEmail);
				Ext.getCmp('Contract-escrowPhone1').setValue(resp.scrow1.escrowPhone);
				Ext.getCmp('Contract-escrowFax1').setValue(resp.scrow1.escrowFax);
				if (resp.scrow1.imagen == '1'){
					Ext.getCmp('scrowOption1').setValue(true);
					Ext.getCmp('scrowOption2').setValue(false);
				}
			}
			if(resp.scrow2 != null){
				Ext.getCmp('Contract-escrowAgent2').setValue(resp.scrow2.escrowAgent);
				Ext.getCmp('Contract-escrowAddress2').setValue(resp.scrow2.escrowAddress);
				Ext.getCmp('Contract-escrowEmail2').setValue(resp.scrow2.escrowEmail);
				Ext.getCmp('Contract-escrowPhone2').setValue(resp.scrow2.escrowPhone);
				Ext.getCmp('Contract-escrowFax2').setValue(resp.scrow2.escrowFax);
				if (resp.scrow2.imagen == '1'){
					Ext.getCmp('scrowOption2').setValue(true);
					Ext.getCmp('scrowOption1').setValue(false);
				}
			}
			/*if (resp.scrow1!=null)
				Ext.getCmp('scrow_val1').setValue(resp.scrow1.imagen);

			if (resp.scrow2!=null)
				Ext.getCmp('scrow_val2').setValue(resp.scrow2.imagen);
			*/
		}
	});
	
}

if (platinum == 1)
	listarScrows();

function eliminarAddon(op) 
{
	var type = Ext.getCmp('ctype2').getValue();
	if(type && type!=0)
	{
		Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(opt)
		{
			if(opt == 'yes')																
			{
				Ext.Ajax.request({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php?cmd=del&addon='+op+'&type_a='+type,
					waitMsg : 'Deleting...',
					success : function(r) 
					{
						Ext.MessageBox.alert('',r.responseText);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});					
						/*Ext.getCmp('addon_act'+op).setValue(false);
						Ext.getCmp('addon_name'+op).setValue('');
						Ext.getCmp('addon_del'+op).hide();
						*/
					}
				});
			}
		});
	}
}

function actualizarAddon(op) 
{
	alert("op");
	var type = Ext.getCmp('addon_name'+op).getValue();
	alert(type);
	alert(Ext.getCmp('ctype2').getValue());
	if(type && type!=0)
	{
		Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(opt)
		{
			if(opt == 'yes')																
			{
				alert(type);
/*				Ext.Ajax.request({
					url     : 'mysetting_tabs/mycontracts_tabs/saveaddons.php?cmd=del&addon='+op+'&type_a='+type,
					waitMsg : 'Deleting...',
					success : function(r) 
					{
						Ext.MessageBox.alert('',r.responseText);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
							cache : false
						});					
						/*Ext.getCmp('addon_act'+op).setValue(false);
						Ext.getCmp('addon_name'+op).setValue('');
						Ext.getCmp('addon_del'+op).hide();
						*/
/*					}
				});*/
			}
		});
	}
}

function eliminarScrow(op) {
	
	Ext.Ajax.request({
		url     : 'mysetting_tabs/mycontracts_tabs/savescrows.php?cmd=del&scrow='+op,
		waitMsg : 'Deleting...',
		success : function(r) {
			Ext.MessageBox.alert('',r.responseText);
			Ext.getCmp('addon_val'+op).setValue('');
		}
	});
	
}

</script>