<?php
/**
 * testmailsett.php
 *
 * Test the mail server settings
 * 
 * @autor   Guillermo Vera G <guilleverag@gmail.com>
 * @version 10.02.2012
 */
if(isset($_POST['server'])){
	require_once('../../mailer/class.phpmailer.php');
	require_once('../../mailer/class.smtp.php');
	
	$email 				= new PHPMailer();
	$email->Mailer 		= "smtp";
	$email->Host 		= addslashes($_POST['server']);
	$email->Port 		= $_POST['port'];
	$email->SMTPAuth 	= true;
	$email->Username 	= $_POST['username'];
	$email->Password 	= $_POST['password'];

	if($email->SmtpConnect()===true){
		$resp = array('success'=>'true','mensaje'=>'Succesfully Connected!!!');
		$email->SmtpClose();
	}else{
		$error='';
		foreach($email->smtp->error as $k => $e){
			$error.=', '.$e;
		}
		if(count($email->smtp->error)==0)
			$error = 'Username or Password not accepted from server';
			
		$resp = array('success'=>'true','mensaje'=>'Connection error!!!,'.$error);
	}
}else{
	include("../myfollowup_tabs/receivemail.class.php");
	
	if($_POST['server_type']==1){ 
		$protocol='imap';
		$auth=false;
	}elseif($_POST['server_type']==2){
		$protocol='pop3';
		$auth=false;
	}elseif($_POST['server_type']==3){
		$protocol='imap';
		$auth=true;
	}elseif($_POST['server_type']==4){
		$protocol='pop3';
		$auth=true;
	}
	
	// Creating a object of reciveMail Class
	$obj = new receiveMail($_POST['imap_username'],$_POST['imap_password'],$_POST['imap_username'],addslashes($_POST['imap_server']),$protocol,$_POST['imap_port'],$auth);
	//Connect to the Mail Box
	$error = $obj->connect();
	if($error===true){
		$resp = array('success'=>'true','mensaje'=>'Succesfully Connected!!!');
	}else{
		$resp = array('success'=>'true','mensaje'=>'Connection error!!!,'.$error);
	}
}

echo json_encode($resp);
?>	