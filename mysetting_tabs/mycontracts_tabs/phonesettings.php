<?php
include "../../properties_conexion.php";
conectar();

$userid = $_COOKIE['datos_usr']["USERID"];
$signs  = null;

$query  = "SELECT * FROM xima.contracts_phonesettings
				WHERE userid = $userid";
$rs     = mysql_query($query);
$row    = mysql_fetch_array($rs);

$skype_enable = $row['skype_enable']==0 ? true : false;
$sms_domain=$row['sms_domain'];

if($sms_domain!=''){
	if($sms_domain=='txt.voice.google.com'){
		$type_sms='1';
	}else{
		$type_sms='0';
	}
}else{
	$type_sms='0';
}

$fax_domain=$row['fax_domain'];
if($fax_domain!=''){
	if($fax_domain=='imap.gmail.com'){
		$type_fax='1';
	}else{
		$type_fax='0';
	}
}else{
	$type_fax='0';
}
?>
<div id="div_skypesett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_plivosett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<!--<div id="div_smssett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_voicesett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>-->

<div id="div_faxsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">
var smsStore = new Ext.data.SimpleStore({
	fields : ['id','name','domain'],
	data   : [
		['1','Google','txt.voice.google.com'],
		['0','Other','']
	]
});
var voiceStore = new Ext.data.SimpleStore({
	fields : ['id','name','domain'],
	data   : [
		['1','Google','voice-noreply@google.com'],
		['0','Other','']
	]
});
var faxStore = new Ext.data.SimpleStore({
	fields : ['id','name','domain'],
	data   : [
		//['1','Google','txt.voice.google.com'],
		['0','Other','']
	]
});

var skypesettForm = new Ext.FormPanel({
	title      : 'Skype Call Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	labelWidth: 150,
	items      : [
		{
			html        : 'Please enable Skype Call, only if you already have Skype installed.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
            xtype: 'panel',
            layout: 'form',
			id: 'sms_delivery',	
            items: [{
				xtype      : 'checkbox',
				fieldLabel : 'Skype Call Enabled',
				name       : 'skype_enable',
				id         : 'skype_enable',
				width      : '200',
				checked	   : <?php echo $skype_enable ? 'true' : 'false';?>
			},{
				xtype      : 'box',
				html       : 'To install Skype, you can download the application <a href="http://www.skype.com/en/download-skype/skype-for-computer/" target="_blank">here</a>.'
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Skype Settings</b></span>',
		handler : function(){
			if (skypesettForm.getForm().isValid()) {
				skypesettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var check = Ext.getCmp('skype_enable');
						skypeCallEnabled = check.checked;
					}
				});
			}
		}
	}/*,{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('PhoneSetting');
		}	
	}*/]
});

var plivosettForm = new Ext.FormPanel({
	title      : 'Plivo Call Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	labelWidth: 150,
	items      : [
		{
			html        : 'Please provide the Autorizations to enable the Plivo Call.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
            xtype: 'panel',
            layout: 'form',
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'AUTH ID',
				name       : 'authid',
				id         : 'authid',
				width      : '200',
				value      : '<?=$row['plivo_authid']?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'AUTH TOKEN',
				name       : 'authtoken',
				id         : 'authtoken',
				width      : '200',
				value      : '<?=$row['plivo_authtoken']?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Plivo Number',
				name       : 'plivo_phone',
				width      : '200',
				value      : '<?=$row['plivo_phone']?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Plivo Forward Number',
				name       : 'plivo_forward',
				width      : '200',
				value      : '<?=$row['plivo_forward']?>',
				allowBlank : false
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Plivo Settings</b></span>',
		handler : function(){
			if (plivosettForm.getForm().isValid()) {
				plivosettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/phonesettings.php',
							cache : false
						});
					}
				});
			}
		}
	}]
});

var smssettForm = new Ext.FormPanel({
	title      : 'Google voice SMS & Dialer Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	labelWidth: 150,
	items      : [
		{
			html        : 'Please provide the server information to enable the SMS Delivery.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'sms_service',
			id            : 'sms_service',
			hiddenName    : 'himap_service',
			fieldLabel    : 'Mail Service',
			store         : smsStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_sms?>',
			listeners     : {
				'select'  : selectSms
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_sms=='0' ? 'false' : 'true';?>,
			id: 'sms_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Google Voice Number',
				name       : 'sms_phone',
				id         : 'sms_phone',
				width      : '200',
				value      : '<?=addslashes($row['sms_phone'])?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Google Forward Number',
				name       : 'forward_phone',
				id         : 'forward_phone',
				width      : '200',
				value      : '<?=addslashes($row['forward_phone'])?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Domain',
				id         : 'sms_domain',
				name       : 'sms_domain',
				width      : '200',
				value      : '<?=$row['sms_domain']?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with txt.voice.yourcompany.com)</div>'
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save SMS Settings</b></span>',
		handler : function(){
			if (smssettForm.getForm().isValid()) {
				smssettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/phonesettings.php',
							cache : false
						});
					}
				});
			}
		}
	}/*,{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('PhoneSetting');
		}	
	}*/]
});
var voicesettForm = new Ext.FormPanel({
	title      : 'Google Voice message Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the email information to enable the Voice mail.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_sms=='0' ? 'false' : 'true';?>,
			id: 'voice_delivery',	
            items: [{
				xtype         : 'combo',
				name          : 'voice_service',
				id            : 'voice_service',
				hiddenName    : 'hvoice_service',
				fieldLabel    : 'Voice Service',
				store         : voiceStore,
				triggerAction : 'all',
				mode          : 'local',
				editable      : false,
				selectOnFocus : true,
				displayField  : 'name',
				valueField    : 'id',
				value		  : '<?=$type_sms?>',
				listeners     : {
					'select'  : selectVoice
				}
			},{
				xtype      : 'textfield',
				fieldLabel : 'Email',
				name       : 'voice_email',
				id         : 'voice_email',
				width      : '200',
				value      : '<?=addslashes($row['voice_email'])?>',
				allowBlank : false
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Voice Settings</b></span>',
		handler : function(){
			if (voicesettForm.getForm().isValid()) {
				voicesettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/phonesettings.php',
							cache : false
						});
					}
				});
			}
		}
	}/*,{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('PhoneSetting');
		}	
	}*/]
});
var faxsettForm = new Ext.FormPanel({
	title      : 'Fax Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Fax Delivery.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'fax_service',
			id            : 'fax_service',
			hiddenName    : 'hfax_service',
			fieldLabel    : 'Mail Service',
			store         : faxStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_fax?>',
			listeners     : {
				'select'  : selectFax
			}
		},{
            xtype: 'panel',
            layout: 'form',
			//hidden: <?=$type_fax=='0' ? 'false' : 'true';?>,
			id: 'fax_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Sender Domain',
				id         : 'fax_domain',
				name       : 'fax_domain',
				width      : '200',
				value      : '<?=$row['fax_domain']?>',
				allowBlank : false
			},{
				xtype      : 'textfield',
				fieldLabel : 'Receiver Domain',
				id         : 'fax_domain_receiver',
				name       : 'fax_domain_receiver',
				width      : '200',
				value      : '<?=$row['fax_domain_receiver']?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your domain with yourcompany.com)</div>'
			}]
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Fax Settings</b></span>',
		handler : function(){
			if (faxsettForm.getForm().isValid()) {
				faxsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savephonesett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/mycontracts_tabs/phonesettings.php',
							cache : false
						});
					}
				});
			}
		}
	}/*,{
		tooltip: 'View Help',
		cls:'x-btn-text-icon',
		iconAlign: 'left',
		text: ' ',
		width: 30,
		height: 30,
		icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
		scale: 'medium',
		handler: function(){
			//alert(user_loged+"||"+user_block+"||"+user_web);
			if(!user_loged || user_block || user_web){ login_win.show(); return false;}
			showVideoHelp('PhoneSetting');
		}	
	}*/]
});
function selectSms(combo,record,index){
	//var service=Ext.getCmp('delivery_service').getValue();
	Ext.getCmp('sms_domain').setValue(record.get('domain'));
}
function selectFax(combo,record,index){
	//var service=Ext.getCmp('delivery_service').getValue();
	Ext.getCmp('fax_domain').setValue(record.get('domain'));
}
function selectVoice(combo,record,index){
	//var service=Ext.getCmp('delivery_service').getValue();
	Ext.getCmp('voice_email').setValue(record.get('domain'));
}
skypesettForm.render('div_skypesett');
plivosettForm.render('div_plivosett');
//smssettForm.render('div_smssett');
//voicesettForm.render('div_voicesett');
faxsettForm.render('div_faxsett');
</script>