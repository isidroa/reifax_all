<?php
/**
 * contactsettings.php
 *
 * Just a simple form to set the settings for get contacts.
 * 
 * @autor   Guillermo Vera G <guilleverag@gmail.com>
 * @version 25-10-2013
 */

include "../../properties_conexion.php";
conectar();

$userid 	= $_COOKIE['datos_usr']["USERID"];
$block 		= true;
$overwrite 	= false;
$premium 	= true;

$query = 'SELECT * FROM xima.contracts_contactsettings WHERE userid='.$userid;
$result = mysql_query($query) or die($query.mysql_error());
if(mysql_num_rows($result) > 0){
	$r = mysql_fetch_array($result);
	
	$block 		= intval($r['block']) == 0 ? true : false;
	$overwrite 	= intval($r['overwrite']) == 0 ? true : false;
	$premium 	= intval($r['premium']) == 0 ? true : false;
}else{
	$query = 'INSERT INTO xima.contracts_contactsettings (userid) VALUES ('.$userid.')';
	mysql_query($query) or die($query.mysql_error());
}

$que="SELECT contactpremiun FROM xima.ximausrs WHERE userid=".$userid;
$result=mysql_query($que) or die($que.mysql_error());
$r=mysql_fetch_array($result);
$contactpremiun=$r[0];
?>

<div id="div_contact" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">

var contactsettForm = new Ext.FormPanel({
	title      : 'Get Contacts Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Block properties if the contacts are blocked?',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
            xtype: 'radiogroup',
            fieldLabel: '',
            items: [
                {boxLabel: 'Yes', id: 'radioblockY', name: 'rb-block', inputValue: 0 <?php if($block){?>, checked: true<?php }?>},
                {boxLabel: 'No', id: 'radioblockN', name: 'rb-block', inputValue: 1  <?php if(!$block){?>, checked: true<?php }?>}
            ]
        },{
			html        : 'Overwrite existing contacts?',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px',
				marginTop : '10px'
			}
		},{
            xtype: 'radiogroup',
            fieldLabel: '',
            items: [
                {boxLabel: 'Yes', id: 'radiooverwriteY', name: 'rb-overwrite', inputValue: 0  <?php if($overwrite){?>, checked: true<?php }?>},
                {boxLabel: 'No', id: 'radiooverwriteN', name: 'rb-overwrite', inputValue: 1  <?php if(!$overwrite){?>, checked: true<?php }?>}
            ]
        }
		<?php if($contactpremiun == 1){?>
		,{
			html        : 'Get premium contacts?',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px',
				marginTop : '10px'
			}
		},{
            xtype: 'radiogroup',
            fieldLabel: '',
            items: [
                {boxLabel: 'Yes', id: 'radiopremiumY', name: 'rb-premium', inputValue: 0 <?php if($premium){?>, checked: true<?php }?>},
                {boxLabel: 'No', id: 'radiopremiumN', name: 'rb-premium', inputValue: 1  <?php if(!$premium){?>, checked: true<?php }?>}
            ]
        }
		<?php }?>
		,{
			xtype: 'hidden',
			name: 'userid',
			value: <?php echo $userid;?>
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Settings</b></span>',
		handler : function(){
			if (contactsettForm.getForm().isValid()) {
				contactsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savecontactsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	},{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Reset Default Settings</b></span>',
		handler : function(){
			Ext.getCmp('radioblockY').setValue(true);
			Ext.getCmp('radiooverwriteN').setValue(true);
			if(document.getElementById('radiopremiumY')) Ext.getCmp('radiopremiumY').setValue(true);
			
			if (contactsettForm.getForm().isValid()) {
				contactsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savecontactsett.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	}]
});

contactsettForm.render('div_contact');
</script>
