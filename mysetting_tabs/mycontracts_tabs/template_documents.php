<?php
	include "../../properties_conexion.php";
	conectar();
	$userid=$_COOKIE['datos_usr']['USERID']; 
	$query  = "SELECT * FROM xima.templates
				WHERE userid = $userid and template_type=2 order by `default` desc";
	$rs     = mysql_query($query);
	$checkedTemplateid=0;
	while ($rowcr = mysql_fetch_array($rs)) {
		$checkedTemplate = $rowcr['name'];
		$checkedTemplateid = $rowcr['id'];
		
		$comboTemplates .= "['{$rowcr['id']}','{$rowcr['name']}'],";
	}
	
	$checked='';
	
?>
<style type="text/css">
.add-variable {
	background-image: url("http://www.reifax.com/img/add.gif") !important;
}
</style>
<div align="left" id="todo_mytemplate_docs_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="mytemplate_docs_data_div" align="center" style=" background-color:#FFF; margin:auto; width:900px;">
    	<div id="mytemplate_docs_filters"></div><br />
        <div id="mytemplate_docs_properties" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<script>
	var seleccionarTemplate= '<?php echo $checkedTemplate; ?>';
	var seleccionarTemplateId= <?php echo $checkedTemplateid; ?>;
	
	var storetemplates = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			<?php echo $comboTemplates;?>
			['0','New Template']
		]
	});
	var toolbarmydocstemplate=new Ext.Toolbar({
		renderTo: 'mytemplate_docs_filters',
		items: [
			new Ext.Button({
				tooltip: 'New template.',
				id: 'new_templatedoc',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					newTemplateDocs();
				}
			}),new Ext.Button({
				tooltip: 'Delete template.',
				id: 'delete_templatedoc',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'docs',
							modo: 'eliminar',
							id : Ext.getCmp('template_iddoc').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Template Deleted');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_documents.php',
								cache : false
							});
							
						}
					});	
				}
			}),new Ext.Button({
				id: 'default_templatedoc',
				tooltip: 'Set Default',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/arrow_green.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'docs',
							modo: 'setdefault',
							id : Ext.getCmp('template_iddoc').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Operation Completed');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url   : 'mysetting_tabs/mycontracts_tabs/template_documents.php',
								cache : false
							});
							
						}
					});	
				}
			}),{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 260,
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'loadtemplates',
						userid: <?php echo $userid; ?>,
						template_type: 2
					},
					fields:[
						{name:'id', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							Ext.getCmp('combotemplatesdocs').setValue(0); 
						}
					}
				}),
				displayField  : 'name',
				valueField    : 'id',
				name          : 'ftemplate',
				id			  : 'combotemplatesdocs',
				value         : seleccionarTemplate,
				hiddenName    : 'ftemplatedoc11',
				hiddenValue   : seleccionarTemplate,
				allowBlank    : false,
				editable	  : false,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(record.get('id')!=0){
							getTemplateDocs(record.get('id'));
						}else{
							newTemplateDocs();  
						}
					}
				}
			}/*,new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('TemplateDocuments');
				}
			})*/	
		]
	});
	function newTemplateDocs(){
		Ext.getCmp('delete_templatedoc').setVisible(false);
		Ext.getCmp('default_templatedoc').setVisible(false);
		Ext.getCmp('new_templatedoc').setVisible(false);
		
		Ext.getCmp('combotemplatesdocs').setValue(0);
		Ext.getCmp('templatenamedoc').setValue('Template name');
		Ext.getCmp('templatenamedoc').setReadOnly(false);
		Ext.getCmp('templateeditordoc').setValue('');
		Ext.getCmp('template_iddoc').setValue(0);
		Ext.getCmp('defaulttemplatedoc').setValue(0);
		Ext.getCmp('buttontemplatedoc').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Template</b></span>');
	}
	
	function getTemplateDocs(id){
		loading_win.show();
		Ext.getCmp('delete_templatedoc').setVisible(true);
		Ext.getCmp('default_templatedoc').setVisible(true);
		Ext.getCmp('new_templatedoc').setVisible(true);
		Ext.getCmp('templatenamedoc').setReadOnly(true);
		Ext.getCmp('buttontemplatedoc').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>');
		Ext.Ajax.request({
			url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
			method  : 'POST',
			params    : {
				type: 'docs',
				modo: 'obtener',
				id : id,
				'userid': <?php echo $userid;?>
			},
			waitMsg : 'Getting Info',
			success : function(r) {
				loading_win.hide();
				var resp   = Ext.decode(r.responseText);
	
				if (resp.data!=null) {
					Ext.getCmp('templatenamedoc').setValue(resp.data.name);
					Ext.getCmp('templateeditordoc').setValue(resp.data.body);
					Ext.getCmp('template_iddoc').setValue(resp.data.id);	
					Ext.getCmp('defaulttemplatedoc').setValue(resp.data.defaulttemplate);
				} 
			}
		});
	}
	
	
	var variablesStoreDocs=new Ext.data.ArrayStore({
		fields    : ['id', 'texto'],
		data      : [
			['','Select'],
			<?php echo $combo; ?>
		]
	});
	var variableInsertarDoc='<?php echo $checked; ?>';
	
	var template_docs_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		frame      : true,
		method     : 'POST',
		renderTo: 'mytemplate_docs_properties', 
		labelWidth: 60,
		items      : [
			{
				xtype		  : 'textfield',
				fieldLabel	  : 'Name',
				name		  : 'name',
				id		  : 'templatenamedoc',
				readOnly  : true,
				width: 300,
				allowBlank    : false,
				value: 'Template name'
			},{
				xtype         : 'combo',
				fieldLabel	  : 'Default',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 60,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
						['0','No'],
						['1','Yes']
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				name          : 'defaulttemplatedoc',
				hiddenName    : 'defaulttemplatedoc1',
				id			  : 'defaulttemplatedoc',	
				allowBlank    : false,
				readOnly  : true,
				editable	  : false
			},{
				xtype:'htmleditor',
				id: 'templateeditordoc',
				fieldLabel : 'Body',
				autoScroll: true,
				height:450,
				width: 800,
				enableFont:false,
				plugins: [
					new Ext.ux.form.HtmlEditor.Word()
					,new Ext.ux.form.HtmlEditor.HR()						  
				],
				name: 'body',    
				enableSourceEdit : false
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : <?php echo $userid; ?>
			},{
				xtype         : 'hidden',
				name          : 'template_id',
				id          : 'template_iddoc'
			},{
				xtype         : 'hidden',
				name          : 'template_type',
				value         : 2
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>',
			id		: 'buttontemplatedoc',
			handler : function(){
				if (template_docs_form.getForm().isValid()) {
					template_docs_form.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savetemplate.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							if (resp.mensaje == 'Template saved!') {
								updater.update({
									url   : 'mysetting_tabs/mycontracts_tabs/template_documents.php',
									cache : false
								});
							}
						}
					});
				}
			}
		},{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Clear</b></span>',
			handler : function(){
				Ext.getCmp('templateeditordoc').setValue('');
			}
		}]
	});
	
	Ext.getCmp("templateeditordoc").getToolbar().addItem([{
		xtype: 'tbseparator'
	}]);
	
	Ext.getCmp("templateeditordoc").getToolbar().addItem([{
		xtype         : 'combo',
		mode          : 'local',
		fieldLabel    : 'Vars',
		triggerAction : 'all',
		displayField  : 'name',
		valueField    : 'id',
		name          : 'variableInsertarDoc',
		id          : 'variableInsertarDoc',
		value         : variableInsertarDoc,
		allowBlank    : false,
		editable	  : false,
		width: 		    200,
		store         : new Ext.data.JsonStore({
			root:'results',
			totalProperty:'total',
			autoLoad: true, 
			baseParams: {
				type: 'template-variables'
			},
			fields:[
				{name:'id', type:'string'},
				{name:'name', type:'string'}
			],
			url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
			listeners     : {
				'load'  : function(store, records) {
					Ext.getCmp('variableInsertarDoc').setValue(variableInsertarDoc); 
				}
			}
		}),
		listeners	  : {
			'select'  : function(combo,record,index){
				variableInsertarDoc = record.get('id');
			}
		}
	}]);	
	Ext.getCmp("templateeditordoc").getToolbar().addButton([{
		iconCls: 'icon', 
		icon: 'http://www.reifax.com/img/add.gif',
		handler: function () {
			Ext.getCmp("templateeditordoc").insertAtCursor(variableInsertarDoc);
		},
		tooltip: 'Insert Variable'
	}]);
	
	newTemplateDocs();
</script>