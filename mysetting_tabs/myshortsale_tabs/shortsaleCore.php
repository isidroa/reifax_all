<?php
	session_start();
	$bkouserid	=	$_SESSION['bkouserid'];
	include_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/includes/globalFunctions.php");
	if(!mysql_ping())
		$conex_functions=conectar('xima');
	
	function insertlogs($usersource, $usertarget, $operation, $query, $line){
		$query	=	str_replace("'","",$query);
		$query	=	"
			INSERT INTO  xima.logsfull 
				(`usersource`, `usertarget`, `operation`, `query`, `insertdate`, `php`)
			VALUES
				('$usersource','$usertarget','$operation','$query',NOW(),'PHP: $_SERVER[PHP_SELF], LINE: $line')";
		mysql_query($query);
	}
	$userid = isset($_POST['userBackOffice'])	?	$_POST['userBackOffice']	:	$_COOKIE['datos_usr']["USERID"];
	$idfunction=$_POST['idfunction'];
	switch($idfunction){
		//Para Obtener el Nombre del Contrato a Editar
		case "comboProcessorsStore":
			if($_COOKIE['datos_usr']["idusertype"]==52){
				$tableAgent	=	"shortsale_processor_detail";
				$where		=	"a.userid_processor";
			}else{
				$tableAgent	=	"followagent";
				$where		=	"b.userid";
			}
			$query = "
				SELECT * FROM xima.shortsale_processor a
					INNER JOIN xima.$tableAgent b on a.id_processor=b.id_processor
				WHERE $where=$userid
				ORDER BY a.name";
			$result = mysql_query($query);
			while($row=mysql_fetch_assoc($result))
				$rows[]=$row;
			//$rows[]=array('id' => '0', 'name' => 'Upload new contract', 'type' => 'O');
			//echo $array = json_encode($rows);
			echo '{"total": '.count($rows).', "results":'.json_encode($rows).'}';
		break;
		case "searchProcessorInfo":
			$query = "
				SELECT a.*,b.packageName,b.packageFile FROM xima.shortsale_processor_detail a
				INNER JOIN xima.shortsale_processor b ON a.id_processor=b.id_processor
				WHERE userid_processor=$userid";
			$result = mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
			while($row=mysql_fetch_assoc($result)){
				$rows[]=$row;
			}
			echo json_encode(array("success"=>true, "data"=>$rows));
		break;
		case "uploadProcessorPackage":
			$path					=	$_SERVER['DOCUMENT_ROOT']."/shortSale";
			$_POST['packagename']	=	trim($_POST['packagename']);
			$query					=	"SELECT * FROM xima.shortsale_packages WHERE name='$_POST[packagename]' AND id_processor=$_POST[idProcessor]";
			mysql_query($query);
			if(mysql_affected_rows()<=0){
				$filename  = RF_Util::parce_special_chars(str_replace(' ', '_', $_FILES["pdfupload"]['name']));
				$extension = strtolower(strrchr($filename,'.'));
				
				if($extension != '.pdf')
					die(json_encode(array('success'=>'false','mensaje'=>"The uploaded file its not a valid PDF")));
					
				$newFileName = "$userid-".date("YmdHis")."-$filename";
				
				if (!move_uploaded_file($_FILES["pdfupload"]['tmp_name'], $path."/".$newFileName))
					die(json_encode(array('success'=>'false','mensaje'=>"Fallo al copiar. $newFileName")));
				else {
					copy($path."/".$newFileName, $path."/temp/".$newFileName);
					unlink($path."/".$newFileName);
					copy($path."/temp/".$newFileName, $path."/".$newFileName);
					unlink($path."/temp/".$newFileName);				

					if($_SERVER['HTTP_HOST']=='xima3.reifax.com')
						copy($path."/".$newFileName, 'C:/inetpub/wwwroot/xima3/mysetting_tabs/mycontracts_tabs/$userid/'.$newFileName);
					
					$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios
					//$last = system(substr(__FILE__, 0, strlen(__FILE__) - strlen(strrchr(__FILE__,  '\\'))).'\gs5.exe -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=./template_upload/$newFileName ./template_upload/jesus.pdf', $valid);

					system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/$newFileName\"", $valid);
					
					if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						@unlink($path."/".$newFileName);
						rename($path."/temp_by_jesus".$id_name.'.pdf', $path."/".$newFileName);
						//Extract old Package Name to Delete
						$query	=	"SELECT * FROM xima.shortsale_processor WHERE id_processor=$_POST[idProcessor] AND packageName IS NOT NULL";
						$result	=	mysql_query($query);
						if(mysql_affected_rows()>0)
							$oldPackage	=	mysql_fetch_assoc($result);
						
						$query	=	"
							UPDATE xima.shortsale_processor SET
								packageName	=	'$_POST[packagename]',
								packageFile	=	'$newFileName'
							WHERE 
								id_processor=$_POST[idProcessor]";
						if(!mysql_query($query)){
							@unlink($path."/".$newFileName);
							@unlink($path."/temp_by_jesus".$id_name.'.pdf');
							die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
						}
						unlink($path."/".$$oldPackage);
						echo json_encode(array("success"=>true,"mensaje"=>"Package uploaded successfully.","packageName"=>$_POST['packagename']));
					}else{
						die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
						@unlink($path."/".$newFileName);
						@unlink($path."/temp_by_jesus".$id_name.'.pdf');
					}
					@unlink($_FILES["pdfupload"]['tmp_name']);
				}
			}else
				die(json_encode(array("success"=>false,"mensaje"=>"Please try with another package name, $_POST[packagename], already exist for this Processor.")));
		break;
		case "uploadNewPackage":
			$checks			=	json_decode($_POST['checks']);
			$ctrlProcessor	=	($_POST["ctrlProcessor"]==="true"	?	""	:	"$userid/");
			$path			=	$_SERVER['DOCUMENT_ROOT']."/shortSale";
			
			if(!is_dir($path."/$ctrlProcessor"))
				@mkdir($path."/$ctrlProcessor");
				
			$_POST['packagename']	=	trim($_POST['packagename']);
			$query	=	"SELECT * FROM xima.shortsale_packages WHERE name='$_POST[packagename]' AND id_processor=$_POST[idProcessor]";
			mysql_query($query);
			if(mysql_affected_rows()<=0){
				$filename  = RF_Util::parce_special_chars(str_replace(' ', '_', $_FILES["pdfupload"]['name']));
				$extension = strtolower(strrchr($filename,'.'));
				
				if($extension != '.pdf')
					die(json_encode(array('success'=>'false','mensaje'=>"The uploaded file its not a valid PDF")));
					
				$newFileName = "$userid-".date("YmdHis")."-$filename";
				
				if (!move_uploaded_file($_FILES["pdfupload"]['tmp_name'], $path."/$ctrlProcessor".$newFileName))
					die(json_encode(array('success'=>'false','mensaje'=>"Fallo al copiar. $newFileName")));
				else {
					copy($path."/$ctrlProcessor".$newFileName, $path."/temp/".$newFileName);
					unlink($path."/$ctrlProcessor".$newFileName);
					copy($path."/temp/".$newFileName, $path."/$ctrlProcessor".$newFileName);
					unlink($path."/temp/".$newFileName);				

					if($_SERVER['HTTP_HOST']=='xima3.reifax.com')
						copy($path."/$ctrlProcessor".$newFileName, 'C:/inetpub/wwwroot/xima3/mysetting_tabs/mycontracts_tabs/$userid/'.$newFileName);
					
					$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios
					//$last = system(substr(__FILE__, 0, strlen(__FILE__) - strlen(strrchr(__FILE__,  '\\'))).'\gs5.exe -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=./template_upload/$newFileName ./template_upload/jesus.pdf', $valid);

					system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/{$ctrlProcessor}temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/{$ctrlProcessor}$newFileName\"", $valid);
					
					if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						@unlink($path."/$ctrlProcessor".$newFileName);
						rename($path."/{$ctrlProcessor}temp_by_jesus".$id_name.'.pdf', $path."/$ctrlProcessor".$newFileName);
						mysql_query("START TRANSACTION");
						$query	=	"
							INSERT INTO xima.shortsale_packages 
								(id_processor, userid, packageName, fileName) 
							VALUES 
								($_POST[idProcessor],". ($ctrlProcessor===""	?	"null"	:	"$userid") .", '$_POST[packagename]', '$newFileName')";
						if(!mysql_query($query)){
							mysql_query("ROLLBACK");
							@unlink($path."/$ctrlProcessor".$newFileName);
							@unlink($path."/{$ctrlProcessor}temp_by_jesus".$id_name.'.pdf');
							die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
						}
						$idPachage	=	mysql_insert_id();
						foreach($checks as $check){
							$query	=	"
								INSERT INTO xima.shortsale_package_require 
									SELECT 
										null,
										id_require,
										$idPachage,".
										($ctrlProcessor==="" ? "null"	:	"$userid").",
										'".addslashes($check->value)."',
										{$check->required}
									FROM xima.shortsale_require WHERE id_require={$check->id}
							";
							if(!mysql_query($query)){
								mysql_query("ROLLBACK");
								@unlink($path."/$ctrlProcessor".$newFileName);
								@unlink($path."/{$ctrlProcessor}temp_by_jesus".$id_name.'.pdf');
								die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
							}
						}
						mysql_query("COMMIT");
						$usersNames	=	array();
						if($ctrlProcessor===""){	//if is Processor
							$query		=	"SELECT userid FROM xima.shortsale_processor_user WHERE id_processor=$_POST[idProcessor]";
							$result		=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
							$students	=	array();
							$badInserts	=	array();
							while($row=mysql_fetch_assoc($result))
								$students[]	=	$row['userid'];
							
							foreach($students as $student){
								$query	=	"
									INSERT INTO xima.shortsale_package_require 
										SELECT 
											null,
											id_requireTemp,
											$idPachage,
											$student,
											descName,
											`require`
										FROM xima.shortsale_package_require WHERE id_packages=$idPachage AND userid IS NULL
								";
								if(!mysql_query($query)){
									$badInserts[]	=	$student;
								}
							}
							if(count($badInserts)>0){
								$query		=	"SELECT CONCAT(name,' ',surname) as name FROM xima.ximausrs WHERE userid IN (".implode(",",$badInserts).")";
								$result		=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
								while($row=mysql_fetch_assoc($result))
									$usersNames[]=$row['name'];
							}
						}
						if(count($usersNames)<=0){
							$mensaje	=	"Package uploaded successfully.";
						}else{
							$mensaje	=	"Please copy this message and make a ticket with this info: this users ".implode(",",$usersNames)." do not have this new package appropriately configured";
						}
						echo json_encode(array("success"=>true,"mensaje"=>$mensaje));
					}else{
						die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
						@unlink($path."/$ctrlProcessor".$newFileName);
						@unlink($path."/{$ctrlProcessor}temp_by_jesus".$id_name.'.pdf');
					}
					@unlink($_FILES["pdfupload"]['tmp_name']);
				}
			}else
				die(json_encode(array("success"=>false,"mensaje"=>"Please try with another package name, $_POST[packagename], already exist for this Processor.")));
		break;
		case "viewPackageProcessor":
			$query	=	"SELECT * FROM xima.shortsale_processor WHERE id_processor=$_POST[idProcessor] AND packageName IS NOT NULL";
			$result	=	mysql_query($query);
			$result	=	mysql_fetch_assoc($result);
			echo json_encode(array("success"=>true,"package"=>"shortSale/".$result['packageFile']));
		break;
		case "viewPackage":
			$query	=	"SELECT CONCAT('shortSale/',IF(userid IS NULL, fileName, CONCAT('$userid/',fileName))) AS filename FROM xima.shortsale_packages WHERE id_packages=$_POST[idPackage]";
			$result	=	mysql_query($query) or die(json_encode(array("success"=>false,"error"=>mysql_error())));
			$result	=	mysql_fetch_assoc($result);
			echo json_encode(array("success"=>true,"package"=>$result['filename']));
		break;
		case "searchPackages":
			if($_COOKIE['datos_usr']["idusertype"]==52){
				$query	=	"
					SELECT a.* FROM xima.shortsale_packages a
					INNER JOIN xima.shortsale_processor b on a.id_processor=b.id_processor
					WHERE a.id_processor=$_POST[idProcessor] AND a.userid IS NULL AND b.userid_processor=$userid AND b.status=1 AND a.status=1";
			}else{
				$query	=	"
					SELECT * FROM xima.shortsale_packages 
					WHERE id_processor=$_POST[idProcessor] AND (userid IS NULL OR userid=$userid) AND status=1";
			}
			$result	=	mysql_query($query);
			$rows	=	array();
			while($row=mysql_fetch_assoc($result)){
				$row['userid']	=	($_COOKIE['datos_usr']["idusertype"]==52	?	false	:	($userid==$row['userid']	?	false	:	true));
				$rows[]=$row;
			}
			echo json_encode(array("success"=>true,"data"=>$rows));
		break;
		case "searchRequireNew":
			$query	=	"
				SELECT b.id_require,b.displayName,b.descName, 0 AS `require`,c.`name`
				FROM xima.shortsale_require b
					INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
				ORDER BY c.order,b.ordShow";
			$result	=	mysql_query($query);
			while($row=mysql_fetch_assoc($result)){
				$rows[$row['name']][]	=	array(
					"id_require"	=>	$row['id_require'],
					"displayName"	=>	$row['displayName'],
					"descName"		=>	$row['descName'],
					"gridName"		=>	$row['gridName'],
					"require"		=>	$row['require']
				);
			}
			echo json_encode(array("success"=>true,"data"=>$rows));
		break;
		case "searchRequire":
			$query	=	"
				SELECT a.id_require,b.displayName,a.descName,a.`require`,c.`name`
				FROM xima.shortsale_package_require a
					INNER JOIN xima.shortsale_require b ON a.id_requireTemp=b.id_require
					INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
				WHERE a.id_packages=$_POST[idPackage] AND b.`status`=1 AND a.userid ".(($_COOKIE['datos_usr']["idusertype"]==52)	?	"is null"	:	"=$userid")."
				ORDER BY c.order,b.ordShow";
			$result	=	mysql_query($query);
			while($row=mysql_fetch_assoc($result)){
				$rows[$row['name']][]	=	array(
					"id_require"	=>	$row['id_require'],
					"displayName"	=>	$row['displayName'],
					"descName"		=>	$row['descName'],
					"gridName"		=>	$row['gridName'],
					"require"		=>	$row['require']
				);
			}
			echo json_encode(array("success"=>true,"data"=>$rows));
		break;
		case "updateRequiteText":
			$query	=	"
				UPDATE xima.shortsale_package_require SET
					descName	=	'".str_replace("'","\'",$_POST['newValue'])."'
				WHERE id_require=$_POST[idRequire]";
			if(!mysql_query($query))
				die(json_encode(array("success"=>false,"msg"=>"Sorry cant update this value", "sql"=>$query." ==> ".mysql_error())));
			else{
				if($_COOKIE['datos_usr']["idusertype"]==52){
					$query	=	"SELECT id_packages,id_requireTemp FROM xima.shortsale_package_require WHERE id_require=$_POST[idRequire]";
					$result	=	mysql_query($query) or die(json_encode(array("success"=>false,"msg"=>"Error:". __LINE__)));
					$result	=	mysql_fetch_assoc($result);
					echo $query	=	"
						UPDATE xima.shortsale_package_require SET
							descName	=	'".str_replace("'","\'",$_POST['newValue'])."'
						WHERE id_packages=$result[id_packages] AND id_requireTemp=$result[id_requireTemp] AND userid IS NOT NULL";
					if(!mysql_query($query))
						die(json_encode(array("success"=>false,"msg"=>"Sorry cant update this value")));
				}
			}
			echo json_encode(array("success"=>true));
		break;
		case "updateRequiteValue":
			$query	=	"
				UPDATE xima.shortsale_package_require SET
					`require`	=	$_POST[newValue]
				WHERE id_require=$_POST[idRequire]";
			if(!mysql_query($query))
				die(json_encode(array("success"=>false,"msg"=>"Sorry cant update this value", "sql"=>$query." ==> ".mysql_error())));
			else{
				if($_COOKIE['datos_usr']["idusertype"]==52){
					$query	=	"SELECT id_packages,id_requireTemp FROM xima.shortsale_package_require WHERE id_require=$_POST[idRequire]";
					$result	=	mysql_query($query) or die(json_encode(array("success"=>false,"msg"=>"Error:". __LINE__)));
					$result	=	mysql_fetch_assoc($result);
					$query	=	"
						UPDATE xima.shortsale_package_require SET
							`require`	=	$_POST[newValue]
						WHERE id_packages=$result[id_packages] AND id_requireTemp=$result[id_requireTemp] AND userid IS NOT NULL";
					if(!mysql_query($query))
						die(json_encode(array("success"=>false,"msg"=>"Sorry cant update this value","error"=>$query." ===> ".mysql_error())));
				}
			}
			echo json_encode(array("success"=>true));
		break;
		case "deletePachage":
			$query	=	"
				UPDATE xima.shortsale_packages SET
					status=0
				WHERE id_packages=$_POST[idPackage]";
			if(!mysql_query($query))
				die(json_encode(array("success"=>false,"msg"=>"Sorry can't delete this package", "sql"=>$query." ==> ".mysql_error())));
			echo json_encode(array("success"=>true,"msg"=>"Package deleted successfully."));
		break;
		case "updateProcessorInfo":
			$_POST['agent']		=	addslashes($_POST['agent']);
			$_POST['company']	=	addslashes($_POST['company']);
			if($_POST['ctrlProcessor']==="true"){
				$table	=	"shortsale_processor_detail";
				$where	=	"id_processor=$_POST[idProcessor]";
			}else{
				$table	=	"followagent";
				$where	=	"userid=$userid AND agentid=$_POST[agentid]";
			}
			$query	=	"
				UPDATE xima.$table SET 
					agent		=	'$_POST[agent]',
					email		=	'$_POST[email]',
					tollfree	=	'$_POST[tollfree]',
					phone1		=	'$_POST[phone1]', 
					typeph1		=	$_POST[typeph1], 
					phone2		=	'$_POST[phone2]', 
					typeph2		=	$_POST[typeph2], 
					phone3		=	'$_POST[phone3]', 
					typeph3		=	$_POST[typeph3], 
					fax			=	'$_POST[fax]',
					urlsend		=	'$_POST[urlsend]',  
					typeph4		=	$_POST[typeph4],  
					typeph5		=	$_POST[typeph5],  
					typeph6		=	$_POST[typeph6],  
					phone6		=	'$_POST[phone6]', 
					email2		=	'$_POST[email2]',
					typeemail1	=	$_POST[typeemail1], 
					typeemail2	=	$_POST[typeemail2], 
					typeurl1	=	$_POST[typeurl1], 
					urlsend2	=	'$_POST[urlsend2]', 
					typeurl2	=	$_POST[typeurl2], 
					company		=	'$_POST[company]',
					address1	=	'$_POST[address1]', 
					address2	=	'$_POST[address2]', 
					typeaddress1=	$_POST[typeaddress1], 
					typeaddress2=	$_POST[typeaddress2], 
					agenttype	=	$_POST[agenttype] 
			WHERE $where";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
			echo json_encode(array("success"=>true,"mensaje"=>"Info updated Successfully"));
		break;
		case "searchShortSales":
			$query	=	"
				SELECT c.userid, CONCAT(c.`name`,' ',c.surname) AS `name`, a.id_processor, b.id_user FROM xima.shortsale_processor a 
					INNER JOIN xima.shortsale_processor_user b ON a.id_processor=b.id_processor 
					INNER JOIN xima.ximausrs c ON b.userid=c.userid
				WHERE a.userid_processor=$_POST[useridProcessor]";
			$result	=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
			while($row=mysql_fetch_assoc($result))
				$rows[]=$row;
			echo json_encode(array("total"=>count($rows), "results"=>$rows));
		break;
		case "assignShortSalesByUserid":
			$users		=	explode(",",$_POST["users"]);
			$badInserts	=	array();
			$exists		=	array();
			$errors		=	array();
			$usersNames	=	array();
			$existNames	=	array();
			foreach($users as $useridNew){
				$validCommit=	false;
				$query		=	"SELECT * FROM xima.shortsale_processor_user WHERE id_processor=$_POST[idProcessor] AND userid=$useridNew";
				$result		=	mysql_query($query);
				if(mysql_affected_rows()>0){
					$exists[]	=	$useridNew;
				}else{
					mysql_query("START TRANSACTION");
					$query	=	"INSERT INTO xima.shortsale_processor_user VALUES (null,$_POST[idProcessor],$useridNew)";
					if(!mysql_query($query)){
						$badInserts[]	=	$useridNew;
						$validCommit	=	true;
						mysql_query("ROLLBACK");
					}else{
						$query		=	"
							INSERT INTO xima.followagent 
								SELECT 
									null,$useridNew,agent,agenttype,
									email,typeemail1,email2,typeemail2,email3,typeemail3,email4,typeemail4,email5,typeemail5,
									tollfree,
									phone1,typeph1,phone2,typeph2,phone3,typeph3,phone4,typeph4,phone5,typeph5,phone6,typeph6,
									fax,urlsend,typeurl1,urlsend2,typeurl2,company,address1,typeaddress1,address2,typeaddress2,agentblock,$_POST[idProcessor]
								FROM xima.shortsale_processor_detail WHERE id_processor=$_POST[idProcessor]";
						if(!mysql_query($query)){
							$badInserts[]	=	$useridNew;
							$errors[]		=	array("Userid"=>$useridNew,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
							$validCommit	=	true;
							mysql_query("ROLLBACK");
						}else{
							$query		=	"SELECT * FROM xima.shortsale_packages WHERE id_processor=$_POST[idProcessor] AND userid IS NULL";
							if(!$result	=	mysql_query($query)){
								$badInserts[]	=	$useridNew;
								$errors[]		=	array("Userid"=>$useridNew,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
								$validCommit	=	true;
								mysql_query("ROLLBACK");
							}else{
								$packages	=	array();
								while($row=mysql_fetch_assoc($result))
									$packages[]	=	$row['id_packages'];
								
								foreach($packages as $package){
									$query	=	"
										INSERT INTO xima.shortsale_package_require 
											SELECT 
												null,
												id_requireTemp,
												$package,
												$useridNew,
												descName,
												`require`
											FROM xima.shortsale_package_require WHERE id_packages=$package AND userid IS NULL
									";
									if(!mysql_query($query)){
										$badInserts[]	=	$useridNew;
										$errors[]		=	array("Userid"=>$useridNew,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
										$validCommit	=	true;
										mysql_query("ROLLBACK");
									}else{
										$idPackage	=	mysql_insert_id();
										$query		=	"
											INSERT INTO xima.contracts_campos
												SELECT 
													null,$useridNew,'SS-$useridNew-$idPackage',camp_page,camp_type,camp_idtc,camp_text,camp_posx,camp_posy
												FROM xima.contracts_campos WHERE camp_contract_id='SS-$package'";
										if(!mysql_query($query)){
											$badInserts[]	=	$useridNew;
											$errors[]		=	array("Userid"=>$useridNew,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
											$validCommit	=	true;
											mysql_query("ROLLBACK");
										}
									}
								}
							}
						}
					}
					if($validCommit===false){
						mysql_query("COMMIT");
						insertlogs($bkouserid,$useridNew,"Assignment to Processor ID:$_POST[idProcessor]",'',__LINE__);
					}
				}
			}
			if(count($badInserts)>0){
				$query		=	"SELECT CONCAT(name,' ',surname) as name FROM xima.ximausrs WHERE userid IN (".implode(",",$badInserts).")";
				$result		=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
				while($row=mysql_fetch_assoc($result))
					$usersNames[]=$row['name'];
			}
			if(count($usersNames)<=0)
				$mensaje	=	"Processor assigned successfully.";
			else
				$mensaje	=	"Please copy this message and make a ticket with this info, this users:<br>".implode(",",$usersNames)."<br>do not have a processor appropriately configured";
			
			if(count($exists)>0){
				$query		=	"SELECT CONCAT(name,' ',surname) as name FROM xima.ximausrs WHERE userid IN (".implode(",",$exists).")";
				$result		=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
				while($row=mysql_fetch_assoc($result))
					$existNames[]=$row['name'];
			}
			if(count($existNames)<=0)
				$mensaje2	=	"";
			else
				$mensaje2	=	"<br>this users:<br>".implode(",",$existNames)."<br>already exists a processor appropriately configured.";
			echo json_encode(array("success"=>true,"mensaje"=>$mensaje.$mensaje2,"errors"=>$errors));
		break;
		case "deleteShortSale":
			$users		=	explode(",",$_POST['users']);
			$badInserts	=	array();
			$errors		=	array();
			foreach($users	as $userToDelete){
				$packagesToDelete	=	array();
				$filesToDelete		=	array();
				$validCommit		=	false;
				$query	=	"SELECT CONCAT('\'SS-',userid,'-',id_packages,'\'') as id_packages, fileName FROM xima.shortsale_packages WHERE id_processor=$_POST[idProcessor] AND userid=$userToDelete";
				$result	=	mysql_query($query);
				while($row	=	mysql_fetch_assoc($result)){
					$packagesToDelete[]	=	$row['id_packages'];
					$filesToDelete[]	=	$row['fileName'];
				}

				mysql_query("START TRANSACTION");
				$query	=	"DELETE FROM xima.shortsale_processor_user WHERE userid=$userToDelete AND id_processor=$_POST[idProcessor]";
				if(!mysql_query($query)){
					$badInserts[]	=	$userToDelete;
					$errors[]		=	array("Userid"=>$userToDelete,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
					$validCommit	=	true;
					mysql_query("ROLLBACK");
				}else{
					if(count($packagesToDelete)>0){
						$query	=	"DELETE FROM xima.shortsale_packages WHERE id_processor=$_POST[idProcessor] AND userid=$userToDelete";
						if(!mysql_query($query)){
							$badInserts[]	=	$userToDelete;
							$errors[]		=	array("Userid"=>$userToDelete,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
							$validCommit	=	true;
							mysql_query("ROLLBACK");
						}else{
							$query	=	"DELETE FROM xima.contracts_campos WHERE camp_contract_id IN (".implode(",",$packagesToDelete).")";
							if(!mysql_query($query)){
								$badInserts[]	=	$userToDelete;
								$errors[]		=	array("Userid"=>$userToDelete,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
								$validCommit	=	true;
								mysql_query("ROLLBACK");
							}else{
								$query	=	"DELETE FROM xima.shortsale_followup WHERE id_processor=$_POST[idProcessor] AND userid=$userToDelete";
								if(!mysql_query($query)){
									$badInserts[]	=	$userToDelete;
									$errors[]		=	array("Userid"=>$userToDelete,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error());
									$validCommit	=	true;
									mysql_query("ROLLBACK");
								}
							}
						}
					}
				}
				if($validCommit===false){
					mysql_query("COMMIT");
					insertlogs($bkouserid,$userToDelete,"Remove from Processor ID:$_POST[idProcessor]",'',__LINE__);
					if(count($packagesToDelete)>0){
						foreach($filesToDelete as $fileName){
							@unlink($_SERVER["DOCUMENT_ROOT"]."/shortSale/$userToDelete/".$fileName);
						}
					}
				}
			}
			if(count($badInserts)>0){
				$query		=	"SELECT CONCAT(name,' ',surname) as name FROM xima.ximausrs WHERE userid IN (".implode(",",$badInserts).")";
				$result		=	mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__ ,"error"=>$query." -- ".mysql_error())));
				while($row=mysql_fetch_assoc($result))
					$usersNames[]=$row['name'];
			}
			if(count($usersNames)<=0)
				$mensaje	=	"User's removed successfully.";
			else
				$mensaje	=	"Please copy this message and make a ticket with this info, this users:<br>".implode(",",$usersNames)."<br>do not was deleted appropriately";
			
			echo json_encode(array("success"=>true,"mensaje"=>$mensaje));
		break;
	}
?>