<?php
/**
 * myshortsale.php
 * 
 * @autor   Jesus Marquez  <?@?.com>             Original Code
 * @version 19.12.2013
 */

include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];

$query    = "SELECT professional FROM xima.permission WHERE userid='$userid'";
$rs       = mysql_query($query);
$rp       = mysql_fetch_array($rs);
$platinum = $rp[0];
//$usrstate = explode(',',$rp[1]);

$query     = 'SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result    = mysql_query($query) or die("Error: ". __LINE__ .$query.mysql_error());
$datos_usr = mysql_fetch_array($result);

?>
<style type="text/css">
.x-form-file-wrap {
	position     : relative;
	height       : 22px;
	width        : 210px;
}
.x-form-file-wrap .x-form-file {
	position     : absolute;
	right        : 0;
	-moz-opacity : 0;
	filter       : alpha(opacity: 0);
	opacity      : 0;
	z-index      : 2;
	height       : 22px;
}
.x-form-file-wrap .x-form-file-btn {
	position     : absolute;
	right        : 0;
	z-index      : 1;
}
.x-form-file-wrap .x-form-file-text {
	position     : absolute;
	left         : 0;
	z-index      : 3;
	color        : #777;
}
.formulario {
	padding-top  : 2px; 
	color        : #274F7B; 
	font-weight  : bold; 
	font-size    : 14px;
}
</style>

<div id="div_formularioShortSale" class="formulario"></div>

<div id="div_formularioProcessorPackage" class="formulario"></div>

<div id="div_shortSalePackages" class="formulario"></div>

<div id="div_shortSaleRequire" class="formulario"></div>

<script type="text/javascript">

var ctrlProcessor	=	(Ext.util.Cookies.get('datos_usr[idusertype]')==52	?	true	:	false);
console.debug(1,ctrlProcessor);
// ----------------------------------------------------

function modifyShortSale(typecontract,idcontract,userid){
	//Ext.MessageBox.alert('',idcontract);
	new Ext.util.Cookies.set("idmodifycontract", idcontract);		//Id del Paquete
	new Ext.util.Cookies.set("typemodifycontract", typecontract);	//Tipo de Contrato
	new Ext.util.Cookies.set("placemodifycontract", userid);	//Userid para identificarlos cuando sean processor o alumno
	
	if(document.getElementById('idmodifycontract')){
		var tab = tabs.getItem('idmodifycontract');
		tabs.remove(tab);
	}
	Ext.getCmp('principal').add({
		//xtype:	'component', 
		title:		'Modify Package', 
		closable:	true,
		autoScroll:	true,
		id:			'idmodifycontract', 
		autoLoad:	{url:'/custom_contract/manager.php',scripts:true}
	}).show();
}

var processorStore = new Ext.data.JsonStore({
	url:		'/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
	method:		'post',
	totalProperty:	'total',
	root:		'results',
	baseParams:{
		idfunction:	"comboProcessorsStore"
	},
	fields: [
		{name:'id_processor', type:'string'},
		{name:'name', type:'string'},
		{name:'packageName', type:'string'},
		{name:'type', type:'string'},
		{name:'agent', type:'string'},
		{name:'agenttype', type:'string'},
		{name:'company', type:'string'},
		{name:'typeemail1', type:'string'},
		{name:'typeemail2', type:'string'},
		{name:'email', type:'string'},
		{name:'email2', type:'string'},
		{name:'typeurl1', type:'string'},
		{name:'urlsend', type:'string'},
		{name:'typeurl2', type:'string'},
		{name:'urlsend2', type:'string'},
		{name:'typeph1', type:'string'},
		{name:'phone1', type:'string'},
		{name:'typeph2', type:'string'},
		{name:'phone2', type:'string'},
		{name:'typeph3', type:'string'},
		{name:'phone3', type:'string'},
		{name:'typeph4', type:'string'},
		{name:'phone4', type:'string'},
		{name:'typeph5', type:'string'},
		{name:'phone5', type:'string'},
		{name:'typeph6', type:'string'},
		{name:'phone6', type:'string'},
		{name:'typeaddress1', type:'string'},
		{name:'address1', type:'string'},
		{name:'typeaddress2', type:'string'},
		{name:'address2', type:'string'},
		{name:'type', type:'string'},
		{name:'agentid', type:'string'}
	],
	autoLoad: true
});
function showData(record) {
	Ext.getCmp('panelProcessorEdit').getForm().findField('agent').setValue(record.agent);
	Ext.getCmp('panelProcessorEdit').getForm().findField('agenttype').setValue(record.agenttype);
	Ext.getCmp('panelProcessorEdit').getForm().findField('company').setValue(record.company);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeemail1').setValue(record.typeemail1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeemail2').setValue(record.typeemail2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('email').setValue(record.email);
	Ext.getCmp('panelProcessorEdit').getForm().findField('email2').setValue(record.email2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeurl1').setValue(record.typeurl1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('urlsend').setValue(record.urlsend);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeurl2').setValue(record.typeurl2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('urlsend2').setValue(record.urlsend2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph1').setValue(record.typeph1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone1').setValue(record.phone1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph2').setValue(record.typeph2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone2').setValue(record.phone2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph3').setValue(record.typeph3);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone3').setValue(record.phone3);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph4').setValue(record.typeph4);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone4').setValue(record.phone4);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph5').setValue(record.typeph5);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone5').setValue(record.phone5);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeph6').setValue(record.typeph6);
	Ext.getCmp('panelProcessorEdit').getForm().findField('phone6').setValue(record.phone6);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeaddress1').setValue(record.typeaddress1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('address1').setValue(record.address1);
	Ext.getCmp('panelProcessorEdit').getForm().findField('typeaddress2').setValue(record.typeaddress2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('address2').setValue(record.address2);
	Ext.getCmp('panelProcessorEdit').getForm().findField('agentid').setValue(record.agentid);
	/*if(record.packageName){
		Ext.getCmp('processorPackageT').update('<span style=\'font-size: 15px;\'><b>'+record.packageName+'</b></span>');
		Ext.getCmp('processorPackageVD').show();
	}else{
		Ext.getCmp('processorPackageT').update('This Negotiator dosent have Package Uploaded');
		Ext.getCmp('processorPackageVD').hide();
	}*/
}
// ----------------------------------------------------------------

var formulario0 = new Ext.Panel({
	title      : 'Negotiator Name',
	border     : true,
	bodyStyle  : 'padding: 10px;text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [{
		xtype		:	'panel',
		layout		:	'table',
		hidden		:	ctrlProcessor,
		layoutConfig:	{ columns : 4 },
		rowspan		:	3,
		items		:	[{
			xtype			:	'combo',
			name			:	'cprocessor1',
			id				:	'cprocessor1',
			hiddenName		:	'type',
			fieldLabel		:	'<b>Contract Name</b>', 
			typeAhead		:	true,
			store			:	processorStore,
			triggerAction	:	'all',
			mode			:	'local',
			editable		:	false,
			autoLoad		:	true,
			emptyText		:	'Select a Negotiator...',
			selectOnFocus	:	true,
			allowBlank		:	false,
			displayField	:	'name',
			valueField		:	'id_processor',
			//value         : '0',
			width			:	300,
			listeners		:	{
				'select'  : function(combo, record, index) {
					if(Ext.getCmp('buttonUploadNewPackage').hidden==true){
						Ext.getCmp('buttonUploadNewPackage').show();
						packageForm.doLayout();
					}
					Ext.getCmp('renderShortSalePackage').items.each(function(item){
						item.destroy();
					});
					Ext.getCmp('renderShortSaleRequire').items.each(function(item){
						item.destroy();
					});
					showData(record.data);
					Ext.getCmp('cprocessor2').setValue(Ext.getCmp('cprocessor1').getValue());
					searchPackages();
				}
			}
		},{
			xtype		:	'button',
			scale		:	'medium',
			icon		:	'/img/add_doc.png',
			tooltip		:	'Add new Negotiator',
			listeners	:	{
				click	:	function(){
					Ext.Msg.show({
					   title      : 'Info!',
					   msg        : 'If you want to create a negotiator, Please Contact us. Thank you.',
					   buttons    : Ext.MessageBox.OK,
					   icon       : Ext.MessageBox.INFO
					});
				}
			}
		},{
			xtype    : 'button',
			tooltip: 'View Help',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
			style    : {
				marginRight : 10
			},
			handler: function(){
				//alert(user_loged+"||"+user_block+"||"+user_web);
				if(!user_loged || user_block || user_web){ login_win.show(); return false;}
				/*var win = new Ext.Window({
					layout      : 'fit',
					title		: 'Video Help',
					width       : 1010,
					height      : 590,
					modal	 	: true,
					plain       : true,
					autoScroll:false,
					autoLoad:{ url:'training/videoHelpView.php', scripts: true, params: {source:'Mycontracts'} }
				});
				win.show();*/
				showVideoHelp('Mycontracts');
			}
		}]
	},{
		xtype         : 'form',
		id            : 'panelProcessorEdit',
		items         : [{
			xtype        :'spacer',
			height       : 10
		},{
			xtype          : 'fieldset',
			checkboxToggle : false,
			title          : 'Negotiator Info',
			id             : 'idtemplate1',
			border         : 'false',
			layout         : 'column',
			//rowspan        : 2,
			items          : [{
				layout     	: 'form',
				columnWidth	: .50,
				items		: [{
					xtype     	: 'textfield',
					name      	: 'agent',
					width		: 290,
					fieldLabel	: 'Contact',
					allowBlank	: false
				}]
			},{
				layout     	: 'form',
				items		: [{
					xtype         : 'combo',
					columnWidth	  : .50,
					mode          : 'local',
					fieldLabel    : 'Type',
					triggerAction : 'all',
					width		  : 290,
					store         : new Ext.data.JsonStore({
						id:'storetype',
						root:'results',
						totalProperty:'total',
						baseParams: {
							type: 'agenttype',
							'userid': <?php echo $userid;?>
						},
						fields:[
							{name:'idtype', type:'string'},
							{name:'name', type:'string'}
						],
						url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						autoLoad: true
					}),
					displayField  : 'name',
					valueField    : 'idtype',
					name          : 'fagenttype',
					value         : 'Agent',
					hiddenName    : 'agenttype',
					hiddenValue   : '1',
					allowBlank    : false,
					listeners	  : {
						beforequery: function(qe){
							delete qe.combo.lastQuery;
						}
					}
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Email',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyemail1',
						value         : '0',
						hiddenName    : 'typeemail1',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'email',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Email 2',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyemail2',
						value         : '0',
						hiddenName    : 'typeemail2',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'email2',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype     : 'textfield',
					width	  : 290,
					name      : 'company',
					fieldLabel: 'Company'
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Website 1',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Personal'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyurl1',
						value         : '0',
						hiddenName    : 'typeurl1',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'urlsend',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Website 2',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Personal'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyurl2',
						value         : '0',
						hiddenName    : 'typeurl2',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'urlsend2',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname1',
						value         : '0',
						hiddenName    : 'typeph1',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone1',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone 2',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname2',
						value         : '0',

						hiddenName    : 'typeph2',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone2',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone 3',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname3',
						value         : '0',
						hiddenName    : 'typeph3',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone3',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone 4',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname4',
						value         : '0',
						hiddenName    : 'typeph4',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone4',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone 5',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname5',
						value         : '0',
						hiddenName    : 'typeph5',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone5',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Phone 6',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office'],
								['2','Cell'],
								['3','Home Fax'],
								['6','Office Fax'],
								['4','TollFree'],
								['5','O. TollFree']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'typephname6',
						value         : '0',
						hiddenName    : 'typeph6',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'phone6',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Address 1',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyaddress1',
						value         : '0',
						hiddenName    : 'typeaddress1',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'address1',
						width	  : 165
					}]
				}]
			},{
				layout     	: 'form',
				columnWidth	  : .50,
				items		: [{
					xtype	  : 'compositefield',
					fieldLabel: 'Address 2',
					items	  : [{
						xtype         : 'combo',
						mode          : 'local',
						triggerAction : 'all',
						width		  : 120,
						editable	  : false,
						store         : new Ext.data.ArrayStore({
							id        : 0,
							fields    : ['valor', 'texto'],
							data      : [
								['0','Home'],
								['1','Office']
							]
						}),
						displayField  : 'texto',
						valueField    : 'valor',
						name          : 'tyaddress2',
						value         : '0',
						hiddenName    : 'typeaddress2',
						hiddenValue   : '0',
						allowBlank    : false
					},{
						xtype     : 'textfield',
						name      : 'address2',
						width	  : 165
					}]
				}]
			},{
				xtype     : 'hidden',
				columnWidth	  : .50,
				name      : 'agentid'
			}]
		}]
	}],
	listeners	:	{
		afterrender	:	function(){
			if(ctrlProcessor){
				setTimeout(function(){
					Ext.Ajax.request({
						url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
						method  : 'POST',
						waitMsg : 'Getting Info',
						success : function(r,o) {
							var resp	=	$.parseJSON(r.responseText);
							showData(resp.data[0]);
							searchPackages(resp.data[0].id_processor);
							Ext.getCmp('cprocessor2').setValue(resp.data[0].id_processor);
							Ext.getCmp('cprocessor1').setValue(resp.data[0].id_processor);
							/*var temp	=	resp.data[0].packageName;
							if(temp){
								if(temp.trim().length>0){
									Ext.getCmp('processorPackageT').update('<span style=\'font-size: 15px;\'><b>'+temp+'</b></span>');
									Ext.getCmp('processorPackageVD').show();
								}
							}*/
						},
						params	:	{
							idfunction	:	'searchProcessorInfo'
						}
					});
				},300);
			}
		}
	},
	buttonAlign : 'center',
	buttons     : [{
		text	: '<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save</b></span>',
		handler	: function() {
			if(Ext.getCmp("panelProcessorEdit").getForm().isValid()) { 
				Ext.getCmp("panelProcessorEdit").getForm().submit({
					url     : '/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp    = a.result;
						/*var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/myshortsale_tabs/myshortsale.php',
							cache : false
						});*/
						processorStore.reload();
						Ext.Msg.show({
						   title      : 'Info',
						   msg        : resp.mensaje,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.INFO
						});
					},
					failure : function(response, o){ 
						var resp    = o.result;
						Ext.Msg.show({
						   title      : 'Info',
						   msg        : resp.mensaje,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.WARNING
						});
					},
					params	:	{
						idfunction		:	"updateProcessorInfo",
						ctrlProcessor	:	ctrlProcessor,
						idProcessor		:	Ext.getCmp('cprocessor1').getValue()
					}
				});
			}
		}
	}]
});
formulario0.render('div_formularioShortSale');
/*var formulario1 = new Ext.Panel({
	border     : true,
	bodyStyle  : 'padding: 10px;text-align:left;',
	frame      : true,
	items:[{
		xtype			:	'fieldset',
		id				:	'processorPackageF',
		checkboxToggle	:	false,
		title			:	'Negotiator Package',
		border			:	'false',
		items			:	[{
			xtype       :	'form', 
			fileUpload	:	true,
			id          :	'panelProcessorPackageUpload',
			hidden		:	true,
			layout      :	'column',
			items		:	[{
				layout: 'form',
				columnWidth	  : .50,
				labelWidth	: 90,
				items:[{					
					xtype      : 'textfield',
					id         : 'processorPdfName',
					fieldLabel : 'Package Name',
					name       : 'packagename',
					allowBlank : false,
					width      : 250
				}]
			},{
				layout: 'form',
				columnWidth	  : .50,
				labelWidth	: 70,
				items:[{											
					xtype       : 'fileuploadfield',
					id          : 'processorIdUpload',
					emptyText   : 'Select pdf',
					fieldLabel  : 'Upload File',
					name        : 'pdfupload',
					allowBlank	: false,
					width		: 250,
					buttonText  : 'Browse...'
				}]
			}],
			listeners	:	{
				show	:	function(){
					Ext.get('processorIdUpload').setWidth(150);
				}
			},
			buttonAlign	:	'center',
			buttons		:	[{
				text	:	'Upload',
				listeners	:	{
					click	:	function(){
						if(Ext.getCmp("panelProcessorPackageUpload").getForm().isValid()) { 
							Ext.getCmp("panelProcessorPackageUpload").getForm().submit({
								url     : '/mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
								waitMsg : 'Saving...',
								success : function(f, a){ 
									var resp    = a.result;
									Ext.Msg.show({
									   title      : 'Info',
									   msg        : resp.mensaje,
									   buttons    : Ext.MessageBox.OK,
									   icon       : Ext.MessageBox.INFO
									});
									Ext.getCmp('processorPackageT').update('<span style=\'font-size: 15px;\'><b>'+resp.packageName+'</b></span>');
									Ext.getCmp('panelProcessorPackageUpload').hide();
									Ext.getCmp('processorPackageInfo').show();
								},
								failure : function(response, o){ 
									var resp    = o.result;
									Ext.Msg.show({
									   title      : 'Info',
									   msg        : resp.mensaje,
									   buttons    : Ext.MessageBox.OK,
									   icon       : Ext.MessageBox.WARNING
									});
								},
								params	:	{
									idfunction		:	"uploadProcessorPackage",
									idProcessor		:	Ext.getCmp('cprocessor1').getValue()
								}
							});
						}
					}
				}
			},{
				text	:	'Cancel',
				listeners	:	{
					click	:	function(){
						Ext.getCmp('panelProcessorPackageUpload').hide();
						Ext.getCmp('processorPackageInfo').show();
					}
				}
			}]
		},{
			layout	:	'column',
			id		:	'processorPackageInfo',
			items	:	[{
				xtype		:	'box',
				id			:	'processorPackageT',
				html		:	'This Negotiator dosent have Package Uploaded',
				columnWidth	:	.75,
				cls			:	'x-form-item'
			},{
				xtype		:	'button',
				id			:	'processorPackageU',
				columnWidth	:	.10,
				hidden		:	!ctrlProcessor,
				text		:	'Upload',
				listeners	:	{
					click	:	function(){
						Ext.getCmp("panelProcessorPackageUpload").show();
						Ext.getCmp("processorPackageInfo").hide();
					}
				}
			},{
				xtype		:	'button',
				hidden		:	true,
				id			:	'processorPackageVD',
				columnWidth	:	.15,
				text		:	'View/Download',
				listeners	:	{
					click	:	function(){
						Ext.Ajax.request({
							url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
							method  : 'POST',
							waitMsg : 'Getting Info',
							success : function(r,o) {
								var resp	=	$.parseJSON(r.responseText);
								var Digital	=	new Date()
								var hours	=	Digital.getHours()
								var minutes	=	Digital.getMinutes()
								var seconds	=	Digital.getSeconds()
								var url 	=	'http://www.reifax.com/'+resp.package;
								window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
							},
							params	:	{
								idfunction	:	'viewPackageProcessor',
								idProcessor	:	Ext.getCmp('cprocessor1').getValue()
							}
						});
					}
				}
			}]
		},{
			layout     	: 'form',
			items		: [{}]
		}]
	}]
});
formulario1.render('div_formularioProcessorPackage');*/

var packageForm = new Ext.FormPanel({
	title      : 'Short Sale Package',
	border     : false,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [{
		//xtype	:	'compositefield',		
		layout	:	'table',
		layoutConfig : {
			columns  : 2
		},		
		items	:	[{
			xtype	:	'panel',
			items	:	[{
				xtype			:	'combo',
				name			:	'cprocessor2',
				id				:	'cprocessor2',
				hiddenName		:	'idProcessor',
				hidden			:	ctrlProcessor,
				//hideLabel		:	ctrlProcessor,
				typeAhead		:	true,
				store			:	processorStore,
				fieldLabel		:	'<b>Negotiator</b>',  
				triggerAction	:	'all',
				mode			:	'local',
				editable		:	false,
				emptyText		:	'Select a Negotiator...',
				selectOnFocus	:	true,
				allowBlank		:	ctrlProcessor,
				displayField	:	'name',
				valueField		:	'id_processor',
				//value         : 0,
				width			:	300,
				listeners		:	{
					'select'  :	function(combo, record, index) {
						if(Ext.getCmp('buttonUploadNewPackage').hidden==true){
							Ext.getCmp('buttonUploadNewPackage').show();
							packageForm.doLayout();
						}
						Ext.getCmp('renderShortSalePackage').items.each(function(item){
							item.destroy();
						});
						Ext.getCmp('renderShortSaleRequire').items.each(function(item){
							item.destroy();
						});
						//showData(Ext.getCmp('cprocessor1').findRecord(Ext.getCmp('cprocessor1').valueField || Ext.getCmp('cprocessor1').displayField, Ext.getCmp('cprocessor1').getValue())); //get record from combo
						showData(record.data);
						Ext.getCmp('cprocessor1').setValue(Ext.getCmp('cprocessor2').getValue());
						searchPackages();
					}
				}
			}]
		},{
			xtype		:	'button',
			icon		:	'/img/add_doc.png',
			id			:	'buttonUploadNewPackage',
			tooltip		:	'Add Package',
			scale		:	'medium',
			hidden		:	!ctrlProcessor,
			listeners	:	{
				click	:	function(){
					loading_win.show();
					Ext.getCmp('renderShortSaleRequire').items.each(function(item){
						item.destroy();
					});
					Ext.getCmp('renderShortSalePackage').hide();
					Ext.getCmp('panelShortSalePackageUpload').show();
					Ext.getCmp('packageInsert').show();
					Ext.getCmp('packageCancel').show();
					searchRequire('','searchRequireNew');
					loading_win.show();
				}
			}
		}]
	},{
		xtype       :	'panel', 
		id          :	'panelShortSalePackageUpload',
		hidden		:	true,
		layout      :	'column',
		items		:	[{
			layout: 'form',
			columnWidth	  : .50,
			labelWidth	: 90,
			items:[{					
				xtype      : 'textfield',
				id         : 'pdfname',
				fieldLabel : 'Package Name',
				name       : 'packagename',
				allowBlank : false,
				width      : 250
			}]
		},{
			layout: 'form',
			columnWidth	  : .50,
			labelWidth	: 70,
			items:[{											
				xtype       : 'fileuploadfield',
				id          : 'idupload',
				emptyText   : 'Select pdf',
				fieldLabel  : 'Upload File',
				name        : 'pdfupload',
				allowBlank	: false,
				width		: 250,
				buttonText  : 'Browse...'
			}]
		}],
		listeners:	{
			show	:	function(){
				Ext.get('idupload').setWidth(150);
			}
		}
	},{
		xtype        : 'panel',
		id			 : 'renderShortSalePackage',
		border       : true,
		bodyStyle    : 'padding-left: 20px;',
		//width        : 800,
		items       : []
	}],
	buttonAlign :	'center',
	buttons		:	[{
		text    :	'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Upload</b></span>',
		id		:	'packageInsert',
		hidden	:	true,
		handler	:	function(){
			var data	=	shortSaleRequireForm.getForm().getValues();
			var checks	=	Array();
			var string	=	0, on = 0;
			$.each(data,function(index,value){
				var temp	=	index.split("-");
				if(temp[0]=="t"){
					checks.push({
						id		:	temp[1],
						value	:	value,
						required:	data['c-'+temp[1]]	===	"on"	?	1	:	0
					});
					if(value.trim().length<=0)
						string	=	string	+	1;
					if(data['c-'+temp[1]]=="on")
						on	=	on	+	1;
				}
			});
			if(on<=0){
				Ext.Msg.show({
				   title	:	'Information',
				   msg		:	'You must select at least one option.',
				   buttons	:	Ext.MessageBox.OK,
				   icon		:	Ext.MessageBox.INFO
				});
				return;
			}
			if(string>0){
				Ext.Msg.show({
				   title	:	'Information',
				   msg		:	'There should be no empty description, please check and try again.',
				   buttons	:	Ext.MessageBox.OK,
				   icon		:	Ext.MessageBox.INFO
				});
				return;
			}
			if (packageForm.getForm().isValid()) {
				packageForm.getForm().submit({
					url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
					waitMsg : 'Saving...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.Msg.show({
						   title      : 'Info',
						   msg		  : resp.mensaje,
						   buttons    : Ext.MessageBox.OK,
						   icon       : Ext.MessageBox.INFO
						});
						var tab     = tabs3.getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url   : 'mysetting_tabs/myshortsale_tabs/myshortsale.php',
							cache : false
						});
					},
					params:{
						idfunction		:	'uploadNewPackage',
						ctrlProcessor	:	ctrlProcessor,
						idProcessor		:	Ext.getCmp('cprocessor2').getValue(),
						checks			:	Ext.util.JSON.encode(checks)
					},
					failure : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	},{
		text    :	'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Cancel</b></span>',
		id		:	'packageCancel',
		hidden	:	true,
		handler	:	function(){
			Ext.getCmp('renderShortSaleRequire').items.each(function(item){
				item.destroy();
			});
			Ext.getCmp('panelShortSalePackageUpload').hide();
			Ext.getCmp('renderShortSalePackage').show();
			Ext.getCmp('packageInsert').hide();
			Ext.getCmp('packageCancel').hide();
		}
	}]
});
packageForm.render('div_shortSalePackages');

var shortSaleRequireForm = new Ext.FormPanel({
	title      : 'Documents Required By package',
	border     : true,
	bodyStyle  : 'padding: 10px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	id         : 'formShortSaleRequire',
	name       : 'formShortSaleRequire',
	items      : [
		{
			xtype	:	'spacer',
			height	:	10
		},{
			xtype	:	'panel',
			id		:	'renderShortSaleRequire',
			border	:	true,
		},{
			xtype	:	'box',
			html	:	'Any changes made to these settings will be applied automatically.',
			cls		:	'x-form-item'
		}
	]
});
shortSaleRequireForm.render('div_shortSaleRequire');

function searchPackages(idProcessor) {
	Ext.Ajax.request({
		url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
		method  : 'POST',
		params    : {
			idProcessor	:	((typeof idProcessor	==	"undefined")	?	Ext.getCmp('cprocessor2').getValue()	:	idProcessor),
			idfunction	:	'searchPackages'
		},
		waitMsg : 'Getting Packages',
		success : function(r) {

			var resp	=	Ext.decode(r.responseText);
			Ext.each(resp.data,function(data,index){
				var temp	=	{
					xtype	:	'fieldset',
					layout       : 'table',
					layoutConfig : {
						columns  : 9
					},
					items	:	[{
						xtype	:	'box',
						width	:	300,
						html	:	'<span style=\'font-size: 15px;\'><b>'+data.packageName+'</b></span>'
					},{
						xtype	:	'spacer',
						width	:	10
					},{
						xtype       :	'button',
						text        :	'Required',
						idPackage	:	data.id_packages,
						listeners	:	{
							click	:	function(button){
								loading_win.show();
								Ext.getCmp('renderShortSaleRequire').items.each(function(item){
									item.destroy();
								});
								searchRequire(button.idPackage);
							}
						}
					},{
						xtype	:	'spacer',
						width	:	10
					},{
						xtype       :	'button',
						text        :	'Update',
						idPackage	:	data.id_packages,
						userid		:	data.userid,
						listeners	:	{
							click	:	function(button) {
								var navegador=navigator.userAgent;
								if(navegador.indexOf('MSIE') != -1) {
									if(parseInt(BrowserDetect.version)<9){
										Ext.Msg.show({
										   title      : '',
										   //msg        : 'This option works only with Firefox and Chrome. Internet Explorer users please download and use any of the mentioned browsers.',
										   msg		  : 'This option only works with IE9 or above.',
										   width      : 400,
										   buttons    : Ext.MessageBox.OK,
										   icon       : Ext.MessageBox.INFO
										});
									}else{
										modifyShortSale('shortSale',button.idPackage,button.userid);
									}
								}else{
									modifyShortSale('shortSale',button.idPackage,button.userid);
								}
							}
						},
					},{
						xtype	:	'spacer',
						width	:	10
					},{
						xtype       :	'button',
						text        :	'View/Download',
						idPackage	:	data.id_packages,
						userid		:	data.userid,
						listeners	:	{
							click	:	function(button) {
								Ext.Ajax.request({
									url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
									method  : 'POST',
									waitMsg : 'Getting Info',
									success : function(r,o) {
										var resp	=	$.parseJSON(r.responseText);
										var Digital	=	new Date()
										var hours	=	Digital.getHours()
										var minutes	=	Digital.getMinutes()
										var seconds	=	Digital.getSeconds()
										var url 	=	'http://www.reifax.com/'+resp.package;
										window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
									},
									params	:	{
										idfunction	:	'viewPackage',
										idPackage	:	button.idPackage
									}
								});
							}
						},
					},{
						xtype	:	'spacer',
						hidden	:	ctrlProcessor===true ?	false	:	data.userid,
						width	:	10
					},{
						xtype       :	'button',
						text        :	'Delete',
						hidden		:	ctrlProcessor===true ?	false	:	data.userid,
						idPackage	:	data.id_packages,
						listeners	:	{
							click	:	function(button){
								deletePackage(button.idPackage);
							}
						}
					}]
				}
				Ext.getCmp('renderShortSalePackage').add(temp);
				Ext.getCmp('renderShortSalePackage').doLayout();
			});
		}
	});
}

function deletePackage(idPackage) {
	Ext.Ajax.request({
		url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp	=	Ext.decode(r.responseText);
			if(resp.success===false){
				Ext.Msg.show({
				   title      : 'Warning!',
				   msg        : resp.msg,
				   buttons    : Ext.MessageBox.OK,
				   icon       : Ext.MessageBox.WARNING
				});
			}else{
				Ext.Msg.show({
				   title      : 'Info!',
				   msg        : resp.msg,
				   buttons    : Ext.MessageBox.OK,
				   icon       : Ext.MessageBox.INFO
				});
				var tab     =	tabs3.getActiveTab();
				var updater	=	tab.getUpdater(); 
				updater.update({
					url		:	'mysetting_tabs/myshortsale_tabs/myshortsale.php',
					cache	:	false
				});
			}
		},
		params	:	{
			idfunction	:	"deletePachage",
			idPackage	:	idPackage
		}
	});
}
function searchRequire(idPackage,idfunction) {
	idfunction	=	(typeof idfunction === "undefined"	?	"searchRequire"	:	idfunction);
	Ext.Ajax.request({
		url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		success : function(r) {
			var resp	=	Ext.decode(r.responseText);
			var sections=	resp.data;
			var cS		=	1;
			var cC		=	1;
			$.each(sections,function(indexSection,section){
				var objetos	=	new Array();
				$.each(section,function(indexCheck,check){
					var temp	=	{
						xtype		:	'compositefield',
						hideLabel	:	true,
						items	:	[{
							xtype		:	'checkbox',
							id			:	'check-'+cC,
							name		:	'c-'+check.id_require,
							boxLabel	:	check.displayName,
							idRequire	:	check.id_require,
							checked		:	parseInt(check.require),
							ctrlSave	:	idfunction=="searchRequireNew"	?	false	:	true,
							listeners	:	{
								check	:	function(check,value){
									if(check.ctrlSave){
										Ext.Ajax.request({
											url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
											method  : 'POST',
											waitMsg : 'Getting Info',
											success : function(r) {
												var resp	=	Ext.decode(r.responseText);
												if(resp.success==false){
													Ext.Msg.show({
													   title      : 'Warning!',
													   msg        : resp.msg,
													   buttons    : Ext.MessageBox.OK,
													   icon       : Ext.MessageBox.INFO
													});
												}
											},
											params	:	{
												idfunction	:	'updateRequiteValue',
												idRequire	:	check.idRequire,
												newValue	:	((value===true)	?	1	:	0)
											}
										});
									}
								}
							}
						},{
							xtype		:	'textfield',
							idRequire	:	check.id_require,
							idParent	:	'check-'+cC,
							name		:	't-'+check.id_require,
							allowBlank	:	false,
							value		:	check.descName,
							ctrlSave	:	idfunction=="searchRequireNew"	?	false	:	true,
							listeners	:	{
								afterrender	:	function(text){
									var widthCheck	=	Ext.getCmp(text.idParent).getWidth();
									var widthParent	=	Ext.getCmp(text.el.up('.x-form-composite').id).getWidth();
									text.setWidth(widthParent-widthCheck-5);
								},
								change	:	function(text, newValue, oldValue){
									if(text.ctrlSave){
										Ext.Ajax.request({
											url     : 'mysetting_tabs/myshortsale_tabs/shortsaleCore.php',
											method  : 'POST',
											waitMsg : 'Getting Info',
											success : function(r) {
												var resp	=	Ext.decode(r.responseText);
												if(resp.success==false){
													Ext.Msg.show({
													   title      : 'Warning!',
													   msg        : resp.msg,
													   buttons    : Ext.MessageBox.OK,
													   icon       : Ext.MessageBox.INFO
													});
												}
											},
											params	:	{
												idfunction	:	'updateRequiteText',
												idRequire	:	text.idRequire,
												newValue	:	newValue
											}
										});
									}
								}
							}
						}]
					}
					objetos.push(temp);
					cC	=	cC	+	1;
				});
				var temp	=	{
					xtype	:	'fieldset',
					id		:	'section-'+cS,
					title	:	'Section '+cS+' - '+indexSection,
					items	:	[objetos]
				}
				Ext.getCmp('renderShortSaleRequire').add(temp);
				Ext.getCmp('renderShortSaleRequire').doLayout();
				cS	=	cS	+	1;
			});
			loading_win.hide();
			Ext.getCmp('renderShortSaleRequire').doLayout();
		},
		params	:	{
			idfunction	:	idfunction,
			idPackage	:	idPackage
		}
	});
}
</script>