<?php  
include("templates/properties_template.php");
include("properties_conexion.php");
$conex=conectar('xima');
$cliente=1;	
$realtor=false;
$gets = '';
if(isset($_GET['rbko'])){
	$gets = '&rbko='.$_GET['rbko'].'&bkou='.$_GET['bkou'].'&freeactive='.$_GET['freeactive'].'&freedays='.$_GET['freedays'].'&txtamountpay='.$_GET['txtamountpay'];
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>REIFAX Plans &amp; Pricing </title>
<?php tagHeadHeader(false);?>
<script>
	if(document.URL.substr(0,5)=='https')document.location=document.URL.substr(0,4)+document.URL.substr(5);
</script>
</head>
<?php 
	include('advertisingLoadNew.php');
?>
<body> 
<div align="center">
	<div id="body_central">
        <div id="layout_center" align="center" style=" position:relative;"><?php topLoginHeader(false,true,false);?>
          <div align="center" style="width:100%; margin:auto;height:100%; clear:both; ">
                <div id="tabs" style="height:100%;"></div>
           </div>
        </div>
              
        <div id="layout_top"><div id="T1" style="height:10px; vertical-align:middle; "><?php echo $top;?></div></div>
       
        <div id="layout_bottom"><div id="B1" style="height:110px; vertical-align:middle;"><?php echo $bottom;?></div></div>
        
        <div id="layout_left"><?php echo $left;?></div>
        
        <div id="layout_right"><?php echo $right;?></div>
	</div>
</div>

<?php googleanalytics(); ?>
<script>
Ext.QuickTips.init();
var tabs=tabs3=viewport=null;
var user_block=false;

var system_width=document.body.offsetWidth;
//alert(system_width);
/*if(system_width<1000)
	system_width=660;*/
	
if(system_width>1000)
	system_width=980;
	
document.getElementById('layout_center').style.width= 660;

 
Ext.onReady(function(){
	
	tabs = new Ext.TabPanel({
        renderTo: 'tabs',
		activeTab:0,
		height: 2850,
		width: 660,
        plain:true,
		enableTabScroll:true,
		defaults:{	autoScroll: false},
        items:[{
			title: ' Plans &amp; Pricing ',
			id: 'planspricing',
			width: 660,
			autoLoad: {
				url: 'http://www.reifax.com/xima3/Pricing/index.php?procode=<?php echo $_GET['procode'].$gets;?>',
				timeout: 10800,
				scripts: true
			}
		}]
    });
	
	
	var ancho = document.body.offsetWidth;
	if(ancho < 1024) ancho = 1024;
	if(ancho > 1260) ancho = 1260;
	
	 viewport = new Ext.Panel({
		  renderTo: 'body_central',
		  id: 'viewport',
		  layout:'border',
		  monitorResize: true,
		  hideBorders: true,
		  width: ancho,
		  height: 2900,
		  items:[
			{
				region: 'south',
				id: 'south_panel',
				layout: 'hbox',
				height: 120,
				minSize: 120,
				maxSize: 120,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_bottom'
			},
			{
				region: 'east',
				id: 'east_panel',
				layout: 'vbox',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_right'
			},
			{
				region: 'west',
				id: 'west_panel',
				width: 300,
				minSize: 300,
				maxSize: 300,
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_left'
			},
			{
				region: 'center',
				id: 'center_panel',
				layout: 'hbox',
				width: 'auto',
				layoutConfig:{align:'middle'},
				collapseMode: 'mini',
				collapsible: false,
				split: false,
				contentEl: 'layout_center'
			}
		  ]
	});
	
	if(document.body.offsetWidth < 1200){
		 var west = Ext.getCmp('west_panel');
		 west.collapse();
	}
	
});
</script>

</body>
</html>