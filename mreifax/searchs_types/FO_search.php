<div align="left" class="search_realtor_fondo" id="FO_search_div" style="display:none;">
    <table width="600" border="0" cellpadding="0" cellspacing="0" style="font-size:12px; margin:auto;">		
        <tr>    	    	
            <td width="120" colspan="2" style="color:#FFF; font-weight:bold;">Data</td>                
            <td width="240" colspan="6" style="color:#FFF; font-weight:bold;">Search By</td>                
            <td width="130" colspan="2" style="color:#FFF; font-weight:bold;">State</td>         
            <td width="130" style="color:#FFF; font-weight:bold;">County</td>      	
        </tr>
        <tr>    	    	
            <td width="115"><select name="combo_search_types" id="FO_combo_search_types" style="width:115px;" onchange="doSearchTypeFilter(this.value);">
            <option value="PR" >Public Records</option> 
            <option value="FS" >For Sale</option>
            <option value="FR" >For Rent</option>
            <option value="FO" selected="selected">Foreclosures</option>
                <!--
				<option value="BO" >By Owner</option>
	            <option value="BOR" >By Owner Rent</option>
				-->
            </select></td>
            <td width="5">&nbsp;</td>
            <td colspan="5" width="235"><select name="combo_search_by" id="FO_combo_search_by" style="width:105px;" onchange="doSearchByFilter(this.value);">
            <option value="LOCATION" <?php if($search_by_type=='LOCATION') echo 'selected="selected"';?>>Location</option>
            <option value="PARCELID" <?php if($search_by_type=='PARCELID') echo 'selected="selected"';?>>Parcelid</option>
            <option value="CASE" <?php if($search_by_type=='CASE') echo 'selected="selected"';?>>Case-Number</option>
            <option value="MAP" <?php if($search_by_type=='MAP') echo 'selected="selected"';?>>Map</option>
            </select></td>
            <td width="5">&nbsp;</td>
            <td width="130"><select name="state_search" id="FO_state_search" style="width:120px;" onchange="doChangeState(this.value);">
            <?php
                $query='select idstate,state FROM xima.lsstate where is_showed="Y" order by state';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($state_search==$r['idstate'])
                        echo '<option value="'.$r['idstate'].'" selected="selected">'.$r['state'].'</option>';
                    else
                        echo '<option value="'.$r['idstate'].'">'.$r['state'].'</option>';
                }
            ?>
            </select></td>
            <td width="5">&nbsp;</td>
            <td width="130"><select name="county_search" id="FO_county_search" style="width:125px;" onchange="if(search_by_type=='MAP') mapSearch.centerMapCounty(this.value,true); else mapSearch.centerMapCounty(this.value,false); search_county=this.value;">
            <?php
                $query='select idcounty,county FROM xima.lscounty where is_showed=1 and idstate='.$state_search.' order by county';
                $result=mysql_query($query) or die($query.mysql_error());
                $xs=0;
                while($r=mysql_fetch_array($result)){
                    if($county_search==$r['idcounty'])
                        echo '<option value="'.$r['idcounty'].'" selected="selected">'.$r['county'].'</option>';
                    else
                        echo '<option value="'.$r['idcounty'].'">'.$r['county'].'</option>';
                }
            ?>
            </select></td>
        </tr>
        <tr>    	    	
            <td colspan="8" width="360" style="color:#FFF; font-weight:bold;">
                <span id="FO_tsearch_l">Location</span>
            </td>                               
            <td colspan="2" width="130" id="FO_tproptype" style="color:#FFF; font-weight:bold; ">Type</td>         
            <td width="130" id="FO_tpropfore" style="color:#FFF; font-weight:bold; ">Foreclosure Status</td> 
        </tr>        
        <tr>             	
            <td colspan="7" width="355">
                <input type="text" name="search" id="FO_search" size="40" maxlength="2048" style="font-size:17px;" value="Address, City or Zip Code" onfocus="colocarDefault('FO_search',this.value);" onblur="colocarDefault2('FO_search',this.value);">
            </td>                
            <td width="5">&nbsp;</td>                
            <td width="125">
                <select id="FO_proptype" style="width:120px;" name="proptype" onchange="verEquity(this.value,'FO_pequity','FO_tpequity');"><option value="">Any Type</option><option value="01">Single Family</option><option value="04">Condo/Town/Villa</option><option value="03">Multi Family +10</option><option value="08">Multi Family -10</option><option value="11">Commercial</option><option value="00">Vacant Land</option><option value="02">Mobile Home</option><option value="99">Other</option></select>
            </td>   	        
            <td width="5">&nbsp;</td>        
            <td width="130">
                <select name="pendes" id="FO_pendes" style="width:125px;"><option value="-1">Any</option><option value="P">Pre-Foreclosed</option><option value="F">Foreclosed</option></select>
            </td> 
        </tr>        
        <tr> 
            <td colspan="2" width="120" id="FO_tentrydate" style="color:#FFF; font-weight:bold;">Entry Date</td>                
            <td colspan="2" width="70" id="FO_tbeds" style="color:#FFF; font-weight:bold; ">Beds</td>                
            <td colspan="2" width="70" id="FO_tbath" style="color:#FFF; font-weight:bold; ">Baths</td>                
            <td colspan="2" width="100" id="FO_tsqft" style="color:#FFF; font-weight:bold; ">Sqft</td>         
            <td colspan="2" width="130" id="FO_tpequity" style="color:#FFF; font-weight:bold; ">Debt Equity</td>                
            <td width="130" id="FO_toccupied" style="color:#FFF; font-weight:bold;">&nbsp;</td>      	
        </tr>        
        <tr>       
            <td width="115">
                <input type="text" name="entrydate" id="FO_entrydate" value="yyyymmdd" size="10" maxlength="8" onfocus="if(this.value=='yyyymmdd')this.value='';" onblur="if(this.value=='')this.value='yyyymmdd';">
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="65">
                <select name="bed" id="FO_bed"><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="65">
                <select name="bath" id="FO_bath"><option value="-1">Any</option><option value="1">1+</option><option value="2">2+</option><option value="3">3+</option><option value="4">4+</option><option value="5">5+</option></select>
            </td>        
            <td width="5">&nbsp;</td>        
            <td width="95">
                <select name="sqft" id="FO_sqft"><option value="-1">Any</option><option value="250">250+</option><option value="500">500+</option><option value="1000">1,000+</option><option value="1250">1,250+</option><option value="1500">1,500+</option><option value="1750">1,750+</option><option value="2000">2,000+</option><option value="2250">2,250+</option><option value="2500">2,500+</option><option value="2750">2,750+</option><option value="3000">3,000+</option><option value="3250">3,250+</option><option value="3500">3,500+</option><option value="3750">3,750+</option><option value="4000">4,000+</option><option value="5000">5,000+</option><option value="10000">10,000+</option></select>
            </td>       	
            <td width="5">&nbsp;</td>            	
            <td width="125">
                <select name="pequity" id="FO_pequity" style="width:118px;"><option value="-1">Any</option><option value="<-10">Less Than -10</option><option value="<0">Less Than 0</option><option value=">=0">Equal or Greater 0</option><option value=">10">Greater Than 10</option><option value=">20">Greater Than 20</option><option value=">30">Greater Than 30</option><option value=">40">Greater Than 40</option><option value=">50">Greater Than 50</option><option value=">60">Greater Than 60</option></select>        
            </td>        
            <td width="5">&nbsp;</td>    	
            <td width="130" style="padding-top:0px;">
                	<img src="img/button_search.png" alt="Xima Usa Search" onClick="searchForm('FO_search','xxx');">
            </td>   	
        </tr>    
    </table>
</div>