<?php 	
/**
Consideraciones
================
1.- En funcion de lo que se selecciono en el combo de propiedad o formato de salida se esta poniendo la tabla principal para el select
2.- En funcion del tipo de propiedad seleccionado distinto a ALL se usa el psummary o el mlsresidentiasl para el xcode
3.- Para el caso del foreclosure status se toma el marketvalue.pendes
4.- El search de mortgage,rental y pendes necesitan tener left join con psummary por los campos definidos por el result
5.- El $arrtablas tienes todas las tablas que se usaran en el query, bien sea las que van en el filtro (where) o si estan en los campos de select
*/
function buscaritem($array,$item)
{
	if(!isset($array))return 0;
	$enc=0;
	foreach ($array as $val) 
	{
	    if($val==$item)
			$enc=1;
	}
	return $enc;
}

if($_POST['ocproperty']=='')// ESTO ES SOLO DE PRUEBA DIRECTA, QUITARLO CUANDO ESTE LISTO
{
	$_POST['ocproperty']='FS';
	$_POST['occounty']='5';
	$_POST['ocproptype']='01';
	$_POST['ocforeclosure']='F';
	$_POST['ocstate']='FL';
	$_POST['cbhpsummary*city']='Start With';
	$_POST['txpsummary*city']='ciudad';
	$_POST['cbhpsummary*saleprice']='Equal';
	$_POST['txpsummary*saleprice']='15200';
	$_POST['cbhpsummary*waterf']='Yes';
	$_POST['txpsummary*waterf']='';
	$_POST['cbhmarketvalue*marketmedia']='Equal';
	$_POST['txmarketvalue*marketmedia']='5000';
	//echo "<pre>";print_r($_POST);echo "</pre>";
}

include('../properties_conexion.php');
conectar();

$propoutput=$_POST['ocproperty'];
$county=$_POST['occounty'];
$protype=$_POST['ocproptype'];
if($protype=='*')$protype='';
$foreclosure=$_POST['ocforeclosure'];
if($foreclosure=='-1')$foreclosure='';
$state=$_POST['ocstate'];
unset($arrtablas,$arrfiltros,$filtrosadv);
$tabla='';
reset($_POST);
while (list($key, $value) = each($_POST)) 
{
	if (substr($key,0,2)<>'oc')//si no son parametros de busqueda iniciales
	{
		$tabla=$key;
		$tabla=str_replace('cbh','',$tabla);
		$tabla=str_replace('tx','',$tabla);
		$arr=explode('*',trim($tabla));
		$tabla=$arr[0];
		$campo=$arr[1];
		$idct=$arr[2];
		$combofiltro=$value;
		
		list($key, $value) = each($_POST);
		$valorfiltro=$value;

		if((isset($valorfiltro) && strlen($valorfiltro)>0 ) || ($combofiltro=='Yes' || $combofiltro=='No' ) )
		{
			if(isset($arrtablas))
			{
				if(buscaritem($arrtablas,$tabla)==0) 
					$arrtablas[]=$tabla;
			}
			else
				$arrtablas[]=$tabla;
			
//			echo "Tabla: $tabla; Campo: $campo; ComboFiltro: $combofiltro; ValorFiltro: $valorfiltro<br />\n";
			$arrfiltros[]=array('tabla'=>$tabla, 'campo'=>$campo, 'comboFiltro'=>$combofiltro, 'valorfiltro'=>$valorfiltro);
			$filtrosadv[]=array('idct'=>$idct, 'comboFiltro'=>$combofiltro, 'valorfiltro'=>$valorfiltro);
		}
	}
}

$query="UPDATE xima.xima_system_var SET
		search_type='$propoutput',search_filter_county='$county',
		search_filter_proptype='$protype',
		search_filter_pendes='$foreclosure',
		filter_search_adv='".json_encode($filtrosadv)."'
		WHERE userid=".$_COOKIE['datos_usr']['USERID'];//search_filter_mapa="'.(string)$_POST['search_mapa'].'",
mysql_query($query) or die($query.mysql_error());


//buscando la tabla principal para el query 
//echo "<pre>";print_r($arrtablas);echo "</pre>";
$maintable='';
if($propoutput=='PR'){	$maintable='psummary';	$siproptype='psummary';	}
if($propoutput=='FO'){	$maintable='pendes';	$siproptype='psummary'; $arrtablas[]=$siproptype; }
if($propoutput=='MO'){	$maintable='mortgage';	$siproptype='psummary'; $arrtablas[]=$siproptype; }
if($propoutput=='FS'){	$maintable='mlsresidential';	$siproptype='mlsresidential';}
if($propoutput=='FR'){	$maintable='rental';	$siproptype='mlsresidential'; $arrtablas[]=$siproptype; }

//buscando los campos que van en el select del query
//echo "<pre>";print_r($arrtablas);echo "</pre>";
$sql1="SELECT * FROM xima.`gridresult` s WHERE s.`idGrid`='$propoutput'";
$res1=mysql_query($sql1) or die($sql1.mysql_error());
$rowmain=mysql_fetch_array($res1);
$idcts=$rowmain['campos'];
					
$sql2="SELECT c.`Tabla`, c.`Campos` FROM xima.camptit c WHERE c.`IDTC` in ($idcts)";
$res2=mysql_query($sql2) or die($sql2.mysql_error());
$camposselect='';
$ir=0;
while($row=mysql_fetch_array($res2))
{
	if($ir>0)$camposselect.=',';
	$camposselect.=$row['Tabla'].'.'.$row['Campos'];
	if(!buscaritem($arrtablas,$row['Tabla']) && arrtablas<>'marketvalue' && arrtablas<>$maintable)
		$arrtablas[]=$row['Tabla']; 

	$ir++;
}

//Para el caso del foreclosure status se toma el marketvalue.pendes
if($foreclosure<>'' && strlen($foreclosure)>0)
	array_unshift($arrfiltros,array('tabla'=>'marketvalue', 'campo'=>'pendes', 'comboFiltro'=>'EXACT', 'valorfiltro'=>$foreclosure));	

//En funcion del tipo de propiedad seleccionado distinto a ALL se usa el psummary o el mlsresidentiasl para el xcode
if($protype<>'' && strlen($protype)>0)
{
	array_unshift($arrfiltros,array('tabla'=>$siproptype, 'campo'=>'xcode', 'comboFiltro'=>'EXACT', 'valorfiltro'=>$protype));	
	$arrtablas[]=$siproptype; 
}

//buscando las tablas que van en el join del query
if(isset($arrtablas))
{
	unset($arraux);
	foreach ($arrtablas as $val) 
		if(!buscaritem($arraux,$val) && $val<>'marketvalue' && $val<>$maintable)
			$arraux[]=$val;
	unset($arrtablas);
	$arrtablas=$arraux;
	//echo "<pre>";print_r($arrtablas);echo "</pre>";
}
$leftjoin=" LEFT JOIN latlong ON ($maintable.parcelid=latlong.parcelid)
			LEFT JOIN marketvalue ON ($maintable.parcelid=marketvalue.parcelid) 
			LEFT JOIN imagenes ON ($maintable.parcelid=imagenes.parcelid) ";
if(isset($arrtablas))
{
	foreach ($arrtablas as $val) 
		$leftjoin.=" LEFT JOIN $val ON ($maintable.parcelid=$val.parcelid) ";
}

//buscando los filtros que van en el where del query
//echo "<pre>";print_r($arrfiltros);echo "</pre>";
$wherejoin=" WHERE 1=1 ";
if(isset($arrfiltros))
{
	$filtro='';
	$cant=count($arrfiltros);
	for($ifor=0;$ifor<$cant;$ifor++) 
	{
		$tabla=$arrfiltros[$ifor]['tabla'];
		$campo=$arrfiltros[$ifor]['campo'];
		$condicional=$arrfiltros[$ifor]['comboFiltro'];
		$value=$arrfiltros[$ifor]['valorfiltro'];
	
		switch(strtoupper($condicional))
		{
			//STRING
			case "START WITH":
				$filtro="LEFT(".$tabla.".".$campo.",10)='".$value."'"; 
			break;
			case "EXACT":
				$filtro=$tabla.".".$campo."='".$value."'"; 
			break;
			case "CONTAINS":
				$filtro="INSTR(".$tabla.".".$campo.",'".$value."')";
			break;
			case "NOT START WITH":
				$filtro="LEFT(".$tabla.".".$campo.",10)<>'".$value."'"; 
			break;
			case "NOT EXACT":
				$filtro=$tabla.".".$campo."<>'".$value."'";
			break;
			case "NOT CONTAINS":
				$filtro="INSTR(".$tabla.".".$campo.",'".$value."')=0";
			break;
						
			//NUMBERS Y DATE
			case "EQUAL":
				$filtro=$tabla.".".$campo."=".$value; 
			break;
			case "GREATER THAN":
				$filtro=$tabla.".".$campo.">".$value; 
			break;
			case "LESS THAN":
				$filtro=$tabla.".".$campo."<".$value; 
			break;
			case "EQUAL OR LESS":
				$filtro=$tabla.".".$campo."<=".$value; 
			break;
			case "EQUAL OR GREATER":
				$filtro=$tabla.".".$campo.">=".$value; 
			break;
			case "BETWEEN":							
				//$filtro="(".$tabla.".".$campo." BETWEEN ".$value." AND ".$value2.")"; 
			break;
						
			//BOOLEAN
			case "YES":
				if($campo=='astatus')$filtro=$tabla.".".$campo."='A'";
				else if($campo=='sold')$filtro=$tabla.".".$campo."='S'";
				else $filtro=$tabla.".".$campo."='Y'";
			break;
			case "NO":
				if($campo=='astatus')$filtro=$tabla.".".$campo."<>'A'";
				else $filtro=$tabla.".".$campo."='N'";
			break;
		}
	
		$wherejoin.=" AND ".$filtro;
		
	}
}

$MAINSQL="";
$MAINSQL.="SELECT $camposselect FROM $maintable $leftjoin $wherejoin ";

echo "<br>".$MAINSQL;

?>