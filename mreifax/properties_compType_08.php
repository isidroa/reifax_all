<?php
//===========Iniciaciones============
//=================================================================================================
	$QUERYEJECUTADO='';
	$campo_calculo='ffc';//campo por el que se calcula
	$master_tbl="rtmaster";//tabla principal del comparable
	$custom_sel="rtm.xcode,";//Selecciones basicas
	$array_campos=array();//guarda los campos y tipo para mostrar en pantalla.
	$data_arr=array();//se guarda lo calculado
	$MainData=array();//se aloja toda la data q sera vaciada en el grid
	$array_parcelIds=array();//se guardan todos los s para ser usados como filtro en los sub-siguientes selects
	$stat="";
	$beds_where=" ";
	$year_where=" ";
	$_calculo="truncate(sqrt((69.1* (ll.latitude- #lat))*(69.1*(ll.latitude-#lat))+(69.1*((ll.longitude-(#lon))*cos(#lat/57.29577951)))*(69.1*((ll.longitude-(#lon))*cos(#lat/57.29577951)))),2)";
	$factor_latlong=0.016;
	$factor_latlong_2=0.025;
	
	//==============Filtros===============
	//=============================================================================================
	if(isset($_POST['userid'])){
		if ($_POST["status"]=="A"){
			if(isset($_POST['filter']) && $_POST['filter']!=''){
				$filters = explode('^',$_POST['filter']);
				$r=array();
				while(count($filters)>0){
					$filteraux = array_pop($filters);
					$filteraux = explode('||',$filteraux);
					$r[$filteraux[0]] = $filteraux[1];
				}
				
			}else{
				$query='select filter_active_xcode, filter_active_distance,filter_active_beds,filter_active_bedscon,filter_active_baths,filter_active_bathscon,
				filter_active_garea,filter_active_garea_from,filter_active_garea_to,filter_active_larea,filter_active_larea_from,filter_active_larea_to,
				filter_active_pool,filter_active_waterf,filter_active_built,
				filter_active_ldate,filter_active_ldate_from,filter_active_ldate_to,
				filter_active_mapa, filter_active_subd 
				FROM xima.xima_system_var WHERE userid='.$_POST['userid'];
				$result=mysql_query($query) or die($query.mysql_error());
				$r=mysql_fetch_array($result);
			}
			
			$XCODE= $r['filter_active_xcode'];
			$distance=(float) $r['filter_active_distance'];
			$beds=(integer) $r['filter_active_beds'];
			$cond_beds=$r['filter_active_bedscon'];
			$baths=(integer) $r['filter_active_baths'];
			$cond_baths=$r['filter_active_bathscon'];
			$lsqft=(integer) $r['filter_active_garea'];
			$larea=(integer) $r['filter_active_larea'];
			$pool=(integer) $r['filter_active_pool'];
			$wf=(integer) $r['filter_active_waterf'];
			$Garea_From=(integer) $r['filter_active_garea_from'];
			$Garea_To=(integer) $r['filter_active_garea_to'];
			$Larea_From=(integer) $r['filter_active_larea_from'];
			$Larea_To=(integer) $r['filter_active_larea_to'];
			$yb=(integer) $r['filter_active_built'];
			$maplatlong=0;
			$maplatlong= $r['filter_active_mapa'];
			$closing=-1;
			$ClosingDT_From=-1;
			$ClosingDT_To=-1;
			$ldate=(integer)$r['filter_active_ldate'];
			$Ldate_From=(integer) $r['filter_active_ldate_from'];
			$Ldate_To=(integer) $r['filter_active_ldate_to'];
			$subd=(integer) $r['filter_active_subd'];
		}else{
			if(isset($_POST['filter']) && $_POST['filter']!=''){
				$filters = explode('^',$_POST['filter']);
				$r=array();
				while(count($filters)>0){
					$filteraux = array_pop($filters);
					$filteraux = explode('||',$filteraux);
					$r[$filteraux[0]] = $filteraux[1];
				}
				
			}else{
				$query='select filter_comp_xcode, filter_comp_distance,filter_comp_beds,filter_comp_bedscon,filter_comp_baths,filter_comp_bathscon,
				filter_comp_garea,filter_comp_garea_from,filter_comp_garea_to,filter_comp_larea,filter_comp_larea_from,filter_comp_larea_to,
				filter_comp_pool,filter_comp_waterf,filter_comp_built,
				filter_comp_closingdt,filter_comp_closingdt_from,filter_comp_closingdt_to,
				filter_comp_mapa, filter_comp_subd 
				FROM xima.xima_system_var WHERE userid='.$_POST['userid'];
				$result=mysql_query($query) or die($query.mysql_error());
				$r=mysql_fetch_array($result);
			}
			
			$XCODE= $r['filter_comp_xcode'];
			$distance=(float) $r['filter_comp_distance'];
			$beds=(integer) $r['filter_comp_beds'];
			$cond_beds=$r['filter_comp_bedscon'];
			$baths=(integer) $r['filter_comp_baths'];
			$cond_baths=$r['filter_comp_bathscon'];
			$lsqft=(integer) $r['filter_comp_garea'];
			$larea=(integer) $r['filter_comp_larea'];
			$pool=(integer) $r['filter_comp_pool'];
			$wf=(integer) $r['filter_comp_waterf'];
			$Garea_From=(integer) $r['filter_comp_garea_from'];
			$Garea_To=(integer) $r['filter_comp_garea_to'];
			$Larea_From=(integer) $r['filter_comp_larea_from'];
			$Larea_To=(integer) $r['filter_comp_larea_to'];
			$yb=(integer) $r['filter_comp_built'];
			$maplatlong=0;
			$maplatlong=$r['filter_comp_mapa'];
			$closing=(integer)$r['filter_comp_closingdt'];
			$ClosingDT_From=(integer) $r['filter_comp_closingdt_from'];
			$ClosingDT_To=(integer) $r['filter_comp_closingdt_to'];
			$ldate=-1;
			$Ldate_From=-1;
			$Ldate_To=-1;
			$subd=(integer) $r['filter_comp_subd'];
		}
	}else{
		$XCODE=-1;
		$distance=(float) -1;
		$beds=(integer) -1;
		$cond_beds='';
		$baths=(integer) -1;
		$cond_baths='';
		$lsqft=(integer) -1;
		$larea=(integer) -1;
		$pool=(integer) -1;
		$wf=(integer) -1;
		$Garea_From=(integer) -1;
		$Garea_To=(integer) -1;
		$Larea_From=(integer) -1;
		$Larea_To=(integer) -1;
		$yb=(integer) -1;
		$maplatlong=0;
		$closing=(integer)-1;
		$ClosingDT_From=(integer) -1;
		$ClosingDT_To=(integer) -1;
		$ldate=-1;
		$Ldate_From=-1;
		$Ldate_To=-1;
		$subd=-1;
	}
	
	//=============================================================================================
//=================================================================================================




//===========Comparado============
//=================================================================================================
   $query = "select 
   if(r.$campo_calculo is not null and r.$campo_calculo != '',r.$campo_calculo,p.$campo_calculo) $campo_calculo, 
   if(r.lsqft is not null and r.lsqft != '', r.lsqft,p.lsqft) lsqft,
   if(r.larea is not null and r.larea != '',r.larea,p.bheated) larea,
   if(r.yrbuilt is not null and r.yrbuilt != '', r.yrbuilt,p.yrbuilt) yrbuilt,
   p.sdname,
   ll.latitude,
   ll.longitude
   From psummary p
    LEFT JOIN rtmaster r ON (r.parcelid=p.parcelid)
   INNER JOIN latlong ll ON (ll.parcelid=p.parcelid)
   Where p.parcelid='$el_comparado' AND p.xcode='$proper'";
   $result = mysql_query($query) or die($query.mysql_error());
	
	if(mysql_num_rows($result)>0){
	//===========Datos============
	//============================================================================================
		$row= mysql_fetch_array($result, MYSQL_ASSOC);
		$lat=$row["latitude"];
		$lon=$row["longitude"];
		$campo_cal_compar=$row[$campo_calculo];
		$com_lsqft=$row["lsqft"];
		$com_larea=$row["larea"];
		$com_year=$row["yrbuilt"];
		$com_subd=$row["sdname"];
		$latmax=$lat+$factor_latlong;
		$latmin=$lat-$factor_latlong;
		$lonmax=$lon+$factor_latlong;
		$lonmin=$lon-$factor_latlong;
		if($distance>1){
			$latmax=$lat+$factor_latlong_2;
			$latmin=$lat-$factor_latlong_2;
			$lonmax=$lon+$factor_latlong_2;
			$lonmin=$lon-$factor_latlong_2;
		}
		mysql_free_result($result);	
		
	//=============================================================================================
//=================================================================================================




//===========Funciones===============
//=================================================================================================
	if(!$bpoaction || ($bpoaction && !isset($_POST['no_func']))){
		require('includes/class/polygon.geo.class.php');
		function _pointINpolygon($arrPoly,$lat,$lng) 
		{
			$lng=($lng);
			$vertex = new vertex($lng,$lat);
			$polygon =& new geo_polygon();
			$_lat=0;$_lng=0;
			foreach($arrPoly as & $coord)
			{
				$_lng=($coord["long"]);
				$_lat=$coord["lat"];		
				$polygon->addv($_lng,$_lat);
			}	
			$isInside = ($polygon->isInside($vertex))? true:false;	
			
			return $isInside;
		}
		function compare($x, $y)
		{
			global $campo_calculo;
			$col=$campo_calculo;
			if ( $x[$col] == $y[$col] )
				return 0;
			else if ( $x[$col] < $y[$col] )
				 return -1;
			else
				 return 1;
		}
		
		function saveData($sql_str,$params,$valida,$taken)
		{
			global $QUERYEJECUTADO;
			$QUERYEJECUTADO=$sql_str.$params;
					
			//echo $QUERYEJECUTADO;
			$nfilas=0;
			$retorno = mysql_query($sql_str.$params) or die($sql_str.$params." - ".mysql_error());
			$nfilas=mysql_num_rows($retorno);
		
			global $array_parcelIds,$el_comparado,$data_arr,$MainData,$distance_0_1,$distance_0_2,$bpoaction;
			$first_time=true;
			$data_arr1=$MainData1=$array_parcelIds1=array();
			$data_arr2=$MainData2=$array_parcelIds2=array();
			
			if($bpoaction) $va_pid='parcelid'; else $va_pid='pid';
			if($bpoaction) $va_lsqft='lsqft'; else $va_lsqft='pin_lsqft';
		
			while ($fila= mysql_fetch_array($retorno, MYSQL_ASSOC))
			{		
				$omitir=false;
				for ($z=0;$z<count($array_parcelIds);$z++)
				{
					if ($array_parcelIds[$z]==$fila[$va_pid])	
					{
						$omitir=true;$first_time=false;
						break;
					}//if
				}//for
	
				//corrije bug que hacia q el comparado se mostrase con los comparables
				if ( ($first_time==true) and ($fila[$va_pid]==$el_comparado) ) $omitir=true;
				
				if (($omitir==false))
				{
					if($fila["Distance"]<0.0000001)$fila["Distance"]=0.00;
					$data_arr[]=array("comparado"=>$el_comparado,"comparable"=>$fila[$va_pid],"distance"=>$fila["Distance"],"taken"=>$taken,"lsqft"=>$fila[$va_lsqft]);
					$MainData[]=$fila;
					$array_parcelIds[]=$fila[$va_pid];
					
					if($fila["Distance"]<$distance_0_1){
						$data_arr1[]=array("comparado"=>$el_comparado,"comparable"=>$fila[$va_pid],"distance"=>$fila["Distance"],"taken"=>$taken,"lsqft"=>$fila[$va_lsqft]);
						$MainData1[]=$fila;
						$array_parcelIds1[]=$fila[$va_pid];
					}
					
					if($fila["Distance"]<$distance_0_2){
						$data_arr2[]=array("comparado"=>$el_comparado,"comparable"=>$fila[$va_pid],"distance"=>$fila["Distance"],"taken"=>$taken,"lsqft"=>$fila[$va_lsqft]);
						$MainData2[]=$fila;
						$array_parcelIds2[]=$fila[$va_pid];
					}
					
					
				}//if
			}//while
			
			//echo $valida;
			if ($valida=='true') 
			{
				$data_arr=$data_arr1;
				$MainData=$MainData1;
				$array_parcelIds=$array_parcelIds1;
				//echo $distance_0_5.' '.count($array_parcelIds);
				if(count($array_parcelIds)<=2) 
				{
					$data_arr=$data_arr2;
					$MainData=$MainData2;
					$array_parcelIds=$array_parcelIds2;
				}
			}
		
			mysql_free_result($retorno);	
			return true;
			
			
		}
		
		//================================================================			
						
		function findFilter($value)
		{
			global $params_0_1,$params_0_2;
			$string_ret="";
				
			switch ($value)
			{
				case "params_0_1":
					$string_ret=$params_0_1;
					break;
				case "params_0_2":
					$string_ret=$params_0_2;
					break;
			}
			
			return $string_ret;
		}
	}
//=================================================================================================



//===========Configuraciones===============
//=================================================================================================
	//===========Configuracion por Status===============
	//=============================================================================================
	//if ( ($bedscompar>0) ) $beds_where=" AND ((rtm.beds=$bedscompar)) ";
	if ($_POST["status"]=="A") 
	{
		$campo_price="lprice";
		$price_where=" AND (rtm.lprice>= $priceFactor ) ";
		$_status=array("A");
	}
	else
	{
		$campo_price="saleprice";
		$price_where=" AND (rtm.saleprice>= $priceFactor ) ";
		$year_where.=" and ( date(rtm.closingdt)>= DATE_SUB( curdate(), interval '365' day)) ";
		$_status=array("CC","CS");
	}
	if(($campo_cal_compar>0) )
	{
		$sqft_1_1__0_9="AND ( (rtm.$campo_calculo<=(#lsq*1.1)) and (rtm.$campo_calculo>=(#lsq*0.9) ) AND (rtm.$campo_calculo>0))";
		$sqlpriceselect="(rtm.$campo_price/rtm.$campo_calculo)";
		$sqlpricewhere=" rtm.$campo_calculo>0 and ";
	}
	else
	{	
		$sqlpriceselect="(rtm.$campo_price)";
		$sqlpricewhere=' ';
	}
	for($x=0;$x<count($_status);$x++) 
	{
		if($x>0) $stat.=" or ";
		$stat.="rtm.status='".$_status[$x]."'";	
	}
	if($XCODE!='-1') $proper=$XCODE;
	
	//=============================================================================================
	
	//===========Configuracion por Modulo===============
	//=============================================================================================
		$distance_0_1="0.5";
		$distance_0_2="1.0";
		//=========================================================================================
//=================================================================================================	

	

//===========Filtrando===============
//=================================================================================================
	$_newFilter="";
		
	if ( $distance>0  )  {	$distance_0_1=$distance;$distance_0_2=$distance;}
	
	if ( $beds>0  )  { $_newFilter.=" AND (rtm.beds$cond_beds$beds)";}
	if ( $baths>0  )  { $_newFilter.=" AND (rtm.bath$cond_baths$baths)";}
	
	if($Garea_From>=0 && $Garea_To>=0)
	{
		$_newFilter.=" AND (rtm.lsqft>=".$Garea_From." and rtm.lsqft<=".$Garea_To.")";
		$sqft_1_2__0_8="";	$sqft_1_1__0_9="";
		
	}elseif($lsqft>0){		
		if($com_lsqft>0){
			$div=round($lsqft/100,2);
			$_lsqft=$com_lsqft+($com_lsqft*$div);
			$_lsqft1=$com_lsqft-($com_lsqft*$div);		
	
			$_newFilter.=" AND (rtm.lsqft>=".$_lsqft1." and rtm.lsqft<=".$_lsqft." and rtm.lsqft>0)";
			$sqft_1_2__0_8="";	$sqft_1_1__0_9="";
		}
	}	
	
	if($Larea_From>=0 && $Larea_To>=0)
	{
		$_newFilter.=" AND (rtm.larea>=".$Larea_From." and rtm.larea<=".$Larea_To.")";
		$sqft_1_2__0_8="";	$sqft_1_1__0_9="";
	}
	elseif($larea>0){		
		if($com_larea){
			$div=round($larea/100,2);
			$_lsqft=$com_larea+($com_larea*$div);
			$_lsqft1=$com_larea-($com_larea*$div);		
	
			$_newFilter.=" AND (rtm.larea>=".$_lsqft1." and rtm.larea<=".$_lsqft." and rtm.larea>0)";
			$sqft_1_2__0_8="";	$sqft_1_1__0_9="";
		}
	}	
	
	if ( ($yb>=0) && ($com_year>0)  )  { $_newFilter.=" AND ((rtm.yrbuilt>=($com_year-$yb)) and (rtm.yrbuilt<=($com_year+$yb)))"; }
	
	if ( ($pool>=0) && ($pool!='-1')  ) {   $_pool=($pool==1)? "Y":"N"; $_newFilter.=" AND rtm.pool='$_pool'";}
	
	if ( ($wf>=0) && ($wf!='-1')  ) {   $_wf=($wf==1)? "Y":"N"; $_newFilter.=" AND rtm.waterf='$_wf'";}
	
	if ( $closing>0  )  {$year_where=""; $_newFilter.=" AND ( date(rtm.closingdt)>= DATE_SUB( curdate(), interval '".($closing*30)."' day)) "; }
	else if($ClosingDT_From>0 && $ClosingDT_To>0){$year_where=""; $_newFilter.=" AND (rtm.closingdt>=".$ClosingDT_From." and rtm.closingdt<=".$ClosingDT_To." )"; }

	if ( $ldate>0  )    {$year_where=""; $_newFilter.=" AND ( date(rtm.Ldate)>= DATE_SUB( curdate(), interval '".($ldate*30)."' day)) "; }
	else if($Ldate_From>0 && $Ldate_To>0){$year_where=""; $_newFilter.=" AND (rtm.Ldate>=".$Ldate_From." and rtm.Ldate<=".$Ldate_To." )"; }
	
	if ($subd > 0 && strlen(trim($com_subd))>0) {
		$_newFilter.=" AND `psummary`.sdname = '".$com_subd."' ";
	}
	
	if($maplatlong!=0){
		$maplatlong=explode('/',$maplatlong);
		if(count($maplatlong)==2){
			$maplatlong1=explode(',',$maplatlong[0]);
			$maplatlong2=explode(',',$maplatlong[1]);
			if($maplatlong1[0]>$maplatlong2[0])
				$_newFilter.=" AND (ll.latitude>=".$maplatlong2[0]." and ll.latitude<=".$maplatlong1[0];
			else
				$_newFilter.=" AND (ll.latitude>=".$maplatlong1[0]." and ll.latitude<=".$maplatlong2[0];
			if($maplatlong1[1]>$maplatlong2[1])
				$_newFilter.=" and ll.longitude>=".$maplatlong2[1]." and ll.longitude<=".$maplatlong1[1].")";
			else
				$_newFilter.=" and ll.longitude>=".$maplatlong1[1]." and ll.longitude<=".$maplatlong2[1].")";
		}
		elseif(count($maplatlong)>2){
			$latmax=0;
			$longmax=-1000;
			$latmin=1000;
			$longmin=1000;
			$arrPoly=array();
			
			foreach($maplatlong as $k => $val){
				$aux=explode(',',$val);
				
				if($latmax<$aux[0]) $latmax=$aux[0];
				if($latmin>$aux[0]) $latmin=$aux[0];
				
				if($longmax<$aux[1]) $longmax=$aux[1];
				if($longmin>$aux[1]) $longmin=$aux[1];
				$arrPoly[]=array("lat"=>$aux[0],"long"=>$aux[1]);
			}
			
			$_newFilter.=" AND (ll.latitude>=".$latmin." and ll.latitude<=".$latmax;
			$_newFilter.=" and ll.longitude>=".$longmin." and ll.longitude<=".$longmax.")";				
		}
	}
	
	if(isset($_POST['no_filter']) && $_POST['no_filter']==true) $_newFilter="";
//=================================================================================================



//===========Campos a visualizar===============
//=================================================================================================
if($bpoaction){
	$sql_fields="Select
	rtmaster.*,
	rtmaster.saleprice as rsaleprice,
	mlsresidential.mlnumber,
	mlsresidential.orgprice,
	mlsresidential.dom,
	mlsresidential.apxtotsqft,
	mlsresidential.carportd,
	mlsresidential.carportq,
	psummary.buildingv,
	psummary.ccoded,
	psummary.landv,
	psummary.stories,
	psummary.taxablev,
	psummary.units,
	psummary.unit,
	(rtmaster.saleprice/rtmaster.lsqft) as price_sqft,
	marketvalue.*,
	latlong.latitude,
	latlong.longitude
	FROM rtmaster LEFT JOIN (marketvalue,latlong,mlsresidential,psummary) ON
	(rtmaster.parcelid  = marketvalue.parcelid and rtmaster.parcelid = latlong.parcelid and rtmaster.parcelid  = mlsresidential.parcelid and psummary.parcelid=rtmaster.parcelid ) And  rtmaster.xcode='$proper'  limit 0,1";
	
	$result = mysql_query($sql_fields) or die($sql_fields.mysql_error());
	
	$i = 0;
	while ($i < mysql_num_fields($result)) {
	
		$meta = mysql_fetch_field($result, $i);
			
			$array_campos[]=array(0 => strtolower($meta->name),1 => $meta->type);
	
		$i++;
	}
	
	mysql_free_result($result);
}
//=================================================================================================



//===========Comparables===============
//=================================================================================================
	//===========distancia principal===============
	
	//=============================================================================================
if($bpoaction){	
		$sql_0_5="SELECT
		rtm.parcelid,
		rtm.address,
		rtm.lsqft,
		rtm.larea,
		rtm.zip,
		rtm.beds,
		rtm.bath,
		rtm.pool,
		rtm.waterf,
		rtm.yrbuilt,
		rtm.closingdt,
		rtm.ldate,
		rtm.type,
		rtm.saleprice,
		rtm.lprice,
		rtm.Status,
		rtm.city,
		rtm.tsqft,
		$custom_sel
		psummary.buildingv,
		psummary.ccoded,
		psummary.landv,
		psummary.stories,
		psummary.taxablev,
		psummary.units,
		psummary.unit,
		mlsresidential.un,
		mlsresidential.mlnumber,
		mlsresidential.orgprice,
		mlsresidential.dom,
		mlsresidential.apxtotsqft,
		mlsresidential.carportd,
		mlsresidential.carportq,
		marketvalue.Pendes,
		marketvalue.marketvalue,
		marketvalue.marketmedia,
		marketvalue.marketpctg,
		marketvalue.offertmedia,
		marketvalue.offertpctg,
		marketvalue.offertvalue,
		marketvalue.debttv,
		marketvalue.sold,
		ll.latitude,
		ll.longitude,
		$sqlpriceselect as price_sqft,
		$_calculo as Distance,
		rtm.lsqft 
		FROM rtmaster as rtm 
		LEFT JOIN (mlsresidential) ON (rtm.parcelid=mlsresidential.parcelid)
		LEFT JOIN (psummary) ON (rtm.parcelid=psummary.parcelid)
		LEFT JOIN (latlong as ll) ON (rtm.parcelid=ll.parcelid)
		LEFT JOIN (marketvalue) ON (rtm.parcelid=marketvalue.parcelid)
		Where  rtm.xcode='$proper' and rtm.parcelid<>'$el_comparado' AND ($stat)  ";
		
		$params_0_1="$price_where $beds_where  $sqft_1_1__0_9 $year_where #filters And (ll.latitude>=".$latmin." and ll.latitude<=".$latmax." and ll.longitude>=".$lonmin." and ll.longitude<=".$lonmax.")";
		
		if(isset($_POST['sort'])) $params_0_1.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		else $params_0_1.=" order by mlsresidential.lsqft";
}else{
	include("properties_getgridcamptit.php");
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
	if ($_POST["status"]=="A")
		$ArIDCT = getArray('active','comparables',false);
	else{
		if(isset($_POST['userid']) || isset($_POST['report'])) $ArIDCT = getArray('comp','comparables',false);
		else $ArIDCT = array(45,47,46,9,55,53);
	}
	
	if($permission['realtor_esp']==1) $ArIDCT[0]=97;
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$orderby='';
	$limit='';
	$jointable=array('`rtmaster`','`properties_php`','`psummary`','`latlong`','`marketvalue`','`diffvalue`');
	$campos='p.parcelid as pid, concat("_",rtm.status,"-",p.pendes,if(`marketvalue`.sold="S",concat("-",`marketvalue`.sold),"")) as status, p.latitude as pin_xlat, p.longitude as pin_xlong, p.address as pin_address, rtm.lsqft as pin_lsqft, rtm.larea as pin_larea, rtm.beds as pin_bed, rtm.bath as pin_bath, rtm.'.$campo_price.' as pin_saleprice, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip';
	
	foreach($hdArray as $k => $val){
		if(array_search('`'.$val->tabla.'`',$jointable)===false)
			$jointable[]='`'.$val->tabla.'`';
		if($val->tabla=='rtmaster')
			$campos.=', rtm.'.$val->name;
		elseif($val->tabla=='latlong')
			$campos.=', ll.'.$val->name;
		else
			$campos.=', `'.$val->tabla.'`.'.$val->name;
		
		if(isset($_POST['sort']) && ($val->name==$_POST['sort'])){
			if($val->tabla=='rtmaster') $orderby=' ORDER BY rtm.'.$val->name.' '.$_POST['dir'];
			elseif($val->tabla=='latlong') $orderby=' ORDER BY ll.'.$val->name.' '.$_POST['dir'];
			else $orderby=' ORDER BY `'.$val->tabla.'`.'.$val->name.' '.$_POST['dir'];
		}
	}
	if(!isset($_POST['sort'])) $orderby=' ORDER BY Distance ASC';
	if(isset($_POST['sort']) && ($_POST['sort']=='Distance' || $_POST['sort']=='status')) $orderby=' ORDER BY Distance '.$_POST['dir'];
	
	$sql_0_5="SELECT ".$campos.", $_calculo as Distance FROM ";
	foreach($jointable as $k => $val){
		if($k==0) $sql_0_5.="$val rtm ";
		elseif($val=='`properties_php`') $sql_0_5.="LEFT JOIN $val p ON (rtm.parcelid=p.parcelid) ";
		elseif($val=='`latlong`') $sql_0_5.="LEFT JOIN $val ll ON (rtm.parcelid=ll.parcelid) ";
		else $sql_0_5.="LEFT JOIN $val ON (rtm.parcelid=$val.parcelid) ";
	}
	
	$sql_0_5.=" WHERE rtm.parcelid<>'$el_comparado' AND rtm.xcode='$proper' AND ($stat)";
	
	if($_POST['array_no_taken']!='' && $_POST['array_no_taken']!="''")
		$sql_0_5.=" AND rtm.parcelid NOT IN (".$_POST['array_no_taken'].")";
	
	$sql_0_5.=" $price_where $beds_where  $sqft_1_1__0_9 $year_where $_newFilter And (ll.latitude>=".$latmin." and ll.latitude<=".$latmax." and ll.longitude>=".$lonmin." and ll.longitude<=".$lonmax.")";
	
	$sql_0_5.=$orderby;
	
	if($_POST['array_taken']!='' && $_POST['array_taken']!="''"){
		$sql_0_5="SELECT ".$campos.", $_calculo as Distance FROM ";
		foreach($jointable as $k => $val){
			if($k==0) $sql_0_5.="$val rtm ";
			elseif($val=='`properties_php`') $sql_0_5.="LEFT JOIN $val p ON (rtm.parcelid=p.parcelid) ";
			elseif($val=='`latlong`') $sql_0_5.="LEFT JOIN $val ll ON (rtm.parcelid=ll.parcelid) ";
			else $sql_0_5.="LEFT JOIN $val ON (rtm.parcelid=$val.parcelid) ";
		}
		
		$sql_0_5.=" WHERE rtm.parcelid IN (".$_POST['array_taken'].")";
		
		if($_POST['array_no_taken']!='' && $_POST['array_no_taken']!="''")
			$sql_0_5.=" AND rtm.parcelid NOT IN (".$_POST['array_no_taken'].")";
		
		$sql_0_5.=" AND ($stat)";
		$sql_0_5.=$orderby;     
	}
	$params_0_1='';
	
	$_llaves=array("#lat","#lon","#lsq");
	$_values=array($lat,$lon,$campo_cal_compar);
	$sql_0_5=str_replace($_llaves, $_values,$sql_0_5);
	//if($_POST['userid']==73) echo $sql_0_5; 
}
	//=============================================================================================			
		

//===========Ejecucion===============
//=================================================================================================
	//===========Guardar data===============
	//=============================================================================================
	if($bpoaction){	
		$_llaves=array("#lat","#lon","#lsq","#filters");
		$_values=array($lat,$lon,$campo_cal_compar,$_newFilter);

		if($_POST['array_taken']!='' && $_POST['array_taken']!="''"){
			$sql_0_5="SELECT
			rtm.parcelid,
			rtm.address,
			rtm.lsqft,
			rtm.larea,
			rtm.zip,
			rtm.beds,
			rtm.bath,
			rtm.pool,
			rtm.waterf,
			rtm.yrbuilt,
			rtm.closingdt,
			rtm.ldate,
			rtm.type,
			rtm.saleprice,
			rtm.lprice,
			rtm.Status,
			rtm.city,
			rtm.tsqft,
			$custom_sel
			psummary.buildingv,
			psummary.ccoded,
			psummary.landv,
			psummary.stories,
			psummary.taxablev,
			psummary.units,
			psummary.unit,
			mlsresidential.un,
			mlsresidential.mlnumber,
			mlsresidential.orgprice,
			mlsresidential.dom,
			mlsresidential.apxtotsqft,
			mlsresidential.carportd,
			mlsresidential.carportq,
			marketvalue.Pendes,
			marketvalue.marketvalue,
			marketvalue.marketmedia,
			marketvalue.marketpctg,
			marketvalue.offertmedia,
			marketvalue.offertpctg,
			marketvalue.offertvalue,
			marketvalue.debttv,
			marketvalue.sold,
			ll.latitude,
			ll.longitude,
			$sqlpriceselect as price_sqft,
			$_calculo as Distance,
			rtm.lsqft 
			FROM rtmaster as rtm 
			LEFT JOIN (mlsresidential) ON (rtm.parcelid=mlsresidential.parcelid)
			LEFT JOIN (psummary) ON (rtm.parcelid=psummary.parcelid)
			LEFT JOIN (latlong as ll) ON (rtm.parcelid=ll.parcelid)
			LEFT JOIN (marketvalue) ON (rtm.parcelid=marketvalue.parcelid)
			where rtm.parcelid IN (".str_replace("\'","'",$_POST['array_taken']).") ";
			//echo $sql_0_5;
			
			$params_0_1='';
		}


		$params_0_1 = str_replace($_llaves, $_values, $params_0_1);
		$sql_0_5 = str_replace($_llaves, $_values, $sql_0_5);
		
		$params_0_2 = str_replace($_llaves, $_values, $params_0_2);
		$sql_1_0 = str_replace($_llaves, $_values, $sql_1_0);
	}

	if($_POST['array_taken']!='' && $_POST['array_taken']!="''"){
		saveData($sql_0_5,$params_0_1,'false','true');
	}else{
		saveData($sql_0_5,$params_0_1,'true','true');
	}
	//=============================================================================================
	
	
	//===========Creacion de vFilas===============
	//=============================================================================================
		$lat=0;
		$lon=0;
		$num_rows=0;
		$xmls="";
		
		//usort($MainData, 'compare');
		//usort($data_arr, 'compare');
			
		$array_parcelIds=array();
		$array_datos=array();
		if($bpoaction){ 
			$va_pid='parcelid'; 
			$va_lat='latitude';
			$va_lon='longitude';
		}else{ 
			$va_pid='pid';
			$va_lat='pin_xlat';
			$va_lon='pin_xlong';
		}
		
		if ($_POST["status"]=="A") $campo_price="lprice";else $campo_price="saleprice";
		
		for($j=0;$j<count($MainData);$j++)//recorremos el array de data
		{
			$lat=$MainData[$j][$va_lat];$lon=$MainData[$j][$va_lon];
			if(is_null($lat)) $lat=0;if(is_null($lon)) $lon=0;

			$asignar=true;
			//guardamos el array de datos en nuestro array - requeirdo para [Maria Olivera]
			if($maplatlong!=0){
				if(count($maplatlong)>2){
					if($lat>0 && _pointINpolygon($arrPoly,$lat,$lon)){
						array_push($array_parcelIds,$MainData[$j][$va_pid]);
					}else{
						$asignar=false;
					}
				}else{
					array_push($array_parcelIds,$MainData[$j][$va_pid]);
				}
			}else{
				array_push($array_parcelIds,$MainData[$j][$va_pid]);
			}
			//////////////////////////////////////
			
			if (strlen($lat)>0 && $asignar)	
				$array_datos[]=$MainData[$j];
				
			if ($bpoaction && strlen($lat)>0 && $asignar)
			{
				$xmls="http://www.reifax.com/img/nophoto.gif";			
				
				if ($num_rows>0)
				{
					$Filas.=",";
				}
		
						
				//se crea el array maestro para javascript desde php				
						
						if ($data_arr[$j]["comparable"]==$MainData[$j]["parcelid"]) $distan=$data_arr[$j]["distance"];
						if ($data_arr[$j]["comparable"]==$MainData[$j]["parcelid"]) $take=$data_arr[$j]["taken"];
						
						if(isset($_POST['filterA'])) $take='true';
		
			for ($i=0;$i<count($array_campos);$i++)
			{
				
				if ($i==0)
					{
						if($distan==0) $distan='0.00';
						$good_price=(empty($MainData[$j][$campo_price])) ? 0:$MainData[$j][$campo_price];
						$Filas.="{\"xlat\" : ".$lat.",\"xlong\" : ".$lon.
						",\"ind\": ".($num_rows+1). //controla la pocision de los registros, 
						",\"id\": \"".$MainData[$j]["parcelid"].
						"\",\"mls\": \"".$xmls.
						"\",\"sold\": \"".$MainData[$j]["sold"].
						"\",\"_beds\": ".$MainData[$j]["beds"].
						",\"_bath\": ".$MainData[$j]["bath"].
		
						",\"x_price\": ".$good_price.
						",\"status\": \"".$MainData[$j]["Status"].
						"\",\"pendes\": \"".$MainData[$j]["Pendes"].
						"\",\"distance\": ".$distan.
						",\"taken\": ".$take.
						",\"un\": \"".$MainData[$j]["un"].
						"\",\"mlnumber\": \"".$MainData[$j]["mlnumber"]."\",";
		
					}
				if ($i>0)
					{
						if($nameField<>'pendes' && $nameField<>'status')
							$Filas.=",";
					}
		
			
				unset($no_null_data);
				$nameField=strtolower($array_campos[$i][0]);
				$elType=$array_campos[$i][1];
				if ($elType=="string") $no_null_data=""; else $no_null_data=0;
				//evaluar los tipos de data;
				if (!is_null($MainData[$j][$array_campos[$i][0]])) $no_null_data=$MainData[$j][$array_campos[$i][0]];
				switch (true)
				{
					case $elType=="string" || $elType=="date":
						if($nameField<>'pendes'&& $nameField<>'status')// && $nameField<>'mlnumber' 
						//if($nameField<>'un')
						$Filas.="\"".$nameField."\": \"".$no_null_data."\"";	
						break;
					case "real":
						if($nameField<>'pendes'&& $nameField<>'status')// && $nameField<>'mlnumber' 
						//if($nameField<>'un')
						$Filas.="\"".$nameField."\": ".$no_null_data;
						break;
					case "int":
						if($nameField<>'pendes'&& $nameField<>'status') //&& $nameField<>'mlnumber' 
						//if($nameField<>'un')
						$Filas.="\"".$nameField."\": ".$no_null_data;
						break;
				}
		
		
		
			}
		$Filas.="}";
			
			
						 
				$num_rows++;	
			}
		
		}
	//=============================================================================================
	
	
	//===========Creacion de diffValue===============
	//=============================================================================================
	if($bpoaction){	
		for($i=0;$i<count($array_parcelIds);$i++)//recorremos el array de s
		{
		$xSql="Select diffvalue.* FROM diffvalue where diffvalue.parcelID='$array_parcelIds[$i]'";
		$result = mysql_query($xSql) or die (mysql_error());
		$row=mysql_fetch_array($result, MYSQL_ASSOC);
			
			if(is_null($row["ParcelID"]))		$parcelid=""; else $parcelid=$row["ParcelID"];
			if(is_null($row["Bath"]))			$bath=""; else $bath=$row["Bath"];
			if(is_null($row["Beds"]))			$beds=""; else $beds=$row["Beds"];
			if(is_null($row["ClosingDT"]))		$closingdt=""; else $closingdt=$row["ClosingDT"];
			if(is_null($row["Lsqft"]))			$lsqft=""; else $lsqft=$row["Lsqft"];
			if(is_null($row["Larea"]))			$larea=""; else $larea=$row["Larea"];
			if(is_null($row["Pool"]))			$pool=""; else $pool=$row["Pool"];
			if(is_null($row["SalePrice"]))		$saleprice=""; else $saleprice=$row["SalePrice"];
			if(is_null($row["TSqft"]))			$tsqft=""; else $tsqft=$row["TSqft"];
		
			if(is_null($row["Value"]))			$value=""; else $value=$row["Value"];
			if(is_null($row["Zip"]))			$zip=""; else $zip=$row["Zip"];
			if(is_null($row["waterf"]))			$waterf=""; else $waterf=$row["WaterF"];
		
			if ($i>0) $Diff.=",";		
				$Diff.= 
				"'".$array_parcelIds[$i]."':".
				"{\"parcelid\":\"".str_replace($_quitar,"",$parcelid). 
				"\",\"bath\":\"".str_replace($_quitar,"",$bath).
				"\",\"beds\":\"".str_replace($_quitar,"",$beds).
				"\",\"closingdt\":\"".str_replace($_quitar,"",$closingdt).
				"\",\"lsqft\":\"".str_replace($_quitar,"",$lsqft).
				"\",\"larea\":\"".str_replace($_quitar,"",$larea).
				"\",\"pool\":\"".str_replace($_quitar,"",$pool).
				"\",\"saleprice\":\"".str_replace($_quitar,"",$saleprice).
				"\",\"tsqft\":\"".str_replace($_quitar,"",$tsqft).
				"\",\"value\":\"".str_replace($_quitar,"",$value).
				"\",\"zip\":\"".str_replace($_quitar,"",$zip).
				"\",\"waterf\":\"".str_replace($_quitar,"",$waterf)."\"}";
			
			mysql_free_result($result);
		}// end de For
		$Diff="{".$Diff."}";
	//=============================================================================================
	
	
	//===========Creacion de Camptit===============
	//=============================================================================================
		//mysql_select_db("xima");
		$rSql="SELECT camptit.* FROM xima.camptit";
		$result = mysql_query($rSql) or die (mysql_error());
		$i=0;
		$array_campos=array();
		while($row=mysql_fetch_array($result, MYSQL_ASSOC)){
			if ($i>0) $Camptit.=",";		
				$Camptit.= 
				"{\"table\":\"".strtolower($row["Tabla"]). 
				"\",\"title\":\"".$row["Titulos"].  
				"\",\"field\":\"".strtolower($row["Campos"]).
				"\",\"desc\":\"".strtolower($row["Desc"]).  
				"\"}";
			$i++;
		}
	}
	//=============================================================================================
//=============================================================================================
	}
?>