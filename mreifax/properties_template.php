<?php 
$_SERVERXIMA="http://www.reifax.com/";
//if(isset($_GET['webuser'])){$r=true;}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES CON MAPA
	function tagHeadHeader1()
	{?> 
    	<title>REIFAX | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
         <meta content='IE=8' http-equiv='X-UA-Compatible'/>
<?php
		if(isset($_COOKIE['datos_usr']['cleancache']) && $_COOKIE['datos_usr']['cleancache']=='Y')
		{
?>		
		<meta http-equiv="cache-control" content="no-cache"> 
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<?php
		}
?> 
        <meta http-equiv="Content-Language" content="EN-US"> 
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/favicon.png">
		
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.3"></script>
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.Word.js"></script>
      <!-- data view plugins -->
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/DataView-more.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/data-view.css"/>
    <!-- fileuploadinput -->
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/FileUploadField.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/fileuploadfield.css"/>
    
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />

		<link href="http://www.reifax.com/includes/css/layout2.css" rel="stylesheet" type="text/css" />
		<link href="http://www.reifax.com/includes/css/properties_css.css" rel="stylesheet" type="text/css" />
	<script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/geo.js"></script>	
<?php 	//handlingError();
	}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES SIN MAPA
	function tagHeadHeader2()
	{
?>  	
        <title>REIFAX | Foreclosures, Foreclosure, Foreclosed Homes, Foreclosure Listings, REO, Repos, Florida</title>
        <meta content='IE=8' http-equiv='X-UA-Compatible'/> 
        <meta http-equiv="Content-Language" content="EN-US">   
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/favicon.png">
		
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
        <script type="text/javascript" src="http://www.reifax.com/MANT/lib/ext/examples/form/plugins/Ext.ux.form.HtmlEditor.Word.js"></script>
         <!-- data view plugins -->
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/DataView-more.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/data-view.css"/>
    <!-- fileuploadinput -->
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ux/FileUploadField.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/ux/fileuploadfield.css"/>
    

		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />

		<link href="http://www.reifax.com/includes/css/layout2.css" rel="stylesheet" type="text/css" />
		<link href="http://www.reifax.com/includes/css/properties_css.css" rel="stylesheet" type="text/css" />
    <script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="http://www.reifax.com/MANT/LIB/geo.js"></script>    
<?php 	//handlingError();
	}

	//DEFINICIONES EN EL TAG DE <HEAD> COMUNES SIN MAPA
	function tagHeadHeader3()
	{
?>  	
        <meta content='IE=8' http-equiv='X-UA-Compatible'/> 
        <meta http-equiv="Content-Language" content="EN-US">   
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="keywords" content="Foreclosures, foreclosure, foreclosed Homes, foreclosure properties, foreclosure listings, short sales, buy foreclosures, repo homes, reo properties, florida foreclosure, florida foreclosures, foreclosure sales, tax foreclosures, reo, miami, florida, properties" />
		<meta name="description" content="Research foreclosed homes and foreclosure listings by City or by County to find best foreclosure deals in your area. Identify properties with over 30% equity. REIFAX, a web-based property search system" />
		<meta name="title" content="REI Property Fax - Foreclosure Listings">
		<meta name="DC.Title" content="Florida foreclosure, Foreclosures, foreclosure, foreclosed homes, foreclosure listings, short sale, reo, Miami, Florida">
		<meta name="DC.Subject" content="Foreclosures, foreclosure, foreclosed, homes, foreclosed homes, buy foreclosures, short sale, homes foreclosures, bank foreclosures, foreclosure home, county foreclosures, bank owned, miami, ft lauderdale, dade, broward, palm beach, florida">
		<meta name="DC.Description" content="Research foreclosed properties and foreclosure listings, by city or by county, investment property, home equity. Identify properties with over 30% equity. Find best deals in your area using REIFAX web-based property search system">
		<meta name="robots" content="all">
		<meta name="rating" content="General">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="2 days">
		<meta name="category" content="Real Estate Search">
		<meta name="author" content="Francisco Mago">
		<meta name="reply-to" content="info@reifax.com">
		<meta name="copyright" content="REI Property Fax, 2010">
		<meta name="expires" content="never">
		<meta name="generator" content="Microsoft FrontPage 5.0">  
		<link rel="shortcut icon" type="image/ico" href="https://www.reifax.com/favicon.png">
		
		<script type="text/javascript" src="https://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="https://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
        <script type="text/javascript" src="https://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
    

		<link rel="stylesheet" type="text/css" href="https://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="https://www.reifax.com/MANT/LIB/ext/examples/ux/css/ux-all.css" />
		<link rel="stylesheet" type="text/css" href="https://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />

		<link href="https://www.reifax.com/includes/css/layout2.css" rel="stylesheet" type="text/css" />
		<link href="https://www.reifax.com/includes/css/properties_css.css" rel="stylesheet" type="text/css" />
    <script src="https://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="https://www.reifax.com/MANT/LIB/geo.js"></script>    
<?php 	//handlingError();
	}
	
	//TOP HEADER: logo xima xima y links de Register Now! | Log In | Log Out | About Us | Contact Us | Training | Tutorial
	function topLoginHeader()
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		
		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
				$result=mysql_query($que) or die($que.mysql_error());
				$r=mysql_fetch_array($result);
				$htmls=$r['master_htmls'];?>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
                </table>
                <p>&nbsp;</p>
                               
				<? }else{   ?>        
			<div align="center" style=" <?php if (!$realtor && !$investor && !$webmaster){?>height:80px;<? }else{?>height:160px;<? }?> width:100%; position:relative;">
            	<div align="left" style="height:23px; line-height:23px; padding-left:2px; padding-right:10px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;"><?php if (!$realtor && !$investor && !$webmaster){ ?>
                        	<a href="http://www.reifax.com/properties_register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                            
                        	<a href="#" onClick="session_release(); return false;" title="Log Off">Log Off</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<!--<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" ></a>
                            <span style="color:#999999; ">|&nbsp;</span>-->
                        	<a href="properties_search.php">Go Search</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                          <?php if ($realtor || $investor || $webmaster){ ?><li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="">      </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li><? } ?>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.reifax.com/properties_aboutus.php">About Us</a><? } else {?> <a href="http://www.reifax.com/properties_aboutus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">About Us</a><?php } ?>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/properties_contactus.php">Contact Us</a><? } else {?> <a href="https://www.reifax.com/properties_contactus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">Contact Us</a><?php } ?>
                           <!--<span style="color:#999999; ">|&nbsp;</span>-->
                        </li>
						<!--
							<li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.reifax.com/properties_training.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.ximausa.com/FLORIDA/FLORIDA.html" target="_blank">Florida</a><?php } ?>
                        </li>-->
                    </ul>
                </div>
                <div style=" padding-bottom:20px; padding-top:20px;">
                       <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                        <?php
							 if ($realtor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Realtor Web">
                   <?php } elseif ($investor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Investor Web">
                    <?php } elseif ($webmaster){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Webmaster Web">
                  <?php }else{?> <img src="img/foreclosures-foreclosed-homes1.png" width="300" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos"><?php } ?></a> 
                </div>
                                
          <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
          <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:0px; right:1px;  margin: -2px 0px 0px 0px; "> <?php if ($realtor || $investor || $webmaster){ ?><img src="http://www.reifax.com/<?php echo "$photo";?>" height="120" alt="Realtor Web " /><? }?></div><div id="sdSlBh"></div><?php if (!$realtor && !$investor && !$webmaster){ ?> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <?php }?><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
			<!-- END ProvideSupport.com Graphics Chat Button Code -->
            </div>
<?php 
 } }

	//TOP HEADER: logo xima xima y links de Register Now! | Log In | Log Out | About Us | Contact Us | Training | Tutorial
	function topLoginHeader2()
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid,$webmaster, $webmasterid;
		
		 
?>
			<div align="center" style=" <?php if (!$realtor && !$investor && !$webmaster){?>height:80px;<? }else{?>height:160px;<? }?> width:100%;">
            	<div align="left" style="height:23px; line-height:23px; padding-left:2px; padding-right:10px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;">
                        	<a href="http://www.reifax.com/properties_register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        	<a href="#" onClick="session_release(); return false;" title="Log Off">Log Off</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.reifax.com/properties_aboutus.php">About Us</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.reifax.com/properties_contactus.php">Contact Us</a>
                            <!--<span style="color:#999999; ">|&nbsp;</span>-->
                        </li>
                        <!--<li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.reifax.com/properties_training.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="http://www.ximausa.com/FLORIDA/FLORIDA.html" target="_blank">Florida</a>
                        </li>-->
                    </ul>
                </div>
                 <div style=" padding-bottom:20px; padding-top:20px;">
                       <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                        <?php
							 if ($realtor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" width="142" height="60" alt="Company Logo">
                   <?php } elseif ($investor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Investor Web">
                                <?php } elseif ($webmaster){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Webmaster Web">
                  <?php }else {?> <img src="img/foreclosures-foreclosed-homes1.png" width="300" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos"><?php } ?></a> 
                </div>
                                
          <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
          <div id="ciSlBh"  style="z-index:100; position: relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat; background-position:top center; padding:5px 5px 5px 5px; z-index:200; position: absolute; top:0px; right:1; margin: -2px 0px 0px 0px; "> <?php if ($realtor || $investor || $webmaster){ ?><img src="http://www.reifax.com/<?php echo "$photo";?>" width="70" height="70" alt="Realtor Web" /><? }?></div><div id="sdSlBh"></div><?php if (!$realtor && !$investor && !$webmaster){ ?> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <?php }?><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
			<!-- END ProvideSupport.com Graphics Chat Button Code -->
<?php 
	 }

	//TOP HEADER: logo xima xima y links de Register Now! | Log In | Log Out | About Us | Contact Us | Training | Tutorial
	function topLoginHeader3()
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		
		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
				$result=mysql_query($que) or die($que.mysql_error());
				$r=mysql_fetch_array($result);
				$htmls=$r['master_htmls'];?>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
                </table>
                <p>&nbsp;</p>
                               
				<? }else{   ?>        
			<div align="center" style=" <?php if (!$realtor && !$investor && !$webmaster){?>height:80px;<? }else{?>height:160px;<? }?> width:100%; position:relative;">
            	<div align="left" style=" height:23px; line-height:23px; padding-left:2px; padding-right:10px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <?php /*?><li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;"><?php if (!$realtor && !$investor && !$webmaster){ ?>
                        	<a href="https://www.reifax.com/properties_register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li><?php */?>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                            
                        	<a href="#" onClick="session_release(); return false;" title="Log Off">Log Off</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<!--<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" ></a>
                            <span style="color:#999999; ">|&nbsp;</span>-->
                        	<a href="properties_search.php">Go Search</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                          <?php if ($realtor || $investor || $webmaster){ ?><li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="">      </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li><? } ?>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/properties_aboutus.php">About Us</a><? } else {?> <a href="https://www.reifax.com/properties_aboutus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">About Us</a><?php } ?>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/properties_contactus.php">Contact Us</a><? } else {?> <a href="https://www.reifax.com/properties_contactus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">Contact Us</a><?php } ?>
                            <!--<span style="color:#999999; ">|&nbsp;</span>-->
                        </li>
                        <!--
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/properties_training.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/FLORIDA/FLORIDA.html" target="_blank">Florida</a><?php } ?>
                        </li>
                        -->
                    </ul>
                </div>
                <div style=" padding-bottom:20px; padding-top:20px;">
                       <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                        <?php
							 if ($realtor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="https://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Realtor Web">
                   <?php } elseif ($investor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="https://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Investor Web">
                    <?php } elseif ($webmaster){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="https://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Webmaster Web">
                  <?php }else{?> <img src="https://www.reifax.com/img/foreclosures-foreclosed-homes1.png" width="300" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos"><?php } ?>
						</a> 
                </div>
                                
          <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
          <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:0px; right:1px;  margin: -2px 0px 0px 0px; "> <?php if ($realtor || $investor || $webmaster){ ?><img src="https://www.reifax.com/<?php echo "$photo";?>" height="120" alt="Realtor Web " /><? }?></div><div id="sdSlBh"></div><?php if (!$realtor && !$investor && !$webmaster){ ?> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("https")==0?"https://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <?php }?><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
			<!-- END ProvideSupport.com Graphics Chat Button Code -->
            </div>
<?php 
 } }


	//TOP HEADER: About Us | Contact Us | Training | Tutorial
	function topLoginHeader4()
	{
		global $_SERVERXIMA, $realtor, $realtorid, $investor, $investorid, $webmaster, $webmasterid;
		
		if($webmaster){
		 	$que="SELECT master_htmls FROM xima.ximausrs WHERE userid=".$webmasterid;
				$result=mysql_query($que) or die($que.mysql_error());
				$r=mysql_fetch_array($result);
				$htmls=$r['master_htmls'];?>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                <td height="100%" valign="top"><strong><? echo $htmls;?></strong></td>
                </tr>
                </table>
                <p>&nbsp;</p>
                               
				<? }else{   ?>        
			<div align="center" style=" <?php if (!$realtor && !$investor && !$webmaster){?>height:80px;<? }else{?>height:160px;<? }?> width:100%; position:relative;">
            	<div align="left" style=" height:23px; line-height:23px; padding-left:2px; padding-right:10px;">
                	<ul style="list-style-image:none; list-style-position:outside; list-style-type:none; font-size:12px; text-align:center;">
                        <?php /*?><li id="top_m_reg" style="line-height:19px; padding:0 1px; margin-left:10px; font-weight:bold; float:left;"><?php if (!$realtor && !$investor && !$webmaster){ ?>
                        	<a href="http://www.reifax.com/properties_register.php">Register <span style="color:#F00;">Now!</span></a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li id="top_m_login" style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="#" onClick="login_win.show(); return false;">Log In</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li><?php */?>
                        <li id="top_m_user" style=" display:none;line-height:19px; padding:0 1px; margin-left:5px; font-weight:bold; float:left; ">
                        	<a id="top_m_user_name" href="#" onclick="tabs.setActiveTab(0); if(tabs3) tabs3.setActiveTab(0);" style="text-decoration:underline; color: #0000FF" title="My Account">
                            </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                            
                        	<a href="#" onClick="session_release(); return false;" title="Log Off">Log Off</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li id="top_m_logout" style=" display:none;line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<!--<a href="#" onClick="session_release(); return false;"><img src="img/logoff.gif" ></a>
                            <span style="color:#999999; ">|&nbsp;</span>-->
                        	<a href="properties_search.php">Go Search</a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                          <?php if ($realtor || $investor || $webmaster){ ?><li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<a href="">      </a>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li><? } ?>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.reifax.com/properties_aboutus.php">About Us</a><? } else {?> <a href="http://www.reifax.com/properties_aboutus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">About Us</a><?php } ?>
                            <span style="color:#999999; ">|&nbsp;</span>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="https://www.reifax.com/properties_contactus.php">Contact Us</a><? } else {?> <a href="https://www.reifax.com/properties_contactus.php<?php if ($realtor){ echo "?webuser=".$realtorid;} if ($investor){ echo "?webowner=".$investorid;}?>">Contact Us</a><?php } ?>
                            <!--<span style="color:#999999; ">|&nbsp;</span>-->
                        </li>
                        <!--
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.reifax.com/properties_training.php">Training</a>
                            <span style="color:#999999; ">|&nbsp;</span><?php } ?>
                        </li>
                        <li style="line-height:19px; padding:0 1px; margin-left:0; float:left;">
                        	<?php if (!$realtor && !$investor && !$webmaster){ ?><a href="http://www.reifax.com/FLORIDA/FLORIDA.html" target="_blank">Florida</a><?php } ?>
                        </li>
                        -->
                    </ul>
                </div>
                <div style=" padding-bottom:20px; padding-top:20px;">
                       <a href="<?php echo $_SERVERXIMA;?>" target="_self">
                        <?php
							 if ($realtor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$realtorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Realtor Web">
                   <?php } elseif ($investor){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$investorid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Investor Web">
                    <?php } elseif ($webmaster){ 
							 $query='SELECT * FROM xima.ximausrs WHERE userid='.$webmasterid;
                             $result = mysql_query($query) or die($query.mysql_error());
                                 $datos_usr=mysql_fetch_array($result);
								$photo=$datos_usr['profimg'];
								$photo2=$datos_usr['companyimg'];?><img src="http://www.reifax.com/<?php echo "$photo2";?>" height="130" alt="Company Webmaster Web">
                  <?php }else{?> <img src="img/foreclosures-foreclosed-homes1.png" width="300" alt="REIFAX, Foreclosure listings, foreclosed homes, bank foreclosures, county foreclosures, homes foreclosure, short sale, homes with equity, repos"><?php } ?></a> 
                </div>
                                
          <!-- BEGIN ProvideSupport.com Graphics Chat Button Code-->
          <div id="ciSlBh"  style="z-index:100; position:relative; left:1px; "> </div><div id="scSlBh" style=" no-repeat;  padding:5px 5px 5px 5px; z-index:200; position:absolute; top:0px; right:1px;  margin: -2px 0px 0px 0px; "> <?php if ($realtor || $investor || $webmaster){ ?><img src="http://www.reifax.com/<?php echo "$photo";?>" height="120" alt="Realtor Web " /><? }?></div><div id="sdSlBh"></div><?php if (!$realtor && !$investor && !$webmaster){ ?> <script type="text/javascript">var seSlBh=document.createElement("script");seSlBh.type="text/javascript";var seSlBhs=(location.protocol.indexOf("http")==0?"http://secure.providesupport.com/image":"http://image.providesupport.com")+"/js/ximausa/safe-standard.js?ps_h=SlBh\u0026ps_t="+new Date().getTime();setTimeout("seSlBh.src=seSlBhs;document.getElementById('sdSlBh').appendChild(seSlBh)",1)</script> <?php }?><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=ximausa">Live Support</a></div></noscript>
			<!-- END ProvideSupport.com Graphics Chat Button Code -->
            </div>
<?php 
 } }

	 //infromacion para el google-analytics
	function googleanalytics()
	{
?>
		<script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
        try {
        var pageTracker = _gat._getTracker("UA-7654830-1");
        pageTracker._trackPageview();
        } catch(err) {}</script>
<?php 
	}
	
	function handlingError()
	{
?>
			<script type="text/javascript">
        Ext.ux.ErrorHandler.init();
		Ext.ux.ErrorHandler.on("error", function(ex) {
			// send information to the browser's console
			// send information to server for logging or to email to the developer
			// alert the user with a friendly message
			// etc, etc
			var navegador='';
			if(Ext.isChrome) navegador='CHROME';
			else if(Ext.isGecko) navegador='FIREFOX';
			else if(Ext.isIE) navegador='IE';
			else if(Ext.isOpera) navegador='OPERA';
			else if(Ext.isSafari) navegador='SAFARI';
			else navegador='INDEFINIDO';
			//alert(ex.name+'\n\n'+ex.message+'\n\n'+ex.url+'\n\n'+ex.lineNumber);
			
			Ext.Ajax.request( 
			{  
				waitMsg: 'Validating...',
				url: 'properties_handlingError.php', 
				method: 'POST',
				timeout :600000,
				params: { 
					type: ex.name,
					message: ex.message,
					url: ex.url,
					line: ex.lineNumber,
					nav: navegador
				},
				
				failure:function(response,options){
					Ext.MessageBox.alert('Warning','ERROR');
				},
				success:function(response,options){
					//alert(response.responseText);
					alert(ex.name+'\n\n'+ex.message+'\n\n'+ex.url+'\n\n'+ex.lineNumber);
					/*login_win.hide();
					login_win_allcounty.hide();
					login_win_commercial.hide();
					contract_win.hide();*/
					loading_win.hide();
				}                                
			});
		});
		    </script>
		<?php }
?>			