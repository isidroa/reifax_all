<div id="grid-example"></div>
<script>
	var selected_dataRG = new Array();
	var AllCheckRG=false;
	var limitRG=50;
	
///////////Cargas de data dinamica///////////////
	var storeRG = new Ext.data.Store({
		url: 'coresearch.php?resultType=advance&systemsearch=<?php echo $_POST['systemsearch'] ?>&groupbylevel=1',
		reader: new Ext.data.JsonReader(),
		remoteSort: true,
		listeners: {
			'load': function(store,data,obj){
				if (AllCheckRG){
					Ext.get(gridRG.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckRG=true;
					gridRG.getSelectionModel().selectAll();
					selected_dataRG=new Array();
				}else{
					AllCheckRG=false;
					var sel = [];
					if(selected_dataRG.length > 0){
						for(val in selected_dataRG){
							var ind = gridRG.getStore().find('pid',selected_dataRG[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridRG.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
	});
///////////FIN Cargas de data dinamica///////////////
    
	var smRG = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_dataRG.indexOf(record.get('pid'))==-1)
					selected_dataRG.push(record.get('pid'));
				
				if(Ext.fly(gridRG.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckRG=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_dataRG = selected_dataRG.remove(record.get('pid'));
				AllCheckRG=false;				
			}
		}
	});
		

//-------------------- Creacion del Grid----------------
	var gridRG = new Ext.grid.GridPanel({
		renderTo: 'grid-example',	
		loadMask : true,	  
        store: storeRG,
		columns: [],
		border: false,
		enableColLock: false,
		height: system_height-200,  //dimensiones del grid
        width: 'auto',
		sm: smRG,
		listeners: {
			"rowclick": function(grid, row, e) {
				if(this.getView().findCellIndex(e.getTarget()) !== 0){
					var record = this.store.getAt(row);
					var pid = record.get('pid');
					var groupselect = record.get('groupselect');
					//alert(pid+': '+owner);
					if(document.getElementById('resultFromGroupTab')){
						var tab = tabs.getItem('resultFromGroupTab');
						tabs.remove(tab);
					}
					
					tabs.add({
						title: 'From Result Group',
						id: 'resultFromGroupTab',
						autoLoad: {url: 'result_tabs/properties_advance_result.php', timeout: 10800, scripts: true, params: {systemsearch: '<?php echo $_POST['systemsearch'];?>', groupbylevel: 2, groupselect: groupselect}},
						closable: true, 
						tbar: new Ext.Toolbar({
							id:'menu_result_advFG',
							cls: 'no-border',
							width: 'auto',
							items: [' ',
								{
									 tooltip: 'Click to Show/Hide Map',
									 id: 'toolbarMapResultAdvFG',
									 iconCls:'icon',
									 icon: 'http://www.reifax.com/img/toolbar/map.png',
									 iconAlign: 'top',
									 width: 40,
									 hidden:icon_result,
									 scale: 'medium',
									 enableToggle: true,
									 handler: function(){
										if(document.getElementById('mapResultAdvFG').style.display=='none'){
											document.getElementById('mapResultAdvFG').style.display='';
												
											mapResultAdvFG.getCenterPins();
										}else
											document.getElementById('mapResultAdvFG').style.display='none';
									 }
								},{
									 tooltip: 'Click to View Legend',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/toolbar/legend.png',
									  hidden:icon_result,
									 scale: 'medium',
									 handler: function(){
										var dataLegend = [
											['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
											['A-F','http://www.reifax.com/img/houses/verdel.png','Active Forclosed'],
											['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Forclosed Sold'],
											['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Forclosed'],
											['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Forclosed Sold'],
											['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
											['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Forclosed'],
											['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Forclosed Sold'],
											['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Forclosed'],
											['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Forclosed Sold'],
											['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
											['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Forclosed'],
											['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Forclosed Sold'],
											['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Forclosed'],
											['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Forclosed Sold'],
											['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
											['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Forclosed'],
											['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Forclosed Sold'],
											['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Forclosed'],
											['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Forclosed Sold'],
											['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
										];
										 
										 var store = new Ext.data.ArrayStore({
											idIndex: 0,
											fields: [
												'status', 'url', 'description'
											]
										 });
										
										store.loadData(dataLegend);
										
										 var listView = new Ext.list.ListView({
											store: store,
											multiSelect: false,
											emptyText: 'No Legend to display',
											columnResize: false,
											columnSort: false,
											columns: [{
												header: 'Color',
												width: .15,
												dataIndex: 'url',
												tpl: '<img src="{url}">'
											},{
												header: 'Status',
												width: .2,
												dataIndex: 'status'
											},{
												header: 'Description',
												dataIndex: 'description'
											}]
										});
										
										var win = new Ext.Window({
											
											layout      : 'fit',
											width       : 370,
											height      : 300,
											modal	 	: true,
											plain       : true,
											items		: listView,
								
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
									 }
								},{
									 tooltip: 'Click to Print Report',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/toolbar/printer.png',
									 scale: 'medium',
									 hidden:icon_result,
									 handler: function(){
										if(!user_loged || user_web){ login_win.show(); return false;} 
										if(user_block){ Ext.MessageBox.alert('Warning','You are not allowed to view - print detailed information.'); return false;}
										
										var parcelids_res='';
										if(!AllCheckRFG){
											var results = selected_dataRFG;
											if(results.length > 0){
												parcelids_res=results[0];
												for(i=1; i<results.length; i++){
													parcelids_res+=','+results[i];
												}
											}else{
												Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
											}
										}
										
										loading_win.show();
										Ext.Ajax.request( 
											{  
												waitMsg: 'Printing Report...',
												url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
												method: 'POST', 
												params: {
													userweb:user_web,
													realtorid:user_webid,
													parcelids_res:parcelids_res,
													template_res: -1,
													groupbylevel: 2,
													groupselect: groupselect
												},
												
												failure:function(response,options){
													Ext.MessageBox.alert('Warning','Your result must have less than 1000 records.');
													loading_win.hide();
												},
												success:function(response,options){
													var rest = Ext.util.JSON.decode(response.responseText);
													//alert(rest.pdf);
													var url='http://www.reifax.com/'+rest.pdf;
													//alert(url);
													loading_win.hide();
													window.open(url);
													
												}                                
											 }
										);
									 }
								},{
									 tooltip: 'Click to Print Labels',
									 //text: 'Labels',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 hidden:icon_mylabel,
									 icon: 'http://www.reifax.com/img/toolbar/label.png',
									 scale: 'medium',
									 handler: function(){
										if(!user_loged || user_web){ login_win.show(); return false;} 
										if(user_block){ Ext.MessageBox.alert('Warning','You are not allowed to view - print Labels detailed information.'); return false;}
										
										var parcelids_res='';
										if(!AllCheckRFG){
											var results = selected_dataRFG;
											if(results.length > 0){
												parcelids_res=results[0];
												for(i=1; i<results.length; i++){
													parcelids_res+=','+results[i];
												}
											}
										}
										
										var simple = new Ext.FormPanel({
											labelWidth: 150, 
											url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>',
											frame:true,
											title: 'Property Labels',
											bodyStyle:'padding:5px 5px 0',
											width: 400,
											waitMsgTarget : 'Generated Labels...',
											
											items: [{
													xtype: 'combo',
													editable: false,
													displayField:'title',
													valueField: 'val',
													store: new Ext.data.SimpleStore({
														fields: ['val', 'title'],
														data : [
																[5160,5160],
																[5161,5161],
																[5162,5162],
																[5197,5197],
																[5163,5163]
														]
													}),
													name: 'label_type',
													fieldLabel: 'Label Type',
													mode: 'local',
													value: 5160,
													triggerAction: 'all',
													selectOnFocus:true,
													allowBlank:false
												},{
													xtype: 'combo',
													editable: false,
													displayField:'title',
													valueField: 'val',
													store: new Ext.data.SimpleStore({
														fields: ['val', 'title'],
														data : [
																[8,8],
																[9,9],
																[10,10]
														]
													}),
													name: 'label_size',
													fieldLabel: 'Label Size',
													mode: 'local',
													value: 8,
													triggerAction: 'all',
													selectOnFocus:true,
													allowBlank:false
												},{
													xtype: 'combo',
													editable: false,
													displayField:'title',
													valueField: 'val',
													store: new Ext.data.SimpleStore({
														fields: ['val', 'title'],
														data : [
																[0,'Owner'],
																[1,'Property']
														]
													}),
													name: 'address_type',
													fieldLabel: 'Address',
													mode: 'local',
													value: 0,
													triggerAction: 'all',
													selectOnFocus:true,
													allowBlank:false
												},{
													xtype: 'combo',
													editable: false,
													store: new Ext.data.SimpleStore({
														fields: ['val', 'title'],
														data : [
																['L','Left'],
																['C','Center']
														]
													}),
													displayField:'title',
													valueField: 'val',
													name: 'align_type',
													fieldLabel: 'Alingment',
													mode: 'local',
													value: 'L',
													triggerAction: 'all',
													selectOnFocus:true,
													allowBlank:false
												},{
													xtype: 'combo',
													editable: false,
													displayField:'title',
													valueField: 'val',
													store: new Ext.data.SimpleStore({
														fields: ['val', 'title'],
														data : [
																['N','No'],
																['Y','Yes']
														]
													}),
													name: 'resident_type',
													fieldLabel: 'Current Resident Or',
													mode: 'local',
													value: 'N',
													triggerAction: 'all',
													selectOnFocus:true,
													allowBlank:false
												},{
													xtype: 'hidden',
													name: 'type',
													value: 'result'
												},{
													xtype: 'hidden',
													name: 'parcelids_res',
													value: parcelids_res
												},{
													xtype: 'hidden',
													name: 'groupbylevel',
													value: 2
												},{
													xtype: 'hidden',
													name: 'groupselect',
													value: groupselect
												}
											],
									
											buttons: [{
												text: 'Apply',
												handler  : function(){
														loading_win.show();
														simple.getForm().submit({
															success: function(form, action) {
																//Ext.Msg.alert("Failure", action.result.pdf);
																var url='http://www.reifax.com/'+action.result.pdf;
																loading_win.hide();
																window.open(url);
																//window.open(url,'Print Labels',"fullscreen",'');
															},
															failure: function(form, action) {
																Ext.Msg.alert("Failure", action.result.msg);
																loading_win.hide();
															}
														});
													}
											},{
												text: 'Cancel',
												handler  : function(){
														simple.getForm().reset();
													}
											}]
										});
										win = new Ext.Window({
											
											layout      : 'fit',
											width       : 370,
											height      : 300,
											modal	 	: true,
											plain       : true,
											items		: simple,
								
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
										win.addListener("beforeshow",function(win){
											simple.getForm().reset();
										});
									 }
								},{
									tooltip: 'Click to Excel Report',
									iconCls:'icon',
									iconAlign: 'top',
									width: 40,
									icon: 'http://www.reifax.com/img/toolbar/excel.png',
									scale: 'medium',
									hidden:icon_result,
									handler: function(){
										if(!user_loged || user_web){ login_win.show(); return false;} 
										if(user_block){ Ext.MessageBox.alert('Warning','You are not allowed to view - print Excel detailed information.'); return false;} 
										
										var ownerShow='false';
										var parcelids_res='';
										if(!AllCheckRFG){
											var results = selected_dataRFG;
											if(results.length > 0){
												parcelids_res=results[0];
												for(i=1; i<results.length; i++){
													parcelids_res+=','+results[i];
												}
											}else{
												Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
											}
										}
										
										loading_win.show();
										Ext.Ajax.request({  
											waitMsg: 'Excel Report...',
											url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
											method: 'POST', 
											params: {
												userweb:user_web,
												realtorid:user_webid,
												ownerShow: ownerShow,
												parcelids_res:parcelids_res,
												template_res: -1,
												groupbylevel: 2,
												groupselect: groupselect
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','Your result must have less than 1000 records.');
											},
											success:function(response,options){
												var rest = Ext.util.JSON.decode(response.responseText);
												var url='http://www.reifax.com/'+rest.excel;
												loading_win.hide();
												//alert(url);
												location.href= url;
												//window.open(url);
											}                                
										});
										
										
									}
									
								},
								
								//Sugerencia 12501 12502 12504 agregada por Luis R Castro 05/06/2015
								{
						 tooltip: 'Follow Up',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/ximaicon/followup.jpg',
						 //hidden: <?php echo ($permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
									var parcelids_res='';
										if(!AllCheckRFG){
											var results = selected_dataRFG;
											if(results.length > 0){
												parcelids_res=results[0];
												for(i=1; i<results.length; i++){
													parcelids_res+=','+results[i];
												}
											}else{
												Ext.MessageBox.alert('Follow Up','You must check-select the records to be exported.'); return false;
											}
										}
										
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Follow up...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowmail/properties_followmail_ids.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
								method: 'POST', 
								timeout :600000,
								params: {
									userweb:user_web,
									realtorid:user_webid
								},
								
								failure:function(response,options){
									Ext.MessageBox.alert('Warning','ERROR');
									loading_win.hide();
								},
								success:function(response,options){
									loading_win.hide();
									var rest = Ext.util.JSON.decode(response.responseText);
									
									Ext.MessageBox.show({
										title:    'Follow Up',
										msg:      'Do you Want To add the properties to?',
										buttons: {yes: 'Buying', no: 'Selling',cancel: 'Cancel'},
										fn: function(btn) {
											var typeFollow='B';
											if(btn=='cancel'){
												return false;
											}else if(btn=='yes'){
												typeFollow='B';
											}else if(btn=='no'){
												typeFollow='S';
											}
											loading_win.show();
											Ext.Ajax.request({  
													waitMsg: 'Follow Up...',
													url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php', 
													method: 'POST', 
													timeout :600000,
													params: {
														'userid': <?php echo $_COOKIE['datos_usr']['USERID'];?>, 
														'followlist': true,
														typefollower: 1
													},
													
													failure:function(response,options){
														Ext.MessageBox.alert('Warning','ERROR');
														loading_win.hide();
													},
													success:function(response,options){
														loading_win.hide();
														var rest1 = Ext.util.JSON.decode(response.responseText);
														if(rest1.total==0){
															followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,parcelids_res,typeFollow);
														}else{
															 Ext.MessageBox.show({
																   title:    'Follow Up',
																   msg:      'Do you Want To add the properties to?',
																   buttons: {yes: 'My account', no: 'Other account',cancel: 'Cancel'},
																   fn: function(btn) {
																		if( btn == 'yes') {
																			followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,rest.ids,typeFollow);
																		}else if(btn == 'no'){
																			var simple = new Ext.FormPanel({
																				frame: true,
																				title: 'Select user',
																				width: 350,
																				waitMsgTarget : 'Waiting...',
																				labelWidth: 75,
																				defaults: {width: 230},
																				labelAlign: 'left',
																				items: [{
																							xtype         : 'combo',
																							mode          : 'local',
																							triggerAction : 'all',
																							fieldLabel	  : 'User',
																							width		  : 130,
																							editable	  : false,
																							emptyText     : 'Select user',
																							store         : new Ext.data.JsonStore({
																								id        : 0,
																								fields    : ['userid', 'name'],
																								data      : rest1.records
																							}),
																							displayField  : 'name',
																							valueField    : 'userid',
																							name          : 'userfollow',
																							value         : rest1.records[0].userid,
																							hiddenName    : 'fuserfollow',
																							hiddenValue   : rest1.records[0].userid,
																							allowBlank    : false
																						}],
																				
																					buttons: [{
																						text: 'Accept',
																						handler: function(){
																							var values = simple.getForm().getValues();
																							if(values.userfollow!=''){
																								winStatus.close();
																								followUp(values.fuserfollow,parcelids_res,typeFollow);
																							}
																						}
																					}]
																				});
																			 
																			var winStatus = new Ext.Window({
																				layout      : 'fit',
																				width       : 240,
																				height      : 180,
																				modal	 	: true,
																				plain       : true,
																				items		: simple,
																				closeAction : 'close',
																				buttons: [{
																					text     : 'Close',
																					handler  : function(){
																						winStatus.close();
																						loading_win.hide();
																					}
																				}]
																			});
																			winStatus.show();
																		}else{
																			return;
																		}
																  }
															 });
														}
														//Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
													}                                
											});
										}
									});
									
									function followUp(user, pids, type){
										loading_win.show();
										Ext.Ajax.request({  
												waitMsg: 'Follow Up...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
												method: 'POST', 
												timeout :600000,
												params: {
													type: 'multi-insert',
													pids: pids,
													county: rest.county,
													updatecontact: '',
													userid: user,
													typeFollow: type
												},
												
												failure:function(response,options){
													Ext.MessageBox.alert('Warning','ERROR');
													loading_win.hide();
												},
												success:function(response,options){
													loading_win.hide();
													Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
												}                                
										});
									}
									
								}                                
							 });
							
						 }
					}
								///////////////////////////////////////////////
								
								,{
									 tooltip: 'Click to Manage Template',
									 iconCls:'icon',
									 icon: 'http://www.reifax.com/img/toolbar/template.png',
									 iconAlign: 'top',
									 width: 40,
									 scale: 'medium',
									 handler: function(){
										ShowManageTemplate();
									 }
								},new Ext.form.ComboBox({
									id: 'templateComboFG',
									fieldLabel: '',
									triggerAction: 'all',
									mode: 'remote',
									forceSelection: true,
									store: new Ext.data.JsonStore({
										url: 'properties_manage_template.php',
										id: 0,
										fields: [
											'tID',
											'tname'
										]
									}),
									value: 'Default',
									width: 130,
									valueField: 'tID',
									displayField: 'tname',
									listeners:{
										'select': function (combo,record,index){
											ResultTemplateFG=record.data.tID;
											AllCheckRFG=false;
											selected_dataRFG=new Array();
											storeRFG.load({'params': {'ResultTemplate': ResultTemplateFG}});
										}
									}
								}),
								'->',{ 
									 tooltip: 'Click to Close From Result Group',
									 iconCls:'icon',
									 iconAlign: 'top',
									 width: 40,
									 icon: 'http://www.reifax.com/img/cancel.png',
									 scale: 'medium',
									  
									 handler: function(){
										 var tab = tabs.getItem('resultFromGroupTab');
										 tabs.remove(tab);
									 }
								  }
							],
							autoShow: true
						})
					}).show();
		
					if(Ext.isIE){
						var tab = tabs.getItem('resultFromGroupTab');
						tabs.setActiveTab(tab);
						tab.getEl().repaint();
					}
				}
			}
		},
		tbar: new Ext.PagingToolbar({
			id: 'pagingRG',
            pageSize: limitRG,
            store: storeRG,
            displayInfo: true,
			displayMsg: 'Total: {2} Records',
			emptyMsg: "No records to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 records per page.',
				text: 50,
				handler: function(){
					limitRG=50;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 records per page.',
				text: 80,
				handler: function(){
					limitRG=80;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 records per page.',
				text: 100,
				handler: function(){
					limitRG=100;
					Ext.getCmp('pagingRG').pageSize = limitRG;
					Ext.getCmp('pagingRG').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_resg_group'
			})
			]
        })
	});

 
 	storeRG.on('metachange', function(){
		if(typeof(storeRG.reader.jsonData.columns) === 'object') {
			var columns = [];
			columns.push(smRG);
			Ext.each(storeRG.reader.jsonData.columns, function(column){
				columns.push(column);
			});

			gridRG.getColumnModel().setConfig(columns);
		}
	});

	/////////////Inicializar Grid////////////////////////
	storeRG.load();
	/////////////FIN Inicializar Grid////////////////////



</script>