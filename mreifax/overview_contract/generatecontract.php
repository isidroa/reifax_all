<?php
	/**
	 * generatecontract.php
	 *
	 * Generate the contract for download.
	 *
	 * @autor   Juan Vargas  <?@?.com>             Original Code
	 *          Alex Barrios <alexbariv@gmail.com> Review, clean, order, strip, comments, version control and fixes
	 * @version 15.04.2011
	 */

	/*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/

    /*include('c:/inetpub/wwwroot/mant/classes/connection.class.php');
    include('c:/inetpub/wwwroot/mant/classes/globalReifax.class.php');
    $Connect=   new ReiFax(
        array(
            'downloadFiles' =>  FALSE
        )
    );
    $_SERVER['REIFAX_DOCUMENT_ROOT']=$Connect->documentRoot;
    include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
    $Errors =   new managerErrors(
        array(
            'downloadFiles' =>  FALSE
        )
    );*/
	
	include "../properties_conexion.php";
	include "../FPDF/limpiar.php";
	
	include($_SERVER['DOCUMENT_ROOT'].'/mysetting_tabs/myfollowup_tabs/followemail.class.php');

	function cleanwords($cad)
	{
		$cadfull='';
		for($i=0;$i<strlen($cad);$i++)
		{
			$d=substr($cad,$i,1);
			if(ord($d)<=127)
			{
				//echo $d,': ',ord($d),"<br>";
				$cadfull.=$d;
			}
		}
		return $cadfull;
	}

	//Validar que tenga mailsettings
	/*if($_POST['validar_settings']=='Y'){
		$userid    = $_COOKIE['datos_usr']["USERID"];
		$query    = "SELECT * FROM xima.contracts_mailsettings
						WHERE userid = $userid";
		$rs       = mysql_query($query);
		$mailInfo = mysql_fetch_array($rs);
		if (strlen($mailInfo['server'])>0) {
			echo "{success:true,mail:'true'}";	
		}else{
			echo "{success:true,mail:'false'}";	
		}
	}*/
	//echo "{success:true, pdf:'$file',add:'$addendum',data:'$addendata'}";
	// -------------------------------------------
	//   DELETE OLD PDFS
	// -------------------------------------------

	limpiardirpdf2('../FPDF/generated/');


	// -------------------------------------------
	//   GET FORM DATA
	// -------------------------------------------

	$type       = $_POST['type'];
	$county     = $_POST['county'];
	$parcelid   = $_POST['pid'];

	$templateemail = $_POST['contracttemplate'];
	$contrOpc   = $_POST['options'];

	$offer      = $_POST['offer'];
	$dateAcc    = $_POST['dateAcc'];
	$dateClo    = $_POST['dateClo'];

	$rname      = $_POST['rname'];
	$remail     = $_POST['remail'];
	$mlnumber   = $_POST['mlnaux'];
	$address    = $_POST['addr'];

	$sendbyemail   = $_POST['sendbyemail'];
	$sendmail   = $_POST['sendmail'];
	$addendum   = 'on';//$_POST['addendum'];
	$addendata  = $_POST['fieldAddeum'];
	$sendme     = $_POST['sendme'];
	$addons     = 'on';//$_POST['addons'];
	$addondata  = $_POST['fieldAddonG'];
	$deposit    = $_POST['deposit'];
	$inspection = $_POST['inspection'];
	$scrow      = $_POST['scrow'];

	$chseller   = $_POST['csinfo'];
	$sellname   = $_POST['sellname'];

	$chbuyer    = $_POST['cbinfo'];
	$buyername  = $_POST['buyername'];

	$comma      = '';
	//if ($contrOpc == '1')
		$comma  = ",";

	$listingagent  = $_POST['listingagent'];
	$listingbroker    = $_POST['listingbroker'];
	$buyeragent = $_POST['buyeragent'];
	$buyerbroker      = $_POST['buyerbroker'];
	// -------------------------------------------
	//   HELPING FUNCTIONS
	// -------------------------------------------

	// Load the fields from the file that has the
	// data extracted with pdftk
	function load_field_data( $field_report_fn ) {

		$ret_val = array();
		$fp      = fopen( $field_report_fn, "r" );

		if( $fp ) {
			$line = '';
			$rec  = array();

			while(($line= fgets($fp, 2048))!== FALSE) {

				$line = rtrim( $line );       // remove white spaces

				if( $line== '---' ) {
					if( 0 < count($rec) ) {   // end of the record
						$ret_val[] = $rec;
						$rec       = array();
					}
					continue;                 // next line
				}

				// Divide the line between name and value
				$data_pos = strpos( $line, ':' );
				$name     = substr( $line, 0, $data_pos+ 1 );
				$value    = substr( $line, $data_pos+ 2 );

				if( $name == 'FieldStateOption:' ) {
					// Pack FieldStateOption in his own array
					if( !array_key_exists('FieldStateOption:',$rec) )
						$rec['FieldStateOption:']= array();

					$rec['FieldStateOption:'][]= $value;

				} else
					$rec[ $name ]= $value;

			}

			if( 0< count($rec))  // Pack the end record
				$ret_val[]= $rec;

			fclose( $fp );
		}

		return $ret_val;

	}


	// Fix to get the correct aspect ratio of a image in a PDF
	/*function PixelToDPI($px) {

		return abs(round((($px/110)*72),0));
	}*/

	// -------------------------------------------
	//   DEFAULT VALS
	// -------------------------------------------

	$userid    = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

	$arrayCamp = Array('txtseller','txtbuyer','txtaddress',
					   'txtcounty','txtlegal1','txtlegal2',
					   'txtprice','txtlistingsales','txtbuyeraddress1',
					   'txtselleraddress1','txtpage','txtdatebuyer1',
					   'txtdatebuyer2','txtcollectedfunds','txtselleraddress2','txtselleraddress3',
					   'txtbroker','txtcooperatingsales','txtcooperatingbroker','txtparcelid','txtlegalotro','txtdate','txtdatetoday');

	conectarPorNameCounty($county);


	// -------------------------------------------
	//   Obtain all the data to fill the document
	// -------------------------------------------

	$valores      = Array();
	$balancevalue = 0;
	/*$query        = "SELECT * FROM xima.contracts_default c
						WHERE contract='".$type."' AND `defaultoption`='Y' AND userid=".$userid;*/
	$query        = "SELECT * FROM xima.contracts_custom c
						WHERE id=".$type." and userid=".$userid;

	$result       = mysql_query($query) or die($query.mysql_error());
	$total        = mysql_num_rows($result);


	if ( $total>0 ) {
		$r              = mysql_fetch_array($result);
		/*$valores[1]     = $r['name'];
		$valores[8]     = $r['address'];
		$usatemplate    = $r['template'];
		$activotemplate = $r['activetemplate'];

		if ( $usatemplate=='Y' && $activotemplate=='Y' ) {

			$id    = $r['id'];
			$q     = "SELECT min(id) AS minid FROM xima.contracts_default c
						WHERE contract='".$type."' AND userid=".$userid;
			$res   = mysql_query($q) or die($q.mysql_error());
			$ro    = mysql_fetch_array($res);
			$minid = $ro['minid'];

			if ( $id==$minid )
				$pdfusar = '1';
			else
				$pdfusar = '2';

			$borrartemplate = 'Y';

		} else {

			$pdfusar        = 'default';
			$borrartemplate = 'N';
			
		}*/
		$tplusar=$r['tplactive'];
		if($tplusar==1){
			$valores[1] = $r['tpl1_name'];
			$valores[8] = $r['tpl1_addr'];
		}else{
			$valores[1] = $r['tpl2_name'];
			$valores[8] = $r['tpl2_addr'];
		}
		$pdfusar=$r['filename'];
		$borrartemplate = 'N';
	} else {

		$query  = "SELECT
					x.`HOMETELEPHONE`, x.`ADDRESS`, x.`STATE`, x.`CITY`, x.`NAME`, x.`SURNAME`
					FROM xima.ximausrs x WHERE userid=".$userid;

		$result         = mysql_query($query) or die($query.mysql_error());
		$r              = mysql_fetch_array($result);
		$valores[1]     = $r['NAME'].' '.$r['SURNAME'];
		$valores[8]     = $r['ADDRESS'];
		$pdfusar        = 'default';
		$borrartemplate = 'N';

	}

	$query        = "SELECT owner, address, ozip, city, owner_a, owner_c, owner_z, owner_s, phonenumber1 from psummary where parcelid='".$parcelid."'";
	$result       = mysql_query($query) or die($query.mysql_error());
	$r            = mysql_fetch_array($result);
	$valores[0]   = $r['owner'];
	$valores[3]   = $county;
	$valores[9]   = $r['owner_a'];
	$valores[14]   = $r['owner_c'].', '.$r['owner_s'].' '.$r['owner_z'];
	$address      = $r['address'];
	$city         = $r['city'];
	$zip          = $r['ozip'];
	$valores[4]   = "Folio Number: ".$parcelid."  Legal Description as Shown in Public Records.";

	$query        = "SELECT lprice, agent, address, city, zip from mlsresidential where parcelid='".$parcelid."'";
	$result       = mysql_query($query) or die($query.mysql_error());
	$r            = mysql_fetch_array($result);

	//if ($contrOpc == '1')
		$r['lprice'] = $offer;

	$valores[6]   = (int)$r['lprice'];

	if ($contrOpc == '1') //Agregado por Jesus
		$valores[6]   = number_format($r['lprice'],2,'.',"$comma"); 

	$balancevalue = $balancevalue+$r['lprice'];
	$valores[7]   = '';//$r['agent'];

	if($address=='')
		$address  = $r['address'];

	if($city=='')
		$city     = $r['city'];

	if($zip=='')
		$zip      = $r['zip'];

	$valores[2]   = $address.', '.$city.', '.$zip;
	$txtaddress = $valores[2];
	$valores[10]  = '1';

	$fecha        = date('m-d-Y');
	$valores[11]  = $fecha;
	$verificafirma="select * from xima.contracts_signature c where type in(1,2) and userid=".$userid;
	$result1= mysql_query($verificafirma) or die($verificafirma.mysql_error());
	$cantidadf=mysql_num_rows($result1);
	if($cantidadf>1)
		$valores[12]  = $fecha;

	$valores[7]=$listingagent;
	$valores[16]=$listingbroker;
	$valores[17]=$buyeragent;
	$valores[18]=$buyerbroker;
	$valores[19]=$parcelid;
	$valores[20]='As Shown in Public Records';
	$valores[21]=date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
	$monthtoday=substr(date('F',mktime(0, 0, 0, date("m")  , date("d"), date("Y"))),0,3);
	$datetoday=date('j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
	$valores[22]=$monthtoday.' '.$datetoday;	////Format: May 28, 2012
	// -------------------------------------------
	//   Generate the document and fill the data
	// -------------------------------------------

	$contratos       = array('ContractPurchase','leadBasedPaint','ResidentialContract','TX_unimproved_property','TX_one_to_four_residential','TX_residential_condominium_contract_resale','TX_lead_based_paint');
	$camposContratos = array('contract','lead','residential','tx_unimproved','tx_one_to_four','tx_residential','tx_lead');

	$ruta            = getCwd();
	$s               = 0;

	if ($pdfusar!='default') {

		//Tomar plantilla de cliente
		$templatePdf = $contratos[$type].$userid.$pdfusar;

		//$orig        = "C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/{$templatePdf}.pdf";
		$orig        = "C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/template_upload/{$pdfusar}";
		//$dest        = "{$ruta}/{$templatePdf}.pdf";
		$dest        = "{$ruta}/{$pdfusar}";
		copy($orig,$dest);

		$nombre = $ruta.'/campos1.pdf.fields';

		if (file_exists($nombre))
			unlink($nombre);

		//passthru( $ruta.'/pdftk/pdftk '.$templatePdf.'.pdf dump_data_fields >> '.$nombre );
		passthru( $ruta.'/pdftk/pdftk '.$pdfusar.' dump_data_fields >> '.$nombre );
		
		$fields      = $nombre;

	} else {
		$fields      = $camposContratos[$type].'.pdf.fields';
		$templatePdf = $contratos[$type];
	}


	// -------------------------------------------
	//   Check the data field loaded for the new
	//   Document
	// -------------------------------------------

	$campos                 = array();
	$valoresPredeterminados = array();
	$field_arr              = load_field_data($fields);

	foreach( $field_arr as $field ) { // itera en los campos

		$campos[]=$field['FieldName:'];

		if ( $field['FieldName:']=='txtdeposit' || $field['FieldName:']=='txtaditional' || $field['FieldName:']=='txtotherprice' ) {

			if (strlen($deposit)!=0 && $field['FieldName:']=='txtdeposit')
				$field['FieldValue:'] = $deposit;

			$balancevalue=$balancevalue-$field['FieldValue:'];

			if ($contrOpc == '1') //Agregada por Jesus
				$field['FieldValue:'] = number_format($field['FieldValue:'],2,'.',"$comma"); 
			else
				$field['FieldValue:'] = (int)$field['FieldValue:'];

		}

		$valoresPredeterminados[]=$field['FieldValue:'];

	}

	$valores[13] = number_format($balancevalue,2,'.',"$comma");


	// -------------------------------------------
	//   Create a new template with the values
	// -------------------------------------------

	require_once( 'forge_fdf.php' );

	$fdf_data_strings = array();
	$fdf_data_names   = array();



	foreach( $campos as $key => $value ) {

		$encontrado = true;
		$count      = 0;
		while ($encontrado && $count<count($arrayCamp)) {

			if ($value==$arrayCamp[$count]) {
				$encontrado = false;
				$fdf_data_strings[ strtr($arrayCamp[$count], '~', '.') ] = $valores[$count];
			}

			$count++;

		}
		if($encontrado) {
			if (!stristr($value,'chk'))
				$fdf_data_strings[ strtr($value, '~', '.') ] = htmlspecialchars_decode($valoresPredeterminados[$key]);
		}
	}

	//if ($contrOpc == '1') {
		$fdf_data_strings[ strtr('txtdateacceptance1', '~', '.') ] = $dateAcc;
		$fdf_data_strings[ strtr('txtdateclosing', '~', '.') ]     = $dateClo;
		$fdf_data_strings[ strtr('txtdayscancel', '~', '.') ]      = $inspection;
	//}
	if ($addendum=='on' and trim($addendata)<>"") {
		if($userid==1719 || $userid==3138){
			$fdf_data_strings[ strtr('txtaditionalterms1', '~', '.') ] = "Subject to no lien, violations, open permits on property and subject to clear and marketable title.";
		}else{
			$fdf_data_strings[ strtr('txtaditionalterms1', '~', '.') ] = "See additional Addendum page";
		}
	}
	//if ($chseller=='on')
	if(strlen(trim($sellname))>0)
		$fdf_data_strings[ strtr('txtseller', '~', '.') ] = $sellname;
		
	//if ($chbuyer=='on')
	if(strlen(trim($buyername))>0)
		$fdf_data_strings[ strtr('txtbuyer', '~', '.') ]  = $buyername;
		
	$txtbuyer   = $fdf_data_strings[ strtr('txtbuyer', '~', '.') ];
	////////////Agregado por Jesus////////////
	//Se Agrego porque Hay Usuarios que Tiene el txtbuyer fijo en el contrato y yo le cambie el nombre a ese campo para que el sistema
	//no lo tome y por eso es que no lo estama montando con el $fdf_data_strings[ strtr('txtbuyer', '~', '.') ]; sino con el 
	//$fdf_data_strings[ strtr('txtbuyer_jesus', '~', '.') ];
	if($txtbuyer==""){
		$txtbuyer = $fdf_data_strings[ strtr('txtbuyer_jesus', '~', '.') ];
	}
	////////////Hasta Aqui por Jesus//////////////$txtaddress = $fdf_data_strings[ strtr('txtaddress', '~', '.') ];
	//$txtaddress = $fdf_data_strings[ strtr('txtaddress', '~', '.') ];
	//$offerPrice = number_format($fdf_data_strings[ strtr('txtdeposit', '~', '.') ],2,'.',','); //Comentado por Jesus
	$offerPrice = (int)$fdf_data_strings[ strtr('txtdeposit', '~', '.') ];


	// -------------------------------------------
	//   Generate the document and fill the data
	// -------------------------------------------

	$fields_hidden   = array();
	$fields_readonly = array();

	$fdf= forge_fdf( '', $fdf_data_strings,
						 $fdf_data_names,
						 $fields_hidden,
						 $fields_readonly );
	$fdf_fn = tempnam( '.', 'fdf' );
	$fp     = fopen( $fdf_fn, 'w' );

	if( $fp ) {

		fwrite( $fp, $fdf );
		fclose( $fp );

		//$file    = 'FPDF/generated/'.$contratos[$type].$parcelid.'.pdf';
		$file    = 'FPDF/generated/'.$parcelid.'.pdf';
		$aux = "";
		if ($contrOpc == '1')
			$aux = "flatten";

		// FILL THE PDF WITH THE DATA
		//passthru("{$ruta}/pdftk/pdftk {$templatePdf}.pdf fill_form {$fdf_fn} output C:/inetpub/wwwroot/{$file} $aux");
		passthru("{$ruta}/pdftk/pdftk {$pdfusar} fill_form {$fdf_fn} output C:/inetpub/wwwroot/{$file} $aux");

		// FINAL PDF FILE
		$archivo = 'C:/inetpub/wwwroot/'.$file;

		//pdf agregar firmas
		//adjuntaFirma('ContractPurchase', $userid, $archivo);
		
		// --------------
		// ADD NEW PAGES WITH THE ADDENDUMS IF SELECTED

		if ($addendum=='on' and trim($addendata)<>"") {

			require_once '../FPDF/fpdf.php';

			$pdf = new FPDF('P', 'cm', 'A4');

			$pdf->Open();

			$pdf->SetAutoPageBreak(true);
			$pdf->SetTextColor(0,0,0);

			$y_axis_initial = 25;

			$arrEsp = explode('|', $addendata);
			$carr   = count($arrEsp);

			$pdf->AddPage('P','A4');

			$pdf->SetY(0.5); $pdf->SetX(1.3);

			$pdf->SetFont('Helvetica', 'B', 12);
			$pdf->Cell(0, 2, 'Addendums',0,0,'C');

			$pdf->SetY(2.3); $pdf->SetX(1.3);

			$text = '';

			for ($iK = 1; $iK < $carr; $iK++) {

				$query   = "SELECT content FROM xima.contracts_addendum
								WHERE userid = $userid AND id=".$arrEsp[$iK]."
								ORDER BY id ASC";
				$rs      = mysql_query($query);

				if ($rs) {
					$row = mysql_fetch_array($rs);

					$text .= utf8_encode(" - ".$row[0]."\n\n");

				}
			}

			$pdf->SetFont('Helvetica', '', 11);
			$pdf->MultiCell(18,0.5,$text);

			$pdf->Output("{$ruta}/{$userid}.pdf");

			passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/{$userid}.pdf cat output {$ruta}/addend_{$userid}.pdf");

			copy("{$ruta}/addend_{$userid}.pdf","{$archivo}");

		}

		// --------------
		// SCROW DEPOSIT LETTER

		if ($scrow=='on') {

			// Text of the letter
			$date  = date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
			$doc   = "\n\n$date\n\n";
			$doc  .= "Re: $txtbuyer\n\n";
			$doc  .= "Property Address: $txtaddress\n\n";
			$doc  .= "To Whom It May Concern:\n\n";
			$doc  .= "In connection with the above referenced real estate transaction, ";
			$doc  .= "please note that our title company, has received a deposit from the\n\n";
			$doc  .= "Buyer: $txtbuyer\n\n";
			//$doc  .= "In the amount of $ ".number_format($deposit,2)." as earnest money towards the purchase "; Comented By Kristy Freddy Frank and Jesus por orden de frank 10-08-2013        
			$doc  .= "In the amount of $ 1,000.00 as earnest money towards the purchase ";
			$doc  .= "of the above captioned property on $date.\n\n";
			$doc  .= "If you have any questions, please do not hesitate to contact the undersigned.\n\n";

			// Get header and Footer
			$query   = "SELECT imagen,place FROM xima.contracts_scrow
							WHERE userid = $userid ORDER BY place ASC";
			$rs      = mysql_query($query);
			$path   = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/scrows/';
			$imgSc  = null;
			if ($rs) {
				while ($row = mysql_fetch_array($rs)) {
					$imgSc["s{$row['place']}"] = $path.$row[0];
				}
			}

			require_once '../FPDF/fpdf.php';

			$pdf = new FPDF('P', 'cm', 'A4');

			$pdf->Open();

			$pdf->SetTextColor(0,0,0);

			$pdf->SetAutoPageBreak(false);

			$pdf->AddPage('P','A4');

			$pdf->SetFont('Helvetica', '', 13);

			if ($imgSc['s1']!='') {
				$pdf->SetY(2); $pdf->SetX(1.3);
				$pdf->Image($imgSc['s1'],null,null,18);
			}

			$pdf->SetY(9); $pdf->SetX(1.3);
			$pdf->MultiCell(18,0.5,$doc);

			if ($imgSc['s2']!='') {
				$pdf->SetY(20); $pdf->SetX(1.3);
				$pdf->Image($imgSc['s2'],null,null,18);
			}

			$pdf->Output("{$ruta}/{$userid}.pdf");

			passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/{$userid}.pdf cat output {$ruta}/scrow_{$userid}.pdf");

			copy("{$ruta}/scrow_{$userid}.pdf","{$archivo}");

		}

		// --------------
		// ADD NEW PAGES WITH THE ADDONS IF SELECTED

		if ($addons=='on' and strlen(trim($addondata))>0) {

			$arrEsp = explode('|', $addondata);
			$carr   = count($arrEsp);
			
			for ($iK = 1; $iK < $carr; $iK++) {


				$query   = "SELECT place,document FROM xima.contracts_addonscustom
								WHERE userid = $userid AND type = $type AND id=".$arrEsp[$iK]."
								ORDER BY place ASC";
				$rs      = mysql_query($query);
				
				if ($rs) {
					while ($row = mysql_fetch_array($rs)) {
						//echo 'aqui '.$row[1];
						$fK = 'C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/addons/'.$row[1];
						//echo $fK; 
						copy("$fK","{$ruta}/addon_{$userid}.pdf");

						passthru("{$ruta}/pdftk/pdftk {$archivo} {$ruta}/addon_{$userid}.pdf cat output {$ruta}/finaladdon_{$userid}.pdf");
						
						passthru("{$ruta}/pdftk/pdftk {$ruta}/finaladdon_{$userid}.pdf output {$archivo}");

					}
				}
			}
		}

		/************************************************************
		*	Function Send Email and Saved Document and Saved on FollowUp
		************************************************************/
		include("generatecontractFunctions.php");
		
		unlink( $fdf_fn ); // delete temp file
		@unlink("{$ruta}/finaladdon_{$userid}.pdf");
		@unlink("{$ruta}/addon_{$userid}.pdf");
		@unlink("{$ruta}/addend_{$userid}.pdf");
		@unlink("{$ruta}/scrow_{$userid}.pdf");
		@unlink("{$ruta}/{$userid}.pdf");

		if($borrartemplate=='Y')
			unlink($templatePdf.'.pdf');



		if($sendmail=="on") $totalMailsSend	=	controlSendMails($userid);	else	$totalMailsSend=0;
		echo "{success:true, pdf:'$file',add:'$addendum',data:'$addendata',totalSend:'$totalMailsSend'}";

	} else{
	   echo '{msg: unable to write temp fdf file}';
	}
?>
