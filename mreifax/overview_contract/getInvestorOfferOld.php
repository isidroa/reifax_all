<?php
/**
 * getInvestorOffer.php
 *
 * Return the offer value of investors.
 *
 * @autor   Guillermo Vera <guilleverag@gmail.com>
 * @version 28.06.2011
 */  

set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot'. PATH_SEPARATOR);
include("properties_conexion.php");

if(is_numeric($_GET['db'])) {
	$db         = conectarPorIdCounty($_GET['db']); 
	$_GET['db'] = $db; 
} else { 
	$db         = conectarPorNameCounty($_GET['db']); 
	$db         = explode('^',$db); 
	$db         = $db[0]; 
	$_GET['db'] = $db;
}

$q      = "SELECT IF(r.xcode is null, p.xcode, r.xcode) as xcode, m.lprice, m.status 
			FROM psummary p
			LEFT JOIN rtmaster r ON (p.parcelid=r.parcelid) 
			LEFT JOIN mlsresidential m on (p.parcelid=m.parcelid)
			WHERE p.parcelid='".$_GET['pid']."'";
		
$result = mysql_query($q) or die($q.mysql_error());   
$res    = mysql_fetch_array($result);

$_POST["prop"]=$res['xcode'];
$_POST['pid']=$_GET['pid'];
$_POST['bd']=$_GET['db'];
$lprice=($res['status']=='A' && floatval(str_replace(',','',$res['lprice']))>0) ? floatval(str_replace(',','',$res['lprice'])) : 999999999;
$roundTo=$_POST['roundTo'];
$factor=$_POST['factor'];
$investOpc = $_POST['lowMedian'] == 1 ? 'L' : 'M';

$_POST['no_include']='true';
$_POST['type']='invest';
$_POST['id']=$_GET['pid'];
$_POST['status']='CS,CC';
$_POST['reset']='true';
$_POST['array_taken']=$_POST['parcelids_cco'];
$_POST['userid']=$_COOKIE['datos_usr']['USERID'];
$_POST['sort']='saleprice';
$_POST['dir']='ASC';

ob_start();
$Filas='';
include('properties_look4comparables.php');
$content = ob_get_contents();
ob_end_clean();

$_arrResult=explode("^",$content);
if(trim($_arrResult[0])!='ERROR')	
	$vFilas   = json_decode("[".trim($_arrResult[0])."]");
else
	$vFilas   ='';
	
$_POST["comparables"]='';
foreach($vFilas as $k => $val){
	if($investOpc=='M' || ($investOpc=='L' && $k<4)){
		if($_POST["comparables"]!='') $_POST["comparables"].=',';
		$_POST["comparables"].='"'.$val->id.'"';
	}
}

$_POST['status']='CC';
ob_start();
include('properties_calculate.php');
$content = ob_get_contents();
ob_end_clean();

$calculate=explode("^",$content);
$marketvalue = $calculate[0];


$_POST['status']='A';
$_POST['no_func']='true';
$_POST['array_taken']=$_POST['parcelids_cca'];
$_POST['sort']='lprice';
if($investOpc=='L'){
	$_POST['start']=0;
	$_POST['limit']=4;
}

ob_start();
$Filas='';
include('properties_look4comparables.php');
$content = ob_get_contents();
ob_end_clean();

$_arrResult=explode("^",$content);
if(trim($_arrResult[0])!='ERROR')	

	$vFilas   = json_decode("[".trim($_arrResult[0])."]");
else
	$vFilas   ='';
	
$_POST["comparables"]='';
foreach($vFilas as $k => $val){
	if($investOpc=='M' || ($investOpc=='L' && $k<4)){
		if($_POST["comparables"]!='') $_POST["comparables"].=',';
		$_POST["comparables"].='"'.$val->id.'"';
	}
}

ob_start();
include('properties_calculate.php');
$content = ob_get_contents();
ob_end_clean();

$calculate=explode("^",$content);
$activevalue = $calculate[0];

//echo $lprice.' - '.$marketvalue.' - '.$activevalue;

$marketvalue=floatval(str_replace(',','',$marketvalue));
$activevalue=floatval(str_replace(',','',$activevalue));

$selected=$lprice;
if($marketvalue > 0 && $selected > $marketvalue) $selected = $marketvalue;
if($activevalue > 0 && $selected > $activevalue) $selected = $activevalue;
$fact = floatval($factor/100);
$iovalue = (floatval($selected) * floatval($fact)); 

//echo ' - '.$selected.' - '.$iovalue.' - ';

if($roundTo==500)
	$final = floor($iovalue/1000).'500.00';
else if($roundTo==10)
	$final = floor($iovalue/10).'0.00';
else if($roundTo==100)
	$final = floor($iovalue/100).'00.00';
else 
	$final = (floor($iovalue/1000)+1).'000.00';

echo $final;
?>