<?php
// Send the document by email with and attach
function SendEMailOver($attach,$mln,$address,$userid,$name,$email,$send,$copy,$template,$bd,$pid,$type,$valInContract) {

	$enviado='';
	$boundary = md5(uniqid(time()));
	
	$query = "SELECT email,name,surname,hometelephone,mobiletelephone, address
				FROM xima.ximausrs WHERE userid='$userid'";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);

	$phone    = strlen($row[4]) == 0 ? $row[3] : $row[4];
	$userName = $row[1].' '.$row[2];
	$emailUser = $row[0];
	$addressUser = $row[5];
	
	$query    = "SELECT * FROM xima.contracts_mailsettings
					WHERE userid = $userid";
	$rs       = mysql_query($query);
	$mailInfo = mysql_fetch_array($rs);
		
	if (strlen($mailInfo['server'])>0) {
	
		
		// * --------------------
		
		require_once '../mailer/class.phpmailer.php';
		//Elige template de email a utilizar
		if($template!=0){
			$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$template";
			$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
			$mailTemplate = mysql_fetch_array($resultTemplate);
			
			$usarHtml=true;
			$subject = cleanwords($mailTemplate['subject']);
			$body = cleanwords($mailTemplate['body']);
			
			//Leer variables
			/*$query     = "SELECT campos, titulos, `desc`, tabla FROM xima.camptit WHERE variable=1"; 
			$rscr      = mysql_query($query) or die($query.mysql_error());
			$arrayNames=Array();
			while ($rowcr = mysql_fetch_array($rscr)) {
				$valor=	$rowcr['desc'];
				if($valor==''){
					$valor=	$rowcr['titulos'];
				}
				if($valor!="Property Address" && $valor!="Contact Email 1"){
					$arrayNames[$rowcr['campos']]=$valor;
				}else if($valor=="Contact Email 1"){
					$arrayNames['email1']=$valor;
				}else{
					$arrayNames['addressp']=$valor;
				}
			}
			$queryVar = "select r.*,x.*, a.*, a.email as email1, u.offer, u.bd as county, p.bath ,p.beds ,p.address as addressp ,
								p.ccoded ,p.city ,p.folio ,p.landv ,
								p.legal ,p.lsqft ,p.owner,
								p.owner_a ,p.owner_c ,p.owner_p ,p.owner_s ,p.owner_z ,p.saledate ,p.saleprice ,p.sdname ,p.sfp,
								p.stories ,p.taxablev ,p.tsqft ,p.unit ,p.yrbuilt ,p.ozip ,p.waterf, m.lprice,
								l.mtg_bor1, l.mtg_lender, l.mtg_recdat, l.mtg_amount,
								f.case_numbe,f.totalpendes,f.defowner1,f.judgedate,f.plaintiff1,f.mtg1type,
								c.tpl1_addr,c.tpl1_addr2,c.tpl1_addr3,c.tpl1_name
							from psummary p
							left join mlsresidential m on p.parcelid=m.parcelid
							left join mortgage l on p.parcelid=l.parcelid
							left join pendes f on p.parcelid=f.parcelid
							left join xima.followup u on p.parcelid=u.parcelid and u.userid=$userid
							left join xima.followagent a ON (u.agent=a.agent AND a.userid=$userid)
							left join xima.ximausrs x on x.userid=$userid
							left join xima.profile r on r.userid=$userid
							left join xima.contracts_custom c on (c.userid=$userid and c.id=$type)
							where p.parcelid='$pid' and x.userid=$userid";
			$resultVar=mysql_query($queryVar) or die($queryVar.mysql_error());
			$arrayVar=mysql_fetch_assoc($resultVar);
			
			$agent = $arrayVar['agent'];
			$agentmail = $arrayVar['email1'];
			$agentmail2 = $arrayVar['email2'];
			if($agentmail==''){
				$arrayVar['email1'] = $email;
			}
			if($agentmail2==''){
				$arrayVar['email2'] = $email; 
			}
			if($agent==''){
				$arrayVar['agent'] = $name;
			}
			foreach($arrayVar as $key => $var){
				//echo $arrayNames[$key].' '.$var;
				$subject=str_replace('{%'.$arrayNames[strtolower($key)].'%}',$var,$subject);
				$body=str_replace('{%'.$arrayNames[strtolower($key)].'%}',$var,$body);
				
			}*/
			include_once('../mysetting_tabs/mycontracts_tabs/function_template.php'); 
			$subject=replaceTemplate($subject,$userid,$pid,$type,$name,$email,$_POST['offer']);
			$body=replaceTemplate($body,$userid,$pid,$type,$name,$email,$_POST['offer']);
		}else{
			$usarHtml=false;
			if($userid==933 || $userid==2846 || $userid==2883){
				$subject  = "{$address}-Cash Offer, You Represent Us";
				
				$body   .= "Dear,\r\n\r\n";
				$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
				$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
				$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
				$body    .= "    \t* We close Cash. We are well funded.\r\n";
				$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
				$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
				$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
				$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
				$body   .="Thank you and have an amazing day!\r\n\r\n";
				$body    .= "Best Regards,\r\n";
				$body    .= "{$row[1]} {$row[2]}.\r\n";
				$body    .= "Summit Home Buyers, LLC\r\n";
				$body    .= "$phone\r\n";
			}else{
				$subject  = "Offer for the MLS number $mln, Address {$address}";
				if($userid==1719 || $userid==1641){
					$texto="Dear $name\r\n\r\n";
				}else{
					$texto="Dear Mr/Mrs $name\r\n\r\n";
				}
				$body     = $texto;
				$body   .= "Please find attached a contract with our offer regarding the ";
				$body   .= "property with the address: {$address}\r\n";
				$body   .= "We wait for your prompt response\r\n\r\n";
				$body   .= "Regards {$row[1]} {$row[2]}.\r\n";
				$body   .= "$phone\r\n";
				$body   .= "{$row[0]}\r\n\r\n";
			}
		}
		if ($send == 'on') {
			$sendTo = strtolower($email);
			$respuesta=json_decode(composeEmail($userid, NULL, $pid, $sendTo, $subject, $body, 0, false, 'B', true, array($attach), $valInContract['offer'],'0',$valInContract['pof'],$valInContract['emd'],$valInContract['rademdums'],$bd));
			/*if($userid=='2482'){
				print_r($respuesta);
			}*/
			if(!$respuesta->success){
				echo json_encode($respuesta); exit();return;
			}

			/*$mail            = new phpmailer();		
			$mail->PluginDir = "../mailer/";
			$mail->Mailer    = "smtp";
			$mail->Host      = $mailInfo['server'];
			$mail->Port      = $mailInfo['port'];
			$mail->SMTPAuth  = true;	
			$mail->Username  = $mailInfo['username'];
			$mail->Password  = $mailInfo['password'];
			
			// * --------------------
			
			$mail->From     = $mail->Username;
			$mail->FromName = "{$row[1]} {$row[2]}";
		
			$mail->IsHTML($usarHtml);
			$mail->Subject  = $subject;
				
			$mail->Body    = $body;
			
			$mail->AddAttachment($attach);
			
			$mail->AddAddress($sendTo,"$name");
			//$mail->Send();
			if(!$mail->Send()){    
				echo "{success:false, pdf:'Message was not sent ({$mail->ErrorInfo}).',add:'',data:'',error:'{$mail->ErrorInfo}'}"; exit();return;}*/
		}
	
		if ($copy == 'on') {
			$sendTo = strtolower($row[0]);
			if($userid==1719){
				$sendTo = 'juandej18@gmail.com';
			}
			$mail            = new phpmailer();		
			$mail->PluginDir = "../mailer/";
			$mail->Mailer    = "smtp";
			$mail->Host      = $mailInfo['server'];
			$mail->Port      = $mailInfo['port'];
			$mail->SMTPAuth  = true;	
			$mail->Username  = $mailInfo['username'];
			$mail->Password  = $mailInfo['password'];
			
			// * --------------------
			
			$mail->From     = $mail->Username;
			$mail->FromName = "{$row[1]} {$row[2]}";
		
			$mail->IsHTML($usarHtml);
			$mail->Subject  = $subject;
				
			$mail->Body    = $body;
		
			$mail->AddAttachment($attach);
			
			$mail->AddAddress($sendTo,"$name");
			//$mail->Send();
			if(!$mail->Send()){    
				echo "{success:false, pdf:'Message was not sent ({$mail->ErrorInfo}).',add:'',data:'',error:'{$mail->ErrorInfo}'}"; exit();return;}
		}
	
	}
}

    // -------------
    // SEND THE DOCUMENT BY EMAIL IF SELECTED
	if($sendbyemail == 'yes'){
		if ($sendmail=='on' or $sendme=='on') {

			$newArchivo=$_SERVER['REIFAX_DOCUMENT_ROOT']."/MailAttach/$userid/document_".$parcelid.date('YmdHisu').'.pdf';
			if(!is_dir($_SERVER['REIFAX_DOCUMENT_ROOT'].'/MailAttach/'.$userid)){
				mkdir($_SERVER['REIFAX_DOCUMENT_ROOT'].'/MailAttach/'.$userid,0777,true);
			}
			copy($archivo,$newArchivo);
			
			SendEMailOver($newArchivo,$mlnumber,$address,$userid,$rname,$remail,$sendmail,$sendme,$templateemail,$county,$parcelid,$type,$valInContract);
	
			// Save to My Document
	
			$query  = "SELECT address FROM psummary WHERE parcelid='$parcelid'";
			$rs     = mysql_query($query);
			$row    = mysql_fetch_array($rs);
	
			$dir    = 'saved_documents';
			$moment = date('YmdHisu');
	
			$doc    = $_SERVER['REIFAX_DOCUMENT_ROOT']."/$dir/{$contratos[$type]}$parcelid_$moment.pdf";
			$url    = "http://www.reifax.com/$dir/{$contratos[$type]}$parcelid_$moment.pdf";
	
			copy("$archivo",$_SERVER['REIFAX_DOCUMENT_ROOT']."/$dir/{$contratos[$type]}$parcelid_$moment.pdf");
	
			$query  = "INSERT INTO xima.saveddoc
						(usr,parcelid,url,directorio,sdate,address,name)
					VALUES
						($userid,'$parcelid','$url','$doc',NOW(),'{$row[0]}','contract_{$contratos[$type]}$parcelid.pdf')";
			$rs     = mysql_query($query);
	
		}
	
		if ($sendmail=='on'){
			$query='SELECT count(*) FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$parcelid.'"';
			$result=mysql_query($query) or die($query.mysql_error());
			$r=mysql_fetch_array($result);
			if($r[0]==0){
				//Agregar agente to my follow agent si no existe ya.//ESto ya ni existe o no hay no se usa (Guilles y Jesus 23-01-2015)
				/*$query='INSERT INTO xima.followagent (userid,agent,email,tollfree,phone1,phone2,phone3,fax, phone6,
					typeph1,typeph2,typeph3,typeph4,typeph5,typeph6,email2,typeemail1,typeemail2,company)
					SELECT f.userid, r.agent, r.agentemail, r.agenttollfree, r.agentph, r.agentph2, r.officephone, r.agentfax, r.officetollfree,
					0, 2, 1, 3, 4, 5, r.officeemail, 0, 1, r.officename
					FROM xima.followup f
					INNER JOIN mlsresidential r ON (f.parcelid=r.parcelid)
					LEFT JOIN xima.followagent a ON (r.agent=a.agent AND a.userid=f.userid)
					WHERE f.userid='.$_COOKIE['datos_usr']['USERID'].' AND f.parcelid="'.$parcelid.'"  and a.agent is null';
				mysql_query($query) or die($query.mysql_error());
				
				//Asignacion del agent a la propiedad.
				$query='INSERT INTO xima.follow_assignment (parcelid,userid,agentid,principal)
					SELECT f.parcelid,f.userid, a.agentid, 1
					FROM xima.followup f
					INNER JOIN mlsresidential r ON (f.parcelid=r.parcelid)
					LEFT JOIN xima.followagent a ON (r.agent=a.agent AND a.userid=f.userid)
					WHERE f.userid='.$_COOKIE['datos_usr']['USERID'].' AND f.parcelid="'.$parcelid.'"';
				mysql_query($query) or die($query.mysql_error());*/
				
				
				//asiganación de la propiedad en las notificaciones de cada follower. //ESto ya ni existe o no hay no se usa (Guilles y Jesus 23-01-2015)
				/*$query='SELECT follower_id FROM xima.follower WHERE userid='.$_COOKIE['datos_usr']['USERID'];
				$result2=mysql_query($query) or die($query.mysql_error());
				
				if(mysql_num_rows($result2)>0){
					$i=0;
					$query='INSERT INTO xima.follow_notification VALUES ';
					while($r2=mysql_fetch_array($result2)){
						if($i>0) $query.=',';
						$query.='("'.$parcelid.'",'.$_COOKIE['datos_usr']['USERID'].','.$r2[0].',0,1)';
						$i++;
					}
					mysql_query($query) or die($query.mysql_error());
				}*/
			}else{
				$query="UPDATE xima.followup SET `type`=IF(`type`='M','BM',IF(`type`='DEL','B',`type`)) WHERE userid={$userid} AND parcelid='{$parcelid}'";
				$result=mysql_query($query) or die($query.mysql_error());
			}
			if($offer==''){
				$offer=0;
			}
			$coffer 	= 0; 
			$task 		= 7;
			$contract 	= 0;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= 'Contract and Documents Sent By Bulk Contract';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			/*$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow)
		VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.')';
			mysql_query($query) or die($query.mysql_error());*/
		}
	}
?>