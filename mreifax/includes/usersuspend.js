function nuevoAjax()
{
	var xmlhttp=false; 	try 
	{ xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 	}
	catch(e)
	{ 	try	{ xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); } 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 
	return xmlhttp; 
}

function formatMoneda(num,longEntera, decSep,thousandSep) //formatear numeros xx,xxx.xx
{
	var arg;
	var entero;
	if(typeof(num) == 'undefined') return;
	if(typeof(decSep) == 'undefined') decSep = ',';
	if(typeof(thousandSep) == 'undefined') thousandSep = '.';
	
	if(thousandSep == '.'){	arg=/\./g;}
	else if(thousandSep == ','){arg=/\,/g;}
	
	if(typeof(arg) != 'undefined'){	num = num.toString().replace(arg,'');	}
	
	num = num.toString().replace(/,/g,'.');	
	if (num.indexOf('.') != -1)	{	entero = num.substring(0, num.indexOf('.'));	}
	else entero = num;
	
	if (entero.length > longEntera)
	{	//alert("El n�mero introducido excede de " + longEntera + " digitos en su parte entera");	
		return "0,00";	
	}
	
	if(isNaN(num))	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10)	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+thousandSep+ num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + decSep + cents);
}

function validarCampoTexto(tipo)//validar campos de texto
{
	if(tipo==0)//solo numero entero
		if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
	if(tipo==1)//solo numero y slash --> Fecha de expiracion de tarjeta MM/YYYY
		if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 47) event.returnValue = false;
}

function MarcaSelectedState(statesel)//Busca y Muestra(Marca) el estado del usuario a editar
{
	if(document.getElementById("State").style.visibility=="visible")//combo de state
	{
		var field=document.getElementById("sState");
		var enc=0;
		for (i=0; i<field.length;i++)
		{
			if(field[i].value==statesel)
			{
				enc=1;
				break;
			}
		}
		if(enc==1)
			field[i].selected=true;
	}
	else//campo de textode state
	{
		document.getElementById("sState2").value=statesel;
	}
}

function ComboCreditCard(valuesel)//carga el combo de Card Type
{
	var cad='<select name="cbCardtype" id="cbCardtype" >'+
				'<option '+SelectedCombo(valuesel,"none")+' value="none">- Select -</option>'+
				'<option '+SelectedCombo(valuesel,"MasterCard")+' value="MasterCard">Master Card</option>'+
				'<option '+SelectedCombo(valuesel,"Visa")+' value="Visa">Visa</option>'+
				'<option '+SelectedCombo(valuesel,"AmericanExpress")+' value="AmericanExpress">American Express</option>'+
				'<option '+SelectedCombo(valuesel,"Discover")+' value="Discover">Discover</option>'+
			'</select>';
	return cad;								
}

function SelectedCombo(valuesel,valuecomp)//Marca la opcion Selected en funcion del valor que tenga ese registro
{
	if (valuesel.toLowerCase()==valuecomp.toLowerCase()) return " selected ";
	return "";								
}

function VerTarjetaCheque(tarjeta,cheque)//mostrar las filas de pago tarjeta de credito o cheque
{
	if( (tarjeta=='' || tarjeta==null || tarjeta=='none') &&  (cheque=='' || cheque==null || cheque=='none') )//no tiene forma de pago ni tarjeta de credito ni cheque
	{
		document.getElementById("rowCard1").style.display="inline";
		document.getElementById("rowCard2").style.display="inline";
		document.getElementById("rowCard3").style.display="none";
	}
	else
	{
		if( (tarjeta!='' && tarjeta!=null && tarjeta!='none') && (cheque=='' || cheque==null || cheque=='none') )//paga con tarjeta de credito 
		{
			document.getElementById("rowCard1").style.display="inline";
			document.getElementById("rowCard2").style.display="inline";
			document.getElementById("rowCard3").style.display="none";
		}
		if((cheque!='' && cheque!=null && cheque!='none') && (tarjeta=='' || tarjeta==null || tarjeta=='none') )//paga con cheque
		{
			document.getElementById("rowCard1").style.display="none";
			document.getElementById("rowCard2").style.display="none";
			document.getElementById("rowCard3").style.display="inline";
		}
	}
}

function generarParametros()//Generar los parametros que se van a pasar para actualizar
{
	var parametros=""
	parametros+="name="+document.getElementById("txNameDeudor").value;
	parametros+="&surname="+document.getElementById("txSurnameDeudor").value;	
	parametros+="&email="+document.getElementById("txEmailDeudor").value;	
	parametros+="&pass="+document.getElementById("txPassDeudor").value;	
	parametros+="&address="+document.getElementById("txAddressDeudor").value;
	parametros+="&hometelephone="+document.getElementById("txHomephoneDeudor").value;	
	parametros+="&mobiletelephone="+document.getElementById("txMobilephoneDeudor").value;	
	parametros+="&country="+document.getElementById("sCountry").options[document.getElementById("sCountry").selectedIndex].value;
	parametros+="&state="+document.getElementById("sState").options[document.getElementById("sState").selectedIndex].value;
	parametros+="&zipcode="+document.getElementById("txZipcodeDeudor").value;
	parametros+="&licensen="+document.getElementById("txLicensenDeudor").value;
	parametros+="&officenum="+document.getElementById("txOfficenumDeudor").value;
	parametros+="&brokern="+document.getElementById("txBrokernDeudor").value;
	parametros+="&privilege="+document.getElementById("cbChangePrivilege").options[document.getElementById("cbChangePrivilege").selectedIndex].value;
	parametros+="&usertype="+document.getElementById("txUsertypeDeudor").value;
	parametros+="&status="+document.getElementById("cbChangeStatus").options[document.getElementById("cbChangeStatus").selectedIndex].value;
	parametros+="&procode="+document.getElementById("txProcodeDeudor").value;
	parametros+="&cardtype="+document.getElementById("cbCardtype").options[document.getElementById("cbCardtype").selectedIndex].value;
	parametros+="&cardholdername="+document.getElementById("txCardholdernameDeudor").value;
	parametros+="&cardnumber="+document.getElementById("txCardnumberDeudor").value;
	parametros+="&expirationdate="+document.getElementById("txExpirationdateDeudor").value;
	parametros+="&csvnumber="+document.getElementById("txCsvnumberDeudor").value;
	if(document.getElementById("useCheck").checked==true)//pago con cheque
		parametros+="&billingaddress="+document.getElementById("txBillingaddressDeudorCheque").value;
	else//pago con tarjeta de credito
		parametros+="&billingaddress="+document.getElementById("txBillingaddressDeudorCard").value;
	parametros+="&billingcity="+document.getElementById("txBillingcityDeudor").value;
	parametros+="&billingstate="+document.getElementById("txBillingstateDeudor").value;
	parametros+="&billingzip="+document.getElementById("txBillingzipDeudor").value;
	parametros+="&routingnumber="+document.getElementById("txRoutingnumberDeudor").value;
	parametros+="&bankaccount="+document.getElementById("txBankaccountDeudor").value;
	parametros+="&idprod="+document.getElementById("cbProduct").options[document.getElementById("cbProduct").selectedIndex].value;
	parametros+="&priceprod="+document.getElementById("cbProduct").options[document.getElementById("cbProduct").selectedIndex].title;
	return parametros;
}

function GuardarUsuario(userid,itemid)//hace el UPDATE en la tabla
{
	if(document.getElementById("txNameDeudor").value==''){alert("Please enter your Name");return;}
	if(document.getElementById("txSurnameDeudor").value==''){alert("Please enter your Last Name");return;}
	if(document.getElementById("txEmailDeudor").value==''){alert("Please enter your E-Mail");return;}
	if(document.getElementById("txPassDeudor").value==''){alert("Please enter your Password");return;}
	if(document.getElementById("txHomephoneDeudor").value==''){alert("Please enter your Home Phone");return;}
	if(document.getElementById("txLicensenDeudor").value==''){alert("Please enter your Licence Number");return;}
	if(document.getElementById("cbChangePrivilege").selectedIndex==0){alert("Please select your Privilege");return;}
	if(document.getElementById("cbChangeStatus").selectedIndex==0){alert("Please select your Status");return;}
	if(document.getElementById("useCheck").checked==true)//pago con cheque
	{
		if(document.getElementById("txRoutingnumberDeudor").value==''){alert("Please enter a Routing Number");return;}
		if(document.getElementById("txBankaccountDeudor").value==''){alert("Please enter a Bank Account");return;}
		if(document.getElementById("txBillingaddressDeudorCheque").value==''){alert("Please enter a Billing Address");return;}
	}
	else//pago con tarjeta de credito
	{
		if(document.getElementById("cbCardtype").selectedIndex==0){alert("Please select  a Card Type");return;}
		if(document.getElementById("txCardholdernameDeudor").value==''){alert("Please enter a Holder Name");return;}
		if(document.getElementById("txCardnumberDeudor").value==''){alert("Please enter a Card Number");return;}
		if(document.getElementById("txExpirationdateDeudor").value==''){alert("Please enter a Expiraton Date");return;}
		if(document.getElementById("txCsvnumberDeudor").value==''){alert("Please enter a CSV Number");return;}
		if(document.getElementById("txBillingaddressDeudorCard").value==''){alert("Please enter a Billing Address");return;}
	}
	if(document.getElementById("txBillingcityDeudor").value==''){alert("Please enter a Billing City");return;}
	if(document.getElementById("txBillingstateDeudor").value==''){alert("Please enter a Billing State");return;}
	if(document.getElementById("txBillingzipDeudor").value==''){alert("Please enter a Billing Zip Code");return;}
	if(document.getElementById("cbProduct").selectedIndex==0){alert("Please select a Product");return;}
	document.getElementById("reloj").style.visibility="visible";
	parametros=generarParametros();
	var ajax=nuevoAjax();
	ajax.open("POST", "respuesta_users.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(parametros+"&oper=modificar"+"&ID="+userid);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)	
		{
			var results=ajax.responseText;
//alert(results);			
			document.getElementById("reloj").style.visibility="hidden";
			FillGrid();
			MostrarDetalles(userid,itemid);
		}
	}
}

function MostrarDatosSuspendido()//Mostrar los datos del usuario suspendido
{
	if(useridSuspendido!='' && useridSuspendido!=null)
	{
		var ajax=nuevoAjax();
		ajax.open("POST", "resp_usersuspend.php", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("userid="+useridSuspendido);
		ajax.onreadystatechange=function()
		{
			if (ajax.readyState==4)	
			{
				var results=ajax.responseText;
				var VUsers=new Array();
				VUsers=eval('['+results+']');
//TIPO DE CLIENTES--------------------------------------------------------------------------------
				switch(VUsers[0].usertype)
				{
					case 'Realtor': 	document.getElementById("rRealtor").checked=true;
										document.getElementById("rBroker").checked=false;
										document.getElementById("rInvestor").checked=false;
										document.getElementById("txtRealtor").value=VUsers[0].licensen;
										document.getElementById("txtOffice").value=VUsers[0].officenum;
										document.getElementById("office").style.visibility='visible';
										document.getElementById("_office").style.visibility='hidden';
										document.getElementById("_office2").style.visibility='hidden';
										document.getElementById("_office3").style.visibility='hidden';
										document.getElementById("officey").checked=false;
										document.getElementById("officen").checked=true;
										break;
					case 'RealtorWO': 	document.getElementById("rRealtor").checked=true;
										document.getElementById("rBroker").checked=false;
										document.getElementById("rInvestor").checked=false;
										document.getElementById("txtRealtor").value=VUsers[0].licensen;
										document.getElementById("txtOffice").value=VUsers[0].officenum;
										document.getElementById("office").style.visibility='visible';
										document.getElementById("_office").style.visibility='visible';
										document.getElementById("_office2").style.visibility='visible';
										document.getElementById("_office3").style.visibility='visible';
										document.getElementById("officey").checked=true;
										document.getElementById("officen").checked=false;
//alert("rw userid="+useridSuspendido);
										break;
					case 'Broker': 		document.getElementById("rRealtor").checked=false;
										document.getElementById("rBroker").checked=true;
										document.getElementById("rInvestor").checked=false;
										document.getElementById("txtBroker").value=VUsers[0].brokern;
										document.getElementById("txtOffice").value=VUsers[0].officenum;
										document.getElementById("office").style.visibility='hidden';
										document.getElementById("_office").style.visibility='visible';
										document.getElementById("_office2").style.visibility='visible';
										document.getElementById("_office3").style.visibility='visible';
										break;
					case 'Investor':	document.getElementById("rRealtor").checked=false;
										document.getElementById("rBroker").checked=false;
										document.getElementById("rInvestor").checked=true;
										document.getElementById("licence1").style.visibility='hidden';
										document.getElementById("licence").style.visibility='hidden';
										document.getElementById("divRealtor").style.visibility='hidden';
										document.getElementById("divBroker").style.visibility='hidden';
										document.getElementById("office").style.visibility='hidden';
										document.getElementById("_office").style.visibility='hidden';
										document.getElementById("_office2").style.visibility='hidden';
										document.getElementById("_office3").style.visibility='hidden';
										break;
				}
//Contact Information--------------------------------------------------------------------------------
				document.getElementById("txtFirstName").value=VUsers[0].name;
				document.getElementById("txtLastName").value=VUsers[0].surname;
				var field=document.getElementById("sCountry");
				var enc=0;
				for (i=0; i<field.length;i++)
				{
					if(field[i].value==VUsers[0].country)
					{
						enc=1;
						break;
					}
				}
				if(enc==1)
					field[i].selected=true;
				selectCountry();
				MarcaSelectedState(VUsers[0].state);
				document.getElementById("txtCity").value=VUsers[0].city;
				document.getElementById("txtAddress1").value=VUsers[0].address;
				document.getElementById("txtZip").value=VUsers[0].zipcode;
				document.getElementById("txtMobil").value=VUsers[0].mobiletelephone;
				document.getElementById("txtPhone").value=VUsers[0].hometelephone;
				document.getElementById("txtEmail").value=VUsers[0].email;
				document.getElementById("txtEmail2").value=VUsers[0].email;
				document.getElementById("txtPassword").value=VUsers[0].pass;
				document.getElementById("txtPassword2").value=VUsers[0].pass;
//Billing Information--------------------------------------------------------------------------------
				if(VUsers[0].cardtype!='' && VUsers[0].cardtype!=null)
				{
					document.getElementById("useCheck").checked==false;
					field=document.getElementById("cardtype");
					enc=0;
					for (i=0; i<field.length;i++)
					{
						if(field[i].value==VUsers[0].cardtype)
						{
							enc=1;
							break;
						}
					}
					if(enc==1)field[i].selected=true;
					document.getElementById("holder").value=VUsers[0].cardholdername;
					document.getElementById("cardnumber").value=VUsers[0].cardnumber;
					document.getElementById("exdate").value=VUsers[0].expirationdate;
					document.getElementById("csv").value=VUsers[0].csvnumber;
				
				}
				else
				{
					document.getElementById("useCheck").checked==true;
					document.getElementById("routing").value=VUsers[0].routingnumber;
					document.getElementById("bankA").value=VUsers[0].bankaccount;
				}
				if(VUsers[0].billingaddress==VUsers[0].address)
					document.getElementById("same").checked=true;
				else
					document.getElementById("same").checked=false;
				document.getElementById("sameAdd").value=VUsers[0].billingaddress;
				document.getElementById("sameZip").value=VUsers[0].billingzip;
				document.getElementById("sameCity").value=VUsers[0].billingcity;
				document.getElementById("sameState").value=VUsers[0].billingstate;
//Promotion Code, Account Executive y Productos--------------------------------------------------------------------------------
				if(VUsers[0].procode!='' && VUsers[0].procode!=null)
				{
					document.getElementById("prom").value=VUsers[0].procode.toUpperCase();
					MostrarProductos();
				}
				document.getElementById("txtSaler").value=VUsers[0].executive;
//AcceptDeclineRead Terms and Conditions--------------------------------------------------------------------------------
					document.getElementById("accept").checked=false;
					document.getElementById("denie").checked=true;
			} //fin de ajax readyState
		}//fin de ajax.onreadystatechange
	}
}