//************************ [ Maria Oliveira]
//FUNCION LLAMADA EN onblur EN LA PAGINA register.php EN EL CAMPO email
//COMPRUEBA QUE EL EMAIL NO EXISTA EN LA BASE DE DATOS

function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(E){
				xmlhttp = false;
			}
		}
		if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
			xmlthttp = new XMLHttpRequest();
		}
		return xmlhttp;
}
	
function MostrarError(){
	var email;
	if(document.getElementById("txtEmail")){
		email = document.getElementById('txtEmail').value;
	}else{ email= document.getElementById('usr_email').value; }
	

	var ajax=nuevoAjax();
	ajax.open("POST", "includes/errorMail.php",true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("email="+email);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			var resultado=ajax.responseText;
			var error=document.getElementById("errorEmail");
			if(resultado!="")	document.getElementById("btnsubmit").disabled=true; 
			else if(resultado=="") document.getElementById("btnsubmit").disabled=false;
			error.innerHTML=resultado;
		}
	}
}

function checkMail2(email){
	ajax=objetoAjax();
	ajax.open("POST", "includes/errorMail.php",true);
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("email="+email+"&check=Yes");
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			var resultado=ajax.responseText;
			var error=document.getElementById("errorEmail");
			if(resultado!="")	document.getElementById("btnsubmit").disabled=true; 
			else if(resultado=="") document.getElementById("btnsubmit").disabled=false;
			error.innerHTML=resultado;
		}
	}
}
