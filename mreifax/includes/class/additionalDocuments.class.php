<?php
	class additionalDocuments{
		function additionalDocuments(){
			return true;
		}
		function add($type,$data,$users){
			switch((string)$type){
				case "backoffice":
					return $this->addFromBackoffice($data,$users);
				break;
			}
		}
		private function addFromBackoffice($data,$users){
			$positions	=	array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6");
			$path		=	'C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs';
			$usersResult=	array();
			foreach($data->contracts as $contract){
				$contractOriginalID	=	$contract->idContract;
				$query	=	"SELECT * FROM xima.contracts_addonscustom WHERE type='$contractOriginalID' AND userid={$users->userid}";
				if(!mysql_query($query))
					$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"Error en el Query Linea:". __LINE__ ,"detail"=>mysql_error());
				else{
					if(mysql_affected_rows()==6)
						$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No tiene disponibilidad para otro documento adicional","detail"=>"");
					else{
						$query="SELECT GROUP_CONCAT(place) as positions FROM xima.contracts_addonscustom WHERE type='$contractOriginalID'";
						if(!$result=mysql_query($query))
							$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"Error en el Query Linea:". __LINE__ ,"detail"=>mysql_error());
						else{
							$result=mysql_fetch_assoc($result);
							$result=$result['positions'];
							foreach(explode(",",$result) as $index => $data)
								unset($positions[$data]);
							$positions	=	array_values($positions);
							foreach($contract->addons as $addonIndex => $addon){
								$query	=	"SELECT * FROM xima.contracts_addonscustom WHERE BINARY addon_name='{$addon->addonRealName}' AND userid={$users->userid} AND type='$contractOriginalID'";
								if(!$result	=	mysql_query($query))
									$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"Error en el Query Linea:". __LINE__ ,"detail"=>mysql_error());
								else{
									if(mysql_affected_rows()>0)
										$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"Ya Tiene un documento adicional llamado: ".str_replace(strstr($_POST["file"],"."),"",$_POST["file"]),"detail"=>"");
									else{	
										$placeAddon=$positions[$addonIndex];
										$userIndex	=	$users->userid;
										$newFileName = "$contractOriginalID-$userIndex-$placeAddon.pdf";
										
										if(!@copy($path.'/template/contracts/MDocuments/'.$addon->addonFileName, $path."/temp_change_contract/".$newFileName))
											$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Primer Paso Interno","detail"=>implode(" ",error_get_last()));
										else{
											if(!@copy($path."/temp_change_contract/".$newFileName, $path.'/addons/'.$newFileName)){
												$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Segundo Paso Interno","detail"=>implode(" ",error_get_last()));
												@unlink($path."/temp_change_contract/".$newFileName);
											}else{
												if(!@unlink($path."/temp_change_contract/".$newFileName))
													$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Tercer Paso Interno","detail"=>implode(" ",error_get_last()));
												else{
													$id_name = $this->getmicrotime("Hisu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios

													system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/addons/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/addons/$newFileName\"", $valid);

													if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
													
														if(!@unlink($path.'/addons/'.$newFileName))
															$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Quinto Paso Interno","detail"=>implode(" ",error_get_last()));
														else{
															
															if(!@rename($path.'/addons/temp_by_jesus'.$id_name.'.pdf', $path.'/addons/'.$newFileName))
																$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Sexto Paso Interno","detail"=>implode(" ",error_get_last()));
															else{
																$query = "
																	INSERT INTO xima.contracts_addonscustom 
																		(userid, type, place, addon_act, addon_name, document)
																		VALUES 
																		('$userIndex','$contractOriginalID','$placeAddon','1','{$addon->addonRealName}','$newFileName')";
																
																if(mysql_query($query)){
																	insertlogs($_SESSION['bkouserid'], $users->userid, 'Additional document has been added', "Original: id:".mysql_insert_id()." Name: {$addon->addonRealName}");
																	require_once($_SERVER["DOCUMENT_ROOT"].'/custom_contract/fpdf/fpdf.php');
																	require_once($_SERVER["DOCUMENT_ROOT"].'/custom_contract/fpdi/fpdi.php');
																	
																	if($_SERVER['HTTP_HOST']=='developer.reifax.com')
																		copy($path.'/addons/'.$newFileName, 'C:/inetpub/wwwroot/xima3/mysetting_tabs/mycontracts_tabs/addons/'.$newFileName);
																	
																	$pdf = new FPDI();
																	$addonPages = $pdf->setSourceFile($path."/addons/$newFileName");
																	unset($pdf);
																	$addonOriginalID=mysql_insert_id()."-$userIndex-$placeAddon";
																	
																	//Asign Templates to Contract
																	if($addon->addonTemplate!='0000'){
																		$_POST['idfunction']		=	'7';
																		$_POST['userCustom']		=	$userIndex;
																		$_POST['pages_new']			=	$addonPages;
																		$_POST['template_original']	=	$addon->addonTemplate;
																		$_POST['idContractCustom']	=	$addonOriginalID;
																		$tempContent=$this->requireExternalPhp($_POST);
																		unset($_POST['idfunction']);
																		unset($_POST['userCustom']);
																		unset($_POST['pages_new']);
																		unset($_POST['template_original']);
																		unset($_POST['idContractCustom']);
																		if($tempContent->success){
																			//Verificamos si ya tiene el Template para Eliminarcelo.!
																			$query	=	"
																				SELECT b.temp_id AS delUsuario FROM contracts_templates a 
																					INNER JOIN (SELECT * FROM contracts_templates WHERE temp_userid='{$users->userid}') b 
																					ON a.temp_name=b.temp_name 
																				WHERE a.temp_id='{$addon->addonTemplate}'";
																			$result	=	mysql_query($query);
																			if(mysql_affected_rows()>0){
																				$result	=	mysql_fetch_object($result);
																				$query	=	"DELETE FROM contracts_templates_campos WHERE temp_temp_id='{$result->delUsuario}'";
																				mysql_query($query);
																				$query	=	"DELETE FROM contracts_templates WHERE temp_id='{$result->delUsuario}'";
																				mysql_query($query);
																			}
																			//Le Agregamos el Nuevo Template para que lo tenga actualizado y disponible
																			$query	=	"
																				INSERT INTO contracts_templates 
																					SELECT null,'$userIndex',temp_name FROM contracts_templates 
																					WHERE temp_id='{$addon->addonTemplate}' AND temp_userid NOT IN ('9999999','0')";
																			if(mysql_query($query)){
																				if(mysql_affected_rows()>0){
																					$query	=	"
																						INSERT INTO contracts_templates_campos 
																							SELECT null,LAST_INSERT_ID(),'$userIndex',temp_page,temp_type,temp_idtc,temp_text,temp_posx,temp_posy FROM contracts_templates_campos 
																							WHERE temp_temp_id='{$addon->addonTemplate}'";
																					mysql_query($query);
																				}
																			}
																		}else{
																			$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"El documento adicional se subi� pero no se le aplico el template","detail"=>$tempContent->mensaje);
																		}
																	}
																}else{
																	$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"Contract Can't be Uploaded! 1","detail"=>mysql_error());
																	@unlink($path.'/addons/'.$newFileName);
																}
															}
														}
													}else
														$usersResult[]	=	array("userName"=>$users->name." ".$users->surname,"error"=>"No se pudo Realizar el Cuarto Paso Interno","detail"=>implode(" ",error_get_last()));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return array("result"=>$usersResult);
		}
		private function requireExternalPhp($_POST){
			ob_start();
			require($_SERVER['DOCUMENT_ROOT'].'/custom_contract/includes/php/functions.php');
			$tempContent = ob_get_contents();
			ob_end_clean();
			return json_decode($tempContent);
		}
		private function getmicrotime($format, $utimestamp = null) {
			if (is_null($utimestamp))
			$utimestamp = microtime(true);

			$timestamp = floor($utimestamp);
			$milliseconds = round(($utimestamp - $timestamp) * 1000000);

			return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
		}
	}
?>