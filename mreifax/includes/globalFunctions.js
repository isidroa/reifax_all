function RF_in_array (needle, haystack, argStrict) {
  var key = '',
    strict = !! argStrict;

  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true;
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) {
        return true;
      }
    }
  }

  return false;
}
/*****************************************
*	By Jesus Similar to in_array as in PHP
*****************************************/
function empty (mixed_var) {
	// Checks if the argument variable is empty
	// undefined, null, false, number 0, empty string,
	// string "0", objects without properties and empty arrays
	// are considered empty
	//
	// *     example 1: empty(null);
	// *     returns 1: true
	// *     example 2: empty(undefined);
	// *     returns 2: true
	// *     example 3: empty([]);
	// *     returns 3: true
	// *     example 4: empty({});
	// *     returns 4: true
	// *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
	// *     returns 5: false
	var undef, key, i, len;
	var emptyValues = [undef, null, false, 0, "", "0"];

	for (i = 0, len = emptyValues.length; i < len; i++) {
		if (mixed_var === emptyValues[i])
			return true;
	}

	if (typeof mixed_var === "object") {
		for (key in mixed_var) {
	// TODO: should we check for own properties only?
	//if (mixed_var.hasOwnProperty(key)) {
		return false;
	//}
		}
		return true;
	}

		return false;
	}
  /*****************************************
  *	By Jesus Similar to strpos as in PHP
  *****************************************/
  function strpos (haystack, needle, offset) {
    // *     example 1: strpos('Kevin van Zonneveld', 'e', 5);
    // *     returns 1: 14
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
  }

  /********************************************************************
  *	By Jesus, Similar to Sleep in PHP
  ********************************************************************/
  function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }
