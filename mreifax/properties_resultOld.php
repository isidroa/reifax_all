<?php 
//print_r($_COOKIE['query_search']);
$_SERVERXIMA="http://www.reifax.com/";
$realtor=$_POST['userweb']=="false" ? false:true;	
$realtorid=$_POST['realtorid'];

$platinum=2;
if(isset($_COOKIE['datos_usr']['USERID'])){
	include('properties_conexion.php');
	conectar();
	
	$query='select * from xima.permission WHERE userid='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$permission=$r;
}

?> 
<div align="left" style="height:100%">
	<div id="body_central" style="height:100%">
		<div id="tabsResult" style="padding-top:2px;"></div>
	</div>
</div>

<script>
if(document.getElementById('result_control_mapa_div')){
	mapResult.map.getRootElement().removeChild(mapResult._mapTool);
}
if(document.getElementById('result_control_mapa_divAdv')){
	mapResultAdv.map.getRootElement().removeChild(mapResultAdv._mapTool);
}

var tabsResult=null;
var ancho=640;
var selected_dataR = new Array();
var deselected_dataR = new Array();
var AllCheckR=false;
var selected_dataRFG = new Array();
var deselected_dataRFG = new Array();
var AllCheckRFG=false;
<?php if($_COOKIE['datos_usr']['idstatus']!=8 && $_COOKIE['datos_usr']['idstatus']!=9){?>
if(user_loged) ancho=system_width;
<?php }?>

var mapResult = mapResultAdv = mapResultAdvFG = null;
var icon_result=icon_mylabel=false;
var ResultTemplate=-1;
var ResultTemplateFG=-1;
if(realtor_block!=false){
	var icon_result=true; 
	var icon_mylabel=true;
}
if(user_web!=false)	var icon_mylabel=true;

tabsResult = new Ext.TabPanel({
	renderTo: 'tabsResult',
	activeTab: 0,
	width: ancho,
	height: tabs.getHeight(),
	plain:true,
	enableTabScroll:true,
	defaults:{	autoScroll: false},
	listeners: {
		'tabchange': function(tabpanel,tab){
			if(tab){
				if(tab.id!='BasicResult'){
					if(document.getElementById('result_control_mapa_div'))
						document.getElementById('result_control_mapa_div').style.display='none';
					if(document.getElementById('result_control_mapa_divAdv'))
						document.getElementById('result_control_mapa_divAdv').style.display='';
				}else{
					if(document.getElementById('result_control_mapa_div'))
						document.getElementById('result_control_mapa_div').style.display='';
					if(document.getElementById('result_control_mapa_divAdv'))
						document.getElementById('result_control_mapa_divAdv').style.display='none';
				}
			}
		}
	},
	items:[
		{
			title: 'Basic Result',
			id: 'BasicResult',
			autoLoad: {url: 'result_tabs/properties_basic_result.php', timeout: 10800, scripts: true, params: {userweb: '<?php echo $_POST['userweb'];?>', realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>,systemsearch: '<?php echo $_POST['systemsearch'];?>'}},
			tbar: new Ext.Toolbar({
				id:'menu_result_bas',
				cls: 'no-border',
				width: 'auto',
				items: [' ',
					{
						 tooltip: 'Show/Hide Map',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 hidden:icon_result,
						 icon: 'http://www.reifax.com/img/toolbar/map.png',
						 id: 'toolbarMapResult',
						 enableToggle: true,
						 handler: function(){
							if(document.getElementById('mapResult').style.display=='none'){
								document.getElementById('mapResult').style.display='';
																	
								if(document.getElementById('result_control_mapa_div'))
									mapResult.map.getRootElement().removeChild(mapResult._mapTool);
								mapResult.curBoton="AVG";
								mapResult.ins_toolbar("320px","overview");
								
							}else{
								document.getElementById('mapResult').style.display='none';
								if(document.getElementById('result_control_mapa_div'))
									mapResult.map.getRootElement().removeChild(mapResult._mapTool);
							}
						 }
					},{
						 tooltip: 'View Legend',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/toolbar/legend.png',
						 hidden:icon_result,
						 handler: function(){
							var dataLegend = [
								['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
								['A-F','http://www.reifax.com/img/houses/verdel.png','Active Foreclosed'],
								['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Foreclosed Sold'],
								['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Foreclosed'],
								['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Foreclosed Sold'],
								['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
								['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Foreclosed'],
								['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Foreclosed Sold'],
								['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Foreclosed'],
								['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Foreclosed Sold'],
								['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
								['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Foreclosed'],
								['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Foreclosed Sold'],
								['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Foreclosed'],
								['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Foreclosed Sold'],
								['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
								['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Foreclosed'],
								['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Foreclosed Sold'],
								['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Foreclosed'],
								['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Foreclosed Sold'],
								['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
							];
							 
							 var store = new Ext.data.ArrayStore({
								idIndex: 0,
								fields: [
									'status', 'url', 'description'
								]
							 });
							
							store.loadData(dataLegend);
							
							 var listView = new Ext.list.ListView({
								store: store,
								multiSelect: false,
								emptyText: 'No Legend to display',
								columnResize: false,
								columnSort: false,
								columns: [{
									header: 'Color',
									width: .15,
									dataIndex: 'url',
									tpl: '<img src="{url}">'
								},{
									header: 'Status',
									width: .2,
									dataIndex: 'status'
								},{
									header: 'Description',
									dataIndex: 'description'
								}]
							});
							
							var win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: listView,
					
								buttons: [{
									text     : 'Print',
									handler  : function(){
										var htmlTag = new Array();
										var i=0;
										
										htmlTag.push('<table>'+
											'<tr>'+
												'<td>Color</td>'+
												'<td>Status</td>'+
												'<td>Description</td>'+
											'</tr>');
										
										while(i<dataLegend.length){
											htmlTag.push(
											'<tr>'+
												'<td><img src="'+dataLegend[i][1]+'" /></td>'+
												'<td>'+dataLegend[i][0]+'</td>'+
												'<td>'+dataLegend[i][2]+'</td>'+
											'</tr>');
											i++;
										}
										htmlTag.push('</table>');

										var WindowObject = window.open('', "TrackHistoryData", 
															  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
										
										WindowObject.document.write(htmlTag);
										WindowObject.document.close();
										WindowObject.focus();
										WindowObject.print();
										WindowObject.close();
									}
								},{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
							 tooltip: 'XRay Report',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/exray.png',
							 handler: function(){
								 if(!user_loged){ login_win.show(); return false;}
								 else if(user_block || user_web){Ext.Msg.alert('Warning', 'You are either not logged in or are not allowed to view detailed information.'); return false;}
								 var latlong = document.getElementById('result_mapa_search_latlong').value;
								 
								 if(latlong=='-1'){
									Ext.Msg.alert("XRay Report", 'Map shape/polygon is required to execute the XRay Report.');
									return false;
								}
								 
								 var simple = new Ext.FormPanel({
									url: 'reports_types/properties_xray.php',
									frame:true,
									title: 'Xray Report.',
									width: 400,
									
									items: [{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['texto', 'valor'],
											data : [
													['Single Family','01'],
													['Condo/Town/Villa','04'],
													['Multi Family +10','03'],
													['Multi Family -10','08'],
													['Commercial','11'],
													['Vacant Land','00'],
													['Mobile Home','02'],
													['Other','99']
											]
										}),
										displayField:'texto',
										valueField: 'valor',
										name: 'proptypeName',
										fieldLabel: 'Property Type',
										mode: 'local',
										value: '01',
										hiddenName: 'proptype',
										hiddenValue:'01',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									}],
									
									buttons: [{
											text: 'Continue',
											handler: function(){
												var values = simple.getForm().getValues();
												<?php if($_POST['systemsearch']=='advance'){?>
												var bd = document.getElementById('occounty').value;
												<?php }else{?>
												var bd = document.getElementById(search_type+'_county_search').value;
												<?php }?>
												var xcode = values.proptype;
												win.close();
			 
												if(document.getElementById('reportsTab')){
													var tab = tabs.getItem('reportsTab');
													tabs.remove(tab);
												}
												
												tabs.add({
													title: ' Reports ',
													id: 'reportsTab',
													autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, timeout: 10800, params: {latlong:latlong}},
													closable: true,
													tbar: new Ext.Toolbar({
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															 tooltip: 'Print XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/printer.png',
															 handler: function(){
																Ext.Ajax.request( 
																{  
																	waitMsg: 'Printing...',
																	url: 'imprimir/properties_xray_print.php', 
																	method: 'POST',
																	timeout :600000,
																	params: { 
																		db: bd,
																		proper: xcode,
																		type: 'P',
																		latlong: latlong
																	},
																	
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','ERROR');
																	},
																	success:function(response,options){
																		
																		var results=response.responseText;
																		if(Ext.isIE)
																			window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
																		 else
																			window.open(results,'_newtab');
																	}                                
																});
															 }
														},{
															 tooltip: 'Save XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/save.png',
															 handler: function(){
																 var simple = new Ext.FormPanel({
																	url: 'imprimir/properties_xray_print.php',
																	frame:true,
																	title: 'Saved Documents.',
																	width: 400,
																	waitMsgTarget : 'Saving Documents...',
																	
																	items: [{
																				xtype     : 'textfield',
																				name      : 'name_save',
																				fieldLabel: 'Name',
																				value     : '',
																				width: 200
																			},{
																				xtype     : 'hidden',
																				name      : 'db',
																				value     : bd
																			},{
																				xtype     : 'hidden',
													
																				name      : 'proper',
																				value     : xcode
																			},{
																				xtype     : 'hidden',
																				name      : 'type',
																				value     : 'S'
																			},{
																				xtype     : 'hidden',
																				name      : 'latlong',
																				value     : latlong
																			}],
																	
																	buttons: [{
																			text: 'Save',
																			handler: function(){
																				loading_win.show();
																				simple.getForm().submit({
																					success: function(form, action) {
																						loading_win.hide();
																						win.close();
																						Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																					},
																					failure: function(form, action) {
																						loading_win.hide();
																						Ext.Msg.alert("Failure", action.result.msg);
																					}
																				});
																			}
																		},{
																			text: 'Cancel',
																			handler  : function(){
																					simple.getForm().reset();
																					win.close();
																				}
																		}]
																	});
																 
																var win = new Ext.Window({
																	layout      : 'fit',
																	width       : 400,
																	height      : 170,
																	modal	 	: true,
																	plain       : true,
																	items		: simple,
																	closeAction : 'hide',
																	buttons: [{
																		text     : 'Close',
																		handler  : function(){
																			win.close();
																		}
																	}]
																});
																win.show();
															 }
														},'->',{
															 tooltip: 'Close XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/cancel.png',
															 handler: function(){
																 var tab = tabs.getItem('reportsTab');
																 tabs.remove(tab);
															 }
														}]
													})
												}).show();
											}
										},{
											text: 'Cancel',
											handler  : function(){
													simple.getForm().reset();
													win.close();
												}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 170,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'hide',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
							 }
							},{
							 tooltip: 'Discount Report',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
							 handler: function(){
								 if(!user_loged){ login_win.show(); return false;}
								 else if(user_block || user_web){Ext.Msg.alert('Warning', 'You are either not logged in or are not allowed to view detailed information.'); return false;}
								 var latlong = document.getElementById('result_mapa_search_latlong').value;
								 
								 if(latlong=='-1'){
									Ext.Msg.alert("Discount Report", 'Map shape/polygon is required to execute the Discount Report.');
									return false;
								}
								 
								 var simple = new Ext.FormPanel({
									url: 'reports_types/properties_xray.php',
									frame:true,
									title: 'Discount Report.',
									width: 400,
									
									items: [{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['texto', 'valor'],
											data : [
													['Single Family','01'],
													['Condo/Town/Villa','04'],
													['Multi Family +10','03'],
													['Multi Family -10','08'],
													['Commercial','11'],
													['Vacant Land','00'],
													['Mobile Home','02'],
													['Other','99']
											]
										}),
										displayField:'texto',
										valueField: 'valor',
										name: 'proptypeName',
										fieldLabel: 'Property Type',
										mode: 'local',
										value: '01',
										hiddenName: 'proptype',
										hiddenValue:'01',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									}],
									
									buttons: [{
											text: 'Continue',
											handler: function(){
								 				var values = simple.getForm().getValues();
												<?php if($_POST['systemsearch']=='advance'){?>
												var bd = document.getElementById('occounty').value;
												<?php }else{?>
												var bd = document.getElementById(search_type+'_county_search').value;
												<?php }?>
												var xcode = values.proptype;
												win.close();
															 
												 if(document.getElementById('reportsTab')){
													 var tab = tabs.getItem('reportsTab');
													 tabs.remove(tab);
												 }
												 
												tabs.add({
													title: ' Reports ',
													id: 'reportsTab',
													autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, timeout: 10800, params: {latlong:latlong}},
													closable: true,
													tbar: new Ext.Toolbar({
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															 tooltip: 'Print Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/printer.png',
															 handler: function(){
																Ext.Ajax.request( 
																{  
																	waitMsg: 'Printing...',
																	url: 'imprimir/properties_rebate_print.php', 
																	method: 'POST',
																	timeout :600000,
																	params: { 
																		db: bd,
																		proper: xcode,
																		type: 'P',
																		latlong: latlong
																	},
																	
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','ERROR');
																	},
																	success:function(response,options){
																		
																		var results=response.responseText;
																		if(Ext.isIE)
																			window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
																		 else
																			window.open(results,'_newtab');
																	}                                
																});
															 }
														},{
															 tooltip: 'Save Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/save.png',
															 handler: function(){
																 var simple = new Ext.FormPanel({
																	url: 'imprimir/properties_rebate_print.php',
																	frame:true,
																	title: 'Saved Documents.',
																	width: 400,
																	waitMsgTarget : 'Saving Documents...',
																	
																	items: [{
																				xtype     : 'textfield',
																				name      : 'name_save',
																				fieldLabel: 'Name',
																				value     : '',
																				width: 200
																			},{
																				xtype     : 'hidden',
																				name      : 'db',
																				value     : bd
																			},{
																				xtype     : 'hidden',
				
																				name      : 'proper',
																				value     : xcode
																			},{
																				xtype     : 'hidden',
																				name      : 'type',
																				value     : 'S'
																			},{
																				xtype     : 'hidden',
																				name      : 'latlong',
																				value     : latlong
																			}],
																	
																	buttons: [{
																			text: 'Save',
																			handler: function(){
																				loading_win.show();
																				simple.getForm().submit({
																					success: function(form, action) {
																						loading_win.hide();
																						win.close();
																						Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																					},
																					failure: function(form, action) {
																						loading_win.hide();
																						Ext.Msg.alert("Failure", action.result.msg);
																					}
																				});
																			}
																		},{
																			text: 'Cancel',
																			handler  : function(){
																					simple.getForm().reset();
																					win.close();
																				}
																		}]
																	});
																 
																var win = new Ext.Window({
																	layout      : 'fit',
																	width       : 400,
																	height      : 170,
																	modal	 	: true,
																	plain       : true,
																	items		: simple,
																	closeAction : 'hide',
																	buttons: [{
																		text     : 'Close',
																		handler  : function(){
																			win.close();
																		}
																	}]
																});
																win.show();
															 }
														},'->',{
															 tooltip: 'Close Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/cancel.png',
															 handler: function(){
																 var tab = tabs.getItem('reportsTab');
																 tabs.remove(tab);
															 }
														}]
													})
												}).show();
											}
										},{
											text: 'Cancel',
											handler  : function(){
													simple.getForm().reset();
													win.close();
												}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 170,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'hide',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
								 
							 }
					},{
						 tooltip: 'Print Report',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
					 	 icon: 'http://www.reifax.com/img/toolbar/printer.png',
						 hidden:icon_result,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							loading_win.show();
							Ext.Ajax.request( 
								{  
									waitMsg: 'Printing Report...',
									url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
									method: 'POST', 
									timeout :600000,
									params: {userweb:user_web,
											 realtorid:user_webid},
									
									failure:function(response,options){
										Ext.MessageBox.alert('Warning','file can not be generated');
										loading_win.hide();
									},
									success:function(response,options){
										var rest = Ext.util.JSON.decode(response.responseText);
										//alert(rest.pdf);
										var url='http://www.reifax.com/'+rest.pdf;
										//alert(url);
										loading_win.hide();
										window.open(url);
										
									}                                
								 }
							);
						 }
					},{
						 tooltip: 'Print Labels',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 hidden:icon_mylabel,
						 icon: 'http://www.reifax.com/img/toolbar/label.png',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties to Label  you must have the Platinum version.'); return false;}
							var simple = new Ext.FormPanel({
								labelWidth: 150, 
								url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>',
								frame:true,
								title: 'Property Labels',
								bodyStyle:'padding:5px 5px 0',
								width: 400,
								waitMsgTarget : 'Generated Labels...',
								
								items: [{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[5160,5160],
													[5161,5161],
													[5162,5162],
													[5197,5197],
													[5163,5163]
											]
										}),
										name: 'label_type',
										fieldLabel: 'Label Type',
										mode: 'local',
										value: 5160,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[8,8],
													[9,9],
													[10,10]
											]
										}),
										name: 'label_size',
										fieldLabel: 'Label Size',
										mode: 'local',
										value: 8,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[0,'Owner'],
													[1,'Property']
											]
										}),
										name: 'address_type',
										fieldLabel: 'Address',
										mode: 'local',
										value: 0,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['L','Left'],
													['C','Center']
											]
										}),
										displayField:'title',
										valueField: 'val',
										name: 'align_type',
										fieldLabel: 'Alingment',
										mode: 'local',
										value: 'L',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['N','No'],
													['Y','Yes']
											]
										}),
										name: 'resident_type',
										fieldLabel: 'Current Resident Or',
										mode: 'local',
										value: 'N',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'hidden',
										name: 'type',
										value: 'result'
									}
								],
						
								buttons: [{
									text: 'Apply',
									handler  : function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													//Ext.Msg.alert("Failure", action.result.pdf);
													var url='http://www.reifax.com/'+action.result.pdf;
													loading_win.hide();
													window.open(url);
													//window.open(url,'Print Labels',"fullscreen",'');
												},
												failure: function(form, action) {
													Ext.Msg.alert("Failure", action.result.msg);
													loading_win.hide();
												}
											});
										}
								},{
									text: 'Cancel',
									handler  : function(){
											simple.getForm().reset();
										}
								}]
							});
							win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: simple,
					
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							win.addListener("beforeshow",function(win){
								simple.getForm().reset();
							});
						 }
					},{
						tooltip: 'Excel Report',
						cls:'x-btn-text-icon',
						iconAlign: 'left',
						text: ' ',
						width: 30,
						height: 30,
						scale: 'medium',
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						hidden:icon_result,
						handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties to Excel  you must have the Platinum version.'); return false;}
							
							var ownerShow='false';
							Ext.Msg.show({
								title:'Excel Report',
								msg: 'Would you like to save Excel Report with Owner Data?',
								buttons: Ext.Msg.YESNO,
								fn: function(btn, text){
									if (btn == 'yes'){
										ownerShow='true';
									}
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Excel Report...',
										url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
										timeout: 106000,
										method: 'POST', 
										params: {
											userweb:user_web,
											realtorid:user_webid,
											ownerShow: ownerShow
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','file can not be generated');
										},
										success:function(response,options){
											var rest = Ext.util.JSON.decode(response.responseText);
											var url='http://www.reifax.com/'+rest.excel;
											loading_win.hide();
											//alert(url);
											location.href= url;
											//window.open(url);
										}                                
									});
								}
							}); 
							
							
						}
					},{
						 tooltip: 'Intelligent Market',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 hidden: <?php echo ($permission['platinum']==1 || $permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>, 
						 icon: 'http://www.reifax.com/img/toolbar/investment.png',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to view Intelligent Market you must have the Platinum version.'); return false;}

							//reset tab investment
							if(document.getElementById('resultGroupTab')){
								var tab = tabs.getItem('resultGroupTab');
								tabs.remove(tab); 
							}
							if(document.getElementById('resultFromGroupTab')){
								var tab = tabs.getItem('resultFromGroupTab');
								tabs.remove(tab); 
							}
							
							if(!num_rows_all){ Ext.MessageBox.alert('Warning','Please, narrow your search. The result page must have 25000 records or less to be able to execute the Intelligent Market.'); return false;}
						
							var simple = new Ext.FormPanel({
								labelWidth: 100, 
								url:'properties_coresearch.php',
								frame:true,
								title: 'Investment',
								bodyStyle:'padding:5px 5px 0',
								width: 400,
								waitMsgTarget : 'Generated ...',
								
								items: [{
									xtype: 'combo',
									editable: false,
									displayField:'title',
									valueField: 'val',
									store: new Ext.data.SimpleStore({
										fields: ['val', 'title'],
										data : [
											['ownername','OWNER NAME'],
											['owneraddress','OWNER ADDRESS'],
											['agentname','AGENT NAME'], 
											['lender','LENDER'],
											['plaintiff','PLAINTIFF']
										]
									}),
									name: 'resultbyname',
									fieldLabel: 'Result By',
									mode: 'local',
									value: 'ownername',
									hiddenName: 'resultby',
									hiddenValue:'ownername',
									triggerAction: 'all',
									selectOnFocus:true,
									allowBlank:false,
									listeners: {
										'select': function(combo,record,index){
											if(record.get('val')=='ownername' || record.get('val')=='owneraddress'){
												Ext.getCmp('groupbycombo').setValue('CO');
												Ext.getCmp('groupbycombo').setDisabled(false);
											}else{
												Ext.getCmp('groupbycombo').setValue('ALL');
												Ext.getCmp('groupbycombo').setDisabled(true);
											}
										}
									}
								},{
									xtype: 'combo',
									id: 'groupbycombo',
									editable: false,
									displayField:'title',
									valueField: 'val',
									store: new Ext.data.SimpleStore({
										fields: ['val', 'title'],
										data : [
												['CO','CORPORATION'],
												['TR','TRUST'],
												['FB','FINANCIAL BANK'],
												['IN','INDIVIDUAL'],
												['ALL','ALL']
										]
									}),
									name: 'groupbyname',
									fieldLabel: 'Group By',
									mode: 'local',
									value: 'CORPORATION',
									hiddenName: 'groupby',
									hiddenValue:'CO',
									triggerAction: 'all',
									selectOnFocus:true,
									allowBlank:false
								},{
									 xtype: 'textfield',
									 fieldLabel: 'Owns',
									 name: 'owns',
									 value:2,
									 allowBlank:false																		
								}],
						
								buttons: [{
									text: 'Apply',
									handler  : function(){							
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												tabs.add({
													title: ' Result Group',
													id: 'resultGroupTab',
													autoLoad: {url: 'result_tabs/properties_group_result.php', scripts: true, timeout: 10800, discardUrl:true, nocache:true, params:{systemsearch: '<?php echo $_POST['systemsearch'];?>'}},
													tbar: new Ext.Toolbar({
														id:'menu_result_advG',
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/toolbar/printer.png',
															handler: function(){
																if(!user_loged || user_web){ login_win.show(); return false;} 
																if(user_block) Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.');
																
																var parcelids_res='';
																if(!AllCheckRG){
																	var results = selected_dataRG;
																	if(results.length > 0){
																		parcelids_res=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res+=','+results[i];
																		}
																	}else{
																		Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
																	}
																}
													
																loading_win.show();
																Ext.Ajax.request({  
																	waitMsg: 'Printing Report...',
																	url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																	method: 'POST', 
																	timeout :600000,
																	params: {
																		userweb:user_web,
																		realtorid:user_webid,
																		parcelids_res:parcelids_res,
																		template_res:-1,
																		groupbylevel: 1
																	},
																	
																	failure:function(response,options){
																		Ext.MessageBox.alert('Warning','file can not be generated');
																		loading_win.hide();
																	},
																	success:function(response,options){
																		var rest = Ext.util.JSON.decode(response.responseText);
																		var url='http://www.reifax.com/'+rest.pdf;
																		loading_win.hide();
																		window.open(url);
																	
																	}                                
																});
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Print Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
													
														},{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/toolbar/excel.png',
															handler: function(){
																if(!user_loged || user_web){ login_win.show(); return false;} 
																if(user_block) Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.');
																
																var ownerShow='false';
																var parcelids_res='';
																if(!AllCheckRG){
																	var results = selected_dataRG;
																	if(results.length > 0){
																		parcelids_res=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res+=','+results[i];
																		}
																	}else{
																		Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
																	}
																}
													
																loading_win.show();
																Ext.Ajax.request({  
																	waitMsg: 'Excel Report...',
																	url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																	method: 'POST', 
																	timeout :600000,
																	params: {
																		userweb:user_web,
																		realtorid:user_webid,
																		ownerShow: ownerShow,
																		parcelids_res:parcelids_res,
																		template_res:-1,
																		groupbylevel: 1
																	},
												
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','file can not be generated');
																	},
																	
																	success:function(response,options){
																		var rest = Ext.util.JSON.decode(response.responseText);
																		var url='http://www.reifax.com/'+rest.excel;
																		loading_win.hide();
																		location.href= url;
																	}                                
																});
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Excel Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
														},'->',{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/cancel.png',
															handler: function(){
																var tab = tabs.getItem('resultGroupTab');
																tabs.remove(tab);
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Close Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
														}],
														autoShow: true
													}),
													closable: true
												}).show();
												
												win.close();
											}
										});
									}
								},{
									text: 'Cancel',
									handler  : function(){
										simple.getForm().reset();
									}
								}]
							});
							
							win = new Ext.Window({	
								layout      : 'fit',
								width       : 370,
								height      : 250,
								modal	 	: true,
								plain       : true,
								items		: simple,
								
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}	
								}]
							});
							
							win.show();
							win.addListener("beforeshow",function(win){simple.getForm().reset();});
						 }
					},{
						 tooltip: 'Follow Up',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/ximaicon/followup.jpg',
						 hidden: <?php echo ($permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Follow up...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowmail/properties_followmail_ids.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
								method: 'POST', 
								timeout :600000,
								params: {
									userweb:user_web,
									realtorid:user_webid
								},
								
								failure:function(response,options){
									Ext.MessageBox.alert('Warning','ERROR');
									loading_win.hide();
								},
								success:function(response,options){
									loading_win.hide();
									var rest = Ext.util.JSON.decode(response.responseText);
									if(rest.totalpropertys>100){
										Ext.Msg.alert("Follow Up", 'You can only follow 100 properties at a time');
										return false;
									}
									Ext.MessageBox.show({
										title:    'Follow Up',
										msg:      'Do you Want To add the properties to?',
										buttons: {yes: 'Buying', no: 'Selling',cancel: 'Cancel'},
										fn: function(btn) {
											var typeFollow='B';
											if(btn=='cancel'){
												return false;
											}else if(btn=='yes'){
												typeFollow='B';
											}else if(btn=='no'){
												typeFollow='S';
											}
											loading_win.show();
											Ext.Ajax.request({  
													waitMsg: 'Follow Up...',
													url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php', 
													method: 'POST', 
													timeout :600000,
													params: {
														'userid': <?php echo $_COOKIE['datos_usr']['USERID'];?>, 
														'followlist': true,
														typefollower: 1
													},
													
													failure:function(response,options){
														Ext.MessageBox.alert('Warning','ERROR');
														loading_win.hide();
													},
													success:function(response,options){
														loading_win.hide();
														var rest1 = Ext.util.JSON.decode(response.responseText);
														if(rest1.total==0){
															followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,rest.ids,typeFollow);
														}else{
															 Ext.MessageBox.show({
																   title:    'Follow Up',
																   msg:      'Do you Want To add the properties to?',
																   buttons: {yes: 'My account', no: 'Other account',cancel: 'Cancel'},
																   fn: function(btn) {
																		if( btn == 'yes') {
																			followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,rest.ids,typeFollow);
																		}else if(btn == 'no'){
																			var simple = new Ext.FormPanel({
																				frame: true,
																				title: 'Select user',
																				width: 350,
																				waitMsgTarget : 'Waiting...',
																				labelWidth: 75,
																				defaults: {width: 230},
																				labelAlign: 'left',
																				items: [{
																							xtype         : 'combo',
																							mode          : 'local',
																							triggerAction : 'all',
																							fieldLabel	  : 'User',
																							width		  : 130,
																							editable	  : false,
																							emptyText     : 'Select user',
																							store         : new Ext.data.JsonStore({
																								id        : 0,
																								fields    : ['userid', 'name'],
																								data      : rest1.records
																							}),
																							displayField  : 'name',
																							valueField    : 'userid',
																							name          : 'userfollow',
																							value         : rest1.records[0].userid,
																							hiddenName    : 'fuserfollow',
																							hiddenValue   : rest1.records[0].userid,
																							allowBlank    : false
																						}],
																				
																					buttons: [{
																						text: 'Accept',
																						handler: function(){
																							var values = simple.getForm().getValues();
																							if(values.userfollow!=''){
																								winStatus.close();
																								followUp(values.fuserfollow,rest.ids,typeFollow);
																							}
																						}
																					}]
																				});
																			 
																			var winStatus = new Ext.Window({
																				layout      : 'fit',
																				width       : 240,
																				height      : 180,
																				modal	 	: true,
																				plain       : true,
																				items		: simple,
																				closeAction : 'close',
																				buttons: [{
																					text     : 'Close',
																					handler  : function(){
																						winStatus.close();
																						loading_win.hide();
																					}
																				}]
																			});
																			winStatus.show();
																		}else{
																			return;
																		}
																  }
															 });
														}
														//Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
													}                                
											});
										}
									});
									
									function followUp(user, pids, type){
										loading_win.show();
										Ext.Ajax.request({  
												waitMsg: 'Follow Up...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
												method: 'POST', 
												timeout :600000,
												params: {
													type: 'multi-insert',
													pids: pids,
													county: rest.county,
													updatecontact: '',
													userid: user,
													typeFollow: type
												},
												
												failure:function(response,options){
													Ext.MessageBox.alert('Warning','ERROR');
													loading_win.hide();
												},
												success:function(response,options){
													loading_win.hide();
													Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
												}                                
										});
									}
									
								}                                
							 });
							
						 }
					},{
						 tooltip: 'Mailing Campaigns',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/ximaicon/mailcampaing.jpg',
						 hidden: <?php echo ($permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							var simple = new Ext.FormPanel({
													frame:true,
													title: 'Mailing Campaign',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													items: [{
																xtype     : 'textfield',
																name      : 'campaign',
																id: 'campaign',
																allowBlank: false,
																fieldLabel: 'Name Campaign',
																value     : '',
																width: 200
															}],
													buttons: [{
															text: 'Accept',
															handler: function(){
																if(simple.getForm().isValid()){
																	loading_win.show();
																	Ext.Ajax.request( 
																		{  
																			waitMsg: 'Mailing Campaings...',
																			url: 'mysetting_tabs/myfollowup_tabs/myfollowmail/properties_followmail_ids.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																			method: 'POST', 
																			timeout :600000,
																			params: {
																				userweb:user_web,
																				realtorid:user_webid
																			},
																			
																			failure:function(response,options){
																				Ext.MessageBox.alert('Warning','ERROR');
																				loading_win.hide();
																			},
																			success:function(response,options){
																				var rest = Ext.util.JSON.decode(response.responseText);
																				
																				if(rest.totalpropertys>500){
																					loading_win.hide();
																					Ext.Msg.alert("Mailing Campaigns", 'You can only add 500 properties at a time');
																					return false;
																				}
																				
																				Ext.Ajax.request( 
																				{  
																					waitMsg: 'Mailing Campaings...',
																					url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
																					method: 'POST', 
																					timeout :600000,
																					params: {
																						type:'verify',
																						pids: rest.ids,
																						campaign: Ext.getCmp('campaign').getValue()
																					},
																					
																					failure:function(response,options){
																						Ext.MessageBox.alert('Warning','ERROR');
																						loading_win.hide();
																					},
																					success:function(response,options){
																						var respuesta = Ext.util.JSON.decode(response.responseText);
																						if(respuesta.exist==2){
																							loading_win.hide();
																							Ext.MessageBox.alert('Warning','Campaign name already exists');
																						}else if(respuesta.exist==1){
																							loading_win.hide();
																							Ext.MessageBox.confirm('Mailing', 'Some properties already in another campaign, change?', saveCampaign);
																						}else{
																							saveCampaign(null);
																						}
																					}
																				});
																				function saveCampaign(btn){
																					if(btn=='no'){
																						win.close();
																						return;
																					}else{
																						loading_win.show();
																						Ext.Ajax.request({  
																								waitMsg: 'Mailing Campaings...',
																								url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
																								method: 'POST', 
																								timeout :600000,
																								params: {
																									type: 'multi-insert',
																									pid: rest.ids,
																									county: rest.county,
																									campaign: Ext.getCmp('campaign').getValue()
																								},
																								
																								failure:function(response,options){
																									Ext.MessageBox.alert('Warning','ERROR');
																									loading_win.hide();
																									win.close();
																								},
																								success:function(response,options){
																									loading_win.hide();
																									win.close();
																									Ext.Msg.alert("Mailing Campaings", 'Properties included in the Mailing Campaign.');
																								}                                
																							 }
																						);
																					}
																				}
																				
																			}                                
																		 }
																	);
																}
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	win.close();
																}
														}]
													});
													var win = new Ext.Window({
														layout      : 'fit',
														width       : 400,
														height      : 170,
														modal	 	: true,
														plain       : true,
														items		: simple,
														closeAction : 'hide',
														buttons: [{
															text     : 'Close',
															handler  : function(){
																win.close();
															}
														}]
													});
													win.show();
						 }
					},{
						 tooltip: 'View Help',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium', 
						icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
						handler: function(){
							//alert(user_loged+"||"+user_block+"||"+user_web);
							if(!user_loged || user_block || user_web){ login_win.show(); return false;}
							showVideoHelp('BasicResult');
							
						}
					},'->',{
						 tooltip: 'Close Result',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/cancel.png',
						 handler: function(){
							 var tab = tabs.getItem('resultTab');
							 tabs.remove(tab);
						 }
					  }
				],
				autoShow: true
			})
		}<?php if(isset($_COOKIE['datos_usr']['USERID']) && 
		$_COOKIE['datos_usr']['idstatus']!=8 && $_COOKIE['datos_usr']['idstatus']!=9){?>
		,{
			title: 'Advanced Result',
			id: 'AdvanceResult',
			autoLoad: {url: 'result_tabs/properties_advance_result.php', timeout: 10800, scripts: true, params: {systemsearch: '<?php echo $_POST['systemsearch'];?>',userweb: '<?php echo $_POST['userweb'];?>', realtorid: <?php if(strlen($_POST['realtorid'])>0) echo $_POST['realtorid']; else echo -1;?>}},
			tbar: new Ext.Toolbar({
				id:'menu_result_adv',
				cls: 'no-border',
				width: 'auto',
				items: [' ',
					{
						 id: 'toolbarMapResultAdv',
						 tooltip: 'Show/Hide Map',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/toolbar/map.png',
						 hidden:icon_result,
						 enableToggle: true,
						 handler: function(){								
							if(document.getElementById('mapResultAdv').style.display=='none'){
								document.getElementById('mapResultAdv').style.display='';
																	
								if(document.getElementById('result_control_mapa_divAdv'))
									mapResultAdv.map.getRootElement().removeChild(mapResult._mapTool);
								mapResultAdv.curBoton="AVG";
								mapResultAdv.ins_toolbar("320px","overview");
								
							}else{
								document.getElementById('mapResultAdv').style.display='none';
								if(document.getElementById('result_control_mapa_divAdv'))
									mapResultAdv.map.getRootElement().removeChild(mapResult._mapTool);
							}
						 }
					},{
						 tooltip: 'View Legend',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/toolbar/legend.png',
						 hidden:icon_result,
						 handler: function(){
							var dataLegend = [
								['S','http://www.reifax.com/img/houses/verdetotal.png','Subject'],
								['A-F','http://www.reifax.com/img/houses/verdel.png','Active Foreclosed'],
								['A-F-S','http://www.reifax.com/img/houses/verdel_s.png','Active Foreclosed Sold'],
								['A-P','http://www.reifax.com/img/houses/verdel.png','Active Pre-Foreclosed'],
								['A-P-S','http://www.reifax.com/img/houses/verdel_s.png','Active Pre-Foreclosed Sold'],
								['A-N','http://www.reifax.com/img/houses/verdeb.png','Active'],
								['CC-F','http://www.reifax.com/img/houses/cielol.png','By Owner Foreclosed'],
								['CC-F-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Foreclosed Sold'],
								['CC-P','http://www.reifax.com/img/houses/cielol.png','By Owner Pre-Foreclosed'],
								['CC-P-S','http://www.reifax.com/img/houses/cielol_s.png','By Owner Pre-Foreclosed Sold'],
								['CC-N','http://www.reifax.com/img/houses/cielo.png','By Owner'],
								['CS-F','http://www.reifax.com/img/houses/marronl.png','Closed Sale Foreclosed'],
								['CS-F-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Foreclosed Sold'],
								['CS-P','http://www.reifax.com/img/houses/marronl.png','Closed Sale Pre-Foreclosed'],
								['CS-P-S','http://www.reifax.com/img/houses/marronl_s.png','Closed Sale Pre-Foreclosed Sold'],
								['CS-N','http://www.reifax.com/img/houses/marronb.png','Closed Sale'],
								['N-F','http://www.reifax.com/img/houses/grisl.png','Non-Active Foreclosed'],
								['N-F-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Foreclosed Sold'],
								['N-P','http://www.reifax.com/img/houses/grisl.png','Non-Active Pre-Foreclosed'],
								['N-P-S','http://www.reifax.com/img/houses/grisl_s.png','Non-Active Pre-Foreclosed Sold'],
								['N-N','http://www.reifax.com/img/houses/grisb.png','Non-Active']
							];
							 
							 var store = new Ext.data.ArrayStore({
								idIndex: 0,
								fields: [
									'status', 'url', 'description'
								]
							 });
							
							store.loadData(dataLegend);
							
							 var listView = new Ext.list.ListView({
								store: store,
								multiSelect: false,
								emptyText: 'No Legend to display',
								columnResize: false,
								columnSort: false,
								columns: [{
									header: 'Color',
									width: .15,
									dataIndex: 'url',
									tpl: '<img src="{url}">'
								},{
									header: 'Status',
									width: .2,
									dataIndex: 'status'
								},{
									header: 'Description',
									dataIndex: 'description'
								}]
							});
							
							var win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: listView,
					
								buttons: [{
									text     : 'Print',
									handler  : function(){
										var htmlTag = new Array();
										var i=0;
										
										htmlTag.push('<table>'+
											'<tr>'+
												'<td>Color</td>'+
												'<td>Status</td>'+
												'<td>Description</td>'+
											'</tr>');
										
										while(i<dataLegend.length){
											htmlTag.push(
											'<tr>'+
												'<td><img src="'+dataLegend[i][1]+'" /></td>'+
												'<td>'+dataLegend[i][0]+'</td>'+
												'<td>'+dataLegend[i][2]+'</td>'+
											'</tr>');
											i++;
										}
										htmlTag.push('</table>');

										var WindowObject = window.open('', "TrackHistoryData", 
															  "width=420,height=225,top=250,left=345,toolbars=no,scrollbars=no,status=no,resizable=no");
										
										WindowObject.document.write(htmlTag);
										WindowObject.document.close();
										WindowObject.focus();
										WindowObject.print();
										WindowObject.close();
									}
								},{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
							 tooltip: 'XRay Report',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/exray.png',
							 handler: function(){
								 if(!user_loged){ login_win.show(); return false;}
								 else if(user_block || user_web){Ext.Msg.alert('Warning', 'You are either not logged in or are not allowed to view detailed information.'); return false;}
								 var latlong = document.getElementById('result_mapa_search_latlongAdv').value;
								 
								 if(latlong=='-1'){
									Ext.Msg.alert("XRay Report", 'Map shape/polygon is required to execute the XRay Report.');
									return false;
								}
								 
								 var simple = new Ext.FormPanel({
									url: 'reports_types/properties_xray.php',
									frame:true,
									title: 'Xray Report.',
									width: 400,
									
									items: [{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['texto', 'valor'],
											data : [
													['Single Family','01'],
													['Condo/Town/Villa','04'],
													['Multi Family +10','03'],
													['Multi Family -10','08'],
													['Commercial','11'],
													['Vacant Land','00'],
													['Mobile Home','02'],
													['Other','99']
											]
										}),
										displayField:'texto',
										valueField: 'valor',
										name: 'proptypeName',
										fieldLabel: 'Property Type',
										mode: 'local',
										value: '01',
										hiddenName: 'proptype',
										hiddenValue:'01',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									}],
									
									buttons: [{
											text: 'Continue',
											handler: function(){
												var values = simple.getForm().getValues();
												<?php if($_POST['systemsearch']=='advance'){?>
												var bd = document.getElementById('occounty').value;
												<?php }else{?>
												var bd = document.getElementById(search_type+'_county_search').value;
												<?php }?>
												var xcode = values.proptype;
												win.close();
			 
												if(document.getElementById('reportsTab')){
													var tab = tabs.getItem('reportsTab');
													tabs.remove(tab);
												}
												
												tabs.add({
													title: ' Reports ',
													id: 'reportsTab',
													autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, timeout: 10800, params: {latlong:latlong}},
													closable: true,
													tbar: new Ext.Toolbar({
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															 tooltip: 'Print XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/printer.png',
															 handler: function(){
																Ext.Ajax.request( 
																{  
																	waitMsg: 'Printing...',
																	url: 'imprimir/properties_xray_print.php', 
																	method: 'POST',
																	timeout :600000,
																	params: { 
																		db: bd,
																		proper: xcode,
																		type: 'P',
																		latlong: latlong
																	},
																	
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','ERROR');
																	},
																	success:function(response,options){
																		
																		var results=response.responseText;
																		if(Ext.isIE)
																			window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
																		 else
																			window.open(results,'_newtab');
																	}                                
																});
															 }
														},{
															 tooltip: 'Save XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/save.png',
															 handler: function(){
																 var simple = new Ext.FormPanel({
																	url: 'imprimir/properties_xray_print.php',
																	frame:true,
																	title: 'Saved Documents.',
																	width: 400,
																	waitMsgTarget : 'Saving Documents...',
																	
																	items: [{
																				xtype     : 'textfield',
																				name      : 'name_save',
																				fieldLabel: 'Name',
																				value     : '',
																				width: 200
																			},{
																				xtype     : 'hidden',
																				name      : 'db',
																				value     : bd
																			},{
																				xtype     : 'hidden',
													
																				name      : 'proper',
																				value     : xcode
																			},{
																				xtype     : 'hidden',
																				name      : 'type',
																				value     : 'S'
																			},{
																				xtype     : 'hidden',
																				name      : 'latlong',
																				value     : latlong
																			}],
																	
																	buttons: [{
																			text: 'Save',
																			handler: function(){
																				loading_win.show();
																				simple.getForm().submit({
																					success: function(form, action) {
																						loading_win.hide();
																						win.close();
																						Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																					},
																					failure: function(form, action) {
																						loading_win.hide();
																						Ext.Msg.alert("Failure", action.result.msg);
																					}
																				});
																			}
																		},{
																			text: 'Cancel',
																			handler  : function(){
																					simple.getForm().reset();
																					win.close();
																				}
																		}]
																	});
																 
																var win = new Ext.Window({
																	layout      : 'fit',
																	width       : 400,
																	height      : 170,
																	modal	 	: true,
																	plain       : true,
																	items		: simple,
																	closeAction : 'hide',
																	buttons: [{
																		text     : 'Close',
																		handler  : function(){
																			win.close();
																		}
																	}]
																});
																win.show();
															 }
														},'->',{
															 tooltip: 'Close XRay Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/cancel.png',
															 handler: function(){
																 var tab = tabs.getItem('reportsTab');
																 tabs.remove(tab);
															 }
														}]
													})
												}).show();
											}
										},{
											text: 'Cancel',
											handler  : function(){
													simple.getForm().reset();
													win.close();
												}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 170,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'hide',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
							 }
							},{
							 tooltip: 'Discount Report',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
							 handler: function(){
								 if(!user_loged){ login_win.show(); return false;}
								 else if(user_block || user_web){Ext.Msg.alert('Warning', 'You are either not logged in or are not allowed to view detailed information.'); return false;}
								 var latlong = document.getElementById('result_mapa_search_latlongAdv').value;
								 
								 if(latlong=='-1'){
									Ext.Msg.alert("Discount Report", 'Map shape/polygon is required to execute the Discount Report.');
									return false;
								}
								 
								 var simple = new Ext.FormPanel({
									url: 'reports_types/properties_xray.php',
									frame:true,
									title: 'Discount Report.',
									width: 400,
									
									items: [{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['texto', 'valor'],
											data : [
													['Single Family','01'],
													['Condo/Town/Villa','04'],
													['Multi Family +10','03'],
													['Multi Family -10','08'],
													['Commercial','11'],
													['Vacant Land','00'],
													['Mobile Home','02'],
													['Other','99']
											]
										}),
										displayField:'texto',
										valueField: 'valor',
										name: 'proptypeName',
										fieldLabel: 'Property Type',
										mode: 'local',
										value: '01',
										hiddenName: 'proptype',
										hiddenValue:'01',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									}],
									
									buttons: [{
											text: 'Continue',
											handler: function(){
								 				var values = simple.getForm().getValues();
												<?php if($_POST['systemsearch']=='advance'){?>
												var bd = document.getElementById('occounty').value;
												<?php }else{?>
												var bd = document.getElementById(search_type+'_county_search').value;
												<?php }?>
												var xcode = values.proptype;
												win.close();
															 
												 if(document.getElementById('reportsTab')){
													 var tab = tabs.getItem('reportsTab');
													 tabs.remove(tab);
												 }
												 
												tabs.add({
													title: ' Reports ',
													id: 'reportsTab',
													autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, timeout: 10800, params: {latlong:latlong}},
													closable: true,
													tbar: new Ext.Toolbar({
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															 tooltip: 'Print Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/printer.png',
															 handler: function(){   
																Ext.Ajax.request( 
																{  
																	waitMsg: 'Printing...',
																	url: 'imprimir/properties_rebate_print.php', 
																	method: 'POST',
																	timeout :600000,
																	params: { 
																		db: bd,
																		proper: xcode,
																		type: 'P',
																		latlong: latlong
																	},
																	
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','ERROR');
																	},
																	success:function(response,options){
																		
																		var results=response.responseText;
																		if(Ext.isIE)
																			window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
																		 else
																			window.open(results,'_newtab');
																	}                                
																});
															 }
														},{
															 tooltip: 'Save Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/toolbar/save.png',
															 handler: function(){
																 var simple = new Ext.FormPanel({
																	url: 'imprimir/properties_rebate_print.php',
																	frame:true,
																	title: 'Saved Documents.',
																	width: 400,
																	waitMsgTarget : 'Saving Documents...',
																	
																	items: [{
																				xtype     : 'textfield',
																				name      : 'name_save',
																				fieldLabel: 'Name',
																				value     : '',
																				width: 200
																			},{
																				xtype     : 'hidden',
																				name      : 'db',
																				value     : bd
																			},{
																				xtype     : 'hidden',
				
																				name      : 'proper',
																				value     : xcode
																			},{
																				xtype     : 'hidden',
																				name      : 'type',
																				value     : 'S'
																			},{
																				xtype     : 'hidden',
																				name      : 'latlong',
																				value     : latlong
																			}],
																	
																	buttons: [{
																			text: 'Save',
																			handler: function(){
																				loading_win.show();
																				simple.getForm().submit({
																					success: function(form, action) {
																						loading_win.hide();
																						win.close();
																						Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																					},
																					failure: function(form, action) {
																						loading_win.hide();
																						Ext.Msg.alert("Failure", action.result.msg);
																					}
																				});
																			}
																		},{
																			text: 'Cancel',
																			handler  : function(){
																					simple.getForm().reset();
																					win.close();
																				}
																		}]
																	});
																 
																var win = new Ext.Window({
																	layout      : 'fit',
																	width       : 400,
																	height      : 170,
																	modal	 	: true,
																	plain       : true,
																	items		: simple,
																	closeAction : 'hide',
																	buttons: [{
																		text     : 'Close',
																		handler  : function(){
																			win.close();
																		}
																	}]
																});
																win.show();
															 }
														},'->',{
															 tooltip: 'Close Discount Report',
															 cls:'x-btn-text-icon',
															 iconAlign: 'left',
															 text: ' ',
															 width: 30,
															 height: 30,
															 scale: 'medium',
															 icon: 'http://www.reifax.com/img/cancel.png',
															 handler: function(){
																 var tab = tabs.getItem('reportsTab');
																 tabs.remove(tab);
															 }
														}]
													})
												}).show();
											}
										},{
											text: 'Cancel',
											handler  : function(){
													simple.getForm().reset();
													win.close();
												}
										}]
									});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 170,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'hide',
									buttons: [{
										text     : 'Close',
										handler  : function(){
											win.close();
										}
									}]
								});
								win.show();
								 
							 }
							},{
						 tooltip: 'Print Report',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/toolbar/printer.png',
						 hidden:icon_result,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							
							var parcelids_res='';
							var parcelids_res_des='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
								}
							}else{
								var results = deselected_dataR;
								if(results.length > 0){
									parcelids_res_des=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res_des+=','+results[i];
									}
								}
							}
							
							loading_win.show();
							Ext.Ajax.request( 
								{  
									waitMsg: 'Printing Report...',
									url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
									method: 'POST', 
									timeout :600000,
									params: {
										userweb:user_web,
										realtorid:user_webid,
										parcelids_res:parcelids_res,
										parcelids_res_des:parcelids_res_des,
										template_res: Ext.getCmp('templateCombo').getValue()
									},
									
									failure:function(response,options){
										Ext.MessageBox.alert('Warning','file can not be generated');
										loading_win.hide();
									},
									success:function(response,options){
										var rest = Ext.util.JSON.decode(response.responseText);
										//alert(rest.pdf);
										var url='http://www.reifax.com/'+rest.pdf;
										//alert(url);
										loading_win.hide();
										window.open(url);
										
									}                                
								 }
							);
						 }
					},{
						 tooltip: 'Print Labels',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 hidden:icon_mylabel,
						 icon: 'http://www.reifax.com/img/toolbar/label.png',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties to Label you must have the Platinum version.'); return false;}
							
							var parcelids_res='';
							var parcelids_res_des='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Print Labels','You must check-select the records to be labeled.'); return false;
								}
							}else{
								var results = deselected_dataR;
								if(results.length > 0){
									parcelids_res_des=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res_des+=','+results[i];
									}
								}
							}
							
							var simple = new Ext.FormPanel({
								labelWidth: 150, 
								url:'toolbars_types/properties_label.php?systemsearch=<?php echo $_POST['systemsearch'];?>',
								frame:true,
								title: 'Property Labels',
								bodyStyle:'padding:5px 5px 0',
								width: 400,
								waitMsgTarget : 'Generated Labels...',
								
								items: [{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[5160,5160],
													[5161,5161],
													[5162,5162],
													[5197,5197],
													[5163,5163]
											]
										}),
										name: 'label_type',
										fieldLabel: 'Label Type',
										mode: 'local',
										value: 5160,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[8,8],
													[9,9],
													[10,10]
											]
										}),
										name: 'label_size',
										fieldLabel: 'Label Size',
										mode: 'local',
										value: 8,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													[0,'Owner'],
													[1,'Property']
											]
										}),
										name: 'address_type',
										fieldLabel: 'Address',
										mode: 'local',
										value: 0,
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['L','Left'],
													['C','Center']
											]
										}),
										displayField:'title',
										valueField: 'val',
										name: 'align_type',
										fieldLabel: 'Alingment',
										mode: 'local',
										value: 'L',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'combo',
										editable: false,
										displayField:'title',
										valueField: 'val',
										store: new Ext.data.SimpleStore({
											fields: ['val', 'title'],
											data : [
													['N','No'],
													['Y','Yes']
											]
										}),
										name: 'resident_type',
										fieldLabel: 'Current Resident Or',
										mode: 'local',
										value: 'N',
										triggerAction: 'all',
										selectOnFocus:true,
										allowBlank:false
									},{
										xtype: 'hidden',
										name: 'parcelids_res',
										value: parcelids_res
									},{
										xtype: 'hidden',
										name: 'parcelids_res_des',
										value: parcelids_res_des
									},{
										xtype: 'hidden',
										name: 'type',
										value: 'result'
									}
								],
						
								buttons: [{
									text: 'Apply',
									handler  : function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													//Ext.Msg.alert("Failure", action.result.pdf);
													var url='http://www.reifax.com/'+action.result.pdf;
													loading_win.hide();
													window.open(url);
													//window.open(url,'Print Labels',"fullscreen",'');
												},
												failure: function(form, action) {
													Ext.Msg.alert("Failure", action.result.msg);
													loading_win.hide();
												}
											});
										}
								},{
									text: 'Cancel',
									handler  : function(){
											simple.getForm().reset();
										}
								}]
							});
							win = new Ext.Window({
								
								layout      : 'fit',
								width       : 370,
								height      : 300,
								modal	 	: true,
								plain       : true,
								items		: simple, 
					
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
							win.addListener("beforeshow",function(win){
								simple.getForm().reset();
							});
						 }
					},{
						tooltip: 'Excel Report',
						cls:'x-btn-text-icon',
						iconAlign: 'left',
						text: ' ',
						width: 30,
						height: 30,
						scale: 'medium',
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						hidden:icon_result,
						handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties to Excel you must have the Platinum version.'); return false;} 
							
							var ownerShow='false';
							var parcelids_res='';
							var parcelids_res_des='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
								}
							}else{
								var results = deselected_dataR;
								if(results.length > 0){
									parcelids_res_des=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res_des+=','+results[i];
									}
								}
							}
							
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',
								url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
								method: 'POST', 
								timeout :600000,
								params: {
									userweb:user_web,
									realtorid:user_webid,
									ownerShow: ownerShow,
									parcelids_res:parcelids_res,
									parcelids_res_des:parcelids_res_des,
									template_res: Ext.getCmp('templateCombo').getValue()
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
							
							
						}
						
					},{
						 tooltip: 'Intelligent Market',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 hidden: <?php echo ($permission['platinum']==1 || $permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>, 
						 icon: 'http://www.reifax.com/img/toolbar/investment.png',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to view Intelligent Market you must have the Platinum version.'); return false;}

							//reset tab investment
							if(document.getElementById('resultGroupTab')){
								var tab = tabs.getItem('resultGroupTab');
								tabs.remove(tab); 
							}
							if(document.getElementById('resultFromGroupTab')){
								var tab = tabs.getItem('resultFromGroupTab');
								tabs.remove(tab); 
							}
							
							if(!num_rows_all){ Ext.MessageBox.alert('Warning','Please, narrow your search. The result page must have 25000 records or less to be able to execute the Intelligent Market.'); return false;}
							
							Ext.get(gridR.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
							gridR.getSelectionModel().selectAll();
						
							var simple = new Ext.FormPanel({
								labelWidth: 100, 
								url:'properties_coresearch.php',
								frame:true,
								title: 'Investment',
								bodyStyle:'padding:5px 5px 0',
								width: 400,
								waitMsgTarget : 'Generated ...',
								
								items: [{
									xtype: 'combo',
									editable: false,
									displayField:'title',
									valueField: 'val',
									store: new Ext.data.SimpleStore({
										fields: ['val', 'title'],
										data : [
											['ownername','OWNER NAME'],
											['owneraddress','OWNER ADDRESS'],
											['agentname','AGENT NAME'], 
											['lender','LENDER'],
											['plaintiff','PLAINTIFF']
										]
									}),
									name: 'resultbyname',
									fieldLabel: 'Result By',
									mode: 'local',
									value: 'ownername',
									hiddenName: 'resultby',
									hiddenValue:'ownername',
									triggerAction: 'all',
									selectOnFocus:true,
									allowBlank:false,
									listeners: {
										'select': function(combo,record,index){
											if(record.get('val')=='ownername' || record.get('val')=='owneraddress'){
												Ext.getCmp('groupbycombo').setValue('CO');
												Ext.getCmp('groupbycombo').setDisabled(false);
											}else{
												Ext.getCmp('groupbycombo').setValue('ALL');
												Ext.getCmp('groupbycombo').setDisabled(true);
											}
										}
									}
								},{
									xtype: 'combo',
									id: 'groupbycombo',
									editable: false,
									displayField:'title',
									valueField: 'val',
									store: new Ext.data.SimpleStore({
										fields: ['val', 'title'],
										data : [
												['CO','CORPORATION'],
												['TR','TRUST'],
												['FB','FINANCIAL BANK'],
												['IN','INDIVIDUAL'],
												['ALL','ALL']
										]
									}),
									name: 'groupbyname',
									fieldLabel: 'Group By',
									mode: 'local',
									value: 'CORPORATION',
									hiddenName: 'groupby',
									hiddenValue:'CO',
									triggerAction: 'all',
									selectOnFocus:true,
									allowBlank:false
								},{
									 xtype: 'textfield',
									 fieldLabel: 'Owns',
									 name: 'owns',
									 value:2,
									 allowBlank:false																		
								}],
						
								buttons: [{
									text: 'Apply',
									handler  : function(){							
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												tabs.add({
													title: ' Result Group',
													id: 'resultGroupTab',
													autoLoad: {url: 'result_tabs/properties_group_result.php', scripts: true, timeout: 10800, discardUrl:true, nocache:true, params:{systemsearch: '<?php echo $_POST['systemsearch'];?>'}},
													tbar: new Ext.Toolbar({
														id:'menu_result_advG',
														cls: 'no-border',
														width: 'auto',
														items: [' ',{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/toolbar/printer.png',
															handler: function(){
																if(!user_loged || user_web){ login_win.show(); return false;} 
																if(user_block) Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.');
																
																var parcelids_res='';
																var parcelids_res_des='';
																if(!AllCheckRG){
																	var results = selected_dataRG;
																	if(results.length > 0){
																		parcelids_res=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res+=','+results[i];
																		}
																	}else{
																		Ext.MessageBox.alert('Print Report','You must check-select the records to be printed.'); return false;
																	}
																}else{
																	var results = deselected_dataRG;
																	if(results.length > 0){
																		parcelids_res_des=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res_des+=','+results[i];
																		}
																	}
																}
													
																loading_win.show();
																Ext.Ajax.request({  
																	waitMsg: 'Printing Report...',
																	url: 'toolbars_types/properties_pdf.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																	method: 'POST', 
																	timeout :600000,
																	params: {
																		userweb:user_web,
																		realtorid:user_webid,
																		parcelids_res:parcelids_res,
																		parcelids_res_des:parcelids_res_des,
																		template_res:-1,
																		groupbylevel: 1
																	},
																	
																	failure:function(response,options){
																		Ext.MessageBox.alert('Warning','file can not be generated');
																		loading_win.hide();
																	},
																	success:function(response,options){
																		var rest = Ext.util.JSON.decode(response.responseText);
																		var url='http://www.reifax.com/'+rest.pdf;
																		loading_win.hide();
																		window.open(url);
																	
																	}                                
																});
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Print Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
													
														},{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/toolbar/excel.png',
															handler: function(){
																if(!user_loged || user_web){ login_win.show(); return false;} 
																if(user_block) Ext.MessageBox.alert('Warning','To be able to export any type of properties to Excel you must have the Platinum version.');
																
																var ownerShow='false';
																var parcelids_res='';
																var parcelids_res_des='';
																if(!AllCheckRG){
																	var results = selected_dataRG;
																	if(results.length > 0){
																		parcelids_res=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res+=','+results[i];
																		}
																	}else{
																		Ext.MessageBox.alert('Excel Report','You must check-select the records to be exported.'); return false;
																	}
																}else{
																	var results = deselected_dataRG;
																	if(results.length > 0){
																		parcelids_res_des=results[0];
																		for(i=1; i<results.length; i++){
																			parcelids_res_des+=','+results[i];
																		}
																	}
																}
													
																loading_win.show();
																Ext.Ajax.request({  
																	waitMsg: 'Excel Report...',
																	url: 'toolbars_types/properties_excel.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																	method: 'POST', 
																	timeout :600000,
																	params: {
																		userweb:user_web,
																		realtorid:user_webid,
																		ownerShow: ownerShow,
																		parcelids_res:parcelids_res,
																		parcelids_res_des:parcelids_res_des,
																		template_res:-1,
																		groupbylevel: 1
																	},
												
																	failure:function(response,options){
																		loading_win.hide();
																		Ext.MessageBox.alert('Warning','file can not be generated');
																	},
																	
																	success:function(response,options){
																		var rest = Ext.util.JSON.decode(response.responseText);
																		var url='http://www.reifax.com/'+rest.excel;
																		loading_win.hide();
																		location.href= url;
																	}                                
																});
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Excel Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
														},'->',{
															cls:'x-btn-text-icon',
															iconAlign: 'left',
															text: ' ',
															width: 30,
															height: 30,
															scale: 'medium',
															icon: 'http://www.reifax.com/img/cancel.png',
															handler: function(){
																var tab = tabs.getItem('resultGroupTab');
																tabs.remove(tab);
															},listeners: {
																mouseover: function(but,e){
																	but.setText('Close Report');
																	but.removeClass('x-btn-text-icon');
																	but.addClass('x-btn-text-icon');
																},
																mouseout: function(but,e){
																	but.setText(' ');
																}
															}
														}],
														autoShow: true
													}),
													closable: true
												}).show();
												
												win.close();
											}
										});
									}
								},{
									text: 'Cancel',
									handler  : function(){
										simple.getForm().reset();
									}
								}]
							});
							
							win = new Ext.Window({	
								layout      : 'fit',
								width       : 370,
								height      : 250,
								modal	 	: true,
								plain       : true,
								items		: simple,
								
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}	
								}]
							});
							
							win.show();
							win.addListener("beforeshow",function(win){simple.getForm().reset();});
						 }
					},{
						 tooltip: 'Follow Up',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/ximaicon/followup.jpg',
						 hidden: <?php echo ($permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>,
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							
							var parcelids_res='';
							var parcelids_res_des='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert("Follow Up",'You must check-select the records to be included.'); return false;
								}
							}else{
								var results = deselected_dataR;
								if(results.length > 0){
									parcelids_res_des=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res_des+=','+results[i];
									}
								}
							}
							
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Follow up...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowmail/properties_followmail_ids.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
								method: 'POST', 
								timeout :600000,
								params: {
									userweb:user_web,
									realtorid:user_webid,
									parcelids_res:parcelids_res,
									parcelids_res_des:parcelids_res_des,
									template_res: Ext.getCmp('templateCombo').getValue()
								},
								
								failure:function(response,options){
									Ext.MessageBox.alert('Warning','ERROR');
									loading_win.hide();
								},
								success:function(response,options){
									loading_win.hide();
									var rest = Ext.util.JSON.decode(response.responseText);
									if(rest.totalpropertys>100){
										Ext.Msg.alert("Follow Up", 'You can only follow 100 properties at a time');
										return false;
									}
									Ext.MessageBox.show({
										title:    'Follow Up',
										msg:      'Do you Want To add the properties to?',
										buttons: {yes: 'Buying', no: 'Selling',cancel: 'Cancel'},
										fn: function(btn) {
											var typeFollow='B';
											if(btn=='cancel'){
												return false;
											}else if(btn=='yes'){
												typeFollow='B';
											}else if(btn=='no'){
												typeFollow='S';
											}
											loading_win.show();
											Ext.Ajax.request({  
													waitMsg: 'Follow Up...',
													url: 'mysetting_tabs/myfollowup_tabs/myfollowlist/properties_followlist.php', 
													method: 'POST', 
													timeout :600000,
													params: {
														'userid': <?php echo $_COOKIE['datos_usr']['USERID'];?>, 
														'followlist': true,
														typefollower: 1
													},
													
													failure:function(response,options){
														Ext.MessageBox.alert('Warning','ERROR');
														loading_win.hide();
													},
													success:function(response,options){
														loading_win.hide();
														var rest1 = Ext.util.JSON.decode(response.responseText);
														if(rest1.total==0){
															followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,rest.ids,typeFollow);
														}else{
															 Ext.MessageBox.show({
																   title:    'Follow Up',
																   msg:      'Do you Want To add the properties to?',
																   buttons: {yes: 'My account', no: 'Other account',cancel: 'Cancel'},
																   fn: function(btn) {
																		if( btn == 'yes') {
																			followUp(<?php echo $_COOKIE['datos_usr']['USERID'];?>,rest.ids,typeFollow);
																		}else if(btn == 'no'){
																			var simple = new Ext.FormPanel({
																				frame: true,
																				title: 'Select user',
																				width: 350,
																				waitMsgTarget : 'Waiting...',
																				labelWidth: 75,
																				defaults: {width: 230},
																				labelAlign: 'left',
																				items: [{
																							xtype         : 'combo',
																							mode          : 'local',
																							triggerAction : 'all',
																							fieldLabel	  : 'User',
																							width		  : 130,
																							editable	  : false,
																							emptyText     : 'Select user',
																							store         : new Ext.data.JsonStore({
																								id        : 0,
																								fields    : ['userid', 'name'],
																								data      : rest1.records
																							}),
																							displayField  : 'name',
																							valueField    : 'userid',
																							name          : 'userfollow',
																							value         : rest1.records[0].userid,
																							hiddenName    : 'fuserfollow',
																							hiddenValue   : rest1.records[0].userid,
																							allowBlank    : false
																						}],
																				
																					buttons: [{
																						text: 'Accept',
																						handler: function(){
																							var values = simple.getForm().getValues();
																							if(values.userfollow!=''){
																								winStatus.close();
																								followUp(values.fuserfollow,rest.ids,typeFollow);
																							}
																						}
																					}]
																				});
																			 
																			var winStatus = new Ext.Window({
																				layout      : 'fit',
																				width       : 240,
																				height      : 180,
																				modal	 	: true,
																				plain       : true,
																				items		: simple,
																				closeAction : 'close',
																				buttons: [{
																					text     : 'Close',
																					handler  : function(){
																						winStatus.close();
																						loading_win.hide();
																					}
																				}]
																			});
																			winStatus.show();
																		}else{
																			return;
																		}
																  }
															 });
														}
														//Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
													}                                
											});	
										}
									});
									
									function followUp(user,pids,type){	
										loading_win.show(); 
										Ext.Ajax.request({  
												waitMsg: 'Follow Up...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
												method: 'POST', 
												timeout :600000,
												params: {
													type: 'multi-insert',
													pids: pids,
													county: rest.county,
													updatecontact: '',
													userid: user,
													typeFollow: type 
												},
												
												failure:function(response,options){
													Ext.MessageBox.alert('Warning','ERROR');
													loading_win.hide();
												},
												success:function(response,options){
													loading_win.hide();
													Ext.Msg.alert("Follow Up", 'Follow Up Properties successfully completed.');
												}                                
										});
									}
								}                                
							 });
							
						 }
					},{
						 tooltip: 'Mailing Campaign',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/ximaicon/mailcampaing.jpg',
						 hidden: <?php echo ($permission['professional']==1 || $permission['professional_esp']==1) ? 'false' : 'true';?>,
						 id: 'mailing_campaings_result_advance',
						 handler: function(){
							if(!user_loged || user_web){ login_win.show(); return false;} 
							if(user_block){ Ext.MessageBox.alert('Warning','To be able to export any type of properties you must have the Platinum version.'); return false;}
							
							var parcelids_res='';
							var parcelids_res_des='';
							if(!AllCheckR){
								var results = selected_dataR;
								if(results.length > 0){
									parcelids_res=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res+=','+results[i];
									}
								}else{
									Ext.MessageBox.alert("Mailing Campaings",'You must check-select the records to be included.'); return false;
								}
							}else{
								var results = deselected_dataR;
								if(results.length > 0){
									parcelids_res_des=results[0];
									for(i=1; i<results.length; i++){
										parcelids_res_des+=','+results[i];
									}
								}
							}
							
							var simple = new Ext.FormPanel({
													frame:true,
													title: 'Mailing Campaign',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													items: [{
																xtype     : 'textfield',
																name      : 'campaignadv',
																id: 'campaignadv',
																allowBlank: false,
																fieldLabel: 'Name Campaign',
																value     : '',
																width: 200
															}],
													buttons: [{
															text: 'Accept',
															handler: function(){
																if(simple.getForm().isValid()){
																	loading_win.show();
																	Ext.Ajax.request( 
																		{  
																			waitMsg: 'Mailing Campaings...',
																			url: 'mysetting_tabs/myfollowup_tabs/myfollowmail/properties_followmail_ids.php?systemsearch=<?php echo $_POST['systemsearch'];?>', 
																			method: 'POST', 
																			timeout :600000,
																			params: {
																				userweb:user_web,
																				realtorid:user_webid,
																				parcelids_res:parcelids_res,
																				parcelids_res_des:parcelids_res_des,
																				template_res: Ext.getCmp('templateCombo').getValue()
																			},
																			
																			failure:function(response,options){
																				Ext.MessageBox.alert('Warning','ERROR');
																				loading_win.hide();
																			},
																			success:function(response,options){
																				var rest = Ext.util.JSON.decode(response.responseText);
																				
																				if(rest.totalpropertys>500){
																					loading_win.hide();
																					Ext.Msg.alert("Mailing Campaigns", 'You can only add 500 properties at a time');
																					return false;
																				} 
																				
																				Ext.Ajax.request( 
																				{  
																					waitMsg: 'Mailing Campaings...',
																					url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
																					method: 'POST', 
																					timeout :600000,
																					params: {
																						type:'verify',
																						pids: rest.ids,
																						campaign: Ext.getCmp('campaignadv').getValue()
																					},
																					
																					failure:function(response,options){
																						Ext.MessageBox.alert('Warning','ERROR');
																						loading_win.hide();
																					},
																					success:function(response,options){
																						var respuesta = Ext.util.JSON.decode(response.responseText);
																						if(respuesta.exist==2){
																							loading_win.hide();
																							Ext.MessageBox.alert('Warning','Campaign name already exists');
																						}else if(respuesta.exist==1){
																							loading_win.hide();
																							Ext.MessageBox.confirm('Mailing', 'Some properties already in another campaign, change?', saveCampaign);
																						}else{
																							saveCampaign(null);
																						}
																					}
																				});
																				function saveCampaign(btn){
																					if(btn=='no'){
																						win.close();
																						return;
																					}else{
																						loading_win.show();
																						Ext.Ajax.request({  
																								waitMsg: 'Mailing Campaings...',
																								url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
																								method: 'POST', 
																								timeout :600000,
																								params: {
																									type: 'multi-insert',
																									pid: rest.ids,
																									county: rest.county,
																									campaign: Ext.getCmp('campaignadv').getValue()
																								},
																								
																								failure:function(response,options){
																									Ext.MessageBox.alert('Warning','ERROR');
																									loading_win.hide();
																									win.close();
																								},
																								success:function(response,options){
																									loading_win.hide();
																									win.close();
																									Ext.Msg.alert("Mailing Campaings", 'Properties included in the Mailing Campaign.');
																								}                                
																							 }
																						);
																					}
																				}
																				
																			}                                
																		 }
																	);
																}
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	win.close();
																}
														}]
													});
													var win = new Ext.Window({
														layout      : 'fit',
														width       : 400,
														height      : 170,
														modal	 	: true,
														plain       : true,
														items		: simple,
														closeAction : 'hide',
														buttons: [{
															text     : 'Close',
															handler  : function(){
																win.close();
															}
														}]
													});
													win.show();
						 }
					},{
						 tooltip: 'Manage Template',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/toolbar/template.png',
						 hidden:icon_result,
						 handler: function(){
							ShowManageTemplate();
						 }
					},new Ext.form.ComboBox({
						id: 'templateCombo',
						fieldLabel: '',
						triggerAction: 'all',
						mode: 'remote',
						forceSelection: true,
						store: new Ext.data.JsonStore({
							url: 'properties_manage_template.php',
							id: 0,
							fields: [
								'tID',
								'tname'
							]
						}),
						value: 'Default',
						width: 130,
						valueField: 'tID',
						displayField: 'tname',
						listeners:{
							'select': function (combo,record,index){
								ResultTemplate=record.data.tID;
								AllCheckR=false;
								selected_dataR=new Array();
								gridR.getSelectionModel().clearSelections();
								storeR.load({'params': {'ResultTemplate': ResultTemplate}});
							}
						}
					}),{
						tooltip: 'View Help',
						cls:'x-btn-text-icon',
						iconAlign: 'left',
						text: ' ',
						width: 30,
						height: 30,
						scale: 'medium',
						icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
						handler: function(){
							//alert(user_loged+"||"+user_block+"||"+user_web);
							if(!user_loged || user_block || user_web){ login_win.show(); return false;}
							showVideoHelp('AdvancedResult');
						}
					},'->',{
						 tooltip: 'Close Result',
						 cls:'x-btn-text-icon',
						 iconAlign: 'left',
						 text: ' ',
						 width: 30,
						 height: 30,
						 scale: 'medium',
						 icon: 'http://www.reifax.com/img/cancel.png',
						 handler: function(){
							 var tab = tabs.getItem('resultTab');
							 tabs.remove(tab);
						 }
					  }
				],
				autoShow: true
			}) 
		}
		<?php }?>
	]
});
</script>

