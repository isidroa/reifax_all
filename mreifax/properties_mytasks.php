<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	
	///Actualizacion de lprice y status
	$query="SELECT distinct f.bd FROM `xima`.`followup` f WHERE f.userid=$userid order by f.bd";
	$result=mysql_query($query) or die($query.mysql_error());
	
	while($r=mysql_fetch_array($result)){
		conectarPorNameCounty($r['bd']);
		$queryM="update `xima`.`followup` f 
			SET f.lprice=IF((select count(*) FROM mlsresidential WHERE parcelid=f.parcelid)=0,f.lprice,(select lprice FROM mlsresidential WHERE parcelid=f.parcelid)),
			f.status=IF((Select count(*) FROM psummary m WHERE m.parcelid=f.parcelid)=0,f.status,IF((Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid) is null,'NF',(Select IF(m.status='A','A','NA') FROM mlsresidential m WHERE m.parcelid=f.parcelid)))
			WHERE f.userid=$userid";
		mysql_query($queryM);
	}
	//echo 'aqui'; return;
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mytasks_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="mytasks_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="mytasks_filters"></div><br />
        <div id="mytasks_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	Right Click -> Overview / Contract 
            <span style="margin-left:15px; margin-right:15px;">
                  -     
            </span>
            Double Click -> Follow History
       	</div>
	</div>
</div>
<script>

	var limitmyfollowup 			= 50;
	var selected_datamyfollowup 	= new Array();
	var AllCheckmyfollowup 			= false;
	
	//filter variables
	var filteraddressmyfollowup 	= '';
	var filtermlnumbermyfollowup 	= '';
	var filteragentmyfollowup 		= '';
	var filterstatusmyfollowup 		= 'ALL';
	var filterndatemyfollowup 		= '';
	var filterndatebmyfollowup 		= '';
	var filterntaskmyfollowup 		= '-1';
	var filtercontractmyfollowup 	= '-1';
	var filterpofmyfollowup 		= '-1';
	var filteremdmyfollowup 		= '-1';
	var filterademdumsmyfollowup 	= '-1';
	var filtermsjmyfollowup 		= '-1';
	var filterhistorymyfollowup 	= '-1';
	var filterfield					= 'address';
	var filterdirection				= 'ASC';
	var filterofferreceivedmyfollowup = '-1';
	
	var message_win=new Ext.Window({
								 width:170,
								 autoHeight: true,
								 resizable: false,
								 modal: true,
								 border:false,
								 closable:false,
								 plain: true,
								 buttonAlign: 'center', 
								 html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;">Please do not use any symbols just plain letters and numbers. Thanks</div>',
								 buttons: [{
									text     : 'Close',
									handler  : function(){
										message_win.hide();
									}
								}]
							});
	
	var storemyfollowtask = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'float'},
			   {name: 'coffer', type: 'float'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'float'},
			   'statusalt'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowup;
				obj.params.mlnumber=filtermlnumbermyfollowup;
				obj.params.agent=filteragentmyfollowup;
				obj.params.status=filterstatusmyfollowup;
				obj.params.ndate=filterndatemyfollowup;
				obj.params.ndateb=filterndatebmyfollowup;
				obj.params.ntask=filterntaskmyfollowup;
				obj.params.contract=filtercontractmyfollowup;
				obj.params.pof=filterpofmyfollowup;
				obj.params.emd=filteremdmyfollowup;
				obj.params.ademdums=filterademdumsmyfollowup;
				obj.params.msj=filtermsjmyfollowup;
				obj.params.history=filterhistorymyfollowup;
				obj.params.offerreceived=filterofferreceivedmyfollowup;
			},
			'load' : function (store,data,obj){
				if (AllCheckmyfollowup){
					Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowup=true;
					gridmyfollowtask.getSelectionModel().selectAll();
					selected_datamyfollowup=new Array();
				}else{
					AllCheckmyfollowup=false;
					Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowup.length > 0){
						for(val in selected_datamyfollowup){
							var ind = gridmyfollowtask.getStore().find('pid',selected_datamyfollowup[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowtask.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });

	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/drop-no.gif" />'; 
		else return '<img src="../../img/drop-yes.gif" />';
	}
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/notes/new_msj.png" />';
		else return '';
	}
	function creaVentana(dato,r,pid){
							var ocultar=false;
							var princ='';
							var title='';
							if(r.records[dato]==null){
								r.records[dato]=new Array();
								ocultar=true;
								title='New Contact';
								r.records[dato].parcelid=pid;
							}else{
								title='Contact '+(dato+1)+' of '+r.total;
							}
							if(r.records[dato].principal=='1'){
								princ='(Default)';
								title='Contact '+(dato+1)+' of '+r.total+' '+princ;
							}
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
								frame: true,
								title: title,
								width: 400,
								waitMsgTarget : 'Waiting...',
								labelWidth: 75,
								defaults: {width: 230},
								labelAlign: 'left',
								items: [{
											xtype     : 'combo',
											//hidden: !ocultar,
											fieldLabel: 'Contact',
											triggerAction: 'all',
											//forceSelection: true, 
											selectOnFocus: true,
											typeAhead: true,
											typeAheadDelay: 10,
											minChars: 0,
											mode: 'remote',
											store: new Ext.data.JsonStore({
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
												forceSelection: true,
												root: 'records',
												baseParams		: {
													'userid'	:  <?php echo $userid;?>,
													'sort'		: 'agent',
													'dir'		: 'ASC'
												},
												id: 0,
												fields: [
													'agentid',
													'agent'
												]
											}),
											valueField: 'agentid',
											displayField: 'agent',
											name          : 'fagent',
											value         : r.records[dato].agent, //agent.get('agentype'),
											hiddenName    : 'agent',
											hiddenValue   : r.records[dato].agentid, //agent.get('agenttype'),
											listeners	  : {
												select: function(combo,record,index){
													var agentid=record.get('agentid');
													//return;
													Ext.Ajax.request({
														waitMsg: 'Seeking...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															agentid: agentid
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															loading_win.hide();
															var r1=Ext.decode(response.responseText);
															if(r1.total>0){
																var value=r1.records[0];
																//simple.getForm().findField('agent').setValue(value.agent);
																simple.getForm().findField('agenttype').setValue(value.agenttype);
																simple.getForm().findField('company').setValue(value.company);
																simple.getForm().findField('typeemail1').setValue(value.typeemail1);
																simple.getForm().findField('typeemail2').setValue(value.typeemail2);
																simple.getForm().findField('email').setValue(value.email);
																simple.getForm().findField('email2').setValue(value.email2);
																simple.getForm().findField('typeurl1').setValue(value.typeurl1);
																simple.getForm().findField('urlsend').setValue(value.urlsend);
																simple.getForm().findField('typeurl2').setValue(value.typeurl2);
																simple.getForm().findField('urlsend2').setValue(value.urlsend2);
																simple.getForm().findField('typeph1').setValue(value.typeph1);
																simple.getForm().findField('phone1').setValue(value.phone1);
																simple.getForm().findField('typeph2').setValue(value.typeph2);
																simple.getForm().findField('phone2').setValue(value.phone2);
																simple.getForm().findField('typeph3').setValue(value.typeph3);
																simple.getForm().findField('phone3').setValue(value.phone3);
																simple.getForm().findField('typeph4').setValue(value.typeph4);
																simple.getForm().findField('fax').setValue(value.fax);
																simple.getForm().findField('typeph5').setValue(value.typeph5);
																simple.getForm().findField('tollfree').setValue(value.tollfree);
																simple.getForm().findField('typeph6').setValue(value.typeph6);
																simple.getForm().findField('phone6').setValue(value.phone6);
																simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
																simple.getForm().findField('address1').setValue(value.address1);
																simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
																simple.getForm().findField('address2').setValue(value.address2);
																simple.getForm().findField('type').setValue('insert');
																simple.getForm().findField('agentid').setValue(value.agentid);
																//simple.getForm().findField('pid').setValue('');
															}
														}
													});
												},
												change: function(combo, newvalue,oldvalue){
													if(isNaN(newvalue)){
														simple.getForm().findField('agenttype').setValue('Agent');
														simple.getForm().findField('company').setValue('');
														simple.getForm().findField('typeemail1').setValue(0);
														simple.getForm().findField('typeemail2').setValue(0);
														simple.getForm().findField('email').setValue('');
														simple.getForm().findField('email2').setValue('');
														simple.getForm().findField('typeurl1').setValue(0);
														simple.getForm().findField('urlsend').setValue('');
														simple.getForm().findField('typeurl2').setValue(0);
														simple.getForm().findField('urlsend2').setValue('');
														simple.getForm().findField('typeph1').setValue(0);
														simple.getForm().findField('phone1').setValue('');
														simple.getForm().findField('typeph2').setValue(0);
														simple.getForm().findField('phone2').setValue('');
														simple.getForm().findField('typeph3').setValue(0);
														simple.getForm().findField('phone3').setValue('');
														simple.getForm().findField('typeph4').setValue(0);
														simple.getForm().findField('fax').setValue('');
														simple.getForm().findField('typeph5').setValue(0);
														simple.getForm().findField('tollfree').setValue('');
														simple.getForm().findField('typeph6').setValue(0);
														simple.getForm().findField('phone6').setValue('');
														simple.getForm().findField('typeaddress1').setValue(0);
														simple.getForm().findField('address1').setValue('');
														simple.getForm().findField('typeaddress2').setValue(0);
														simple.getForm().findField('address2').setValue('');
														simple.getForm().findField('agentid').setValue('')
													}
												}
											}
										},/*{
											xtype     : 'textfield', 
											name      : 'agent',
											fieldLabel: 'Contact',
											allowBlank: false,
											value	  : r.records[dato].agent
										},*/{
											xtype         : 'combo',
											mode          : 'remote',
											fieldLabel    : 'Type',
											triggerAction : 'all',
											width		  : 130,
											store         : new Ext.data.JsonStore({
												id:'storetype',
												root:'results',
												totalProperty:'total',
												autoLoad: true, 
												baseParams: {
													type: 'agenttype',
													'userid': <?php echo $userid;?>
												},
												fields:[
													{name:'idtype', type:'string'},
													{name:'name', type:'string'}
												],
												url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
											}),
											displayField  : 'name',
											valueField    : 'idtype',
											name          : 'fagenttype',
											value         : r.records[dato].agentype, //agent.get('agentype'),
											hiddenName    : 'agenttype',
											hiddenValue   : r.records[dato].agenttype, //agent.get('agenttype'),
											allowBlank    : false,
											listeners	  : {
												beforequery: function(qe){
													delete qe.combo.lastQuery;
												}
											}
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Email',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyemail1',
												value	  : r.records[dato].typeemail1,
												hiddenName    : 'typeemail1',
												hiddenValue   : r.records[dato].typeemail1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'email',
												value	  : r.records[dato].email,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Email 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyemail2',
												value         : r.records[dato].typeemail2,
												hiddenName    : 'typeemail2',
												hiddenValue   : r.records[dato].typeemail2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'email2',
												value	  : r.records[dato].email2,
												width	  : 165
											}]
										},{
											xtype     : 'textfield',
											name      : 'company',
											fieldLabel: 'Company',
											value	  : r.records[dato].company
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Website 1',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Personal'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyurl1',
												value         : r.records[dato].typeurl1,
												hiddenName    : 'typeurl1',
												hiddenValue   : r.records[dato].typeurl1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'urlsend',
												value     : r.records[dato].urlsend,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Website 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Personal'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyurl2',
												value         : r.records[dato].typeurl2,
												hiddenName    : 'typeurl2',
												hiddenValue   : r.records[dato].typeurl2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'urlsend2',
												value     : r.records[dato].urlsend2,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname1',
												value         : r.records[dato].typeph1,
												hiddenName    : 'typeph1',
												hiddenValue   : r.records[dato].typeph1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone1',
												width	  : 165,
												value	  : r.records[dato].phone1
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname2',
												value         : r.records[dato].typeph2,
												hiddenName    : 'typeph2',
												hiddenValue   : r.records[dato].typeph2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone2',
												width	  : 165,
												value	  : r.records[dato].phone2
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 3',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname3',
												value         : r.records[dato].typeph3,
												hiddenName    : 'typeph3',
												hiddenValue   : r.records[dato].typeph3,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone3',
												width	  : 165,
												value	  : r.records[dato].phone3
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 4',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname4',
												value         : r.records[dato].typeph4,
												hiddenName    : 'typeph4',
												hiddenValue   : r.records[dato].typeph4,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'fax',
												width	  : 165,
												value	  : r.records[dato].fax
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 5',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname5',
												value         : r.records[dato].typeph5,
												hiddenName    : 'typeph5',
												hiddenValue   : r.records[dato].typeph5,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'tollfree',
												width	  : 165,
												value	  : r.records[dato].tollfree
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Phone 6',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office'],
														['2','Cell'],
														['3','Home Fax'],
														['6','Office Fax'],
														['4','TollFree'],
														['5','O. TollFree']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'typephname6',
												value         : r.records[dato].typeph6,
												hiddenName    : 'typeph6',
												hiddenValue   : r.records[dato].typeph6,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'phone6',
												width	  : 165,
												value	  : r.records[dato].phone6
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Address 1',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyaddress1',
												value	  : r.records[dato].typeaddress1,
												hiddenName    : 'typeaddress1',
												hiddenValue   : r.records[dato].typeaddress1,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'address1',
												value	  : r.records[dato].address1,
												width	  : 165
											}]
										},{
											xtype	  : 'compositefield',
											fieldLabel: 'Address 2',
											items	  : [{
												xtype         : 'combo',
												mode          : 'local',
												triggerAction : 'all',
												width		  : 60,
												store         : new Ext.data.ArrayStore({
													id        : 0,
													fields    : ['valor', 'texto'],
													data      : [
														['0','Home'],
														['1','Office']
													]
												}),
												displayField  : 'texto',
												valueField    : 'valor',
												name          : 'tyaddress2',
												value         : r.records[dato].typeaddress2,
												hiddenName    : 'typeaddress2',
												hiddenValue   : r.records[dato].typeaddress2,
												allowBlank    : false
											},{
												xtype     : 'textfield',
												name      : 'address2',
												value	  : r.records[dato].address2,
												width	  : 165
											}]
										},{
											xtype     : 'hidden',  
											name      : 'type'
										},{
											xtype     : 'hidden',
											name      : 'agentid',
											value     : r.records[dato].agentid
										},{
											xtype     : 'hidden',
											name      : 'pid',
											value     : pid
										}],
										buttons: [{
											text     : 'Save',
											id: 'agentsave',
											hidden	 : !ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(-1,r);*/
												loading_win.show();
												if(simple.getForm().findField('agentid').getValue()==''){
													simple.getForm().findField('type').setValue('insert'); 
													simple.getForm().submit({
														success: function(form, action) {
																if(action.result.agentid!=''){
																	Ext.Ajax.request( 
																	{  
																		waitMsg: 'Adding...',
																		url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																		method: 'POST',
																		timeout :600000,
																		params: { 
																			type	: 'assignment-add',
																			agentid	: action.result.agentid, 
																			userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																			pid		: simple.getForm().findField('pid').getValue() 
																		},
																		
																		failure:function(response,options){
																			loading_win.hide();
																			Ext.MessageBox.alert('Warning','ERROR');
																		},
																		success:function(response,options){
																			var resp=Ext.decode(response.responseText);
																			if(resp.existe==0){
																				alert('Contact inserted.');
																			}else{
																				alert('Contact already assigned.');
																			}
																			pid=simple.getForm().findField('pid').getValue();
																			win.close();
																			loading_win.show();
																			Ext.Ajax.request({
																				waitMsg: 'Seeking...',
																				url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																				method: 'POST',
																				timeout :600000,
																				params: { 
																					pid: pid, 
																					userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																					type: 'assignment'
																				},
																				
																				failure:function(response,options){
																					loading_win.hide();
																					Ext.MessageBox.alert('Warning','ERROR');
																				},
																				success:function(response,options){
																					loading_win.hide();
																					var r=Ext.decode(response.responseText);
																					creaVentana(0,r,pid);
																				}
																			});
																		}                                
																	});
																}
															
														},
														failure: function(form, action) {
															loading_win.hide();
															if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
																Ext.Msg.alert('Error',
																	'Status:'+action.response.status+': '+
																	action.response.statusText);
															}
															if (action.failureType === Ext.form.Action.SERVER_INVALID){
																// server responded with success = false
																Ext.Msg.alert('Invalid', action.result.errormsg);
															}
															if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
																Ext.Msg.alert('Error',
																	'Please check de red market field, before insert contact.');
															}
														}
													});
												}else{
													Ext.Ajax.request( 
													{  
														waitMsg: 'Adding...',
														url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
														method: 'POST',
														timeout :600000,
														params: { 
															type	: 'assignment-add',
															agentid	: simple.getForm().findField('agentid').getValue(), 
															userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
															pid		: simple.getForm().findField('pid').getValue() 
														},
														
														failure:function(response,options){
															loading_win.hide();
															Ext.MessageBox.alert('Warning','ERROR');
														},
														success:function(response,options){
															var resp=Ext.decode(response.responseText);
															if(resp.existe==0){
																alert('Contact inserted.');
															}else{
																alert('Contact already assigned.');
															}
															pid=simple.getForm().findField('pid').getValue();
															win.close();
															loading_win.show();
															Ext.Ajax.request({
																waitMsg: 'Seeking...',
																url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
																method: 'POST',
																timeout :600000,
																params: { 
																	pid: pid,
																	userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																	type: 'assignment'
																},
																
																failure:function(response,options){
																	loading_win.hide();
																	Ext.MessageBox.alert('Warning','ERROR');
																},
																success:function(response,options){
																	loading_win.hide();
																	var r=Ext.decode(response.responseText);
																	creaVentana(0,r,pid);
																}
															});
														}                                
													});
												}
											}
										},{
											text     : 'Cancel',
											id: 'agentcancel',
											hidden	 : !ocultar,
											handler  : function(){
												pid=simple.getForm().findField('pid').getValue();
												win.close();
												creaVentana(0,r,pid);
											}
										},{
											text     : 'Add',
											id: 'agentadd',
											hidden	 : ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(-1,r);*/
												Ext.getCmp('agentsave').setVisible(true);
												Ext.getCmp('agentcancel').setVisible(true);
												Ext.getCmp('agentadd').setVisible(false);
												Ext.getCmp('agentupdate').setVisible(false);
												Ext.getCmp('agentremove').setVisible(false);
												Ext.getCmp('agentset').setVisible(false);
												Ext.getCmp('agentfirst').setVisible(false);
												Ext.getCmp('agentprev').setVisible(false);
												Ext.getCmp('agentnext').setVisible(false);
												Ext.getCmp('agentlast').setVisible(false);
												simple.setTitle('New Contact');
												//simple.getForm().findField('selectagent').setVisible(true);
												simple.getForm().findField('agent').setValue('');
												simple.getForm().findField('agenttype').setValue('Agent');
												simple.getForm().findField('company').setValue('');
												simple.getForm().findField('typeemail1').setValue(0);
												simple.getForm().findField('typeemail2').setValue(0);
												simple.getForm().findField('email').setValue('');
												simple.getForm().findField('email2').setValue('');
												simple.getForm().findField('typeurl1').setValue(0);
												simple.getForm().findField('urlsend').setValue('');
												simple.getForm().findField('typeurl2').setValue(0);
												simple.getForm().findField('urlsend2').setValue('');
												simple.getForm().findField('typeph1').setValue(0);
												simple.getForm().findField('phone1').setValue('');
												simple.getForm().findField('typeph2').setValue(0);
												simple.getForm().findField('phone2').setValue('');
												simple.getForm().findField('typeph3').setValue(0);
												simple.getForm().findField('phone3').setValue('');
												simple.getForm().findField('typeph4').setValue(0);
												simple.getForm().findField('fax').setValue('');
												simple.getForm().findField('typeph5').setValue(0);
												simple.getForm().findField('tollfree').setValue('');
												simple.getForm().findField('typeph6').setValue(0);
												simple.getForm().findField('phone6').setValue('');
												simple.getForm().findField('typeaddress1').setValue(0);
												simple.getForm().findField('address1').setValue('');
												simple.getForm().findField('typeaddress2').setValue(0);
												simple.getForm().findField('address2').setValue('');
												simple.getForm().findField('type').setValue('insert');
												simple.getForm().findField('agentid').setValue('');
												//simple.getForm().findField('pid').setValue('');
											}
										},{
											text     : 'Update',
											id: 'agentupdate',
											hidden	 : ocultar,
											handler  : function(){
												simple.getForm().findField('type').setValue('update');
												loading_win.show();
												simple.getForm().submit({
													success: function(form, action) {
														loading_win.hide();
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														alert('Contact Successfully Updated.');
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: pid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(dato,r,pid);
															}
														});
													},
													failure: function(form, action) {
														loading_win.hide();
														if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
															Ext.Msg.alert('Error',
																'Status:'+action.response.status+': '+
																action.response.statusText);
														}
														if (action.failureType === Ext.form.Action.SERVER_INVALID){
															// server responded with success = false
															Ext.Msg.alert('Invalid', action.result.errormsg);
														}
														if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
															Ext.Msg.alert('Error',
																'Please check de red market field, before update contact.');
														}
													}
												});	
											}
										},{
											text     : 'Remove',
											id: 'agentremove',
											hidden	 : ocultar,
											handler  : function(){
												/*win.close();
												creaVentana(r.total-1,r);*/
												//simple.getForm().findField('type').setValue('assignment-del');
												Ext.Ajax.request( 
												{  
													waitMsg: 'Removing...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														type	: 'assignment-del',
														agentid	: r.records[dato].agentid, //agentid,
														userid	: r.records[dato].userid, //record.get('userid'),
														pid		: r.records[dato].parcelid, //record.get('pid')
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														alert('Contact Successfully Removed.');
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: r.records[dato].parcelid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid);
															}
														});
													}                                
												});		
											}
										},{
											text     : 'Set Principal',
											id: 'agentset',
											hidden	 : ocultar,
											handler  : function(){
												var agentname = r.records[dato].agent;//agent.get('agent');
												loading_win.show();
												Ext.Ajax.request( 
												{  
													waitMsg: 'Assigning...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														type		: 'assignment-principal',
														agentname	: agentname,
														userid		: r.records[dato].userid,
														pid			: r.records[dato].parcelid
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														alert('Contact Successfully Updated.');
														pid=simple.getForm().findField('pid').getValue();
														win.close();
														loading_win.show();
														Ext.Ajax.request({
															waitMsg: 'Seeking...',
															url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
															method: 'POST',
															timeout :600000,
															params: { 
																pid: r.records[dato].parcelid,
																userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
																type: 'assignment'
															},
															
															failure:function(response,options){
																loading_win.hide();
																Ext.MessageBox.alert('Warning','ERROR');
															},
															success:function(response,options){
																loading_win.hide();
																var r=Ext.decode(response.responseText);
																creaVentana(0,r,pid);
															}
														});
													}                                
												});
											}
										}]
								});
								 
								var win = new Ext.Window({
									layout      : 'fit',
									width       : 400,
									height      : 550,
									modal	 	: true,
									plain       : true,
									items		: simple,
									closeAction : 'close',
									buttons: [{
											text     : '|<',
											id: 'agentfirst',
											width: 30, 
											hidden	 : (dato==0), 	
											handler  : function(){
												/*win.close();
												creaVentana(0,r);*/
												dato=0;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '<',
											id: 'agentprev',
											width: 30, 
											hidden	 : (dato-1<0), 	
											handler  : function(){
												/*win.close();
												creaVentana(dato-1,r);*/
												dato=dato-1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '>',
											id: 'agentnext',
											width: 30, 
											hidden	 : (dato+1>r.total-1),
											handler  : function(){
												/*win.close();
												creaVentana(dato+1,r);*/
												dato=dato+1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
											text     : '>|',
											id: 'agentlast',
											width: 30, 
											hidden	 : (dato+1>r.total-1), 	
											handler  : function(){
												/*win.close();
												creaVentana(r.total-1,r);*/
												dato=r.total-1;
												var value=r.records[dato];
												Ext.getCmp('agentfirst').setVisible(!(dato==0));
												Ext.getCmp('agentprev').setVisible(!(dato-1<0));
												Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
												Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
												var princ='';
												if(value.principal=='1')
													princ='(Default)';
												simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
												simple.getForm().findField('agent').setValue(value.agent);
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('company').setValue(value.company);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeurl1').setValue(value.typeurl1);
												simple.getForm().findField('urlsend').setValue(value.urlsend);
												simple.getForm().findField('typeurl2').setValue(value.typeurl2);
												simple.getForm().findField('urlsend2').setValue(value.urlsend2);
												simple.getForm().findField('typeph1').setValue(value.typeph1);
												simple.getForm().findField('phone1').setValue(value.phone1);
												simple.getForm().findField('typeph2').setValue(value.typeph2);
												simple.getForm().findField('phone2').setValue(value.phone2);
												simple.getForm().findField('typeph3').setValue(value.typeph3);
												simple.getForm().findField('phone3').setValue(value.phone3);
												simple.getForm().findField('typeph4').setValue(value.typeph4);
												simple.getForm().findField('fax').setValue(value.fax);
												simple.getForm().findField('typeph5').setValue(value.typeph5);
												simple.getForm().findField('tollfree').setValue(value.tollfree);
												simple.getForm().findField('typeph6').setValue(value.typeph6);
												simple.getForm().findField('phone6').setValue(value.phone6);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
												simple.getForm().findField('type').setValue('update');
												simple.getForm().findField('agentid').setValue(value.agentid);	
											}
										},{
										text     : 'Close',
										handler  : function(){
											win.close();
											loading_win.hide();
										}
									}]
								});
								win.show();
							}  
	function creaMenuTask(e,rowIndex){ 
		x = e.clientX;
 		y = e.clientY; 
		var record = gridmyfollowtask.getStore().getAt(rowIndex);
		var pid = record.get('pid');
		var county = record.get('county');
		var status = record.get('status');
		
		var contact = new Ext.Action({
			text: 'Go to Contact',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
					method: 'POST',
					timeout :600000,
					params: { 
						pid: pid,
						userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
						type: 'assignment'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var r=Ext.decode(response.responseText);
						creaVentana(0,r,pid);
					}
				});
			}
		});
		var contract = new Ext.Action({
			text: 'Go to Contract',
			handler: function(){
				Ext.Ajax.request( 
				{  
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
					method: 'POST',
					timeout :600000,
					params: { 
						pid: pid,
						userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						
						var r=Ext.decode(response.responseText);
						
						if(r.results=='error'){
							Ext.Msg.alert("Follow Up", 'Contract its not generated or not in system.');
						}else{ 
							if(Ext.isIE)
								window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
							 else
								window.open(r.url,'_newtab');
						}
						return false;
					}                                
				});
			}
		});
		
		var status = new Ext.Action({
			text: 'Change Status',
			handler: function(){
				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
					frame: true,
					title: 'Follow Up - Status Change',
					width: 350,
					waitMsgTarget : 'Waiting...',
					labelWidth: 75,
					defaults: {width: 230},
					labelAlign: 'left',
					items: [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								fieldLabel	  : 'Status',
								width		  : 130,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['N','None'],
										['UC','Under Contract'],
										['PS','Pending Sale']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'statusaltname',
								value         : 'N',
								hiddenName    : 'statusalt',
								hiddenValue   : 'N',
								allowBlank    : false
							},{
								xtype     : 'hidden',
								name      : 'pid',
								value     : pid
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'change-status'
							}],
					
					buttons: [{
							text: 'Change',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow Up", 'Status Changed.');
										storemyfollowtask.load();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});
				 
				var win = new Ext.Window({
					layout      : 'fit',
					width       : 240,
					height      : 180,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
							loading_win.hide();
						}
					}]
				});
				win.show();
			}
		});
		var follow_history = new Ext.Action({
			text: 'Go to Follow History',
			handler: function(){
				gridmyfollowtask.fireEvent('rowdblclick', gridmyfollowtask, rowIndex);
			}
		});
		var overview = new Ext.Action({
			text: 'Go to Overview',
			handler: function(){
				createOverview(county,pid,status,false,false);
			}
		});
		
		var menu = new Ext.menu.Menu({
			items: [
				contact,status,overview,contract,follow_history
			]
		});
		
		menu.showAt([x,y]);
		return false;
	}
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenuTask(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive Call." src="../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../img/notes/reci_other.png" />'; break;
		}
	}

	var smmyfollowup = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowup.indexOf(record.get('pid'))==-1)
					selected_datamyfollowup.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowtask.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowup=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowup = selected_datamyfollowup.remove(record.get('pid'));
				AllCheckmyfollowup=false;
				Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'mytasks_filters',
		items: [
			new Ext.Button({
				tooltip: 'Click to delete selected follow.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					if(selected_datamyfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					var pids='\''+selected_datamyfollowup[0]+'\'';
					for(i=1; i<selected_datamyfollowup.length; i++)
						pids+=',\''+selected_datamyfollowup[i]+'\''; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowtask.load({params:{start:0, limit:limitmyfollowup}});
							Ext.Msg.alert("Follow Up", 'Follow delete.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to print follow.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filteraddressmyfollowup,
							mlnumber: filtermlnumbermyfollowup,
							agent: filteragentmyfollowup,
							status: filterstatusmyfollowup,
							ndate: filterndatemyfollowup,
							ndateb: filterndatebmyfollowup,
							ntask: filterntaskmyfollowup,
							contract: filtercontractmyfollowup,
							pof: filterpofmyfollowup,
							emd: filteremdmyfollowup,
							ademdums: filterademdumsmyfollowup,
							msj: filtermsjmyfollowup,
							history: filterhistorymyfollowup,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to export Excel.',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				scale: 'medium',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filteraddressmyfollowup,
							mlnumber: filtermlnumbermyfollowup,
							agent: filteragentmyfollowup,
							status: filterstatusmyfollowup,
							ndate: filterndatemyfollowup,
							ndateb: filterndatebmyfollowup,
							ntask: filterntaskmyfollowup,
							contract: filtercontractmyfollowup,
							pof: filterpofmyfollowup,
							emd: filteremdmyfollowup,
							ademdums: filterademdumsmyfollowup,
							msj: filtermsjmyfollowup,
							history: filterhistorymyfollowup,
							sort: filterfield,
							dir: filterdirection
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				scale: 'medium',
				handler: function(){
					filteraddressmyfollowup = '';
					filtermlnumbermyfollowup = '';
					filteragentmyfollowup = '';
					filterstatusmyfollowup = 'ALL';
					filterndatemyfollowup = '';
					filterndatebmyfollowup = '';
					filterntaskmyfollowup = '-2';
					filtercontractmyfollowup = '-1';
					filterpofmyfollowup = '-1';
					filteremdmyfollowup = '-1';
					filterademdumsmyfollowup = '-1';
					filtermsjmyfollowup = '-1';
					filterhistorymyfollowup = '-1';	
					filterofferreceivedmyfollowup = '-1';
					var formmyfollowup = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'mytasks_filters',
						id: 'formmytasks',
						name: 'formmytasks',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowup=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fmlnumber',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtermlnumbermyfollowup=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteragentmyfollowup=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fndate',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'Task Date',
									width		  : 130,
									editable	  : false,
									format		  : 'Y-m-d',
									name		  : 'fndate',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterndatemyfollowup=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fndateb',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'Between',
									width		  : 130,
									editable	  : false,
									format		  : 'Y-m-d',
									name		  : 'fndateb',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterndatebmyfollowup=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fntask',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Task',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fntaskname',
									value         : '-1',
									hiddenName    : 'fntask',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterntaskmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : 'ALL',
									hiddenName    : 'fstatus',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterstatusmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontract',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Contract',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcontractname',
									value         : '-1',
									hiddenName    : 'fcontract',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtercontractmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fpof',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Proof of Funds',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpofname',
									value         : '-1',
									hiddenName    : 'fpof',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterpofmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'femd',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'EMD',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'femdname',
									value         : '-1',
									hiddenName    : 'femd',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filteremdmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fademdums',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Addendums', 
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fademdumsname',
									value         : '-1',
									hiddenName    : 'fademdums',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterademdumsmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmsj',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Message',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Yes'],
											['0','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fmsjname',
									value         : '-1',
									hiddenName    : 'fmsj',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtermsjmyfollowup = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fofferreceived',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Offer Received',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fofferreceived',
									value         : '-1',
									hiddenName    : 'foffer',
									hiddenValue   : '-1',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterofferreceivedmyfollowup = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowtask.load({params:{start:0, limit:limitmyfollowup}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteraddressmyfollowup = '';
									filtermlnumbermyfollowup = '';
									filteragentmyfollowup = '';
									filterstatusmyfollowup = 'ALL';
									filterndatemyfollowup = '';
									filterndatebmyfollowup = '';
									filterntaskmyfollowup = '-2';
									filtercontractmyfollowup = '-1';
									filterpofmyfollowup = '-1';
									filteremdmyfollowup = '-1';
									filterademdumsmyfollowup = '-1';
									filtermsjmyfollowup = '-1';
									filterhistorymyfollowup = '-1';	
									filterofferreceivedmyfollowup = '-1';
									
									Ext.getCmp('formmytasks').getForm().reset();
									
									storemyfollowtask.load({params:{start:0, limit:limitmyfollowup}});
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 450,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowup,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			})
		]
	});
	var gridmyfollowtask = new Ext.grid.GridPanel({
		renderTo: 'mytasks_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowtask,
		stripeRows: true,
		sm: smmyfollowup, 
		columns: [
			smmyfollowup,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'M', width: 25, sortable: true, tooltip: 'New Messages Chat.', dataIndex: 'msj', renderer: msjRender},{header: 'V', width: 25, sortable: true, tooltip: 'View.', dataIndex: 'msj', renderer: viewRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender},{header: 'T', width: 25, renderer: taskRender, tooltip: 'Next Task.', dataIndex: 'ntask', sortable: true},{header: 'Next Date', width: 60, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate', align: 'right', sortable: true}";	
					else
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Offer %', width: 50, sortable: true, tooltip: 'Offer Percent [(offer/list price)*100%].', dataIndex: 'offerpercent', align: 'right'}
				,{header: 'L. Price', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Listing Price', dataIndex: 'lprice', align: 'right'}
				,{header: 'Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Offer.', dataIndex: 'offer', align: 'right'}
				,{header: 'C. Offer', renderer: Ext.util.Format.usMoney, width: 70, sortable: true, tooltip: 'Contra Offer.', dataIndex: 'coffer', align: 'right'}
				
				,{header: 'LU', width: 30, sortable: true, tooltip: 'Days of last insert history.', dataIndex: 'lasthistorydate', align: 'right'}
				,{header: 'C', width: 25, sortable: true, tooltip: 'Contract.', dataIndex: 'contract', renderer: checkRender}
				,{header: 'P', width: 25, sortable: true, tooltip: 'Prof of Funds.', dataIndex: 'pof', renderer: checkRender}
				,{header: 'E', width: 25, sortable: true, tooltip: 'EMD.', dataIndex: 'emd', renderer: checkRender}
				,{header: 'A', width: 25, sortable: true, tooltip: 'Addendums.', dataIndex: 'rademdums', renderer: checkRender}
				,{header: 'O', width: 25, sortable: true, tooltip: 'Offer Received.', dataIndex: 'offerreceived', renderer: checkRender}";
		   ?>			 
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmytasks',
            pageSize: limitmyfollowup,
            store: storemyfollowtask,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowup=50;
					Ext.getCmp('pagingmyfollowup').pageSize = limitmyfollowup;
					Ext.getCmp('pagingmyfollowup').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowup=80;
					Ext.getCmp('pagingmyfollowup').pageSize = limitmyfollowup;
					Ext.getCmp('pagingmyfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowup=100;
					Ext.getCmp('pagingmyfollowup').pageSize = limitmyfollowup;
					Ext.getCmp('pagingmyfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'Contract its not generated or not in system.');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var status = new Ext.Action({
					text: 'Change Status',
					handler: function(){
						var simple = new Ext.FormPanel({
							url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
							frame: true,
							title: 'Follow Up - Status Change',
							width: 350,
							waitMsgTarget : 'Waiting...',
							labelWidth: 75,
							defaults: {width: 230},
							labelAlign: 'left',
							items: [{
										xtype         : 'combo',
										mode          : 'local',
										triggerAction : 'all',
										fieldLabel	  : 'Status',
										width		  : 130,
										store         : new Ext.data.ArrayStore({
											id        : 0,
											fields    : ['valor', 'texto'],
											data      : [
												['N','None'],
												['UC','Under Contract'],
												['PS','Pending Sale']
											]
										}),
										displayField  : 'texto',
										valueField    : 'valor',
										name          : 'statusaltname',
										value         : 'N',
										hiddenName    : 'statusalt',
										hiddenValue   : 'N',
										allowBlank    : false
									},{
										xtype     : 'hidden',
										name      : 'pid',
										value     : pid
									},{
										xtype     : 'hidden',
										name      : 'type',
										value     : 'change-status'
									}],
							
							buttons: [{
									text: 'Change',
									handler: function(){
										loading_win.show();
										simple.getForm().submit({
											success: function(form, action) {
												loading_win.hide();
												win.close();
												Ext.Msg.alert("Follow Up", 'Status Changed.');
												storemyfollowtask.load();
											},
											failure: function(form, action) {
												loading_win.hide();
												Ext.Msg.alert("Failure", "ERROR");
											}
										});
									}
								},{
									text: 'Reset',
									handler  : function(){
										simple.getForm().reset();
										win.close();
									}
								}]
							});
						 
						var win = new Ext.Window({
							layout      : 'fit',
							width       : 240,
							height      : 180,
							modal	 	: true,
							plain       : true,
							items		: simple,
							closeAction : 'close',
							buttons: [{
								text     : 'Close',
								handler  : function(){
									win.close();
									loading_win.hide();
								}
							}]
						});
						win.show();
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						status,overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
					method: 'POST',
					timeout :600000,
					params: { 
						parcelid: pid,
						userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var rsch=Ext.decode(response.responseText);
						var followfus=rsch.records[0];
						var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Complete Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfus.task,
											hiddenName    : 'task',
											hiddenValue   : followfus.task,
											allowBlank    : false
										},{
											xtype: 'button',
											tooltip: 'Click to View Contacts.',
											iconCls:'icon',
											iconAlign: 'top',
											width: 40,
											icon: 'http://www.reifax.com/img/agent.png',
											scale: 'medium',
											handler: function(){
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														pid: pid,
														userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid);
													}
												});
											}
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'sheduledetail',
											fieldLabel: 'Schedule Detail',
											value	  : followfus.detail
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail'
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 3,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: false},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: false},
												{boxLabel: 'EMD', name: 'emd', checked: false},
												{boxLabel: 'Addendums', name: 'rademdums', checked: false},
												{boxLabel: 'Offer Received', name: 'offerreceived'} 
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'complete'
										},{
											xtype     : 'hidden',
											name      : 'idfus',
											value     : followfus.idfus
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : followfus.parcelid
										}],
								
								buttons: [{
										text: 'Complete',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'Completed Follow Schedule Task.');
													storemyfollowtask.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 530,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
					}
				});
			}
		}
	});
	
	storemyfollowtask.load({params:{start:0, limit:limitmyfollowup}});

	if(document.getElementById('tabs')){
		if(document.getElementById('todo_mytasks_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_mytasks_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>