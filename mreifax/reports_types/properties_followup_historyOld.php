<?php
	include("../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>

<div id="report_content">
	<br clear="all">
    <div class="overview_realtor_titulo" style=" width:100%">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<h1 align="center" >FOLLOW HISTORY</h1>
    <br clear="all">
	<div id="follow_history_grid" style="width:650px;"></div>
	<br>
</div>

<script>
	//document.getElementById('follow_history_grid').innerHTML='&nbsp;';
	var followstore = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followhistory.php',
		fields: [ 'idfuh','parcelid','userid',
		   {name: 'odate', type: 'date', dateFormat: 'Y-m-d'},
		   {name: 'offer', type: 'float'},
		   {name: 'coffer', type: 'float'},
		   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
		   {name: 'contract', type: 'bool'},
		   {name: 'pof', type: 'bool'},
		   {name: 'emd', type: 'bool'},
		   {name: 'realtorsadem', type: 'bool'},
		   {name: 'sellersadem', type: 'bool'},
		   {name: 'detail'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'parcelid': "<?php echo $_POST['parcelid'];?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true
    });
	
	var followdetail = new Ext.ux.grid.RowExpander({
		tpl : new Ext.Template(
			'<br><p style="margin-left:50px;font-size:14px;"><b>Detail:</b> {detail}</p>'
		)
	});
	var followsm = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../img/drop-no.gif" />'; 
		else return '<img src="../img/drop-yes.gif" />';
	}
	
	var followgrid = new Ext.grid.GridPanel({
		store: followstore,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [
				followsm,
				followdetail,
				{header: "Date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'odate', tooltip: 'Insert/Update Date.'},
				{header: "Next Date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'ndate', tooltip: 'Next Date.'},
				{header: "Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'offer', tooltip: 'Offer.'},
				{header: "C. Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'coffer', tooltip: 'Contra Offer.'},
				{header: "C", width: 5, dataIndex: 'contract', renderer: checkRender, tooltip: 'Contract Send.'},
				{header: "P", width: 5, dataIndex: 'pof', renderer: checkRender, tooltip: 'Prof of Funds.'},
				{header: "E", width: 5, dataIndex: 'emd', renderer: checkRender, tooltip: 'EMD.'},
				{header: "A", width: 5, dataIndex: 'realtorsadem', renderer: checkRender, tooltip: 'Ademdums.'}
			]
		}),
		viewConfig: {
			forceFit:true
		},  
		sm: followsm,      
		width: 600,
		height: 300,
		plugins: followdetail,
		iconCls: 'icon-grid',
		renderTo: 'follow_history_grid'
	});
	
	followstore.load();

if(document.getElementById('tabs')){
	if(document.getElementById('report_content').offsetHeight > tabs.getHeight()){
		tabs.setHeight(document.getElementById('report_content').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content').offsetHeight+150);
	}
}
</script>