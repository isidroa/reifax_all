<?php
	include('../properties_overview_function.php');
	$db_data=$_GET['db'];
	$pid=$_GET['pid'];
	$xcode=$_GET['xcode'];
	$print = isset($_GET['document']) || isset($par_no_conexion) ? true : false;
	$ocomp = isset($_GET['overview_comp']) ? true : false;
	$array_taken = isset($_POST['array_taken']) ? $_POST['array_taken'] : '';
	$orderField = isset($_POST['orderField']) ? $_POST['orderField'] : 'Distance';
	$orderDir = isset($_POST['orderDir']) ? $_POST['orderDir'] : 'ASC';
	conectarPorBD($db_data);

	$que="SELECT idcounty from xima.lscounty WHERE replace(bd,' ','')='".str_replace('1','',$db_data)."'";
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$county=$r[0];
	
	$loged=false;
	$block=true;
	$permission=array();
	if(isset($_COOKIE['datos_usr']['USERID'])){
		$loged=true;
		if ($realtor){ $loged=false; }
		
		$query='select block_county,block_commercial,block_realtorweb 
		from xima.xima_system_var 
		WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		$block=($r['block_county']=='Y' || $r['block_commercial']=='Y' || $r['block_realtorweb']=='Y') ? true : false;
		$realtorweb=$r['block_realtorweb']=='Y' ? true : false;
		
		$query='select * from xima.permission WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		$result=mysql_query($query) or die($query.mysql_error());
		$permission=mysql_fetch_array($result);
		
		if($realtorweb && $permission['adevertisingweb']==0)
			$loged=false;
	}
	
	if(!$loged || $block){
		
		echo '<div style="font-size:12px; margin-top: 5px;">';
		if($realtorweb){
			echo 'This feature is only available for full registered users.<br>';
		}elseif($permission['platinum']!=1 && $permission['professional']!=1 && $permission['professional_esp']!=1){
			echo 'This feature is not available for not commercial users.<br>';
		}else{
			echo 'This feature is only available for registered users.<br>';
			echo 'If you are a Register User please <a href="javascript:void();" onclick="login_win.show();return false;">log In</a>.<br>';
			echo '<a href="https:www.reifax.com/register.php" style=" color:red;">Register Now!!!!</a>';
		}
		echo '</div>';
		return false;	
	}
	
	$map="compactive_mymap";
	$grid_render="compactive_sujeto";
	$grid_render2="compactive_comparables";
	$pagin_comp_tol="pagingActive";
	$pin="PinCompActive";
	$imgPin="ImageCompActive";
	if(isset($par_no_conexion)){
		$map="Rcompactive_mymap";
		$grid_render="Rcompactive_sujeto";
		$grid_render2="Rcompactive_comparables";
		$pagin_comp_tol="RpagingActive";
		$pin="PinCompActiveR";
		$imgPin="ImageCompActiveR";
	}
	if($ocomp){
		$map="Ccompactive_mymap";
		$grid_render="Ccompactive_sujeto";
		$grid_render2="Ccompactive_comparables";
		$pagin_comp_tol="CpagingActive";
		$pin="PinCompActiveC";
		$imgPin="ImageCompActiveC";
	}
	
	overviewCompActive($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir);
?>