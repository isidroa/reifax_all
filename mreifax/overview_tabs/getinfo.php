<?php
/**
 * getinfo.php
 *
 * Gets the info of a parcel ID
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 05.04.2011
 */

include "../properties_conexion.php"; 
conectar();

$db     = strtolower($_GET['db']);
$pid    = $_GET['pid'];
$userid = $_COOKIE['datos_usr']["USERID"]; // $userid = $_GET['USERID'];

conectarPorBD($db);

$query   = "SELECT * FROM fl{$db}.mlsresidential WHERE parcelid='$pid'";
$rs      = mysql_query($query);

if ($rs) {
	
	$row  = mysql_fetch_array($rs);
	
	$row['userid']=$userid;
	$row['dateAccept'] = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")));
	$row['dateClose']  = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")));

	$query             = "SELECT platinum FROM xima.permission WHERE userid='$userid'";
	$rs                = mysql_query($query);
	$rpinv             = mysql_fetch_array($rs);
	
	$row['platinum']   = $rpinv[0];
	
	$query             = "SELECT professional_esp FROM xima.permission WHERE userid='$userid'";
	$rs                = mysql_query($query);
	$rpinv             = mysql_fetch_array($rs);
	
	$row['professional_esp']   = $rpinv[0];
	
	$query             = "SELECT owner,owner_a,owner_c,owner_s,owner_z from fl{$db}.psummary where parcelid='$pid'";
	$rs                = mysql_query($query);
	$rinfo             = mysql_fetch_array($rs);
	
	$row['sellname']   = $rinfo[0];
	$row['selladdress']= $rinfo[1];
	$row['sellcity']   = $rinfo[2];
	$row['sellstate']  = $rinfo[3];
	$row['sellzip']    = $rinfo[4];

	$query              = "SELECT count(*) FROM xima.contracts_mailsettings WHERE userid = $userid";
	$rs                 = mysql_query($query);
	$mailInfo           = mysql_fetch_array($rs);

    $row['mailconf']    = 0;
	if ($mailInfo[0]>0)
		$row['mailconf'] = 1;
	
	$query="select bd, offer, address, mlnumber, a.* from xima.followup f
		LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$pid."')";
	$result=mysql_query($query) or die($query.mysql_error());
	
	if(mysql_num_rows($result)>0){
		$r=mysql_fetch_array($result);
		$remail     = $r['email'];
		
		if($remail==''){
			$remail = $r['email2'];
		}
		if($remail!=''){
			$row['Agent']=$r['agent'];
			$row['AgentEmail']=$remail;
			$row['OfficeName']=$r['company'];
		}
	}
	
	$resp = array('success'=>'true','data'=>$row);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

if ($error != "") 
	$resp = array('success'=>'true','mensaje'=>$error);

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	