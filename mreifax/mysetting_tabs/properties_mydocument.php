<?php
	include("../properties_conexion.php");
	conectar();
	
	include ("../properties_getgridcamptit.php");		
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYDoc','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	$que="SELECT professional_esp FROM xima.permission WHERE userid=".$userid;
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$botoninvestor=$r[0]==1?'false':'true';

	$que="SELECT contactpremiun FROM xima.ximausrs WHERE userid=".$userid;
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$contactpremiun=$r[0];
	
	$query = 'SELECT * FROM xima.contracts_contactsettings WHERE userid='.$userid;
	$result = mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($result) > 0){
		$r = mysql_fetch_array($result);
		
		$getContactBlock 		= intval($r['block']);
		$getContactOverwrite 	= intval($r['overwrite']);
		$getContactPremium 		= intval($r['premium']);
	}else{
		$getContactBlock 		= 0;
		$getContactOverwrite 	= 1;
		$getContactPremium 		= 0;
	}
	$query="select *,if(DATE_FORMAT(fecha,'%Y/%m/%d')<>CURDATE(),'SI','NO') as control from mantenimiento.emailusercontrol where userid=$userid";
	$result=mysql_query($query) or die($query.mysql_error());
	if(mysql_affected_rows()<=0)
		$totalsent=0;
	else{
		$r=mysql_fetch_assoc($result);
		if($r['control']=="SI")
			$totalsent=0;
		else
			$totalsent=$r['totalSendMail'];
	}
	
	$dateAccept = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")));
	$dateClose = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")));
	
?>
<div align="center" id="todo_mydocument_panel">
    <div id="mydocument_data_div" align="center" style=" margin:auto; width:100%">
    <?php // Incluido por Luis R Castro 28/04/2015 Suggestion 12356 // ?>
    <div id="mydocument_filters"></div>
   
    <?php /////////////////////////////////////// ?>
        <div id="mydocument_properties"></div>
    </div>
</div>
<?php // Incluido por Luis R Castro 28/04/2015 Suggestion 12356 // ?>

<script type="text/javascript" src="mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>

<script>

	var botoninvestor=<?php echo $botoninvestor; ?>;
	var totalsent=<?php echo $totalsent; ?>;
	var totalenviados=0;
	var fechaAcc=<?php echo "'".$dateAccept."'"; ?>;
	var fechaClo=<?php echo "'".$dateClose."'"; ?>;
	var useridspider= <?php echo $userid;?>;
	
	//variables for get contacts
	var contactpremiun= <?php echo $contactpremiun;?>;
	var getContactBlock= <?php echo $getContactBlock;?>;
	var getContactOverwrite = <?php echo $getContactOverwrite;?>;
	var getContactPremium = <?php echo $getContactPremium;?>;
	
	var totalnosent=0;
	var limitmydocuments 			= 50;
	var selected_datamydocuments 	= new Array();
	var AllCheckmydocuments 			= false;
	
	//filter variables
	var filtersMyDocuments = {
		property		: {
			address		: '',
			parcelid	: '',
			name	: ''
			
		},
		archive			: {
			sdatec		: 'Equal',
			sdate		: '',
			sdateb		: ''
		},
		
		
		contract		: '-1',
	
		order			: {
			field		: 'address',
			direction	: 'ASC'
		}
	};
	
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	checkPendings(<?php echo $userid;?>);
	
	<?php ///////////////////////?>
	
	function urldoc(value, metaData, record, rowIndex){
		var urlamazon = record.get('urlamazon');
		var url = urlamazon ? urlamazon : value;
		
		var testA=/^https:\/\/s3.+php$/;
		if(testA.test(url)){
			url= '/includes/downs3.php?file='+url;
		}
		url=url.replace('reifax.com/docs/','reifax.com/');
		return '<a href="'+url+'" target="_blank"><img src="http://www.reifax.com/img/doc.png"/></a>';
	}
		<?php // Incluido por Luis R Castro 28/04/2015 Suggestion 12356 // ?>
	
		var toolbarmydocument=new Ext.Toolbar({
		renderTo: 'mydocument_filters',
		items: [ new Ext.Button({
					tooltip: 'Delete Documents',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
                     icon: 'http://www.reifax.com/img/toolbar/trash.png',
                toggleGroup: 'searchtoogle',
                handler: function(){

                               var seld = new Array();
                                var selpid = new Array();

                                seld= smmydocument.getSelections();

                                if (seld.length!=0)
                                {
                               for (i=0;i<seld.length;i++)
                                   {
                                       if (i>0)
                                           selpid+=",";

                                           //selpid+= seld[i].get('pid');//COMENTADO POR SMITH 21/09/2012 03:00
										   selpid+= seld[i].json.pid;
                                    }

                    Ext.Ajax.request({
                                waitMsg: 'Delete My Document...',
                                url: 'mysetting_tabs/mydocument_tabs/delete.php',
                                method: 'POST',
                                params: { pid: selpid,
                                          nelim: seld.length},

                                success:function(response,options)
                                {
                                    if(response.responseText== 'true' || response.responseText==true)
                                        Ext.Msg.alert('Mensaje', 'Property delete successfully to My document.');
                                    else
                                        Ext.Msg.alert('Mensaje', response.responseText);
                                },
                                failure:function(response,options)
                                {
                                    Ext.Msg.alert("Failure", response.responseText);
                                }

                                   });
                              var tab=tabs3.getActiveTab();
                               var updater = tab.getUpdater();
                               updater.update({url: 'mysetting_tabs/properties_mydocument.php'});
                               }
                                 else
                                 { alert(' my document are not selected'); }

                   }
            				
			}),
			new Ext.Button({
				tooltip: 'Filter Archives',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmydocument = new Ext.FormPanel({
						url:'mysetting_tabs/mydocument_tabs/properties_document.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'mydocument_filters', 
						id: 'formmydocument',
						name: 'formmydocument',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 380},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersMyDocuments.property.address,	
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMyDocuments.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fname',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Name',
									name		  : 'fname',
									value		  : filtersMyDocuments.property.name,	
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMyDocuments.property.name=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fparcelid',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Folio',
									name		  : 'fparcelid',
									value		  : filtersMyDocuments.property.parcelid,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMyDocuments.property.parcelid=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontract',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Contract',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcontractname',
									value         : filtersMyDocuments.contract,
									hiddenName    : 'fcontract',
									hiddenValue   : filtersMyDocuments.contract,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMyDocuments.contract = record.get('valor');
										
										}
									}
								}	
								
								]
							},{
								layout	: 'form',
								id		: 'fsdate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Archive Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fsdatecname',
										hiddenName: 'fsdatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersMyDocuments.archive.sdatec,
										listeners: {
											'select': function (combo,record,index){
												filtersMyDocuments.archive.sdatec = record.get('id');
												var secondfield = Ext.getCmp('fsdateb');
												secondfield.setValue('');
												filtersMyDocuments.archive.sdateb='';
												
												if(filtersMyDocuments.archive.sdatec=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fsdate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'Y-m-d',
											name		  : 'fsdate',
											value		  : filtersMyDocuments.archive.sdate,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersMyDocuments.archive.sdate=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'Y-m-d',
										id			  : 'fsdateb',
										name		  : 'fsdateb',
										hidden		  : filtersMyDocuments.archive.sdatec!='Between',
										value		  : filtersMyDocuments.archive.sdateb,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersMyDocuments.archive.sdateb=newvalue;
											}
										}
									}]
								}]	
							}]
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersMyDocuments.property.address = formmydocument.getForm().findField('faddress').getValue();
									filtersMyDocuments.property.name = formmydocument.getForm().findField('fname').getValue();
									filtersMyDocuments.property.parcelid = formmydocument.getForm().findField('fparcelid').getValue();
									filtersMyDocuments.contract = formmydocument.getForm().findField('fcontract').getValue();
									filtersMyDocuments.archive.sdatec = formmydocument.getForm().findField('fsdatec').getValue();
									filtersMyDocuments.archive.sdate = formmydocument.getForm().findField('fsdate').getValue();
									filtersMyDocuments.archive.sdateb = formmydocument.getForm().findField('fsdateb').getValue();
									storemydocument.load({params:{start:0, limit:limitmydocuments}});
									//storemydocumentAll.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersMyDocuments.property.address = '';
									filtersMyDocuments.property.name = '';
									filtersMyDocuments.property.parcelid = '';
									filtersMyDocuments.contract = '-1';
									
									filtersMyDocuments.archive.sdate = '';
									filtersMyDocuments.archive.sdateb = '';
									filtersMyDocuments.archive.sdatec = 'Equal';
									
									formmydocument.getForm().findField('faddress').setValue('');
									formmydocument.getForm().findField('fparcelid').setValue('');
									formmydocument.getForm().findField('fname').setValue('');
									formmydocument.getForm().findField('fcontract').setValue('-1');
									formmydocument.getForm().findField('fsdatec').setValue('Equal');
									formmydocument.getForm().findField('fsdate').setValue('');
									formmydocument.getForm().findField('fsdateb').setValue('');
								
								storemydocument.load({params:{start:0, limit:limitmydocuments}});
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 710,
								height      : 460,
								modal	 	: true,  
								plain       : true,
								items		: formmydocument,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersMyDocuments.property.address = '';
					filtersMyDocuments.property.name = '';
					filtersMyDocuments.property.parcelid = '';
					filtersMyDocuments.contract = '-1';
					filtersMyDocuments.contractid = '';				
					filtersMyDocuments.archive.sdatec = 'Equal';
					filtersMyDocuments.archive.sdate = '';
					filtersMyDocuments.archive.sdateb = '';
					storemydocument.load({params:{start:0, limit:limitmydocuments}});
					
				}
			})
			
		]
	});
	
	<?php //////////////////////////////////////////////////////////////////// ?>
	
	
	var storemydocument = new Ext.data.JsonStore({
        url: 'mysetting_tabs/mydocument_tabs/properties_document.php',
		fields: [
            <?php 
		   		echo "'ind','parcelid'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",
			   {name: 'contract', type: 'bool'},
			   {name: 'sdate', type: 'date', dateFormat: 'Y-m-d H:i:s'} "
			   
			   
		    ?>
        ],
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'sdate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		},listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersMyDocuments.property.address;
				obj.params.name=filtersMyDocuments.property.name;
				obj.params.parcelid=filtersMyDocuments.property.parcelid;
				obj.params.contract=filtersMyDocuments.contract;
				obj.params.sdatec = filtersMyDocuments.archive.sdatec;
				obj.params.sdate=filtersMyDocuments.archive.sdate;
				obj.params.sdateb=filtersMyDocuments.archive.sdateb;
			}
		}
    });
	var limitmydocuments = 50;
	
	var smmydocument = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	var gridmydocument = new Ext.grid.GridPanel({
		renderTo: 'mydocument_properties',
		cls: 'grid_comparables',
		width: 980, 
		height: 2300,
		store: storemydocument,		
		columns: [
				  smmydocument,
			<?php 
		   		echo "{id: 'ind' , header: 'Ind.', width: 50, sortable: true, tooltip: 'Index Property.', dataIndex: 'ind'},{header: '', hidden: true, editable: false, dataIndex: 'parcelid'}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='url'){
						echo ",{header: '".$val->title."', width: 130, renderer: urldoc, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					
					}
					// Luis R Castro 30/04/2015 Sugerencia 12356 
					elseif($val->name=='sdate'){
						echo ",{header: 'Archive Date', width: 130, format: 'Y-m-d H:i:s' , renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: '".$val->name."', align: 'right', sortable: true, tooltip: 'Archive Date'}";		
						}else{
						echo ",{header: '".$val->title."', sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";}
						///
						
				}
		   ?>	  
			
		],
		sm: smmydocument,
		tbar: new Ext.PagingToolbar({
			id: 'pagingmydocuments',
            pageSize: limitmydocuments,
            store: storemydocument,
            displayInfo: true,
			displayMsg: 'Total: {2} Documents.',
			emptyMsg: "No documents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 documents mail per page.',
				text: 50,
				handler: function(){
					limitmydocuments=50;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 documents mail per page.',
				text: 80,
				handler: function(){
					limitmydocuments=80;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 documents mail per page.',
				text: 100,
				handler: function(){
					limitmydocuments=100;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
	});
	
	storemydocument.load({params:{start:0, limit:limitmydocuments}});
</script>