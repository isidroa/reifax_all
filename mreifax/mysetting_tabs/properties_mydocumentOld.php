<?php
	include("../properties_conexion.php");
	conectar();
	
	include ("../properties_getgridcamptit.php");		
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYDoc','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
?>
<div align="center" id="todo_mydocument_panel">
    <div id="mydocument_data_div" align="center" style=" margin:auto; width:100%">
        <div id="mydocument_properties"></div>
    </div>
</div>
<script>
	function urldoc(value, metaData, record, rowIndex){
		var urlamazon = record.get('urlamazon');
		var url = urlamazon ? urlamazon : value;
		
		var testA=/^https:\/\/s3.+php$/;
		if(testA.test(url)){
			url= '/includes/downs3.php?file='+url;
		}
		url=url.replace('reifax.com/docs/','reifax.com/');
		return '<a href="'+url+'" target="_blank"><img src="http://www.reifax.com/img/doc.png"/></a>';
	}
	
	var storemydocument = new Ext.data.JsonStore({
        url: 'mysetting_tabs/mydocument_tabs/getdocuments.php',
		fields: [
           <?php 
		   		echo "'ind','pid','urlamazon'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'sdate',
			direction: 'DESC' // or 'DESC' (case sensitive for local sorting)
		}
    });
	var limitmydocuments = 50;
	
	var smmydocument = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	var gridmydocument = new Ext.grid.GridPanel({
		renderTo: 'mydocument_properties',
		cls: 'grid_comparables',
		width: 980, 
		height: 2300,
		store: storemydocument,		
		columns: [
				  smmydocument,
			<?php 
		   		echo "{id: 'ind' , header: 'Ind.', width: 30, sortable: true, tooltip: 'Index Property.', dataIndex: 'ind'},{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='url')
						echo ",{header: '".$val->title."', width: 30, renderer: urldoc, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					else
						echo ",{header: '".$val->title."', sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
				}
		   ?>	  
			
		],
		sm: smmydocument,
		tbar: new Ext.PagingToolbar({
			id: 'pagingmydocuments',
            pageSize: limitmydocuments,
            store: storemydocument,
            displayInfo: true,
			displayMsg: 'Total: {2} Documents.',
			emptyMsg: "No documents to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 documents mail per page.',
				text: 50,
				handler: function(){
					limitmydocuments=50;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 documents mail per page.',
				text: 80,
				handler: function(){
					limitmydocuments=80;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 documents mail per page.',
				text: 100,
				handler: function(){
					limitmydocuments=100;
					Ext.getCmp('pagingmydocuments').pageSize = limitmydocuments;
					Ext.getCmp('pagingmydocuments').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
	});
	
	storemydocument.load({params:{start:0, limit:limitmydocuments}});
</script>