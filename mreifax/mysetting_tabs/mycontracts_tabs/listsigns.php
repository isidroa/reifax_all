<?php
/**
 * listsings.php
 *
 * Return the list of signatures associated to an user ID to show in a extJs Grid.
 * (Not in use)
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 01.04.2011
 */

include "../../properties_conexion.php"; 
conectar();

$userid  = $_COOKIE['datos_usr']["USERID"];

$query   = "SELECT * FROM xima.contracts_signature  WHERE userid = $userid";
$rs      = mysql_query($query);

if ($rs) {

	$type = '';
	
	while ($row = mysql_fetch_array($rs)) {
		
		switch ($row['type']) {
			case 1 : $type = "Buyer's Signature"; break;
			case 2 : $type = "Buyer's Initials"; break;
			case 3 : $type = "Seller's Signature"; break;
			case 4 : $type = "Seller's Initials"; break;
		}
		
		$row['typestr'] = $type;
		$data[]         = $row;
		
	}
	
	$resp = array('success'=>'true','data'=>$data);	
	
} else
	$resp = array('success'=>'true','mensaje'=>mysql_error());

// Return a Json response to get interpreted by extJs
echo json_encode($resp);

?>	