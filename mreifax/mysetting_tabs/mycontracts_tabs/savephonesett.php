<?php

	include "../../properties_conexion.php";  
	conectar();
	
	$userid = $_COOKIE['datos_usr']["USERID"];
	
	$query='SELECT * FROM xima.contracts_phonesettings WHERE userid='.$userid;
	$result = mysql_query($query) or die($query.mysql_error());
	
	if(mysql_num_rows($result)==0){
		$query='insert into xima.contracts_phonesettings (userid) VALUES ('.$userid.')';
		mysql_query($query) or die($query);
	}
	
	if(isset($_POST['sms_phone'])){
		$set="sms_phone='".addslashes($_POST['sms_phone'])."', sms_domain='".addslashes($_POST['sms_domain'])."', forward_phone='".addslashes($_POST['forward_phone'])."'";
	}else if(isset($_POST['voice_email'])){
		$set="voice_email='".addslashes($_POST['voice_email'])."'";
	}else if(isset($_POST['fax_domain'])){
		$set=" fax_domain='".addslashes($_POST['fax_domain'])."',fax_domain_receiver='".addslashes($_POST['fax_domain_receiver'])."'";
	}elseif(isset($_POST['authid'])){
		require_once('../../../plivo/plivo.php');
		try{
			$p = new RestAPI($_POST['authid'], $_POST['authtoken']);
		}catch(Exception $e){
			echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$e));
			return false;
		}
		$phone = $_POST['plivo_phone'];
		$forward = $_POST['plivo_forward'];
		
		//Validate numbers
		$phone = str_replace(array(' ','-','+','(',')'),'',$phone);
		$forward = str_replace(array(' ','-','+','(',')'),'',$forward);
		
		$phone = substr($phone,0,1)=='1' ? $phone : '1'.$phone;
		$forward = substr($forward,0,1)=='1' ? $forward : '1'.$forward;
				
		// Get Aplications Details
		$responseA = $p->get_applications();
		
		if(isset($responseA['response']['error'])){
			echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseA['response']['error']));
			return false;
		}else{
			$HasReifaxApp = false;
			$HasReifaxForward = false;
			foreach($responseA['response']['objects'] as $k=>$apps){
				if($apps['app_name']=='ReiFax App'){
					$HasReifaxApp=true;
					$app_id = $apps['app_id'];
				}elseif($apps['app_name']=='ReiFax Forward'){
					$HasReifaxForward=true;
					$app_forward = $apps['app_id'];
				}
			}
			
			if(!$HasReifaxApp){
				// Create Application
				$params = array(
					'app_name' => 'ReiFax App',
					'answer_url' => 'http://www.reifax.com/plivo/answer_url.php?plivoNum='.$phone,
					'answer_method' => 'POST',
					'hangup_url' => 'http://www.reifax.com/plivo/hangup_url.php',
					'hangup_method' => 'POST',
					'default_endpoint_app' => true
				);
				$responseA = $p->create_application($params);
				
				if(isset($responseA['response']['error'])){
					echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseA['response']['error']));
					return false;
				}else
					$app_id = $responseA['response']['app_id'];
			}else{
				// Modify Application
				$params = array(
					'app_id' => $app_id,
					'answer_url' => 'http://www.reifax.com/plivo/answer_url.php?plivoNum='.$phone,
					'default_endpoint_app' => true
				);
				$responseA = $p->modify_application($params);
				
				if(isset($responseA['response']['error'])){
					echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseA['response']['error']));
					return false;
				}
			}
		
			if(!$HasReifaxForward){
				// Create Application
				$params = array(
					'app_name' => 'ReiFax Forward',
					'answer_url' => 'http://www.reifax.com/plivo/forward_url.php?forwardNum='.$forward,
					'answer_method' => 'POST',
					'hangup_url' => 'http://www.reifax.com/plivo/hangup_url.php',
					'hangup_method' => 'POST',
					'default_number_app' => true
				);
				$responseA = $p->create_application($params);
				
				if(isset($responseA['response']['error'])){
					echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseA['response']['error']));
					return false;
				}else
					$app_forward = $responseA['response']['app_id'];
			}else{
				// Modify Application
				$params = array(
					'app_id' => $app_forward,
					'answer_url' => 'http://www.reifax.com/plivo/forward_url.php?forwardNum='.$forward,
					'default_number_app' => true
				);
				$responseA = $p->modify_application($params);
				
				if(isset($responseA['response']['error'])){
					echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseA['response']['error']));
					return false;
				}
			}
		}
		
		// Get Endpoints Details
		$responseEP = $p->get_endpoints();
		
		if(isset($responseEP['response']['error'])){
			echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseEP['response']['error']));
			return false;
		}else{
			$HasReifaxEndpoint = false;
			$endpoint_user='';
			$endpoint_pass='';
			foreach($responseEP['response']['objects'] as $k=>$endpoints){
				if($endpoints['alias']=='ReiFax Endpoint'){
					$HasReifaxEndpoint=true;
					$endpoint_id = $endpoints['endpoint_id'];
					$endpoint_user = $endpoints['username'];
					$endpoint_pass = 'reifax';
				}
			}
			
			if(!$HasReifaxEndpoint){
				// Create Endpoint
				$params = array(
					'app_id' => $app_id,
					'username' => 'ReiFaxUser',
					'password' => 'reifax',
					'alias' => 'ReiFax Endpoint'
				);
				$responseEP = $p->create_endpoint($params);
				
				if(isset($responseEP['response']['error'])){
					echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$responseEP['response']['error']));
					return false;
				}else{
					$endpoint_id = $responseEP['response']['endpoint_id'];
					$endpoint_user = $responseEP['response']['username'];
					$endpoint_pass = 'reifax';
				}
			}
		}
		
		// Link application to number
		$params = array(
			'number' => $phone,
			'app_id' => $app_forward,
		);
		$response = $p->link_application_number($params);
		
		if(isset($response['response']['error'])){
			echo json_encode(array('success'=>'true','savesett'=>'no','mensaje'=>$response['response']['error']));
			return false;
		}
				
		$set=" plivo_authid='".mysql_real_escape_string($_POST['authid'])."',plivo_authtoken='".mysql_real_escape_string($_POST['authtoken'])."',plivo_ep_user='".mysql_real_escape_string($endpoint_user)."',plivo_ep_pass='".mysql_real_escape_string($endpoint_pass)."',plivo_phone='".mysql_real_escape_string($phone)."',plivo_forward='".mysql_real_escape_string($forward)."'";
	}else{
		$set='skype_enable='.(isset($_POST['skype_enable']) ? 0 : 1);
		$skypeCallEnabled = isset($_POST['skype_enable']);
	}
	
	$query  = "UPDATE xima.contracts_phonesettings SET $set WHERE userid=$userid";
	if (mysql_query($query))
		$resp = array('success'=>'true','savesett'=>'yes','mensaje'=>'Settings saved!');
	else
		$resp = array('success'=>'true','savesett'=>'no','mensaje'=>$query.' '.mysql_error());
						
	echo json_encode($resp);
?>