<?php
	/**
	* saveaddons.php
	*
	* Save the addons of a document.
	* 
	* @autor   Alex Barrios <alexbariv@gmail.com>
	* @version 07.04.2011
	*/

	include('c:/inetpub/wwwroot/mant/classes/connection.class.php');
	include('c:/inetpub/wwwroot/mant/classes/globalReifax.class.php');
	$Connect=	new ReiFax(
		array(
			'downloadFiles'	=>	FALSE
		)
	);
	$_SERVER['REIFAX_DOCUMENT_ROOT']=$Connect->documentRoot;
	include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
	$Errors	=	new managerErrors(
		array(
			'downloadFiles'	=>	FALSE
		)
	);

include_once("../../properties_conexion.php"); 
if(!mysql_ping()){
	conectar();
}
$path=$_SERVER['REIFAX_DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs';
// Allowed images
$mensaje='';
$error='';
$userid    = isset($_POST['userBackOffice'])	?	$_POST['userBackOffice']	:	$_COOKIE['datos_usr']["USERID"];

// ELIMINACION DE CONTRATOS Y ADICIONALES DOCUMENTOS
if($_GET['cmd'] == 'del') 
{

	$query = "SELECT id,document,place,type FROM xima.contracts_addonscustom
		WHERE userid = $userid AND type = $_GET[id]";
	$result      = mysql_query($query) or die($query.mysql_error());
	while($row = mysql_fetch_array($result))
	{
		$_POST['userBackOffice']=	$userid;
		$_GET['addon']			=	$row['place'];
		$_GET['type_a']			=	$row['type'];
		ob_start();
		require($_SERVER['DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs/saveaddons.php');
		$tempContent = ob_get_contents();
		ob_end_clean();
		unset($_POST['userBackOffice']);
		unset($_GET['addon']);
		unset($_GET['type_a']);
		if($tempContent!="Addon deleted")
			die(json_encode(array("success"=>false)));
	}

	// eliminando contratos 
	$query = "DELETE FROM xima.contracts_custom
				WHERE userid = $userid AND id = {$_GET['id']}";
	@mysql_query($query);	

	if($filename<>''){
		unlink( $path.'/template_upload/'.$filename);
	}
	
	echo 'Contract deleted';
	return;
} 
else 
{
	if ($_FILES["pdfupload"]['name']!='') {
		$_POST['pdfname']	=	trim($_POST['pdfname']);
		$query	=	"SELECT * FROM xima.contracts_custom WHERE name='$_POST[pdfname]' AND userid='$userid'";
		mysql_query($query);
		if(mysql_affected_rows()<=0){
			$filename  = str_replace(' ', '_', $_FILES["pdfupload"]['name']);
			$extension = strtolower(strrchr($filename,'.'));
			
			if($extension != '.pdf') {
				$extension = '';
				$mensaje   = 'The uploaded file its not a valid PDF';
		
			}
			
			// No errors were found?
			if($error == "") {
				
				$newFileName = "$userid-".date("YmdHis")."-$filename";
				
				if (!move_uploaded_file($_FILES["pdfupload"]['tmp_name'], $path.'/template_upload/'.$newFileName)) {
					$mensaje .= "Fallo al copiar. $newFileName";
					$newFileName = '';
				} else {
					
					copy($path.'/template_upload/'.$newFileName, $path."/temp_change_contract/".$newFileName);
					unlink($path.'/template_upload/'.$newFileName);
					copy($path."/temp_change_contract/".$newFileName, $path.'/template_upload/'.$newFileName);
					unlink($path."/temp_change_contract/".$newFileName);				

					$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios
					
					system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.16/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -sOutputFile=\"$path/template_upload/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/template_upload/$newFileName\"", $valid);
					
					if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						unlink($path.'/template_upload/'.$newFileName);
						chmod($path.'/template_upload/temp_by_jesus'.$id_name.'.pdf',0777);
						copy($path.'/template_upload/temp_by_jesus'.$id_name.'.pdf', $path.'/template_upload/'.$newFileName);
						unlink($path.'/template_upload/temp_by_jesus'.$id_name.'.pdf');
						$query = "INSERT INTO xima.contracts_custom (userid, name, filename, date_upload, type) 
								  VALUES ($userid,'{$_POST['pdfname']}','$newFileName',now(),'N')";
				
						if (mysql_query($query)){
							$mensaje = "Changes Successfully Applied!";
						}

					}else{
						$mensaje .= "Contract Can't be Uploaded!";
						$newFileName = '';
						@unlink('template_upload/'.$newFileName);
					}
					@unlink($_FILES["pdfupload"]['tmp_name']);
				}
			}
		}else{
			die(json_encode(array('success'=>'false','mensaje'=>"Please try with another document name, $_POST[pdfname], already exist.")));
		}
	} else {
		
		if ($_POST['option1'] == 'on')
			$active = 1;
		if ($_POST['option2'] == 'on')
			$active = 2;	
			
		$query = "UPDATE xima.contracts_custom 
			SET tpl1_name		=	'$_POST[name1]', 
				tpl1_addr		=	'$_POST[address11]', 
				tpl1_addr2		=	'$_POST[address12]', 
				tpl1_addr3		=	'$_POST[address13]', 
				tpl1_type		=	'$_POST[cOptionType1]', 
				tplactive		=	'$active', 
				tpl2_name		=	'$_POST[name2]', 
				tpl2_addr		=	'$_POST[address21]',
				tpl2_addr2		=	'$_POST[address22]',
				tpl2_addr3		=	'$_POST[address23]',
				tpl2_type		=	'$_POST[cOptionType2]', 
				inspectionDays	=	$_POST[inspectionDays]
			WHERE id = '$_POST[type]' 
				  AND userid = '$userid'";
	
		if (mysql_query($query)){
			$mensaje = "Changes Successfully Applied!";
		}
	
	}
	
	if ($mensaje != "") 
		$resp = array('success'=>'true','mensaje'=>$mensaje);
	
	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);
}

?>	