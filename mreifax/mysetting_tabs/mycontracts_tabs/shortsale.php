<?php
include "../../properties_conexion.php";
conectar();

$userid    = $_COOKIE['datos_usr']["USERID"];

// Get all the images order by signature type
// the type its from 1 to 8, the first 4 elements are buyer's and 
// the second 4 elementes are for seller's
/*$query     = "SELECT * FROM xima.shortsale_company  
				WHERE userid = $userid ";
$rs        = mysql_query($query);
if ($rs) {
	while ($row = mysql_fetch_array($rs)) {
		
	}
}*/

?>

<div id="myShortID" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<input type="hidden" name="_lookup" id="_lookup">


<script type="text/javascript">
	var storeagent 		= new Ext.data.JsonStore({
		autoLoad		: true,
		url				: 'mysetting_tabs/mycontracts_tabs/myshortsales.php?type=getcompanys',
		fields			: [
			{name		: 'id', 	type: 'int'},
			{name		: 'userid', 	type: 'int'},
			{name		: 'name'},
			{name		: 'address'},
			{name		: 'phone'}
		],
		root			: 'records',
		totalProperty	: 'total'
	});
	
	var smagent = new Ext.grid.CheckboxSelectionModel({
		checkOnly	: true, 
		width		: 25
	});
	
	function phoneRender(value, metaData, rec, rowIndex, colIndex, store) {
		var num = colIndex-3;
		var type = 'typeph'+num;
		var tyvalue = rec.get(type);
		var classvalue = '';
		
		if(tyvalue == 0) 
			classvalue = 'home';
		else{ 
			if(tyvalue == 1) 
				classvalue = 'office';
			else 
				classvalue = 'cell';
		}
		if(value.length == 0) classvalue = '';
		
		return '<div class="'+classvalue+'">'+value+'</div>';
	}
	
	function agentPrincipal(value, metaData, rec, rowIndex, colIndex, store){
		var principal = rec.get('principal');
		var classvalue = '';
		
		if(principal==1){
			classvalue = 'agentprincipal';
		}
		
		return '<div class="'+classvalue+'">'+value+'</div>';  
	}
	
	var gridagent 		= new Ext.grid.GridPanel({
		cls				: 'grid_comparables',
		width			: 720,
		height			: 190,
		store			: storeagent,
		stripeRows		: true,
		sm				: smagent, 
		columns			: [
			smagent,
			{
				header		: 'Name', 
				width		: 200, 
				sortable	: true, 
				tooltip		: 'Name', 
				dataIndex	: 'name'
			},{
				header		: 'Address', 
				width		: 300, 
				sortable	: true, 
				tooltip		: 'Address', 
				dataIndex	: 'address'
			},{
				header		: 'Phone', 
				width		: 200, 
				sortable	: true, 
				tooltip		: 'Phone', 
				dataIndex	: 'phone'
			}			
		]
	});
	
	var tbagent = new Ext.Toolbar({
		items: [
			' ',
			{
				tooltip: 'Remove Company',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler	: function (){
					if(smagent.getCount()==0 || smagent.getCount()>1){ 
						Ext.Msg.alert("Short Sale", 'You must previously select(check) only one company in grid to be removed.');
						return false;
					}
					
					var agent = smagent.getSelected();												
					var agentid = agent.get('id');
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Removing...',
						url: 'mysetting_tabs/mycontracts_tabs/myshortsales.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type	: 'delete',
							agentid	: agentid,
							userid	: agent.get('userid'),
							idcompany: agent.get('id')
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							Ext.MessageBox.alert("Short Sale",'Removed sucessfully.');
							storeagent.load();
						}                                
					});
				}
			},
			'-',
			new Ext.Button({
				tooltip: 'New Company',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/mycontracts_tabs/myshortsales.php',
						frame: true,
						title: 'Add Short Sale',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 230},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'name',
									fieldLabel: 'Name',
									allowBlank: false
								},{
									xtype     : 'textfield',
									name      : 'address',
									fieldLabel: 'Address'
								},{
									xtype     : 'textfield',
									name      : 'phone',
									fieldLabel: 'Phone'
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'insert'
								}],
						
						buttons: [{
								text: 'Insert',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Short Sale", 'New Short Sale company');
											storeagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", "ERROR");
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 340,
						height      : 250,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			}),
			new Ext.Button({
				tooltip: 'Edit Company',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/update.png',
				handler: function(){
					if(smagent.getCount()==0 || smagent.getCount()>1){ 
						Ext.Msg.alert("Follow Agent", 'You must previously select(check) only one company in grid to be edited.');
						return false;
					}
					
					var agent = smagent.getSelected();
					
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/mycontracts_tabs/myshortsales.php',
						frame: true,
						title: 'Short Sale',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 230},
						labelAlign: 'left',
						items: [{
									xtype     : 'textfield',
									name      : 'name',
									fieldLabel: 'Name',
									allowBlank: false,
									value	  : agent.get('name')
								},{
									xtype     : 'textfield',
									name      : 'address',
									fieldLabel: 'Address',
									value	  : agent.get('address')
								},{
									xtype     : 'textfield',
									name      : 'phone',
									fieldLabel: 'Phone',
									value	  : agent.get('phone')
								},{
									xtype     : 'hidden',
									name      : 'type',
									value     : 'update'
								},{
									xtype     : 'hidden',
									name      : 'idcompany',
									value     : agent.get('id')
								}],
						
						buttons: [{
								text: 'Update',
								handler: function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Short Sale", 'Updated Company.');
											storeagent.load();
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", "ERROR");
										}
									});
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 340,
						height      : 250,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
								loading_win.hide();
							}
						}]
					});
					win.show();
				}
			})
		]
	});
	
	var winagent 	= new Ext.Window({
		title		: 'Agent',
		layout      : 'fit',
		width       : 950,
		height      : 200,
		modal	 	: true,
		plain       : true,
		items		: gridagent,
		closeAction : 'close',
		tbar		: tbagent,
		
		buttons: [{
			text     : 'Close',
			handler  : function(){
				winagent.close();
			}
		}]
	});
	
	//winagent.show();
	myShortForm = new Ext.FormPanel({
		frame      : true,
		autoWidth  : true,
		fileUpload : true,
		method     : 'POST',
		bodyStyle  : 'padding-left:100px;text-align:left;',
		id         : 'formSig',
		name       : 'formSig',
		items      : gridagent,
		tbar		: tbagent
	})
	myShortForm.render('myShortID');
</script>
