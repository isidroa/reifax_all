<?php
/**
 * saveaddons.php
 *
 * Save the addons of a document.
 * 
 * @autor   Jesus Marquez <jesusemarquezz@gmail.com>
 * @version 29.10.2014
 */
include "../../properties_conexion.php"; 
conectar();

$userid  = $_COOKIE['datos_usr']["USERID"];

// Delete addons and end execution if the command its recieve
if (strlen($_GET['scrow']) and $_GET['cmd'] == 'del') {
	
	$query = "DELETE FROM xima.contracts_scrow
				WHERE userid = $userid AND place = {$_GET['scrow']}";
	mysql_query($query);	
	
	echo 'Escrow deleted';
	die();
	
} else {
	$resp='';
	// Check the form fields
	for ($i = 1 ; $i <=2 ; $i ++) {
		// Delete the row of the previous image and insert a new one
		$query = "SELECT * FROM xima.contracts_scrow WHERE userid = $userid AND place = $i";
		$result= mysql_query($query);
		if(mysql_num_rows($result)>0){
			$query	=	"
				UPDATE xima.contracts_scrow SET 
					escrowAgent='".trim(addslashes(str_replace("´","'",$_POST['Contract-escrowAgent'.$i])))."',
					escrowAddress='".trim(addslashes(str_replace("´","'",$_POST['Contract-escrowAddress'.$i])))."',
					imagen='".trim($_POST['scrowOption'.$i] == 'on'	?	'1'	:	'0')."',
					escrowEmail='".trim($_POST['Contract-escrowEmail'.$i])."',
					escrowPhone='".trim($_POST['Contract-escrowPhone'.$i])."',
					escrowFax='".trim($_POST['Contract-escrowFax'.$i])."' 
				WHERE userid = $userid and place='$i'";
			if(mysql_query($query)){
				$resp = array('success'=>'true','mensaje'=>'The Escrow has been updated');
			}else{
				$resp = array('success'=>'true','mensaje'=>mysql_error());
			}
		}else{
			$query = "
				INSERT INTO xima.contracts_scrow 
					(userid, place, imagen, escrowAgent, escrowAddress, escrowEmail, escrowPhone, escrowFax) 
						VALUES 
					(
						$userid,$i,
						'".trim($_POST['scrowOption'.$i] == 'on'	?	'1'	:	'0')."',
						'".addslashes(str_replace("´","'",trim($_POST['Contract-escrowAgent'.$i])))."',
						'".addslashes(str_replace("´","'",trim($_POST['Contract-escrowAddress'.$i])))."',
						'".trim($_POST['Contract-escrowEmail'.$i])."',
						'".trim($_POST['Contract-escrowPhone'.$i])."',
						'".trim($_POST['Contract-escrowFax'.$i])."')
				";
			if (mysql_query($query)){
				$resp = array('success'=>'true','mensaje'=>'Changes saved successfully.');
			}else{
				$resp = array('success'=>'true','mensaje'=>mysql_error());
			}
		}
	}
	echo json_encode($resp);
}

?>	