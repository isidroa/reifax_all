<?php
	/**
	* saveaddons.php
	*
	* Save the addons of a document.
	*
	* @autor   Alex Barrios <alexbariv@gmail.com>
	* @version 07.04.2011
	*/

	include('c:/inetpub/wwwroot/mant/classes/connection.class.php');
	include('c:/inetpub/wwwroot/mant/classes/globalReifax.class.php');
	$Connect=	new ReiFax(
		array(
			'downloadFiles'	=>	FALSE
		)
	);
	$_SERVER['REIFAX_DOCUMENT_ROOT']=$Connect->documentRoot;
	include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
	$Errors	=	new managerErrors(
		array(
			'downloadFiles'	=>	FALSE,
			'AP'=>'SAVE-ADDONS'
		)
	);

include_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
if(!mysql_ping()){
	conectar();
}
$path=$_SERVER['REIFAX_DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs';
// Allowed images
$error   = "";
$userid    = isset($_POST['userBackOffice'])	?	$_POST['userBackOffice']	:	$_COOKIE['datos_usr']["USERID"];
$type    = $_POST['type_a'];

// Delete addons and end execution if the command its recieve
if (isset($_GET['cmd']) and $_GET['cmd'] == 'del') {//strlen($_GET['addon']) and

	$query = "SELECT id,document,CONCAT(id,'-',userid,'-',place) AS idTemplate FROM xima.contracts_addonscustom
				WHERE userid = $userid AND place = {$_GET['addon']} AND type = {$_GET['type_a']}";
	$rscr      = mysql_query($query) or die($query.mysql_error());
	$rowcr = mysql_fetch_array($rscr);
	$document	= $rowcr['document'];
	$idTemplate = $rowcr['idTemplate'];
	if(!empty($document)){
		unlink($path.'/addons/'.$document);
	}

	$query = "DELETE FROM xima.contracts_addonscustom
				WHERE userid = $userid AND place = {$_GET['addon']} AND type = {$_GET['type_a']}";

	if(@mysql_query($query)){
		$query = "DELETE FROM xima.contracts_campos WHERE camp_contract_id='$idTemplate'";
		@mysql_query($query);
	}

	echo "Addon deleted";
	return;
} else {
	$errors	=	array();
	// Check the form fields
	for ($i = 1 ; $i <=6 ; $i ++) {

		// ---
		$addon_act  = isset($_POST["addon_act".$i]) and $_POST["addon_act".$i] == 'on' ? '1' : '0';
		$addon_name = $_POST["addon_name".$i];
		$sql        = '';

		$newFileName='';
		if ($_FILES["addo".$i]['name']!='') {

			$filename = $_FILES["addo".$i]['name'];

			if (strlen($addon_name)==0)
				$addon_name = $filename;

			$extension = strtolower(strrchr($filename,'.'));

			if($extension != '.pdf') {
				$extension = '';
				$error     = 'The uploaded file its not a valid PDF';

			}
			// No errors were found?
			if($error == "") {

				$newFileName = "$type-$userid-"."$i".$extension;

				//@unlink($path.'/addons/'.$newFileName);

				if (!move_uploaded_file($_FILES["addo{$i}"]['tmp_name'], $path.'/addons/'.$newFileName)) {
					$error .= "Fallo al copiar. $newFileName";
					$newFileName = '';
				}else{
					copy($path.'/addons/'.$newFileName, $path."/temp_change_contract/".$newFileName);
					unlink($path.'/addons/'.$newFileName);
					copy($path."/temp_change_contract/".$newFileName, $path.'/addons/'.$newFileName);
					unlink($path."/temp_change_contract/".$newFileName);

					$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios
					system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.16/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -sOutputFile=\"$path/addons/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/addons/$newFileName\"", $valid);

					if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						unlink($path.'/addons/'.$newFileName);
						chmod($path.'/addons/temp_by_jesus'.$id_name.'.pdf',0777);
						copy($path.'/addons/temp_by_jesus'.$id_name.'.pdf', $path.'/addons/'.$newFileName);
						unlink($path.'/addons/temp_by_jesus'.$id_name.'.pdf');
					}else{
						$error .= 'Contract Can\'t be Uploaded!';
						unlink($path.'addons/'.$newFileName);
						$newFileName = '';
					}
					unlink($_FILES["addo{$i}"]['tmp_name']);
				}
			}
		}

		$query = "SELECT id FROM xima.contracts_addonscustom
			WHERE userid = $userid AND place = $i AND type = $type";
		$rs = mysql_query($query);

		if (mysql_num_rows($rs)>0) {

			if ($newFileName != ''){
				$sql = ",document = '$newFileName'";
			}

			$query = "UPDATE xima.contracts_addonscustom
						SET
							addon_act = '$addon_act',
							addon_name = '$addon_name',
							emd= '".(isset($_POST['emd-'.$i])	?	'1'	:	'0')."',
							pof= '".(isset($_POST['pof-'.$i])	?	'1'	:	'0')."',
							`add`= '".(isset($_POST['add-'.$i])	?	'1'	:	'0')."'
							$sql
	                    WHERE userid = $userid AND place = $i AND type = $type";

		} else {

			if (!empty($addon_name) and !empty($newFileName)) {
				$query	=	"SELECT *  FROM xima.contracts_addonscustom WHERE userid='$userid' AND type='$type' AND addon_name='$addon_name'";
				mysql_query($query);
				if(mysql_affected_rows()<=0){
					$query	=	"
						INSERT INTO xima.contracts_addonscustom (userid, type, place, addon_act, addon_name, document)
						VALUES ($userid,$type,$i,'$addon_act','$addon_name','$newFileName')";
				}else{
					@unlink($path.'/addons/'.$newFileName);
					$errors[]	=	$addon_name;
				}
			}
		}

		$newFileName = "";	///Limpio la Variable para que no Edite los DEmas Addons con el Ultimo Nombre ADDED BY JESUS

		if (mysql_query($query))
			$resp = array('success'=>'true','mensaje'=>'Settings saved successfully');
		else
			$resp = array('success'=>'true','mensaje'=>mysql_error()." $query $_POST");

	}

	if($error != "")
		die(
			json_encode(
				array(
					'success'=>'true',
					'mensaje'=>$error
				)
			)
		);

	if(count($errors)>0)
		die(
			json_encode(
				array(
					'success'=>'true',
					'mensaje'=>"This adicional documents cant be uploaded ".(count($errors)==1	?	"because its name already exist: "	:	"because their names already exist: ").implode(", ",$errors)
				)
			)
		);

	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);

}
?>
