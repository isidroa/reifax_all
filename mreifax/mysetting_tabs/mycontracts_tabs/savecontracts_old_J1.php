<?php
/**
 * saveaddons.php
 *
 * Save the addons of a document.
 * 
 * @autor   Alex Barrios <alexbariv@gmail.com>
 * @version 07.04.2011
 */

include_once "../../properties_conexion.php"; 
conectar();
$path='C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs';
// Allowed images
$mensaje = "";
$userid  = $_COOKIE['datos_usr']["USERID"];

// ELIMINACION DE CONTRATOS Y ADICIONALES DOCUMENTOS
if($_GET['cmd'] == 'del') 
{
	$query = "SELECT id,filename FROM xima.contracts_custom
				WHERE userid = $userid AND id = {$_GET['id']}";
	$rscr      = mysql_query($query) or die($query.mysql_error());
	$rowcr = mysql_fetch_array($rscr);
	$filename = $rowcr['filename'];
	$idcont = $rowcr['id'];


	$query = "SELECT id,document FROM xima.contracts_addonscustom
				WHERE userid = $userid AND type = $idcont";
	$rscr      = mysql_query($query) or die($query.mysql_error());
	while($rowcr = mysql_fetch_array($rscr))
	{
		$document = $rowcr['document'];
		if($document<>'') unlink($path.'/addons/'.$document);
	
	}
	// eliminando los addons del contrato a eliminar
	$query = "DELETE FROM xima.contracts_addonscustom
				WHERE userid = $userid AND type = {$_GET['id']}";
	@mysql_query($query);	

	// eliminando contratos 
	$query = "DELETE FROM xima.contracts_custom
				WHERE userid = $userid AND id = {$_GET['id']}";
	@mysql_query($query);	

	if($filename<>'') unlink( $path.'/template_upload/'.$filename);
	
	echo "Contract deleted";
	return;
} 
else 
{
	
	if ($_FILES["pdfupload"]['name']!='') {
		
		$filename  = str_replace(' ', '_', $_FILES["pdfupload"]['name']);
		$extension = strtolower(strrchr($filename,'.'));
		
		if($extension != '.pdf') {
			$extension = '';
			$mensaje   = "The uploaded file its not a valid PDF";
	
		}
		
		// No errors were found?
		if($error == "") {
			
			$newFileName = "$userid-"."$filename";
			
			if (!move_uploaded_file($_FILES["pdfupload"]['tmp_name'], $path.'/template_upload/'.$newFileName)) {
				$mensaje .= "Fallo al copiar. $newFileName";
				$newFileName = '';
			} else {
				@unlink($_FILES["pdfupload"]['tmp_name']);
				
				$query = "INSERT INTO xima.contracts_custom (userid, name, filename, date_upload, type) 
						  VALUES ($userid,'{$_POST['pdfname']}','$newFileName',now(),'N')";
		
				if (mysql_query($query))
					$mensaje = "Contract saved successfully!";
	
			}
		}
		
	} else {
		
		if ($_POST['option1'] == 'on')
			$active = 1;
		if ($_POST['option2'] == 'on')
			$active = 2;	
			
		$query = "UPDATE xima.contracts_custom 
					SET tpl1_name = '{$_POST['name1']}', 
						tpl1_addr = '{$_POST['address1']}', 
						tplactive = '$active', 
						tpl2_name = '{$_POST['name2']}', 
						tpl2_addr = '{$_POST['address2']}'
					WHERE id = '{$_POST['type']}' 
						  AND userid = '$userid'";
	
		if (mysql_query($query))
			$mensaje = "Contract saved successfully!";
	
	}
	
	if ($mensaje != "") 
		$resp = array('success'=>'true','mensaje'=>$mensaje);
	
	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);
}

?>	