<?php
	/**
	* savesings.php
	*
	* Save the signature image sendit by the upload form
	* 
	* @autor   Alex Barrios <alexbariv@gmail.com>
	* @version 01.04.2011
	*/

	include('c:/inetpub/wwwroot/mant/classes/connection.class.php');
	include('c:/inetpub/wwwroot/mant/classes/globalReifax.class.php');
	$Connect=	new ReiFax(
		array(
			'downloadFiles'	=>	FALSE
		)
	);
	$_SERVER['REIFAX_DOCUMENT_ROOT']=$Connect->documentRoot;
	include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
	$Errors	=	new managerErrors(
		array(
			'downloadFiles'	=>	FALSE
		)
	);
	
	include_once("../../properties_conexion.php");
	if(!mysql_ping()){
		conectar();
	}

	$path=$_SERVER['REIFAX_DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs/signatures/';

// Allowed images
$resp   = array('success'=>true,'mensaje'=>'No Change');
$mime   = array('image/gif' => 'gif', 'image/jpeg' => 'jpeg', 'image/png' => 'png');
$error  = "";
$userid	= isset($_POST['userBackOffice'])	?	$_POST['userBackOffice']	:	$_COOKIE['datos_usr']["USERID"];


// Delete addons and end execution if the command its recieve
if (strlen($_GET['type'])>0 and $_GET['cmd'] == 'del') {
	
	$type = addslashes($_GET['type']);
	
	$query = "DELETE FROM xima.contracts_signature
				WHERE userid = $userid AND type = $type";
	@mysql_query($query);	
	
	echo "Signature deleted ";
	die();
	
} else {

	// Check the form fields
	for ($i == 1 ; $i <=4 ; $i ++) {
	
		if ($_FILES["image{$i}"]['name']!='') {
			$fileInfo = getimagesize($_FILES["image{$i}"]['tmp_name']);
		
			 // No Image?
			if ( empty($fileInfo) )
				$error .= "The uploaded file doesnt seem to be an image. ";
			else {
			// Check Image
				$fileMime = $fileInfo['mime'];
				$extension = ($mime[$fileMime] == 'jpeg') ? 'jpg' : $mime[$fileMime];
				
				if(!$extension) {
					$extension = '';
					$error     = "No valid image file!";
				}
			}
			// No errors were found?
			if($error == "") {
				
				$newFileName = "$userid-"."$i.".$extension;
				
				list($width, $height) = getimagesize($_FILES["image{$i}"]['tmp_name']);
				
				if ($extension == 'png')
					$src = imagecreatefrompng($_FILES["image{$i}"]['tmp_name']);
				
				if ($extension == 'jpeg' or $extension == 'jpg')
					$src = imagecreatefromjpeg($_FILES["image{$i}"]['tmp_name']);
					
				if ($extension == 'gif')
					$src = imagecreatefromgif($_FILES["image{$i}"]['tmp_name']);
				
				$newwidth = $width * 55 / $height;
				
				$tmp = imagecreatetruecolor($newwidth,55);
				
				imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,55,$width,$height);
				
				
				if ($extension == 'png')
					$src = imagepng($tmp,$_FILES["image{$i}"]['tmp_name']);
				
				if ($extension == 'jpeg' or $extension == 'jpg')
					$src = imagejpeg($tmp,$_FILES["image{$i}"]['tmp_name']);
					
				if ($extension == 'gif')
					$src = imagegif($tmp,$_FILES["image{$i}"]['tmp_name']);
				
				
				@unlink($path.$newFileName);
				
				if (!move_uploaded_file($_FILES["image{$i}"]['tmp_name'], $path.$newFileName))
					$error .= 'Fallo al copiar.';
				else {
	
					// Delete (if) the previous image and copy the new
					@unlink($_FILES["image{$i}"]['tmp_name']);
							
					// Delete the row of the previous image and insert a new one
					$query = "DELETE FROM xima.contracts_signature 
								WHERE userid = $userid AND type = $i";
					@mysql_query($query);	
				
					$query = "INSERT INTO xima.contracts_signature (userid, imagen, type) 
							  VALUES ($userid,'$newFileName',$i)";
				
					if (mysql_query($query))
						$resp = array('success'=>'true','mensaje'=>'The image has been uploaded');
					else
						$resp = array('success'=>'true','mensaje'=>mysql_error());
					
				}
			}
		}
	}
	
	if ($error != "") 
		$resp = array('success'=>'true','mensaje'=>$error);
	
	// Return a Json response to get interpreted by extJs
	echo json_encode($resp);
	
}

?>	