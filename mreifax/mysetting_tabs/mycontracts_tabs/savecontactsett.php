<?php
/**
 * savecontactsett.php
 *
 * Save the get contact settings
 * 
 * @autor   Guillermo Vera G <guilleverag@gmail.com>
 * @version 25-10-2013
 */

include "../../properties_conexion.php"; 
conectar();

$userid 	= $_POST["userid"];
$block 		= $_POST["rb-block"];
$overwrite 	= $_POST["rb-overwrite"];
$premium 	= $_POST["rb-premium"];

$query='UPDATE xima.contracts_contactsettings
SET block = '.$block.', overwrite='.$overwrite.', premium='.$premium.' 
WHERE userid='.$userid;

if (mysql_query($query))
	$resp = array('success'=>'true','mensaje'=>'Settings saved!');
else
	$resp = array('success'=>'true','mensaje'=>$query.' '.mysql_error());
					
echo json_encode($resp);
?>	