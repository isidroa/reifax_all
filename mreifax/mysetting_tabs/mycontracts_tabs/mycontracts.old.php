<?php 
	include("../../properties_conexion.php"); 
	conectar();
	$userid= $_COOKIE['datos_usr']["USERID"];
	$query='SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
	$result = mysql_query($query) or die($query.mysql_error());
	$datos_usr=mysql_fetch_array($result);
	
	
?>
<style>
	.x-form-file-wrap {
		position: relative;
		height: 22px;
		width: 210px;
	}
	.x-form-file-wrap .x-form-file {
		position: absolute;
		right: 0;
		-moz-opacity: 0;
		filter:alpha(opacity: 0);
		opacity: 0;
		z-index: 2;
		height: 22px;
	}
	.x-form-file-wrap .x-form-file-btn {
		position: absolute;
		right: 0;
		z-index: 1;
	}
	.x-form-file-wrap .x-form-file-text {
		position: absolute;
		left: 0;
		z-index: 3;
		color: #777;
	}
</style>
<script type="text/javascript" src="FileUploadField.js"></script>
<div id="formulario1" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>
<script>

function getTemplate(){
	 var idcontract = Ext.getCmp('ctype1').getValue();
	 var arrayContratos = ['ContractPurchase','leadBasedPaint','ResidentialContract'];
	 var url='http://www.ximausa.com/overview_contract/'+arrayContratos[idcontract]+'.pdf';
	
	window.open(url); 
}

var contracts=new Ext.data.SimpleStore({
										fields: ['id','name'],
											data  : [
											  ['0','As is Sales and Purchase Far Bar contract'],
											  ['1','Lead_Base contract'],
											  ['2','Far Bar Sales and Purchase contract']
											]
									});
formulario = new Ext.FormPanel({
				frame:true,
				fileUpload:	true,
				method:'POST',
				bodyStyle:'padding:10px 10px 0;text-align:left;',
				id: 'formul',
				name: 'formul',
				items:[{
						xtype: 'combo',
						name: 'ctype1',
						id: 'ctype1',
						hiddenName	:'type',
						fieldLabel: '<b>Contract Type</b>',
						typeAhead: true,
						store: contracts,
						triggerAction: 'all',
						mode: 'local',
						editable: false,
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank: false,
						displayField:'name',
						valueField: 'id',
						value:'0',
						width: 300,
						listeners: {
							 'select': buscar
						}
					},{
						xtype:'spacer',
						height: 10
					},{
						xtype: 'panel',
						id:'panelContractEdit',
						layout:'form',
						items: [{
								xtype: 'label',
								id:'labelinfo',
								text: 'The default name is and the Contract Template is ',
								align: 'center',
								style: {
									marginBottom: '10px'
								}
							},{
								xtype:'spacer',
								height: 10
							},{
								width: 50,
								id:'filter',
								xtype:'button',
								text:'Get Template',
								handler: getTemplate
							},{
								xtype:'panel',
								layout:'table',
								defaults: {
									// applied to each contained panel
									bodyStyle:'padding:5px; marginLeft: 10px'
								},
								layoutConfig: {
									// The total column count must be specified here
									columns: 4
								},
								items: [{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items:[{
												xtype:'checkbox',
												fieldLabel:'Option 1',
												id:'option1',
												name:'option1'
										  },{	xtype:'hidden',
										  		name:'id1',
												id:'id1'
											}],
									rowspan: 2
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items: [{
												xtype: 'textfield',
												id: 'name1',
												fieldLabel:'Name',
												name: 'name1',
												width: '200',
												allowBlank:true,
												disabled:false,
												style: {
													width: '95%',
													marginBottom: '10px'
												}
											},{
												xtype: 'textfield',
												id: 'address1',
												fieldLabel:'Address',
												name: 'address1',
												width: '200',
												allowBlank: true,
												disabled: false,
												style: {
													width: '95%',
													marginBottom: '10px'
												}
											}],
									rowspan: 2	
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items:[{
												xtype:'label',
												text:'',
												fieldLabel:'Template',
												id:'template1',
												name:'template1'
										  },{
												xtype:'checkbox',
												fieldLabel:'Active',
												id:'activetemplate1',
												name:'activetemplate1'
										  }]	
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									width: 300,
									labelWidth: 50,
									items: [{
                                                        xtype       :'fileuploadfield',
                                                        id          :'form-file1',
                                                        emptyText   :'Select pdf',
                                                        fieldLabel  :'Pdf',
                                                        name        :'pdf1',
                                                        buttonCfg   :{
                                                                        text    :'Browse...',
																		width   :15
                                                                    }
                                                    }],
									rowspan: 2
								}]

							},{
								xtype:'panel',
								layout:'table',
								defaults: {
									// applied to each contained panel
									bodyStyle:'padding:5px; marginLeft: 10px'
								},
								layoutConfig: {
									// The total column count must be specified here
									columns: 4
								},
								items: [{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items:[{
												xtype:'checkbox',
												fieldLabel:'Option 2',
												id:'option2',
												name:'option2'
										  },{	xtype:'hidden',
										  		name:'id2',
												id:'id2'
											}],
									rowspan: 2
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items: [{
												xtype: 'textfield',
												id: 'name2',
												fieldLabel:'Name',
												name: 'name2',
												width: '200',
												allowBlank:true,
												disabled:false,
												style: {
													width: '95%',
													marginBottom: '10px'
												}
											},{
												xtype: 'textfield',
												id: 'address2',
												fieldLabel:'Address',
												name: 'address2',
												width: '200',
												allowBlank:true,
												disabled:false,
												style: {
													width: '95%',
													marginBottom: '10px'
												}
											}],
									rowspan: 2	
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									labelWidth: 60,
									items:[{
												xtype:'label',
												text:'',
												fieldLabel:'Template',
												id:'template2',
												name:'template2'
										  },{
												xtype:'checkbox',
												fieldLabel:'Active',
												id:'activetemplate2',
												name:'activetemplate2'
										  }]	
								},{
									xtype:'panel',
									border:'false',
									layout:'form',
									width: 300,
									labelWidth: 50,
									items: [{
                                                        xtype       :'fileuploadfield',
                                                        id          :'form-file2',
                                                        emptyText   :'Select pdf',
                                                        fieldLabel  :'Pdf',
                                                        name        :'pdf2',
                                                        buttonCfg   :{
                                                                        text    :'Browse...',
																		width   :15
                                                                    }
                                                    }],
									rowspan: 2
								}]

							}]
					
					}],
			buttonAlign:'center',		
			buttons     :[{
				text        :'Save',
				handler     :function(){
								if(formulario.getForm().isValid()){
									formulario.getForm().submit({
										url     :'mysetting_tabs/mycontracts_tabs/savedefaults.php',
										waitMsg :'Saving...',
										success :function(response, o){
													//buscar();
												},
										failure :function(response, o){
													//obj = Ext.util.JSON.decode(response.responseText);
													
												}
									});
								}
							}
			}]

});
formulario.render('formulario1');

function buscar(){
	Ext.Ajax.request({  
		waitMsg		:'Wait please...',
		url		:'mysetting_tabs/mycontracts_tabs/buscar.php', 
		method		:'POST', 
		params		:{ 
						idvalue	: Ext.getCmp('ctype1').getValue()
					},					
		failure		:function(response,options){
						Ext.MessageBox.alert('Warning','Error user show');
					},
					
		success		:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);						
						if(rest.succes==false){
							Ext.MessageBox.alert('Warning',rest.msg); return;
						}							
						arrtask=rest.results;
						if(arrtask.length>0){
							Ext.getCmp('name1').setValue(arrtask[0].name);
							Ext.getCmp('form-file1').setValue('');
							Ext.getCmp('address1').setValue(arrtask[0].address);
							Ext.getCmp('id1').setValue(arrtask[0].id);
							if(arrtask[0].defaultoption=='Y'){
								Ext.getCmp('option1').setValue(true);
								Ext.getCmp('labelinfo').setText('The default name is '+arrtask[0].name);
							}else{
								Ext.getCmp('option1').setValue(false);
							}
							if(arrtask[0].template=='Y'){
								Ext.getCmp('template1').setText('On');
							}else{
								Ext.getCmp('template1').setText('Off');
							}
							if(arrtask[0].activetemplate=='Y'){
								Ext.getCmp('activetemplate1').setValue(true);
							}else{
								Ext.getCmp('activetemplate1').setValue(false);
							}
						}else{
							Ext.getCmp('form-file1').setValue('');
							Ext.getCmp('form-file2').setValue('');
							Ext.getCmp('id1').setValue('');
							Ext.getCmp('name1').setValue('');
							Ext.getCmp('address1').setValue('');
							Ext.getCmp('option1').setValue(false);
							Ext.getCmp('template1').setText('Off');
							Ext.getCmp('activetemplate1').setValue(false);
							Ext.getCmp('id2').setValue('');
							Ext.getCmp('name2').setValue('');
							Ext.getCmp('address2').setValue('');
							Ext.getCmp('option2').setValue(false);
							Ext.getCmp('template2').setText('Off');
							Ext.getCmp('activetemplate2').setValue(false);
						}
						if(arrtask.length>1){
							Ext.getCmp('form-file2').setValue('');
							Ext.getCmp('id2').setValue(arrtask[1].id);
							Ext.getCmp('name2').setValue(arrtask[1].name);
							Ext.getCmp('address2').setValue(arrtask[1].address);
							if(arrtask[1].defaultoption=='Y'){
								Ext.getCmp('option2').setValue(true);
								Ext.getCmp('labelinfo').setText('The default name is '+arrtask[1].name);
							}else{
								Ext.getCmp('option2').setValue(false);
							}
							if(arrtask[1].template=='Y'){
								Ext.getCmp('template2').setText('On');
							}else{
								Ext.getCmp('template2').setText('Off');
							}
							if(arrtask[1].activetemplate=='Y'){
								Ext.getCmp('activetemplate2').setValue(true);
							}else{
								Ext.getCmp('activetemplate2').setValue(false);
							}
						}
					}                                
	})	
}
/*function cambiaCheckbox(idcheck){
	for(i=1;i<5;i++){
		if(("buyer"+i)!=idcheck){
			document.getElementById('buyer'+i).checked=false;
		}else{
			document.getElementById('buyer'+i).checked='checked';
		}
	}
}*/
buscar();
</script>