<?php
	include "../../properties_conexion.php";
	conectar();
	$userid=$_COOKIE['datos_usr']['USERID']; 	
?>
<style type="text/css">
#mytemplate_fonts_system .x-grid3-cell-inner {
  font-size: 16px;
  padding: 4px 0 0px 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_mytemplate_fonts_panel" style="background-color:#FFF;border-color:#FFF">
	<div id="mytemplate_fonts_data_div" align="center" style=" background-color:#FFF; margin:auto; width:980px;">
    	<div id="mytemplate_fonts_filters"></div><br />
        <div id="mytemplate_fonts_system" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<form id="hiddenUploadFont">
	<input type='file' id='fieldHiddenUploadFont' style='display:none;'>
</form>
<script>
	$("#fieldHiddenUploadFont").bind('change',function(){
		var filename = $(this).val()
		filename = filename.split('.').pop().toLowerCase();
		if(filename!="ttf"){
			Ext.Msg.show({
				title:'Warning',
				msg:'This file extension are not supported, please check that it is ttf',
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.MessageBox.OK,
			});
		}else{
			if(window.FormData){
				loading_win.show();
				var formData	=	new FormData();
				formData.append('file', $('#fieldHiddenUploadFont')[0].files[0]);
				formData.append('idfunction', "uploadUserFont");
				$.ajax({
					url: "/custom_contract/includes/php/functions.php",
					type: "POST",
					dataType: 'json',
					data: formData,
					processData: false,
					contentType: false,
					success: function (res){
						loading_win.hide();
						if(res.success==false){
							Ext.Msg.show({
								title:'Warning',
								msg:res.msg,
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.MessageBox.OK,
							});
						}else{
							Ext.getCmp('idGridMytemplate_fonts_system').getStore().reload();
							controlStylePersonalizedFonts();
						}
					}
				});
			}
		}
	});
	
	var InboxTabBar = new Ext.Toolbar({
		renderTo: 'mytemplate_fonts_filters',
		items:[
			new Ext.Button({
				tooltip: 'Add Font',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				style:'top:10px',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'small',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					$("#fieldHiddenUploadFont").trigger('click');
				}
			}),
			new Ext.Button({
				tooltip: 'Delete Fonts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'small',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(smmyfollowFonts.getSelections().length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the font(s) to be eliminated.'); return false;
					}
					loading_win.show();
					
					var pids="",coma="";
					Ext.each(smmyfollowFonts.getSelections(), function(row, index) {
						coma	=	(index==0)	?	""	:	",";
						pids	+=	coma+row.data.idFont; 
					});

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: "/custom_contract/includes/php/functions.php",
						method: 'POST',
						timeout :600000,
						params: { 
							idfunction: 'deleteUserFont',
							pids: pids,
							userCustom: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							//storemyfollowFonts.load({params:{start:0, limit:limitmyfollowmailInbox}});
							storemyfollowFonts.load();
							Ext.Msg.alert("Information", 'Font(s) deleted.');
							
						}                                
					});
				}
			}),
			new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				scale: 'small',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('FollowMyemail');
				}
			})
		]
	});
	var smmyfollowFonts = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				/*if(selected_datamyFontsUser.indexOf(record.get('idFont'))==-1)
					selected_datamyFontsUser.push(record.get('idFont'));
				
				if(Ext.fly(gridmyfollowFonts.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailInbox=true;*/
			},
			"rowdeselect": function(selectionModel,index,record){
				/*selected_datamyFontsUser = selected_datamyFontsUser.remove(record.get('idFont'));
				AllCheckmyfollowmailInbox=false;
				Ext.get(gridmyfollowFonts.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');*/
			}
		}		
	});
	var dateFont = function(value, metaData, record, rowIndex, colIndex, store){
		//return Ext.Date.format(value, 'd/m/Y');
		return value;
	}
	var renderFontSpecial = function(value, metaData, record, rowIndex, colIndex, store){
		metaData.style+="font-family:"+record.data.fontName+";";
		return "Example for this font named: "+record.data.fontName;
	}
	storemyfollowFonts = new Ext.data.JsonStore({
		url: '/custom_contract/includes/php/functions.php', 
		autoLoad:true,
		idProperty:'idFont',
		fields: [
			{name: 'idFont', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'fontName', type: 'string'},
			{name: 'fontFileName', type: 'string'},
			{name: 'uploadDate'}
		],
		root: 'results',
		totalProperty: 'total',
		baseParams: {
			'userCustom'	:	<?php echo $userid;?>,
			'idfunction'	:  	'managerGetFonts'
		},
		remoteSort: true,
		sortInfo: {
			field: 'fontName', 
			direction: 'ASC'
		},
		listeners: {
			beforeload: function(store,obj){
			}, 
			load: function(store,records,opt){
			},
			exception: function(dp,ty,ac,opt,res,arg){
			}
		}
	});
	var gridmyfollowFonts = new Ext.grid.EditorGridPanel({
		renderTo: 'mytemplate_fonts_system',
		id:'idGridMytemplate_fonts_system',
		cls: 'grid_comparables',
		height: 3000,
		store: storemyfollowFonts,
		viewConfig: {
			getRowClass: function (record, rowIndex, rp, store) {
				rp.tstyle += 'height: 30px;';
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowFonts
			,{header: "Font Name", width: 180, align: 'left', sortable: true, dataIndex: 'fontName'}
			,{header: "File Name", width: 180, align: 'left', sortable: true, dataIndex: 'fontFileName'}		
			,{header: 'Test', width: 300, sortable: true, align: 'left', renderer: renderFontSpecial}
			,{header: "Upload Date", width: 160, sortable: true, align: 'center', dataIndex: 'uploadDate',renderer: dateFont}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowFonts',
			store: 			storemyfollowFonts,
			displayInfo: 	true,
			displayMsg: 	'Total: {2} Fonts.',
			emptyMsg: 		'No Fonts to display'
		}),
		listeners: {
		},
		sm: smmyfollowFonts,
		frame:false,
		loadMask:true,
		border: false
	});
</script>