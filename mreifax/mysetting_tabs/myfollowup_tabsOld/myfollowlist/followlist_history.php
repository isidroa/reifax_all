<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	if($_COOKIE['datos_usr']['USERID']!=$_POST['userid']){
		$query="UPDATE xima.follow_notification SET history=0 WHERE parcelid='".$_POST['parcelid']."' AND userid=".$_POST['userid']." AND followid=".$_COOKIE['datos_usr']['USERID'];
		mysql_query($query) or die($query.mysql_error());
	}
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>
<style>
.x-grid3-row-expander-header {
  background-color: transparent;
  background-position: 1px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
.x-grid3-row-expander-header-collapse {
  background-color: transparent;
  background-position: -24px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
</style>
<div id="reportlist_content" style="text-align:center !important;">
	<br clear="all">
	<h1 align="center" >FOLLOW HISTORY</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center><div id="followlist_history_grid" style="width:650px;"></div></center>
	<br>
</div>

<script>
	var followstore = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
		fields: [ 'idfuh','parcelid','userid',
		   {name: 'odate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
		   {name: 'offer', type: 'float'},
		   {name: 'coffer', type: 'float'},
		   {name: 'contract', type: 'bool'},
		   {name: 'task', type: 'int'},
		   {name: 'pof', type: 'bool'},
		   {name: 'emd', type: 'bool'},
		   {name: 'realtorsadem', type: 'bool'},
		   {name: 'offerreceived', type: 'bool'},
		   {name: 'detail'},
		   {name: 'sheduledetail'},
		   {name: 'userid_follow', type: 'int'},
		   {name: 'name_follow'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'parcelid': "<?php echo $_POST['parcelid'];?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'odate',
			direction: 'ASC'
		}
    });
	
	var followdetail = new Ext.ux.grid.RowExpander({
		header: '<div id="headerexpander" class="x-grid3-row-expander-header">&nbsp;</div>',
		id: 'followdetailID',
		tpl : new Ext.Template(
			'<br><p style="margin-left:50px;font-size:14px;"><b>Schedule Task Detail:</b> {sheduledetail}</p>',
			'<br><p style="margin-left:50px;font-size:14px;"><b>Completed Task Detail:</b> {detail}</p>'
		)
	});
	var followsm = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../img/drop-no.gif" />'; 
		else return '<img src="../img/drop-yes.gif" />';
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive a Call." src="../../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../../img/notes/reci_other.png" />'; break;
		}
	}
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var followgrid = new Ext.grid.GridPanel({
		store: followstore,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [
				followsm,
				followdetail,
				{header: "Date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'odate', tooltip: 'Insert/Update Date.'},
				{header: "Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'offer', tooltip: 'Offer.'},
				{header: "C. Offer", renderer: Ext.util.Format.usMoney, dataIndex: 'coffer', tooltip: 'Contra Offer.'},
				{header: "User", renderer: userRender, dataIndex: 'userid_follow'},
				{header: "T", width: 5, dataIndex: 'task', renderer: taskRender, tooltip: 'Type of Task.'},
				{header: "C", width: 5, dataIndex: 'contract', renderer: checkRender, tooltip: 'Contract Send.'},
				{header: "P", width: 5, dataIndex: 'pof', renderer: checkRender, tooltip: 'Prof of Funds.'},
				{header: "E", width: 5, dataIndex: 'emd', renderer: checkRender, tooltip: 'EMD.'},
				{header: "A", width: 5, dataIndex: 'realtorsadem', renderer: checkRender, tooltip: 'Addendums.'},
				{header: "O", width: 5, dataIndex: 'offerreceived', renderer: checkRender, tooltip: 'Offer Received.'}
			]
		}),
		viewConfig: {
			forceFit:true,
			getRowClass: function(record, rowIndex, rp, ds){ // rp = rowParams
				if(true){
					return 'x-grid3-row-expanded';
				}
				return 'x-grid3-row-collapsed';
			}
		},  
		sm: followsm,      
		width: 600,
		height: tabs.getHeight()-200,
		plugins: followdetail,
		iconCls: 'icon-grid',
		renderTo: 'followlist_history_grid',
		listeners: {
			'headerclick': function (grid,column,e){
				var colmod = grid.getColumnModel();
				var headerID = colmod.getColumnId(column);
				var count = grid.getStore().getCount();
				
				if(headerID=='followdetailID'){
					var col = document.getElementById('headerexpander').className;
					if(col=='x-grid3-row-expander-header'){
						colmod.setColumnHeader(column,'<div id="headerexpander" class="x-grid3-row-expander-header-collapse">&nbsp;</div>');
						for(i=0;i<count;i++){
							followdetail.expandRow(grid.view.getRow(i));
						}
					}else{
						colmod.setColumnHeader(column,'<div id="headerexpander" class="x-grid3-row-expander-header">&nbsp;</div>');
						
						for(i=0;i<count;i++){
							followdetail.collapseRow(grid.view.getRow(i));
						}
					}
				}
			}
		}
	});
	
	followstore.load();

if(document.getElementById('followListTab')){
	if(document.getElementById('reportlist_content').offsetHeight > Ext.getCmp('tabsFollowDetail').getHeight()){
		tabs.setHeight(document.getElementById('reportlist_content').offsetHeight+100);
		viewport.setHeight(document.getElementById('reportlist_content').offsetHeight+150);
	}
}
</script>