<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmail_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
  		<div id="myfollowmail_filters"></div><br />
        <div id="myfollowmail_properties" align="left"></div> 
        <div align="left" style="color:#F00; font-size:14px;">
        	Right Click -> Overview / Contract 
            <span style="margin-left:15px; margin-right:15px;">
                  -     
            </span>
            Double Click -> Follow History
       	</div>
	</div>
</div>
<script>

	var limitmyfollowmail 			= 50;
	var selected_datamyfollowmail 	= new Array();
	var selected_datamyfollowmailid 	= new Array();
	var AllCheckmyfollowmail 			= false;
	
	//filter variables
	var filteraddressmyfollowmail 	= '';
	var filtermlnumbermyfollowmail 	= '';
	var filterstatusmyfollowmail 	= 'ALL';
	var filtercampaignmyfollowmail 	= 'ALL';
	var filterfield					= 'address';
	var filterdirection				= 'ASC';

	var storemyfollowmail = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowmail;
				obj.params.mlnumber=filtermlnumbermyfollowmail;
				obj.params.status=filterstatusmyfollowmail;
				obj.params.campaign=filtercampaignmyfollowmail;
			},
			'load' : function (store,data,obj){ 
				if (AllCheckmyfollowmail){
					Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowmail=true;
					gridmyfollowmail.getSelectionModel().selectAll();
					selected_datamyfollowmail=new Array(); 
				}else{
					AllCheckmyfollowmail=false;
					Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowmail.length > 0){
						for(val in selected_datamyfollowmail){
							var ind = gridmyfollowmail.getStore().find('pid',selected_datamyfollowmail[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						
						if (sel.length > 0)
							gridmyfollowmail.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemyfollowmailall = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'city','campaign','pid_camp',
				{name: 'sendmail', type: 'int'},
				{name: 'lasthistorydate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				'statusalt','type'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: 'address',
			direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filteraddressmyfollowmail;
				obj.params.mlnumber=filtermlnumbermyfollowmail;
				obj.params.status=filterstatusmyfollowmail;
				obj.params.campaign=filtercampaignmyfollowmail;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		var status = value+'-'+record.get('statusalt');
		switch(status){
			case 'A-N': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-UC': return '<div title="Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -100px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'A-PS': return '<div title="Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -140px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NA-N': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-UC': return '<div title="Non-Active Under Contract" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -80px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			case 'NA-PS': return '<div title="Non-Active Pending Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -120px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF-N': case 'NF-UC': case 'NF-PS': return '<div title="Not For Sale" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S-N': case 'S-UC': case 'S-PS':  return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive Call." src="../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../img/notes/reci_other.png" />'; break;
		}
	}
	
	function followupRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'FM': return '<img title="Followed." src="../../img/ix1y64.gif" />'; break;
			default: return ''; break;
		}
	}

	var smmyfollowmail = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmail.indexOf(record.get('pid'))==-1)
					selected_datamyfollowmail.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowmail.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on')){
					AllCheckmyfollowmail=true;  
				}
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmail = selected_datamyfollowmail.remove(record.get('pid'));
				AllCheckmyfollowmail=false;
				Ext.get(gridmyfollowmail.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'myfollowmail_filters',
		items: [
			new Ext.Button({
				tooltip: 'Click to delete',
				iconCls:'icon',
				iconAlign: 'top',
				scale: 'medium',
				width: 40,
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be deleted.'); return false;
					}
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{
						var pids='\''+selected_datamyfollowmail[0]+'\'';
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=',\''+selected_datamyfollowmail[i]+'\'';
						}
					}
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Properties deleted.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to send another Mail.',
				iconCls:'icon',
				iconAlign: 'top',
				scale: 'medium',
				width: 40,
				icon: 'http://www.reifax.com/img/ximaicon/mailcampaing.jpg',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be sent.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'resend',
							pid: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Mailing Campaings Properties sended again.');
							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Click to Follow Up selected properties.',
				iconCls:'icon',
				iconAlign: 'top',
				scale: 'medium',
				width: 40,
				icon: 'http://www.reifax.com/img/ximaicon/followup.jpg',
				handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be followed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					} 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'followup',
							pid: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
							storemyfollowmailall.load();
							gridmyfollowmail.getSelectionModel().deselectRange(0,limitmyfollowmail);
							Ext.Msg.alert("Mailing Campaings", 'Properties followed.');  
							
						}                                
					});
				}
			}),new Ext.Button({
				 tooltip: 'Click to Print Report',
				 iconCls:'icon',
				 iconAlign: 'top',
				 width: 40,
				 icon: 'http://www.reifax.com/img/toolbar/printer.png',
				 scale: 'medium',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					Ext.Ajax.request( 
						{  
							waitMsg: 'Printing Report...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_pdf.php', 
							method: 'POST', 
							timeout :600000,
							params: {
								userweb: 'false',
								parcelids_res: pids,
								template_res: 'Default',
								printType: 0
							},
							
							failure:function(response,options){
								Ext.MessageBox.alert('Warning','file can not be generated');
								loading_win.hide();
							},
							success:function(response,options){
								var rest = Ext.util.JSON.decode(response.responseText);
								//alert(rest.pdf);
								var url='http://www.reifax.com/'+rest.pdf;
								//alert(url);
								loading_win.hide();
								window.open(url);
								
							}                                
						 }
					);
				 }
			}),new Ext.Button({
				 tooltip: 'Click to Excel Report',
				 iconCls:'icon',
				 iconAlign: 'top',
				 width: 40,
				 icon: 'http://www.reifax.com/img/toolbar/excel.png',
				 scale: 'medium',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					var ownerShow='false';
					Ext.Msg.show({
						title:'Excel Report',
						msg: 'Would you like to save Excel Report with Owner Data?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn, text){
							if (btn == 'yes'){
								ownerShow='true';
							}
							loading_win.show();
							Ext.Ajax.request({  
								waitMsg: 'Excel Report...',

								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_excel.php', 
								timeout: 106000,
								method: 'POST', 
								params: {
									userweb:'false',
									parcelids_res:pids,
									ownerShow: ownerShow,
									template_res: 'Default'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','file can not be generated');
								},
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									//alert(url);
									location.href= url;
									//window.open(url);
								}                                
							});
						}
					});
				 }
			}),new Ext.Button({
				 tooltip: 'Click to Print Labels',
				 iconCls:'icon',
				 iconAlign: 'top',
				 width: 40,
				 icon: 'http://www.reifax.com/img/toolbar/label.png',
				 scale: 'medium',
				 hidden:icon_result,
				 handler: function(){
					if(selected_datamyfollowmail.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					if(AllCheckmyfollowmail==true){
						var totales = storemyfollowmailall.getRange(0,storemyfollowmailall.getCount());
						var pids=totales[0].data.pid;
						for(i=1;i<storemyfollowmailall.getCount();i++){
							pids+=','+totales[i].data.pid;	
						}
					}else{
						var pids=selected_datamyfollowmail[0];
						for(i=1; i<selected_datamyfollowmail.length; i++){
							pids+=','+selected_datamyfollowmail[i];
						}
					}
					
					var simple = new Ext.FormPanel({
						labelWidth: 150, 
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_mail_labels.php', 
						frame:true,
						title: 'Property Labels',
						bodyStyle:'padding:5px 5px 0',
						width: 400,
						waitMsgTarget : 'Generated Labels...',
						
						items: [{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[5160,5160],
											[5161,5161],
											[5162,5162],
											[5197,5197],
											[5163,5163]
									]
								}),
								name: 'label_type',
								fieldLabel: 'Label Type',
								mode: 'local',
								value: 5160,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[8,8],
											[9,9],
											[10,10]
									]
								}),
								name: 'label_size',
								fieldLabel: 'Label Size',
								mode: 'local',
								value: 8,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											[0,'Owner'],
											[1,'Property']
									]
								}),
								name: 'address_type',
								fieldLabel: 'Address',
								mode: 'local',
								value: 0,
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['L','Left'],
											['C','Center']
									]
								}),
								displayField:'title',
								valueField: 'val',
								name: 'align_type',
								fieldLabel: 'Alingment',
								mode: 'local',
								value: 'L',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'combo',
								editable: false,
								displayField:'title',
								valueField: 'val',
								store: new Ext.data.SimpleStore({
									fields: ['val', 'title'],
									data : [
											['N','No'],
											['Y','Yes']
									]
								}),
								name: 'resident_type',
								fieldLabel: 'Current Resident Or',
								mode: 'local',
								value: 'N',
								triggerAction: 'all',
								selectOnFocus:true,
								allowBlank:false
							},{
								xtype: 'hidden',
								name: 'type',
								value: 'result'
							},{
								xtype: 'hidden',
								name: 'parcelids_res',
								value: pids
							}
						],
				
						buttons: [{
							text: 'Apply',
							handler  : function(){
									loading_win.show();
									simple.getForm().submit({
										success: function(form, action) {
											//Ext.Msg.alert("Failure", action.result.pdf);
											var url='http://www.reifax.com/'+action.result.pdf;
											loading_win.hide();
											window.open(url);
											//window.open(url,'Print Labels',"fullscreen",'');
										},
										failure: function(form, action) {
											Ext.Msg.alert("Failure", action.result.msg);
											loading_win.hide();
										}
									});
								}
						},{
							text: 'Cancel',
							handler  : function(){
									simple.getForm().reset();
								}
						}]
					});
					win = new Ext.Window({
						
						layout      : 'fit',
						width       : 370,
						height      : 300,
						modal	 	: true,
						plain       : true,
						items		: simple,
			
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
					win.addListener("beforeshow",function(win){
						simple.getForm().reset();
					});
				 }
			}),new Ext.Button({
				tooltip: 'Click to filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				scale: 'medium',
				handler: function(){
					filteraddressmyfollowmail = '';
					filtermlnumbermyfollowmail = '';
					filterstatusmyfollowmail = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					var formmyfollowmail = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'myfollowmail_filters',
						id: 'formmyfollowmail',
						name: 'formmyfollowmail',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filteraddressmyfollowmail=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fmlnumber',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Mlnumber',
									name		  : 'fmlnumber',
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtermlnumbermyfollowmail=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : 'ALL',
									hiddenName    : 'fstatus',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterstatusmyfollowmail = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcampaign',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Campaign',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										id:'persID',
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'campaigns',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'},
											{name:'persFirstName', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followmail.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcampaignname',
									value         : 'ALL',
									hiddenName    : 'fcampaign',
									hiddenValue   : 'ALL',
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtercampaignmyfollowmail = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
									storemyfollowmailall.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filteraddressmyfollowmail = '';
									filtermlnumbermyfollowmail = '';
									filterstatusmyfollowmail = 'ALL';
									filtercampaignmyfollowmail 	= 'ALL';
									
									Ext.getCmp('formmyfollowmail').getForm().reset();
									
									storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
									storemyfollowmailall.load();
									win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 250,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowmail,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Click to remove filters',
				iconCls:'icon',
				iconAlign: 'top',
				width: 40,
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				scale: 'medium',
				handler: function(){
					filteraddressmyfollowmail = '';
					filtermlnumbermyfollowmail = '';
					filterstatusmyfollowmail = 'ALL';
					filtercampaignmyfollowmail 	= 'ALL';
					
					storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
					storemyfollowmailall.load();
				}
			})	
		]
	});
	
	var gridmyfollowmail = new Ext.grid.GridPanel({
		renderTo: 'myfollowmail_properties',
		cls: 'grid_comparables',
		width: 945,
		height: 400,
		store: storemyfollowmail,
		stripeRows: true,
		sm: smmyfollowmail, 
		columns: [
			smmyfollowmail,
			{header: '', hidden: true, editable: false, dataIndex: 'pid'},
			{header: 'F', width: 25, renderer: followupRender, tooltip: 'Followed.', dataIndex: 'type'},
			{header: 'TS', width: 25, sortable: true, tooltip: 'Times Sent', dataIndex: 'sendmail'},
			{header: 'Campaign', width: 100, sortable: true, tooltip: 'Campaign', dataIndex: 'campaign'}
			<?php 
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					elseif($val->name!='agent')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
		   ?>
		   ,{header: "City", dataIndex: 'city', tooltip: 'City.'}	
		   ,{header: "Last mail date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'lasthistorydate', tooltip: 'Last Regular Mail Date.'}		
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyfollowmail',
            pageSize: limitmyfollowmail,
            store: storemyfollowmail,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Mail.',
			emptyMsg: "No follow Mail to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows mail per page.',
				text: 50,
				handler: function(){
					limitmyfollowmail=50;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows mail per page.',
				text: 80,
				handler: function(){
					limitmyfollowmail=80;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows mail per page.',
				text: 100,
				handler: function(){
					limitmyfollowmail=100;
					Ext.getCmp('pagingmyfollowmail').pageSize = limitmyfollowmail;
					Ext.getCmp('pagingmyfollowmail').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_group'
			})]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filterfield=sorted.field;
				filterdirection=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						overview
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowdblclick': function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				
				if(document.getElementById('reportsTab')){
					var tab = tabs.getItem('reportsTab');
					tabs.remove(tab);
				}
				
				/*var followTbar = new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						tooltip: 'Click to Follow Agents.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/agent.png',
						scale: 'medium',
						handler: function(){
							var storeagent 		= new Ext.data.JsonStore({
								autoLoad		: true,
								url				: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
								fields			: [
									{name		: 'agentid', 	type: 'int'},
									{name		: 'agent'},
									{name		: 'email'},
									{name		: 'tollfree'},
									{name		: 'phone1'},
									{name		: 'typeph1', 	type: 'int'},
									{name		: 'phone2'},
									{name		: 'typeph2', 	type: 'int'},
									{name		: 'phone3'},
									{name		: 'typeph3', 	type: 'int'},
									{name		: 'fax'},
									{name		: 'principal', 	type: 'int'},
									{name		: 'urlsend'}
								],
								root			: 'records',
								totalProperty	: 'total',
								baseParams		: {
									'userid'	: record.get('userid'),
									'pid'		: record.get('pid'),
									'type'		: 'assignment'
								},
								remoteSort		: true,
								sortInfo		: {
									field		: 'agent',
									direction	: 'ASC'
								}
							});
							
							var smagent = new Ext.grid.CheckboxSelectionModel({
								checkOnly	: true, 
								width		: 25
							});
							
							function phoneRender(value, metaData, rec, rowIndex, colIndex, store) {
								var num = colIndex-3;
								var type = 'typeph'+num;
								var tyvalue = rec.get(type);
								var classvalue = '';
								
								if(tyvalue == 0) 
									classvalue = 'home';
								else{ 
									if(tyvalue == 1) 
										classvalue = 'office';
									else 
										classvalue = 'cell';
								}
								if(value.length == 0) classvalue = '';
								
								return '<div class="'+classvalue+'">'+value+'</div>';
							}
							
							function agentPrincipal(value, metaData, rec, rowIndex, colIndex, store){
								var principal = rec.get('principal');
								var classvalue = '';
								
								if(principal==1){
									classvalue = 'agentprincipal';
								}
								
								return '<div class="'+classvalue+'">'+value+'</div>';  
							}
							
							var gridagent 		= new Ext.grid.GridPanel({
								cls				: 'grid_comparables',
								width			: 800,
								height			: 190,
								store			: storeagent,
								stripeRows		: true,
								sm				: smagent, 
								columns			: [
									smagent,
									{
										header		: 'Agent', 
										width		: 150, 
										sortable	: true, 
										tooltip		: 'Agent name,', 
										dataIndex	: 'agent',
										renderer	: agentPrincipal
									},{
										header		: 'Email', 
										width		: 150, 
										sortable	: true, 
										tooltip		: 'Agent Email.', 
										dataIndex	: 'email'
									},{
										header		: 'Toll Free', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Phone to Toll Free.', 
										dataIndex	: 'tollfree'
									},{
										header		: 'Phone 1', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Phone 1.', 
										dataIndex	: 'phone1', 
										renderer: phoneRender
									},{
										header		: 'Phone 2', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Phone 2.', 
										dataIndex	: 'phone2', 
										renderer: phoneRender
									},{
										header		: 'Phone 3', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Phone 3.', 
										dataIndex	: 'phone3', 
										renderer: phoneRender
									},{
										header		: 'Fax', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Fax.', 
										dataIndex	: 'fax'
									},{
										header		: 'Send Page', 
										width		: 100, 
										sortable	: true, 
										tooltip		: 'Url of page to send documents.', 
										dataIndex	: 'urlsend'
									}			
								]
							});
							
							var tbagent = new Ext.Toolbar({
								items: [
									new Ext.form.ComboBox({
										id: 'comboagent',
										fieldLabel: '',
										triggerAction: 'all',
										forceSelection: true, 
										typeAhead: true,
										typeAheadDelay: 10,
										minChars: 0,
										mode: 'remote',
										store: new Ext.data.JsonStore({
											url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
											forceSelection: true,
											root: 'records',
											baseParams		: {
												'userid'	: record.get('userid'),
												'sort'		: 'agent',
												'dir'		: 'ASC'
											},
											id: 0,
											fields: [
												'agentid',
												'agent'
											]
										}),
										valueField: 'agentid',
										displayField: 'agent'
									}),
									' ',
									{
										text	: '>>',
										tooltip : 'Assign agent to the follow.',
										handler	: function (){
											var agentid = Ext.getCmp('comboagent').getValue();
											if(agentid==''){ 
												Ext.Msg.alert("Follow Agent", 'You must previously select a agent in combo to be assignment.');
												return false;
											}
											
											Ext.Ajax.request( 
											{  
												waitMsg: 'Assigning...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
												method: 'POST',
												timeout :600000,
												params: { 
													type	: 'assignment-add',
													agentid	: agentid,
													userid	: record.get('userid'),
													pid		: record.get('pid')
												},
												
												failure:function(response,options){
													loading_win.hide();
													Ext.MessageBox.alert('Warning','ERROR');
												},
												success:function(response,options){
													Ext.MessageBox.alert("Follow Agent",'Assignment sucessfully.');
													storeagent.load();
												}                                
											});
										}
									},
									'-',
									{
										text	: '<<',
										tooltip : 'Remove agent to the follow.',
										handler	: function (){
											if(smagent.getCount()==0 || smagent.getCount()>1){ 
												Ext.Msg.alert("Follow Agent", 'You must previously select(check) only one agent in grid to be removed.');
												return false;
											}
											
											var agent = smagent.getSelected();												
											var agentid = agent.get('agentid');
											
											Ext.Ajax.request( 
											{  
												waitMsg: 'Removing...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
												method: 'POST',
												timeout :600000,
												params: { 
													type	: 'assignment-del',
													agentid	: agentid,
													userid	: record.get('userid'),
													pid		: record.get('pid')
												},
												
												failure:function(response,options){
													loading_win.hide();
													Ext.MessageBox.alert('Warning','ERROR');
												},
												success:function(response,options){
													Ext.MessageBox.alert("Follow Agent",'Removed sucessfully.');
													storeagent.load();
												}                                
											});
										}
									},
									'-',
									{
										text	: 'Set Principal',
										handler	: function (){
											if(smagent.getCount()==0 || smagent.getCount()>1){ 
												Ext.Msg.alert("Follow Agent", 'You must previously select(check) only one agent in grid to be principal.');
												return false;
											}
											
											var agent = smagent.getSelected();												
											var agentname = agent.get('agent');
											
											Ext.Ajax.request( 
											{  
												waitMsg: 'Assigning...',
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
												method: 'POST',
												timeout :600000,
												params: { 
													type		: 'assignment-principal',
													agentname	: agentname,
													userid		: record.get('userid'),
													pid			: record.get('pid')
												},
												
												failure:function(response,options){
													loading_win.hide();
													Ext.MessageBox.alert('Warning','ERROR');
												},
												success:function(response,options){
													Ext.MessageBox.alert("Follow Agent",'Sucessfully setting.');
													storeagent.load();
												}                                
											});
										}
									},
									'-',
									new Ext.Button({
										tooltip: 'Click to add a new agent.',
										iconCls:'icon',
										iconAlign: 'top',
										width: 30,
										icon: 'http://www.reifax.com/img/add.gif',
										handler: function(){
											var simple = new Ext.FormPanel({
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
												frame: true,
												title: 'Follow Agent',
												width: 350,
												waitMsgTarget : 'Waiting...',
												labelWidth: 75,
												defaults: {width: 230},
												labelAlign: 'left',
												items: [{
															xtype     : 'textfield',
															name      : 'agent',
															fieldLabel: 'Agent',
															allowBlank: false
														},{
															xtype     : 'textfield',
															name      : 'email',
															fieldLabel: 'Email',
															vtype	  : 'email'
														},{
															xtype     : 'textfield',
															name      : 'tollfree',
															fieldLabel: 'Toll Free'
														},{
															xtype     : 'textfield',
															name      : 'urlsend',
															fieldLabel: 'Send Page'
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname1',
																value         : '0',
																hiddenName    : 'typeph1',
																hiddenValue   : '0',
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone1',
																width	  : 165
															}]
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone 2',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname2',
																value         : '0',
																hiddenName    : 'typeph2',
																hiddenValue   : '0',
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone2',
																width	  : 165
															}]
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone 3',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname3',
																value         : '0',
																hiddenName    : 'typeph3',
																hiddenValue   : '0',
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone3',
																width	  : 165
															}]
														},{
															xtype     : 'textfield',
															name      : 'fax',
															fieldLabel: 'Fax'
														},{
															xtype     : 'hidden',
															name      : 'type',
															value     : 'insert'
														}],
												
												buttons: [{
														text: 'Insert',
														handler: function(){
															loading_win.show();
															simple.getForm().submit({
																success: function(form, action) {
																	loading_win.hide();
																	win.close();
																	Ext.Msg.alert("Follow Agent", 'New Follow Agent.');
																	storeagent.load();
																},
																failure: function(form, action) {
																	loading_win.hide();
																	Ext.Msg.alert("Failure", "ERROR");
																}
															});
														}
													},{
														text: 'Reset',
														handler  : function(){
															simple.getForm().reset();
															win.close();
														}
													}]
												});
											 
											var win = new Ext.Window({
												layout      : 'fit',
												width       : 340,
												height      : 350,
												modal	 	: true,
												plain       : true,
												items		: simple,
												closeAction : 'close',
												buttons: [{
													text     : 'Close',
													handler  : function(){
														win.close();
														loading_win.hide();
													}
												}]
											});
											win.show();
										}
									}),
									new Ext.Button({
										tooltip: 'Click to edit selected agent.',
										iconCls:'icon',
										iconAlign: 'top',
										width: 30,
										icon: 'http://www.reifax.com/img/refresh.gif',
										handler: function(){
											if(smagent.getCount()==0 || smagent.getCount()>1){ 
												Ext.Msg.alert("Follow Agent", 'You must previously select(check) only one agent in grid to be edited.');
												return false;
											}
											
											var agent = smagent.getSelected();
											
											var simple = new Ext.FormPanel({
												url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
												frame: true,
												title: 'Follow Agent',
												width: 350,
												waitMsgTarget : 'Waiting...',
												labelWidth: 75,
												defaults: {width: 230},
												labelAlign: 'left',
												items: [{
															xtype     : 'textfield',
															name      : 'agent',
															fieldLabel: 'Agent',
															allowBlank: false,
															value	  : agent.get('agent')
														},{
															xtype     : 'textfield',
															name      : 'email',
															fieldLabel: 'Email',
															vtype	  : 'email',
															value	  : agent.get('email')
														},{
															xtype     : 'textfield',
															name      : 'tollfree',
															fieldLabel: 'Toll Free',
															value	  : agent.get('tollfree')
														},{
															xtype     : 'textfield',
															name      : 'urlsend',
															fieldLabel: 'Send Page',
															value	  : agent.get('urlsend')
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname1',
																value         : agent.get('typeph1'),
																hiddenName    : 'typeph1',
																hiddenValue   : agent.get('typeph1'),
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone1',
																width	  : 165,
																value	  : agent.get('phone1')
															}]
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone 2',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname2',
																value         : agent.get('typeph2'),
																hiddenName    : 'typeph2',
																hiddenValue   : agent.get('typeph2'),
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone2',
																width	  : 165,
																value	  : agent.get('phone2')
															}]
														},{
															xtype	  : 'compositefield',
															fieldLabel: 'Phone 3',
															items	  : [{
																xtype         : 'combo',
																mode          : 'local',
																triggerAction : 'all',
																width		  : 60,
																store         : new Ext.data.ArrayStore({
																	id        : 0,
																	fields    : ['valor', 'texto'],
																	data      : [
																		['0','Home'],
																		['1','Office'],
																		['2','Cell']
																	]
																}),
																displayField  : 'texto',
																valueField    : 'valor',
																name          : 'typephname3',
																value         : agent.get('typeph3'),
																hiddenName    : 'typeph3',
																hiddenValue   : agent.get('typeph3'),
																allowBlank    : false
															},{
																xtype     : 'textfield',
																name      : 'phone3',
																width	  : 165,
																value	  : agent.get('phone3')
															}]
														},{
															xtype     : 'textfield',
															name      : 'fax',
															fieldLabel: 'Fax',
															value	  : agent.get('fax')
														},{
															xtype     : 'hidden',
															name      : 'type',
															value     : 'update'
														},{
															xtype     : 'hidden',
															name      : 'agentid',
															value     : agent.get('agentid')
														}],
												
												buttons: [{
														text: 'Update',
														handler: function(){
															loading_win.show();
															simple.getForm().submit({
																success: function(form, action) {
																	loading_win.hide();
																	win.close();
																	Ext.Msg.alert("Follow Agent", 'Updated Agent.');
																	storeagent.load();
																},
																failure: function(form, action) {
																	loading_win.hide();
																	Ext.Msg.alert("Failure", "ERROR");
																}
															});
														}
													},{
														text: 'Reset',
														handler  : function(){
															simple.getForm().reset();
															win.close();
														}
													}]
												});
											 
											var win = new Ext.Window({
												layout      : 'fit',
												width       : 340,
												height      : 350,
												modal	 	: true,
												plain       : true,
												items		: simple,
												closeAction : 'close',
												buttons: [{
													text     : 'Close',
													handler  : function(){
														win.close();
														loading_win.hide();
													}
												}]
											});
											win.show();
										}
									})
								]
							});
							
							var winagent 	= new Ext.Window({
								title		: 'Agent',
								layout      : 'fit',
								width       : 950,
								height      : 200,
								modal	 	: true,
								plain       : true,
								items		: gridagent,
								closeAction : 'close',
								tbar		: tbagent,
								
								buttons: [{
									text     : 'Close',
									handler  : function(){
										winagent.close();
									}
								}]
							});
							
							winagent.show();
						}
					},
					'->',{
						 tooltip: 'Click to Close Follow',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/cancel.png',
						 scale: 'medium',
						  
						 handler: function(){
							 var tab = tabs.getItem('reportsTab');
							 tabs.remove(tab);
						 }
					}]
				});
				
				var tbarhistory =  new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to Delete Selected Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/del_doc.png',
						 scale: 'medium',
						 handler: function(){
							var followfuh = followsm.getSelections();
							var idfuh = '';
							if(followfuh.length > 0){
								idfuh=followfuh[0].get('idfuh');
								for(i=1; i<followfuh.length; i++){
									idfuh+=','+followfuh[i].get('idfuh');
								}
							}
							Ext.Ajax.request( 
							{  
								waitMsg: 'Deleting...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									type: 'delete',
									idfuh: idfuh
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
									followstore.load();
								}                                
							});
						 }
					},{
						 tooltip: 'Click to New Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Add History',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : '0',
											hiddenName    : 'task',
											hiddenValue   : '0',
											allowBlank    : false
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail'
										},{
											xtype	  : 'checkboxgroup',
											fieldLabel: 'Document',
											columns	  : 2,
											itemCls   : 'x-check-group-alt',
											items	  : [
												{boxLabel: 'Contract', name: 'contract'},
												{boxLabel: 'Proof of Funds', name: 'pof'},
												{boxLabel: 'EMD', name: 'emd'},
												{boxLabel: 'Ademdums', name: 'rademdums'}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										}],
								
								buttons: [{
										text: 'Add',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow History", 'New Follow History.');
													followstore.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 380,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
						 tooltip: 'Click to Update Select Follow History.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/refresh.gif',
						 scale: 'medium',
						 handler: function(){
							var followfuh = followsm.getSelections();
							if(followfuh.length > 1){
								Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
								return false;
							}else if(followfuh.length == 0){
								Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
								frame: true,
								title: 'Update History',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0,
											value	  : followfuh[0].get('offer')
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0,
											value	  : followfuh[0].get('coffer')
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfuh[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfuh[0].get('task'),
											allowBlank    : false
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail',
											value	  : followfuh[0].get('detail')
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 2,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: !followfuh[0].get('contract')},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh[0].get('pof')},
												{boxLabel: 'EMD', name: 'emd', checked: !followfuh[0].get('emd')},
												{boxLabel: 'Ademdums', name: 'rademdums', checked: !followfuh[0].get('realtorsadem')}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfuh',
											value     : followfuh[0].get('idfuh')
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Up", 'Updated Follow History.');
													followstore.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 380,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},new Ext.Button({
						tooltip: 'Click to print history.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 4,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 4,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});
				
				var tbarschedule = new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to Delete Selected Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/del_doc.png',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							var idfus = '';
							if(followfus.length > 0){
								idfus=followfus[0].get('idfus');
								for(i=1; i<followfus.length; i++){
									idfus+=','+followfus[i].get('idfus');
								}
							}
							Ext.Ajax.request( 
							{  
								waitMsg: 'Deleting...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									type: 'delete',
									idfus: idfus
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
									followstore_schedule.load();
								}                                
							});
						 }
					},{
						 tooltip: 'Click to New Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Add Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype		  : 'datefield',
											allowBlank	  : false,
											name		  : 'odate',
											fieldLabel	  : 'Date',
											format		  : 'Y-m-d'
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : '0',
											hiddenName    : 'task',
											hiddenValue   : '0',
											allowBlank    : false
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail'
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										}],
								
								buttons: [{
										text: 'Add',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'New Follow Schedule.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 380,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
						 tooltip: 'Click to Update Select Follow Schedule.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/refresh.gif',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							if(followfus.length > 1){
								Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
								return false;
							}else if(followfus.length == 0){
								Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Update Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     	  : 'datefield',
											name      	  : 'odate',
											fieldLabel	  : 'Date',
											value	  	  : followfus[0].get('odate'),
											format	  	  : 'Y-m-d'
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfus[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfus[0].get('task'),
											allowBlank    : false
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail',
											value	  : followfus[0].get('detail')
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'update'
										},{
											xtype     : 'hidden',
											name      : 'idfus',
											value     : followfus[0].get('idfus')
										}],
								
								buttons: [{
										text: 'Update',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'Updated Follow Schedule.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 380,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},{
						 tooltip: 'Click to Complete Follow Schedule Task.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/refresh.png',
						 scale: 'medium',
						 handler: function(){
							var followfus = followsm_schedule.getSelections();
							if(followfus.length > 1){
								Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
								return false;
							}else if(followfus.length == 0){
								Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
								return false;
							}
							
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Complete Schedule',
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype     : 'numberfield',
											name      : 'offer',
											fieldLabel: 'Offer',
											minValue  : 0
										},{
											xtype     : 'numberfield',
											name      : 'coffer',
											fieldLabel: 'C. Offer',
											minValue  : 0
										},{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Task',
											triggerAction : 'all',
											store         : new Ext.data.ArrayStore({
												id        : 0,
												fields    : ['valor', 'texto'],
												data      : [
													['0','No Task'],
													['1','Send SMS'],
													['2','Receive SMS'],
													['3','Send FAX'],
													['4','Receive FAX'],
													['5','Send EMAIL'],
													['6','Receive EMAIL'],
													['7','Send DOC'],
													['8','Receive DOC'],
													['9','Make CALL'],
													['10','Receive CALL'],
													['11','Send R. MAIL'],
													['12','Receive R. MAIL'],
													['13','Send OTHER'],
													['14','Receive OTHER']
												]
											}),
											displayField  : 'texto',
											valueField    : 'valor',
											name          : 'ftaskname',
											value         : followfus[0].get('task'),
											hiddenName    : 'task',
											hiddenValue   : followfus[0].get('task'),
											allowBlank    : false
										},{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Detail',
											value	  : followfus[0].get('detail')
										},{
											xtype: 'checkboxgroup',
											fieldLabel: 'Document',
											columns: 2,
											itemCls: 'x-check-group-alt',
											items: [
												{boxLabel: 'Contract', name: 'contract', checked: false},
												{boxLabel: 'Proof of Funds', name: 'pof', checked: false},
												{boxLabel: 'EMD', name: 'emd', checked: false},
												{boxLabel: 'Addendums', name: 'rademdums', checked: false}
											]
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'complete'
										},{
											xtype     : 'hidden',
											name      : 'idfus',
											value     : followfus[0].get('idfus')
										},{
											xtype     : 'hidden',
											name      : 'parcelid',
											value     : record.get('pid') 
										}],
								
								buttons: [{
										text: 'Complete',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Schedule", 'Completed Follow Schedule Task.');
													followstore_schedule.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 380,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						 }
					},new Ext.Button({
						tooltip: 'Click to print history.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 5,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 5,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});
				
				var tbarmessages = new Ext.Toolbar({
					cls: 'no-border',
					width: 'auto',
					items: [' ',{
						 tooltip: 'Click to post message.',
						 iconCls:'icon',
						 iconAlign: 'top',
						 width: 40,
						 icon: 'http://www.reifax.com/img/add.gif',
						 scale: 'medium',
						 handler: function(){
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followmessages.php',
								frame: true,
								title: 'Follow Messages',
								width: 450,
								waitMsgTarget : 'Waiting...',
								labelWidth: 75,
								defaults: {width: 330},
								labelAlign: 'left',
								items: [{
											xtype	  :	'textarea',
											name	  : 'sms',
											fieldLabel: 'Message',
											value	  : '',
											blankText : 'post a Message...',
											height	  : 200
										},{
											xtype     : 'hidden',
											name      : 'type',
											value     : 'insert'
										},{
											xtype     : 'hidden',
											name      : 'userid',
											value     : record.get('userid')
										},{
											xtype     : 'hidden',
											name      : 'pid',
											value     : record.get('pid') 
										}],
								
								buttons: [{
										text: 'Post',
										handler: function(){
											loading_win.show();
											simple.getForm().submit({
												success: function(form, action) {
													loading_win.hide();
													win.close();
													Ext.Msg.alert("Follow Messages", 'Posted messages sucesfully.');
													followstore_messages.load();
												},
												failure: function(form, action) {
													loading_win.hide();
													Ext.Msg.alert("Failure", "ERROR");
												}
											});
										}
									},{
										text: 'Reset',
										handler  : function(){
											simple.getForm().reset();
											win.close(); 
										} 
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 440,
								height      : 350,
								modal	 	: true,  
								plain       : true,
								items		: simple,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
						}
					},new Ext.Button({
						tooltip: 'Click to print messages.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/printer.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									printType: 6,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.pdf;
									loading_win.hide();
									window.open(url);							
								}                                
							});
						}
					}),new Ext.Button({
						tooltip: 'Click to export Excel.',
						iconCls:'icon',
						iconAlign: 'top',
						width: 40,
						icon: 'http://www.reifax.com/img/toolbar/excel.png',
						scale: 'medium',
						handler: function(){
							loading_win.show();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Checking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
								method: 'POST',
								timeout :600000,
								params: { 

									printType: 6,
									parcelid: record.get('pid'),
									userid: record.get('userid'), 
									county: record.get('county')
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								
								success:function(response,options){
									var rest = Ext.util.JSON.decode(response.responseText);
									var url='http://www.reifax.com/'+rest.excel;
									loading_win.hide();
									location.href= url;								
								}                                
							});
						}
					})]
				});*/
				
				tabsFollow.add({
					title: ' Follow ',
					id: 'followTab', 
					closable: true,
					items: [	
						new Ext.TabPanel({
							id: 'historyTab',
							activeTab: 0,
							width: ancho,
							height: tabs.getHeight(),
							plain:true,
							listeners: {
								'tabchange': function( tabpanel, tab ){
									if(tab.id=='followhistoryInnerTab')
										if(document.getElementById('follow_history_grid')) followstore.load();
									else if(tab.id=='followsheduleInnerTab')
										if(document.getElementById('follow_schedule_grid')) followstore_schedule.load();
								}
							},
							items:[{
								title: 'History',
								id: 'followhistoryInnerTab',
								autoLoad: {
									url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', 
									scripts	: true, 
									params	:{
										parcelid 	: record.get('pid'), 
										userid 		: record.get('userid'), 
										county 		: record.get('county') 
									}, 
									timeout	: 10800
								},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
								//,tbar: tbarhistory
							},{
								title: 'Schedule',
								id: 'followsheduleInnerTab',
								autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
								//,tbar: tbarschedule
							},{
								title: 'Messages',
								id: 'followmessagesInnerTab',
								autoLoad: {url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followmessages.php', scripts: true, params:{parcelid:record.get('pid'), userid:record.get('userid'), county:record.get('county') }, timeout: 10800},
								enableTabScroll:true,
								defaults:{ autoScroll: false}
								//,tbar: tbarmessages
							}]
						})
					]
					//,tbar: followTbar
				}).show();
			}
		}
	});
	
	storemyfollowmail.load({params:{start:0, limit:limitmyfollowmail}});
	storemyfollowmailall.load();
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowmail_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowmail_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
</script>