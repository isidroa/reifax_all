function sendContract(selected,grid,store,userid, progressbar_win, progressbar, mailenviados, completetask, selling){
	selling = selling || false;
	
	Ext.apply(Ext.form.VTypes, {
		moneda : function(v) {
			return /^[\d]*(.[\d]{2})?$/.test(v);
		},
		monedaText: 'Wrong format in the amount.(Example: 1500.00)',
		monedaMask: /^[\d\.]/i
	});
	var contractseal = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			['0','Editable Contract'],
			['1','Sealed Contract']
		]
	});
	// Helper function to show windows for toolbar options
	function showWin(ttle,cmp) {
		win = new Ext.Window({
			title       : ttle,
			minimzable  : false,
			maximizable : false,
			resizable   : false,
			dragable    : false,
			modal       : true,
			id          : 'auxWin',
			hideMode    : 'offsets',
			layout      : 'fit',
			autoDestroy : true,
			autoWidth   : true,
			autoHeight  : true,
			items       : [ cmp ]
		});
		win.show();
	}
	// Add and save and addendum to the DB
	function agregar() {

		addendform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					allowBlank    : false
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						addendform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.msg);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});

		showWin('Add Addendum',addendform);

	}

	// Edit and save and addendum to the DB
	function modificar(sel,name,content) {
		var modifyform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					value         : name,
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					value         : content,
					allowBlank    : false
				},{
					xtype         : 'hidden',
					name          : 'id',
					value         : sel
				},{
					xtype         : 'hidden',
					name          : 'cmd',
					value         : 'mod'
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						modifyform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.mensaje);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});
		showWin('Edit Addendum',modifyform);
	}
	// START OPTIONS FOR THE GRID

	// Reader of the data
	var addReader = new Ext.data.JsonReader(
		{
			successProperty : 'success',
			root            : 'data',
			id              : 'id'
		}, Ext.data.Record.create([
			{name : 'id', type: 'int'},
			{name : 'user', type: 'int'},
			{name : 'name', type: 'string'},
			{name : 'active', type: 'string'}
		])
	);

	// Selection model for the checkbox column
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false
	});

	// columns of the grid
	var addColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			{  header: 'Name', dataIndex: 'name' },
			selmode
		]
	});

	// Toolbar definition.
	var toolbar = new Ext.Toolbar({
		items : [
			{
				text    : ' Add ',
				handler : agregar
			},{
				text    : ' Edit ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					if (selId.length > 0) {

						Ext.Ajax.request({
							url     : 'mysetting_tabs/mycontracts_tabs/listaddendum.php?cmd=chk&id='+selId[0].id,
							success : function(r,a) {
								var resp   = Ext.decode(r.responseText);
								modificar(selId[0].id, resp.data[0].name, resp.data[0].content);
							}
						});

					} else
						Ext.MessageBox.alert('','Please select the row to edit.');

				}
			},{
				text    : ' Delete ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					var ckEliminar = function(btn) {

						if (btn == 'yes') {

							Ext.Ajax.request({
								url     : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php?cmd=del&id='+selId[0].id,
								success : function(r) {
									Ext.MessageBox.alert('',r.responseText);
									Ext.getCmp('addGrid').store.reload();
								}
							});
						}
					}

					if (selId.length > 0)
						Ext.MessageBox.confirm('Confirm','Are you sure?',ckEliminar);
					else
						Ext.MessageBox.alert('','Please select the row to delete.');

				}
			}
		]
	});

	// END OPTIONS FOR THE GRID

	// Definition of the addendum grid
	var gridAdem = new Ext.grid.GridPanel({
		id              : 'addGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : true,
			url         : 'mysetting_tabs/mycontracts_tabs/listaddendum.php',

			reader      : addReader
		}),
		sm               : selmode,
		cm               : addColumn,
		height           : 100,
		frame            : true,
		tbar             : toolbar,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	
	//Definition of the panel with all the options and the grid
	var addend  = new Ext.Panel({
		xtype         : 'panel',
		hidden        : true,
		collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddend',
		title         : 'Addendum Options',
		width         : 410,
		labelWidth    : 150,
		items         : [ gridAdem ]

	});
	
	// -- Addendum Panel (END)
	
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false,
	});
	
	var addOnColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			{  header: 'Name', dataIndex: 'name' },
			selmode
		]
	});
	
	// Definition of the addon grid
	var gridAdon = new Ext.grid.GridPanel({
		id              : 'addonGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : false,
			reader      : addReader,
			url         : 'mysetting_tabs/mycontracts_tabs/listaddons.php',
			listeners   : {
				'load'   : function () {										
					var recs = [];
					Ext.getCmp('addonGrid').getStore().each(function(rec){
						if(rec.data.active == 'true'){
							recs.push(rec);
						}
					});
					selmode.selectRecords(recs);		
				}
			}
		}),
		sm               : selmode,
		cm               : addOnColumn,
		height           : 100,
		frame            : true,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	

	//Definition of the panel with all the options and the grid
	var addond  = new Ext.Panel({
		xtype         : 'panel',
		hidden        : true,
		collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddon',
		title         : 'Additional Documents Options',
		width         : 410,
		labelWidth    : 150,
		items         : [ gridAdon ]

	});
	
	

	// -- Addon Documents Panel (END)

	// -- Additional Info Panel

	var addinfo = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		hidden        : true,
		layout        : 'form',
		id            : 'formOpt',
		title         : 'Additional Info',
		collapsible   : true,
		collapsed     : false,
		width         : 410,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'deposit',
				name          : 'deposit',
				fieldLabel    : 'Initial Deposit',
				style         : 'text-align:right',
				allowBlank    : true,
				vtype         : 'moneda'
			},{
				xtype         : 'textfield',
				id            : 'inspection',
				name          : 'inspection',
				fieldLabel    : 'Inspection Days',
				style         : 'text-align:right',
				emptyText     : 15,
				allowBlank    : true
			},{
				xtype         : 'datefield',
				id            : 'dateAcc',
				name          : 'dateAcc',
				width         : 100,
				fieldLabel    : 'Acceptance date',
				allowBlank    : true
			},{
				xtype         : 'datefield',
				id            : 'dateClo',
				name          : 'dateClo',
				width         : 100,
				fieldLabel    : 'Closing date',
				allowBlank    : true
			}
		]
	});




	// -- Change buyer

	var buyer  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		hidden        : true,
		layout        : 'form',
		id            : 'formBuy',
		title         : 'Change Buyer Info',
		width         : 410,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'buyername',
				name          : 'buyername',
				fieldLabel    : 'Buyer Name',
				allowBlank    : true
			}
		]
	});



	// -- Change seller

	var seller  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		hidden        : true,
		layout        : 'form',
		id            : 'formSel',
		title         : 'Change Seller Info',
		width         : 410,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'sellname',
				name          : 'sellname',
				fieldLabel    : 'Seller Name',
				allowBlank    : true
			}
		]
	});
	
	var panelSelling = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		hidden        : true,
		layout        : 'form',
		id            : 'panelSelling',
		title         : 'Change Info',
		width         : 410,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'sellingname',
				name          : 'sellingname',
				fieldLabel    : 'Seller Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'sellingaddress',
				name          : 'sellingaddress',
				fieldLabel    : 'Seller Address',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'buyername1',
				name          : 'buyername1',
				fieldLabel    : 'Buyer Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'buyeraddress',
				name          : 'buyeraddress',
				fieldLabel    : 'Buyer Address',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'agentname',
				name          : 'agentname',
				fieldLabel    : 'Agent Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'brokername',
				name          : 'brokername',
				fieldLabel    : 'Broker Name',
				allowBlank    : true
			}
		]
	});
	var checks  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		bodyStyle     : 'padding : 5px;',
		id            : 'formChecks', 
		width         : 430,
		items         : [
			{
				
				xtype   : 'checkboxgroup',
				columns : 3,
				items   : [
					{   
						id            : 'cbinfo',
						name          : 'cbinfo',
						boxLabel      : 'Change buyer info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('cbinfo').getValue();

								if (ad == true)
									Ext.getCmp('formBuy').show();
								else
									Ext.getCmp('formBuy').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'csinfo',
						name          : 'csinfo',
						boxLabel      : 'Change seller info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('csinfo').getValue();

								if (ad == true)
									Ext.getCmp('formSel').show();
								else
									Ext.getCmp('formSel').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'addons',
						name          : 'addons',
						boxLabel      : 'Additional Documents',
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('addons').getValue();
								var tp = Ext.getCmp('ctype').getValue();
								

								if (ad == true) {
									Ext.getCmp('addonGrid').store.load({params: {type : tp}});
									Ext.getCmp('formAddon').show();
								} else
									Ext.getCmp('formAddon').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'addendum',
						name          : 'addendum',
						boxLabel      : 'Addendum Pages',
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('addendum').getValue();

								if (ad == true)
									Ext.getCmp('formAddend').show();
								else
									Ext.getCmp('formAddend').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'scrow',
						name          : 'scrow',
						boxLabel      : 'Escrow Letter',
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
					},{
						xtype         : 'checkbox',
						id            : 'csellinginfo',
						name          : 'csellinginfo',
						boxLabel      : 'Change info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('csellinginfo').getValue();

								if (ad == true)
									Ext.getCmp('panelSelling').show();
								else
									Ext.getCmp('panelSelling').hide();

							}
						}
					}

				]
			}
		]
	});
	
	var checksDocuments  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		bodyStyle     : 'padding : 5px;',
		id            : 'formChecksDocuments', 
		width         : 430,
		hidden		  : true,	
		items         : [{
			xtype: 'compositefield',
			fieldLabel: 'Documents',
			items: [{
						xtype:'label',
						text:'Documents to be sent:',
						width: 150
					},{
						xtype   : 'checkboxgroup',
						columns : 2,
						itemCls: 'x-check-group-alt',
						width: 280,
						items   : [
							{boxLabel: 'Proof of Funds', name: 'pof'},
							{boxLabel: 'EMD', name: 'emd'},
							{boxLabel: 'Addendums', name: 'rademdums'}
						]
			}]
		}]
	});
	var checksComplete  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		id            : 'checksComplete', 
		width         : 430,
		labelWidth    : 130,
		hidden		  : true,	
		layout        : 'form',
		items         : [
		{
			xtype         : 'checkbox',
			id            : 'completetask',
			name          : 'completetask',
			fieldLabel    : 'Completed the Task Selected'
		}]
	});
	var panelMailSetting=new Ext.FormPanel({
								title      : 'Please Configure your Mail Settings',
								border     : true,
								bodyStyle  : 'padding: 10px; text-align:left;',
								frame      : true,
								fileUpload : true,
								method     : 'POST',
								items      : [
									{
										html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
										cls         : 'x-form-item',
										style       : {
											marginBottom : '10px'
										}
									},{
										xtype      : 'textfield',
										fieldLabel : 'Server Address',
										name       : 'server',
										width      : '200'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
									},{
										xtype      : 'textfield',
										fieldLabel : 'Port Number',
										name       : 'port',
										width      : '200',
										value      : '25'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
									},{
										xtype      : 'textfield',
										fieldLabel : 'Server Username',
										name       : 'username',
										width      : '200'
									},{
										xtype      : 'box',
										html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
									},{
										xtype      : 'textfield',
										inputType  : 'password',
										fieldLabel : 'Server Password',
										name       : 'password',
										width      : '200'
									}
								],
								buttonAlign :'center',
								buttons     : [{
									text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
									handler : function(){
										if (panelMailSetting.getForm().isValid()) {
											panelMailSetting.getForm().submit({
												url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
												waitMsg : 'Saving...',
												success : function(f, a){ 
													var resp = a.result;
													alert(resp.mensaje);
													if(resp.savesett=='yes'){
														winMailSett.hide();
														if(totalsent<50){
															totalenviados=0;
															totalnosent=0;
															progressbar_win.show();
															genFinalContract(0);
														}else{
															alert('Daily email contracts limit exceeded');
														}
													}
														
												}
											});
										}
									}
								}]
							});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	
	function genFinalContract(pid) {
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);

		selId = Ext.getCmp('addGrid').selModel.selections.items;
		var temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddeum').setValue(temp);

		selId = Ext.getCmp('addonGrid').selModel.selections.items;
		temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddonG').setValue(temp);
		Ext.getCmp('pids').setValue(selected[pid]);	
		generate.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				if(action.result.enviado=='1'){
					totalenviados=totalenviados+1;
					if(action.result.enviadoagente=='1'){
						totalsent=totalsent+1;
						mailenviados.setText('Total Sent '+(totalsent))
					}
				}else{
					totalnosent++;
				}
				if((pid)==selected.length-1){
					progressbar_win.hide();
					win.close();
					alert('Contracts sent '+totalenviados+'\nContracts not sent '+totalnosent);
					store.load({params:{start:0, limit:limitmyfollowup}});
					
				}else{
					if(totalsent<50){
						genFinalContract(pid+1);
					}else{
						alert('Daily email contracts limit exceeded');
						progressbar_win.hide();
						win.close();
						alert('Contracts sent '+totalenviados+'\nContracts not sent '+totalnosent);
						store.load({params:{start:0, limit:limitmyfollowup}});
					}
				}
			},
			failure : function(form, action) {
				progressbar_win.hide();
				win.close();
				Ext.MessageBox.alert('Warning', action.result.error);
			}
		});
	}
	
	var emailConfig = '0'; 
	var generate = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followgetcontract.php', 
		frame         : true,
		monitorValid  : true,
		id            : 'GenMainPanel',
		title         : 'Select Contract.',
		width         : 450,
		labelWidth    : 130,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype         : 'combo',
				name          : 'ctype',
				id            : 'ctype',
				hiddenName    : 'type',
				fieldLabel    : 'Contract',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'contracts',
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'tplactive', type:'int'},
						{name:'tpl1_name', type:'string'},
						{name:'tpl1_addr', type:'string'},
						{name:'tpl2_name', type:'string'},
						{name:'tpl2_addr', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							var sel=records[0].get('id');
							Ext.getCmp('ctype').setValue(sel); 
							if(selling==true){
								if(records[0].get('tplactive')==1){
									Ext.getCmp('sellingname').setValue(records[0].get('tpl1_name'));
									Ext.getCmp('sellingaddress').setValue(records[0].get('tpl1_addr'));
								}else{
									Ext.getCmp('sellingname').setValue(records[0].get('tpl2_name'));
									Ext.getCmp('sellingaddress').setValue(records[0].get('tpl2_addr'));
								}
							}
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(selling==true){
							if(record.get('tplactive')==1){
								Ext.getCmp('sellingname').setValue(record.get('tpl1_name'));
								Ext.getCmp('sellingaddress').setValue(record.get('tpl1_addr'));
							}else{
								Ext.getCmp('sellingname').setValue(record.get('tpl2_name'));
								Ext.getCmp('sellingaddress').setValue(record.get('tpl2_addr'));
							}
						}
					}
				}
			},{
				xtype         : 'combo',
				name          : 'coptions',
				id            : 'coptions',
				hiddenName    : 'options',
				fieldLabel    : 'Generate Options',
				typeAhead     : true,
				store         : contractseal,
				mode          : 'local',
				triggerAction : 'all',
				editable      : false,
				emptyText     : 'Select ...',
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value         : '0',
				width         : 260
			},{
				xtype         : 'hidden',
				name          : 'county',
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id			  : 'pids'
			},{
				xtype         : 'hidden',
				name          : 'mlnaux',
				id            : 'mlnaux'
			},{
				xtype         : 'hidden',
				name          : 'fieldAddeum',
				id            : 'fieldAddeum'
			},{
				xtype         : 'hidden',
				name          : 'fieldAddonG',
				id            : 'fieldAddonG'
			},{
				xtype         : 'hidden',
				name          : 'addr',
				id            : 'addr'
			},{
				xtype         : 'hidden',
				name          : 'sendtype',
				id            : 'sendtype'
			},
			checks,
			addend,
			addond,
			addinfo,
			buyer,
			seller,
			panelSelling,
			{
				xtype         : 'checkbox',
				id            : 'sendmail',
				name          : 'sendmail',
				fieldLabel    : 'Send Contract By Email', 
				listeners     : {
					'check'   : function () {

						var ad = Ext.getCmp('sendmail').getValue();
						var ad1 = Ext.getCmp('sendme').getValue();
						if (ad == true || ad1 == true){
							Ext.getCmp('formChecksDocuments').show();
							Ext.getCmp('sendbyfax').setValue(false);
							Ext.getCmp('sendBut').setVisible(true);
							Ext.getCmp('paneltemplatesemail').setVisible(true);
							if(completetask){
								Ext.getCmp('checksComplete').setVisible(true);
							}
						}else{
							Ext.getCmp('formChecksDocuments').hide();
							Ext.getCmp('sendBut').setVisible(false);
							Ext.getCmp('paneltemplatesemail').setVisible(false);
							Ext.getCmp('checksComplete').setVisible(false);
						}

					}
				}
			},{
				xtype         : 'checkbox',
				id            : 'sendme',
				name          : 'sendme',
				fieldLabel    : 'Send Copy to me',
				listeners     : {
					'check'   : function () {

						var ad = Ext.getCmp('sendme').getValue();
						var ad1 = Ext.getCmp('sendmail').getValue();
						if (ad == true || ad1 == true){
							Ext.getCmp('sendbyfax').setValue(false);
							Ext.getCmp('sendBut').setVisible(true);
							Ext.getCmp('paneltemplatesemail').setVisible(true);
						}else{
							Ext.getCmp('sendBut').setVisible(false);
							Ext.getCmp('paneltemplatesemail').setVisible(false);
						}
						

					}
				}
			},{
				xtype         : 'checkbox',
				id            : 'sendbyfax',
				name          : 'sendbyfax',
				fieldLabel    : 'Send Fax',
				listeners     : {
					'check'   : function () {

						var ad = Ext.getCmp('sendbyfax').getValue();
						if (ad == true){
							Ext.getCmp('sendme').setValue(false);
							Ext.getCmp('sendmail').setValue(false);
							Ext.getCmp('sendFax').setVisible(true);
							Ext.getCmp('paneltemplatesfax').setVisible(true);
						}else{
							Ext.getCmp('sendFax').setVisible(false);
							Ext.getCmp('paneltemplatesfax').setVisible(false);
						}				
						

					}
				}
			},
			checksComplete,
			checksDocuments,
			{
				xtype: 'panel',
				layout: 'form',
				hidden: true,
				id: 'paneltemplatesemail',	
				items: [{
					xtype         : 'combo',
					name          : 'ccontracttemplate',
					id            : 'ccontracttemplate',
					hiddenName    : 'contracttemplate',
					fieldLabel    : 'Template Email',
					typeAhead     : true,
					autoSelect    : true,
					mode          : 'local',
					store         : new Ext.data.JsonStore({
						root:'results',
						totalProperty:'total',
						autoLoad: true, 
						baseParams: {
							type: 'emailtemplates',
							'userid': userid
						},
						fields:[
							{name:'id', type:'int'},
							{name:'name', type:'string'},
							{name:'default', type:'int'}
						],
						url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						listeners     : {
							'load'  : function(store, records) {
								var sel=records[0].get('id');
								//Ext.getCmp('ccontracttemplate').setValue(sel); 
								for(i=1;i<records.length;i++){
									if(records[i].get('default')==1)
										sel=records[i].get('id');
								}
								Ext.getCmp('ccontracttemplate').setValue(sel); 
							}
						}
					}),
					triggerAction : 'all', 
					editable      : false,
					selectOnFocus : true,
					allowBlank    : false,
					displayField  : 'name',
					valueField    : 'id',
					value		  : 0,
					width         : 260
				}]
			},{
				xtype: 'panel',
				layout: 'form',
				hidden: true,
				id: 'paneltemplatesfax',	
				items: [{
					xtype         : 'combo',
					name          : 'ccontracttemplatefax',
					id            : 'ccontracttemplatefax',
					hiddenName    : 'contracttemplatefax',
					fieldLabel    : 'Template Fax',
					typeAhead     : true,
					autoSelect    : true,
					mode          : 'local',
					store         : new Ext.data.JsonStore({
						root:'results',
						totalProperty:'total',
						autoLoad: true, 
						baseParams: {
							type: 'docstemplates',
							template_type: 4,
							'userid': userid
						},
						fields:[
							{name:'id', type:'int'},
							{name:'name', type:'string'},
							{name:'default', type:'int'}
						],
						url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						listeners     : {
							'load'  : function(store, records) {
								loading_win.hide();
								var sel=records[0].get('id');
								//Ext.getCmp('ccontracttemplate').setValue(sel); 
								for(i=1;i<records.length;i++){
									if(records[i].get('default')==1)
										sel=records[i].get('id');
								}
								Ext.getCmp('ccontracttemplatefax').setValue(sel); 
							}
						}
					}),
					triggerAction : 'all', 
					editable      : false,
					selectOnFocus : true,
					allowBlank    : false,
					displayField  : 'name',
					valueField    : 'id',
					value		  : 0,
					width         : 260
				}]
			}
		],
		style       : "text-align:left",
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send Email',
				handler : function() {
					Ext.getCmp('sendtype').setValue('5');
					var sendmail=Ext.getCmp('sendmail').getValue();
					var sendme=Ext.getCmp('sendme').getValue();
					if(sendme || sendmail){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php',
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									if(totalsent<50){
										totalenviados=0;
										totalnosent=0;
										progressbar_win.show();
										genFinalContract(0);
									}else{
										alert('Daily email contracts limit exceeded');
									}
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
					//genFinalContract(0);
				},
				id      : 'sendBut',
				hidden  : true
			},{
				text    : 'Send Fax',
				handler : function() {
					Ext.getCmp('sendtype').setValue('3');
					var sendbyfax=Ext.getCmp('sendbyfax').getValue();
					if(sendbyfax){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php',
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									if(totalsent<50){
										totalenviados=0;
										totalnosent=0;
										progressbar_win.show();
										genFinalContract(0);
									}else{
										alert('Daily email contracts limit exceeded');
									}
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
					//genFinalContract(0);
				},
				id      : 'sendFax',
				hidden  : true
			},{
				text    : 'Cancel',
				handler : function(){
					win.close();
				}
			}
		]
	});
	
	var message = "<div style=\"padding:10px 0 10px 30px\">In order to be able to edit the ";
	message += "contracts you must have Abobe Reader Pro.<br>";
	message += "You can also do it with the following  free ";
	message += "softwares FoxitReader at ";
	message += "<a href='http://www.foxitsoftware.com/pdf/reader/' target='_new'>";
	message += "http://www.foxitsoftware.com/pdf/reader/</a> ";
	message += "and PDF-XChange viewer at ";
	message += "<a href='http://www.tracker-software.com/product/pdf-xchange-viewer' target='_new'>";
	message += "http://www.tracker-software.com/product/pdf-xchange-viewer</a></div>";
	
	var dismessage="<div style=\"padding:10px 0 10px 30px\">WARNING<br/>";
	dismessage+="The following Document Generator feature of REIFAX.com (herein after known as REIFAX) is for the sole purpose of modifying existing documents that the user uploads to the system.";
	dismessage+="No documents will be supplied by REIFAX .";
	dismessage+="The user MUST HAVE legal authority to upload, modify, change, fill-in and use any document he intends to modify and submit to another buyer or seller.";
	dismessage+="Legal authority means that if a copyrighted document is uploaded, the user MUST HAVE written permission of the person or entity who drafted the document, or have paid an authorized re-seller for the use of that document, i.e. FAR/BAR forms.";
	dismessage+="By uploading any document, the user hereby agrees to these terms and conditions for using the REIFAX Document Generator.";
	dismessage+="Violation of these terms and conditions could result in the user violating copyright laws.";
	dismessage+="The only information that will be filled in by REIFAX, on any form, if applicable is information that is available in the public record.";
	dismessage+="Any and all other information will have to be supplied by the user.</div>";
	var disclaimer = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		items       : [
			{ xtype: "panel", html: dismessage }
		],
		buttons : [{
				text     : 'Accept',
				handler  : function(){
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php',
						method  : 'POST',
						params: { module:'acceptcontract',
								  action:'accept'	
						},
						waitMsg : 'Wait...',
						success : function(r) {
							var resp   = Ext.decode(r.responseText)
							if(resp.msg=='true'){
								disclaimer.close();
								Ext.getCmp('dateAcc').setValue(fechaAcc);
								Ext.getCmp('dateClo').setValue(fechaClo);
								win.show();
							}
						}
					});
					disclaimer.close();
				}
			},{
				text     : 'Decline',
				handler  : function(){
					disclaimer.close();
				}
			}]
	});
	var win = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				generate.getForm().reset();
			}
		},
		items       : [
			generate,
			{ xtype: "panel", html: message }
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				win.close();
			}
		}]
	});
	//Verificar que haya aceptado el disclaimer
	Ext.Ajax.request({
		url     : 'overview_contract/verifications.php',
		method  : 'POST',
		params: { module:'acceptcontract',
				  action:'verificate'	
		},
		waitMsg : 'Wait...',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)
			if(resp.msg=='true'){
				win.show();
				Ext.getCmp('dateAcc').setValue(fechaAcc);
				Ext.getCmp('dateClo').setValue(fechaClo);
				Ext.getCmp('formOpt').show();
				Ext.getCmp('csinfo').show();
				Ext.getCmp('cbinfo').show();
				Ext.getCmp('coptions').setValue(1);
				if(selling==true){
					Ext.getCmp('csellinginfo').show();
					Ext.getCmp('csellinginfo').setValue(true);
				}
			}else{
				disclaimer.show();
			}
		}
	});	
}

function sendEmail(selected,grid,store,userid, progressbar_win, progressbar){
	var ind = 1;
	var att = {
		xtype: 'compositefield',
		fieldLabel: 'Attach',
		items: [
			{
				xtype: 'fileuploadfield',
				emptyText: 'Select a file',
				name: 'archivo'+ind,
				buttonText: 'Browse',
				width: 200
			},new Ext.Button({
				text: 'Another file',
				handler: function(){
					att.items[0].name = 'archivo'+(ind+1);

					formEmail.insert(ind,att);
					ind++;
					formEmail.doLayout();
				}
			})
		]
	};
	
	var panelMailSetting=new Ext.FormPanel({
		title      : 'Please Configure your Mail Settings',
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		fileUpload : true,
		method     : 'POST',
		items      : [
			{
				html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
				cls         : 'x-form-item',
				style       : {
					marginBottom : '10px'
				}
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				width      : '200',
				value      : '25'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Username',
				name       : 'username',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
			},{
				xtype      : 'textfield',
				inputType  : 'password',
				fieldLabel : 'Server Password',
				name       : 'password',
				width      : '200'
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
			handler : function(){
				if (panelMailSetting.getForm().isValid()) {
					panelMailSetting.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							alert(resp.mensaje);
							if(resp.savesett=='yes'){
								totalenviados=0;
								totalnosent=0;
								winMailSett.hide();
								progressbar_win.show();	
								sendMailTemplate(0);
							}
						}
					});
				}
			}
		}]
	});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	function sendMailTemplate(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		if(pid+1==selected.length)
			Ext.getCmp('action-delete').setValue('delete');
		Ext.getCmp('pid').setValue(selected[pid]);
		
		formEmail.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				if(action.result.enviado=='1'){
					totalenviados=totalenviados+1;
				}else{
					totalnosent++;
				}
				if((pid)==selected.length-1){
					progressbar_win.hide();
					winTemplate.close();
					alert('Mails sent '+totalenviados+'\nMails not sent '+totalnosent);
					store.load({params:{start:0, limit:limitmyfollowup}});
				}else{
					Ext.getCmp('action-upload').setValue('');
					sendMailTemplate(pid+1);
				}
			},
			failure : function(form, action) {
				progressbar_win.hide();
				winTemplate.close();
				Ext.MessageBox.alert('Warning', action.result.msg);
			}
		});
	}
	var formEmail = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendmail.php', 
		frame         : true,
		monitorValid  : true,
		fileUpload : true,
		id            : 'formEmail',
		title         : 'Select Template.',
		width         : 450,
		labelWidth    : 130,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'emailtemplates',
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel); 
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260
			},{
				xtype         : 'checkbox',
				id            : 'sendmail',
				name          : 'sendmail',
				fieldLabel    : 'Send to Contact',
				listeners     : {
					'check'   : function () {


					}
				}
			},{
				xtype         : 'checkbox',
				id            : 'sendme',
				name          : 'sendme',
				fieldLabel    : 'Send Copy to me'
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					var sendmail=Ext.getCmp('sendmail').getValue();
					var sendme=Ext.getCmp('sendme').getValue();
					if(sendme || sendmail){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php', 
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									totalenviados=0;
									totalnosent=0;
									progressbar_win.show();
									Ext.getCmp('action-upload').setValue('upload');
									Ext.getCmp('cantidadupload').setValue(ind);
									sendMailTemplate(0);
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
				},
				id      : 'sendBut',
				hidden  : false
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	formEmail.insert(ind,att);
	var winTemplate = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				formEmail.getForm().reset();
			}
		},
		items       : [
			formEmail
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winTemplate.close();
			}
		}]
	});
	winTemplate.show();
}

function sendEmailSms(selected,grid,store,userid, progressbar_win, progressbar){
	function sendSms(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		Ext.getCmp('pid').setValue(selected[pid]);
		
		formSms.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				if(action.result.enviado=='1'){
					totalenviados=totalenviados+1;
				}else{
					totalnosent++;
				}
				if((pid)==selected.length-1){
					progressbar_win.hide();
					winTemplate.close(); 
					alert('SMS sent '+totalenviados+'\nSMS not sent '+totalnosent);
					store.load({params:{start:0, limit:limitmyfollowup}});
				}else{
					Ext.getCmp('action-upload').setValue('');
					sendSms(pid+1);
				}
			},
			failure : function(form, action) {
				progressbar_win.hide();
				winTemplate.close();
				Ext.MessageBox.alert('Warning', action.result.msg);
			}
		});
	}
	var panelMailSetting=new Ext.FormPanel({
		title      : 'Please Configure your Mail Settings',
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		fileUpload : true,
		method     : 'POST',
		items      : [
			{
				html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
				cls         : 'x-form-item',
				style       : {
					marginBottom : '10px'
				}
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				width      : '200',
				value      : '25'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Username',
				name       : 'username',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
			},{
				xtype      : 'textfield',
				inputType  : 'password',
				fieldLabel : 'Server Password',
				name       : 'password',
				width      : '200'
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
			handler : function(){
				if (panelMailSetting.getForm().isValid()) {
					panelMailSetting.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							alert(resp.mensaje);
							if(resp.savesett=='yes'){
								totalenviados=0;
								totalnosent=0;
								winMailSett.hide();
								progressbar_win.show();	
								sendSms(0);
							}
						}
					});
				}
			}
		}]
	});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	var formSms = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendsms.php', 
		frame         : true,
		autoHeight  : true,
		monitorValid  : true,
		id            : 'formSms',
		title         : 'Select Template.',
		width         : 450,
		labelWidth    : 100,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'docstemplates',
						template_type: 3,
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							loading_win.hide();
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel);
							if(sel!=0){
								Ext.getCmp('sms_newtemplate').setVisible(false);
							}
							winTemplate.syncSize();
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260,
				listeners     : {
					'select'  : function(combo,record,index){
						var valor = record.get('id');
						if(valor==0){
							Ext.getCmp('sms_newtemplate').setVisible(true);
						}else{
							Ext.getCmp('sms_newtemplate').setVisible(false);
						}
						winTemplate.syncSize();
					}
				}
			},{
				xtype: 'panel',
				layout: 'form',
				id: 'sms_newtemplate',	
				items: [{
					xtype	  :	'textarea',
					height	  : 200,
					width	  : 300,
					name	  : 'body',
					id		  : 'sms_newtemplatetext',	
					name	  : 'sms_newtemplatetext',	
					fieldLabel: 'Text',
					enableKeyEvents: true,
					autoScroll: true
				}]
			},{
				xtype: 'panel',
				layout: 'form',
				id: 'checkssms',
				hidden: true,
				labelWidth    : 180,	
				items: [{
					xtype         : 'checkbox',
					id            : 'sendcell',
					name          : 'sendcell',
					fieldLabel    : 'Send To Cell Phone only'
				},{
					xtype         : 'checkbox',
					id            : 'sendany',
					name          : 'sendany',
					fieldLabel    : 'Send to Cell Phone or first Available'
				}]
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					loading_win.show();
					var sendcell= true; //Ext.getCmp('sendcell').getValue();
					var sendany=true; //Ext.getCmp('sendany').getValue();
					if(sendcell || sendany){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php', 
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								loading_win.hide();
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									totalenviados=0;
									totalnosent=0;
									progressbar_win.show();
									sendSms(0);
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
				},
				id      : 'sendBut',
				hidden  : false
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	var winTemplate = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				formSms.getForm().reset();
			}
		},
		items       : [
			formSms
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winTemplate.close();
			}
		}]
	});
	winTemplate.show(); 
	loading_win.show();
}

function sendEmailFax(selected,grid,store,userid, progressbar_win, progressbar){
	var ind = 1;
	var att = {
		xtype: 'compositefield',
		fieldLabel: 'Attach',
		items: [
			{
				xtype: 'fileuploadfield',
				emptyText: 'Select a file',
				name: 'archivo'+ind,
				buttonText: 'Browse',
				width: 200
			},new Ext.Button({
				text: 'Another file',
				handler: function(){
					att.items[0].name = 'archivo'+(ind+1);

					formEmail.insert(ind,att);
					ind++;
					formEmail.doLayout();
				}
			})
		]
	};
	
	var panelMailSetting=new Ext.FormPanel({
		title      : 'Please Configure your Mail Settings',
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		fileUpload : true,
		method     : 'POST',
		items      : [
			{
				html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
				cls         : 'x-form-item',
				style       : {
					marginBottom : '10px'
				}
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				width      : '200',
				value      : '25'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Username',
				name       : 'username',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
			},{
				xtype      : 'textfield',
				inputType  : 'password',
				fieldLabel : 'Server Password',
				name       : 'password',
				width      : '200'
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
			handler : function(){
				if (panelMailSetting.getForm().isValid()) {
					panelMailSetting.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							alert(resp.mensaje);
							if(resp.savesett=='yes'){
								totalenviados=0;
								totalnosent=0;
								winMailSett.hide();
								progressbar_win.show();	
								sendMailTemplate(0);
							}
						}
					});
				}
			}
		}]
	});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	function sendMailTemplate(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		if(pid+1==selected.length)
			Ext.getCmp('action-delete').setValue('delete');
		Ext.getCmp('pid').setValue(selected[pid]);
		
		formEmail.getForm().submit({
			timeout : 60,
			success : function(form, action) {
				if(action.result.enviado=='1'){
					totalenviados=totalenviados+1;
				}else{
					totalnosent++;
				}
				if((pid)==selected.length-1){
					progressbar_win.hide();
					winTemplate.close();
					alert('Fax sent '+totalenviados+'\nFax not sent '+totalnosent);
					store.load({params:{start:0, limit:limitmyfollowup}});
				}else{
					Ext.getCmp('action-upload').setValue('');
					sendMailTemplate(pid+1);
				}
			},
			failure : function(form, action) {
				progressbar_win.hide();
				//winTemplate.close();
				Ext.MessageBox.alert('Warning', action.result.msg);
			}
		});
	}
	var newtemplate_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		title         : 'New Template.',
		frame      : true,
		method     : 'POST',
		labelWidth: 60,
		items      : [
			{
				xtype:'htmleditor',
				id: 'newtemplateeditorfax',
				fieldLabel : 'Body',
				autoScroll: true,
				height:450,
				width: 800,
				plugins: [
						  new Ext.ux.form.HtmlEditor.Word()  
						 ],
				name: 'body',    
				enableSourceEdit : false
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : 'Send',
			handler : function(){
				totalenviados=0;
				totalnosent=0;	
				Ext.getCmp('nuevotemplate').setValue(Ext.getCmp('newtemplateeditorfax').getValue()); 
				progressbar_win.show();
				//winTemplate.close();
				winNewTemplate.close();
				sendMailTemplate(0);
			}
		},{
			text    : 'Cancel',
			handler : function(){
				winNewTemplate.close();
			}
		}]
	});
	var variableInsertarNewFax = '';
		
	
	var winNewTemplate = new Ext.Window({
		//width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				newtemplate_form.getForm().reset();
			}
		},
		items       : [
			newtemplate_form
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winNewTemplate.close();
			}
		}]
	});
	var formEmail = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendfax.php', 
		frame         : true,
		monitorValid  : true,
		fileUpload : true,
		id            : 'formEmail',
		title         : 'Select Template.',
		width         : 450,
		labelWidth    : 130,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'docstemplates',
						template_type: 4,
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							loading_win.hide();
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel); 
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'nuevotemplate',
				id            : 'nuevotemplate'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					var sendmail=true; //Ext.getCmp('sendmail').getValue();
					var sendme= true; //Ext.getCmp('sendme').getValue();
					if(sendme || sendmail){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php', 
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									
									Ext.getCmp('action-upload').setValue('upload');
									Ext.getCmp('cantidadupload').setValue(ind);
									
									if(Ext.getCmp('ccontracttemplate').getValue()==0){
										
										winNewTemplate.show();
										Ext.getCmp("newtemplateeditorfax").getToolbar().addItem([{
											xtype: 'tbseparator'
										}]);
										
										Ext.getCmp("newtemplateeditorfax").getToolbar().addItem([{
											xtype         : 'combo',
											mode          : 'local',
											fieldLabel    : 'Vars',
											triggerAction : 'all',
											displayField  : 'name',
											valueField    : 'id',
											name          : 'variableInsertarNewFax',
											id          : 'variableInsertarNewFax',
											value         : variableInsertarNewFax,
											allowBlank    : false,
											editable	  : false,
											width: 		    200,
											store         : new Ext.data.JsonStore({
												root:'results',
												totalProperty:'total',
												autoLoad: true, 
												baseParams: {
													type: 'template-variables'
												},
												fields:[
													{name:'id', type:'string'},
													{name:'name', type:'string'}
												],
												url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
												listeners     : {
													'load'  : function(store, records) {
														Ext.getCmp('variableInsertarNewFax').setValue(variableInsertarNewFax); 
													}
												}
											}),
											listeners	  : {
												'select'  : function(combo,record,index){
													variableInsertarNewFax = record.get('id');
												}
											}
										}]);
										Ext.getCmp("newtemplateeditorfax").getToolbar().addButton([{
											iconCls: 'icon', 
											icon: 'http://www.reifax.com/img/add.gif', 
											handler: function () {
												Ext.getCmp("newtemplateeditorfax").insertAtCursor(variableInsertarNewFax);
											},
											tooltip: 'Insert Variable'
										}]);
									}else{
										totalenviados=0;
										totalnosent=0;
										progressbar_win.show();
										sendMailTemplate(0);
									}
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
				},
				id      : 'sendBut',
				hidden  : false
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	formEmail.insert(ind,att);
	var winTemplate = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				formEmail.getForm().reset();
			}
		},
		items       : [
			formEmail
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winTemplate.close();
			}
		}]
	});
	winTemplate.show(); 
	loading_win.show();
}

function createFollowHistory(grid,indexRow,tabFollowing,idPanelHistory){
	var record = grid.getStore().getAt(indexRow);
	var pid = record.get('pid');
	var county = record.get('county');
	var status = record.get('status');
	
	if(document.getElementById(idPanelHistory)){
		var tab = tabsFollow.getItem(idPanelHistory);
		tabsFollow.remove(tab);
	}
	
}

function getContacts(selected,grid,store,userid, progressbar_win, progressbar, limitfollow){ 
	Ext.MessageBox.show({
	   title:    'Contacts',
	   msg:      'Overwrite existing contacts?',
	   buttons: {yes: 'Yes', no: 'No',cancel: 'Cancel'},
	   fn: updateContacts
	});
	function updateContacts(btn){
		if(btn=='cancel'){
			return;
		}
		urlProgressBar = 'mysetting_tabs/myfollowup_tabs/properties_getcontacts.php?updatecontact='+btn;
		countProgressBar = 0;
		typeProgressBar = 'Contact';
		progressbar.updateProgress(
			((countProgressBar+1)/selected.length),
			"Processing "+(countProgressBar+1)+" of "+selected.length
		);
		progressbar_win.show();
		var pidssend=selected[countProgressBar];
		countProgressBar++;

		Ext.Ajax.request({  
			waitMsg: 'Checking...',
			url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
			method: 'POST',
			timeout :120000, 
			params: { 
				userid:userid,
				pids: pidssend
			},
					
			failure:function(response,options){
				progressbar_win.hide();
				Ext.MessageBox.alert('Warning','Operation Failure');
			},
			success:function(response,options){								
				var respresiden = Ext.decode(response.responseText);

				if(respresiden.mlsresidential==true || respresiden.num==0 || respresiden.num=='0')
				{
					Ext.Ajax.request({  
						waitMsg: 'Checking...',
						url: urlProgressBar, 
						method: 'POST',
						timeout :120000, 
						params: { 
							pids: '\''+pidssend+'\''
							,'agent':respresiden.agent
							,'agentph':respresiden.agentph
							,'agentcell':respresiden.agentcell
							,'agentfax':respresiden.agentfax
							,'agentemail':respresiden.agentemail
							,'agenttollfree':respresiden.agenttollfree
							,'office':respresiden.office
							,'officeph':respresiden.officeph
							,'officefax':respresiden.officefax
							,'officeemail':respresiden.officeemail
							,'officetollfree':respresiden.officetollfree										
							,'runspider':respresiden.runspider										
							,'mlsresidential':respresiden.mlsresidential										
						},										
						failure:function(response1,options1){
							progressbar_win.hide();
							var output = '';
							for (prop in response1) {
							  output += prop + ': ' + response1[prop]+'; ';
							}
							Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
						},
						success: ejecutarUpdatesGetContact
					});										
				}
				else
				{							
					var client = new XMLHttpRequest();
					client.onreadystatechange = function () {
						if (this.readyState == 4 && this.status == 200) {
							progressbar_win.hide();
							var resp = Ext.decode(this.responseText);
							
							Ext.Ajax.request({  
								waitMsg: 'Checking...',
								url: urlProgressBar, 
								method: 'POST',
								timeout :120000, 
								params: { 
									pids: '\''+pidssend+'\''
									,'agent':resp.agent
									,'agentph':resp.agentph
									,'agentcell':resp.agentcell
									,'agentfax':resp.agentfax
									,'agentemail':resp.agentemail
									,'agenttollfree':resp.agenttollfree
									,'office':resp.office
									,'officeph':resp.officeph
									,'officefax':resp.officefax
									,'officeemail':resp.officeemail
									,'officetollfree':resp.officetollfree										
									,'runspider':resp.runspider										
									,'mlsresidential':resp.mlsresidential										
								},
								
								failure:function(response2,options2){
									progressbar_win.hide();
									var output = '';
									for (prop in response2) {
									  output += prop + ': ' + response2[prop]+'; ';
									}
									Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
								},
								success: ejecutarUpdatesGetContact
							});										
						}
					};
					client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+userid+'&p='+pidssend);
					client.send();									
				}//else if(resp.inresidential==true)
			}
		});						
	}
	function ejecutarUpdatesGetContact(response,options){
		progressbar.updateProgress(
			((countProgressBar+1)/selected.length),
			"Processing "+(countProgressBar+1)+" of "+selected.length
		);		
		progressbar_win.show();

		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			if((countProgressBar)==selected.length){
				Ext.Msg.alert('Success','Operation completed');
				progressbar_win.hide();
				store.load({params:{start:0, limit:limitfollow}});
			}else{
				var pidssend=selected[countProgressBar];
				countProgressBar++;

						Ext.Ajax.request({  
							waitMsg: 'Checking...',
							url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
							method: 'POST',
							timeout :120000, 
							params: { 
								userid:useridspider,
								pids: pidssend
							},
									
							failure:function(response,options){
								progressbar_win.hide();
								Ext.MessageBox.alert('Warning','Operation Failure');
							},
							success:function(response,options){								
								var respresiden = Ext.decode(response.responseText);

								if(respresiden.mlsresidential==true || respresiden.num==0 || respresiden.num=='0')
								{
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url: urlProgressBar, 
										method: 'POST',
										timeout :120000, 
										params: { 
											pids: '\''+pidssend+'\''
											,'agent':respresiden.agent
											,'agentph':respresiden.agentph
											,'agentcell':respresiden.agentcell
											,'agentfax':respresiden.agentfax
											,'agentemail':respresiden.agentemail
											,'agenttollfree':respresiden.agenttollfree
											,'office':respresiden.office
											,'officeph':respresiden.officeph
											,'officefax':respresiden.officefax
											,'officeemail':respresiden.officeemail
											,'officetollfree':respresiden.officetollfree										
											,'runspider':respresiden.runspider										
											,'mlsresidential':respresiden.mlsresidential										
										},										
										failure:function(response1,options1){
											progressbar_win.hide();
											var output = '';
											for (prop in response1) {
											  output += prop + ': ' + response1[prop]+'; ';
											}
											Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
										},
										success:ejecutarUpdatesGetContact
									});										
								}
								else
								{							
									var client = new XMLHttpRequest();
									client.onreadystatechange = function () {
										if (this.readyState == 4 && this.status == 200) {
											progressbar_win.hide();
											var resp = Ext.decode(this.responseText);
											
											Ext.Ajax.request({  
												waitMsg: 'Checking...',
												url: urlProgressBar, 
												method: 'POST',
												timeout :120000, 
												params: { 
													pids: '\''+pidssend+'\''
													,'agent':resp.agent
													,'agentph':resp.agentph
													,'agentcell':resp.agentcell
													,'agentfax':resp.agentfax
													,'agentemail':resp.agentemail
													,'agenttollfree':resp.agenttollfree
													,'office':resp.office
													,'officeph':resp.officeph
													,'officefax':resp.officefax
													,'officeemail':resp.officeemail
													,'officetollfree':resp.officetollfree										
													,'runspider':resp.runspider										
													,'mlsresidential':resp.mlsresidential										
												},
												
												failure:function(response2,options2){
													progressbar_win.hide();
													var output = '';
													for (prop in response2) {
													  output += prop + ': ' + response2[prop]+'; ';
													}
													Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
												},
												success:ejecutarUpdatesGetContact
											});										
										}
									};
									client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+useridspider+'&p='+pidssend);
									client.send();							
								}//else if(resp.inresidential==true)
							}
						});						
				
				
			}
		}else{
			progressbar_win.hide();
		}	
	}
}