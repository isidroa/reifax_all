<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmailOutbox_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowmail_progressBar"></div>
        <div id="myfollowmail_propertiesOutbox" align="left"></div> 
	</div>
</div>
<script>
	var storemyfollowmailOutbox = null;
	var limitmyfollowmailOutbox = 50;
Ext.onReady(function() {
	var selected_datamyfollowmailOutbox 	= new Array();
	var AllCheckmyfollowmailOutbox 		= false;
	var totalMailOutbox					= 0;
	var currentMailOutbox				= 0;
	var sincMailOutbox					= 1;
	
	//filter variables
	var filterEmailMyfollowMailOutbox 		= '';
	var filterNameMyfollowMailOutbox 		= '';
	var filterContentMyfollowMailOutbox 	= '';
	var filterTypeMyfollowMailOutbox 		= -1;
	var filterDateAfMyfollowMailOutbox 		= '';
	var filterDateBeMyfollowMailOutbox		= '';
	var filterFieldMyfollowMailOutbox		= 'fromName_msg';
	var filterDirectionMyfollowMailOutbox	= 'ASC';

	storemyfollowmailOutbox = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'to_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'agent', type: 'string'},
			{name: 'ap', type: 'string'},
			{name: 'address', type: 'string'},
			{name: 'task', type: 'int'}
	    ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': 		<?php echo $userid;?>,
			'typeEmail':	1,
			'checkmail': 	0,
			'currentMail': 	currentMailOutbox,
			'totalMail':  	totalMailOutbox
		},
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				if(sincMailOutbox==1) progressBarOutboxWin.show();
				
				obj.params.toemail 	= filterEmailMyfollowMailOutbox;
				obj.params.name		= filterNameMyfollowMailOutbox;
				obj.params.content	= filterContentMyfollowMailOutbox;
				obj.params.dateAf	= filterDateAfMyfollowMailOutbox;
				obj.params.dateBe	= filterDateBeMyfollowMailOutbox;
				obj.params.etype	= filterTypeMyfollowMailOutbox;
			}, 
			load: function(store,records,opt){
				currentMailOutbox = store.reader.jsonData.currentMail;
				if(totalMailOutbox == 0) totalMailOutbox = store.reader.jsonData.totalMail;

				if(currentMailOutbox<totalMailOutbox){
					
					progressBarOutbox.updateProgress(
						(currentMailOutbox/totalMailOutbox),
						"Downloading emails "+currentMailOutbox+" of "+totalMailOutbox
					);
					
					storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox, currentMail:currentMailOutbox, totalMail:totalMailOutbox,checkmail:sincMailOutbox}});
				}else{
					progressBarOutboxWin.hide();
					progressBarOutbox.updateProgress(
						0,
						"Initializing download emails..."
					);
					currentMailOutbox = 0;
					totalMailOutbox = 0;
					sincMailOutbox=0;
				}
			}
		}
    });
	
	var progressBarOutbox = new Ext.ProgressBar({
		text: "Initializing download emails..."
	});
	
	var progressBarOutboxWin=new Ext.Window({
		title: 'Sync Email, please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarOutbox]
	});

	var smmyfollowmailOutbox = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmailOutbox.indexOf(record.get('idmail'))==-1)
					selected_datamyfollowmailOutbox.push(record.get('idmail'));
				
				if(Ext.fly(gridmyfollowmailOutbox.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailOutbox=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmailOutbox = selected_datamyfollowmailOutbox.remove(record.get('idmail'));
				AllCheckmyfollowmailOutbox=false;
				Ext.get(gridmyfollowmailOutbox.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}		
	});
	var gridmyfollowmailOutbox = new Ext.grid.EditorGridPanel({
		renderTo: 'myfollowmail_propertiesOutbox',
		cls: 'grid_comparables',
		height: 400,
		store: storemyfollowmailOutbox,
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowmailOutbox
			,{header: 'T', width: 25, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
			,{header: 'A', width: 20, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
			,{header: 'P', width: 20, sortable: true, align: 'center', dataIndex: 'ap', renderer: assignmentProperty}
			,{id: 'idmail', header: "To Mail", width: 150, align: 'left', sortable: true, dataIndex: 'to_msg'}
		    ,{header: "Date", width: 120, sortable: true, align: 'center', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'), dataIndex: 'msg_date'}		
			,{header: 'Subject', width: 400, sortable: true, align: 'left', dataIndex: 'subject'}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowmailOutbox',
            pageSize: 		limitmyfollowmailOutbox,
            store: 			storemyfollowmailOutbox,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display'
		}),
		listeners: {
			'sortchange': function (grid, sorted){
				filterFieldMyfollowMailOutbox		= sorted.field;
				filterDirectionMyfollowMailOutbox	= sorted.direction;
			},
			'rowdblclick': function(grid, rowIndex, e){
				Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
				var record = grid.getStore().getAt(rowIndex);
				
				viewMailDetail(record.get('idmail'),record.get('userid'),record.get('ap'),tabsFollowEmails);
			}
		},
		sm: smmyfollowmailOutbox,
		frame:false,
		loadMask:true,
		border: false
	});
	
	var OutboxTabBar = new Ext.Toolbar({
		id: 'followOutboxTbar',
		items:[
			/*new Ext.Button({
				//tooltip: 'Click to sync email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/refresh25.png',
				handler: function(){
					totalMailOutbox=0;
					currentMailOutbox=0;
					sincMailOutbox=1;
					storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox, currentMail:currentMailOutbox, totalMail:totalMailOutbox,checkmail:sincMailOutbox}});
				}
			}),*/
			new Ext.Button({
				tooltip: 'Click to delete email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowmailOutbox.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the emails to be eliminated.'); return false;
					}
					loading_win.show();
					
					var pids=selected_datamyfollowmailOutbox[0];
					for(i=1; i<selected_datamyfollowmailOutbox.length; i++)
						pids+=','+selected_datamyfollowmailOutbox[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							selected_datamyfollowmailOutbox 	= new Array();
							storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
							Ext.Msg.alert("My email - Outbox", 'Emails deleted.');
							
						}                                
					});
				}
			}),
			new Ext.Button({
				tooltip: 'Click to filter emails.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					
					var formmyfollowmailOutbox = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						id: 'formmyfollowmailOutbox',
						name: 'formmyfollowmailOutbox',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'To mail',
									name		  : 'femail',
									value		  : filterEmailMyfollowMailOutbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterEmailMyfollowMailOutbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email Content',
									name		  : 'fcontent',
									value		  : filterContentMyfollowMailOutbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterContentMyfollowMailOutbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdateb',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'Date Between',
									name		  : 'fdateb',
									format		  : 'Y-m-d',
									value		  : filterDateBeMyfollowMailOutbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateBeMyfollowMailOutbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdatea',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'To',
									name		  : 'fdatea',
									format		  : 'Y-m-d',
									value		  : filterDateAfMyfollowMailOutbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateAfMyfollowMailOutbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Email Type',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											[-1,'All'],
											[1,'SMS'],
											[3,'Fax'],
											[5,'Email'],
											[15,'Voice Mail']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'etypename',
									value         : filterTypeMyfollowMailOutbox,
									hiddenName    : 'etype',
									hiddenValue   : filterTypeMyfollowMailOutbox,
									allowBlank    : false,
									width		  : 150,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterTypeMyfollowMailOutbox = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(b){
									storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
									b.findParentByType('window').close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(b){
									filterEmailMyfollowMailOutbox 	= '';
									filterNameMyfollowMailOutbox 	= '';
									filterContentMyfollowMailOutbox = '';
									filterDateAfMyfollowMailOutbox 	= '';
									filterDateBeMyfollowMailOutbox	= '';
									filterTypeMyfollowMailOutbox	= -1;
									
									b.findParentByType('form').getForm().reset();
									
									storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
									b.findParentByType('window').close();
								}
							},{
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Close&nbsp;&nbsp; ',
								handler  	  : function(b){
									b.findParentByType('window').close();
								}
							}
						]
					});
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 650,
						height      : 220,
						modal	 	: true,  
						plain       : true,
						items		: formmyfollowmailOutbox,
						closeAction : 'close'
					});
					win.show();
				}
			})/*,
			new Ext.Button({
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/contactAssign.png',
				handler: function(){
					if(selected_datamyfollowmailOutbox.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the email to be assigned.'); return false;
					}else if(selected_datamyfollowmailOutbox.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one email to be assigned.'); return false;
					}
					
					var idmail = selected_datamyfollowmailOutbox[0];
					var record = storemyfollowmailOutbox.getAt(storemyfollowmailOutbox.find('idmail',idmail));
					
					var newContact  = new Ext.Panel({
						xtype         : 'panel',
						border        : true,
						bodyStyle     : 'padding : 5px; border: 2px solid #cccccc',
						hidden        : false,
						layout        : 'form',
						id            : 'formNC',
						title         : 'New Contact Information',
						width         : 340,
						labelWidth    : 70,
						items         : [
							{
								xtype     : 'textfield',
								name      : 'agent',
								fieldLabel: 'Contact',
								allowBlank: false,
								value	  : record.get('fromName_msg')
							},{
								xtype         : 'combo',
								mode          : 'remote',
								fieldLabel    : 'Type',
								triggerAction : 'all',
								width		  : 130,
								store         : new Ext.data.JsonStore({
									id:'storetype',
									root:'results',
									totalProperty:'total',
									baseParams: {
										type: 'agenttype',
										'userid': <?php echo $userid;?>
									},
									fields:[
										{name:'idtype', type:'string'},
										{name:'name', type:'string'}
									],
									url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
								}),
								displayField  : 'name',
								valueField    : 'idtype',
								name          : 'fagenttype',
								value         : 'Agent',
								hiddenName    : 'agenttype',
								hiddenValue   : '1',
								allowBlank    : false,
								listeners	  : {
									beforequery: function(qe){
										delete qe.combo.lastQuery;
									}
								}
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Email',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyemail1',
									value         : '0',
									hiddenName    : 'typeemail1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'email',
									width	  : 165,
									value	  : record.get('from_msg') 
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Email 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyemail2',
									value         : '0',
									hiddenName    : 'typeemail2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'email2',
									width	  : 165
								}]
							},{
								xtype     : 'textfield',
								name      : 'company',
								fieldLabel: 'Company'
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Website 1',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Personal'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyurl1',
									value         : '0',
									hiddenName    : 'typeurl1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'urlsend',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Website 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Personal'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyurl2',
									value         : '0',
									hiddenName    : 'typeurl2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'urlsend2',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname1',
									value         : '0',
									hiddenName    : 'typeph1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone1',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname2',
									value         : '0',

									hiddenName    : 'typeph2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone2',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 3',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname3',
									value         : '0',
									hiddenName    : 'typeph3',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone3',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 4',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname4',
									value         : '0',
									hiddenName    : 'typeph4',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'fax',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 5',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname5',
									value         : '0',
									hiddenName    : 'typeph5',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'tollfree',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 6',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname6',
									value         : '0',
									hiddenName    : 'typeph6',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone6',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Address 1',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyaddress1',
									value         : '0',
									hiddenName    : 'typeaddress1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'address1',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Address 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyaddress2',
									value         : '0',
									hiddenName    : 'typeaddress2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'address2',
									width	  : 165
								}]
							},{
								xtype: 'hidden',
								name: 'userid',
								value: <?php echo $userid;?>
							}
						]
					});
					
					var listContact  = new Ext.Panel({
						xtype         : 'panel',
						border        : true,
						bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
						hidden        : true,
						layout        : 'form',
						id            : 'formLC',
						title         : 'Select Register Contact',
						width         : 340,
						labelWidth    : 75,
						items         : [
							{
								id			  : 'idComboContact',
								xtype         : 'combo',
								mode          : 'remote',
								fieldLabel    : 'Contact',
								triggerAction : 'all',
								width		  : 130,
								store         : new Ext.data.JsonStore({
									id:'agentid',
									root:'records',
									totalProperty:'total',
									baseParams: {
										'userid': <?php echo $userid;?>
									},
									fields:[
										{name:'agentid', type:'int'},
										{name:'agent', type:'string'}
									],
									url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
								}),
								displayField  : 'agent',
								valueField    : 'agentid',
								name          : 'fcontactname',
								hiddenName    : 'contactname',
								allowBlank	  : false,
								forceSelection: true,
								minChars	  : 2,								
								queryDelay	  : 1,
								typeAhead 	  : true,
								typeAheadDelay: 1
							},
							{
								xtype: 'hidden',
								name: 'userid',
								value: <?php echo $userid;?>
							}
						]
					});
					
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupOutbox.php',
						frame: true,
						title: 'Assignment Contact',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 330}, 
						labelAlign: 'left',
						buttonsAlign: 'center',
						items: [
								{
									xtype     : 'hidden',
									name      : 'type',
									id		  : 'typePrincipal',
									value     : 'assignmentContact'
								},{
									xtype     : 'hidden',
									name      : 'assignType',
									id		  : 'assignType',
									value     : 'nc'
								},{
									xtype   	: 'radiogroup',
									fieldLabel  : 'Assign To',
									columns 	: 2,
									width		: 250,
									items   	: [
										{   
											xtype         : 'radio',
											name          : 'radioC',
											boxLabel      : 'New Contact',
											submitValue	  : 'ncontact',
											checked		  : true,
											listeners     : {
												'check'   : function (radio,valor) {
													if (valor){
														Ext.getCmp('formLC').hide();
														Ext.getCmp('formNC').show();
														win.setHeight(570);
														Ext.getCmp('assignType').setValue('nc');
													}
												}
											}
										},{
											xtype         : 'radio',
											name          : 'radioC',
											submitValue	  : 'rcontact',
											boxLabel      : 'Register Contact',
											listeners     : {
												'check'   : function (radio,valor) {
													if(valor){
														Ext.getCmp('formLC').show();
														Ext.getCmp('formNC').hide();
														win.setHeight(220); 
														Ext.getCmp('assignType').setValue('rc');
													}
		
												}
											}
										}
									]
								},
								newContact,
								listContact
								],
						
						buttons: [{
								text: 'Assign',
								handler: function(){
									loading_win.show();
									
									var url='mysetting_tabs/myfollowup_tabs/properties_followupOutbox.php';
									if(Ext.getCmp('assignType').getValue()=='nc'){
										url='mysetting_tabs/myfollowup_tabs/properties_followagent.php';
										Ext.getCmp('typePrincipal').setValue('insert');
									}
									
									simple.getForm().submit({
										url: url,
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Contact", "The assignment of the contact is successful");
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", action.result.msg);
										}
									});
								}
							},{
								text: 'Close',
								handler  : function(){
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						autoWidth	: true,
						height		: 570,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close'
					});
					win.show();
					
				}
				,listeners: {
					mouseover: function(but,e){
						but.setText('Contact Assign');
						but.removeClass('x-btn-text-icon');
						but.addClass('x-btn-text-icon');
					},
					mouseout: function(but,e){
						but.setText(' ');
					}
				}
			})*/,
			new Ext.Button({
				tooltip: 'Click to compose email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/composeEmail.png',
				handler: function(){
					mailCompose(<?php echo $userid;?>,-1,'-1',false,false,'','');
				}
			})
		]
	});
	Ext.getCmp('followOutbox').add(OutboxTabBar);
	Ext.getCmp('followOutbox').doLayout();
	
		
	storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox, currentMail:currentMailOutbox, totalMail:totalMailOutbox,checkmail:sincMailOutbox}});
	
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowmail_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowmail_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}	
});
</script>