<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
?>
<style>
.x-grid3-row-expander-header {
  background-color: transparent;
  background-position: 1px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
.x-grid3-row-expander-header-collapse {
  background-color: transparent;
  background-position: -24px 0px;
  background-repeat: no-repeat;
  background-image: url("http://www.reifax.com/MANT/LIB/ext/resources/images/default/grid/row-expand-sprite.gif");
  margin-left:-1px;
}
</style>
<div id="report_content_schedule" style="text-align:center !important;">
	<br clear="all">
	<h1 align="center" >FOLLOW SCHEDULE</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center><div id="follow_schedule_grid" style="width:650px;"></div></center>
	<br>
</div>

<script>
	var followstore_schedule = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
		fields: [ 'idfus','parcelid','userid',
		   {name: 'odate', type: 'date', dateFormat: 'Y-m-d'},
		   {name: 'task', type: 'int'},
		   {name: 'detail'},
		   {name: 'userid_follow', type: 'int'},
		   {name: 'name_follow'}
		],
		baseParams: {'userid': <?php echo $_POST['userid'];?>, 'parcelid': "<?php echo $_POST['parcelid'];?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true
    });
	
	var followdetail_schedule = new Ext.ux.grid.RowExpander({
		header: '<div id="headerexpander_schedule" class="x-grid3-row-expander-header">&nbsp;</div>',
		id: 'followdetailID_schedule',
		tpl : new Ext.Template(
			'<br><p style="margin-left:50px;font-size:14px;"><b>Detail:</b> {detail}</p>'
		)
	});
	var followsm_schedule = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	function taskRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 0: return ''; break;
			case 1: return '<img title="Send SMS." src="../../../img/notes/send_sms.png" />'; break;
			case 2: return '<img title="Receive SMS." src="../../../img/notes/reci_sms.png" />'; break;
			case 3: return '<img title="Send Fax." src="../../../img/notes/send_fax.png" />'; break;
			case 4: return '<img title="Receive Fax." src="../../../img/notes/reci_fax.png" />'; break;
			case 5: return '<img title="Send Email." src="../../../img/notes/send_email.png" />'; break;
			case 6: return '<img title="Receive Email." src="../../../img/notes/reci_email.png" />'; break;
			case 7: return '<img title="Send Document." src="../../../img/notes/send_doc.png" />'; break;
			case 8: return '<img title="Receive Document." src="../../../img/notes/reci_doc.png" />'; break;
			case 9: return '<img title="Make Call." src="../../../img/notes/send_call.png" />'; break;
			case 10: return '<img title="Receive a Call." src="../../../img/notes/reci_call.png" />'; break;
			case 11: return '<img title="Send Regular Mail." src="../../../img/notes/send_mail.png" />'; break;
			case 12: return '<img title="Receive Regular Mail." src="../../../img/notes/reci_mail.png" />'; break;
			case 13: return '<img title="Send Other." src="../../../img/notes/send_other.png" />'; break;
			case 14: return '<img title="Receive Other." src="../../../img/notes/reci_other.png" />'; break;
		}
	}
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var followgrid_schedule = new Ext.grid.GridPanel({
		store: followstore_schedule,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [
				followsm_schedule,
				followdetail_schedule,
				{header: "Date", renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'odate', tooltip: 'Insert/Update Date.'},
				{header: "User", renderer: userRender, dataIndex: 'userid_follow'},
				{header: "T", width: 5, dataIndex: 'task', renderer: taskRender, tooltip: 'Type of Task.'}
			]
		}),
		viewConfig: {
			forceFit:true
		},  
		sm: followsm_schedule,      
		width: 600,
		height: tabs.getHeight()-200,
		plugins: followdetail_schedule,
		iconCls: 'icon-grid',
		renderTo: 'follow_schedule_grid',
		listeners: {
			'headerclick': function (grid,column,e){
				var colmod = grid.getColumnModel();
				var headerID = colmod.getColumnId(column);
				var count = grid.getStore().getCount();
				
				if(headerID=='followdetailID_schedule'){
					var col = document.getElementById('headerexpander_schedule').className;
					if(col=='x-grid3-row-expander-header'){
						colmod.setColumnHeader(column,'<div id="headerexpander_schedule" class="x-grid3-row-expander-header-collapse">&nbsp;</div>');
						for(i=0;i<count;i++){
							followdetail.expandRow(grid.view.getRow(i));
						}
					}else{
						colmod.setColumnHeader(column,'<div id="headerexpander_schedule" class="x-grid3-row-expander-header">&nbsp;</div>');
						
						for(i=0;i<count;i++){
							followdetail.collapseRow(grid.view.getRow(i));
						}
					}
				}
			}
		}
	});
	
	followstore_schedule.load();

if(document.getElementById('tabs')){
	if(document.getElementById('report_content_schedule').offsetHeight > tabs.getHeight()){
		tabs.setHeight(document.getElementById('report_content_schedule').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content_schedule').offsetHeight+150);
	}
}
</script>