<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
	$userid = $_POST['userid'];
	$pid = $_POST['parcelid'];
	$type = isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div id="report_content">
	<br clear="all">
	<h1 align="center" >FOLLOW EMAIL</h1>
    <div class="overview_realtor_titulo" style=" width:100%; text-align:center;">
    	<span class="overview_realtor_titulop" >
        <?php echo $address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
        </span> <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
    </div>
    <br clear="all">
	<center>
    	<div id="follow_email_grid<?php echo $type;?>" style="width:650px;"></div>
    </center>
	<br>
</div> 
<script>
	//filter variables
	var filterEmailMyfollowMailHistory<?php echo $type;?> 		= '';
	var filterToEmailMyfollowMailHistory<?php echo $type;?> 	= '';
	var filterNameMyfollowMailHistory<?php echo $type;?> 		= '';
	var filterContentMyfollowMailHistory<?php echo $type;?> 	= '';
	var filterDateAfMyfollowMailHistory<?php echo $type;?> 	= '';
	var filterDateBeMyfollowMailHistory<?php echo $type;?>		= '';
	var filterTypeMyfollowMailHistory<?php echo $type;?>		= -1;
	
	var followstore_email<?php echo $type;?> = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followemail.php',
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'to_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'ap', type: 'string'},
			{name: 'type', type: 'int'},
			{name: 'task', type: 'int'}
	    ],
		
		baseParams: {'userid': <?php echo $userid;?>, 'parcelid': "<?php echo $pid;?>" },
		root: 'records',
		totalProperty: 'total',
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){			
				obj.params.email 	= filterEmailMyfollowMailHistory<?php echo $type;?>;
				obj.params.toemail	= filterToEmailMyfollowMailHistory<?php echo $type;?>;
				obj.params.name		= filterNameMyfollowMailHistory<?php echo $type;?>;
				obj.params.content	= filterContentMyfollowMailHistory<?php echo $type;?>;
				obj.params.dateAf	= filterDateAfMyfollowMailHistory<?php echo $type;?>;
				obj.params.dateBe	= filterDateBeMyfollowMailHistory<?php echo $type;?>;
				obj.params.etype	= filterTypeMyfollowMailHistory<?php echo $type;?>;
			}
		}
    });
	
	var followsm_email<?php echo $type;?> = new Ext.grid.CheckboxSelectionModel({checkOnly: true, width:25});
	
	var followgrid<?php echo $type;?> = new Ext.grid.GridPanel({
		store: followstore_email<?php echo $type;?>,
		cm: new Ext.grid.ColumnModel({
			defaults: {
				width: 20,
				sortable: true
			},
			columns: [
				followsm_email<?php echo $type;?>
				,{header: 'T', width: 20, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
				,{header: 'A', width: 20, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
				,{id: 'idmail', header: "From", width: 150, align: 'left', sortable: true, dataIndex: 'fromName_msg'}
				,{header: "From Mail", width: 150, align: 'left', sortable: true, dataIndex: 'from_msg'}
				,{header: "To Mail", width: 150, align: 'left', sortable: true, dataIndex: 'to_msg'}
				,{header: "Date", width: 150, sortable: true, align: 'center', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'), dataIndex: 'msg_date'}		
				,{header: 'Subject', width: 300, sortable: true, align: 'left', dataIndex: 'subject'}
			]
		}),
		viewConfig: {
			forceFit:true,
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		sm: followsm_email<?php echo $type;?>,      
		width: 700,
		height: 400,
		iconCls: 'icon-grid',
		renderTo: 'follow_email_grid<?php echo $type;?>',
		tbar: new Ext.PagingToolbar({
            pageSize: 		50,
            store: 			followstore_email<?php echo $type;?>,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display'
		}),
		listeners: {
			'rowdblclick': function(grid, rowIndex, e){
				Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
				var record = grid.getStore().getAt(rowIndex);
				
				viewMailDetail(record.get('idmail'),record.get('userid'),'<?php echo $pid;?>',<?php echo $type=='B' ? 'tabsFollow' : 'tabsFollowSelling';?>); 
			}
		}
	});
	
	var followtbar_email<?php echo $type;?> = new Ext.Toolbar({
		id: 'followTbarEmail<?php echo $type;?>',
		items:[
			new Ext.Button({
				tooltip: 'Click to delete email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(followsm_email<?php echo $type;?>.getCount()==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the email to delete.'); return false;
					}
					
					var sels = followsm_email<?php echo $type;?>.getSelections();
					var pids=sels[0].get('idmail');
					for(i=1; i<sels.length; i++)
						pids+=','+sels[i].get('idmail');
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							followsm_email<?php echo $type;?>.clearSelections( true )
							followstore_email<?php echo $type;?>.load();
							Ext.Msg.alert("Follow - Email", 'Emails deleted.');
							
						}                                
					});
				}
			}),
			new Ext.Button({
				tooltip: 'Click to compose email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/composeEmail.png',
				handler: function(){
					mailCompose(<?php echo $userid;?>,-1,'<?php echo $pid;?>',false,false,'','');
				}
			}),
			new Ext.Button({
				tooltip: 'Click to reply email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/responderEmail.png',
				handler: function(){
					if(followsm_email<?php echo $type;?>.getCount()==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the email to reply.'); return false;
					}else if(followsm_email<?php echo $type;?>.getCount()>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one email to reply.'); return false;
					}
					
					var rec = followsm_email<?php echo $type;?>.getSelected();
					
					mailCompose(<?php echo $userid;?>,rec.get('idmail'),'<?php echo $pid;?>',true,false,rec.get('subject'),rec.get('from_msg'));
				}
			}),
			new Ext.Button({
				tooltip: 'Click to forward email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/reenviarEmail.png',
				handler: function(){
					if(followsm_email<?php echo $type;?>.getCount()==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the email to forward.'); return false;
					}else if(followsm_email<?php echo $type;?>.getCount()>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one email to forward.'); return false;
					}
					
					var rec = followsm_email<?php echo $type;?>.getSelected();
					
					mailCompose(<?php echo $userid;?>,rec.get('idmail'),'<?php echo $pid;?>',false,true,rec.get('subject'),'');
				}
			}),
			new Ext.Button({
				tooltip: 'Click to filter emails.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					
					var formmyfollowmailHistory = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						id: 'formmyfollowmailHistory',
						name: 'formmyfollowmailHistory',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'From mail',
									name		  : 'femail',
									value		  : filterEmailMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterEmailMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftoemail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'To mail',
									name		  : 'ftoemail',
									value		  : filterToEmailMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterToEmailMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fname',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'From',
									name		  : 'fname',
									value		  : filterNameMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterNameMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email Content',
									name		  : 'fcontent',
									value		  : filterContentMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterContentMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdateb',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'Date Between',
									name		  : 'fdateb',
									format		  : 'Y-m-d',
									value		  : filterDateBeMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateBeMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdatea',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'To',
									name		  : 'fdatea',
									format		  : 'Y-m-d',
									value		  : filterDateAfMyfollowMailHistory<?php echo $type;?>,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateAfMyfollowMailHistory<?php echo $type;?> = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Email Type',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											[-1,'All'],
											[1,'SMS'],
											[3,'Fax'],
											[5,'Email'],
											[15,'Voice Mail']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'etypename',
									value         : filterTypeMyfollowMailHistory<?php echo $type;?>,
									hiddenName    : 'etype',
									hiddenValue   : filterTypeMyfollowMailHistory<?php echo $type;?>,
									allowBlank    : false,
									width		  : 150,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterTypeMyfollowMailHistory<?php echo $type;?> = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(b){
									followstore_email<?php echo $type;?>.load({params:{start:0, limit:50}});
									b.findParentByType('window').close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(b){
									filterEmailMyfollowMailHistory<?php echo $type;?> 		= '';
									filterToEmailMyfollowMailHistory<?php echo $type;?> 	= '';
									filterNameMyfollowMailHistory<?php echo $type;?> 		= '';
									filterContentMyfollowMailHistory<?php echo $type;?> 	= '';
									filterDateAfMyfollowMailHistory<?php echo $type;?> 	= '';
									filterDateBeMyfollowMailHistory<?php echo $type;?>		= '';
									filterTypeMyfollowMailHistory<?php echo $type;?>		= -1;
									
									b.findParentByType('form').getForm().reset();
									
									followstore_email<?php echo $type;?>.load({params:{start:0, limit:50}});
									b.findParentByType('window').close();
								}
							},{
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Close&nbsp;&nbsp; ',
								handler  	  : function(b){
									b.findParentByType('window').close();
								}
							}
						]
					});
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 650,
						height      : 230,
						modal	 	: true,  
						plain       : true,
						items		: formmyfollowmailHistory,
						closeAction : 'close'
					});
					win.show();
				}
			})
		]
	});
	Ext.getCmp('followemailInnerTab<?php echo $type;?>').add(followtbar_email<?php echo $type;?>);
	Ext.getCmp('followemailInnerTab<?php echo $type;?>').doLayout();
	
	followstore_email<?php echo $type;?>.load({params:{start:0, limit:50}}); 

if(document.getElementById('tabs')){
	if(document.getElementById('report_content').offsetHeight > tabs.getHeight()){
		tabs.setHeight(document.getElementById('report_content').offsetHeight+100);
		viewport.setHeight(document.getElementById('report_content').offsetHeight+150);
	}
}
</script>