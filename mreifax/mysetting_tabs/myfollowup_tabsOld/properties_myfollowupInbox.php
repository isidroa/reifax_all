<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmailInbox_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowmail_progressBar"></div>
        <div id="myfollowmail_propertiesInbox" align="left"></div> 
	</div>
</div>

<script>
 var storemyfollowmailInbox = null;
 var limitmyfollowmailInbox = 50;
 
 Ext.onReady(function() {
	
	var selected_datamyfollowmailInbox 	= new Array();
	var AllCheckmyfollowmailInbox 		= false;
	var totalMailInbox					= 0;
	var currentMailInbox				= 0;
	var sincMailInbox						= 1;
	
	//filter variables
	var filterEmailMyfollowMailInbox 		= '';
	var filterNameMyfollowMailInbox 		= '';
	var filterContentMyfollowMailInbox 		= '';
	var filterDateAfMyfollowMailInbox 		= '';
	var filterDateBeMyfollowMailInbox		= '';
	var filterTypeMyfollowMailInbox			= -1;
	var filterFieldMyfollowMailInbox		= 'fromName_msg';
	var filterDirectionMyfollowMailInbox	= 'ASC';

	storemyfollowmailInbox = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'agent', type: 'string'},
			{name: 'ap', type: 'string'},
			{name: 'address', type: 'string'},
			{name: 'task', type: 'int'}
	    ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': 		<?php echo $userid;?>,
			'typeEmail':	0,
			'checkmail': 	0,
			'currentMail': 	currentMailInbox,
			'totalMail':  	totalMailInbox
		},
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				if(sincMailInbox==1) progressBarInboxWin.show();
				
				obj.params.email 	= filterEmailMyfollowMailInbox;
				obj.params.name		= filterNameMyfollowMailInbox;
				obj.params.content	= filterContentMyfollowMailInbox;
				obj.params.dateAf	= filterDateAfMyfollowMailInbox;
				obj.params.dateBe	= filterDateBeMyfollowMailInbox;
				obj.params.etype	= filterTypeMyfollowMailInbox;
			}, 
			load: function(store,records,opt){
				currentMailInbox = store.reader.jsonData.currentMail;
				if(totalMailInbox == 0) totalMailInbox = store.reader.jsonData.totalMail;

				if(currentMailInbox<totalMailInbox){
					
					progressBarInbox.updateProgress(
						(currentMailInbox/totalMailInbox),
						"Downloading emails "+currentMailInbox+" of "+totalMailInbox
					);
					
					storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox, currentMail:currentMailInbox, totalMail:totalMailInbox,checkmail:sincMailInbox}});
				}else{
					progressBarInboxWin.hide();
					progressBarInbox.updateProgress(
						0,
						"Initializing download emails..."
					);
					currentMailInbox = 0;
					totalMailInbox = 0;
					sincMailInbox=0;
				}
			}
		}
    });
	
	var progressBarInbox = new Ext.ProgressBar({
		text: "Initializing download emails..."
	});
	
	var progressBarInboxWin=new Ext.Window({
		title: 'Sync Email, please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarInbox]
	});

	var smmyfollowmailInbox = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmailInbox.indexOf(record.get('idmail'))==-1)
					selected_datamyfollowmailInbox.push(record.get('idmail'));
				
				if(Ext.fly(gridmyfollowmailInbox.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailInbox=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmailInbox = selected_datamyfollowmailInbox.remove(record.get('idmail'));
				AllCheckmyfollowmailInbox=false;
				Ext.get(gridmyfollowmailInbox.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}		
	});
	var gridmyfollowmailInbox = new Ext.grid.EditorGridPanel({
		renderTo: 'myfollowmail_propertiesInbox',
		cls: 'grid_comparables',
		height: 400,
		store: storemyfollowmailInbox,
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowmailInbox
			,{header: 'T', width: 25, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
			,{header: 'A', width: 20, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
			,{header: 'C', width: 20, sortable: true, align: 'center', dataIndex: 'ac', renderer: assignmentContact}
			,{header: 'P', width: 20, sortable: true, align: 'center', dataIndex: 'ap', renderer: assignmentProperty}
			,{id: 'idmail', header: "From", width: 150, align: 'left', sortable: true, dataIndex: 'fromName_msg'}
			,{header: "From Mail", width: 150, align: 'left', sortable: true, dataIndex: 'from_msg'}
		    ,{header: "Date", width: 120, sortable: true, align: 'center', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'), dataIndex: 'msg_date'}		
			,{header: 'Subject', width: 400, sortable: true, align: 'left', dataIndex: 'subject'}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowmailInbox',
            pageSize: 		limitmyfollowmailInbox,
            store: 			storemyfollowmailInbox,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display'
		}),
		listeners: {
			'sortchange': function (grid, sorted){
				filterFieldMyfollowMailInbox		= sorted.field;
				filterDirectionMyfollowMailInbox	= sorted.direction;
			},
			'rowdblclick': function(grid, rowIndex, e){
				Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
				var record = grid.getStore().getAt(rowIndex);
				
				viewMailDetail(record.get('idmail'),record.get('userid'),record.get('ap'),tabsFollowEmails); 
			}
		},
		sm: smmyfollowmailInbox,
		frame:false,
		loadMask:true,
		border: false
	});
	
	var InboxTabBar = new Ext.Toolbar({
		id: 'followInboxTbar',
		items:[
			new Ext.Button({
				tooltip: 'Click to sync email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/refresh25.png',
				handler: function(){
					totalMailInbox=0;
					currentMailInbox=0;
					sincMailInbox=1;
					storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox, currentMail:currentMailInbox, totalMail:totalMailInbox,checkmail:sincMailInbox}});
				}
			}),
			new Ext.Button({
				tooltip: 'Click to delete email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowmailInbox.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the emails to be eliminated.'); return false;
					}
					loading_win.show();
					
					var pids=selected_datamyfollowmailInbox[0];
					for(i=1; i<selected_datamyfollowmailInbox.length; i++)
						pids+=','+selected_datamyfollowmailInbox[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							selected_datamyfollowmailInbox 	= new Array();
							storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
							Ext.Msg.alert("My email - Inbox", 'Emails deleted.');
							
						}                                
					});
				}
			}),
			new Ext.Button({
				tooltip: 'Click to filter emails.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					
					var formmyfollowmailInbox = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						id: 'formmyfollowmailInbox',
						name: 'formmyfollowmailInbox',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'From mail',
									name		  : 'femail',
									value		  : filterEmailMyfollowMailInbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterEmailMyfollowMailInbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fname',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'From',
									name		  : 'fname',
									value		  : filterNameMyfollowMailInbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterNameMyfollowMailInbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdateb',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'Date Between',
									name		  : 'fdateb',
									format		  : 'Y-m-d',
									value		  : filterDateBeMyfollowMailInbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateBeMyfollowMailInbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdatea',
								items	: [{
									xtype		  : 'datefield',
									fieldLabel	  : 'To',
									name		  : 'fdatea',
									format		  : 'Y-m-d',
									value		  : filterDateAfMyfollowMailInbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterDateAfMyfollowMailInbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email Content',
									name		  : 'fcontent',
									value		  : filterContentMyfollowMailInbox,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filterContentMyfollowMailInbox = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Email Type',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											[-1,'All'],
											[1,'SMS'],
											[3,'Fax'],
											[5,'Email'],
											[15,'Voice Mail']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'etypename',
									value         : filterTypeMyfollowMailInbox,
									hiddenName    : 'etype',
									hiddenValue   : filterTypeMyfollowMailInbox,
									allowBlank    : false,
									width		  : 150,
									listeners	  : {
										'select'  : function(combo,record,index){
											filterTypeMyfollowMailInbox = record.get('valor');
										}
									}
								}]
							}]
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(b){
									storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
									b.findParentByType('window').close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(b){
									filterEmailMyfollowMailInbox 	= '';
									filterNameMyfollowMailInbox 	= '';
									filterContentMyfollowMailInbox 	= '';
									filterDateAfMyfollowMailInbox 	= '';
									filterDateBeMyfollowMailInbox	= '';
									filterTypeMyfollowMailInbox		= -1;
									
									b.findParentByType('form').getForm().reset();
									
									storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
									b.findParentByType('window').close();
								}
							},{
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Close&nbsp;&nbsp; ',
								handler  	  : function(b){
									b.findParentByType('window').close();
								}
							}
						]
					});
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 650,
						height      : 220,
						modal	 	: true,  
						plain       : true,
						items		: formmyfollowmailInbox,
						closeAction : 'close'
					});
					win.show();
				}
			}),
			new Ext.Button({
				tooltip: 'Click to Assign Contact to email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/contactAssign.png',
				handler: function(){
					if(selected_datamyfollowmailInbox.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the email to be assigned.'); return false;
					}else if(selected_datamyfollowmailInbox.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) only one email to be assigned.'); return false;
					}
					
					var idmail = selected_datamyfollowmailInbox[0];
					var record = storemyfollowmailInbox.getAt(storemyfollowmailInbox.find('idmail',idmail));
					
					var newContact  = new Ext.Panel({
						xtype         : 'panel',
						border        : true,
						bodyStyle     : 'padding : 5px; border: 2px solid #cccccc',
						hidden        : false,
						layout        : 'form',
						id            : 'formNC',
						title         : 'New Contact Information',
						width         : 340,
						labelWidth    : 70,
						items         : [
							{
								xtype     : 'textfield',
								name      : 'agent',
								fieldLabel: 'Contact',
								allowBlank: false,
								value	  : record.get('fromName_msg')
							},{
								xtype         : 'combo',
								mode          : 'remote',
								fieldLabel    : 'Type',
								triggerAction : 'all',
								width		  : 130,
								store         : new Ext.data.JsonStore({
									id:'storetype',
									root:'results',
									totalProperty:'total',
									baseParams: {
										type: 'agenttype',
										'userid': <?php echo $userid;?>
									},
									fields:[
										{name:'idtype', type:'string'},
										{name:'name', type:'string'}
									],
									url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
								}),
								displayField  : 'name',
								valueField    : 'idtype',
								name          : 'fagenttype',
								value         : 'Agent',
								hiddenName    : 'agenttype',
								hiddenValue   : '1',
								allowBlank    : false,
								listeners	  : {
									beforequery: function(qe){
										delete qe.combo.lastQuery;
									}
								}
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Email',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyemail1',
									value         : '0',
									hiddenName    : 'typeemail1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'email',
									width	  : 165,
									value	  : record.get('from_msg') 
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Email 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyemail2',
									value         : '0',
									hiddenName    : 'typeemail2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'email2',
									width	  : 165
								}]
							},{
								xtype     : 'textfield',
								name      : 'company',
								fieldLabel: 'Company'
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Website 1',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Personal'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyurl1',
									value         : '0',
									hiddenName    : 'typeurl1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'urlsend',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Website 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Personal'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyurl2',
									value         : '0',
									hiddenName    : 'typeurl2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'urlsend2',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname1',
									value         : '0',
									hiddenName    : 'typeph1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone1',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname2',
									value         : '0',

									hiddenName    : 'typeph2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone2',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 3',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname3',
									value         : '0',
									hiddenName    : 'typeph3',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone3',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 4',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname4',
									value         : '0',
									hiddenName    : 'typeph4',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'fax',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 5',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname5',
									value         : '0',
									hiddenName    : 'typeph5',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'tollfree',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Phone 6',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office'],
											['2','Cell'],
											['3','Home Fax'],
											['6','Office Fax'],
											['4','TollFree'],
											['5','O. TollFree']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'typephname6',
									value         : '0',
									hiddenName    : 'typeph6',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'phone6',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Address 1',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyaddress1',
									value         : '0',
									hiddenName    : 'typeaddress1',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'address1',
									width	  : 165
								}]
							},{
								xtype	  : 'compositefield',
								fieldLabel: 'Address 2',
								items	  : [{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									width		  : 60,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['0','Home'],
											['1','Office']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'tyaddress2',
									value         : '0',
									hiddenName    : 'typeaddress2',
									hiddenValue   : '0',
									allowBlank    : false
								},{
									xtype     : 'textfield',
									name      : 'address2',
									width	  : 165
								}]
							},{
								xtype: 'hidden',
								name: 'userid',
								value: <?php echo $userid;?>
							}
						]
					});
					
					var listContact  = new Ext.Panel({
						xtype         : 'panel',
						border        : true,
						bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
						hidden        : true,
						layout        : 'form',
						id            : 'formLC',
						title         : 'Select Register Contact',
						width         : 340,
						labelWidth    : 75,
						items         : [
							{
								id			  : 'idComboContact',
								xtype         : 'combo',
								mode          : 'remote',
								fieldLabel    : 'Contact',
								triggerAction : 'all',
								width		  : 130,
								store         : new Ext.data.JsonStore({
									id:'agentid',
									root:'records',
									totalProperty:'total',
									baseParams: {
										'userid': <?php echo $userid;?>
									},
									fields:[
										{name:'agentid', type:'int'},
										{name:'agent', type:'string'}
									],
									url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
								}),
								displayField  : 'agent',
								valueField    : 'agentid',
								name          : 'fcontactname',
								hiddenName    : 'contactname',
								allowBlank	  : false,
								forceSelection: true,
								minChars	  : 2,								
								queryDelay	  : 1,
								typeAhead 	  : true,
								typeAheadDelay: 1
							},
							{
								xtype: 'hidden',
								name: 'userid',
								value: <?php echo $userid;?>
							}
						]
					});
					
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame: true,
						title: 'Assignment Contact',
						width: 350,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						defaults: {width: 330}, 
						labelAlign: 'left',
						buttonsAlign: 'center',
						items: [
								{
									xtype     : 'hidden',
									name      : 'type',
									id		  : 'typePrincipal',
									value     : 'assignmentContact'
								},{
									xtype     : 'hidden',
									name      : 'assignType',
									id		  : 'assignType',
									value     : 'nc'
								},{
									xtype   	: 'radiogroup',
									fieldLabel  : 'Assign To',
									columns 	: 2,
									width		: 250,
									items   	: [
										{   
											xtype         : 'radio',
											name          : 'radioC',
											boxLabel      : 'New Contact',
											submitValue	  : 'ncontact',
											checked		  : true,
											listeners     : {
												'check'   : function (radio,valor) {
													if (valor){
														Ext.getCmp('formLC').hide();
														Ext.getCmp('formNC').show();
														win.setHeight(570);
														Ext.getCmp('assignType').setValue('nc');
													}
												}
											}
										},{
											xtype         : 'radio',
											name          : 'radioC',
											submitValue	  : 'rcontact',
											boxLabel      : 'Register Contact',
											listeners     : {
												'check'   : function (radio,valor) {
													if(valor){
														Ext.getCmp('formLC').show();
														Ext.getCmp('formNC').hide();
														win.setHeight(220); 
														Ext.getCmp('assignType').setValue('rc');
													}
		
												}
											}
										}
									]
								},
								newContact,
								listContact
								],
						
						buttons: [{
								text: 'Assign',
								handler: function(){
									loading_win.show();
									
									var url='mysetting_tabs/myfollowup_tabs/properties_followupEmail.php';
									if(Ext.getCmp('assignType').getValue()=='nc'){
										url='mysetting_tabs/myfollowup_tabs/properties_followagent.php';
										Ext.getCmp('typePrincipal').setValue('insert');
									}
									
									simple.getForm().submit({
										url: url,
										success: function(form, action) {
											loading_win.hide();
											win.close();
											Ext.Msg.alert("Follow Contact", "The assignment of the contact is successful");
											storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
										},
										failure: function(form, action) {
											loading_win.hide();
											Ext.Msg.alert("Failure", action.result.msg);
										}
									});
								}
							},{
								text: 'Close',
								handler  : function(){
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						autoWidth	: true,
						height		: 570,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close'
					});
					win.show();
					
				}
			}),
			new Ext.Button({
				tooltip: 'Click to compose email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/composeEmail.png',
				handler: function(){
					mailCompose(<?php echo $userid;?>,-1,'-1',false,false,'','');
				}
			})
		]
	});
	Ext.getCmp('followInbox').add(InboxTabBar);
	Ext.getCmp('followInbox').doLayout();
	
		
	storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox, currentMail:currentMailInbox, totalMail:totalMailInbox,checkmail:sincMailInbox}});
	
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowmail_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowmail_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
});
</script>