<?php  
	define('FPDF_FONTPATH','../../../FPDF/font/');	
	include("../../../properties_conexion.php");
	conectar();
	$realtor=$_POST['userweb']=="true" ? true:false;	
	$realtorid= $_POST['realtorid'];
	if($realtor){ 
		$imp=100; $usr= $realtorid;
	}else{
		$imp=500; $usr= $_COOKIE['datos_usr']['USERID'];
	}
	$_SERVERXIMA="http://www.reifax.com/";
	$ownerShow=false;
	
	if(isset($_POST['ownerShow']) && $_POST['ownerShow']=="true") $ownerShow=true;

	$_GET['resultType']='basic';
	$_GET['systemsearch']=isset($_POST['systemsearch']) ? $_POST['systemsearch'] : (isset($_GET['systemsearch']) ? $_GET['systemsearch']:'basic'); 
	$_POST['ResultTemplate']=-1;
	
	$rest_total=explode(',',$_POST['parcelids_res']);
	
	
	set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Excel/Classes/');
	include '../../../Excel/Classes/PHPExcel.php';
	include '../../../properties_getgridcamptit.php';
	include '../../../Excel/Classes/PHPExcel/Writer/Excel2007.php';
	
	$excel = new PHPExcel();

	$excel->getProperties()->setCreator("REI Property Fax");
	$excel->getProperties()->setTitle("REI Property Fax Reports");
	$excel->getProperties()->setSubject("REI Property Fax Reports");
	$excel->setActiveSheetIndex(0);
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
	'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
	'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
	'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
	'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ');
	
	//$id = getArray($_POST['template_res'],'template');
	$id = getArray('PR','result');
	
	if($ownerShow){
		array_push($id,592);
		array_push($id,591);
		array_push($id,25);
		array_push($id,24);
		array_push($id,23);
		array_push($id,22);
		array_push($id,21);
		array_push($id,20);
	}
	
	//print_r($id);
	
	
	conectarPorIdCounty($county);
	$ArTab=getCamptitTipo($id, "Tabla",'defa');	
	$myArrF=getCamptitTipo($id, "Campos",'defa');
	$myArrTit=getCamptitTipo($id, "Titulos",'defa');
	$myArrNum = count($myArrTit);

	foreach($myArrTit as $i=>$val){
		$excel->getActiveSheet()->setCellValue($abc[$i+1].'2', $val);
		$excel->getActiveSheet()->getColumnDimension($abc[$i+1])->setAutoSize(true);
	}
	
	//Formacion de Tabla
	$sql_tabla=$xSql;
	$fila_tabla=3;
	$fila_tabla_inicio=3;
	
	$querybd="SELECT distinct f.bd FROM `xima`.`followup` f order by f.bd";
	$resultbd=mysql_query($querybd) or die($querybd.mysql_error());
	$datos=array();
	$datos_id=array();
	while($rbd=mysql_fetch_array($resultbd)){ 
		conectarPorNameCounty($rbd['bd']);
		
		$sql_tabla="SELECT p.parcelid, p.bath,p.beds,p.bheated,p.buildingv,p.landv,p.lsqft,p.owner,p.owner_a,p.owner_c,p.owner_p,p.owner_s,p.owner_z,p.saleprice,p.taxablev,p.unit,p.yrbuilt,f.pof as pendes,p.waterf,p.ozip,p.address,p.pool,p.phonenumber1,p.phonenumber2 FROM psummary p left join pendes f on p.parcelid=f.parcelid  where p.parcelid IN ('".implode("','",$rest_total)."')";
		$res=mysql_query($sql_tabla) or die('Error: '.mysql_error()."<BR>Query: $sql_tabla");


		if(isset($mv)){
			$excel->getActiveSheet()->setCellValue($abc[$myArrNum-3].'1' ,'Calculate:');
			$excel->getActiveSheet()->setCellValue($abc[$myArrNum-2].'1' ,$mv);
			$excel->getActiveSheet()->setCellValue($abc[$myArrNum-1].'1' ,$media);
			$excel->getActiveSheet()->setCellValue($abc[$myArrNum].'1' ,$mp);
			unset($mp,$media,$mv);
		}
		
		while($r=mysql_fetch_array($res)){
			$datos[]=$r;
			$datos_id[]=$r['parcelid'];
		}
	}
	//print_r($datos);
	foreach($rest_total as $k => $val1){
		$ind=array_search($val1,$datos_id);
		if($ind!==false) $r=$datos[$ind];
		
		foreach($myArrF as $i=>$val){
			if(strtolower($val)=='parcelid' || strtolower($val)=='folio')
			{
//					$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,floatval($r[$val]));
				$excel->getActiveSheet()->setCellValueExplicit($abc[$i+1].$fila_tabla ,'\''.$r[$val],TYPE_STRING);
			}
			
			if(strtolower($val)=='pendes' && (is_null($r[$val]) || empty($r[$val]) || $r[$val]=='' || $r[$val]==' '))
				$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,'N');
			else
				$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,$r[$val]);
			if(!is_null($take_comp) && strtolower($val)=='parcelid'){
				$tak='NO';
				foreach($take_comp as $k=>$va){
					if($r[$val]==$va){
						$tak='YES';
					}
				}
				$excel->getActiveSheet()->setCellValue('A'.$fila_tabla ,$tak);
			}
		}
		$fila_tabla++;
	}
	
	if(count($rest_total)==0){
		foreach($datos as $k => $val1){
			$r=$datos[$k];
			foreach($myArrF as $i=>$val){
				if(strtolower($val)=='parcelid' || strtolower($val)=='folio')
				{
//					$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,floatval($r[$val]));
					$excel->getActiveSheet()->setCellValueExplicit($abc[$i+1].$fila_tabla ,'\''.$r[$val],TYPE_STRING);
				}
				
				if(strtolower($val)=='pendes' && (is_null($r[$val]) || empty($r[$val]) || $r[$val]=='' || $r[$val]==' '))
					$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,'N');
				else
					$excel->getActiveSheet()->setCellValue($abc[$i+1].$fila_tabla ,$r[$val]);
				if(!is_null($take_comp) && strtolower($val)=='parcelid'){
					$tak='NO';
					foreach($take_comp as $k=>$va){
						if($r[$val]==$va){
							$tak='YES';
						}
					}
					$excel->getActiveSheet()->setCellValue('A'.$fila_tabla ,$tak);
				}
			}
			$fila_tabla++;
		}
	}
	
	$excel->getActiveSheet()->getStyle('B'.$fila_tabla_inicio)->applyFromArray(
	array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		)
	)
	);
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B'.$fila_tabla_inicio), 'B'.$fila_tabla_inicio.':'.$abc[($myArrNum)].$fila_tabla );
		
	$excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			array(
				'font'    => array(
					'bold'      => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
					'top'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'bottom'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'right'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					),
					'left'     => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK
					)
				),
				'fill' => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						'argb' => 'FF87cefa'
					)
				)
			)
	);
	
	$excel->getActiveSheet()->duplicateStyle( $excel->getActiveSheet()->getStyle('B2'), 'B2:'.$abc[($myArrNum)].'2' );
	$excel->setActiveSheetIndex(0);
	$objWriter = new PHPExcel_Writer_Excel2007($excel);
	$nombre = 'Excel/archivos/'.strtotime("now").'.xlsx';
	$objWriter->save('C:/inetpub/wwwroot/'.$nombre);
	echo "{success:true, excel:'$nombre'}";
?>
