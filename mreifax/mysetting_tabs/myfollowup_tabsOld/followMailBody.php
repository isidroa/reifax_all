<?php
	include("../../properties_conexion.php");
	conectar();	

	$userid = $_POST['userid'];
	$mailid = $_POST['mailid'];
	$pid	= isset($_POST['pid']) ? $_POST['pid'] : '-1';
	if($pid==0) $pid='-1';
	
	$query="UPDATE xima.follow_emails 
	SET seen=1 
	WHERE userid=$userid AND idmail=$mailid";
	mysql_query($query) or die($query.mysql_error());
	
	$query="SELECT fe.subject,fe.fromName_msg,fe.from_msg,fe.to_msg,fe.msg_date,fb.body 
	FROM xima.follow_emails fe
	INNER JOIN xima.follow_emails_body fb ON (fe.idmail=fb.idmail) 
	WHERE fe.userid=$userid AND fe.idmail=$mailid";
	$result = mysql_query($query) or die($query.mysql_error());
	$r = mysql_fetch_array($result);
?>
<style>
.mailSubject {
	background-color: transparent;
	border-bottom: 2px solid #EEEEEE;
	color: #222222;
	font-family: arial,sans-serif;
	font-size: 2.3em;
	font-weight: normal;
	margin: 12px 1px 9px;
	padding: 0 0 5px 8px;
}	

.mailButtons {
	float: right;
	height: 25px;
	margin-top: 18px;
	width: 150px;
}
.mailImgBut{
	display:inline;
	width:11px;
	height:11px;
}
.overviewBotonCss3{
	margin-right: 4px;
}

.mailFrom{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailFromName{
	color:#222222;
	font-weight:bold;
}
.mailFromEmail{
	color:#555555;
}
.mailFromDate{
	color:#222222;
	float:right;
}

.mailTo{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailToEmail{
	color:#555555;
}

.mailBody{
	font-size: 120%;
	margin: 15px 10px 5px;
	text-align: left;
}

.mailAttach{
	margin: 0 10px;
}
.mailAttachItem{
	padding-right: 5px;
}
</style>	
	
    <div class="mailButtons">
        <a title="reply" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',true,false,'<?php echo $r['subject'];?>','<?php echo $r['from_msg'];?>');">
        	<img title="reply" class="mailImgBut" src="http://www.reifax.com/img/myemail/responder.png" />
        </a>
        <a title="forward" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',false,true,'<?php echo $r['subject'];?>','');">
        	<img title="forward" class="mailImgBut" src="http://www.reifax.com/img/myemail/reenviar.png" />
        </a>
        <a title="close" class="overviewBotonCss3" href="javascript:closeTab();">
        	<img title="close" class="mailImgBut" src="http://www.reifax.com/img/myemail/cerrar.png" /> 
        </a>
    </div>
    <h1 class="mailSubject"><?php echo $r['subject'];?></h1>
    
    <div class="mailFrom">
    	From: 
        <span class="mailFromName"><?php echo $r['fromName_msg'];?></span>
        <span class="mailFromEmail"><?php echo $r['from_msg'];?></span>
        <span class="mailFromDate"><?php echo date('l jS \of F Y, h:i:s A',strtotime($r['msg_date']));?></span>
   	</div>
    <div class="mailTo">
        To: 
        <span class="mailToEmail"><?php echo $r['to_msg'];?></span>
   	</div>
    <div align="center" class="mailBody">
		<?php print_r(html_entity_decode($r['body']));?>
	</div>
    <div class="mailAttach">
    	<?php 
		$query='select * FROM xima.follow_emails_attach where idmail='.$mailid;
		$resultA = mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($resultA)>0){
			$i=1;
			while($rA=mysql_fetch_array($resultA)){
				echo '<a class="mailAttachItem" target="_blank" href="MailAttach/'.$userid.'/'.$rA['filename'].'">'.($i++).' '.$rA['filename'].'</a><br>';
			}
		}else{
    		echo '&nbsp;';
		}
		?>
    </div>
    
    <script>
		function closeTab(){
			var tab = tabsFollow.getItem('viewMailTab');
			tabsFollow.remove(tab);
		}
		window.scroll(0,0);  
	</script>