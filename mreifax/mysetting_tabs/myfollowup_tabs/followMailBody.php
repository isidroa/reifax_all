<?php
	include("../../properties_conexion.php");
	conectar();	
	
	$docsDir = 'F://ReiFaxDocs/';

	$userid = isset($_GET['userid']) ? $_GET['userid'] : $_POST['userid'];
	$mailid = isset($_GET['mailid']) ? $_GET['mailid'] : $_POST['mailid'];
	$pid	= isset($_POST['pid']) ? $_POST['pid'] : (isset($_GET['pid']) ? $_GET['pid'] : '-1');
	$tabs	= isset($_POST['tabs']) ? $_POST['tabs'] : 'tabsFollowId';
	$typeFollow = ($tabs=='tabsFollowId' || $tabs=='tabsFollowEmailsId') ? 'B' : 'S';
	$county = isset($_POST['county']) ? $_POST['county'] : '';
	if($pid==0) $pid='-1';
	
	if($pid!='-1'){
		$query='SELECT bd,status,statusalt FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$pid.'"';
		$result = mysql_query($query) or die($query.mysql_error());
		$r = mysql_fetch_array($result);
		
		$county = $r[0];
		$statusProperty = $r[1];
		$statusAltProperty = $r[2];
	}
	
	$progressBar = 'progressBarViewMail'.$mailid;
	$progressWin = 'progressWinViewMail'.$mailid;
	
	$query="UPDATE xima.follow_emails 
	SET seen=1 
	WHERE userid=$userid AND idmail=$mailid";
	mysql_query($query) or die($query.mysql_error());
	
	$query="SELECT fe.subject,fe.fromName_msg,fe.from_msg,IF(fe.to_msg is null,c.imap_username,fe.to_msg) to_msg,fe.msg_date,fb.body 
	FROM xima.follow_emails fe
	INNER JOIN xima.follow_emails_body fb ON (fe.idmail=fb.idmail) 
	INNER JOIN xima.contracts_mailsettings c ON (fe.userid=c.userid)
	WHERE fe.userid=$userid AND fe.idmail=$mailid";
	$result = mysql_query($query) or die($query.mysql_error());

?>
<style>

.overviewBotonCss3 {
  background: -moz-linear-gradient(center top , #FFFFFF, #EEEEEE) repeat scroll 0 0 transparent;
  border: thin solid #BBBBBB;
  border-radius: 4px 4px 4px 4px;
  color: #1D6AAA;
  font-weight: bold;
  padding: 10px 20px;
  position: relative;
  text-decoration: none;
  font-size:11px;
  display:block;
  float: left;
  width:0px;
  height:12px;
}

.mailSubject {
	background-color: transparent;
	border-bottom: 2px solid #EEEEEE;
	color: #222222;
	font-family: arial,sans-serif;
	font-size: 2.3em;
	font-weight: normal;
	margin: 12px 1px 9px;
	padding: 0 0 5px 8px;
}	

.mailButtons {
	float: right;
	height: 25px;
	margin-top: 5px;
}
.mailImgBut {
  left: 13px;
  max-height: 11px;
  position: absolute;
  top: 10px;
}
.mailImgButBig {
  left: 7px;
  max-height: 25px;
  position: absolute;
  top: 5px;
}
.overviewBotonCss3{
	margin-right: 4px;
}

.mailFrom{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailFromName{
	color:#222222;
	font-weight:bold;
}
.mailFromEmail{
	color:#555555;
}
.mailFromDate{
	color:#222222;
	float:right;
}

.mailTo{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailToEmail{
	color:#555555;
}

.mailBody{
	font-size: 120%;
	margin: 15px 10px 5px;
	text-align: left;
	height: 500px;
	overflow:auto;
	overflow-x:hidden;
}

.mailAttach {
  font-size: 14px;
  margin: 0 10px;
}
.mailAttachItem{
	padding-right: 5px;
}
</style>	

<?php
	if(mysql_num_rows($result)>0){
		$r = mysql_fetch_array($result);
?>

    <div class="mailButtons">
    	<a title="Delete Email" class="overviewBotonCss3" href="javascript:actionEmail('delete');">
        	<img title="Delete Email" class="mailImgButBig" src="http://www.reifax.com/img/myemail/deleteEmail.png" />
        </a>
    	<?php if($pid != '-1'){?>
        <a title="View Menu" class="overviewBotonCss3" href="javascript:actionEmail('view');">
        	<img title="View Menu" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/icono_ojo.png" />
        </a>
        <a title="New Scheduled Task" class="overviewBotonCss3" href="javascript:actionEmail('schedule');">
        	<img title="New Scheduled Task" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/scheduled_tasks.png" />
        </a>
        <a title="Send Contracts" class="overviewBotonCss3" href="javascript:actionEmail('contract');">
        	<img title="Send Contracts" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/guarantee.png" />
        </a>
        <a title="Send Email" class="overviewBotonCss3" href="javascript:actionEmail('email');">
        	<img title="Send Email" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/email.png" />
        </a>
        <a title="Send SMS" class="overviewBotonCss3" href="javascript:actionEmail('sms');">
        	<img title="Send SMS" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/phone.png" />
        </a>
        <a title="Send Fax" class="overviewBotonCss3" href="javascript:actionEmail('fax');">
        	<img title="Send Fax" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/fax.png" />
        </a>
        <a title="Make Call" class="overviewBotonCss3" href="javascript:actionEmail('call');">
        	<img title="Make Call" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/gvoicemult.png" />
        </a>
    	<?php }?>
        <a title="reply" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',true,false,'<?php echo str_replace("'",'',$r['subject']);?>','<?php echo $r['from_msg'];?>','<?php echo $typeFollow;?>','<?php echo $county;?>');">
        	<img title="reply" class="mailImgBut" src="http://www.reifax.com/img/myemail/responder.png" />
        </a>
        <a title="forward" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',false,true,'<?php echo str_replace("'",'',$r['subject']);?>','','<?php echo $typeFollow;?>','<?php echo $county;?>');">
        	<img title="forward" class="mailImgBut" src="http://www.reifax.com/img/myemail/reenviar.png" />
        </a>
        <a title="close" class="overviewBotonCss3" href="javascript:closeTab();">
        	<img title="close" class="mailImgBut" src="http://www.reifax.com/img/myemail/cerrar.png" /> 
        </a>
    </div>
    <h1 class="mailSubject"><?php echo $r['subject'];?></h1>
    
    <div class="mailFrom">
    	From: 
        <span class="mailFromName"><?php echo $r['fromName_msg'];?></span>
        <span class="mailFromEmail"><?php echo $r['from_msg'];?></span>
        <span class="mailFromDate"><?php echo date('l jS \of F Y, h:i:s A',strtotime($r['msg_date']));?></span>
   	</div>
    <div class="mailTo">
        To: 
        <span class="mailToEmail"><?php echo $r['to_msg'];?></span>
   	</div>
    <div align="center" class="mailBody">
		<?php 
			
			$html = html_entity_decode(stripslashes($r['body']));			
			print_r(preg_replace('/[\x00-\x1F\x80-\xFF]/',"",$html));
			//print_r($html);
		?>
	</div>
    <div class="mailAttach">
    	<?php 
		$query='select distinct url,filename FROM xima.follow_emails_attach where idmail='.$mailid;
		$resultA = mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($resultA)>0){
			$i=1;
			while($rA=mysql_fetch_array($resultA)){
				if(strlen($rA['url'])>0)
					echo '<a class="mailAttachItem" target="_blank" href="'.$rA['url'].'">'.($i++).' '.basename($rA['filename']).'</a><br>';
				else
					echo '<a class="mailAttachItem" target="_blank" href="/MailAttach/'.$userid.'/'.urldecode($rA['filename']).'">'.($i++).' '.urldecode($rA['filename']).'</a><br>';
			}
		}else{
    		echo '&nbsp;';
		}
		?>
    </div>
<?php }else{?>
	<div class="mailButtons">
        <a title="close" class="overviewBotonCss3" href="javascript:closeTab();">
        	<img title="close" class="mailImgBut" src="http://www.reifax.com/img/myemail/cerrar.png" /> 
        </a>
    </div>
    <h1 class="mailSubject">Sorry, the content has been removed.</h1>
<?php }?>

<script type="text/javascript" src="mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>
 <script>
	function closeTab(){
		var tabs = Ext.getCmp('<?php echo $tabs;?>');
		var tab = tabs.getItem('viewMailTab');
		tabs.remove(tab);
	}
	
	function actionEmail(type){
		var selected =new Array();
		var sel=new Array();
		var userid = '<?php echo $userid;?>';
		var pid = '<?php echo $pid;?>';
		var mailid = <?php echo $mailid;?>;
		var county = '<?php echo $county;?>';
		var status = '<?php echo $statusProperty;?>';
		var statusAlt = '<?php echo $statusAltProperty;?>';
		var structAux = {'pid':pid, 'status': 'PENDING'};
		sel['pid']=pid;
		sel['status']='PENDING'
		console.log(structAux);
		selected.push(sel);
		
		
		switch(type){
			case 'delete':
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
					method: 'POST',
					timeout :600000,
					params: { 
						pids: mailid,
						userid: userid,
						type: 'delete'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						closeTab();
					}
				});
			break;
			
			case 'email':
				sendEmailFH(pid,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'sms':
				sendEmailSmsFH(pid,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'fax':
				sendEmailFax(pid,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'call':
				makeMultipleCalls(selected,false,userid,false,pid,'<?php echo $typeFollow;?>'); 
				//creaVentana
			break;
			
			case 'schedule':
				var simple = new Ext.FormPanel({
					frame: true,
					title: 'Add Schedule',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype		  : 'datefield',
								allowBlank	  : false,
								name		  : 'odate',
								fieldLabel	  : 'Date',
								format		  : 'Y-m-d'
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : '1',
								hiddenName    : 'task',
								hiddenValue   : '1',
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											pid: pid,
											userid: userid,
											type: 'assignment'
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,userid);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Schedule Detail'
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : '<?php echo $typeFollow;?>'
							}],
					
					buttons: [{
							text: 'Create',
							handler: function(){
								var values = simple.getForm().getValues();
								var pidval = new Array();
								pidval.push(structAux);
								win.close();
								console.log(structAux);
								console.log(pidval);
								
								<?php echo $progressWin;?>.show();
								crearTasks('mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',userid,pidval,0,values,<?php echo $progressBar;?>,<?php echo $progressWin;?>,false);
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});
				 
				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 400,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			break
			
			case 'contract':
				sendContractJ(selected,false,false,userid, <?php echo $progressWin;?>,<?php echo $progressBar;?>,false,false);
			break
			
			case 'view':
				var simple = new Ext.FormPanel({
				url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				frame: true,
				width: 220,
				waitMsgTarget : 'Waiting...',
				labelWidth: 100,
				defaults: {width: 200},
				labelAlign: 'left',
				items: [
					new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Contact',
						handler: function(){
							win.close();
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									pid: pid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(0,r,pid,userid);
								}
							});
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Change Status',
						handler: function(){
							win.close();
							statusAltPanel(null,userid,pid,status);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Overview',
						handler: function(){
							win.close();
							createOverview(county,pid,statusAlt,false,false);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Follow History',
						handler: function(){
							win.close();
							
							var recordHistory = Ext.data.Record.create([
								'status',
								'county'
							]);
							
							var myRecordHistory = new recordHistory({
								status: statusAlt,
								county: county
							});
							
								var tf='<?php echo $typeFollow;?>';
						var typeFollow='<?php echo $typeFollow;?>';
						var activeHistoryTab='';
							
							// Agregado por Luis R Castro 29/09/2015 
						//Top Bar principal
	var followTbar = new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'View Contacts',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/agent.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					method: 'POST',
					timeout :86400,
					params: {
						pid: pid,
						userid: userid,
						type: 'assignment'
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var r=Ext.decode(response.responseText);
						creaVentana(0,r,pid,useridspider);
					}
				});
			}
		},{
			 tooltip: 'Short Sale',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/shortsale.png',
			 hidden: true,
			 handler: function(){
				var option = 'width=930px,height=555px';
				 window.open('https://www.reifax.com/xima3/mysetting_tabs/myfollowup_tabs/shortsaleform.php?pid='+pid ,'Signature',option);

			 }
		},'->',{
			 tooltip: 'Close Follow History',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/cancel.png',
			 handler: function(){
				 var tab = tabs.getItem(id);
				 tabs.remove(tab);
			 }
		}]
	});


	//Top Bar History Tab
	var tbarhistory =  new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				var idfuh = '';
				if(followfuh.length > 0){
					idfuh=followfuh[0].get('idfuh');
					for(i=1; i<followfuh.length; i++){
						idfuh+=','+followfuh[i].get('idfuh');
					}
				}
				Ext.Ajax.request(
				{
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					method: 'POST',
					timeout :86400,
					params: {
						type: 'delete',
						idfuh: idfuh
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
						followstore.reload();
					}
				});
			 }
		},{
			tooltip: 'New History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Add History',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : '1',
								hiddenName    : 'task',
								hiddenValue   : '1',
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											pid: pid,
											userid: userid,
											type: 'assignment'
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Detail'
							},
							//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
							{
											xtype		  : 'datefield',
											width		  : 90,
											fieldLabel    : 'Date',
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'dateadd',
											value         :new Date(),
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													dateadd=newvalue;
												}
											}
									},
							////////////////////////////////////////////////////////7
							{
								xtype	  : 'checkboxgroup',
								fieldLabel: 'Document',
								columns	  : 3,
								itemCls   : 'x-check-group-alt',
								items	  : [
									{boxLabel: 'Contract', name: 'contract'},
									{boxLabel: 'Proof of Funds', name: 'pof'},
									{boxLabel: 'EMD', name: 'emd'},
									{boxLabel: 'Addendums', name: 'rademdums'},
									{boxLabel: 'Offer Received', name: 'offerreceived'}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'insert'
							},{
								xtype     : 'hidden',
								name      : 'parcelid',
								value     : pid
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],

					buttons: [{
							text: 'Add',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow History", 'New Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});

				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 500,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},{
			tooltip: 'Edit History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				if(followfuh.length > 1){
					Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
					return false;
				}else if(followfuh.length == 0){
					Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
					return false;
				}

				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Update History',
					width: 550,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0,
								value	  : followfuh[0].get('offer')
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0,
								value	  : followfuh[0].get('coffer')
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : followfuh[0].get('task'),
								hiddenName    : 'task',
								hiddenValue   : followfuh[0].get('task'),
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											pid: pid,
											userid: userid,
											type: 'assignment'
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},//Agregado por Luis R Castro Sugerencia 12597 05/06/2015
							{
											xtype		  : 'datefield',
											width		  : 90,
											fieldLabel    : 'Date',
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'dateadd',
											value	  : followfuh[0].get('dateadd'),
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													dateadd=newvalue;
												}
											}
									},
							////////////////////////////////////////////////////////7
							{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'sheduledetail',
								fieldLabel: 'Schedule Detail',
								value	  : followfuh[0].get('sheduledetail')
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Complete Detail',
								value	  : followfuh[0].get('detail')
							},{
								xtype: 'checkboxgroup',
								fieldLabel: 'Document',
								columns: 3,
								itemCls: 'x-check-group-alt',
								items: [ // Ticket 12681 Agregado por Luis R Castro 17/06/2015
									{boxLabel: 'Contract', name: 'contract', checked: followfuh[0].get('contract')==1},
									{boxLabel: 'Proof of Funds', name: 'pof', checked: followfuh[0].get('pof')==1},
									{boxLabel: 'EMD', name: 'emd', checked: followfuh[0].get('emd')==1},
									{boxLabel: 'Addendums', name: 'rademdums', checked: followfuh[0].get('realtorsadem')==1},
									{boxLabel: 'Offer Received', name: 'offerreceived', checked: followfuh[0].get('offerreceived')==1}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'update'
							},{
								xtype     : 'hidden',
								name      : 'idfuh',
								value     : followfuh[0].get('idfuh')
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],

					buttons: [{
							text: 'Update',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow Up", 'Updated Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});

				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 550,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},new Ext.Button({
			tooltip: 'Print History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 4,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 4,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore.reload();
			}
		}),
		
		// Agregado por Luis R Castro Sugerencia 11693 18/05/2015
		
		new Ext.Button({
				tooltip: 'Send Contracts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/guarantee.png',
				handler: function(){
					
				
					
					
					sendContractJ(pid,'','',useridspider,'', '',Ext.getCmp('mailenviadosfollow'),false,'','','','',0,0,0,0,0,'followup',1);
				}
			}),new Ext.Button({
				tooltip: 'Send Email.',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/email.png',
				handler: function(){
					
					sendEmailFH(pid,useridspider);
				}
			}),new Ext.Button({
				tooltip: 'Send SMS',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/phone.png',
				handler: function(){
					
					sendEmailSmsFH(pid,useridspider);
				}
			}),new Ext.Button({
				tooltip: 'Send Fax',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/fax.png',
				handler: function(){
										
					sendEmailFaxFH(pid,useridspider);
				}
			}),
		
		////////////////////////////////////////////////////
		
		
		// Agregado por Luis R Castro Sugerencias 11818 - 11884 12/05/2015
		new Ext.Button({
				tooltip: 'Block Property',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
				
					
					loading_win.show();
				blockFollow('yes');

					function blockFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'block',
								pids: pid
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
							Ext.Msg.alert("Follow Up", 'Added to Follow Block this property.');
						//storemyfollowup.load({params:{start:0, limit:limitmyfollowup}});
							}                                
						});
					}
				}
			})
		/////////////////////////////////////////////////////////
		]
	});

	//Top Bar Follow Schedule
	var tbarschedule = new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				var idfus = '';
				if(followfus.length > 0){
					idfus=followfus[0].get('idfus');
					for(i=1; i<followfus.length; i++){
						idfus+=','+followfus[i].get('idfus');
					}
				}
				Ext.Ajax.request(
				{
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
					method: 'POST',
					timeout :86400,
					params: {
						type: 'delete',
						idfus: idfus
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
						followstore_schedule.reload();
					}
				});
			 }
		},{
			tooltip: 'New Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var selected = new Array(), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule;
				selected.push(pid);

				createScheduleTask(selected, store, userid, progressBarTask_win_schedule, progressBarTask_schedule, selling);
			 }
		},{
			tooltip: 'Edit Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}

				var idfus = followfus[0].get('idfus'), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule;
				editScheduleTask(idfus, userid, selling, store);
			 }
		},{
			tooltip: 'Complete Task',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/refresh.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections(), selectedAux=new Array();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}

				for(i=0;i<followfus.length;i++){
					selectedAux.push({
						'pid': pid,
						'id': followfus[i].get('idfus'),
						'task': followfus[i].get('task')
					});
				}

				loading_win.show();
				completeMultiTask(selectedAux,followgrid_schedule,followstore_schedule,userid, progressBarTask_win_schedule, progressBarTask_schedule, false);
			 }
		},new Ext.Button({
			tooltip: 'Print Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 5,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request(
				{
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php',
					method: 'POST',
					timeout :86400,
					params: {
						printType: 5,
						parcelid: pid,
						userid: userid,
						county: county
					},

					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},

					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;
					}
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore_schedule.reload();
			}
		}),
		// Agregado por Luis R Castro Sugerencias 11818 - 11884 12/05/2015
		new Ext.Button({
				tooltip: 'Block Property',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
				
					
					loading_win.show();
				blockFollow('yes');

					function blockFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'block',
								pids: pid
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'Added to Follow Block this property.');
								storemyfollowup.load({params:{start:0, limit:limitmyfollowup}});
							}                                
						});
					}
				}
			})
		/////////////////////////////////////////////////////////
		]
	});

						//////////////////////////////////////////
						
						if(tf=='B')
						{
							var tabsFH = Ext.getCmp('tabsFollowId');
							activeHistoryTab=0;
							var f=2;
							}else{
							var tabsFH = Ext.getCmp('ext-comp-1036');
							activeHistoryTab=2;
							var f=3;
							}
							// Agregado por Luis R Castro 29/09/2015 Ticket 12915
							tabsFH.add({
		title: ' Follow ',
		id: id,
		closable: true,
		items: [
			new Ext.TabPanel({
				id: 'historyTab'+typeFollow,
				activeTab: activeHistoryTab,
				width: ancho,
				height: tabs.getHeight(),
				plain:true,
				listeners: {
					'tabchange': function( tabpanel, tab ){
						if(tab.id=='followhistoryInnerTab'+typeFollow)
							if(document.getElementById('follow_history_grid')) followstore.reload();
						else if(tab.id=='followsheduleInnerTab'+typeFollow)
							if(document.getElementById('follow_schedule_grid')) followstore_schedule.reload();
					}
				},
				items:[{
					title: 'History',
					id: 'followhistoryInnerTab'+typeFollow,
					autoLoad: {
						url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php',
						scripts	: true,
						params	:{
							parcelid 	: pid,
							userid 		: userid,
							county 		: county,
							typeFollow	: typeFollow
						},
						timeout	: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarhistory
				},{
					title: 'Schedule',
					id: 'followsheduleInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarschedule
				},{
					title: 'Email',
					id: 'followemailInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followemail.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow,
							tab : tt,
							from	: 'Follow'
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
				},{
					title: 'Dashboard',
					id: 'followdashboardInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followdashboard.php',
						scripts: true,
						params:{
							parcelid	: pid,
							userid		: userid,
							county		: county,
							typeFollow	: typeFollow
						},
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
			   }]
			})
		],
		tbar: followTbar
	}).show();
	var ta = Ext.getCmp('ext-comp-1047');
	tabsFH.setActiveTab(id);
	ta.setActiveTab(f);
							//////////////////////////////////////////
							
						//createFollowHistory(Ext.getCmp('<?php echo $tabs;?>'), 'followTab', myRecordHistory, pid, '<?php echo $typeFollow;?>', 0, userid);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Contract',
						handler: function(){
							win.close();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									pid: pid,
									userid: userid
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									
									var r=Ext.decode(response.responseText);
									
									var Digital=new Date()
									var hours=Digital.getHours()
									var minutes=Digital.getMinutes()
									var seconds=Digital.getSeconds()
									if(r.results=='error'){
										Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
									}else{ 
										if(Ext.isIE){
											window.open('http://docs.google.com/gview?url='+r.url+'?time='+hours+minutes+seconds,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
											//window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
										}else{
											window.open('http://docs.google.com/gview?url='+r.url+'?time='+hours+minutes+seconds,'_newtab');
										}
									}
									return false;
								}                                
							});
						}
					})
				]
			});
			var win = new Ext.Window({
				layout      : 'fit',
				width       : 230,
				height      : 313,
				modal	 	: true,
				plain       : true,
				items		: simple,
				closeAction : 'close',
				buttons: [{
					text     : 'Close',
					handler  : function(){
						win.close();
						loading_win.hide();
					}
				}]
			});
			win.show();
			break
		}
	}
	
	var <?php echo $progressBar;?> = new Ext.ProgressBar({
		text: ""
	});
	
	var <?php echo $progressWin;?> =new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [<?php echo $progressBar;?>]
	});
	
	var fechaAcc=<?php echo "'".date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")))."'"; ?>;
	var fechaClo=<?php echo "'".date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")))."'"; ?>;
	
	window.scroll(0,0);
</script>