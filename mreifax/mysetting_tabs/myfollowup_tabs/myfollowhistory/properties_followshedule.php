<?php
	include($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	conectar();	
	$docsDir = 'F:/ReiFaxDocs/';
	
	if(isset($_POST['type'])){
		if($_POST['type']=='delete'){
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$_POST['idfus'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='insert' || $_POST['stype']=='insert'){	
			$parcelid 		= $_POST['parcelid'];
			$userid 		= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$typeExec		= isset($_POST['typeExec']) ? $_POST['typeExec'] : 1; //manual 1, automatic 2
			// modificado por jesibel 05/10/2015 para la app reifax followup
			//$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$userid_follow = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$odate 			= strlen($_POST['odate'])>0 ? "'".$_POST['odate']."'" : 'NOW()';
			
			if(strlen($_POST['ohour'])>0){
				$ampm = explode(' ',$_POST['ohour']);
				$times = explode(':',$ampm[0]);
				if(strlen($times[1])==1) $times[1] = '0'.$times[1];
				$ohour = $times[0].':'.$times[1].' '.$ampm[1];
				
				$ohour = "'".date('H:i:s',strtotime($ohour))."'";
			}else
				$ohour		= 'NOW()';	
				
			$task 			= $_POST['task'];
			$detail 		= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$status			= strlen($_POST['status'])>0 ? $_POST['status'] : 'PENDING';
			$complete		= strlen($_POST['complete'])>0 ? $_POST['complete'] : '';
			$docs_task		= isset($_POST['sendtype']) ? intval($_POST['sendtype']) : 0;
			$body			= '';
			$subject		= '';
			$idtemplate		= -1;
			$attachEmail	= array();
			
			if(intval($task) == 1){
				$body = strlen($_POST['sms_newtemplatetext'])>0 ? $_POST['sms_newtemplatetext'] : '';
				$subject = '';
				$idtemplate = strlen($_POST['contracttemplate'])>0 && $_POST['contracttemplate']!=0 ? $_POST['contracttemplate'] : -1;
			}elseif(intval($task) == 3){
				$body = strlen($_POST['body'])>0 ? $_POST['body'] : '';
				$subject = '';
				$idtemplate = strlen($_POST['contracttemplate'])>0 && $_POST['contracttemplate']!=0 ? $_POST['contracttemplate'] : -1;
			}elseif(intval($task) == 5){
				$body = strlen($_POST['body'])>0 ? $_POST['body'] : '';
				$subject = strlen($_POST['subject'])>0 ? $_POST['subject'] : '';
				$idtemplate = strlen($_POST['contracttemplate'])>0 && $_POST['contracttemplate']!=0 ? $_POST['contracttemplate'] : -1;
			}elseif(intval($task) == 7){
				$s = explode('/',$status);
				
				if($docs_task == 3){
					$idtemplate = strlen($_POST['contracttemplatefax'])>0 && $_POST['contracttemplatefax']!=0 ? $_POST['contracttemplatefax'] : 0;
					$status = $s[0];
				}else{
					$idtemplate = strlen($_POST['contracttemplate'])>0 && $_POST['contracttemplate']!=0 ? $_POST['contracttemplate'] : 0;
					$status = $s[1];
				}
				
				$query="SELECT * FROM xima.templates WHERE userid = $userid and id = $idtemplate";
				$result = mysql_query($query) or die($query.mysql_error());
				$r = mysql_fetch_assoc($result);
				
				$subject = $r['subject'];
				$body = $r['body'];
			}
			
			if($idtemplate > 0){
				include_once($_SERVER['DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs/function_template.php'); 
				$query='SELECT bd FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$parcelid.'"';
				$result = mysql_query($query) or die($query.mysql_error());
				$r = mysql_fetch_assoc($result);
				
				conectarPorNameCounty($r['bd']);
				
				$body=replaceTemplate($body,$userid,$parcelid);
				$body=str_replace("'","\'",$body);
				
				if($subject!=''){
					$subject=replaceTemplate($subject,$userid,$parcelid);
					$subject=str_replace("'","\'",$subject);
				}
				conectar();
			}elseif($idtemplate == 0 && intval($task) == 7){
				
				$query = "SELECT x.email, x.name, x.surname, x.hometelephone, x.mobiletelephone 
				FROM xima.ximausrs x inner join xima.profile p on x.userid=p.userid
				WHERE x.userid='$userid'";
				$rs    = mysql_query($query);
				$row   = mysql_fetch_array($rs);
				
				$address    = $_POST['addr'];
				$phone		= (strlen($row[4])>0 ? $row[4] : $row[3]);
				
				
				if($userid==933 || $userid==2846 || $userid==2883){
					$subject  = "{$address}-Cash Offer, You Represent Us";
					
					$body   .= "Dear,\r\n\r\n";
					$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
					$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
					$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
					$body    .= "    \t* We close Cash. We are well funded.\r\n";
					$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
					$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
					$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
					$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
					$body   .="Thank you and have an amazing day!\r\n\r\n";
					$body    .= "Best Regards,\r\n";
					$body    .= "{$row[1]} {$row[2]}.\r\n";
					$body    .= "Summit Home Buyers, LLC\r\n";
					$body    .= "$phone\r\n";
				}else{
					$subject  = "Offer for the MLS number $mln, Address {$address}";
					if($userid==1719 || $userid==1641){
						$texto="Dear $name<br><br>";
					}else{
						$texto="Dear Mr/Mrs $name<br><br>";
					}
					$body     = $texto;
					$body   .= "Please find attached a contract with our offer regarding the ";
					$body   .= "property with the address: {$address}<br>";
					$body   .= "We wait for your prompt response<br><br>";
					$body   .= "Regards {$row[1]} {$row[2]}.<br>";
					$body   .= "$phone<br>";
					$body   .= "{$row[0]}<br><br>";
				}
			}
			
			if(isset($_FILES['file7']) && strlen(basename($_FILES['file7']['name']))>3){
				$i=7;
				$aux = 'file'.$i;
				$dir = $docsDir."schedule_contract/$userid/";
				//Crear Directorio de Attachment
				if(!is_dir($dir)){
					mkdir($dir);
				}
				
				while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
					//subir archivo
					@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
					$attachEmail[]=array('dir'=> $dir.basename($_FILES[$aux]['name']), 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.basename($_FILES[$aux]['name']));
												
					$i++;
					$aux = 'file'.$i;
				}
			}elseif(isset($_FILES['file8']) && strlen(basename($_FILES['file8']['name']))>3){
				$i=8;
				$aux = 'file'.$i;
				$dir = $docsDir."schedule_contract/$userid/";
				//Crear Directorio de Attachment
				if(!is_dir($dir)){
					mkdir($dir);
				}
				
				while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
					//subir archivo
					@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
					$attachEmail[]=array('dir'=> $dir.basename($_FILES[$aux]['name']), 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.basename($_FILES[$aux]['name']));
												
					$i++;
					$aux = 'file'.$i;
				}
			}elseif(isset($_FILES['archivo1']) && strlen(basename($_FILES['archivo1']['name']))>3){
				$i=1;
				$aux = 'archivo'.$i;
				$dir = $docsDir."schedule_contract/$userid/";
				//Crear Directorio de Attachment
				if(!is_dir($dir)){
					mkdir($dir);
				}
				
				while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
					//subir archivo
					@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
					$attachEmail[]=array('dir'=> $dir.basename($_FILES[$aux]['name']), 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.basename($_FILES[$aux]['name']));
												
					$i++;
					$aux = 'archivo'.$i;
				}
			}
			
			if(isset($_POST['file7']) && strlen($_POST['file7'])>3){
				$i=7;
				$aux = 'file'.$i;
				$dir = $docsDir."schedule_contract/$userid/";
						
				while(isset($_POST[$aux]) && strlen($_POST[$aux])>3){
					$attachEmail[]=array('dir'=> $dir.basename($_POST[$aux]), 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.basename($_POST[$aux]));
												
					$i++;
					$aux = 'file'.$i;
				}
			}
			
			if(intval($task) == 7){
				$query='INSERT INTO xima.followup_schedule (parcelid,userid,sdate,odate,ohour,task,detail,userid_follow,follow_type,status,typeExec,subject,body,idtemplate,complete_detail,docs_task,postContract)
				VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$odate.','.$ohour.',"'.$task.'","'.mysql_real_escape_string($detail).'",'.$userid_follow.',"'.$typeFollow.'","'.$status.'",'.$typeExec.',"'.mysql_real_escape_string($subject).'","'.mysql_real_escape_string($body).'",'.$idtemplate.',"'.mysql_real_escape_string($complete).'",'.$docs_task.',"'.mysql_real_escape_string(json_encode($_POST)).'")';
			}else{
				$query='INSERT INTO xima.followup_schedule (parcelid,userid,sdate,odate,ohour,task,detail,userid_follow,follow_type,status,typeExec,subject,body,idtemplate,complete_detail)
				VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$odate.','.$ohour.',"'.$task.'","'.mysql_real_escape_string($detail).'",'.$userid_follow.',"'.$typeFollow.'","'.$status.'",'.$typeExec.',"'.mysql_real_escape_string($subject).'","'.mysql_real_escape_string($body).'",'.$idtemplate.',"'.mysql_real_escape_string($complete).'")';
			}
			
			mysql_query($query) or die($query.mysql_error());
			$idinsert = mysql_insert_id();
			
			if(count($attachEmail)>0){
				$query='INSERT INTO xima.followup_schedule_attach (idfus,dir,url) VALUES ';
				foreach($attachEmail as $k => $v){
					if($k>0) $query.=', ';
					$query.='('.$idinsert.',"'.$v['dir'].'","'.$v['url'].'")';
				}
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo json_encode(array('success'=>true));
		}
		elseif($_POST['type']=='get'){	
			$idfus = $_POST['idfus'];
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$query='SELECT *,IF(hour(ohour)>12,hour(ohour)-12,hour(ohour)) hour, minute(ohour) min, if(hour(ohour)>12,\'PM\',\'AM\') ampm 
			FROM xima.followup_schedule WHERE userid='.$userid.' AND idfus='.$idfus;
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				$r = mysql_fetch_assoc($result);
				
				echo json_encode(array('success'=> true, 'data'=>$r));
			}else
				echo json_encode(array('success'=> false, 'msg'=>'No schedule found.'));
		}
		elseif($_POST['type']=='update'){				
			$idfus 			= $_POST['idfus'];
			$odate 			= strlen($_POST['odate'])>0 ? '"'.$_POST['odate'].'"' : 'NOW()';
			if(strlen($_POST['ohour'])>0){
				$ampm = explode(' ',$_POST['ohour']);
				$times = explode(':',$ampm[0]);
				if(strlen($times[1])==1) $times[1] = '0'.$times[1];
				$ohour = $times[0].':'.$times[1].' '.$ampm[1];
				
				$ohour = "'".date('H:i:s',strtotime($ohour))."'";
			}else
				$ohour		= 'NOW()';	
			$task 			= $_POST['task'];
			$typeExec		= isset($_POST['typeExec']) ? $_POST['typeExec'] : 1; //manual 1, automatic 2
			$userid_follow 	= $_COOKIE['datos_usr']['USERID']; 
			$detail 		= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$complete		= strlen($_POST['complete'])>0 ? $_POST['complete'] : '';
			$subject		= strlen($_POST['subject'])>0 ? $_POST['subject'] : '';
			$body			= strlen($_POST['body'])>0 ? $_POST['body'] : '';

			$query='UPDATE xima.followup_schedule 
			SET sdate=NOW(), odate='.$odate.', ohour='.$ohour.', 
			task="'.$task.'", detail="'.mysql_real_escape_string($detail).'", 
			complete_detail="'.mysql_real_escape_string($complete).'", 
			subject="'.mysql_real_escape_string($subject).'", body="'.mysql_real_escape_string($body).'", 
			typeExec='.$typeExec.',
			userid_follow='.$userid_follow.' 
			WHERE idfus='.$idfus;
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='complete'){
			$parcelid 	= $_POST['parcelid'];
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= $_POST['task'];
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$sheduledetail 	= strlen($_POST['sheduledetail'])>0 ? $_POST['sheduledetail'] : '';
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$_POST['idfus'].')';
			mysql_query($query) or die($query.mysql_error());
					
			echo '{success: true}';
		}elseif($_POST['type']=='complete-all'){
			$ids 	= $_POST['ids'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,task,detail,sheduledetail,userid_follow,follow_type)
			SELECT parcelid, '.$userid.', NOW(), task, "'.$detail.'", detail, '.$userid_follow.', "'.$typeFollow.'"
			FROM xima.followup_schedule
			WHERE idfus IN ('.$ids.')';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$ids.')';
			mysql_query($query) or die($query.mysql_error());
					
			echo '{success: true}';
		}elseif($_POST['type']=='delete-multi'){
			$query='DELETE FROM xima.followup_schedule WHERE idfus in ('.$_POST['idfus'].') AND userid='.$_POST['userid'];
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='voicemail'){
			$parcelid 	= $_POST['parcelid'];
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= 9;
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$sheduledetail 	= strlen($_POST['sheduledetail'])>0 ? $_POST['sheduledetail'] : '';
			
			$detail='Call Made: '.$_POST['datecall'].'. Duration: '.$_POST['duration'].'. 
					Contact: '.$_POST['contactname'].'. Phone Number: '.$_POST['phonenumber'].'. Details: '.$detail;
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			if($_POST['completetask']=='true'){
				include_once('../properties_followupEmail.php');
				completeTaskFollow($userid,0,$parcelid,9,'',0,1,1,1,0);	
			}		
			echo '{success: true}';
		}
	}else{
		$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
		$idfus 	= isset($_POST['idfus']) ? $_POST['idfus'] : '';
		$query='SELECT s.*, concat(x.name," ",x.surname) as name_follow, 
		concat(f.address,trim(concat(" ",IF(f.unit is null,"",f.unit))),", ",IF(f.city is null, "",f.city),", ",IF(f.zip is null, "", f.zip)) fulladdress
		FROM xima.followup_schedule s 
		left join xima.ximausrs x ON (s.userid_follow=x.userid)
		LEFT JOIN xima.followup f ON (s.userid=f.userid AND s.parcelid=f.parcelid)
		WHERE s.parcelid="'.$_POST['parcelid'].'" AND s.userid='.$_POST['userid'];
		'ORDER BY s.odate desc';
		
		if($idfus!=''){
			$query.=' and s.idfus='.$idfus;
		}
		$query.=' ORDER BY s.odate desc';
		$result=mysql_query($query) or die($query.mysql_error());
		$vFila=array();
		while($r=mysql_fetch_assoc($result))
			$vFila[]=$r;
			
		//echo '{success: true, total: '.count($vFila).', records:'.json_encode($vFila).'}';
		echo json_encode(array('success' => true, 'total' => count($vFila), 'records' =>$vFila, 'sql' => $query));
	}
?>