<?php
	include("../../../properties_conexion.php");
	conectarPorNameCounty($_POST['county']);
	
	$_SERVERXIMA="http://www.reifax.com/";
	
	$query="SELECT p.address,p.unit,p.city,p.zip FROM properties_php p WHERE parcelid='".$_POST['parcelid']."'";
	$result = mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	
	$address=$r['address'];
	$unit=$r['unit'];
	$county=$_POST['county'];
	$city=$r['city'];
	$zip=$r['zip'];
	
	$userid = $_POST['userid'];
	$pid = $_POST['parcelid'];
	$type = isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
?>
<div id="report_content">
	<div class="dashboard">
        <table>
            <tr>
                <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                    <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; text-align:center;">
                        <a id="followbuttonRefreshDashboard" class="overviewBotonCss3" href="javascript:void(0);">Refresh</a>
                        <label class="bluetext">
                            <?php echo $type=='B' ? 'Buying' : 'Selling';?>
                            <!--<?php echo '  '.$address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
                            <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>-->
                        </label>
                   	</div>
                    <div style="float:right; margin-right:5px; margin-top:5px; color:#FFF;">
                        <label class="bluetext">
                            <?php echo '  '.$address; if(strlen(trim($unit))>0) echo ' '.$unit;?>
                            <?php echo ', '.$county.', '.$city.', FLORIDA, '.$zip;?>
                        </label>
                   	</div>
                </th>
            </tr>
        </table>
        
        <div id="followproperties" class="columns">
            <div class="loadingBlock"></div>
            <table>
                <tr>
                    <th>
                        <span class="bluetext">Properties</span>
                    </th>
                    <th>
                        <span class="bluetext">Qty</span>
                    </th>
                </tr>
                <tr class="par" style="display:none;">
                    <td>
                        <div class="cuote"></div>All
                    </td>
                    <td>
                        <div id="all">--</div>
                    </td>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Active For Sale
                    </td>
                    <td>
                        <div id="afs">--</div>
                    </td>
                </tr>
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Non-Active
                    </td>
                    <td>
                        <div id="na">--</div>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Not For Sale
                    </td>
                    <td>
                        <div id="nfs">--</div>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Sold
                    </td>
                    <td>
                        <div id="s">--</div>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="follownew" class="columns">
            <div class="loadingBlock"></div>
            <table>
                <tr>
                    <th>
                        <span class="bluetext">New</span>
                    </th>
                    <th>
                        <span class="bluetext">Qty</span>
                    </th>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Voice Mail
                    </td>
                    <td>
                        <span id="vm">--</span>
                    </td>
                </tr>
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>SMS
                    </td>
                    <td>
                        <span id="sms">--</span>
                    </td>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Fax
                    </td>
                    <td>
                        <span id="fax">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Emails
                    </td>
                    <td>
                        <span id="email">--</span>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="followstatus" class="columns">
            <div class="loadingBlock"></div>
            <table>
                <tr>
                    <th>
                        <span class="bluetext">Status</span>
                    </th>
                    <th>
                        <span class="bluetext">Qty</span>
                    </th>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Pending Contracts
                    </td>
                    <td>
                        <div id="pc">--</div>
                    </td>
                </tr>
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Contracts Sent
                    </td>
                    <td>
                        <div id="cs">--</div>
                    </td>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Offers Received
                    </td>
                    <td>
                        <div id="or">--</div>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Proof of Funds
                    </td>
                    <td>
                        <div id="pof">--</div>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Earnest Money Deposit
                    </td>
                    <td>
                        <div id="emd">--</div>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Addendums
                    </td>
                    <td>
                        <div id="ad">--</div>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div><?php if($type=='B') echo 'Counter Offers'; else echo 'Offers';?>
                    </td>
                    <td>
                        <div id="co">--</div>
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="clear">&nbsp;</div>
        
        <table>
            <tr>
                <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                    <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; width:49%;">
                        <label class="bluetext">
                            Period: 
                        </label>
                        <select class="shortBox" id="followPeriod1" name="period1">
                            <option value="A">All</option>
                            <option value="T">Today</option>
                            <option value="LW">Last Week</option>
                            <option value="LM">Last Month</option>
                            <option value="B">Between</option>
                        </select>
                        <div class="between" id="followbetweenContainer1">
                            <label class="bluetext">
                                From: 
                            </label>
                            <input type="text" id="followfrom" name="from" />
                            <label class="bluetext">
                                To: 
                            </label>
                            <input type="text" id="followto" name="to" />
                        </div> 
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:5px; color:#FFF; width:49%;">
                        <label class="bluetext">
                            Period: 
                        </label>
                        <select class="shortBox" id="followPeriod2" name="period2">
                            <option value="A">All</option>
                            <option value="D">Due</option>
                            <option value="T">Today</option>
                            <option value="1W">1 Week</option>
                            <option value="1M">1 Month</option>
                            <option value="B">Between</option>
                        </select>
                        <div class="between" id="followbetweenContainer2">
                            <label class="bluetext">
                                From: 
                            </label>
                            <input type="text" id="followfrom2" name="from2" />
                            <label class="bluetext">
                                To: 
                            </label>
                            <input type="text" id="followto2" name="to2" />
                        </div>
                    </div>
                </th>
            </tr>
        </table>
        
        <div id="followtask" class="columns two">
            <div class="loadingBlock"></div>
            <table>
                <tr>
                    <th>
                        <span class="bluetext">Task</span>
                    </th>
                    <th>
                        <span class="bluetext">Sent</span>
                    </th>
                    <th>
                        <span class="bluetext">Received</span>
                    </th>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Call
                    </td>
                    <td>
                        <span id="scall">--</span>
                    </td>
                    <td>
                        <span id="rcall">--</span>
                    </td>
                </tr>
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>SMS
                    </td>
                    <td>
                        <span id="ssms">--</span>
                    </td>
                    <td>
                        <span id="rsms">--</span>
                    </td>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Fax
                    </td>
                    <td>
                        <span id="sfax">--</span>
                    </td>
                    <td>
                        <span id="rfax">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Email
                    </td>
                    <td>
                        <span id="semail">--</span>
                    </td>
                    <td>
                        <span id="remail">--</span>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Documents
                    </td>
                    <td>
                        <span id="sdoc">--</span>
                    </td>
                    <td>
                        <span id="rdoc">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Mail
                    </td>
                    <td>
                        <span id="smail">--</span>
                    </td>
                    <td>
                        <span id="rmail">--</span>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Voice Mail
                    </td>
                    <td>
                        <span id="svmail">--</span>
                    </td>
                    <td>
                        <span id="rvmail">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Other
                    </td>
                    <td>
                        <span id="sother">--</span>
                    </td>
                    <td>
                        <span id="rother">--</span>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="followpending" class="columns two">
            <div class="loadingBlock"></div>
            <table>
                <tr>
                    <th>
                        <span class="bluetext">Pending Task</span>
                    </th>
                    <th>
                        <span class="bluetext">Qty</span>
                    </th>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Call
                    </td>
                    <td>
                        <span id="pcall">--</span>
                    </td>
                </tr>
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>SMS
                    </td>
                    <td>
                        <span id="psms">--</span>
                    </td>
                </tr>
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Fax
                    </td>
                    <td>
                        <span id="pfax">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Email
                    </td>
                    <td>
                        <span id="pemail">--</span>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Documents
                    </td>
                    <td>
                        <span id="pdoc">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Mail
                    </td>
                    <td>
                        <span id="pmail">--</span>
                    </td>
                </tr>
                
                <tr class="par">
                    <td>
                        <div class="cuote"></div>Voice Mail
                    </td>
                    <td>
                        <span id="pvmail">--</span>
                    </td>
                </tr>
                
                <tr class="impar">
                    <td>
                        <div class="cuote"></div>Other
                    </td>
                    <td>
                        <span id="pother">--</span>
                    </td>
                </tr>
            </table>
        </div>
    </div> 
</div> 
<script>
	$(document).ready(function(){		
		loadAllData();
		$('#followPeriod1').bind('change',loadPeriod1Data);
		$('#followPeriod2').bind('change',loadPeriod2Data);
		$('#followbuttonRefreshDashboard').click(loadAllData);
		
		var dates1 = $( "#followbetweenContainer1 #followfrom, #followbetweenContainer1 #followto" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		var nowDate = new Date();
		var currMonth = nowDate.getMonth()+1;
		if(currMonth<10){
			currMonth='0'+currMonth;
		}
		var currDay = nowDate.getDate();
		if(currDay<10){
			currDay='0'+currDay;
		}
		var currYear = nowDate.getFullYear();
		dates1.val(currYear+'-'+currMonth+'-'+currDay);
		
		var dates2 = $( "#followbetweenContainer2 #followfrom2, #followbetweenContainer2 #followto2" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		dates2.val(currYear+'-'+currMonth+'-'+currDay);
		
		$('#followbetweenContainer1 #followfrom').change(loadPeriod1Data);
		$('#followbetweenContainer1 #followto').change(loadPeriod1Data);
		$('#followbetweenContainer2 #followfrom2').change(loadPeriod2Data); 
		$('#followbetweenContainer2 #followto2').change(loadPeriod2Data); 
	
	
	
		function loadProperties(){
			var loading = $('#followproperties .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=properties&userid=<?php echo $userid;?>&type=<?php echo $type;?>&pid=<?php echo $pid;?>",
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#followproperties #'+i).removeClass();
						if(v==0)
							$('#followproperties #'+i).addClass('no-check');
						else
							$('#followproperties #'+i).addClass('check');
						$('#followproperties #'+i).html('&nbsp;');
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadNew(){
			var loading = $('#follownew .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=new&userid=<?php echo $userid;?>&type=<?php echo $type;?>&pid=<?php echo $pid;?>",
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#follownew #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadStatus(){
			var loading = $('#followstatus .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=status&userid=<?php echo $userid;?>&type=<?php echo $type;?>&pid=<?php echo $pid;?>",
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#followstatus #'+i).removeClass();
						if(v==0)
							$('#followstatus #'+i).addClass('no-check');
						else
							$('#followstatus #'+i).addClass('check');
						$('#followstatus #'+i).html('&nbsp;');
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadTask(){
			var period1 = $('#followPeriod1').val();
			var from = $('#followbetweenContainer1 #followfrom').val();
			var to = $('#followbetweenContainer1 #followto').val();
			
			var loading = $('#followtask .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=task&userid=<?php echo $userid;?>&type=<?php echo $type;?>&pid=<?php echo $pid;?>&period1="+period1+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$.each(v, function(i2,v2){
							$('#followtask #'+i2).html(v2);
						});
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadPendingTask(){
			var period2 = $('#followPeriod2').val();
			var from = $('#followbetweenContainer2 #followfrom2').val();
			var to = $('#followbetweenContainer2 #followto2').val();
			
			var loading = $('#followpending .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=pending&userid=<?php echo $userid;?>&type=<?php echo $type;?>&pid=<?php echo $pid;?>&period2="+period2+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#followpending #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadAllData(){	
			loadProperties();
			loadNew();
			loadStatus();
			
			loadTask();
			loadPendingTask();
		}
		
		function loadPeriod1Data(){		
			if($('#followPeriod1').val()=='B'){
				$( "#followbetweenContainer1:hidden" ).fadeIn(300);
			}else{
				$( "#followbetweenContainer1:visible" ).fadeOut(300);
			}
			loadTask();
		}
		
		function loadPeriod2Data(){
			if($('#followPeriod2').val()=='B'){ 
				$( "#followbetweenContainer2:hidden" ).fadeIn(300);
			}else{
				$( "#followbetweenContainer2:visible" ).fadeOut(300);
			}
			loadPendingTask();
		}
	});
	
</script>