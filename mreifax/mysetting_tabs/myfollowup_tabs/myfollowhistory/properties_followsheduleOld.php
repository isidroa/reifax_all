<?php
	include("../../../properties_conexion.php");
	conectar();	
	
	if(isset($_POST['type'])){
		if($_POST['type']=='delete'){
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$_POST['idfus'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='insert'){	
			$parcelid 	= $_POST['parcelid'];
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$odate 		= strlen($_POST['odate'])>0 ? "'".$_POST['odate']."'" : 'NOW()';
			$task 		= $_POST['task'];
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			
			$query='INSERT INTO xima.followup_schedule (parcelid,userid,odate,task,detail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.','.$odate.',"'.$task.'","'.$detail.'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='update'){	
			$idfus 		= $_POST['idfus'];
			$odate 		= strlen($_POST['odate'])>0 ? "'".$_POST['odate']."'" : 'NOW()';
			$task 		= $_POST['task'];
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$query='UPDATE xima.followup_schedule SET odate='.$odate.', task="'.$task.'", detail="'.$detail.'", 
					userid_follow='.$userid_follow.' WHERE idfus='.$idfus;
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}
		elseif($_POST['type']=='complete'){
			$parcelid 	= $_POST['parcelid'];
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= $_POST['task'];
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$sheduledetail 	= strlen($_POST['sheduledetail'])>0 ? $_POST['sheduledetail'] : '';
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$_POST['idfus'].')';
			mysql_query($query) or die($query.mysql_error());
					
			echo '{success: true}';
		}elseif($_POST['type']=='complete-all'){
			$ids 	= $_POST['ids'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,task,detail,sheduledetail,userid_follow,follow_type)
			SELECT parcelid, '.$userid.', NOW(), task, "'.$detail.'", detail, '.$userid_follow.', "'.$typeFollow.'"
			FROM xima.followup_schedule
			WHERE idfus IN ('.$ids.')';
			mysql_query($query) or die($query.mysql_error());
			
			$query='DELETE FROM xima.followup_schedule WHERE idfus IN ('.$parcelid.')';
			mysql_query($query) or die($query.mysql_error());
					
			echo '{success: true}';
		}elseif($_POST['type']=='delete-multi'){
			$query='DELETE FROM xima.followup_schedule WHERE idfus in ('.$_POST['idfus'].') AND userid='.$_POST['userid'];
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='voicemail'){
			$parcelid 	= $_POST['parcelid'];
			//$userid 	= $_COOKIE['datos_usr']['USERID'];
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			$userid_follow = $_COOKIE['datos_usr']['USERID'];
			$offer 		= strlen($_POST['offer'])>0 ? $_POST['offer']:0;
			$coffer 	= strlen($_POST['coffer'])>0 ? $_POST['coffer']:0;
			$task 		= 9;
			$contract 	= $_POST['contract']=='on' ? 0:1;
			$pof 		= $_POST['pof']=='on' ? 0:1;
			$emd 		= $_POST['emd']=='on' ? 0:1;
			$rademdums 	= $_POST['rademdums']=='on' ? 0:1;
			$offerreceived 	= $_POST['offerreceived']=='on' ? 0:1;
			$detail 	= strlen($_POST['detail'])>0 ? $_POST['detail'] : '';
			$sheduledetail 	= strlen($_POST['sheduledetail'])>0 ? $_POST['sheduledetail'] : '';
			
			$detail='Call Made: '.$_POST['datecall'].'. Duration: '.$_POST['duration'].'. 
					Contact: '.$_POST['contactname'].'. Phone Number: '.$_POST['phonenumber'].'. Details: '.$detail;
			
			$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type)
			VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'","'.$sheduledetail.'",'.$userid_follow.',"'.$typeFollow.'")';
			mysql_query($query) or die($query.mysql_error());
			
			if($_POST['completetask']=='true'){
				include_once('../properties_followupEmail.php');
				completeTaskFollow($userid,0,$parcelid,9,'',0,1,1,1,0);	
			}		
			echo '{success: true}';
		}
	}else{
		$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
		$idfus 	= isset($_POST['idfus']) ? $_POST['idfus'] : '';
		$query='SELECT s.*, concat(x.name," ",x.surname) as name_follow, 
		concat(f.address,trim(concat(" ",IF(f.unit is null,"",f.unit))),", ",IF(f.city is null, "",f.city),", ",IF(f.zip is null, "", f.zip)) fulladdress
		FROM xima.followup_schedule s 
		left join xima.ximausrs x ON (s.userid_follow=x.userid)
		LEFT JOIN xima.followup f ON (s.userid=f.userid AND s.parcelid=f.parcelid)
		WHERE s.parcelid="'.$_POST['parcelid'].'" AND s.userid='.$_POST['userid'];
		'ORDER BY s.odate desc';
		
		if($idfus!=''){
			$query.=' and s.idfus='.$idfus;
		}
		$query.=' ORDER BY s.odate desc';
		$result=mysql_query($query) or die($query.mysql_error());
		$vFila=array();
		while($r=mysql_fetch_array($result))
			$vFila[]=$r;
			
		echo '{success: true, total: '.count($vFila).', records:'.json_encode($vFila).'}';
	}
?>