<?php
	include($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	include_once $_SERVER['DOCUMENT_ROOT'].'/FPDF/dompdf/dompdf_config.inc.php';
	include_once($_SERVER['DOCUMENT_ROOT'].'/mailer/class.phpmailer.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/custom_contract/functions.php");
	include_once('followemail.class.php');
	conectar();	
	
	$query="SELECT s.*, p.profname  
	FROM xima.followup_schedule s
	INNER JOIN xima.profile p ON (s.userid=p.userid) 
	INNER JOIN xima.followup f ON (s.parcelid=f.parcelid AND s.userid=f.userid)
	WHERE f.`type`!='DEL' AND s.typeExec=2 AND s.statusExec=0 
	AND 
	(
		s.odate is null 
		OR s.odate < curdate() 
		OR (s.odate = curdate() AND (s.ohour is null OR s.ohour <= curtime()))
	)
	ORDER BY s.userid,s.task,s.odate ASC,s.ohour ASC";
	$result = mysql_query($query) or die($query.mysql_error());
	
	$schedules	= array();
	$idfus 		= array();
	if(mysql_num_rows($result)>0){		
		
		while($r=mysql_fetch_assoc($result)){
			$schedules[]=$r;
			$idfus[]=$r['idfus'];
		}
		
		$query='SELECT idfus FROM xima.followup_schedule 
		WHERE idfus IN ('.implode(',',$idfus).')';
		mysql_query($query) or die($query.mysql_error());
		
		$query='UPDATE xima.followup_schedule 
		SET statusExec=1 
		WHERE idfus IN ('.implode(',',$idfus).')';
		mysql_query($query) or die($query.mysql_error());
		
		$fileHandle = fopen('log_followschedule_exec.txt','a');
		
		
		foreach($schedules as $k => $schedule){
			$userid 		= $schedule['userid'];
			$userid_follow	= $userid;
			$idfus 			= $schedule['idfus'];
			$id				= $schedule['idfus']; 
			$parcelid 		= $schedule['parcelid'];
			$typeFollow 	= $schedule['follow_type'];
			$offer 			= 0;
			$coffer 		= 0;
			$contract 		= 1;
			$pof 			= 1;
			$emd 			= 1;
			$rademdums 		= 1;
			$offerreceived 	= 1;
			$detail 		= strlen($schedule['complete_detail'])>0 	? $schedule['complete_detail'] 	: '';
			$sheduledetail 	= strlen($schedule['detail'])>0 			? $schedule['detail'] 			: '';
			
			$subject 		= strlen($schedule['subject'])>0 			? $schedule['subject'] 			: ''; 
			$body 			= strlen($schedule['body'])>0 				? $schedule['body'] 			: ''; 
			$task 			= $schedule['task'];
			$docstask		= $schedule['docs_task'];
			$user_name		= $schedule['profname'];
			$success		= false;
			$POSTContract	= strlen($schedule['postContract'])>0 		? $schedule['postContract']		: '';
			$_POST['userid'] = $userid;
			
			//Log comment
			fwrite($fileHandle,"\n".date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': id schedule '.$idfus.', followType '.$typeFollow.". \n");
			fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': type of task '.$task.', docs task '.$docstask.". \n");
			
			switch($task){
				case 1: //send sms
					$q="SELECT a.* 
					FROM xima.follow_assignment f
					INNER JOIN xima.followagent a ON (f.userid=a.userid AND f.agentid=a.agentid)
					WHERE f.userid=$userid AND f.parcelid='".$parcelid."' AND f.principal=1";
					$res = mysql_query($q) or die(json_encode(array('success' => false, 'error'=>$q)));
					$r=mysql_fetch_array($res);							
					
					$agentName = $r['agent'];
					$agentcell='';
					if($r['typeph1']==2){
						$agentcell=$r['phone1'];
					}else if($r['typeph2']==2){
						$agentcell=$r['phone2'];
					}else if($r['typeph3']==2){
						$agentcell=$r['phone3'];
					}else if($r['typeph4']==2){
						$agentcell=$r['phone4'];
					}else if($r['typeph5']==2){
						$agentcell=$r['phone5'];
					}else if($r['typeph6']==2){
						$agentcell=$r['phone6'];
					}
					
					if($agentcell!=''){
						$agentcell = ereg_replace("[^0-9]", "", $agentcell);
						
						$query='SELECT * 
						FROM xima.follow_sms 
						where phone='.$agentcell;
						$result = mysql_query($query) or die($query.mysql_error());
						
						$smsService = 'xminder';
						//$smsService = 'carrierlookup';
						
						$sendTo = '';
						if(mysql_num_rows($result)==0){
							switch($smsService){
								case 'carrierlookup':
									$url = "http://www.carrierlookup.com/index.php/api/lookup?key=78873e0d94b469388f52be19f62b0d540fa5d9e7&number=".$agentcell;
									
									$ch = curl_init();
									$timeout = 5;
									curl_setopt($ch, CURLOPT_URL, $url);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
									$data = curl_exec($ch);
									curl_close($ch);
									
									$result = json_decode($data);
									
									$result = $result->Response;
									$carrier = strtolower($result->carrier);
									
									if($result->carrier_type=='mobile' && strlen($carrier)>0){
										$query='SELECT * FROM xima.follow_carriers WHERE carrier_search LIKE "%'.strtolower($carrier).'%" LIMIT 1';
										$result = mysql_query($query);
										if($result === false){
											//Saved error in pending task.
											$query='UPDATE xima.followup_schedule 
											SET status="Failed sent sms! Wrong phone number." 
											WHERE idfus='.$idfus;
											mysql_query($query);
											$success = false;
											
										}
										
										if(mysql_num_rows($result)>0){
											$sms = mysql_fetch_assoc($result);
											
											$sendTo = $agentcell.$sms['sms_email'];
											
											$query="INSERT INTO xima.follow_sms (phone,sms_email,mms_email,carrier,date_insert) 
											VALUES ('$agentcell','$sendTo','".($agentcell.$sms['mms_email'])."','".$carrier."',NOW())";
											mysql_query($query);
										}else{
											//Saved error in pending task.
											$query='UPDATE xima.followup_schedule 
											SET status="Failed sent sms! Wrong phone number." 
											WHERE idfus='.$idfus;
											mysql_query($query);
											$success = false;
											fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Failed sent sms! Wrong phone number.'."\n");
										}
									}
									
									if($result->carrier_type=='landline'){
										$query="INSERT INTO xima.follow_sms (phone,sms_email,mms_email,carrier,date_insert,status) 
										VALUES ('$agentcell','','','',NOW(),1)";
										mysql_query($query);
										
										//Saved error in pending task.
										$query='UPDATE xima.followup_schedule 
										SET status="Failed sent sms! Wrong phone number." 
										WHERE idfus='.$idfus;
										mysql_query($query);
										$success = false;
										fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Failed sent sms! Wrong phone number.'."\n");
									}
								break;
								case 'xminder':
									$url = "http://x2text.xminder.com/LCR-API/api.php?uuid=87fb3a19-3a94-4cb7-f0a5-0ff1a8b29714&p1=".$agentcell;
									$xml = simplexml_load_file($url);
									
									if($xml === false){
										//Saved error in pending task.
										$query='UPDATE xima.followup_schedule 
										SET status="Failed sent sms! Wrong phone number." 
										WHERE idfus='.$idfus;
										mysql_query($query);
										$success = false;
									}
									
									$result = $xml->results->result;
									if($result->status == 'OK' && $result->wless == 'y'){
										$sendTo = ''.$result->sms_address;
										
										$query="INSERT INTO xima.follow_sms (phone,sms_email,mms_email,carrier,date_insert) 
										VALUES ('$agentcell','$sendTo','".$result->mms_address."','".$result->carrier_name."',NOW())";
										mysql_query($query);
									}else{
										$query="INSERT INTO xima.follow_sms (phone,sms_email,mms_email,carrier,date_insert,status) 
										VALUES ('$agentcell','','','',NOW(),1)";
										mysql_query($query);
										
										//Saved error in pending task.
										$query='UPDATE xima.followup_schedule 
										SET status="Failed sent sms! Wrong phone number.", statusExec=0 
										WHERE idfus='.$idfus;
										mysql_query($query);
										$success = false;
										fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Failed sent sms! Wrong phone number.'."\n");
									}
								break;
							}
						}else{
							$r = mysql_fetch_assoc($result);
							if($r['status']==0) $sendTo = $r['sms_email'];
							else{
								//Saved error in pending task.
								$query='UPDATE xima.followup_schedule 
								SET status="Failed sent sms! Wrong phone number.", statusExec=0  
								WHERE idfus='.$idfus;
								mysql_query($query);
								$success = false;
								fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Failed sent sms! Wrong phone number.'."\n");
							}
						}
						
						if($sendTo!=''){
							$content = composeEmail($userid, NULL, $parcelid, $sendTo, 'Sent SMS to '.$rname.' ('.$agentcell.')', $body, '1', false, $typeFollow, false);
							
							$respuesta=json_decode($content,true);
							if($respuesta['msg']==''){
								$success 	= true;
								$detail 	= '<a href="javascript:viewMailDetail('.$respuesta['id'].','.$userid.',\''.$parcelid.'\',tabsFollow);" >View Content.</a>';
								fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': SMS success.'."\n");
							}else{
								$enviadoagente='0';
								//Saved error in pending task.
								$query='UPDATE xima.followup_schedule 
								SET status="ERROR: '.$respuesta['msg'].'", statusExec=0 
								WHERE idfus='.$idfus;
								mysql_query($query);
								$success = false;
								fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: '.$respuesta['msg'].". \n");
							}
						}
					}else{
						//Saved error in pending task.
						$query='UPDATE xima.followup_schedule 
						SET status="ERROR: Phone not found.", statusExec=0  
						WHERE idfus='.$idfus;
						mysql_query($query);
						$success = false;
						fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Phone not found.'."\n");
					}
				break;
				
				case 3: //send fax
					$q="SELECT a.* 
					FROM xima.follow_assignment f
					INNER JOIN xima.followagent a ON (f.userid=a.userid AND f.agentid=a.agentid)
					WHERE f.userid=$userid AND f.parcelid='".$parcelid."' AND f.principal=1";
					$res = mysql_query($q) or die(json_encode(array('success' => false, 'error'=>$q)));
					$r=mysql_fetch_array($res);							
					
					$agentName 	= $r['agent'];
					$agentEmail	= $r['email'];
					$agentfax	= '';
					if($r['typeph1']==3){
						$agentfax=$r['phone1'];
					}else if($r['typeph2']==3){
						$agentfax=$r['phone2'];
					}else if($r['typeph3']==3){
						$agentfax=$r['phone3'];
					}else if($r['typeph4']==3){
						$agentfax=$r['phone4'];
					}else if($r['typeph5']==3){
						$agentfax=$r['phone5'];
					}else if($r['typeph6']==3){
						$agentfax=$r['phone6'];
					}else if(strlen($r['fax'])>0){
						$agentfax=$r['fax'];
					}
					if($agentfax==''){
						if($r['typeph1']==6){
							$agentfax=$r['phone1'];
						}else if($r['typeph2']==6){
							$agentfax=$r['phone2'];
						}else if($r['typeph3']==6){
							$agentfax=$r['phone3'];
						}else if($r['typeph4']==6){
							$agentfax=$r['phone4'];
						}else if($r['typeph5']==6){
							$agentfax=$r['phone5'];
						}else if($r['typeph6']==6){
							$agentfax=$r['phone6'];
						}else if(strlen($r['fax'])>0){
							$agentfax=$r['fax'];
						}
					}

					if($agentfax!=''){
						$idinsert = sendFAX($userid,$parcelid,$id,$body,$agentfax,$typeFollow); 
						
						if($idinsert!==false){
							$success 	= true;
							$detail 	= '<a href="javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$parcelid.'\',tabsFollow);" >View Content.</a>';
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': FAX success.'."\n");
						}else{
							//Saved error in pending task.
							$query='UPDATE xima.followup_schedule 
							SET status="ERROR: Failure sending FAX, please try again.", statusExec=0  
							WHERE idfus='.$idfus;
							mysql_query($query);
							$success = false;
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Failure sending FAX, please try again.'."\n");
						}
					}else{
						//Saved error in pending task.
						$query='UPDATE xima.followup_schedule 
						SET status="ERROR: Fax not found.", statusExec=0  
						WHERE idfus='.$idfus;
						mysql_query($query);
						$success = false;
						fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Fax not found.'."\n");
					}
				break;
				
				case 5: //send email
					$q="SELECT a.* 
					FROM xima.follow_assignment f
					INNER JOIN xima.followagent a ON (f.userid=a.userid AND f.agentid=a.agentid)
					WHERE f.userid=$userid AND f.parcelid='".$parcelid."' AND f.principal=1";
					$res = mysql_query($q) or die($q.mysql_error());
					$r=mysql_fetch_array($res);							
					
					$agentName 	= $r['agent'];
					$agentEmail	= $r['email'];
					if($agentEmail==''){
						$agentEmail = $r['email2'];
					}elseif($agentEmail==''){
						$agentEmail = $r['email3'];
					}elseif($agentEmail==''){
						$agentEmail = $r['email4'];
					}elseif($agentEmail==''){
						$agentEmail = $r['email5'];
					}
					
					if($agentEmail!=''){
						
						$idinsert = sendEMAIL($userid,$parcelid,$id,$subject,$body,$agentEmail,$typeFollow);
						
						if($idinsert!==false){
							$success 	= true;
							$detail 	= '<a href="javascript:viewMailDetail('.$idinsert.','.$userid.',\''.$parcelid.'\',tabsFollow);" >View Content.</a>';
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': EMAIL success.'."\n");
						}else{
							//Saved error in pending task.
							$query='UPDATE xima.followup_schedule 
							SET status="ERROR: Failure sending email, please try again.", statusExec=0  
							WHERE idfus='.$idfus;
							mysql_query($query);
							$success = false;
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Failure sending email, please try again.'."\n");
						}
					}else{
						//Saved error in pending task.
						$query='UPDATE xima.followup_schedule 
						SET status="ERROR: Email not found.", statusExec=0  
						WHERE idfus='.$idfus;
						mysql_query($query);
						$success = false;
						fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Email not found.'."\n");
					}
				break;
				
				case 7: //send docs
					$q="SELECT a.* 
					FROM xima.follow_assignment f
					INNER JOIN xima.followagent a ON (f.userid=a.userid AND f.agentid=a.agentid)
					WHERE f.userid=$userid AND f.parcelid='".$parcelid."' AND f.principal=1";
					$res = mysql_query($q) or die(json_encode(array('success' => false, 'error'=>$q)));
					$r=mysql_fetch_array($res);	
					
					if($docstask==5){
						$agentName 	= $r['agent'];
						$agentEmail	= $r['email'];
						if($agentEmail==''){
							$agentEmail = $r['email2'];
						}elseif($agentEmail==''){
							$agentEmail = $r['email3'];
						}elseif($agentEmail==''){
							$agentEmail = $r['email4'];
						}elseif($agentEmail==''){
							$agentEmail = $r['email5'];
						}
						
						if($agentEmail!=''){
							if($POSTContract!=''){
								$idinsert = sendDOCS($userid,$parcelid,$id,$subject,$body,$agentEmail,$agentfax,$docstask,$typeFollow,'false',$POSTContract);
								//print_r($idinsert);
								if($idinsert!==false){
									$query='DELETE FROM xima.followup_schedule WHERE idfus='.$id;
									mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
									$success = false;
									fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': DOCS EMAIL success.'."\n");
								}else{
									$success = false;
									fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': DOCS EMAIL failed.'."\n");
								}
							}else{
								//Saved error in pending task.
								$query='UPDATE xima.followup_schedule 
								SET status="ERROR: Insufficient data to complete the task, please delete and re-schedule." 
								WHERE idfus='.$idfus;
								mysql_query($query);
								$success = false;
							}
						}else{
							//Saved error in pending task.
							$query='UPDATE xima.followup_schedule 
							SET status="ERROR: Email not found.", statusExec=0 
							WHERE idfus='.$idfus;
							mysql_query($query);
							$success = false;
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Email not found.'."\n");
						}
					}else{
						$agentName 	= $r['agent'];
						$agentEmail	= $r['email'];
						$agentfax	= '';
						if($r['typeph1']==3){
							$agentfax=$r['phone1'];
						}else if($r['typeph2']==3){
							$agentfax=$r['phone2'];
						}else if($r['typeph3']==3){
							$agentfax=$r['phone3'];
						}else if($r['typeph4']==3){
							$agentfax=$r['phone4'];
						}else if($r['typeph5']==3){
							$agentfax=$r['phone5'];
						}else if($r['typeph6']==3){
							$agentfax=$r['phone6'];
						}else if(strlen($r['fax'])>0){
							$agentfax=$r['fax'];
						}
						if($agentfax==''){
							if($r['typeph1']==6){
								$agentfax=$r['phone1'];
							}else if($r['typeph2']==6){
								$agentfax=$r['phone2'];
							}else if($r['typeph3']==6){
								$agentfax=$r['phone3'];
							}else if($r['typeph4']==6){
								$agentfax=$r['phone4'];
							}else if($r['typeph5']==6){
								$agentfax=$r['phone5'];
							}else if($r['typeph6']==6){
								$agentfax=$r['phone6'];
							}else if(strlen($r['fax'])>0){
								$agentfax=$r['fax'];
							}
						}
						
						if($agentfax!=''){
							if($POSTContract!=''){
								$idinsert = sendDOCS($userid,$parcelid,$id,$subject,$body,$agentEmail,$agentfax,$docstask,$typeFollow,'false',$POSTContract);
								
								if($idinsert!==false){
									$query='DELETE FROM xima.followup_schedule WHERE idfus='.$id;
									mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
									$success = false;
									fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': DOCS FAX success.'."\n");
								}else{
									$success = false;
									fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': DOCS FAX failed.'."\n");
								}
							}else{
								//Saved error in pending task.
								$query='UPDATE xima.followup_schedule 
								SET status="ERROR: Insufficient data to complete the task, please delete and re-schedule." 
								WHERE idfus='.$idfus;
								mysql_query($query);
								$success = false;
							}
						}else{
							//Saved error in pending task.
							$query='UPDATE xima.followup_schedule 
							SET status="ERROR: Fax not found.", statusExec=0 
							WHERE idfus='.$idfus;
							mysql_query($query);
							$success = false;
							fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': ERROR: Fax not found.'."\n");
						}
					}
				break;
				
				default: 
					$success = true;
					fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Regular Task.'."\n");
				break;
			}
			
			if($success){
				$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived,detail,sheduledetail,userid_follow,follow_type,typeExec)
				VALUES ("'.$parcelid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.mysql_real_escape_string($detail).'","'.mysql_real_escape_string($sheduledetail).'",'.$userid_follow.',"'.$typeFollow.'",2)';
				mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
				fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Insert history.'."\n");
				
				$query='DELETE FROM xima.followup_schedule WHERE idfus='.$id;
				mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));
				fwrite($fileHandle,date('Y-m-d H:i:s').' - '.$userid.' ('.$userid_follow.') - pid '.$parcelid.': Deleted Schedule.'."\n");
			}
		}
		
		fclose($fileHandle);
	}
?>
<script>
	window.close();
</script>