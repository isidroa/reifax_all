<?php
	include("../../properties_conexion.php");   
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYShortSale','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	$query='SELECT * FROM xima.shortsale_processor
	WHERE userid_processor='.$userid;
	$result=mysql_query($query) or die($query.mysql_error());
	$r = mysql_fetch_assoc($result);
	$idProcessor = $r['id_processor'];
	
	$query = "SELECT b.id_require,b.gridName,b.displayName,b.descName
	FROM xima.shortsale_require b 
	INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
	WHERE b.status=1 
	ORDER BY c.order,b.ordShow";
	$result = mysql_query($query);
	$gridCampos=array();
	
	while($r=mysql_fetch_array($result)){
		$gridCampos[]=array('desc'=>str_replace("'","\'",$r['descName']), 'name'=>$r['gridName'], 'longName'=>$r['displayName'], 'data'=>$r['gridName']);
	}
	//echo 'aqui'; return;
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mysspfollowup_panel" style="background-color:#FFF;border-color:#FFF">
	<!--<br clear="all" />-->
	<div id="mysspfollowup_data_div" align="center" style=" background-color:#FFF; margin:auto;">
  		<div id="mysspfollowup_filters"></div>
        <!--<div align="left" style="color:#000; font-size:14px;">
        	Click View Icon -> Display Menu Options / Click Row -> Follow History / Click Email Icon -> Follow Email
       	</div>--><br />
        <div id="mysspfollowup_properties" align="left"></div> 
	</div>
</div>
<script type="text/javascript" src="/mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>
<script>
	var useridspider				= <?php echo $userid;?>;
	var limitmysspfollowup 			= 50;
	var selected_datamysspfollowup 	= new Array();
	var AllCheckmysspfollowup 		= false;
	var idProcessor					= <?php echo $idProcessor;?>;
	
	//filter variables
	var filtersSSP = {
		property		: {
			address		: '',
			county		: 'ALL'
		},
		ss_status		: 'ALL',
		processor		: 'ALL',
		user			: 'ALL',
		package			: 'ALL',
		set				: 1,
		order			: {
			field		: 'address',
			direction	: 'ASC'
		},
		status			: {
		<?php foreach($gridCampos as $k=>$val){
			if($k>0) echo ',';
			echo "s".$val['data'].": '-1'";
		}?>
		}
	};
	
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	var storemysspfollowup = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'ss_status','usuario','package',
			   {name: 'id_processor', type: 'int'},
			   {name: 'id_packages', type: 'int'},
			   {name: 'id_shortsale', type: 'int'}";
			   foreach($gridCampos as $k=>$val){
		   			echo ",'".$val['data']."'";
			   }
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'idProcessor': idProcessor
		},
		remoteSort: true,
		sortInfo: {
			field: filtersSSP.order.field,
			direction: filtersSSP.order.direction
		},
		listeners: {
			'beforeload': function(store,obj){
				storemysspfollowupAll.load();
				AllCheckmysspfollowup=false;
				selected_datamysspfollowup=new Array();
				smmysspfollowup.deselectRange(0,limitmysspfollowup);
				obj.params.address=filtersSSP.property.address;
				obj.params.county=filtersSSP.property.county;
				obj.params.ss_status=filtersSSP.ss_status;
				obj.params.processor=filtersSSP.processor;
				obj.params.user=filtersSSP.user;
				obj.params.package=filtersSSP.package;
				<?php foreach($gridCampos as $k=>$val){
					echo "obj.params."."s".$val['data']."=filtersSSP.status."."s".$val['data'].";";
				}?>
				
			},
			'load' : function (store,data,obj){
				if (AllCheckmysspfollowup){
					Ext.get(gridmysspfollowup.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmysspfollowup=true;
					gridmysspfollowup.getSelectionModel().selectAll();
					selected_datamysspfollowup=new Array();
				}else{
					AllCheckmysspfollowup=false;
					Ext.get(gridmysspfollowup.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamysspfollowup.length > 0){
						for(val in selected_datamysspfollowup){
							var ind = gridmysspfollowup.getStore().find('pid',selected_datamysspfollowup[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmysspfollowup.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemysspfollowupAll = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'ss_status','usuario','package',
			   {name: 'id_processor', type: 'int'},
			   {name: 'id_packages', type: 'int'},
			   {name: 'id_shortsale', type: 'int'}";
			   foreach($gridCampos as $k=>$val){
		   			echo ",'".$val['data']."'";
			   }
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'idProcessor': idProcessor
		},
		remoteSort: true,
		sortInfo: {
			field: filtersSSP.order.field,
			direction: filtersSSP.order.direction
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersSSP.property.address;
				obj.params.county=filtersSSP.property.county;
				obj.params.ss_status=filtersSSP.ss_status;
				obj.params.processor=filtersSSP.processor;
				obj.params.user=filtersSSP.user;
				obj.params.package=filtersSSP.package;
				<?php foreach($gridCampos as $k=>$val){
					echo "obj.params."."s".$val['data']."=filtersSSP.status."."s".$val['data'].";";
				}?>
			}
		}
    });
	
	var smmysspfollowup = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamysspfollowup.indexOf(record.get('id_shortsale'))==-1)
					selected_datamysspfollowup.push(record.get('id_shortsale'));
				
				if(Ext.fly(gridmysspfollowup.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmysspfollowup=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamysspfollowup = selected_datamysspfollowup.remove(record.get('id_shortsale'));
				AllCheckmysspfollowup=false;
				Ext.get(gridmysspfollowup.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmySSPfollow=new Ext.Toolbar({
		renderTo: 'mysspfollowup_filters',
		items: [
			new Ext.Button({
				tooltip: 'Print Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamysspfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmysspfollowup==true){
						var totales = storemysspfollowupAll.getRange(0,storemysspfollowupAll.getCount());
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1;i<storemysspfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.id_shortsale+'\'';	
						}
					}else{	
						var pids='\''+selected_datamysspfollowup[0]+'\'';
						for(i=1; i<selected_datamysspfollowup.length; i++)
							pids+=',\''+selected_datamysspfollowup[i]+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 7,
							sort: filtersSSP.order.field,
							dir: filtersSSP.order.direction,
							typeFollow: 'SSP',
							pids: pids,
							set: filtersSSP.set,
							idProcessor: idProcessor
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamysspfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmysspfollowup==true){
						var totales = storemysspfollowupAll.getRange(0,storemysspfollowupAll.getCount());
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1;i<storemysspfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.id_shortsale+'\'';	
						}
					}else{	
						var pids='\''+selected_datamysspfollowup[0]+'\'';
						for(i=1; i<selected_datamysspfollowup.length; i++)
							pids+=',\''+selected_datamysspfollowup[i]+'\''; 
					}
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 8,
							sort: filtersSSP.order.field,
							dir: filtersSSP.order.direction,
							typeFollow: 'SSP',
							pids: pids,
							idProcessor: idProcessor
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmyfollowupSSP = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						id: 'formmyfollowupSSP',
						name: 'formmyfollowupSSP',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:3},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersSSP.property.address,	
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSSP.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type			: 'load-countys',
											id_processor	:	<?php echo $idProcessor;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtersSSP.property.county,
									hiddenName    : 'fcounty',
									hiddenValue   : filtersSSP.property.county,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSSP.property.county = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'SS Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','ALL'],
											['Preparing Package','Preparing Package'],
											['Package Complete','Package Complete'],
											['Package Faxed','Package Faxed'],
											['Package Received','Package Received'],
											['BPO Complete','BPO Complete'],
											['Counter Offer','Counter Offer'],
											['Verbal Agreement','Verbal Agreement'],
											['Written Agreement','Written Agreement'],
											['Closed','Closed']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatus',
									value         : filtersSSP.ss_status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSSP.ss_status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fuser',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Users',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-users',
											'idProcessor': idProcessor,
											'ALL': true
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fusername',
									value         : filtersSSP.user,
									hiddenName    : 'fuser',
									hiddenValue   : filtersSSP.user,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSSP.user = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								colspan	: 2,
								id		: 'fpackage',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Package',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-packages',
											'userid': <?php echo $userid;?>,
											processor: idProcessor,
											'ALL': true
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpackagename',
									value         : filtersSSP.package,
									hiddenName    : 'fpackage',
									hiddenValue   : filtersSSP.package,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSSP.package = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('processor',idProcessor); 
										}
									}
								}]
							}
							<?php foreach($gridCampos as $k=>$val){?>
							,{
								layout	: 'form',
								id		: 'f<?php echo $val['data'];?>',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : '<?php echo $val['data'];?>',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Not Required'],
											['1','Required'],
											['2','Uploaded'],
											['3','Acepted'],
											['4','Rejected']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'f<?php echo $val['data'];?>name',
									value         : filtersSSP.status.<?php echo "s".$val['data'];?>,
									hiddenName    : 'f<?php echo $val['data'];?>',
									hiddenValue   : filtersSSP.status.<?php echo "s".$val['data'];?>,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSSP.status.<?php echo "s".$val['data'];?> = record.get('valor');
										}
									}
								}]
							}
							<?php }?>
							]
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								//icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Load&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'load-filters'
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning',output);
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											if(rest.total==0){
												Ext.MessageBox.alert('Warning','You don\'t have default filters saved');
											}else{
												formmyfollowupSSP.getForm().findField('faddress').setValue(rest.data.address);
												formmyfollowupSSP.getForm().findField('fstatus').setValue(rest.data.status);
												formmyfollowupSSP.getForm().findField('fcounty').setValue(rest.data.county);
												formmyfollowupSSP.getForm().findField('fuser').setValue(rest.data.user);
												formmyfollowupSSP.getForm().findField('fpackage').setValue(rest.data.package);
												<?php foreach($gridCampos as $k=>$val){
													echo "formmyfollowupSSP.getForm().findField('f".$val['data']."').setValue(rest.data.".('s'.$val['data']).");";
												}?>
											}
										}
									});
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/save.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Save&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid: useridspider,
											type: 'save-filters',
											address: formmyfollowupSSP.getForm().findField('faddress').getValue(),
											user: formmyfollowupSSP.getForm().findField('fuser').getValue(),
											package: formmyfollowupSSP.getForm().findField('fpackage').getValue(),
											status: formmyfollowupSSP.getForm().findField('fstatus').getValue(),
											county: formmyfollowupSSP.getForm().findField('fcounty').getValue(),
											<?php foreach($gridCampos as $k=>$val){
												if($k>0) echo ',';
												echo "s".$val['data'].": formmyfollowupSSP.getForm().findField('f".$val['data']."').getValue()";
											}?>
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Error saving filters');
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											Ext.MessageBox.alert('Warning','Default search successfully saved');
										}
									});
								}				
							},'->',{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSSP.property.address = formmyfollowupSSP.getForm().findField('faddress').getValue();
									filtersSSP.user 			= formmyfollowupSSP.getForm().findField('fuser').getValue();
									filtersSSP.package 			= formmyfollowupSSP.getForm().findField('fpackage').getValue();
									filtersSSP.ss_status 		= formmyfollowupSSP.getForm().findField('fstatus').getValue();
									filtersSSP.property.county 	= formmyfollowupSSP.getForm().findField('fcounty').getValue();
									<?php foreach($gridCampos as $k=>$val){
										echo "filtersSSP.status."."s".$val['data']."= formmyfollowupSSP.getForm().findField('f".$val['data']."').getValue();";
									}?>
									
									storemysspfollowup.load({params:{start:0, limit:limitmysspfollowup}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSSP.property.address = '';
									filtersSSP.processor = 'ALL';
									filtersSSP.user = 'ALL';
									filtersSSP.package = 'ALL';
									filtersSSP.ss_status = 'ALL';
									filtersSSP.property.county = 'ALL';
									<?php foreach($gridCampos as $k=>$val){
										echo "filtersSSP.status."."s".$val['data']."= -1;";
									}?>
									
									formmyfollowupSSP.getForm().findField('faddress').setValue('');
									formmyfollowupSSP.getForm().findField('fuser').setValue('ALL');
									formmyfollowupSSP.getForm().findField('fpackage').setValue('ALL');
									formmyfollowupSSP.getForm().findField('fstatus').setValue('ALL');
									formmyfollowupSSP.getForm().findField('fcounty').setValue('ALL');
									<?php foreach($gridCampos as $k=>$val){
										echo "formmyfollowupSSP.getForm().findField('f".$val['data']."').setValue('-1');";
									}?>
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 980,
								height      : 650,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowupSSP,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersSSP.property.address = '';
					filtersSSP.processor = 'ALL';
					filtersSSP.user = 'ALL';
					filtersSSP.package = 'ALL';
					filtersSSP.ss_status = 'ALL';
					filtersSSP.property.county = 'ALL';
					<?php foreach($gridCampos as $k=>$val){
						echo "filtersSSP.status."."s".$val['data']."= -1;";
					}?>
					
					storemysspfollowup.load({params:{start:0, limit:limitmysspfollowup}});
				}
			}),new Ext.Button({
				tooltip: 'View / Download Required Docs',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/icons_jesus/Preview-24.png',
				handler: function(){
					if(selected_datamysspfollowup.length<=0 || selected_datamysspfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package.'); return false;
					}
					var record = gridmysspfollowup.getSelectionModel().getSelected();
					var title = (record.get('address')+', '+record.get('usuario')+' - '+record.get('package'));
					
					openUploadedDocs(tabs, 'followSSUDTab', record.get('userid'), title, record.get('id_shortsale'), record.get('id_packages'), gridmysspfollowup.getStore(), true);
				}
			}),new Ext.Button({
				tooltip: 'View Status / Upload Required Docs',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/icons_jesus/sign-up-24.png',
				handler: function(){
					if(selected_datamysspfollowup.length<=0 || selected_datamysspfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package.'); return false;
					}
					var record = gridmysspfollowup.getSelectionModel().getSelected();
					var title = (record.get('address')+', '+record.get('usuario')+' - '+record.get('package'));
					
					openRequiredDocs(tabs, 'followSSRDTab', record.get('userid'), title, record.get('id_shortsale'), record.get('id_packages'), gridmysspfollowup.getStore(), true);
				}
			})
		]
	});
	
	var progressBarFollowSSP= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarFollow_winSSP=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarFollowSSP]
	});
	
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	function renderNumeros(val){
		return '$'+val;
	}
	function checkShortSaleRender(value, metaData, rec, rowIndex, colIndex, store){
		switch(value){
			case '0': 
			return '<div title="Not Required" style="background-color: #D3D3D3; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;			
			
			case '1': 
			return '<div title="Required" style="background-color: #D01616; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '2': 
			return '<div title="Uploaded" style="background-color: #083772; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '3': 
			return '<div title="Acepted" style="background-color: #66ab16; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '4': 
			return '<div title="Rejected" style="background-color: #EDED00; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
		}
	}
	
	var gridmysspfollowup = new Ext.grid.GridPanel({
		renderTo: 'mysspfollowup_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemysspfollowup,
		stripeRows: true,
		sm: smmysspfollowup, 
		columns: [
			smmysspfollowup,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
				echo ",{header: 'SS Status', width: 100, sortable: true, tooltip: 'Short Sale Status', dataIndex: 'ss_status', menuDisabled: true}";	
		   		foreach($hdArray as $k=>$val){
		   			if($val->name!='status')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'User', width: 80, sortable: true, tooltip: 'Short Sale User Name', dataIndex: 'usuario'}";
				echo ",{header: 'Package', width: 80, sortable: true, tooltip: 'Short Sale Package Name', dataIndex: 'package'}";
				foreach($gridCampos as $k=>$val){
					echo ",{header: '".$val['name']."', width: 25, hidden: ".($k>19 ? 'true' : 'false').", sortable: true, tooltip: '".$val['desc']."', dataIndex: '".$val['data']."', renderer: checkShortSaleRender, menuDisabled: true}";
				}
				
		   ?>			 
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmysspfollowup',
            pageSize: limitmysspfollowup,
            store: storemysspfollowup,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmysspfollowup=50;
					Ext.getCmp('pagingmysspfollowup').pageSize = limitmysspfollowup;
					Ext.getCmp('pagingmysspfollowup').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupSSP'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmysspfollowup=80;
					Ext.getCmp('pagingmysspfollowup').pageSize = limitmysspfollowup;
					Ext.getCmp('pagingmysspfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSSP'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmysspfollowup=100;
					Ext.getCmp('pagingmysspfollowup').pageSize = limitmysspfollowup;
					Ext.getCmp('pagingmysspfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSSP'
			}),
			'-',{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 150,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
					<?php
						$page = (count($gridCampos) / 20.00);
						$i=0.00;
						do{
							$i++;
							if($i>1) echo ',';
							echo "['$i','Set $i']";
						}while($i<$page)
					?>
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				value		  : '1',
				name          : 'gridcombo',
				allowBlank    : false,
				listeners	  : {
					'select'  : function(combo, record, index){
						var col = gridmysspfollowup.getColumnModel();
						var ini = 0, page = record.get('valor'), i=0;
						filtersSSP.set = page;
						
						for(i=ini; i<<?php echo count($gridCampos);?>; i++){
							if(Math.floor(i/20.0) == (parseInt(page)-1))
								col.setHidden(i+7, false);
							else
								col.setHidden(i+7, true);
						}
					}
				}
			}]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filtersSSP.order.field=sorted.field;
				filtersSSP.order.direction=sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = e ? grid.getView().findCellIndex(e.getTarget()) : -1; 
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var id_shortsale = record.get('id_shortsale');
				var id_packages = record.get('id_packages');
				var userid = record.get('userid');
				var ss_status = record.get('ss_status');
				var typeFollow = 'SSP';
				
				if(cell == 2){
					var formStatus  = new Ext.FormPanel({
						border     : true,
						bodyStyle  : 'padding: 10px; text-align:left;',
						frame      : true,
						items		: [{
							xtype         : 'combo',
							mode          : 'local',
							fieldLabel    : 'SS Status',
							triggerAction : 'all',
							width		  : 130,
							store         : new Ext.data.ArrayStore({
								id        : 0,
								fields    : ['valor', 'texto'],
								data      : [
									['Preparing Package','Preparing Package'],
									['Package Complete','Package Complete'],
									['Package Faxed','Package Faxed'],
									['Package Received','Package Received'],
									['BPO Complete','BPO Complete'],
									['Counter Offer','Counter Offer'],
									['Verbal Agreement','Verbal Agreement'],
									['Written Agreement','Written Agreement'],
									['Closed','Closed']
								]
							}),
							displayField  : 'texto',
							valueField    : 'valor',
							name          : 'ss_status',
							value         : ss_status,
							allowBlank    : false
						}],
						buttonAlign :	'center',
						buttons		:	[{
							text    :	'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Apply</b></span>',
							handler	:	function(){
								formStatus.getForm().submit({
									url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
									waitMsg : 'Saving...',
									success : function(f, a){ 
										winStatus.close();
										var resp = a.result;
										if(resp.success){
											storemysspfollowup.reload();
										}
										Ext.MessageBox.alert('SS Status', resp.mensaje);
									},
									params:{
										type		:	'changeStatus',
										id_shortsale:	id_shortsale
									},
									failure : function(f, a){ 
										var resp = a.result;
										Ext.MessageBox.alert('SS Status', resp.mensaje);
									}
								});
							}
						},{
							text    :	'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Cancel</b></span>',
							handler	:	function(){
								formStatus.getForm().reset();
								winStatus.close();
							}
						}]
					});
					
					var winStatus = new Ext.Window({
						layout      : 'fit',
						width       : 300,
						height      : 120,
						modal	 	: true,
						title		: 'Change SS Status',
						plain       : true,
						items		: formStatus,
						closeAction : 'close'
					}).show();
					
				}else if(cell > 2){ 
					var title = (record.get('address')+', '+record.get('usuario')+' - '+record.get('package'));
					
					openRequiredDocs(tabs, 'followSSRDTab', userid, title, id_shortsale, id_packages, grid.getStore(), true);
				}
			}
		}
	});
	
	storemysspfollowup.load({params:{start:0, limit:limitmysspfollowup}});
</script>