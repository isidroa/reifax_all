<?php
	define('FPDF_FONTPATH','../../../FPDF/font/');	
	$_SERVERXIMA="http://www.reifax.com/";
	include('../../../FPDF/mysql_table.php');
	include("../../../FPDF/limpiar.php");
	include("../../../properties_conexion.php");
	conectar();
	
	limpiardirpdf2('../../../FPDF/generated/');	//Se borran los pdf viejos
	
	$printType = $_POST['printType'];
	$pids=$_POST['parcelids_res'];
	
	class PDF extends PDF_MySQL_Table
	{
		function Header()
		{
			global $pid,$printType,$county; 
			//Title
			$this->SetFont('Arial','',10);
			$this->Cell(0,6,'Properties REI FAX.',0,1,'L');
			switch($printType){
				case 0: $this->Cell(0,6,'My Follow Up - Mailing Campaign',0,1,'C'); break;
			}
			
			$this->Ln(2);
			//Ensure table header is output
			parent::Header();
		}
	}
	
	$pdf=new PDF('L');		//Landscape -- hoja horizontal
	$pdf->Open();
	$pdf->AddPage();
	
	include("../../../properties_getgridcamptit.php");

	switch($printType){
		case 0:
			$id = getArray('MYFollow','result');
			
			$ArTab=getCamptitTipo($id, "Tabla",'defa');	
			$ArCamp=getCamptitTipo($id, "Campos",'defa');
			$ArTit=getCamptitTipo($id, "Titulos",'defa');
			$ArSize=getCamptitTipo($id, "r_size",'defa');
			
			for($i=0; $i< count($ArCamp); $i++){
				$pdf->AddCol($ArCamp[$i],$ArSize[$i],$ArTit[$i],'C');
			}
			$pdf->AddCol('campaign',30,'Campaign','C');
			$pdf->AddCol('city',30,'City','C');
			$pdf->AddCol('lasthistorydate',30,'Last mail date','C');
			
			$xSql="SELECT f.parcelid as pid, f.campaign, f.userid, f.mlnumber,
			 	IF(status='A','Active',IF(status='NA','Non-Active',IF(status='NF','Not For Sale','Sold'))) as status, f.bd as county, 
				f.address, f.unit, f.city, f.zip, f.agent, f.statusalt, f.sendmail, f.lasthistorydate, f.type
				FROM xima.followup f 
				WHERE userid =".$_COOKIE['datos_usr']['USERID']." and type in ('M','FM') 
				and f.parcelid IN ('".str_replace(',',"','",$pids)."')"; 
		break;
	}
	
	$prop=array('HeaderColor'=>array(135,206,250),
            'color1'=>array(255,255,255),
            'color2'=>array(255,255,255),
            'padding'=>2,
			'align'=>'C');
		
	$pdf->Table($xSql,$prop,$rest_total);		

	$file='FPDF/generated/'.strtotime("now").'.pdf';
	$pdf->Output('C:/inetpub/wwwroot/'.$file, 'F');
	echo "{success:true, pdf:'$file'}";
?>