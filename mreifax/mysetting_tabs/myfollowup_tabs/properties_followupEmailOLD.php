<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	include_once("receivemail.class.php");
	include_once('followemail.class.php');
	conectar();	
	
	//$docsDir = 'F:/ReiFaxDocs/';
	$docsDir = 'C:/inetpub/wwwroot/';
	$userid = $_POST['userid'];
	$typeEmail = $_POST['typeEmail']; //0->INBOX, 1->OUTBOX
	
	if(isset($_POST['type'])){
		if($_POST['type']=='delete'){
			//Eliminación de los attachment en carpeta
			$query='Select * from xima.follow_emails_attach WHERE idmail IN ('.$_POST['pids'].')';
			$result = mysql_query($query) or die($query.mysql_error());
			
			while($r=mysql_fetch_array($result)){
				if(strlen($r['url'])>0)
					@unlink($docsDir."MailAttach/$userid/".$r['filename']);
			}
			
			//Eliminacion de los attachment en tabla
			$query='DELETE FROM xima.follow_emails_attach WHERE 
			idmail IN ('.$_POST['pids'].')'; 
			mysql_query($query) or die($query.mysql_error());
			
			//Eliminacion de los bodys
			$query='DELETE FROM xima.follow_emails_body WHERE 
			idmail IN ('.$_POST['pids'].')'; 
			mysql_query($query) or die($query.mysql_error());
			
			//Eliminación de los mails
			$query='DELETE FROM xima.follow_emails WHERE 
			userid='.$userid.' AND idmail IN ('.$_POST['pids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='unread'){			
			$seen = isset($_POST['read']) ? $_POST['read'] : 0;
			//Marcar como no leidos
			$query='UPDATE xima.follow_emails 
			SET seen = '.$seen.' 
			WHERE idmail IN ('.$_POST['pids'].')'; 
			mysql_query($query) or die($query.mysql_error());
			
			//echo '{success: true}';
			echo json_encode(array('success' => true));
			
		}elseif($_POST['type']=='property_Assig'){
			$idmail = $_POST['idmail'];
			$query = "SELECT fe.idmail,fea.parcelid FROM xima.follow_emails fe 
					  LEFT JOIN xima.follow_emails_assigment fea ON (fe.idmail=fea.idmail AND fe.userid=fea.userid)
					  WHERE fe.userid=$userid AND fe.idmail=$idmail";
			
			$result=mysql_query($query) or die($query.mysql_error());

			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;
			
			//echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
			echo json_encode(array('success' => true, 'total' =>count($vFilas), 'records' =>$vFilas, 'sql' => $query));
			
		}elseif($_POST['type']=='searchProperty'){
			$query = "select parcelid, IF(type='S', concat('Selling: ',address, ' ', unit), IF(type='SS', concat('Short Sale: ',address, ' ', unit), concat('Buying: ',address, ' ', unit))) as address
			from xima.followup 
			WHERE userid=$userid  
			AND (type='F' or type='FM' or type='B' or type='BM' or type='S' or type='SS')";
			
			if(isset($_POST['query']) && strlen($_POST['query'])>1){
				$query.=" AND address LIKE '%".$_POST['query']."%'";
			}
			
			$query.="ORDER BY address";

			$result=mysql_query($query) or die($query.mysql_error());

			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;
			
			//echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
			echo json_encode(array('success' => true, 'total' =>count($vFilas), 'records' =>$vFilas, 'sql' => $query));
			
		}elseif($_POST['type']=='searchPropertyAgent'){
			$agentid = $_POST['agentid'];

			if($agentid !== '0'){
				$query="SELECT agenttype from xima.followagent where userid=$userid and agentid=$agentid";
				$res = mysql_query($query) or die($query.mysql_error());
				$r = mysql_fetch_array($res);
				
				if($r[0]==12){
					$query = "select parcelid, address  
					from xima.followup 
					WHERE userid=$userid
					AND (type='F' or type='FM' or type='B' or type='BM' or type='S')";
				}else{				
					$query = "select fa.parcelid,f.address  
					from xima.follow_assignment fa 
					INNER JOIN xima.followup f ON (f.userid=fa.userid AND f.parcelid=fa.parcelid)
					WHERE fa.userid=$userid AND fa.agentid=$agentid
					AND (type='F' or type='FM' or type='B' or type='BM' or type='S')";
				}
			}else{
				$query = "select parcelid, IF(type='S', concat('Selling: ',address), concat('Buying: ',address)) as address
				from xima.followup 
				WHERE userid=$userid 
				AND (type='F' or type='FM' or type='B' or type='BM' or type='S')
				ORDER BY address";
			}
			$result=mysql_query($query) or die($query.mysql_error());

			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;
			
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas).'}';
		}elseif($_POST['type']=='assignmentProperty'){
			$agentid=$_POST['agentid'];
			$idmail=$_POST['idmail'];
			$pid=$_POST['parcelid'];
			$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
			
			//Busqueda del task del Email
			$query="SELECT task FROM xima.follow_emails WHERE idmail=$idmail and userid=$userid";
			$result = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error())));
			$r = mysql_fetch_array($result);
			
			$task = $r['task'];
			
			assigmentProperty($userid,$idmail,$pid,$task,$typeFollow);
			
			echo '{success: true}';
		}elseif($_POST['type']=='assignmentContact'){
			$agentid=$_POST['contactname'];
			$email = $_POST['email'];
			
			$query="SELECT email,email2,email3,email4,email5 FROM xima.followagent WHERE userid=$userid AND agentid=$agentid";
			$result=mysql_query($query) or die($query.mysql_error());
			$r = mysql_fetch_array($result);
			
			if(strlen($r['email'])==0){
				$set = 'email="'.$email.'"';	
			}elseif(strlen($r['email2'])==0){
				$set = 'email2="'.$email.'"';
			}elseif(strlen($r['email3'])==0){
				$set = 'email3="'.$email.'"';
			}elseif(strlen($r['email4'])==0){
				$set = 'email4="'.$email.'"';
			}elseif(strlen($r['email5'])==0){
				$set = 'email5="'.$email.'"';
			}else{
				echo '{success: false, msg:"The contact does not have an email space for the upgrade, please go to \'My Contacts\' to make the assignment."}';
				return false;
			}
			
			$query="UPDATE xima.followagent SET $set WHERE userid=$userid AND agentid=$agentid";
			mysql_query($query) or die($query.mysql_error());
			
			//Assignment agent to Emails.
			$query='SELECT fe.idmail FROM xima.follow_emails fe 
			LEFT JOIN xima.follow_emails_assigment fea ON (fe.idmail=fea.idmail AND fe.userid=fea.userid)
			WHERE fe.userid='.$userid.' 
			AND fe.from_msg="'.$email.'"
			AND fea.agentid IS NULL';
			$result = mysql_query($query) or die($query.mysql_error());
			while($r=mysql_fetch_array($result)){
				insertEmailContact($userid,$r['idmail'],$agentid);
			}
			
			echo '{success: true}';
		}elseif($_POST['type']=='getEmailContent'){
			$mailID = $_POST['mailid'];
			$query  = "SELECT body from xima.follow_emails_body WHERE idmail=$mailID";
			$result = mysql_query($query) or die($query.mysql_error());
			$r 		= mysql_fetch_array($result);
			
			print_r(preg_replace('/[\x00-\x1F\x80-\xFF]/',"",html_entity_decode(str_replace(array("\\n","\n","\\r","\r"),array('','','',''),$r['body']))));
		}elseif($_POST['type']=='getEmailDetail'){
			$mailid = $_POST['mailid'];
			$query="SELECT fe.subject,fe.fromName_msg,fe.from_msg,IF(fe.to_msg is null,c.imap_username,fe.to_msg) to_msg,fe.msg_date,fb.body 
					FROM xima.follow_emails fe
					INNER JOIN xima.follow_emails_body fb ON (fe.idmail=fb.idmail) 
					INNER JOIN xima.contracts_mailsettings c ON (fe.userid=c.userid)
					WHERE fe.userid=$userid AND fe.idmail=$mailid";
			$result = mysql_query($query) or die($query.mysql_error());
			//$r 		= mysql_fetch_assoc($result);
			
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC))
				$vFilas[]=$row;	
			
			//echo json_encode(array('success'=> true, 'records'=> $r));
			echo json_encode(array('success' => true,'records' =>$vFilas, 'sql' => $sql));
			
		}elseif($_POST['type']=='getEmailAttach'){ 
			$mailID = $_POST['mailid'];
			
			$query  = "SELECT distinct url,filename from xima.follow_emails_attach WHERE idmail=$mailID";
			$result = mysql_query($query) or die($query.mysql_error());
			
			$attach = array();
			while($r = mysql_fetch_array($result)){
				$attach[] = $r;
			}
			
			echo json_encode(array('success' => true, 'total' => count($attach), 'attach' => $attach));
		}elseif($_POST['type']=='getTemplateEmail'){ 
			$idtemplate = $_POST['idtemplate'];
			$parcelid = $_POST['pid'];
			$county = $_POST['county'];
			
			$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$idtemplate";
			$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
			$mailTemplate = mysql_fetch_array($resultTemplate);
			
			$usarHtml=true;
			$subject = $mailTemplate['subject'];
			$body = $mailTemplate['body'];
			
			if($county==''){
				$query = "SELECT bd FROM xima.followup WHERE userid = $userid and parcelid='$parcelid'";
				$result=mysql_query($query) or die($query.mysql_error());
				$r = mysql_fetch_array($result);
				
				$county = $r[0];
			}
			
			conectarPorNameCounty(strtolower($county));
			include_once('../../mysetting_tabs/mycontracts_tabs/function_template.php'); 
			$subject=replaceTemplate($subject,$userid,$parcelid,0,'','');
			$body=replaceTemplate($body,$userid,$parcelid,0,'','');
			
			echo json_encode(array('success' => true, 'subject' => $subject, 'body'=> $body));
		}elseif($_POST['type']=='composeEmail'){
			//header('Content-Type: text/javascript, charset=UTF-8');
			$mailID = is_numeric($_POST['mailid']) ? $_POST['mailid'] : NULL;
			$pid = $_POST['pid'];
			$to = $_POST['to'];
			$sub = mysql_real_escape_string($_POST['subject']);
			$msg = mysql_real_escape_string($_POST['msg']);
			$task = isset($_POST['task']) ? $_POST['task'] : 5;
			$cc = isset($_POST['cc']) && $_POST['cc']==true ? true : false;
			$typeFollow = (isset($_POST['typeFollow']) && strlen($_POST['typeFollow'])>0) ? $_POST['typeFollow'] : 'B';
			$contract = isset($_POST['contract']) ? $_POST['contract'] : 1;
			$pof = isset($_POST['pof']) ? $_POST['pof'] : 1;
			$emd = isset($_POST['emd']) ? $_POST['emd'] : 1;
			$adem = isset($_POST['rademdums']) ? $_POST['rademdums'] : 1;
			$offer = isset($_POST['offer']) ? $_POST['offer'] : 0;
			$complete = isset($_POST['complete']) ? $_POST['complete'] : true;
			
			$query='SELECT m.*,m.username AS email,if(length(p.profname)>0,p.profname,concat(x.name," ", x.surname)) as name
			FROM xima.contracts_mailsettings m
			INNER JOIN xima.ximausrs x ON (m.userid=x.userid)
			INNER JOIN xima.profile p ON (m.userid=p.userid)
			WHERE m.userid='.$userid;
			$result = mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error())));
			
			if(mysql_num_rows($result)>0){
				$r=mysql_fetch_array($result);
				
				if(strlen($r['server'])>0){
					$user=$r['username'];
					$pass=$r['password'];
					$server=$r['server'];
					$puerto=$r['port'];
					$from = $r['email'];
					$name = $r['name'];
					$attachEmail = array();
					
					if(isset($_FILES['file7']) && strlen(basename($_FILES['file7']['name']))>3){
						$i=7;
						$aux = 'file'.$i;
						$dir = $docsDir."MailAttach/$userid/";
						//Crear Directorio de Attachment
						if(!is_dir($dir)){
							mkdir($dir);
						}
						
						while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
							//subir archivo
							@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
							$attachEmail[]=$dir.basename($_FILES[$aux]['name']);
														
							$i++;
							$aux = 'file'.$i;
						}
					}elseif(isset($_FILES['file8']) && strlen(basename($_FILES['file8']['name']))>3){
						$i=8;
						$aux = 'file'.$i;
						$dir = $docsDir."MailAttach/$userid/";
						//Crear Directorio de Attachment
						if(!is_dir($dir)){
							mkdir($dir);
						}
						
						while(isset($_FILES[$aux]) && strlen(basename($_FILES[$aux]['name']))>3){
							//subir archivo
							@move_uploaded_file($_FILES[$aux]['tmp_name'],$dir.basename($_FILES[$aux]['name']));
							$attachEmail[]=$dir.basename($_FILES[$aux]['name']);
														
							$i++;
							$aux = 'file'.$i;
						}
					}
					
					if(isset($_POST['file7']) && strlen($_POST['file7'])>3){
						$i=7;
						$aux = 'file'.$i;
						$dir = $docsDir."MailAttach/$userid/";
								
						while(isset($_POST[$aux]) && strlen($_POST[$aux])>3){
							if(substr($_POST[$aux],0,4)=='http')
								$attachEmail[]=$_POST[$aux];
							else
								$attachEmail[]=$dir.basename($_POST[$aux]);
														
							$i++;
							$aux = 'file'.$i;
						}
					}
					
					include_once($_SERVER['DOCUMENT_ROOT'].'/mailer/class.phpmailer.php');
					
					$email 				= new PHPMailer();
					$email->PluginDir	= $_SERVER['DOCUMENT_ROOT'].'/mailer/';
					$email->Mailer 		= "smtp";
					$email->Host 		= $server;
					$email->Port 		= $puerto;
					$email->SMTPAuth 	= true;
					
					//if($userid==73) $email->SMTPDebug	= true;
					
					$email->Username 	= $user;
					$email->Password 	= $pass;
					
					
					foreach($attachEmail as $k => $file){
						if(substr($file,0,4)=='http') $email->AddStringAttachment(file_get_contents($file), basename($file), $encoding = 'base64');
						else $email->AddAttachment($file);
					}
					
					
					$email->From = $from;
					$email->FromName = $name;
					$email->AddReplyTo($from, $name);
					$email->AddAddress($to);
					
					
					//SEND CC for user email
					if($cc && $to!=$from)
						$email->AddAddress($from);
					
					$email->Subject = $sub;
					
										
					$rSearch=array('"',"'");
					$rReplace=array('','');
					
					$attachSend = count($attachEmail)>0 ? 1 : 0;
					
					//Insert Email
					$query="INSERT INTO  xima.follow_emails 
					(userid, msg_id, from_msg,fromName_msg, to_msg, subject, msg_date, type, attachments,task,seen) 
					VALUES($userid,'".sha1($userid.$sub.date())."','".mysql_real_escape_string($from)."',
					'".mysql_real_escape_string($name)."', '".mysql_real_escape_string($to)."', 
					'".$sub."', NOW(), 1, $attachSend, $task, 1)";		
						 
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error()))); 
					$idinsert = mysql_insert_id();
					
					//Insert body part
					$query="INSERT INTO  xima.follow_emails_body
					(idmail,body) VALUES
					($idinsert,'$msg')";
					mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error())));
					
					//Insert attach part
					foreach($attachEmail as $k => $file){
						if(substr($file,0,4)=='http')
							$filename = $file;
						else
							$filename = basename($file);
						
						$query="INSERT INTO  xima.follow_emails_attach
						(idmail,filename) VALUES
						($idinsert,'".mysql_real_escape_string($filename)."')";
						mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query.mysql_error(), 'msg'=>$query.mysql_error())));
					}
										
					
					$email->Body = $msg.'<br>#MSG-ID#='.$idinsert.';';
					$email->Body = preg_replace('/[\x00-\x1F\x80-\xFF]/',"",str_replace(array("\\n","\n","\\r","\r","\'"),array('<br>','<br>','','',"'"),$email->Body));
					
					$email->IsHTML(true);

					if($email->Send()){
						if($pid != '-1' && $complete){
							assigmentProperty($userid,$idinsert,$pid,$task,$typeFollow,$contract,$pof,$emd,$adem,$offer);
						}
						elseif($complete===false) insertEmailAssingment($userid,$idinsert,$pid);
						
						echo json_encode(array('success' => true, 'msg' => '', 'id' => $idinsert));
						//echo '{success:true}';
					}else{
						$query="DELETE FROM xima.follow_emails WHERE idmail=$idinsert AND userid=$userid";
						echo json_encode(array('success' => false, 'error' => $email->ErrorInfo, 'msg'=>$email->ErrorInfo));
					}
					
				}else
					echo json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
			}else
				echo json_encode(array('success' => false, 'msg' => 'You must set your email configuration in \'My settings\' -> \'Mail settings\' -> \'Email Delivery Configuration\'.'));
		}
	}elseif($_POST['checkmail']==1){
		//echo "Inicio: ".date('Y-m-d H:i:s')."<br>";		
		$query='SELECT * FROM xima.contracts_mailsettings WHERE userid='.$userid;
		$result = mysql_query($query);
		if($result === false){
			echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
			return true;
		}
		
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			
			if(strlen($r['imap_server'])>0){
				$user=$r['imap_username'];
				$pass=$r['imap_password'];
				$server=$r['imap_server'];
				$puerto=$r['imap_port'];
				$fTimeSinc=true;
				
				$rSearch=array('\\','"',"'");
				$rReplace=array('','','');
				
				if($r['imap_type']==1){ 
					$protocol='imap';
					$auth=false;
				}elseif($r['imap_type']==2){
					$protocol='pop3';
					$auth=false;
				}elseif($r['imap_type']==3){
					$protocol='imap';
					$auth=true;
				}elseif($r['imap_type']==4){
					$protocol='pop3';
					$auth=true;
				}
				
				$carpeta = $typeEmail == 0 ? 'INBOX' : 'SENT';
				// Creating a object of reciveMail Class
				$obj = new receiveMail($user,$pass,$user,$server,$protocol,$puerto,$auth,$carpeta); 
				//Connect to the Mail Box
				$conE = $obj->connect();
				if($conE!==true){//If connection fails give error message and exit
					echo json_encode(array('success' => false, 'msg' => $conE));
					return false;
				}
				
				if(!isset($_POST['nextDownload'])){
					$query="SELECT date(msg_date) lastdateupdate 
					FROM xima.follow_emails 
					WHERE userid=$userid and type=0 and date(msg_date) <= date(now())  
					ORDER BY msg_date desc limit 2";
					$result = mysql_query($query);
					if($result === false){
						echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
						return true;
					}
					
					
					if(mysql_num_rows($result)>0){
						$lastdateupdate='2050-01-01';
						while($r=mysql_fetch_array($result)){						
						
							$lastdateupdate1=$r['lastdateupdate'];
							
							$lastdateupdate = $lastdateupdate1 <= $lastdateupdate ? $lastdateupdate1 : $lastdateupdate;
						}
					}else{
						$timestamp = strtotime('-1 month');
						$lastdateupdate=date('Y-m-d', $timestamp);
					}
					
					//if($userid==3686) $lastdateupdate = '2014-03-30';
					
					// Get total list of mails in order.
					$mails = $obj->getCountHeaderMail($fTimeSinc,$carpeta,$lastdateupdate);
					//Get total Mails to downloading.
					
					$i=0;
					while($i<count($mails)){					
						
						$query="INSERT INTO xima.follow_emails_read 
						VALUES ($userid,".$mails[$i].",'','',-1,NOW())";
						mysql_query($query);
												
						$i++;
					}
				}
				
				//get 10 emails for dowloading
				$query='SELECT * FROM xima.follow_emails_read WHERE userid='.$userid.' AND status=-1';
				$result = mysql_query($query);
				if($result === false){
					echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
					return true;
				}
				
				
				$i=0;
				$cantMail=mysql_num_rows($result);
				while($r=mysql_fetch_assoc($result)){
					$uid = $r['uid'];
					$d=$obj->getHeaders($uid);
					
					if($d!==false){						
						$msg_id = sha1($d['message_id']);
						
						$query='SELECT * FROM xima.follow_emails_read 
						WHERE userid='.$userid.' AND uid='.$uid.' AND msg_id="'.$msg_id.'"';
						$result2 = mysql_query($query) or die($query.mysql_error());
						
						if($result2 === false){
							echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
							return true;
						}
						
						if(mysql_num_rows($result2)>0){
							$query='DELETE FROM xima.follow_emails_read 
							WHERE userid='.$userid.' AND uid='.$uid.' AND status=-1';
							if(mysql_query($query) === false){
								echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
								return true;
							}
							$cantMail--;
						}else{
							$query='UPDATE xima.follow_emails_read 
							SET status=0, msg_id="'.$msg_id.'", message_id="'.substr($d['message_id'],0,255).'", msg_date="'.$d['datereifax'].'" 
							WHERE userid='.$userid.' AND uid='.$uid.' AND status=-1';
							if(mysql_query($query) === false){
								echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
								return true;
							}
							
							$i++;
						}
					}else{
						$query='DELETE FROM xima.follow_emails_read 
						WHERE userid='.$userid.' AND uid='.$uid.' AND status=-1';
						if(mysql_query($query) === false){
							echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
							return true;
						}
						$cantMail--;
					}
					
					if($i>=10) break;
				}
				
				$mails=array();
				$query="SELECT msg_id,uid FROM xima.follow_emails_read WHERE userid=$userid AND status=0";
				$result=mysql_query($query);
				
				if($result===false){
					echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
					$obj->close_mailbox();
					return true;
				}
				
				while($r=mysql_fetch_assoc($result))
					$mails[]=array('uid'=>$r['uid'],'msg_id'=>$r['msg_id']);
				
				//if($userid==73) print_r($mails);
				
				$stepMail = 0;
				$totalMail = isset($_POST['totalMail']) && intval($_POST['totalMail'])>0 ? intval($_POST['totalMail']) : $cantMail;
				
				if(count($mails)>0){
					$breakCont=0;
					while($breakCont<10){
												
						$numMail = array_shift($mails);
							
						unset($head,$bodymsg);
						$head=$obj->getHeaders($numMail['uid']);
						$bodymsg=str_replace($rSearch,$rReplace,htmlentities($obj->getBody($numMail['uid'])));
						$head['to']='';
						$msg_id=$numMail['msg_id'];


						$query="SELECT idmail FROM xima.follow_emails WHERE userid=$userid AND (uid=".$numMail['uid']." AND msg_id='".$msg_id."')";
						$resultCheck = mysql_query($query); 
						if($resultCheck===false){
							echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
							$obj->close_mailbox();
							return true;
						}
						if(mysql_num_rows($resultCheck)==0 && strlen($head['message_id'])>0){							
							$fromEmail = str_replace($rSearch,$rReplace,$head['from']);
							$nameEmail = str_replace($rSearch,$rReplace,$head['fromName']);
							$subEmail  = str_replace($rSearch,$rReplace,$head['subject']);
							
							
							$query="INSERT INTO  xima.follow_emails 
							(userid, msg_id, uid, from_msg,fromName_msg, subject, msg_date, size, type) 
							VALUES($userid,'".$msg_id."',".$numMail['uid'].",'".$fromEmail."',
							'".$nameEmail."',
							'".$subEmail."',
							'".$head['datereifax']."','".$head['size']."',$typeEmail)";	
	
								
							if(mysql_query($query)===false){
								echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
								$obj->close_mailbox();
								return true;
							}
							$idinsert = mysql_insert_id();
							
							
							//Insert body part
							$query="INSERT INTO  xima.follow_emails_body
							(idmail,body) VALUES
							($idinsert,'".addslashes($bodymsg)."')";
							if(mysql_query($query)===false){
								echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
								$obj->close_mailbox();
								return false;
							}
							
							// Get attached File from Mail Return name of file in comma separated string  args.	
							if(!is_dir($docsDir."MailAttach/$userid/")){
								mkdir($docsDir."MailAttach/$userid/");
							}
							
							$str=$obj->GetAttach($numMail['uid'],$docsDir."MailAttach/$userid/"); 
							$ar=explode("^",$str);
							foreach($ar as $key=>$value)
							{
								if($value<>'')
								{
									$query="INSERT INTO  xima.follow_emails_attach (idmail, filename) VALUES($idinsert,'$value')";
									if(mysql_query($query)===false){
										echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
										$obj->close_mailbox();
										return true;
									}
									
									$query="UPDATE  xima.follow_emails SET attachments=1 WHERE idmail=$idinsert";
									if(mysql_query($query)===false){
										echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
										$obj->close_mailbox();
										return true;
									} 					
								}
							}
							
							//Mail delivery failure
							if($nameEmail=='Mail Delivery Subsystem' && $fromEmail=='mailer-daemon@googlemail.com'){
								mailDeliveryFailure(html_entity_decode($bodymsg),$userid);
								
								$query="UPDATE xima.follow_emails SET seen=1 WHERE idmail=$idinsert";
								if(mysql_query($query)===false){
									echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($query.mysql_error()), 'msg' => mysql_real_escape_string($query.mysql_error())));
									$obj->close_mailbox();
									return true;
								}
							}
							
							//Check for #MSG-ID# in reply email.
							preg_match('/#MSG-ID#=(.*);/', html_entity_decode($bodymsg), $msgid);
							$msgidCount = count($msgid)>0 && strlen(trim($msgid[0]))>0 ? true : false;
							
							$propertyInsertedEmail = true;
							//Email assingment for reply
							if($msgidCount){
								$val = $msgid[count($msgid)-1];

								$q = "select fea.parcelid, fe.task 
								from xima.follow_emails fe
								INNER JOIN xima.follow_emails_assigment fea ON (fe.userid=fea.userid AND fe.idmail=fea.idmail)
								INNER JOIN xima.followup f ON (fea.parcelid=f.parcelid AND fe.userid=f.userid) 
								WHERE fe.idmail='$val'";
								$res = mysql_query($q);
								if($res===false){
									echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($q.mysql_error()), 'msg' => mysql_real_escape_string($q.mysql_error())));
									$obj->close_mailbox();
									return true;
								}
								
								if(mysql_num_rows($res)>0){
									$r = mysql_fetch_array($res);									
									assigmentProperty($userid,$idinsert,$r[0],($r[1])+1);
									$propertyInsertedEmail = false;
								}
							}
							//Email assingment
							assingmentEmail($userid,$idinsert,$fromEmail,$subEmail,$propertyInsertedEmail);	
						}else{
							$query="UPDATE xima.follow_emails_read 
							SET status=1 
							WHERE userid=$userid AND uid=".$numMail['uid']." AND msg_id='".$msg_id."'";
							mysql_query($query);
						}
						
						$stepMail++;
						//echo "Fin: ".date('Y-m-d H:i:s')."<br>";
						$breakCont++;
						
						if(count($mails)==0){
							$sql="UPDATE xima.contracts_mailsettings SET imap_sinc_fTime=0 WHERE userid=$userid";
							if(mysql_query($sql)===false){
								echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($sql.mysql_error()), 'msg' => mysql_real_escape_string($sql.mysql_error())));
								$obj->close_mailbox();
								return true;
							}
							
							$breakCont=10;
						}
					}
					$obj->close_mailbox();   //Close Mail Box
					unset($obj);
					
				}else{
					$stepMail=0;
					$cantMail=0;
				}
		 	
				$vFilas=array();
				$sql="SELECT f.`idmail`, f.`userid`, f.`from_msg`,f.`fromName_msg`, f.`to_msg`, 
				f.`seen`, f.`subject`, f.`msg_date` , f.`attachments`, IF(fe.agentid is null,0,fe.agentid) ac,
				IF(fe.parcelid is null OR fu.address is null,
					IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1),
					fe.parcelid) ap, 
				f.`task`, fa.agent, fu.address 
				FROM xima.follow_emails f
				LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
				LEFT JOIN xima.followagent fa ON (f.userid=fa.userid AND fe.agentid=fa.agentid)
				LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
				WHERE f.`userid`=$userid and f.`type`=$typeEmail 
				order by msg_date DESC"; 
				$result2=mysql_query($sql);
				if($result2===false){ 
					echo json_encode(array('success' => false, 'error' => mysql_real_escape_string($sql.mysql_error()), 'msg' => mysql_real_escape_string($sql.mysql_error())));
					return true;
				}

				$num=mysql_num_rows($result2);
				while ($row=mysql_fetch_array($result2,MYSQL_ASSOC))
					$vFilas[]=$row;	
					
				if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
				else $vFilas2=$vFilas;			
				
				$cantContact = checkPendingContact($userid,$typeEmail);
				$cantProperty = checkPendingProperty($userid,$typeEmail);
				
				echo '{success: true, totalMail: '.$totalMail.', currentMail: '.($totalMail - ($cantMail-10)).', total: '.(count($vFilas)).', cantEmail: 0, cantContact: '.$cantContact.', cantProperty: '.$cantProperty.', records:'.json_encode($vFilas2).'}';
			}else
				echo json_encode(array('success' => false, 'msg' => 'Not provided the configuration of email reading, please provide it in My Settings - Mail Settings'));
		} 
	}elseif($_POST['checkmail']==2){
		$cant = checkPendingEmail($userid,$typeEmail);
		if($cant !== false){
			if(isset($_POST['pendings'])){
				$cantContact = checkPendingContact($userid,$typeEmail);
				$cantProperty = checkPendingProperty($userid,$typeEmail);
				
				echo json_encode(array('success' => true, 'cantEmail' => $cant, 'cantContact' => $cantContact, 'cantProperty' => $cantProperty));
			}else
				echo json_encode(array('success' => true, 'cant' => $cant));
		}
	}else{ 
		$email 		= isset($_POST['email']) 		? $_POST['email'] 		: '';
		$address	= isset($_POST['address'])		? $_POST['address']		: '';
		$toemail 	= isset($_POST['toemail']) 		? $_POST['toemail'] 	: '';
		$name 		= isset($_POST['name']) 		? $_POST['name'] 		: '';
		$content 	= isset($_POST['content']) 		? $_POST['content'] 	: '';
		$datebe 	= isset($_POST['dateBe']) 		? $_POST['dateBe'] 		: '';
		$dateaf 	= isset($_POST['dateAf']) 		? $_POST['dateAf'] 		: '';
		$datety		= isset($_POST['dateTy']) 		? $_POST['dateTy'] 		: 'Equal';
		$etype		= isset($_POST['etype'])		? $_POST['etype']		: -1;
		$attach 	= isset($_POST['attach'])		? $_POST['attach']  	: -1;
		$pcontact 	= isset($_POST['pcontact'])		? $_POST['pcontact'] 	: -1;
		$pproperty 	= isset($_POST['pproperty'])	? $_POST['pproperty']  	: -1;
		$seen 		= isset($_POST['seen'])			? $_POST['seen']  		: -1;
		
		$vFilas=array();
		$sql="SELECT SQL_CALC_FOUND_ROWS f.`idmail`, f.task,f.`userid`, f.`from_msg`,f.`fromName_msg`, f.`to_msg`, 
		f.`seen`, f.`subject`, f.`msg_date` , f.`attachments`, IF(fe.agentid is null,0,fe.agentid) ac,
		IF(fe.parcelid is null OR fu.address is null,
			IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1),
			fe.parcelid) ap, 
		f.`task`, fa.agent, fu.address   
		FROM xima.follow_emails f
		LEFT JOIN xima.follow_emails_assigment fe ON (f.userid=fe.userid AND f.idmail=fe.idmail)
		LEFT JOIN xima.followagent fa ON (f.userid=fa.userid AND fe.agentid=fa.agentid)
		LEFT JOIN xima.followup fu ON (f.userid=fu.userid AND fe.parcelid=fu.parcelid)
		WHERE f.`userid`=$userid and f.`type`=$typeEmail"; 
		
		//filters
		if(trim($address)!='')
			$sql.=' AND fu.address LIKE \'%'.trim($address).'%\'';
					
		if(trim($name)!='')
			$sql.=' AND f.`fromName_msg` LIKE \'%'.trim($name).'%\'';
		
		if(trim($toemail)!='')
			$sql.=' AND f.`to_msg` LIKE \'%'.trim($toemail).'%\'';
		
		if(trim($email)!='')
			$sql.=' AND f.`from_msg` LIKE \'%'.trim($email).'%\'';
			
		if(trim($content)!='')
			$sql.=' AND f.`subject` LIKE \'%'.trim($content).'%\'';
		
		if($etype!=-1)
			$sql.=' AND (f.`task`='.$etype.' OR f.`task`='.($etype+1).')';
			
		if($attach!=-1){
			if($attach==0)
				$sql.=' AND (f.`attachments`=1)';
			else
				$sql.=' AND (f.`attachments`=0)';
		}
		
		if($pcontact!=-1){
			if($pcontact==1)
				$sql.=' AND fe.agentid is not null';
			else
				$sql.=' AND fe.agentid is null AND f.seen=0';
		}
		
		if($seen!=-1 && $pcontact!=0){
			$sql.=" AND f.seen = ".$seen;
		}
		
		if($pproperty!=-1){
			if($pproperty==0)
				$sql.=' AND fe.agentid is not null AND (fe.parcelid is null OR fu.address is null) AND IF((SELECT count(*) FROM xima.follow_assignment WHERE userid=f.userid AND agentid=fe.agentid)>0,0,-1)=0';
			else
				$sql.=' AND fe.parcelid is not null';
		}
		
		if(trim($datebe)!=''){
			switch($datety){
				case 'Equal':
					$sql.=' AND date(f.`msg_date`)=\''.$datebe.'\'';
				break;
				
				case 'Greater Than':
					$sql.=' AND date(f.`msg_date`)>\''.$datebe.'\'';
				break;
				
				case 'Less Than':
					$sql.=' AND date(f.`msg_date`)<\''.$datebe.'\'';
				break;
				
				case 'Equal or Less':
					$sql.=' AND date(f.`msg_date`)<=\''.$datebe.'\'';
				break;
				
				case 'Equal or Greater':
					$sql.=' AND date(f.`msg_date`)>=\''.$datebe.'\'';
				break;
		
				case 'Between':
					if(trim($dateaf)!='')
						$sql.=' AND date(f.`msg_date`) BETWEEN \''.$datebe.'\' AND \''.$dateaf.'\'';
					else
						$sql.=' AND date(f.`msg_date`)>=\''.$datebe.'\'';
				break;
				
			}
		}	
		
		//orders
		if(isset($_POST['sort'])) $sql.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		else $sql.="ORDER BY msg_date DESC";
		
		//Limits
		if(isset($_POST['start'])) $sql.=' LIMIT '.$_POST['start'].', '.$_POST['limit'];
		
		//print_r($sql);

		$result2=mysql_query($sql) or die('{success: false, error: "'.str_replace($rSearch,$rReplace,$sql.mysql_error()).'"}');
		
		//Total count
		$query='SELECT FOUND_ROWS();';
		$restnum = mysql_query($query);
		$rnum = mysql_fetch_array($restnum);
		$totalnum = $rnum[0];
		
		while ($row=mysql_fetch_array($result2,MYSQL_ASSOC))
			$vFilas[]=$row;		

		$cantContact = checkPendingContact($userid,$typeEmail);
		$cantProperty = checkPendingProperty($userid,$typeEmail);
		
		//echo '{success: true, totalMail: 0, currentMail: 0, total: '.$totalnum.', cantEmail: -1, cantContact: '.$cantContact.', cantProperty: '.$cantProperty.', records:'.json_encode($vFilas).'}';
		
		echo json_encode(array('success' => true,' totalMail'=> 0, 'total' => $totalnum, 'cantEmail'=>-1, 'cantContact'=>$cantContact, 'cantProperty'=>$cantProperty,'records' =>$vFilas, 'sql' => $sql));
	}
?>