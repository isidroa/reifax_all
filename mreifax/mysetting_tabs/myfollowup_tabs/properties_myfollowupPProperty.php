<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmailPProperty_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowmail_progressBar"></div>
        <div id="myfollowmail_propertiesPProperty" align="left"></div> 
	</div>
</div>

<script>
 var storemyfollowmailPProperty = null; 
 var limitmyfollowmailPProperty = 50;
 Ext.onReady(function() {
	
	var selected_datamyfollowmailPProperty 	= new Array();
	var AllCheckmyfollowmailPProperty 		= false;
	var totalMailPProperty					= 0;
	var currentMailPProperty				= 0;
	var sincMailPProperty					= 1;
	
	//filter variables
	var filterEmailMyfollowMailPProperty 		= '';
	var filterNameMyfollowMailPProperty 		= '';
	var filterContentMyfollowMailPProperty 		= '';
	var filterDateAfMyfollowMailPProperty 		= '';
	var filterDateBeMyfollowMailPProperty		= '';
	var filterTypeMyfollowMailPProperty			= -1;
	var filterFieldMyfollowMailPProperty		= 'fromName_msg';
	var filterDirectionMyfollowMailPProperty	= 'ASC';
	

	storemyfollowmailPProperty = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'agent', type: 'string'},
			{name: 'ap', type: 'string'},
			{name: 'address', type: 'string'},
			{name: 'task', type: 'int'}
	    ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': 		<?php echo $userid;?>,
			'typeEmail':	0,
			'contactAssignNotProperty': 1,
			'checkmail': 	0,
			'currentMail': 	currentMailPProperty,
			'totalMail':  	totalMailPProperty
		},
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				obj.params.email 	= filterEmailMyfollowMailPProperty;
				obj.params.name		= filterNameMyfollowMailPProperty;
				obj.params.content	= filterContentMyfollowMailPProperty;
				obj.params.dateAf	= filterDateAfMyfollowMailPProperty;
				obj.params.dateBe	= filterDateBeMyfollowMailPProperty;
				obj.params.etype	= filterTypeMyfollowMailPProperty;
			}
		}
    });
	
	var smmyfollowmailPProperty = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmailPProperty.indexOf(record.get('idmail'))==-1)
					selected_datamyfollowmailPProperty.push(record.get('idmail'));
				
				if(Ext.fly(gridmyfollowmailPProperty.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailPProperty=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmailPProperty = selected_datamyfollowmailPProperty.remove(record.get('idmail'));
				AllCheckmyfollowmailPProperty=false;
				Ext.get(gridmyfollowmailPProperty.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}		
	});
	
	var gridmyfollowmailPProperty = new Ext.grid.EditorGridPanel({
		renderTo: 'myfollowmail_propertiesPProperty',
		cls: 'grid_comparables',
		height: 400,
		store: storemyfollowmailPProperty,
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowmailPProperty
			,{header: 'T', width: 28, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
			,{header: 'A', width: 23, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
			,{header: 'C', width: 23, sortable: true, align: 'center', dataIndex: 'ac', renderer: assignmentContact}
			,{header: 'P', width: 23, sortable: true, align: 'center', dataIndex: 'ap', renderer: assignmentProperty}
			,{id: 'idmail', header: "From", width: 150, align: 'left', sortable: true, dataIndex: 'fromName_msg'}
			,{header: "From Mail", width: 150, align: 'left', sortable: true, dataIndex: 'from_msg'}
		    ,{header: "Date", width: 120, sortable: true, align: 'center', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'), dataIndex: 'msg_date'}		
			,{header: 'Subject', width: 400, sortable: true, align: 'left', dataIndex: 'subject'}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowmailPProperty',
            pageSize: 		limitmyfollowmailPProperty,
            store: 			storemyfollowmailPProperty,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display'
		}),
		listeners: {
			'sortchange': function (grid, sorted){
				filterFieldMyfollowMailPProperty		= sorted.field;
				filterDirectionMyfollowMailPProperty	= sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = grid.getView().findCellIndex(e.getTarget()); 
				if(cell !== 0 && cell !== 4){
					Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
					var record = grid.getStore().getAt(rowIndex);
					
					viewMailDetail(record.get('idmail'),record.get('userid'),record.get('ap'),tabsFollowEmails);
				}
			}
		},
		sm: smmyfollowmailPProperty,
		frame:false,
		loadMask:true,
		border: false
	});
 
		
	storemyfollowmailPProperty.load({params:{start:0, limit:limitmyfollowmailPProperty}});
	
	if(document.getElementById('tabs')){
		if(document.getElementById('todo_myfollowmail_panel').offsetHeight > tabs.getHeight()){
			tabs.setHeight(document.getElementById('todo_myfollowmail_panel').offsetHeight+100);
			tabs2.setHeight(tabs.getHeight());
			tabs3.setHeight(tabs.getHeight());
			viewport.setHeight(tabs.getHeight());
		}
	}
});
</script>