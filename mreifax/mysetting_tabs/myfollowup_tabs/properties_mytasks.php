<?php
	include($_SERVER['DOCUMENT_ROOT']."/properties_conexion.php");
	conectar();
	include ($_SERVER['DOCUMENT_ROOT']."/properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	$que="SELECT professional_esp FROM xima.permission WHERE userid=".$userid;
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$botoninvestor=$r[0]==1?'false':'true';

	$que="SELECT contactpremiun FROM xima.ximausrs WHERE userid=".$userid;
	$result=mysql_query($que) or die($que.mysql_error());
	$r=mysql_fetch_array($result);
	$contactpremiun=$r[0];
	
	$query = 'SELECT * FROM xima.contracts_contactsettings WHERE userid='.$userid;
	$result = mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($result) > 0){
		$r = mysql_fetch_array($result);
		
		$getContactBlock 		= intval($r['block']);
		$getContactOverwrite 	= intval($r['overwrite']);
		$getContactPremium 		= intval($r['premium']);
	}else{
		$getContactBlock 		= 0;
		$getContactOverwrite 	= 1;
		$getContactPremium 		= 0;
	}
	
	conectar();
	$query="select *,if(DATE_FORMAT(fecha,'%Y/%m/%d')<>CURDATE(),'SI','NO') as control from mantenimiento.emailusercontrol where userid=$userid";
	$result=mysql_query($query) or die($query.mysql_error());
	if(mysql_affected_rows()<=0)
		$totalsent=0;
	else{
		$r=mysql_fetch_assoc($result);
		if($r['control']=="SI")
			$totalsent=0;
		else
			$totalsent=$r['totalSendMail'];
	}
	
	$dateAccept = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")));
	$dateClose = date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")));
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mytasks_panel" style="background-color:#FFF;border-color:#FFF">
	<div id="mytasks_data_div" align="center" style=" background-color:#FFF; margin:auto; width:970px;">
  		<div id="mytasks_filters"></div><br />
        <div id="mytasks_properties" align="left"></div> 
	</div>
</div>
<script>
	var botoninvestor=<?php echo $botoninvestor; ?>;
	var totalsent=<?php echo $totalsent; ?>;
	var totalenviados=0;
	var fechaAcc=<?php echo "'".$dateAccept."'"; ?>;
	var fechaClo=<?php echo "'".$dateClose."'"; ?>;
	var useridspider= <?php echo $userid;?>;
	
	//variables for get contacts
	var contactpremiun= <?php echo $contactpremiun;?>;
	var getContactBlock= <?php echo $getContactBlock;?>;
	var getContactOverwrite = <?php echo $getContactOverwrite;?>;
	var getContactPremium = <?php echo $getContactPremium;?>;
	
	var totalnosent=0;
	//Progress Bar Variables
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	var limitmyfollowtask 			= 50;
	var selected_datamyfollowtask 	= new Array();
	var AllCheckmyfollowtask 			= false;
	var loadedBPT					= false;
	
	//filter variables
	var filtersBPT = {
		property		: {
			address		: '',
			county		: 'ALL',
			city		: 'ALL',
			zip			: '',
			status		: 'ALL',
			xcode		: 'ALL'
		},
		agent			: '',
		task			: {
			datec		: 'Equal',
			date		: '',
			dateb		: '',
			sdatec		: 'Equal',
			sdate		: '',
			sdateb		: '',
			type		: '-2',
			execType	: '-1',
			status		: '-1'
		},
		order			: {
			field		: 'address',
			direction	: 'ASC'
		}
	};
	
	var storemyfollowtask = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000,
			autoAbort: true
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'int'},
			   {name: 'coffer', type: 'int'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'nhour', type: 'date', dateFormat: 'h:i:s'},
			   {name: 'etime', type: 'date', dateFormat: 'Y-m-d h:i:s'},
			   {name: 'stime', type: 'date', dateFormat: 'Y-m-d h:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'int'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'dom', type: 'int'},
			   {name: 'pendes'},
			   {name: 'idfus', type: 'int'},
			   {name: 'typeExec', type: 'int'},
			     {name: 'agent'},
				 {name: 'phone1'},
			   'statusalt',
			   'statusschedule',
			   'detail'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'pendingtask': 'yes'
		},
		remoteSort: true,
		sortInfo: {
			field: 'etime',
			direction: 'DESC'
		},
		listeners: {
			'beforeload': function(store,obj){
				AllCheckmyfollowtask=false;
				selected_datamyfollowtask=new Array();
				smmyfollowtask.deselectRange(0,limitmyfollowtask);
				obj.params.address=filtersBPT.property.address;
				obj.params.agent=filtersBPT.agent;
					obj.params.phone1=filtersBPT.phone1;
				obj.params.status=filtersBPT.property.status;
				obj.params.ndate=filtersBPT.task.date;
				obj.params.ndateb=filtersBPT.task.dateb;
				obj.params.ndatec=filtersBPT.task.datec;
				obj.params.sdate=filtersBPT.task.sdate;
				obj.params.sdateb=filtersBPT.task.sdateb;
				obj.params.sdatec=filtersBPT.task.sdatec;
				obj.params.ntask=filtersBPT.task.type;
				obj.params.execType=filtersBPT.task.execType;
				obj.params.tstatus=filtersBPT.task.status;
				obj.params.zip=filtersBPT.property.zip;
				obj.params.county=filtersBPT.property.county;
				obj.params.city=filtersBPT.property.city;
				obj.params.xcode=filtersBPT.property.xcode;
			},
			'load' : function (store,data,obj){
				storemyfollowtaskAll.load();
				if (AllCheckmyfollowtask){
					Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyfollowtask=true;
					gridmyfollowtask.getSelectionModel().selectAll();
					selected_datamyfollowtask=new Array();
				}else{
					AllCheckmyfollowtask=false;
					Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyfollowtask.length > 0){
						for(val in selected_datamyfollowtask){
							var ind = gridmyfollowtask.getStore().find('pid',selected_datamyfollowtask[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyfollowtask.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	var storemyfollowtaskAll = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
			timeout: 3600000
		}),
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",{name: 'offer', type: 'int'},
			   {name: 'coffer', type: 'int'},
			   {name: 'ndate', type: 'date', dateFormat: 'Y-m-d'},
			   {name: 'nhour', type: 'date', dateFormat: 'h:i:s'},
			   {name: 'etime', type: 'date', dateFormat: 'Y-m-d h:i:s'},
			   {name: 'ntask', type: 'int'},
			   {name: 'contract', type: 'bool'},
			   {name: 'pof', type: 'bool'},
			   {name: 'emd', type: 'bool'},
			   {name: 'rademdums', type: 'bool'},
			   {name: 'sademdums', type: 'bool'},
			   {name: 'offerreceived', type: 'bool'},
			   {name: 'msj', type: 'bool'},
			   {name: 'lasthistorydate', type: 'int'},
			   {name: 'offerpercent', type: 'int'},
			   {name: 'lprice', type: 'int'},
			   {name: 'userid_follow', type: 'int'},
			   {name: 'name_follow'},
			   {name: 'dom', type: 'int'},
			   {name: 'pendes'},
			   {name: 'idfus', type: 'int'},
			   {name: 'typeExec', type: 'int'},
			   {name: 'agent'},
				 {name: 'phone1'},
			   'statusalt',
			   'statusschedule',
			   'detail'"
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>,
			'pendingtask': 'yes'
		},
		remoteSort: true,
		sortInfo: {
			field: 'etime',
			direction: 'DESC'
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersBPT.property.address;
				obj.params.agent=filtersBPT.agent;
				obj.params.phone1=filtersBPT.phone1;
				obj.params.status=filtersBPT.property.status;
				obj.params.ndate=filtersBPT.task.date;
				obj.params.ndateb=filtersBPT.task.dateb;
				obj.params.ndatec=filtersBPT.task.datec;
				obj.params.sdate=filtersBPT.task.sdate;
				obj.params.sdateb=filtersBPT.task.sdateb;
				obj.params.sdatec=filtersBPT.task.sdatec;
				obj.params.ntask=filtersBPT.task.type;
				obj.params.execType=filtersBPT.task.execType;
				obj.params.tstatus=filtersBPT.task.status;
				obj.params.zip=filtersBPT.property.zip;
				obj.params.county=filtersBPT.property.county;
				obj.params.city=filtersBPT.property.city;
				obj.params.xcode=filtersBPT.property.xcode;
			},
			'load' : function (store,data,obj){
				
			}
		}
    });
	function checkRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/drop-no.gif" />'; 
		else return '<img src="../../img/drop-yes.gif" />';
	}
	
	function statusRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 'A': return '<div title="Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -40px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;			
			
			case 'NA': return '<div title="Non-Active" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -20px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'NF': return '<div title="By Owner" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px -60px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
			
			case 'S': return '<div title="Sold" style="background: url(\'../../img/notes/semaforo_status.png\') no-repeat scroll 0px 0px transparent; width:20px; height: 20px;">&nbsp;</div>'; break;
		}
	}
	
	function msjRender(value, metaData, record, rowIndex, colIndex, store) {
		if(value) return '<img src="../../img/notes/new_msj.png" />';
		else return '';
	}
	 
	function creaMenuTask(e,rowIndex){ 
		x = e.clientX;
 		y = e.clientY; 
		var record = gridmyfollowtask.getStore().getAt(rowIndex);
		var pid = record.get('pid');
		var county = record.get('county');
		var status = record.get('status');
		
		var simple = new Ext.FormPanel({
			url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
			frame: true,
			width: 220,
			waitMsgTarget : 'Waiting...',
			labelWidth: 100,
			defaults: {width: 200},
			labelAlign: 'left',
			items: [
				new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contact',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>,
								type: 'assignment'
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var r=Ext.decode(response.responseText);
								creaVentana(0,r,pid,useridspider);
							}
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Change Status',
					handler: function(){
						win.close();
						statusAltPanel(storemyfollowtask,<?php echo $userid;?>,pid,record.get('statusalt'));
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Overview',
					handler: function(){
						win.close();
						createOverview(county,pid,status,false,false);
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contract',
					handler: function(){
						win.close();
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				}),new Ext.Button({
					height: 45,
					text: 'Edit Task',
					handler: function(){
						win.close();
						loading_win.show();
						
						editScheduleTask(record.get('idfus'), <?php echo $_COOKIE['datos_usr']['USERID'];?>, false, storemyfollowtask);
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Follow History',
					handler: function(){
						win.close();

						if(document.getElementById('followTab')){
							var tab = tabsFollow.getItem('followTab');
							tabsFollow.remove(tab);
						}
						
						createFollowHistory(tabsFollow, 'followTab', record, pid, 'B', 0, useridspider);
					}
				})
			]
		});
		var win = new Ext.Window({
			layout      : 'fit',
			width       : 230,
			height      : 370,
			modal	 	: true,
			plain       : true,
			items		: simple,
			closeAction : 'close',
			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
					loading_win.hide();
				}
			}]
		});
		win.show();
		return false;
	}
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenuTask(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}
	function execRender(value, metaData, record, rowIndex, colIndex, store) {
		switch(value){
			case 1: return ''; break; //'<img title="Manual" src="../../img/notes/manual.png" />'
			case 2: return '<img title="Automatic" src="../../img/notes/automatic.png" />'; break;
		}
	}

	var smmyfollowtask = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowtask.indexOf(record.get('pid'))==-1)
					selected_datamyfollowtask.push(record.get('pid'));
				
				if(Ext.fly(gridmyfollowtask.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowtask=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowtask = selected_datamyfollowtask.remove(record.get('pid'));
				AllCheckmyfollowtask=false;
				Ext.get(gridmyfollowtask.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmyfollowtask=new Ext.Toolbar({
		renderTo: 'mytasks_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowtask.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be eliminated.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowtask==true){
						var numTotales = storemyfollowtaskAll.getCount();
						var totales = storemyfollowtaskAll.getRange(0,numTotales);
						var idfus='\''+totales[0].data.idfus+'\'';
						for(i=1;i<numTotales;i++){
							idfus+=',\''+totales[i].data.idfus+'\'';	
						}
						
						//Confirmacion para eliminar todos
						Ext.MessageBox.show({
							title:    'Follow Up',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will delete all '+numTotales+' selected tasks from your Pending Task.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteFollow
						});
					}else{	
						var totales = gridmyfollowtask.getSelectionModel().getSelections();
						var numTotales = totales.length;
						var idfus='\''+totales[0].data.idfus+'\'';
						for(i=1; i<numTotales; i++)
							idfus+=',\''+totales[i].data.idfus+'\''; 
						
						deleteFollow('yes');
					}
					 
					function deleteFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'delete-multi',
								idfus: idfus,
								userid: <?php echo $userid;?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
								//storemyfollowtaskAll.load();
								Ext.Msg.alert("Follow Up Task", 'Tasks delete.');
								
							}                                
						});
					}
				}
			}),new Ext.Button({
				tooltip: 'Edit task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/update.png',
				handler: function(){
					if(selected_datamyfollowtask.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the record to be edit.'); return false;
					}else if(selected_datamyfollowtask.length>1){
						Ext.Msg.alert('Warning', 'You must select(check) only one record to be edit.'); return false;
					}
					loading_win.show();
					editScheduleTask(gridmyfollowtask.getSelectionModel().getSelections()[0].data.idfus, <?php echo $userid;?>, false, storemyfollowtask);
				}
			}),new Ext.Button({
				tooltip: 'Print Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyfollowtask.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowtask==true){
						var totales = storemyfollowtaskAll.getRange(0,storemyfollowtaskAll.getCount());
						var pids='\''+totales[0].data.idfus+'\'';
						for(i=1;i<storemyfollowtaskAll.getCount();i++){
							pids+=',\''+totales[i].data.idfus+'\'';	
						}
					}else{	
						var totales = gridmyfollowtask.getSelectionModel().getSelections();
						var pids='\''+totales[0].data.idfus+'\'';
						for(i=1; i<totales.length; i++)
							pids+=',\''+totales[i].data.idfus+'\'';  
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersBPT.property.address,
							agent: filtersBPT.agent,
								phone1: filtersBPT.phone1,
							status: filtersBPT.property.status,
							ndate: filtersBPT.task.date,
							ndateb: filtersBPT.task.dateb,
							ndatec: filtersBPT.task.datec,
							sdate: filtersBPT.task.sdate,
							sdateb: filtersBPT.task.sdateb,
							sdatec: filtersBPT.task.sdatec,
							ntask: filtersBPT.task.type,
							tstatus: filtersBPT.task.status,
							execType: filtersBPT.task.execType,
							sort: filtersBPT.order.field,
							dir: filtersBPT.order.direction,
							pids: pids,
							pendingtask: 'yes'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyfollowtask.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					loading_win.show();
					if(AllCheckmyfollowtask==true){
						var totales = storemyfollowtaskAll.getRange(0,storemyfollowtaskAll.getCount());
						var pids='\''+totales[0].data.idfus+'\'';
						for(i=1;i<storemyfollowtaskAll.getCount();i++){
							pids+=',\''+totales[i].data.idfus+'\'';	
						}
					}else{	
						var totales = gridmyfollowtask.getSelectionModel().getSelections();
						var pids='\''+totales[0].data.idfus+'\'';
						for(i=1; i<totales.length; i++)
							pids+=',\''+totales[i].data.idfus+'\'';  
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 0,
							address: filtersBPT.property.address,
							agent: filtersBPT.agent,
								phone1: filtersBPT.phone1,
							status: filtersBPT.property.status,
							ndate: filtersBPT.task.date,
							ndateb: filtersBPT.task.dateb,
							ndatec: filtersBPT.task.datec,
							sdate: filtersBPT.task.sdate,
							sdateb: filtersBPT.task.sdateb,
							sdatec: filtersBPT.task.sdatec,
							ntask: filtersBPT.task.type,
							tstatus: filtersBPT.task.status,
							execType: filtersBPT.task.execType,
							sort: filtersBPT.order.field,
							dir: filtersBPT.order.direction,
							pids: pids,
							pendingtask: 'yes'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmyfollowup = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						//renderTo: 'mytasks_filters',
						id: 'formmytasks',
						name: 'formmytasks',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersBPT.property.address,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBPT.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fzip',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Zip Code',
									name		  : 'fzip',
									value		  : filtersBPT.property.zip,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBPT.property.zip=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-countys',
											'userid': <?php echo $userid;?>,
											'pendingTask': 'yes'
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtersBPT.property.county,
									hiddenName    : 'fcounty',
									hiddenValue   : filtersBPT.property.county,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.property.county = record.get('valor');
											combo.findParentByType('form').getForm().findField('fcity').setValue("ALL");
											filtersBPT.property.city='ALL';
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcity',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'City',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-citys',
											'userid': <?php echo $userid;?>,
											county: filtersBPT.property.county,
											'pendingTask': 'yes'
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcityname',
									value         : filtersBPT.property.city,
									hiddenName    : 'fcity',
									hiddenValue   : filtersBPT.property.city,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.property.city = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('county',filtersBPT.property.county);
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fxcode',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Property Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-xcodes',
											'userid': <?php echo $userid;?>,
											'pendingTask': 'yes'
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fxcodename',
									value         : filtersBPT.property.xcode,
									hiddenName    : 'fxcode',
									hiddenValue   : filtersBPT.property.xcode,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.property.xcode = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Property Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','-Select-'],
											['A','Active'],
											['NA','Non-Active'],
											['NF','Not for Sale'],
											['S','Sold'] 
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatusname',
									value         : filtersBPT.property.status,
									hiddenName    : 'fstatus',
									hiddenValue   : filtersBPT.property.status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.property.status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fagent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Contact',
									name		  : 'fagent',
									value		  : filtersBPT.agent,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersBPT.agent=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fntask',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Task',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER'],
											['15','Send VOICE MAIL'],
											['16','Receive VOICE MAIL'],
											['17','Make NOTE']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fntaskname',
									value         : filtersBPT.task.type,
									hiddenName    : 'fntask',
									hiddenValue   : filtersBPT.task.type,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.task.type = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fndate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Exec. Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fndatecname',
										hiddenName: 'fndatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersBPT.task.datec,
										listeners: {
											'select': function (combo,record,index){
												filtersBPT.task.datec = record.get('id');
												var secondfield = Ext.getCmp('fndateb');
												secondfield.setValue('');
												filtersBPT.task.dateb='';
												
												if(filtersBPT.task.datec=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fndate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fndate',
											value		  : filtersBPT.task.date,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersBPT.task.date=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fndateb',
										name		  : 'fndateb',
										hidden		  : filtersBPT.task.datec!='Between',
										value		  : filtersBPT.task.dateb,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersBPT.task.dateb=newvalue;
											}
										}
									}]
								}]	
							},{
								layout	: 'form',
								id		: 'fsdate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Sche. Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fsdatecname',
										hiddenName: 'fsdatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersBPT.task.sdatec,
										listeners: {
											'select': function (combo,record,index){
												filtersBPT.task.sdatec = record.get('id');
												var secondfield = Ext.getCmp('fndateb');
												secondfield.setValue('');
												filtersBPT.task.sdateb='';
												
												if(filtersBPT.task.sdatec=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fsdate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fsdate',
											value		  : filtersBPT.task.sdate,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersBPT.task.sdate=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fsdateb',
										name		  : 'fsdateb',
										hidden		  : filtersBPT.task.sdatec!='Between',
										value		  : filtersBPT.task.sdateb,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersBPT.task.sdateb=newvalue;
											}
										}
									}]
								}]	
							},{
								layout	: 'form',
								id		: 'ftstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Status Task',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Pending'],
											['2','Error']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'ftstatusname',
									value         : filtersBPT.task.status,
									hiddenName    : 'ftstatus',
									hiddenValue   : filtersBPT.task.status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.task.status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fexectype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Exec. Type',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['1','Manual'],
											['2','Automatic']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fexectypename',
									value         : filtersBPT.task.execType,
									hiddenName    : 'fexectype',
									hiddenValue   : filtersBPT.task.execType,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersBPT.task.execType = record.get('valor');
										}
									}
								}]
							}]
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								//icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Load&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'load-filters'
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning',output);
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											if(rest.total==0){
												Ext.MessageBox.alert('Warning','You don\'t have default filters saved');
											}else{
												formmyfollowup.getForm().findField('faddress').setValue(rest.data.address);
												formmyfollowup.getForm().findField('fzip').setValue(rest.data.zip);
												formmyfollowup.getForm().findField('fagent').setValue(rest.data.agent);
												formmyfollowup.getForm().findField('fstatus').setValue(rest.data.status);
												formmyfollowup.getForm().findField('fcounty').setValue(rest.data.county);
												formmyfollowup.getForm().findField('fcity').setValue(rest.data.city);
												formmyfollowup.getForm().findField('fxcode').setValue(rest.data.xcode);
												
												formmyfollowup.getForm().findField('fndate').setValue(rest.data.ndate);
												formmyfollowup.getForm().findField('fndateb').setValue(rest.data.ndateb);
												formmyfollowup.getForm().findField('fsdate').setValue(rest.data.sdate);
												formmyfollowup.getForm().findField('fsdateb').setValue(rest.data.sdateb);
												
												if(rest.data.ntask==-2){
													rest.data.ntask=-1;
												}
												formmyfollowup.getForm().findField('fntask').setValue(rest.data.ntask);
												formmyfollowup.getForm().findField('ftstatus').setValue(rest.data.tstatus);
												formmyfollowup.getForm().findField('fexectype').setValue(rest.data.exectype);
											}
										}
									});
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/save.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Save&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_followup.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'save-filters',
											address: formmyfollowup.getForm().findField('faddress').getValue(),
											agent: formmyfollowup.getForm().findField('fagent').getValue(),
											status: formmyfollowup.getForm().findField('fstatus').getValue(),
											zip: formmyfollowup.getForm().findField('fzip').getValue(),
											county: formmyfollowup.getForm().findField('fcounty').getValue(),
											city: formmyfollowup.getForm().findField('fcity').getValue(),
											xcode: formmyfollowup.getForm().findField('fxcode').getValue(),
											
											ndate: formmyfollowup.getForm().findField('fndate').getValue(),
											ndateb: formmyfollowup.getForm().findField('fndateb').getValue(),
											sdate: formmyfollowup.getForm().findField('fndate').getValue(),
											sdateb: formmyfollowup.getForm().findField('fndateb').getValue(),
											
											ntask: formmyfollowup.getForm().findField('fntask').getValue(),
											tstatus: formmyfollowup.getForm().findField('ftstatus').getValue(),
											execType: formmyfollowup.getForm().findField('fexectype').getValue()
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Error saving filters');
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											Ext.MessageBox.alert('Warning','Default search successfully saved');
										}
									});
								}				
							},'->',{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersBPT.property.address = formmyfollowup.getForm().findField('faddress').getValue();
									filtersBPT.agent = formmyfollowup.getForm().findField('fagent').getValue();
									filtersBPT.property.status = formmyfollowup.getForm().findField('fstatus').getValue();
									filtersBPT.property.zip = formmyfollowup.getForm().findField('fzip').getValue();
									filtersBPT.property.county = formmyfollowup.getForm().findField('fcounty').getValue();
									filtersBPT.property.city = formmyfollowup.getForm().findField('fcity').getValue();
									filtersBPT.property.xcode = formmyfollowup.getForm().findField('fxcode').getValue();
									
									filtersBPT.task.date = formmyfollowup.getForm().findField('fndate').getValue();
									filtersBPT.task.dateb = formmyfollowup.getForm().findField('fndateb').getValue();
									filtersBPT.task.datec = formmyfollowup.getForm().findField('fndatec').getValue();
									filtersBPT.task.sdate = formmyfollowup.getForm().findField('fsdate').getValue();
									filtersBPT.task.sdateb = formmyfollowup.getForm().findField('fsdateb').getValue();
									filtersBPT.task.sdatec = formmyfollowup.getForm().findField('fsdatec').getValue();
									
									filtersBPT.task.type = formmyfollowup.getForm().findField('fntask').getValue();
									filtersBPT.task.execType = formmyfollowup.getForm().findField('fexectype').getValue();
									filtersBPT.task.status = formmyfollowup.getForm().findField('ftstatus').getValue();
									
									storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
									//storemyfollowtaskAll.load();
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersBPT.property.address = '';
									filtersBPT.agent = '';
									filtersBPT.property.status = 'ALL';
									filtersBPT.task.date = '';
									filtersBPT.task.dateb = '';
									filtersBPT.task.datec = 'Equal';
									filtersBPT.task.sdate = '';
									filtersBPT.task.sdateb = '';
									filtersBPT.task.sdatec = 'Equal';
									filtersBPT.task.type = '-1';
									filtersBPT.task.status = '-1';
									filtersBPT.task.execType = '-1';									
									filtersBPT.property.zip='';
									filtersBPT.property.county = 'ALL';
									filtersBPT.property.city = 'ALL';
									filtersBPT.property.xcode = 'ALL';
									
									//Ext.getCmp('formmytasks').getForm().reset();
									formmyfollowup.getForm().findField('faddress').setValue('');
									formmyfollowup.getForm().findField('fzip').setValue('');
									formmyfollowup.getForm().findField('fagent').setValue('');
									formmyfollowup.getForm().findField('fstatus').setValue('ALL');
									formmyfollowup.getForm().findField('fcounty').setValue('ALL');
									formmyfollowup.getForm().findField('fcity').setValue('ALL');
									formmyfollowup.getForm().findField('fxcode').setValue('ALL');
									
									formmyfollowup.getForm().findField('fndate').setValue('');
									formmyfollowup.getForm().findField('fndateb').setValue('');
									formmyfollowup.getForm().findField('fndatec').setValue('Equal');
									formmyfollowup.getForm().findField('fndate').setValue('');
									formmyfollowup.getForm().findField('fndateb').setValue('');
									formmyfollowup.getForm().findField('fndatec').setValue('Equal');
									
									formmyfollowup.getForm().findField('fntask').setValue('-1');
									formmyfollowup.getForm().findField('fexectype').setValue('-1');
									formmyfollowup.getForm().findField('ftstatus').setValue('-1');
									//storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
									//win.close();
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 650,
								height      : 450,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowup,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersBPT.property.address = '';
					filtersBPT.agent = '';
					filtersBPT.property.status = 'ALL';
					filtersBPT.task.date = '';
					filtersBPT.task.dateb = '';
					filtersBPT.task.datec = 'Equal';
					filtersBPT.task.sdate = '';
					filtersBPT.task.sdateb = '';
					filtersBPT.task.sdatec = 'Equal';
					filtersBPT.task.type = '-1';
					filtersBPT.task.status = '-1';
					filtersBPT.task.execType = '-1';									
					filtersBPT.property.zip='';
					filtersBPT.property.county = 'ALL';
					filtersBPT.property.city = 'ALL';
					filtersBPT.property.xcode = 'ALL';
					
					storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
					//storemyfollowtaskAll.load();
				}
			}),{
				tooltip: 'Complete Task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/refresh25.png',
				handler: function(){
					if(selected_datamyfollowtask.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the record to be completed.'); return false;
					}
					if(AllCheckmyfollowtask==true){
						selectedAux=new Array();
						var totales = storemyfollowtaskAll.getRange(0,storemyfollowtaskAll.getCount());
						
						for(i=0;i<storemyfollowtaskAll.getCount();i++){
							selectedAux.push({
								'pid': totales[i].data.pid, 
								'id': totales[i].data.idfus, 
								'task': totales[i].data.ntask
							});
						}
					
					}else{
						var selectedAux1 = gridmyfollowtask.getSelectionModel().getSelections();
						selectedAux=new Array();
						
						for(i=0;i<selectedAux1.length;i++){
							selectedAux.push({
								'pid': selectedAux1[i].data.pid, 
								'id': selectedAux1[i].data.idfus, 
								'task': selectedAux1[i].data.ntask
							});
						}
					}
					
					loading_win.show();
					completeMultiTask(selectedAux,gridmyfollowtask,storemyfollowtask,useridspider, progressBarTask_win, progressBarTask, false);
				}
			}/*,new Ext.Button({
				tooltip: 'View Help',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
				handler: function(){
					//alert(user_loged+"||"+user_block+"||"+user_web);
					if(!user_loged || user_block || user_web){ login_win.show(); return false;}
					showVideoHelp('BuyingPendingTask');
				}
			})*/
		]
	});
	
	function ejecutarUpdatesTask(response,options){
		progressBarTask.updateProgress(
			((countProgressBar+1)/selected_datamyfollowtask.length),
			"Processing "+(countProgressBar+1)+" of "+selected_datamyfollowtask.length
		);
		
		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			if((countProgressBar)==selected_datamyfollowtask.length-1 || selected_datamyfollowcontract.length<=0){
				progressBarTask_win.hide();
				storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
				//storemyfollowtaskAll.load();
				//smmyfollowcontract.deselectRange(0,limitmyfollowtask);
			}else{
				countProgressBar++;
				Ext.Ajax.request({  
					waitMsg: 'Checking...',
					url: urlProgressBar, 
					method: 'POST',
					timeout :120000, 
					params: { 
						pids: '\''+selected_datamyfollowtask[countProgressBar]+'\'',
					},
					
					failure:function(response,options){
						progressBarTask_win.hide();
						var output = '';
						for (prop in response) {
						  output += prop + ': ' + response[prop]+'; ';
						}
						Ext.MessageBox.alert('Warning',output);
					},
					
					success: ejecutarUpdatesTask                      
				});
			}
		}else{
			progressBarTask_win.hide();
			//smmyfollowcontract.deselectRange(0,limitmyfollowtask);
		}	
	}
	var progressBarTask= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarTask_win=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarTask]
	});
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	var gridmyfollowtask = new Ext.grid.EditorGridPanel({
		renderTo: 'mytasks_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemyfollowtask,
		stripeRows: true,
		sm: smmyfollowtask,
		clicksToEdit: 1, 
		columns: [
			smmyfollowtask,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'},{header: 'User', width: 40, hidden: true, editable: false, renderer: userRender, dataIndex: 'userid_follow'},{header: 'V', width: 25, sortable: true, tooltip: 'View.', dataIndex: 'msj', renderer: viewRender}";
		   		foreach($hdArray as $k=>$val){
		   			if($val->name=='status')
						echo ",{header: 'S', width: 25, sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', renderer: statusRender}";	
					else
						if($val->name!='agent')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				
				echo ",{header: 'T', width: 25, renderer: taskRender, tooltip: 'Next Task.', dataIndex: 'ntask', sortable: true}";
				echo ",{header: 'Sche. Time', width: 110, renderer: Ext.util.Format.dateRenderer('m/d/Y h:i A'), dataIndex: 'stime', align: 'right', sortable: true, tooltip: 'The time the task was created.'}";
				echo ",{header: 'Exec. Time', width: 110, renderer: Ext.util.Format.dateRenderer('m/d/Y h:i A'), dataIndex: 'etime', align: 'right', sortable: true, tooltip: 'The time the task will be executed.'}";
				echo ",{header: 'S. Detail', width: 200, sortable: true, tooltip: 'Schedule Detail', dataIndex: 'detail'}";
				echo ",{header: 'Status', width: 200, sortable: true, tooltip: 'Status Schedule Task', dataIndex: 'statusschedule'}";
				echo ",{header: 'E', width: 25, renderer: execRender, tooltip: 'Execution Type', dataIndex: 'typeExec', sortable: true}";
					// Agregado por luis r Castro Sugerencia 11859 12/05/2015
				echo ",{header: 'Agent', width: 100,  tooltip: 'Agent', dataIndex: 'agent', sortable: true}";
				echo ",{header: 'Phone', width: 100, tooltip: 'Phone', dataIndex: 'phone1', sortable: true}";
		   //////////////////
		   ?>			 
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmytasks',
            pageSize: limitmyfollowtask,
            store: storemyfollowtask,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowtask=50;
					Ext.getCmp('pagingmytasks').pageSize = limitmyfollowtask;
					Ext.getCmp('pagingmytasks').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupBT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowtask=80;
					Ext.getCmp('pagingmytasks').pageSize = limitmyfollowtask;
					Ext.getCmp('pagingmytasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupBT'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowtask=100;
					Ext.getCmp('pagingmytasks').pageSize = limitmyfollowtask;
					Ext.getCmp('pagingmytasks').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupBT'
			}),'-',new Ext.form.Label({
				text: 'Total Sent: '+totalsent,
				id: 'mailenviadostask'
			})]
        }),
		
		listeners: {
			'click': function(){
				if(document.getElementById('generate_contract')){
					var tab = tabs.getItem('generate_contract');
					tabs.remove(tab);
				}				
			},
			'afteredit': function (oGrid_Event) {
				var fieldValue = oGrid_Event.value;
				if(fieldValue=='BEGIN')
				{Ext.MessageBox.alert('Warning','Error select BEGIN'); return;}
				//alert(fieldValue+' '+oGrid_Event.record.data.pid+' '+oGrid_Event.field)
				Ext.Ajax.request({   
					waitMsg: 'Saving changes...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php', 
					method: 'POST',
					params: {
						type: "change-lprice", 
						pid: oGrid_Event.record.data.pid,
						field: oGrid_Event.field,
						value: fieldValue,
						originalValue: oGrid_Event.record.modified,
						userid: <?php echo $userid;?>
					},
					
					failure:function(response,options){
						Ext.MessageBox.alert('Warning','Error editing');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						
						if(rest.succes==false)
							Ext.MessageBox.alert('Warning',rest.msg);
							
						storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
					}
				 });
			},
			'sortchange': function (grid, sorted){
				filtersBPT.order.field=sorted.field;
				filtersBPT.order.direction=sorted.direction;
			},
			'rowcontextmenu': function(grid, rowIndex, e){
				e.preventDefault();
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var county = record.get('county');
				var status = record.get('status');
				
				var contract = new Ext.Action({
					text: 'Go to Contract',
					handler: function(){
						Ext.Ajax.request( 
						{  
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: <?php echo $_COOKIE['datos_usr']['USERID'];?>
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								
								var r=Ext.decode(response.responseText);
								
								if(r.results=='error'){
									Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
								}else{ 
									if(Ext.isIE)
										window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
									 else
										window.open(r.url,'_newtab');
								}
								return false;
							}                                
						});
					}
				});
				
				var status = new Ext.Action({
					text: 'Change Status',
					handler: function(){
						statusAltPanel(storemyfollowtask,<?php echo $userid;?>,pid,record.get('statusalt'));
					}
				});
				
				var overview = new Ext.Action({
					text: 'Go to Overview',
					handler: function(){
						createOverview(county,pid,status,false,false);
					}
				});
				
				var menu = new Ext.menu.Menu({
					items: [
						status,overview,contract
					]
				});
				
				menu.showAt(e.getXY());
				return false;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = grid.getView().findCellIndex(e.getTarget()); 
				if(cell !== 0 && cell !== 3 && cell !== 14 && cell !== 15 && cell !== 16){
					var record = grid.getStore().getAt(rowIndex);
					var pid = record.get('pid');
					var county = record.get('county');
					var status = record.get('status');
					var idfus = record.get('idfus');
					var typeFollow = 'B';
					
					loading_win.show();
					editScheduleTask(idfus, <?php echo $userid;?>, false, storemyfollowtask);
				}
			}
		}
	});
	
	loadedBPT=true;
	storemyfollowtask.load({params:{start:0, limit:limitmyfollowtask}});
</script>