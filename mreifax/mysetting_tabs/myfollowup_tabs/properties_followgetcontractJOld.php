<?php
////////////////////******************************************************************////////////////////
////////////////////******************************************************************////////////////////   
////////////////////*****************************BY JESUS*****************************////////////////////
////////////////////******************************************************************////////////////////
////////////////////******************************************************************////////////////////

//header ("Cache-Control: no-cache, must-revalidate"); //no guardar en CACHE
//header ("Pragma: no-cache");
require_once('../../custom_contract/fpdf/fpdf.php');
require_once('../../custom_contract/fpdi/fpdi.php');
require_once("../../properties_conexion.php");
require_once($_SERVER['DOCUMENT_ROOT']."/custom_contract/functions.php");

//   DELETE OLD PDFS
// -------------------------------------------

if($_SERVER['HTTP_HOST']=='xima3.reifax.com')
	$folder_to_clean='../../custom_contract/temp/';
else
	$folder_to_clean='../custom_contract/temp/';

//limpiardirpdf2($folder_to_clean);


// -------------------------------------------
//   GET FORM DATA
// -------------------------------------------
$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
$type       = $_POST['type'];
$county     = $_POST['county'];
$parcelid   = $_POST['pid'];
$sendme     = $_POST['sendme'];
$sendmail   = $_POST['sendmail'];
$sendtype 	= intval($_POST['sendtype']);
$typeFollow = isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';

conectar();
$q="select bd, offer, address, mlnumber, a.* from followup f
		LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$parcelid."')";
$res = mysql_query($q) or die($q.mysql_error());
while($r=mysql_fetch_array($res)){
	$county=$r['bd'];
	$rname      = $r['agent'];
	$offer      = $r['offer'];
	$rcompany	= $r['company'];
	if($offer==0){
		$resp = array('success'=>'true','enviado'=>'0');
		
		echo json_encode($resp);
		//Saved error in pending task.
		$_POST['onlyFunctions']=true;
		include_once('properties_followupEmail.php');
		scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',7,'','ERROR: Offer price $0.00',$typeFollow);
		return;
	}
	if($sendtype==5){
		$remail     = $r['email'];
		
		if($remail==''){
			$remail = $r['email2'];
		}
		if($remail=='' && $sendmail=='on'){
			$resp = array('success'=>'true','enviado'=>'0');
			
			echo json_encode($resp);
			//Saved error in pending task.
			$_POST['onlyFunctions']=true;
			include_once('properties_followupEmail.php');
			scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',7,'','ERROR: Email not found.',$typeFollow);
			return;
		}
	}else if($sendtype==3){
		$agentfax='';
		if($r['typeph1']==3){
			$agentfax=$r['phone1'];
		}else if($r['typeph2']==3){
			$agentfax=$r['phone2'];
		}else if($r['typeph3']==3){
			$agentfax=$r['phone3'];
		}else if($r['typeph4']==3){
			$agentfax=$r['fax'];
		}else if($r['typeph5']==3){
			$agentfax=$r['tollfree'];
		}else if($r['typeph6']==3){
			$agentfax=$r['phone6'];
		}
		if($agentfax==''){
			if($r['typeph1']==6){
				$agentfax=$r['phone1'];
			}else if($r['typeph2']==6){
				$agentfax=$r['phone2'];
			}else if($r['typeph3']==6){
				$agentfax=$r['phone3'];
			}else if($r['typeph4']==6){
				$agentfax=$r['fax'];
			}else if($r['typeph5']==6){
				$agentfax=$r['tollfree'];
			}else if($r['typeph6']==6){
				$agentfax=$r['phone6'];
			}
		}
		
		$query='SELECT fax_domain FROM  xima.contracts_phonesettings p WHERE userid = '.$userid;
		$result = mysql_query($query) or die($query.mysql_error());
		$r=mysql_fetch_array($result);
		
		if(strlen(trim($agentfax)) == 0 || strlen(trim($r['fax_domain'])) == 0){
			$resp = array('success'=>'true','enviado'=>'0');
			
			echo json_encode($resp);
			//Saved error in pending task.
			
			$error = strlen(trim($r['fax_domain'])) == 0 ? 'ERROR: fax_domain settings not found.':'ERROR: Phone not found.';
			
			$_POST['onlyFunctions']=true;
			include_once('properties_followupEmail.php');
			scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',7,'',$error,$typeFollow);
			return;
		}
	}
}

$conex_functions=conectarPorNameCounty($county);
//Obtain Real County Name
$aux		=	explode("^",$conex_functions);
$idCounty	=	$aux[1];
$query		=	"SELECT County FROM xima.lscounty WHERE idCounty='$idCounty'";
$result		=	mysql_query($query);
$result		=	mysql_fetch_assoc($result);
$countyName	=	strtoupper($result['County']);
//End Obtain Real County Name
$templateemail = $_POST['contracttemplate'];
$templatefax = $_POST['contracttemplatefax']; 
$completetask=$_POST['completetask']; 
//echo $q.' '.$county; return;
$contrOpc   = $_POST['options'];

$query		=	"SELECT * FROM xima.systemfonts WHERE idFont='$_POST[contractFontType]'";
$result		=	mysql_query($query);
$result		=	mysql_fetch_assoc($result);
$contractFontName	=	$result['usageName'];
$contractFontSize 	=	$_POST['contractFontSize'];
$contractColor 		=	$_POST['contractFontColor'];
$query		=	"SELECT * FROM xima.systemfonts WHERE idFont='$_POST[addonFontType]'";
$result		=	mysql_query($query);
$result		=	mysql_fetch_assoc($result);
$addonFontName	=	$result['usageName'];
$addonFontSize 	=	$_POST['addonFontSize'];
$addonColor 	=	$_POST['addonFontColor'];

$dateAcc    = $_POST['dateAcc'];
$dateClo    = $_POST['dateClo'];

$mlnumber   = $_POST['mlnaux'];
$address    = $_POST['addr'];

//$addendum   = $_POST['addendum'];
$addendata  = $_POST['fieldAddeum'];

//$addons     = $_POST['addons'];
$addondata  = $_POST['fieldAddonG'];
$deposit    = $_POST['deposit'];
$inspection = $_POST['inspection'];
$scrow      = $_POST['scrow'];

$chseller   = $_POST['csinfo'];
$sellname   = $_POST['sellname'];

$chbuyer    = $_POST['cbinfo'];
$buyername  = $_POST['buyername'];

$listingagent  = $rname;
$listingbroker    = $rcompany;
$buyeragent = $rname;
$buyerbroker      = $rcompany;

$sellingoptions= $_POST['csellinginfo'];
if($sellingoptions=='on'){
	$chseller   = 'on';
	$sellname   = $_POST['sellingname'];
	$chbuyer    = 'on';
	$buyername  = $_POST['buyername1'];
}
   
// -------------------------------------------
//   GET FORM DATA
// -------------------------------------------
//$type       = $_POST['type'];
//$county     = $_POST['county'];
//$parcelid   = $_POST['pid'];						////Parcel id de la Propiedad
//$templateemail = $_POST['contracttemplate'];
//$contrOpc   = $_POST['options'];
//$offer      = $_POST['offer'];
//$dateAcc    = $_POST['dateAcc'];
//$dateClo    = $_POST['dateClo'];
//$rname      = $_POST['listingagent'];
//$remail     = $_POST['remail'];
//$mlnumber   = $_POST['mlnaux'];
//$address    = $_POST['addr'];
$sendbyemail   = $_POST['sendbyemail'];
//$sendmail   = $_POST['sendmail'];
$addendum   = 'on';//$_POST['addendum'];
//$addendata  = $_POST['fieldAddeum'];
//$sendme     = $_POST['sendme'];
$addons     = 'on';//$_POST['addons'];
//$addondata  = $_POST['fieldAddonG'];	////Ids de los Adendum del Contrato Seleccionados
//$deposit    = $_POST['deposit'];
//$inspection = $_POST['inspection'];
//$scrow      = $_POST['scrow'];
//$chseller   = $_POST['csinfo'];
//$sellname   = $_POST['sellname'];
//$chbuyer    = $_POST['cbinfo'];
//$buyername  = $_POST['buyername'];
//$listingagent  = $_POST['listingagent'];
//$listingbroker    = $_POST['listingbroker'];
//$buyeragent = $_POST['buyeragent'];
//$buyerbroker      = $_POST['buyerbroker'];

////////////////////////////////Recibo y Creo Variables////////////////////////////////
//$userid = $_COOKIE['datos_usr']["USERID"];		////User ID
$contract_id = $_POST['type'];					////Contrato ID
switch($contractColor){
	case "black": 
		$contractColor=explode(",","0,0,0");
	break;
	case "blue":
		$contractColor=explode(",","0,0,255");
	break;
}
switch($addonColor){
	case "black": 
		$addonColor=explode(",","0,0,0");
	break;
	case "blue":
		$addonColor=explode(",","0,0,255");
	break;
}
$pdf = new FPDI();
$pdf->AddFont('Comic','',"comic.php");
$pdf->AddFont('Verdana','',"verdana.php");
$pdf->AddFont('BKANT','',"BKANT.php");
$pdf->AddFont('Tahoma','',"tahoma.php");
$pdf->AddFont('cour','',"cour.php");
$pdf->AddFont('ariblk','',"ariblk.php");
$pdf->AddFont('SCRIPTBL','',"SCRIPTBL.php");
$query = "SELECT campos, `desc`, tabla, idtc, type FROM xima.camptit WHERE variable='1' AND idtc<>733";
$result = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
while($rows = mysql_fetch_assoc($result)){
	$array_campos[] = $rows;
}
//print_r($array_campos);
//echo "<BR><BR>";

////////////////Extraigo la Informacion de todas las Variables estandard////////////////
$query = "SELECT tabla FROM xima.camptit WHERE variable=1 AND idtc<>733 GROUP BY tabla";
$result = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
$array_tablas = array();

/////////Recorro las Tablas/////////
while($tablas = mysql_fetch_array($result)){
	$query = "SELECT campos FROM xima.camptit WHERE variable=1 AND tabla='$tablas[tabla]'";
	$result1 = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
	$total_campos = mysql_affected_rows();
	$c=1;$campos_aux="";
	/////////Agrupo los Campos de la Respectiva Tabla para luego hacer el Query/////////
	while($campos = mysql_fetch_array($result1)){
		$campos_aux .= $campos['campos'];
		if($c<$total_campos){
			$campos_aux .= ",";
		}
		$c++;
	}
	$array_tablas[] = array('tabla' => $tablas[tabla], 'campos' => $campos_aux);
}

$variables_originales = array();
foreach($array_tablas as $aux){
	if($aux['tabla']=='mlsresidential' or $aux['tabla']=='psummary' or $aux['tabla']=='pendes' or $aux['tabla']=='mortgage'){
		$query = "SELECT $aux[campos] FROM $aux[tabla] WHERE parcelid='$parcelid'";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		while($rows = mysql_fetch_assoc($result)){
			$variables_originales[$aux['tabla']] = $rows;
		}
		if($aux['tabla']=='mlsresidential'){
			if(mysql_num_rows($result)<=0)
				//$variables_originales[$aux['tabla']] = Array('county' => obtainRealCountyNotForSale($county)); Comented By Jesus 20-06-2013
				$variables_originales[$aux['tabla']] = Array('county' => $countyName);
			else
				$variables_originales['mlsresidential']['county']=$countyName;
		}
	}
	if($aux['tabla']=='ximausrs' or $aux['tabla']=='profile' or $aux['tabla']=='contracts_mailsettings' or $aux['tabla']=='contracts_phonesettings' or $aux['tabla']=='contracts_scrow'){
		$query = "SELECT $aux[campos] FROM xima.$aux[tabla] WHERE userid='$userid'";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		while($rows = mysql_fetch_assoc($result)){
			$variables_originales[$aux['tabla']] = $rows;
		}
	}	
	if($aux['tabla']=='followagent'){
		$query = "SELECT a.$aux[campos] FROM xima.followup f LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) 
			WHERE f.userid=$userid and parcelid='$parcelid'";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		while($rows = mysql_fetch_assoc($result)){
			$variables_originales[$aux['tabla']] = $rows;
		}
	}	
	if($aux['tabla']=='followup'){
		$query = "SELECT f.$aux[campos] FROM xima.followup f LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) 
			WHERE f.userid=$userid and parcelid='$parcelid'";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		while($rows = mysql_fetch_assoc($result)){
			$variables_originales[$aux['tabla']] = $rows;
		}
	}	
	if($aux['tabla']=='contracts_custom'){
		$query = "SELECT 
					IF((
						SELECT tplactive FROM xima.$aux[tabla] WHERE userid='$userid' AND id='$contract_id')=1
						,'tpl1_name,tpl1_addr,tpl1_addr2,tpl1_addr3','tpl2_name as tpl1_name,tpl2_addr AS tpl1_addr,tpl2_addr2 AS tpl1_addr2,tpl2_addr3 AS tpl1_addr3')";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		$result = mysql_fetch_array($result);
		$campos = $result[0];
		$query = "SELECT $campos FROM xima.$aux[tabla] WHERE userid='$userid' AND id='$contract_id'";
		$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
		while($rows = mysql_fetch_assoc($result)){
			$variables_originales[$aux['tabla']] = $rows;
		}

	}
}
//print_r($variables_originales);
//echo "<BR><BR><BR>";

$array_campos_y_valores_idtc = array();
$array_campos_y_valores_desc = array();
foreach(array_keys($variables_originales) as $tabla){
	foreach(array_keys($variables_originales[$tabla]) as $campo){
		foreach($array_campos as $aux){
			if($aux['tabla']==$tabla and $aux['campos']==$campo){
				$idtc=$aux['idtc'];
				$desc=$aux['desc'];
				break;
			}
		}
		$array_campos_y_valores_idtc[$idtc] = $variables_originales[$tabla][$campo];
		$array_campos_y_valores_desc['{%'.$desc.'%}'] = $variables_originales[$tabla][$campo];
	}
}

/////////////////Modifico los Campos que Vienen del formulario de Generar el Contrato/////////////////
if(trim($_POST['buyername'])<>""){
	
}

////////////////Obtengo el Contrato a Utilizar////////////////
$query = "SELECT CONCAT('template_upload/',filename) AS filename FROM xima.contracts_custom c WHERE c.userid='$userid' AND c.id='$contract_id'";
$result = mysql_query($query) or die("Error: A ".mysql_error());
$r = mysql_fetch_array($result);

////////////////Obtengo el Numero de Paginas del Contrato////////////////
////////////////   Se puede Hacer de las Dos Maneras     ////////////////
//$pagecount = getNumPagesInPDF(array('../mysetting_tabs/mycontracts_tabs/'.$r['filename']));				
$pagecount = $pdf->setSourceFile('../../mysetting_tabs/mycontracts_tabs/'.$r['filename']);

for($i=1;$i<=$pagecount;$i++){
	//$pagecount = $pdf->setSourceFile('helloworld.pdf');
	$tplidx = $pdf->importPage($i, '/MediaBox');
	$new = $pdf->getTemplateSize($tplidx);
	//$pdf->addPage('P','reifax');
	$pdf->addPage('P', array($new['w'],$new['h'])); // lo dejo asi para que tome el tamaño original del documento
	$pdf->SetFont($contractFontName,'',$contractFontSize);
	$pdf->SetTextColor($contractColor[0],$contractColor[1],$contractColor[2]);

	$query = "SELECT * FROM xima.contracts_campos WHERE camp_userid='$userid' AND camp_contract_id='$contract_id' AND camp_page=$i";
	$result = mysql_query($query) or die("Error: C ".mysql_error());
	$rows = array();
	while($row=mysql_fetch_assoc($result)){
		$rows[]=$row;
	}
	$pdf->useTemplate($tplidx, 0, 0, $new['w'], $new['h']);	
	foreach($rows as $b){
		if($b['camp_type']=='variable'){
			$x=($b['camp_posx']/1.5);
			$y=($b['camp_posy']/1.5)+(0.352777778*25);
			//1 point = 0.352777778 milimetro
			$x=$x*(0.352777778);
			$y=$y*(0.352777778);
			switch($b['camp_idtc']){
				case "16":	//Legal Description
					$pdf->text($x,$y,"Legal Description as Shown in Public Records");
					break;				
				case "20":	//Owner Name == Seller
					if(strlen(trim($sellname))<1)
						$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					else
						$pdf->text($x,$y,$sellname);
					break;
				case "95":	//Listing Price
					$pdf->text($x,$y,number_format($array_campos_y_valores_idtc[$b['camp_idtc']],2));
					break;
				case "716":	//Contac Name
					if(strlen(trim($_POST['listingagent']))<1)
						$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					else
						$pdf->text($x,$y,$_POST['listingagent']);
					break;
				case "717":	//Contac Email 1
					if(strlen(trim($_POST['remail']))<1)
						$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					else
						$pdf->text($x,$y,$_POST['remail']);
					break;
				case "734":	//Buyer Name
					if(strlen(trim($buyername))<1)
						$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					else
						$pdf->text($x,$y,$buyername);
					break;
				case "743":	//Contac Company
					if(strlen(trim($_POST['listingbroker']))<1)
						$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					else
						$pdf->text($x,$y,$_POST['listingbroker']);
					break;
				case "XX1":	//Date Today
					$pdf->text($x,$y,date("m/d/Y"));
					break;
				case "XX2":	//Initial Deposit
					$pdf->text($x,$y,number_format($deposit,2));
					break;
				case "XX3": //Inspections Days
					$pdf->text($x,$y,$inspection);
					break;								
				case "XX4":	//Acceptance Date
					$pdf->text($x,$y,date($dateAcc));
					break;
				case "XX5":	//Closing Date
					$pdf->text($x,$y,date($dateClo));
					break;
				case "XX6":	//Balance To Close
					$Balance_To_Close = (int)$offer-(int)$deposit;
					$pdf->text($x,$y,number_format($Balance_To_Close,2));
					break;
				case "XX7":	//Offer Price
					$offer_price=(int)$offer;
					$pdf->text($x,$y,number_format($offer_price,2));
					break;
				case "XX8":	//Buyer's Agent
					$pdf->text($x,$y,$buyeragent);
					break;
				case "XX9":	//Buyer's Broker
					$pdf->text($x,$y,$buyerbroker);
					break;
				default:
					$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
			}
		}
		if($b['camp_type']=='variable_user'){
			$x=($b['camp_posx']/1.5);
			$y=($b['camp_posy']/1.5)+(0.352777778*25);
			//1 point = 0.352777778 milimetro
			$x=$x*(0.352777778);
			$y=$y*(0.352777778);
			$campo_concatenado = $b['camp_text'];
			foreach(array_keys($array_campos_y_valores_desc) as $campo){
				$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
			}
			$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
			$campo_concatenado = str_replace("{%Initial Deposit%}",$deposit,$campo_concatenado);
			$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
			$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
			$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
			$campo_concatenado = str_replace("{%Balance To Close%}",number_format((int)$offer-(int)$deposit,2),$campo_concatenado);
			$campo_concatenado = str_replace("{%Offer Price%}",number_format((int)$offer,2),$campo_concatenado);
			$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
			$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
			$pdf->text($x,$y,$campo_concatenado);
			
		}
		
		if($b['camp_type']=='variable_math'){
			$x=($b['camp_posx']/1.5);
			$y=($b['camp_posy']/1.5)+(0.352777778*25);
			//1 point = 0.352777778 milimetro
			$x=$x*(0.352777778);
			$y=$y*(0.352777778);
			$campo_concatenado = str_replace(")","%}",str_replace("(","{%",str_replace("%}","",str_replace("{%","",$b['camp_text']))));
			foreach(array_keys($array_campos_y_valores_desc) as $campo){
				$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
			}
			//$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
			$campo_concatenado = str_replace("{%Initial Deposit%}",$deposit,$campo_concatenado);
			//$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
			//$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
			//$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
			$campo_concatenado = str_replace("{%Balance To Close%}",((int)$offer-(int)$deposit),$campo_concatenado);
			$campo_concatenado = str_replace("{%Offer Price%}",$offer,$campo_concatenado);
			//$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
			//$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
			$aux_math	=	explode(" ",$campo_concatenado);
			switch($aux_math[1]){
				case "+":
					$campo_concatenado	=	number_format(($aux_math[0]	+	$aux_math[2]),2,".",",");
					break;
				case "-":
					$campo_concatenado	=	number_format(($aux_math[0]	-	$aux_math[2]),2,".",",");
					break;
				case "*":
					$campo_concatenado	=	number_format(($aux_math[0]	*	$aux_math[2]),2,".",",");
					break;
				case "/":
					$campo_concatenado	=	number_format(($aux_math[0]	/	$aux_math[2]),2,".",",");
					break;
				case "%":
					$campo_concatenado	=	number_format(($aux_math[0]	*	($aux_math[2]/100)),2,".",",");
					break;
			}
			$pdf->text($x,$y,$campo_concatenado);
			
		}
		
		if($b['camp_type']=='check'){
			switch((int)$b['camp_idtc']){
				case 1:
					$imagen='../../img/check1.jpg';
					$y=(($b['camp_posy']-2.5)/1.5);
					break;
				case 2:
					$imagen='../../img/check2.jpg';
					$y=(($b['camp_posy']-1.5)/1.5);
					break;
				case 3:
					$imagen='../../img/check3.jpg';
					$y=(($b['camp_posy']-2.5)/1.5);
					break;
			}
			$x=(($b['camp_posx']-3)/1.5);
			//$y=(((int)$b['camp_posy']-1.5)/1.5);
			//1 point = 0.352777778 milimetro
			$x=$x*(0.352777778);
			$y=$y*(0.352777778);			
			$pdf->Image("..".$imagen,$x,$y,3,3,'JPG');
		}
		if($b['camp_type']=='signature'){
			
			$query = "SELECT imagen FROM xima.contracts_signature WHERE `userid`='$userid' AND `type`=$b[camp_idtc]";
			$result = mysql_query($query) or die("Error: C ".mysql_error());
			$imagen = mysql_fetch_assoc($result);
			$aux=explode("|",$b['camp_posx']);
			$x=$aux[0];
			$width=(($aux[1]/1.5)*0.352777778)-(0.352777778*3);

			$aux=explode("|",$b['camp_posy']);
			$y=$aux[0];
			$height=(($aux[1]/1.5)*0.352777778)-(0.352777778*1);

			$x=($x / 1.5);
			$y=($y / 1.5);
			//1 point = 0.352777778 milimetro
			$x=$x*(0.352777778);
			$y=$y*(0.352777778);
			//echo "<BR> X: ".$x." Y: ".$y." Width: ".$width." Height: ".$height;
			if(mysql_affected_rows()>0){
				$pdf->Image("../../mysetting_tabs/mycontracts_tabs/signatures/".$imagen['imagen'],$x,$y,$width,$height);
			}
		}
	}

	//$e=true;
	if($e==true){ 
		print_r($new);
		echo "<br>";
		echo (int)$new['h'];
	}
	
		
	unset($rows); //limpio el array
}//Fin del Ciclo para cargar todas las Paginas
	$pdf->SetTextColor(0,0,0);
    // --------------
    // ADD NEW PAGES WITH THE ADDENDUMS IF SELECTED

    if ($addendum=='on' and trim($addendata)<>"") {

        $pdf->SetAutoPageBreak(true);
        $pdf->SetTextColor(0,0,0);

        $y_axis_initial = 25;

        $arrEsp = explode('|', $addendata);
        $carr   = count($arrEsp);

        $pdf->AddPage('P');

        $pdf->SetY(0.5); $pdf->SetX(1.3);

        $pdf->SetFont($contractFontName, '', 16);
        //$pdf->SetFont('Helvetica', 'B', 16);
        $pdf->Cell(0, 30, 'Addendums',0,0,'C');

        $pdf->SetY(25); $pdf->SetX(15);

        $text = '';

        for ($iK = 1; $iK < $carr; $iK++) {

            $query   = "SELECT content FROM xima.contracts_addendum
                            WHERE userid = $userid AND id=".$arrEsp[$iK]."
                            ORDER BY id ASC";
            $rs      = mysql_query($query);

            if ($rs) {
                $row = mysql_fetch_array($rs);

                $text .= utf8_encode(" - ".$row[0]."\n\n");

            }
        }

        $pdf->SetFont($contractFontName, '', $contractFontSize);
        //$pdf->SetFont('Helvetica', '', 11);
        $pdf->MultiCell(185,5,$text);
    }
	
    // --------------
    // SCROW DEPOSIT LETTER
	$address = "$array_campos_y_valores_idtc[538], $array_campos_y_valores_idtc[10], $array_campos_y_valores_idtc[536]";	//538 Property Address, 10 Property City, 536 Property Zip
	
    if ($scrow=='on') {

		// Text of the letter
        $date  = date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $doc   = "\n\nDate: $date\n\n";
        $doc  .= "Re: ".(strlen(trim($buyername))<1 ? $array_campos_y_valores_idtc[734] : $buyername)."\n\n";	//734 Buyer Name
        $doc  .= "Property Address: $address\n\n";
        $doc  .= "To Whom It May Concern:\n\n";
        $doc  .= "In connection with the above referenced real estate transaction, ";
        $doc  .= "please note that $array_campos_y_valores_idtc[752], has received a deposit from the\n\n";	//752 Escrow Agent
        $doc  .= "Buyer: ".(strlen(trim($buyername))<1 ? $array_campos_y_valores_idtc[734] : $buyername)."\n\n";	//734 Buyer Name
		//$doc  .= "In the amount of $ ".number_format($deposit,2)." as earnest money towards the purchase "; Comented By Kristy Freddy Frank and Jesus por orden de frank 10-08-2013
        $doc  .= "In the amount of $ 1,000.00 as earnest money towards the purchase ";
        $doc  .= "of the above captioned property on $date.\n\n";
        $doc  .= "If you have any questions, please do not hesitate to contact the undersigned.\n\n";

        // Get header and Footer
        $query   = "SELECT imagen,place FROM xima.contracts_scrow
                        WHERE userid = $userid ORDER BY place ASC";
        $rs      = mysql_query($query);
        $path   = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/scrows/';
        $imgSc  = null;
        if ($rs) {
            while ($row = mysql_fetch_array($rs)) {
                $imgSc["s{$row['place']}"] = $path.$row[0];
            }
        }

        $pdf->SetAutoPageBreak(false);

		$pdf->SetTextColor(0,0,0);
		
        $pdf->AddPage('P');

        $pdf->SetFont('Helvetica', '', 13);

        if ($imgSc['s1']!='') {
            $pdf->SetY(20); $pdf->SetX(15);
            $pdf->Image($imgSc['s1'],null,null,185);
        }

        $pdf->SetY(100); $pdf->SetX(15);
        $pdf->MultiCell(185,5,$doc);

        if ($imgSc['s2']!='') {
            $pdf->SetY(210); $pdf->SetX(15);
            $pdf->Image($imgSc['s2'],null,null,185);
        }
    }

	// --------------
    // ADD NEW PAGES WITH THE ADDONS IF SELECTED
	
	if(strlen(trim($addondata))>0){
		$total_addendum=explode("|",substr($addondata,1));
		foreach($total_addendum as $other){
			$query = "SELECT CONCAT('addons/',document) AS filename,CONCAT(id,'-',userid,'-',place) AS addon_id  FROM xima.contracts_addonscustom c WHERE c.userid='$userid' AND c.id='$other'";
			$result = mysql_query($query) or die("Error: A ".mysql_error());
			$r = mysql_fetch_array($result);
			$addon_id=$r['addon_id'];
			$page_A_count = $pdf->setSourceFile('../../mysetting_tabs/mycontracts_tabs/'.$r['filename']);

			for($j=1;$j<=$page_A_count;$j++){
				$tplidx = $pdf->importPage($j, '/MediaBox');
				$new = $pdf->getTemplateSize($tplidx);
				$pdf->addPage('P', array($new['w'],$new['h'])); // lo dejo asi para que tome el tamaño original del documento
				$pdf->SetFont($addonFontName,'',$addonFontSize);
				$pdf->SetTextColor($addonColor[0],$addonColor[1],$addonColor[2]);

				$query = "SELECT * FROM xima.contracts_campos WHERE camp_userid='$userid' AND camp_contract_id='$addon_id' AND camp_page=$j";
				$result = mysql_query($query) or die("Error: C ".mysql_error());
				$rows = array();
				while($row=mysql_fetch_assoc($result)){
					$rows[]=$row;
				}
				////////////////OJO ESTO SE COLOCA ANTES O DESPUES; DEPENDE DE DONDE QUIERE QUE SALGA EL TEXTO////////////////
				$pdf->useTemplate($tplidx, 0, 0, $new['w'], $new['h']);
				foreach($rows as $b){
					if($b['camp_type']=='variable'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						switch($b['camp_idtc']){
							case "16":	//Legal Description
								$pdf->text($x,$y,"Legal Description as Shown in Public Records");
								break;				
							case "20":	//Owner Name == Seller
								if(strlen(trim($sellname))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$sellname);
								break;
							case "95":	//Listing Price
								$pdf->text($x,$y,number_format($array_campos_y_valores_idtc[$b['camp_idtc']],2));
								break;
							case "716":	//Contac Name
								if(strlen(trim($_POST['listingagent']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['listingagent']);
								break;
							case "717":	//Contac Email 1
								if(strlen(trim($_POST['remail']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['remail']);
								break;
							case "734":	//Buyer Name
								if(strlen(trim($buyername))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$buyername);
								break;
							case "743":	//Contac Company
								if(strlen(trim($_POST['listingbroker']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['listingbroker']);
								break;
							case "XX1":	//Date Today
								$pdf->text($x,$y,date("m/d/Y"));
								break;
							case "XX2":	//Initial Deposit
								$pdf->text($x,$y,number_format($deposit,2));
								break;
							case "XX3": //Inspections Days
								$pdf->text($x,$y,$inspection);
								break;								
							case "XX4":	//Acceptance Date
								$pdf->text($x,$y,date($dateAcc));
								break;
							case "XX5":	//Closing Date
								$pdf->text($x,$y,date($dateClo));
								break;
							case "XX6":	//Balance To Close
								$Balance_To_Close = (int)$offer-(int)$deposit;
								$pdf->text($x,$y,number_format($Balance_To_Close,2));
								break;
							case "XX7":	//Offer Price
								$offer_price=(int)$offer;
								$pdf->text($x,$y,number_format($offer_price,2));
								break;
							case "XX8":	//Buyer's Agent
								$pdf->text($x,$y,$buyeragent);
								break;
							case "XX9":	//Buyer's Broker
								$pdf->text($x,$y,$buyerbroker);
								break;
							default:
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
						}
					}
					if($b['camp_type']=='variable_user'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						$campo_concatenado = $b['camp_text'];
						foreach(array_keys($array_campos_y_valores_desc) as $campo){
							$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
						}
						$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
						$campo_concatenado = str_replace("{%Initial Deposit%}",$deposit,$campo_concatenado);
						$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
						$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
						$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
						$campo_concatenado = str_replace("{%Balance To Close%}",number_format((int)$offer-(int)$deposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Offer Price%}",number_format((int)$offer,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
						$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
						$pdf->text($x,$y,$campo_concatenado);
					}
					if($b['camp_type']=='variable_math'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						$campo_concatenado = str_replace(")","%}",str_replace("(","{%",str_replace("%}","",str_replace("{%","",$b['camp_text']))));
						foreach(array_keys($array_campos_y_valores_desc) as $campo){
							$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
						}
						//$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
						$campo_concatenado = str_replace("{%Initial Deposit%}",$deposit,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
						$campo_concatenado = str_replace("{%Balance To Close%}",((int)$offer-(int)$deposit),$campo_concatenado);
						$campo_concatenado = str_replace("{%Offer Price%}",$offer,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
						$aux_math	=	explode(" ",$campo_concatenado);
						switch($aux_math[1]){
							case "+":
								$campo_concatenado	=	number_format(($aux_math[0]	+	$aux_math[2]),2,".",",");
								break;
							case "-":
								$campo_concatenado	=	number_format(($aux_math[0]	-	$aux_math[2]),2,".",",");
								break;
							case "*":
								$campo_concatenado	=	number_format(($aux_math[0]	*	$aux_math[2]),2,".",",");
								break;
							case "/":
								$campo_concatenado	=	number_format(($aux_math[0]	/	$aux_math[2]),2,".",",");
								break;
							case "%":
								$campo_concatenado	=	number_format(($aux_math[0]	*	($aux_math[2]/100)),2,".",",");
								break;
						}
						$pdf->text($x,$y,$campo_concatenado);
						
					}
					
					if($b['camp_type']=='check'){
						switch((int)$b['camp_idtc']){
							case 1:
								$imagen='../../img/check1.jpg';
								$y=(($b['camp_posy']-2.5)/1.5);
								break;
							case 2:
								$imagen='../../img/check2.jpg';
								$y=(($b['camp_posy']-1.5)/1.5);
								break;
							case 3:
								$imagen='../../img/check3.jpg';
								$y=(($b['camp_posy']-2.5)/1.5);
								break;
						}
						$x=(($b['camp_posx']-3)/1.5);
						//$y=(((int)$b['camp_posy']-1.5)/1.5);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);			
						$pdf->Image("..".$imagen,$x,$y,3,3,'JPG');
					}
					if($b['camp_type']=='signature'){
						
						$query = "SELECT imagen FROM xima.contracts_signature WHERE `userid`='$userid' AND `type`=$b[camp_idtc]";
						$result = mysql_query($query) or die("Error: C ".mysql_error());
						$imagen = mysql_fetch_assoc($result);
						$aux=explode("|",$b['camp_posx']);
						$x=$aux[0];
						$width=(($aux[1]/1.5)*0.352777778)-(0.352777778*3);

						$aux=explode("|",$b['camp_posy']);
						$y=$aux[0];
						$height=(($aux[1]/1.5)*0.352777778)-(0.352777778*1);

						$x=($x / 1.5);
						$y=($y / 1.5);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						//echo "<BR> X: ".$x." Y: ".$y." Width: ".$width." Height: ".$height;
						if(mysql_affected_rows()>0){
							$pdf->Image("../../mysetting_tabs/mycontracts_tabs/signatures/".$imagen['imagen'],$x,$y,$width,$height);
						}
					}
				}

				//$e=true;
				if($e==true){ 
					print_r($new);
					echo "<br>";
					echo (int)$new['h'];
				}
				
				
			}//Fin del Ciclo para cargar todas las Paginas
			
		}
	}
	
/*
$tplidx = $pdf->importPage(2, '/MediaBox');
$pdf->addPage('P');
$pdf->getTemplateSize($tplidx);
$pdf->useTemplate($tplidx, 0, 0, $new['w'],$new['h']);
*/

//$pdf->Output('newpdf.pdf', 'I');	/////////////Abre El Documento

//$pdf->Output('../custom_contract/temp/'.$parcelid.'.pdf', 'F');
$pdf->Output('C:/inetpub/wwwroot/custom_contract/temp/'.$parcelid.'.pdf', 'F');
$archivo='C:/inetpub/wwwroot/custom_contract/temp/'.$parcelid.'.pdf';
$enviado='0';
    if ((($sendmail=='on' || $sendme=='on') && $sendtype==5) || $sendtype==3) {
        if($sendtype==5){
			$templa = $templateemail;
		}else if($sendtype==3){
			$templa = $templatefax;
		}
		/*********************************************
		*	This File Contens the Function SendEMail()
		*********************************************/
		include("properties_followgetcontractEmail.php");
		$enviado=SendEMail($archivo,$mlnumber,$address,$userid,$rname,$remail,$sendmail,$sendme,$templa,$county,$parcelid,$sendtype,$agentfax,$offer,$type,$typeFollow);

        // Save to My Document

        /*$query  = "SELECT address FROM psummary WHERE parcelid={$parcelid}";
        $rs     = mysql_query($query);
        $row    = mysql_fetch_array($rs);

        $dir    = 'saved_documents';
        $moment = date('YmdHisu');

        $doc    = "C:/inetpub/wwwroot/$dir/{$contratos[$type]}$parcelid_$moment.pdf";
        $url    = "http://www.reifax.com/$dir/{$contratos[$type]}$parcelid_$moment.pdf";

        copy("$archivo","C:/inetpub/wwwroot/$dir/{$contratos[$type]}$parcelid_$moment.pdf");

        $query  = "INSERT INTO xima.saveddoc
                    (usr,parcelid,url,directorio,sdate,address,name)
                VALUES
                    ($userid,$parcelid,'$url','$doc',NOW(),'{$row[0]}','contract_{$contratos[$type]}$parcelid.pdf')";
        $rs     = mysql_query($query);*/


    }

	if($sendmail=='on' && $enviado=='1'){
		$update="update xima.follow_contracts_sent set cant=cant+1 where userid=$userid";
		mysql_query($update) or die($update.mysql_error());
		$enviadoagente=1;
	}else{
		$enviadoagente=0;
	}
	if($enviadoagente==1)	$totalMailsSend	=	controlSendMails($userid,$parcelid);	else	$totalMailsSend=0;
    $resp = array('success'=>'true','enviado'=>$enviado, 'pdf'=>"custom_contract/temp/$parcelid.pdf", 'enviadoagente'=> $totalMailsSend);
		
	echo json_encode($resp);
	
	unset($rows);unset($r);unset($r2); //limpio el array

mysql_close($conex_functions);
clearstatcache();
?>   