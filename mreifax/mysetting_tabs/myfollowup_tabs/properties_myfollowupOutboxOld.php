<?php
	include("../../properties_conexion.php");
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYFollow','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID'];
	
	
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
.notSeenMailClass td{
	font-weight:bold !important;
}
</style>
<div align="left" id="todo_myfollowmail_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="myfollowmailOutbox_data_div" align="center" style=" background-color:#FFF; margin:auto; width:950px;">
        <div id="myfollowmail_progressBar"></div>
        <div id="myfollowmail_propertiesOutbox" align="left"></div> 
	</div>
</div>
<script>
	var storemyfollowmailOutbox = null;
	var limitmyfollowmailOutbox = 50;
Ext.onReady(function() {
	var selected_datamyfollowmailOutbox 	= new Array();
	var AllCheckmyfollowmailOutbox 		= false;
	var totalMailOutbox					= 0;
	var currentMailOutbox				= 0;
	var sincMailOutbox					= 0;
	
//filter variables
var filtersMEO = {
	address		: '',
	pending		: {
		contact		: -1,
		property	: -1
	},
	order		: {
		field		: 'fromName_msg',
		direction	: 'ASC'
	},
	email		: {
		from	: {
			name	: '',
			mail	: ''
		},
		to		: {
			mail	: ''
		},
		dates	: {
			after	: '',
			before	: '',
			type	: 'Equal'
		},
		type	: -1,
		attach	: -1,
		seen	: -1,
		content	: ''
	}
};

	storemyfollowmailOutbox = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({  
			url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
			timeout: 3600000 
		}),
		
		fields: [
			{name: 'idmail', type: 'int'},
			{name: 'userid', type: 'int'},
			{name: 'from_msg', type: 'string'},
			{name: 'fromName_msg', type: 'string'},
			{name: 'to_msg', type: 'string'},
			{name: 'subject', type: 'string'},
			{name: 'msg_date', type: 'date', dateFormat: 'Y-m-d H:i:s'},
			{name: 'attachments', type: 'int'},
			{name: 'seen', type: 'int'},
			{name: 'ac', type: 'int'},
			{name: 'agent', type: 'string'},
			{name: 'ap', type: 'string'},
			{name: 'address', type: 'string'},
			{name: 'task', type: 'int'}
	    ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': 		<?php echo $userid;?>,
			'typeEmail':	1,
			'checkmail': 	0,
			'currentMail': 	currentMailOutbox,
			'totalMail':  	totalMailOutbox
		},
		remoteSort: true,
		sortInfo: {
			field: 'msg_date', 
			direction: 'DESC'
		},
		listeners: {
			beforeload: function(store,obj){
				if(sincMailOutbox==1) progressBarOutboxWin.show();
				
				obj.params.toemail 	= filtersMEO.email.to.mail;
				obj.params.content	= filtersMEO.email.content;
				obj.params.dateAf	= filtersMEO.email.dates.after;
				obj.params.dateBe	= filtersMEO.email.dates.before;
				obj.params.dateTy	= filtersMEO.email.dates.type;
				obj.params.etype	= filtersMEO.email.type;
				obj.params.attach	= filtersMEO.email.attach;
				obj.params.pproperty= filtersMEO.pending.property;
			}, 
			load: function(store,records,opt){
				currentMailOutbox = store.reader.jsonData.currentMail;
				if(totalMailOutbox == 0) totalMailOutbox = store.reader.jsonData.totalMail;

				if(currentMailOutbox<totalMailOutbox){
					
					progressBarOutbox.updateProgress(
						(currentMailOutbox/totalMailOutbox),
						"Downloading emails "+currentMailOutbox+" of "+totalMailOutbox
					);
					
					storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox, currentMail:currentMailOutbox, totalMail:totalMailOutbox,checkmail:sincMailOutbox}});
				}else{
					progressBarOutboxWin.hide();
					progressBarOutbox.updateProgress(
						0,
						"Initializing emails download..."
					);
					currentMailOutbox = 0;
					totalMailOutbox = 0;
					sincMailOutbox=0;
				}
			},
			exception: function(){
				progressBarOutboxWin.hide();
				progressBarOutbox.updateProgress(
					0,
					"Initializing emails download..."
				);
				currentMailOutbox = 0;
				totalMailOutbox = 0;
				sincMailOutbox=0;
				Ext.Msg.alert('Sync Failed', 'You must set your email configuration in \'Follow up\' -> \'My settings\' -> \'Mail settings\' -> \'Email Reader Configuration\'.');
			}
		}
    });
	
	var progressBarOutbox = new Ext.ProgressBar({
		text: "Initializing emails download..."
	});
	
	var progressBarOutboxWin=new Ext.Window({
		title: 'Sync Email, please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarOutbox]
	});

	var smmyfollowmailOutbox = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyfollowmailOutbox.indexOf(record.get('idmail'))==-1)
					selected_datamyfollowmailOutbox.push(record.get('idmail'));
				
				if(Ext.fly(gridmyfollowmailOutbox.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyfollowmailOutbox=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyfollowmailOutbox = selected_datamyfollowmailOutbox.remove(record.get('idmail'));
				AllCheckmyfollowmailOutbox=false;
				Ext.get(gridmyfollowmailOutbox.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}		
	});
	var gridmyfollowmailOutbox = new Ext.grid.EditorGridPanel({
		renderTo: 'myfollowmail_propertiesOutbox',
		cls: 'grid_comparables',
		height: 3000,
		store: storemyfollowmailOutbox,
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.get('seen')==0) {
					return 'notSeenMailClass';
				} else {
					return '';
				}
			}
		},
		stripeRows: true,
		columns: [	
			smmyfollowmailOutbox
			,{header: 'T', width: 28, sortable: true, align: 'center', dataIndex: 'task', renderer: taskRender}
			,{header: 'A', width: 23, sortable: true, align: 'center', dataIndex: 'attachments', renderer: attachments}
			,{header: 'P', width: 23, sortable: true, align: 'center', dataIndex: 'ap', renderer: assignmentProperty}
			,{id: 'idmail', header: "To Mail", width: 150, align: 'left', sortable: true, dataIndex: 'to_msg'}		
			,{header: 'Subject', width: 620, sortable: true, align: 'left', dataIndex: 'subject'}
			,{header: "Date", width: 60, sortable: true, align: 'center', renderer: dateRender, dataIndex: 'msg_date'}
		],
		tbar: new Ext.PagingToolbar({
			id: 			'pagingmyfollowmailOutbox',
            pageSize: 		limitmyfollowmailOutbox,
            store: 			storemyfollowmailOutbox,
            displayInfo: 	true,
			displayMsg: 	'Total: {2} Emails.',
			emptyMsg: 		'No Emails to display',
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyfollowmailOutbox=50;
					Ext.getCmp('pagingmyfollowmailOutbox').pageSize = limitmyfollowmailOutbox;
					Ext.getCmp('pagingmyfollowmailOutbox').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupEmailInbox'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyfollowmailOutbox=80;
					Ext.getCmp('pagingmyfollowmailOutbox').pageSize = limitmyfollowmailOutbox;
					Ext.getCmp('pagingmyfollowmailOutbox').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupEmailInbox'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyfollowmailOutbox=100;
					Ext.getCmp('pagingmyfollowmailOutbox').pageSize = limitmyfollowmailOutbox;
					Ext.getCmp('pagingmyfollowmailOutbox').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupEmailInbox'
			})]
		}),
		listeners: {
			'sortchange': function (grid, sorted){
				filtersMEO.order.field		= sorted.field;
				filtersMEO.order.direction	= sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = grid.getView().findCellIndex(e.getTarget()); 
				if(cell !== 0 && cell !== 3){
					Ext.fly(grid.getView().getRow(rowIndex)).removeClass('notSeenMailClass');
					var record = grid.getStore().getAt(rowIndex);
					
					viewMailDetail(record.get('idmail'),record.get('userid'),record.get('ap'),tabsFollowEmails);
				}
			}
		},
		sm: smmyfollowmailOutbox,
		frame:false,
		loadMask:true,
		border: false
	});
	
	var OutboxTabBar = new Ext.Toolbar({
		id: 'followOutboxTbar',
		items:[
			new Ext.Button({
				tooltip: 'Delete Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyfollowmailOutbox.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the emails to be eliminated.'); return false;
					}
					loading_win.show();
					
					var pids=selected_datamyfollowmailOutbox[0];
					for(i=1; i<selected_datamyfollowmailOutbox.length; i++)
						pids+=','+selected_datamyfollowmailOutbox[i]; 

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'delete',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							selected_datamyfollowmailOutbox 	= new Array();
							storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
							Ext.Msg.alert("My email - Outbox", 'Emails deleted.');
							
						}                                
					});
				}
			}),
			new Ext.Button({
				tooltip: 'Filter Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					
					var formmyfollowmailOutbox = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						id: 'formmyfollowmailOutbox',
						name: 'formmyfollowmailOutbox',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:2},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'femail',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'To mail',
									name		  : 'femail',
									value		  : filtersMEO.email.to.mail,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMEO.email.to.mail = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcontent',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Email Content',
									name		  : 'fcontent',
									value		  : filtersMEO.email.content,
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersMEO.email.content = newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'ftype',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Email Type',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											[-1,'All'],
											[1,'SMS'],
											[3,'Fax'],
											[5,'Email'],
											[15,'Voice Mail']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'etypename',
									value         : filtersMEO.email.type,
									hiddenName    : 'etype',
									hiddenValue   : filtersMEO.email.type,
									allowBlank    : false,
									width		  : 150,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.email.type = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fattach',
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Attachment',
									triggerAction : 'all',
									width		  : 150,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fattachname',
									value         : filtersMEO.email.attach,
									hiddenName    : 'fattach',
									hiddenValue   : filtersMEO.email.attach,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.email.attach = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fpendingproperty',
								colspan	: 2,
								items 	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Pending Property',
									triggerAction : 'all',
									width		  : 150,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Yes'],
											['1','No']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpendingpropertyname',
									value         : filtersMEO.pending.property,
									hiddenName    : 'fpendingproperty',
									hiddenValue   : filtersMEO.pending.property,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersMEO.pending.property = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fdate',
								width	: 600,
								colspan	: 2,
								items	: [{
									xtype: 'compositefield',
									labelWidth: 120,
									fieldLabel: 'Email Date',
									items:[{
										width: 100,
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										forceSelection: true,
										editable: false,
										name: 'fdatecname',
										hiddenName: 'fdatec',
										displayField: 'id',
										valueField: 'id',
										store: new Ext.data.ArrayStore({
											fields: ['id'],
											data  : [
												['Equal'],
												['Greater Than'],
												['Less Than'],
												['Equal or Less'],
												['Equal or Greater'],
												['Between']
											]
										}),
										value: filtersMEO.email.dates.type,
										listeners: {
											'select': function (combo,record,index){
												filtersMEO.email.dates.type = record.get('id');
												var secondfield = Ext.getCmp('fdatea');
												secondfield.setValue('');
												filtersMEO.email.dates.after='';
												
												if(filtersMEO.email.dates.type=='Between')
													secondfield.setVisible(true);
												else
													secondfield.setVisible(false);	
													
												Ext.getCmp('fdate').doLayout();
											}
										}
									},{
											xtype		  : 'datefield',
											width		  : 90,
											editable	  : false,
											format		  : 'm/d/Y',
											name		  : 'fdateb',
											value		  : filtersMEO.email.dates.before,
											listeners	  : {
												'change'  : function(field,newvalue,oldvalue){
													filtersMEO.email.dates.before=newvalue;
												}
											}
									},{
										xtype		  : 'datefield',
										width		  : 90,
										editable	  : false,
										format		  : 'm/d/Y',
										id			  : 'fdatea',
										name		  : 'fdatea',
										hidden		  : filtersMEO.email.dates.type!='Between',
										value		  : filtersMEO.email.dates.after,
										listeners	  : {
											'change'  : function(field,newvalue,oldvalue){
												filtersMEO.email.dates.after=newvalue;
											}
										}
									}]
								}]
							}]	
						}],
						buttons:[
							{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(b){
									storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
									b.findParentByType('window').close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(b){
									filtersMEO.email.to.mail 		= '';
									filtersMEO.email.content 		= '';
									filtersMEO.email.dates.after 	= '';
									filtersMEO.email.dates.before	= '';
									filtersMEO.email.dates.type		= 'Equal';
									filtersMEO.email.attach			= -1;
									filtersMEO.email.type			= -1;
									filtersMEO.pending.property		= -1;
									
									b.findParentByType('form').getForm().reset();
									
									storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
									b.findParentByType('window').close();
								}
							},{
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Close&nbsp;&nbsp; ',
								handler  	  : function(b){
									b.findParentByType('window').close();
								}
							}
						]
					});
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 650,
						height      : 280,
						modal	 	: true,  
						plain       : true,
						items		: formmyfollowmailOutbox,
						closeAction : 'close'
					});
					win.show();
				}
			}),
			new Ext.Button({
				tooltip: 'Reset Filter Emails',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersMEO.email.to.mail 		= '';
					filtersMEO.email.content 		= '';
					filtersMEO.email.dates.after 	= '';
					filtersMEO.email.dates.before	= '';
					filtersMEO.email.dates.type		= 'Equal';
					filtersMEO.email.attach			= -1;
					filtersMEO.email.type			= -1;
					filtersMEO.pending.property		= -1;
					
					storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox}});
				}
			}),
			new Ext.Button({
				tooltip: 'Compose Email',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/composeEmail.png',
				handler: function(){
					mailCompose(<?php echo $userid;?>,-1,'-1',false,false,'','');
				}
			})
		]
	});
	Ext.getCmp('followOutbox').add(OutboxTabBar);
	Ext.getCmp('followOutbox').doLayout();
	
		
	storemyfollowmailOutbox.load({params:{start:0, limit:limitmyfollowmailOutbox, currentMail:currentMailOutbox, totalMail:totalMailOutbox,checkmail:sincMailOutbox}});	
});
</script>