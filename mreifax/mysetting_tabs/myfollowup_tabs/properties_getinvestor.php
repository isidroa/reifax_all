<?php
/**
 * getInvestorOffer.php
 *
 * Return the offer value of investors.
 *
 * @autor   Guillermo Vera <guilleverag@gmail.com>
 * @version 28.06.2011
 */  

set_include_path(get_include_path() . PATH_SEPARATOR .'C:\\inetpub\\wwwroot'. PATH_SEPARATOR);
include("properties_conexion.php");

	conectar();
	
	$pids    = $_POST['pids'];
	$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	
	//Verificación de los DREIAPRO
	$dreiapro = false;
	$query='SELECT * FROM xima.ximausrs WHERE userid='.$userid.' AND (procode="DREIAPRO" OR executive=777 OR executive=3456 OR executive=3640)';
	$result=mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($result)>0) $dreiapro=true;
	
	//Actualizacion de los defaults
	$query = "UPDATE xima.xima_system_var
	SET investor_offer_factor=".$_GET['factor'].", investor_offer_round=".$_GET['roundTo'].", 
	investor_offer_type=".$_GET['lowMedian']."
	WHERE userid=".$userid;
	mysql_query($query);
	
	/*$arrayPids=explode(',',$pids);
	print_r($arrayPids);*/
	
	$q1="select * from xima.followup where userid=$userid and parcelid in (".$_POST['pids'].")";
	$res1 = mysql_query($q1) or die($q1.mysql_error());
	while($r1=mysql_fetch_array($res1)){
		$db=$r1['bd'];
		$pid=$r1['parcelid'];
		$lprice=(floatval(str_replace(',','',$r1['lprice']))>0) ? floatval(str_replace(',','',$r1['lprice'])) : 999999999;
		
		$_GET['db']=$db;
		if(is_numeric($db)) {
			$db         = conectarPorIdCounty($_GET['db']); 
			$_GET['db'] = $db; 
		} else { 
			$db         = conectarPorNameCounty($_GET['db']); 
			$db         = explode('^',$db); 
			$db         = $db[0]; 
			$_GET['db'] = $db;
		}
		$_GET['pid']=$pid;
		$q      = "SELECT IF(r.xcode is null, p.xcode, r.xcode) as xcode, m.lprice, m.status 
					FROM psummary p
					LEFT JOIN rtmaster r ON (p.parcelid=r.parcelid) 
					LEFT JOIN mlsresidential m on (p.parcelid=m.parcelid)
					WHERE p.parcelid='".$_GET['pid']."'";
				
		$result = mysql_query($q) or die($q.mysql_error());   
		$res    = mysql_fetch_array($result);
		
		$_POST["prop"]=$res['xcode'];
		$_POST['pid']=$_GET['pid'];
		$_POST['bd']=$_GET['db'];
		
		$roundTo=$_GET['roundTo'];
		$factor=$_GET['factor'];
		$investOpc = $_GET['lowMedian'] == 1 ? 'L' : 'M';
		
		$_POST['no_include']='true';
		$_POST['type']='invest';
		$_POST['id']=$_GET['pid'];
		$_POST['status']='CS,CC';
		$_POST['reset']='true';
		$_POST['array_taken']='';
		$_POST['userid']=$_COOKIE['datos_usr']['USERID'];
		$_POST['sort']='saleprice';
		$_POST['dir']='ASC';
		
		ob_start();
		$Filas='';
		include('properties_look4comparables.php');
		$content = ob_get_contents();
		ob_end_clean();
		
		$_arrResult=explode("^",$content);
		
		if(trim($_arrResult[0])!='ERROR')	
			$vFilas   = json_decode("[".trim($_arrResult[0])."]");
		else
			$vFilas   ='';
			
			
		$_POST["comparables"]='';
		foreach($vFilas as $k => $val){
			if($investOpc=='M' || ($investOpc=='L' && $k<4)){
				if($_POST["comparables"]!='') $_POST["comparables"].=',';
				$_POST["comparables"].='"'.$val->id.'"';
			}
		}
		
		$_POST['status']='CC';
		ob_start();
		include('properties_calculate.php');
		$content = ob_get_contents();
		ob_end_clean();
		
		
		$calculate=explode("^",$content);
		$marketvalue = $calculate[0];
		
		
		$_POST['status']='A';
		$_POST['no_func']='true';
		$_POST['array_taken']='';
		$_POST['sort']='lprice';
		if($investOpc=='L'){
			$_POST['start']=0;
			$_POST['limit']=4;
		}
		
		ob_start();
		$Filas='';
		include('properties_look4comparables.php');
		$content = ob_get_contents();
		ob_end_clean();
				
		$_arrResult=explode("^",$content);
		if(trim($_arrResult[0])!='ERROR')	
		
			$vFilas   = json_decode("[".trim($_arrResult[0])."]");
		else
			$vFilas   ='';
			
		$_POST["comparables"]='';
		foreach($vFilas as $k => $val){
			if($investOpc=='M' || ($investOpc=='L' && $k<4)){
				if($_POST["comparables"]!='') $_POST["comparables"].=',';
				$_POST["comparables"].='"'.$val->id.'"';
			}
		}
		
		ob_start();
		include('properties_calculate.php');
		$content = ob_get_contents();
		ob_end_clean();
		
		$calculate=explode("^",$content);
		$activevalue = $calculate[0];
		
		//echo $lprice.' - '.$marketvalue.' - '.$activevalue;
		
		$marketvalue=floatval(str_replace(',','',$marketvalue));
		$activevalue=floatval(str_replace(',','',$activevalue));
		
		$selected=$lprice;
		if($marketvalue > 0 && $selected>$marketvalue) $selected=$marketvalue;
		if($activevalue > 0 && $selected>$activevalue) $selected=$activevalue;
		if($dreiapro){
			$r = mt_rand() / mt_getrandmax() * 3.0;
			$plus = mt_rand() / mt_getrandmax();
			
			if($plus < 0.5) $r *= -1.0;

			$factor += $r; 
		}
		$fact = floatval($factor/100);
		$iovalue = (floatval($selected) * floatval($fact)); 
		
		
		
		//echo ' - '.$selected.' - '.$iovalue.' - ';
		
		if($roundTo==500)
			$final = floor($iovalue/1000).'500.00';
		else if($roundTo==10)
			$final = floor($iovalue/10).'0.00';
		else if($roundTo==100)
			$final = floor($iovalue/100).'00.00';
		else 
			$final = (floor($iovalue/1000)+1).'000.00';
		
		//echo $marketvalue.' '.$activevalue.' '.$selected.' ';
		/*$query	= "UPDATE xima.followup SET offer=$final
				WHERE userid=$userid AND parcelid='$pid'";
		mysql_query($query) or die($query.mysql_error());*/
		//$userid_follow = $_COOKIE['datos_usr']['USERID'];
		$userid_follow = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
		$offer 		= $final;
		$coffer 	= 0;
		$task 		= 13;
		$contract 	= 1;
		$pof 		= 1;
		$emd 		= 1;
		$rademdums 	= 1;
		$offerreceived 	= 1;
		$detail 	= 'New calculated offer price';
		
		$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived, detail,userid_follow)
		VALUES ("'.$pid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'",'.$userid.')';
		mysql_query($query) or die($query.mysql_error());
	}
	$resp = array('success'=>'true','exito'=>1,'factor'=>$factor,'iovalue'=>$iovalue,'final'=>$final);
		
	echo json_encode($resp);
?>