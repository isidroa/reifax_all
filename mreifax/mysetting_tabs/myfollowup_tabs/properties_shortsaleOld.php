<?php  
	include('../../properties_conexion.php');
	include "../../simple_html_dom.php"; 
	$dbConnect = conectar();
	$county = $_POST['county'];
	$pid = $_POST['pid'];
	$typeFollow 	= isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
	
	function historySS($type,$id_shortsale,$ids_required,$status_require,$task,$cancelText=''){
		//Historico de la propiedad///////////////////
		$query="SELECT sf.parcelid, sf.userid, a.agent as processor, sp.packageName, concat(x.name,' ',x.surname) as user   
		FROM xima.shortsale_followup sf 
		INNER JOIN xima.followagent a ON (sf.id_processor=a.id_processor AND sf.userid=a.userid)
		INNER JOIN xima.shortsale_packages sp ON (sf.id_packages=sp.id_packages)
		INNER JOIN xima.ximausrs x ON (sf.userid=x.userid) 
		WHERE sf.id_shortsale=$id_shortsale";
		$result = mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
		$r = mysql_fetch_assoc($result);
		
		$pid=$r['parcelid'];
		$userid=$r['userid'];
		$userid_follow = $_COOKIE['datos_usr']['USERID'];
		$offer 		= 0;
		$coffer 	= 0;
		$task 		= $task;
		$contract 	= 1;
		$pof 		= 1;
		$emd 		= 1;
		$rademdums 	= 1;
		$offerreceived 	= 1;
		
		if($type != 'Status Change'){
			$detail 	= 'Docs <b>'.$type.'</b> by <b>'.($task == 19 ? $r['processor'] : $r['user']).'</b> for Package <b>'.$r['packageName'].'</b>:<br>';
			
			$query = "SELECT b.displayName  
			FROM xima.shortsale_followup_status s
			INNER JOIN xima.shortsale_package_require a ON (a.id_require=s.id_require AND a.userid=$userid)
			INNER JOIN xima.shortsale_require b ON a.id_requireTemp=b.id_require
			WHERE s.id_require IN (".implode(',',$ids_required).") and s.id_shortsale=$id_shortsale
			AND s.status = $status_require 
			ORDER BY b.ordShow";
			$result	=	mysql_query($query) or die($query.mysql_error());
			
			while($row=mysql_fetch_assoc($result)){
				$detail.= '<b>'.$row['displayName'].'</b><br>';
			}
		}else{
			$detail 	= 'Negotiator <b>'.$r['processor'].'</b> for Package <b>'.$r['packageName'].'</b> Changed Short Sale Status to:<br><b>'.$status_require.'</b>';
		}
		
		if($cancelText!=''){
			$detail.='<br><b>Rejected Explain:</b><br> '.$cancelText;
		}
					
		$query='INSERT INTO xima.followup_history (parcelid,userid,odate,offer,coffer,task,contract,pof,emd,realtorsadem,offerreceived, detail,userid_follow,follow_type)
		VALUES ("'.$pid.'",'.$userid.',NOW(),'.$offer.','.$coffer.', "'.$task.'",'.$contract.','.$pof.','.$emd.','.$rademdums.','.$offerreceived.',"'.$detail.'",'.$userid_follow.',"SS")';
		mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
		/////////////////////////////////////////////
	}
	
	if(isset($_POST['type'])){
		if($_POST['type']=='insert'){
			$processor = $_POST['processor'];
			$package = $_POST['package'];
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$pid = $_POST['parcelid'];
			
			$query="SELECT * FROM xima.shortsale_followup 
			WHERE userid=$userid AND parcelid='$pid'  
			AND id_processor=$processor AND id_packages=$package";
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				echo '{success: true, existe: true}';	
				return true;
			}else{			
				$query="INSERT INTO xima.shortsale_followup (id_processor,id_packages,parcelid,userid,bd,address,unit) 
				SELECT $processor,$package,'$pid',$userid,bd,address,unit  
				FROM xima.followup WHERE userid=$userid and parcelid='$pid' 
				LIMIT 1";
				mysql_query($query) or die($query.mysql_error());
				$id_shortsale = mysql_insert_id();
				
				$query="INSERT INTO xima.shortsale_followup_status (id_shortsale,id_require,status) 
				SELECT $id_shortsale,`id_require`,`require` 
				FROM xima.shortsale_package_require 
				WHERE id_packages=$package AND userid=$userid";
				mysql_query($query) or die($query.mysql_error());
				
				$query="SELECT agentid, agent 
				FROM xima.followagent
				WHERE userid=$userid AND id_processor=$processor
				LIMIT 1";
				$result = mysql_query($query) or die($query.mysql_error());
				$r = mysql_fetch_assoc($result);
				
				$query="INSERT INTO xima.follow_assignment (parcelid,userid,agentid,principal) 
				Values ('$pid', $userid, $r[agentid], 0)";
				mysql_query($query) or die($query.mysql_error());
				
				$query="UPDATE xima.followup 
				SET agent = '$r[agent]' 
				WHERE userid=$userid AND parcelid='$pid'";
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo '{success: true, existe: false}';
		}elseif($_POST['type']=='delete'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$query='DELETE FROM xima.shortsale_followup WHERE userid='.$userid.' AND id_shortsale IN ('.$_POST['ids'].')';
			mysql_query($query) or die($query.mysql_error());
			
			echo '{success: true}';
		}elseif($_POST['type']=='loadRequireStatus'){
			$id_packages = $_POST['id_packages'];
			$id_shortsale = $_POST['id_shortsale'];
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$query = "SELECT a.id_require,b.gridName,b.displayName,a.descName,a.`require`,c.`name`,s.`status`, s.cancelText 
			FROM xima.shortsale_followup_status s
			INNER JOIN xima.shortsale_package_require a ON (a.id_require=s.id_require AND a.userid=$userid)
			INNER JOIN xima.shortsale_require b ON a.id_requireTemp=b.id_require
			INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
			WHERE s.id_shortsale=$id_shortsale AND a.id_packages=$id_packages  
			ORDER BY c.order,b.ordShow";
			$result	=	mysql_query($query) or die($query.mysql_error());
			
			$rows = array();
			while($row=mysql_fetch_assoc($result)){
				$rows[$row['name']][]	=	array(
					"id_require"	=>	$row['id_require'],
					"gridName"		=>	$row['gridName'],
					"displayName"	=>	$row['displayName'],
					"descName"		=>	$row['descName'],
					"require"		=>	$row['require'],
					"status"		=>	$row['status'],
					"cancelText"	=>	$row['cancelText']
				);
			}
			
			echo json_encode(array("success"=>true,"data"=>$rows));
		}elseif($_POST['type']=='loadRequireDocs'){
			$id_shortsale = $_POST['id_shortsale'];
			
			$query="SELECT d.id_docs, d.fileName, d.fileUrl 
			FROM xima.shortsale_followup_docs d
			WHERE d.id_shortsale=$id_shortsale";
			$result	=	mysql_query($query) or die($query.mysql_error());
			
			$rows = array();
			while($row=mysql_fetch_assoc($result)){
				$rows[] = $row;
			}
			
			echo json_encode(array("success"=>true,"total"=>count($rows),"data"=>$rows));
		}elseif($_POST['type']=='getDoc'){
			$id_docs = $_POST['id_doc'];
			
			$query="SELECT d.requires, d.id_shortsale, f.userid  
			FROM xima.shortsale_followup_docs d 
			INNER JOIN xima.shortsale_followup f ON (d.id_shortsale=f.id_shortsale)
			WHERE d.id_docs=$id_docs";
			$result	=	mysql_query($query) or die($query.mysql_error());
			$r = mysql_fetch_assoc($result);
			
			$requires = $r['requires'];
			$id_shortsale = $r['id_shortsale'];
			$userid = $r['userid'];
			
			$query = "SELECT b.gridName,a.descName,s.`status`, s.cancelText 
			FROM xima.shortsale_followup_status s
			INNER JOIN xima.shortsale_package_require a ON (a.id_require=s.id_require AND a.userid=$userid)
			INNER JOIN xima.shortsale_require b ON a.id_requireTemp=b.id_require
			WHERE s.id_require IN ($requires) and s.id_shortsale=$id_shortsale
			ORDER BY b.ordShow";
			$result	=	mysql_query($query) or die($query.mysql_error());
			
			$rows = array();
			while($row=mysql_fetch_assoc($result)){
				$rows[]	=	array(
					"gridName"		=>	$row['gridName'],
					"descName"		=>	$row['descName'],
					"status"		=>	$row['status'],
					"cancelText"	=>	$row['cancelText']
				);
			}
			
			echo json_encode(array("success"=>true,"data"=>$rows));
		}elseif($_POST['type']=='deleteDoc'){
			$id_doc = $_POST['id_doc'];
			
			$query='SELECT id_shortsale,requires,fileSrc 
			FROM xima.shortsale_followup_docs 
			WHERE id_docs='.$id_doc;
			$result	=	mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				$r = mysql_fetch_assoc($result);
				$id_shortsale = $r['id_shortsale'];
				$require = $r['requires'];
				
				$query="UPDATE xima.shortsale_followup_status 
				SET status=1 
				WHERE id_shortsale=$id_shortsale  
				AND id_require IN ($require)
				AND status IN ('2','3','4')";
				mysql_query($query) or die($query.mysql_error());
				
				$path	=	$_SERVER['DOCUMENT_ROOT'].$r['fileSrc'];
				@unlink($path);
				
				$query="DELETE FROM xima.shortsale_followup_docs WHERE id_docs=$id_doc";
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo '{success: true}';
		}elseif($_POST['type']=='acceptDoc'){
			$id_shortsale = $_POST['id_shortsale'];
			$ids_required = array();
			
			foreach($_POST as $k => $r){
				//echo substr($k,0,7);
				if(substr($k,0,7)=='checkSS'){
					$id = explode('-',$k);
					$ids_required[] = $id[1];
				}
			}
			
			if(count($ids_required) == 0)
				die(json_encode(array('success'=>false,'mensaje'=>"You must select at least one required document this in the file to be uploaded.")));
				
			$query = "UPDATE xima.shortsale_followup_status 
			SET status='3' 
			WHERE id_shortsale=$id_shortsale 
			AND id_require IN (".implode(',',$ids_required).")";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
			
			historySS('Accepted',$id_shortsale,$ids_required,"'3'",19);
			
			echo json_encode(array("success"=>true,"mensaje"=>"The document status was changed to Accepted (This status can only be changed for documents that are Uploaded or Rejected)."));
		}elseif($_POST['type']=='requireDoc'){
			$id_shortsale = $_POST['id_shortsale'];
			$ids_required = array();
			
			foreach($_POST as $k => $r){
				//echo substr($k,0,7);
				if(substr($k,0,7)=='checkSS'){
					$id = explode('-',$k);
					$ids_required[] = $id[1];
				}
			}
			
			if(count($ids_required) == 0)
				die(json_encode(array('success'=>false,'mensaje'=>"You must select at least one required document this in the file to be uploaded.")));
				
			$query = "UPDATE xima.shortsale_followup_status 
			SET status='1' 
			WHERE id_shortsale=$id_shortsale 
			AND id_require IN (".implode(',',$ids_required).") 
			AND status='0'";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
			
			echo json_encode(array("success"=>true,"mensaje"=>"The document status was changed to Required (This status can only be changed for documents that are Not Required)."));
		}elseif($_POST['type']=='notRequireDoc'){
			$id_shortsale = $_POST['id_shortsale'];
			$ids_required = array();
			
			foreach($_POST as $k => $r){
				//echo substr($k,0,7);
				if(substr($k,0,7)=='checkSS'){
					$id = explode('-',$k);
					$ids_required[] = $id[1];
				}
			}
			
			if(count($ids_required) == 0)
				die(json_encode(array('success'=>false,'mensaje'=>"You must select at least one required document this in the file to be uploaded.")));
				
			$query = "UPDATE xima.shortsale_followup_status 
			SET status='0' 
			WHERE id_shortsale=$id_shortsale 
			AND id_require IN (".implode(',',$ids_required).")
			AND status IN ('1','2','4')";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
			
			echo json_encode(array("success"=>true,"mensaje"=>"The document status was changed to Not Required (This status can only be changed for documents that are Required, Uploaded or Rejected)."));
		}elseif($_POST['type']=='cancelDoc'){
			$id_shortsale = $_POST['id_shortsale'];
			$cancelText = isset($_POST['cancelText']) ? $_POST['cancelText'] : '';
			$ids_required = array();
			
			foreach($_POST as $k => $r){
				//echo substr($k,0,7);
				if(substr($k,0,7)=='checkSS'){
					$id = explode('-',$k);
					$ids_required[] = $id[1];
				}
			}
			
			if(count($ids_required) == 0)
				die(json_encode(array('success'=>false,'mensaje'=>"You must select at least one required document this in the file to be uploaded.")));
				
			$query = "UPDATE xima.shortsale_followup_status 
			SET status='4', cancelText='$cancelText' 
			WHERE id_shortsale=$id_shortsale 
			AND id_require IN (".implode(',',$ids_required).") 
			AND status IN ('2','3')";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
			
			historySS('Rejected',$id_shortsale,$ids_required,"'4'",19,$cancelText);
			
			echo json_encode(array("success"=>true,"mensaje"=>"The document status was changed to Rejected (This status can only be changed for documents that are Uploaded or Accepted)."));
		}elseif($_POST['type']=='changeStatus'){
			$id_shortsale = $_POST['id_shortsale'];
			$ss_status = $_POST['ss_status'];
							
			$query = "UPDATE xima.shortsale_followup 
			SET ss_status='$ss_status' 
			WHERE id_shortsale=$id_shortsale";
			mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
			
			historySS('Status Change',$id_shortsale,array(),$ss_status,19);
			
			echo json_encode(array("success"=>true,"mensaje"=>"The Short Sale Status changed sucessfully."));
		}elseif($_POST['type']=='uploadDoc'){
			$userid = isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$id_shortsale = $_POST['id_shortsale'];
			$task = isset($_POST['task']) ? $_POST['task'] : 18;
			$ids_required = array();
			
			foreach($_POST as $k => $r){
				//echo substr($k,0,7);
				if(substr($k,0,7)=='checkSS'){
					$id = explode('-',$k);
					$ids_required[] = $id[1];
				}
			}
			
			if(count($ids_required) == 0)
				die(json_encode(array('success'=>false,'mensaje'=>"You must select at least one required document this in the file to be uploaded.")));
			$path	=	$_SERVER['DOCUMENT_ROOT']."/shortSale";
			
			if(!is_dir($path."/$userid/"))
				@mkdir($path."/$userid/");
			
			if(!is_dir($path."/$userid/$id_shortsale/"))
				@mkdir($path."/$userid/$id_shortsale/");
				
			$_POST['filename']	=	trim($_POST['filename']);
			$query	=	"SELECT * FROM xima.shortsale_followup_docs WHERE fileName='$_POST[filename]' AND id_shortsale=$id_shortsale";
			mysql_query($query);
			if(mysql_affected_rows()<=0){
				$filename  = str_replace(' ', '_', $_FILES["pdfupload"]['name']);
				$extension = strtolower(strrchr($filename,'.'));
				
				if($extension != '.pdf')
					die(json_encode(array('success'=>false,'mensaje'=>"The uploaded file its not a valid PDF")));
					
				$newFileName = "$id_shortsale-$userid-".date("YmdHis")."-$filename";
				
				if (!move_uploaded_file($_FILES["pdfupload"]['tmp_name'], $path."/$userid/$id_shortsale/".$newFileName))
					die(json_encode(array('success'=>false,'mensaje'=>"Fallo al copiar. $newFileName")));
				else {
					copy($path."/$userid/$id_shortsale/".$newFileName, $path."/temp/".$newFileName);
					unlink($path."/$userid/$id_shortsale/".$newFileName);
					copy($path."/temp/".$newFileName, $path."/$userid/$id_shortsale/".$newFileName);
					unlink($path."/temp/".$newFileName);				
					
					$id_name = date("Hmiu"); ////Validador para el Nombre temporal para que nunca choquen los usuarios
					
					system("C:/inetpub/wwwroot/mysetting_tabs/mycontracts_tabs/gs9.05/bin/gswin64c.exe -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"$path/$userid/temp_by_jesus$id_name.pdf\" -c .setpdfwrite -f \"$path/$userid/$id_shortsale/$newFileName\"", $valid);
					
					if($valid == 0){ ////Si se creo Correctamente el pdf nuevo
						@unlink($path."/$userid/$id_shortsale/".$newFileName);
						rename($path."/$userid/temp_by_jesus".$id_name.'.pdf', $path."/$userid/$id_shortsale/".$newFileName);
						$query	=	"
							INSERT INTO xima.shortsale_followup_docs 
								(id_shortsale, fileName, fileUrl, fileSrc, requires) 
							VALUES 
								($id_shortsale, '$_POST[filename]', 'http://$_SERVER[SERVER_NAME]/shortsale/$userid/$id_shortsale/$newFileName', '/shortsale/$userid/$id_shortsale/$newFileName', '".implode(',',$ids_required)."')";
						$result = mysql_query($query) or die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
						$id_docs = mysql_insert_id();

						$query = "UPDATE xima.shortsale_followup_status 
						SET status='2' 
						WHERE id_shortsale=$id_shortsale AND id_require IN (".implode(',',$ids_required).") 
						AND status IN ('1','2','4')";
						if(!mysql_query($query)){
							@unlink($path."/$userid/$id_shortsale/".$newFileName);
							$query	=	"
							DELETE FROM xima.shortsale_followup_docs 
							WHERE id_docs=$id_docs";
							mysql_query($query);
							die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));	
						}
						
						historySS('Uploaded',$id_shortsale,$ids_required,"'2'",$task);
						
						echo json_encode(array("success"=>true,"mensaje"=>"Document uploaded successfully."));

					}else{
						@unlink($path."/$userid/$id_shortsale/".$newFileName);
						@unlink($path."/$userid/temp_by_jesus".$id_name.'.pdf');
						
						die(json_encode(array("success"=>false,"mensaje"=>"Please try again later. Error: ". __LINE__,"error"=>$query." -- ".mysql_error())));
					}
					@unlink($_FILES["pdfupload"]['tmp_name']);
				}
			}else
				die(json_encode(array("success"=>false,"mensaje"=>"Please delete or try with another document name, $_POST[filename], already exist for this Short Sale.")));
		}elseif($_POST['type']=='load-filters'){
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			
			$query="SELECT filter FROM shortsale_filter WHERE userid=".$userid;
			
			$result=mysql_query($query) or die($query.mysql_error());
			
			if ($result) {
				$row  = mysql_fetch_array($result);
				$row = json_decode($row['filter']);
				
				$total=1;
				$resp = array('success'=>true,'total'=>$total,'data'=>$row);	
			}else
				$resp = array('success'=>true, 'total'=> 0, 'mensaje'=>mysql_error());
			
			echo json_encode($resp);
		}elseif($_POST['type']=='save-filters'){
			$userid 	= isset($_POST['userid']) 	? $_POST['userid'] 		: $_COOKIE['datos_usr']['USERID'];
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$county 	= isset($_POST['county']) 	? $_POST['county'] 		: 'ALL';
			$package 	= isset($_POST['package']) 	? $_POST['package'] 	: 'ALL';
			$user 		= isset($_POST['user']) 	? $_POST['user'] 		: 'ALL';
			$processor 	= isset($_POST['processor'])? $_POST['processor'] 	: 'ALL';
			
			$filter = array('address'=>$address, 
			'county'=>$county,
			'status'=>$status,
			'package'=>$package,
			'user'=>$user,
			'processor'=>$processor);
			
			$query = "SELECT b.gridName
			FROM xima.shortsale_require b 
			INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
			WHERE b.status=1 
			ORDER BY c.order,b.ordShow";
			$result = mysql_query($query);
			
			while($r=mysql_fetch_assoc($result)){
				$filter['s'.$r['gridName']]=(isset($_POST['s'.$r['gridName']]) ? $_POST['s'.$r['gridName']] : '-1');
			}
						  
			$query='SELECT * FROM xima.shortsale_filter WHERE userid='.$userid;
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)==0){
				$query='insert into xima.shortsale_filter (userid,filter) VALUES ('.$userid.',\''.json_encode($filter).'\')';
				mysql_query($query) or die($query);
			}else{
				$query="UPDATE xima.shortsale_filter SET filter='".json_encode($filter)."' WHERE userid=".$userid;
				mysql_query($query) or die($query.mysql_error());
			}
			
			echo '{success: true}';
		}elseif($_POST['type']=='load-processor'){
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$ALL = isset($_POST['ALL']) ? true : false;
			$SELECT = isset($_POST['SELECT']) ? true : false;
			
			$q="SELECT distinct p.id_processor,a.agent 
			FROM xima.shortsale_processor_user p
			INNER JOIN xima.followagent a ON (p.userid=a.userid AND p.id_processor=a.id_processor)
			WHERE p.userid=$userid
			ORDER BY a.agent";
			$result=mysql_query($q) or die($q.mysql_error());
			
			$data = array();

			if($ALL){
				$row['valor']='ALL';
				$row['texto']='ALL';
				$data [] = $row;
			}
			if($SELECT){
				$row['valor']='-1';
				$row['texto']='SELECT';
				$data [] = $row;
			}
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC)){
				$row2['valor']=$row['id_processor'];
				$row2['texto']=$row['agent'];
				$data [] = $row2;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='load-packages-make'){
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$ALL = isset($_POST['ALL']) ? true : false;
			$SELECT = isset($_POST['SELECT']) ? true : false;
			$id_processor = $_POST['processor'];
			
			$q="SELECT distinct p.id_packages,p.packageName 
			FROM xima.shortsale_packages p 
			WHERE (p.userid is null or p.userid=$userid) 
			AND p.id_processor=$id_processor   
			AND p.status=1 
			ORDER BY p.packageName";
			$result=mysql_query($q) or die($q.mysql_error());
			
			$data = array();

			if($ALL){
				$row['valor']='ALL';
				$row['texto']='ALL';
				$data [] = $row;
			}
			if($SELECT){
				$row['valor']='-1';
				$row['texto']='SELECT';
				$data [] = $row;
			}
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC)){
				$row2['valor']=$row['id_packages'];
				$row2['texto']=$row['packageName'];
				$data [] = $row2;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='load-packages'){
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$ALL = isset($_POST['ALL']) ? true : false;
			$SELECT = isset($_POST['SELECT']) ? true : false;
			$id_processor = $_POST['processor'];
			
			if($id_processor == 'ALL'){
				$q="SELECT distinct p.id_packages,p.packageName 
				FROM xima.shortsale_followup f
				INNER JOIN xima.shortsale_packages p ON (f.id_packages=p.id_packages)
				WHERE f.userid=$userid  
				AND p.status=1 
				ORDER BY p.packageName";
			}else{
				$q="SELECT distinct p.id_packages,p.packageName 
				FROM xima.shortsale_followup f
				INNER JOIN xima.shortsale_packages p ON (f.id_packages=p.id_packages)
				WHERE f.id_processor=$id_processor  
				AND p.status=1 
				ORDER BY p.packageName";
			}
			$result=mysql_query($q) or die($q.mysql_error());
			
			$data = array();

			if($ALL){
				$row['valor']='ALL';
				$row['texto']='ALL';
				$data [] = $row;
			}
			if($SELECT){
				$row['valor']='-1';
				$row['texto']='SELECT';
				$data [] = $row;
			}
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC)){
				$row2['valor']=$row['id_packages'];
				$row2['texto']=$row['packageName'];
				$data [] = $row2;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='load-users'){
			$ALL = isset($_POST['ALL']) ? true : false;
			$SELECT = isset($_POST['SELECT']) ? true : false;
			$idProcessor = $_POST['idProcessor'];
			
			$q="SELECT distinct f.userid, concat(x.name,' ',x.surname) as user 
			FROM xima.shortsale_followup f
			INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
			WHERE f.id_processor=$idProcessor 
			ORDER BY concat(x.name,' ',x.surname)";
			$result=mysql_query($q) or die($q.mysql_error());
			
			$data = array();

			if($ALL){
				$row['valor']='ALL';
				$row['texto']='ALL';
				$data [] = $row;
			}
			if($SELECT){
				$row['valor']='-1';
				$row['texto']='SELECT';
				$data [] = $row;
			}
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC)){
				$row2['valor']=$row['userid'];
				$row2['texto']=$row['user'];
				$data [] = $row2;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='load-countys'){
			$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
			$idProcessor= isset($_POST['id_processor']) ? $_POST['id_processor'] : -1;
			
			if($idProcessor != -1)
				$q="SELECT distinct(bd) as county FROM shortsale_followup f where id_processor=$idProcessor";
			else
				$q="SELECT distinct(bd) as county FROM shortsale_followup f where userid=$userid";
			$result=mysql_query($q) or die($q.mysql_error());
			
			$data = array();
			
			$row['valor']='ALL';
			$row['texto']='ALL';
			$data [] = $row;
			while ($row=mysql_fetch_array($result,MYSQL_ASSOC)){
				$row['valor']=$row['county'];
				$row['texto']=$row['county'];
				$data [] = $row;
			}
			echo '{success: true, total: '.count($data).', results:'.json_encode($data).'}';
		}elseif($_POST['type']=='printShortSale'){
			$id_shortsale = $_POST['id_shortsale'];
			
			$query='SELECT d.fileSrc 
			FROM xima.shortsale_followup_docs d
			WHERE d.id_shortsale='.$id_shortsale;
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				$pdfs = array();
				while($r=mysql_fetch_assoc($result)){
					$pdfs[] = 'C:/inetpub/wwwroot'.$r['fileSrc'];
				}
				
				require_once($_SERVER["DOCUMENT_ROOT"].'/custom_contract/fpdf/fpdf.php');
				require_once($_SERVER["DOCUMENT_ROOT"].'/custom_contract/fpdi/fpdi.php');
				
				$pdf = new FPDI();
				
				foreach($pdfs as $k => $val){
					$pagecount = $pdf->setSourceFile($val);
					
					for($i=1;$i<=$pagecount;$i++){
						$tplidx = $pdf->importPage($i, '/MediaBox');
						$new = $pdf->getTemplateSize($tplidx);
						$pdf->addPage('P', array($new['w'],$new['h'])); // lo dejo asi para que tome el tamaño original del documento
						$pdf->useTemplate($tplidx, 0, 0, $new['w'], $new['h']);
					}
				}
				$archivo='custom_contract/temp/'.$id_shortsale.date('His').'.pdf';
				$pdf->Output('C:/inetpub/wwwroot/'.$archivo, 'F');
				
				echo json_encode(array('success'=>true, 'pdf'=>$archivo));
			}else{
				echo json_encode(array('success'=>false, 'msj'=>'No documents required for selected short sale.'));
			}
		}elseif($_POST['type']=='viewProcesorPackage'){
			$id_shortsale = $_POST['id_shortsale'];
			
			$query='SELECT sp.packageFile  
			FROM xima.shortsale_followup sf
			INNER JOIN xima.shortsale_processor sp ON (sf.id_processor=sp.id_processor)
			WHERE sf.id_shortsale='.$id_shortsale;
			$result = mysql_query($query) or die($query.mysql_error());
			
			if(mysql_num_rows($result)>0){
				$r=mysql_fetch_assoc($result);
				
				if(strlen($r['packageFile'])>10)
					echo json_encode(array('success'=>true, 'pdf'=>'shortSale/'.$r['packageFile']));
				else
					echo json_encode(array('success'=>false, 'msj'=>"This Negotiator doesn´t have a Negotiator Package."));
			}else{
				echo json_encode(array('success'=>false, 'msj'=>"This Negotiator doesn´t have a Negotiator Package."));
			}
		}elseif($_POST['type']=='TaskLoad'){
			$userid 	= $_POST['userid'];
			$address 	= isset($_POST['address']) 	? $_POST['address'] 	: '';
			$mlnumber 	= isset($_POST['mlnumber']) ? $_POST['mlnumber'] 	: '';
			$agent 		= isset($_POST['agent']) 	? $_POST['agent'] 		: '';
			$status 	= isset($_POST['status']) 	? $_POST['status'] 		: 'ALL';
			$statusalt 	= isset($_POST['statusalt']) 	? $_POST['statusalt'] 		: 'ALL';
			$ndate 		= isset($_POST['ndate']) 	? $_POST['ndate'] 		: '';
			$ndateb 	= isset($_POST['ndateb']) 	? $_POST['ndateb'] 		: '';
			$ndatec 	= isset($_POST['ndatec']) 	? $_POST['ndatec'] 		: 'Equal';
			$ntask 		= isset($_POST['ntask']) 	? $_POST['ntask'] 		: '-2';
			$contract 	= isset($_POST['contract']) ? $_POST['contract'] 	: '-1';
			$pof 		= isset($_POST['pof']) 		? $_POST['pof'] 		: '-1';
			$emd 		= isset($_POST['emd']) 		? $_POST['emd'] 		: '-1';
			$offerreceived 		= isset($_POST['offerreceived']) 		? $_POST['offerreceived'] 		: '-1';
			$ademdums 	= isset($_POST['ademdums']) ? $_POST['ademdums'] 	: '-1';
			$msj 		= isset($_POST['msj']) 		? $_POST['msj'] 		: '-1';
			$pt 		= isset($_POST['pt']) 		? $_POST['pt'] 			: '-1';
			$ct 		= isset($_POST['ct']) 		? $_POST['ct'] 			: '-1';
			$lu 		= isset($_POST['lu']) 		? $_POST['lu'] 			: '-1';
			$pe 		= isset($_POST['pe']) 		? $_POST['pe'] 			: '-1';
			$history 	= isset($_POST['history']) 	? $_POST['history'] 	: '-1';
			$zip 		= isset($_POST['zip']) 	? $_POST['zip'] 		: '';
			$followdate 	= isset($_POST['followdate']) 	? $_POST['followdate'] 		: '';
			$followdateb 	= isset($_POST['followdateb']) 	? $_POST['followdateb'] 		: '';
			$followdatec 	= isset($_POST['followdatec']) 	? $_POST['followdatec'] 		: 'Equal';
			$pendingtask = isset($_POST['pendingtask']) ? $_POST['pendingtask'] : '';
			$county 	= isset($_POST['county']) 	? $_POST['county'] 	: 'ALL';
			$city 	= isset($_POST['city']) 	? $_POST['city'] 	: 'ALL';
			$xcode 	= isset($_POST['xcode']) 	? $_POST['xcode'] 	: 'ALL';
			
			if($pendingtask=='yes'){
				$query="SELECT f.parcelid as pid, f.userid, f.mlnumber, f.status, f.bd as county,
				f.address, f.lprice, f.marketvalue, f.activevalue, f.dom, f.unit, f.zip, f.agent,
				f.offer, f.coffer, f.contract, f.pof, f.emd, f.realtorsadem as rademdums, f.msj,
				DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ((f.offer/f.lprice)*100) as offerpercent,
				f.statusalt, f.offerreceived, f.followdate, 
				concat(x.name,' ',x.surname) as name_follow ,
				f.userid_follow, f.pendes, s.odate as ndate, s.task as ntask, s.idfus, s.status as statusschedule
				FROM xima.shortsale_followup ss 
				INNER JOIN xima.followup f ON (ss.parcelid=f.parcelid and ss.userid=f.userid)
				INNER JOIN followup_schedule s ON (ss.parcelid=s.parcelid and ss.userid=s.userid) 
				LEFT JOIN xima.ximausrs x ON (f.userid_follow=x.userid) 
				WHERE f.userid =".$userid; 
			}else{
				$query="SELECT f.parcelid as pid, f.userid, f.mlnumber, f.status, f.bd as county,
				f.address, f.lprice, f.marketvalue, f.activevalue, f.dom, f.unit, f.zip, f.agent,
				f.offer, f.coffer, f.contract, f.pof, f.emd, f.realtorsadem as rademdums, f.msj,
				DATEDIFF(NOW(),f.lasthistorydate) as lasthistorydate, ((f.offer/f.lprice)*100) as offerpercent,
				f.statusalt, f.offerreceived, 
				f.followdate, concat(x.name,' ',x.surname) as name_follow ,
				f.userid_follow, f.pendes, s.odate as ndate, s.task as ntask, s.idfuh
				FROM xima.shortsale_followup ss 
				INNER JOIN xima.followup f ON (ss.parcelid=f.parcelid and ss.userid=f.userid)
				INNER JOIN followup_history s ON (ss.parcelid=s.parcelid and ss.userid=s.userid)
				LEFT JOIN xima.ximausrs x ON (f.userid_follow=x.userid)
				WHERE f.userid =".$userid; 
			}
			
			//filters
			if(trim($address)!='')
				$query.=' AND f.address LIKE \'%'.trim($address).'%\'';
				
			if(trim($mlnumber)!='')
				$query.=' AND f.mlnumber=\''.trim($mlnumber).'\'';
				
			if(trim($agent)!='')
				$query.=' AND f.agent LIKE \'%'.trim($agent).'%\'';
			
			if($status!='ALL')
				$query.=' AND f.status=\''.$status.'\'';
			
			if($statusalt!='ALL')
				$query.=' AND s.code=\''.$statusalt.'\'';
				
			if(trim($ndate)!=''){
				switch($ndatec){
					case 'Equal':
						$query.=' AND date(s.odate)=\''.$ndate.'\'';
					break;
					
					case 'Greater Than':
						$query.=' AND date(s.odate)>\''.$ndate.'\'';
					break;
					
					case 'Less Than':
						$query.=' AND date(s.odate)<\''.$ndate.'\'';
					break;
					
					case 'Equal or Less':
						$query.=' AND date(s.odate)<=\''.$ndate.'\'';
					break;
					
					case 'Equal or Greater':
						$query.=' AND date(s.odate)>=\''.$ndate.'\'';
					break;
			
					case 'Between':
						if(trim($ndateb)!='')
							$query.=' AND date(s.odate) BETWEEN \''.$ndate.'\' AND \''.$ndateb.'\'';
						else
							$query.=' AND date(s.odate)>=\''.$ndate.'\'';
					break;
					
				}
			}
				
			if(trim($ntask)!='-2'){
				if(trim($ntask)!='-1')
					$query.=' AND s.task='.$ntask;
				else
					$query.=' AND s.task<>0';
			}
				
			if($contract!='-1'){
				if($contract!='0')
					$query.=' AND f.contract=1';
				else
					$query.=' AND f.contract<=0';
			}
			
			if($ademdums!='-1')
				$query.=' AND f.realtorsadem='.$ademdums;
			
			if($pof!='-1')
				$query.=' AND f.pof='.$pof;
			
			if($emd!='-1')
				$query.=' AND f.emd='.$emd;
				
			if($offerreceived!='-1')
				$query.=' AND f.offerreceived='.$offerreceived;
				
			if($msj!='-1')
				$query.=' AND f.msj='.$msj;
				
			if($pt!='-1'){
				if($pt=='0')
					$query.=' AND (SELECT count(*) FROM xima.followup_schedule s WHERE s.userid=f.userid and s.parcelid=f.parcelid) > 0';
				else
					$query.=' AND (SELECT count(*) FROM xima.followup_schedule s WHERE s.userid=f.userid and s.parcelid=f.parcelid) <= 0';
			}
			
			if($pe!='-1'){
				if($pe=='0')
					$query.=' AND f.msj > 0';
				else
					$query.=' AND f.msj <= 0';
			}
			
			if($lu!='-1'){
				if($lu=='0')
					$query.=' AND DATEDIFF(NOW(),lasthistorydate) > 0';
				else
					$query.=' AND DATEDIFF(NOW(),lasthistorydate) <= 0';
			}
			
			if($ct!='-1'){
				if($ct=='0')
					$query.=' AND (SELECT count(*) FROM xima.followup_history h WHERE H.userid=f.userid and h.parcelid=f.parcelid) > 0';
				else
					$query.=' AND (SELECT count(*) FROM xima.followup_history h WHERE h.userid=f.userid and h.parcelid=f.parcelid) <= 0';
			}
				
			if(trim($zip)!='')
				$query.=' AND f.zip LIKE \'%'.trim($zip).'%\'';
			
			if(trim($followdate)!=''){
				switch($followdatec){
					case 'Equal':
						$query.=' AND date(f.followdate)=\''.$followdate.'\'';
					break;
					
					case 'Greater Than':
						$query.=' AND date(f.followdate)>\''.$followdate.'\'';
					break;
					
					case 'Less Than':
						$query.=' AND date(f.followdate)<\''.$followdate.'\'';
					break;
					
					case 'Equal or Less':
						$query.=' AND date(f.followdate)<=\''.$followdate.'\'';
					break;
					
					case 'Equal or Greater':
						$query.=' AND date(f.followdate)>=\''.$followdate.'\'';
					break;
			
					case 'Between':
						if(trim($followdateb)!='')
							$query.=' AND date(f.followdate) BETWEEN \''.$followdate.'\' AND \''.$followdateb.'\'';
						else
							$query.=' AND date(f.followdate)>=\''.$followdate.'\'';
					break;
					
				}
			}	
			
			if($county!='ALL')
				$query.=' AND f.bd LIKE \'%'.trim($county).'%\'';
			
			if($city!='ALL')
				$query.=' AND f.city LIKE \'%'.trim($city).'%\'';
			
			if($xcode!='ALL')
				$query.=' AND f.xcode=\''.trim($xcode).'\'';
				
			//orders
			if(isset($_POST['sort'])){
				$add = '';
				if($_POST['sort']=='followdate') $add=', f.address ASC';
				
				$query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'].$add;
			}else $query.=' ORDER BY f.address ASC';
			//echo $query;
			$result=mysql_query($query) or die($query.mysql_error());
			$i=1;
			$vFilas = array();
			while($r=mysql_fetch_object($result))
				$vFilas[]=$r;
								 
			
			if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
			else $vFilas2=$vFilas;
			
			echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
		}
	}else{
		$userid 	= isset($_POST['userid']) 		? $_POST['userid'] 		: $_COOKIE['datos_usr']['USERID'];
		$idProcessor= isset($_POST['idProcessor'])	? $_POST['idProcessor']	: '-1';
		$address 	= isset($_POST['address']) 		? $_POST['address'] 	: '';
		$processor 	= isset($_POST['processor']) 	? $_POST['processor'] 	: 'ALL';
		$user 		= isset($_POST['user']) 		? $_POST['user'] 		: 'ALL';
		$package	= isset($_POST['package']) 		? $_POST['package'] 	: 'ALL';
		$ss_status 	= isset($_POST['ss_status']) 	? $_POST['ss_status'] 	: 'ALL';
		$county 	= isset($_POST['county']) 		? $_POST['county'] 		: 'ALL';
		$pendingtask= isset($_POST['pendingtask']) 	? $_POST['pendingtask'] : '';
		
		$query = "SELECT b.gridName
		FROM xima.shortsale_require b 
		INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
		WHERE b.status=1 
		ORDER BY c.order,b.ordShow";
		$result = mysql_query($query);
		$gridCampos=array();
		
		while($r=mysql_fetch_assoc($result)){
			$gridCampos[$r['gridName']]=(isset($_POST['s'.$r['gridName']]) ? $_POST['s'.$r['gridName']] : '-1');
		}
		
		if($idProcessor!='-1'){
			$query="SELECT f.parcelid as pid, f.userid, f.ss_status, 
			f.bd as county, f.address, f.unit, f.id_shortsale,  
			concat(x.name,' ',x.surname) as usuario, sp.packageName as package, f.id_packages, f.id_processor 
			FROM xima.shortsale_followup f 
			INNER JOIN xima.ximausrs x ON (f.userid=x.userid)
			INNER JOIN xima.shortsale_packages sp ON (f.id_packages=sp.id_packages)
			WHERE f.id_processor =".$idProcessor;
		}else{
			$query="SELECT f.parcelid as pid, f.userid, f.ss_status, f.bd as county, f.address, f.unit, 
			f.id_shortsale, fa.agent as processor, sp.packageName as package, f.id_packages, f.id_processor  
			FROM xima.shortsale_followup f 
			INNER JOIN xima.followagent fa ON (f.userid=fa.userid AND f.id_processor=fa.id_processor)
			INNER JOIN xima.shortsale_packages sp ON (f.id_packages=sp.id_packages)
			LEFT JOIN xima.followup fu ON (f.parcelid=fu.parcelid and f.userid=fu.userid)
			WHERE f.userid =".$userid; 
		}

		//filters
		if(trim($address)!='')
			$query.=' AND f.address LIKE \'%'.trim($address).'%\'';
			
		if(trim($user)!='ALL')
			$query.=' AND f.userid='.$user;
			
		if(trim($processor)!='ALL')
			$query.=' AND f.id_processor='.$processor;
		
		if(trim($package)!='ALL')
			$query.=' AND f.id_packages='.$package;
		
		if($ss_status!='ALL')
			$query.=' AND f.ss_status=\''.$ss_status.'\'';
		
		if($county!='ALL')
			$query.=' AND f.bd LIKE \'%'.trim($county).'%\'';
			
		//orders
		if(isset($_POST['sort'])){			
			$query.=' ORDER BY '.$_POST['sort'].' '.$_POST['dir'];
		}else $query.=' ORDER BY f.address ASC';
		
		//echo $query;
		$result=mysql_query($query) or die($query.mysql_error());
		$i=1;
		$vFilas = array();
		
		while($r=mysql_fetch_array($result,MYSQL_ASSOC)){
			$id_shortsale 	= $r['id_shortsale'];
			$id_packages 	= $r['id_packages'];
			$user			= $r['userid'];

			$query = "SELECT b.gridName, 
			IF(s.id_status IS NOT NULL, s.id_status, -1) id_status, 
			IF(s.status IS NOT NULL, s.status, 0) status
			FROM xima.shortsale_package_require a
			INNER JOIN xima.shortsale_require b ON (a.id_requireTemp=b.id_require)
			INNER JOIN xima.shortsale_section c ON (b.id_section=c.id_section)
			LEFT JOIN xima.shortsale_followup_status s ON (s.id_shortsale=$id_shortsale AND s.id_require=a.id_require)
			WHERE a.id_packages=$id_packages AND a.userid=$user AND b.status=1 
			ORDER BY c.order,b.ordShow";
			//echo $query.'<br><br>';
			$result2 = mysql_query($query) or die($query.mysql_error());
			
			$display = true;
			while($r1=mysql_fetch_array($result2)){
				$r[$r1['gridName']] = $r1['status'];
				if($gridCampos[$r1['gridName']]!= '-1' && $gridCampos[$r1['gridName']] != $r1['status'])
					$display = false;
			}
			
			if($display) $vFilas[]=$r;
		}						
		
		if(isset($_POST['start'])) $vFilas2=array_slice($vFilas,$_POST['start'],$_POST['limit']);
		else $vFilas2=$vFilas;
		
		echo '{success: true, total: '.count($vFilas).', records:'.json_encode($vFilas2).'}';
	}	
?>