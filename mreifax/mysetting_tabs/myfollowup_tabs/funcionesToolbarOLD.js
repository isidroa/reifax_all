function modifyaddons(id,placecontract){
	var idcontract = id;
	var typecontract = "addon";
	if(idcontract && idcontract!=0)
	{
		//Ext.MessageBox.alert('',idcontract);
		new Ext.util.Cookies.set("idmodifycontract", idcontract);
		new Ext.util.Cookies.set("typemodifycontract", typecontract);
		if(typecontract=="addon"){
			new Ext.util.Cookies.set("placemodifycontract", placecontract);
		}

		if(document.getElementById('idmodifycontract')){
			var tab = tabs.getItem('idmodifycontract');
			tabs.remove(tab);
		}

		Ext.getCmp('principal').add({
			//xtype: 'component', 
			title: 'Modify Contract', 
			closable 	: true,
			autoScroll	: true,
			id: 'idmodifycontract', 
			autoLoad:	{url:'/custom_contract/manager.php',scripts:true}
		}).show();
	}
}

function getTemplateById(idtemplate, userid, type, callback){
	Ext.Ajax.request( 
	{  
		waitMsg: 'Checking...',
		url: 'mysetting_tabs/mycontracts_tabs/get_templates.php', 
		method: 'POST',
		timeout :0,
		params: { 
			modo: 'obtener',
			userid: userid,
			id: idtemplate,
			type: type!='email' ? 'docs' : 'email'
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var e = Ext.decode(response.responseText);
			console.log(e.data);
			if(type=='email')
				callback(e.data.body, e.data.subject);
			else
				callback(e.data.body);
		}                                
	});
}

function sendContractJ(selected,grid,store,userid, progressbar_win, progressbar, mailenviados, completetask, selling,callback,values,selectedStatus){
	selling = selling || false;
	var follow = 'B', totalnoschedule = 0;
	if(selling==true){
		follow = 'S';
	}
	var globalVars	=	{};
	$.ajax({
		url:	'/custom_contract/includes/php/functions.php',
		async:true,
		type:	'post',
		data:{
			idfunction:	'getStandarVars'
		},
		dataType:'json',
		success:function(data){
			globalVars	=	data.data;
		}
	});
	Ext.apply(Ext.form.VTypes, {
		moneda : function(v) {
			return /^[\d]*(.[\d]{2})?$/.test(v);
		},
		monedaText: 'Wrong format in the amount.(Example: 1500.00)',
		monedaMask: /^[\d\.]/i
	});
	var contractseal = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			['0','Editable Contract'],
			['1','Sealed Contract']
		]
	});
	// Helper function to show windows for toolbar options
	function showWin(ttle,cmp) {
		win = new Ext.Window({
			title       : ttle,
			minimzable  : false,
			maximizable : false,
			resizable   : false,
			dragable    : false,
			modal       : true,
			id          : 'auxWin',
			hideMode    : 'offsets',
			layout      : 'fit',
			autoDestroy : true,
			autoWidth   : true,
			autoHeight  : true,
			items       : [ cmp ]
		});
		win.show();
	}
	// Add and save and addendum to the DB
	function agregar() {

		addendform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					allowBlank    : false
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						addendform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.msg);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});

		showWin('Add Addendum',addendform);

	}

	// Edit and save and addendum to the DB
	function modificar(sel,name,content) {
		var modifyform = new Ext.FormPanel({
			url           : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php',
			frame         : true,
			monitorValid  : true,
			autoHeight    : true,
			autoWidth     : true,
			formBind      : true,
			buttonAlign   : 'center',
			items         : [
				{
					xtype         : 'textfield',
					id            : 'name',
					name          : 'name',
					fieldLabel    : 'Addendum Name',
					width         : '250px',
					value         : name,
					allowBlank    : false
				},{
					xtype         : 'textarea',
					id            : 'contentt',
					name          : 'contentt',
					fieldLabel    : 'Content',
					width         : '250px',
					height        : '200px',
					value         : content,
					allowBlank    : false
				},{
					xtype         : 'hidden',
					name          : 'id',
					value         : sel
				},{
					xtype         : 'hidden',
					name          : 'cmd',
					value         : 'mod'
				}
			],
			buttons     : [
				{
					text    : 'Save',
					handler : function() {
						modifyform.getForm().submit({
							success : function(form, action) {
								Ext.MessageBox.alert('', action.result.mensaje);
								Ext.getCmp('addGrid').store.reload();
								Ext.getCmp('auxWin').close();
							},
							failure : function(form, action) {
								Ext.MessageBox.alert('Warning', action.result.mensaje);
							}
						});
					}
				},{
					text    : 'Cancel',
					handler : function(){
						Ext.getCmp('auxWin').close();
					}
				}
			]
		});
		showWin('Edit Addendum',modifyform);
	}
	// START OPTIONS FOR THE GRID

	// Reader of the data
	var addReader = new Ext.data.JsonReader(
		{
			successProperty : 'success',
			root            : 'data',
			id              : 'id'
		}, Ext.data.Record.create([
			{name : 'id', type: 'int'},
			{name : 'user', type: 'int'},
			{name : 'name', type: 'string'},
			{name : 'emd', type: 'string'},
			{name : 'pof', type: 'string'},
			{name : 'add', type: 'string'},
			{name : 'active', type: 'string'}
		])
	);

	// Selection model for the checkbox column
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false
	});

	// columns of the grid
	var addColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			{  header: 'Name', dataIndex: 'name' },
			selmode
		]
	});

	// Toolbar definition.
	var toolbar = new Ext.Toolbar({
		items : [
			{
				text    : ' Add ',
				handler : agregar
			},{
				text    : ' Edit ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					if (selId.length > 0) {

						Ext.Ajax.request({
							url     : 'mysetting_tabs/mycontracts_tabs/listaddendum.php?cmd=chk&id='+selId[0].id,
							success : function(r,a) {
								var resp   = Ext.decode(r.responseText);
								modificar(selId[0].id, resp.data[0].name, resp.data[0].content);
							}
						});

					} else
						Ext.MessageBox.alert('','Please select the row to edit.');

				}
			},{
				text    : ' Delete ',
				handler : function() {

					selId = Ext.getCmp('addGrid').selModel.selections.items;

					var ckEliminar = function(btn) {

						if (btn == 'yes') {

							Ext.Ajax.request({
								url     : 'mysetting_tabs/mycontracts_tabs/saveaddendum.php?cmd=del&id='+selId[0].id,
								success : function(r) {
									Ext.MessageBox.alert('',r.responseText);
									Ext.getCmp('addGrid').store.reload();
								}
							});
						}
					}

					if (selId.length > 0)
						Ext.MessageBox.confirm('Confirm','Are you sure?',ckEliminar);
					else
						Ext.MessageBox.alert('','Please select the row to delete.');

				}
			}
		]
	});

	// END OPTIONS FOR THE GRID

	// Definition of the addendum grid
	var gridAdem = new Ext.grid.GridPanel({
		id              : 'addGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : true,
			baseParams	:	{	//Added By Jesus 21/03/2013 for used in followList
				userid	: userid
			},
			url         : 'mysetting_tabs/mycontracts_tabs/listaddendum.php',

			reader      : addReader
		}),
		sm               : selmode,
		cm               : addColumn,
		height           : 150,
		frame            : true,
		tbar             : toolbar,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	
	//Definition of the panel with all the options and the grid
	var addend  = new Ext.Panel({
		xtype         : 'panel',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddend',
		title         : 'Addendum Options',
		width         : 450,
		labelWidth    : 150,
		items         : [ gridAdem ]

	});
	
	// -- Addendum Panel (END)
	
	var selmode = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false,
	});
	
	var addOnColumn = new Ext.grid.ColumnModel({
		defaults : {
			width    : 200,
			sortable : false
		},
		columns: [
			selmode,
			{
				header: 'Name',
				dataIndex: 'name'
			},{
				header: 'E',
				width:30,
				tooltip:'Earnest money Deposit',
				dataIndex: 'emd',
				renderer:function(value, metaData, record, rowIndex, colIndex, store) {
					metaData.attr = 'ext:qtip="Earnest money Deposit"';
					if(value=='1') return '<img src="../../img/drop-yes.gif" />';
					else return '<img src="../../img/drop-no.gif" />';
				}
			},{
				header: 'P',
				width:30,
				tooltip:'Proof of Funds',
				dataIndex: 'pof',
				renderer:function(value, metaData, record, rowIndex, colIndex, store) {
					metaData.attr = 'ext:qtip="Proof of Funds"';
					if(value=='1') return '<img src="../../img/drop-yes.gif" />';
					else return '<img src="../../img/drop-no.gif" />';
				}
			},{
				header: 'A',
				width:30,
				tooltip:'Addendums',
				dataIndex: 'add',
				renderer:function(value, metaData, record, rowIndex, colIndex, store) {
					metaData.attr = 'ext:qtip="Addendums"';
					if(value=='1') return '<img src="../../img/drop-yes.gif" />';
					else return '<img src="../../img/drop-no.gif" />';
				}
			},{
				header: 'Options',
				width:60,
				renderer: function(val,id,r){
					return '<input type="button" value="Update" onClick="modifyaddons(\''+r.json.id+'\',\''+r.json.place+'\');">';
				},
				dataIndex: 'somefieldofyourstore'
			}
		]
	});
	
	// Definition of the addon grid
	var gridAdon = new Ext.grid.GridPanel({
		id              : 'addonGrid',
		store           : new Ext.data.Store({
			autoDestroy : true,
			autoLoad    : false,
			reader      : addReader,
			url         : 'mysetting_tabs/mycontracts_tabs/listaddons.php',
			baseParams	:	{	//Added By Jesus 21/03/2013 for used in followList
				userid	: userid
			},
			listeners   : {
				'load'   : function () {										
					var recs = [];
					Ext.getCmp('addonGrid').getStore().each(function(rec){
						if(rec.data.active == 'true'){
							recs.push(rec);
						}
					});
					selmode.selectRecords(recs);		
				}
			}
		}),
		sm               : selmode,
		cm               : addOnColumn,
		height           : 158,
		frame            : true,
		viewConfig       : {
			emptyText : 'No records'
		}
	});
	

	//Definition of the panel with all the options and the grid
	var addond  = new Ext.Panel({
		xtype         : 'panel',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formAddon',
		title         : 'Additional Documents',
		width         : 450,
		labelWidth    : 150,
		items         : [ gridAdon ]

	});
	
	

	// -- Addon Documents Panel (END)

	// -- Additional Info Panel

	var addinfo = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		layout        : 'form',
		id            : 'formOpt',
		title         : 'Additional Info',
		//collapsible   : true,
		collapsed     : false,
		width         : 450,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'deposit',
				name          : 'deposit',
				fieldLabel    : 'Initial Deposit',
				style         : 'text-align:right',
				allowBlank    : true,
				vtype         : 'moneda'
			},{
				xtype         : 'textfield',
				id            : 'additionalDeposit',
				name          : 'additionalDeposit',
				fieldLabel    : 'Additional Deposit',
				style         : 'text-align:right',
				allowBlank    : true,
				vtype         : 'moneda'
			},{
				xtype         : 'textfield',
				id            : 'inspection',
				name          : 'inspection',
				fieldLabel    : 'Inspection Days',
				style         : 'text-align:right',
				emptyText     : 15,
				allowBlank    : true
			},{
				xtype         : 'datefield',
				id            : 'dateAcc',
				name          : 'dateAcc',
				width         : 100,
				fieldLabel    : 'Acceptance date',
				allowBlank    : true
			},{
				xtype         : 'datefield',
				id            : 'dateClo',
				name          : 'dateClo',
				width         : 100,
				fieldLabel    : 'Closing date',
				allowBlank    : true
			}
		]
	});




	// -- Change buyer

	var buyer  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		layout        : 'form',
		id            : 'formBuy',
		title         : 'Buyer Info',
		width         : 450,
		labelWidth    : 150,
		defaults      : {
			width:200
		},
		items         : [{
			xtype		:	'panel',
			width		:	400,
			layout		:	'table',
			items:[{
				layout		:	'form',
				labelWidth	:	150,
				items		:	[{
					xtype         : 'textfield',
					id            : 'buyername',
					name          : 'buyername',
					fieldLabel    : 'Buyer Name',
					width		  : 200,
					allowBlank    : true
				}]
			},{
				xtype         : 'button',
				tooltip		  : 'Get Contact',
				id			  : 'buyerGetContact',
				scale		  :	'medium',
				icon          : 'http://www.reifax.com/img/ximaicon/user_green.png',
				handler       : function() {
					var ocultar=false;
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						width: 450,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						//defaults: {width: 350},
						labelAlign: 'left',
						items: [{
							xtype     : 'combo',
							hidden: ocultar,
							fieldLabel: 'Contact',
							triggerAction: 'all',
							//forceSelection: true,
							width: 250,
							selectOnFocus: true,
							typeAhead: true,
							typeAheadDelay: 10,
							minChars: 0,
							mode: 'remote',
							store: new Ext.data.JsonStore({
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
								forceSelection: true,
								root: 'records',
								baseParams		: {
									'userid'	:  useridlogin,
									'sort'		: 'agent',
									'dir'		: 'ASC'
								},
								id: 0,
								fields: [
									'agentid',
									'agent'
								]
							}),
							valueField: 'agentid',
							displayField: 'agent',
							name          : 'fagent',
							//value         : r.records[dato].agent, //agent.get('agentype'),
							hiddenName    : 'agent',
							id			  : 'nameagent',
							readOnly	  : ocultar,
							editable	  : !ocultar,
							allowBlank    : false,
							//hiddenValue   : r.records[dato].agentid, //agent.get('agenttype'),
							listeners	  : {
								select: function(combo,record,index){
									var agentid=record.get('agentid');
									//return;
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											userid: useridlogin,
											agentid: agentid
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r1=Ext.decode(response.responseText);
											if(r1.total>0){
												var value=r1.records[0];
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
											}
										}
									});
								},
								change: function(combo, newvalue,oldvalue){
									if(isNaN(newvalue)){
										simple.getForm().findField('agenttype').setValue('Agent');
										simple.getForm().findField('typeemail1').setValue(0);
										simple.getForm().findField('typeemail2').setValue(0);
										simple.getForm().findField('email').setValue('');
										simple.getForm().findField('email2').setValue('');
										simple.getForm().findField('typeaddress1').setValue(0);
										simple.getForm().findField('address1').setValue('');
										simple.getForm().findField('typeaddress2').setValue(0);
										simple.getForm().findField('address2').setValue('');
									}
								}
							}
						},{
							xtype         : 'combo',
							mode          : 'remote',
							fieldLabel    : 'Type',
							triggerAction : 'all',
							readOnly	  : !ocultar,
							width		  : 130,
							editable	  : false,
							store         : new Ext.data.JsonStore({
								id:'storetype',
								root:'results',
								totalProperty:'total',
								autoLoad: true,
								baseParams: {
									type: 'agenttype',
									'userid': useridlogin
								},
								fields:[
									{name:'idtype', type:'string'},
									{name:'name', type:'string'}
								],
								url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
							}),
							displayField  : 'name',
							valueField    : 'idtype',
							name          : 'fagenttype',
							hiddenName    : 'agenttype',
							allowBlank    : false,
							listeners	  : {
								beforequery: function(qe){
									delete qe.combo.lastQuery;
								}
							}
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Email',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyemail1',
								hiddenName    : 'typeemail1',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'email',
								width	  : 165
							}]
						},{

							xtype	  : 'compositefield',
							fieldLabel: 'Email 2',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyemail2',
								hiddenName    : 'typeemail2',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'email2',
								width	  : 165
							}]
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Address 1',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyaddress1',
								hiddenName    : 'typeaddress1',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'address1',
								width	  : 165
							}]
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Address 2',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyaddress2',
								hiddenName    : 'typeaddress2',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'address2',
								width	  : 165
							}]
						}]
					});

					var win = new Ext.Window({
						layout      : 'fit',
						width       : 470,
						height      : 250,
						modal	 	: true,
						plain       : true,
						title		: 'Search Contact',
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Use this',
							handler  : function(){
								Ext.getCmp('buyername').setValue(simple.getForm().findField('agent').getRawValue().toUpperCase());
								Ext.getCmp('baddress1').setValue(simple.getForm().findField('address1').getValue().toUpperCase());
								Ext.getCmp('baddress2').setValue(simple.getForm().findField('address2').getValue().toUpperCase());
								win.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				}
			}]
		},{
			xtype         : 'textfield',
			id            : 'baddress1',
			name          : 'baddress1',
			fieldLabel    : 'Address Line 1',
			allowBlank    : true
		},{
			xtype         : 'textfield',
			id            : 'baddress2',
			name          : 'baddress2',
			fieldLabel    : 'Address Line 2',
			allowBlank    : true
		},{
			xtype         : 'textfield',
			id            : 'baddress3',
			name          : 'baddress3',
			fieldLabel    : 'Address Line 3',
			allowBlank    : true
		}]
	});



	// -- Change seller

	var seller  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		layout        : 'form',
		id            : 'formSel',
		title         : 'Seller Info',
		width         : 450,
		labelWidth    : 150,
		defaults      : {
			width:200
		},
		items         : [{
			xtype		:	'panel',
			width		:	400,
			layout		:	'table',
			items:[{
				layout		:	'form',
				labelWidth    : 150,
				items:[{
					xtype         : 'textfield',
					id            : 'sellname',
					name          : 'sellname',
					fieldLabel    : 'Seller Name',
					width		  : 200,
					allowBlank    : true
				}]
			},{
				xtype         : 'button',
				tooltip		  : 'Get Contact',
				id			  : 'sellerGetContact',
				scale		  :	'medium',
				icon          : 'http://www.reifax.com/img/ximaicon/user_green.png',
				handler       : function() {
					var ocultar=false;
					var simple = new Ext.FormPanel({
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
						frame: true,
						width: 450,
						waitMsgTarget : 'Waiting...',
						labelWidth: 75,
						//defaults: {width: 350},
						labelAlign: 'left',
						items: [{
							xtype     : 'combo',
							hidden: ocultar,
							fieldLabel: 'Contact',
							triggerAction: 'all',
							//forceSelection: true,
							width: 250,
							selectOnFocus: true,
							typeAhead: true,
							typeAheadDelay: 10,
							minChars: 0,
							mode: 'remote',
							store: new Ext.data.JsonStore({
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
								forceSelection: true,
								root: 'records',
								baseParams		: {
									'userid'	:  useridlogin,
									'sort'		: 'agent',
									'dir'		: 'ASC'
								},
								id: 0,
								fields: [
									'agentid',
									'agent'
								]
							}),
							valueField: 'agentid',
							displayField: 'agent',
							name          : 'fagent',
							//value         : r.records[dato].agent, //agent.get('agentype'),
							hiddenName    : 'agent',
							id			  : 'nameagent',
							readOnly	  : ocultar,
							editable	  : !ocultar,
							allowBlank    : false,
							//hiddenValue   : r.records[dato].agentid, //agent.get('agenttype'),
							listeners	  : {
								select: function(combo,record,index){
									var agentid=record.get('agentid');
									//return;
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
										method: 'POST',
										timeout :86400,
										params: {
											userid: useridlogin,
											agentid: agentid
										},

										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r1=Ext.decode(response.responseText);
											if(r1.total>0){
												var value=r1.records[0];
												simple.getForm().findField('agenttype').setValue(value.agenttype);
												simple.getForm().findField('typeemail1').setValue(value.typeemail1);
												simple.getForm().findField('typeemail2').setValue(value.typeemail2);
												simple.getForm().findField('email').setValue(value.email);
												simple.getForm().findField('email2').setValue(value.email2);
												simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
												simple.getForm().findField('address1').setValue(value.address1);
												simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
												simple.getForm().findField('address2').setValue(value.address2);
											}
										}
									});
								},
								change: function(combo, newvalue,oldvalue){
									if(isNaN(newvalue)){
										simple.getForm().findField('agenttype').setValue('Agent');
										simple.getForm().findField('typeemail1').setValue(0);
										simple.getForm().findField('typeemail2').setValue(0);
										simple.getForm().findField('email').setValue('');
										simple.getForm().findField('email2').setValue('');
										simple.getForm().findField('typeaddress1').setValue(0);
										simple.getForm().findField('address1').setValue('');
										simple.getForm().findField('typeaddress2').setValue(0);
										simple.getForm().findField('address2').setValue('');
									}
								}
							}
						},{
							xtype         : 'combo',
							mode          : 'remote',
							fieldLabel    : 'Type',
							triggerAction : 'all',
							readOnly	  : !ocultar,
							width		  : 130,
							editable	  : false,
							store         : new Ext.data.JsonStore({
								id:'storetype',
								root:'results',
								totalProperty:'total',
								autoLoad: true,
								baseParams: {
									type: 'agenttype',
									'userid': useridlogin
								},
								fields:[
									{name:'idtype', type:'string'},
									{name:'name', type:'string'}
								],
								url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
							}),
							displayField  : 'name',
							valueField    : 'idtype',
							name          : 'fagenttype',
							hiddenName    : 'agenttype',
							allowBlank    : false,
							listeners	  : {
								beforequery: function(qe){
									delete qe.combo.lastQuery;
								}
							}
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Email',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyemail1',
								hiddenName    : 'typeemail1',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'email',
								width	  : 165
							}]
						},{

							xtype	  : 'compositefield',
							fieldLabel: 'Email 2',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyemail2',
								hiddenName    : 'typeemail2',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'email2',
								width	  : 165
							}]
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Address 1',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyaddress1',
								hiddenName    : 'typeaddress1',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'address1',
								width	  : 165
							}]
						},{
							xtype	  : 'compositefield',
							fieldLabel: 'Address 2',
							items	  : [{
								xtype         : 'combo',
								mode          : 'local',
								triggerAction : 'all',
								width		  : 120,
								readOnly	  : !ocultar,
								editable	  : false,
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['0','Home'],
										['1','Office']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'tyaddress2',
								hiddenName    : 'typeaddress2',
								allowBlank    : false
							},{
								xtype     : 'textfield',
								name      : 'address2',
								width	  : 165
							}]
						}]
					});

					var win = new Ext.Window({
						layout      : 'fit',
						width       : 470,
						height      : 250,
						modal	 	: true,
						plain       : true,
						title		: 'Search Contact',
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Use this',
							handler  : function(){
								Ext.getCmp('sellname').setValue(simple.getForm().findField('agent').getRawValue().toUpperCase());
								Ext.getCmp('saddress1').setValue(simple.getForm().findField('address1').getValue().toUpperCase());
								Ext.getCmp('saddress2').setValue(simple.getForm().findField('address2').getValue().toUpperCase());
								win.close();
							}
						},{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				}
			}]
		},{
			xtype         : 'textfield',
			id            : 'saddress1',
			name          : 'saddress1',
			fieldLabel    : 'Address Line 1',
			allowBlank    : true
		},{
			xtype         : 'textfield',
			id            : 'saddress2',
			name          : 'saddress2',
			fieldLabel    : 'Address Line 2',
			allowBlank    : true
		},{
			xtype         : 'textfield',
			id            : 'saddress3',
			name          : 'saddress3',
			fieldLabel    : 'Address Line 3',
			allowBlank    : true
		}
		]
	});
	
	var panelSelling = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		hidden        : true,
		layout        : 'form',
		id            : 'panelSelling',
		title         : 'Change Info',
		width         : 410,
		labelWidth    : 150,
		items         : [
			{
				xtype         : 'textfield',
				id            : 'sellingname',
				name          : 'sellingname',
				fieldLabel    : 'Seller Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'sellingaddress',
				name          : 'sellingaddress',
				fieldLabel    : 'Seller Address',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'buyername1',
				name          : 'buyername1',
				fieldLabel    : 'Buyer Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'buyeraddress',
				name          : 'buyeraddress',
				fieldLabel    : 'Buyer Address',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'agentname',
				name          : 'agentname',
				fieldLabel    : 'Agent Name',
				allowBlank    : true
			},{
				xtype         : 'textfield',
				id            : 'brokername',
				name          : 'brokername',
				fieldLabel    : 'Broker Name',
				allowBlank    : true
			}
		]
	});
	var checks  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		bodyStyle     : 'padding : 5px;',
		id            : 'formChecks', 
		width         : 430,
		items         : [
			{
				
				xtype   : 'checkboxgroup',
				columns : 3,
				items   : [
					{   
						id            : 'cbinfo',
						name          : 'cbinfo',
						boxLabel      : 'Change buyer info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('cbinfo').getValue();

								if (ad == true)
									Ext.getCmp('formBuy').show();
								else
									Ext.getCmp('formBuy').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'csinfo',
						name          : 'csinfo',
						boxLabel      : 'Change seller info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('csinfo').getValue();

								if (ad == true)
									Ext.getCmp('formSel').show();
								else
									Ext.getCmp('formSel').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'addons',
						name          : 'addons',
						boxLabel      : 'Additional Documents',
						hidden		  : true,
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('addons').getValue();
								var tp = Ext.getCmp('ctype').getValue();
								

								if (ad == true) {
									Ext.getCmp('addonGrid').store.load({params: {type : tp}});
									Ext.getCmp('formAddon').show();
								} else
									Ext.getCmp('formAddon').hide();

							}
						}
					},{
						xtype         : 'checkbox',
						id            : 'addendum',
						name          : 'addendum',
						boxLabel      : 'Addendum Pages',
						hidden		  : true,
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('addendum').getValue();

								if (ad == true)
									Ext.getCmp('formAddend').show();
								else
									Ext.getCmp('formAddend').hide();

							}
						}
					},
/*					{	//////////Comentado por Jesus ya esta siendo usado//////////
						xtype         : 'checkbox',
						id            : 'scrow',
						name          : 'scrow',
						boxLabel      : 'Escrow Letter',
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
					},*/
					{
						xtype         : 'checkbox',
						id            : 'csellinginfo',
						name          : 'csellinginfo',
						boxLabel      : 'Change info',
						hidden        : true,
						onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
						onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);},
						listeners     : {
							'check'   : function () {

								var ad = Ext.getCmp('csellinginfo').getValue();

								if (ad == true)
									Ext.getCmp('panelSelling').show();
								else
									Ext.getCmp('panelSelling').hide();

							}
						}
					}

				]
			}
		]
	});
	
	var checksDocuments  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		id            : 'formChecksDocuments',
		width         : 450,
		title		  : 'Documents to Show in the Follow Up System',
		//hidden		  : true,	
		//labelWidth	  :	0,
		layout        : 'form',
		items         : [
		{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 2 },
			items        : [
				{
					layout: 'form',
					labelWidth	: 100,
					items:[
						{
							xtype:'checkbox',
							fieldLabel: 'Proof of Funds', 
							name: 'pof'
						}
					]
				},{
					layout: 'form',
					labelWidth	: 140,
					items:[
						{
							xtype:'checkbox',
							fieldLabel: 'Earnest money Deposit', 
							name: 'emd'
						}
					]
				},{
					layout: 'form',
					labelWidth	: 100,
					items:[
						{										
							xtype:'checkbox',
							fieldLabel: 'Addendums', 
							name: 'rademdums'
						}
					]
				}
			]
		}]
	});

	var checksComplete  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		id            : 'checksComplete', 
		width         : 430,
		labelWidth    : 130,
		hidden		  : true,	
		layout        : 'form',
		items         : [
		{
			xtype         : 'checkbox',
			id            : 'completetask1',
			name          : 'completetask1',
			fieldLabel    : 'Completed the Task Selected'
		}]
	});
	var panelTemplateEmails  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		id            : 'paneltemplatesemail', 
		width         : 430,
		labelWidth    : 130,
		hidden		  : true,	
		layout        : 'form',
		items         : [
		{
			xtype         : 'combo',
			name          : 'ccontracttemplate',
			id            : 'ccontracttemplate',
			hiddenName    : 'contracttemplate',
			fieldLabel    : 'Template Email',
			typeAhead     : true,
			autoSelect    : true,
			mode          : 'local',
			store         : new Ext.data.JsonStore({
				root:'results',
				totalProperty:'total',
				autoLoad: true, 
				baseParams: {
					type: 'emailtemplates',
					'userid': userid
				},
				fields:[
					{name:'id', type:'int'},
					{name:'name', type:'string'},
					{name:'default', type:'int'}
				],
				url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
				listeners     : {
					'load'  : function(store, records) {
						var sel=records[0].get('id');
						//Ext.getCmp('ccontracttemplate').setValue(sel); 
						for(i=1;i<records.length;i++){
							if(records[i].get('default')==1)
								sel=records[i].get('id');
						}
						Ext.getCmp('ccontracttemplate').setValue(sel); 
					}
				}
			}),
			triggerAction : 'all', 
			editable      : false,
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value		  : 0,
			width         : 260
		}]
	});
	var panelTemplateFax  = new Ext.Panel({
		xtype         : 'panel',
		border        : false,
		id            : 'paneltemplatesfax', 
		width         : 430,
		labelWidth    : 130,
		hidden		  : true,	
		layout        : 'form',
		items         : [
		{
			xtype         : 'combo',
			name          : 'ccontracttemplatefax',
			id            : 'ccontracttemplatefax',
			hiddenName    : 'contracttemplatefax',
			fieldLabel    : 'Template Fax',
			typeAhead     : true,
			autoSelect    : true,
			mode          : 'local',
			store         : new Ext.data.JsonStore({
				root:'results',
				totalProperty:'total',
				autoLoad: true, 
				baseParams: {
					type: 'docstemplates',
					template_type: 4,
					'userid': userid
				},
				fields:[
					{name:'id', type:'int'},
					{name:'name', type:'string'},
					{name:'default', type:'int'}
				],
				url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
				listeners     : {
					'load'  : function(store, records) {
						loading_win.hide();
						var sel=records[0].get('id');
						//Ext.getCmp('ccontracttemplate').setValue(sel); 
						for(i=1;i<records.length;i++){
							if(records[i].get('default')==1)
								sel=records[i].get('id');
						}
						Ext.getCmp('ccontracttemplatefax').setValue(sel); 
					}
				}
			}),
			triggerAction : 'all', 
			editable      : false,
			selectOnFocus : true,
			allowBlank    : false,
			displayField  : 'name',
			valueField    : 'id',
			value		  : 0,
			width         : 260
		}]
	});
	var fontAndSizeContract  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		id            : 'formFontAndSizeContract',
		width         : 450,
		title		  : 'Font and size style for final contract',
		//hidden		  : true,	
		//labelWidth	  :	0,
		items         : [
		{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 3 },
			items        : [
				{
					layout: 'form',
					labelWidth	: 45,
					items:[
						{				
							xtype         : 'combo',
							name          : 'ccontractFontType',
							id            : 'ccontractFontType',
							hiddenName    : 'contractFontType',
							fieldLabel    : 'Type',
							typeAhead     : true,
							autoSelect    : true,
							//hidden		  : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFonts'
								},
								fields:[
									{name:'idFont', type:'int'},
									{name:'fontName', type:'string'},
									{name:'usageName', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('ccontractFontType').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'contract_font_type',
												userId		:	userid
											}
										});
									}
								}
							}),
							triggerAction : 'all', 
							itemSelector:	'div.x-boundlist-item',
							tpl: new Ext.XTemplate(
								'<tpl for=".">'+
									'<div class="x-boundlist-item" style="font-family:{fontName};font-size:15px">{fontName}</div>'+
								'</tpl>'
							),
							editable      : false,
							forceSelection:	true,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'fontName',
							valueField    : 'idFont',
							width         : 160,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.idFont,
											camp		:	'contract_font_type',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				},{
					layout: 'form',
					labelWidth	: 40,
					bodyStyle	: 'padding : 0 0 0 10px;',
					items:[
						{
							xtype         : 'combo',
							name          : 'ccontractFontSize',
							id            : 'ccontractFontSize',
							hiddenName    : 'contractFontSize',
							fieldLabel    : 'Size',
							typeAhead     : true,
							autoSelect    : true,
							//hidden		  : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFontsSize'
								},
								fields:[
									{name:'id', type:'int'},
									{name:'name', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('ccontractFontSize').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'contract_font_size',
												userId		:	userid
											}
										});
									}
								}
							}),
							triggerAction : 'all',
							editable      : false,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'name',
							valueField    : 'id',
							width         : 40,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.id,
											camp		:	'contract_font_size',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				},{
					layout: 'form',
					labelWidth	: 40,
					bodyStyle	: 'padding : 0 0 0 10px;',
					items:[
						{
							xtype         : 'combo',
							name          : 'ccontractFontColor',
							id            : 'ccontractFontColor',
							hiddenName    : 'contractFontColor',
							fieldLabel    : 'Color',
							typeAhead     : true,
							autoSelect    : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFontsColor'
								},
								fields:[
									{name:'id', type:'string'},
									{name:'name', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('ccontractFontColor').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'contract_font_color',
												userId		:	userid
											}
										});
									}
								}
							}),
							tpl: new Ext.XTemplate(
								'<tpl for=".">'+
								   '<tpl>'+
										'<div class="x-combo-list-item" style="background:{id}">'+
											'<h3> {name}  </h3>'+
										'</div>'+
									'</tpl>'+
								'</tpl>'
							),
							triggerAction : 'all',
							editable      : false,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'name',
							valueField    : 'id',
							width         : 60,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.id,
											camp		:	'contract_font_color',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				}
			]
		}]
	});
	var fontAndSizeAddons  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		id            : 'formFontAndSizeAddons',
		width         : 450,
		title		  : 'Font and size style for final additional documents',
		//hidden		  : true,	
		//labelWidth	  :	0,
		items         : [
		{
			xtype        : 'panel',
			layout       : 'table',
			layoutConfig : { columns : 3 },
			items        : [
				{
					layout: 'form',
					labelWidth	: 45,
					items:[
						{				
							xtype         : 'combo',
							name          : 'aaddonFontType',
							id            : 'aaddonFontType',
							hiddenName    : 'addonFontType',
							fieldLabel    : 'Type',
							typeAhead     : true,
							autoSelect    : true,
							//hidden		  : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFonts'
								},
								fields:[
									{name:'idFont', type:'int'},
									{name:'fontName', type:'string'},
									{name:'usageName', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('aaddonFontType').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'addon_font_type',
												userId		:	userid
											}
										});
									}
								}
							}),
							triggerAction : 'all', 
							itemSelector:	'div.x-boundlist-item',
							tpl: new Ext.XTemplate(
								'<tpl for=".">'+
									'<div class="x-boundlist-item" style="font-family:{fontName};font-size:15px">{fontName}</div>'+
								'</tpl>'
							),
							editable      : false,
							forceSelection:	true,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'fontName',
							valueField    : 'idFont',
							width         : 160,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.idFont,
											camp		:	'addon_font_type',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				},{
					layout: 'form',
					labelWidth	: 40,
					bodyStyle	: 'padding : 0 0 0 10px;',
					items:[
						{
							xtype         : 'combo',
							name          : 'aaddonFontSize',
							id            : 'aaddonFontSize',
							hiddenName    : 'addonFontSize',
							fieldLabel    : 'Size',
							typeAhead     : true,
							autoSelect    : true,
							//hidden		  : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFontsSize'
								},
								fields:[
									{name:'id', type:'int'},
									{name:'name', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('aaddonFontSize').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'addon_font_size',
												userId		:	userid
											}
										});
									}
								}
							}),
							triggerAction : 'all',
							editable      : false,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'name',
							valueField    : 'id',
							width         : 40,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.id,
											camp		:	'addon_font_size',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				},{
					layout: 'form',
					labelWidth	: 40,
					bodyStyle	: 'padding : 0 0 0 10px;',
					items:[
						{
							xtype         : 'combo',
							name          : 'aaddonFontColor',
							id            : 'aaddonFontColor',
							hiddenName    : 'addonFontColor',
							fieldLabel    : 'Color',
							typeAhead     : true,
							autoSelect    : true,
							//hidden		  : true,
							mode          : 'local',  
							store         : new Ext.data.JsonStore({
								root:'results',
								totalProperty:'total',
								autoLoad: true, 
								baseParams: {
									idfunction: 'getSystemFontsColor'
								},
								fields:[
									{name:'id', type:'string'},
									{name:'name', type:'string'}
								],
								url:'/custom_contract/includes/php/functions.php',
								listeners     : {
									'load'  : function(store, records) {
										Ext.Ajax.request({
											url: '/custom_contract/includes/php/functions.php',
											method: 'POST',
											timeout:0,
											success: function(response, request) {
												var jsonData = Ext.util.JSON.decode(response.responseText);
												Ext.getCmp('aaddonFontColor').setValue(jsonData.data);
											},
											failure: function(response, request) {
												var errMessage = 'Error en la petición ' + request.url + 
																' status ' + response.status + 
																' statusText ' + response.statusText + 
																' responseText ' + response.responseText;
															   
												alert(errMessage);
											},
											params: {
												idfunction	:	'changeDataStyleFonts',
												type		:	'search',
												camp		:	'addon_font_color',
												userId		:	userid
											}
										});
									}
								}
							}),
							tpl: new Ext.XTemplate(
								'<tpl for=".">'+
								   '<tpl>'+
										'<div class="x-combo-list-item" style="background:{id}">'+
											'<h3> {name}  </h3>'+
										'</div>'+
									'</tpl>'+
								'</tpl>'
							),
							triggerAction : 'all',
							editable      : false,
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'name',
							valueField    : 'id',
							width         : 60,
							listeners     : {
								'select'  : function(combo, record, index){
									Ext.Ajax.request({
										url: '/custom_contract/includes/php/functions.php',
										method: 'POST',
										timeout:0,
										success: function(response, request) {
											
										},
										failure: function(response, request) {
											var errMessage = 'Error en la petición ' + request.url + 
															' status ' + response.status + 
															' statusText ' + response.statusText + 
															' responseText ' + response.responseText;
														   
											alert(errMessage);
										},
										params: {
											idfunction	:	'changeDataStyleFonts',
											type		:	'update',
											value		:	record.data.id,
											camp		:	'addon_font_color',
											userId		:	userid
										}
									});
								}
							}
						}
					]
				}
			]
		}]
	});
	var panelMailSetting=new Ext.FormPanel({
		title      : 'Please Configure your Mail Settings',
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		fileUpload : true,
		method     : 'POST',
		items      : [
			{
				html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
				cls         : 'x-form-item',
				style       : {
					marginBottom : '10px'
				}
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.comm or use a gmail account with ssl://smtp.gmail.com or yahoo with smtp.mail.yahoo.com)</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				width      : '200',
				value      : '25'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">25 default, 465 if you want to use gmail or other SSL enabled server</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Username',
				name       : 'username',
				width      : '200'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
			},{
				xtype      : 'textfield',
				inputType  : 'password',
				fieldLabel : 'Server Password',
				name       : 'password',
				width      : '200'
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
			handler : function(){
				if (panelMailSetting.getForm().isValid()) {
					panelMailSetting.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var controlMail	=	(Ext.util.Cookies.get('datos_usr[idusertype]'))== 1	?	150	:	30;
							var resp = a.result;
							alert(resp.mensaje);
							if(resp.savesett=='yes'){
								winMailSett.hide();
								if(totalsent<controlMail){
									totalenviados=0;
									totalnosent=0;
									progressbar_win.show();
									genFinalContract(0);
								}else{
									alert('Daily email contracts limit exceeded');
								}
							}
								
						}
					});
				}
			}
		}]
	});
	var winMailSett = new Ext.Window({
		id: 'winMailSett',
		width       : 400,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				panelMailSetting.getForm().reset();
			}
		},
		items       : [
			panelMailSetting
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				winMailSett.close();
			}
		}]
	});	
	
	function genFinalContract(pid,userid) {
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);

		selId = Ext.getCmp('addGrid').selModel.selections.items;
		var temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddeum').setValue(temp);

		selId = Ext.getCmp('addonGrid').selModel.selections.items;
		temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddonG').setValue(temp);
		Ext.getCmp('pids').setValue(selected[pid]);	
		///////////Decido a que php va a llamar para generar el contrato, si al de Juan o al de Jesus///////////
		Ext.getCmp('ctype').getStore().each(function(rec){
			if(rec.data.id == Ext.getCmp('ctype').getValue()){
				if(rec.data.type=="N"){
					generate.getForm().url="mysetting_tabs/myfollowup_tabs/properties_followgetcontractJ.php";
				}else{
					generate.getForm().url="mysetting_tabs/myfollowup_tabs/properties_followgetcontract.php";
				}
			}
		});
		
		if(callback){
			var status = (selectedStatus[pid]['status']).split('/');
			if(Ext.getCmp('sendtype').getValue() == '3' && status[0] != 'PENDING' && status[0] != '') totalnoschedule++;
			else if(Ext.getCmp('sendtype').getValue() == '5' && status[1] != 'PENDING' && status[1] != '') totalnoschedule++;
			
			generate.getForm().url='mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php';
			generate.getForm().submit({
				params	: {
					userid			: userid,
					task			: values.task, 
					detail			: values.detail,
					odate			: values.odate,
					ohour			: values.ohour,
					parcelid		: selectedStatus[pid]['pid'],
					status			: selectedStatus[pid]['status'],
					typeExec		: values.type,
					scheduleTask	: true
				},
				timeout : 86400,
				success : function(form, action) {
					if((pid)==selected.length-1){						
						if(document.getElementById('generate_contract')){
							var tab = tabs.getItem('generate_contract');
							tabs.remove(tab);
						}
						
						progressbar_win.hide();
						win.close();
						Ext.Msg.alert("Follow Schedule", 'All Follow Schedule Task were created. '+totalnoschedule+' presented errors. <br>Please, verify them on the Pending Task tab');
						if(store) store.reload();
					}else{
						genFinalContract(pid+1,userid);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					win.close();
					Ext.MessageBox.alert('Warning', 'Error creating schedule task.');
					//console.log(form, action);
				}
			});
		}else{												
			generate.getForm().submit({
				params	: {
					userid	:	userid
				},
				timeout : 86400,
				success : function(form, action) {
					var controlMail	=	(Ext.util.Cookies.get('datos_usr[idusertype]'))== 1	?	150	:	30;
					if(action.result.enviado=='1'){
						totalenviados=totalenviados+1;
						if(action.result.enviadoagente>0){
							totalsent	=	action.result.enviadoagente;
							if(mailenviados) mailenviados.setText('Total Sent '+(totalsent))
						}
					}else{
						totalnosent++;
					}
					if((pid)==selected.length-1){
						progressbar_win.hide();
						win.close();
						alert('Contracts sent '+totalenviados+'\nContracts not sent '+totalnosent);
						if(store) store.reload();
						
					}else{
						if(totalsent<controlMail){
							genFinalContract(pid+1,userid);
						}else{
							alert('Daily email contracts limit exceeded');
							progressbar_win.hide();
							win.close();
							alert('Contracts sent '+totalenviados+'\nContracts not sent '+totalnosent);
							if(store) store.reload();
						}
					}
				},
				failure : function(form, action) {
					console.log(action);
					progressbar_win.hide();
					win.close();
					//Ext.MessageBox.alert('Warning', action.response.statusText);
				}
			});
		}
	}

	function getViewContract(pid,userid) {
		loading_win.show();
		selId = Ext.getCmp('addGrid').selModel.selections.items;
		var temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddeum').setValue(temp);

		selId = Ext.getCmp('addonGrid').selModel.selections.items;
		temp = '';
		for (i=0; i<=selId.length-1; i++) {
			temp = temp + '|' + selId[i].id
		}
		Ext.getCmp('fieldAddonG').setValue(temp);
		Ext.getCmp('pids').setValue(selected[pid]);	
		///////////Decido a que php va a llamar para generar el contrato, si al de Juan o al de Jesus///////////
		Ext.getCmp('ctype').getStore().each(function(rec){
			if(rec.data.id == Ext.getCmp('ctype').getValue()){
				if(rec.data.type=="N"){
					generate.getForm().url="mysetting_tabs/myfollowup_tabs/properties_followgetcontractJ.php";
				}else{
					generate.getForm().url="mysetting_tabs/myfollowup_tabs/properties_followgetcontract.php";
				}
			}
		});												
		Ext.getCmp('sendtype').setValue('9');
		generate.getForm().submit({
			params	: {
				userid	:	userid
			},
			timeout : 86400,
			success : function(form, action) {
				loading_win.hide();
				var Digital=new Date()
				var hours=Digital.getHours()
				var minutes=Digital.getMinutes()
				var seconds=Digital.getSeconds()
				var url = 'http://www.reifax.com/'+action.result.pdf;
				//var url = 'http://www.reifax.com/custom_contract/view/web/viewer.html?url=/'+action.result.pdf;
				//window.open(url); //Agregado por Jesus
				window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus
			},
			failure : function(form, action) {
				loading_win.hide();
				Ext.MessageBox.alert('Warning', action.result.error);
			}
		});
	}

	function modifyContract(typecontract,placecontract){
		if(typecontract=="addon"){
			//var idcontract = Ext.getCmp('addon_id'+placecontract).getValue();
		}else{
			var idcontract = Ext.getCmp('ctype').getValue();			
		}
		
		if(idcontract && idcontract!=0)
		{
			//Ext.MessageBox.alert('',idcontract);
			new Ext.util.Cookies.set("idmodifycontract", idcontract);
			new Ext.util.Cookies.set("typemodifycontract", typecontract);
			if(typecontract=="addon"){
				new Ext.util.Cookies.set("placemodifycontract", placecontract);
			}
	
			if(document.getElementById('idmodifycontract')){
				var tab = tabs.getItem('idmodifycontract');
				tabs.remove(tab);
			}
	
			Ext.getCmp('principal').add({
				//xtype: 'component', 
				title: 'Modify Contract', 
				closable 	: true,
				autoScroll	: true,
				id: 'idmodifycontract', 
				autoLoad:	{url:'/custom_contract/manager.php',scripts:true}
			}).show();
		}

	}	
	var email  = new Ext.Panel({
		xtype         : 'panel',
		border        : true,
		bodyStyle     : 'padding : 10px; border: 2px solid #cccccc',
		//hidden        : true,
		//collapsible   : true,
		collapsed     : false,
		layout        : 'form',
		id            : 'formDel',
		title         : 'Email Delivery',
		width         : 450,
		//labelWidth    : 150,
		items         : [
			{
				xtype        : 'panel',
				layout       : 'table',
				layoutConfig : { columns : 2 },
				items        : [
				{
					layout: 'form',
					labelWidth	: 140,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendmail',
						name          : 'sendmail',
						fieldLabel    : 'Send Document By Email',
						//checked		  : true,
						//handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
								var ad = Ext.getCmp('sendmail').getValue();
								/*var ad1 = Ext.getCmp('sendme').getValue();*/
								if (ad == true /*|| ad1 == true*/){									
									if(!callback) Ext.getCmp('sendBut').setVisible(true);
									
									Ext.getCmp('sendbyfax').setValue(false);
									Ext.getCmp('paneltemplatesemail').setVisible(true);
								}else{
									if(!callback) Ext.getCmp('sendBut').setVisible(false);
									
									Ext.getCmp('paneltemplatesemail').setVisible(false);
								}
								
							}
						}
					}]
				},{
					layout: 'form',
					labelWidth	: 130,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendbyfax',
						name          : 'sendbyfax',
						fieldLabel    : 'Send Document By Fax',
						//handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
								var ad = Ext.getCmp('sendbyfax').getValue();
								if (ad == true){
									Ext.getCmp('sendmail').setValue(false);
									if(!callback) Ext.getCmp('sendFax').setVisible(true);
									Ext.getCmp('paneltemplatesfax').setVisible(true);
								}else{
									if(!callback) Ext.getCmp('sendFax').setVisible(false);
									Ext.getCmp('paneltemplatesfax').setVisible(false);
								}				
								
							}
						}
					}]
				}/*,{
					layout: 'form',
					labelWidth	: 140,
					items:[
					{
						xtype         : 'checkbox',
						id            : 'sendme',
						name          : 'sendme',
						fieldLabel    : 'Send Copy to me',
						//handler		  : button_generate_and_send,
						listeners     : {
							'check'   : function () {
								var ad = Ext.getCmp('sendme').getValue();
								var ad1 = Ext.getCmp('sendmail').getValue();
								if (ad == true || ad1 == true){
									Ext.getCmp('sendbyfax').setValue(false);
									Ext.getCmp('sendBut').setVisible(true);
									Ext.getCmp('paneltemplatesemail').setVisible(true);
								}else{
									Ext.getCmp('sendBut').setVisible(false);
									Ext.getCmp('paneltemplatesemail').setVisible(false);
								}
								
							}
						}
					}]
				},{
					layout: 'form',
					labelWidth	: 130,
					colspan : 2,
					items:[
						{										
							xtype         : 'checkbox',
							id            : 'scrow',
							name          : 'scrow',
							fieldLabel    : 'Escrow Letter',
							onHide        : function(){this.getEl().up('.x-form-item').setDisplayed(false);},
							onShow        : function(){this.getEl().up('.x-form-item').setDisplayed(true);}
						}
					]
				}*/,{
					layout: 'form',
					labelWidth	: 140,
					colspan:	2,
					items:[panelTemplateEmails]
				},{
					layout: 'form',
					labelWidth	: 130,
					colspan:	2,
					items:[panelTemplateFax]
				},
				{
					xtype         : 'hidden',
					id            : 'rname',
					name          : 'rname'
				}]
			}
		]

	});
	
	var emailConfig = '0'; 
	var generate = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followgetcontract.php', 
		frame         : true,
		monitorValid  : true,
		id            : 'GenMainPanel',
		title         : 'Select Contract.',
		//width         : 450,	//Comentado por Jesus
		labelWidth    : 130,
		waitMsgTarget : 'Generating...',
		layout		  : 'column',
		defaults: {
			columnWidth: '.5',
			bodyStyle:'padding:5px'
		},
		items : [{
			layout: 'form',
			autoHeight: true,
			items : [{
				xtype: 'panel',
				layout: 'table',
				layoutConfig : { columns : 4 },
				items: [{
					xtype:'checkbox',
					fieldLabel: '',
					hideLabel:true,
					width:20,
					tooltip:'with or without this',
					labelSeparator: '',
					checked:true,
					id: 'withContract',
					name: 'withContract'
				},{
					layout: 'form',
					items: [{
						xtype         : 'combo',
						name          : 'ctype',
						id            : 'ctype',
						hiddenName    : 'type',
						fieldLabel    : 'Document',
						typeAhead     : true,
						autoSelect    : true,
						mode          : 'local',
						store         : contractsFL,
						/*store         : new Ext.data.JsonStore({
							root:'results',
							totalProperty:'total',
							autoLoad: true,
							baseParams: {
								type: 'contracts',
								'userid': userid
							},
							fields:[
								{name:'id', type:'int'},
								{name:'name', type:'string'},
								{name:'type', type:'string'},
								{name:'tplactive', type:'int'},
								{name:'tpl1_name', type:'string'},
								{name:'tpl1_addr', type:'string'},
								{name:'tpl2_name', type:'string'},
								{name:'tpl2_addr', type:'string'},
								{name:'inspectionDays', type:'int'}
							],
							url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
							listeners     : {
								'load'  : function(store, records) {
									var sel=records[0].get('id');
									Ext.getCmp('ctype').setValue(sel);
									if(selling==true){
										if(records[0].get('tplactive')==1){
											Ext.getCmp('sellingname').setValue(records[0].get('tpl1_name'));
											Ext.getCmp('sellingaddress').setValue(records[0].get('tpl1_addr'));
										}else{
											Ext.getCmp('sellingname').setValue(records[0].get('tpl2_name'));
											Ext.getCmp('sellingaddress').setValue(records[0].get('tpl2_addr'));
										}
									}
								}
							}
						}),*/
						triggerAction : 'all',
						editable      : false,
						selectOnFocus : true,
						allowBlank    : false,
						displayField  : 'name',
						valueField    : 'id',
						value		  : defaultcontract,
						width         : 240,
						listeners	  : {
							'select'  : function(combo,record,index){
								var id=	record.data.tplactive;
								console.debug(record);
								if(record.data.type=="N"){
									Ext.getCmp('controlcombooptionshide').hide();
									Ext.getCmp('buttonmoddifycontract2').show();
								}else{
									Ext.getCmp('controlcombooptionshide').show();
									Ext.getCmp('buttonmoddifycontract2').hide();
								}
								if(record.data['tpl'+id+'_type']=='B'){
									Ext.getCmp('buyername').setValue(record.data['tpl'+id+'_name']);
									Ext.getCmp('baddress1').setValue(record.data['tpl'+id+'_addr']);
									Ext.getCmp('baddress2').setValue(record.data['tpl'+id+'_addr2']);
									Ext.getCmp('baddress3').setValue(record.data['tpl'+id+'_addr3']);
									Ext.getCmp('sellname').setValue('');
									Ext.getCmp('saddress1').setValue('');
									Ext.getCmp('saddress2').setValue('');
									Ext.getCmp('saddress3').setValue('');
									if(selected.length>1){
										Ext.getCmp('sellname').setReadOnly(true);
										Ext.getCmp('saddress1').setReadOnly(true);
										Ext.getCmp('saddress2').setReadOnly(true);
										Ext.getCmp('saddress3').setReadOnly(true);
										Ext.getCmp('sellerGetContact').setDisabled(true);
										Ext.getCmp('buyerGetContact').setDisabled(false);
										Ext.getCmp('buyername').setReadOnly(false);
										Ext.getCmp('baddress1').setReadOnly(false);
										Ext.getCmp('baddress2').setReadOnly(false);
										Ext.getCmp('baddress3').setReadOnly(false);
									}
								}else{
									Ext.getCmp('sellname').setValue(record.data['tpl'+id+'_name']);
									Ext.getCmp('saddress1').setValue(record.data['tpl'+id+'_addr']);
									Ext.getCmp('saddress2').setValue(record.data['tpl'+id+'_addr2']);
									Ext.getCmp('saddress3').setValue(record.data['tpl'+id+'_addr3']);
									Ext.getCmp('buyername').setValue('');
									Ext.getCmp('baddress1').setValue('');
									Ext.getCmp('baddress2').setValue('');
									Ext.getCmp('baddress3').setValue('');
									if(selected.length>1){
										Ext.getCmp('buyername').setReadOnly(true);
										Ext.getCmp('baddress1').setReadOnly(true);
										Ext.getCmp('baddress2').setReadOnly(true);
										Ext.getCmp('baddress3').setReadOnly(true);
										Ext.getCmp('buyerGetContact').setDisabled(true);
										Ext.getCmp('sellerGetContact').setDisabled(false);
										Ext.getCmp('sellname').setReadOnly(false);
										Ext.getCmp('saddress1').setReadOnly(false);
										Ext.getCmp('saddress2').setReadOnly(false);
										Ext.getCmp('saddress3').setReadOnly(false);
									}
								}
								if(selling==true){
									if(record.get('tplactive')==1){
										Ext.getCmp('sellingname').setValue(record.get('tpl1_name'));
										Ext.getCmp('sellingaddress').setValue(record.get('tpl1_addr'));
									}else{
										Ext.getCmp('sellingname').setValue(record.get('tpl2_name'));
										Ext.getCmp('sellingaddress').setValue(record.get('tpl2_addr'));
									}
								}
								Ext.getCmp('inspection').setValue(record.data.inspectionDays);
								var tp = combo.getValue();
								Ext.getCmp('addonGrid').store.load({params: {type : tp}});
							}
						}
					}]
				},{
					xtype        : 'panel',
					layout       : 'table',
					id: 'controlcombooptionshide',
					items: [
					{
						layout: 'form',
						labelWidth	: 130,
						items:[{
							xtype         : 'combo',
							name          : 'coptions',
							id            : 'coptions',
							hiddenName    : 'options',
							fieldLabel    : 'Generate Options',
							typeAhead     : true,
							store         : contractseal,
							mode          : 'local',
							triggerAction : 'all',
							editable      : false,
							emptyText     : 'Select ...',
							selectOnFocus : true,
							allowBlank    : false,
							displayField  : 'name',
							valueField    : 'id',
							value         : '0',
							width         : 260
						}]
					}]
				},{
					xtype    : 'button',
					id		 : 'buttonmoddifycontract2',
					hidden	 : true,
					cls		 : 'x-btn-text-icon',
					icon	 : '/img/update.png',
					iconAlign: 'left',
					//text     : 'Modify contract',
					scale:		'medium',
					style    : {
						marginRight : 10,
						marginLeft  : 150
					},
					handler  : function (){
						var navegador=navigator.userAgent;
						if(navegador.indexOf('MSIE') != -1) {
							if(parseInt(BrowserDetect.version)<9){
								Ext.Msg.show({
								   title      : '',
								   //msg        : 'This option works only with Firefox and Chrome. Internet Explorer users please download and use any of the mentioned browsers.',
								   msg		  : 'This option only works with IE9 or above.',
								   width      : 400,
								   buttons    : Ext.MessageBox.OK,
								   icon       : Ext.MessageBox.INFO
								})
							}else{
								modifyContract("contract","0");
							}
						}else{
							modifyContract("contract","0");
						}
					}
					//handler  : deleteContract
				}]
			},{
				xtype         : 'hidden',
				name          : 'county',
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id			  : 'pids'
			},{
				xtype         : 'hidden',
				name          : 'mlnaux',
				id            : 'mlnaux'
			},{
				xtype         : 'hidden',
				name          : 'fieldAddeum',
				id            : 'fieldAddeum'
			},{
				xtype         : 'hidden',
				name          : 'fieldAddonG',
				id            : 'fieldAddonG'
			},{
				xtype         : 'hidden',
				name          : 'addr',
				id            : 'addr'
			},{
				xtype         : 'hidden',
				name          : 'sendtype',
				id            : 'sendtype'
			},{
				xtype         : 'hidden',
				name          : 'typeFollow',
				id            : 'typeFollow',
				value		  : follow
			},{
				xtype         : 'hidden',
				name          : 'completetask',
				id            : 'completetask',
				value		  : completetask
			},{
				xtype         : 'hidden',
				name          : 'stype',
				value		  : 'insert'
			},
			addond,
			//checks,
			email,
			//checksDocuments,
			fontAndSizeContract,
			fontAndSizeAddons
			]
		},{
			layout: 'form',
			autoHeight: true,
			items : [
				buyer,
				seller,
				addinfo,
				//addend,
				panelSelling,
				checksComplete
			]
		}],
		style       : "text-align:left",
		listeners	:	{
			'afterrender':function(){
				Ext.getCmp('deposit').setValue(globalVars.initialDeposit);
				Ext.getCmp('additionalDeposit').setValue(globalVars.additionalDeposit);

				/////////////////////////////////Ejecutamos el Select del Combo de los Contratos////////////////////////////////
				Ext.getCmp('ctype').fireEvent('select',Ext.getCmp('ctype'),Ext.getCmp('ctype').store.getAt(Ext.getCmp('ctype').selectedIndex+1));
				//////////////Le Incremente 1 al indice para que no tomara el espacio en blanco desde un principio//////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}
		},		
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Schedule Task',
				handler : function() {
					if(Ext.getCmp('deposit').getValue() <= 0){
						Ext.MessageBox.alert('Warning','Please, Initial Deposit is Obligatory.',function(){
							Ext.getCmp('deposit').focus();
						});
					}else{
						var sendmail=Ext.getCmp('sendmail').getValue();
						//var sendme=Ext.getCmp('sendme').getValue();
						var sendbyfax=Ext.getCmp('sendbyfax').getValue();
						
						if(sendmail) Ext.getCmp('sendtype').setValue('5'); 
						else if(sendbyfax) Ext.getCmp('sendtype').setValue('3');
						else{
							Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
							return false;
						}
						
						progressbar_win.show();
						totalnoschedule = 0;
						genFinalContract(0,userid);
						
					}
				},
				hidden  : callback ? false : true
			},{
				text    : 'Send Email',
				userid	: userid,
				handler : function() {
					var btmtmp	=	this;
					if(Ext.getCmp('deposit').getValue() <= 0){
						Ext.MessageBox.alert('Warning','Please, Initial Deposit is Obligatory.',function(){
							Ext.getCmp('deposit').focus();
						});
						return;
					}
					if(Ext.getCmp('buyername').getValue() == ''){
						Ext.MessageBox.alert('Warning','Please, enter a Buyer Name.',function(){
							Ext.getCmp('buyername').focus();
						});
						return;
					}
					Ext.getCmp('sendtype').setValue('5');
					var sendmail=Ext.getCmp('sendmail').getValue();
					//var sendme=Ext.getCmp('sendme').getValue();
					if(sendmail){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php',
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								var controlMail	=	(Ext.util.Cookies.get('datos_usr[idusertype]'))== 1	?	150	:	30;
								if(resp.mail=='true'){
									if(totalsent<controlMail){
										totalenviados=0;
										totalnosent=0;
										progressbar_win.show();
										genFinalContract(0,btmtmp.userid);
									}else{
										alert('Daily email documents limit exceeded');
									}
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option ');
						return false;
					}
				},
				id      : 'sendBut',
				hidden  : true
			},{
				text    : 'Send Fax',
				userid	: userid,
				handler : function() {
					var btmtmp	=	this;
					if(Ext.getCmp('deposit').getValue() <= 0){
						Ext.MessageBox.alert('Warning','Please, Initial Deposit is Obligatory.',function(){
							Ext.getCmp('deposit').focus();
						});
					}
					if(Ext.getCmp('buyername').getValue() == ''){
						Ext.MessageBox.alert('Warning','Please, enter a Buyer Name.',function(){
							Ext.getCmp('buyername').focus();
						});
						return;
					}
					Ext.getCmp('sendtype').setValue('3');
					var sendbyfax=Ext.getCmp('sendbyfax').getValue();
					if(sendbyfax){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php',
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									var controlMail	=	(Ext.util.Cookies.get('datos_usr[idusertype]'))== 1	?	150	:	30;
									if(totalsent<controlMail){
										totalenviados=0;
										totalnosent=0;
										progressbar_win.show();
										genFinalContract(0,btmtmp.userid);
									}else{
										alert('Daily email documents limit exceeded');
									}
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option ');
						return false;
					}
				},
				id      : 'sendFax',
				hidden  : true
			},{
				text	: 'View',
				userid	: userid,
				handler	: function(){
					if(Ext.getCmp('deposit').getValue() <= 0){
						Ext.MessageBox.alert('Warning','Please, Initial Deposit is Obligatory.',function(){
							Ext.getCmp('deposit').focus();
						});
					}else
						getViewContract(0,this.userid);
				}
			},{
				text    : 'Cancel',
				handler : function(){
					//win.close();
					var tab = tabs.getItem('generate_contract');
					tabs.remove(tab);					
				}
			}
		]
	});
	
	var message = "<div style=\"padding:10px 0 10px 30px\">In order to be able to edit the ";
	message += "contracts you must have Abobe Reader Pro.<br>";
	message += "You can also do it with the following  free ";
	message += "softwares FoxitReader at ";
	message += "<a href='http://www.foxitsoftware.com/pdf/reader/' target='_new'>";
	message += "http://www.foxitsoftware.com/pdf/reader/</a> ";
	message += "and PDF-XChange viewer at ";
	message += "<a href='http://www.tracker-software.com/product/pdf-xchange-viewer' target='_new'>";
	message += "http://www.tracker-software.com/product/pdf-xchange-viewer</a></div>";
	
	var dismessage="<div style=\"padding:10px 0 10px 30px\">WARNING<br/>";
	dismessage+="The following Document Generator feature of REIFAX.com (herein after known as REIFAX) is for the sole purpose of modifying existing documents that the user uploads to the system.";
	dismessage+="No documents will be supplied by REIFAX .";
	dismessage+="The user MUST HAVE legal authority to upload, modify, change, fill-in and use any document he intends to modify and submit to another buyer or seller.";
	dismessage+="Legal authority means that if a copyrighted document is uploaded, the user MUST HAVE written permission of the person or entity who drafted the document, or have paid an authorized re-seller for the use of that document, i.e. FAR/BAR forms.";
	dismessage+="By uploading any document, the user hereby agrees to these terms and conditions for using the REIFAX Document Generator.";
	dismessage+="Violation of these terms and conditions could result in the user violating copyright laws.";
	dismessage+="The only information that will be filled in by REIFAX, on any form, if applicable is information that is available in the public record.";
	dismessage+="Any and all other information will have to be supplied by the user.</div>";
	var disclaimer = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		items       : [
			{ xtype: "panel", html: dismessage }
		],
		buttons : [{
				text     : 'Accept',
				handler  : function(){
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php',
						method  : 'POST',
						params: { module:'acceptcontract',
								  action:'accept'	
						},
						waitMsg : 'Wait...',
						success : function(r) {
							var resp   = Ext.decode(r.responseText)
							if(resp.msg=='true'){
								disclaimer.close();
								if(document.getElementById('dateAcc')) Ext.getCmp('dateAcc').setValue(fechaAcc);
								if(document.getElementById('dateClo')) Ext.getCmp('dateClo').setValue(fechaClo);
								//Ext.getCmp('completetask').setValue(completetask);
								win.show();
							}
						}
					});
					disclaimer.close();
				}
			},{
				text     : 'Decline',
				handler  : function(){
					disclaimer.close();
				}
			}]
	});
	var win = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		listeners   : {
			'beforeshow' : function() {
				generate.getForm().reset();
			}
		},
		items       : [
			generate,
			{ xtype: "panel", html: message }
		],
		buttons : [{
			text     : 'Close',
			handler  : function(){
				win.close();
			}
		}]
	});
	//Verificar que haya aceptado el disclaimer
	Ext.Ajax.request({
		url     : 'overview_contract/verifications.php',
		method  : 'POST',
		params: { module:'acceptcontract',
				  action:'verificate'	
		},
		waitMsg : 'Wait...',
		success : function(r) {
			var resp   = Ext.decode(r.responseText)
			if(resp.msg=='true'){
				//win.show();
				if(document.getElementById('generate_contract')){
					var tab = tabs.getItem('generate_contract');
					tabs.remove(tab);
				}
				//////////CREO EL TAB PARA LOS CONTRATOS//////////				
				Ext.getCmp('principal').add({
					//xtype: 'component', 
					title: 'Document Manager',
					id:'generate_contract',
					closable 	: true,
					autoScroll	: false,
					items: [generate]
				}).show();

				/////////////////////////////////Ejecutamos el Select del Combo de los Contratos////////////////////////////////
				/*Ext.getCmp('ctype').store.on('load',function(){
					Ext.getCmp('ctype').fireEvent('select',Ext.getCmp('ctype'),Ext.getCmp('ctype').store.getAt(Ext.getCmp('ctype').selectedIndex+1));
				});*/
				//////////////Le Incremente 1 al indice para que no tomara el espacio en blanco desde un principio//////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////				

				if(document.getElementById('dateAcc')) Ext.getCmp('dateAcc').setValue(fechaAcc);
				if(document.getElementById('dateClo')) Ext.getCmp('dateClo').setValue(fechaClo);
				//Ext.getCmp('completetask').setValue(completetask);
				//Ext.getCmp('formOpt').show();
				//Ext.getCmp('csinfo').show();
				//Ext.getCmp('cbinfo').show();
				Ext.getCmp('coptions').setValue(1);
				if(selling==true){
					Ext.getCmp('csellinginfo').show();
					Ext.getCmp('csellinginfo').setValue(true);
				}
			}else{
				disclaimer.show();
			}
		}
	});	
}

function sendEmail(selected,grid,store,userid, progressbar_win, progressbar, selling, completetask,callback,values,selectedStatus){
	selling = selling || false;
	var follow = 'B';
	if(selling==true){
		follow = 'S';
	}
	store = store || false;
	completetask = completetask || false;
	var ind = 1;
	var att = {
		xtype: 'compositefield',
		fieldLabel: 'Attach',
		items: [
			{
				xtype: 'fileuploadfield',
				emptyText: 'Select a file',
				name: 'archivo'+ind,
				buttonText: 'Browse',
				width: 200
			},new Ext.Button({
				text: 'Another file',
				handler: function(){
					att.items[0].name = 'archivo'+(ind+1);
					ind++;
					formEmail.insert(ind,att);
					formEmail.doLayout();
				}
			})
		]
	};

	var arraemalisent='', iforemsent=0, mailsending, filecode='', totalnoschedule = 0;
	function sendMailTemplate(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		if(pid+1==selected.length)
			Ext.getCmp('action-delete').setValue('delete');
		Ext.getCmp('pid').setValue(selected[pid]);
		
		if((pid+1)==1)
		{
			var fec=new Date();
			filecode=fec.getTime();
		}
		
		if(callback){
			if(selectedStatus[pid]['status'] != 'PENDING' && selectedStatus[pid]['status'] != '') totalnoschedule++;
			
			formEmail.getForm().submit({
				timeout : 86400,
				url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				params	: {
					task: values.task, 
					detail: values.detail,
					odate: values.odate,
					ohour: values.ohour,
					parcelid: selectedStatus[pid]['pid'],
					status: selectedStatus[pid]['status'],
					typeExec: values.type,
					type: 'insert'
				},
				success : function(form, action) {
					console.log(action);
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close();
						Ext.Msg.alert("Follow Schedule", 'All Follow Schedule Task were created. \n '+totalnoschedule+' presented errors. Please, verify them on the Pending Task tab');
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendMailTemplate(pid+1);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					winTemplate.close();
					Ext.MessageBox.alert('Warning', 'Error creating schedule task.');
					console.log(action);
				}
			});
		} else {	
			formEmail.getForm().submit({
				timeout : 86400,
				params:{iforemsent:iforemsent,filecode:filecode},
				success : function(form, action) {
					if(iforemsent>0)arraemalisent+=",";
					arraemalisent+="{'email':'"+action.result.remail+"','send':"+action.result.enviadoagente+",'erroremail':'"+action.result.erroremail+"','address':'"+action.result.address+"','agent':'"+action.result.agent+"'}";
					iforemsent++;
					if(action.result.enviado=='1'){
						totalenviados=totalenviados+1;
					}else{
						totalnosent++;
					}
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close();
						//alert('Mails sent '+totalenviados+'\nMails not sent '+totalnosent);
						if(store) store.reload();
						arraemalisent='['+arraemalisent+']';
						responseSendMail(arraemalisent,action.result.filecode,action.result.userid);
						/*mailsending = Ext.decode(arraemalisent);
						console.log(arraemalisent);
						console.log(mailsending[1].email);*/
						pid=0;
						iforemsent=0;
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendMailTemplate(pid+1,action.result.filecode);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					winTemplate.close();
					Ext.MessageBox.alert('Warning', action.response.responseText);
					console.log(action);
				}
			});
		}
	}
	
	function responseSendMail(arraemalisent,filecode,u){
		mailsending = Ext.decode(arraemalisent);
		//console.log(arraemalisent);
		var resulthtml=	'<table  border="0" cellpadding="0" cellspacing="0" style=" font-size:10pt">'+
						'	  <tr bgcolor="#ffffff" align="left"><td colspan="6"><a href="mysetting_tabs/myfollowup_tabs/download.php?i='+u+filecode+'" target="_blank">Click to download</a></td></tr>'+
						'	  <tr bgcolor="#e2eefe" align="center">'+
						'		<td width="40px">&nbsp;</td>'+
						'		<td width="150px"><b>Address</b></td>'+
						'		<td width="150px"><b>Agent</b></td>'+
						'		<td width="150px"><b>Email</b></td>'+
						'		<td width="60px"><b>Send</b></td>'+
						'		<td width="250px"><b>Msg</b></td>'+
						'	  </tr>'
						;
								
		for (var i=0;i<mailsending.length;i++)
		{
			resulthtml+= '	<tr bgcolor="'+((i%2)==0?'#ffffff':'#fafafa')+'" >'+
						'		<td align="center">'+(i+1)+'</td>'+
						'		<td>'+mailsending[i].address+'</td>'+
						'		<td>'+((mailsending[i].agent==null || mailsending[i].agent=='null' || mailsending[i].agent=='undefined' || mailsending[i].agent=='')?'':mailsending[i].agent)+'</td>'+
						'		<td>'+((mailsending[i].email==null || mailsending[i].email=='null' || mailsending[i].email=='undefined' || mailsending[i].email=='')?'':mailsending[i].email)+'</td>'+
						'		<td align="center">'+(mailsending[i].send==1?'<img src="img/drop-yes.gif" border="0" />':'<img src="img/drop-no.gif" border="0" />')+'</td>'+
						'		<td>'+((mailsending[i].erroremail==null || mailsending[i].erroremail=='null' || mailsending[i].erroremail=='undefined' || mailsending[i].erroremail=='')?'':mailsending[i].erroremail)+'</td>'+
						'	</tr>';
//						'		<td>'+(mailsending.data[i]['errorsending'].length>0?mailsending.data[i]['errorsending']:'&nbsp;')+'</td>'+
		}
		resulthtml+='</table>';
								
		var w9 = new Ext.Window({
			title: 'Response Email', 
			width: 770,
			height: 400,
			closable :true,
			modal: true,
			layout: 'fit',
			autoScroll : true,
			plain: true,
			bodyStyle: 'padding:5px;',
			html: resulthtml
		});
		w9.show();								
	}
	
	var formEmail = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendmail.php', 
		frame         : true,
		monitorValid  : true,
		fileUpload : true,
		id            : 'formEmail',
		labelWidth	  : 70,
		defaults	  : {width: 600},
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype: 'radiogroup',
				fieldLabel: 'Email By',
				items: [
					{	
						boxLabel: 'Template', 
						name: 'rtemplateselect', 
						inputValue: 'T',
						listeners: {
							'check': function(r, check){
								if(check){
									Ext.getCmp('ccontracttemplate').setVisible(true);
									var newvalue = Ext.getCmp('ccontracttemplate').getValue();
									getTemplateById(newvalue,userid,'email',function(b,s){
										Ext.getCmp('email_newtemplatebody').setValue(b);
										Ext.getCmp('email_newtemplatesubject').setValue(s);
										loading_win.hide();
									}); 
								}else{
									Ext.getCmp('email_newtemplatebody').setValue('');
									Ext.getCmp('email_newtemplatesubject').setValue('');
									Ext.getCmp('ccontracttemplate').setVisible(false);
								}
							}
						}
					},
					{boxLabel: 'New', name: 'rtemplateselect', inputValue: 'N', checked: true}
				]
			},{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				hidden		  : true,
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'docstemplates',
						'userid': userid,
						template_type: 1
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel); 
							loading_win.hide();
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				listeners     : {
					'select'  : function(combo,record,index){
						var newvalue = record.get('id');
						getTemplateById(newvalue,userid,'email',function(b,s){
							Ext.getCmp('email_newtemplatebody').setValue(b);
							Ext.getCmp('email_newtemplatesubject').setValue(s);
							loading_win.hide();
						}); 
					}
				}
			},{
				xtype: 'textfield',
				fieldLabel: 'Subject',
				name: 'subject',
				id: 'email_newtemplatesubject'
			},{
				xtype:'htmleditor',
				id: 'email_newtemplatebody',
				fieldLabel : 'Body',
				autoScroll: true,
				height:200,
				name: 'body',    
				enableSourceEdit : false
			},{
				xtype         : 'hidden',
				name          : 'sendmail',
				id			  : 'sendmail',
				value         : 'on'
			},{
				xtype         : 'hidden',
				name          : 'sendme',
				id			  : 'sendme',
				value         : 'off'
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			},{
				xtype         : 'hidden',
				name          : 'typeFollow',
				id            : 'typeFollow',
				value		  : follow
			},{
				xtype         : 'hidden',
				name          : 'completetask',
				id            : 'completetask',
				value		  : completetask
			},{
				xtype         : 'hidden',
				name          : 'nuevotemplate',
				id            : 'nuevotemplate'
			},{
				xtype         : 'hidden',
				name          : 'nuevotemplatesub',
				id            : 'nuevotemplatesub'
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					var sendmail=Ext.getCmp('sendmail').getValue();
					//var sendme=Ext.getCmp('sendme').getValue();
					if(sendmail){
						Ext.Ajax.request({
							url     : 'overview_contract/verifications.php', 
							method  : 'POST',
							params: { module:'mailsettings',
									  action:''	
							},
							waitMsg : 'Getting Info',
							success : function(r) {
								var resp   = Ext.decode(r.responseText)
								if(resp.mail=='true'){
									totalenviados=0;
									totalnosent=0;
									//progressbar_win.show();
									Ext.getCmp('action-upload').setValue('upload');
									Ext.getCmp('cantidadupload').setValue(ind);
									//sendMailTemplate(0);

									totalenviados=0;
									totalnosent=0;
									progressbar_win.show();
									sendMailTemplate(0);
								}else{
									winMailSett.show();
								}
							}
						});
					}else{
						Ext.Msg.alert('Warning', 'You must check at least one sending option '); 
						return false;
					}
				},
				id      : 'sendBut',
				hidden  : callback ? true : false
			},{
				text: 'Schedule Task',
				handler: function(){
					Ext.getCmp('action-upload').setValue('upload');
					Ext.getCmp('cantidadupload').setValue(ind);
					
					progressbar_win.show();
					totalnoschedule = 0;
					sendMailTemplate(0);
				},
				hidden: callback ? false : true
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	formEmail.insert(ind,att);
	var winTemplate = new Ext.Window({
		width       : 750,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		title		: 'Email Panel',
		listeners   : {
			'beforeshow' : function() {
				formEmail.getForm().reset();
			}
		},
		items       : [
			formEmail
		]
	});
	winTemplate.show();
}

function sendEmailSms(selected,grid,store,userid, progressbar_win, progressbar, selling, completetask,callback,values,selectedStatus){
	selling = selling || false;
	var follow = 'B', totalnoschedule = 0; 
	if(selling==true){
		follow = 'S';
	}
	completetask = completetask || false;
	
	function sendSms(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		Ext.getCmp('pid').setValue(selected[pid]);
		
		if(callback){
			if(selectedStatus[pid]['status'] != 'PENDING' && selectedStatus[pid]['status'] != '') totalnoschedule++;

			formSms.getForm().submit({
				timeout : 86400,
				url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				params	: {
					task: values.task, 
					detail: values.detail,
					odate: values.odate,
					ohour: values.ohour,
					parcelid: selectedStatus[pid]['pid'],
					status: selectedStatus[pid]['status'],
					typeExec: values.type,
					type: 'insert'
				},
				success : function(form, action) {
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close(); 
						Ext.Msg.alert("Follow Schedule", 'All Follow Schedule Task were created. '+totalnoschedule+' presented errors. <br>Please, verify them on the Pending Task tab');
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendSms(pid+1);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					winTemplate.close();
					Ext.MessageBox.alert('Warning', 'Error creating schedule task.');
				}
			});
		} else {
			formSms.getForm().submit({
				timeout : 86400,
				success : function(form, action) {
					if(action.result.enviado=='1'){
						totalenviados=totalenviados+1;
					}else{
						totalnosent++;
					}
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close(); 
						alert('SMS sent '+totalenviados+'\nSMS not sent '+totalnosent);
						if(store) store.reload();
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendSms(pid+1);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					winTemplate.close();
					Ext.MessageBox.alert('Warning', action.result.msg);
				}
			});
		}
	}
	
	var formSms = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendsms_email.php', 
		frame         : true,
		autoHeight  : true,
		monitorValid  : true,
		id            : 'formSms',
		labelWidth    : 70,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype: 'radiogroup',
				fieldLabel: 'SMS By',
				items: [
					{	
						boxLabel: 'Template', 
						name: 'rtemplateselect', 
						inputValue: 'T',
						listeners: {
							'check': function(r, check){
								if(check){
									var valor = Ext.getCmp('ccontracttemplate').getValue();
									getTemplateById(valor,userid,'sms',function(b){
										Ext.getCmp('sms_newtemplatetext').setValue(b);
										loading_win.hide();
									});
										
									Ext.getCmp('ccontracttemplate').setVisible(true);
								}else{
									Ext.getCmp('sms_newtemplatetext').setValue('');
									Ext.getCmp('ccontracttemplate').setVisible(false);
								}
							}
						}
					},
					{boxLabel: 'New', name: 'rtemplateselect', inputValue: 'N', checked: true}
				]
				
			},{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				hidden		  : true,
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'docstemplates',
						template_type: 3,
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							var sel=records[0].get('id');
 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel);
							loading_win.hide();
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 300,
				listeners     : {
					'select'  : function(combo,record,index){
						var valor = record.get('id');
						getTemplateById(valor,userid,'sms',function(b){
							Ext.getCmp('sms_newtemplatetext').setValue(b);
							loading_win.hide();
						});
					}
				}
			},{
				xtype	  :	'textarea',
				height	  : 200,
				width	  : 300,
				name	  : 'body',
				id		  : 'sms_newtemplatetext',	
				name	  : 'sms_newtemplatetext',	
				fieldLabel: 'Text',
				enableKeyEvents: true,
				autoScroll: true
			},{
				xtype: 'panel',
				layout: 'form',
				id: 'checkssms',
				hidden: true,
				labelWidth    : 180,	
				items: [{
					xtype         : 'checkbox',
					id            : 'sendcell',
					name          : 'sendcell',
					fieldLabel    : 'Send To Cell Phone only'
				},{
					xtype         : 'checkbox',
					id            : 'sendany',
					name          : 'sendany',
					fieldLabel    : 'Send to Cell Phone or first Available'
				}]
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			},{
				xtype         : 'hidden',
				name          : 'typeFollow',
				id            : 'typeFollow',
				value		  : follow
			},{
				xtype         : 'hidden',
				name          : 'completetask',
				id            : 'completetask',
				value		  : completetask
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					loading_win.show();
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php', 
						method  : 'POST',
						params: { module:'mailsettings',
								  action:''	
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							var resp   = Ext.decode(r.responseText)
							if(resp.mail=='true'){
								totalenviados=0;
								totalnosent=0;
								progressbar_win.show();
								sendSms(0);
							}else{
								winMailSett.show();
							}
						}
					});
				},
				id      : 'sendBut',
				hidden  : callback ? true : false
			},{
				text    : 'Schedule Task',
				handler : function() {
					progressbar_win.show();
					totalnoschedule = 0;
					sendSms(0);
				},
				hidden  : callback ? false : true
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	var winTemplate = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		title		: 'SMS Panel',
		listeners   : {
			'beforeshow' : function() {
				formSms.getForm().reset();
			}
		},
		items       : [
			formSms
		]
	});
	winTemplate.show(); 
	loading_win.show();
}

function sendEmailFax(selected,grid,store,userid, progressbar_win, progressbar, selling, completetask,callback,values,selectedStatus){
	selling = selling || false;
	var follow = 'B', totalnoschedule = 0;
	if(selling==true){
		follow = 'S';
	}
	completetask = completetask || false;
	var ind = 1;
	var att = {
		xtype: 'compositefield',
		fieldLabel: 'Attach',
		items: [
			{
				xtype: 'fileuploadfield',
				emptyText: 'Select a file',
				name: 'archivo'+ind,
				buttonText: 'Browse',
				width: 200
			},new Ext.Button({
				text: 'Another file',
				handler: function(){
					att.items[0].name = 'archivo'+(ind+1);

					formEmail.insert(ind,att);
					ind++;
					formEmail.doLayout();
				}
			})
		]
	};
	
	function sendMailTemplate(pid){
		progressbar.updateProgress(
			((pid+1)/selected.length),
			"Processing "+(pid+1)+" of "+selected.length
		);
		if(pid+1==selected.length)
			Ext.getCmp('action-delete').setValue('delete');
		Ext.getCmp('pid').setValue(selected[pid]);
		
		if(callback){
			if(selectedStatus[pid]['status'] != 'PENDING' && selectedStatus[pid]['status'] != '') totalnoschedule++;
			
			formEmail.getForm().submit({
				timeout : 86400,
				url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				params	: {
					task: values.task, 
					detail: values.detail,
					odate: values.odate,
					ohour: values.ohour,
					parcelid: selectedStatus[pid]['pid'],
					status: selectedStatus[pid]['status'],
					typeExec: values.type,
					type: 'insert'
				},
				success : function(form, action) {
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close(); 
						Ext.Msg.alert("Follow Schedule", 'All Follow Schedule Task were created. '+totalnoschedule+' presented errors. <br>Please, verify them on the Pending Task tab');
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendMailTemplate(pid+1);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					winTemplate.close();
					Ext.MessageBox.alert('Warning', 'Error creating schedule task.');
				}
			});
		} else {
			formEmail.getForm().submit({
				timeout : 86400,
				success : function(form, action) {
					if(action.result.enviado=='1'){
						totalenviados=totalenviados+1;
					}else{
						totalnosent++;
					}
					if((pid)==selected.length-1){
						progressbar_win.hide();
						winTemplate.close();
						alert('Fax sent '+totalenviados+'\nFax not sent '+totalnosent);
						if(store) store.reload();
					}else{
						Ext.getCmp('action-upload').setValue('');
						sendMailTemplate(pid+1);
					}
				},
				failure : function(form, action) {
					progressbar_win.hide();
					//winTemplate.close();
					Ext.MessageBox.alert('Warning', action.result.msg);
				}
			});
		}
	}
	var variableInsertarNewFax = '';
	
	var formEmail = new Ext.FormPanel({
		url           : 'mysetting_tabs/myfollowup_tabs/properties_followsendfax.php', 
		frame         : true,
		monitorValid  : true,
		fileUpload : true,
		id            : 'formEmail',
		labelWidth    : 70,
		waitMsgTarget : 'Generating...',
		items : [
			{
				xtype: 'radiogroup',
				fieldLabel: 'Fax By',
				items: [
					{	
						boxLabel: 'Template', 
						name: 'rtemplateselect', 
						inputValue: 'T',
						listeners: {
							'check': function(r, check){
								if(check){
									var valor = Ext.getCmp('ccontracttemplate').getValue();
									getTemplateById(valor,userid,'fax',function(b){
										Ext.getCmp('fax_newtemplatetext').setValue(b);
										loading_win.hide();
									});
									Ext.getCmp('ccontracttemplate').setVisible(true);
								}else{
									Ext.getCmp('fax_newtemplatetext').setValue('');
									Ext.getCmp('ccontracttemplate').setVisible(false);
								}
							}
						}
					},
					{boxLabel: 'New', name: 'rtemplateselect', inputValue: 'N', checked: true}
				]
			},{
				xtype         : 'combo',
				name          : 'ccontracttemplate',
				id            : 'ccontracttemplate',
				hiddenName    : 'contracttemplate',
				hidden		  : true,
				fieldLabel    : 'Template',
				typeAhead     : true,
				autoSelect    : true,
				mode          : 'local',
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'docstemplates',
						template_type: 4,
						'userid': userid
					},
					fields:[
						{name:'id', type:'int'},
						{name:'name', type:'string'},
						{name:'default', type:'int'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							loading_win.hide();
							var sel=records[0].get('id');
							//Ext.getCmp('ccontracttemplate').setValue(sel); 
							for(i=1;i<records.length;i++){
								if(records[i].get('default')==1)
									sel=records[i].get('id');
							}
							Ext.getCmp('ccontracttemplate').setValue(sel);
							loading_win.hide(); 
						}
					}
				}),
				triggerAction : 'all', 
				editable      : false,
				selectOnFocus : true,
				allowBlank    : false,
				displayField  : 'name',
				valueField    : 'id',
				value		  : 0,
				width         : 260,
				listeners     : {
					'select'  : function(combo,record,index){
						var valor = record.get('id');
						getTemplateById(valor,userid,'fax',function(b){
							Ext.getCmp('fax_newtemplatetext').setValue(b);
							loading_win.hide();
						});
					}
				}
			},{
				xtype	  :	'textarea',
				height	  : 200,
				width	  : 300,
				name	  : 'body',
				id		  : 'fax_newtemplatetext',		
				fieldLabel: 'Text',
				enableKeyEvents: true,
				autoScroll: true
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : userid
			},{
				xtype         : 'hidden',
				name          : 'pid',
				id            : 'pid'
			},{
				xtype         : 'hidden',
				name          : 'nuevotemplate',
				id            : 'nuevotemplate'
			},{
				xtype         : 'hidden',
				name          : 'action-upload',
				id            : 'action-upload'
			},{
				xtype         : 'hidden',
				name          : 'action-delete',
				id            : 'action-delete',
				value		  : ''
			},{
				xtype         : 'hidden',
				name          : 'cantidadupload',
				id            : 'cantidadupload'
			},{
				xtype         : 'hidden',
				name          : 'typeFollow',
				id            : 'typeFollow',
				value		  : follow
			},{
				xtype         : 'hidden',
				name          : 'completetask',
				id            : 'completetask',
				value		  : completetask
			}
			
		],
		style       : "text-align:left",  
		formBind    : true,
		buttonAlign : 'center', 
		buttons     : [
			{
				text    : 'Send',
				handler : function() {
					Ext.Ajax.request({
						url     : 'overview_contract/verifications.php', 
						method  : 'POST',
						params: { module:'mailsettings',
								  action:''	
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							var resp   = Ext.decode(r.responseText)
							if(resp.mail=='true'){
								
								Ext.getCmp('action-upload').setValue('upload');
								Ext.getCmp('cantidadupload').setValue(ind);

								totalenviados=0;
								totalnosent=0;
								progressbar_win.show();
								sendMailTemplate(0);
							}else{
								winMailSett.show();
							}
						}
					});
				},
				id      : 'sendBut',
				hidden  : callback ? true : false
			},{
				text: 'Schedule Task',
				handler: function(){
					Ext.getCmp('action-upload').setValue('upload');
					Ext.getCmp('cantidadupload').setValue(ind);
					
					progressbar_win.show();
					totalnoschedule = 0;
					sendMailTemplate(0);
				},
				hidden: callback ? false : true
			},{
				text    : 'Cancel',
				handler : function(){
					winTemplate.close();
				}
			}
		]
	});
	formEmail.insert(ind,att);
	var winTemplate = new Ext.Window({
		width       : 450,
		autoHeight  : true,
		modal       : true,
		plain       : true,
		title		: 'FAX Panel',
		listeners   : {
			'beforeshow' : function() {
				formEmail.getForm().reset();
			}
		},
		items       : [
			formEmail
		]
	});
	winTemplate.show(); 
	loading_win.show();
}

var wheremls=false;
function getContacts(selected,store,userid, progressbar_win, progressbar, contactpremiun, block, overwrite, premium, callback, cbparam1, cbparam2){ 
	var blockproperties = block == 1 ? 'no' : 'yes';
	var overwrite		= overwrite == 0 ? 'yes' : 'no';
	wheremls			= false;
	
	if(contactpremiun == 1){
		if(premium != 1){
			wheremls = true;
		}
		updateContacts(overwrite);
	}else{
		updateContacts(overwrite);
	}
	
	var COUNTBLOCKEDSPROPERTIES=0;
	var COUNTNOBLOCKEDSPROPERTIES=0;
	function updateContacts(btn){
		if(btn=='cancel'){
			return;
		}
		urlProgressBar = 'mysetting_tabs/myfollowup_tabs/properties_getcontacts.php?updatecontact='+btn+'&blockproperties='+blockproperties;
		countProgressBar = 0;
		typeProgressBar = 'Contact';
		progressbar.updateProgress(
			((countProgressBar+1)/selected.length),
			"Processing "+(countProgressBar+1)+" of "+selected.length
		);
		progressbar_win.show();
		var pidssend=selected[countProgressBar];
		countProgressBar++;

		Ext.Ajax.request({  
			waitMsg: 'Checking...',
			url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
			method: 'POST',
			timeout :86400, 
			params: { 
				userid:userid,
				pids: pidssend,
				wheremls: wheremls
			},
					
			failure:function(response,options){
				progressbar_win.hide();
				Ext.MessageBox.alert('Warning','Operation Failure');
			},
			success:function(response,options){								
				var respresiden = Ext.decode(response.responseText);

				if(respresiden.mlsresidential==true )//|| respresiden.num==0 || respresiden.num=='0'
				{
					Ext.Ajax.request({  
						waitMsg: 'Checking...',
						url: urlProgressBar+'&sp=NOSPone', 
						method: 'POST',
						timeout :86400, 
						params: { 
							pids: '\''+pidssend+'\''
							,'agent':respresiden.agent
							,'agentph':respresiden.agentph
							,'agentcell':respresiden.agentcell
							,'agentfax':respresiden.agentfax
							,'agentemail':respresiden.agentemail
							,'agenttollfree':respresiden.agenttollfree
							,'office':respresiden.office
							,'officeph':respresiden.officeph
							,'officefax':respresiden.officefax
							,'officeemail':respresiden.officeemail
							,'officetollfree':respresiden.officetollfree										
							,'runspider':respresiden.runspider										
							,'mlsresidential':respresiden.mlsresidential
							,'userid':userid
						},										
						failure:function(response1,options1){
							progressbar_win.hide();
							var output = '';
							for (prop in response1) {
							  output += prop + ': ' + response1[prop]+'; ';
							}
							Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
						},
						success: ejecutarUpdatesGetContact
					});										
				}
				else
				{							
					var client = new XMLHttpRequest();
					client.onreadystatechange = function () {
						if (this.readyState == 4 && this.status == 200) {
							progressbar_win.hide();
							var resp = Ext.decode(this.responseText);
							
							Ext.Ajax.request({  
								waitMsg: 'Checking...',
								url: urlProgressBar, 
								method: 'POST',
								timeout :86400, 
								params: { 
									pids: '\''+pidssend+'\''
									,'agent':resp.agent
									,'agentph':resp.agentph
									,'agentcell':resp.agentcell
									,'agentfax':resp.agentfax
									,'agentemail':resp.agentemail
									,'agenttollfree':resp.agenttollfree
									,'office':resp.office
									,'officeph':resp.officeph
									,'officefax':resp.officefax
									,'officeemail':resp.officeemail
									,'officetollfree':resp.officetollfree										
									,'runspider':resp.runspider										
									,'mlsresidential':resp.mlsresidential
									,'userid':userid										
								},
								
								failure:function(response2,options2){
									progressbar_win.hide();
									var output = '';
									for (prop in response2) {
									  output += prop + ': ' + response2[prop]+'; ';
									}
									Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
								},
								success: ejecutarUpdatesGetContact
							});										
						}
					};
					client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+userid+'&p='+pidssend+'&sp=YESSPone');
					client.send();									
				}//else if(resp.inresidential==true)
			}
		});						
	}
	function ejecutarUpdatesGetContact(response,options){
		progressbar.updateProgress(
			((countProgressBar+1)/selected.length),
			"Processing "+(countProgressBar+1)+" of "+selected.length
		);		
		progressbar_win.show();

		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			
			if(resp.blockproperties==1 || resp.blockproperties=='1' ) 
				COUNTBLOCKEDSPROPERTIES=COUNTBLOCKEDSPROPERTIES+1;
			else  
				COUNTNOBLOCKEDSPROPERTIES=COUNTNOBLOCKEDSPROPERTIES+1;
			
			if((countProgressBar)>=selected.length){
				progressbar_win.hide();
				store.reload();
				
				if(callback) callback(userid,cbparam1,cbparam2,progressbar,progressbar_win,store);
				//else Ext.Msg.alert('Success','The System updated the properties\'s contact <br>and changed the status of some properties that are no longer \'For sale\'. <br/>'+COUNTBLOCKEDSPROPERTIES+' Properties blocked');
				else Ext.Msg.alert('Success','The System updated the properties\'s contact.<br/>'+COUNTBLOCKEDSPROPERTIES+' Properties blocked');
			}else{
				var pidssend=selected[countProgressBar];
				countProgressBar++;

						Ext.Ajax.request({  
							waitMsg: 'Checking...',
							url:'mysetting_tabs/myfollowup_tabs/checkParcelidMlsresidential.php',
							method: 'POST',
							timeout :86400, 
							params: { 
								userid:useridspider,
								pids: pidssend,
								wheremls: wheremls
							},
									
							failure:function(response,options){
								progressbar_win.hide();
								Ext.MessageBox.alert('Warning','Operation Failure');
							},
							success:function(response,options){								
								var respresiden = Ext.decode(response.responseText);

								
								if(respresiden.mlsresidential==true )//|| respresiden.num==0 || respresiden.num=='0')
								{
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url: urlProgressBar+'&sp=NOSPtwiceomore', 
										method: 'POST',
										timeout :86400, 
										params: { 
											pids: '\''+pidssend+'\''
											,'agent':respresiden.agent
											,'agentph':respresiden.agentph
											,'agentcell':respresiden.agentcell
											,'agentfax':respresiden.agentfax
											,'agentemail':respresiden.agentemail
											,'agenttollfree':respresiden.agenttollfree
											,'office':respresiden.office
											,'officeph':respresiden.officeph
											,'officefax':respresiden.officefax
											,'officeemail':respresiden.officeemail
											,'officetollfree':respresiden.officetollfree										
											,'runspider':respresiden.runspider										
											,'mlsresidential':respresiden.mlsresidential
											,'userid':userid										
										},										
										failure:function(response1,options1){
											progressbar_win.hide();
											var output = '';
											for (prop in response1) {
											  output += prop + ': ' + response1[prop]+'; ';
											}
											Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
										},
										success:ejecutarUpdatesGetContact
									});										
								}
								else
								
								{							
									var client = new XMLHttpRequest();
									client.onreadystatechange = function () {
										if (this.readyState == 4 && this.status == 200) {
											progressbar_win.hide();
											var resp = Ext.decode(this.responseText);
											
											Ext.Ajax.request({  
												waitMsg: 'Checking...',
												url: urlProgressBar, 
												method: 'POST',
												timeout :86400, 
												params: { 
													pids: '\''+pidssend+'\''
													,'agent':resp.agent
													,'agentph':resp.agentph
													,'agentcell':resp.agentcell
													,'agentfax':resp.agentfax
													,'agentemail':resp.agentemail
													,'agenttollfree':resp.agenttollfree
													,'office':resp.office
													,'officeph':resp.officeph
													,'officefax':resp.officefax
													,'officeemail':resp.officeemail
													,'officetollfree':resp.officetollfree										
													,'runspider':resp.runspider										
													,'mlsresidential':resp.mlsresidential
													,'userid':userid									
												},
												
												failure:function(response2,options2){
													progressbar_win.hide();
													var output = '';
													for (prop in response2) {
													  output += prop + ': ' + response2[prop]+'; ';
													}
													Ext.MessageBox.alert('Warning','Operation Failure. Try Again');
												},
												success:ejecutarUpdatesGetContact
											});										
										}
									};
									client.open('GET', 'mysetting_tabs/myfollowup_tabs/properties_getcontactsdetail.php?u='+useridspider+'&p='+pidssend+'&sp=YESSPtwiceomore');
									client.send();							
								}//else if(resp.inresidential==true)
							}
						});						
				
				
			}
		}else{
			progressbar_win.hide();
			if(callback) callback(userid,cbparam1,cbparam2,progressbar,progressbar_win,store);
		}	
	}
}

function creaVentana(dato,r,pid,userid,multiple){
	multiple = multiple || false;
	var ocultar=false;
	var princ='';
	var title='';
	if(r.records[dato]==null){
		r.records[dato]=new Array();
		ocultar=true;
		title='New Contact';
		r.records[dato].parcelid=pid;
	}else{
		title='Contact '+(dato+1)+' of '+r.total;
	}
	if(r.records[dato].principal=='1'){
		princ='(Default)';
		title='Contact '+(dato+1)+' of '+r.total+' '+princ;
	}
	var simple = new Ext.FormPanel({
		url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
		frame: true,
		title: title,
		width: 450,
		waitMsgTarget : 'Waiting...',
		labelWidth: 75,
		//defaults: {width: 350},
		labelAlign: 'left',
		items: [{
				xtype     : 'combo',
				//hidden: !ocultar,
				fieldLabel: 'Contact',
				triggerAction: 'all',
				//forceSelection: true,
				width: 250, 
				selectOnFocus: true,
				typeAhead: true,
				typeAheadDelay: 10,
				minChars: 0,
				mode: 'remote',
				store: new Ext.data.JsonStore({
					url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					forceSelection: true,
					root: 'records',
					baseParams		: {
						'userid'	:  userid,
						'sort'		: 'agent',
						'dir'		: 'ASC'
					},
					id: 0,
					fields: [
						'agentid',
						'agent'
					]
				}),
				valueField: 'agentid',
				displayField: 'agent',
				name          : 'fagent',
				value         : r.records[dato].agent, //agent.get('agentype'),
				hiddenName    : 'agent',
				id			  : 'nameagent',
				readOnly	  : !ocultar,
				editable	  : ocultar, 
				allowBlank    : false,
				hiddenValue   : r.records[dato].agentid, //agent.get('agenttype'),
				listeners	  : {
					select: function(combo,record,index){
						var agentid=record.get('agentid');
						//return;
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :86400,
							params: { 
								userid: userid,
								agentid: agentid
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var r1=Ext.decode(response.responseText);
								if(r1.total>0){
									var value=r1.records[0];
									simple.getForm().findField('agenttype').setValue(value.agenttype);
									simple.getForm().findField('company').setValue(value.company);
									simple.getForm().findField('typeemail1').setValue(value.typeemail1);
									simple.getForm().findField('typeemail2').setValue(value.typeemail2);
									simple.getForm().findField('email').setValue(value.email);
									simple.getForm().findField('email2').setValue(value.email2);
									simple.getForm().findField('typeurl1').setValue(value.typeurl1);
									simple.getForm().findField('urlsend').setValue(value.urlsend);
									simple.getForm().findField('typeurl2').setValue(value.typeurl2);
									simple.getForm().findField('urlsend2').setValue(value.urlsend2);
									simple.getForm().findField('typeph1').setValue(value.typeph1);
									simple.getForm().findField('phone1').setValue(value.phone1);
									simple.getForm().findField('typeph2').setValue(value.typeph2);
									simple.getForm().findField('phone2').setValue(value.phone2);
									simple.getForm().findField('typeph3').setValue(value.typeph3);
									simple.getForm().findField('phone3').setValue(value.phone3);
									simple.getForm().findField('typeph4').setValue(value.typeph4);
									simple.getForm().findField('fax').setValue(value.fax);
									simple.getForm().findField('typeph5').setValue(value.typeph5);
									simple.getForm().findField('tollfree').setValue(value.tollfree);
									simple.getForm().findField('typeph6').setValue(value.typeph6);
									simple.getForm().findField('phone6').setValue(value.phone6);
									simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
									simple.getForm().findField('address1').setValue(value.address1);
									simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
									simple.getForm().findField('address2').setValue(value.address2);
									simple.getForm().findField('type').setValue('insert');
									simple.getForm().findField('agentid').setValue(value.agentid);
									//simple.getForm().findField('pid').setValue('');
								}
							}
						});
					},
					change: function(combo, newvalue,oldvalue){
						if(isNaN(newvalue)){
							Ext.getCmp('nameagent').setReadOnly(false);
							Ext.getCmp('nameagent').setEditable(true);
							simple.getForm().findField('agenttype').setValue('Agent');
							simple.getForm().findField('company').setValue('');
							simple.getForm().findField('typeemail1').setValue(0);
							simple.getForm().findField('typeemail2').setValue(0);
							simple.getForm().findField('email').setValue('');
							simple.getForm().findField('email2').setValue('');
							simple.getForm().findField('typeurl1').setValue(0);
							simple.getForm().findField('urlsend').setValue('');
							simple.getForm().findField('typeurl2').setValue(0);
							simple.getForm().findField('urlsend2').setValue('');
							simple.getForm().findField('typeph1').setValue(0);
							simple.getForm().findField('phone1').setValue('');
							simple.getForm().findField('typeph2').setValue(0);
							simple.getForm().findField('phone2').setValue('');
							simple.getForm().findField('typeph3').setValue(0);
							simple.getForm().findField('phone3').setValue('');
							simple.getForm().findField('typeph4').setValue(0);
							simple.getForm().findField('fax').setValue('');
							simple.getForm().findField('typeph5').setValue(0);
							simple.getForm().findField('tollfree').setValue('');
							simple.getForm().findField('typeph6').setValue(0);
							simple.getForm().findField('phone6').setValue('');
							simple.getForm().findField('typeaddress1').setValue(0);
							simple.getForm().findField('address1').setValue('');
							simple.getForm().findField('typeaddress2').setValue(0);
							simple.getForm().findField('address2').setValue('');
							simple.getForm().findField('agentid').setValue('')
						}
					}
				}
			},{
				xtype         : 'combo',
				mode          : 'remote',
				fieldLabel    : 'Type',
				triggerAction : 'all',
				width		  : 130,
				editable	  : false,
				store         : new Ext.data.JsonStore({
					id:'storetype',
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'agenttype',
						'userid': userid
					},
					fields:[
						{name:'idtype', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php'
				}),
				displayField  : 'name',
				valueField    : 'idtype',
				name          : 'fagenttype',
				value         : r.records[dato].agentype, //agent.get('agentype'),
				hiddenName    : 'agenttype',
				hiddenValue   : r.records[dato].agenttype, //agent.get('agenttype'),
				allowBlank    : false,
				listeners	  : {
					beforequery: function(qe){
						delete qe.combo.lastQuery;
					}
				}
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Email',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,	
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyemail1',
					value	  : r.records[dato].typeemail1,
					hiddenName    : 'typeemail1',
					hiddenValue   : r.records[dato].typeemail1,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'email',
					value	  : r.records[dato].email,
					width	  : 165
				}]
			},{

				xtype	  : 'compositefield',
				fieldLabel: 'Email 2',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyemail2',
					value         : r.records[dato].typeemail2,
					hiddenName    : 'typeemail2',
					hiddenValue   : r.records[dato].typeemail2,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'email2',
					value	  : r.records[dato].email2,
					width	  : 165
				}]
			},{
				xtype     : 'textfield',
				width	  : 250,
				name      : 'company',
				fieldLabel: 'Company',
				value	  : r.records[dato].company
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Website 1',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Personal'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyurl1',
					value         : r.records[dato].typeurl1,
					hiddenName    : 'typeurl1',
					hiddenValue   : r.records[dato].typeurl1,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'urlsend',
					value     : r.records[dato].urlsend,
					width	  : 165
				}]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Website 2',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Personal'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyurl2',
					value         : r.records[dato].typeurl2,
					hiddenName    : 'typeurl2',
					hiddenValue   : r.records[dato].typeurl2,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'urlsend2',
					value     : r.records[dato].urlsend2,
					width	  : 165
				}]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname1',
					value         : r.records[dato].typeph1,
					hiddenName    : 'typeph1',
					hiddenValue   : r.records[dato].typeph1,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'phone1',
					width	  : 165,
					value	  : r.records[dato].phone1
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph1==3 || r.records[dato].typeph1==6 || r.records[dato].typeph1==7 || r.records[dato].phone1==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].phone1,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to plivo call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!plivoCallEnabled || r.records[dato].typeph1==3 || r.records[dato].typeph1==6 || r.records[dato].typeph1==7 || r.records[dato].phone1==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/plico_call.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var plivophone = r.records[dato].phone1;
						plivophone = formatUSNumber(plivophone);
						
						plivocall(userid,plivophone,multiple,pid,r.records[dato].agent);
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph1==3 || r.records[dato].typeph1==6 || r.records[dato].phone1==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].phone1;
						
						if(r.records[dato].typeph1 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;		
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone 2',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname2',
					value         : r.records[dato].typeph2,
					hiddenName    : 'typeph2',
					hiddenValue   : r.records[dato].typeph2,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'phone2',
					width	  : 165,
					value	  : r.records[dato].phone2
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph2==3 || r.records[dato].typeph2==6 || r.records[dato].typeph2==7 || r.records[dato].phone2==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].phone2,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph2==3 || r.records[dato].typeph2==6 || r.records[dato].typeph2==7 || r.records[dato].phone2==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].phone2,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph2==3 || r.records[dato].typeph2==6 || r.records[dato].phone2==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].phone2;
						
						if(r.records[dato].typeph2 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');							
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone 3',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname3',
					value         : r.records[dato].typeph3,
					hiddenName    : 'typeph3',
					hiddenValue   : r.records[dato].typeph3,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'phone3',
					width	  : 165,
					value	  : r.records[dato].phone3
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph3==3 || r.records[dato].typeph3==6 || r.records[dato].typeph3==7 || r.records[dato].phone3==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].phone3,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to plivo call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!plivoCallEnabled || r.records[dato].typeph3==3 || r.records[dato].typeph3==6 || r.records[dato].typeph3==7 || r.records[dato].phone3==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/plico_call.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var plivophone = r.records[dato].phone3;
						plivophone = formatUSNumber(plivophone);
						
						plivocall(userid,plivophone,multiple,pid,r.records[dato].agent);
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph3==3 || r.records[dato].typeph3==6 || r.records[dato].phone3==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].phone3;
						
						if(r.records[dato].typeph3 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');							
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone 4',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname4',
					value         : r.records[dato].typeph4,
					hiddenName    : 'typeph4',
					hiddenValue   : r.records[dato].typeph4,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'fax',
					width	  : 165,
					value	  : r.records[dato].fax
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph4==3 || r.records[dato].typeph4==6 || r.records[dato].typeph4==7 || r.records[dato].fax==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].fax,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to plivo call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!plivoCallEnabled || r.records[dato].typeph4==3 || r.records[dato].typeph4==6 || r.records[dato].typeph4==7 || r.records[dato].fax==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/plico_call.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var plivophone = r.records[dato].fax;
						plivophone = formatUSNumber(plivophone);
						
						plivocall(userid,plivophone,multiple,pid,r.records[dato].agent);
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph4==3 || r.records[dato].typeph4==6 || r.records[dato].fax==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].fax;
						
						if(r.records[dato].typeph4 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;	
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');						
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone 5',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname5',
					value         : r.records[dato].typeph5,
					hiddenName    : 'typeph5',
					hiddenValue   : r.records[dato].typeph5,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'tollfree',
					width	  : 165,
					value	  : r.records[dato].tollfree
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph5==3 || r.records[dato].typeph5==6 || r.records[dato].typeph5==7 || r.records[dato].tollfree==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].tollfree,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to plivo call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!plivoCallEnabled || r.records[dato].typeph5==3 || r.records[dato].typeph5==6 || r.records[dato].typeph5==7 || r.records[dato].tollfree==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/plico_call.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var plivophone = r.records[dato].tollfree;
						plivophone = formatUSNumber(plivophone);
						
						plivocall(userid,plivophone,multiple,pid,r.records[dato].agent);
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph5==3 || r.records[dato].typeph5==6 || r.records[dato].tollfree==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].tollfree;
						
						if(r.records[dato].typeph5 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');							
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Phone 6',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office'],
							['2','Cell'],
							['3','Home Fax'],
							['6','Office Fax'],
							['4','TollFree'],
							['5','O. TollFree'],
							['7','Skype']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'typephname6',
					value         : r.records[dato].typeph6,
					hiddenName    : 'typeph6',
					hiddenValue   : r.records[dato].typeph6,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'phone6',
					width	  : 165,
					value	  : r.records[dato].phone6
				},new Ext.Button({
					tooltip: 'Click to call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (r.records[dato].typeph6==3 || r.records[dato].typeph6==6 || r.records[dato].typeph6==7 || r.records[dato].phone6==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/gvoice.png',
					handler: function(){
						makeCall(userid,r.records[dato].phone6,multiple,pid,r.records[dato].agent);
						if(multiple==true){
							win.close();
						}
					}
				}),new Ext.Button({
					tooltip: 'Click to plivo call to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!plivoCallEnabled || r.records[dato].typeph6==3 || r.records[dato].typeph6==6 || r.records[dato].typeph6==7 || r.records[dato].phone6==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/plico_call.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var plivophone = r.records[dato].phone6;
						plivophone = formatUSNumber(plivophone);
						
						plivocall(userid,plivophone,multiple,pid,r.records[dato].agent);
					}
				}),new Ext.Button({
					tooltip: 'Click to skype to this number.',
					iconCls:'icon',
					iconAlign: 'top',
					hidden: (!skypeCallEnabled || r.records[dato].typeph6==3 || r.records[dato].typeph6==6 || r.records[dato].phone6==''),
					width: 30,
					icon: 'http://www.reifax.com/img/toolbar/Skypeicon_16px.png',
					handler: function(){
						if(multiple==true){
							win.close();
						}
						var phoneskype = r.records[dato].phone6;
						
						if(r.records[dato].typeph6 != 7 && phoneskype[0] != '+'){
							phoneskype = '+1'+phoneskype;
							phoneskype = phoneskype.replace('-','');
							phoneskype = phoneskype.replace('(','');
							phoneskype = phoneskype.replace(')','');
							phoneskype = phoneskype.replace(' ','');							
						}
						
						skypecall(userid,phoneskype,multiple,pid,r.records[dato].agent);
						if(phoneskype)
							document.location = 'skype:'+phoneskype.replace(' ','')+'?call';
						else
							document.location = 'skype:'+phoneskype+'?call';
					}
				})]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Address 1',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyaddress1',
					value	  : r.records[dato].typeaddress1,
					hiddenName    : 'typeaddress1',
					hiddenValue   : r.records[dato].typeaddress1,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'address1',
					value	  : r.records[dato].address1,
					width	  : 165
				}]
			},{
				xtype	  : 'compositefield',
				fieldLabel: 'Address 2',
				items	  : [{
					xtype         : 'combo',
					mode          : 'local',
					triggerAction : 'all',
					width		  : 120,
					editable	  : false,
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['0','Home'],
							['1','Office']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'tyaddress2',
					value         : r.records[dato].typeaddress2,
					hiddenName    : 'typeaddress2',
					hiddenValue   : r.records[dato].typeaddress2,
					allowBlank    : false
				},{
					xtype     : 'textfield',
					name      : 'address2',
					value	  : r.records[dato].address2,
					width	  : 165
				}]
			},{
				xtype     : 'hidden',  
				name      : 'type'
			},{
				xtype     : 'hidden',
				name      : 'agentid',
				value     : r.records[dato].agentid
			},{
				xtype     : 'hidden',
				name      : 'pid',
				value     : pid
			},{
				xtype     : 'hidden',
				name      : 'userid',
				value     : userid
			}],
			buttons: [{
				text     : 'Save',
				id: 'agentsave',
				hidden	 : !ocultar,
				handler  : function(){
					if (!simple.getForm().isValid()) {
						return false;
					}
					loading_win.show();
					if(simple.getForm().findField('agentid').getValue()==''){
						simple.getForm().findField('type').setValue('insert'); 
						simple.getForm().submit({
							success: function(form, action) {
									if(action.result.agentid!=''){
										Ext.Ajax.request( 
										{  
											waitMsg: 'Adding...',
											url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
											method: 'POST',
											timeout :86400,
											params: { 
												type	: 'assignment-add',
												agentid	: action.result.agentid, 
												userid: userid,
												pid		: simple.getForm().findField('pid').getValue() 
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												var resp=Ext.decode(response.responseText);
												if(resp.existe==0){
													alert('Contact inserted.');
												}else{
													alert('Contact already assigned.');
												}
												pid=simple.getForm().findField('pid').getValue();
												win.close();
												loading_win.show();
												Ext.Ajax.request({
													waitMsg: 'Seeking...',
													url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
													method: 'POST',
													timeout :86400,
													params: { 
														pid: pid, 
														userid: userid,
														type: 'assignment'
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														loading_win.hide();
														var r=Ext.decode(response.responseText);
														creaVentana(0,r,pid,userid,multiple);
													}
												});
											}                                
										});
									}
								
							},
							failure: function(form, action) {
								loading_win.hide();
								if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
									Ext.Msg.alert('Error',
										'Status:'+action.response.status+': '+
										action.response.statusText);
								}
								if (action.failureType === Ext.form.Action.SERVER_INVALID){
									// server responded with success = false
									Ext.Msg.alert('Invalid', action.result.errormsg);
								}
								if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
									Ext.Msg.alert('Error',
										'Please check de red market field, before insert contact.');
								}
							}
						});
					}else{
						Ext.Ajax.request( 
						{  
							waitMsg: 'Adding...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :86400,
							params: { 
								type	: 'assignment-add',
								agentid	: simple.getForm().findField('agentid').getValue(), 
								userid: userid,
								pid		: simple.getForm().findField('pid').getValue() 
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								var resp=Ext.decode(response.responseText);
								if(resp.existe==0){
									alert('Contact inserted.');
								}else{
									alert('Contact already assigned.');
								}
								pid=simple.getForm().findField('pid').getValue();
								win.close();
								loading_win.show();
								Ext.Ajax.request({
									waitMsg: 'Seeking...',
									url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
									method: 'POST',
									timeout :86400,
									params: { 
										pid: pid,
										userid: userid,
										type: 'assignment'
									},
									
									failure:function(response,options){
										loading_win.hide();
										Ext.MessageBox.alert('Warning','ERROR');
									},
									success:function(response,options){
										loading_win.hide();
										var r=Ext.decode(response.responseText);
										creaVentana(0,r,pid,userid,multiple);
									}
								});
							}                                
						});
					}
				}
			},{
				text     : 'Cancel',
				id: 'agentcancel',
				hidden	 : !ocultar,
				handler  : function(){
					pid=simple.getForm().findField('pid').getValue();
					win.close();
					creaVentana(0,r,pid,userid,multiple);
				}
			},{
				text     : 'Add',
				id: 'agentadd',
				hidden	 : ocultar,
				handler  : function(){
				
					Ext.getCmp('agentsave').setVisible(true);
					Ext.getCmp('agentcancel').setVisible(true);
					Ext.getCmp('agentadd').setVisible(false);
					Ext.getCmp('agentupdate').setVisible(false);
					Ext.getCmp('agentremove').setVisible(false);
					Ext.getCmp('agentset').setVisible(false);
					Ext.getCmp('agentfirst').setVisible(false);
					Ext.getCmp('agentprev').setVisible(false);
					Ext.getCmp('agentnext').setVisible(false);
					Ext.getCmp('agentlast').setVisible(false);
					simple.setTitle('New Contact');
					Ext.getCmp('nameagent').setReadOnly(false);
					Ext.getCmp('nameagent').setEditable(true);
					simple.getForm().findField('agent').setValue('');
					simple.getForm().findField('agenttype').setValue('Agent');
					simple.getForm().findField('company').setValue('');
					simple.getForm().findField('typeemail1').setValue(0);
					simple.getForm().findField('typeemail2').setValue(0);
					simple.getForm().findField('email').setValue('');
					simple.getForm().findField('email2').setValue('');
					simple.getForm().findField('typeurl1').setValue(0);
					simple.getForm().findField('urlsend').setValue('');
					simple.getForm().findField('typeurl2').setValue(0);
					simple.getForm().findField('urlsend2').setValue('');
					simple.getForm().findField('typeph1').setValue(0);
					simple.getForm().findField('phone1').setValue('');
					simple.getForm().findField('typeph2').setValue(0);
					simple.getForm().findField('phone2').setValue('');
					simple.getForm().findField('typeph3').setValue(0);
					simple.getForm().findField('phone3').setValue('');
					simple.getForm().findField('typeph4').setValue(0);
					simple.getForm().findField('fax').setValue('');
					simple.getForm().findField('typeph5').setValue(0);
					simple.getForm().findField('tollfree').setValue('');
					simple.getForm().findField('typeph6').setValue(0);
					simple.getForm().findField('phone6').setValue('');
					simple.getForm().findField('typeaddress1').setValue(0);
					simple.getForm().findField('address1').setValue('');
					simple.getForm().findField('typeaddress2').setValue(0);
					simple.getForm().findField('address2').setValue('');
					simple.getForm().findField('type').setValue('insert');
					simple.getForm().findField('agentid').setValue('');
					//simple.getForm().findField('pid').setValue('');
				}
			},{
				text     : 'Update',
				id: 'agentupdate',
				hidden	 : ocultar,
				handler  : function(){
					if (!simple.getForm().isValid()) {
						return false;
					}
					simple.getForm().findField('type').setValue('update');
					loading_win.show();
					simple.getForm().submit({
						success: function(form, action) {
							loading_win.hide();
							pid=simple.getForm().findField('pid').getValue();
							win.close();
							//Actualizar los grid de los pending task por si se corrigio alguno.
							if(typeof(storemyfollowtask) != "undefined") storemyfollowtask.reload();
							if(typeof(storemysellingfollowtask) != "undefined") storemysellingfollowtask.reload();
							
							alert('Contact Successfully Updated.');
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :86400,
								params: { 
									pid: pid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(dato,r,pid,userid,multiple);
								}
							});
						},
						failure: function(form, action) {
							loading_win.hide();
							if (action.failureType === Ext.form.Action.CONNECT_FAILURE) {
								Ext.Msg.alert('Error',
									'Status:'+action.response.status+': '+
									action.response.statusText);
							}
							if (action.failureType === Ext.form.Action.SERVER_INVALID){
								// server responded with success = false
								Ext.Msg.alert('Invalid', action.result.errormsg);
							}
							if (action.failureType === Ext.form.Action.CLIENT_INVALID) {
								Ext.Msg.alert('Error',
									'Please check de red market field, before update contact.');
							}
						}
					});	
				}
			},{
				text     : 'Remove',
				id: 'agentremove',
				hidden	 : ocultar,
				handler  : function(){
					
					//simple.getForm().findField('type').setValue('assignment-del');
					Ext.Ajax.request( 
					{  
						waitMsg: 'Removing...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
						method: 'POST',
						timeout :86400,
						params: { 
							type	: 'assignment-del',
							agentid	: r.records[dato].agentid, //agentid,
							userid	: r.records[dato].userid, //userid,
							pid		: r.records[dato].parcelid, //pid
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							alert('Contact Successfully Removed.');
							pid=simple.getForm().findField('pid').getValue();
							win.close();
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :86400,
								params: { 
									pid: r.records[dato].parcelid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(0,r,pid,userid,multiple);
								}
							});
						}                                
					});		
				}
			},{
				text     : 'Set Principal',
				id: 'agentset',
				hidden	 : ocultar,
				handler  : function(){
					var agentname = r.records[dato].agent;//agent.get('agent');
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Assigning...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
						method: 'POST',
						timeout :86400,
						params: { 
							type		: 'assignment-principal',
							agentname	: agentname,
							userid		: r.records[dato].userid,
							pid			: r.records[dato].parcelid
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						success:function(response,options){
							loading_win.hide();
							alert('Contact Successfully Updated.');
							pid=simple.getForm().findField('pid').getValue();
							win.close();
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :86400,
								params: { 
									pid: r.records[dato].parcelid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(0,r,pid,userid,multiple);
								}
							});
						}                                
					});
				}
			}]
	});
	 
	var win = new Ext.Window({
		layout      : 'fit',
		width       : 470, 
		height      : 600,
		modal	 	: true,
		plain       : true,
		items		: simple,
		closeAction : 'close',
		buttons: [{
				text     : '|<',
				id: 'agentfirst',
				width: 30, 
				hidden	 : (dato==0), 	
				handler  : function(){
					
					dato=0;
					var value=r.records[dato];
					Ext.getCmp('agentfirst').setVisible(!(dato==0));
					Ext.getCmp('agentprev').setVisible(!(dato-1<0));
					Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
					Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
					var princ='';
					if(value.principal=='1')
						princ='(Default)';
					simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
					simple.getForm().findField('agent').setValue(value.agent);
					simple.getForm().findField('agenttype').setValue(value.agenttype);
					simple.getForm().findField('company').setValue(value.company);
					simple.getForm().findField('typeemail1').setValue(value.typeemail1);
					simple.getForm().findField('typeemail2').setValue(value.typeemail2);
					simple.getForm().findField('email').setValue(value.email);
					simple.getForm().findField('email2').setValue(value.email2);
					simple.getForm().findField('typeurl1').setValue(value.typeurl1);
					simple.getForm().findField('urlsend').setValue(value.urlsend);
					simple.getForm().findField('typeurl2').setValue(value.typeurl2);
					simple.getForm().findField('urlsend2').setValue(value.urlsend2);
					simple.getForm().findField('typeph1').setValue(value.typeph1);
					simple.getForm().findField('phone1').setValue(value.phone1);
					simple.getForm().findField('typeph2').setValue(value.typeph2);
					simple.getForm().findField('phone2').setValue(value.phone2);
					simple.getForm().findField('typeph3').setValue(value.typeph3);
					simple.getForm().findField('phone3').setValue(value.phone3);
					simple.getForm().findField('typeph4').setValue(value.typeph4);
					simple.getForm().findField('fax').setValue(value.fax);
					simple.getForm().findField('typeph5').setValue(value.typeph5);
					simple.getForm().findField('tollfree').setValue(value.tollfree);
					simple.getForm().findField('typeph6').setValue(value.typeph6);
					simple.getForm().findField('phone6').setValue(value.phone6);
					simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
					simple.getForm().findField('address1').setValue(value.address1);
					simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
					simple.getForm().findField('address2').setValue(value.address2);
					simple.getForm().findField('type').setValue('update');
					simple.getForm().findField('agentid').setValue(value.agentid);	
				}
			},{
				text     : '<',
				id: 'agentprev',
				width: 30, 
				hidden	 : (dato-1<0), 	
				handler  : function(){
					
					dato=dato-1;
					var value=r.records[dato];
					Ext.getCmp('agentfirst').setVisible(!(dato==0));
					Ext.getCmp('agentprev').setVisible(!(dato-1<0));
					Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
					Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
					var princ='';
					if(value.principal=='1')
						princ='(Default)';
					simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
					simple.getForm().findField('agent').setValue(value.agent);
					simple.getForm().findField('agenttype').setValue(value.agenttype);
					simple.getForm().findField('company').setValue(value.company);
					simple.getForm().findField('typeemail1').setValue(value.typeemail1);
					simple.getForm().findField('typeemail2').setValue(value.typeemail2);
					simple.getForm().findField('email').setValue(value.email);
					simple.getForm().findField('email2').setValue(value.email2);
					simple.getForm().findField('typeurl1').setValue(value.typeurl1);
					simple.getForm().findField('urlsend').setValue(value.urlsend);
					simple.getForm().findField('typeurl2').setValue(value.typeurl2);
					simple.getForm().findField('urlsend2').setValue(value.urlsend2);
					simple.getForm().findField('typeph1').setValue(value.typeph1);
					simple.getForm().findField('phone1').setValue(value.phone1);
					simple.getForm().findField('typeph2').setValue(value.typeph2);
					simple.getForm().findField('phone2').setValue(value.phone2);
					simple.getForm().findField('typeph3').setValue(value.typeph3);
					simple.getForm().findField('phone3').setValue(value.phone3);
					simple.getForm().findField('typeph4').setValue(value.typeph4);
					simple.getForm().findField('fax').setValue(value.fax);
					simple.getForm().findField('typeph5').setValue(value.typeph5);
					simple.getForm().findField('tollfree').setValue(value.tollfree);
					simple.getForm().findField('typeph6').setValue(value.typeph6);
					simple.getForm().findField('phone6').setValue(value.phone6);
					simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
					simple.getForm().findField('address1').setValue(value.address1);
					simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
					simple.getForm().findField('address2').setValue(value.address2);
					simple.getForm().findField('type').setValue('update');
					simple.getForm().findField('agentid').setValue(value.agentid);	
				}
			},{
				text     : '>',
				id: 'agentnext',
				width: 30, 
				hidden	 : (dato+1>r.total-1),
				handler  : function(){
					
					dato=dato+1;
					var value=r.records[dato];
					Ext.getCmp('agentfirst').setVisible(!(dato==0));
					Ext.getCmp('agentprev').setVisible(!(dato-1<0));
					Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
					Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
					var princ='';
					if(value.principal=='1')
						princ='(Default)';
					simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
					simple.getForm().findField('agent').setValue(value.agent);
					simple.getForm().findField('agenttype').setValue(value.agenttype);
					simple.getForm().findField('company').setValue(value.company);
					simple.getForm().findField('typeemail1').setValue(value.typeemail1);
					simple.getForm().findField('typeemail2').setValue(value.typeemail2);
					simple.getForm().findField('email').setValue(value.email);
					simple.getForm().findField('email2').setValue(value.email2);
					simple.getForm().findField('typeurl1').setValue(value.typeurl1);
					simple.getForm().findField('urlsend').setValue(value.urlsend);
					simple.getForm().findField('typeurl2').setValue(value.typeurl2);
					simple.getForm().findField('urlsend2').setValue(value.urlsend2);
					simple.getForm().findField('typeph1').setValue(value.typeph1);
					simple.getForm().findField('phone1').setValue(value.phone1);
					simple.getForm().findField('typeph2').setValue(value.typeph2);
					simple.getForm().findField('phone2').setValue(value.phone2);
					simple.getForm().findField('typeph3').setValue(value.typeph3);
					simple.getForm().findField('phone3').setValue(value.phone3);
					simple.getForm().findField('typeph4').setValue(value.typeph4);
					simple.getForm().findField('fax').setValue(value.fax);
					simple.getForm().findField('typeph5').setValue(value.typeph5);
					simple.getForm().findField('tollfree').setValue(value.tollfree);
					simple.getForm().findField('typeph6').setValue(value.typeph6);
					simple.getForm().findField('phone6').setValue(value.phone6);
					simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
					simple.getForm().findField('address1').setValue(value.address1);
					simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
					simple.getForm().findField('address2').setValue(value.address2);
					simple.getForm().findField('type').setValue('update');
					simple.getForm().findField('agentid').setValue(value.agentid);	
				}
			},{
				text     : '>|',
				id: 'agentlast',
				width: 30, 
				hidden	 : (dato+1>r.total-1), 	
				handler  : function(){
					
					dato=r.total-1;
					var value=r.records[dato];
					Ext.getCmp('agentfirst').setVisible(!(dato==0));
					Ext.getCmp('agentprev').setVisible(!(dato-1<0));
					Ext.getCmp('agentnext').setVisible(!(dato+1>r.total-1));
					Ext.getCmp('agentlast').setVisible(!(dato+1>r.total-1));
					var princ='';
					if(value.principal=='1')
						princ='(Default)';
					simple.setTitle('Contact '+(dato+1)+' of '+r.total+' '+princ);
					simple.getForm().findField('agent').setValue(value.agent);
					simple.getForm().findField('agenttype').setValue(value.agenttype);
					simple.getForm().findField('company').setValue(value.company);
					simple.getForm().findField('typeemail1').setValue(value.typeemail1);
					simple.getForm().findField('typeemail2').setValue(value.typeemail2);
					simple.getForm().findField('email').setValue(value.email);
					simple.getForm().findField('email2').setValue(value.email2);
					simple.getForm().findField('typeurl1').setValue(value.typeurl1);
					simple.getForm().findField('urlsend').setValue(value.urlsend);
					simple.getForm().findField('typeurl2').setValue(value.typeurl2);
					simple.getForm().findField('urlsend2').setValue(value.urlsend2);
					simple.getForm().findField('typeph1').setValue(value.typeph1);
					simple.getForm().findField('phone1').setValue(value.phone1);
					simple.getForm().findField('typeph2').setValue(value.typeph2);
					simple.getForm().findField('phone2').setValue(value.phone2);
					simple.getForm().findField('typeph3').setValue(value.typeph3);
					simple.getForm().findField('phone3').setValue(value.phone3);
					simple.getForm().findField('typeph4').setValue(value.typeph4);
					simple.getForm().findField('fax').setValue(value.fax);
					simple.getForm().findField('typeph5').setValue(value.typeph5);
					simple.getForm().findField('tollfree').setValue(value.tollfree);
					simple.getForm().findField('typeph6').setValue(value.typeph6);
					simple.getForm().findField('phone6').setValue(value.phone6);
					simple.getForm().findField('typeaddress1').setValue(value.typeaddress1);
					simple.getForm().findField('address1').setValue(value.address1);
					simple.getForm().findField('typeaddress2').setValue(value.typeaddress2);
					simple.getForm().findField('address2').setValue(value.address2);
					simple.getForm().findField('type').setValue('update');
					simple.getForm().findField('agentid').setValue(value.agentid);	
				}
			},{
			text     : 'Close',
			handler  : function(){
				win.close();
				loading_win.hide();
			}
		}]
	});
	win.show();
}

function skypecall(userid,phone,multiple,parcelid,contactname){
	var startDate = new Date();
	var startDuration = startDate.getTime();	
	
	if(multiple!=true){
		countCalls=0;
		selectedData=new Array();
		storeFol=null;
		complete=false;
		typeFollowC='B';
	}
	
	var simpleCall = new Ext.FormPanel({
		frame: true,
		title: 'Call',
		width: 490,
		waitMsgTarget : 'Waiting...',
		labelWidth: 100,
		defaults: {width: 350},
		labelAlign: 'left',
		items: [
			{
				xtype	  :	'textfield',
				readOnly: true,
				fieldLabel: 'Call To',
				name: 'callto',
				value: contactname
			},{
				xtype	  :	'textfield',
				readOnly: true,
				fieldLabel: 'Phone Number',
				name: 'calltophone',
				value: phone
			},{
				xtype	  :	'textarea',
				height	  : 100,
				name	  : 'detail',
				fieldLabel: 'Details'
			},{
				xtype     : 'hidden',
				name      : 'typeFollow',
				value     : typeFollowC
			},{
				xtype     : 'hidden',
				name      : 'type',
				value     : 'voicemail'
			},{
				xtype     : 'hidden',
				name      : 'completetask',
				value     : complete
			},{
				xtype     : 'hidden',
				name      : 'userid',
				value     : userid
			},{
				xtype     : 'hidden',
				name      : 'parcelid',
				value     : parcelid
			},{
				xtype     : 'hidden',
				name      : 'phonenumber',
				value     : phone
			},{
				xtype     : 'hidden',
				name      : 'contactname',
				value     : contactname
			},{
				xtype     : 'hidden',
				id		  : 'idCallDuration',
				name      : 'duration',
				value     : 0
			},{
				xtype     : 'hidden',
				id		  : 'idCallDate',
				name      : 'datecall',
				value     : ''
			}],
		
		buttons: [{
			text: 'Save Call',
			handler: function(){
				loading_win.show();
				var endDuration = new Date().getTime();
				var finalDuration = endDuration - startDuration;
				finalDuration = Math.floor(finalDuration / 1000);
				finalDuration = Math.floor((((finalDuration % 31536000) % 86400) % 3600) / 60);
				
				Ext.getCmp('idCallDuration').setValue(finalDuration.toString()+' minutes long');
				Ext.getCmp('idCallDate').setValue(startDate.format('m/d/y g:i A'));
				
				simpleCall.getForm().submit({
					timeout : 86400,
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
					success : function(form, action) {
						loading_win.hide();
						if(multiple==true){
							countCalls++;
							makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
						}
						winCall.close();
					}
				});
				
			}
		},{
			text: 'Cancel Call',
			handler: function(){
				if(multiple==true){
					countCalls++;
					makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
				}
				winCall.close();
			}
		}]
	});
	var winCall = new Ext.Window({
		width       : 490,
		autoHeight  : true,
		modal       : false,
		plain       : true,
		items       : [
			simpleCall
		]
	});
	winCall.show();
}

function makeCall(userid,phone,multiple,parcelid,contactname){
	//loading_win.show();

	if(multiple!=true){
		countCalls=0;
		selectedData=new Array();
		storeFol=null;
		complete=false;
		typeFollowC='B';
	}
	var startCallTime = new Date().getTime();
	var simpleCall = new Ext.FormPanel({
		frame: true,
		title: 'Call',
		width: 490,
		waitMsgTarget : 'Waiting...',
		labelWidth: 100,
		defaults: {width: 350},
		labelAlign: 'left',
		items: [
			{
				xtype	  :	'textfield',
				readOnly: true,
				fieldLabel: 'Call To',
				name: 'callto',
				value: contactname
			},{
				xtype	  :	'textfield',
				readOnly: true,
				fieldLabel: 'Phone Number',
				name: 'calltophone',
				value: phone
			},{
				xtype	  :	'textarea',
				height	  : 100,
				name	  : 'detail',
				fieldLabel: 'Details'
			},{
				xtype     : 'hidden',
				name      : 'typeFollow',
				value     : typeFollowC
			},{
				xtype     : 'hidden',
				name      : 'type',
				value     : 'voicemail'
			},{
				xtype     : 'hidden',
				name      : 'completetask',
				value     : complete
			},{
				xtype     : 'hidden',
				name      : 'userid',
				value     : userid
			},{
				xtype     : 'hidden',
				name      : 'parcelid',
				value     : parcelid
			},{
				xtype     : 'hidden',
				name      : 'phonenumber',
				value     : phone
			},{
				xtype     : 'hidden',
				name      : 'contactname',
				value     : contactname
			},{
				xtype     : 'hidden',
				id		  : 'idCallDuration',
				name      : 'duration',
				value     : 0
			},{
				xtype     : 'hidden',
				id		  : 'idCallDate',
				name      : 'datecall',
				value     : 0
			}],

		buttons: [{
			text: 'Save Call',
			handler: function(){
				loading_win.show();
				var endCallTime = new Date().getTime(), callTime = (endCallTime-startCallTime);
				var ms = callTime % 1000;
			    callTime = (callTime - ms) / 1000;
			    var secs = callTime % 60;
			    callTime = (callTime - secs) / 60;
			    var mins = callTime % 60;
			    var hrs = (callTime - mins) / 60;
			
			    callTime = (hrs>0 ? hrs +':' : '') + (mins>0 ? mins + ':' : '') + secs + '.' + ms;
				
				Ext.getCmp('idCallDuration').setValue(callTime);
				Ext.getCmp('idCallDate').setValue(new Date());

				simpleCall.getForm().submit({
					timeout : 86400,
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
					success : function(form, action) {
						loading_win.hide();
						if(multiple==true){
							countCalls++;
							makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
						}
						winCall.close();
					}
				});
			}
		},{
			text: 'Cancel Call',
			handler: function(){
				if(multiple==true){
					countCalls++;
					makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
				}
				winCall.close();
			}
		}]
	});
	var winCall = new Ext.Window({
		width       : 490,
		autoHeight  : true,
		modal       : false,
		plain       : true,
		items       : [
			simpleCall
		]
	});
	winCall.show();	
	/*loading_win.show();
	
	if(multiple!=true){
		countCalls=0;
		selectedData=new Array();
		storeFol=null;
		complete=false;
		typeFollowC='B';
	}
	
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url:'mysetting_tabs/myfollowup_tabs/properties_followmakecall.php',
		method: 'POST',
		timeout :86400, 
		params: { 
			userid: userid,
			phone: phone
		},
		failure:function(response,options){
			loading_win.hide();
			var resp = Ext.decode(response.responseText);
			Ext.MessageBox.alert('Warning',resp.msg);
		},
		success:function(response,options){								
			loading_win.hide();
			var resp = Ext.decode(response.responseText);
			var idLastCall=resp.idLastCall;
			var cancelar=false;
			
			if(resp.msg!=''){
				Ext.MessageBox.alert('Warning',resp.msg); 
				return false;
			}
			
			var simpleCall = new Ext.FormPanel({
				frame: true,
				title: 'Call',
				width: 490,
				waitMsgTarget : 'Waiting...',
				labelWidth: 100,
				defaults: {width: 350},
				labelAlign: 'left',
				items: [
					{
						xtype	  :	'textfield',
						readOnly: true,
						fieldLabel: 'Call To',
						name: 'callto',
						value: contactname
					},{
						xtype	  :	'textfield',
						readOnly: true,
						fieldLabel: 'Phone Number',
						name: 'calltophone',
						value: phone
					},{
						xtype	  :	'textarea',
						height	  : 100,
						name	  : 'detail',
						fieldLabel: 'Details'
					},{
						xtype     : 'hidden',
						name      : 'typeFollow',
						value     : typeFollowC
					},{
						xtype     : 'hidden',
						name      : 'type',
						value     : 'voicemail'
					},{
						xtype     : 'hidden',
						name      : 'completetask',
						value     : complete
					},{
						xtype     : 'hidden',
						name      : 'userid',
						value     : userid
					},{
						xtype     : 'hidden',
						name      : 'parcelid',
						value     : parcelid
					},{
						xtype     : 'hidden',
						name      : 'phonenumber',
						value     : phone
					},{
						xtype     : 'hidden',
						name      : 'contactname',
						value     : contactname
					},{
						xtype     : 'hidden',
						id		  : 'idCallDuration',
						name      : 'duration',
						value     : 0
					},{
						xtype     : 'hidden',
						id		  : 'idCallDate',
						name      : 'datecall',
						value     : 0
					}],
				
				buttons: [{
					text: 'Save Call',
					handler: function(){
						loading_win.show();
						Ext.Ajax.request({  
							waitMsg: 'Checking...',
							url:'mysetting_tabs/myfollowup_tabs/getvoicecall.php',
							method: 'POST',
							timeout :86400, 
							params: { 
								userid: userid,
								phone: phone
							},
							failure:function(response,options){
								loading_win.hide();
								var resp = Ext.decode(response.responseText);
								Ext.MessageBox.alert('Warning',resp.msg);
							},
							success:function(response,options){								
								var resp = Ext.decode(response.responseText);
								idLastCall = resp.idLastCall;
								Ext.getCmp('idCallDuration').setValue(resp.duration);
								Ext.getCmp('idCallDate').setValue(resp.fecha);
								
								simpleCall.getForm().submit({
									timeout : 86400,
									url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
									success : function(form, action) {
										loading_win.hide();
										if(multiple==true){
											countCalls++;
											makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
										}
										winCall.close();
									}
								});
							}
						});
					}
				},{
					text: 'Cancel Call',
					handler: function(){
						if(multiple==true){
							countCalls++;
							makeMultipleCalls(selectedData,storeFol,userid,complete,countCalls,typeFollowC);
						}
						winCall.close();
					}
				}]
			});
			var winCall = new Ext.Window({
				width       : 490,
				autoHeight  : true,
				modal       : false,
				plain       : true,
				items       : [
					simpleCall
				]
			});
			winCall.show();
		}
	});*/
}
var countCalls;
var selectedData;
var storeFol;
var complete;
var typeFollowC;
function makeMultipleCalls(selected,store,userid,completetask,pid,typeFoll,bulkcompletedtask){
	loading_win.show();
	countCalls=pid;
	selectedData=selected;
	storeFol=store;
	complete=completetask;
	typeFollowC=typeFoll;
	if(pid<selected.length){
		Ext.Ajax.request({
			waitMsg: 'Seeking...',
			url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
			method: 'POST',
			timeout :86400,
			params: { 
				pid: selected[pid],
				userid: userid,
				type: 'assignment'
			},
			
			failure:function(response,options){
				loading_win.hide();
				Ext.MessageBox.alert('Warning','ERROR');
			},
			success:function(response,options){
				loading_win.hide();
				var r=Ext.decode(response.responseText);
				creaVentana(0,r,selected[pid],userid,true);
			}
		});
	}else{
		loading_win.hide();
		if(store) store.reload();
	}
}

//Create Follow History
function createFollowHistory(tabs, id, record, pid, typeFollow, activeHistoryTab, userid){
	var county = record.get('county');
	var status = record.get('status');
	/*var address = record.get('address');
	var city = record.get('city');
	var zip = record.get('zip');*/
	
	
	if(document.getElementById(id)){
		var tab = tabs.getItem(id);
		tabs.remove(tab);
	}
	
	//Top Bar principal
	var followTbar = new Ext.Toolbar({
		cls: 'no-border', 
		width: 'auto',
		items: [' ',{
			tooltip: 'View Contacts',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/agent.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						pid: pid,
						userid: userid,
						type: 'assignment'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						var r=Ext.decode(response.responseText);
						creaVentana(0,r,pid,useridspider);
					}
				});
			}
		},{
			 tooltip: 'Short Sale',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/shortsale.png',
			 hidden: true,
			 handler: function(){
				var option = 'width=930px,height=555px';	
				 window.open('https://www.reifax.com/xima3/mysetting_tabs/myfollowup_tabs/shortsaleform.php?pid='+pid ,'Signature',option);
				 
			 }
		},'->',{
			 tooltip: 'Close Follow History',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/cancel.png',
			 handler: function(){ 
				 var tab = tabs.getItem(id);
				 tabs.remove(tab);
			 }
		}]
	}); 
	
	
	//Top Bar History Tab
	var tbarhistory =  new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				var idfuh = '';
				if(followfuh.length > 0){
					idfuh=followfuh[0].get('idfuh');
					for(i=1; i<followfuh.length; i++){
						idfuh+=','+followfuh[i].get('idfuh');
					}
				}else{
					Ext.Msg.alert("Follow History", 'You must select one or more follow history to delete.');
					return false;
				}
				Ext.Ajax.request( 
				{  
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						type: 'delete',
						idfuh: idfuh
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow History",'Deleted Follow History.');
						followstore.reload();
					}                                
				});
			 }
		},{
			tooltip: 'New History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Add History',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['17','Make NOTE'],
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : '1',
								hiddenName    : 'task',
								hiddenValue   : '1',
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
										method: 'POST',
										timeout :86400,
										params: { 
											pid: pid,
											userid: userid,
											type: 'assignment'
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Detail'
							},{
								xtype	  : 'checkboxgroup',
								fieldLabel: 'Document',
								columns	  : 3,
								itemCls   : 'x-check-group-alt',
								items	  : [
									{boxLabel: 'Contract', name: 'contract'},
									{boxLabel: 'Proof of Funds', name: 'pof'},
									{boxLabel: 'EMD', name: 'emd'},
									{boxLabel: 'Addendums', name: 'rademdums'},
									{boxLabel: 'Offer Received', name: 'offerreceived'}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'insert'
							},{
								xtype     : 'hidden',
								name      : 'parcelid',
								value     : pid
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],
					
					buttons: [{
							text: 'Add',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow History", 'New Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});
				 
				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 490,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},{
			tooltip: 'Edit History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfuh = followsm.getSelections();
				if(followfuh.length > 1){
					Ext.Msg.alert("Follow History", 'Only edit one follow history at time.');
					return false;
				}else if(followfuh.length == 0){
					Ext.Msg.alert("Follow History", 'You must select one follow history to edit.');
					return false;
				}
				
				var simple = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followhistory.php',
					frame: true,
					title: 'Update History',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype     : 'numberfield',
								name      : 'offer',
								fieldLabel: 'Offer',
								minValue  : 0,
								value	  : followfuh[0].get('offer')
							},{
								xtype     : 'numberfield',
								name      : 'coffer',
								fieldLabel: 'C. Offer',
								minValue  : 0,
								value	  : followfuh[0].get('coffer')
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['17','Make NOTE'],
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : followfuh[0].get('task'),
								hiddenName    : 'task',
								hiddenValue   : followfuh[0].get('task'),
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
										method: 'POST',
										timeout :86400,
										params: { 
											pid: pid,
											userid: userid,
											type: 'assignment'
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,useridspider);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'sheduledetail',
								fieldLabel: 'Schedule Detail',
								value	  : followfuh[0].get('sheduledetail')
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Complete Detail',
								value	  : followfuh[0].get('detail')
							},{
								xtype: 'checkboxgroup',
								fieldLabel: 'Document',
								columns: 3,
								itemCls: 'x-check-group-alt',
								items: [
									{boxLabel: 'Contract', name: 'contract', checked: !followfuh[0].get('contract')},
									{boxLabel: 'Proof of Funds', name: 'pof', checked: !followfuh[0].get('pof')},
									{boxLabel: 'EMD', name: 'emd', checked: !followfuh[0].get('emd')},
									{boxLabel: 'Addendums', name: 'rademdums', checked: !followfuh[0].get('realtorsadem')},
									{boxLabel: 'Offer Received', name: 'offerreceived', checked: !followfuh[0].get('offerreceived')}
								]
							},{
								xtype     : 'hidden',
								name      : 'type',
								value     : 'update'
							},{
								xtype     : 'hidden',
								name      : 'idfuh',
								value     : followfuh[0].get('idfuh')
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : typeFollow
							}],
					
					buttons: [{
							text: 'Update',
							handler: function(){
								loading_win.show();
								simple.getForm().submit({
									success: function(form, action) {
										loading_win.hide();
										win.close();
										Ext.Msg.alert("Follow Up", 'Updated Follow History.');
										followstore.reload();
									},
									failure: function(form, action) {
										loading_win.hide();
										Ext.Msg.alert("Failure", "ERROR");
									}
								});
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});
				 
				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 500,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			 }
		},new Ext.Button({
			tooltip: 'Print History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						printType: 4,
						parcelid: pid,
						userid: userid, 
						county: county
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);							
					}                                
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						printType: 4,
						parcelid: pid,
						userid: userid, 
						county: county
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;								
					}                                
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh History',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore.reload();
			}
		})]
	});
	
	//Top Bar Follow Schedule
	var tbarschedule = new Ext.Toolbar({
		cls: 'no-border',
		width: 'auto',
		items: [' ',{
			tooltip: 'Delete Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				var idfus = '';
				if(followfus.length > 0){
					idfus=followfus[0].get('idfus');
					for(i=1; i<followfus.length; i++){
						idfus+=','+followfus[i].get('idfus');
					}
				}else{
					Ext.Msg.alert("Follow Schedule", 'You must select one or more follow schedule to delete.');
					return false;
				}
				Ext.Ajax.request( 
				{  
					waitMsg: 'Deleting...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						type: 'delete',
						idfus: idfus
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						Ext.MessageBox.alert("Follow Schedule",'Deleted Follow Schedule.');
						followstore_schedule.reload();
					}                                
				});
			 }
		},{
			tooltip: 'New Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/add.gif',
			 handler: function(){
				var selected = new Array(), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule; 
				selected.push(pid);
				
				createScheduleTask(selected, store, userid, progressBarTask_win_schedule, progressBarTask_schedule, selling);
			 }
		},{
			tooltip: 'Edit Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/toolbar/update.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}
				
				var idfus = followfus[0].get('idfus'), selling = (typeFollow == 'B' ? false : true), store = followstore_schedule;
				editScheduleTask(idfus, userid, selling, store);
			 }
		},{
			tooltip: 'Complete Task',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			 icon: 'http://www.reifax.com/img/refresh.png',
			 handler: function(){
				var followfus = followsm_schedule.getSelections(), selectedAux=new Array();
				if(followfus.length > 1){
					Ext.Msg.alert("Follow Schedule", 'Only edit one follow schedule at time.');
					return false;
				}else if(followfus.length == 0){
					Ext.Msg.alert("Follow Schedule", 'You must select one follow schedule to edit.');
					return false;
				}

				for(i=0;i<followfus.length;i++){
					selectedAux.push({
						'pid': pid, 
						'id': followfus[i].get('idfus'), 
						'task': followfus[i].get('task')
					});
				}
				
				loading_win.show();		
				completeMultiTask(selectedAux,followgrid_schedule,followstore_schedule,userid, progressBarTask_win_schedule, progressBarTask_schedule, false);
			 }
		},new Ext.Button({
			tooltip: 'Print Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/printer.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						printType: 5,
						parcelid: pid,
						userid: userid, 
						county: county
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.pdf;
						loading_win.hide();
						window.open(url);							
					}                                
				});
			}
		}),new Ext.Button({
			tooltip: 'Export Excel',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/toolbar/excel.png',
			handler: function(){
				loading_win.show();
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						printType: 5,
						parcelid: pid,
						userid: userid, 
						county: county
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);
						var url='http://www.reifax.com/'+rest.excel;
						loading_win.hide();
						location.href= url;								
					}                                
				});
			}
		}),new Ext.Button({
			tooltip: 'Refresh Schedule',
			cls:'x-btn-text-icon',
			iconAlign: 'left',
			text: ' ',
			width: 30,
			height: 30,
			scale: 'medium',
			icon: 'http://www.reifax.com/img/refresh.gif',
			handler: function(){
				followstore_schedule.reload();
			}
		})]
	});
	
	//Create Tab Follow History with Top Bars
	tabs.add({
		title: ' Follow ',
		id: id, 
		closable: true,
		items: [	
			new Ext.TabPanel({
				id: 'historyTab'+typeFollow,
				activeTab: activeHistoryTab,
				width: ancho,
				height: tabs.getHeight(),
				plain:true,
				listeners: {
					'tabchange': function( tabpanel, tab ){
						if(tab.id=='followhistoryInnerTab'+typeFollow)
							if(document.getElementById('follow_history_grid')) followstore.reload();
						else if(tab.id=='followsheduleInnerTab'+typeFollow)
							if(document.getElementById('follow_schedule_grid')) followstore_schedule.reload();
					}
				},
				items:[{
					title: 'History',
					id: 'followhistoryInnerTab'+typeFollow,
					autoLoad: {
						url		: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followhistory.php', 
						scripts	: true, 
						params	:{
							parcelid 	: pid, 
							userid 		: userid, 
							county 		: county,
							typeFollow	: typeFollow 
						}, 
						timeout	: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarhistory
				},{
					title: 'Schedule',
					id: 'followsheduleInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followshedule.php', 
						scripts: true, 
						params:{
							parcelid	: pid, 
							userid		: userid, 
							county		: county,
							typeFollow	: typeFollow 
						}, 
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false},
					tbar: tbarschedule
				},{
					title: 'Email',
					id: 'followemailInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followemail.php', 
						scripts: true, 
						params:{
							parcelid	: pid, 
							userid		: userid, 
							county		: county,
							typeFollow	: typeFollow
						}, 
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
				},{
					title: 'Dashboard',
					id: 'followdashboardInnerTab'+typeFollow,
					autoLoad: {
						url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/followdashboard.php', 
						scripts: true, 
						params:{
							parcelid	: pid, 
							userid		: userid, 
							county		: county,
							typeFollow	: typeFollow
						}, 
						timeout: 86400
					},
					enableTabScroll:true,
					defaults:{ autoScroll: false}
			   }]
			})
		],
		tbar: followTbar
	}).show();
	
}

function statusAltPanel(store,userid,pid,statusalt){
	var simple = new Ext.FormPanel({
	url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
	frame: true,
	title: 'Follow Up - Status Change',
	width: 350,
	waitMsgTarget : 'Waiting...',
	labelWidth: 50,
	labelAlign: 'left',
	items: [{
		xtype         : 'combo',
		mode          : 'remote', 
		triggerAction : 'all',
		fieldLabel	  : 'Status',
		width		  : 180,
		store         : new Ext.data.JsonStore({
			root:'results',
			totalProperty:'total',
			baseParams: {
				'type': 'get-alt-status',
				'userid': userid
			},
			fields:[
				{name:'code', type:'string'},
				{name:'status', type:'string'}
			],
			url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
		}),
		displayField  : 'status',
		valueField    : 'code',
		name          : 'statusaltname',
		value         : statusalt,
		hiddenName    : 'statusalt',
		hiddenValue   : statusalt,
		allowBlank    : false
	},{
				xtype     : 'hidden',
				name      : 'pid',
				value     : pid
			},{
				xtype     : 'hidden',
				name      : 'type',
				value     : 'change-status'
			}],
	
	buttons: [
		{
			text: 'Custom Status',
			handler: function(){
				winStatus.close();
				var simple2 = new Ext.FormPanel({
					url: 'mysetting_tabs/myfollowup_tabs/properties_followup.php',
					frame: true,
					title: 'Follow Up - New Alternative Status',
					width: 350,
					waitMsgTarget : 'Waiting...',
					labelWidth: 80,
					labelAlign: 'left',
					items: [{
						xtype         : 'combo',
						id			  : 'cstatusalt',
						mode          : 'remote', 
						triggerAction : 'all',
						fieldLabel	  : 'Alt. Status',
						width		  : 180,
						store         : new Ext.data.JsonStore({
							root:'results',
							autoLoad: true,
							totalProperty:'total',
							baseParams: {
								'type': 'get-alt-status',
								'userid': userid,
								'onlyMy': true
							},
							fields:[
								{name:'code', type:'string'},
								{name:'status', type:'string'}
							],
							url:'mysetting_tabs/myfollowup_tabs/properties_followup.php'
						}),
						displayField  : 'status',
						valueField    : 'code',
						name          : 'statusaltname',
						hiddenName    : 'statusalt',
						hiddenValue	  : '-1',
						value		  : 'New Status'
					},{
						xtype		  : 'textfield',
						id			  : 'tstatus',
						fieldLabel	  : 'New Status',
						name		  : 'status',
						value		  : ''
					},{
						xtype     : 'hidden',
						name      : 'userid',
						value     : userid
					},{
						xtype     : 'hidden',
						id		  : 'add-alt-status',
						name      : 'type',
						value     : ''
					}],
					buttons: [{
						text: 'Update',
						handler: function(){
							loading_win.show();
							Ext.getCmp('add-alt-status').setValue('upd-alt-status');
							
							var s = Ext.getCmp('tstatus').getValue(),
							sa = Ext.getCmp('cstatusalt').getValue();
							
							if(sa == '-1'){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'You must select an alternative status to update.');
								return false;
							}else if(s.length <= 0){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'You must define the new status to update.');
								return false;
							}
							
							simple2.getForm().submit({
								success: function(form, action) {
									winStatus2.close();
									loading_win.hide();
									Ext.Msg.alert("Follow Up", 'Status Updated.');
								},
								failure: function(form, action) {
									loading_win.hide();
									Ext.Msg.alert("Failure", "ERROR");
								}
							});
						}
					},{
						text: 'Del',
						handler: function(){
							loading_win.show();
							Ext.getCmp('add-alt-status').setValue('del-alt-status');
							
							var sa = Ext.getCmp('cstatusalt').getValue();
							
							if(sa == '-1'){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'You must select an alternative status to delete.');
								return false;
							}
							
							simple2.getForm().submit({
								success: function(form, action) {
									winStatus2.close();
									loading_win.hide();
									Ext.Msg.alert("Follow Up", 'Status Deleted.');
								},
								failure: function(form, action) {
									loading_win.hide();
									Ext.Msg.alert("Failure", "ERROR");
								}
							});
						}
					},{
						text: 'Add',
						handler: function(){
							loading_win.show();
							Ext.getCmp('add-alt-status').setValue('add-alt-status');
							
							var s = Ext.getCmp('tstatus').getValue();
							
							if(s.length <= 0){
								loading_win.hide();
								Ext.Msg.alert("Follow Up", 'You must define the new status to add.');
								return false;
							}
							
							simple2.getForm().submit({
								success: function(form, action) {
									winStatus2.close();
									loading_win.hide();
									Ext.Msg.alert("Follow Up", 'Status Added.');
								},
								failure: function(form, action) {
									loading_win.hide();
									Ext.Msg.alert("Failure", "ERROR");
								}
							});
						}
					},{
						text: 'Cancel',
						handler  : function(){
							simple2.getForm().reset();
							winStatus2.close();
						}
					}]
				});
				var winStatus2 = new Ext.Window({
					layout      : 'fit',
					width       : 370,
					height      : 180,
					modal	 	: true,
					plain       : true,
					items		: simple2,
					closeAction : 'close'
				});
				winStatus2.show();
			}
		},{
			text: 'Apply',
			handler: function(){
				loading_win.show();
				simple.getForm().submit({
					success: function(form, action) {
						loading_win.hide();
						winStatus.close();
						Ext.Msg.alert("Follow Up", 'Status Changed.');
						if(store) store.reload();
					},
					failure: function(form, action) {
						loading_win.hide();
						Ext.Msg.alert("Failure", "ERROR");
					}
				});
			}
		},{
			text: 'Cancel',
			handler  : function(){
				simple.getForm().reset();
				winStatus.close();
			}
		}]
	});
	 
	var winStatus = new Ext.Window({
		layout      : 'fit',
		width       : 320,
		height      : 140,
		modal	 	: true,
		plain       : true,
		items		: simple,
		closeAction : 'close'
	});
	winStatus.show();
}

function crearTasks(url,userid,selected,ind,values,progressBar,progressWin,store,win,totalnoschedule){
	progressBar.updateProgress(
		((ind+1)/selected.length),
		"Processing "+(ind+1)+" of "+selected.length
	);
	
	if(ind==0) totalnoschedule=0;
	if(selected[ind]['status'] != 'PENDING' && selected[ind]['status'] != '') totalnoschedule++;
	
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url: url, 
		method: 'POST',
		timeout :0,
		params: { 
			parcelid: selected[ind]['pid'],
			userid: userid,
			task: values.task, 
			detail: values.detail,
			complete: values.complete,
			odate: values.odate,
			ohour: values.ohour,
			status: selected[ind]['status'],
			typeExec: values.type,
			typeFollow: values.typeFollow,
			type: 'insert'
		},
		
		failure:function(response,options){
			progressWin.close();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			if((ind)==selected.length-1){
				if(win) win.close();
				loading_win.hide();
				progressWin.hide();
				if(store) store.reload();
				Ext.Msg.alert("Follow Schedule", 'All Follow Schedule Task were created. '+totalnoschedule+' presented errors. <br>Please, verify them on the Pending Task tab');
			}else{
				 crearTasks(url,userid,selected,ind+1,values,progressBar,progressWin,store,win,totalnoschedule);
			}
		}                                
	});		
}

function contactsDataByTask(userid,selected,values,progressBar,progressWin,store,selling,noSearchContacts){
	var selectedAux = selected[0];
	var noSearchContacts = noSearchContacts ? false : true;
	
	for(var i=1; i<selected.length; i++){
		selectedAux += ','+selected[i]; 
	}
	
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
		method: 'POST',
		timeout :0,
		params: { 
			pids: selectedAux,
			userid: userid,
			task: values.task,
			type: 'checkfollowagent'
		},
		
		failure:function(response,options){
			progressWin.close();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var contact = Ext.util.JSON.decode(response.responseText);
			if(contact.success != 'false'){				
				var propertiesNoContacts = new Array();
				for(var i=0; i<contact.selected.length; i++){
					var aux = contact.selected[i];
					if(aux['status']=='ERROR: Contact not found.') propertiesNoContacts.push(aux['pid']);
				}
				
				if(propertiesNoContacts.length > 0 && noSearchContacts){
					getContacts(propertiesNoContacts,store,userid, progressWin, progressBar, contact.contactpremiun, contact.block, contact.overwrite, contact.premium, function(userid,selected,values,progressBar,progressWin,store){
					contactsDataByTask(userid,selected,values,progressBar,progressWin,store,selling,true);
				 }, selected, values);
				}else{
					var grid = '', selectedTask=parseInt(values.task);
					var callback = function(userid,selected,values,progressBar,progressWin,store,win){
						crearTasks('mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',userid,selected,0,values,progressBar,progressWin,store,win);
					};
					var selected2 = new Array();
					for(var i=0; i<contact.selected.length; i++){
						selected2.push(contact.selected[i]['pid']);
					}
					
					progressWin.hide();
					
					switch(selectedTask){
						case 1: 
							sendEmailSms(selected2,grid,store,userid, progressWin, progressBar, selling, false,callback,values,contact.selected);
						break;
						case 3: 
							sendEmailFax(selected2,grid,store,userid, progressWin, progressBar, selling, false,callback,values,contact.selected);
						break;
						case 5: 
							sendEmail(selected2,grid,store,userid, progressWin, progressBar, selling, false,callback,values,contact.selected);
						break;
						case 7: 
							sendContractJ(selected2,grid,store,userid, progressWin, progressBar, Ext.getCmp('mailenviadostask'), false, selling,callback,values,contact.selected);
						break;
						case 9:
							values.type=1;
							callback(userid,contact.selected,values,progressBar,progressWin,store);
						break;
						default: 
							var textTask = (selectedTask == 2 ? 'SMS Received' : 
								(selectedTask == 4 ? 'FAX Received' : 
								(selectedTask == 6 ? 'EMAIL Received' :
								(selectedTask == 8 ? 'DOC Received' :
								(selectedTask == 10 ? 'CALL Received' :
								(selectedTask == 11 ? 'R. MAIL Sender' :
								(selectedTask == 12 ? 'R. MAIL Received' :
								(selectedTask == 13 ? 'OTHER Sender' :
								(selectedTask == 14 ? 'OTHER Received' :
								(selectedTask == 15 ? 'VOICE MAIL Sender' :
								(selectedTask == 16 ? 'VOICE MAIL Received' : 
								'Make NOTE')))))))))));
								
							var simple = new Ext.FormPanel({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
								frame: true,
								title: 'Complete Schedule Task - '+textTask,
								width: 490,
								waitMsgTarget : 'Waiting...',
								labelWidth: 100,
								defaults: {width: 350},
								labelAlign: 'left',
								items: [{
											xtype	  :	'textarea',
											height	  : 100,
											name	  : 'detail',
											fieldLabel: 'Complete Detail'
										}],
								
								buttons: [{
										text: 'Schedule Task',
										handler: function(){
											loading_win.show();
											var v = simple.getForm().getValues();
											values.complete = v.detail;
											values.type=1;
											
											callback(userid,contact.selected,values,progressBar,progressWin,store,win);
										}
									},{
										text: 'Cancel',
										handler  : function(){
											simple.getForm().reset();
											win.close();
										}
									}]
								});
							 
							var win = new Ext.Window({
								layout      : 'fit',
								width       : 490,
								height      : 200,
								modal	 	: true,
								plain       : true,
								items		: simple,
								closeAction : 'close'
							});
							win.show();
						break;
					}
				}
			}else{
				progressWin.close();
				Ext.MessageBox.alert('Error','You can not create these tasks, you must have Offer Price for all the properties selected.');
			}
			
		}
	});
}

function createScheduleTask(selected, store, userid, progressWin, progressBar, selling){
	var followType = selling ? 'S' : 'B';
	var datenow = new Date();
	var simple = new Ext.FormPanel({
		frame: true,
		title: 'Add Schedule',
		width: 490,
		waitMsgTarget : 'Waiting...',
		labelWidth: 100,
		defaults: {width: 350},
		labelAlign: 'left',
		items: [{
					xtype		  : 'datefield',
					allowBlank	  : false,
					name		  : 'odate',
					fieldLabel	  : 'Exec. Date',
					format		  : 'Y-m-d',
					allowBlank	  : false,
					value		  : datenow.format('Y-m-d')
				},{
					xtype: 'compositefield',
					fieldLabel: 'Exec. Hour',
					items: [
						{
							xtype: 'spinnerfield',
							name: 'ohour_hour',
							minValue: 1,
							maxValue: 12,
							incrementValue: 1,
							accelerate: true,
							width: 115,
							value: datenow.format('h')
						},{
							xtype: 'spinnerfield',
							name: 'ohour_min',
							minValue: 0,
							maxValue: 59,
							incrementValue: 1,
							accelerate: true,
							width: 115,
							value: datenow.format('i')
						},{
							xtype         : 'combo',
							mode          : 'local',
							triggerAction : 'all',
							store         : new Ext.data.ArrayStore({
								id        : 0,
								fields    : ['valor', 'texto'],
								data      : [
									['AM','AM'],
									['PM','PM']
								]
							}),
							displayField  : 'texto',
							valueField    : 'valor',
							name          : 'fohour_ampm',
							value         : datenow.format('A'),
							hiddenName    : 'ohour_ampm',
							hiddenValue   : datenow.format('A'),
							allowBlank    : false,
							width: 110
						}
						
					]
				},{
					xtype         : 'combo',
					mode          : 'local',
					fieldLabel    : 'Exec. Type',
					triggerAction : 'all',
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['2','Automatic'],
							['1','Manual']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'ftypename',
					value         : '2',
					hiddenName    : 'type',
					hiddenValue   : '2',
					allowBlank    : false,
					listeners	  : {
						'select'  : function(combo,record,index){
							var type = record.get('valor');
							var task = combo.findParentByType('form').getForm().findField('scheduleTaskType').getStore();
							
							if(type=='1'){
								task.loadData([
									['17','Make NOTE'],
									['1','Send SMS'],
									['2','Receive SMS'],
									['3','Send FAX'],
									['4','Receive FAX'],
									['5','Send EMAIL'],
									['6','Receive EMAIL'],
									['7','Send DOC'],
									['8','Receive DOC'],
									['9','Make CALL'],
									['10','Receive CALL'],
									['11','Send R. MAIL'],
									['12','Receive R. MAIL'],
									['13','Send OTHER'],
									['14','Receive OTHER'],
									['15','Send VOICE MAIL'],
									['16','Receive VOICE MAIL']
								]);
							}else{
								task.loadData([
									['1','Send SMS'],
									['3','Send FAX'],
									['5','Send EMAIL'],
									['7','Send DOC']
								]);
							}
						}
					}
				},{
					xtype         : 'combo',
					mode          : 'local',
					id			  : 'scheduleTaskType',
					fieldLabel    : 'Task',
					triggerAction : 'all',
					store         : new Ext.data.ArrayStore({
						id        : 0,
						fields    : ['valor', 'texto'],
						data      : [
							['1','Send SMS'],
							['3','Send FAX'],
							['5','Send EMAIL'],
							['7','Send DOC']
						]
					}),
					displayField  : 'texto',
					valueField    : 'valor',
					name          : 'ftaskname',
					value         : '1',
					hiddenName    : 'task',
					hiddenValue   : '1',
					allowBlank    : false
				},{
					xtype	  :	'textarea',
					height	  : 100,
					name	  : 'detail',
					fieldLabel: 'Schedule Detail'
				},{
					xtype     : 'hidden',
					name      : 'typeFollow',
					value     : followType
				}],
		
		buttons: [{
				text: 'Create',
				handler: function(){
					var values = simple.getForm().getValues();
					values.ohour = values.ohour_hour + ':' + values.ohour_min +' '+values.ohour_ampm;

					win.close();
					progressWin.show();
					progressBar.updateProgress(
						0.33,
						"Verifying the contact data of selected properties."
					);
					
					contactsDataByTask(userid,selected,values,progressBar,progressWin,store,selling);
				}
			},{
				text: 'Cancel',
				handler  : function(){
					simple.getForm().reset();
					win.close();
				}
			}]
		});
	 
	var win = new Ext.Window({
		layout      : 'fit',
		width       : 490,
		height      : 330,
		modal	 	: true,
		plain       : true,
		items		: simple,
		closeAction : 'close'
	});
	win.show();
}

function editScheduleTask(idfus, userid, selling, store){
	var followType = selling ? 'S' : 'B';
	
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php', 
		method: 'POST',
		timeout :0,
		params: { 
			idfus: idfus,
			userid: userid,
			type: 'get'
		},
		
		failure:function(response,options){
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var task = Ext.util.JSON.decode(response.responseText);
			
			if(task.success!=true){ 
				Ext.MessageBox.alert('Warning',task.msg); 
				return false;
			}
			
			var data = task.data;
			var showBody = (data.task != 1 && data.task != 3 && data.task != 5 && data.task != 7), 
			showSubject =  ((data.task != 5 && data.task != 7) || (data.task == 7 && data.docs_task != 5)),
			showComplete = (data.task == 1 || data.task == 3 || data.task == 5 || data.task == 7);
			var dataTask = [
				['1','Send SMS'],
				['3','Send FAX'],
				['5','Send EMAIL'],
				['7','Send DOC']
			];
			
			if(parseInt(data.typeExec) == 1){
				dataTask = [
					['17','Make NOTE'],
					['1','Send SMS'],
					['2','Receive SMS'],
					['3','Send FAX'],
					['4','Receive FAX'],
					['5','Send EMAIL'],
					['6','Receive EMAIL'],
					['7','Send DOC'],
					['8','Receive DOC'],
					['9','Make CALL'],
					['10','Receive CALL'],
					['11','Send R. MAIL'],
					['12','Receive R. MAIL'],
					['13','Send OTHER'],
					['14','Receive OTHER'],
					['15','Send VOICE MAIL'],
					['16','Receive VOICE MAIL']
				];
			}
			
			var simple = new Ext.FormPanel({
				url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				frame: true,
				title: 'View/Edit Schedule',
				width: 490,
				waitMsgTarget : 'Waiting...',
				labelWidth: 100,
				defaults: {width: 350},
				labelAlign: 'left',
				items: [{
							xtype		  : 'displayfield',
							fieldLabel    : 'Status',
							value		  : data.status
						},{
							xtype		  : 'datefield',
							allowBlank	  : false,
							name		  : 'odate',
							fieldLabel	  : 'Exec. Date',
							format		  : 'Y-m-d',
							allowBlank	  : false,
							value		  : data.odate
						},{
							xtype: 'compositefield',
							fieldLabel: 'Exec. Hour',
							items: [
								{
									xtype: 'spinnerfield',
									name: 'ohour_hour',
									minValue: 1,
									maxValue: 12,
									incrementValue: 1,
									accelerate: true,
									width: 115,
									value: data.hour
								},{
									xtype: 'spinnerfield',
									name: 'ohour_min',
									minValue: 0,
									maxValue: 59,
									incrementValue: 1,
									accelerate: true,
									width: 115,
									value: data.min
								},{
									xtype         : 'combo',
									mode          : 'local',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['AM','AM'],
											['PM','PM']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fohour_ampm',
									value         : data.ampm,
									hiddenName    : 'ohour_ampm',
									hiddenValue   : data.ampm,
									allowBlank    : false,
									width: 110
								}
								
							]
						},{
							xtype         : 'combo',
							mode          : 'local',
							fieldLabel    : 'Exec. Type',
							triggerAction : 'all',
							store         : new Ext.data.ArrayStore({
								id        : 0,
								fields    : ['valor', 'texto'],
								data      : [
									['2','Automatic'],
									['1','Manual']
								]
							}),
							displayField  : 'texto',
							valueField    : 'valor',
							name          : 'ftypename',
							value         : data.typeExec,
							hiddenName    : 'typeExec',
							hiddenValue   : data.typeExec,
							allowBlank    : false,
							listeners	  : {
								'select'  : function(combo,record,index){
									var type = record.get('valor');
									var task = combo.findParentByType('form').getForm().findField('scheduleTaskType').getStore();
									
									if(type=='1'){
										task.loadData([
											['17','Make NOTE'],
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER'],
											['15','Send VOICE MAIL'],
											['16','Receive VOICE MAIL']
										]);
									}else{
										task.loadData([
											['1','Send SMS'],
											['3','Send FAX'],
											['5','Send EMAIL'],
											['7','Send DOC']
										]);
									}
								}
							}
						},{
							xtype         : 'combo',
							mode          : 'local',
							id			  : 'scheduleTaskType',
							fieldLabel    : 'Task',
							triggerAction : 'all',
							store         : new Ext.data.ArrayStore({
								id        : 0,
								fields    : ['valor', 'texto'],
								data      : dataTask
							}),
							displayField  : 'texto',
							valueField    : 'valor',
							name          : 'ftaskname',
							value         : data.task,
							hiddenName    : 'task',
							hiddenValue   : data.task,
							allowBlank    : false
						},{
							xtype	  :	'textarea',
							height	  : 100,
							name	  : 'detail',
							fieldLabel: 'Schedule Detail',
							value	  : data.detail
						},{
							xtype	  :	'textarea',
							height	  : 100,
							name	  : 'complete',
							fieldLabel: 'Complete Detail',
							value	  : data.complete_detail,
							hidden	  : showComplete
						},{
							xtype	  : 'fieldset',
							title	  : 'Sending Information',
            				autoHeight: true,
							labelWidth: 60,
							defaults: {width: 350},
							labelAlign: 'left',
							width	  : 450,
							hidden	  : showBody,
							items	  : [{
								xtype	  :	'textfield',
								name	  : 'subject',
								fieldLabel: 'Title',
								value	  : data.subject,
								hidden	  : showSubject
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'body',
								fieldLabel: 'Detail',
								value	  : data.body,
								hidden	  : showBody
							}]
						},{
							xtype     : 'hidden',
							name      : 'type',
							value     : 'update'
						},{
							xtype     : 'hidden',
							name      : 'idfus',
							value     : data.idfus
						}],
				
				buttons: [{
						text: 'Complete',
						handler: function(){
							loading_win.show();
							
							var selectedAux=new Array();
							selectedAux.push({
								'id': 	data.idfus, 
								'task': data.task
							}); 
							
							win.close();
							
							completeMultiTask(selectedAux,null,store,userid, null, null, selling);
						}
					},{
						text: 'Update',
						handler: function(){
							loading_win.show();
							var values = simple.getForm().getValues();
							values.ohour = values.ohour_hour + ':' + values.ohour_min +' '+values.ohour_ampm;
							
							simple.getForm().submit({
								params: {
									ohour: values.ohour
								},
								success: function(form, action) {
									loading_win.hide();
									win.close();
									Ext.Msg.alert("Follow Schedule", 'Updated Follow Schedule.');
									if(store) store.reload();
								},
								failure: function(form, action) {
									loading_win.hide();
									Ext.Msg.alert("Failure", "ERROR");
								}
							});
						}
					},{
						text: 'Cancel',
						handler  : function(){
							simple.getForm().reset();
							loading_win.hide();
							win.close();
						}
					}]
				});
			 
			var win = new Ext.Window({
				layout      : 'fit',
				width       : 490,
				height      : showBody == false ? (showSubject == false ? 520 : 500) : 450,
				modal	 	: true,
				plain       : true,
				items		: simple,
				closeAction : 'close',
				listeners	: {
					'beforeclose' : function(){
						loading_win.hide();
					}
				}
			});
			win.show();
		}
	});
}

function completeMultiTask(selected,grid,store,userid, progressWin, progressBar, selling, complete, totalnoschedule){
	var selectedAux = selected.shift();
	var completeFinal = complete ? complete : 0;
	totalnoschedule = totalnoschedule ? totalnoschedule : 0;
	loading_win.show();
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url: 'mysetting_tabs/myfollowup_tabs/properties_followshedule_complete.php', 
		method: 'POST',
		timeout :600000,
		params: { 
			userid: userid,
			idfus: selectedAux['id']
		},
		
		failure:function(response,options){
			loading_win.hide();
			if(progressWin) progressWin.close();
			if(store) store.reload();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var r = Ext.util.JSON.decode(response.responseText);
			
			if(r.success) completeFinal++;
			else totalnoschedule++;
			
			completeMultiTaskAgain(selected,grid,store,userid, progressWin, progressBar, selling, completeFinal, totalnoschedule);
		}  
	});
}

function completeMultiTaskAgain(selected,grid,store,userid, progressWin, progressBar, selling, complete, totalnoschedule){
	if(selected.length > 0){
		completeMultiTask(selected,grid,store,userid, progressWin, progressBar, selling, complete, totalnoschedule);
	}else{
		loading_win.hide();
		if(progressWin) progressWin.hide();
		if(store) store.reload();
		Ext.Msg.alert("Follow Schedule", 'Completed '+complete+' Task. '+totalnoschedule+' Tasks have errors. <br>Please, verify them on the Pending Task tab');
	}
}

//Short Sale Functions
function checkShortSaleRenderInForm(status,cancelText){
	switch(status){
		case '0': case 0: 
		return '<div title="Not Required" style="background-color: #D3D3D3; border: 2px solid; display: inline; padding: 5px 5px 0; font-size: 5px;">&nbsp;</div>'; break;			
		
		case '1': case 1: 
		return '<div title="Required" style="background-color: #D01616; border: 2px solid; display: inline; padding: 5px 5px 0; font-size: 5px;">&nbsp;</div>'; break;
		
		case '2': case 2:
		return '<div title="Uploaded" style="background-color: #083772; border: 2px solid; display: inline; padding: 5px 5px 0; font-size: 5px;">&nbsp;</div>'; break;
		
		case '3': case 3: 
		return '<div title="Acepted" style="background-color: #66ab16; border: 2px solid; display: inline; padding: 5px 5px 0; font-size: 5px;">&nbsp;</div>'; break;
		
		case '4': case 4: 
		return '<div title="Rejected - '+cancelText+'" style="background-color: #EDED00; border: 2px solid; display: inline; padding: 5px 5px 0; font-size: 5px;">&nbsp;</div>'; break;
	}
}

function loadRequireStatus(id_packages,id_shortsale,userid,processor){
	Ext.Ajax.request({
		url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		method  : 'POST',
		waitMsg : 'Getting Info',
		params	:	{
			type			: 'loadRequireStatus',
			id_packages		: id_packages,
			id_shortsale 	: id_shortsale,
			userid			: userid
		},
		success : function(r) {
			var resp	=	Ext.decode(r.responseText);
			var sections=	resp.data;
			var cS		=	1;
			var cC		=	1;
			
			Ext.getCmp('SSRequire').items.each(function(item){
				item.destroy();
			});
			
			$.each(sections,function(indexSection,section){
				var objetos	=	new Array();
				$.each(section,function(indexCheck,check){
					if(!processor && (check.status == '0' || check.status == '3')){
						var temp	=	{
							xtype		:	'panel',
							bodyStyle  	: 	'padding: 3px; padding-left: 17px; text-align:left; font-size:13px;',
							html		:	checkShortSaleRenderInForm(check.status,check.cancelText)+' '+check.gridName+' | '+check.descName
						}
					}else{
						var temp	=	{
							xtype		:	'checkbox',
							id			:	'checkSS-'+cC,
							boxLabel	:	checkShortSaleRenderInForm(check.status,check.cancelText)+' '+check.gridName+' | '+check.descName,
							name		:	'checkSS-'+check.id_require,
							idRequire	:	check.id_require,
							checked		:	false,
							hideLabel	: 	true,
							autoScroll	:	false
						}
					}
					objetos.push(temp);
					cC++;
				});
				var temp	=	{
					xtype	:	'fieldset',
					id		:	'sectionSS-'+cS,
					title	:	'Section '+cS+' - '+indexSection,
					//width	:	820,
					items	:	[objetos]
				}
				Ext.getCmp('SSRequire').add(temp);
				Ext.getCmp('SSRequire').doLayout();
				cS++;
			});
			loading_win.hide();
			Ext.getCmp('SSRequire').doLayout();
		}
	});
}

function loadRequireDocs(id_packages,id_shortsale,store,processor){
	Ext.Ajax.request({
		url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		method  : 'POST',
		waitMsg : 'Getting Documents Info',
		params	:	{
			type			: 'loadRequireDocs',
			id_shortsale 	: id_shortsale
		},
		success : function(r) {
			var resp	=	Ext.decode(r.responseText);
			
			Ext.getCmp('SSDocsRequire').items.each(function(item){
				item.destroy();
			});
			
			if(resp.total > 0){
				Ext.each(resp.data,function(data,index){
					var temp	=	{
						xtype	:	'fieldset',
						layout	:	'hbox',
						layoutConfig: {
							padding:'0 5 0 5',
							align:'top'
						},
						items	:	[{
							xtype	:	'label',
							text	:	data.fileName
						},{
							xtype	:	'spacer',
							flex	: 	1
							//width	:	10
						},{
							xtype       :	'button',
							text        :	'View',
							width		:	75,
							idDocs		:	data.id_docs,
							listeners	:	{
								click	:	function(button){
									var url = data.fileUrl;
									var Digital=new Date();
									var hours=Digital.getHours();
									var minutes=Digital.getMinutes();
									var seconds=Digital.getSeconds();
									
									window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds);
								}
							}
						},{
							xtype	:	'spacer',
							width	:	10
						},{
							xtype       :	'button',
							text        :	'Download',
							width		:	75,
							idDocs		:	data.id_docs,
							listeners	:	{
								click	:	function(button){
									var url = data.fileUrl;
									var Digital=new Date();
									var hours=Digital.getHours();
									var minutes=Digital.getMinutes();
									var seconds=Digital.getSeconds();
									
									window.open(url+'?time='+hours+minutes+seconds);
								}
							}
						},{
							xtype	:	'spacer',
							width	:	10,
							hidden	:	processor
						},{
							xtype       :	'button',
							text        :	'Delete',
							width		:	75,
							hidden		:	processor,
							idDocs		:	data.id_docs,
							listeners	:	{
								click	:	function(button){
									
									Ext.Ajax.request({
										url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										method  : 'POST',
										waitMsg : 'Getting Info',
										params	:	{
											type	: 'deleteDoc',
											id_doc	: button.idDocs
										},
										success : function(r) {
											var resp	=	Ext.decode(r.responseText);
											if(resp.success){
												loading_win.show();
												loadRequireDocs(id_packages,id_shortsale,store);
												if(store) store.reload();
												Ext.MessageBox.alert('', 'Document Deleted successfully');
												loading_win.hide();
											}
										}
									});
								}
							}
						},{
							xtype	:	'spacer',
							width	:	10
						},{
							xtype       :	'button',
							text        :	'Docs',
							enableToggle:	true,
							toggleGroup	:	'SSDocsRequireStatus',
							width		:	75,
							idDocs		:	data.id_docs,
							listeners	:	{
								toggle	:	function(button, pressed){
									if(pressed){
										Ext.Ajax.request({
											url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
											method  : 'POST',
											waitMsg : 'Getting Info',
											params	:	{
												type	: 'getDoc',
												id_doc	: button.idDocs
											},
											success : function(r) {
												var resp	=	Ext.decode(r.responseText);
												if(resp.success){
													loading_win.show();
													
													var id = 'SSDocsRequireStatus'+button.idDocs;
													var panel = Ext.getCmp(id);
													
													panel.items.each(function(item){
														item.destroy();
													});
													
													Ext.each(resp.data,function(data,index){
														var temp = {
															xtype		:	'panel',
															bodyStyle  	: 	'padding: 3px; padding-left: 5px; text-align:left; font-size:13px;',
															html		:	checkShortSaleRenderInForm(data.status,data.cancelText)+' '+data.gridName+' | '+data.descName
														};
														
														panel.add(temp);
														panel.doLayout();
													});
													
													panel.show();
													
													loading_win.hide();
												}
											}
										});
									}else{
										var id = 'SSDocsRequireStatus'+button.idDocs;
										var panel = Ext.getCmp(id);
										
										panel.items.each(function(item){
											item.destroy();
										});
										
										panel.hide();
									}
								}
							}
						}]
					};
					
					//console.log(temp);
					Ext.getCmp('SSDocsRequire').add(temp);
					Ext.getCmp('SSDocsRequire').add({
						xtype		: 'fieldset',
						id			: 'SSDocsRequireStatus'+data.id_docs,
						hidden		: true		
					});
					Ext.getCmp('SSDocsRequire').doLayout();
				});
			}else{
				var temp = {
					xtype	:	'label',
					width	:	300,
					text	:	'No uploaded required documents.'
				};
				
				Ext.getCmp('SSDocsRequire').add(temp);
				Ext.getCmp('SSDocsRequire').doLayout();
			}
		}
	});
}

function openUploadedDocs(tabs, id, userid, title, id_shortsale, id_packages, store, processor){
	var processorActive = processor ? true : false;
	
	if(document.getElementById(id)){
		var tab = tabs.getItem(id);
		tabs.remove(tab);
	}
	
	var shortSaleDocsUploadedPanel = new Ext.Panel({
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		id		   : 'SSDocsRequire',
		title	   : title,
		autoScroll : true,
		items	   : {
			xtype	:	'label',
			width	:	300,
			text	:	'No uploaded required documents.'
		}
	});	
	
	var followSSUDTbar = new Ext.Toolbar({
		cls: 'no-border', 
		width: 'auto',
		items: [' ',{
			tooltip: 'View All Docs',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: ' View All ',
			//width: 30,
			height: 30,
			scale: 'medium',
			handler: function(){
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						id_shortsale: id_shortsale,
						type: 'printShortSale'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);

						loading_win.hide();
						if(rest.success){
							var url = 'http://www.reifax.com/'+rest.pdf;
							var Digital=new Date();
							var hours=Digital.getHours();
							var minutes=Digital.getMinutes();
							var seconds=Digital.getSeconds();
							
							window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds);
						}else{
							Ext.MessageBox.alert('Warning',response.responseText);
						}
					}                                
				});
			}
		},' ',{
			tooltip: 'Download All Docs',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: ' Download All ',
			//width: 30,
			height: 30,
			scale: 'medium',
			handler: function(){
				Ext.Ajax.request( 
				{  
					waitMsg: 'Checking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php', 
					method: 'POST',
					timeout :86400,
					params: { 
						id_shortsale: id_shortsale,
						type: 'printShortSale'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					
					success:function(response,options){
						var rest = Ext.util.JSON.decode(response.responseText);

						loading_win.hide();
						if(rest.success){
							var url = 'http://www.reifax.com/'+rest.pdf;
							var Digital=new Date();
							var hours=Digital.getHours();
							var minutes=Digital.getMinutes();
							var seconds=Digital.getSeconds();
							
							window.open(url+'?time='+hours+minutes+seconds);
						}else{
							Ext.MessageBox.alert('Warning',response.responseText);
						}
					}                                
				});
			}
		},'->',{
			 tooltip: 'Close',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/cancel.png',
			 handler: function(){ 
				 var tab = tabs.getItem(id);
				 tabs.remove(tab);
			 }
		}]
	});
	
	tabs.add({
		title		: ' Required Docs ',
		id			: id, 
		closable	: true,
		items		: shortSaleDocsUploadedPanel,
		tbar		: followSSUDTbar
	}).show();
	
	loadRequireDocs(id_packages,id_shortsale,store,processorActive);
}

function openRequiredDocs(tabs, id, userid, title, id_shortsale, id_packages, store, processor){
	var processorActive = processor ? true : false;
	
	if(document.getElementById(id)){
		var tab = tabs.getItem(id);
		tabs.remove(tab);
	}
	
	var shortSaleRequireForm = new Ext.FormPanel({
		border     : true,
		bodyStyle  : 'padding: 10px; text-align:left;',
		frame      : true,
		fileUpload : true,
		title	   : title,
		method     : 'POST',
		name       : 'formul3',
		items      : [
			{
				xtype       :	'panel', 
				id          :	'panelSSFileUpload',
				layout      :	'column',
				items		:	[{
					layout: 'form',
					columnWidth	  : .50,
					items:[{					
						xtype      : 'textfield',
						fieldLabel : 'Document Name',
						name       : 'filename',
						allowBlank : processorActive,
						width      : 250
					}]
				},{
					layout: 'form',
					columnWidth	  : .50,
					labelWidth	: 130,
					items:[{											
						xtype       : 'fileuploadfield',
						emptyText   : 'Select pdf document',
						fieldLabel  : 'Upload Document',
						name        : 'pdfupload',
						allowBlank	: processorActive,
						width		: 250,
						buttonText  : 'Browse...'
					}]
				},{
					xtype: 'hidden',
					name: 'id_shortsale',
					value: id_shortsale
				},{
					xtype: 'hidden',
					name: 'userid',
					value: userid
				}]
			},{
				xtype	:	'panel',
				id		:	'SSRequire',
				border	:	true
			}
		]
	});
	
	var followSSRDTbar = new Ext.Toolbar({
		cls: 'no-border', 
		width: 'auto',
		items: [' ',{
			tooltip: 'Upload',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: checkShortSaleRenderInForm(2,'')+' Upload ',
			height: 30,
			scale: 'medium',
			handler: function(){
				var v = shortSaleRequireForm.getForm().getValues();

				if(v.filename.length == 0){
					Ext.MessageBox.alert('Error', 'You must name and upload a pdf file.');
					return false;
				}
				
				if (shortSaleRequireForm.getForm().isValid()) {
					shortSaleRequireForm.getForm().submit({
						url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							if(resp.success){
								loading_win.show();
								shortSaleRequireForm.getForm().reset();
								loadRequireStatus(id_packages,id_shortsale,userid,processorActive);
								if(store) store.reload();
							}
							Ext.MessageBox.alert('', resp.mensaje);
						},
						params:{
							type	:	'uploadDoc',
							task	:	(processorActive ? 19 : 18)
						},
						failure : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
						}
					});
				}
			}
		},{
			xtype	:	'spacer',
			width	:	5,
			hidden	:	!processorActive,
		},{
			tooltip: 'Accepted',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: checkShortSaleRenderInForm(3,'')+' Accepted ',
			height: 30,
			scale: 'medium',
			hidden: !processorActive,
			handler: function(){				
				if (shortSaleRequireForm.getForm().isValid()) {
					shortSaleRequireForm.getForm().submit({
						url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							if(resp.success){
								loading_win.show();
								shortSaleRequireForm.getForm().reset();
								loadRequireStatus(id_packages,id_shortsale,userid,processorActive);
								if(store) store.reload();
							}
							Ext.MessageBox.alert('', resp.mensaje);
						},
						params:{
							type	:	'acceptDoc'
						},
						failure : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
						}
					});
				}
			}
		},{
			xtype	:	'spacer',
			width	:	5,
			hidden	:	!processorActive,
		},{
			tooltip: 'Rejected',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: checkShortSaleRenderInForm(4,'')+' Rejected ',
			height: 30,
			scale: 'medium',
			hidden: !processorActive,
			handler: function(){				
				if (shortSaleRequireForm.getForm().isValid()) {
					var winCancel = new Ext.Window({
						layout      : 'fit',
						width       : 300,
						height      : 300,
						modal	 	: true,
						title		: 'Rejected explain (Optional)',
						plain       : true,
						items		: new Ext.FormPanel({
							border     : true,
							bodyStyle  : 'padding: 10px; text-align:left;',
							frame      : true,
							items		: [{
								xtype		: 'textarea',
								hideLabel	: 	true,
								fieldLabel 	: '',
								anchor		: '100%',
								height		: 200,
								name		: 'cancelText',
								id			: 'cancelTextID',
								value		: ''
							}],
							buttonAlign :	'center',
							buttons		:	[{
								text    :	'<span style=\'color: #4B8A08; font-size: 14px;\'><b>OK</b></span>',
								handler	:	function(){
									var cancelText = Ext.getCmp('cancelTextID').getValue();
									winCancel.close();
									shortSaleRequireForm.getForm().submit({
										url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										waitMsg : 'Saving...',
										success : function(f, a){ 
											var resp = a.result;
											if(resp.success){
												loading_win.show();
												shortSaleRequireForm.getForm().reset();
												loadRequireStatus(id_packages,id_shortsale,userid,processorActive);
												if(store) store.reload();
											}
											Ext.MessageBox.alert('', resp.mensaje);
										},
										params:{
											type		:	'cancelDoc',
											cancelText 	:	cancelText
										},
										failure : function(f, a){ 
											var resp = a.result;
											Ext.MessageBox.alert('', resp.mensaje);
										}
									});
								}
							}]
						}),
						closeAction : 'close'
					}).show();
				}
			}
		},{
			xtype	:	'spacer',
			width	:	5,
			hidden	:	!processorActive,
		},{
			tooltip: 'Required',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: checkShortSaleRenderInForm(1,'')+' Required ',
			height: 30,
			scale: 'medium',
			hidden: !processorActive,
			handler: function(){				
				if (shortSaleRequireForm.getForm().isValid()) {
					shortSaleRequireForm.getForm().submit({
						url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							if(resp.success){
								loading_win.show();
								shortSaleRequireForm.getForm().reset();
								loadRequireStatus(id_packages,id_shortsale,userid,processorActive);
								if(store) store.reload();
							}
							Ext.MessageBox.alert('', resp.mensaje);
						},
						params:{
							type	:	'requireDoc'
						},
						failure : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
						}
					});
				}
			}
		},{
			xtype	:	'spacer',
			width	:	5,
			hidden	:	!processorActive,
		},{
			tooltip: 'Not Required',
			cls:'x-btn-text',
			iconAlign: 'left',
			text: checkShortSaleRenderInForm(0,'')+' Not Required ',
			height: 30,
			scale: 'medium',
			hidden: !processorActive,
			handler: function(){				
				if (shortSaleRequireForm.getForm().isValid()) {
					shortSaleRequireForm.getForm().submit({
						url     : 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							if(resp.success){
								loading_win.show();
								shortSaleRequireForm.getForm().reset();
								loadRequireStatus(id_packages,id_shortsale,userid,processorActive);
								if(store) store.reload();
							}
							Ext.MessageBox.alert('', resp.mensaje);
						},
						params:{
							type	:	'notRequireDoc'
						},
						failure : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
						}
					});
				}
			}
		},'->',{
			 tooltip: 'Close',
			 cls:'x-btn-text-icon',
			 iconAlign: 'left',
			 text: ' ',
			 width: 30,
			 height: 30,
			 scale: 'medium',
			 icon: 'http://www.reifax.com/img/cancel.png',
			 handler: function(){ 
				 var tab = tabs.getItem(id);
				 tabs.remove(tab);
			 }
		}]
	});
	
	
	tabs.add({
		title		: ' Status / Upload Docs ',
		id			: id, 
		closable	: true,
		items		: shortSaleRequireForm,
		tbar		: followSSRDTbar
	}).show();
	
	loadRequireStatus(id_packages,id_shortsale,userid, processorActive);
}

function createShortSale(selected, store, userid, progressWin, progressBar){
	var processor = 'SELECT', package = 'SELECT';
	
	var simple = new Ext.FormPanel({
		frame: true,
		title: 'Make Short Sale',
		width: 490,		
		waitMsgTarget : 'Waiting...',
		labelWidth: 100,
		defaults: {width: 350},
		labelAlign: 'left',
		items: [{
			xtype         : 'combo',
			mode          : 'remote',
			fieldLabel    : 'Negotiator',
			triggerAction : 'all',
			width		  : 130,
			store         : new Ext.data.JsonStore({
				root:'results',
				totalProperty:'total',
				baseParams: {
					type: 'load-processor',
					'userid': userid,
					'SELECT': true
				},
				fields:[
					{name:'valor', type:'string'},
					{name:'texto', type:'string'}
				],
				url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
			}),
			displayField  : 'texto',
			valueField    : 'valor',
			name          : 'fprocessorname',
			value         : processor,
			hiddenName    : 'fprocessor',
			hiddenValue   : processor,
			allowBlank    : false,
			listeners	  : {
				'select'  : function(combo,record,index){
					processor = record.get('valor');
					combo.findParentByType('form').getForm().findField('fpackage').setValue("SELECT");
					package = 'SELECT';
				},
				beforequery: function(qe){
					delete qe.combo.lastQuery;
				}
			}
		},{
			xtype         : 'combo',
			mode          : 'remote',
			fieldLabel    : 'Package',
			triggerAction : 'all',
			width		  : 130,
			store         : new Ext.data.JsonStore({
				root:'results',
				totalProperty:'total',
				baseParams: {
					type: 'load-packages-make',
					'userid': userid,
					processor: processor,
					'SELECT': true
				},
				fields:[
					{name:'valor', type:'string'},
					{name:'texto', type:'string'}
				],
				url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
			}),
			displayField  : 'texto',
			valueField    : 'valor',
			name          : 'fpackagename',
			value         : package,
			hiddenName    : 'fpackage',
			hiddenValue   : package,
			allowBlank    : false,
			listeners	  : {
				'select'  : function(combo,record,index){
					package = record.get('valor');
				},
				beforequery: function(qe){
					delete qe.combo.lastQuery;
					qe.combo.getStore().setBaseParam('processor',processor); 
				}
			}
		}],
		
		buttons: [{
				text: 'Create',
				handler: function(){
					var values = simple.getForm().getValues();
					if(values.fprocessor =='-1' || values.fpackage == '-1'){
						Ext.Msg.alert("Warning", "You need to select a Negotiator and a Package.");
						return false;
					}
					win.close();
					progressWin.show();
					loading_win.show();
					createShortSaleProcess(selected, 0, store, userid, progressWin, progressBar, values.fprocessor, values.fpackage, 0, 0);				
				}
			},{
				text: 'Cancel',
				handler  : function(){
					progressWin.hide();
					simple.getForm().reset();
					win.close();
				}
			}]
		});
	 
	var win = new Ext.Window({
		layout      : 'fit',
		width       : 280,
		height      : 180,
		modal	 	: true,
		plain       : true,
		items		: simple,
		closeAction : 'close'
	});
	win.show();
}
function createShortSaleProcess(selected, ind, store, userid, progressWin, progressBar, processor, package, existe, insertado){
	progressBar.updateProgress(
		((ind+1)/selected.length),
		"Processing "+(ind+1)+" of "+selected.length
	);
	
	Ext.Ajax.request({  
		waitMsg: 'Checking...',
		url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php', 
		method: 'POST',
		timeout :86400,
		params: { 
			parcelid: selected[ind],
			userid: userid,
			processor: processor, 
			package: package,
			type: 'insert'
		},
		
		failure:function(response,options){
			loading_win.hide();
			progressWin.close();
			if(store) store.reload();
			Ext.MessageBox.alert('Warning','ERROR');
		},
		
		success:function(response,options){
			var r = Ext.util.JSON.decode(response.responseText);
			r.existe ? existe++ : insertado++;
			
			if((ind)==selected.length-1){
				loading_win.hide();
				progressWin.hide();
				if(store) store.reload();
				//Ext.MessageBox.alert('Follow Up','Short Sale Made: ('+insertado+'/'+selected.length+')<br>Already in Short Sale: ('+existe+'/'+selected.length+')<br>Short Sale Not Made: ('+(selected.length-insertado-existe)+'/'+selected.length+')');
				Ext.MessageBox.alert('Follow Up',insertado+' SS package created out of '+selected.length+'. '+existe+' already in the SS follow up list.');
			}else{
				 createShortSaleProcess(selected, ind+1, store, userid, progressWin, progressBar, processor, package, existe, insertado);
			}
		}  
	});
}