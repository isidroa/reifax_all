<?php
	include_once "../../properties_conexion.php"; 
	include_once 'class.googlevoice.php';
	conectar();
	
	$_POST['onlyFunctions']=true;
	include_once('properties_followupEmail.php');
	
	$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	$parcelid   = $_POST['pid'];
	$sendcell     = $_POST['sendcell'];
	$sendany   = $_POST['sendany'];
	$templatesms = $_POST['contracttemplate']; 
	$newtemplate = $_POST['sms_newtemplatetext']; 
	
	$q="select bd, offer, address, mlnumber, a.* from followup f
		LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$parcelid."')";
	$res = mysql_query($q) or die($q.mysql_error());
	while($r=mysql_fetch_array($res)){
		$county=$r['bd'];
		$rname      = $r['agent'];
		$remail     = $r['email'];
		$offer      = $r['offer'];
		$mlnumber = $r['mlnumber'];
		$address = $r['address'];
		
		$agentcell='';
		if($r['typeph1']==2){
			$agentcell=$r['phone1'];
		}else if($r['typeph2']==2){
			$agentcell=$r['phone2'];
		}else if($r['typeph3']==2){
			$agentcell=$r['phone3'];
		}else if($r['typeph4']==2){
			$agentcell=$r['fax'];
		}else if($r['typeph5']==2){
			$agentcell=$r['tollfree'];
		}else if($r['typeph6']==2){
			$agentcell=$r['phone6'];
		}
		if(($agentcell=='') && $userid!=1719 ){
			$resp = array('success'=>'true','enviado'=>'0');
			
			echo json_encode($resp);
			
			//Saved error in pending task.
			scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',1,'','ERROR: Phone not found.',$typeFollow);
			
			return;
		}
	}
	$agentcell = ereg_replace("[^0-9]", "", $agentcell);
	if(substr($phoneSend,0,1)!='1'){
		$agentcell='1'.$agentcell;
	}
	
	conectarPorNameCounty($county);
	
	$query = "SELECT x.email, x.name, x.surname, x.hometelephone, x.mobiletelephone, x.address,
			p.proffax, p.companyname
			FROM xima.ximausrs x inner join xima.profile p on x.userid=p.userid
			WHERE x.userid='$userid'";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);

	$userPhone    = strlen($row[4]) == 0 ? $row[3] : $row[4];
	$userName = $row[1].' '.$row[2];
	$emailUser = $row[0];
	$addressUser = $row[5];
	$userFax = $row[6];
	$userCompany = $row[7];
	
	//Elige template
	if($templatesms!=0){
		$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$templatesms";
		$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
		$mailTemplate = mysql_fetch_array($resultTemplate);
		
		$body = $mailTemplate['body'];
	}else{
		$body=$newtemplate;
	}
	
	include_once('../../mysetting_tabs/mycontracts_tabs/function_template.php'); 
	$body=replaceTemplate($body,$userid,$parcelid,$type,0,'','');
	$body=str_replace("'","\'",$body);
	
	$query='SELECT m.username, m.password, c.sms_phone FROM xima.contracts_phonesettings c
			inner join xima.contracts_mailsettings m on c.userid=m.userid
			where m.userid='.$userid;
	$result = mysql_query($query) or die($query.mysql_error());
	
	if(mysql_num_rows($result)==0){
		$resp = array('success'=>'false','msg'=>'Not provided the configuration of phone or mail settings, please provide it in Follow Up - My Settings'); 
	}else{
		$r=mysql_fetch_array($result);
		if(trim($r['sms_phone'])==''){
			$resp = array('success'=>'false','msg'=>'Google Voice phone not provided, please provide it in Follow Up - My Settings - Phone Settings'); 
			echo json_encode($resp);
			return;
		}
		
		$gv = new GoogleVoice($r['username'], $r['password']);
		$gv->sms($agentcell,$body);
		//return;
		
		$enviadoagente='1';
		$rSearch=array('"',"'");
		$rReplace=array('','');	
		
		//Insert Email
		$query="INSERT INTO  xima.follow_emails 
		(userid, msg_id, from_msg,fromName_msg, to_msg, subject, msg_date, type, attachments,task) 
		VALUES($userid,'".sha1($userid.$sub.date())."','".str_replace($rSearch,$rReplace,$from)."',
		'".str_replace($rSearch,$rReplace,$name)."', '".str_replace($rSearch,$rReplace,$agentcell)."', 
		'".str_replace($rSearch,$rReplace,$sub)."', NOW(), 1, 0, 1)";		
			 
		mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query))); 
		$idinsert = mysql_insert_id();
		
		//Insert body part
		$query="INSERT INTO  xima.follow_emails_body
		(idmail,body) VALUES
		($idinsert,'$body')";
		mysql_query($query) or die(json_encode(array('success' => false, 'error'=>$query)));

		assigmentProperty($userid,$idinsert,$parcelid,1,$_POST['typeFollow'],1,1,1,1,$offer);
		$completetask=$_POST['completetask'];
		if($completetask=='true'){
			completeTaskFollow($userid,0,$parcelid,1,'',0,1,1,1,0);		
		}
		
		$resp = array('success'=>'true','enviado'=>$enviadoagente,'cell'=> $agentcell,'status_voice'=>$gv->status); 
	}
		
	echo json_encode($resp);
?>
