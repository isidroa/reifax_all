<?php
	include_once "../../properties_conexion.php"; 
	conectar();
	
	$text="";
	$_POST['iforemsent']++;
	if($_POST['iforemsent']==1)
	{
		$text.="\r\n ".str_pad('Item', 8).str_pad('Address', 30).str_pad('Agent', 35).str_pad('Email', 35).str_pad('Status', 15).'Error Message';
		$text.="\r\n ======================================================================================================================================================================================================";
	}
	
	$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	$parcelid   = $_POST['pid'];
	$sendme     = $_POST['sendme'];
	$sendmail   = $_POST['sendmail'];
	$templateemail = $_POST['contracttemplate']; 
	$actionupload=$_POST['action-upload']; 
	$actiondelete=$_POST['action-delete']; 
	$cantidadupload=$_POST['cantidadupload']; 
	$completetask=$_POST['completetask']; 
	$nuevotemplate=$_POST['nuevotemplate']; 
	$nuevotemplatesub=$_POST['nuevotemplatesub'];
	$typeFollow = isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B';
	
	//print_r($_POST);
	
	$q="select bd, offer, address, mlnumber, f.agent, email, email2 from followup f
		LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) where f.userid=$userid and parcelid in ('".$parcelid."')";
	$res = mysql_query($q) or die($q.mysql_error());
	while($r=mysql_fetch_array($res)){
		$county=$r['bd'];
		$rname      = $r['agent'];
		$remail     = $r['email'];
		$offer      = $r['offer'];
		$mlnumber = $r['mlnumber'];
		$address = $r['address'];
		if($remail==''){
			$remail = $r['email2'];
		}
		if($remail=='' && $sendme!='on'){
			$aux='NOT SENT';
			$text.="\r\n ".str_pad($_POST['iforemsent'].'.-', 8).str_pad($address, 30).str_pad($rname, 35).str_pad($remail, 35).str_pad($aux, 15).'Empty email';
			$f1=fopen("../../saved_documents/temp/".$userid.$_POST['filecode'].".txt","a+");
			fputs($f1,$text);
			fclose($f1);	
			$resp = array('success'=>'true','enviado'=>'0','remail'=> $remail, 'erroremail'=> 'Empty email', 'address'=> $address, 'agent'=> '', 'filecode'=> $_POST['filecode'], 'userid'=> $userid );			
			echo json_encode($resp);
			//Saved error in pending task.
			$_POST['onlyFunctions']=true;
			include_once('properties_followupEmail.php');
			scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',5,'','ERROR: Email not found.',$typeFollow);
			return;
		}
	}
	
	if($actionupload=='upload'){
		$j=7;
		for($i = 1; $i <= $cantidadupload; $i++){
			if($_FILES["archivo".$i]['name']!=''){
				$_FILES['file'.$j]=$_FILES['archivo'.$i];
				$j++;
			}
		}
	}else{
		$j=7;
		for($i = 1; $i <= $cantidadupload; $i++){
			if($_FILES["archivo".$i]['name']!=''){
				$_FILES['file'.$j]=$_FILES['archivo'.$i];
				$j++;
			}
		}
	}
	$query = "SELECT x.email, x.name, x.surname, x.hometelephone, x.mobiletelephone, x.address,
			p.proffax, p.companyname
			FROM xima.ximausrs x inner join xima.profile p on x.userid=p.userid
			WHERE x.userid='$userid'";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);

	$userPhone    = strlen($row[4]) == 0 ? $row[3] : $row[4];
	$userName = $row[1].' '.$row[2];
	$emailUser = $row[0];
	$addressUser = $row[5];
	$userFax = $row[6];
	$userCompany = $row[7];
	
	if($templateemail!=0 && $templateemail!=-1){
		conectarPorNameCounty($county);
		$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$templateemail";
		$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
		$mailTemplate = mysql_fetch_array($resultTemplate);
		
		$usarHtml=true;
		$subject = $mailTemplate['subject'];
		$body = $mailTemplate['body'];
		
		include_once('../../mysetting_tabs/mycontracts_tabs/function_template.php'); 
		$subject=replaceTemplate($subject,$userid,$parcelid,0,'','');
		$body=replaceTemplate($body,$userid,$parcelid,$type,0,'','');
	}else if($templateemail==0){
		$usarHtml=false;
		if($userid==933 || $userid==2846 || $userid==2883){
			$subject  = "{$address}-Cash Offer, You Represent Us";
			
			$body   .= "Dear,\r\n\r\n";
			$body   .= "Please find attached our offer for the property with proof of funds and earnest money deposit letter.  I would like to explain just a few things before you look at our offer.  We submit all our offers through our company Summit Home Buyers, LLC of which I am the Managing Member.  I am not a licensed real estate agent.\r\n\r\n ";
			$body    .= "You will notice on our contract that we've put your information on the broker/agent info, we do this on all our deals to give the listing agents the opportunity to act on our behalf so they can collect the commission on our side (the buyer's side) of the deal as well as the seller's.\r\n\r\n";
			$body    .= "We buy 15-20 investment properties a month, and I just wanted to explain our process so you would know what to expect as you'll probably be receiving a number of offers from us here on out.\r\n\r\n";
			$body    .= "    \t* We close Cash. We are well funded.\r\n";
			$body    .= "    \t* We close Fast and on Time with No Contingencies, making you look good with your asset manager. When we put a property under contract, We Close.\r\n";
			$body    .="    \t* Hassle Free Closing. You don't have to waste your time hand holding us to closing. We have closed over 100 deals.\r\n";
			$body    .="    \t* Let us do the Dirty Work. We know what we are doing and are not afraid of getting our hands dirty both with title issues and/or property condition and area(s).\r\n\r\n ";
			$body    .= "Please don't hesitate to contact us if you have any questions, email is always the quickest way to get in touch with me.  If you could let us know you've received our offer, and keep us updated we would appreciate it.  We look forward to hearing from you.\r\n\r\n ";
			$body   .="Thank you and have an amazing day!\r\n\r\n";
			$body    .= "Best Regards,\r\n";
			$body    .= "{$row[1]} {$row[2]}.\r\n";
			$body    .= "Summit Home Buyers, LLC\r\n";
			$body    .= "$phone\r\n";
		}else{
			$subject  = "Offer for the MLS number $mln, Address {$address}";
			if($userid==1719 || $userid==1641){
				$texto="Dear $name\r\n\r\n";
			}else{
				$texto="Dear Mr/Mrs $name\r\n\r\n";
			}
			$body     = $texto;
			$body   .= "Please find attached a contract with our offer regarding the ";
			$body   .= "property with the address: {$address}\r\n";
			$body   .= "We wait for your prompt response\r\n\r\n";
			$body   .= "Regards {$row[1]} {$row[2]}.\r\n";
			$body   .= "$phone\r\n";
			$body   .= "{$row[0]}\r\n\r\n";
		}
	}else if($templateemail==-1){
		$subject=$nuevotemplatesub;
		$body=$nuevotemplate;
	}
	
	$_POST['subject']=$subject; 
	$_POST['msg']=$body;
	$_POST['task']='5';
	$_POST['type']='composeEmail';
	
	if($sendmail=='on' && $sendme=='on'){
		$_POST['pid']=$parcelid;
		$_POST['to']=$remail;
		$_POST['cc']=true;
	}else if($sendmail=='on' && $sendme!='on'){
		$_POST['pid']=$parcelid;
		$_POST['to']=$remail;
		$_POST['cc']=false;
	}else if($sendmail!='on' && $sendme=='on'){
		$_POST['pid']='-1';
		$_POST['to']=$emailUser;
		$_POST['cc']=true;
	}
	
	ob_start();
	include('properties_followupEmail.php');
	$content = ob_get_contents();
	ob_end_clean();
	
	$respuesta=json_decode($content,true);
	$aux=($enviadoagente==1)?'SENT':'NOT SENT';
	$text.="\r\n ".str_pad($_POST['iforemsent'].'.-', 8).str_pad($address, 30).str_pad($rname, 35).str_pad($remail, 35).str_pad($aux, 15).$respuesta['msg'];
	
	if($respuesta['msg']=='' && $sendmail=='on'){
		$enviadoagente='1';
		$completetask=$_POST['completetask'];
		if($sendmail=='on' && $completetask=='true'){
			completeTaskFollow($userid,0,$parcelid,5,'',0,1,1,1,0);	
		}
	}else{
		$enviadoagente='0';
		//Saved error in pending task.
		scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],'NOW()',5,'','ERROR: '.$respuesta['msg'],$typeFollow);
	}
	
	$f1=fopen("../../saved_documents/temp/".$userid.$_POST['filecode'].".txt","a+");
	fputs($f1,$text);
	fclose($f1);	
 
	$resp = array('success'=>'true','enviado'=>'1', 'pdf'=>"$file", 'enviadoagente'=> $enviadoagente, 'remail'=> $remail, 'erroremail'=> $respuesta['msg'], 'address'=> $address, 'agent'=> $rname, 'filecode'=> $_POST['filecode'], 'userid'=> $userid );
	echo json_encode($resp);
?>