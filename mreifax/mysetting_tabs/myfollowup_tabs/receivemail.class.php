<?php
// Main ReciveMail Class File - Version 1.1 (02-06-2009)
/*
 * File: recivemail.class.php
 * Description: Reciving mail With Attechment
 * Version: 1.1
 * Created: 01-03-2006
 * Modified: 02-06-2009
 * Author: Mitul Koradia
 * Email: mitulkoradia@gmail.com
 * Cell : +91 9825273322
 */
 
/***************** Changes *********************
*
* 1) Added feature to retrive embedded attachment - Changes provided by. Antti <anttiantti83@gmail.com>
* 2) Added SSL Supported mailbox.
*
**************************************************/

class receiveMail
{
	var $server='';
	var $username='';
	var $password='';
	
	var $marubox='';					
	
	var $email='';			
	
	function receiveMail($username,$password,$EmailAddress,$mailserver='localhost',$servertype='pop',$port='110',$ssl = false, $type='INBOX') //Constructure
	{
		if($servertype=='imap')
		{
			if($port=='') $port='143'; 
			$strConnect='{'.$mailserver.':'.$port. '/imap'.($ssl ? "/ssl/novalidate-cert" : "").'}'.$type;
		}
		else
		{
			$strConnect='{'.$mailserver.':'.$port. '/pop3'.($ssl ? "/ssl/novalidate-cert" : "").'}'.$type; 
		}
		$this->server			=	$strConnect;
		$this->username			=	$username;
		$this->password			=	$password;
		$this->email			=	$EmailAddress;
	}
	function connect() //Connect To the Mail Box
	{
		$this->marubox=@imap_open($this->server,$this->username,$this->password);
		
		if($this->marubox===false)
		{
			$error='Error: Connecting to mail server';
			foreach(imap_errors() as $k => $e){
				$error.=', '.$e;
			}
			return $error;
		}
		return true;		
	}
	//Obtener la cantidad total de mails ordenados.
	function getCountHeaderMail($fTime,$type='INBOX',$date='2012-01-01',$format='ALL'){
		if($this->marubox===false)
			return false;
		
		$headers=imap_search($this->marubox, $format.' SINCE "'.$date.'"',SE_UID);
		
		if($headers){
			sort($headers);
			
			return $headers;
		}else return false;
	}
	
	function getHeaders($mid) // Get Header info
	{
		if($this->marubox===false)
			return false;
		
		$mail_details = false;
		$overview = imap_fetch_overview($this->marubox,$mid, FT_UID);
		$overview = $overview[0];
		$name = substr($overview->from,0,(strpos($overview->from,' <')!==false ? strpos($overview->from,' <') : strlen($overview->from)));

		if(strtolower($name)!='mailer-daemon' && strtolower($name)!='postmaster')
		{		
			preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i',$overview->from,$mailFromEmail);
			preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i',$overview->to,$mailToEmail);
			
			$mail_details=array(
				'from'=>$mailFromEmail[0],
				'fromName'=>iconv_mime_decode($name,0,"UTF-8"),
				'to'=>$mailToEmail[0],
				'date'=>$overview->date,
				'subject'=>iconv_mime_decode($overview->subject,0,"UTF-8"),
				'message_id'=>$overview->message_id,
				'msgno'=>$overview->msgno,
				'uid'=>$overview->uid,
				'size'=>$overview->size,
				'datereifax'=>date('Y-m-d H:i:s',strtotime($overview->date))			
			);			
		}
		//marcar como leidos los descargados inicialmente.
		@imap_setflag_full($this->marubox,$mid,'\\Seen',ST_UID);
		
		return $mail_details;
	}

	function get_mime_type(&$structure) //Get Mime type Internal Private Use
	{ 
		$primary_mime_type = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"); 
		
		if($structure->subtype) { 
			return $primary_mime_type[(int) $structure->type] . '/' . $structure->subtype; 
		} 
		return "TEXT/PLAIN"; 
	} 
	function get_part($stream, $msg_number, $mime_type, $structure = false, $part_number = false) //Get Part Of Message Internal Private Use
	{ 
		if(!$structure) { 
			$structure = imap_fetchstructure($stream, $msg_number, FT_UID); 
		} 
		if($structure) { 
			if($mime_type == $this->get_mime_type($structure))
			{ 
				if(!$part_number) 
				{ 
					$part_number = "1"; 
				} 
				$text = imap_fetchbody($stream, $msg_number, $part_number, FT_UID); 
				if($structure->encoding == 3) 
				{ 
					return imap_base64($text); 
				} 
				else if($structure->encoding == 4) 
				{ 
					return imap_qprint($text); 
				} 
				else
				{ 
					return $text; 
				} 
			} 
			if($structure->type == 1) /* multipart */ 
			{ 
				while(list($index, $sub_structure) = each($structure->parts))
				{ 
					if($part_number)
					{ 
						$prefix = $part_number . '.'; 
					} 
					$data = $this->get_part($stream, $msg_number, $mime_type, $sub_structure, $prefix . ($index + 1)); 
					if($data)
					{ 
						return $data; 
					} 
				} 
			} 
		} 
		return false; 
	} 
	
	function GetAttach($mid,$path) // Get Atteced File from Mail
	{
		if($this->marubox===false)
			return false;
		
		$struckture = imap_fetchstructure($this->marubox,$mid, FT_UID);
		$ar="";
		if($struckture->parts)
        {
			//if($mid==5804) print_r($struckture->parts);
			foreach($struckture->parts as $key => $value)
			{
				$enc=$struckture->parts[$key]->encoding;
				if($struckture->parts[$key]->ifparameters)
				{
					$name=$struckture->parts[$key]->parameters[0]->value;
					$att = $struckture->parts[$key]->parameters[0]->attribute;
					if($att != 'BOUNDARY' && $att != 'CHARSET' && $att != 'SIZE'){
							$message = imap_fetchbody($this->marubox,$mid,$key+1, FT_UID);
							//print_r($message);
							if ($enc == 0)
								$message = imap_8bit($message);
							if ($enc == 1)
								$message = imap_8bit ($message);
							if ($enc == 2)
								$message = imap_binary ($message);
							if ($enc == 3)
								$message = imap_base64 ($message); 
							if ($enc == 4)
								$message = quoted_printable_decode($message);
							if ($enc == 5)
								$message = $message;
							//print_r($message);
							$fp=fopen($path.$name,"w");
							fwrite($fp,$message);
							fclose($fp);
							$ar=$ar.$name."^";
					}
				}
				if($struckture->parts[$key]->ifdparameters)
				{
					$name= $struckture->parts[$key]->dparameters[0]->value;
					$att = $struckture->parts[$key]->dparameters[0]->attribute;
					if($att != 'BOUNDARY' && $att != 'CHARSET' && $att != 'SIZE'){
							$message = imap_fetchbody($this->marubox,$mid,$key+1, FT_UID);
							//print_r($message);
							if ($enc == 0)
								$message = imap_8bit($message);
							if ($enc == 1)
								$message = imap_8bit ($message);
							if ($enc == 2)
								$message = imap_binary ($message);
							if ($enc == 3)
								$message = imap_base64 ($message); 
							if ($enc == 4)
								$message = quoted_printable_decode($message);
							if ($enc == 5)
								$message = $message;
							//print_r($message);
							$fp=fopen($path.$name,"w");
							fwrite($fp,$message);
							fclose($fp);
							$ar=$ar.$name."^";
					}
				}
				
				
				// Support for embedded attachments starts here
				if($struckture->parts[$key]->parts)
				{
					foreach($struckture->parts[$key]->parts as $keyb => $valueb)
					{
						$enc=$struckture->parts[$key]->parts[$keyb]->encoding;
						
						if($struckture->parts[$key]->parts[$keyb]->ifparameters)
						{
							$name=$struckture->parts[$key]->parts[$keyb]->parameters[0]->value;
							$att = $struckture->parts[$key]->parts[$keyb]->parameters[0]->attribute;
							if($att != 'BOUNDARY' && $att != 'CHARSET' && $att != 'SIZE'){
									$partnro = ($key+1).".".($keyb+1);
									$message = imap_fetchbody($this->marubox,$mid,$partnro, FT_UID);
									if ($enc == 0)
										   $message = imap_8bit($message);
									if ($enc == 1)
										   $message = imap_8bit ($message);
									if ($enc == 2)
										   $message = imap_binary ($message);
									if ($enc == 3)
										   $message = imap_base64 ($message);
									if ($enc == 4)
										   $message = quoted_printable_decode($message);
									if ($enc == 5)
										   $message = $message;
									$fp=fopen($path.$name,"w");
									fwrite($fp,$message);
									fclose($fp);
									$ar=$ar.$name."^";
							}
						}
						if($struckture->parts[$key]->parts[$keyb]->ifdparameters)
						{
							$name=$struckture->parts[$key]->parts[$keyb]->dparameters[0]->value;
							$att = $struckture->parts[$key]->parts[$keyb]->dparameters[0]->attribute;
							if($att != 'BOUNDARY' && $att != 'CHARSET' && $att != 'SIZE'){
									$partnro = ($key+1).".".($keyb+1);
									$message = imap_fetchbody($this->marubox,$mid,$partnro, FT_UID);
									if ($enc == 0)
										   $message = imap_8bit($message);
									if ($enc == 1)
										   $message = imap_8bit ($message);
									if ($enc == 2)
										   $message = imap_binary ($message);
									if ($enc == 3)
										   $message = imap_base64 ($message);
									if ($enc == 4)
										   $message = quoted_printable_decode($message);
									if ($enc == 5)
										   $message = $message;
									$fp=fopen($path.$name,"w");
									fwrite($fp,$message);
									fclose($fp);
									$ar=$ar.$name."^";
							}
						}
					}
				}				
			}
		}
		$ar=substr($ar,0,(strlen($ar)-1));
		return $ar;
	}
	function getBody($mid) // Get Message Body
	{
		if($this->marubox===false)
			return false;
		
		$body = $this->get_part($this->marubox, $mid, "TEXT/HTML");
		if ($body == "")
			$body = $this->get_part($this->marubox, $mid, "TEXT/PLAIN");
		if ($body == "") { 
			return "";
		}
		return $body;
		//return imap_body($this->marubox, $mid, FT_UID);
	}
	function deleteMails($mid) // Delete That Mail
	{
		if($this->marubox===false)
			return false;
	
		imap_delete($this->marubox,$mid, FT_UID);
	}
	function close_mailbox() //Close Mail Box
	{
		if($this->marubox===false)
			return false;

		imap_close($this->marubox,CL_EXPUNGE);
	}
}
?>