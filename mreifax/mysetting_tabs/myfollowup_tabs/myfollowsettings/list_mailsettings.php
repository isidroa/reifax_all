<?php
include "../../../properties_conexion.php";
conectar();

$userid = $_POST['userid'];
$signs  = null;

$query  = "SELECT * FROM xima.contracts_mailsettings
				WHERE userid = $userid";
$rs     = mysql_query($query);
$row    = mysql_fetch_array($rs);
$server_delivery=$row['server'];

if($server_delivery!=''){
	if($server_delivery=='ssl://smtp.gmail.com'){
		$type_email='1';
	}else if($server_delivery=='smtp.mail.yahoo.com'){
		$type_email='3';
	}else if($server_delivery=='smtp.live.com'){
		$type_email='2';
	}else if($server_delivery=='smtp.aol.com '){
		$type_email='4';
	}else{
		$type_email='0';
	}
}else{
	$type_email='1';
}
$server_incoming=$row['imap_server'];
if($server_incoming!=''){
	if($server_incoming=='imap.gmail.com'){
		$type_imap='1';
	}else if($server_incoming=='smtp.mail.yahoo.com'){
		$type_imap='3';
	}else if($server_incoming=='pop3.live.com'){
		$type_imap='2';
	}else if($server_incoming=='pop.aol.com '){
		$type_imap='4';
	}else{
		$type_imap='0';
	}
}else{
	$type_email='1';
}
?>

<div id="div_list_imapsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<div id="div_list_smtpsett" style="padding-top:2px; color:#274F7B; font-weight:bold; font-size:14px;"></div>

<script type="text/javascript">

var imapStore = new Ext.data.SimpleStore({
	fields : ['id','name'],
	data   : [
		['1','IMAP'],
		['2','POP3'],
		['3','IMAP/SSL'],
		['4','POP3/SSL']
	]
});
var incomingStore = new Ext.data.SimpleStore({
	fields : ['id','name','server','port','imap'],
	data   : [
		['1','Gmail','imap.gmail.com','993','3'],
		['2','Hotmail','pop3.live.com','995','2'],
		['4','Aol','pop.aol.com','110','2'],
		['0','Other','','','2']
	]
});
var smtpsettForm = new Ext.FormPanel({
	title      : 'Email Reader Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Reader in the My Follow Up / My Emails.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'imap_service',
			id            : 'list_imap_service',
			hiddenName    : 'himap_service',
			fieldLabel    : 'Mail Service',
			store         : incomingStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_imap?>',
			listeners     : {
				'select'  : selectImap
			}
		},{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_imap=='0' ? 'false' : 'true';?>,
			id: 'list_imap_delivery',	
            items: [{
				xtype         : 'combo',
				name          : 'imap_type',
				id            : 'list_imap_type',
				hiddenName    : 'server_type',
				fieldLabel    : 'Type',
				store         : imapStore,
				triggerAction : 'all',
				mode          : 'local',
				editable      : false,
				selectOnFocus : true,
				displayField  : 'name',
				valueField    : 'id',
				value		  : '<?=$row['imap_type']>0 ? $row['imap_type'] : 2;?>',
				listeners     : {
					'select'  : function() {
						if (Ext.getCmp('list_imap_type').getValue() == '1')
							Ext.getCmp('list_imap_port').setValue(143);
							
						if (Ext.getCmp('list_imap_type').getValue() == '2')
							Ext.getCmp('list_imap_port').setValue(110);
							
						if (Ext.getCmp('list_imap_type').getValue() == '3')
							Ext.getCmp('list_imap_port').setValue(993);
					}
				}
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">If you want to use gmail please select IMAP/SSL</div> '
			},{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'imap_server',
				id         : 'list_imap_server',
				width      : '200',
				value      : '<?=addslashes($row['imap_server'])?>',
				allowBlank : false
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with imap.gmail.com )</div>'
			},{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				id         : 'list_imap_port',
				name       : 'imap_port',
				width      : '200',
				value      : '<?=strlen($row['imap_port'])>0 ? $row['imap_port'] : '143'?>',
				allowBlank : false
			}]
		},{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'imap_username',
			width      : '200',
			value      : '<?=$row['imap_username']?>',
			allowBlank : false
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'imap_password',
			value      : '<?=$row['imap_password']?>',
			width      : '200',
			allowBlank : true
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Incomming Settings</b></span>',
		handler : function(){
			if (smtpsettForm.getForm().isValid()) {
				smtpsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					params: {
						userid: <?=$userid?>
					},
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = Ext.getCmp("tabsListFollowSettings").getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_mailsettings.php', 
							cache : false,
							params: {
								userid: <?=$userid?>
							}
						});
					}
				});
			}
		}
	},{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Test Connection</b></span>',
		handler : function(){
			if (smtpsettForm.getForm().isValid()) {
				smtpsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/testmailsett.php',
					waitMsg : 'Testing...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	}]
});
var serviceStore = new Ext.data.SimpleStore({
	fields : ['id','name','server','port'],
	data   : [
		['1','Gmail','ssl://smtp.gmail.com','465'],
		['2','Hotmail','smtp.live.com','25'],
		['3','Yahoo','smtp.mail.yahoo.com','587'],
		['4','Aol','smtp.aol.com','587'],
		['0','Other','','']
	]
});
function selectImap(combo,record,index){
	var service=Ext.getCmp('list_imap_service').getValue();
	if(service==0){
		Ext.getCmp('list_imap_delivery').setVisible(true);
	}else{
		Ext.getCmp('list_imap_delivery').setVisible(false);
		
	}
	Ext.getCmp('list_imap_type').setValue(record.get('imap'));
	Ext.getCmp('list_imap_server').setValue(record.get('server'));
	Ext.getCmp('list_imap_port').setValue(record.get('port'));
}
function selectService(combo,record,index){
	var service=Ext.getCmp('list_delivery_service').getValue();
	if(service==0){
		Ext.getCmp('list_server_delivery').setVisible(true);
		Ext.getCmp('port_delivery').setVisible(true);
	}else{
		Ext.getCmp('list_server_delivery').setVisible(false);
		Ext.getCmp('port_delivery').setVisible(false);
		
	}
	Ext.getCmp('list_server_deliv').setValue(record.get('server'));
	Ext.getCmp('list_port_deliv').setValue(record.get('port'));
}
var imapsettForm = new Ext.FormPanel({
	title      : 'Email Delivery Configuration',
	border     : true,
	bodyStyle  : 'padding: 10px; padding-left:100px; text-align:left;',
	frame      : true,
	fileUpload : true,
	method     : 'POST',
	items      : [
		{
			html        : 'Please provide the server information to enable the Email Delivery in the Generate Contract Dialog.',
			cls         : 'x-form-item',
			style       : {
				marginBottom : '10px'
			}
		},{
			xtype         : 'combo',
			name          : 'delivery_service',
			id            : 'list_delivery_service',
			hiddenName    : 'hdelivery_service',
			fieldLabel    : 'Mail Service',
			store         : serviceStore,
			triggerAction : 'all',
			mode          : 'local',
			editable      : false,
			selectOnFocus : true,
			displayField  : 'name',
			valueField    : 'id',
			value		  : '<?=$type_email?>',
			listeners     : {
				'select'  : selectService
			}
		},{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_email=='0' ? 'false' : 'true';?>,
			id: 'list_server_delivery',	
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Server Address',
				name       : 'server',
				id		   : 'list_server_deliv',	
				width      : '200',
				value      : '<?=addslashes($row['server'])?>'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with ssl://smtp.gmail.com)</div>'
			}]
        },{
            xtype: 'panel',
            layout: 'form',
			hidden: <?=$type_email=='0' ? 'false' : 'true';?>,
			id: 'port_delivery',
            items: [{
				xtype      : 'textfield',
				fieldLabel : 'Port Number',
				name       : 'port',
				id		   : 'list_port_deliv',	
				width      : '200',
				value      : '<?=strlen($row['port'])>0 ? $row['port'] : '25'?>'
			},{
				xtype      : 'box',
				html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. Configure your server with mail.yourcompany.com or use a gmail account with ssl://smtp.gmail.com)</div>'
			}]
        },{
			xtype      : 'textfield',
			fieldLabel : 'Server Username',
			name       : 'username',
			id		   : 'list_user_delivery',
			width      : '200',
			value      : '<?=$row['username']?>'
		},{
			xtype      : 'box',
			html       : ' <div style="margin:10px;margin-left:110px;font-size:12px;">(Eg. yourname@yourcompany.com , username@gmail.com)</div>'
		},{
			xtype      : 'textfield',
			inputType  : 'password',
			fieldLabel : 'Server Password',
			name       : 'password',
			id		   : 'list_password_delivery',
			value      : '<?=$row['password']?>',
			width      : '200'
		}
	],
	buttonAlign :'center',
	buttons     : [{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Outgoing Settings</b></span>',
		handler : function(){
			if (imapsettForm.getForm().isValid()) {
				imapsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/savemailsett.php',
					waitMsg : 'Saving...',
					params: {
						userid: <?=$userid?>
					},
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
						var tab     = Ext.getCmp("tabsListFollowSettings").getActiveTab();
						var updater = tab.getUpdater();
						updater.update({
							url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_mailsettings.php', 
							cache : false,
							params: {
								userid: <?=$userid?>
							}
						});
					}
				});
			}
		}
	},{
		text    :'<span style=\'color: #4B8A08; font-size: 14px;\'><b>Test Connection</b></span>',
		handler : function(){
			if (imapsettForm.getForm().isValid()) { 
				imapsettForm.getForm().submit({
					url     : 'mysetting_tabs/mycontracts_tabs/testmailsett.php',
					params: {
						userid: <?=$userid?>
					},
					waitMsg : 'Testing...',
					success : function(f, a){ 
						var resp = a.result;
						Ext.MessageBox.alert('', resp.mensaje);
					}
				});
			}
		}
	}]
});

imapsettForm.render('div_list_imapsett');
smtpsettForm.render('div_list_smtpsett');

</script>
