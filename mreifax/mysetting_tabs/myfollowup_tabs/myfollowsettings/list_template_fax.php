<?php
	include "../../../properties_conexion.php";
	conectar();
	$userid=$_POST['userid'];
	$query  = "SELECT * FROM xima.templates
				WHERE userid = $userid and template_type=4 order by `default` desc";
	$rs     = mysql_query($query);
	$checkedTemplateid=0;
	while ($rowcr = mysql_fetch_array($rs)) {
		$checkedTemplate = $rowcr['name'];
		$checkedTemplateid = $rowcr['id'];
		
		$comboTemplates .= "['{$rowcr['id']}','{$rowcr['name']}'],";
	}
	$checked='';
	
?>
<style type="text/css">
.add-variable {
	background-image: url("http://www.reifax.com/img/add.gif") !important;
}
</style>
<div align="left" id="todo_listmytemplate_fax_panel" style="background-color:#FFF;border-color:#FFF">
	<br clear="all" />
	<div id="listmytemplate_fax_data_div" align="center" style=" background-color:#FFF; margin:auto; width:900px;">
    	<div id="listmytemplate_fax_filters"></div><br />
        <div id="listmytemplate_fax_properties" style="color:#274F7B; font-weight:bold; font-size:14px;"></div> 
	</div>
</div>
<script>
	var seleccionarTemplate= '<?php echo $checkedTemplate; ?>';
	var seleccionarTemplateId= <?php echo $checkedTemplateid; ?>;
	
	var storetemplates = new Ext.data.SimpleStore({
		fields : ['id','name'],
		data   : [
			<?php echo $comboTemplates;?>
			['0','New Template']
		]
	});
	var toolbarmydocstemplate=new Ext.Toolbar({
		renderTo: 'listmytemplate_fax_filters',
		items: [
			new Ext.Button({
				id: 'listnew_templatefax',
				tooltip: 'New Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/add.gif',
				handler: function(){
					newListTemplateFax();
				}
			}),new Ext.Button({
				id: 'listdelete_templatefax',
				tooltip: 'Delete Template',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/del_doc.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'fax',
							modo: 'eliminar',
							id : Ext.getCmp('listtemplate_idfax').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Template Deleted');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_fax.php', 
								cache : false,
								params	: {
									userid: <?php echo $userid; ?>
								}
							});
							
						}
					});	
				}
			}),new Ext.Button({
				id: 'listdefault_templatefax',
				tooltip: 'Set Default',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/arrow_green.png',
				handler: function(){
					loading_win.show();
					Ext.Ajax.request({
						url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
						method  : 'POST',
						params    : {
							type: 'fax',
							modo: 'setdefault',
							id : Ext.getCmp('listtemplate_idfax').getValue(),
							'userid': <?php echo $userid;?>
						},
						waitMsg : 'Getting Info',
						success : function(r) {
							loading_win.hide();
							Ext.MessageBox.alert('', 'Operation Completed');
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							updater.update({
								url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_fax.php', 
								cache : false,
								params	: {
									userid: <?php echo $userid; ?>
								}
							});
							
						}
					});	
				}
			}),{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 260,
				store         : new Ext.data.JsonStore({
					root:'results',
					totalProperty:'total',
					autoLoad: true, 
					baseParams: {
						type: 'loadtemplates',
						userid: <?php echo $userid; ?>,
						template_type: 4
					},
					fields:[
						{name:'id', type:'string'},
						{name:'name', type:'string'}
					],
					url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
					listeners     : {
						'load'  : function(store, records) {
							Ext.getCmp('listcombotemplatesfax').setValue(0); 
						}
					}
				}),
				displayField  : 'name',
				valueField    : 'id',
				name          : 'ftemplate',
				id			  : 'listcombotemplatesfax',
				value         : seleccionarTemplate,
				hiddenName    : 'ftemplatedoc11',
				hiddenValue   : seleccionarTemplate,
				allowBlank    : false,
				editable	  : false,
				listeners	  : {
					'select'  : function(combo,record,index){
						if(record.get('id')!=0){
							getListTemplateFax(record.get('id'));
						}else{
							newListTemplateFax();  
						}
					}
				}
			}	
		]
	});
	function newListTemplateFax(){
		Ext.getCmp('listdelete_templatefax').setVisible(false);
		Ext.getCmp('listdefault_templatefax').setVisible(false);
		Ext.getCmp('listnew_templatefax').setVisible(false);
		
		Ext.getCmp('listcombotemplatesfax').setValue(0);
		Ext.getCmp('listtemplatenamefax').setValue('Template name');
		Ext.getCmp('listtemplatenamefax').setReadOnly(false);
		Ext.getCmp('listtemplateeditorfax').setValue('');
		Ext.getCmp('listtemplate_idfax').setValue(0);
		Ext.getCmp('listdefaulttemplatefax').setValue(0);
		Ext.getCmp('listbuttontemplatefax').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Save Template</b></span>');
	}
	
	function getListTemplateFax(id){
		loading_win.show();
		Ext.getCmp('listdelete_templatefax').setVisible(true);
		Ext.getCmp('listdefault_templatefax').setVisible(true);
		Ext.getCmp('listnew_templatefax').setVisible(true);
		Ext.getCmp('listtemplatenamefax').setReadOnly(true);
		Ext.getCmp('listbuttontemplatefax').setText('<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>');
		Ext.Ajax.request({
			url     : 'mysetting_tabs/mycontracts_tabs/get_templates.php',
			method  : 'POST',
			params    : {
				type: 'docs',
				modo: 'obtener',
				id : id,
				'userid': <?php echo $userid;?>
			},
			waitMsg : 'Getting Info',
			success : function(r) {
				loading_win.hide();
				var resp   = Ext.decode(r.responseText);
	
				if (resp.data!=null) {
					Ext.getCmp('listtemplatenamefax').setValue(resp.data.name);
					Ext.getCmp('listtemplateeditorfax').setValue(resp.data.body);
					Ext.getCmp('listtemplate_idfax').setValue(resp.data.id);	
					Ext.getCmp('listdefaulttemplatefax').setValue(resp.data.defaulttemplate);
				} 
			}
		});
	}
	
	
	var variablesStoreFax=new Ext.data.ArrayStore({
		fields    : ['id', 'texto'],
		data      : [
			['','Select'],
			<?php echo $combo; ?>
		]
	});
	var variableListInsertarFax='<?php echo $checked; ?>';
	
	var template_docs_form = new Ext.FormPanel({
		bodyStyle  : 'padding: 10px; padding-left:10px; text-align:left;',
		frame      : true,
		method     : 'POST',
		renderTo: 'listmytemplate_fax_properties', 
		labelWidth: 60,
		items      : [
			{
				xtype		  : 'textfield',
				fieldLabel	  : 'Name',
				name		  : 'name',
				id		  : 'listtemplatenamefax',
				readOnly  : true,
				width: 300,
				allowBlank    : false,
				value: 'Template name'
			},{
				xtype         : 'combo',
				fieldLabel	  : 'Default',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 60,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
						['0','No'],
						['1','Yes']
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				name          : 'defaulttemplatefax',
				hiddenName    : 'defaulttemplatefax1',
				id			  : 'listdefaulttemplatefax',	
				allowBlank    : false,
				readOnly  : true,
				editable	  : false
			},{
				xtype:'htmleditor',
				id: 'listtemplateeditorfax',
				fieldLabel : 'Body',
				autoScroll: true,
				height:450,
				width: 800,
				plugins: [
						  new Ext.ux.form.HtmlEditor.Word()  
						 ],
				name: 'body',    
				enableSourceEdit : false
			},{
				xtype         : 'hidden',
				name          : 'userid',
				value         : <?php echo $userid; ?>
			},{
				xtype         : 'hidden',
				name          : 'template_id',
				id          : 'listtemplate_idfax'
			},{
				xtype         : 'hidden',
				name          : 'template_type',
				value         : 4
			}
		],
		buttonAlign :'center',
		buttons     : [{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Update Template</b></span>',
			id		: 'listbuttontemplatefax',
			handler : function(){
				if (template_docs_form.getForm().isValid()) {
					template_docs_form.getForm().submit({
						url     : 'mysetting_tabs/mycontracts_tabs/savetemplate.php',
						waitMsg : 'Saving...',
						success : function(f, a){ 
							var resp = a.result;
							Ext.MessageBox.alert('', resp.mensaje);
							
							var tab     = tabsTemplates.getActiveTab();
							var updater = tab.getUpdater();
							if (resp.mensaje == 'Template saved!') {
								updater.update({
									url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_fax.php', 
									cache : false,
									params	: {
										userid: <?php echo $userid; ?>
									}
								});
							}
						}
					});
				}
			}
		},{
			text    : '<span style=\'color: #4B8A08; font-size: 14px;\'><b>Clear</b></span>',
			handler : function(){
				Ext.getCmp('listtemplateeditorfax').setValue('');
			}
		}]
	});
	
	Ext.getCmp("listtemplateeditorfax").getToolbar().addItem([{
		xtype: 'tbseparator'
	}]);
	
	Ext.getCmp("listtemplateeditorfax").getToolbar().addItem([{
		xtype         : 'combo',
		mode          : 'local',
		fieldLabel    : 'Vars',
		triggerAction : 'all',
		displayField  : 'name',
		valueField    : 'id',
		name          : 'variableListInsertarFax',
		id          : 'variableListInsertarFax',
		value         : variableListInsertarFax,
		allowBlank    : false,
		editable	  : false,
		width: 		    200,
		store         : new Ext.data.JsonStore({
			root:'results',
			totalProperty:'total',
			autoLoad: true, 
			baseParams: {
				type: 'template-variables'
			},
			fields:[
				{name:'id', type:'string'},
				{name:'name', type:'string'}
			],
			url:'mysetting_tabs/myfollowup_tabs/properties_followagent.php',
			listeners     : {
				'load'  : function(store, records) {
					Ext.getCmp('variableListInsertarFax').setValue(variableListInsertarFax); 
				}
			}
		}),
		listeners	  : {
			'select'  : function(combo,record,index){
				variableListInsertarFax = record.get('id');
			}
		}
	}]);	
	Ext.getCmp("listtemplateeditorfax").getToolbar().addButton([{
		iconCls: 'icon', 
		icon: 'http://www.reifax.com/img/add.gif',
		handler: function () {
			Ext.getCmp("listtemplateeditorfax").insertAtCursor(variableListInsertarFax);
		},
		tooltip: 'Insert Variable'
	}]);
	
	newListTemplateFax();
</script>