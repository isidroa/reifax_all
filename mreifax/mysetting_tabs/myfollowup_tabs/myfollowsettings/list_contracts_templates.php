<?php
	$userid= $_POST['userid'];
?>
<div align="left" id="todo_listmytemplates_panel" style="border-color:#FFF">
	<br clear="all" />
	<div id="mytemplates_list_data_div" align="center" style=" margin:auto; width:950px;">
    	<div id="list_mytemplates_properties"></div> 
	</div>
</div>
<script>
	var tabsTemplates = new Ext.TabPanel(
	{
		renderTo: 'list_mytemplates_properties',
		activeTab: 0,
		plain:true,
		enableTabScroll:true,
		defaults:{  autoScroll: false},
		items:[
			  {
				title: 'Email',
				id: 'ListEmailTemplate',
				autoLoad: {
					url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_email.php', 
					scripts: true, 
					timeout: 600000,
					params	: {
						userid: <?php echo $userid; ?>
					}
				}
			  },{
				title: 'Documents',
				id: 'ListDocumentsTemplate',
				autoLoad: {
					url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_documents.php', 
					scripts: true, 
					timeout: 600000,
					params	: {
						userid: <?php echo $userid; ?>
					}
				}
			  },{
				title: 'SMS',
				id: 'ListSMSTemplate',
				autoLoad: {
					url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_sms.php', 
					scripts: true, 
					timeout: 600000,
					params	: {
						userid: <?php echo $userid; ?>
					}
				}
			  },{
				title: 'Fax',
				id: 'ListFaxTemplate',
				autoLoad: {
					url: 'mysetting_tabs/myfollowup_tabs/myfollowsettings/list_template_fax.php', 
					scripts: true, 
					timeout: 600000,
					params	: {
						userid: <?php echo $userid; ?>
					}
				}
			  }
			 ]
	});
</script>
