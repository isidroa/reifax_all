<?php
	include("../../properties_conexion.php");
	conectar();	
	
	$docsDir = 'F://ReiFaxDocs/';

	$userid = isset($_GET['userid']) ? $_GET['userid'] : $_POST['userid'];
	$mailid = isset($_GET['mailid']) ? $_GET['mailid'] : $_POST['mailid'];
	$pid	= isset($_POST['pid']) ? $_POST['pid'] : (isset($_GET['pid']) ? $_GET['pid'] : '-1');
	$tabs	= isset($_POST['tabs']) ? $_POST['tabs'] : 'tabsFollowId';
	$typeFollow = ($tabs=='tabsFollowId' || $tabs=='tabsFollowEmailsId') ? 'B' : 'S';
	$county = isset($_POST['county']) ? $_POST['county'] : '';
	if($pid==0) $pid='-1';
	
	if($pid!='-1'){
		$query='SELECT bd,status,statusalt FROM xima.followup WHERE userid='.$userid.' AND parcelid="'.$pid.'"';
		$result = mysql_query($query) or die($query.mysql_error());
		$r = mysql_fetch_array($result);
		
		$county = $r[0];
		$statusProperty = $r[1];
		$statusAltProperty = $r[2];
	}
	
	$progressBar = 'progressBarViewMail'.$mailid;
	$progressWin = 'progressWinViewMail'.$mailid;
	
	$query="UPDATE xima.follow_emails 
	SET seen=1 
	WHERE userid=$userid AND idmail=$mailid";
	mysql_query($query) or die($query.mysql_error());
	
	$query="SELECT fe.subject,fe.fromName_msg,fe.from_msg,IF(fe.to_msg is null,c.imap_username,fe.to_msg) to_msg,fe.msg_date,fb.body 
	FROM xima.follow_emails fe
	INNER JOIN xima.follow_emails_body fb ON (fe.idmail=fb.idmail) 
	INNER JOIN xima.contracts_mailsettings c ON (fe.userid=c.userid)
	WHERE fe.userid=$userid AND fe.idmail=$mailid";
	$result = mysql_query($query) or die($query.mysql_error());

?>
<style>

.overviewBotonCss3 {
  background: -moz-linear-gradient(center top , #FFFFFF, #EEEEEE) repeat scroll 0 0 transparent;
  border: thin solid #BBBBBB;
  border-radius: 4px 4px 4px 4px;
  color: #1D6AAA;
  font-weight: bold;
  padding: 10px 20px;
  position: relative;
  text-decoration: none;
  font-size:11px;
  display:block;
  float: left;
  width:0px;
  height:12px;
}

.mailSubject {
	background-color: transparent;
	border-bottom: 2px solid #EEEEEE;
	color: #222222;
	font-family: arial,sans-serif;
	font-size: 2.3em;
	font-weight: normal;
	margin: 12px 1px 9px;
	padding: 0 0 5px 8px;
}	

.mailButtons {
	float: right;
	height: 25px;
	margin-top: 5px;
}
.mailImgBut {
  left: 13px;
  max-height: 11px;
  position: absolute;
  top: 10px;
}
.mailImgButBig {
  left: 7px;
  max-height: 25px;
  position: absolute;
  top: 5px;
}
.overviewBotonCss3{
	margin-right: 4px;
}

.mailFrom{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailFromName{
	color:#222222;
	font-weight:bold;
}
.mailFromEmail{
	color:#555555;
}
.mailFromDate{
	color:#222222;
	float:right;
}

.mailTo{
	margin: 0 10px;
	font-size: 1.2em;
	font-family: arial,sans-serif;
}
.mailToEmail{
	color:#555555;
}

.mailBody{
	font-size: 120%;
	margin: 15px 10px 5px;
	text-align: left;
	height: 500px;
	overflow:auto;
	overflow-x:hidden;
}

.mailAttach {
  font-size: 14px;
  margin: 0 10px;
}
.mailAttachItem{
	padding-right: 5px;
}
</style>	

<?php
	if(mysql_num_rows($result)>0){
		$r = mysql_fetch_array($result);
?>

    <div class="mailButtons">
    	<a title="Delete Email" class="overviewBotonCss3" href="javascript:actionEmail('delete');">
        	<img title="Delete Email" class="mailImgButBig" src="http://www.reifax.com/img/myemail/deleteEmail.png" />
        </a>
    	<?php if($pid != '-1'){?>
        <a title="View Menu" class="overviewBotonCss3" href="javascript:actionEmail('view');">
        	<img title="View Menu" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/icono_ojo.png" />
        </a>
        <a title="New Scheduled Task" class="overviewBotonCss3" href="javascript:actionEmail('schedule');">
        	<img title="New Scheduled Task" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/scheduled_tasks.png" />
        </a>
        <a title="Send Contracts" class="overviewBotonCss3" href="javascript:actionEmail('contract');">
        	<img title="Send Contracts" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/guarantee.png" />
        </a>
        <a title="Send Email" class="overviewBotonCss3" href="javascript:actionEmail('email');">
        	<img title="Send Email" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/email.png" />
        </a>
        <a title="Send SMS" class="overviewBotonCss3" href="javascript:actionEmail('sms');">
        	<img title="Send SMS" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/phone.png" />
        </a>
        <a title="Send Fax" class="overviewBotonCss3" href="javascript:actionEmail('fax');">
        	<img title="Send Fax" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/fax.png" />
        </a>
        <a title="Make Call" class="overviewBotonCss3" href="javascript:actionEmail('call');">
        	<img title="Make Call" class="mailImgButBig" src="http://www.reifax.com/img/toolbar/gvoicemult.png" />
        </a>
    	<?php }?>
        <a title="reply" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',true,false,'<?php echo str_replace("'",'',$r['subject']);?>','<?php echo $r['from_msg'];?>','<?php echo $typeFollow;?>','<?php echo $county;?>');">
        	<img title="reply" class="mailImgBut" src="http://www.reifax.com/img/myemail/responder.png" />
        </a>
        <a title="forward" class="overviewBotonCss3" href="javascript:mailCompose(<?php echo $userid;?>,<?php echo $mailid;?>,'<?php echo $pid;?>',false,true,'<?php echo str_replace("'",'',$r['subject']);?>','','<?php echo $typeFollow;?>','<?php echo $county;?>');">
        	<img title="forward" class="mailImgBut" src="http://www.reifax.com/img/myemail/reenviar.png" />
        </a>
        <a title="close" class="overviewBotonCss3" href="javascript:closeTab();">
        	<img title="close" class="mailImgBut" src="http://www.reifax.com/img/myemail/cerrar.png" /> 
        </a>
    </div>
    <h1 class="mailSubject"><?php echo $r['subject'];?></h1>
    
    <div class="mailFrom">
    	From: 
        <span class="mailFromName"><?php echo $r['fromName_msg'];?></span>
        <span class="mailFromEmail"><?php echo $r['from_msg'];?></span>
        <span class="mailFromDate"><?php echo date('l jS \of F Y, h:i:s A',strtotime($r['msg_date']));?></span>
   	</div>
    <div class="mailTo">
        To: 
        <span class="mailToEmail"><?php echo $r['to_msg'];?></span>
   	</div>
    <div align="center" class="mailBody">
		<?php 
			
			$html = html_entity_decode(stripslashes($r['body']));			
			print_r(preg_replace('/[\x00-\x1F\x80-\xFF]/',"",str_replace("\n",'<br>',$html)));
			//print_r($html);
		?>
	</div>
    <div class="mailAttach">
    	<?php 
		$query='select * FROM xima.follow_emails_attach where idmail='.$mailid;
		$resultA = mysql_query($query) or die($query.mysql_error());
		if(mysql_num_rows($resultA)>0){
			$i=1;
			while($rA=mysql_fetch_array($resultA)){
				if(strlen($rA['url'])>0)
					echo '<a class="mailAttachItem" target="_blank" href="'.$rA['url'].'">'.($i++).' '.basename($rA['filename']).'</a><br>';
				else
					echo '<a class="mailAttachItem" target="_blank" href="/MailAttach/'.$userid.'/'.$rA['filename'].'">'.($i++).' '.$rA['filename'].'</a><br>';
			}
		}else{
    		echo '&nbsp;';
		}
		?>
    </div>
<?php }else{?>
	<div class="mailButtons">
        <a title="close" class="overviewBotonCss3" href="javascript:closeTab();">
        	<img title="close" class="mailImgBut" src="http://www.reifax.com/img/myemail/cerrar.png" /> 
        </a>
    </div>
    <h1 class="mailSubject">Sorry, the content has been removed.</h1>
<?php }?>

<script type="text/javascript" src="mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>
 <script>
	function closeTab(){
		var tabs = Ext.getCmp('<?php echo $tabs;?>');
		var tab = tabs.getItem('viewMailTab');
		tabs.remove(tab);
	}
	
	function actionEmail(type){
		var selected =new Array();
		var userid = '<?php echo $userid;?>';
		var pid = '<?php echo $pid;?>';
		var mailid = <?php echo $mailid;?>;
		var county = '<?php echo $county;?>';
		var status = '<?php echo $statusProperty;?>';
		var statusAlt = '<?php echo $statusAltProperty;?>';
		selected.push(pid);
		
		
		switch(type){
			case 'delete':
				loading_win.show();
				Ext.Ajax.request({
					waitMsg: 'Seeking...',
					url: 'mysetting_tabs/myfollowup_tabs/properties_followupEmail.php', 
					method: 'POST',
					timeout :600000,
					params: { 
						pids: mailid,
						userid: userid,
						type: 'delete'
					},
					
					failure:function(response,options){
						loading_win.hide();
						Ext.MessageBox.alert('Warning','ERROR');
					},
					success:function(response,options){
						loading_win.hide();
						closeTab();
					}
				});
			break;
			
			case 'email':
				sendEmail(selected,false,false,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'sms':
				sendEmailSms(selected,false,false,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'fax':
				sendEmailFax(selected,false,false,userid,<?php echo $progressWin;?>,<?php echo $progressBar;?>);
			break;
			
			case 'call':
				makeMultipleCalls(selected,false,userid,false,0,'<?php echo $typeFollow;?>'); 
				//creaVentana
			break;
			
			case 'schedule':
				var simple = new Ext.FormPanel({
					frame: true,
					title: 'Add Schedule',
					width: 490,
					waitMsgTarget : 'Waiting...',
					labelWidth: 100,
					defaults: {width: 350},
					labelAlign: 'left',
					items: [{
								xtype		  : 'datefield',
								allowBlank	  : false,
								name		  : 'odate',
								fieldLabel	  : 'Date',
								format		  : 'Y-m-d'
							},{
								xtype         : 'combo',
								mode          : 'local',
								fieldLabel    : 'Task',
								triggerAction : 'all',
								store         : new Ext.data.ArrayStore({
									id        : 0,
									fields    : ['valor', 'texto'],
									data      : [
										['1','Send SMS'],
										['2','Receive SMS'],
										['3','Send FAX'],
										['4','Receive FAX'],
										['5','Send EMAIL'],
										['6','Receive EMAIL'],
										['7','Send DOC'],
										['8','Receive DOC'],
										['9','Make CALL'],
										['10','Receive CALL'],
										['11','Send R. MAIL'],
										['12','Receive R. MAIL'],
										['13','Send OTHER'],
										['14','Receive OTHER'],
										['15','Send VOICE MAIL'],
										['16','Receive VOICE MAIL'],
										['17','Make NOTE']
									]
								}),
								displayField  : 'texto',
								valueField    : 'valor',
								name          : 'ftaskname',
								value         : '1',
								hiddenName    : 'task',
								hiddenValue   : '1',
								allowBlank    : false
							},{
								xtype: 'button',
								tooltip: 'View Contacts',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/agent.png',
								handler: function(){
									loading_win.show();
									Ext.Ajax.request({
										waitMsg: 'Seeking...',
										url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
										method: 'POST',
										timeout :600000,
										params: { 
											pid: pid,
											userid: userid,
											type: 'assignment'
										},
										
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','ERROR');
										},
										success:function(response,options){
											loading_win.hide();
											var r=Ext.decode(response.responseText);
											creaVentana(0,r,pid,userid);
										}
									});
								}
							},{
								xtype	  :	'textarea',
								height	  : 100,
								name	  : 'detail',
								fieldLabel: 'Schedule Detail'
							},{
								xtype     : 'hidden',
								name      : 'typeFollow',
								value     : '<?php echo $typeFollow;?>'
							}],
					
					buttons: [{
							text: 'Create',
							handler: function(){
								var values = simple.getForm().getValues();
								win.close();
								<?php echo $progressWin;?>.show();
								crearTasks('mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',userid,selected,0,values,<?php echo $progressBar;?>,<?php echo $progressWin;?>,false);
							}
						},{
							text: 'Reset',
							handler  : function(){
								simple.getForm().reset();
								win.close();
							}
						}]
					});
				 
				var win = new Ext.Window({
					layout      : 'fit',
					width       : 490,
					height      : 400,
					modal	 	: true,
					plain       : true,
					items		: simple,
					closeAction : 'close',
					buttons: [{
						text     : 'Close',
						handler  : function(){
							win.close();
						}
					}]
				});
				win.show();
			break
			
			case 'contract':
				sendContractJ(selected,false,false,userid, <?php echo $progressWin;?>,<?php echo $progressBar;?>,false,false);
			break
			
			case 'view':
				var simple = new Ext.FormPanel({
				url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
				frame: true,
				width: 220,
				waitMsgTarget : 'Waiting...',
				labelWidth: 100,
				defaults: {width: 200},
				labelAlign: 'left',
				items: [
					new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Contact',
						handler: function(){
							win.close();
							loading_win.show();
							Ext.Ajax.request({
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									pid: pid,
									userid: userid,
									type: 'assignment'
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									loading_win.hide();
									var r=Ext.decode(response.responseText);
									creaVentana(0,r,pid,userid);
								}
							});
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Change Status',
						handler: function(){
							win.close();
							statusAltPanel(null,userid,pid,status);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Overview',
						handler: function(){
							win.close();
							createOverview(county,pid,statusAlt,false,false);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Follow History',
						handler: function(){
							win.close();
							
							var recordHistory = Ext.data.Record.create([
								'status',
								'county'
							]);
							
							var myRecordHistory = new recordHistory({
								status: statusAlt,
								county: county
							});
							
							createFollowHistory(Ext.getCmp('<?php echo $tabs;?>'), 'followTab', myRecordHistory, pid, '<?php echo $typeFollow;?>', 0, userid);
						}
					}),new Ext.Button({
						//tooltip: 'Click to show 100 follows per page.',
						height: 45,
						text: 'Go to Contract',
						handler: function(){
							win.close();
							Ext.Ajax.request( 
							{  
								waitMsg: 'Seeking...',
								url: 'mysetting_tabs/myfollowup_tabs/myfollowcontract/properties_seek_contract.php', 
								method: 'POST',
								timeout :600000,
								params: { 
									pid: pid,
									userid: userid
								},
								
								failure:function(response,options){
									loading_win.hide();
									Ext.MessageBox.alert('Warning','ERROR');
								},
								success:function(response,options){
									
									var r=Ext.decode(response.responseText);
									
									var Digital=new Date()
									var hours=Digital.getHours()
									var minutes=Digital.getMinutes()
									var seconds=Digital.getSeconds()
									if(r.results=='error'){
										Ext.Msg.alert("Follow Up", 'The Contract has not been generated or is not in the system');
									}else{ 
										if(Ext.isIE){
											window.open('http://docs.google.com/gview?url='+r.url+'?time='+hours+minutes+seconds,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
											//window.open(r.url,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
										}else{
											window.open('http://docs.google.com/gview?url='+r.url+'?time='+hours+minutes+seconds,'_newtab');
										}
									}
									return false;
								}                                
							});
						}
					})
				]
			});
			var win = new Ext.Window({
				layout      : 'fit',
				width       : 230,
				height      : 313,
				modal	 	: true,
				plain       : true,
				items		: simple,
				closeAction : 'close',
				buttons: [{
					text     : 'Close',
					handler  : function(){
						win.close();
						loading_win.hide();
					}
				}]
			});
			win.show();
			break
		}
	}
	
	var <?php echo $progressBar;?> = new Ext.ProgressBar({
		text: ""
	});
	
	var <?php echo $progressWin;?> =new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [<?php echo $progressBar;?>]
	});
	
	var fechaAcc=<?php echo "'".date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+5, date("Y")))."'"; ?>;
	var fechaClo=<?php echo "'".date('m/d/Y',mktime(0, 0, 0, date("m")  , date("d")+35, date("Y")))."'"; ?>;
	
	window.scroll(0,0);
</script>