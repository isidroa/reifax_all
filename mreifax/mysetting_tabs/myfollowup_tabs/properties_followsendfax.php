<?php
	include_once $_SERVER['DOCUMENT_ROOT']."/properties_conexion.php"; 
	include_once $_SERVER['DOCUMENT_ROOT'].'/FPDF/dompdf/dompdf_config.inc.php';
	include_once('followemail.class.php');
	conectar();
	
	$userid 	= isset($_POST['userid']) ? $_POST['userid'] : $_COOKIE['datos_usr']['USERID'];
	$parcelid   = $_POST['pid'];
	$sendme     = $_POST['sendme'];
	$sendmail   = $_POST['sendmail'];
	$templateemail = $_POST['contracttemplate']; 
	$actionupload=$_POST['action-upload']; 
	$actiondelete=$_POST['action-delete']; 
	$cantidadupload=$_POST['cantidadupload']; 
	$completetask=$_POST['completetask']; 
	$nuevotemplate=isset($_POST['body']) ? $_POST['body'] : $_POST['nuevotemplate'];
	$typeFollow = isset($_POST['typeFollow']) ? $_POST['typeFollow'] : 'B'; 
	// agregado por Luis R Castro 21/08/2015 Proyecto Contacts
	$contactname=isset($_POST['contactname']) ? $_POST['contactname'] : 0;
	
	$status = 'PENDING';
	 
	conectar();
	$q="select bd, offer, address, mlnumber, a.* 
	from xima.followup f
	LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid) 
	where f.userid=$userid and parcelid in ('".$parcelid."')";
	$res = mysql_query($q) or die($q.mysql_error());
	while($r=mysql_fetch_array($res)){
		$county=$r['bd'];
		$rname      = $r['agent'];
		$remail     = $r['email'];
		$offer      = $r['offer'];
		$mlnumber = $r['mlnumber'];
		$address = $r['address'];
		$agentfax='';
		if($r['typeph1']==3){
			$agentfax=$r['phone1'];
		}else if($r['typeph2']==3){
			$agentfax=$r['phone2'];
		}else if($r['typeph3']==3){
			$agentfax=$r['phone3'];
		}else if($r['typeph4']==3){
			$agentfax=$r['phone4'];
		}else if($r['typeph5']==3){
			$agentfax=$r['phone5'];
		}else if($r['typeph6']==3){
			$agentfax=$r['phone6'];
		}else if(strlen($r['fax'])>0){
			$agentfax=$r['fax'];
		}
		if($agentfax==''){
			if($r['typeph1']==6){
				$agentfax=$r['phone1'];
			}else if($r['typeph2']==6){
				$agentfax=$r['phone2'];
			}else if($r['typeph3']==6){
				$agentfax=$r['phone3'];
			}else if($r['typeph4']==6){
				$agentfax=$r['phone4'];
			}else if($r['typeph5']==6){
				$agentfax=$r['phone5'];
			}else if($r['typeph6']==6){
				$agentfax=$r['phone6'];
			}else if(strlen($r['fax'])>0){
				$agentfax=$r['fax'];
			}
		}
		if($agentfax==''){
			$status = 'ERROR: Fax not found.';
		}
	}
	

	$allowedExtensions = array("pdf");
	$archivos=array();
	$ruta = getCwd().'/myfollowupload/';
	if($actionupload=='upload'){
		$errorextension='';
		for($i = 1; $i <= $cantidadupload; $i++){
			if($_FILES["archivo".$i]['name']!=''){
				$name_of_uploaded_file = basename($_FILES["archivo".$i]['name']);
				$path_of_uploaded_file = $ruta . $name_of_uploaded_file;
				$tmp_path = $_FILES["archivo".$i]["tmp_name"];
				if(in_array(end(explode(".", $name_of_uploaded_file)), $allowedExtensions)){
					if(is_uploaded_file($tmp_path)){
						if(!copy($tmp_path,$path_of_uploaded_file)){
						  $errors .= '\n error while copying the uploaded file';
						}else{
							$archivos[]=$path_of_uploaded_file;
						}	
					}
				}else{
					$errorextension='Only pdf files are allowed';
					break;
				}
			}
		}
		if($errorextension!=''){
			echo json_encode(array('success' => false, 'msg'=>$errorextension));
			return;
		}
	}else{
		for($i = 1; $i <= $cantidadupload; $i++){
			if($_FILES["archivo".$i]['name']!=''){
				$name_of_uploaded_file = basename($_FILES["archivo".$i]['name']);
				$path_of_uploaded_file = $ruta . $name_of_uploaded_file;
				$archivos[]=$path_of_uploaded_file;
			}
		}
	}
	
	$body='';
	//Elige template de email a utilizar
	if($templateemail>0 && !isset($_POST['body'])){
		
		$queryTemplate  = "SELECT * FROM xima.templates WHERE userid = $userid and id=$templateemail";
		$resultTemplate=mysql_query($queryTemplate) or die($queryTemplate.mysql_error());
		$mailTemplate = mysql_fetch_array($resultTemplate);
		
		$body = $mailTemplate['body'];
		
		
	}else{
		$body=$nuevotemplate;
	}
	
	conectarPorNameCounty($county);
	
	include_once($_SERVER['DOCUMENT_ROOT'].'/mysetting_tabs/mycontracts_tabs/function_template.php'); 
	$body=replaceTemplate($body,$userid,$parcelid,0,'','');
		
	$rutapdftk = $_SERVER['DOCUMENT_ROOT'].'/overview_contract';
	$archivo = $ruta.'template.pdf';
	$dompdf = new DOMPDF();
	
	$dompdf->load_html($body);
	//echo $body;
	//return;
	$dompdf->render();
	
	$pdfoutput = $dompdf->output();
	$filename = $pdfoutput;
	
	$fp = fopen($archivo, "a");
	fwrite($fp, $pdfoutput);
	fclose($fp); 
	$pdfs=' ';
	
	foreach($archivos as $arch){
		$pdfs.=$arch.' ';
	}
	$docsDir = 'F:/ReiFaxDocs/';
	$dir = $docsDir."MailAttach/$userid/";
	//Crear Directorio de Attachment
	if(!is_dir($dir)){
		mkdir($dir);
	}
	$fecha= date('YmdHms');
	$attachpdf=$dir.'fax_'.sha1($fecha.$userid.$parcelid).'.pdf';
	
	passthru("{$rutapdftk}/pdftk/pdftk {$archivo} {$pdfs} cat output {$attachpdf}");
	
	$agentfax = ereg_replace("[^0-9]", "", $agentfax);
	if(substr($agentfax,0,1)=='1'){
		$agentfax=substr($agentfax,1);
	}
	$query    = "SELECT * FROM xima.contracts_mailsettings m
				inner join xima.contracts_phonesettings p on m.userid=p.userid
				WHERE m.userid = $userid";
	$rs       = mysql_query($query);
	$mailInfo = mysql_fetch_array($rs);
	$sendTo = strtolower($agentfax.'@'.$mailInfo['fax_domain']);
	
	if($status!='PENDING'){
		$dir = $docsDir."schedule_contract/$userid/";
		$file = 'fax_'.sha1($fecha.$userid.$parcelid).'.pdf';
		copy($attachpdf,$dir.$file);
		$attachEmail=array('dir'=> $dir.$file, 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.$file);
		
		scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],3,'',$status,$typeFollow,1,'','',-1,'',$attachEmail);
		$resp = array('success'=>'true','enviado'=>'0');
		echo json_encode($resp);
		return;
	}
	
	$_POST['subject']=''; 
	$_POST['msg']='';
	$_POST['task']='3';
	$_POST['type']='composeEmail';
		// agregado por Luis R Castro 21/08/2015 Proyecto Contacts
	if($contactname>0)
	{
	$query = "select * from xima.followagent WHERE userid=$userid AND agentid=$contactname";
	$rs    = mysql_query($query);
	$row   = mysql_fetch_array($rs);
		if($r['typeph1']==3){
			$agentfax=$r['phone1'];
		}else if($r['typeph2']==3){
			$agentfax=$r['phone2'];
		}else if($r['typeph3']==3){
			$agentfax=$r['phone3'];
		}else if($r['typeph4']==3){
			$agentfax=$r['phone4'];
		}else if($r['typeph5']==3){
			$agentfax=$r['phone5'];
		}else if($r['typeph6']==3){
			$agentfax=$r['phone6'];
		}else if(strlen($r['fax'])>0){
			$agentfax=$r['fax'];
		}
		if($agentfax==''){
			if($r['typeph1']==6){
				$agentfax=$r['phone1'];
			}else if($r['typeph2']==6){
				$agentfax=$r['phone2'];
			}else if($r['typeph3']==6){
				$agentfax=$r['phone3'];
			}else if($r['typeph4']==6){
				$agentfax=$r['phone4'];
			}else if($r['typeph5']==6){
				$agentfax=$r['phone5'];
			}else if($r['typeph6']==6){
				$agentfax=$r['phone6'];
			}else if(strlen($r['fax'])>0){
				$agentfax=$r['fax'];
			}
		}
		if($agentfax==''){
			$status = 'ERROR: Fax not found.';
		}
	$sendTo = strtolower($agentfax.'@'.$mailInfo['fax_domain']);
		}
	////////////////////////////////////////////

	$_POST['pid']=$parcelid;
	$_POST['to']=$sendTo;
	$_POST['file7']='fax_'.sha1($fecha.$userid.$parcelid).'.pdf';
	
	ob_start();
		include_once('properties_followupEmail.php');
		$content = ob_get_contents();
	ob_end_clean();
	
	$respuesta=json_decode($content,true);
	if($respuesta['msg']==''){
		$enviadoagente='1';
		$completetask=$_POST['completetask'];
		if($completetask=='true'){
			completeTaskFollow($userid,0,$parcelid,3,'',0,1,1,1,0);
				
		}
	}else{
		$enviadoagente='0';
		$dir = $docsDir."schedule_contract/$userid/";
		$file = 'fax_'.sha1($fecha.$userid.$parcelid).'.pdf';
		copy($attachpdf,$dir.$file);
		$attachEmail=array('dir'=> $dir.$file, 'url'=>'http://www.reifax.com/docs/schedule_contract/'.$userid.'/'.$file);
		//Saved error in pending task.
		scheduleTaskFollow($userid,$parcelid,$_COOKIE['datos_usr']['USERID'],3,'','ERROR: '.$respuesta['msg'],$typeFollow,1,'','',-1,'',$attachEmail);
	}
	if($actiondelete=='delete'){
		foreach($archivos as $archivo){
			if(file_exists($archivo))
				unlink($archivo);
		}
	}
	
	$resp = array('success'=>'true','pass'=> "{$rutapdftk}/pdftk/pdftk {$archivo} {$pdfs} cat output {$attachpdf}",'enviado'=>$enviadoagente, 'pdf'=>"$attachpdf", 'enviadoagente'=> $enviadoagente);
		
	echo json_encode($resp);
	
?>
