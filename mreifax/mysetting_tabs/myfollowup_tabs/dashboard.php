<div class="dashboard">
    <table>
        <tr>
            <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                <div
                <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; text-align:center;">
                    <a id="buttonRefreshDashboard" class="overviewBotonCss3" href="javascript:void(0);">Refresh</a>
                    <label class="bluetext">
                        Type: 
                    </label>
                    <select class="shortBox" id="Type" name="type">
                        <option value="B">Buying</option>
                        <option value="S">Selling</option>
                    </select>
                </div>
            </th>
        </tr>
    </table>
    
    <div id="properties" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Properties</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>All
                </td>
                <td>
                    <span id="all">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Active For Sale
                </td>
                <td>
                    <span id="afs">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Non-Active
                </td>
                <td>
                    <span id="na">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Not For Sale
                </td>
                <td>
                    <span id="nfs">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Sold
                </td>
                <td>
                    <span id="s">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="new" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">New</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="vm">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="sms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="fax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Emails
                </td>
                <td>
                    <span id="email">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="status" class="columns">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Status</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Pending Contracts
                </td>
                <td>
                    <span id="pc">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Contracts Sent
                </td>
                <td>
                    <span id="cs">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Offers Received
                </td>
                <td>
                    <span id="or">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Proof of Funds
                </td>
                <td>
                    <span id="pof">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>
                    Earnest Money Deposit
                </td>
                <td>
                    <span id="emd">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>
                    Addendums
                </td>
                <td>
                    <span id="ad">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div><span id="nameCO">Counter Offers</span>
                </td>
                <td>
                    <span id="co">--</span>
                </td>
            </tr>
        </table>
	</div>
    
    <div class="clear">&nbsp;</div>
    
    <table>
        <tr>
            <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                <div style="float:left; margin-left:5px; margin-top:5px; color:#FFF; width:49%;">
                    <label class="bluetext">
                        Period: 
                    </label>
                    <select class="shortBox" id="Period1" name="period1">
                        <option value="A">All</option>
                        <option value="T">Today</option>
                        <option value="LW">Last Week</option>
                        <option value="LM">Last Month</option>
                        <option value="B">Between</option>
                    </select>
                    <div class="between" id="betweenContainer1">
                    	<label class="bluetext">
                            From: 
                        </label>
                        <input type="text" id="from" name="from" />
                        <label class="bluetext">
                            To: 
                        </label>
                        <input type="text" id="to" name="to" />
                    </div> 
                </div>
                <div style="float:left; margin-left:10px; margin-top:5px; color:#FFF; width:49%;">
                    <label class="bluetext">
                        Period: 
                    </label>
                    <select class="shortBox" id="Period2" name="period2">
                        <option value="A">All</option>
                        <option value="D">Due</option>
                        <option value="T">Today</option>
                        <option value="1W">1 Week</option>
                        <option value="1M">1 Month</option>
                        <option value="B">Between</option>
                    </select>
                    <div class="between" id="betweenContainer2">
                    	<label class="bluetext">
                            From: 
                        </label>
                        <input type="text" id="from2" name="from2" />
                        <label class="bluetext">
                            To: 
                        </label>
                        <input type="text" id="to2" name="to2" />
                    </div>
                </div>
            </th>
        </tr>
    </table>
    
    <div id="task" class="columns two">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Task</span>
                </th>
                <th>
                    <span class="bluetext">Sent</span>
                </th>
                <th>
                    <span class="bluetext">Received</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Call
                </td>
                <td>
                    <span id="scall">--</span>
                </td>
                <td>
                    <span id="rcall">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="ssms">--</span>
                </td>
                <td>
                    <span id="rsms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="sfax">--</span>
                </td>
                <td>
                    <span id="rfax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Email
                </td>
                <td>
                    <span id="semail">--</span>
                </td>
                <td>
                    <span id="remail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Documents
                </td>
                <td>
                    <span id="sdoc">--</span>
                </td>
                <td>
                    <span id="rdoc">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Mail
                </td>
                <td>
                    <span id="smail">--</span>
                </td>
                <td>
                    <span id="rmail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="svmail">--</span>
                </td>
                <td>
                    <span id="rvmail">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Other
                </td>
                <td>
                    <span id="sother">--</span>
                </td>
                <td>
                    <span id="rother">--</span>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="pending" class="columns two">
    	<div class="loadingBlock"></div>
        <table>
            <tr>
                <th>
                    <span class="bluetext">Pending Task</span>
                </th>
                <th>
                    <span class="bluetext">Qty</span>
                </th>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Call
                </td>
                <td>
                    <span id="pcall">--</span>
                </td>
            </tr>
            <tr class="impar">
                <td>
                    <div class="cuote"></div>SMS
                </td>
                <td>
                    <span id="psms">--</span>
                </td>
            </tr>
            <tr class="par">
                <td>
                    <div class="cuote"></div>Fax
                </td>
                <td>
                    <span id="pfax">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Email
                </td>
                <td>
                    <span id="pemail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Documents
                </td>
                <td>
                    <span id="pdoc">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Mail
                </td>
                <td>
                    <span id="pmail">--</span>
                </td>
            </tr>
            
            <tr class="par">
                <td>
                    <div class="cuote"></div>Voice Mail
                </td>
                <td>
                    <span id="pvmail">--</span>
                </td>
            </tr>
            
            <tr class="impar">
                <td>
                    <div class="cuote"></div>Other
                </td>
                <td>
                    <span id="pother">--</span>
                </td>
            </tr>
        </table>
    </div>
</div> 

<script>
	$(document).ready(function(){		
		loadAllData();
		$('#Type').bind('change',loadAllData);
		$('#Period1').bind('change',loadPeriod1Data);
		$('#Period2').bind('change',loadPeriod2Data);
		$('#buttonRefreshDashboard').click(loadAllData);
		
		var dates1 = $( "#betweenContainer1 #from, #betweenContainer1 #to" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		var nowDate = new Date();
		var currMonth = nowDate.getMonth()+1;
		if(currMonth<10){
			currMonth='0'+currMonth;
		}
		var currDay = nowDate.getDate();
		if(currDay<10){
			currDay='0'+currDay;
		}
		var currYear = nowDate.getFullYear();
		dates1.val(currYear+'-'+currMonth+'-'+currDay);
		
		var dates2 = $( "#betweenContainer2 #from2, #betweenContainer2 #to2" ).datepicker({
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat: 'yy-mm-dd'
		});
		dates2.val(currYear+'-'+currMonth+'-'+currDay); 
		
		$('#betweenContainer1 #from').change(loadPeriod1Data);
		$('#betweenContainer1 #to').change(loadPeriod1Data);
		$('#betweenContainer2 #from2').change(loadPeriod2Data); 
		$('#betweenContainer2 #to2').change(loadPeriod2Data); 
	
		
		function loadProperties(type){
			var loading = $('#properties .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=properties&userid=<?php echo $_COOKIE['datos_usr']['USERID'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#properties #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadNew(type){
			var loading = $('#new .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=new&userid=<?php echo $_COOKIE['datos_usr']['USERID'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#new #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadStatus(type){
			var loading = $('#status .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=status&userid=<?php echo $_COOKIE['datos_usr']['USERID'];?>&type="+type,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#status #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadTask(type,period1){
			var period1 = $('#Period1').val();
			var from = $('#betweenContainer1 #from').val();
			var to = $('#betweenContainer1 #to').val();
			
			var loading = $('#task .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=task&userid=<?php echo $_COOKIE['datos_usr']['USERID'];?>&type="+type+"&period1="+period1+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$.each(v, function(i2,v2){
							$('#task #'+i2).html(v2);
						});
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadPendingTask(type){
			var period2 = $('#Period2').val();
			var from = $('#betweenContainer2 #from2').val();
			var to = $('#betweenContainer2 #to2').val();
			
			var loading = $('#pending .loadingBlock');
			loading.fadeIn(300);
			
			$.ajax({
				type	:'POST',
				url		:'mysetting_tabs/myfollowup_tabs/properties_dashboard.php',
				data	: "loadType=pending&userid=<?php echo $_COOKIE['datos_usr']['USERID'];?>&type="+type+"&period2="+period2+"&from="+from+"&to="+to,
				dataType:'json',
				success	:function (result){
					
					$.each(result, function(i,v){
						$('#pending #'+i).html(v);
					});
					loading.fadeOut(300);
				}
			})
		}
		
		function loadAllData(){
			var type = $('#Type').val();
			
			if(type=='B')
				$('#nameCO').html('Counter Offers');
			else
				$('#nameCO').html('Offers');
			
			loadProperties(type);
			loadNew(type);
			loadStatus(type);
			
			loadTask(type);
			loadPendingTask(type);
		}
		
		function loadPeriod1Data(){
			var type = $('#Type').val();
			
			if($('#Period1').val()=='B'){
				$( "#betweenContainer1:hidden" ).fadeIn(300);
			}else{
				$( "#betweenContainer1:visible" ).fadeOut(300);
			}
			loadTask(type);
		}
		
		function loadPeriod2Data(){
			var type = $('#Type').val();
			
			if($('#Period2').val()=='B'){ 
				$( "#betweenContainer2:hidden" ).fadeIn(300);
			}else{
				$( "#betweenContainer2:visible" ).fadeOut(300);
			}
			loadPendingTask(type);
		}
	});
	
</script>