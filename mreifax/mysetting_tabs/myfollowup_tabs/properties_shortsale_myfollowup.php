<?php
	include("../../properties_conexion.php");   
	conectar();
	include ("../../properties_getgridcamptit.php");	
	
	
	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Searc
	$ArIDCT = getArray('MYShortSale','result');
	
	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);
	
	$userid=$_COOKIE['datos_usr']['USERID']; 
	
	$query = "SELECT b.id_require,b.gridName,b.displayName,b.descName
	FROM xima.shortsale_require b 
	INNER JOIN xima.shortsale_section c ON b.id_section=c.id_section
	WHERE b.status=1 
	ORDER BY c.order,b.ordShow";
	$result = mysql_query($query);
	$gridCampos=array();
	
	while($r=mysql_fetch_array($result)){
		$gridCampos[]=array('desc'=>str_replace("'","\'",$r['descName']), 'name'=>$r['gridName'], 'longName'=>$r['displayName'], 'data'=>$r['gridName']);
	}
	//echo 'aqui'; return;
?>
<style>
.x-grid3-cell-inner {
  padding: 1px;
}
</style>
<div align="left" id="todo_mysellingfollowup_panel" style="background-color:#FFF;border-color:#FFF">
	<!--<br clear="all" />-->
	<div id="mysellingfollowup_data_div" align="center" style=" background-color:#FFF; margin:auto;">
  		<div id="myssfollowup_filters"></div>
        <!--<div align="left" style="color:#000; font-size:14px;">
        	Click View Icon -> Display Menu Options / Click Row -> Follow History / Click Email Icon -> Follow Email
       	</div>--><br />
        <div id="myssfollowup_properties" align="left"></div> 
	</div>
</div>
<script type="text/javascript" src="/mysetting_tabs/myfollowup_tabs/funcionesToolbar.js?<?php echo filemtime(dirname(__FILE__).'/funcionesToolbar.js'); ?>"></script>
<script>
	var useridspider= <?php echo $userid;?>;
	var limitmyssfollowup 			= 50;
	var selected_datamyssfollowup 	= new Array();
	var AllCheckmyssfollowup 			= false;
	
	//filter variables
	var filtersSS = {
		property		: {
			address		: '',
			county		: 'ALL'
		},
		processor		: 'ALL',
		package			: 'ALL',
		ss_status		: 'ALL',
		set				: 1,
		order			: {
			field		: 'address',
			direction	: 'ASC'
		},
		status			: {
		<?php foreach($gridCampos as $k=>$val){
			if($k>0) echo ',';
			echo "s".$val['data'].": '-1'";
		}?>
		}
	};
	
	var urlProgressBar = '';
	var countProgressBar = 0;
	var typeProgressBar = '';
	
	checkPendings(<?php echo $userid;?>);
	
	var storemyssfollowup = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'ss_status','processor','package',
			   {name: 'id_processor', type: 'int'},
			   {name: 'id_packages', type: 'int'},
			   {name: 'id_shortsale', type: 'int'}";
			   foreach($gridCampos as $k=>$val){
		   			echo ",'".$val['data']."'";
			   }
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: filtersSS.order.field,
			direction: filtersSS.order.direction
		},
		listeners: {
			'beforeload': function(store,obj){
				storemyssfollowupAll.load();
				AllCheckmyssfollowup=false;
				selected_datamyssfollowup=new Array();
				smmyssfollowup.deselectRange(0,limitmyssfollowup);
				obj.params.address=filtersSS.property.address;
				obj.params.county=filtersSS.property.county;
				obj.params.ss_status=filtersSS.ss_status;
				obj.params.processor=filtersSS.processor;
				obj.params.package=filtersSS.package;
				<?php foreach($gridCampos as $k=>$val){
					echo "obj.params."."s".$val['data']."=filtersSS.status."."s".$val['data'].";";
				}?>
				
			},
			'load' : function (store,data,obj){
				if (AllCheckmyssfollowup){
					Ext.get(gridmyssfollowup.getView().getHeaderCell(0)).first().addClass('x-grid3-hd-checker-on');
					AllCheckmyssfollowup=true;
					gridmyssfollowup.getSelectionModel().selectAll();
					selected_datamyssfollowup=new Array();
				}else{
					AllCheckmyssfollowup=false;
					Ext.get(gridmyssfollowup.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');
					var sel = [];
					if(selected_datamyssfollowup.length > 0){
						for(val in selected_datamyssfollowup){
							var ind = gridmyssfollowup.getStore().find('pid',selected_datamyssfollowup[val]);
							if(ind!=-1){
								sel.push(ind);
							}
						}
						if (sel.length > 0)
							gridmyssfollowup.getSelectionModel().selectRows(sel);
					}
				}
			}
		}
    });
	
	var storemyssfollowupAll = new Ext.data.JsonStore({
        url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
		fields: [
           <?php 
		   		echo "'pid','userid','county'";
		   		foreach($hdArray as $k=>$val){
		   			echo ",'".$val->name."'";
				}
				echo ",'ss_status','processor','package',
			   {name: 'id_processor', type: 'int'},
			   {name: 'id_packages', type: 'int'},
			   {name: 'id_shortsale', type: 'int'}";
			   foreach($gridCampos as $k=>$val){
		   			echo ",'".$val['data']."'";
			   }
		   ?>
        ],
		root: 'records',
		totalProperty: 'total',
		baseParams: {
			'userid': <?php echo $userid;?>
		},
		remoteSort: true,
		sortInfo: {
			field: filtersSS.order.field,
			direction: filtersSS.order.direction
		},
		listeners: {
			'beforeload': function(store,obj){
				obj.params.address=filtersSS.property.address;
				obj.params.county=filtersSS.property.county;
				obj.params.ss_status=filtersSS.ss_status;
				obj.params.processor=filtersSS.processor;
				obj.params.package=filtersSS.package;
				<?php foreach($gridCampos as $k=>$val){
					echo "obj.params."."s".$val['data']."=filtersSS.status."."s".$val['data'].";";
				}?>
			}
		}
    });

	function creaMenuSS(e,rowIndex){ 
		x = e.clientX;
 		y = e.clientY; 
		var record = gridmyssfollowup.getStore().getAt(rowIndex);
		var pid = record.get('pid');
		var county = record.get('county');
		var status = record.get('status');
		var address = record.get('address');
		
		var simple = new Ext.FormPanel({
			url: 'mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',
			frame: true,
			width: 220,
			waitMsgTarget : 'Waiting...',
			labelWidth: 100,
			defaults: {width: 200},
			labelAlign: 'left',
			items: [
				new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Contact',
					handler: function(){
						win.close();
						loading_win.show();
						Ext.Ajax.request({
							waitMsg: 'Seeking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								pid: pid,
								userid: useridspider,
								type: 'assignment'
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							success:function(response,options){
								loading_win.hide();
								var r=Ext.decode(response.responseText);
								creaVentana(0,r,pid,useridspider);
							}
						});
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Overview',
					handler: function(){
						win.close();
						createOverview(county,pid,status,false,false);
					}
				}),new Ext.Button({
					//tooltip: 'Click to show 100 follows per page.',
					height: 45,
					text: 'Go to Follow History',
					handler: function(){
						win.close();
						gridmyssfollowup.fireEvent('rowclick', gridmyssfollowup, rowIndex);
					}
				}),new Ext.Button({
					height: 45,
					text: 'Go to Completed Task',
					handler: function(){
						win.close();
						var interval = setInterval(function(){
							if(document.getElementById('mysellingcompletetasks_properties') != null && loadedSCT===true){
								clearInterval(interval);
								filtersSSCT.property.address = address;
								storemyfollowcompletess.load({params:{start:0, limit:limitmyfollowcompleteselling}});
							}
						}, 500);
						tabsFollowSS.setActiveTab(2);
					}
				})
			]
		});
		var win = new Ext.Window({
			layout      : 'fit',
			width       : 230,
			height      : 260,
			modal	 	: true,
			plain       : true,
			items		: simple,
			closeAction : 'close',
			buttons: [{
				text     : 'Close',
				handler  : function(){
					win.close();
					loading_win.hide();
				}
			}]
		});
		win.show();
		return false;
	}
	
	function viewRender(value, metaData, record, rowIndex, colIndex, store) {
		return String.format('<a href="javascript:void(0)" title="Click to view Menu" onclick="creaMenuSS(event,{0})"><img src="../../img/toolbar/icono_ojo.png" width="20px" height="20px" /></a>',rowIndex);
	}
	
	var smmyssfollowup = new Ext.grid.CheckboxSelectionModel({
		checkOnly: true, 
		width:25,
		listeners: {
			"rowselect": function(selectionModel,index,record){
				if(selected_datamyssfollowup.indexOf(record.get('pid'))==-1)
					selected_datamyssfollowup.push(record.get('pid'));
				
				if(Ext.fly(gridmyssfollowup.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
					AllCheckmyssfollowup=true;
			},
			"rowdeselect": function(selectionModel,index,record){
				selected_datamyssfollowup = selected_datamyssfollowup.remove(record.get('pid'));
				AllCheckmyssfollowup=false;
				Ext.get(gridmyssfollowup.getView().getHeaderCell(0)).first().removeClass('x-grid3-hd-checker-on');				
			}
		}
	});
	
	var toolbarmySSfollow=new Ext.Toolbar({
		renderTo: 'myssfollowup_filters',
		items: [
			new Ext.Button({
				tooltip: 'Delete Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/myemail/deleteEmail.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be eliminated.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyssfollowup==true){
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1;i<storemyssfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.id_shortsale+'\'';	
						}
						
						//Confirmacion para eliminar todos
						Ext.MessageBox.show({
							title:    'Follow Up',
							msg:      '<strong style="color:red;">WARNING</strong> Confirming this action will delete all the properties from your  Following.<br>Are you sure you want to continue?',
							buttons: {yes: 'Accept',cancel: 'Cancel'},
							fn: deleteFollow
						});
					}else{	
						var totales = gridmyssfollowup.getSelectionModel().getSelections();
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1; i<totales.length; i++)
							pids+=',\''+totales[i].data.id_shortsale+'\''; 
						
						deleteFollow('yes'); 
					}
					
					function deleteFollow(btn){
						if(btn=='cancel'){
							loading_win.hide();
							return false;
						}
						
						Ext.Ajax.request( 
						{  
							waitMsg: 'Checking...',
							url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php', 
							method: 'POST',
							timeout :600000,
							params: { 
								type: 'delete',
								ids: pids
							},
							
							failure:function(response,options){
								loading_win.hide();
								Ext.MessageBox.alert('Warning','ERROR');
							},
							
							success:function(response,options){
								loading_win.hide();
								storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
								Ext.Msg.alert("Follow Up", 'Follow delete.');
								
							}                                
						});
					}
				}
			}),new Ext.Button({
				tooltip: 'Print Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/printer.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be printed.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyssfollowup==true){
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1;i<storemyssfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.id_shortsale+'\'';	
						}
					}else{	
						var totales = gridmyssfollowup.getSelectionModel().getSelections();
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1; i<totales.length; i++)
							pids+=',\''+totales[i].data.id_shortsale+'\''; 
					}
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_pdf.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 7,
							sort: filtersSS.order.field,
							dir: filtersSS.order.direction,
							typeFollow: 'SS',
							pids: pids,
							set: filtersSS.set,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.pdf;
							loading_win.hide();
							window.open(url);							
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Export Excel',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/excel.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be exported.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyssfollowup==true){
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1;i<storemyssfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.id_shortsale+'\'';	
						}
					}else{	
						var totales = gridmyssfollowup.getSelectionModel().getSelections();
						var pids='\''+totales[0].data.id_shortsale+'\'';
						for(i=1; i<totales.length; i++)
							pids+=',\''+totales[i].data.id_shortsale+'\''; 
					}
					
					loading_win.show();
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/myfollowprint/properties_excel.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							printType: 8,
							sort: filtersSS.order.field,
							dir: filtersSS.order.direction,
							typeFollow: 'SS',
							pids: pids,
							userid: <?php echo $userid;?>
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							var url='http://www.reifax.com/'+rest.excel;
							loading_win.hide();
							location.href= url;
						}                                
					});
				}
			}),new Ext.Button({
				tooltip: 'Filter Follow',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter.png',
				handler: function(){
					var formmyfollowupSS = new Ext.FormPanel({
						url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
						frame:true,
						bodyStyle:'padding:5px 5px 0;text-align:left;',
						title: 'Filters',
						id: 'formmyfollowupSS',
						name: 'formmyfollowupSS',
						items:[{
							xtype: 'fieldset',
							title: 'Filters',
							layout: 'table',
							layoutConfig: {columns:3},
							defaults: {width: 320},
							
							items: [{
								layout	: 'form',
								id		: 'faddress',
								items	: [{
									xtype		  : 'textfield',
									fieldLabel	  : 'Address',
									name		  : 'faddress',
									value		  : filtersSS.property.address,	
									listeners	  : {
										'change'  : function(field,newvalue,oldvalue){
											filtersSS.property.address=newvalue;
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fcounty',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'County',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-countys',
											'userid': <?php echo $userid;?>
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fcountyname',
									value         : filtersSS.property.county,
									hiddenName    : 'fcounty',
									hiddenValue   : filtersSS.property.county,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSS.property.county = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fstatus',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'SS Status',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['ALL','ALL'],
											['Preparing Package','Preparing Package'],
											['Package Complete','Package Complete'],
											['Package Faxed','Package Faxed'],
											['Package Received','Package Received'],
											['BPO Complete','BPO Complete'],
											['Counter Offer','Counter Offer'],
											['Verbal Agreement','Verbal Agreement'],
											['Written Agreement','Written Agreement'],
											['Closed','Closed']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fstatus',
									value         : filtersSS.ss_status,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSS.ss_status = record.get('valor');
										}
									}
								}]
							},{
								layout	: 'form',
								id		: 'fprocessor',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Negotiator',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-processor',
											'userid': <?php echo $userid;?>,
											'ALL': true
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fprocessorname',
									value         : filtersSS.processor,
									hiddenName    : 'fprocessor',
									hiddenValue   : filtersSS.processor,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSS.processor = record.get('valor');
											combo.findParentByType('form').getForm().findField('fpackage').setValue("ALL");
											filtersSS.package='ALL';
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
										}
									}
								}]
							},{
								layout	: 'form',
								colspan	: 2,
								id		: 'fpackage',
								items	: [{
									xtype         : 'combo',
									mode          : 'remote',
									fieldLabel    : 'Package',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.JsonStore({
										root:'results',
										totalProperty:'total',
										baseParams: {
											type: 'load-packages',
											'userid': <?php echo $userid;?>,
											processor: filtersSS.processor,
											'ALL': true
										},
										fields:[
											{name:'valor', type:'string'},
											{name:'texto', type:'string'}
										],
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php'
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'fpackagename',
									value         : filtersSS.package,
									hiddenName    : 'fpackage',
									hiddenValue   : filtersSS.package,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSS.package = record.get('valor');
										},
										beforequery: function(qe){
											delete qe.combo.lastQuery;
											qe.combo.getStore().setBaseParam('processor',filtersSS.processor); 
										}
									}
								}]
							}
							<?php foreach($gridCampos as $k=>$val){?>
							,{
								layout	: 'form',
								id		: 'f<?php echo $val['data'];?>',
								items	: [{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : '<?php echo $val['data'];?>',
									triggerAction : 'all',
									width		  : 130,
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['-1','-Select-'],
											['0','Not Required'],
											['1','Required'],
											['2','Uploaded'],
											['3','Acepted'],
											['4','Rejected']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'f<?php echo $val['data'];?>name',
									value         : filtersSS.status.<?php echo "s".$val['data'];?>,
									hiddenName    : 'f<?php echo $val['data'];?>',
									hiddenValue   : filtersSS.status.<?php echo "s".$val['data'];?>,
									allowBlank    : false,
									listeners	  : {
										'select'  : function(combo,record,index){
											filtersSS.status.<?php echo "s".$val['data'];?> = record.get('valor');
										}
									}
								}]
							}
							<?php }?>
							]
						}],
						bbar:[
							{
								iconCls		  : 'icon',
								//icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Load&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid:useridspider,
											type: 'load-filters'
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning',output);
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											if(rest.total==0){
												Ext.MessageBox.alert('Warning','You don\'t have default filters saved');
											}else{
												formmyfollowupSS.getForm().findField('faddress').setValue(rest.data.address);
												formmyfollowupSS.getForm().findField('fstatus').setValue(rest.data.status);
												formmyfollowupSS.getForm().findField('fcounty').setValue(rest.data.county);
												formmyfollowupSS.getForm().findField('fprocessor').setValue(rest.data.processor);
												formmyfollowupSS.getForm().findField('fpackage').setValue(rest.data.package);
												<?php foreach($gridCampos as $k=>$val){
													echo "formmyfollowupSS.getForm().findField('f".$val['data']."').setValue(rest.data."."s".$val['data'].");";
												}?>
											}
										}
									});
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/save.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Save&nbsp;&nbsp; ',
								handler  	  : function(){
									loading_win.show();
									Ext.Ajax.request({  
										waitMsg: 'Checking...',
										url:'mysetting_tabs/myfollowup_tabs/properties_shortsale.php',
										method: 'POST',
										timeout :120000, 
										params: { 
											userid: useridspider,
											type: 'save-filters',
											address: formmyfollowupSS.getForm().findField('faddress').getValue(),
											processor: formmyfollowupSS.getForm().findField('fprocessor').getValue(),
											package: formmyfollowupSS.getForm().findField('fpackage').getValue(),
											status: formmyfollowupSS.getForm().findField('fstatus').getValue(),
											county: formmyfollowupSS.getForm().findField('fcounty').getValue(),
											<?php foreach($gridCampos as $k=>$val){
												if($k>0) echo ',';
												echo "s".$val['data'].": formmyfollowupSS.getForm().findField('f".$val['data']."').getValue()";
											}?>
										},
										failure:function(response,options){
											loading_win.hide();
											Ext.MessageBox.alert('Warning','Error saving filters');
										},
										success: function(response,options){
											loading_win.hide();
											var rest = Ext.util.JSON.decode(response.responseText);
											Ext.MessageBox.alert('Warning','Default search successfully saved');
										}
									});
								}				
							},'->',{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/search.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Search&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSS.property.address 	= formmyfollowupSS.getForm().findField('faddress').getValue();
									filtersSS.processor 		= formmyfollowupSS.getForm().findField('fprocessor').getValue();
									filtersSS.package 			= formmyfollowupSS.getForm().findField('fpackage').getValue();
									filtersSS.ss_status 		= formmyfollowupSS.getForm().findField('fstatus').getValue();
									filtersSS.property.county 	= formmyfollowupSS.getForm().findField('fcounty').getValue();
									<?php foreach($gridCampos as $k=>$val){
										echo "filtersSS.status."."s".$val['data']."= formmyfollowupSS.getForm().findField('f".$val['data']."').getValue();";
									}?>
									
									storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
									win.close();
								}				
							},{
								iconCls		  : 'icon',
								icon		  : 'http://www.reifax.com/img/toolbar/reset.png',
								scale		  : 'medium',
								width		  : 120,
								text		  : 'Reset&nbsp;&nbsp; ',
								handler  	  : function(){
									filtersSS.property.address = '';
									filtersSS.processor = 'ALL';
									filtersSS.package = 'ALL';
									filtersSS.ss_status = 'ALL';
									filtersSS.property.county = 'ALL';
									<?php foreach($gridCampos as $k=>$val){
										echo "filtersSS.status."."s".$val['data']."= -1;";
									}?>
									
									formmyfollowupSS.getForm().findField('faddress').setValue('');
									formmyfollowupSS.getForm().findField('fprocessor').setValue('ALL');
									formmyfollowupSS.getForm().findField('fpackage').setValue('ALL');
									formmyfollowupSS.getForm().findField('fstatus').setValue('ALL');
									formmyfollowupSS.getForm().findField('fcounty').setValue('ALL');
									<?php foreach($gridCampos as $k=>$val){
										echo "formmyfollowupSS.getForm().findField('f".$val['data']."').setValue('-1');";
									}?>
								}
							}
						]
					});
					var win = new Ext.Window({
								layout      : 'fit',
								width       : 980,
								height      : 650,
								modal	 	: true,  
								plain       : true,
								items		: formmyfollowupSS,
								closeAction : 'close',
								buttons: [{
									text     : 'Close',
									handler  : function(){
										win.close();
									}
								}]
							});
							win.show();
				}
			}),new Ext.Button({
				tooltip: 'Remove Filters',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/filter-delete.png',
				handler: function(){
					filtersSS.property.address = '';
					filtersSS.processor = 'ALL';
					filtersSS.package = 'ALL';
					filtersSS.ss_status = 'ALL';
					filtersSS.property.county = 'ALL';
					<?php foreach($gridCampos as $k=>$val){
						echo "filtersSS.status."."s".$val['data']."= -1;";
					}?>
					storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
				}
			}),new Ext.Button({
				tooltip: 'View / Download Bank Package',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				//icon: 'http://www.reifax.com/img/icons_jesus/Preview-24.png',
				icon: 'http://www.reifax.com/img/toolbar/view-packages.png',
				handler: function(){
					if(selected_datamyssfollowup.length<=0 || selected_datamyssfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package to be printed.'); return false;
					}
					var record = gridmyssfollowup.getSelectionModel().getSelected();
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'custom_contract/pepe.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							county: record.get('county'),
							pid: record.get('pid'),
							idPackage: record.get('id_packages'),
							contractFontSize: '11',
							contractFontColor: 'black',
							contractFontType: 1,
							addonFontSize: '11',
							addonFontColor: 'black',
							addonFontType: 1,
							scrow: 'off',
							fieldAddeum: '',
							fieldAddonG: '',
							sendbyemail: 'no',
							offer: ''
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							loading_win.hide();
							if(rest.success){
								var url = 'http://www.reifax.com/'+rest.pdf;
								var Digital=new Date();
								var hours=Digital.getHours();
								var minutes=Digital.getMinutes();
								var seconds=Digital.getSeconds();
								
								//window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds);
								window.open(url+'?time='+hours+minutes+seconds);
							}else{
								Ext.MessageBox.alert('Warning',response.responseText);
							}
						}                                
					});
				}
			})/*,new Ext.Button({
				tooltip: 'View / Download Negotiator Package',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/view-processor-packages.png',
				handler: function(){
					if(selected_datamyssfollowup.length<=0 || selected_datamyssfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package to be printed.'); return false;
					}
					var record = gridmyssfollowup.getSelectionModel().getSelected();
					loading_win.show();
					
					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_shortsale.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							id_shortsale: record.get('id_shortsale'),
							type		: 'viewProcesorPackage'
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							var rest = Ext.util.JSON.decode(response.responseText);
							
							loading_win.hide();
							if(rest.success){
								var url = 'http://www.reifax.com/'+rest.pdf;
								var Digital=new Date();
								var hours=Digital.getHours();
								var minutes=Digital.getMinutes();
								var seconds=Digital.getSeconds();
								
								window.open(url+'?time='+hours+minutes+seconds);
							}else{
								Ext.MessageBox.alert('Warning',rest.msj);
							}
						}                                
					});
				}
			})*/,new Ext.Button({
				tooltip: 'View / Download Required Docs',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/icons_jesus/Preview-24.png',
				handler: function(){
					if(selected_datamyssfollowup.length<=0 || selected_datamyssfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package.'); return false;
					}
					var record = gridmyssfollowup.getSelectionModel().getSelected();
					var title = (record.get('address')+', '+record.get('processor')+' - '+record.get('package'));
					
					openUploadedDocs(tabsFollowSS, 'followSSUDTab', record.get('userid'), title, record.get('id_shortsale'), record.get('id_packages'), gridmyssfollowup.getStore(), false);
				}
			}),new Ext.Button({
				tooltip: 'View Status / Upload Required Docs',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/icons_jesus/sign-up-24.png',
				handler: function(){
					if(selected_datamyssfollowup.length<=0 || selected_datamyssfollowup.length>1){
						Ext.Msg.alert('Warning', 'You must previously select(check) one package.'); return false;
					}
					var record = gridmyssfollowup.getSelectionModel().getSelected();
					var title = (record.get('address')+', '+record.get('processor')+' - '+record.get('package'));
					
					openRequiredDocs(tabsFollowSS, 'followSSRDTab', record.get('userid'), title, record.get('id_shortsale'), record.get('id_packages'), gridmyssfollowup.getStore(), false);
				}
			})/*,new Ext.Button({
				tooltip: 'New Scheduled Task',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/scheduled_tasks.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records create task.'); return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					var simple = new Ext.FormPanel({
						frame: true,
						title: 'Add Schedule',
						width: 490,
						waitMsgTarget : 'Waiting...',
						labelWidth: 100,
						defaults: {width: 350},
						labelAlign: 'left',
						items: [{
									xtype		  : 'datefield',
									allowBlank	  : false,
									name		  : 'odate',
									fieldLabel	  : 'Date',
									format		  : 'Y-m-d'
								},{
									xtype         : 'combo',
									mode          : 'local',
									fieldLabel    : 'Task',
									triggerAction : 'all',
									store         : new Ext.data.ArrayStore({
										id        : 0,
										fields    : ['valor', 'texto'],
										data      : [
											['1','Send SMS'],
											['2','Receive SMS'],
											['3','Send FAX'],
											['4','Receive FAX'],
											['5','Send EMAIL'],
											['6','Receive EMAIL'],
											['7','Send DOC'],
											['8','Receive DOC'],
											['9','Make CALL'],
											['10','Receive CALL'],
											['11','Send R. MAIL'],
											['12','Receive R. MAIL'],
											['13','Send OTHER'],
											['14','Receive OTHER'],
											['15','Send VOICE MAIL'],
											['16','Receive VOICE MAIL'],
											['17','Make NOTE']
										]
									}),
									displayField  : 'texto',
									valueField    : 'valor',
									name          : 'ftaskname',
									value         : '1',
									hiddenName    : 'task',
									hiddenValue   : '1',
									allowBlank    : false
								},{
									xtype: 'button',
									cls:'x-btn-text-icon',
									iconAlign: 'left',
									text: ' ',
									width: 30,
									height: 30,
									scale: 'medium',
									icon: 'http://www.reifax.com/img/agent.png',
									handler: function(){
										loading_win.show();
										Ext.Ajax.request({
											waitMsg: 'Seeking...',
											url: 'mysetting_tabs/myfollowup_tabs/properties_followagent.php', 
											method: 'POST',
											timeout :600000,
											params: { 
												pid: pid,
												userid: <?php echo $userid;?>,
												type: 'assignment'
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												loading_win.hide();
												var r=Ext.decode(response.responseText);
												creaVentana(0,r,pid,useridspider);
											}
										});
									},listeners: {
										mouseover: function(but,e){
											but.setText('View Contacts');
											but.removeClass('x-btn-text-icon');
											but.addClass('x-btn-text-icon');
										},
										mouseout: function(but,e){
											but.setText(' ');
										}
									}
								},{
									xtype	  :	'textarea',
									height	  : 100,
									name	  : 'detail',
									fieldLabel: 'Schedule Detail'
								},{
									xtype     : 'hidden',
									name      : 'typeFollow',
									value     : 'SS'
								}],
						
						buttons: [{
								text: 'Create',
								handler: function(){
									var values = simple.getForm().getValues();
									win.close();
									progressBarFollow_winSS.show();
									crearTasks('mysetting_tabs/myfollowup_tabs/myfollowhistory/properties_followshedule.php',useridspider,selected_datamyssfollowup,0,values,progressBarFollowSS,progressBarFollow_winSS,storemyssfollowup);
								}
							},{
								text: 'Reset',
								handler  : function(){
									simple.getForm().reset();
									win.close();
								}
							}]
						});
					 
					var win = new Ext.Window({
						layout      : 'fit',
						width       : 490,
						height      : 400,
						modal	 	: true,
						plain       : true,
						items		: simple,
						closeAction : 'close',
						buttons: [{
							text     : 'Close',
							handler  : function(){
								win.close();
							}
						}]
					});
					win.show();
				}
			}),new Ext.Button({
				tooltip: 'Send Contracts',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/guarantee.png',
				handler: function(){
					var aux = 0;
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be updated.'); return false;
					}
					$.each(smmyssfollowup.getSelections(),function(i,ite){
						if(ite.data.offer <= 0){
							aux = aux + 1;
						}
					});
					if(aux > 0){
						Ext.MessageBox.alert('Warning','Sorry, some of the selected properties may have the offer price field empty. According to that, the contracts will not be send. Please, verify it and try it again.');
						 return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					sendContractJ(selected_datamyssfollowup,'',storemyssfollowup,useridspider, progressBarFollow_winSS, progressBarFollowSS,Ext.getCmp('mailenviadosfollowS'),false,true);
				}
			})*/,new Ext.Button({
				tooltip: 'Send Email',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/email.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be updated.'); return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					sendEmail(selected_datamyssfollowup,'',storemyssfollowup,useridspider, progressBarFollow_winSS, progressBarFollowSS,true);
				}
			}),new Ext.Button({
				tooltip: 'Send SMS',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/phone.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be updated.'); return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					sendEmailSms(selected_datamyssfollowup,'',storemyssfollowup,useridspider, progressBarFollow_winSS, progressBarFollowSS,true);
				}
			}),new Ext.Button({
				tooltip: 'Send Fax',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/fax.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be updated.'); return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					sendEmailFax(selected_datamyssfollowup,'',storemyssfollowup,useridspider, progressBarFollow_winSS, progressBarFollowSS,true);
				}
			})/*,new Ext.Button({
				tooltip: 'Block Properties',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/block.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to be blocked.'); return false;
					}
					
					loading_win.show();
					
					if(AllCheckmyssfollowup==true){
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						var pids='\''+totales[0].data.pid+'\'';
						for(i=1;i<storemyssfollowupAll.getCount();i++){
							pids+=',\''+totales[i].data.pid+'\'';	
						}
					}else{	
						var pids='\''+selected_datamyssfollowup[0]+'\'';
						for(i=1; i<selected_datamyssfollowup.length; i++)
							pids+=',\''+selected_datamyssfollowup[i]+'\''; 
					}

					Ext.Ajax.request( 
					{  
						waitMsg: 'Checking...',
						url: 'mysetting_tabs/myfollowup_tabs/properties_followblock.php', 
						method: 'POST',
						timeout :600000,
						params: { 
							type: 'block',
							pids: pids
						},
						
						failure:function(response,options){
							loading_win.hide();
							Ext.MessageBox.alert('Warning','ERROR');
						},
						
						success:function(response,options){
							loading_win.hide();
							Ext.Msg.alert("Follow Up", 'Added to Follow Block '+selected_datamyssfollowup.length+' properties.');
							storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
						}                                
					});
				}
			})*/,new Ext.Button({
				tooltip: 'Make Call',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: ' ',
				width: 30,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/gvoicemult.png',
				handler: function(){
					if(selected_datamyssfollowup.length==0){
						Ext.Msg.alert('Warning', 'You must previously select(check) the records to make call.'); return false;
					}
					if(AllCheckmyssfollowup==true){
						selected_datamyssfollowup=new Array();
						var totales = storemyssfollowupAll.getRange(0,storemyssfollowupAll.getCount());
						
						for(i=0;i<storemyssfollowupAll.getCount();i++){
							if(selected_datamyssfollowup.indexOf(totales[i].data.pid)==-1)
								selected_datamyssfollowup.push(totales[i].data.pid);
						}
					
					}
					makeMultipleCalls(selected_datamyssfollowup,storemyssfollowup,useridspider,false,0,'SS');
				}
			}),
			'->',
			new Ext.Button({
				id: 'syncPendingEmailSS',
				tooltip: 'Pending Email',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: '0',
				width: 40,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/refresh25.png',
				handler: function(){
					var interval = setInterval(function(){
						if(document.getElementById('myfollowmail_propertiesInbox') != null && loadedMEI===true){
							clearInterval(interval);
							filterResetMEI();
							totalMailInbox=0;
							currentMailInbox=0;
							sincMailInbox=1;
							storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox, currentMail:currentMailInbox, totalMail:totalMailInbox,checkmail:sincMailInbox}});
						}
					}, 500);
					tabs4.setActiveTab(2);
				}
			}),
			new Ext.Button({
				id: 'syncPendingContactSS',
				tooltip: 'Pending Contact',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: '0',
				width: 40,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/contactAssignCount.png',
				handler: function(){
					var interval = setInterval(function(){
						if(document.getElementById('myfollowmail_propertiesInbox') != null && loadedMEI===true){
							clearInterval(interval);
							filterResetMEI();
							filtersMEI.pending.contact = 0;
							storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
						}
					}, 500);
					tabs4.setActiveTab(2);
				}
			}),
			new Ext.Button({
				id: 'syncPendingPropertySS',
				tooltip: 'Pending Property',
				cls:'x-btn-text-icon',
				iconAlign: 'left',
				text: '0',
				width: 40,
				height: 30,
				scale: 'medium',
				icon: 'http://www.reifax.com/img/toolbar/propertyAssignCount.png',
				handler: function(){
					var interval = setInterval(function(){
						if(document.getElementById('myfollowmail_propertiesInbox') != null && loadedMEI===true){
							clearInterval(interval);
							filterResetMEI();
							filtersMEI.pending.property = 0;
							storemyfollowmailInbox.load({params:{start:0, limit:limitmyfollowmailInbox}});
						}
					}, 500);
					tabs4.setActiveTab(2);
				}
			})
		]
	});
	
	function ejecutarUpdatesFollow(response,options){
		progressBarFollowSS.updateProgress(
			((countProgressBar+1)/selected_datamyssfollowup.length),
			"Processing "+(countProgressBar+1)+" of "+selected_datamyssfollowup.length
		);
		
		var resp   = Ext.decode(response.responseText);
		if(resp.exito==1){
			if((countProgressBar)==selected_datamyssfollowup.length-1){
				progressBarFollow_winSS.hide();
				storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
			}else{
				countProgressBar++;
				Ext.Ajax.request({  
					waitMsg: 'Checking...',
					url: urlProgressBar, 
					method: 'POST',
					timeout :120000, 
					params: { 
						pids: '\''+selected_datamyssfollowup[countProgressBar]+'\'',
					},
					
					failure:function(response,options){
						progressBarFollow_winSS.hide();
						var output = '';
						for (prop in response) {
						  output += prop + ': ' + response[prop]+'; ';
						}
						Ext.MessageBox.alert('Warning',output);
					},
					
					success: ejecutarUpdatesFollow                      
				});
			}
		}else{
			progressBarFollow_winSS.hide();
			//smmyfollowcontract.deselectRange(0,limitmyssfollowup);
		}	
	}
	
	var progressBarFollowSS= new Ext.ProgressBar({
		text: ""
	});
	
	var progressBarFollow_winSS=new Ext.Window({
		title: 'Please wait...',
		y: 255,
		width:430,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		items: [progressBarFollowSS]
	});
	function userRender(value, metaData, rec, rowIndex, colIndex, store) { 
		var name = rec.get('name_follow');
		return String.format('<a href="javascript:void(0)" qtip="{1}">{0}</a>',value,name);
	}
	function renderNumeros(val){
		return '$'+val;
	}
	function checkShortSaleRender(value, metaData, rec, rowIndex, colIndex, store){
		switch(value){
			case '0': 
			return '<div title="Not Required" style="background-color: #D3D3D3; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;			
			
			case '1': 
			return '<div title="Required" style="background-color: #D01616; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '2': 
			return '<div title="Uploaded" style="background-color: #083772; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '3': 
			return '<div title="Acepted" style="background-color: #66ab16; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
			
			case '4': 
			return '<div title="Rejected" style="background-color: #EDED00; width:18px; height: 18px; border: 2px solid;">&nbsp;</div>'; break;
		}
	}
	
	var gridmyssfollowup = new Ext.grid.GridPanel({
		renderTo: 'myssfollowup_properties',
		cls: 'grid_comparables',
		width: 978,
		height: 3000,
		store: storemyssfollowup,
		stripeRows: true,
		sm: smmyssfollowup, 
		columns: [
			smmyssfollowup,
			<?php 
		   		echo "{header: '', hidden: true, editable: false, dataIndex: 'pid'}";
				echo ",{header: 'V', width: 25, sortable: false, tooltip: 'View.', dataIndex: 'msj', renderer: viewRender, menuDisabled: true}";
		   		echo ",{header: 'SS Status', width: 100, sortable: true, tooltip: 'Short Sale Status', dataIndex: 'ss_status', menuDisabled: true}";	
				foreach($hdArray as $k=>$val){
		   			if($val->name!='status')
						echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";	
				}
				echo ",{header: 'Negotiator', width: 80, sortable: true, tooltip: 'Short Sale Negotiator Name', dataIndex: 'processor'}";
				echo ",{header: 'Package', width: 80, sortable: true, tooltip: 'Short Sale Package Name', dataIndex: 'package'}";
				foreach($gridCampos as $k=>$val){
					echo ",{header: '".$val['name']."', width: 25, hidden: ".($k>19 ? 'true' : 'false').", sortable: true, tooltip: '".$val['desc']."', dataIndex: '".$val['data']."', renderer: checkShortSaleRender, menuDisabled: true}";
				}
				
		   ?>			 
		],
		tbar: new Ext.PagingToolbar({
			id: 'pagingmyssfollowup',
            pageSize: limitmyssfollowup,
            store: storemyssfollowup,
            displayInfo: true,
			displayMsg: 'Total: {2} Follow Up.',
			emptyMsg: "No follow to display",
			items: ['Show:',
			new Ext.Button({
				tooltip: 'Click to show 50 follows per page.',
				text: 50,
				handler: function(){
					limitmyssfollowup=50;
					Ext.getCmp('pagingmyssfollowup').pageSize = limitmyssfollowup;
					Ext.getCmp('pagingmyssfollowup').doLoad(0);
				},
				enableToggle: true,
				pressed: true,
				toggleGroup: 'show_res_groupSS'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 80 follows per page.',
				text: 80,
				handler: function(){
					limitmyssfollowup=80;
					Ext.getCmp('pagingmyssfollowup').pageSize = limitmyssfollowup;
					Ext.getCmp('pagingmyssfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSS'
			}),'-',new Ext.Button({
				tooltip: 'Click to show 100 follows per page.',
				text: 100,
				handler: function(){
					limitmyssfollowup=100;
					Ext.getCmp('pagingmyssfollowup').pageSize = limitmyssfollowup;
					Ext.getCmp('pagingmyssfollowup').doLoad(0);
				},
				enableToggle: true,
				toggleGroup: 'show_res_groupSS'
			}),
			'-',{
				xtype         : 'combo',
				mode          : 'local',
				triggerAction : 'all',
				width		  : 150,
				store         : new Ext.data.ArrayStore({
					id        : 0,
					fields    : ['valor', 'texto'],
					data      : [
					<?php
						$page = (count($gridCampos) / 20.00);
						$i=0.00;
						do{
							$i++;
							if($i>1) echo ',';
							echo "['$i','Set $i']";
						}while($i<$page)
					?>
					]
				}),
				displayField  : 'texto',
				valueField    : 'valor',
				value		  : '1',
				name          : 'gridcombo',
				allowBlank    : false,
				listeners	  : {
					'select'  : function(combo, record, index){
						var col = gridmyssfollowup.getColumnModel();
						var ini = 0, page = record.get('valor'), i=0;
						filtersSS.set = page;
						
						for(i=ini; i<<?php echo count($gridCampos);?>; i++){
							if(Math.floor(i/20.0) == (parseInt(page)-1))
								col.setHidden(i+8, false);
							else
								col.setHidden(i+8, true);
						}
					}
				}
			}]
        }),
		
		listeners: {
			'sortchange': function (grid, sorted){
				filtersSS.order.field=sorted.field;
				filtersSS.order.direction=sorted.direction;
			},
			'rowclick': function(grid, rowIndex, e){
				var cell = e ? grid.getView().findCellIndex(e.getTarget()) : -1; 
				var record = grid.getStore().getAt(rowIndex);
				var pid = record.get('pid');
				var id_shortsale = record.get('id_shortsale');
				var id_packages = record.get('id_packages');
				var userid = record.get('userid');
				var typeFollow = 'SS'; 
				
				if(cell >= 8){
					var title = (record.get('address')+', '+record.get('processor')+' - '+record.get('package'));
					
					openRequiredDocs(tabsFollowSS, 'followSSRDTab', userid, title, id_shortsale, id_packages, grid.getStore(), false);
					
				}else if(cell!=0 && cell!=1 && cell!=2){
					createFollowHistory(tabsFollowSS, 'followSSTab', record, pid, 'B', 0, userid);
				}
			}
		}
	});
	
	storemyssfollowup.load({params:{start:0, limit:limitmyssfollowup}});
</script>