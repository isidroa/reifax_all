<?php 
header("Cache-Control: no-store, no-cache, must-revalidate");
include("../../properties_conexion.php");
conectar();

$query='SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']["USERID"];
$result = mysql_query($query) or die($query.mysql_error());
$datos_usr=mysql_fetch_array($result);
if($datos_usr['setgroup']=='ALL') $allselect='';
else $allselect='and idCounty in ('.$datos_usr['setgroup'].') ';

$query = "Select * From xima.lsstate where state='Florida'";
//echo $query;
$result = mysql_query($query) or die($query." ".mysql_error());

$states="['0','Select'],";
while($row=mysql_fetch_array($result, MYSQL_ASSOC)){
	/*$dataCounty[$row["IdCounty"]]=$row;
	$idCounty_array[]=$row["IdCounty"];*/
	$states.="['".$row['IdState']."','".$row['State']."'],";
}
	

?>
	<div align="center" style=" background-color:#EAEAEA; height:auto;margin-top:-5px;padding-top:10px;">
    	<table width="96%" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
        	<tr align="left" >
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
           	</tr>
        	<tr >
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:16px; text-align:center">Default Values</td>                
           	</tr>
      	</table>
   	</div>
    <div id="formdefault1" align="center" style=" background-color:#EAEAEA; height:auto;margin-top:-5px;padding-top:10px;"></div>
<script>
	countydefault=search_county;
	function loadCountys(){
		var select_state=Ext.getCmp('cestado').getValue();
		countydefault='0';
		condados.load({params:{selectstate:select_state}}); 
	}
	function marcarcounty(){
		Ext.getCmp('ccondado').setValue(countydefault);
	}
	var condados=new Ext.data.JsonStore({  
        url:'mysetting_tabs/myaccount_tabs/getcountys.php',  
        root:'results',  
        fields: ['id','name']  
    });
	
	condados.load({params:{selectstate:search_state}});  
	var estados=new Ext.data.ArrayStore({
										fields: ['id','name'],
										data  : [<?php echo rtrim($states,','); ?>]
									});
	 var formdefault = new Ext.FormPanel({
				frame:true,
				id:'formdefault',
				method:'POST',
				bodyStyle:'padding:5px 5px 0;text-align:left;',
				items:[{
						xtype: 'combo',
						name: 'cestado',
						id: 'cestado',
						hiddenName	:'estado',
						fieldLabel: '<b>Select State</b>',
						typeAhead: true,
						store: estados,
						triggerAction: 'all',
						mode: 'local',
						editable: false,
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank: false,
						displayField:'name',
						valueField: 'id',
						value:search_state,
						width: 150,
						listeners: {
							 'select': loadCountys
						}
					},{
						xtype: 'combo',
						name: 'ccondado',
						id: 'ccondado',
						hiddenName	:'condado',
						fieldLabel: '<b>Select County</b>',
						typeAhead: true,
						store: condados,
						triggerAction: 'all',
						mode: 'local',
						editable: false,
						emptyText	:'Select ...',
						selectOnFocus:true,
						allowBlank: false,
						displayField:'name',
						valueField: 'id',
						width: 150
					}
				],
				buttonAlign:'left',		
				buttons     :[{
					text        :'<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save Defaults</b></span>',
					handler     :function(){
									var select_state=Ext.getCmp('cestado').getValue();
									var select_county=Ext.getCmp('ccondado').getValue();
									if(select_state=='0'){
										Ext.MessageBox.alert('Warning','You must select a state'); return;
									}
									if(select_county=='0'){
										Ext.MessageBox.alert('Warning','You must select a county'); return;
									}
									if(formdefault.getForm().isValid()){
										formdefault.getForm().submit({
											url     :'mysetting_tabs/myaccount_tabs/savedefaultscounty.php',
											waitMsg :'Saving...',
											success :function(response, o){
														Ext.MessageBox.alert('Warning','Updated Successfully');
														search_state=select_state; 
														search_county=select_county;
													},
											failure :function(response, o){
														Ext.MessageBox.alert('Warning','Updated Fail');
													}
										});
									} 
								}
				}]

	});
	var main = new Ext.Panel({  
        renderTo: 'formdefault1', //el elemento donde será insertado  
        items:[
				formdefault
		]
    });  
	condados.addListener('load', marcarcounty);
	//formdefault.render('formdefault1');
</script>