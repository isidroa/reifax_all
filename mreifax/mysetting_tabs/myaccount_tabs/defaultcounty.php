<?php 
header("Cache-Control: no-store, no-cache, must-revalidate");
include("../../properties_conexion.php");
conectar();

//llenar combo de estado con los estados que tiene el usuario
if($_COOKIE['datos_usr']['idusertype']!=1 && $_COOKIE['datos_usr']['idusertype']!=4 && $_COOKIE['datos_usr']['idusertype']!=7){ 
	$query = "Select us.idstate, ls.State
	From xima.userstate us 
	INNER JOIN xima.lsstate ls ON (us.idstate=ls.idstate)
	WHERE us.userid=".$_COOKIE['datos_usr']["USERID"];
}else{
	$query = "Select ls.idstate, ls.State
	From xima.lsstate ls 
	ORDER BY ls.State";	 
}
$result = mysql_query($query) or die($query." ".mysql_error());

$states="['0','Select'],";
while($row=mysql_fetch_array($result, MYSQL_ASSOC)){
	$states.="['".$row['idstate']."','".$row['State']."'],";
}

//determinar los defcounty y defstate
$query = "Select uc.idcounty,lc.idstate 
From xima.usercounty uc 
INNER JOIN xima.lscounty lc ON (uc.idcounty=lc.idcounty)
WHERE uc.defcounty=1 AND uc.userid=".$_COOKIE['datos_usr']["USERID"];
$result = mysql_query($query) or die($query." ".mysql_error());
$r=mysql_fetch_array($result);

$defstate  = $r['idstate'];
$defcounty = $r['idcounty'];

?>
	<div align="center" style=" background-color:#EAEAEA; height:auto;margin-top:-5px;padding-top:10px;">
    	<table width="96%" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
        	<tr align="left" >
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:14px">&nbsp;</td>                
           	</tr>
        	<tr >
           		<td colspan="2" style="color:#274F7B; font-weight:bold; font-size:16px; text-align:center">Default Values</td>                
           	</tr>
      	</table>
   	</div>
    <div id="formdefault1" align="center" style=" background-color:#EAEAEA; height:auto;margin-top:-5px;padding-top:10px;"></div>
<script>
	countydefault=<?php echo strlen($defcounty)>0 ? $defcounty : 1;?>;
	statedefault=<?php echo strlen($defstate)>0 ? $defstate : 1;?>;
	
	function loadCountys(){
		var select_state=Ext.getCmp('cestado').getValue();
		countydefault='0';
		condados.load({params:{selectstate:select_state}}); 
	}
	function marcarcounty(){
		Ext.getCmp('ccondado').setValue(countydefault);
	}
	var condados=new Ext.data.JsonStore({  
        url:'mysetting_tabs/myaccount_tabs/getcountys.php',  
        root:'results',  
        fields: ['id','name']  
    });
	
	condados.load({params:{selectstate:search_state}});  
	var estados=new Ext.data.ArrayStore({
										fields: ['id','name'],
										data  : [<?php echo rtrim($states,','); ?>]
									});
	 var formdefault = new Ext.FormPanel({
				frame:true,
				id:'formdefault',
				method:'POST',
				bodyStyle:'padding:5px 5px 0;text-align:left;',
				items:[{
						xtype:'fieldset',
						title:'Default location',
						items:[{
							xtype: 'combo',
							name: 'cestado',
							id: 'cestado',
							hiddenName	:'estado',
							fieldLabel: '<b>Select State</b>',
							typeAhead: true,
							store: estados,
							triggerAction: 'all',
							mode: 'local',
							editable: false,
							emptyText	:'Select ...',
							selectOnFocus:true,
							allowBlank: false,
							displayField:'name',
							valueField: 'id',
							value:statedefault,
							width: 150,
							listeners: {
								 'select': loadCountys
							}
						},{
							xtype: 'combo',
							name: 'ccondado',
							id: 'ccondado',
							hiddenName	:'condado',
							fieldLabel: '<b>Select County</b>',
							typeAhead: true,
							store: condados,
							triggerAction: 'all',
							mode: 'local',
							editable: false,
							emptyText	:'Select ...',
							selectOnFocus:true,
							allowBlank: false,
							displayField:'name',
							valueField: 'id',
							width: 150,
							value: countydefault
					}]},{
						xtype:'fieldset',
						title:'Advanced search default template',
						labelWidth:110,
						items:[
							new Ext.form.ComboBox({
								id: 'cdefaultTemplate',
								fieldLabel: '<b>Select Template</b>',
								triggerAction: 'all',
								hiddenName:'Template',
								mode: 'remote',
								store: new Ext.data.JsonStore({
									url: 'properties_manage_template.php',
									forceSelection: true,
									fields: [
										'tID',
										'tname'
									]
								}),
								width: 200,
								value: (advanceResultTemplateName!='')?advanceResultTemplateName:null,
								valueNotFoundText:'Default',
								valueField: 'tID',
								displayField: 'tname',
								listeners:{
									'select': function (combo,record,val){
									}
								}
							})
						]
					}
				],
				buttonAlign:'left',		
				buttons     :[{
					text        :'<span style=\'color: #4B8A08; font-size: 15px;\'><b>Save Defaults</b></span>',
					handler     :function(){
						
									var select_state=Ext.getCmp('cestado').getValue();
									var select_county=Ext.getCmp('ccondado').getValue();
									
									var comboTemp=Ext.getCmp('cdefaultTemplate');
									var storeTemp=comboTemp.getStore();
									var selectResultTemplate=comboTemp.getValue();
									if(selectResultTemplate!=null){
										var selectResultTemplateName=storeTemp.getAt(storeTemp.find('tID',selectResultTemplate)).data.tname;	}
										
									
									
									
									if(select_state=='0'){ 
									
										Ext.MessageBox.alert('Warning','You must select a state'); return;
									}
									if(select_county=='0'){
										Ext.MessageBox.alert('Warning','You must select a county'); return;
									}
									if(formdefault.getForm().isValid()){
										formdefault.getForm().submit({
											url     :'mysetting_tabs/myaccount_tabs/savedefaultscounty.php',
											waitMsg :'Saving...',
											paramns	:{
												Template : (selectResultTemplate!=null)?selectResultTemplate: 'NULL'
											},
											success :function(response, o){
														Ext.MessageBox.alert('Warning','Updated Successfully');
														search_state=select_state; 
														search_county=select_county;
														advanceResultTemplate=selectResultTemplate;
														advanceResultTemplateName=selectResultTemplateName;
													},
											failure :function(response, o){
														Ext.MessageBox.alert('Warning','Updated Fail');
													}
										});
									} 
								}
				}]

	});
	var main = new Ext.Panel({  
        renderTo: 'formdefault1', //el elemento donde será insertado  
        items:[
				formdefault
		]
    });  
	condados.addListener('load', marcarcounty);
	//formdefault.render('formdefault1');
</script>