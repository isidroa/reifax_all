<?php
conectarPorBD($_POST['bd']);


$total=$total_P=$total_F=$total_sold=$total_sold_P=0;
$total_no_sold_P=$total_sold_F=$total_no_sold_F=0;
$total_P_0=$total_P_10=$total_P_20=$total_P_30=0;
$total_sold_3=$total_owner_Y=$total_owner_N=0;
$total_UD=$total_no_sold_UD=$total_sold_UD=0;
$total_sold_PE=$total_sold_PE_F=$total_sold_PE_P=0;
$cs=$dom=0;
$sp12=$sp9=$sp6=$sp3=0;
$asp12=$asp9=$asp6=$asp3=array();

$latlong=0;

	if(!isset($_POST['no_func']) or !$_POST['no_func']){
		require('includes/class/polygon.geo.class.php');
		function _builtPolygon($arrPoly){
			$polygon =& new geo_polygon();
			$_lat=0;$_lng=0;
			foreach($arrPoly as & $coord)
			{
				$_lng=($coord["long"]);
				$_lat=$coord["lat"];		
				$polygon->addv($_lng,$_lat);
			}
			
			return $polygon;	
		}
		
		function _pointINpolygon($polygon,$lat,$lng) 
		{
			$lng=($lng);
			$vertex = new vertex($lng,$lat);
			return ($polygon->isInside($vertex))? true:false;	
		}
	}

function porcentaje($val){
	return number_format($val, 2, '.', '');
}

$query="SELECT p.astatus, p.saledate, p.saleprice, mv.pendes, mv.ownocc, mv.debttv, mv.marketpctg, mv.sold, r.status, r.dom, ll.latitude, ll.longitude FROM psummary p LEFT JOIN marketvalue mv ON (p.parcelid=mv.parcelid) LEFT JOIN rtmaster r ON (p.parcelid=r.parcelid) LEFT JOIN latlong ll on (p.parcelid=ll.parcelid)";

if($_POST['tipo']=='result'){
	$parcel=$_POST['pid'];
	
	$res=mysql_query("select p.xcode,l.latitude,l.longitude FROM psummary p LEFT JOIN latlong l ON (p.parcelid=l.parcelid) WHERE p.parcelid='$parcel'") or die(mysql_error());
	$r=mysql_fetch_array($res);
	$lat=$r['latitude'];
	$lon=$r['longitude'];
	$proper=$r['xcode'];
	
	$_calculo="truncate(sqrt((69.1*(ll.latitude-$lat))*(69.1*(ll.latitude-$lat))+(69.1*((ll.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((ll.longitude-($lon))*cos($lat/57.29577951)))),2)";
	
	$query.="WHERE p.parcelid<>'$parcel' AND p.xcode='$proper' AND $_calculo<=".$_POST['distance'];
	//if($_COOKIE['datos_usr']['USERID']==73) echo $query.'<br><br>';
}else{
	$proper=$_POST['proper'];
	$latMax=0;
	$latMin=0;
	$lonMax=0;
	$lonMin=0;
	$arrLats=array();
	$arrLons=array();
	$arrPoly=array();
	
	$latlong=explode('/',$_POST['latlong']);
	foreach($latlong as $ll)
	{
		$basura=explode(",",$ll);
		$arrLats[]=$basura[0];
		$arrLons[]=$basura[1];
		$arrPoly[]=array("lat"=>$basura[0],"long"=>$basura[1]);
	}
	$latMax=max( $arrLats );
	$latMin=min( $arrLats );
	$lonMax=max( $arrLons );
	$lonMin=min( $arrLons );

	$query.="WHERE p.xcode='$proper' AND (ll.LATITUDE>$latMin AND ll.LATITUDE<$latMax AND ll.LONGITUDE>$lonMin AND ll.LONGITUDE<$lonMax) ";
}


//echo $query;
$actual=date('Ym').'01';
$desde=date('Ym',strtotime("-3 month")).'01';
$desde6=date('Ym',strtotime("-6 month")).'01';
$desde9=date('Ym',strtotime("-9 month")).'01';
$desde12=date('Ym',strtotime("-12 month")).'01';

$result=mysql_query($query) or die ($query.' '.mysql_error());
$polygon = _builtPolygon($arrPoly);
while($r=mysql_fetch_array($result)){
	$contar=true;
	if(count($latlong)>2){
		if(_pointINpolygon($polygon,$r["latitude"],$r["longitude"]))
			$contar=true;
		else
			$contar=false;
	}
	
	//if($_COOKIE['datos_usr']['USERID']==73){
	//Arreglo de duplicidades en rtmaster.
	if($r['status']!=NULL){
		if($r['astatus']=='A' && $r['astatus']!=$r['status'])
			$contar=false;
		elseif(($r['astatus']=='CC' || $r['astatus']=='CS') && $r['status']=='A')
			$contar=false;
	}
	//}
	
	if($contar){
		$total++;
		if($r['pendes']=='F' && $r['sold']=='N'){
			$total_F++;
			if($r['astatus']=='A')
				$total_sold_F++;
			else
				$total_no_sold_F++;
		}
		if($r['pendes']=='P' && $r['sold']=='N'){
			$total_P++;
			if($r['astatus']=='A')
				$total_sold_P++;
			else
				$total_no_sold_P++;
			
			if($r['debttv']<0)
				$total_P_0++;
			if($r['debttv']>=0 && $r['debttv']<100)
				$total_P_0_mas++;
			if($r['debttv']>=30 && $r['debttv']<100)
				$total_P_30++;
			
		}
		if($r['pendes']=='N' && $r['debttv']<0){
			$total_UD++;
			if($r['astatus']=='A'){
				$total_sold_UD++;
			}else{
				$total_no_sold_UD++;
			}
		}
		if($r['astatus']=='A'){
			$total_sold++;
			if($r['marketpctg']>=30){
				$total_sold_PE++;
				if($r['pendes']=='F')
					$total_sold_PE_F++;
				if($r['pendes']=='P')
					$total_sold_PE_P++;
			}
		}
		
		if($r['astatus']=='CS' || $r['astatus']=='CC'){
			if($r['status']=='CS'){
				$cs++;
				$dom+=$r['dom'];
			}
			
			if($r['saledate']>=$desde && $r['saledate']<=$actual && $r['saleprice']>=19000){
				$sp3++;
				$asp3[]=$r['saleprice'];
			}
			if($r['saledate']>=$desde6 && $r['saledate']<=$actual && $r['saleprice']>=19000){
				$sp6++;
				$asp6[]=$r['saleprice'];
			}
			if($r['saledate']>=$desde9 && $r['saledate']<=$actual && $r['saleprice']>=19000){
				$sp9++;
				$asp9[]=$r['saleprice'];
			}
			if($r['saledate']>=$desde12 && $r['saledate']<=$actual && $r['saleprice']>=19000){
				$sp12++;
				$asp12[]=$r['saleprice'];
			}
		}
		
		if($r['astatus']=='CS' || $r['astatus']=='CC'){
			if($r['saledate']>=$desde6 && $r['saledate']<=$actual && $r['saleprice']>=19000){
				$total_sold_3++;
			}
		}
		
		if($r['ownocc']=='Y' || $r['ownocc']=='y')
			$total_owner_Y++;
		if($r['ownocc']=='N' || $r['ownocc']=='n')
			$total_owner_N++;
	}
}
unset($polygon);
$total_D=$total_F+$total_P;
if($total>0){
	$pct_D=porcentaje(($total_D*100)/$total);
	$pct_F=porcentaje(($total_F*100)/$total);
	$pct_P=porcentaje(($total_P*100)/$total);
	$pct_sold=porcentaje(($total_sold*100)/$total);
	$pct_owner_N=porcentaje(($total_owner_N*100)/$total);
	$pct_owner_Y=porcentaje(($total_owner_Y*100)/$total);
	$pct_UD=porcentaje(($total_UD*100)/$total);
}else{
	$pct_UD=$pct_D=$pct_F=$pct_P=$pct_sold=$pct_owner_Y=$pct_owner_N=0.00;
}

if($total_F>0){
	$pct_sold_F=porcentaje(($total_sold_F*100)/$total_F);
	$pct_no_sold_F=porcentaje(($total_no_sold_F*100)/$total_F);
}else{
	$pct_sold_F=$pct_no_sold_F=0.00;
}

if($total_P>0){
	$pct_sold_P=porcentaje(($total_sold_P*100)/$total_P);
	$pct_no_sold_P=porcentaje(($total_no_sold_P*100)/$total_P);
	$pct_P_0=porcentaje(($total_P_0*100)/$total_P);
	$pct_P_0_mas=porcentaje(($total_P_0_mas*100)/$total_P);
	$pct_P_30=porcentaje(($total_P_30*100)/$total_P);
}else{
	$pct_sold_P=$pct_no_sold_P=$pct_P_0=$pct_P_0_mas=$pct_P_30=0.00;
}

if($total_UD>0){
	$pct_sold_UD=porcentaje(($total_sold_UD*100)/$total_UD);
	$pct_no_sold_UD=porcentaje(($total_no_sold_UD*100)/$total_UD);
}else{
	$pct_sold_UD=$pct_no_sold_UD=0.00;
}

if($total_sold>0){
	$pct_sold_PE=porcentaje(($total_sold_PE*100)/$total_sold);
	$pct_sold_PE_F=porcentaje(($total_sold_PE_F*100)/$total_sold);
	$pct_sold_PE_P=porcentaje(($total_sold_PE_P*100)/$total_sold);
}else{
	$pct_sold_PE=$pct_sold_PE_F=$pct_sold_PE_P=0.00;
}

$a_sold_3=round($total_sold_3/6,2);

if($cs>0)
	$a_days=porcentaje($dom/$cs);
else
	$a_days=0.00;
	

sort($asp3,SORT_NUMERIC);
sort($asp6,SORT_NUMERIC);
sort($asp9,SORT_NUMERIC);
sort($asp12,SORT_NUMERIC);

if($sp3>0){
	$a_asp3=porcentaje(array_sum($asp3)/$sp3);
	if($sp3%2==0){ 
		$m=$sp3/2; 
		$m_asp3=porcentaje($asp3[$m]);
	}else{
		$m=ceil($sp3/2);
		$m_asp3=$asp3[$m];
		$m=floor($sp3/2);
		$m_asp3+=$asp3[$m];
		$m_asp3=porcentaje($m_asp3/2);
	}
	
}else
	$a_asp3=$m_asp3=0.00;
	
if($sp6>0){
	$a_asp6=porcentaje(array_sum($asp6)/$sp6);
	if($sp6%2==0){ 
		$m=$sp6/2; 
		$m_asp6=porcentaje($asp6[$m]);
	}else{
		$m=ceil($sp6/2);
		$m_asp6=$asp6[$m];
		$m=floor($sp6/2);
		$m_asp6+=$asp6[$m];
		$m_asp6=porcentaje($m_asp6/2);
	}
	
}else
	$a_asp6=$m_asp6=0.00;
	
if($sp9>0){
	$a_asp9=porcentaje(array_sum($asp9)/$sp9);
	if($sp9%2==0){ 
		$m=$sp9/2; 
		$m_asp9=porcentaje($asp9[$m]);
	}else{
		$m=ceil($sp9/2);
		$m_asp9=$asp9[$m];
		$m=floor($sp9/2);
		$m_asp9+=$asp9[$m];
		$m_asp9=porcentaje($m_asp9/2);
	}
	
}else
	$a_asp9=$m_asp9=0.00;
	
if($sp12>0){
	$a_asp12=porcentaje(array_sum($asp12)/$sp12);
	if($sp12%2==0){ 
		$m=$sp12/2; 
		$m_asp12=porcentaje($asp12[$m]);
	}else{
		$m=ceil($sp12/2);
		$m_asp12=$asp12[$m];
		$m=floor($sp12/2);
		$m_asp12+=$asp12[$m];
		$m_asp12=porcentaje($m_asp12/2);
	}
	
}else
	$a_asp12=$m_asp12=0.00;

if($total_sold_3>0)
	$invM=porcentaje($total_sold/$a_sold_3);
else
	$invM=0.00;
	

////////////////////////////////COUNTY//////////////////////////////////////////////
//echo
$query2="SELECT * FROM ".$_POST['bd'].".exray WHERE zip='' and proptype='$proper' ";//order by fecha desc limit 1
$result2=mysql_query($query2) or die($query2.mysql_error());
$xrayRes=array();
$xrayRes=mysql_fetch_array($result2);
////////////////////////////////////////////////////////////////////////////////////

//echo "<pre>";print_r($xrayRes);echo "</pre>";

$html="<div style='font-family:Trebuchet MS,Arial,Helvetica,sans-serif;'><table align='center' cellpadding=0 cellspacing=0 style='font-family:Trebuchet MS,Arial,Helvetica,sans-serif; font-size:120%;'>";

if(!isset($_POST['PAR_XRAY']))
	$html.="<tr class=mortcelltitulo><td colspan=5 align=right><a href='FPDF/Tag/printTag.php?type=xray'; target=_blank><img src=img/printer.png></a></td></tr>";
	
$html.="<tr class=mortcelltitulo style='font-weight:bold'><td colspan=1 align=center>&nbsp;</td><td colspan=2 align=center>Subject Area</td><td colspan=2 align=center>County Area</td></tr><tr class=mortcelltitulo style='font-weight:bold'><td align=center></td><td align=center style='width:70'><strong>Quantity</strong></td><td align=center style='width:60'><strong>Percent</strong></td><td align=center style='width:90'><strong>Quantity</strong></td><td align=center style='width:60'><strong>Percent</strong></td></tr><tr><td >Total Properties</td><td align=right>$total</td><td align=right>100.00</td><td align=right>".intval($xrayRes[total])."</td><td align=right>100.00</td></tr><tr><td >Distress Properties</td><td align=right>$total_D</td><td align=right>$pct_D</td><td align=right>".intval($xrayRes[distress])."</td><td align=right>$xrayRes[pctdistress]</td></tr><tr><td >Pre-Foreclosed</td><td align=right>$total_P</td><td align=right>$pct_P</td><td align=right>".intval($xrayRes[preforeclosure])."</td><td align=right>$xrayRes[pctpreforeclosure]</td></tr><tr><td >Foreclosed</td><td align=right>$total_F</td><td align=right>$pct_F</td><td align=right>".intval($xrayRes[foreclosure])."</td><td align=right>$xrayRes[pctforeclosure]</td></tr><tr><td >Upside Down not in Foreclosure</td><td align=right>$total_UD</td><td align=right>$pct_UD</td><td align=right>".intval($xrayRes[upsidedown])."</td><td align=right>$xrayRes[pctupsidedown]</td></tr><tr><td >Active For Sale</td><td align=right>$total_sold</td><td align=right>$pct_sold</td><td align=right>".intval($xrayRes[sale])."</td><td align=right>$xrayRes[pctsale]</td></tr><tr><td >Owner Occupied</td><td align=right>$total_owner_Y</td><td align=right>$pct_owner_Y</td><td align=right>".intval($xrayRes[ownerY])."</td><td align=right>$xrayRes[pctownerY]</td></tr><tr><td >Not Owner Occupied</td><td align=right>$total_owner_N</td><td align=right>$pct_owner_N</td><td align=right>".intval($xrayRes[ownerN])."</td><td align=right>$xrayRes[pctownerN]</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr><tr><td >Total Pre-Foreclosed</td><td align=right>$total_P</td><td align=right>100.00</td><td align=right>".intval($xrayRes[preforeclosure])."</td><td align=right>100.00</td></tr><tr><td >Pre-Forecloses Active For Sale</td><td align=right>$total_sold_P</td><td align=right>$pct_sold_P</td><td align=right>".intval($xrayRes[total_sold_P])."</td><td align=right>$xrayRes[pcttotal_sold_P]</td></tr><tr><td >Pre-Forecloses Not For Sale</td><td align=right>$total_no_sold_P</td><td align=right>$pct_no_sold_P</td><td align=right>".intval($xrayRes[total_no_sold_P])."</td><td align=right>$xrayRes[pcttotal_no_sold_P]</td></tr><tr><td >Pre-Forecloses with less than 0% equity</td><td align=right>$total_P_0</td><td align=right>$pct_P_0</td><td align=right>".intval($xrayRes[total_P_0])."</td><td align=right>$xrayRes[pcttotal_P_0]</td></tr><tr><td >Pre-Forecloses with more than 0% equity</td><td align=right>$total_P_0_mas</td><td align=right>$pct_P_0_mas</td><td align=right>".intval($xrayRes[total_P_0_mas])."</td><td align=right>$xrayRes[pcttotal_P_0_mas]</td></tr><tr><td >Pre-Forecloses with more than 30% equity</td><td align=right>$total_P_30</td><td align=right>$pct_P_30</td><td align=right>".intval($xrayRes[total_P_30])."</td><td align=right>$xrayRes[pcttotal_P_30]</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr><tr><td >Total Foreclosed</td><td align=right>$total_F</td><td align=right>100.00</td><td align=right>".intval($xrayRes[foreclosure])."</td><td align=right>100.00</td></tr><tr><td >Forecloses Active For Sale</td><td align=right>$total_sold_F</td><td align=right>$pct_sold_F</td><td align=right>".intval($xrayRes[total_sold_F])."</td><td align=right>$xrayRes[pcttotal_sold_F]</td></tr><tr><td >Forecloses Not For Sale</td><td align=right>$total_no_sold_F</td><td align=right>$pct_no_sold_F</td><td align=right>".intval($xrayRes[total_no_sold_F])."</td><td align=right>$xrayRes[pcttotal_no_sold_F]</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr><tr><td >Total Upside Down not in Foreclosure</td><td align=right>$total_UD</td><td align=right>100.00</td><td align=right>".intval($xrayRes[upsidedown])."</td><td align=right>100.00</td></tr><tr><td >Upside Down not in Foreclosure Active For Sale</td><td align=right>$total_sold_UD</td><td align=right>$pct_sold_UD</td><td align=right>".intval($xrayRes[total_sold_UD])."</td><td align=right>$xrayRes[pcttotal_sold_UD]</td></tr><tr><td >Upside Down not in Foreclosure Not For sale</td><td align=right>$total_no_sold_UD</td><td align=right>$pct_no_sold_UD</td><td align=right>".intval($xrayRes[total_no_sold_UD])."</td><td align=right>$xrayRes[pcttotal_no_sold_UD]</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr><tr><td >Total Active For Sale</td><td align=right>$total_sold</td><td align=right>100.00</td><td align=right>".intval($xrayRes[sale])."</td><td align=right>100.00</td></tr><tr><td >Properties For Sale with 30% equity or more</td><td align=right>$total_sold_PE</td><td align=right>$pct_sold_PE</td><td align=right>".intval($xrayRes[total_sold_PE])."</td><td align=right>$xrayRes[pcttotal_sold_PE]</td></tr><tr><td >Pre-Forecloses For Sale with 30% equity or more</td><td align=right>$total_sold_PE_P</td><td align=right>$pct_sold_PE_P</td><td align=right>".intval($xrayRes[total_sold_PE_P])."</td><td align=right>$xrayRes[pcttotal_sold_PE_P]</td></tr><tr><td >Forecloses For Sale with 30% equity or more</td><td align=right>$total_sold_PE_F</td><td align=right>$pct_sold_PE_F</td><td align=right>".intval($xrayRes[total_sold_PE_F])."</td><td align=right>$xrayRes[pcttotal_sold_PE_F]</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr><tr><td >Total Sold in the Last 6 months</td><td align=right>$total_sold_3</td><td align=right>--</td><td align=right>".intval($xrayRes[total_sold_3])."</td><td align=right>--</td></tr><tr><td >Average Sold in the last 6 moths</td><td align=right>$a_sold_3</td><td align=right>--</td><td align=right>".$xrayRes[a_sold_3]."</td><td align=right>--</td></tr><tr><td >Total Active For Sale</td><td align=right>$total_sold</td><td align=right>--</td><td align=right>".intval($xrayRes[sale])."</td><td align=right>--</td></tr><tr><td >Months of inventory</td><td align=right>$invM</td><td align=right>--</td><td align=right>".$xrayRes[invM]."</td><td align=right>--</td></tr><tr><td >Average Days on Market</td><td align=right>$a_days</td><td align=right>--</td><td align=right>".$xrayRes[a_days]."</td><td align=right>--</td></tr><tr><td >Average Sold Price 12 months</td><td align=right>$a_asp12</td><td align=right>--</td><td align=right>".$xrayRes[a_asp12]."</td><td align=right>--</td></tr><tr><td >Median Sold Price 12 months</td><td align=right>$m_asp12</td><td align=right>--</td><td align=right>".$xrayRes[m_asp12]."</td><td align=right>--</td></tr><tr><td >Average Sold Price 9 months</td><td align=right>$a_asp9</td><td align=right>--</td><td align=right>".$xrayRes[a_asp9]."</td><td align=right>--</td></tr><tr><td>Median Sold Price 9 months</td><td align=right>$m_asp9</td><td align=right>--</td><td align=right>".$xrayRes[m_asp9]."</td><td align=right>--</td></tr><tr><td>Average Sold Price 6 months</td><td align=right>$a_asp6</td><td align=right>--</td><td align=right>".$xrayRes[a_asp6]."</td><td align=right>--</td></tr><tr><td>Median Sold Price 6 months</td><td align=right>$m_asp6</td><td align=right>--</td><td align=right>".$xrayRes[m_asp6]."</td><td align=right>--</td></tr><tr><td >Average Sold Price 3 months</td><td align=right>$a_asp3</td><td align=right>--</td><td align=right>".$xrayRes[a_asp3]."</td><td align=right>--</td></tr><tr><td >Median Sold Price 3 months</td><td align=right>$m_asp3</td><td align=right>--</td><td align=right>".$xrayRes[m_asp3]."</td><td align=right>--</td></tr><tr class=mortcelltitulo style='height:2px'><td colspan=5 align=right>&nbsp;</td></tr></table></div>";

$_SESSION['xray_print']['total']=$total; 
$_SESSION['xray_print']['distress']=$total_D;
$_SESSION['xray_print']['pct']['distress']=$pct_D;
$_SESSION['xray_print']['preforeclosure']=$total_P;
$_SESSION['xray_print']['pct']['preforeclosure']=$pct_P;
$_SESSION['xray_print']['foreclosure']=$total_F;
$_SESSION['xray_print']['pct']['foreclosure']=$pct_F;
$_SESSION['xray_print']['upsidedown']=$total_UD;
$_SESSION['xray_print']['pct']['upsidedown']=$pct_UD;
$_SESSION['xray_print']['sale']=$total_sold;
$_SESSION['xray_print']['pct']['sale']=$pct_sold;
$_SESSION['xray_print']['ownerY']=$total_owner_Y;
$_SESSION['xray_print']['pct']['ownerY']=$pct_owner_Y;
$_SESSION['xray_print']['ownerN']=$total_owner_N;
$_SESSION['xray_print']['pct']['ownerN']=$pct_owner_N;

$_SESSION['xray_print']['total_sold_P']=$total_sold_P;
$_SESSION['xray_print']['pct']['total_sold_P']=$pct_sold_P;
$_SESSION['xray_print']['total_no_sold_P']=$total_no_sold_P;
$_SESSION['xray_print']['pct']['total_no_sold_P']=$pct_no_sold_P;
$_SESSION['xray_print']['total_P_0']=$total_P_0;
$_SESSION['xray_print']['pct']['total_P_0']=$pct_P_0;
$_SESSION['xray_print']['total_P_0_mas']=$total_P_0_mas;
$_SESSION['xray_print']['pct']['total_P_0_mas']=$pct_P_0_mas;
$_SESSION['xray_print']['total_P_30']=$total_P_30;
$_SESSION['xray_print']['pct']['total_P_30']=$pct_P_30;

$_SESSION['xray_print']['total_sold_F']=$total_sold_F;
$_SESSION['xray_print']['pct']['total_sold_F']=$pct_sold_F;
$_SESSION['xray_print']['total_no_sold_F']=$total_no_sold_F;
$_SESSION['xray_print']['pct']['total_no_sold_F']=$pct_no_sold_F;

$_SESSION['xray_print']['total_sold_UD']=$total_sold_UD;
$_SESSION['xray_print']['pct']['total_sold_UD']=$pct_sold_UD;
$_SESSION['xray_print']['total_no_sold_UD']=$total_no_sold_UD;
$_SESSION['xray_print']['pct']['total_no_sold_UD']=$pct_no_sold_UD;

$_SESSION['xray_print']['total_sold_PE']=$total_sold_PE;
$_SESSION['xray_print']['pct']['total_sold_PE']=$pct_sold_PE;
$_SESSION['xray_print']['total_sold_PE_P']=$total_sold_PE_P;
$_SESSION['xray_print']['pct']['total_sold_PE_P']=$pct_sold_PE_P;
$_SESSION['xray_print']['total_sold_PE_F']=$total_sold_PE_F;
$_SESSION['xray_print']['pct']['total_sold_PE_F']=$pct_sold_PE_F;

$_SESSION['xray_print']['invM']=$invM;
$_SESSION['xray_print']['a_days']=$a_days;
$_SESSION['xray_print']['total_sold_3']=$total_sold_3;
$_SESSION['xray_print']['a_sold_3']=$a_sold_3;
$_SESSION['xray_print']['a_asp12']=$a_asp12;
$_SESSION['xray_print']['m_asp12']=$m_asp12;
$_SESSION['xray_print']['a_asp9']=$a_asp9;
$_SESSION['xray_print']['m_asp9']=$m_asp9;
$_SESSION['xray_print']['a_asp6']=$a_asp6;
$_SESSION['xray_print']['m_asp6']=$m_asp6;
$_SESSION['xray_print']['a_asp3']=$a_asp3;
$_SESSION['xray_print']['m_asp3']=$m_asp3;

if(isset($_POST['PAR_XRAY'])){
	echo $html;
}else{
	echo "{succes:true, html: \"$html\", query: \"$query\"}";
}
?>