<?php 
session_start();
//include('includes/getLocation.php');
$_SERVERXIMA="http://www.reifax.com/";
$realtor=$_POST['userweb']=="true" ? true:false;	
$realtorid=isset($_POST['realtorid']) ? $_POST['realtorid']:'';
$webmaster="false";	

$platinum=2;
if(isset($_COOKIE['datos_usr']['USERID'])){
	include('properties_conexion.php');
	conectar();
	
	//User Permission
	$query='select platinum,professional,professional_esp from xima.permission WHERE userid='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$platinum = ($r['platinum']==1 || $r['professional']==1 || $r['professional_esp']==1) ? 1 : 0;
	
	//User Default State
	$query='select idstate from xima.userstate WHERE defstate=1 AND userid='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($result)>0){
		$r=mysql_fetch_array($result);
		$defstate = $r['idstate'];
	}
	
	//User Default County
	$query='select idcounty from xima.usercounty WHERE defcounty=1 AND userid='.$_COOKIE['datos_usr']['USERID'];
	$result=mysql_query($query) or die($query.mysql_error());
	if(mysql_num_rows($result)>0){
		$r=mysql_fetch_array($result);
		$defcounty = $r['idcounty'];
	}
}

?>
<div align="left" style="height:100%">
	<div id="body_central" style="height:100%">
		<div id="tabsSearchs" style="padding-top:2px;"></div>
	</div>
</div>
<script>
 
var tabsSearchs=null;var formulsearchadv;
var ancho=640;
<?php //if($_COOKIE['datos_usr']['idstatus']!=8 && $_COOKIE['datos_usr']['idstatus']!=9){?>
if(user_loged) ancho=system_width;
<?php //}?>

var icon_result=icon_mylabel=false;
var ResultTemplate=-1;
if(realtor_block!=false){
	icon_result=true;
	icon_mylabel=true;
}
if(user_web!=false)	icon_mylabel=true;
search_state='<?php echo isset($defstate) ? $defstate : $_POST['county_search'];?>';
search_county='<?php echo isset($defcounty) ? $defcounty : $_POST['state_search'];?>';

tabsSearchs = new Ext.TabPanel({
	renderTo: 'tabsSearchs',
	activeTab: 0,
	width: ancho,
	height: tabs.getHeight(),
	plain:true,
	enableTabScroll:true,
	defaults:{	autoScroll: false},
	items:[
		{
			title: ' Basic Search ',
			id: 'searchTabBasic',
			name: 'searchTabBasic',
			autoLoad: {url: 'searchs_types/basic_search.php', scripts:true, params: {userweb:'<?php echo $realtor;?>', search_type: '<?php echo $_POST['search_type'];?>', search_by_type: '<?php echo $_POST['search_by_type'];?>', state_search: search_state, county_search: search_county,masterweb:'<?php echo $webmaster;?>'}},
			tbar: new Ext.Toolbar({
						cls: 'no-border-search',
						width: 'auto',
						items: [' ',{
							 tooltip: 'Search by Map',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/map.png',
							 handler: function (){
							 
								 if(document.getElementById("mapSearch").style.display=='none'){
									 doSearchByFilter('MAP');
									 document.getElementById(search_type+'_combo_search_by').value = 'MAP';
								 }else{
									 doSearchByFilter('MAP_OFF');
									 document.getElementById(search_type+'_combo_search_by').value = 'LOCATION';
								 }
							 }
							},{
							 tooltip: 'Xray Report',
							 cls:'x-btn-text-icon',
							iconAlign: 'left',
							text: ' ',
							width: 30,
							height: 30,
							scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/exray.png',
							 handler: function(){
								 var bd = document.getElementById(search_type+'_county_search').value;
								 var latlong = document.getElementById('mapa_search_latlong').value;
								 var xcode = document.getElementById(search_type+'_proptype').value;
								 if(Ext.isEmpty(xcode)){ 
									Ext.Msg.alert("XRay Report", 'Property Type is requiered to execute the XRay Report.');
									return false;
								 }
								 if(latlong=='-1'){
									 Ext.Msg.alert("XRay Report", 'Map shape/polygon is requiered to execute the XRay Report.');
									 return false;
								 }
															 
								 if(document.getElementById('reportsTab')){
									 var tab = tabs.getItem('reportsTab');
									 tabs.remove(tab);
								 }
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
								tabs.add({
									title: ' Reports ',
									id: 'reportsTab',
									autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
									closable: true,
									tbar: new Ext.Toolbar({
										cls: 'no-border',
										width: 'auto',
										items: [' ',{
											 tooltip: 'Print XRay Report',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/toolbar/printer.png',
											 handler: function(){
												Ext.Ajax.request( 
												{  
													waitMsg: 'Printing...',
													url: 'imprimir/properties_xray_print.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														db: bd,
														proper: xcode,
														type: 'P',
														latlong: latlong
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														
														var results=response.responseText;
														if(Ext.isIE)
															window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
														 else
															window.open(results,'_newtab');
													}                                
												});
											 }
										},{
											 tooltip: 'Save XRay Report',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/toolbar/save.png',
											 handler: function(){
												 var simple = new Ext.FormPanel({
													url: 'imprimir/properties_xray_print.php',
													frame:true,
													title: 'Saved Documents.',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													
													items: [{
																xtype     : 'textfield',
																name      : 'name_save',
																fieldLabel: 'Name',
																value     : '',
																width: 200
															},{
																xtype     : 'hidden',
																name      : 'db',
																value     : bd
															},{
																xtype     : 'hidden',

																name      : 'proper',
																value     : xcode
															},{
																xtype     : 'hidden',
																name      : 'type',
																value     : 'S'
															},{
																xtype     : 'hidden',
																name      : 'latlong',
																value     : latlong
															}],
													
													buttons: [{
															text: 'Save',
															handler: function(){
																loading_win.show();
																simple.getForm().submit({
																	success: function(form, action) {
																		loading_win.hide();
																		win.close();
																		Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																	},
																	failure: function(form, action) {
																		loading_win.hide();
																		Ext.Msg.alert("Failure", action.result.msg);
																	}
																});
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	simple.getForm().reset();
																	win.close();
																}
														}]
													});
												 
												var win = new Ext.Window({
													layout      : 'fit',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: simple,
													closeAction : 'hide',
													buttons: [{
														text     : 'Close',
														handler  : function(){
															win.close();
														}
													}]
												});
												win.show();
											 }
										},'->',{
											 tooltip: 'Close XRay Report.',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/cancel.png',
											 handler: function(){
												 var tab = tabs.getItem('reportsTab');
												 tabs.remove(tab);
											 }
										}]
									})
								}).show();
								 
							 }
							},{
							 tooltip: 'Discount Report',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
							 handler: function(){
								 var bd = document.getElementById(search_type+'_county_search').value;
								 var latlong = document.getElementById('mapa_search_latlong').value;
								 var xcode = document.getElementById(search_type+'_proptype').value;
								 if(Ext.isEmpty(xcode)){ 
									Ext.Msg.alert("Discount Report", 'Property Type is requiered to execute the Discount Report.');
									return false;
								 }
								 if(latlong=='-1'){
									 Ext.Msg.alert("Discount Report", 'Map shape/polygon is requiered to execute the Discount Report.');
									 return false;
								 }
															 
								 if(document.getElementById('reportsTab')){
									 var tab = tabs.getItem('reportsTab');
									 tabs.remove(tab);
								 }
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
								tabs.add({
									title: ' Reports ',
									id: 'reportsTab',
									autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
									closable: true,
									tbar: new Ext.Toolbar({
										cls: 'no-border',
										width: 'auto',
										items: [' ',{
											 tooltip: 'Print Discount Report',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/toolbar/printer.png',
											 handler: function(){
												Ext.Ajax.request( 
												{  
													waitMsg: 'Printing...',
													url: 'imprimir/properties_rebate_print.php', 
													method: 'POST',
													timeout :600000,
													params: { 
														db: bd,
														proper: xcode,
														type: 'P',
														latlong: latlong
													},
													
													failure:function(response,options){
														loading_win.hide();
														Ext.MessageBox.alert('Warning','ERROR');
													},
													success:function(response,options){
														
														var results=response.responseText;
														if(Ext.isIE)
															window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
														 else
															window.open(results,'_newtab');
													}                                
												});
											 }
										},{
											 tooltip: 'Save Discount Report',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/toolbar/save.png',
											 handler: function(){
												 var simple = new Ext.FormPanel({
													url: 'imprimir/properties_rebate_print.php',
													frame:true,
													title: 'Saved Documents.',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													
													items: [{
																xtype     : 'textfield',
																name      : 'name_save',
																fieldLabel: 'Name',
																value     : '',
																width: 200
															},{
																xtype     : 'hidden',
																name      : 'db',
																value     : bd
															},{
																xtype     : 'hidden',

																name      : 'proper',
																value     : xcode
															},{
																xtype     : 'hidden',
																name      : 'type',
																value     : 'S'
															},{
																xtype     : 'hidden',
																name      : 'latlong',
																value     : latlong
															}],
													
													buttons: [{
															text: 'Save',
															handler: function(){
																loading_win.show();
																simple.getForm().submit({
																	success: function(form, action) {
																		loading_win.hide();
																		win.close();
																		Ext.Msg.alert("Saved Documents", 'Your document has been save.');
																	},
																	failure: function(form, action) {
																		loading_win.hide();
																		Ext.Msg.alert("Failure", action.result.msg);
																	}
																});
															}
														},{
															text: 'Cancel',
															handler  : function(){
																	simple.getForm().reset();
																	win.close();
																}
														}]
													});
												 
												var win = new Ext.Window({
													layout      : 'fit',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: simple,
													closeAction : 'hide',
													buttons: [{
														text     : 'Close',
														handler  : function(){
															win.close();
														}
													}]
												});
												win.show();
											 }
										},'->',{
											 tooltip: 'Click to Close Reports',
											 cls:'x-btn-text-icon',
											 iconAlign: 'left',
											 text: ' ',
											 width: 30,
											 height: 30,
											 scale: 'medium',
											 icon: 'http://www.reifax.com/img/cancel.png',
											 handler: function(){
												 var tab = tabs.getItem('reportsTab');
												 tabs.remove(tab);
											 }
										}]
									})
								}).show();
								 
							 }
							},{
							 tooltip: 'Manage Search',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
							 handler: function(){
								 //alert(user_loged+"||"+user_block+"||"+user_web);
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 ShowSavedSearch();
							 }
							},{
							 tooltip: 'Find Location',
							 cls:'x-btn-text-icon',
							 iconAlign: 'left',
							 text: ' ',
							 width: 30,
							 height: 30,
							 scale: 'medium',
							 hidden: <?php echo $platinum!=1 ? 'true' : 'false';?>,
							 icon: 'http://www.reifax.com/img/toolbar/location.png',
							 handler: function(){
								 //alert(user_loged+"||"+user_block+"||"+user_web);
								 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 if(search_by_type!='GPS'){
									 var formlocation = new Ext.FormPanel({
													frame:true,
													title: 'Get Location',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													items: [{
																xtype		:'combo',
																name		:'distance',
																id			:'distance',
																hiddenName	:'idtype',
																store		:new Ext.data.SimpleStore({
																				fields	:['iddisc'],
																				data 	: [['0.1'],['0.2'],['0.3'],['0.4'],['0.5'],['0.6'],['0.7'],['0.8'],['0.9'],['1'],['1.1'],['1.2'],['1.3'],['1.4'],['1.5'],['1.6'],['1.7'],['1.8'],['1.9'],['2']]
																			}),
																editable	: false,
																displayField:'iddisc',
																valueField	:'iddisc',
																typeAhead	:true,
																fieldLabel	:'Distance Miles',
																mode		:'local',
																triggerAction: 'all',
																emptyText	:'Select ...',
																selectOnFocus:true,
																allowBlank	:false,
																value		:'0.5',
																width		:100
															},{
																xtype: 'hidden',
																id: 'tipomap',
																value:'Basic'
															}],
													buttons: [{
															text: 'Get',
															handler: getLocationGps
														},{
															text: 'Cancel',
															handler  : function(){
																	win.close();
																}
														}]
													});
									 var win = new Ext.Window({
													layout      : 'fit',
													id: 'ventanalocation',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: formlocation,
													closeAction : 'hide'
												});
												win.show();
								 }else{
								 	  doSearchByFilter('LOCATION');	
								 }
							 }
							},{
								tooltip: 'View Help',
								cls:'x-btn-text-icon',
								iconAlign: 'left',
								text: ' ',
								width: 30,
								height: 30,
								scale: 'medium',
								icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
								handler: function(){
									//alert(user_loged+"||"+user_block+"||"+user_web);
									if(!user_loged || user_block || user_web){ login_win.show(); return false;}
									showVideoHelp('BasicSearch');
								}
							},'->'
							,{
								iconCls:'icon',
								icon: 'http://www.reifax.com/img/toolbar/reset.png',
								scale: 'medium',
								text: 'Reset&nbsp;&nbsp; ',
								handler  : function(){
									document.getElementById('pformul').reset();
									if(mapSearch!=null) mapSearch.cleaner();				
								}
							},
							' '
							,{
								iconCls:'icon',
								icon: 'http://www.reifax.com/img/toolbar/search.png',
								scale: 'medium',
								text: 'Search&nbsp;&nbsp; ',
								handler  : function(){
									searchForm(search_type+'_search','xxx');
								}				
							}]
					})
			
		}
		<?php if(isset($_COOKIE['datos_usr']['USERID']) && $_COOKIE['datos_usr']['idstatus']!=8 && $_COOKIE['datos_usr']['idstatus']!=9){?> 
		,{
			title: ' Advanced Search ',
			id: 'searchTabAdv',
			name: 'searchTabAdv',
			autoLoad: {url: 'searchs_types/advance_search.php', scripts:true, params: {userweb:'<?php echo $realtor;?>', search_type: '<?php echo $_POST['search_type'];?>', search_by_type: '<?php echo $_POST['search_by_type'];?>', state_search: search_state, county_search: search_county}}, 
			tbar: new Ext.Toolbar({
				cls: 'no-border-search',
				width: 'auto',
				items: [' ',{
					 tooltip: 'Search By Map',
					 cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					 icon: 'http://www.reifax.com/img/toolbar/map.png',
					 handler: function (){
						
						if(document.getElementById("mapSearchAdv").style.display=='none'){
							mapSearchAdv.control_map();
							mapSearchAdv.centerMapCounty(document.getElementById('occounty').value,true);
							search_by_typeAdv='MAP';
						 }else{
							mapSearchAdv.control_map();
							search_by_typeAdv='MAP_OFF';
						 }
					 }
				},{
					tooltip: 'XRay Report',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/exray.png',
					handler: function(){
						var bd = document.getElementById('occounty').value;
						var latlong = document.getElementById('mapa_search_latlongAdv').value;
						var xcode = document.getElementById('ocproptype').value;
						if(Ext.isEmpty(xcode) || xcode=='*'){ 
							Ext.Msg.alert("XRay Report", 'Property Type is requiered to execute the XRay Report.');
							return false;
						}
						if(latlong=='-1'){
							Ext.Msg.alert("XRay Report", 'Map shape/polygon is requiered to execute the XRay Report.');
							return false;
						}
															 
						if(document.getElementById('reportsTab')){
							var tab = tabs.getItem('reportsTab');
							tabs.remove(tab);
						}
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
								 
						tabs.add({
							title: ' Reports ',
							id: 'reportsTab',
							autoLoad: {url: 'reports_types/properties_xray.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
							closable: true,
							tbar: new Ext.Toolbar({
								cls: 'no-border',
								width: 'auto',
								items: [' ',{
									tooltip: 'Print XRay Report',
									cls:'x-btn-text-icon',
									iconAlign: 'left',
									text: ' ',
									width: 30,
									height: 30,
									scale: 'medium',
									icon: 'http://www.reifax.com/img/toolbar/printer.png',
									handler: function(){
										Ext.Ajax.request( 
										{  
											waitMsg: 'Printing...',
											url: 'imprimir/properties_xray_print.php', 
											method: 'POST',
											timeout :600000,
											params: { 
												db: bd,
												proper: xcode,
												type: 'P',
												latlong: latlong
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												
												var results=response.responseText;
												if(Ext.isIE)
													window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
												else
													window.open(results,'_newtab');
											}                                
										});
									}
								},{
									 tooltip: 'Save XRay Report',
									 cls:'x-btn-text-icon',
									 iconAlign: 'left',
									 text: ' ',
									 width: 30,
									 height: 30,
									 scale: 'medium',
									 icon: 'http://www.reifax.com/img/toolbar/save.png',
									 handler: function(){
										 var simple = new Ext.FormPanel({
											url: 'imprimir/properties_xray_print.php',
											frame:true,
											title: 'Saved Documents.',
											width: 400,
											waitMsgTarget : 'Saving Documents...',
												
											items: [{
												xtype     : 'textfield',
												name      : 'name_save',
												fieldLabel: 'Name',
												value     : '',
												width: 200
											},{
												xtype     : 'hidden',
												name      : 'db',
												value     : bd
											},{
												xtype     : 'hidden',
												name      : 'proper',
												value     : xcode
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'S'
											},{
												xtype     : 'hidden',
												name      : 'latlong',
												value     : latlong
											}],
												
											buttons: [{
												text: 'Save',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Saved Documents", 'Your document has been save.');
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", action.result.msg);
														}
													});
												}
											},{
												text: 'Cancel',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
													 
										var win = new Ext.Window({
											layout      : 'fit',
											width       : 400,
											height      : 170,
											modal	 	: true,
											plain       : true,
											items		: simple,
											closeAction : 'hide',
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
									 }
								},'->',{
									 tooltip: 'Close XRay Report',
									 cls:'x-btn-text-icon',
									 iconAlign: 'left',
									 text: ' ',
									 width: 30,
									 height: 30,
									 scale: 'medium',
									 icon: 'http://www.reifax.com/img/cancel.png',
									 handler: function(){
										 var tab = tabs.getItem('reportsTab');
										 tabs.remove(tab);
									 }
								}]
							})
						}).show();
				 	}
				},{
					 tooltip: 'Discount Report',
					 cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					 icon: 'http://www.reifax.com/img/toolbar/rebate.jpg',
					 handler: function(){
						 var bd = document.getElementById('occounty').value;
						 var latlong = document.getElementById('mapa_search_latlongAdv').value;
						 var xcode = document.getElementById('ocproptype').value;
						 if(Ext.isEmpty(xcode) || xcode=='*'){ 
							Ext.Msg.alert("Discount Report", 'Property Type is requiered to execute the Discount Report.');
							return false;
						 }
						 if(latlong=='-1'){
							 Ext.Msg.alert("Discount Report", 'Map shape/polygon is requiered to execute the Discount Report.');
							 return false;
						 }
														 
						 if(document.getElementById('reportsTab')){
							 var tab = tabs.getItem('reportsTab');
							 tabs.remove(tab);
						 }
						 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
						 
						tabs.add({
							title: ' Reports ',
							id: 'reportsTab',
							autoLoad: {url: 'reports_types/properties_rebate.php?db='+bd+'&proper='+xcode, scripts: true, params: {latlong:latlong}},
							closable: true,
							tbar: new Ext.Toolbar({
								cls: 'no-border',
								width: 'auto',
								items: [' ',{
									 tooltip: 'Print Discount Report',
									 cls:'x-btn-text-icon',
									 iconAlign: 'left',
									 text: ' ',
									 width: 30,
									 height: 30,
									 scale: 'medium',
									 icon: 'http://www.reifax.com/img/toolbar/printer.png',
									 handler: function(){
										Ext.Ajax.request( 
										{  
											waitMsg: 'Printing...',
											url: 'imprimir/properties_rebate_print.php', 
											method: 'POST',
											timeout :600000,
											params: { 
												db: bd,
												proper: xcode,
												type: 'P',
												latlong: latlong
											},
											
											failure:function(response,options){
												loading_win.hide();
												Ext.MessageBox.alert('Warning','ERROR');
											},
											success:function(response,options){
												
												var results=response.responseText;
												if(Ext.isIE)
													window.open(results,null,'width='+screen.width+', height='+screen.height+', top=0, left=0, directories=no, location=no, menubar=yes, resizable=no, scrollbars=yes, status=no, toolbar=no, target=_newtab');
												 else
													window.open(results,'_newtab');
											}                                
										});
									 }
								},{
									 tooltip: 'Save Discount Report',
									 cls:'x-btn-text-icon',
									 iconAlign: 'left',
									 text: ' ',
									 width: 30,
									 height: 30,
									 scale: 'medium',
									 icon: 'http://www.reifax.com/img/toolbar/save.png',
									 handler: function(){
										 var simple = new Ext.FormPanel({
											url: 'imprimir/properties_rebate_print.php',
											frame:true,
											title: 'Saved Documents.',
											width: 400,
											waitMsgTarget : 'Saving Documents...',
											
											items: [{
												xtype     : 'textfield',
												name      : 'name_save',
												fieldLabel: 'Name',
												value     : '',
												width: 200
											},{
												xtype     : 'hidden',
												name      : 'db',
												value     : bd
											},{
												xtype     : 'hidden',
												name      : 'proper',
												value     : xcode
											},{
												xtype     : 'hidden',
												name      : 'type',
												value     : 'S'
											},{
												xtype     : 'hidden',
												name      : 'latlong',
												value     : latlong
											}],
												
											buttons: [{
												text: 'Save',
												handler: function(){
													loading_win.show();
													simple.getForm().submit({
														success: function(form, action) {
															loading_win.hide();
															win.close();
															Ext.Msg.alert("Saved Documents", 'Your document has been save.');
														},
														failure: function(form, action) {
															loading_win.hide();
															Ext.Msg.alert("Failure", action.result.msg);
														}
													});
												}
											},{
												text: 'Cancel',
												handler  : function(){
													simple.getForm().reset();
													win.close();
												}
											}]
										});
												 
										var win = new Ext.Window({
											layout      : 'fit',
											width       : 400,
											height      : 170,
											modal	 	: true,
											plain       : true,
											items		: simple,
											closeAction : 'hide',
											buttons: [{
												text     : 'Close',
												handler  : function(){
													win.close();
												}
											}]
										});
										win.show();
									 }
								},'->',{
									 tooltip: 'Close Discount Report',
									 cls:'x-btn-text-icon',
									 iconAlign: 'left',
									 text: ' ',
									 width: 30,
									 height: 30,
									 scale: 'medium',
									 icon: 'http://www.reifax.com/img/cancel.png',
									 handler: function(){
										 var tab = tabs.getItem('reportsTab');
										 tabs.remove(tab);
									 }
								}]
							})
						}).show();
					 }
				},{
					tooltip: 'Manage Saved Parameters',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/saveparams.png',
					handler: function(){
						//alert(user_loged+"||"+user_block+"||"+user_web);
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
							 ShowSavedSearchParameter();
					}
				},{
					 tooltip: 'Find Location',
					 cls:'x-btn-text-icon',
					 iconAlign: 'left',
					 text: ' ',
					 width: 30,
					 height: 30,
					 scale: 'medium',
					 hidden: <?php echo $platinum!=1 ? 'true' : 'false';?>, 
					 icon: 'http://www.reifax.com/img/toolbar/location.png',
					 handler: function(){
						 //alert(user_loged+"||"+user_block+"||"+user_web);
						 if(!user_loged || user_block || user_web){ login_win.show(); return false;}
						 var formlocation = new Ext.FormPanel({
													frame:true,
													title: 'Get Location',
													width: 400,
													waitMsgTarget : 'Saving Documents...',
													items: [{
																xtype		:'combo',
																name		:'distance',
																id			:'distance',
																hiddenName	:'idtype',
																store		:new Ext.data.SimpleStore({
																				fields	:['iddisc'],
																				data 	: [['0.1'],['0.2'],['0.3'],['0.4'],['0.5'],['0.6'],['0.7'],['0.8'],['0.9'],['1'],['1.1'],['1.2'],['1.3'],['1.4'],['1.5'],['1.6'],['1.7'],['1.8'],['1.9'],['2']]
																			}),
																editable	: false,
																displayField:'iddisc',
																valueField	:'iddisc',
																typeAhead	:true,
																fieldLabel	:'Distance Miles',
																mode		:'local',
																triggerAction: 'all',
																emptyText	:'Select ...',
																selectOnFocus:true,
																allowBlank	:false,
																value		:'0.5',
																width		:100
															},{
																xtype: 'hidden',
																id: 'tipomap',
																value:'Advance'
															}],
													buttons: [{
															text: 'Get',
															handler: getLocationGps
														},{
															text: 'Cancel',
															handler  : function(){
																	win.close();
																}
														}]
													});
									 var win = new Ext.Window({
													layout      : 'fit',
													id: 'ventanalocation',
													width       : 400,
													height      : 170,
													modal	 	: true,
													plain       : true,
													items		: formlocation,
													closeAction : 'hide'
												});
												win.show();
					 }
				}/*,{
					tooltip: 'View Help',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
					icon: 'http://www.reifax.com/img/toolbar/videohelp.png',
					handler: function(){
						//alert(user_loged+"||"+user_block+"||"+user_web);
						if(!user_loged || user_block || user_web){ login_win.show(); return false;}
						showVideoHelp('AdvancedSearch');
					}
				}
				*/,'->'
				,{
					iconCls:'icon',
					//iconAlign: 'top',
					//width: 40,
					icon: 'http://www.reifax.com/img/toolbar/reset.png',
					scale: 'medium',
					text: 'Reset&nbsp;&nbsp; ',
					handler  : function(){
						if(mapSearchAdv!=null)
						{
							search_by_typeAdv='MAP_OFF';
							mapSearchAdv.cleaner()
						}
						
						formulsearchadv.getForm().reset();
						for(x=0;x<arrinpbetween.length;x++)
						{	
							Ext.getCmp(arrinpbetween[x]).setVisible(false);
						}
						arrinpbetween=new Array();
						formulsearchadv.getForm().submit({
							method: 'POST',
							params: { searchType:'advance'},
							waitTitle: 'Please wait..',
							waitMsg: 'Sending data...',
							success: function() {
								//Ext.Msg.alert("Success", "aqui");
								systemsearch='advance';
							},
							failure: function(form, action) {
								obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.errors.reason);
							}
						});
					}
				},
				' '
				,{
					iconCls:'icon',
					//iconAlign: 'top',
					//width: 40,
					icon: 'http://www.reifax.com/img/toolbar/search.png',
					scale: 'medium',
					text: 'Search&nbsp;&nbsp; ',
					handler  : function(){
						formulsearchadv.getForm().submit({
							method: 'POST',
							params: { searchType:'advance'},
							waitTitle: 'Please wait..',
							waitMsg: 'Sending data...',
							success: function() {
								//Ext.Msg.alert("Success", "aqui");
								systemsearch='advance';
								createResult(user_web,realtor_block);
							},
							failure: function(form, action) {
								obj = Ext.util.JSON.decode(action.response.responseText);
								Ext.Msg.alert("Failure", obj.errors.reason);
							}
						});
					}				
				}]
			})			
		}
		<?php }?>
	],
	listeners: {
		'tabchange': function(tabpanel,tab){
			if(tab){
				if(tab.id!='searchTabBasic'){
					if(document.getElementById('control_mapa_div'))
						document.getElementById('control_mapa_div').style.display='none';
					if(document.getElementById('control_mapa_divAdv'))
						document.getElementById('control_mapa_divAdv').style.display='';
				}else{
					if(document.getElementById('control_mapa_div'))
						document.getElementById('control_mapa_div').style.display='';
					if(document.getElementById('control_mapa_divAdv'))
						document.getElementById('control_mapa_divAdv').style.display='none';
				}
			}
		}
	}
});

</script>