<!doctype html>
<html>
<?php
//header ("Cache-Control: no-cache, must-revalidate"); //no guardar en CACHE
//header ("Pragma: no-cache");
?>
<head>
	<style type="text/css">
		.search-item{
			border:1px solid #fff;
			padding:3px;
			background-position:right bottom; 
			background-repeat:no-repeat;
		}
		.desc{	
			padding-right:10px;
		}
		.name{	
			font-size:16px !important;
			color:#000022;	
		}
	
	</style>	
	<script src="includes/js/kinetic.js"></script>
	<script type="text/javascript" src="compatibility.js"></script> <!-- PDFJSSCRIPT_REMOVE_FIREFOX_EXTENSION -->
	<!-- In production, only one script (pdf.js) is necessary -->
	<!-- In production, change the content of PDFJS.workerSrc below -->
	<script type="text/javascript" src="src/core.js"></script>
	<script type="text/javascript" src="src/util.js"></script>
	<script type="text/javascript" src="src/api.js"></script>
	<script type="text/javascript" src="src/canvas.js"></script>
	<script type="text/javascript" src="src/obj.js"></script>
	<script type="text/javascript" src="src/function.js"></script>
	<script type="text/javascript" src="src/charsets.js"></script>
	<script type="text/javascript" src="src/cidmaps.js"></script>
	<script type="text/javascript" src="src/colorspace.js"></script>
	<script type="text/javascript" src="src/crypto.js"></script>
	<script type="text/javascript" src="src/evaluator.js"></script>
	<script type="text/javascript" src="src/fonts.js"></script>
	<script type="text/javascript" src="src/glyphlist.js"></script>
	<script type="text/javascript" src="src/image.js"></script>
	<script type="text/javascript" src="src/metrics.js"></script>
	<script type="text/javascript" src="src/parser.js"></script>
	<script type="text/javascript" src="src/pattern.js"></script>
	<script type="text/javascript" src="src/stream.js"></script>
	<script type="text/javascript" src="src/worker.js"></script>
	<script type="text/javascript" src="external/jpgjs/jpg.js"></script>
	<script type="text/javascript" src="src/jpx.js"></script>

	<script type="text/javascript">
		// Specify the main script used to create a new PDF.JS web worker.
		// In production, change this to point to the combined `pdf.js` file.
		PDFJS.workerSrc = 'src/worker_loader.js';
	</script>
	<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/ext-all.js"></script>
	<script type="text/javascript" src="http://www.reifax.com/MANT/LIB/ext/examples/ux/ux-all.js"></script>
	<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/ext-all.css" />
	<link rel="stylesheet" type="text/css" href="http://www.reifax.com/MANT/LIB/ext/resources/css/xtheme-xima.css" />
	<script>
		if (typeof Range.prototype.createContextualFragment == "undefined") {
		    Range.prototype.createContextualFragment = function (html) {
		        var doc = window.document;
		        var container = doc.createElement("div");
		        container.innerHTML = html;
		        var frag = doc.createDocumentFragment(), n;
		        while ((n = container.firstChild)) {
			  frag.appendChild(n);
		        }
		        return frag;
		    };
		}	
		Ext.onReady(function(){ // Inicializamos 
			coor.loading_win_custom=new Ext.Window({
				width:170,
				x:400,
				y:200,
				autoHeight: true,
				resizable: false,
				modal: true,
				border:false,
				closable:false,
				plain: true,
				html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="../../../../../img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
			}).show();
		});
	</script>	
	<script>
		Ext.namespace('capas');
		var canvas;
		var canvasCtx;

		var ghostcanvass;
		var gctx;
		var HEIGHT;
		var WIDTH;
		var gctx; // fake canvas context
		var stage;
		var layer;
	
		var stagex;
		var layerx;
		capas.posx='';
		capas.posx='';
		capas.bandera=true;

		function validarSeleccionDePosicion(){
			if(capas.posx=='' || capas.posy==''){
				var msgbox = Ext.MessageBox.alert('Warning','Please: FiFFrst Select a Position over Contract PDF Page');
				return false;
			}
			return true;
		}

		function crear() {
			stagex = new Kinetic.Stage({
			container: "container",
			width: canvas.width,
			height: canvas.height
		});
				layerx = new Kinetic.Layer();
			
			layerx.getCanvas().id = 'canvasxcanvas';
			
			stagex.add(layerx);

			var auxJESUS = document.getElementById('canvasxcanvas');
			var otroxx = document.getElementById('container');
		 
			auxJESUS.addEventListener("click",function(e){
					capas.posx = e.pageX - otroxx.offsetLeft;
					capas.posy = e.pageY - otroxx.offsetTop;
					var aux = Ext.getCmp('check1').pressed;
					if(aux==true){
						capas.checksbox(1);
					}
					var aux = Ext.getCmp('check2').pressed;
					if(aux==true){
						capas.checksbox(2);
					}
					var aux = Ext.getCmp('check3').pressed;
					if(aux==true){
						capas.checksbox(3);
					}
					var aux = Ext.getCmp('cmb-Tpl').getValue();
					if(aux!="" && aux!="0000"){
						var combo		= Ext.getCmp('cmb-Tpl');
						var value_id	= combo.getValue();
						var index = 0;
						combo.setValue(combo.store.getAt(index).get(combo.valueField));
						combo.selectedIndex = index;
						capas.firmas(value_id,coor.rut_imagen,coor.typ_imagen);
					}
					var aux = Ext.getCmp('idfieldcontract').getValue();
					if(aux!="" && aux!="0000"){
						var combo			= Ext.getCmp('idfieldcontract');
						var value_display	= combo.getRawValue();
						var value_id		= combo.getValue();
						var index = 0;
						combo.setValue(combo.store.getAt(index).get(combo.valueField));
						combo.selectedIndex = index;
						capas.variable(value_id,value_display);
					}
				}
			,false);
			
		};

	capas.firmas_add = function(id,imagen,type,x,y,width,height){
		capas.box = new Kinetic.Group({
			x: x,
			y: y,
			name:"grupofirmas",
			draggable: true
		});

		layerx.add(capas.box);
		stagex.add(layerx);

		// darth vader
		var img = new Image();
		img.src=imagen;
		var darthVaderImg = new Kinetic.Image({
			image:		img,
			x:			0,
			y:			0,
			idtox:		type,
			idtypex:	"signature",
			height:		height,
			width:		width,
			stroke:		"#00FF80",
			//strokeWidth:4,
			name: 		"image"
		});

		darthVaderImg.on("mouseover", function() {
			var layer = this.getLayer();
			this.setStroke("red");
			layer.draw();
		});
		darthVaderImg.on("mouseout", function() {
			var layer = this.getLayer();
			this.setStroke("#00FF80");
			layer.draw();
		});

		capas.box.add(darthVaderImg);
		addAnchor(capas.box, 0, 0, "topLeft");
		addAnchor(capas.box, darthVaderImg.attrs.width, 0, "topRight");
		addAnchor(capas.box, darthVaderImg.attrs.width, darthVaderImg.attrs.height, "bottomRight");
		addAnchor(capas.box, 0, darthVaderImg.attrs.height, "bottomLeft");

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragstart", function() {
			this.moveToTop();
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		layerx.draw();
		stagex.draw();
		//coor.updateDetalleTemp();
	}
	
	capas.firmas = function(id,imagen,type){
		capas.box = new Kinetic.Group({
			x: capas.posx,
			y: capas.posy,
			name:"grupofirmas",
			draggable: true
		});

		layerx.add(capas.box);
		stagex.add(layerx);

		// darth vader
		var img = new Image();
		var alto;
		var ancho;
		var aditional;
		if(type=='1' || type=='2' || type=='5' || type=='6'){
			alto=80;
			ancho=280;
		}else{
			alto=26;
			ancho=70;
		}
		img.src=imagen;
		var darthVaderImg = new Kinetic.Image({
			image:		img,
			x:			0,
			y:			0,
			idtox:		type,
			idtypex:	"signature",
			height:		alto,
			width:		ancho,
			name: 		"image"
		});

		darthVaderImg.on("mouseover", function() {
			var layer = this.getLayer();
			this.setStroke("red");
			layer.draw();
		});
		darthVaderImg.on("mouseout", function() {
			var layer = this.getLayer();
			this.setStroke("#00FF80");
			layer.draw();
		});

		capas.box.add(darthVaderImg);
		addAnchor(capas.box, 0, 0, "topLeft");
		addAnchor(capas.box, darthVaderImg.attrs.width, 0, "topRight");
		addAnchor(capas.box, darthVaderImg.attrs.width, darthVaderImg.attrs.height, "bottomRight");
		addAnchor(capas.box, 0, darthVaderImg.attrs.height, "bottomLeft");

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragstart", function() {
			this.moveToTop();
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		layerx.draw();
		stagex.draw();
		coor.updateDetalleTemp();
	}
	
	function update(group, activeAnchor) {
		var topLeft = group.get(".topLeft")[0];
		var topRight = group.get(".topRight")[0];
		var bottomRight = group.get(".bottomRight")[0];
		var bottomLeft = group.get(".bottomLeft")[0];
		var image = group.get(".image")[0];

		// update anchor positions
		switch (activeAnchor.getName()) {
			case "topLeft":
				topRight.attrs.y = activeAnchor.attrs.y;
				bottomLeft.attrs.x = activeAnchor.attrs.x;
				break;
			case "topRight":
				topLeft.attrs.y = activeAnchor.attrs.y;
				bottomRight.attrs.x = activeAnchor.attrs.x;
				break;
			case "bottomRight":
				bottomLeft.attrs.y = activeAnchor.attrs.y;
				topRight.attrs.x = activeAnchor.attrs.x;
				break;
			case "bottomLeft":
				bottomRight.attrs.y = activeAnchor.attrs.y;
				topLeft.attrs.x = activeAnchor.attrs.x;
				break;
		}

//		if((bottomLeft.attrs.y - topLeft.attrs.y)>10 && (topRight.attrs.x - topLeft.attrs.x)>10){
			image.setPosition(topLeft.attrs.x, topLeft.attrs.y);
			image.setSize(topRight.attrs.x - topLeft.attrs.x, bottomLeft.attrs.y - topLeft.attrs.y);
/*		}else{
			return false;
		}*/
	}
	function addAnchor(group, x, y, name) {
		var stage = group.getStage();
		var layer = group.getLayer();

		var anchor = new Kinetic.Circle({
			x: x,
			y: y,
			stroke: "red",
			fill: "#ddd",
			strokeWidth: 2,
			radius: 3,
			name: name,
			draggable: true
		});

		anchor.on("dragmove", function() {
			update(group, this);
			layer.draw();
		});
		anchor.on("mousedown", function() {
			group.draggable(false);
			this.moveToTop();
		});
		anchor.on("dragend", function() {
			group.draggable(true);
			layer.draw();
		});
		// add hover styling
		anchor.on("mouseover", function() {
			var layer = this.getLayer();
			document.body.style.cursor = "pointer";
			this.setStrokeWidth(4);
			layer.draw();
		});
		anchor.on("mouseout", function() {
			var layer = this.getLayer();
			document.body.style.cursor = "default";
			this.setStrokeWidth(2);
			layer.draw();
		});
		group.add(anchor);
	}
	
////////////////////////////VARIABLES////////////////////////////
	capas.variable = function(id,display){
/*		if(layerx==undefined){
			crear();
		}		
		if(validarSeleccionDePosicion()){*/
			capas.box = new Kinetic.Rect({
				//x: 2 * 30 + 150,
				//y: 2 * 18 + 40,
				x: capas.posx,
				y: capas.posy-16,
				fill: "#00FF80",
				stroke: "black",
				strokeWidth: 0,
				draggable: true,
				idtox:id,
				idtypex:"variable",
				textox: "{%"+display+"%}",
				width: 130,
				height: 13
			});

			capas.box.on("dragstart", function() {
				capas.box.moveToTop();
				layerx.draw();
				//capas.bandera=false;
			});

			capas.box.on("dragmove", function() {
				document.body.style.cursor = "pointer";
				//capas.bandera=false;
			});
			
			/*
			 * dblclick to remove box for desktop app
			 * and dbltap to remove box for mobile app
			 */
/*			capas.box.on("dblclick dbltap", function() {
				layerx.remove(this);
				layerx.draw();
			});*/

			capas.box.on("dblclick dbltap", function() {
				//this.attrs.stroke="red";
				//this.attrs.strokeWidth=2
				layerx.remove(this);
				layerx.draw();
				coor.updateDetalleTemp();
			});
			
			capas.box.on("mouseup", function() {
				coor.updateDetalleTemp();
			});			
			
			capas.box.on("mouseover", function() {
				document.body.style.cursor = "pointer";
				this.attrs.stroke="red";
				this.attrs.strokeWidth=2;
				layerx.draw();
				//capas.bandera=false;
			});
			capas.box.on("mouseout", function() {
				document.body.style.cursor = "default";
				this.attrs.stroke="black";
				this.attrs.strokeWidth=0;
				layerx.draw();
				//capas.bandera=true;
			});
			
			layerx.add(capas.box);
			stagex.add(layerx);
			coor.updateDetalleTemp();
			//alert(box.index);
/*		}else{
			return false;
		}*/
	}

	capas.variable_add = function(id,display,x,y){

		capas.box = new Kinetic.Rect({
			//x: 2 * 30 + 150,
			//y: 2 * 18 + 40,
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:id,
			idtypex:"variable",
			textox: display,
			width: 130,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
		//coor.updateDetalleTemp();
	}

	capas.variable_user = function(display,x,y,w){

		capas.box = new Kinetic.Rect({
			//x: 2 * 30 + 150,
			//y: 2 * 18 + 40,
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_user",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
		coor.updateDetalleTemp();
	}

	capas.variable_user_add = function(display,x,y,w){

		capas.box = new Kinetic.Rect({
			//x: 2 * 30 + 150,
			//y: 2 * 18 + 40,
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_user",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
	}
////////////////////////////Check's////////////////////////////
	capas.checksbox = function(id){
/*		if(layerx==undefined){
			crear();
		}		*/
//		if(validarSeleccionDePosicion()){
			switch(id){
				case 1:
					var imagen = '/img/check1_sin.png';
					Ext.getCmp('check1').toggle(false);
					break;
				case 2:
					var imagen = '/img/check2_sin.png';
					Ext.getCmp('check2').toggle(false);
					break;
				case 3:
					var imagen = '/img/check3_sin.png';
					Ext.getCmp('check3').toggle(false);
					break;
			}			
			var imageObj = new Image();
			imageObj.src = imagen;
			capas.box = new Kinetic.Image({
				image:		imageObj,
				x:			capas.posx-7,
				y:			capas.posy-8,
				width:		10,
				idtox:		id,
				idtypex:	"check",
				height:		10
			});

			capas.box.draggable(true);	
			capas.box.on("dragstart", function() {
				capas.box.moveToTop();
				layerx.draw();
				//capas.bandera=false;
			});

			
			capas.box.on("mouseup", function() {
				coor.updateDetalleTemp();
			});
			
			capas.box.on("dragmove", function() {
				document.body.style.cursor = "pointer";
				//capas.bandera=false;
			});
			/*
			 * dblclick to remove box for desktop app
			 * and dbltap to remove box for mobile app
			 */
			capas.box.on("dblclick dbltap", function() {
				layerx.remove(this);
				layerx.draw();
				coor.updateDetalleTemp();
			});

			capas.box.on("mouseover", function() {
				document.body.style.cursor = "pointer";
				this.attrs.stroke="red";
				this.attrs.strokeWidth=2;
				layerx.draw();
				//capas.bandera=false;
			});
			capas.box.on("mouseout", function() {
				document.body.style.cursor = "default";
				this.attrs.stroke="white";
				this.attrs.strokeWidth=0;
				layerx.draw();
				//capas.bandera=true;
			});
			layerx.add(capas.box);
			stagex.add(layerx);
			coor.updateDetalleTemp();
/*		}else{
			return false;
		}*/
	}

	capas.checksbox_add = function(id,x,y){
		id=parseInt(id);
		if(layerx==undefined){
			crear();
		}		
		switch(id){
			case 1:
				var imagen = '/img/check1_sin.png';
				break;
			case 2:
				var imagen = '/img/check2_sin.png';
				break;
			case 3:
				var imagen = '/img/check3_sin.png';
				break;
		}
		var imageObj = new Image();
		imageObj.src = imagen;
		capas.box = new Kinetic.Image({
			image:		imageObj,
			x:			parseInt(x),
			y:			parseInt(y),
			width:		10,
			idtox:		id,
			idtypex:	"check",
			height:		10
		});

		capas.box.draggable(true);	
		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();			
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="white";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		layerx.add(capas.box);
		stagex.add(layerx);
		//coor.updateDetalleTemp();
	}
	</script>
<?php 	
	//include("includes/php/combo.php");
?>
	<!--<script type="text/javascript" src="includes/js/boxes.js"></script>-->
	<script type="text/javascript" src="includes/js/ventana.js"></script>
	<script type="text/javascript" src="includes/js/hello.js"></script>
	<script>
	Ext.onReady(function(){ // Inicializamos 
		Ext.Ajax.request({
			url	: '/custom_contract/includes/php/functions.php',
			method	: 'post',
			params	: { idfunction: 1 },
			success	: function(response, opts) {
				var obj = Ext.util.JSON.decode(response.responseText);
				pagess.contract_name = obj.filename;
				pagess.pdf_principal();
			},
			failure	: function(response, opts) {
				alert('server-side failure with status code ' + response.status);
			}
		});
	});
	</script>
	
</head>
<!-- OJO OJO OJO OJO OJO OJO OJO OJO -->
<!--<body onmousedown="return false;">-->
<!-- lo desactive porque no me dejaba trabajar con los formularios -->
<body>
	<!--style="position:absolute;left:4%;top:7%;border:1px solid black;z-index: -1;"-->
	<canvas id="the-canvas"  align="center" width="0" height="0" style="position:absolute;left:2%;top:80;border:1px solid black;z-index: 1;"/></canvas>
	<!--<canvas id="variablesid" align="center" style="position:absolute;left:4%;top:7%;border:1px solid black;z-index: 2;"/></canvas>-->
	<!--style="position:absolute;left:4%;top:7%;border:1px solid black;z-index: 3;"-->
    <div id="container" width="0" height="0" style="position:absolute;left:2%;top:80;border:1px solid black;z-index: 3;">
    </div>
</body>
</html>
<?php 
clearstatcache();
/*
 *1 solucionar el peo de las firmas
 *2  1 cm mas ancho el get agent de la pesta�a contract managere
 * eliminar la funcion del chulito Send Contract by Email cambiar por fax y meter el template, quitar el escrow leter por include escrow leter
 * */
?>