<?php
function getNumPagesInPDF(array $arguments = array()){
	@list($PDFPath) = $arguments;
	$stream = @fopen($PDFPath, "r");
	$PDFContent = @fread ($stream, filesize($PDFPath));
	if(!$stream || !$PDFContent)
		return false;
	   
	$firstValue = 0;
	$secondValue = 0;
	if(preg_match("/\/N\s+([0-9]+)/", $PDFContent, $matches)) {
		$firstValue = $matches[1];
	}
	 
	if(preg_match_all("/\/Count\s+([0-9]+)/s", $PDFContent, $matches)){
		$secondValue = max($matches[1]);
	}
	return (($secondValue != 0) ? $secondValue : max($firstValue, $secondValue));
}

function cleanwords($cad)
{
	$cadfull='';
	for($i=0;$i<strlen($cad);$i++)
	{
		$d=substr($cad,$i,1);
		if(ord($d)<=127)
		{
			//echo $d,': ',ord($d),"<br>";
			$cadfull.=$d;
		}
	}
	return $cadfull;
}

function limpiardirpdf2($ruta)
	{
		$directorio=dir($ruta); 
		$directorio->read();$directorio->read();
		$tiempo=strtotime("now")-1800;//Borra los que estan modificados de hace dos horas
		while ($archivo = $directorio->read()) 
		{ 
			$ext=substr($archivo,strrpos($archivo,'.')+1);
			if(filectime($ruta.$archivo)<$tiempo && $ext=='pdf')
				unlink($ruta.$archivo);  
			//echo $extension." ".$ruta.$archivo." ".date ("Y-n-d_H:i:s", filemtime($ruta.$archivo))." ".date ("Y-n-d_H:i:s")."<br>";
		} 
		$directorio->close(); 	
	}

function obtainRealCountyNotForSale($county){
	switch($county){
		case "PALMBEACH":
			$county	=	"PALM BEACH";
			break;
		case "DESOTO":
			$county	=	"DE SOTO";
			break;
		case "INDIANRIVER":
			$county	=	"INDIAN RIVER";
			break;
		case "SANTAROSA":
			$county	=	"SANTA ROSA";
			break;
		case "STJOHNS":
			$county	=	"ST JOHNS";
			break;
		case "STLUCIE":
			$county	=	"ST LUCIE";
			break;
		default:
			$county	=	$county;
			break;
	}
	return	$county;
}

function verifySendMails($userid,$followUp=false){
	global $totalsent;
	
	if($totalsent>=30){
		
		if($followUp!==false){
			$query	=	"
				SELECT COUNT(*) AS total FROM xima.follow_emails a 
				INNER JOIN xima.follow_emails_assigment b ON a.idmail=b.idmail 
				WHERE b.parcelid='$followUp' and a.userid='$userid'
			";
			$result	=	mysql_query($query) or die(mysql_error());
			$result	=	mysql_fetch_assoc($result);
			$result	=	intval($result['total']);
			
			return intval($result) >= 1 ? true : false;
		}else
			return false;
	}
	
	return true;
}

function controlSendMails($userid,$followUp=false){
	global $totalsent;
	if($followUp<>false){
		$query	=	"
			SELECT COUNT(*) AS total FROM xima.follow_emails a 
			INNER JOIN xima.follow_emails_assigment b ON a.idmail=b.idmail 
			WHERE b.parcelid='$followUp' and a.userid='$userid'
		";
		$result	=	mysql_query($query) or die(mysql_error());
		$result	=	mysql_fetch_assoc($result);
		$result	=	$result['total'];
	}
	$queryU	=	"
		UPDATE mantenimiento.emailusercontrol
		SET 
			totalSendMail = (totalSendMail+1)
		WHERE userid='$userid'";
	if($result<=1 or $followUp==false){
		mysql_query($queryU);
		if(mysql_affected_rows()<=0){
			$query	=	"
				INSERT INTO mantenimiento.emailusercontrol 
						(userid)
					VALUES 
						('$userid')";
			mysql_query($query);
			mysql_query($queryU);
		}
	}else{
		$query	=	"SELECT * FROM mantenimiento.emailusercontrol WHERE totalSendMail>=30 AND DATE_FORMAT(fecha,'%Y-%m-%d')<>DATE(NOW()) AND userid=$userid";
		mysql_query($query) or die(mysql_error());
		if(mysql_affected_rows()>=1)
			mysql_query($queryU) or die(mysql_error());
	}
	$query	=	"SELECT totalSendMail FROM mantenimiento.emailusercontrol WHERE userid='$userid'";
	$result	=	mysql_query($query);
	$result	=	mysql_fetch_assoc($result);
	$totalsent = intval($result['totalSendMail']);
	
	return intval($result['totalSendMail']);
}
?>