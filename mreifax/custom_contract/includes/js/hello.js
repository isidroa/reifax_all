/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

//
// See README for overview
//

'use strict';
Ext.namespace('pagess');


pagess.paginas = new Array();
//var pagess.contract_name;
//alert(pagess.paginas.length);

pagess.pdf_principal = function(){
	PDFJS.getDocument('/mysetting_tabs/mycontracts_tabs/'+pagess.contract_name).then(function(pdf) {

		pdf.getPage(1).then(function(page) {

			var x;
			for(x=1;x<=pdf.numPages;x=x+1){
				var c = new Array(x.toString(),"Page N� "+x);
				window.pagess.paginas.push(c);
				window.pagess.totalpaginas=pdf.numPages;
			}

			w_ejemplo();

			var scale = 1.5;
			
			//
			// Prepare canvas using PDF page dimensions
			//
			var viewport = page.getViewport(scale);
			canvas = document.getElementById('the-canvas');
			canvasCtx = canvas.getContext('2d');

			canvas.height = viewport.height;
			canvas.width = viewport.width;

			var renderContext = {
			      canvasContext: canvasCtx,
			      viewport: viewport
			    };
			page.render(renderContext);
			
			//
			// Render PDF page into canvas canvasCtx
			//
			coor.loading_win_custom.hide();
			crear();
			coor.llenarPagina();
		});
	});
}

pagess.pdf_new = function(page_num){
	var page_num;
	PDFJS.getDocument('/mysetting_tabs/mycontracts_tabs/'+pagess.contract_name).then(function(pdf) {

		pdf.getPage(page_num).then(function(page) {
			//var scale = 1.5; By Jesus
			var scale = 1.5;
			var viewport = page.getViewport(scale);
			
			//
			// Prepare canvas using PDF page dimensions
			//
			canvas = document.getElementById('the-canvas');
			canvasCtx = canvas.getContext('2d');

			canvas.height = viewport.height;
			canvas.width = viewport.width;

			//
			// Render PDF page into canvas canvasCtx
			//
			var renderContext = {
				canvasContext: canvasCtx,
				viewport: viewport
			};
			page.render(renderContext);
		
		});
	});
}