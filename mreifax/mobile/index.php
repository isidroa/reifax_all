<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ReiFax Login</title>
	
    <script src="js/jquery.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">
		function checkPreAuth() {
			var form = $("#loginForm");
			if(localStorage.username != undefined && localStorage.password != undefined) {
				$("#inputEmail", form).val(localStorage.username);
				$("#inputPassword", form).val(localStorage.password);
			}
		}
		
		function handleLogin() {
			var form = $("#loginForm");
			var u = $("#inputEmail", form).val();
			var p = $("#inputPassword", form).val();
			//store
			localStorage.username = u;
			localStorage.password = p;
			$("#loadingBack").show();

		}
		
		// Wait for device API libraries to load
		//
		function onLoad() {
			$("#loadingBack").hide();
			$("#loginForm").submit(handleLogin);
			checkPreAuth();
		}
	</script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>

<?php 
	if(isset($_COOKIE['datos_usr']['USERID'])){
		include_once('../properties_conexion.php');
		conectar();
		
		$query='SELECT * FROM xima.ximausrs WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		$result = mysql_query($query);
		
		if(mysql_num_rows($result)>0){
			$r = mysql_fetch_assoc($result);
			$email = $r['email'];
			$pass = $r['pass'];
		}
	}
?>	
    
  <body onLoad="onLoad()">
	<div id="loadingBack" class="loadingBack"><img src="img/loading.gif"></div>
    <div class="container">

      <form id="loginForm" class="form-signin" action="http://www.reifax.com/resources/php/properties_validacion.php" method="post">
        <a href="index.html"><img src="img/logo-reifax.png" width="100%"></a>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="loginEmail" class="form-control" <?php echo isset($email) ? 'value="'.$email.'"' : '';?>placeholder="Email address" required autofocus tabindex="1">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="loginPass" class="form-control" <?php echo isset($pass) ? 'value="'.$pass.'"' : '';?> placeholder="Password" required tabindex="2">
        <input type="hidden" value="1" name="mobileLogin">
        <button id="submitButton" class="btn btn-lg btn-success btn-block" type="submit" tabindex="3">Sign in</button>
        <a class="btn btn-lg btn-primary btn-block" href="forgotPassword.html">Forgot Password</a>
      	
        <a class="btn btn-lg btn-link btn-block" href="pdf/REIFAX_TermsAndConditions.pdf">Subscriber Terms and Conditions</a>
      </form>
      

    </div> <!-- /container -->
</body></html>