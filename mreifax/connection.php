<?php
	class connect {
		public $conMaster;
		var $ipMasterLocal	=	'reifaxcalculo.cdhpwwjssf1c.us-west-2.rds.amazonaws.com';
		var $ipMasterWan	=	'reifaxcalculo.cdhpwwjssf1c.us-west-2.rds.amazonaws.com';
		public $params			=	array(
			'dataBase'			=>	'xima',
			'typeIp'			=>	'local',
			'mysqlType'			=>	'root',
			'master'			=>	false,
			'ftpType'			=>	'XIMA',
			'mapeoWithMaster'	=>	false,
			'activeLogErrors'	=>	false,
			'downloadFiles'		=>	false,
			'options'			=>	array()
		);
		
		function connect(){
			return true;
		}
		public function mysqlByIp($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			
			$ipToUse	=	$this->getIp($default->typeIp);
			if($default->typeIp=='local')
				$where	=	"a.serverIpLocal='{$ipToUse}'";
			else
				$where	=	"a.serverIpWan='{$ipToUse}'";
				
			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a 
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers 
				WHERE 
					{$where} AND 
					b.mysqlType='{$default->mysqlType}'";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$result	=	$result->fetch_object();
			return	$this->mysqlConnect(
				($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan),
				$result->mysqlUser,
				$result->mysqlPass,
				$result->mysqlPort,
				$default->dataBase,
				$default->options
			);
		}
		public function mysqlByServer($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			
			if($default->master===false)
				$where	=	"a.serverNumber='{$default->serverNumber}'";
			else
				$where	=	"a.master=1";
			
			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a 
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers 
				WHERE 
					{$where} AND 
					b.mysqlType='{$default->mysqlType}'";
			if(!$result	=	$this->conMaster->query($query)){
				echo 'Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error;
				exit;
			}
			$result	=	$result->fetch_object();
			return	$this->mysqlConnect(
				($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan),
				$result->mysqlUser,
				$result->mysqlPass,
				$result->mysqlPort,
				$default->dataBase,
				$default->options
			);
		}
		public function ftpGetData($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			$data		=	new StdClass();
			
			if($default->master===false){
				$where	=	"a.serverNumber='{$default->serverNumber}'";
			}else{
				$where	=	"a.master=1";
			}
				
			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a 
					INNER JOIN mantenimiento.servers_ftp b on a.id_servers=b.id_servers 
				WHERE 
					{$where} AND 
					b.ftpType='{$default->ftpType}'";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$result	=	$result->fetch_object();
			$data->ftpServer=	($default->typeIp=='local'	?	$result->serverIpLocal	:	$result->serverIpWan);
			$data->ftpUser	=	$result->ftpUser;
			$data->ftpPass	=	$result->ftpPass;
			$data->ftpPort	=	$result->ftpPort;
			$data->ftpDir	=	$result->ftpRemoteDir;
			return	$data;
		}
		public function connectInit($optional=array()){	//Conection with Master Server
			$mysqli	=	mysqli_init();
			if(count($optional)>0){
				$this->addOptionals($mysqli,$optional);
				$this->options=array();
			}
			$mysqli->real_connect($this->ipMasterLocal,'masterdb','er1426xy!','xima');
			if($mysqli->connect_errno){
				die('Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
			}
			$mysqli->query('SET NAMES UTF8');
			mysql_query('set time_zone="US/Eastern";');
			$this->conMaster = $mysqli;
		}
		public function checkMaster($optional=array()){
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			$query	=	'SELECT * FROM mantenimiento.servers_conexions WHERE '.($default->typeIp=='local'	?	'serverIpLocal'	:	'serverIpWan').'="'.$this->getIp($default->typeIp).'"';
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			return ($result->num_rows>0	?	true	:	false);
		}
		public function mapeoServers($optional=array()){	//Conection with Mapeo Server's
			$this->resetOptions();
			$default	=	array_merge($this->params,$optional);
			$default	=	json_decode(json_encode($default),FALSE);
			if($default->mapeoWithMaster===false)
				$where	=	"AND a.`master`<>1";
			else
				$where	=	'';
			$query	=	"
				SELECT * FROM mantenimiento.servers_conexions a 
					INNER JOIN mantenimiento.servers_mysql b on a.id_servers=b.id_servers 
				WHERE a.mapeo='1' {$where}";
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			$connections	=	array();
			while($row	=	$result->fetch_object()){
				$connections[]=$this->mysqlConnect(
					($default->typeIp=='local'	?	$row->serverIpLocal	:	$row->serverIpWan),
					$row->mysqlUser,
					$row->mysqlPass,
					$row->mysqlPort,
					$default->dataBase,
					$default->options
				);
			}
			return $connections;
		}
		private function mysqlConnect($serverIp,$serverUser,$serverPass,$serverPort,$dataBase,$optional=array()){
			$mysqli	=	mysqli_init();
			if(count($optional)>0){
				$this->addOptionals($mysqli,$optional);
				$this->options=array();
			}
			$mysqli->real_connect($serverIp, $serverUser, $serverPass, $dataBase, $serverPort);
			if($mysqli->connect_errno){
				die('Error: '. __LINE__ .' Fallo al contenctar a MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
			}
			$mysqli->query("SET NAMES UTF8");
			mysql_query('set time_zone="US/Eastern";');
			return $mysqli;
		}
		public function getIp($typeIp){
			$ip="";
			/*if($typeIp=='local'){
				$exec		=	exec("hostname"); //the "hostname" is a valid command in both windows and linux
				$hostname	=	trim($exec); //remove any spaces before and after
				$ip			=	gethostbyname($hostname); //resolves the hostname using local hosts resolver or DNS
			}else
				$ip	=	$_SERVER['LOCAL_ADDR'];*/
			if($typeIp=='local'){
				$ipRes = shell_exec('ipconfig');
				$ipPattern = '/IPv4 Address[^:]+: ' . 
				   '([\d]{1,3}\.[\d]' .
				   '{1,3}\.[\d]{1,3}' .
				   '\.[\d]{1,3})/';
				if (preg_match_all($ipPattern, $ipRes, $matches)) {
					foreach($matches[1] as $value){
						if(strpos($value,'192.168.1.')!==false){
							$ip=$value;
							break;
						}
					}
				}
				if(empty($ip)){
					$command	=	"/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'";
					$ip = exec ($command);
				}
			}else{
				$ip=file_get_contents('http://bot.whatismyipaddress.com/');
			}
			return $ip;
		}
		public function getServerInfo($optional=array()){
			$this->resetOptions();
			$default=	array_merge($this->params,$optional);
			$default=	json_decode(json_encode($default),FALSE);
			$query	=	'SELECT * FROM mantenimiento.servers_conexions WHERE '.($default->typeIp=='local'	?	'serverIpLocal'	:	'serverIpWan').'="'.$this->getIp($default->typeIp).'"';
			if(!$result	=	$this->conMaster->query($query)){
				die('Error: '. __LINE__ .' MySQL: (' . $this->conMaster->errno . ') ' . $this->conMaster->error);
			}
			return $result->fetch_object();
		}
		public function resetOptions(){
			$this->params	=	array(
				'dataBase'			=>	'xima',
				'typeIp'			=>	'local',
				'mysqlType'			=>	'root',
				'master'			=>	false,
				'ftpType'			=>	'XIMA',
				'mapeoWithMaster'	=>	false,
				'activeLogErrors'	=>	false,
				'downloadFiles'		=>	false,
				'options'			=>	array()
			);
		}
		private function addOptionals(&$mysqli,$optional){
			foreach($optional as $option){
				$mysqli->options($option->name,$option->value); 
			}
		}
	}
?>