<?php
 /*$sufijo = explode('.',$_SERVER['SERVER_NAME']);
 if( !($sufijo[0]=='www' || $sufijo[0]=='reifax') ){
	 header('Location: http://'.$_SERVER['SERVER_NAME'].'/properties_search.php');
 }
	 
 if($sufijo[0]=='reifax')
	header('Location: http://www.reifax.com'); 
 
 if($_SERVER['HTTP_HOST']=='www.reifax.com' && $_SERVER['REQUEST_URI']!='/')
	header('Location: http://www.reifax.com');
 */
 
 
 require_once('resources/template/template2.php');
	require_once('resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>
<style>
	.container > div#bannerPrincipal {
  height: 400px;
}
.img-responsive.margin-bottom-20 {
  width: 100%;
}
h1.main-header {
  color: #8ab420;
  font-size: 35px;
  padding: 0;
  text-align: center;
}
.top-features {
  padding-left: 50px;
}
.top-features > li {
  color: #005c83;
  font-size: 25px;
  line-height: 35px;
  list-style: inside url("http://www.reifax.com/resources/img/listdot.png") disc;
}
.bannerButton {
  font-size: 20px !important;
  margin: 10px 40px 5px;
  padding: 10px;
  text-align: center;
}
.modal {
  background: none repeat scroll 0 0 rgba(0, 0, 0, 0.31);
  bottom: 0;
  display: none;
  left: 0;
  outline: 0 none;
  overflow-x: auto;
  position: fixed;
  right: 0;
  top: 0;
  z-index: 999999;
}
.modal-lg {
  width: 1020px;
}
video {
  display: block !important;
  height: auto !important;
  width: 100% !important;
}
</style>
<div class="container">
    <!-- Header of ReiFax Website -->
    <header>
	<?php $tem=explode('/',$_SERVER['REQUEST_URI']);($tem[1]=='settings')? ReiFaxMenuUser() :ReiFaxMenu(); ?>
	</header>
    
    <!--ReiFax "You are here" Text  --
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here: <span class="greentext">REI</span>Fax Home
    </div>-->
	<!--<div id="bannerPrincipal"></div>-->
    <div class="bannerPrincipal">
        <div style="width: 50%; float: left;" class="row">
            <div class="col-md-5 margin-top-20">
            	<p class="text-center"><img class="img-responsive margin-bottom-20" alt="REIPro Logo" src="/resources/img/logo_REIFAX.png"></p>
            </div>
            <div class="col-md-5">
            	<p><img style="height:288px;cursor: pointer;" onclick="ShowVideo();" class="img-responsive img-center" src="/resources/img/Reifax-Sales-Intro-Cover.png"></p>
            </div>
        </div>
        <div style="width: 50%; float: left;" class="row">
            <div style="margin-top: 30px;" class="col-md-7 tv-creds padl25">
                <h1 class="main-header">The Most Successful <br><span style="color:#005c83; font-size:90%;">Real Estate Information System</span></h1>
            </div>
            <div class="col-md-7 padl25">
                <ul class="top-features">
                    <li>Search Properties by Equity</li>
                    <li>Research Properties</li>
                    <li>Send &amp; Manage Contracts</li>
                    <li>Follow Up in Offers</li>
                    <li>Find Cash &amp; Retail Buyers</li>
                    <li>Foreclosures</li>
                    <li>Short Sales</li>
                </ul>
                <div onclick="GoDemo();" class="buttonblue bigButton bannerButton" style="cursor:pointer;">Order Your Free One on One Demo Now</div>
            </div>
        </div>
  			<div class="clear"> </div>
    </div>
    
    <div class="modal fade in" id="videoModal" aria-hidden="false">
        <div style="position: relative; top: 192px; left: 50%; margin-left: -505px;" class="modal-dialog modal-lg">
            <div class="modal-content">                
                <div class="panel panel-default">                
                    <video controls poster="/resources/img/Reifax-Sales-Intro-Cover.png" style="border:solid 1px #ccc; background-color:transparent;" id="vid1">
                        <source src="https://s3-us-west-2.amazonaws.com/reifaxvideo/reifaxmarketingvideo5.mp4" type="video/mp4"></source>
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
    </div>  
  	
  	<div id="contentPrincipal">
        <!-- Advertising Right Block-->
        <div class="sidebarright">
                <div id="advertisingFramework">
                </div>
        </div>
        <!-- Center Content-->
        <div class="content" style="min-height:1300px;">
            <?php /*?><!-- Advertising Rotative Top-->
            <div id="advertisingRotative">
                <div class="panel">
                    <div class="center">  
                        <ul id="home-carousel">
                            <div class="list_carousel">
                                <div id="advertisingRotativeSlide" class="ux-carousel-slides-wrap">
                                    <!--Avertising Rotative Items-->
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C1.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C2.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C3.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C4.png) no-repeat;">
                                    </div>
                                    <div class="ux-carousel-slide" style="background:url(resources/img/banners/C5.png) no-repeat;">
                                    </div>
                                    
                                </div> 
                                <div id="advertisingRotativePaging" class="ux-carousel-slides-paging">   
                            </div> 
                        </ul>
                    </div>
                </div>
                
                <!-- Rotative Functional-->
                <script type="text/javascript" language="javascript">
                    $(function() {
                        //	Basic carousel
                        $('#advertisingRotativeSlide').carouFredSel({
                            items: 1,
                            auto: {
                                play: true,
                                pauseDuration: 6000
                            },
                            scroll: {
                                items: 1,
                                duration: 600,
                                pauseOnHover: true
                            },
                            pagination : {
                                container	: "#advertisingRotativePaging",
                                duration	: 600
                            }
                        });
                    });
                </script>
            </div>
            <div class="clear">&nbsp;</div><?php */?>
            <!--
            		INICIO DEL SEARCH
            //-->
                    <div>
                        <ul class="css-tabs small mediunTabs" id="Search">
                            <li>
                                <a class="current" href="#PR">Public Records</a>
                            </li>
                            <li>
                                <a class="" href="#FO">Foreclosure</a>
                            </li>
                            <li>
                                <a class="" href="#FS">For Sale</a>
                            </li>
                            <li>
                                <a class="" href="#FR">For Rent</a>
                            </li>
                            <!--
							<li>
                                <a class="" href="#BO">By Owner Sale</a>
                            </li>
                            <li>
                                <a class="" href="#BOR">By Owner Rent</a>
                            </li>
							-->
                        </ul>
                    </div>
                    <div class="panes reports">
                    	<div id="search1" style="display:block" class="title">
                        	<form id="formSearch">
                            <input type="hidden" name="typeSearch">
                        	<div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            State
                                            </label><br>
                                            <select class="fieldtext" name="stadeSearch" style="width:146px">
                                            <?php
                                            conectar('xima');
                                                $sql="SELECT * FROM lsstate where is_showed='Y';";
                                                $resultado=mysql_query($sql);
                                                while($data=mysql_fetch_assoc($resultado))
                                                {
                                                    echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                                }
                                            ?>
                                            </select>
                                </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            County
                                            </label><br>
                                            <select id="SearchCounty" name="countySearch" class="fieldtext" style="width:146px">
                                            </select>
                              	</div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Property Type 
                                            </label><br>
                                            <select name="propertySearch" class="fieldtext" style="width:146px">	
                                                    <option value="">Any Property Type</option>
                                                    <option value="01">Single Family</option>
                                                    <option value="04">Condo/Town/Villa</option>
                                                    <option value="03">Multi Family +10</option>
                                                    <option value="08">Multi Family -10</option>
                                                    <option value="11">Commercial</option>
                                                    <option value="00">Vacant Land</option>
                                                    <option value="02">Mobile Home</option>
                                                    <option value="99">Other</option>
                                            </select>
                                        </label>
                                </div>
                                <div class="contentText">
                                	<a href="#" target="_blank" id="buttonSearch" class="buttonblue bigButton">Search</a>
                                </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                            <div class="contentText">
                                	<label class="whitetext">
                                    	Location
                                   	</label><br>
                                    <input style="width:463px;" name="locationSearch" placeholder="Address, City or Zip Code" class="fieldtext" type="text">
                            </div>
                                <div class="contentText">
                                            <label class="whitetext">
                                            Sqft 
                                            </label><br>
                                            <select class="fieldtext" name="sqftSearch" style="width:146px">
                                            	<option value="-1">Any</option>
                                                <option value="250" >250+</option>
                                                <option value="500" >500+</option>
                                                <option value="1000" >1,000+</option>
                                                <option value="1250" >1,250+</option>
                                                <option value="1500" >1,500+</option>
                                                <option value="1750" >1,750+</option>
                                                <option value="2000" >2,000+</option>
                                                <option value="2250">2,250+</option>
                                                <option value="2500">2,500+</option>
                                                <option value="2750" >2,750+</option>
                                                <option value="3000" >3,000+</option>
                                                <option value="3250" >3,250+</option>
                                                <option value="3500" >3,500+</option>
                                                <option value="3750" >3,750+</option>
                                                <option value="4000" >4,000+</option>
                                                <option value="5000" >5,000+</option>
                                                <option value="10000" >10,000+</option>
                                            </select>
                                        </label>
                                </div>
            				<div class="clear"></div>
                            <div>
                                	<div class="contentText">
                                    <label class="whitetext">Sold Range</label><br>
                                    <input class="fieldtext" placeholder="$ Min" name="MinSearch" style="width:146px">
                                    <input class="fieldtext" placeholder="$ Max" name="MaxSearch" style="width:146px">
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Beds</label><br>
                                    <select class="fieldtext" name="bedsSearch" style="width:146px;height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                                    <div class="contentText">
                                    <label class="whitetext">Baths</label><br>
                                    <select class="fieldtext" name="bathSearch" style="width:146px; height: 22px;">
                                    	<option value="-1">Any</option>
                                        <option value="1" >1+</option>
                                        <option value="2" >2+</option>
                                        <option value="3" >3+</option>
                                        <option value="4" >4+</option>
                                        <option value="5" >5+</option>
                                    </select>
                                    </div>
                            </div>
            				<div class="clear">&nbsp;</div>
                        </div>
                        </div>
                        </form>
                        <!-- fin de los tab//-->
                    
                    
            	
                <div style="width:680px; overflow:hidden;">
                <div class="msgLoad">
                </div>
                <div>
                    <ul class="css-tabs" id="xray">
                        <li>
                            <a class="current" href="#panel1">X-Ray Report</a>
                        </li><!--
                        <li>
                            <a class="" href="#panel2">Discount Report</a>
                        </li>//-->
                    </ul>
                </div>
                <div class="panes reports">
                	<div id="panel1" class="panelContenedor">
              		  <div class="title" style="height:30px; margin-bottom:0px;">
                         <div style="margin-left:45px; color:#FFF; font-weight:bold; font-size:13px;">
                            Looking for Real Estate Statistics? You are in the right place.<br>Specific Information for: <span class="selectProptype"></span> in <span class="selectCounty"></span> County, <span class="selectState"></span>. As of <span id="fechaEray"></span>
                          </div>	
                      </div>
                           <table>
                                    <tr>
                                        <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                                        <div style="float:left; margin-left:45px; color:#FFF; text-align:center;">
                                            <label class="whitetext">
                                    State
                                    </label><br>
                                    <select class="shortBox" id="State" name="state">
                                    <?php
                                        $sql="SELECT * FROM lsstate where is_showed='Y';";
                                        $resultado=mysql_query($sql);
                                        while($data=mysql_fetch_assoc($resultado))
                                        {
                                            echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    County
                                    </label><br>
                                    <select class="shortBox County" id="County" name="cunty">
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    Property Type 
                                    </label><br>
                                    <select class="PR_proptype" name="proptype" id="PR_proptype">	
                                            <option value="">Any Property Type</option>
                                            <option value="01">Single Family</option>
                                            <option value="04">Condo/Town/Villa</option>
                                            <option value="03">Multi Family +10</option>
                                            <option value="08">Multi Family -10</option>
                                            <option value="11">Commercial</option>
                                            <option value="00">Vacant Land</option>
                                            <option value="02">Mobile Home</option>
                                            <option value="99">Other</option>
                                    </select>
                                </label>
                                </div>
                                <div style="clear:both"></div>
                                        </th>
                                        <th style="height:30px;padding: 3px 0px 8px 0px;"  class="title">
                                            <span class="whitetext">Quantity</span>
                                        </th>
                                        <th style="height:30px;padding:3px 0px 8px 0px;" class="title">
                                            <span class="whitetext">%</span>&nbsp;&nbsp;&nbsp;
                                        </th>
                                    </tr>
                                </table>
                            <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Properties - Total</span>
                                        </th>
                                        <th>
                                            <span class="greentext" id="total">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties Owner-occupied
                                        </td>
                                        <td>
                                            <span id="ownerY">--</span>
                                        </td>
                                        <td>
                                            <span id="pctownerY">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Properties Non Owner Occupied
                                        </td>
                                        <td>
                                            <span id="ownerN">--</span>
                                        </td>
                                        <td>
                                            <span id="pctownerN">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Distressed Properties
                                        </td>
                                        <td>
                                            <span id="distress">--</span>
                                        </td>
                                        <td>
                                            <span id="pctdistress">--</span>
                                        </td>
                                    </tr>
                                    
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties
                                        </td>
                                        <td>
                                            <span id="preforeclosure">--</span>
                                        </td>
                                        <td>
                                            <span id="pctpreforeclosure">--</span>
                                        </td>
                                    </tr>
                                    
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties
                                        </td>
                                        <td>
                                            <span id="foreclosure">--</span>
                                        </td>
                                        <td>
                                            <span id="pctforeclosure">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties - Not Pre-Foreclosed/Not Foreclosed 
                                        </td>
                                        <td>
                                            <span id="upsidedown">--</span>
                                        </td>
                                        <td>
                                            <span id="pctupsidedown">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties - Active For Sale
                                        </td>
                                        <td>
                                            <span id="sale">--</span>
                                        </td>
                                        <td>
                                            <span id="pctsale">--</span>
                                        </td>
                                    </tr>
                                    
                                </table>
                                <div class="clear">&nbsp;</div> 
                                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Pre-Foreclosed Properties - Total</span>
                                        </th>
                                        <th>
                                            <span id="titlePreforeclosure" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - Active For Sale 
                                        </td>
                                        <td>
                                            <span id="total_sold_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - Not For Sale 
                                        </td>
                                        <td>
                                            <span id="total_no_sold_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - With Negative Equity (Upside Down)  	
                                            </td>
                                        <td>
                                            <span id="total_P_0">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_0">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - 0% equity or more
                                        </td>
                                        <td>
                                            <span id="total_P_0_mas">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_0_mas">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties - 30% equity or more
                                        </td>
                                        <td>
                                            <span id="total_P_30">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_P_30">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Foreclosed Properties - Total</span>
                                        </th>
                                        <th>
                                            <span id="titleforeclosure" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties - Active For Sale 
                                        </td>
                                        <td>
                                            <span id="total_sold_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_F">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties - Not For Sale
                                        </td>
                                        <td>
                                            <span id="total_no_sold_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_F">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Total</span>
                                        </th>
                                        <th>
                                            <span id="titleupsidedown" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Active For Sale 	
                                        </td>
                                        <td>
                                            <span id="total_sold_UD">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_UD">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Upside Down Properties, not Pre-Foreclosed/not Foreclosed - Not For sale
                                        </td>
                                        <td>
                                            <span id="total_no_sold_UD">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_no_sold_UD">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Properties Active For Sale - Total</span>
                                        </th>
                                        <th>
                                            <span id="titlesale" class="greentext">--</span>
                                        </th>
                                        <th>
                                            <span class="whitetext total">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Properties For Sale - 30% + equity      
                                        </td>
                                        <td>
                                            <span id="total_sold_PE">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE">--</span>
                                        </td>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Pre-Foreclosed Properties For Sale - 30% + equity 
                                        </td>
                                        <td>
                                            <span id="total_sold_PE_P">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE_P">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Foreclosed Properties For Sale - 30% + equity         
                                        </td>
                                        <td>
                                            <span id="total_sold_PE_F">--</span>
                                        </td>
                                        <td>
                                            <span id="pcttotal_sold_PE_F">--</span>
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Other Useful Statistics</span>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Total Properties Sold in the last 6 months
                                        </td>
                                        <td>
                                            <span id="total_sold_3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Properties Sold in the last 6 months
                                        </td>
                                        <td>
                                            <span id="a_sold_3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Total Properties Active For Sale
                                        </td>
                                        <td>
                                            <span id="othersale">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Months of inventory
                                        </td>
                                        <td>
                                            <span id="invM">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days on Market
                                        </td>
                                        <td>
                                            <span id="a_days">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 12 months
                                        </td>
                                        <td>
                                            $<span id="m_asp12">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 9 months
                                        </td>
                                        <td>
                                            $<span id="m_asp9">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 6 months
                                        </td>
                                        <td>
                                            $<span id="m_asp6">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Median Sold Price - last 3 months
                                        </td>
                                        <td>
                                            $<span id="m_asp3">--</span>
                                        </td>
                                        <td>
                                            <span id="">--</span>
                                        </td>
                                    </tr>
                                    <tr>
                                 </table>
                    </div>
                    <!--
                    	final primer panel
                    //-->
                    <!--
                    <div id="panel2" class="panelContenedor">
              		  <div class="title" style="height:30px; margin-bottom:0px; border-bottom:solid 2px #FFF;">
                             <div style="margin-left:45px; color:#FFF; font-weight:bold; font-size:13px;">
                                Discount statistics of Foreclosed and Pre-Foreclosed properties.<br>Specific Information for: <span class="selectProptype"></span> in <span class="selectCounty"></span> County, <span class="selectState"></span>. As of <span id="fechaDisc"></span>
                              </div>	
                          </div>
                          <table>
                                    <tr>
                                        <th style="height:30px; padding:3px 0px 8px 0px;" class="title">
                                        <div style="float:left; margin-left:45px; color:#FFF; text-align:center;">
                                            <label class="whitetext">
                                    State
                                    </label><br>
                                    <select class="shortBox" id="State2" name="state2">
                                    <?php
                                        $sql="SELECT * FROM lsstate where is_showed='Y';";
                                        $resultado=mysql_query($sql);
                                        while($data=mysql_fetch_assoc($resultado))
                                        {
                                            echo '<option value="'.$data['IdState'].'">'.$data['State'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    County
                                    </label><br>
                                    <select class="shortBox County" id="County2" name="cunty2">
                                    </select>
                                    </div><div style="float:left; margin-left:15px;color:#FFF; text-align:center;">
                                    <label class="whitetext">
                                    Property Type 
                                    </label><br>
                                    <select class="PR_proptype" name="proptype2" id="PR_proptype2">	
                                            <option value="">Any Property Type</option>
                                            <option value="01">Single Family</option>
                                            <option value="04">Condo/Town/Villa</option>
                                            <option value="03">Multi Family +10</option>
                                            <option value="08">Multi Family -10</option>
                                            <option value="11">Commercial</option>
                                            <option value="00">Vacant Land</option>
                                            <option value="02">Mobile Home</option>
                                            <option value="99">Other</option>
                                    </select>
                                </label>
                                </div>
                                <div style="clear:both"></div>
                                        </th>
                                        <th style="height:30px;padding: 3px 10px 8px 0px;" class="title">
                                            <span class="whitetext">Quantity</span>
                                        </th>
                                    </tr>
                                </table>
                            <div class="clear">&nbsp;</div>                                             
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Pre-Foreclosed Sold</span>
                                        </th>
                                        <th>
                                            <span id="p_sold" class="greentext">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days of Short Sale 
                                        </td>
                                        <td>
                                            <span id="p_avg_day_ss">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Amount of Discount 
                                        </td>
                                        <td>
                                            $<span id="pc_avg_amo_dis">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Percentage of Discount  	
                                            </td>
                                        <td>
                                            <span id="p_avg_pct_dis">--</span>%
                                        </td>
                                    </tr>
                                 </table>
                                <div class="clear">&nbsp;</div> 
                            
                                <table>
                                    <tr>
                                        <th>
                                            <span class="greentext">Foreclosed Sold</span>
                                        </th>
                                        <th>
                                            <span id="f_sold" class="greentext">--</span>
                                        </th>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Days of Sale After File Date 
                                        </td>
                                        <td>
                                            <span id="f_avg_day_safd">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Days of Sale After Judgement Date (Auction)
                                        </td>
                                        <td>
                                            <span id="f_avg_day_sajd">--</span>
                                        </td>
                                    </tr>
                                    <tr class="par">
                                        <td>
                                            <div class="cuote"></div>Average Amount of Discount 
                                        </td>
                                        <td>
                                            $<span id="pcf_avg_amo_dis">--</span>
                                        </td>
                                    </tr>
                                    <tr class="impar">
                                        <td>
                                            <div class="cuote"></div>Average Percentage of Discount
                                        </td>
                                        <td>
                                            <span id="f_avg_pct_dis">--</span>%
                                        </td>
                                    </tr>
                                 </table>
                    </div>//-->
                    <!--
                    	final segundo panel
                    //-->
                </div>
                                 
                                 </div>
                                 <div class="impar" style="padding:15px; font-size:11px; color:#AAA; text-align:justify">
                                 Information at this web site is provided solely for informational purposes and does not constitute an offer to sell, rent, or advertise real estate. Web site owner does not make any warranties or representations concerning any of the information at this web site, which is deemed reliable but is not guaranteed and should be independently verified.
                                 </div>
                                 
                    </div>
                    
        <div class="clear">&nbsp;</div> 
    </div>
    <!--Footer of ReiFax Website-->
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script type="text/javascript">
	menuClick('menu-home');
	function loadCounty(){
		$('.reports .msgLoad').fadeTo(200,0.5);
		$.ajax({
			type	:'POST',
			url		:'resources/php/properties.php',
			data	: "ejecutar=countylist&state="+$('#State').val(),
			dataType:'json',
			success	:function (resul){
				$('.County,#SearchCounty').html('');
				$(resul).each(function (index,data){
					$('.County,#SearchCounty').append('<option value="'+data.id+'">'+data.county+'</option>');
				});
				if(window.loadInt){
				$('.reports .msgLoad').fadeOut(200);
				}
				else{
					IniCounty.init($('#County'),triggerIni);
				};
			}
		})
	};
	function  punticos(valor)
	{
		var punto=-1;
		var respuesta='';
		for(i=0;i<valor.length+1;i++)
		{
			if(punto==3)
			{
				respuesta=valor.charAt(valor.length - i)+','+respuesta;
				punto=1;
			}
			else
			{
				respuesta=valor.charAt(valor.length - i)+respuesta;
				punto++;
			}
		}
		return respuesta;
	}
	var mes=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
	$(document).ready(function (){ 
		loadPanel($('#xray'));
		loadSearch($('#Search'));
		loadCounty();
		$('.County, .PR_proptype').bind('change',refreshData);
		$('#buttonSearch').bind('click',initSearch);
	});
	function initSearch(e)
	{
		e.preventDefault();
		$('body').fadeOut(200);
		var parametrosEnviar='';
		var parametros=[
			{campo: 'search', 	valor : ($('input[name=locationSearch]').val())?$('input[name=locationSearch]').val():''},
			{campo: 'county',	valor : ($('select[name=countySearch]').val())?$('select[name=countySearch]').val():''},
			{campo: 'tsearch', 	valor : 'location'},
			{campo: 'proptype', 	valor : ($('select[name=propertySearch]').val())?$('select[name=propertySearch]').val():''},
			{campo: 'price_low', 	valor : ($('input[name=MinSearch]').val())?$('input[name=MinSearch]').val():''},
			{campo: 'price_hi', 	valor : ($('input[name=MaxSearch]').val())?$('input[name=MaxSearch]').val():''},
			{campo: 'bed', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'bath', 	valor : ($('select[name=bedsSearch]').val())?$('select[name=bedsSearch]').val():-1},
			{campo: 'sqft', 	valor : ($('select[name=sqftSearch]').val())?$('select[name=sqftSearch]').val():-1},
			{campo: 'pequity', 	valor : -1},
			{campo: 'pendes', 	valor : -1},
			{campo: 'search_mapa', 	valor : -1},
			{campo: 'search_type', 	valor : ($('input[name=typeSearch]').val())?$('input[name=typeSearch]').val():''},
			{campo: 'search_mapa', 	valor : '-1'},
			{campo: 'occupied', 	valor : -1}
		]
		$(parametros).each(function (index){
			parametrosEnviar+=this.campo+'='+this.valor+'&';
			});
		$.ajax({
			url		:'/properties_coresearch.php',
			type	:'POST',
			data	:parametrosEnviar,
			success	:function (resul){
				window.location='result/index.php';
				}
		});
		
	}
	function  refreshData(){
			if($(this).is('.County'))
			{
				$('.County').attr('value',$(this).val());
			}
			else
			{
				$('.PR_proptype').attr('value',$(this).val());
			}	
			$('.selectProptype').html($('#PR_proptype option:selected').html());
			$('.selectCounty').html($('#County option:selected').html());
			$('.selectState').html($('#State option:selected').html());
					window.loadInt=true;
					$('.reports .msgLoad').fadeTo(200,0.5);
					$.ajax({
						type	:'POST', 
						url		:'resources/php/properties.php',
						data	: "ejecutar=propertieslist&county="+$('#County').val()+'&propType='+$('#PR_proptype').val(),
						dataType:'json',
						success	:function (resul)
								{
									var fecha=resul.fechaEray.split(' ')[0].split('-');
									fecha=mes[fecha[1]-1]+' '+fecha[2]+', '+fecha[0];
									$('#fechaEray').html(fecha);
									fecha=resul.fechaDisc.split(' ')[0].split('-');
									fecha=mes[fecha[1]-1]+' '+fecha[2]+', '+fecha[0];
									$('#fechaDisc').html(fecha);
									$(resul.data).each(function (index,data){
											var valor=data.valor.split('.');
											$('#'+data.campo).html((data.campo.substring(0,2)=='pc')? number_format(data.valor,2):punticos(valor[0]));
										})
										$('.total').html('100.00');
										$('.reports .msgLoad').fadeOut(200);
								}
						})
			}
		function loadAdvertising (){
			$.ajax({
				url		:'resources/php/advertiser.php',
				type	:'POST',
				data	:'option=list&county='+$('#County').val(),
				dataType:'json',
				success	:function (respuesta){
					var respuesta=(respuesta)?respuesta:new Array();
					var interna=0;
					if($('#County').val()==2){
						var adv_default={
							1:{
								url	:'//transactionalfundingfl.com/how-transactional-funding-works-for-real-estate-investors/',
								img	:'//www.reifax.com/resources/img/advertise/publicidad_xy-1.png'
								},
							2:{
								url	:'//www.reifax.com/register/ddRegister.php',
								img	:'//www.reifax.com/resources/img/advertise/D8.png'
								},
							3:{
								url	:'//www.reifax.com/company/become.php#',
								img	:'//www.reifax.com/resources/img/advertise/D2.png'
								},
							4:{
								url	:'//messenger.providesupport.com/messenger/ximausa.html#" target="_blank"',
								img	:'//www.reifax.com/resources/img/advertise/D3.png'
								},
							5:{
								url	:'//www.reifax.com/company/contactUs.php#',
								img	:'//www.reifax.com/resources/img/advertise/D6.png'
								},
							6:{
								url	:'//www.reifax.com/training/overviewTraining.php#',
								img	:'//www.reifax.com/resources/img/advertise/D5.png'
								},
							7:{
								url	:'#',
								img	:'#'
								}
						}
					}else{
						var adv_default={
							1:{
								url	:'//www.reifax.com/company/become.php#',
								img	:'//www.reifax.com/resources/img/advertise/D2.png'
								},
							2:{
								url	:'//messenger.providesupport.com/messenger/ximausa.html#" target="_blank"',
								img	:'//www.reifax.com/resources/img/advertise/D3.png'
								},
							3:{
								url	:'//www.reifax.com/company/contactUs.php#',
								img	:'//www.reifax.com/resources/img/advertise/D6.png'
								},
							4:{
								url	:'//www.reifax.com/training/overviewTraining.php#',
								img	:'//www.reifax.com/resources/img/advertise/D5.png'
								},
							5:{
								url	:'#',
								img	:'#'
								},
							6:{
								url	:'#',
								img	:'#'
								},
							7:{
								url	:'#',
								img	:'#'
								}
						}
					}
					
					
					for(i=1;i<8;i++)
					{
						if(respuesta[i])
						{
							$('#advertisingFramework').append('<a class="yourAdHere" target="_blank" style="background:url('+respuesta[i].img+') center no-repeat;" href="'+respuesta[i].url+'"></a>');
						}
						else
						{
							interna++;
							$('#advertisingFramework').append('<a class="yourAdHere" style="background:url('+adv_default[interna].img+') center no-repeat;" href="'+adv_default[interna].url+'"></a>');
						}
					}
					
				}
				
			})	
		}
		function triggerIni()
		{
			$('.County,#SearchCounty').attr('value',$('.County:eq(0)').val());
			$('.PR_proptype').attr('value',$('.PR_proptype:eq(0)').val());
			refreshData();
			loadAdvertising();
		}
		function ShowVideo(){
			$('#videoModal').show();
			$('#videoModal').click(function(){
				$('#videoModal').hide();
			});
		}
</script>