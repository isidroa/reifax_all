var panelPrincipal, panelOverview,store, latitude, longitude, marcadorLocation,panel,infowindow,panelMapa,phone,maxDis,panelComparables,panelCompMapaLista;
var width, height, heightImagen, padding,tabla,county,parcelid, reporte, panelRep, countyName, typeprop, comparable,markProperty, minDis;
var markersArray = [];
var infoArray = [];
var pagings = [];
var markersArrayComps = []; 
var pagingsComps = [];
var markComparable, infoWindowComparable,imageComparable;
var tabletphone; //variable para controlar el mapa de comparables
var distance=0.5;
var listdis="margin-right:1em;overflow:visible;-webkit-mask:0 0 url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACBCAYAAAABiRIUAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABRWSURBVHic7Z17bGRXfcc/596587gz43nt+m3vw0tsrO7GTWBXpaGUAAG1aiVKQWoiUNs/aJFoG6l/tBJVK1DpQ5VoK5BK+aNCQkS8VERpKIWUiibNNgmRkqCYmEl2vfbaM36MZ8bz8rzu6R937jAT1ru2x/ZcL+cjXXn92PuY+c7vnN/j/I4Avgg8iUIBD3qAJ6WUn+33nSj6jxDCr/X7JhTuQglC0YUShKILJQhFF0oQii6UIBRdKEEoulCCUHShBKHoQglC0YUShKILJQhFF0oQii6UIBRdKEEoulCCUHShBKHoQglC0YUShKILJQhFF0oQii6UIBRdKEEoulCCUHShBKHoQglC0YUShKILJQhFF0oQii6UIBRdePp9A8eFEGIWiAOTQKj14/tbX+eBBlAEdoBXgReklM3jvs9+c1cKQggxAJxrHRHgDDAEBFrfe1t/OtL6OglYQA1bGFtAWgiRav37OpAHXpVSWsf0GH3hrhGEECIIvAmYw37zZ/iJIM6GQiFhGAaBQACPx37scDgMQKlUQkpJo9HAsizK5TLb29tIKdNAFlsQOeAlIcQPgeeBopSydMyPeeTcNYIATOBtwG8DMSDa+ctQKEQwGCQWi+Hz+QAYGxsDYH19HcuyqFar1Ot1Njc3KRQKSCmHgWHgja3TvAv4MrDc+v6uE4QAfv+kthQSQkSAXwPeii0EL0AsFuP8+fOcO3eOSCRCLBajVqthWRaNRoNms4mUsv3V4/EghEDTNIQQeDweAoEA2WyWUqnE4uIim5ubJJPJzsu/AnwO+LKUcvWYH/1IEEI8eiIFIYTwAn8KPAC8nZalu/fee7nvvvvw+/0IISiVSjQaDWq1Gtlslnq9TrlcptFoAJDL5QB76NA0DY/Hg6ZphEIhYrEYXq8XwzAwTRPDMKjX67z88su8+OKLVKtVqtUqwAut44+llFvH/2ocHidSEEKI+7E7500DjI6OMjo6ypUrV5BSsr29TaFQIJVKsba2RrlcZnNzEynlvq8ViUQIhUIMDg4SDAaZmJggHA5jmibpdJrnnnuOxcVFms0m2JPOR4CklPLHh/jIx4YQ4tETM4cQQowBY9hDw6lEIkEkEmFycpJIJEKxWGRzc7Mtgnw+T6lUol6vH/ia1WoVy7KwLAvDMCgWi5w+fZqREds5mZqawjAMstksa2trfuBh4AdCiB0p5VLvT338uN5CCCE04AL2i30ZeGhiYkKfm5vD6/W2vYKbN2+ysbFBLpej2Ww6n9pDxePxMDAwwKlTpwiHw4yPj+P1eqlWqywsLLCwsABwDfgKthVbkFIeXJHHjOsthBBCBz4A/ALwPmD00qVLTE1NAbCxscGNGzcoFApks9kDDQv7odFosLW1xdbWFj6fj1QqxcTEBPF4nLm5OUzTZH19/Xwqlfpdy7KGgOeEEF+QUhaP9MYOEbeHrnXg/dju3ijApUuXGBoa4tq1aySTSZaXl9na2jpyMbyearXK6uoqyWSSZDKJaZrMzc0xOjqKruuDwEPYw1vo9mdyF662EMCngff6fD6uXLnCvffeSzqd5vHHHyebzfb73gDaFuPmzZu84x3vYHp6mlgsxjPPPDOWz+fHgG8IIX5ZSlnp973uBdfOIYQQHwU+rWkaly9fZnJyknQ6zdNPP912Gw94XjweD7qut13JWq2GlLJnK6NpGmNjY1y5coVCocCPfvQjlpaWAB6TUj7S08mPASHEo64bMoQQMSHEOeA9ABcvXiQQCLC8vMzS0hKWdfBUghACwzAYGBggkUgQi8U4deoUwWAQn8+HEKKne3fc3hs3bqBpGtPT0xiGAXBRCPGAEGKypwscA64ShBAiALwT+BPgVyYnJxkfH6dQKDA/P8+NGzd6EoSu60QiEYLBYDtUbVkWY2Nj7ViDk+c4CFJK8vk88/PzrK6u4vF4uHTpEsBF4M+B9wkhTh34AseA2+YQP489EXv7yMiImJqa4uWXXyafzzvJpp5O7vP5ME2TcrlMJpPBsqwXNE2bc6KUiUSCer3OxsYGjUbjwNcrl8u8+uqrrK6uct999zExMcHy8vI7gQSwKIR4XEpZ6+lhjghXWQjsDOXPAYGRkREMwyCVSjmJpp5P7vF4kFK28xrAC5ZlcfPmTQqFAqZpEo/HHTN/YKSUVCoVtra2WF9fZ2JiAuz52gwwy0/S767DNYIQQrwZ+I1wODw5OjrKPffcw5NPPkm1Wu1pEtmJz+ejVqtRLLbDAo8CvwfUMplMM5VKUSwWueeee9r5jYPOKyzLotlsMj8/TzgcZnZ2ltOnT5vAu7HzMK7EFYIQQoSwJ5EPjo2NcfnyZZ544gkqlcP11JyMZidSys9hxwz+OZfLlRcXF0mlUszMzJBIJAgGgz/1f/ZDtVrl6tWrXLhwgXg8DnAF+KNWKN51uEIQ2KVsk6ZpGpqmkcvl2NnZOc7r3wD+C/g+sF4qlVhbWyORSBCNRvH5fOi6fuCTOzUWQgiCwaCOPWT80uHc+uHSd0EIIQzgD4FfjEajHikli4uLlMvlY7sHKeUi8C3gb4G/KhaL8ysrK5RKJXw+H4ODgyQSiQN7INVqlevXryOlJBKJOIJ41I0eR98FgT3JeigQCEwFAgHK5XK7guk4kVLuAE8B/wR8vNFofH95eZn19XXAToUPDQ3h9Xr3Pa+wLIutrS2KxSKBQIDh4WGAN2MPVa7CDYJ4AxDy+/1ej8dDtVqlVuuPRyalbEopa1LKr2APHxQKBQqFApZlkUgk2h7IfkVRr9epVCp4vV6GhobA9jruv8N/O3bcIIgPApimiWVZbG9vH5pX0QtSyr/ATqx9cWtrq/baa6+RzWaZmZkhEom0q7L2imVZ7fqMYDDo/PhhIYTeSvG7gr4FplpRyQDwq63vqdVqe7YOTk4iHA4TCAQYGBi4ozmPx+Osr6+ztra2p2tIKb8mhHgSu6j24eXl5clCocDs7CzZbJaVlRWKxeKeh7d6vc729jaGYTjBqmHgQewaitf2dJIjpp+RyrPYgRr9zJkz6LpOPp/fc2GL3+8nFosRDoep1+ssLS1RKh1+EbSUck0I8ZfAp4Cv5nK5t129epXx8XHOnj1LKpVie3vbqa+8LU4xj6ZpjIyMsLy8DLYgJC4RRD9NVQyYAnu4EELsK1wcCATQNI1SqdQuoD1CakAZ+AL2mox27eZ+o5vNZpNardbpxk5hvxauoJ8W4gx2+TzhcJhMJrOnT5lDNBptV1O3LMM/YBe67oc9XbBVBlcXQnweWJdSfiSfz1+o1+sXEomECIfD6LrOxsZGu3xvN2HX63WklJ3BrrcAzwohNDesCuunIMLYRbNomoaUcl+uptfrZWdnp9My/P1RF7a21np+UwhxBnhPuVy+4PP52omxfD5PvV6/7bBnWRb1er3zWYexX4vecu+HRF+GjNasegg7BtFeO7Ef70LX9Z7rFw6KlPIzwEeBz2az2cr169dJp9PMzMwwPj6OaZq7hrudHEepVCIajYJdJngKeyFy3+nXHCKEvdQuYJpmewndcQejeqEV3fwz4JPAD9fW1pifn0dKydmzZ4lEIredV1Sr1fbaUmwx/EwLwgSCYHsLdzKzbqW1UuvvsOcBj5dKJZx4heOFOOtIX8/r4hFhYOBYbvoO9EsQeutozx+Ou2r6EGlieyGPYYe+l5zoJtge1K2QUnZ6Gh5ar0e/6dek0kerSMQwjNvOyt1Oa6LZBB4TQqwD9xcKhQ82Go1Z0zRFNBoll8v91PM1m83OIcXXOvpOvyyEp3Wg6/qJFcMt+G/gf4EVZ8K72+Tyda6njkvKGV0TQ79L0Gn1pggEAsKp0dyD4AUucTv7pco6dusems1mT6VqbkEIEQf+FTskf8br9RIKhVhdvXXrCE3TOt3sKnZvq77TL0FUWgfVapVIJHJiBSGEGAIGgW8A5/x+P+Pj4+i6zvLy8q6FPh6Pp7MqrP169Jt+CaL9iXDi+r3ULfaLVsXTu7B7W8Wi0Wi7BjOfz1OpVHYdLnRd74yy7rDHMPpR0y9BFLGbeFUqlUrACf9qmnZiglOt8PVvAh8CZk6dOuUdGxuj2WySyWTIZDK7JuuEEHi9XjY3N50fbbWOvtMXQUgp60KIDeB6s9mcNQwDr9eLruuuF0SrBvS92NVO749EIuecPlaVSoXt7W0ymcyu2VdnvhQIBJxYRQ3Y4GdZEC2KQBqYFUKcpGHDg11JNQuci0QiTExMUK/XuXbtGrVa7bapeGcpgGEYTjFQDSi3ajr7Tj8FsQxcBR7c2dlpL8Tdawq8H8Gs1mLdfwcuGobBuXPniMfjbG9vk0wm97R0wDAMfD5fp/ifBVaO7q73Rz8FcR34HvCxtbU1/H4/gUCg3UT0ThQKBQzDIBgMOp+0GwfwVKJSyjvWUAghzmLXb3wJGB4dHeX06dN4PB6SyST5fH7PmVqv10s4HCafb1/2u7ikWgr6K4ht4CZQKRaLAWceoWnanhJdlUoF0zTbCaJyubyvApu9IoQYxV5V9iYgFI/HSSQSWJZFJpOhUqnsed7jTCZN0+ws90sCmUO/8QPST0FksUXxfKFQeCAUChEIBPB6vXtawueUxodCIQYGBojH4zSbzdvGM/x+P/l8nnQ6fcc3sdXf6t3YQvgt4A2nT5/Wx8bG2NnZaU8enWYje8Hj8WCaJqFQyKmnTGMnxFzTg6pvgmiVi1lCiMeAB3Z2dtqf+Fqtdkcr0Wg0KBQKVKtVDMNgeHgYv9+/698LIYjH42iadseq61ZF+MPAh4GxaDQ6Fo1G2+0PV1ZWaDQa+6rj1DSt3Wd7Z2fHEf13pJR7KwE/JtyQUHkBaJffOxOuOwnCaU1cqVSoVCoYhnFHL8VpErIHAtjrRS6DXb959uzZdkPUgyxCFkK0ayM6Vp+/uO8THTF9F4SU8qoQ4vlSqTRaKpVGTNMkEol0Bm32RCZz52FY07TbLtp9fRg6Ho8zOjqKaZqkUikWFxcPPE8JBoMMDAxQrVa5efMm2MPE5w90siPELY7/J4H/KRQKaJrGxMQEXu/x9tQQQrwRu/rpaeDcmTNnmJubQ9d1FhYWSCaTBxaDx+NhZGQEIUSndfiMG3tj991CAEgpvy6EGCqVSm8rFArDpmkyNDTEysrKoUcudwklfwx7wcyDADMzMwwODvLSSy9RqVQol8s9xTzC4TADAwOsrq6Sy+Vq2MGofzzwCY8Qt1gIsN2vZKlUYn19vb007zBx3tQOT8QQQnwAWwjnnN7ZPp+PdDpNuVzelxdxKzweD7FYjHw+7wSunPiLazyLTlxhIVr8APhmsVi8UCqVRmKxGIODg6yurrYbkPdKvV7H6/Xi9XqdQNKHgUcMw5gdGBhgeHiYQCDAxsYG6XS6p7iGE6JOJBKYpsn169ep1+tF4NvYe2y4UhCusRCtiOErwKqUklwuRywWO9Qch1OM02F5fgeYdRbahMNhqtUqa2trPQe5NE1D07T2RLJUKlGr1YrAAna42pW4yUIA/CetfhGpVGo6HA5z/vx5stksm5ubPbcJqFQq7W72pmmyurp6YXJykunpaba2tvYdht4NZxMWZyOWZ599FuwCmC8DX3Pzbn+ua20shHD2uXoG8E5PT2NZFtlsllwu13NbY8MwiMViBINBBgcHaTQaLC0tUS6X95xHudM1nM1WEokETz31lPOrDwEvSSldF3twcOX2CK2d8NJCiEeALy0sLOgTExOMjIwQDAZZXV098Epvp0fl+vp6ewum7e3tQ+t2p+s6gUCAiYkJKpUKV69edX71N1LKLxzKRY4Y18whbsE88B9A+01zdtXrpf2wg5Sya/+tXtF1nXA4TDQapV6vk8/nHWvzAnaHuxOB6yyEg5RyXgjxCUDP5/MXLMt6w8DAANFoFK/Xu6el97c5N2Dv19mr9+JMeuPxOLFYrN2CMJ/Pp1vW7hNSyid6usgx4mYLgZTyOex2ga8WCgVWVlaQUpJIJPB6vT1bil7F4LiWTqzB6aPdWqmVxrYO/9bTRY4Z11oIBynlZ4QQ38NOl783mUwGRkZGuHjxIsVikXQ6TbFYvG2F81HgDBHDw8NEIhE2NjZ45ZVXnF8/BXz8JFkGB9d5GbvR4X18idYWjYlEgrGxMfx+PysrK2xubh5JkUwnhmEQiUTaWzhmMpnOLjZ54OvYk8iFI72RI8CVXsZudHgfbwL+GvhIJpPR8vm88Hg8TE1NMTIyQiqVolqtUiwWO7veHxinINbpeDc0NNRed5FMJjsX4jwO/AGQk1K6Y/+nA3BiBNFBDdv7aAJTlmVdaTQapzc2NgiHw/j9/q59MZwtlJyGJHdqPdBZFe3s7BsIBNqiKBaLlMtlp51RAXue8Bp2bWQOl6zAOignThCtjUe+JYT4P+C8ZVnvtCxran19/aFCoTAZCoXQdR2Px0MwGGwHo5wqrNs1J3HCzU5ltJNDcTrH7ezsUCgUKBaLW9hJqieA7wDXWh1lTjwnThAOrVqCLSHEj7E7sFytVCpXKpXKNHBK1/Wzfr8/6Cy6dayD82bfyko4jc+cyqpKpUKtVqNcLlcbjcYOdmzkVexE3HXgGSnl+vE99dFzYgXhIKXcxi7W/RchxIvArwNvbDabp0ulUtDpYwn2cGCa5q61l84G77eIhO5gTxivAt+WUn73iB6n75x4QXQipXyeVmNRIUQEe0fgWewy+gtSylipVIruoePtBnZW8rvYGdjvu60Y9qi4qwTRSSud/u3W8Slol9aPYtdN3ootYGsvi3fuVu5aQeyCxDb9u6WfK7hkWX6/+JkSRGstiDPnUNwCV+cyFMePEoSiCyUIRRdKEIoulCAUXShBKLpQglB0oQSh6EIJQtGFEoSiCyUIRRdKEIoulCAUXShBKLpQglB0oQSh6EIJQtGFEoSiCyUIRRdKEIoulCAUXShBKLpQglB0oQSh6EIJQtGFB3jrSd1mWXHovOX/AQCONeHWIOknAAAAAElFTkSuQmCC') no-repeat;-webkit-mask-size:3em;background-color:#003370;background-image:-webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, #0a7aff), color-stop(2%, #004aa3), color-stop(100%, #00234c));background-image:linear-gradient(top, #0a7aff 0%, #004aa3 2%, #00234c 100%);width:3em;height:3em";
var nophoto='/9j/4AAQSkZJRgABAQAAAQABAAD//gA+Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NzApLCBkZWZhdWx0IHF1YWxpdHkK/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAPABKAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldY...yPiOkUvhiLzo1kjW9gZlYZBG7kV0qPhAPaua8fxTXXhZ4reJ5ZfPjIRFyThs1rQt7WN+5nV/hs559O8NS3NhIujWKoC5kUQjDfLxn8aTw1b6enxMEmn2kNtEtqwCRIFGcc9K51U1oIqnTbs4/6ZGtzwPaX0Xiv7RdWc8KGBwGdCBnivZr0YQoyalfTv5ni4adeVf31pc9RureG8t2hmUMjDBzXD6f8KdHMxvLi7vZDI7P5QZVUZOcdM/rXbeZVm2/490+leCe8Lb28VrbR28KBIo1Cqo7AVJS0lAC1XvLuOzt2ml+6oyasU10SRSrqGUjBBGQaAMiKVZIkdTwygj6Gn7vepf7A0c9dLs/+/K/4Un/CPaN/0CrL/vwv+FAEe/3/AFo3+/60/wD4R7Rf+gVZf9+F/wAKP+Ed0X/oE2X/AH4X/CgQzcPWptNvobpGijOWiYo31Bpn/CO6L/0CbL/vwv8AhV22tLazj8u2gihT+7GoUfpQMmpKWkoA/9k=';
Ext.setup({
	phoneStartupScreen: 'phone_startup.png',
	icon: 'icon.png',
	phoneIcon: 'iconphone.png',
	onReady: function(){
		width=screen.width;
		height=screen.height;
		if(width<=500){
			padding=20;
			tabla=width-20;
			widthImagen=width-25;
			heightImagen=height-150;
		}else if(width>500 && width<=1000){
			padding=200;
			tabla=width-200;
			widthImagen=width-200;
			heightImagen=height-400;
		}else{
			padding=document.body.offsetWidth/4;
			tabla=document.body.offsetWidth/2;
			widthImagen=document.body.offsetWidth/2;
			heightImagen=height-250;
		}
		var functionsTemplates={
							numberFormat: function(num,prefix,mostrar){
								var verifica;
								if(mostrar=='N' && Ext.getCmp('search_properties').getValue()=='PR'){
									verifica=false;
								}else{
									verifica=true;
								}
								if(verifica==true){
									num +='';
									var splitStr = num.split('.');
									var splitLeft = splitStr[0];
									var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
									var regx = /(\d+)(\d{3})/;
									while (regx.test(splitLeft)) {
										splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
									}
									return prefix+splitLeft + splitRight.substring(0,3);
								}else{
									return '';
								}		
							},
							typeProperty: function(val){
								if(val=='RE1'){
									return 'SINGLE FAMILY RESIDENTIAL';
								}else{
									return 'CONDOMINIUM RESIDENTIAL';
								}
							},
							dateFormat: function(date){
								return date.substring(4,6)+'/'+date.substring(6,8)+'/'+date.substring(0,4);
							},
							evaluaData: function(val){
								if(val=='' || val==null || val=='null'){
									return false;
								}else{
									return true;
								}
							},
							typeForeclosure: function(val){
								if(val=='F'){
									return 'Foreclosed';
								}else if(val=='P'){
									return 'Pre-Foreclosed';
								}else{
									return '';
								}
							},
							soldType: function(val){
								if(val=='S'){
									return 'Sold';
								}else{
									return '';
								}
							},
							verificaType: function(val){
								if(comparable=='distress'){
									return false;
								}else{
									return true;
								}
							},
							priceTitle: function(val){
								if(comparable=='comps'){
									return 'Sold Price';
								}else{
									return 'Listing Price';
								}
							},
							verificaPriceComps: function(val){
								if(comparable=='comps'){
									return true;
								}else{
									return false;
								}
							},
							verificaPriceListing: function(val){
								if(comparable=='comps'){
									return false;
								}else{
									return true;
								}
							},
							dateTitle: function(val){
								if(comparable=='comps'){
									return 'Sold Date';
								}else{
									return 'Listing Date';
								}
							},
							verificaStatus: function(val){
								if(val=='A'){
									return true;
								}else{
									return false;
								}
							},
							evaluaValor: function(val){
								if(val>0){
									return '#60b31b';
								}else if(val<0){
									return '#DF0101';
								}else{
									return '#15428b';
								}	
							},
							verificaTablet: function(val){
								if(tabletphone && width>500){
									return false;
								}else{
									return true;
								}
							},
							verificaImagen: function(val){
								if(val==''){
									//return '/9j/4AAQSkZJRgABAQAAAQABAAD//gA+Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NzApLCBkZWZhdWx0IHF1YWxpdHkK/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAPABKAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9/ooooAKCcUdK4Dxp4zitIJLS1k+X7rup5c/3R7epoA1r/xla2uppbx7ZIlOHbPLf7v0/Wukt7iK6gSaFw8bjKsO9fOBg1XVFfUo8gJyi/3h7en9a6vwt40vIIGgikAk6tE46n1GehoA9porzePx5ey7tjxkqcEbBwa6vwxqtzq9pNPcDgPtQhcA+tAG7RRRQAUmcUtcD4y8awWkU1laSjKEpKynkn+6P6mgA8aeM4rOCS1tZOPuu6nlj/dX29TXnWiaLe+LdTWWVT9nzwOxH+H8/p1TRtHvfFuprJIh+z54HYj/AA/nXtel6ZZ6BYKm6NOgaRiACaBCWHh6ystM+x+UrBhhyR1ry7xn4MuNKuzqFgDjOfl/i/8Ar/zr1/8AtGx/5/Lf/v6v+NMlew1KJrYzQTBh91XBNAzxzw14hBm3kDzuksf98ev1FeueHZo59JWWI5RpHI/OvKPGXg640m8N/YA4zn5f4v8A6/8AOr3gvxwLQeVdN+6P+sT0/wBof1FAj1+kpkM0c8KTROHjdQysOhB70+gYtcXrnw00bWrmW5866tZpCWbyWG0k9Tgg12lJQBg+HNGttE0mC3hGWVApcjk4GKyPiOkUvhiLzo1kjW9gZlYZBG7kV0qPhAPaua8fxTXXhZ4reJ5ZfPjIRFyThs1rQt7WN+5nV/hs559O8NS3NhIujWKoC5kUQjDfLxn8aTw1b6enxMEmn2kNtEtqwCRIFGcc9K51U1oIqnTbs4/6ZGtzwPaX0Xiv7RdWc8KGBwGdCBnivZr0YQoyalfTv5ni4adeVf31pc9RureG8t2hmUMjDBzXD6f8KdHMxvLi7vZDI7P5QZVUZOcdM/rXbeZVm2/490+leCe8Lb28VrbR28KBIo1Cqo7AVJS0lAC1XvLuOzt2ml+6oyasU10SRSrqGUjBBGQaAMiKVZIkdTwygj6Gn7vepf7A0c9dLs/+/K/4Un/CPaN/0CrL/vwv+FAEe/3/AFo3+/60/wD4R7Rf+gVZf9+F/wAKP+Ed0X/oE2X/AH4X/CgQzcPWptNvobpGijOWiYo31Bpn/CO6L/0CbL/vwv8AhV22tLazj8u2gihT+7GoUfpQMmpKWkoA/9k=';
									return 'img/nophotocasa.jpg';
								}else{
									return val;
								}
							},
							verificaFont: function(val){
								if(tabletphone && width>500){
									return 16;
								}else{
									return 13;
								}
							}
						}
		var imageLocation = new google.maps.MarkerImage(
				'img/man_location.png',
				new google.maps.Size(32, 31),
				new google.maps.Point(0,0),
				new google.maps.Point(16, 31)
		)
		imageComparable = new google.maps.MarkerImage(
				'img/verdetotal.png',
				new google.maps.Size(32, 31),
				new google.maps.Point(0,0),
				new google.maps.Point(16, 31)
		)
		var devuelveIcon=function(stat,pendes){
			var imagen='';
			if(stat=='A'){
				imagen='verde';
			}else if(stat=='CC'){
				imagen='cielo';
			}else if(stat=='CS'){
				imagen='marron';
			}else{
				imagen='gris';
			}
			if((pendes=='F' || pendes=='P') && login){
				imagen+='l.png';
			}else{
				imagen+='b.png';
			}
			return imagen;
		}
		var panelLoading = new Ext.Panel({
			id: 'panelLoading',
			hideOnMaskTap: false,
			floating: true,
			modal: true,
			centered: true,
			width: 250,
			height: 50,
			html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
		});
		var toolbarOrder = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Apply',
					handler: function(){
						store.proxy.extraParams.orderby=Ext.getCmp('order_by').getValue();
						store.proxy.extraParams.orderdirection=Ext.getCmp('order_direction').getValue();
						store.loadPage(1);
						Ext.getCmp('orderBy').hide();
					}
				},{
					text: 'Cancel',
					handler: function(){
						Ext.getCmp('orderBy').hide();
					}
				}
			]
		});
		var orderBy = new Ext.Panel({
			id: 'orderBy',
			hideOnMaskTap: false,
			floating: true,
			modal: true,
			centered: true,
			width: 350,
			height: 200,
			dockedItems:[toolbarOrder],
			items: [
						{
							xtype: 'selectfield',
							name: 'order_by',
							id: 'order_by',
							label: 'Order By',
							options:[
								{text: 'County',  value: 'p.county,p.address'},
								{text: 'Address',  value: 'p.address'},
								{text: 'Mlnumber',  value: 'mlnumber'},
								{text: 'Parcel ID',  value: 'p.parcelid'},
								{text: 'Year Built',  value: 'mlsresidential.yrbuilt'},
								{text: 'DOM',  value: 'dom'},
								{text: 'Living Area',  value: 'larea'},
								{text: 'Gross Area',  value: 'garea'},
								{text: 'Lot Size',  value: 'tsqft'},
								{text: 'Water Front',  value: 'waterf'},
								{text: 'Pool',  value: 'pool'},
								{text: 'Beds',  value: 'mlsresidential.beds'},
								{text: 'Baths',  value: 'mlsresidential.bath'},
								{text: 'City',  value: 'p.city'},
								{text: 'Zip Code',  value: 'p.zip'},
								{text: 'Price',  value: 'price'},
								{text: 'Property Types',  value: 'p.xcoded'},
								{text: 'Foreclosure',  value: 'marketvalue.pendes'},
								{text: 'Market Value',  value: 'marketvalue.marketvalue'},
								{text: 'Active Value',  value: 'marketvalue.offertvalue'},
								{text: 'Potential Equity',  value: 'marketvalue.marketpctg'},
								{text: 'Active Equity',  value: 'marketvalue.offertpctg'},
								{text: 'Debt to Equity',  value: 'marketvalue.debttv'}
							]
						},{
							xtype: 'selectfield',
							name: 'order_direction',
							id: 'order_direction',
							label: 'Direction',
							options:[
								{text: 'ASC',  value: 'ASC'},
								{text: 'DESC',  value: 'DESC'}
							]
						}	
					]
		});		
		var searchByLocation = function(lat,lon){
			var state = Ext.getCmp('search_state').getValue();
			var county = Ext.getCmp('search_county').getValue();
			var search_type=Ext.getCmp('search_type').getValue(); 
			var searchlocation;
			var searchtext;
			var module='location';
			store.proxy.extraParams = {module:module,state:state,county:county,longitude:lon,searchlocation:searchlocation,latitude:lat,search_type:search_type,orderby:'',orderdirection:''};
			store.currentPage=1;
			store.load();
		}
		var success_callbackLocation=function(p){
			latitude=p.coords.latitude;
			longitude=p.coords.longitude;
			if(marcadorLocation!=null){
				marcadorLocation.setMap(null);
			}
			//TOMO OTRA LATITUD
			if(latitude==7.765374 || latitude==null){
				latitude=26.003146;
				longitude=-80.223937;
			}
			marcadorLocation = new google.maps.Marker({
				position: new google.maps.LatLng(latitude,longitude),
				title : 'Location',
				icon  : imageLocation,
				map: mapPanel.map
			});
			mapPanel.map.setCenter(new google.maps.LatLng(latitude,longitude));
			mapPanel.map.setZoom(12);		
			Ext.Ajax.request({
				url:'php/prueba1.php?latitude='+latitude+'&longitude='+longitude,
				method: 'POST',
				success:function(response, opts){
					var obj=Ext.decode(response.responseText);
					Ext.getCmp('search_county').setValue(obj.idcounty);
					Ext.getCmp('search_properties').setValue('FS');
					Ext.getCmp('equity_public').setVisible(false);
					Ext.getCmp('search_text').setValue('My location');
					Ext.getCmp('search_type').setValue('01');
					searchByLocation(latitude,longitude);
				}	
			});
		}
		var error_callbackLocation=function(p){
			Ext.Msg.alert('Error', p.message, Ext.emptyFn);
		}
		var getLocationGps=function(){
			if(geo_position_js.init()){
				geo_position_js.getCurrentPosition(success_callbackLocation,error_callbackLocation,{enableHighAccuracy:true,options:5000});
			}
			else{
				//Ext.Msg.alert("Error","Functionality not available");
				alert('Location Functionality not available');
			}
		}
		
		function attachMessage(marker, content) {
			google.maps.event.addListener(marker, 'click', function() {
				if(infowindow!=null){
					infowindow.close();
				}	
				infowindow = new google.maps.InfoWindow({
					content: content
				});
				infowindow.open(mapPanel.map,marker);
			});
			  
		}
		function attachMessageComp(marker, content) {
			google.maps.event.addListener(marker, 'click', function() {
				if(infoWindowComparable!=null){
					infoWindowComparable.close();
				}	
				infoWindowComparable = new google.maps.InfoWindow({
					content: content
				});
				infoWindowComparable.open(mapComparables.map,marker);
			});
			  
		}
		var actualizaMapa= function(){
			county=Ext.getCmp('search_county').getValue();
			if(phone){
				panel.items.items[0].flex=1;
				panel.items.items[1].flex=0;
				panel.doLayout();
				Ext.getCmp('buttonmap').setVisible(false);
				Ext.getCmp('buttonlist').setVisible(true);
			}
			if (markersArray && markersArray.length > 0) {
				for(j=0; j< markersArray.length; j++){
					markersArray[j].setMap(null);
				}
				markersArray = [];
			}
			var zoom=15;
			pagings = [];
			Ext.getCmp('paging').setOptions(pagings);
			Ext.getCmp('paging').setValue('');
			Ext.getCmp('paging').getListPanel().setHeight(0);
			if(store.getCount()>0){
				var latAcum=0;
				var lonAcum=0;
				maxDis=0;minDis=0;
				for(i=0; i<store.getCount(); i++){
					var latLng = new google.maps.LatLng(store.data.items[i].data.latitude, store.data.items[i].data.longitude);
					latAcum= latAcum+parseFloat(store.data.items[i].data.latitude);
					lonAcum= lonAcum+parseFloat(store.data.items[i].data.longitude);
					if(Ext.getCmp('search_text').getValue()=='My location'){
						if(store.data.items[i].data.distancia>maxDis){
							maxDis=store.data.items[i].data.distancia;
						}
					}else{
						if(i==0){
							maxDis=store.data.items[i].data.distancia;
							minDis=store.data.items[i].data.distancia;
						}else{
							if(maxDis<store.data.items[i].data.distancia){
								maxDis=store.data.items[i].data.distancia;
							}
							if(minDis>store.data.items[i].data.distancia){
								minDis=store.data.items[i].data.distancia;
							}
						}
					}
					var urlIcon=devuelveIcon(store.data.items[i].data.status,store.data.items[i].data.pendes);
					//alert(urlIcon);
					var markerProp = new google.maps.MarkerImage(
							'img/houses/'+urlIcon,
							new google.maps.Size(32, 31),
							new google.maps.Point(0,0),
							new google.maps.Point(16, 31)
					)
					var marker = new MarkerWithLabel({
						map: mapPanel.map,
						position: latLng,
						icon: markerProp,
						labelContent: i+1,
						labelAnchor: new google.maps.Point(12, 25),
						labelClass: "labels"
					});
					markersArray.push(marker);
					var imagen=store.data.items[i].data.imagen;
					var img = '';
					img=functionsTemplates.verificaImagen(imagen);
					var content='<div style="width:200px;">'+
								'<div style="float:left; margin-right:20px; height:60px; _height:70px; width:200px;overflow:hidden;">'+
								'	<img src="'+img+'" height="60px" width="70px;" style="position:relative; z-index:150;">'+
								'	<div style="float:right;"><div style="'+listdis+'" onClick="javascript:llamaStore(\''+i+'\')"></div></div>'+
								'</div>'+	
								'<div style="font-size: 10px;">'+
								'	<span style="color:#60b31b; font-weight:bold; font-size:13px;">'+
								'		'+functionsTemplates.numberFormat(store.data.items[i].data.price,"$","N")+
								'	</span>'+
								'	<span style="color:#045FB4; font-weight:bold; font-size:13px;">'+
								'		'+store.data.items[i].data.address+
								'	</span>'+
								'</div>'+
								'<div style="font-size: 11px;">'+
								'	'+store.data.items[i].data.city+', '+ store.data.items[i].data.state+', '+ store.data.items[i].data.zip+
								'</div>'+
								'<div style="font-size: 11px;">'+
								'	'+store.data.items[i].data.beds+'/'+store.data.items[i].data.bath+', '+ store.data.items[i].data.larea+' sqft'+
								'</div>'+
								'<div style="font-size: 11px;">'+
								'	<span style="color:#60b31b; font-weight:bold; font-size:13px;">Rei</span><span style="color:#045FB4; font-weight:bold; font-size:13px;">Fax</span> Value: '+functionsTemplates.numberFormat(store.data.items[i].data.marketvalue,"$","Y")+
								'</div></div>';
					attachMessage(marker, content);
					
				}
				
				//Paginacion
				var totalRegistros=store.proxy.reader.jsonData.total;
				var pages=totalRegistros/store.pageSize;
				if(pages<=1){
					pages=0;
				}
				pagings = [];
				var tam=store.pageSize;
				for(j=0; j<=pages; j++){
					var desde=(j*tam)+1;
					var hasta=(j*tam)+tam;
					if(hasta>totalRegistros){
						hasta=totalRegistros;
					}
					//pagings.push({text: desde+' to '+hasta,value: j+1});
					pagings.push({text: 'Page '+(j+1),value: j+1});
				}
				Ext.getCmp('paging').setOptions(pagings);
				var sizeList=0;
				if(pages<=6){
					sizeList=pages*50;
				}else{
					sizeList=300;
				}	
				Ext.getCmp('paging').getListPanel().setHeight(sizeList);
				Ext.getCmp('paging').setValue(store.currentPage);
				//Centrado del mapa
				var latAvg= latAcum/store.getCount();
				var lonAvg= lonAcum/store.getCount();
				var aumentar;
				if(phone){
					aumentar=1;
				}else{
					aumentar=0;
				}
				if(Ext.getCmp('search_text').getValue()=='My location'){
					if(latitude==7.765374 || latitude==null){
						latitude=26.003146;
						longitude=-80.223937;
					}
					var pnt = new google.maps.LatLng(latitude, longitude);
					mapPanel.map.setCenter(pnt);
					if(maxDis<0.1){
						zoom=18-aumentar;
					}else if(maxDis>=0.1 && maxDis<0.17){
						zoom=17-aumentar;
					}else if(maxDis>=0.17 && maxDis<0.35){
						zoom=16-aumentar;
					}else if(maxDis>=0.35 && maxDis<0.70){
						zoom=15-aumentar;
					}else if(maxDis>=0.70 && maxDis<1.5){
						zoom=14-aumentar;
					}else if(maxDis>=1.5 && maxDis<2.5){
						zoom=13-aumentar;
					}else if(maxDis>=2.5 && maxDis<5){
						zoom=11-aumentar;
					}else{
						zoom=8-aumentar;
					}
				}else{
					var pnt = new google.maps.LatLng(latAvg, lonAvg);
					mapPanel.map.setCenter(pnt);
					//maxDis=maxDis-minDis;
					var latinicial= store.data.items[0].data.latitude;
					var loninicial= store.data.items[0].data.longitude;
					var latfinal= store.data.items[store.getCount()-1].data.latitude;
					var lonfinal= store.data.items[store.getCount()-1].data.longitude;
					maxDis=Math.sqrt((69.1* (latfinal- latinicial))*(69.1*(latfinal-latinicial))+(69.1*((lonfinal-(loninicial))*Math.cos(latinicial/57.29577951)))*(69.1*((lonfinal-(loninicial))*Math.cos(latinicial/57.29577951))));
					if(maxDis<0.1){
						zoom=18-aumentar;
					}else if(maxDis>=0.1 && maxDis<0.17){
						zoom=17-aumentar;
					}else if(maxDis>=0.17 && maxDis<0.35){
						zoom=16-aumentar;
					}else if(maxDis>=0.35 && maxDis<0.70){
						zoom=15-aumentar;
					}else if(maxDis>=0.70 && maxDis<1.5){
						zoom=14-aumentar;
					}else if(maxDis>=1.5 && maxDis<2.5){
						zoom=13-aumentar;
					}else if(maxDis>=2.5 && maxDis<5){
						zoom=12-aumentar;
					}else if(maxDis>=5 && maxDis<10){
						zoom=11-aumentar;
					}else{
						zoom=8-aumentar;
					}
				}
				if(totalRegistros==1){
					zoom=18;
				}
			}
			mapPanel.map.setZoom(zoom);
				
		};
		//Panel Login
		var formLogin = new Ext.Panel({
			id: 'formLogin',
			floating: true,
			modal: true,
			centered: true,
			width: 320,
			height: 220,
			scroll: 'vertical',
			items: [
				{
					xtype: 'fieldset',
					title: 'Sign in',
					defaults: {
						labelAlign: 'left'
					},
					items: [
						{
							xtype: 'textfield',
							name: 'email',
							id: 'email',
							label: 'Email'
						},
						{
							xtype: 'passwordfield',
							name: 'password',
							id: 'password',
							label: 'Password'
						}
					]
				},
				{
					xtype: 'button',
					text: 'Sign In',
					ui: 'action',
					handler: function() {
						var email=Ext.getCmp('email').getValue();
						var password=Ext.getCmp('password').getValue();
						Ext.Ajax.request({
							url:'php/login.php?email='+email+'&password='+password,
							method: 'POST',
							success:function(response, opts){
								var obj=Ext.decode(response.responseText);
								var login=obj.login;
								if(login=='true'){
									Ext.getCmp('formLogin').hide();
									window.location.href='/mobile/app/';
								}else{
									alert('Your email or password is incorrect');
								}
							}	
						});
					}
				}
			]
		});
		//Panel leyenda casas
		var panelLegend = new Ext.Panel({
			id: 'panelLegend',
			floating: true,
			modal: true,
			centered: true,
			width: 300,
			height: 300,
			scroll: 'vertical',
			html: '<table style="width:300">'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;">Color</td>'+
					'		<td align="center">Description</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/verdetotal.png"></td>'+
					'		<td>Subject</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/verdeb.png"></td>'+
					'		<td>Active</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/verdel.png"></td>'+
					'		<td>Active Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/verdel.png"></td>'+
					'		<td>Active Pre-Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/cielob.png"></td>'+
					'		<td>By Owner</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/cielol.png"></td>'+
					'		<td>By Owner Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/cielol.png"></td>'+
					'		<td>By Owner Pre-Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/marronb.png"></td>'+
					'		<td>Closed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center"><img src="img/houses/marronl.png"></td>'+
					'		<td>Closed Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center"><img src="img/houses/marronl.png"></td>'+
					'		<td>Closed Pre-Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/grisb.png"></td>'+
					'		<td>Non-Active</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/grisl.png"></td>'+
					'		<td>Non-Active Forclosed</td>'+
					'	</tr>'+
					'	<tr>'+
					'		<td align="center" style="width:80; height:30;"><img src="img/houses/grisl.png"></td>'+
					'		<td>Non-Active Pre-Forclosed</td>'+
					'	</tr>'+
					'</table>'
		});
		//toolbar panel
		var toolbarTop = new Ext.Toolbar({
			dock : 'top',
			items: [
				{
					iconMask: true,
					iconCls:'locate',
					handler: getLocationGps
				},{
					text: 'Login',
					hidden: login,
					handler: function(){
						Ext.getCmp('formLogin').show('pop');
					}
				},{
					text: 'Log off',
					hidden: !login,
					handler: function(){
						Ext.Ajax.request({
							url:'php/logoff.php',
							method: 'POST',
							success:function(response, opts){
								var obj=Ext.decode(response.responseText);
								var login=obj.login;
								if(login=='true'){
									window.location.href='/mobile/app1/';
								}
							}	
						});
					}
				},{
					xtype:'spacer'
				},{
					xtype: 'selectfield',
					id:'paging',
					name: 'paging',
					width: 160,
					height: 80,
					options: pagings,
					listeners:{
						change: function(select, value){
							store.loadPage(value);
						}
					}	
				}
			]
		});
		//tool bar panel mapa
		var toolbarBottom = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					iconMask: true,
					iconCls:'search',
					handler: function(){
						panelPrincipal.setActiveItem(1);
					}
				},{
					id: 'sort',
					iconMask: true,
					iconCls: 'sortbutton',
					handler: function(){
						Ext.getCmp('orderBy').show('pop');
					}
				},{
					id: 'home',
					iconMask: true,
					iconCls: 'home',
					handler: function(){
						Ext.getCmp('panelLegend').show('pop');
					}
				},{
					iconMask: true,
					iconCls:'maps',
					id: 'buttonmap',
					hidden: true,
					handler: function(){
						panel.items.items[1].flex=0;
						panel.items.items[0].flex=1;
						panel.doLayout();
						Ext.getCmp('buttonmap').setVisible(false);
						Ext.getCmp('buttonlist').setVisible(true);
					}
				},{
					iconMask: true,
					id: 'buttonlist',
					iconCls:'info',
					hidden: true,
					handler: function(){
						panel.items.items[0].flex=0;
						panel.items.items[1].flex=1;
						panel.doLayout();
						Ext.getCmp('buttonmap').setVisible(true);
						Ext.getCmp('buttonlist').setVisible(false);
					}
				}
			]
		});
		mapPanel=new Ext.Map({
			id:'map',
			title:'Map',
			useCurrentLocation: false,
			flex: 2
		});
		Ext.regModel('Properties', {
			 fields: [
                {name: 'parcelid', type: 'string'},
                {name: 'address', type: 'string'},
				{name: 'city', type: 'string'},
				{name: 'zip', type: 'string'},
				{name: 'latitude', type: 'string'},
				{name: 'longitude', type: 'string'},
				{name: 'bath', type: 'int'},
				{name: 'beds', type: 'int'},
				{name: 'price', type: 'float'},
				{name: 'xcoded', type: 'string'},
				{name: 'mlnumber', type: 'string'},
				{name: 'larea', type: 'float'},
				{name: 'garea', type: 'float'},
				{name: 'tsqft', type: 'float'},
				{name: 'county', type: 'string'},
				{name: 'yrbuilt', type: 'int'},
				{name: 'waterf', type: 'string'},
				{name: 'dom', type: 'int'},
				{name: 'pool', type: 'string'},
				{name: 'state', type: 'string'},
				{name: 'imagenxima', type: 'string'},
				{name: 'imagenowner', type: 'string'},
				{name: 'imagen', type: 'string'},
				{name: 'distancia', type: 'float'},
				{name: 'status', type: 'string'},
				{name: 'pendes', type: 'string'},
				{name: 'marketvalue', type: 'float'},
				{name: 'tieneImg', type: 'string'}
            ]
		});

		store = new Ext.data.Store({
			model  : 'Properties',
			pageSize: 50,
			currentPage: 1,	
			proxy: {
                type: 'ajax',
                url: 'php/response_grid.php',
				timeout: 600000,
				extraParams: {
		            module: '',
					state: '',
					county: '',
					longitude: '',
					searchlocation: '',
					latitude: '',
					search_text:'',
					search_type:'',
					search_properties:''
		        },
				reader: {
                    root: 'results',
					totalCount: 'total'
                }
            }
		});
		store.addListener('load', actualizaMapa);
		//store.load({params:{module1:'otro'}});
		var propertytemplate=new Ext.XTemplate(
						'<tpl for=".">',
						'<div style="float:left; margin-right:20px; width:70px;overflow:hidden;">',
						'	<img src="{imagen:this.verificaImagen}" height="60px" width="70px;" style="position:relative; z-index:150;">'+
						'</div>',
						'<div style="font-size: 15px;">',
						'		 {#} - <span style="color:#60b31b; font-weight:bold; font-size:15px;">',
						'			{price:this.numberFormat("$","N")}',
						'		</span>',
						'</div>',
						'<div style="font-size: 15px;">',
						'		 <span style="color:#045FB4; font-weight:bold; font-size:15px;">',
						'			{address}',
						'		</span>',
						'</div>',
						'<div style="font-size: 11px;">',
						'		 {city}, {state}, {zip}',
						'</div>',
						'<div style="font-size: 11px;">',
						'	{beds}/{bath}, {larea} sqft',
						'</div>',
						'<div style="font-size: 13px;">',
						'	<span style="color:#60b31b; font-weight:bold; font-size:13px;">Rei</span><span style="color:#045FB4; font-weight:bold; font-size:13px;">Fax</span><span style="font-weight:bold; font-size:13px;"> Value: {marketvalue:this.numberFormat("$","Y")}</span>',
						'</div>',
						'</tpl>',
						functionsTemplates		
					);
		
		var listPanel = new Ext.Panel({
            flex: 1,
			layout: 'fit',
            scroll: 'vertical',
            items: [
                {
                    xtype: 'list',
                    id:'listproperties',
					itemTpl: propertytemplate,
					store: store,
					singleSelect: true,
                    emptyText:'No properties to display',
					onItemDisclosure: true,
					listeners:{
						itemtap:function(list,index,item, even){
							if(infowindow!=null){
									infowindow.close();
								}
							var rec=store.getAt(index);
							var imagenxima=rec.get('imagenxima');
							var imagenowner=rec.get('imagenowner');
							var imagen=rec.get('imagen');
							var img = '';
							/*if(imagenxima!=''){
								img=imagenxima;
							}else if(imagenowner!=''){
								img=imagenowner;
							}else if(imagen!=''){
								img=imagen;
							}else{
								img='img/nophotocasa.jpg';
							}*/
							img=functionsTemplates.verificaImagen(imagen);
							var content='<div style="width:200px">'+
										'<div style="float:left; margin-right:20px; width:200px;overflow:hidden;">'+
										'	<img src="'+img+'" height="60px" width="70px;" style="position:relative; z-index:150;">'+
										'	<div style="float:right;"><div style="'+listdis+'" onClick="javascript:llamaStore(\''+index+'\')"></div></div>'+
										'</div>'+  
										'<div style="font-size: 10px;">'+
										'	<span style="color:#60b31b; font-weight:bold; font-size:13px;">'+
										'		'+functionsTemplates.numberFormat(rec.get('price'),"$","N")+
										'	</span>'+
										'	<span style="color:#045FB4; font-weight:bold; font-size:13px;">'+
										'		'+rec.get('address')+
										'	</span>'+
										'</div>'+
										'<div style="font-size: 11px;">'+
										'	'+rec.get('city')+', '+ rec.get('state')+', '+ rec.get('zip')+
										'</div>'+
										'<div style="font-size: 11px;">'+
										'	'+rec.get('beds')+'/'+rec.get('bath')+', '+ rec.get('larea')+' sqft'+
										'</div>'+
										'<div style="font-size: 11px;">'+
										'	<span style="color:#60b31b; font-weight:bold; font-size:13px;">Rei</span><span style="color:#045FB4; font-weight:bold; font-size:13px;">Fax</span> Value: '+functionsTemplates.numberFormat(rec.get('marketvalue'),'$',"Y")+
										'</div></div>';
							infowindow = new google.maps.InfoWindow({
								content: content
							});
							
							infowindow.open(mapPanel.map,markersArray[index]);
							if(phone){
								panel.items.items[1].flex=0;
								panel.items.items[0].flex=1;
								panel.doLayout();
								Ext.getCmp('buttonmap').setVisible(false);
								Ext.getCmp('buttonlist').setVisible(true);
							}
							
						},
						disclose: function(record, node, index,e){
							//var record=store.getAt(index);
							panelOverview.items.get(0).update(null);
							panelOverview.items.get(1).update(null);
							Ext.Ajax.request({
								url:'php/response_data.php?type=public&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
								method: 'POST',
								success:function(response, opts){
									var data = Ext.decode(response.responseText);
									typeprop=data.ccode;
									panelOverview.items.get(0).update(data);
									Ext.getCmp('panelPropComp').update(data);
									if(markComparable!=null){
										markComparable.setMap(null);
									}
									markComparable = new google.maps.Marker({
										position: new google.maps.LatLng(record.get('latitude'),record.get('longitude')),
										title : 'Comparable',
										icon  : imageComparable,
										map: Ext.getCmp('mapComp').map
									});
									if(markProperty!=null){
										markProperty.setMap(null);
									}
									markProperty = new google.maps.Marker({
										position: new google.maps.LatLng(record.get('latitude'),record.get('longitude')),
										title : 'Overview',
										icon  : imageComparable,
										map: Ext.getCmp('mapOverview').map
									});
									Ext.getCmp('mapOverview').map.setCenter(new google.maps.LatLng(record.get('latitude'),record.get('longitude')));
									Ext.getCmp('mapOverview').map.setZoom(22);
									Ext.getCmp('mapOverview').map.setMapTypeId(google.maps.MapTypeId.HYBRID);
								}	
							});
							Ext.Ajax.request({
								url:'php/response_data.php?type=listing&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
								method: 'POST',
								success:function(response, opts){
									var data = Ext.decode(response.responseText);
									panelOverview.items.get(1).update(data);
								}	
							});
							if(login){
								Ext.getCmp('mortfore').setVisible(false);
								Ext.Ajax.request({
									url:'php/response_data.php?type=mortfore&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
									method: 'POST',
									success:function(response, opts){
										var data = Ext.decode(response.responseText);
										if(data.pendes.length>0){
											Ext.getCmp('mortfore').setText('Foreclosure');
											Ext.getCmp('mortfore').setVisible(true);
										}else if(data.mortgage.length>0){
											Ext.getCmp('mortfore').setText('Mortgage');
											Ext.getCmp('mortfore').setVisible(true);
										}	
										panelOverview.items.get(5).update(data);
									}	
								});
							}
							countyName=record.get('county');
							if(record.get('tieneImg')=='Y'){
								Ext.getCmp('photos').setVisible(true);
							}else{
								Ext.getCmp('photos').setVisible(false);
							}
							parcelid=record.get('parcelid');
							Ext.getCmp('carousel').store.proxy.extraParams={county:county,parcelid:parcelid,width:widthImagen,height:heightImagen};
							Ext.getCmp('carousel').removeAll();
							Ext.getCmp('carousel').removeAll();
							panelPrincipal.setActiveItem(2);
							panelOverview.setActiveItem(0);
						}
					}
                }
            ] 
			
        });
		//Panel mapa y lista
		panel= new Ext.Panel({
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items:[
				mapPanel, listPanel
			]
			
		});
		//Mapa y lista de propiedades
		panelMapa = new Ext.Panel({
			dockedItems: [toolbarTop,toolbarBottom],
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items:[
				panel
			]
		});
		var toolbarSearchTop = new Ext.Toolbar({
			dock : 'top',
			items: [
				{
					iconMask: true,
					iconCls: 'arrow_left',
					handler: function() {
						panelPrincipal.setActiveItem(0);
					}
				},{
					iconMask: true,
					iconCls:'locate',
					handler: function(){
						Ext.getCmp('search_text').setValue('My location');
					}
				}
			]
		});
		var searchProperties = function(){
			var state = Ext.getCmp('search_state').getValue();
			var county = Ext.getCmp('search_county').getValue();
			var search_properties=Ext.getCmp('search_properties').getValue();
			var search_type=Ext.getCmp('search_type').getValue();
			var searchtext=Ext.getCmp('search_text').getValue();
			var baths=Ext.getCmp('baths').getValue();
			var beds=Ext.getCmp('beds').getValue();
			var sqft=Ext.getCmp('sqft').getValue();
			var pendes=Ext.getCmp('pendes').getValue();
			var module='properties';
			//var coords = mapPanel.geo.coords;
			var pequity;
			var occupied=Ext.getCmp('occupied').getValue();
			if(Ext.getCmp('equity_sale').isVisible){
				pequity=Ext.getCmp('equity_sale').getValue();
			}else{
				pequity=Ext.getCmp('equity_public').getValue();
			}
			if(searchtext!='My location'){
				if(marcadorLocation!=null){
					marcadorLocation.setMap(null);
				}
			}	
			/*latitude=coords.latitude;
			longitude=coords.longitude;
			if(latitude==7.765374){
				latitude=26.003146;
				longitude=-80.223937;
			}*/
			store.proxy.extraParams = {module:module,state:state,county:county,search_type:search_type,search_text:searchtext,search_properties:search_properties,latitude:latitude,longitude:longitude,baths:baths,beds:beds,sqft:sqft,pendes:pendes,pequity:pequity,occupied:occupied,orderby:'',orderdirection:''};
			store.currentPage=1;
			store.load();
			panelPrincipal.setActiveItem(0);
		}
		var toolbarSearchBottom = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Search',
					ui: 'notmal',
					width: 80,
					handler: searchProperties	
				},{
					text: 'Reset',
					ui: 'notmal',
					width: 80,
					handler: function() {
						Ext.getCmp('search_county').setValue('16');
						Ext.getCmp('search_properties').setValue('PR');
						Ext.getCmp('search_type').setValue('');
						Ext.getCmp('search_text').setValue('');
						Ext.getCmp('baths').setValue('-1');
						Ext.getCmp('beds').setValue('-1');
						Ext.getCmp('sqft').setValue('-1');
						Ext.getCmp('pendes').setValue('-1');
					}
				}
			]
		});
		var form = new Ext.Panel({
			items: [
				{
					xtype: 'textfield',
					id : 'search_text',
					name : 'search_text',
					label: 'Location'
				},{
					xtype: 'selectfield',
					name : 'search_properties',
					id : 'search_properties',
					label: 'Properties',
					options:[
								{text: 'Public Record',  value: 'PR'},
								{text: 'For Sale',  value: 'FS'},
								{text: 'For Rent',  value: 'FR'}
							],
					listeners:{
						change: function(sel,val){
							if(login){
								if(val=='FS'){
									Ext.getCmp('equity_public').setVisible(false);
									Ext.getCmp('equity_sale').setVisible(true);
								}else if(val=='PR'){
									Ext.getCmp('equity_public').setVisible(true);
									Ext.getCmp('equity_sale').setVisible(false);
								}else{
									Ext.getCmp('equity_public').setVisible(false);
									Ext.getCmp('equity_sale').setVisible(false);
								}
							}else{
								Ext.getCmp('equity_public').setVisible(false);
								Ext.getCmp('equity_sale').setVisible(false);
							}	
						}
					}
				},{
					xtype: 'selectfield',
					id: 'search_state',
					name: 'search_state',
					label: 'State',
					options:[
								{text: 'Florida',  value: '1'}
							]
				},{
					xtype: 'selectfield',
					id:'search_county',
					name: 'search_county',
					label: 'County',
					options: countys
				},{
					xtype: 'selectfield',
					name : 'search_type',
					id : 'search_type',
					label: 'Type',
					options:[
								{text: 'Any Type',  value: ''},
								{text: 'Single Family',  value: '01'},
								{text: 'Condo/Town/Villa',  value: '04'},
								{text: 'Multi Family',  value: '08'},
								{text: 'Commercial',  value: '11'},
								{text: 'Vacant Land',  value: '00'},
								{text: 'Mobile Home',  value: '02'}
							]
				},{
					xtype: 'selectfield',
					name : 'pendes',
					id : 'pendes',
					label: 'Foreclosure',
					hidden: !login,
					options:[
								{text: 'Any',  value: '-1'},
								{text: 'No',  value: 'N'},
								{text: 'Pre-Foreclosed',  value: 'P'},
								{text: 'Foreclosed',  value: 'F'}
							]
				},{
					xtype: 'selectfield',
					id: 'equity_public',
					name: 'equity_public',
					label: 'Debt. Equity',
					hidden: !login,
					options:[
								{text: 'Any',  value: '-1'},
								{text: 'Less than -10',  value: '<-10'},
								{text: 'Less than 0',  value: '<0'},
								{text: 'Equal or Greater 0',  value: '>=0'},
								{text: 'Greater than 10',  value: '>10'},
								{text: 'Greater than 20',  value: '>20'},
								{text: 'Greater than 30',  value: '>30'},
								{text: 'Greater than 40',  value: '>40'},
								{text: 'Greater than 50',  value: '>50'},
								{text: 'Greater than 60',  value: '>60'}
							],
					listeners:{
						change: function(sel,val){
							Ext.getCmp('equity_sale').setValue(val);
						}
					}
				},{
					xtype: 'selectfield',
					id: 'equity_sale',
					name: 'equity_sale',
					label: 'Potential Equity',
					hidden: !login,
					options:[
								{text: 'Any',  value: '-1'},
								{text: 'Less than -10',  value: '<-10'},
								{text: 'Less than 0',  value: '<0'},
								{text: 'Equal or Greater 0',  value: '>=0'},
								{text: 'Greater than 10',  value: '>10'},
								{text: 'Greater than 20',  value: '>20'},
								{text: 'Greater than 30',  value: '>30'},
								{text: 'Greater than 40',  value: '>40'},
								{text: 'Greater than 50',  value: '>50'},
								{text: 'Greater than 60',  value: '>60'}
							],
					listeners:{
						change: function(sel,val){
							Ext.getCmp('equity_public').setValue(val);
						}
					}		
				},{
					xtype: 'selectfield',
					name : 'beds',
					id : 'beds',
					label: 'Beds',
					options:[
								{text: 'Any',  value: '-1'},
								{text: '1+',  value: '1'},
								{text: '2+',  value: '2'},
								{text: '3+',  value: '3'},
								{text: '4+',  value: '4'},
								{text: '5+',  value: '5'}
							]
				},{
					xtype: 'selectfield',
					name : 'baths',
					id : 'baths',
					label: 'Baths',
					options:[
								{text: 'Any',  value: '-1'},
								{text: '1+',  value: '1'},
								{text: '2+',  value: '2'},
								{text: '3+',  value: '3'},
								{text: '4+',  value: '4'},
								{text: '5+',  value: '5'}
							]
				},{
					xtype: 'selectfield',
					name : 'sqft',
					id : 'sqft',
					label: 'Sqft',
					options:[
								{text: 'Any',  value: '-1'},
								{text: '500+',  value: '500'},
								{text: '1000+',  value: '1000'},
								{text: '1500+',  value: '1500'},
								{text: '2000+',  value: '2000'},
								{text: '2500+',  value: '2500'},
								{text: '3000+',  value: '3000'},
								{text: '3500+',  value: '3500'},
								{text: '4000+',  value: '4000'}
							]
				},{
					xtype: 'selectfield',
					name : 'occupied',
					id : 'occupied',
					label: 'Owner Occupied',
					hidden: !login,
					options:[
								{text: 'Any',  value: '-1'},
								{text: 'Yes',  value: 'Y'},
								{text: 'No',  value: 'N'}
							]
				}
			]
		});
		//Panel de Busqueda
		var panelSearch= new Ext.Panel({
			scroll: 'vertical',
			dockedItems: [toolbarSearchTop,toolbarSearchBottom],
			items:[
				form
			]
		});
		var toolbarOverview = new Ext.Toolbar({
			dock : 'top',
			items: [
				{
					iconMask: true,
					iconCls: 'arrow_left',
					handler: function() {
						panelPrincipal.setActiveItem(0);
					}
				},{
					xtype:'spacer'
				},{
					text: 'Details',
					width: 70,
					height: 35,
					handler: function() {
						panelOverview.setActiveItem(0);
					}
				},{
					text: 'Listing',
					width: 70,
					height: 35,
					handler: function() {
						panelOverview.setActiveItem(0);
						panelOverview.setActiveItem(1);
					}
				},{
					id: 'mortfore',
					text: '',
					width: 80,
					height: 35,
					hidden: true,
					handler: function() {
						panelOverview.setActiveItem(0);
						panelOverview.setActiveItem(5);
					}
				},{
					xtype:'spacer'
				}
			]
		});
		
		var toolbarOverviewBottom = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					id: 'photos',
					iconMask: true,
					iconCls: 'photo1',
					handler: function(){
						panelOverview.setActiveItem(2);
					}
				},{
					iconMask: true,
					iconCls: 'maps',
					handler: function(){
						panelOverview.setActiveItem(0);
						panelOverview.setActiveItem(6);
					}
				},{
					id: 'reports',
					text: 'Reports',
					width: 80,
					height: 35,
					hidden: !login,
					handler: function() {
						panelOverview.setActiveItem(0);
						panelPrincipal.setActiveItem(3);
						reporte='xray';
						crearReporte();
					}
				},{
					text: 'Comps',
					width: 70,
					height: 35,
					handler: function() {
						panelOverview.setActiveItem(0);
						Ext.getCmp('listComp').getStore().proxy.url='php/comparables.php';
						Ext.getCmp('listComp').getStore().proxy.extraParams={
																				id: parcelid,
																				prop: typeprop,
																				county: countyName,
																				status: 'CC,CS',
																				type: 'nobpo',
																				orderby:'',
																				orderdirection:''	
																			};
						Ext.getCmp('listComp').getStore().load();
						comparable='comps';
						panelPrincipal.setActiveItem(4);
						markComparable.setMap(Ext.getCmp('mapComp').map);
						Ext.getCmp('mapComp').map.setCenter(new google.maps.LatLng(markComparable.position.lat(), markComparable.position.lng()));
					}
				}
			]
		});
		var tplPublicRecord=new Ext.XTemplate(
						'<tpl for=".">',
						'<tpl if="'+login+'">',
						'<div align="center" style="text-align:center; width:'+document.body.offsetWidth+'">',
						'<div style="text-align:center; width:'+tabla+'; padding-left: '+padding+'; padding-right:'+padding+';">',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:'+tabla+';">',
						'		<tr align="center">',
						'			<td align="center" colspan="4" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">VALUE</td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td align="center" colspan="2"><span style="font-weight:bold;">Active</span></td>',
						'			<td align="center" colspan="2"><span style="font-weight:bold;">Market</span></td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td align="center" colspan="2">{offertvalue:this.numberFormat("$","Y")}</td>',
						'			<td align="center" colspan="2">{marketvalue:this.numberFormat("$","Y")}</td>',
						'		</tr>',
						'	</table>',
						'</div>',
						'</div>',
						'<br/>',
						'<div align="center" style="text-align:center; width:'+document.body.offsetWidth+'">',
						'<div style="text-align:center; width:'+tabla+'; padding-left: '+padding+'; padding-right:'+padding+';">',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:'+tabla+';">',
						'		<tr align="center">',
						'			<td align="center" colspan="3" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">EQUITY</td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td align="center"><span style="font-weight:bold;">Potential</span></td>',
						'			<td align="center"><span style="font-weight:bold;">Active</span></td>',
						'			<td align="center"><span style="font-weight:bold;">Debt To</span></td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td align="center">% <span style="color:{marketpctg:this.evaluaValor};">{marketpctg:this.numberFormat("","Y")}</span></td>',
						'			<td align="center">% <span style="color:{offertpctg:this.evaluaValor};">{offertpctg:this.numberFormat("","Y")}</span></td>',
						'			<td align="center">% <span style="color:{debttv:this.evaluaValor};">{debttv:this.numberFormat("","Y")}</span></td>',
						'		</tr>',
						'	</table>',
						'</div>',
						'</div>',
						'<br/>',
						'</tpl>',
						'<div style="text-align: center; width: '+document.body.offsetWidth+';">',
						'<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">PUBLIC RECORDS</td>',
						'	</tr>',
						'	<tpl if="this.evaluaData(address)">',
						'	<tr align="center">',
						'		<td align="right" width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.address+':</span></td>',
						'		<td align="left">{address}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(unit)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.unit+':</span></td>',
						'		<td align="left">{unit}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(city)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.city+':</span></td>',
						'		<td align="left">{city}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(zip)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.zip+':</span></td>',
						'		<td align="left">{zip}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(folio)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.folio+':</span></td>',
						'		<td align="left">{folio}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(ac)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.ac+':</span></td>',
						'		<td align="left">{ac}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(stories)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.stories+':</span></td>',
						'		<td align="left">{stories}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(yrbuilt)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.yrbuilt+':</span></td>',
						'		<td align="left">{yrbuilt}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(tsqft)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.tsqft+':</span></td>',
						'		<td align="left">{tsqft:this.numberFormat("","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(beds)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.beds+':</span></td>',
						'		<td align="left">{beds}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(bath)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.bath+':</span></td>',
						'		<td align="left">{bath}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(pool)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.pool+':</span></td>',
						'		<td align="left">{pool}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(waterf)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.waterf+':</span></td>',
						'		<td align="left">{waterf}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(lsqft)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.lsqft+':</span></td>',
						'		<td align="left">{lsqft:this.numberFormat("","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(bheated)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.bheated+':</span></td>',
						'		<td align="left">{bheated:this.numberFormat("","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(units)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.units+':</span></td>',
						'		<td align="left">{units}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(ccoded)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.ccoded+':</span></td>',
						'		<td align="left">{ccoded}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(buildingv)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.buildingv+':</span></td>',
						'		<td align="left">{buildingv:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(sfp)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.sfp+':</span></td>',
						'		<td align="left">{sfp:this.numberFormat("","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(landv)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.landv+':</span></td>',
						'		<td align="left">{landv:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(taxablev)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.taxablev+':</span></td>',
						'		<td align="left">{taxablev:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(saledate)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.saledate+':</span></td>',
						'		<td align="left">{saledate:this.dateFormat}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(saleprice)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.saleprice+':</span></td>',
						'		<td align="left">{saleprice:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'</table>',
						'<br/>',
						'<div align="center" style="text-align:center; width:'+document.body.offsetWidth+'">',
						'<div style="text-align:center; width:'+tabla+'; padding-left: '+padding+'; padding-right:'+padding+';">',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:'+tabla+';">',
						'	<tr align="center">',
						'		<td align="center" colspan="4" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">HISTORY SALES</td>',
						'	</tr>',
						'	<tpl for="sales">',
						'		<tr align="center">',
						'			<td colspan=4 align="center"><span style="font-weight:bold;">SALES {#}<?php echo $i;?></span></td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td><span style="font-weight:bold;">'+arrayTitulos.sales.date+':</span></td>',
						'			<td>{date:this.dateFormat}</td>',
						'			<td><span style="font-weight:bold;">'+arrayTitulos.sales.price+':</span></td>',
						'			<td>{price:this.numberFormat("$","Y")}</td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td><span style="font-weight:bold;">'+arrayTitulos.sales.book+':</span></td>',
						'			<td>{book}</td>',
						'			<td><span style="font-weight:bold;">'+arrayTitulos.sales.page+':</span></td>',
						'			<td>{page}</td>',
						'		</tr>',
						'	</tpl>',
						'	</table>',
						'</div>',
						'</div>',
						'<br/>',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'		<tr align="center">',
						'			<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">OWNER INFORMATION</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner)">',
						'		<tr align="left">',
						'			<td align="right"  width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner+':</span></td>',
						'			<td align="left">{owner}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner_a)">',
						'		<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner_a+':</span></td>',
						'			<td align="left">{owner_a}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner_c)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner_c+':</span></td>',
						'			<td align="left">{owner_c}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner_z)">',
						'		<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner_z+':</span></td>',
						'			<td align="left">{owner_z}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner_s)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner_s+':</span></td>',
						'			<td align="left">{owner_s}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(owner_p)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.owner_p+':</span></td>',
						'			<td align="left">{owner_p}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(phonenumber1)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.phonenumber1+':</span></td>',
						'			<td align="left">{phonenumber1}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(phonenumber2)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.psummary.phonenumber2+':</span></td>',
						'			<td align="left">{phonenumber2}</td>',
						'		</tr>',
						'	</tpl>',
						'	</table>',
						'</div>',	
						'</tpl>',
						functionsTemplates		
					);
		var tplListing=new Ext.XTemplate(
						'<tpl for=".">',
						'<div>',
						'<table id="pagtag_table" cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">{proptype:this.typeProperty}</td>',
						'	</tr>',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">',
						'			<tpl if="status===\'A\'">',
						'				For Sale: {lprice:this.numberFormat("$","Y")}',
						'			</tpl></td>',
						'	</tr>',
						'	<tpl if="this.evaluaData(address)">',
						'	<tr align="left">',
						'		<td align="right" width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.address+':</span></td>',
						'		<td align="left">{address}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mlnumber)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.mlnumber+':</span></td>',
						'		<td align="left">{mlnumber}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(state)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.state+':</span></td>',
						'		<td align="left">{state}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(parcelid)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.parcelid+':</span></td>',
						'		<td align="left">{parcelid}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(county)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.county+':</span></td>',
						'		<td align="left">{county}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(status)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.status+':</span></td>',
						'		<td align="left">{status}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(city)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.city+':</span></td>',
						'		<td align="left">{city}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(zip)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.zip+':</span></td>',
						'		<td align="left">{zip}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(lprice)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.lprice+':</span></td>',
						'		<td align="left">{lprice:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(subdno)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.subdno+':</span></td>',
						'		<td align="left">{subdno}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(entrydate)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.entrydate+':</span></td>',
						'		<td align="left">{entrydate:this.dateFormat}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(constype)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.constype+':</span></td>',
						'		<td align="left">{constype}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(ldate)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.ldate+':</span></td>',
						'		<td align="left">{ldate:this.dateFormat}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(apxtotsqft)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.apxtotsqft+':</span></td>',
						'		<td align="left">{apxtotsqft:this.numberFormat("","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(dom)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.dom+':</span></td>',
						'		<td align="left">{dom}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(waterf)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.waterf+':</span></td>',
						'		<td align="left">{waterf}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(yrbuilt)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.yrbuilt+':</span></td>',
						'		<td align="left">{yrbuilt}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(beds)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.beds+':</span></td>',
						'		<td align="left">{beds}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(bath)">',
						'	<tr align="left">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.bath+':</span></td>',
						'		<td align="left">{bath}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(hbath)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.hbath+':</span></td>',
						'		<td align="left">{hbath}</td>',
						'	</tr>',
						'	</table>',
						'	</tpl>',
						'<br clear="all" />',
						'<div align="center" style="text-align:center; width:'+document.body.offsetWidth+'">',
						'<span style="color: #15428B;font-size: 18px;font-weight:bold; padding-bottom:10px;">REMARKS</span>',
						'	<p align="center" style="color: #15428B;font-size: 16px;text-align:justify; padding-left: '+padding+'; padding-right:'+padding+';">{remark1} {remark2} {remark3} {remark4} {remark5}</p>',
						'</div>',
						'<br clear="all" />',
						'<table class=content cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">AGENT / OFFICE</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officename)">',
						'	<tr>',
						'		<td valign=top align="right" width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">Courtesy Of:</span></td>',
						'		<td valign=top align="left">{officename}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agent)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.agent+':</span></td>',
						'		<td valign=top align="left">{agent}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agentph)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.agentph+':</span></td>',
						'		<td valign=top align="left">{agentph}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agentph2)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.agentph2+':</span></td>',
						'		<td valign=top align="left">{agentph2}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agentph3)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.agentph3+':</span></td>',
						'		<td valign=top align="left">{agentph3}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agentfax)">',
						'	<tr align="left">	',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">Agent Fax:</span></td>',
						'		<td valign=top align="left">{agentfax}</td>',
						'	</tr>',
						'	<tr align="left">',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agenttollfree)">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">Agent Tollfree:</span></td>',
						'		<td valign=top align="left">{agenttollfree}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officename)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.officename+':</span></td>',
						'		<td valign=top align="left">{officename}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officefax)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.officefax+':</span></td>',
						'		<td valign=top align="left">{officefax}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officephone)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.officephone+':</span></td>',
						'		<td valign=top align="left">{officephone}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officeotherphone1)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">Office Phone 2:</span></td>',
						'		<td valign=top align="left">{officeotherphone1}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officeotherphone2)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">Office Phone 3:</span></td>',
						'		<td valign=top align="left">{officeotherphone2}</td>',
						'	</tr>  ',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officetollfree)">',
						'	<tr align="left">',
						'		<td valign=top align="right"><span style="font-weight:bold; padding-right:5px;">Office Tollfree:</span></td>',
						'		<td valign=top align="left">{officetollfree}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(agentemail)">',
						'	<tr align="left">',
						'		<td colspan=2 valign=top align="center"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.agentemail+':</span></td>',
						'	</tr>',
						'	<tr align="left">',
						'		<td colspan=2 valign=top align="center">{agentemail}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(officeemail)">',
						'	<tr align="left">',
						'		<td colspan=2 valign=top align="center"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mlsresidential.officeemail+':</span></td>',
						'	</tr>',
						'	<tr align="center">',
						'		<td colspan=2 valign=top align="center">{officeemail}</td>',
						'	</tr>',
						'	</tpl>',
						'</table>',
						'</div',
						'</tpl>',
						functionsTemplates		
					);
		var tplForeclosure=new Ext.XTemplate(
						'<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'	<tpl if="mortgage.length !== 0">',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">MORTGAGE INFORMATION</td>',
						'	</tr>',
						'	</tpl>',
						'<tpl for="mortgage">',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;"></td>',
						'	</tr>',
						'	<tr align="center">',
						'		<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">MORTGAGE {#}</td>',
						'	</tr>',
						'	<tpl if="this.evaluaData(parcelid)">',
						'	<tr align="center">',
						'		<td align="right" width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.parcelid+':</span></td>',
						'		<td align="left">{parcelid}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(docdesc)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.docdesc+':</span></td>',
						'		<td align="left">{docdesc}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_bor1)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_bor1+':</span></td>',
						'		<td align="left">{mtg_bor1}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_lender)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_lender+':</span></td>',
						'		<td align="left">{mtg_lender}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_orbk)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_orbk+':</span></td>',
						'		<td align="left">{mtg_orbk}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_orpg)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_orpg+':</span></td>',
						'		<td align="left">{mtg_orpg}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_recdat)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_recdat+':</span></td>',
						'		<td align="left">{mtg_recdat:this.dateFormat}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg_amount)">',
						'	<tr align="center">',
						'		<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.mortgage.mtg_amount+':</span></td>',
						'		<td align="left">{mtg_amount:this.numberFormat("$","Y")}</td>',
						'	</tr>',
						'	</tpl>',
						'</tpl>',
						'</table>',
						
						'<br/>',
						'<tpl for="pendes">',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:100%;">',
						'		<tr align="center">',
						'			<td align="center" colspan="2" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">FORECLOSURE INFORMATION</td>',
						'		</tr>',
						'	<tpl if="this.evaluaData(parcelid)">',
						'		<tr align="left">',
						'			<td align="right"  width="'+(document.body.offsetWidth/2)+'"><span style="font-weight:bold; padding-right:5px;">Status:</span></td>',
						'			<td align="left">{pendes:this.typeForeclosure} {sold:this.soldType}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(parcelid)">',
						'		<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.parcelid+':</span></td>',
						'			<td align="left">{parcelid}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(case_numbe)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.case_numbe+':</span></td>',
						'			<td align="left">{case_numbe}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(debttv)">',
						'		<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.marketvalue.debttv+':</span></td>',
						'			<td align="left">{debttv}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(defowner1)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.defowner1+':</span></td>',
						'			<td align="left">{defowner1}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(defowner2)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.defowner2+':</span></td>',
						'			<td align="left">{defowner2}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(entrydate)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.entrydate+':</span></td>',
						'			<td align="left">{entrydate:this.dateFormat}</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(plaintiff1)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.plaintiff1+':</span></td>',
						'			<td align="left">{plaintiff1}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1amt)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1amt+':</span></td>',
						'			<td align="left">{mtg1amt:this.numberFormat("$","Y")}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1bal)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1bal+':</span></td>',
						'			<td align="left">{mtg1bal:this.numberFormat("$","Y")}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1book)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1book+':</span></td>',
						'			<td align="left">{mtg1book}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1page)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1page+':</span></td>',
						'			<td align="left">{mtg1page}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1date)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1date+':</span></td>',
						'			<td align="left">{mtg1date:this.dateFormat}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1paymen)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1paymen+':</span></td>',
						'			<td align="left">{mtg1paymen}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(judgeamt)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.judgeamt+':</span></td>',
						'			<td align="left">{judgeamt:this.numberFormat("$","Y")}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(judgedate)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.judgedate+':</span></td>',
						'			<td align="left">{judgedate:this.dateFormat}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1lastpa)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1lastpa+':</span></td>',
						'			<td align="left">{mtg1lastpa}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(attorney)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.attorney+':</span></td>',
						'			<td align="left">{attorney}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1positi)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1positi+':</span></td>',
						'			<td align="left">{mtg1positi}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(attorneyp)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.attorneyp+':</span></td>',
						'			<td align="left">{attorneyp}</td>',
						'		</tr>',
						'	</tpl>',
						'	<tpl if="this.evaluaData(mtg1type)">',
						'	<tr align="left">',
						'			<td align="right"><span style="font-weight:bold; padding-right:5px;">'+arrayTitulos.pendes.mtg1type+':</span></td>',
						'			<td align="left">{mtg1type}</td>',
						'		</tr>',
						'	</tpl>',
						'</table>',
						'</tpl>',
						'<br/>',
						'<div align="center" style="text-align:center; width:'+document.body.offsetWidth+'">',
						'<div style="text-align:center; width:'+tabla+'; padding-left: '+padding+'; padding-right:'+padding+';">',
						'	<table cellpadding="0" cellspacing="0" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:16px;color:#15428b;width:'+tabla+';">',
						'	<tpl if="liens.length !== 0">',
						'	<tr align="center">',
						'		<td align="center" colspan="4" style="font-size: 18px;font-weight:bold; padding-bottom:10px;">LIENS INFORMATION</td>',
						'	</tr>',
						'	</tpl>',
						'	<tpl for="liens">',
						'		<tr align="center">',
						'			<td colspan=4 align="center"><span style="font-weight:bold;">LIEN {#}</span></td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td><span style="font-weight:bold;">Amount:</span></td>',
						'			<td>{amount:this.numberFormat("$","Y")}</td>',
						'			<td><span style="font-weight:bold;">Holder:</span></td>',
						'			<td>{holde}</td>',
						'		</tr>',
						'		<tr align="center">',
						'			<td><span style="font-weight:bold;">Book:</span></td>',
						'			<td>{book}</td>',
						'			<td><span style="font-weight:bold;">Page:</span></td>',
						'			<td>{page}</td>',
						'		</tr>',
						'	</tpl>',
						'	</table>',
						'</div>',
						'</div>',
						functionsTemplates		
					);
		Ext.regModel("Pages", {
			fields: [
				{name : "id" },
				{name : "title" },
				{name : "html" },
				{name : "cmp" }
			]
		});
		var carousel = new Ext.ux.InfiniteCarousel({
			id:'carousel',
			fullscreen : true,
			store : new Ext.data.Store({
				model : "Pages",
				proxy : {
					type : "ajax",
					url : "php/getImages.php",
					extraParams: {
						county: county,
						parcelid: parcelid,
						width:widthImagen,
						height:heightImagen
					},
					reader : {
						type : "json",
						root : "cards"
					}
				}
			})
		});
		var toolbarOrderComps = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Apply',
					handler: function(){
						var orderby='';
						var valororder=Ext.getCmp('order_by_comps').getValue();
						if(comparable=='comps' || comparable=='active'){
							if(valororder!='dom'){
								orderby='rtm.'+valororder;
							}else{
								orderby='mlsresidential.dom';
							}	
						}else{
							if(valororder=='ffc'){
								orderby='`psummary`.bheated';
							}else if(valororder!='dom'){
								orderby='p.'+valororder;
							}else{
								if(comparable=='rental'){
									orderby='mlsresidential.dom';
								}else{
									orderby='';
								}	
							}
						}
						Ext.getCmp('listComp').getStore().proxy.extraParams.orderby=orderby;
						Ext.getCmp('listComp').getStore().proxy.extraParams.orderdirection=Ext.getCmp('order_direction_comps').getValue();
						Ext.getCmp('listComp').getStore().loadPage(1);
						Ext.getCmp('orderByComps').hide();
					}
				},{
					text: 'Cancel',
					handler: function(){
						Ext.getCmp('orderByComps').hide();
					}
				}
			]
		});
		var orderByComps = new Ext.Panel({
			id: 'orderByComps',
			hideOnMaskTap: false,
			floating: true,
			modal: true,
			centered: true,
			width: 350,
			height: 200,
			dockedItems:[toolbarOrderComps],
			items: [
						{
							xtype: 'selectfield',
							name: 'order_by_comps',
							id: 'order_by_comps',
							label: 'Order By',
							options:[
								{text: 'Address',  value: 'address'},
								{text: 'DOM',  value: 'dom'},
								{text: 'Lot Size',  value: 'ffc'},
								{text: 'Beds',  value: 'beds'},
								{text: 'Baths',  value: 'bath'},
								{text: 'Price',  value: 'price'}
							]
						},{
							xtype: 'selectfield',
							name: 'order_direction_comps',
							id: 'order_direction_comps',
							label: 'Direction',
							options:[
								{text: 'ASC',  value: 'ASC'},
								{text: 'DESC',  value: 'DESC'}
							]
						}	
					]
		});
		//Panel comparables
		var mapOverview=new Ext.Map({
			id:'mapOverview',
			title:'mapOverview'
		});
		panelOverview=new Ext.Panel({
			layout: 'card',
			activeItem: 0,
			style: {
				background: '#ffffff'
			},
			dockedItems: [toolbarOverview,toolbarOverviewBottom],
			items:[
				{
					scroll: 'vertical',
					tpl: tplPublicRecord
				},{
					scroll: 'vertical',
					tpl: tplListing
				},carousel,
				{
					scroll:'vertical',
					html:''
				},{
					scroll:'vertical',
					html:''
				},{
					scroll: 'vertical',
					tpl: tplForeclosure
				},mapOverview
			]
		});
		var toolbarReport = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Apply',
					handler: function(){
						distance=Ext.getCmp('distance').getValue();
						crearReporte();
						Ext.getCmp('formReport').hide();
					}
				},{
					text: 'Cancel',
					handler: function(){
						Ext.getCmp('formReport').hide();
					}
				}
			]
		});
		var formReport = new Ext.Panel({
			id: 'formReport',
			hideOnMaskTap: false,
			floating: true,
			modal: true,
			centered: true,
			width: 320,
			height: 220,
			scroll: 'vertical',
			items: [
				{
					xtype: 'fieldset',
					title: 'Report',
					defaults: {
						labelAlign: 'left'
					},
					items: [
						{
							xtype: 'selectfield',
							name: 'distance',
							id: 'distance',
							label: 'Distance',
							value: '0.5',
							options:[
								{text: '0.1',  value: '0.1'},
								{text: '0.2',  value: '0.2'},
								{text: '0.3',  value: '0.3'},
								{text: '0.4',  value: '0.4'},
								{text: '0.5',  value: '0.5'},
								{text: '0.6',  value: '0.6'},
								{text: '0.7',  value: '0.7'},
								{text: '0.8',  value: '0.8'},
								{text: '0.9',  value: '0.9'},
								{text: '1.0',  value: '1.0'},
								{text: '2.1',  value: '2.1'},
								{text: '2.2',  value: '2.2'},
								{text: '2.3',  value: '2.3'},
								{text: '2.4',  value: '2.4'},
								{text: '2.5',  value: '2.5'},
								{text: '2.6',  value: '2.6'},
								{text: '2.7',  value: '2.7'},
								{text: '2.8',  value: '2.8'},
								{text: '2.9',  value: '2.9'},
								{text: '2.0',  value: '2.0'}
							]
						}
					]
				}
			],
			dockedItems:[
				toolbarReport
			]
		});
		var crearReporte = function(){
			panelOverview.setActiveItem(0);
			Ext.getCmp('panelLoading').show();
			var pad=padding;
			if(phone){
				pad=1;
			}
			Ext.Ajax.request({
				url:'php/'+reporte+'.php?distance='+distance+'&parcelid='+parcelid+'&county='+county+'&width='+widthImagen+'&padding='+pad,
				method: 'POST',
				success:function(response, opts){
					var obj=Ext.decode(response.responseText);
					var html=obj.html;
					Ext.getCmp('panelReport').update(html);
					Ext.getCmp('panelLoading').hide();
				}	
			});
		}
		var toolbarPanelReport = new Ext.Toolbar({
			dock : 'top',
			items: [
				{
					iconMask: true,
					iconCls: 'arrow_left',
					handler: function() {
						panelPrincipal.setActiveItem(2);
					}
				},{
					xtype:'spacer'
				},{
					text: 'XRay',
					hidden: !login,
					handler: function(){
						reporte='xray';
						crearReporte();
					}
				},{
					text: 'Discount',
					hidden: !login,
					handler: function(){
						reporte='discount';
						crearReporte();
						
					}
				},{
					xtype:'spacer'
				}
			]
		});
		var toolbarPanelReportBottom = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Settings',
					width: 75,
					height: 35,
					handler: function() {
						Ext.getCmp('formReport').show('pop');
					}
				}
			]
		});		
		var panelReports=new Ext.Panel({
			layout: 'card',
			activeItem: 0,
			style: {
				background: '#ffffff'
			},
			dockedItems: [toolbarPanelReport,toolbarPanelReportBottom],
			items:[
				{
					id: 'panelReport',
					scroll:'vertical',
					html:''
				}
			]
		});
		var toolbarPanelFilters = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					text: 'Apply',
					width: 75,
					height: 35,
					handler: function() {
						var dist=Ext.getCmp('distance_filter').getValue();
						var garea=Ext.getCmp('garea_filter').getValue();
						var larea=Ext.getCmp('larea_filter').getValue();
						var built=Ext.getCmp('built_filter').getValue();
						var closingdt=Ext.getCmp('closingdt_filter').getValue();
						var listingdt=Ext.getCmp('listingdt_filter').getValue();
						var garea_from = (garea!='-2') ? '-1' : Ext.getCmp('garea_from').getValue();
						var garea_to = (garea!='-2') ? '-1' : Ext.getCmp('garea_to').getValue();
						var larea_from = (larea!='-2') ? '-1' : Ext.getCmp('larea_from').getValue();
						var larea_to = (larea!='-2') ? '-1' : Ext.getCmp('larea_to').getValue();
						var closingdt_from = (closingdt!='-2') ? '-1' : Ext.getCmp('closingdt_from').getValue();
						var closingdt_to = (closingdt!='-2') ? '-1' : Ext.getCmp('closingdt_to').getValue();
						var listingdt_from = (listingdt!='-2') ? '-1' : Ext.getCmp('listingdt_from').getValue();
						var listingdt_to = (listingdt!='-2') ? '-1' : Ext.getCmp('listingdt_to').getValue();
						var pool = Ext.getCmp('pool_filter').getValue();
						var waterf = Ext.getCmp('waterf_filter').getValue();
						var bedscon = Ext.getCmp('beds_filter').getValue();
						var beds = Ext.getCmp('beds_filter_text').getValue();
						var bathscon = Ext.getCmp('baths_filter').getValue();
						var baths = Ext.getCmp('baths_filter_text').getValue();
						
						var parametros='distance='+dist+'&garea='+garea+'&garea_from='+garea_from+'&garea_to='+garea_to+'&larea='+larea+
										'&larea_from='+larea_from+'&larea_to='+larea_to+'&built='+built+'&closingdt='+closingdt+
										'&closingdt_from='+closingdt_from+'&closingdt_to='+closingdt_to+'&pool='+pool+'&waterf='+waterf+
										'&ldate='+listingdt+'&ldate_from='+listingdt_from+'&ldate_to='+listingdt_to+
										'&bedscon='+bedscon+'&beds='+beds+'&bathscon='+bathscon+'&baths='+baths+'&type='+comparable;
						Ext.Ajax.request({
							url:'php/filters.php?'+parametros,
							method: 'POST',
							success:function(response, opts){
								var obj=Ext.decode(response.responseText);
								var respuesta=obj.exito;
								if(respuesta=='true'){
									Ext.getCmp('formFilters').hide();
									Ext.getCmp('listComp').getStore().loadPage(1);
								}else{
									alert('Fail on filters')
								}
							}	
						});
					}
				},{
					text: 'Cancel',
					width: 75,
					height: 35,
					handler: function() {
						Ext.getCmp('formFilters').hide();
					}
				}
			]
		});	
		//Panel Filters
		var formFilters = new Ext.Panel({
			id: 'formFilters',
			hideOnMaskTap: false,
			floating: true,
			modal: true,
			centered: true,
			width: 320,
			height: 350,
			scroll: 'vertical',
			items: [
				{
					xtype: 'fieldset',
					title: 'Filters',
					defaults: {
						labelAlign: 'left'
					},
					items: [
						{
							xtype: 'selectfield',
							name: 'distance_filter',
							id: 'distance_filter',
							label: 'Distance',
							options:[
								{text: 'Select',  value: '-1'},
								{text: '0.1',  value: '0.1'},
								{text: '0.2',  value: '0.2'},
								{text: '0.3',  value: '0.3'},
								{text: '0.4',  value: '0.4'},
								{text: '0.5',  value: '0.5'},
								{text: '0.6',  value: '0.6'},
								{text: '0.7',  value: '0.7'},
								{text: '0.8',  value: '0.8'},
								{text: '0.9',  value: '0.9'},
								{text: '1.0',  value: '1.0'},
								{text: '1.1',  value: '1.1'},
								{text: '1.2',  value: '1.2'},
								{text: '1.3',  value: '1.3'},
								{text: '1.4',  value: '1.4'},
								{text: '1.5',  value: '1.5'},
								{text: '1.6',  value: '1.6'},
								{text: '1.7',  value: '1.7'},
								{text: '1.8',  value: '1.8'},
								{text: '1.9',  value: '1.9'},
								{text: '2.0',  value: '2.0'}
							]
						},{
							xtype: 'selectfield',
							name: 'garea_filter',
							id: 'garea_filter',
							label: 'GArea',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'Between',  value: '-2'},
								{text: '1%',  value: '1'},
								{text: '2%',  value: '2'},
								{text: '3%',  value: '3'},
								{text: '4%',  value: '4'},
								{text: '5%',  value: '5'},
								{text: '6%',  value: '6'},
								{text: '7%',  value: '7'},
								{text: '8%',  value: '8'},
								{text: '9%',  value: '9'},
								{text: '10%',  value: '10'},
								{text: '11%',  value: '11'},
								{text: '12%',  value: '12'},
								{text: '13%',  value: '13'},
								{text: '14%',  value: '14'},
								{text: '15%',  value: '15'},
								{text: '16%',  value: '16'},
								{text: '17%',  value: '17'},
								{text: '18%',  value: '18'},
								{text: '19%',  value: '19'},
								{text: '20%',  value: '20'},
								{text: '21%',  value: '21'},
								{text: '22%',  value: '22'},
								{text: '23%',  value: '23'},
								{text: '24%',  value: '24'},
								{text: '25%',  value: '25'}
							],
							listeners:{
								change: function(sel,val){
									if(val=='-2'){
										Ext.getCmp('between_garea').setVisible(true);
									}else{
										Ext.getCmp('between_garea').setVisible(false);
									}	
								}
							}
						},{
							xtype:'panel',
							id: 'between_garea',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'textfield',
										id : 'garea_from',
										name : 'garea_from',
										placeHolder: 'Garea From',
										flex: 1
									},{
										xtype: 'textfield',
										id : 'garea_to',
										name : 'garea_to',
										placeHolder: 'Garea To',
										flex: 1
									}
							]
						},{
							xtype: 'selectfield',
							name: 'larea_filter',
							id: 'larea_filter',
							label: 'LArea',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'Between',  value: '-2'},
								{text: '1%',  value: '1'},
								{text: '2%',  value: '2'},
								{text: '3%',  value: '3'},
								{text: '4%',  value: '4'},
								{text: '5%',  value: '5'},
								{text: '6%',  value: '6'},
								{text: '7%',  value: '7'},
								{text: '8%',  value: '8'},
								{text: '9%',  value: '9'},
								{text: '10%',  value: '10'},
								{text: '11%',  value: '11'},
								{text: '12%',  value: '12'},
								{text: '13%',  value: '13'},
								{text: '14%',  value: '14'},
								{text: '15%',  value: '15'},
								{text: '16%',  value: '16'},
								{text: '17%',  value: '17'},
								{text: '18%',  value: '18'},
								{text: '19%',  value: '19'},
								{text: '20%',  value: '20'},
								{text: '21%',  value: '21'},
								{text: '22%',  value: '22'},
								{text: '23%',  value: '23'},
								{text: '24%',  value: '24'},
								{text: '25%',  value: '25'}
							],
							listeners:{
								change: function(sel,val){
									if(val=='-2'){
										Ext.getCmp('between_larea').setVisible(true);
									}else{
										Ext.getCmp('between_larea').setVisible(false);
									}	
								}
							}
						},{
							xtype:'panel',
							id: 'between_larea',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'textfield',
										id : 'larea_from',
										name : 'larea_from',
										placeHolder: 'Larea From',
										flex: 1
									},{
										xtype: 'textfield',
										id : 'larea_to',
										name : 'larea_to',
										placeHolder: 'Larea To',
										flex: 1
									}
							]
						},{
							xtype: 'selectfield',
							name: 'built_filter',
							id: 'built_filter',
							label: 'Built',
							options:[
								{text: 'Select',  value: '-1'},
								{text: '+/-1 Years',  value: '1'},
								{text: '+/-2 Years',  value: '2'},
								{text: '+/-3 Years',  value: '3'},
								{text: '+/-4 Years',  value: '4'},
								{text: '+/-5 Years',  value: '5'},
								{text: '+/-6 Years',  value: '6'},
								{text: '+/-7 Years',  value: '7'},
								{text: '+/-8 Years',  value: '8'},
								{text: '+/-9 Years',  value: '9'},
								{text: '+/-10 Years',  value: '10'},
								{text: '+/-11 Years',  value: '11'},
								{text: '+/-12 Years',  value: '12'},
								{text: '+/-13 Years',  value: '13'},
								{text: '+/-14 Years',  value: '14'},
								{text: '+/-15 Years',  value: '15'},
								{text: '+/-16 Years',  value: '16'},
								{text: '+/-17 Years',  value: '17'},
								{text: '+/-18 Years',  value: '18'},
								{text: '+/-19 Years',  value: '19'},
								{text: '+/-20 Years',  value: '20'}
							]
						},{
							xtype: 'selectfield',
							name: 'closingdt_filter',
							id: 'closingdt_filter',
							label: 'ClosingDT',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'Between',  value: '-2'},
								{text: '1 Months',  value: '1'},
								{text: '2 Months',  value: '2'},
								{text: '3 Months',  value: '3'},
								{text: '4 Months',  value: '4'},
								{text: '5 Months',  value: '5'},
								{text: '6 Months',  value: '6'},
								{text: '7 Months',  value: '7'},
								{text: '8 Months',  value: '8'},
								{text: '9 Months',  value: '9'},
								{text: '10 Months',  value: '10'},
								{text: '11 Months',  value: '11'},
								{text: '12 Months',  value: '12'},
								{text: '13 Months',  value: '13'},
								{text: '14 Months',  value: '14'},
								{text: '15 Months',  value: '15'},
								{text: '16 Months',  value: '16'},
								{text: '17 Months',  value: '17'},
								{text: '18 Months',  value: '18'},
								{text: '19 Months',  value: '19'},
								{text: '20 Months',  value: '20'},
								{text: '21 Months',  value: '21'},
								{text: '22 Months',  value: '22'},
								{text: '23 Months',  value: '23'},
								{text: '24 Months',  value: '24'}
							],
							listeners:{
								change: function(sel,val){
									if(val=='-2'){
										Ext.getCmp('between_closingdt').setVisible(true);
									}else{
										Ext.getCmp('between_closingdt').setVisible(false);
									}	
								}
							}
						},{
							xtype:'panel',
							id: 'between_closingdt',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'textfield',
										id : 'closingdt_from',
										name : 'closingdt_from',
										placeHolder: 'ClosingDT From',
										flex: 1
									},{
										xtype: 'textfield',
										id : 'closingdt_to',
										name : 'closingdt_to',
										placeHolder: 'ClosingDT To',
										flex: 1
									}
							]
						},{
							xtype: 'selectfield',
							name: 'listingdt_filter',
							id: 'listingdt_filter',
							label: 'ListingDT',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'Between',  value: '-2'},
								{text: '1 Months',  value: '1'},
								{text: '2 Months',  value: '2'},
								{text: '3 Months',  value: '3'},
								{text: '4 Months',  value: '4'},
								{text: '5 Months',  value: '5'},
								{text: '6 Months',  value: '6'},
								{text: '7 Months',  value: '7'},
								{text: '8 Months',  value: '8'},
								{text: '9 Months',  value: '9'},
								{text: '10 Months',  value: '10'},
								{text: '11 Months',  value: '11'},
								{text: '12 Months',  value: '12'},
								{text: '13 Months',  value: '13'},
								{text: '14 Months',  value: '14'},
								{text: '15 Months',  value: '15'},
								{text: '16 Months',  value: '16'},
								{text: '17 Months',  value: '17'},
								{text: '18 Months',  value: '18'},
								{text: '19 Months',  value: '19'},
								{text: '20 Months',  value: '20'},
								{text: '21 Months',  value: '21'},
								{text: '22 Months',  value: '22'},
								{text: '23 Months',  value: '23'},
								{text: '24 Months',  value: '24'}
							],
							listeners:{
								change: function(sel,val){
									if(val=='-2'){
										Ext.getCmp('between_listingdt').setVisible(true);
									}else{
										Ext.getCmp('between_listingdt').setVisible(false);
									}	
								}
							}
						},{
							xtype:'panel',
							id: 'between_listingdt',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'textfield',
										id : 'listingdt_from',
										name : 'listingdt_from',
										placeHolder: 'ListingDT From',
										flex: 1
									},{
										xtype: 'textfield',
										id : 'listingdt_to',
										name : 'listingdt_to',
										placeHolder: 'ListingDT To',
										flex: 1
									}
							]
						},{
							xtype: 'selectfield',
							name: 'pool_filter',
							id: 'pool_filter',
							label: 'Pool',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'No',  value: 'N'},
								{text: 'Yes',  value: 'Y'}
							]
						},{
							xtype: 'selectfield',
							name: 'waterf_filter',
							id: 'waterf_filter',
							label: 'WaterF',
							options:[
								{text: 'Select',  value: '-1'},
								{text: 'No',  value: 'N'},
								{text: 'Yes',  value: 'Y'}
							]
						},{
							xtype:'panel',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'selectfield',
										flex: 3,
										name: 'beds_filter',
										id: 'beds_filter',
										label: 'Beds',
										options:[
											{text: 'Equal',  value: '='},
											{text: 'Greater Than',  value: '>'},
											{text: 'Less Than',  value: '<'},
											{text: 'Equal or Less',  value: '<='},
											{text: 'Equal or Greater',  value: '>='}
										]
									}
									,{
										xtype: 'textfield',
										id : 'beds_filter_text',
										name : 'beds_filter_text',
										placeHolder: 'Beds',
										flex: 1
									}
							]
						},{
							xtype:'panel',
							layout:'hbox',
							height: 45,
							items:[{
										xtype: 'selectfield',
										flex: 3,
										name: 'baths_filter',
										id: 'baths_filter',
										label: 'Baths',
										options:[
											{text: 'Equal',  value: '='},
											{text: 'Greater Than',  value: '>'},
											{text: 'Less Than',  value: '<'},
											{text: 'Equal or Less',  value: '<='},
											{text: 'Equal or Greater',  value: '>='}
										]
									}
									,{
										xtype: 'textfield',
										id : 'baths_filter_text',
										name : 'baths_filter_text',
										placeHolder: 'Baths',
										flex: 1
									}
							]
						}	
					]
				}
			],
			dockedItems:[
				toolbarPanelFilters
			]
		});
		//Panel comparables
		var mapComparables=new Ext.Map({
			id:'mapComp',
			title:'MapComp',
			flex: 2
		});
		
		
		Ext.regModel('Comparables', {
			 fields: [
                {name: 'parcelid', type: 'string'},
                {name: 'Distance', type: 'string'},
				{name: 'lsqft', type: 'string'},
				{name: 'larea', type: 'string'},
				{name: 'zip', type: 'string'},
				{name: 'bath', type: 'int'},
				{name: 'beds', type: 'int'},
				{name: 'city', type: 'float'},
				{name: 'tsqft', type: 'string'},
				{name: 'lprice', type: 'float'},
				{name: 'saledate', type: 'string'},
				{name: 'ldate', type: 'string'},
				{name: 'status', type: 'string'},
				{name: 'xcode', type: 'string'},
				{name: 'saleprice', type: 'int'},
				{name: 'waterf', type: 'string'},
				{name: 'dom', type: 'int'},
				{name: 'pool', type: 'string'},
				{name: 'unit', type: 'string'},
				{name: 'sold', type: 'string'},
				{name: 'LATITUDE', type: 'string'},
				{name: 'LONGITUDE', type: 'string'},
				{name: 'pendes', type: 'string'},
				{name: 'address', type: 'string'},
				{name: 'image', type: 'string'}
            ]
		});
		var storeComparables = new Ext.data.Store({
			id:'storeComp',
			model  : 'Comparables',
			pageSize: 50,
			currentPage: 1,	
			proxy: {
                type: 'ajax',
                url: 'php/comparables.php',
				timeout: 600000,
				extraParams: {
		            id: '',
					prop: '',
					county: 'BROWARD',
					status: 'CC,CS',
					type: 'nobpo'		        
				},
				reader: {
                    root: 'results',
					totalCount: 'total'
                }
            }
		});
		//store.load({params:{module1:'otro'}});verificaFont
		var propertycomparables=new Ext.XTemplate(
						'<tpl for=".">',
						'<tpl if="this.verificaType(address)">',
						'<tpl if="!this.verificaTablet(address)">',
						'<div style="float:left; margin-right:20px; width:70px;overflow:hidden;">',
						'	<img src="{image:this.verificaImagen}" height="60px" width="70px;" style="position:relative; z-index:150;">',
						'</div>',
						'</tpl>',
						'</tpl>',
						'<tpl if="this.verificaType(address)">',
						'<div style="font-size: {lprice:this.verificaFont}px;">',
						' 	{#} - {lprice:this.priceTitle}:',
						'<tpl if="this.verificaPriceListing(address)">',
						'  <span style="color:#60b31b; font-weight:bold; font-size:{address:this.verificaFont}px;">{lprice:this.numberFormat("$","Y")}</span>',
						'</tpl>',
						'<tpl if="this.verificaPriceComps(address)">',
						'	<span style="color:#60b31b; font-weight:bold; font-size:{address:this.verificaFont}px;">{saleprice:this.numberFormat("$","Y")}</span>',
						'</tpl>',
						'<tpl if="this.verificaTablet(address)">',
						'</div>',
						'<div style="font-size: {address:this.verificaFont}px;">',
						'</tpl>',
						' 	{lprice:this.dateTitle}:',
						'<tpl if="this.verificaPriceListing(address)">',
						'  <span style="color:#61380B; font-weight:bold; font-size:{address:this.verificaFont}px;">{ldate:this.dateFormat}</span>',
						'</tpl>',
						'<tpl if="this.verificaPriceComps(address)">',
						'	<span style="color:#61380B; font-weight:bold; font-size:{address:this.verificaFont}px;">{saledate:this.dateFormat}</span>',
						'</tpl>',
						'<tpl if="this.verificaTablet(address)">',
						'</div>',
						'</tpl>',
						'</tpl>',
						'<tpl if="this.verificaType(address)">',
						'<tpl if="this.verificaTablet(address)">',
						'<div style="font-size: {address:this.verificaFont}px;">',
						'</tpl>',
						' 	'+arrayTitulos.mlsresidential.dom+': {dom}',
						'</div>',
						'</tpl>',
						'<div style="font-size: {address:this.verificaFont}px;">',
						'<tpl if="!this.verificaType(address)">',
							'{#} - ',
						'</tpl>',
						' 	<span style="color:#15428b; font-weight:bold; font-size: {address:this.verificaFont}px;">{address}</span>, {zip}',
						'<tpl if="this.verificaTablet(address)">',
						'</div>',
						'<div style="font-size: {lprice:this.verificaFont}px;">',
						'</tpl>',
						'	{beds}/{bath}, {larea} sqft, '+arrayTitulos.psummary.pool+': {pool} '+arrayTitulos.psummary.waterf+': {waterf}',
						'</div>',
						'<div style="font-size: {lprice:this.verificaFont}px;">',
						'	{pendes:this.typeForeclosure} {sold:this.soldType}',
						'</div>',
						'</tpl>',
						functionsTemplates		
					);
		var propertycomparado=new Ext.XTemplate(
						'<tpl for=".">',
						'<div style="padding-left:'+padding/4+';padding-right:'+padding/4+'">',
						'<tpl if="this.verificaStatus(status)">',
						'<div style="font-size: 12px;">',
						'  <span style="color:#60b31b; font-weight:bold; font-size:16px;">{lprice:this.numberFormat("$","Y")}</span>',
						'</div>',
						'</tpl>',
						'<div style="font-size: 16px;">',
						' 	<span style="color:#15428b; font-weight:bold;">{address}</span>, {zip}',
						'</div>',
						'<div style="font-size: 16px;">',
						'	{beds}/{bath}, {lsqft} sqft',
						'</div>',
						'</div>',
						'</tpl>',
						functionsTemplates		
					);
		var listComparables = new Ext.Panel({
            flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
            scroll: 'vertical',
            items: [
                {
					xtype:'panel',
					id: 'panelPropComp',
					tpl: propertycomparado,
					style: {
						background: '#ffffff'
					},
					flex: 2
				},{
                    xtype: 'list',
                    id:'listComp',
					itemTpl:propertycomparables,
					store: storeComparables,
					singleSelect: true,
                    emptyText:'No properties to display',
					onItemDisclosure: true,
					listeners:{
						itemtap:function(list,index,item, even){
							if(infoWindowComparable!=null){
								infoWindowComparable.close();
							}
							var record=Ext.getCmp('listComp').getStore().getAt(index);
							var img=record.get("image");
							var divimage='';
							var h,w;
							if(comparable!='distress'){
								divimage='<div style="float:left; margin-right:5px; height:110px; width:70px;overflow:hidden;">'+
										'	<img src="'+functionsTemplates.verificaImagen(img)+'" height="60px" width="70px;" style="position:relative; z-index:150;">'+
										'</div>';
								h=130;
								w=220;		
							}else{
								h=70;
								w=200;
							}	
							var content='<div style="height:'+h+'px; width:'+w+'px;">'+divimage;
							if(comparable!='distress'){
								content+='<div style="font-size: 12px;">';
									if(comparable=='comps'){	
										content+=functionsTemplates.priceTitle(record.get("saleprice"))+':  <span style="color:#60b31b; font-weight:bold; font-size:13px;">'+functionsTemplates.numberFormat(record.get("saleprice"),"$","Y")+'</span></div>';
										content+='<div style="font-size: 12px;">'+functionsTemplates.dateTitle(record.get("saledate"))+':  <span style="color:#61380B; font-weight:bold; font-size:13px;">'+functionsTemplates.dateFormat(record.get("saledate"))+'</span></div>';
									}else{
										content+=functionsTemplates.priceTitle(record.get("lprice"))+':  <span style="color:#60b31b; font-weight:bold; font-size:13px;">'+functionsTemplates.numberFormat(record.get("lprice"),"$","Y")+'<span></div>';
										content+='<div style="font-size: 12px;">'+functionsTemplates.dateTitle(record.get("ldate"))+':  <span style="color:#61380B; font-weight:bold; font-size:13px;">'+functionsTemplates.dateFormat(record.get("ldate"))+'</span></div>';
									}
								content+='<div style="font-size: 12px;">'+
										' 	'+arrayTitulos.mlsresidential.dom+': '+record.get("dom")+
										'</div>';
							}
							content+='<div style="font-size: 12px;">'+
										' 	<span style="color:#15428b; font-weight:bold;">'+record.get("address")+'</span>, '+record.get("zip")+
										'</div>'+
										'<div style="font-size: 12px;">'+
										'	'+record.get("beds")+'/'+record.get("bath")+', '+record.get("larea")+' sqft, '+arrayTitulos.psummary.pool+': '+record.get("pool")+
										'</div>'+
										'<div style="font-size: 12px;">'+
										'	'+arrayTitulos.psummary.waterf+': '+record.get("waterf")+
										'</div>'+
										'<div style="font-size: 12px;">'+
										''+functionsTemplates.typeForeclosure(record.get("pendes"))+' '+ functionsTemplates.soldType(record.get("sold"))+
										'</div></div>';
							infoWindowComparable = new google.maps.InfoWindow({
								content: content
							});
							
							infoWindowComparable.open(mapComparables.map,markersArrayComps[index]);
							if(tabletphone){
								panelCompMapaLista.items.items[1].flex=0;
								panelCompMapaLista.items.items[0].flex=1;
								panelCompMapaLista.doLayout();
								Ext.getCmp('buttonlistcomps').setVisible(true);
								Ext.getCmp('buttonmapcomps').setVisible(false);
							}
						},
						disclose: function(record, node, index,e){
							if(tabletphone){
								
							}
						}
					},
					flex: 8
                }
            ] 
			
        });
		//Panel mapa y lista
		panelCompMapaLista= new Ext.Panel({
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items:[
				mapComparables, listComparables
			]
			
		});
		var toolbarTopComparables = new Ext.Toolbar({
			dock : 'top',
			items: [
				{
					iconMask: true,
					iconCls: 'arrow_left',
					handler: function() {
						panelPrincipal.setActiveItem(2);
					}
				},{
					xtype:'spacer'
				},{
					text: 'Comps',
					width: 70,
					height: 35,
					handler: function() {
						Ext.getCmp('listComp').getStore().proxy.url='php/comparables.php';
						Ext.getCmp('listComp').getStore().proxy.extraParams={
																				id: parcelid,
																				prop: typeprop,
																				county: countyName,
																				status: 'CC,CS',
																				type: 'nobpo',
																				orderby:'',
																				orderdirection:''	
																			};
						comparable='comps';
						Ext.getCmp('listComp').getStore().currentPage=1;
						Ext.getCmp('listComp').getStore().load();
					}
				},{
					text: 'Active',
					width: 70,
					height: 35,
					hidden: !login,
					handler: function() {
						Ext.getCmp('listComp').getStore().proxy.url='php/comparables.php';
						Ext.getCmp('listComp').getStore().proxy.extraParams={
																				id: parcelid,
																				prop: typeprop,
																				county: countyName,
																				status: 'A',
																				type: 'nobpo',
																				orderby:'',
																				orderdirection:''	
																			};
						comparable='active';
						Ext.getCmp('listComp').getStore().currentPage=1;
						Ext.getCmp('listComp').getStore().load();
					}
				},{
					text: 'Distress',
					width: 80,
					height: 35,
					hidden: !login,
					handler: function() {
						Ext.getCmp('listComp').getStore().proxy.url='php/distress.php';
						Ext.getCmp('listComp').getStore().proxy.extraParams={
																				id: parcelid,
																				prop: typeprop,
																				county: countyName,
																				type: 'nobpo',
																				orderby:'',
																				orderdirection:''	
																			};
						comparable='distress';
						Ext.getCmp('listComp').getStore().currentPage=1;
						Ext.getCmp('listComp').getStore().load();
					}
				},{
					text: 'Rental',
					width: 70,
					height: 35,
					hidden: !login,
					handler: function() {
						Ext.getCmp('listComp').getStore().proxy.url='php/rental.php';
						Ext.getCmp('listComp').getStore().proxy.extraParams={
																				id: parcelid,
																				prop: typeprop,
																				county: countyName,
																				type: 'nobpo',
																				orderby:'',
																				orderdirection:''	
																			};
						comparable='rental';
						Ext.getCmp('listComp').getStore().currentPage=1;
						Ext.getCmp('listComp').getStore().load();
					}
				},{
					xtype:'spacer'
				}
			]
		});
		//tool bar panel mapa
		var toolbarBottomComps = new Ext.Toolbar({
			dock : 'bottom',
			layout: {
				pack: 'center'
			},
			items: [
				{
					xtype:'spacer'
				},{
					/*text: 'Filters',
					width: 75,
					height: 35,*/
					iconMask: true,
					iconCls:'btnFilter',
					hidden: !login,
					handler: function() {
						if(Ext.getCmp('garea_filter').getValue()=='-2'){
							Ext.getCmp('between_garea').setVisible(true);
						}else{
							Ext.getCmp('between_garea').setVisible(false);
						}
						if(Ext.getCmp('larea_filter').getValue()=='-2'){
							Ext.getCmp('between_larea').setVisible(true);
						}else{
							Ext.getCmp('between_larea').setVisible(false);
						}
						if(Ext.getCmp('closingdt_filter').getValue()=='-2'){
							Ext.getCmp('between_closingdt').setVisible(true);
						}else{
							Ext.getCmp('between_closingdt').setVisible(false);
						}
						if(Ext.getCmp('listingdt_filter').getValue()=='-2'){
							Ext.getCmp('between_listingdt').setVisible(true);
						}else{
							Ext.getCmp('between_listingdt').setVisible(false);
						}
						var visible=true;
						if(comparable=='distress'){
							visible=false;
						}
						//Ext.getCmp('distance_filter').setVisible(visible);
						Ext.getCmp('garea_filter').setVisible(visible);
						Ext.getCmp('larea_filter').setVisible(visible);
						Ext.getCmp('built_filter').setVisible(visible);
						Ext.getCmp('closingdt_filter').setVisible(visible);
						Ext.getCmp('listingdt_filter').setVisible(visible);
						Ext.getCmp('pool_filter').setVisible(visible);
						Ext.getCmp('waterf_filter').setVisible(visible);
						Ext.getCmp('beds_filter').setVisible(visible);
						Ext.getCmp('beds_filter_text').setVisible(visible);
						Ext.getCmp('baths_filter').setVisible(visible);
						Ext.getCmp('baths_filter_text').setVisible(visible);
						Ext.getCmp('formFilters').show();
						if(comparable=='active'){
							Ext.getCmp('listingdt_filter').setVisible(true);
							Ext.getCmp('closingdt_filter').setVisible(false);
						}else if(comparable=='comps'){
							Ext.getCmp('listingdt_filter').setVisible(false);
							Ext.getCmp('closingdt_filter').setVisible(true);
						}else if(comparable=='rental'){
							Ext.getCmp('closingdt_filter').setVisible(false);
							Ext.getCmp('listingdt_filter').setVisible(false);
						}
					}
				},{
					id: 'sortcomps',
					iconMask: true,
					iconCls: 'sortbutton',
					handler: function(){
						Ext.getCmp('orderByComps').show('pop');
					}
				},{
					iconMask: true,
					iconCls:'maps',
					id: 'buttonmapcomps',
					hidden: true,
					handler: function(){
						panelCompMapaLista.items.items[1].flex=0;
						panelCompMapaLista.items.items[0].flex=1;
						panelCompMapaLista.doLayout();
						Ext.getCmp('buttonmapcomps').setVisible(false);
						Ext.getCmp('buttonlistcomps').setVisible(true);
					}
				},{
					iconMask: true,
					id: 'buttonlistcomps',
					iconCls:'info',
					hidden: true,
					handler: function(){
						panelCompMapaLista.items.items[0].flex=0;
						panelCompMapaLista.items.items[1].flex=1;
						panelCompMapaLista.doLayout();
						Ext.getCmp('buttonmapcomps').setVisible(true);
						Ext.getCmp('buttonlistcomps').setVisible(false);
					}
				},{
					xtype:'spacer'
				},{
					xtype: 'selectfield',
					id:'pagingcomps',
					name: 'pagingcomps',
					width: 160,
					height: 80,
					options: pagings,
					listeners:{
						change: function(select, value){
							Ext.getCmp('listComp').getStore().loadPage(value);
						}
					}	
				}
			]
		});
		var actualizaMapaComparables= function(){
			if(tabletphone){
				panelCompMapaLista.items.items[0].flex=1;
				panelCompMapaLista.items.items[1].flex=0;
				panelCompMapaLista.doLayout();
				Ext.getCmp('buttonmapcomps').setVisible(false);
				Ext.getCmp('buttonlistcomps').setVisible(true);
			}
			if (markersArrayComps && markersArrayComps.length > 0) {
				for(j=0; j< markersArrayComps.length; j++){
					markersArrayComps[j].setMap(null);
				}
				markersArrayComps = [];
			}
			
			var zoom=15;
			var storeC = Ext.getCmp('listComp').getStore();
			pagingsComps = [];
			Ext.getCmp('pagingcomps').setOptions(pagingsComps);
			Ext.getCmp('pagingcomps').setValue('');
			Ext.getCmp('pagingcomps').getListPanel().setHeight(0);
			if(storeC.getCount()>0){
				var latAcum=0;
				var lonAcum=0;
				maxDis=0;
				for(i=0; i<storeC.getCount(); i++){
					var latLng = new google.maps.LatLng(storeC.data.items[i].data.LATITUDE, storeC.data.items[i].data.LONGITUDE);
					latAcum= latAcum+parseFloat(storeC.data.items[i].data.LATITUDE);
					lonAcum= lonAcum+parseFloat(storeC.data.items[i].data.LONGITUDE);
					if(storeC.data.items[i].data.Distance>maxDis){
						maxDis=storeC.data.items[i].data.Distance;
					}
					var urlIcon=devuelveIcon(storeC.data.items[i].data.status,storeC.data.items[i].data.pendes);
					var markerProp = new google.maps.MarkerImage(
							'img/houses/'+urlIcon,
							new google.maps.Size(32, 31),
							new google.maps.Point(0,0),
							new google.maps.Point(16, 31)
					)
					var marker = new MarkerWithLabel({
						map: mapComparables.map,
						position: latLng,
						icon: markerProp,
						labelContent: i+1,
						labelAnchor: new google.maps.Point(12, 25),
						labelClass: "labels"
					});
					markersArrayComps.push(marker);
					var img=storeC.data.items[i].data.image;
					var divimage='';
					var h,w;
					if(comparable!='distress'){
						divimage='<div style="float:left; margin-right:5px; height:110px; width:70px;overflow:hidden;">'+
								'	<img src="'+functionsTemplates.verificaImagen(img)+'" height="60px" width="70px;" style="position:relative; z-index:150;">'+
								'</div>';
						h=130;
						w=220;		
					}else{
						h=70;
						w=200;
					}	
					var content='<div style="height:'+h+'px; width:'+w+'px;">'+divimage;		
					if(comparable!='distress'){
						content+='<div style="font-size: 12px;">';
							if(comparable=='comps'){	
								content+=functionsTemplates.priceTitle(storeC.data.items[i].data.saleprice)+':  <span style="color:#60b31b; font-weight:bold; font-size:13px;">'+functionsTemplates.numberFormat(storeC.data.items[i].data.saleprice,"$","Y")+'</span></div>';
								content+='<div style="font-size: 12px;">'+functionsTemplates.dateTitle(storeC.data.items[i].data.saledate)+':  <span style="color:#61380B; font-weight:bold; font-size:13px;">'+functionsTemplates.dateFormat(storeC.data.items[i].data.saledate)+'</span></div>';
							}else{
								content+=functionsTemplates.priceTitle(storeC.data.items[i].data.lprice)+':  <span style="color:#60b31b; font-weight:bold; font-size:13px;">'+functionsTemplates.numberFormat(storeC.data.items[i].data.lprice,"$","Y")+'</span></div>';
								content+='<div style="font-size: 12px;">'+functionsTemplates.dateTitle(storeC.data.items[i].data.ldate)+':  <span style="color:#61380B; font-weight:bold; font-size:13px;">'+functionsTemplates.dateFormat(storeC.data.items[i].data.ldate)+'</span></div>';
							}
						content+='<div style="font-size: 12px;">'+
								' 	'+arrayTitulos.mlsresidential.dom+': '+storeC.data.items[i].data.dom+
								'</div>';
					}
					content+='<div style="font-size: 12px;">'+
								' 	<span style="color:#15428b; font-weight:bold;">'+storeC.data.items[i].data.address+'</span>, '+storeC.data.items[i].data.zip+
								'</div>'+
								'<div style="font-size: 12px;">'+
								'	'+storeC.data.items[i].data.beds+'/'+storeC.data.items[i].data.bath+', '+storeC.data.items[i].data.larea+' sqft, '+arrayTitulos.psummary.pool+': '+storeC.data.items[i].data.pool+
								'</div>'+
								'<div style="font-size: 12px;">'+
								' '+arrayTitulos.psummary.waterf+': '+storeC.data.items[i].data.waterf+
								'</div>'+
								'<div style="font-size: 12px;">'+
								''+functionsTemplates.typeForeclosure(storeC.data.items[i].data.pendes)+' '+ functionsTemplates.soldType(storeC.data.items[i].data.sold)+
								'</div></div>';
					attachMessageComp(marker, content);
					
				}
				
				//Paginacion
				var totalRegistros=storeC.proxy.reader.jsonData.total;
				var pages=totalRegistros/storeC.pageSize;
				if(pages<1){
					pages=1;
				}
				pagingsComps = [];
				var tam=storeC.pageSize;
				for(j=0; j<=pages; j++){
					var desde=(j*tam)+1;
					var hasta=(j*tam)+tam;
					if(hasta>totalRegistros){
						hasta=totalRegistros;
					}
					//pagings.push({text: desde+' to '+hasta,value: j+1});
					pagingsComps.push({text: 'Page '+(j+1),value: j+1});
				}
				Ext.getCmp('pagingcomps').setOptions(pagingsComps);
				var sizeList=0;
				if(pages<=6){
					sizeList=pages*50;
				}else{
					sizeList=300;
				}	
				Ext.getCmp('pagingcomps').getListPanel().setHeight(sizeList);
				Ext.getCmp('pagingcomps').setValue(storeC.currentPage);
				//Centrado del mapa
				var latAvg= latAcum/storeC.getCount();
				var lonAvg= lonAcum/storeC.getCount();
				var pnt = new google.maps.LatLng(latAvg, lonAvg);
				mapComparables.map.setCenter(pnt);
				var aumentar;
				if(tabletphone){
					aumentar=1;
				}else{
					aumentar=0;
				}
				if(maxDis<0.1){
					zoom=19-aumentar;
				}else if(maxDis>=0.1 && maxDis<0.17){
					zoom=18-aumentar;
				}else if(maxDis>=0.17 && maxDis<0.35){
					zoom=17-aumentar;
				}else if(maxDis>=0.35 && maxDis<0.70){
					zoom=16-aumentar;
				}else if(maxDis>=0.70 && maxDis<1.5){
					zoom=15-aumentar;
				}else if(maxDis>=1.5 && maxDis<2.5){
					zoom=14-aumentar;
				}else if(maxDis>=2.5 && maxDis<5){
					zoom=12-aumentar;
				}else{
					zoom=9-aumentar;
				}
			}
			mapComparables.map.setZoom(zoom);
				
		};
		Ext.getCmp('listComp').getStore().addListener('load', actualizaMapaComparables);
		//Mapa y lista de propiedades de Comparables
		panelComparables = new Ext.Panel({
			dockedItems:[toolbarTopComparables,toolbarBottomComps],
			items:[
				panelCompMapaLista
			]
		});
		//Panel principal de la aplicacion
		panelPrincipal = new Ext.Panel({
			fullscreen : true,
			layout: 'card',
			activeItem: 0,
			layoutOnOrientationChange: true,
			items:[
				panelMapa,panelSearch,panelOverview,panelReports,panelComparables
			]
		});
		function cambiaContenido(){
			width=screen.width;
			height=screen.height;
			phone=false;
			if(width<=600){
				store.pageSize=10;
				padding=20;
				tabla=250;
				phone=true;
			}else if(width>500 && width<=1000){
				store.pageSize=30;
				padding=200;
				tabla=600;
			}else{
				store.pageSize=50;
				padding=document.body.offsetWidth/4;
				tabla=document.body.offsetWidth/2;
			}
			tabletphone=true;
			if (Ext.is.Phone || Ext.is.Tablet || Ext.is.iPad){
				tabletphone=true;
			}
			/*if (){
				phone=false;
			}*/
			if (phone) {
				panel.items.items[1].flex=0;
				panel.items.items[0].flex=1;
				panel.doLayout();
				Ext.getCmp('buttonlist').setVisible(true);
			}
			if(tabletphone){
				panelCompMapaLista.items.items[1].flex=0;
				panelCompMapaLista.items.items[0].flex=1;
				panelCompMapaLista.doLayout();
				Ext.getCmp('buttonlistcomps').setVisible(true);
			}	
			panelCompMapaLista.doLayout();
		}
		cambiaContenido();
		//geolocation.updateLocation();
		getLocationGps();
	}
	
});
function llamaStore(a){
	var record=store.getAt(parseInt(a));
	panelOverview.items.get(0).update(null);
	panelOverview.items.get(1).update(null);
	Ext.Ajax.request({
		url:'php/response_data.php?type=public&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
		method: 'POST',
		success:function(response, opts){
			var data = Ext.decode(response.responseText);
			typeprop=data.ccode;
			panelOverview.items.get(0).update(data);
			Ext.getCmp('panelPropComp').update(data);
			if(markComparable!=null){
				markComparable.setMap(null);
			}
			markComparable = new google.maps.Marker({
				position: new google.maps.LatLng(record.get('latitude'),record.get('longitude')),
				title : 'Comparable',
				icon  : imageComparable,
				map: Ext.getCmp('mapComp').map
			});
			if(markProperty!=null){
				markProperty.setMap(null);
			}
			markProperty = new google.maps.Marker({
				position: new google.maps.LatLng(record.get('latitude'),record.get('longitude')),
				title : 'Overview',
				icon  : imageComparable,
				map: Ext.getCmp('mapOverview').map
			});
			Ext.getCmp('mapOverview').map.setCenter(new google.maps.LatLng(record.get('latitude'),record.get('longitude')));
			Ext.getCmp('mapOverview').map.setZoom(22);
			Ext.getCmp('mapOverview').map.setMapTypeId(google.maps.MapTypeId.HYBRID);
		}	
	});
	Ext.Ajax.request({
		url:'php/response_data.php?type=listing&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
		method: 'POST',
		success:function(response, opts){
			var data = Ext.decode(response.responseText);
			panelOverview.items.get(1).update(data);
		}	
	});
	if(login){
		Ext.getCmp('mortfore').setVisible(false);
		Ext.Ajax.request({
			url:'php/response_data.php?type=mortfore&county='+record.get('county')+'&parcelid='+record.get('parcelid'),
			method: 'POST',
			success:function(response, opts){
				var data = Ext.decode(response.responseText);
				if(data.pendes.length>0){
					Ext.getCmp('mortfore').setText('Foreclosure');
					Ext.getCmp('mortfore').setVisible(true);
				}else if(data.mortgage.length>0){
					Ext.getCmp('mortfore').setText('Mortgage');
					Ext.getCmp('mortfore').setVisible(true);
				}	
				panelOverview.items.get(5).update(data);
			}	
		});
	}
	countyName=record.get('county');
	if(record.get('tieneImg')=='Y'){
		Ext.getCmp('photos').setVisible(true);
	}else{
		Ext.getCmp('photos').setVisible(false);
	}
	parcelid=record.get('parcelid');
	Ext.getCmp('carousel').store.proxy.extraParams={county:county,parcelid:parcelid,width:widthImagen,height:heightImagen};
	Ext.getCmp('carousel').removeAll();
	Ext.getCmp('carousel').removeAll();
	panelPrincipal.setActiveItem(2);
	panelOverview.setActiveItem(0);
}