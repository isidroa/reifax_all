<?php
	function url_exists($url)
	{
		$url_info = parse_url($url);
		
		if (! isset($url_info['host']))
			return false;
		
		$port = (isset($url_info['post'])?$url_info['port']:80);
		
		if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr)) 
			return false;
		
		$uri = @$url_info['path'] . '?' . @$url_info['query'];
		
		$http = "HEAD $uri HTTP/1.1\r\n";
		$http .= "Host: {$url_info['host']}\r\n";
		$http .= "Connection: close\r\n\r\n";
		
		@fwrite($hwnd, $http);
		
		$response = fgets($hwnd);
		$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";
		
		fclose($hwnd);
		
		if (preg_match($response_match, $response, $matches)) {
			//print_r($matches);
			if ($matches[1] == 404)
				return false;
			else if ($matches[1] == 200)
				return true;
			else
				return false;
			 
		} else {
			return false;
		}
	}
	
	function getImage($parcelid){
		$query='select if(urlxima is null or length(urlxima)<5,NULL,concat(urlxima,parcelid,".",tipo)) imagenxima,
		if(urlxima is null or length(urlxima)<5,NULL,concat(urlxima,"L",parcelid,".",tipo)) imagenthumbnail,
		concat(url,url2,if(letra="Y",mlnumber,substring(mlnumber,2)),".",tipo) imagen, tipo, parcelid, mlnumber, thumbnail,urlxima 
		FROM '.$db_data.'.imagenes WHERE parcelid=\''.$parcelid.'\'';
		$result=mysql_query($query) or die($query.mysql_error());
		//echo $query;
		if(mysql_num_rows($result)>0){
			$r=mysql_fetch_array($result);
			if(is_null($r['imagenxima']) || strlen($r['imagenxima'])<5){
				/*if(url_exists($r['imagen']))
					//return $r['imagen'];
					return createThumb($r['mlnumber'].'.'.$r['tipo'], $r['imagen'], '../img/tmp/',$r['tipo'],60,'L');
				else*/
				return '';
			}else{
				//return $r['imagenxima'];
				//return createThumb($r['parcelid'].'.'.$r['tipo'], $r['imagenxima'], '../img/tmp/',$r['tipo'],60,'L');
				if($r['thumbnail']=='Y'){
				//if(url_exists($r['imagenthumbnail'])){
					//return $r['urlxima'].'L'.$r['parcelid'].'.'.$r['tipo'];
					return $r['imagenthumbnail'];
				}else{
					return '';
				}
			}	
		}else{
			return '';
		}
	}
	
	function createThumb($img_file, $ori_path, $thumb_path, $extension, $tam, $prefix){
		$arrayTypes=Array('jpg'=>'image/jpeg','png'=>'image/png','x-png'=>'image/x-png','gif'=>'image/gif');
		$img_type=$arrayTypes[$extension];
		// get the image source
		$path = $ori_path;
		$img = $path; //.$img_file;
		switch ($img_type) {
			case "image/jpeg":case "image/pjpeg":
				$img_src = @imagecreatefromjpeg($img);
				break;
			case "image/png":
				$img_src = @imagecreatefrompng($img);
				break;
			case "image/x-png":
				$img_src = @imagecreatefrompng($img);
				break;
			case "image/gif":
				$img_src = @imagecreatefromgif($img);
				break;
		}
		$img_width = @imagesx($img_src);
		$img_height = @imagesy($img_src);

		// check width, height, or square
		if ($img_width == $img_height) {
			// square
			$tmp_width = $tam;
			$tmp_height = $tam;
		} else if ($img_height < $img_width) {
			// wide
			$tmp_height = $tam;
			$tmp_width = intval(($img_width / $img_height) * $tam);
			if ($tmp_width % 2 != 0) {
				$tmp_width++;
			}
		} else if ($img_height > $img_width) {
			$tmp_width = $tam;
			$tmp_height = intval(($img_height / $img_width) * $tam);
			if ($tmp_height % 2 != 0) {
				$tmp_height++;
			}
		}
		
		//Crea los resources de la imagen nueva
		$img_new = imagecreatetruecolor($tmp_width, $tmp_height);
		@imagecopyresampled($img_new, $img_src, 0, 0, 0, 0,
				$tmp_width, $tmp_height, $img_width, $img_height);
		
		//Nombre de la imagen creada		
		$thumb = $thumb_path.$prefix.$img_file;
		
		//Crea la imagen de acuerdo al tipo
		switch ($img_type) {
			case "image/jpeg": case "image/pjpeg":
				@imagejpeg($img_new, $thumb);
				break;
			case "image/png":
				@imagepng($img_new, $thumb);
				break;
			case "image/x-png":
				@imagepng($img_new, $thumb);
				break;
			case "image/gif":
				@imagegif($img_new, $thumb);
				break;
		}
		$img64 = base64_encode(file_get_contents($thumb));
		//echo '<img src="data:image/png;base64,'.$img64.'" alt="Descripción"/>';
		//Eliminar imagen
		unlink($thumb);
		// Liberar memoria
		imagedestroy($img_new);
		return $img64;
	}
?>