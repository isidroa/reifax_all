<?php
	include("conexion.php");
	function porcentaje($val){
		return number_format($val, 2, '.', '');
	}
	$pid=$_REQUEST['parcelid'];
	$distance=$_REQUEST['distance'];
	$county=$_REQUEST['county'];
	$width=$_REQUEST['width'];
	$padding=$_REQUEST['padding'];
	$bd_search='`'.conectarPorIdCounty($county).'`';
	
	$res=mysql_query("select p.xcode,l.latitude,l.longitude FROM $bd_search.psummary p LEFT JOIN $bd_search.latlong l ON (p.parcelid=l.parcelid) 
					  WHERE p.parcelid='$pid'") or die(mysql_error());
	$r=mysql_fetch_array($res);
	$lat=$r['latitude'];
	$lon=$r['longitude'];
	$proper=$r['xcode'];
	
	$_calculo="sqrt((69.1*(ll.latitude-$lat))*(69.1*(ll.latitude-$lat))+(69.1*((ll.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((ll.longitude-($lon))*cos($lat/57.29577951))))";
	
	$rebatequery=" p.xcode='$proper' AND $_calculo<=".$distance;
	
	$SPS=$CPS=$SFS=$CFS=0;
	$SPSAD=$CPSAD=$SPSAR1=$SPSAR2=$CPSAR1=$CPSAR2=0.00;
	$SFSADF=$CFSADF=$SFSADJ=$CFSADJ=$SFSAR1=$SFSAR2=$CFSAR1=$CFSAR2=0.00;

	//Pre-Foreclosed
	$query="SELECT count(*) FROM $bd_search.marketvalue m INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) WHERE m.pendes='P' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CPS=$r[0];
	
	$query="SELECT count(*) FROM $bd_search.marketvalue m INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) LEFT JOIN $bd_search.latlong ll ON (m.parcelid=ll.parcelid) 
	WHERE m.pendes='P' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SPS=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(p.file_date,'%Y%m%d')))/count(*) 
			FROM $bd_search.pendes p
			INNER JOIN $bd_search.psummary ps ON (p.parcelid=ps.parcelid)
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			WHERE m.pendes='P' and m.sold='S' and length(p.file_date)>0 and (m.totalpenmort-ps.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CPSAD=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.file_date,'%Y%m%d')))/count(*) 
			FROM $bd_search.pendes pe
			INNER JOIN $bd_search.marketvalue m ON (pe.parcelid=m.parcelid)
			INNER JOIN $bd_search.psummary p ON (pe.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (pe.parcelid=ll.parcelid)
			WHERE m.pendes='P' and m.sold='S' and length(pe.file_date)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SPSAD=$r[0];
	
	$query="SELECT sum(m.totalpenmort)/count(*)
			FROM $bd_search.marketvalue m 
			INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid)
			WHERE m.pendes='P' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CPSAR1=$r[0];
	
	$query="SELECT sum(m.totalpenmort)/count(*)
			FROM $bd_search.marketvalue m 
			INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (m.parcelid=ll.parcelid)
			WHERE m.pendes='P' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SPSAR1=$r[0];
	
	$query="SELECT sum(p.saleprice)/count(*)
			FROM $bd_search.psummary p
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			WHERE m.pendes='P' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CPSAR2=$r[0];
	
	$query="SELECT sum(p.saleprice)/count(*)
			FROM $bd_search.psummary p
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (p.parcelid=ll.parcelid)
			WHERE m.pendes='P' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SPSAR2=$r[0];
	
	//Foreclosed
	$query="SELECT count(*) FROM $bd_search.marketvalue m INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) WHERE m.pendes='F' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CFS=$r[0];
	
	$query="SELECT count(*) FROM $bd_search.marketvalue m 
			INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (m.parcelid=ll.parcelid)
			WHERE m.pendes='F' and m.sold='S' and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SFS=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(p.file_date,'%Y%m%d')))/count(*)
			FROM $bd_search.pendes p
			INNER JOIN $bd_search.psummary ps ON (p.parcelid=ps.parcelid)
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			WHERE m.pendes='F' and m.sold='S' and length(p.file_date)>0 and (m.totalpenmort-ps.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CFSADF=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.file_date,'%Y%m%d')))/count(*)
			FROM $bd_search.pendes pe
			INNER JOIN $bd_search.marketvalue m ON (pe.parcelid=m.parcelid)
			INNER JOIN $bd_search.psummary p ON (pe.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (pe.parcelid=ll.parcelid)
			WHERE m.pendes='F' and m.sold='S' and length(pe.file_date)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SFSADF=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(p.judgedate,'%Y%m%d')))/count(*)
			FROM $bd_search.pendes p
			INNER JOIN $bd_search.psummary ps ON (p.parcelid=ps.parcelid)
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			WHERE m.pendes='F' and m.sold='S' and length(p.judgedate)>0 and (m.totalpenmort-ps.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CFSADJ=$r[0];
	
	$query="SELECT sum(datediff(NOW(),STR_TO_DATE(pe.judgedate,'%Y%m%d')))/count(*)
			FROM $bd_search.pendes pe
			INNER JOIN $bd_search.marketvalue m ON (pe.parcelid=m.parcelid)
			INNER JOIN $bd_search.psummary p ON (pe.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (pe.parcelid=ll.parcelid)
			WHERE m.pendes='F' and m.sold='S' and length(pe.judgedate)>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SFSADJ=$r[0];
	
	$query="SELECT sum(m.totalpenmort)/count(*)
			FROM $bd_search.marketvalue m
			INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid)
			WHERE m.pendes='F' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CFSAR1=$r[0];
	
	$query="SELECT sum(m.totalpenmort)/count(*)
			FROM $bd_search.marketvalue m
			INNER JOIN $bd_search.psummary p ON (m.parcelid=p.parcelid) 
			LEFT JOIN $bd_search.latlong ll ON (m.parcelid=ll.parcelid)
			WHERE m.pendes='F' and m.sold='S' and m.totalpenmort>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SFSAR1=$r[0];
	
	$query="SELECT sum(p.saleprice)/count(*)
			FROM $bd_search.psummary p
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			WHERE m.pendes='F' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0";
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$CFSAR2=$r[0];
	
	$query="SELECT sum(p.saleprice)/count(*)
			FROM $bd_search.psummary p
			INNER JOIN $bd_search.marketvalue m ON (p.parcelid=m.parcelid)
			LEFT JOIN latlong ll ON (p.parcelid=ll.parcelid)
			WHERE m.pendes='F' and m.sold='S' and p.saleprice>0 and (m.totalpenmort-p.saleprice) > 0 AND".$rebatequery;
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	$SFSAR2=$r[0];
	
	$html.="<div id=\"report_content\" style=\"text-align: center;\">";
	$html.="<span align=\"center\" style=\"font-size: 20px; font-weight:bold;\">DISCOUNT REPORT</span>";
	$html.="<div style=\"font-family:Trebuchet MS,Arial,Helvetica,sans-serif;\"><table align=\"center\" cellpadding=0 cellspacing=0 style=\"font-family:Trebuchet MS,Arial,Helvetica,sans-serif; font-size:14px;\"><tr class=mortcelltitulo style=\"font-weight:bold\"><td colspan=2 align=center>Subject Area</td><td align=center>County Area</td></tr><tr class=mortcelltitulo style=\"font-weight:bold\"><td align=center></td><td align=center style=\"width:90\"><strong>Quantity</strong></td><td align=center style=\"width:120\"><strong>Quantity</strong></td></tr><tr><td >Pre-Foreclosed Sold</td><td align=right>$SPS</td><td align=right>$CPS</td></tr><tr><td >Average Days of Shortsale</td><td align=right>".porcentaje($SPSAD)."</td><td align=right>".porcentaje($CPSAD)."</td></tr><tr><td >Average Amount of Discount</td><td align=right>".porcentaje($SPSAR1-$SPSAR2)."</td><td align=right>".porcentaje($CPSAR1-$CPSAR2)."</td></tr><tr><td >Average Percentage of Discount</td><td align=right>"; 
			if($SPSAR1>0) 
				$html.= porcentaje(100-(($SPSAR2/$SPSAR1)*100))."%"; 
			else 
				$html.= "0.00%";
			$html.="</td><td align=right>"; 
				
			if($CPSAR1>0) 
				$html.=porcentaje(100-(($CPSAR2/$CPSAR1)*100))."%"; 
			else 
				$html.="0.00%";
			
			$html.="</td></tr><tr class=mortcelltitulo style=\"height:2px\"><td colspan=3 align=right>&nbsp;</td></tr><tr><td >Foreclosed Sold</td><td align=right>$SFS</td><td align=right>$CFS</td></tr><tr><td >Average Days of Sale After File Date</td><td align=right>".porcentaje($SFSADF)."</td><td align=right>".porcentaje($CFSADF)."</td></tr><tr><td >Average Days of Sale After Judgement Date (Auction)</td><td align=right>".porcentaje($SFSADJ)."</td><td align=right>".porcentaje($CFSADJ)."</td></tr><tr><td >Average Amount of Discount</td><td align=right>".porcentaje($SFSAR1-$SFSAR2)."</td><td align=right>".porcentaje($CFSAR1-$CFSAR2)."</td></tr><tr><td >Average Percentage of Discount</td><td align=right>"; 
			if($SFSAR1>0) 
				$html.=porcentaje(100-(($SFSAR2/$SFSAR1)*100))."%"; 
			else 
				$html.="0.00%";
			
			$html.="</td><td align=right>"; 
			
			if($CFSAR1>0) 
				$html.=porcentaje(100-(($CFSAR2/$CFSAR1)*100))."%"; 
			else 
				$html.="0.00%";
				
			$html.="</td></tr><tr class=mortcelltitulo style=\"height:2px\"><td colspan=3 align=right>&nbsp;</td></tr></table></div>";
	$html.="</div>";
	echo '{"html":"'.addslashes($html).'"}';
?>