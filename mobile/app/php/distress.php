<?php
	include("conexion.php");
	$bpoaction=true;
	$el_comparado=$_REQUEST["id"];
	$county=$_REQUEST['county'];
	conectarPorNameCounty($county);
	$orderby=$_REQUEST['orderby'];
	$orderdirection=$_REQUEST['orderdirection'];
	if($orderby!=''){
		$sortcomparable="ORDER BY $orderby $orderdirection";
	}else{
		$sortcomparable='';
	}
	include ("comparables/properties_getgridcamptit.php");

	$distance_0_5="0.5";
	$factor_latlong=0.015;
	
	//Distance
	$query='select filter_distress_distance FROM xima.xima_system_var WHERE userid='.$_COOKIE['datos_usr']["USERID"];
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);
	if($r['filter_distress_distance']>0){
		$distance_0_5=(float) $r['filter_distress_distance'];
	}	

	   // Query the table
	   $query = "select 
	   psummary.parcelid,
	   psummary.sparcel,
	   'CC' as status,
	   psummary.Lsqft, 
	   psummary.SalePrice,
	   psummary.xcode as propType,
	   ll.latitude,
	   ll.longitude
	   From psummary , latlong as ll
	   Where psummary.parcelid='$el_comparado' and psummary.parcelid=ll.parcelid;";
					
	$result = mysql_query($query) or die($query.mysql_error());
	$row= mysql_fetch_array($result, MYSQL_ASSOC);
	$lat=$row["latitude"];
	$lon=$row["longitude"];
	$latmax=$lat+$factor_latlong;
	$latmin=$lat-$factor_latlong;
	$lonmax=$lon+$factor_latlong;
	$lonmin=$lon-$factor_latlong;
	$sparcel = $row["sparcel"];
	$proper = $row['propType'];
	mysql_free_result($result);	
	//***********************************************************************************************	
	$data_arr=array();//se guarda lo calculado
	$array_parcelIds=array();//se guardan todos los parcelids para ser usados como filtro en los sub-siguientes selects
	$MainData=array();//se aloja toda la data q sera vaciada en el grid

	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
	$ArIDCT = getArray('distress','comparables',false);

	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);

	$orderby='';
	$limit='';
	$jointable=array('`pendes`','`properties_php`','`psummary`','`latlong`','`marketvalue`','`diffvalue`');
	$campos='p.parcelid as pid, p.status as status, p.latitude as LATITUDE, p.longitude as LONGITUDE, p.address as address, `psummary`.lsqft as lsqft, `psummary`.bheated as larea, p.beds as beds, p.bath as bath, p.price as saleprice, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip';

	foreach($hdArray as $k => $val){
		if(array_search('`'.$val->tabla.'`',$jointable)===false)
			$jointable[]='`'.$val->tabla.'`';
		$campos.=', `'.$val->tabla.'`.'.$val->name;
		
		if(isset($_POST['sort']) && ($val->name==$_POST['sort'])){
			$orderby=' ORDER BY `'.$val->tabla.'`.'.$val->name.' '.$_POST['dir'];
		}
	}
	if(!isset($_POST['sort'])) $orderby=' ORDER BY Distance ASC';
	if(isset($_POST['sort']) && ($_POST['sort']=='Distance' || $_POST['sort']=='status')) $orderby=' ORDER BY Distance '.$_POST['dir'];

	if(isset($_POST['limit'])) $limit=' LIMIT '.$_POST['start'].', '.$_POST['limit'];

	//============================================================================
	$_calculo="truncate(sqrt((69.1* (`latlong`.latitude- $lat))*(69.1*(`latlong`.latitude-$lat))+(69.1*((`latlong`.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((`latlong`.longitude-($lon))*cos($lat/57.29577951)))),2)";


	$sql_p_records="SELECT ".$campos.", $_calculo as Distance FROM ";
	foreach($jointable as $k => $val){
		if($k==0) $sql_p_records.="$val ";
		elseif($val=='`properties_php`') $sql_p_records.="LEFT JOIN $val p ON (".$jointable[0].".parcelid=p.parcelid) ";
		else $sql_p_records.="LEFT JOIN $val ON (".$jointable[0].".parcelid=$val.parcelid) ";
	}

	$sql_p_records.=" WHERE `psummary`.parcelid<>'$el_comparado' AND `psummary`.xcode='$proper' AND (`latlong`.LATITUDE>=".$latmin." and `latlong`.LATITUDE<=".$latmax." and `latlong`.LONGITUDE>=".$lonmin." and `latlong`.LONGITUDE<=".$lonmax.") AND `marketvalue`.pendes<>'N' AND `marketvalue`.sold<>'S' $_newFilter $arrtake";

	$sql_p_records.=$sortcomparable;


	if($_POST['array_taken']!='' && $_POST['array_taken']!="''"){
		$sql_p_records="SELECT ".$campos.", $_calculo as Distance FROM ";
		foreach($jointable as $k => $val){
			if($k==0) $sql_p_records.="$val ";
			elseif($val=='`properties_php`') $sql_p_records.="LEFT JOIN $val p ON (".$jointable[0].".parcelid=p.parcelid) ";
			else $sql_p_records.="LEFT JOIN $val ON (".$jointable[0].".parcelid=$val.parcelid) ";
		}
		
		$sql_p_records.=" WHERE `psummary`.parcelid IN (".$_POST['array_taken'].")";
		$sql_p_records.=$orderby;
	}

	//echo $sql_p_records; 

	$result = mysql_query($sql_p_records) or die($sql_p_records.' - '.$_newFilter.mysql_error());
	//=====================================================================

	$num_rows=0;
	$xmls="";
	$_quitar = array("'", '"');

	$array_parcelIds=array();//se guardan todos los parcelids para ser usados como filtro en los sub-siguientes selects
	$array_datos=array();
	if($lat>0){
		while($row=mysql_fetch_array($result, MYSQL_ASSOC))
		{
			if($row["pid"]!=$el_comparado && $row['Distance']<=$distance_0_5){	//Para arreglar el Bug de hace aparecer el comparado en el grid -- 08/02/2008
			
				$lat=$row["xlat"];$lon=$row["xlong"];
				if(is_null($lat)) $lat=0;if(is_null($lon)) $lon=0;

				$asignar=true;
				//guardamos el array de datos en nuestro array - requeirdo para [Maria Olivera]
				if($maplatlong!=0){
					if(count($maplatlong)>2){
						if($lat>0 && _pointINpolygon($arrPoly,$lat,$lon)){
							array_push($array_parcelIds,$row["pid"]);
						}else{
							$asignar=false;
						}
					}else{
						array_push($array_parcelIds,$row["pid"]);
					}
				}else{
					array_push($array_parcelIds,$row["pid"]);
				}
				//////////////////////////////////////
				
				if (strlen($lat)>0 && $asignar)	
					$array_datos[]=$row;	 
						
			}
		}
	}else{
		echo "ERROR";
	}
	mysql_free_result($result);
	
	$num_rows_all=count($array_datos);
	$limit=$_REQUEST['limit'];
	$start=$_REQUEST['start'];
	if($start==''){
		$start=0;
	}
	$array_datos_limit=array_slice($array_datos,$start,$limit);
	//$array_datos_limit=$array_datos;
	unset($array_datos);
	
	$return="{\"total\":\"".$num_rows_all."\",\"results\": [";
	foreach($array_datos_limit as $k=>$val){
		if($k>0) $return.=",";
		$return.="{";
		$m=0;
		foreach($val as $l=>$val2){
			if($m>0) $return.=",";
			if($l=='ozip')
				$return.="\"zip\": \"$val2\"";
			else if($l=='parcelid'){
				$return.="\"$l\": \"$val2\"";
				$img=getImage($val2);
				$return.=",\"image\": \"$img\"";
			}else	
				$return.="\"$l\": \"$val2\"";
			$m++;
		}
		$return.="}";
	}
	
	$return.="]}";
	unset($array_datos_limit);
	unset($hdArray);

	echo $return;
?>