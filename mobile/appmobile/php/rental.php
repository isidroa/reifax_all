<?php
	include("conexion.php");
	$bpoaction=true;
	$el_comparado=$_REQUEST["id"];
	$county=$_REQUEST['county'];
	
	conectarPorNameCounty($county);
	$orderby=$_REQUEST['orderby'];
	$orderdirection=$_REQUEST['orderdirection'];
	if($orderby!=''){
		$sortcomparable="ORDER BY $orderby $orderdirection";
	}else{
		$sortcomparable='';
	}
	include ("comparables/properties_getgridcamptit.php");
	
	$beds=array();
	$bath=array();
	$pool=array();
	$wf=array();
	
	$curparcel=substr($el_comparado,0,6);
	$proper=$_REQUEST['prop'];
	$factor_latlong=0.015;
	
	$query='select filter_rental_distance,filter_rental_beds,filter_rental_bedscon,filter_rental_baths,filter_rental_bathscon,
	filter_rental_garea,filter_rental_garea_from,filter_rental_garea_to,filter_rental_larea,filter_rental_larea_from,filter_rental_larea_to,
	filter_rental_pool,filter_rental_waterf,filter_rental_built
	FROM xima.xima_system_var WHERE userid='.$_COOKIE['datos_usr']["USERID"];
	$result=mysql_query($query) or die($query.mysql_error());
	$r=mysql_fetch_array($result);

	$distance=(float) $r['filter_rental_distance'];
	$beds=(integer) $r['filter_rental_beds'];
	$cond_beds=$r['filter_rental_bedscon'];
	$baths=(integer) $r['filter_rental_baths'];
	$cond_baths=$r['filter_rental_bathscon'];
	$lsqft=(integer) $r['filter_rental_garea'];
	$larea=(integer) $r['filter_rental_larea'];
	$pool=(integer) $r['filter_rental_pool'];
	$wf=(integer) $r['filter_rental_waterf'];
	$Garea_From=(integer) $r['filter_rental_garea_from'];
	$Garea_To=(integer) $r['filter_rental_garea_to'];
	$Larea_From=(integer) $r['filter_rental_larea_from'];
	$Larea_To=(integer) $r['filter_rental_larea_to'];
	$yb=(integer) $r['filter_rental_built'];
	$maplatlong=0;

	$query="Select
		psummary.lsqft,
		psummary.yrbuilt,
		psummary.bheated,
		latlong.latitude,
		latlong.longitude
		FROM psummary
		left join latlong on (psummary.parcelid=latlong.parcelid) 
		WHERE psummary.parcelid='$el_comparado'";
	//echo $query;

	$resp=mysql_query($query) or die($query.mysql_error());
	$r = mysql_fetch_array($resp);

	//print_r($r);

	$lat=$r['latitude'];
	$lon=$r['longitude'];
	$lsqft1=$r['lsqft'];
	$larea1=$r['bheated'];
	$com_year=$r['yrbuilt'];
	$latmax=$lat+$factor_latlong;
	$latmin=$lat-$factor_latlong;
	$lonmax=$lon+$factor_latlong;
	$lonmin=$lon-$factor_latlong;
	
	$filters.="";

	if($lsqft>0){
		$div=round($lsqft/100,2);

		$_lsqft=$lsqft1+($lsqft1*$div);
		$_lsqft1=$lsqft1-($lsqft1*$div);

		$filters.=" and (`rental`.lsqft>".$_lsqft1." and `rental`.lsqft<".$_lsqft.")";
	}elseif($Garea_From>0 && $Garea_To>0){
		$filters.="  AND (`rental`.lsqft>=".$Garea_From." and `rental`.lsqft<=".$Garea_To." and `rental`.lsqft>0) ";
	}

	if($larea>0){
		$div=round($larea/100,2);

		$_larea=$larea1+($larea1*$div);
		$_larea1=$larea1-($larea1*$div);

		$filters.=" and (`mlsresidential`.larea>".$_larea1." and `mlsresidential`.larea<".$_larea.")";
	}elseif($Larea_From>0 && $Larea_To>0){
		$filters.="  AND (`mlsresidential`.larea>=".$Larea_From." and `mlsresidential`.larea<=".$Larea_To." and `mlsresidential`.larea>0) ";
	}

	if (($yb>0) && ($com_year>0)){ 
		$filters.=" AND ((`rental`.yrbuilt>=($com_year-$yb)) and (`rental`.yrbuilt<=($com_year+$yb)))"; 
	}

	if($beds>0){
		$filters.=" and `rental`.beds $cond_beds $beds ";
	}

	if($baths>0){
		$filters.=" and `rental`.bath $cond_baths $baths ";
	}

	if($pool>=0){
			switch($pool){
			case 1: $filters.=" and `rental`.pool='Y' ";
			break;
			case 0: $filters.=" and `rental`.pool='N' ";
			break;
		}
	}

	if($wf>=0){
		switch($wf){
			case 1: $filters.=" and `rental`.waterf='Y' ";
			break;
			case 0: $filters.=" and `rental`.waterf='N' ";
			break;
		}
	}

	if($distance>0){
		$dist=$distance;
	}else{
		$dist=0.5;
	}
	
	//***********************************************************************************************	
	$data_arr=array();//se guarda lo calculado
	$array_parcelIds=array();//se guardan todos los parcelids para ser usados como filtro en los sub-siguientes selects
	$MainData=array();//se aloja toda la data q sera vaciada en el grid

	$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
	$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
	$ArIDCT = getArray('rental','comparables',false);

	$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
	$hdArray=str_replace(  "'",'"', $hdArray);	
	$hdArray   = json_decode($hdArray);

	$orderby='';
	$limit='';
	$jointable=array('`rental`','`properties_php`','`mlsresidential`','`latlong`','`marketvalue`','`diffvalue`');
	$campos='p.parcelid as pid, p.status as status, p.latitude as LATITUDE, p.longitude as LONGITUDE, p.address as address, `psummary`.lsqft as lsqft, `psummary`.bheated as larea, p.beds as beds, p.bath as bath, p.price as saleprice, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip, `rental`.ldate as ldate, `mlsresidential`.dom as dom';

	foreach($hdArray as $k => $val){
		if(array_search('`'.$val->tabla.'`',$jointable)===false)
			$jointable[]='`'.$val->tabla.'`';
		$campos.=', `'.$val->tabla.'`.'.$val->name;
		
		if(isset($_POST['sort']) && ($val->name==$_POST['sort'])){
			$orderby=' ORDER BY `'.$val->tabla.'`.'.$val->name.' '.$_POST['dir'];
		}
	}
	if(!isset($_POST['sort'])) $orderby=' ORDER BY Distance ASC';
	if(isset($_POST['sort']) && ($_POST['sort']=='Distance' || $_POST['sort']=='status')) $orderby=' ORDER BY Distance '.$_POST['dir'];

	if(isset($_POST['limit'])) $limit=' LIMIT '.$_POST['start'].', '.$_POST['limit'];

	//============================================================================
	$_calculo="truncate(sqrt((69.1* (`latlong`.latitude- $lat))*(69.1*(`latlong`.latitude-$lat))+(69.1*((`latlong`.longitude-($lon))*cos($lat/57.29577951)))*(69.1*((`latlong`.longitude-($lon))*cos($lat/57.29577951)))),2)";

	$sql="SELECT ".$campos.", $_calculo as Distance FROM ";
	foreach($jointable as $k => $val){
		if($k==0) $sql.="$val ";
		elseif($val=='`properties_php`') $sql.="LEFT JOIN $val p ON (".$jointable[0].".parcelid=p.parcelid) ";
		else $sql.="LEFT JOIN $val ON (".$jointable[0].".parcelid=$val.parcelid) ";
	}

	$sql.=" WHERE `rental`.parcelid<>'$el_comparado'
	AND p.xcode='$proper' 
	AND (`latlong`.LATITUDE>=".$latmin." and `latlong`.LATITUDE<=".$latmax." and `latlong`.LONGITUDE>=".$lonmin." and `latlong`.LONGITUDE<=".$lonmax.")
	$filters";

	$sql.=$sortcomparable;
	
	$result = mysql_query($sql) or die($sql." ".mysql_error());


	//===================================================================================

	$lat=0;
	$lon=0;
	$num_rows=0;
	$xmls="";
	$_quitar = array("'", '"');

	$array_parcelIds=array();//se guardan todos los parcelids para ser usados como filtro en los sub-siguientes selects
	$array_datos=array();

	while($row=mysql_fetch_array($result, MYSQL_ASSOC))
	{
		if($row["pid"]!=$el_comparado && $row['Distance']<=$dist){	//Para arreglar el Bug de hace aparecer el comparado en el grid -- 08/02/2008
		
			$lat=$row["pin_xlat"];$lon=$row["pin_xlong"];
			if(is_null($lat)) $lat=0; if(is_null($lon)) $lon=0;

			$asignar=true;
			//guardamos el array de datos en nuestro array - requeirdo para [Maria Olivera]
			if($maplatlong!=0){
				if(count($maplatlong)>2){
					if($lat>0 && _pointINpolygon($arrPoly,$lat,$lon)){
						array_push($array_parcelIds,$row["pid"]);
					}else{
						$asignar=false;
					}
				}else{
					array_push($array_parcelIds,$row["pid"]);
				}
			}else{
				array_push($array_parcelIds,$row["pid"]);
			}
			//////////////////////////////////////
			
			if (strlen($lat)>0 && $asignar)	
				$array_datos[]=$row;	 
		}

	}
	mysql_free_result($result);
	
	$num_rows_all=count($array_datos);
	$limit=$_REQUEST['limit'];
	$start=$_REQUEST['start'];
	if($start==''){
		$start=0;
	}
	$array_datos_limit=array_slice($array_datos,$start,$limit);
	//$array_datos_limit=$array_datos;
	unset($array_datos);
	
	$return="{\"total\":\"".$num_rows_all."\",\"results\": [";
	foreach($array_datos_limit as $k=>$val){
		if($k>0) $return.=",";
		$return.="{";
		$m=0;
		foreach($val as $l=>$val2){
			if($m>0) $return.=",";
			if($l=='ozip')
				$return.="\"zip\": \"$val2\"";
			else if($l=='parcelid'){
				$return.="\"$l\": \"$val2\"";
				$img=getImage($val2);
				$return.=",\"image\": \"$img\"";
			}else	
				$return.="\"$l\": \"$val2\"";
			$m++;
		}
		$return.="}";
	}
	
	$return.="]}";
	unset($array_datos_limit);
	unset($hdArray);

	echo $return;
?>