<?php
	include("conexion.php");
	
	$pid=$_REQUEST['parcelid'];
	$county=$_REQUEST['county'];
	$width=$_REQUEST['width'];
	$height=$_REQUEST['height'];
	$bd_search='`'.conectarPorIdCounty($county).'`';
	
	function url_exists($url)
	{
		$url_info = parse_url($url);
		
		if (! isset($url_info['host']))
			return false;
		
		$port = (isset($url_info['post'])?$url_info['port']:80);
		
		if (! $hwnd = @fsockopen($url_info['host'], $port, $errno, $errstr)) 
			return false;
		
		$uri = @$url_info['path'] . '?' . @$url_info['query'];
		
		$http = "HEAD $uri HTTP/1.1\r\n";
		$http .= "Host: {$url_info['host']}\r\n";
		$http .= "Connection: close\r\n\r\n";
		
		@fwrite($hwnd, $http);
		
		$response = fgets($hwnd);
		$response_match = "/^HTTP\/1\.1 ([0-9]+) (.*)$/";
		
		fclose($hwnd);
		
		if (preg_match($response_match, $response, $matches)) {
			//print_r($matches);
			if ($matches[1] == 404)
				return false;
			else if ($matches[1] == 200)
				return true;
			else
				return false;
			 
		} else {
			return false;
		}
	}
	
	$sql_fields="Select
	*  
	FROM $bd_search.imagenes 
	Where parcelid='$pid';";
	
	$result = mysql_query($sql_fields) or die(mysql_error());
	$fila= mysql_fetch_array($result, MYSQL_ASSOC);
	if(mysql_num_rows($result)>0){
		$arr_pics="";
		
		if($fila["letra"]=='Y')
			$mlnumber=$fila["mlnumber"];
		else
			$mlnumber=substr($fila["mlnumber"],1);
		
		if(strlen($fila["urlxima"])>5){
			$arr_pics.=$fila["urlxima"].$fila["parcelid"].".".$fila['tipo'];
			if($fila['nphotos']>1){	
				for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
					if(url_exists($fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo']))
						$arr_pics.=",".$fila["urlxima"].$fila["parcelid"].$fila['sep'].$i.".".$fila['tipo'];
					elseif(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
						$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
				}
			}
		}else{
			if(url_exists($fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'])){
				$arr_pics.=$fila['url'].$fila['url2'].$mlnumber.".".$fila['tipo'];
				if($fila['nphotos']>1){	
					for($i=$fila['inicount']; $i<=$fila['nphotos']; $i++){
						if(url_exists($fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo']))
							$arr_pics.=",".$fila["url"].$fila['url3'].$mlnumber.$fila['sep'].$i.".".$fila['tipo'];
					}
				}
			}else{
				$arr_pics.="img/nophoto.gif";
			}	
		}
	}
	mysql_free_result($result);
	
	$arr_data=explode(",",$arr_pics);
	
	//print_r($arr_data);
	
	$results = array();
	foreach($arr_data as $k => $img){
		$html="<center><div><img src=\"$img\" style=\"width: $width; height: $height;\"></img></div></center>";
		$results[] = array("id" => $k, "title" => "", "html" => $html);
	}
	
	echo json_encode(array("success" => true, "total" => count($results), "cards" => $results));
?>