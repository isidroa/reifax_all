<?php
include("php/conexion.php");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>ReiFax - Register</title>
		<link rel="stylesheet" href="/MANT/LIB/sencha-touch/resources/css/sencha-touch.css" type="text/css">
		<link rel="stylesheet" href="css/mobile-theme.css" type="text/css">
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/favicon.png">
		<script type="text/javascript" src="/MANT/LIB/sencha-touch/sencha-touch.js"></script>
		<script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </head>
    <body>
		<script>
			function nuevoAjax(){
				var xmlhttp=false;
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
					}catch(e){
						try{
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}catch(E){
							xmlhttp = false;
						}
					}
					if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
						xmlthttp = new XMLHttpRequest();
					}
					return xmlhttp;
			}
			var email='';
			var password='';
			var product='';
			var phone='';
			var holdername='';
			var cardtype='';
			var cardnumber='';
			var monthexpiration='';
			var yearexpiration='';
			var csvnumber='';
			var address='';
			var city='';
			var zipcode='';
			var estado='';
			var terms='';
			var validacion=false;
			//paso a https pagina de seguridad.
			if(document.URL.substr(0,5)=='http:')document.location=document.URL.substr(0,4)+'s'+document.URL.substr(4);
			Ext.setup({
				onReady: function(){
					var panelLoading = new Ext.Panel({
						id: 'panelLoading',
						hideOnMaskTap: false,
						floating: true,
						modal: true,
						centered: true,
						width: 250,
						height: 50,
						html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
					});
					var toolbarForm = new Ext.Toolbar({
						dock : 'bottom',
						layout: {
							pack: 'center'
						},
						items: [
							{
								text: 'Submit',
								width: 75,
								height: 35,
								handler: function() {
									validacion=validate();
									//validacion=true;
									if(validacion==true){
										Ext.getCmp('panelTerms').show('pop');
									}
								}
							},{
								text: 'Back',
								width: 75,
								height: 35,
								handler: function() {
									window.location.href='http://reifax.com/mobile/app1/';
								}
							}
						]
					});
					var toolbarRegister = new Ext.Toolbar({
						dock : 'bottom',
						layout: {
							pack: 'center'
						},
						items: [
							{
								text: 'Ok',
								width: 75,
								height: 35,
								handler: function() {
									if(Ext.get('checkterms').getValue()=='on'){
										validacion=true;
									}else{
										alert("Please, you must select Accept to continue the application process.");
										validacion=false;
									}
									Ext.getCmp('panelLoading').show();
									if(validacion==true){
										//Se verifica que el email no este registrado
										Ext.Ajax.request({
											url:'php/response_data.php?type=usuario&email='+email,
											method: 'POST',
											success:function(response, opts){
												var data = Ext.decode(response.responseText);
												if(data.respuesta=='false'){
													//Si el email no esta registrado
													//Se crea la llamada para el cobro automatico de paypal
													var ajax=nuevoAjax();
													var payexdate = monthexpiration+'/'+yearexpiration;
													var payprices;
													if(product=='1'){
														payprices=9.99;
													}else if(product='2'){
														payprices=19.99;
													}
													ajax.open("POST", "http://reifax.com/paypal_cobro.php",true);
													ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
													ajax.send('holder='+holdername+'&cardtype='+cardtype+'&rbko=&cardnum='+cardnumber+'&exdate='+payexdate+'&csv='+csvnumber+'&address='+address+'&city='+city+'&state='+estado+'&zip='+zipcode+'&price='+payprices+'&email='+email+'&ui=');
													ajax.onreadystatechange=function(){
														if (ajax.readyState==4){
															var result=ajax.responseText;
															result=result.split('*');
															var resultado=result[1];
															if(resultado=='FAILURE'){
																//Falla el registro del pago, no se procesa usuario y se manda el mensaje de error
																Ext.getCmp('panelLoading').hide();
																alert(result[2]);
																return false;
															}else{
																//Pago exitoso, se procesa el usuario para insertarlo en la bd
																var paypalid= result[0];
																Ext.Ajax.request({
																	url:'php/newuser.php',
																	extraParams: {
																		email:email,
																		password:password,
																		product:product,
																		phone:phone,
																		holder:holdername,
																		cardtype:cardtype,
																		cardnumber:cardnumber,
																		exdate1:monthexpiration,
																		exdate2:yearexpiration,
																		csv:csvnumber,
																		address:address,
																		city:city,
																		zip: zipcode,
																		state: estado,
																		paypalid: paypalid,
																		rbTiempoPago: 'm',
																		prodprecio: payprices
																	},
																	method: 'POST',
																	success:function(response, opts){
																		var data = Ext.decode(response.responseText);
																		if(data.respuesta=='true'){
																			//usuario ingresado
																			Ext.Ajax.request({
																				url:'php/login.php?email='+email+'&password='+password,
																				method: 'POST',
																				success:function(response, opts){
																					var obj=Ext.decode(response.responseText);
																					var login=obj.login;
																					if(login=='true'){
																						window.location.href='/mobile/app1/';
																					}else{
																						alert('Your email or password is incorrect');
																					}
																				}	
																			});
																		}else{
																			//usuario nno se registro
																			Ext.getCmp('panelLoading').hide();
																		}
																	}
																});
															}
														}
													}
													/* fin paypal */
												}else{
													Ext.getCmp('panelLoading').hide();
													//Si esta registrado
													alert('That email is already registered');
												}
											}	
										});
									}else{
										Ext.getCmp('panelLoading').hide();
									}
								}
							},{
								text: 'Cancel',
								width: 75,
								height: 35,
								handler: function() {
									Ext.getCmp('panelTerms').hide();
								}
							}
						]
					});
					var panelTerms = new Ext.Panel({
						id: 'panelTerms',
						hideOnMaskTap: false,
						floating: true,
						modal: true,
						centered: true,
						width: 300,
						height: 200,
						dockedItems:[
							toolbarRegister
						],
						html:'<div style="padding-top:10px;"><a href="https://www.reifax.com/TermsAndConditions.pdf" style="padding-left: 8px; padding-right: 5px " target="_blank">Accept Terms and Conditions</a>&nbsp;<input type="checkbox" id="checkterms" style="height:20px; width:20px;"/></div><div style="padding-top:15px">This will be a monthly subscription</div>'
					});
					//Validacion de los campos
					function validate(){
						email=Ext.getCmp('email').getValue();
						phone=Ext.getCmp('phone').getValue();
						password=Ext.getCmp('password').getValue();
						product=Ext.getCmp('product').getValue();
						holdername=Ext.getCmp('holdername').getValue();
						cardtype=Ext.getCmp('cardtype').getValue();
						cardnumber=Ext.getCmp('cardnumber').getValue();
						monthexpiration=Ext.getCmp('monthexpiration').getValue();
						yearexpiration=Ext.getCmp('yearexpiration').getValue();
						csvnumber=Ext.getCmp('csvnumber').getValue();
						address=Ext.getCmp('address').getValue();
						city=Ext.getCmp('city').getValue();
						zipcode=Ext.getCmp('zipcode').getValue();
						estado=Ext.getCmp('state').getValue();
						if(email==''){
							alert('Please enter your E-Mail');
							return false;
						}else if(password==''){
							alert('Please enter your Password');
							return false;
						}else if(product==''){
							alert('Please choose your Product');
							return false;
						}else if(holdername==''){
							alert('Please enter a Holder Name');
							return false;
						}else if(cardtype==''){
							alert('Please choose your Card Type');
							return false;
						}else if(cardnumber==''){
							alert('Please enter your Card Number');
							return false;
						}else if(monthexpiration==''){
							alert('Please enter a Expiration Date');
							return false;
						}else if(yearexpiration==''){
							alert('Please enter a Expiration Date');
							return false;
						}else if(csvnumber==''){
							alert('Please enter a CSV Number');
							return false;
						}else if(address==''){
							alert('Please enter your Address');
							return false;
						}else if(city==''){
							alert('Please enter your City');
							return false;
						}else if(zipcode==''){
							alert('Please enter your Zip Code');
							return false;
						}else if(state==''){
							alert('Please enter your State');
							return false;
						}
						return true;
					}
					 panelRegister = new Ext.Panel({
						fullscreen : true,
						scroll: 'vertical',
						dockedItems:[
							toolbarForm
						],
						items:[
								{
									xtype: 'fieldset',
									title: 'User Information',
									defaults: {
										labelAlign: 'left'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'email',
											id: 'email',
											label: 'Email'
										},{
											xtype: 'passwordfield',
											name: 'password',
											id: 'password',
											label: 'Password'
										},{
											xtype: 'textfield',
											name: 'phone',
											id: 'phone',
											label: 'Phone'
										}
									]
								},{
									xtype: 'fieldset',
									title: 'Product Information',
									defaults: {
										labelAlign: 'left'
									},
									items: [
										{
											xtype: 'selectfield',
											name : 'product',
											id : 'product',
											label: 'Product Type',
											options:[
														{text: '- Select -',  value: ''},
														{text: 'Deal Locator - $9.99',  value: '1'},
														{text: 'Foreclosure Locator - $19.99',  value: '2'}
													]
										}
									]
								},{
									xtype: 'fieldset',
									title: 'Billing Information',
									defaults: {
										labelAlign: 'left'
									},
									items: [
										{
											xtype: 'textfield',
											name: 'holdername',
											id: 'holdername',
											label: 'Holder Name'
										},{
											xtype: 'selectfield',
											name : 'cardtype',
											id : 'cardtype',
											label: 'Card Type',
											options:[
														{text: '- Select -',  value: ''},
														{text: 'Master Card',  value: 'MasterCard'},
														{text: 'Visa',  value: 'Visa'},
														{text: 'American Express',  value: 'AmericanExpress'},
														{text: 'Discover',  value: 'Discover'}
													]
										},{
											xtype: 'textfield',
											name: 'cardnumber',
											id: 'cardnumber',
											label: 'Card Number'
										},{
											xtype:'panel',
											layout:'hbox',
											height: 45,
											items:[{
														xtype: 'textfield',
														label: 'Expiration Date',
														id : 'monthexpiration',
														name : 'monthexpiration',
														placeHolder: 'MM',
														flex: 4
													}
													,{
														xtype: 'textfield',
														id : 'yearexpiration',
														name : 'yearexpiration',
														placeHolder: 'YYYY',
														flex: 2
													}
											]
										},{
											xtype: 'textfield',
											name: 'csvnumber',
											id: 'csvnumber',
											label: 'CSV Number'
										},{
											xtype: 'textfield',
											name: 'address',
											id: 'address',
											label: 'Address'
										},{
											xtype: 'textfield',
											name: 'city',
											id: 'city',
											label: 'City'
										},{
											xtype: 'textfield',
											name: 'zipcode',
											id: 'zipcode',
											label: 'Zip'
										},{
											xtype: 'selectfield',
											name : 'state',
											id : 'state',
											label: 'State',
											options:[
														{text: '- Select State -',  value: 'none'},
														{text: 'Alabama',  value: 'Alabama'},
														{text: 'Alaska',  value: 'Alaska'},
														{text: 'Alberta',  value: 'Alberta'},
														{text: 'American Samoa',  value: 'American Samoa'},
														{text: 'Arkansas',  value: 'Arkansas'},
														{text: 'British Columbia',  value: 'British Columbia'},
														{text: 'California',  value: 'California'},
														{text: 'Colorado',  value: 'Colorado'},
														{text: 'Connecticut',  value: 'Connecticut'},
														{text: 'Delaware',  value: 'Delaware'},
														{text: 'District of Columbia',  value: 'District of Columbia'},
														{text: 'Florida',  value: 'Florida'},
														{text: 'Georgia',  value: 'Georgia'},
														{text: 'Guam',  value: 'Guam'},
														{text: 'Hawaii',  value: 'Hawaii'},
														{text: 'Idaho',  value: 'Idaho'},
														{text: 'Illinois',  value: 'Illinois'},
														{text: 'Indiana',  value: 'Indiana'},
														{text: 'Iowa',  value: 'Iowa'},
														{text: 'Kansas',  value: 'Kansas'},
														{text: 'Kentucky',  value: 'Kentucky'},
														{text: 'Louisiana',  value: 'Louisiana'},
														{text: 'Maine',  value: 'Maine'},
														{text: 'Manitoba',  value: 'Manitoba'},
														{text: 'Marshall Islands',  value: 'Marshall Islands'},
														{text: 'Maryland',  value: 'Maryland'},
														{text: 'Massachusetts',  value: 'Massachusetts'},
														{text: 'Michigan',  value: 'Michigan'},
														{text: 'Minnesota',  value: 'Minnesota'},
														{text: 'Mississippi',  value: 'Mississippi'},
														{text: 'Missouri',  value: 'Missouri'},
														{text: 'Montana',  value: 'Montana'},
														{text: 'Nebraska',  value: 'Nebraska'},
														{text: 'Nevada',  value: 'Nevada'},
														{text: 'New Brunswick',  value: 'New Brunswick'},
														{text: 'New Hampshire',  value: 'New Hampshire'},
														{text: 'New Jersey',  value: 'New Jersey'},
														{text: 'New Mexico',  value: 'New Mexico'},
														{text: 'New York',  value: 'New York'},
														{text: 'Newfoundland',  value: 'Newfoundland'},
														{text: 'North Carolina',  value: 'North Carolina'},
														{text: 'North Dakota',  value: 'North Dakota'},
														{text: 'Northwest Territory',  value: 'Northwest Territory'},
														{text: 'Nova Scotia',  value: 'Nova Scotia'},
														{text: 'Ohio',  value: 'Ohio'},
														{text: 'Oklahoma',  value: 'Oklahoma'},
														{text: 'Ontario',  value: 'Ontario'},
														{text: 'Oregon',  value: 'Oregon'},
														{text: 'Palau',  value: 'Palau'},
														{text: 'Pennsylvania',  value: 'Pennsylvania'},
														{text: 'Prince Edward Island',  value: 'Prince Edward Island'},
														{text: 'Puerto Rico',  value: 'Puerto Rico'},
														{text: 'Quebec',  value: 'Quebec'},
														{text: 'Rhode Island',  value: 'Rhode Island'},
														{text: 'Saskatchewan',  value: 'Saskatchewan'},
														{text: 'South Carolina',  value: 'South Carolina'},
														{text: 'South Dakota',  value: 'South Dakota'},
														{text: 'Tennessee',  value: 'Tennessee'},
														{text: 'Texas',  value: 'Texas'},
														{text: 'Utah',  value: 'Utah'},
														{text: 'Vermont',  value: 'Vermont'},
														{text: 'Virgin Islands',  value: 'Virgin Islands'},
														{text: 'Virginia',  value: 'Virginia'},
														{text: 'Washington',  value: 'Washington'},
														{text: 'West Virginia',  value: 'West Virginia'},
														{text: 'Wisconsin',  value: 'Wisconsin'},
														{text: 'Wyoming',  value: 'Wyoming'},
														{text: 'Yukon Territory',  value: 'Yukon Territory'}
													],
											value: 'Florida'
										}
									]
								}
						]
					});
				}
			});
		</script>
	</body>
</html>