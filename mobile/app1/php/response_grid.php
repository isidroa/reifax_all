<?php
	include("conexion.php");
	include("functions.php");
	$arrayStates=Array('FL'=>'FLORIDA','GA'=>'GEORGIA');
	function quitarCaracteres2($cad)
	{
		$cad=strtoupper($cad);
		
		$cad=str_replace('\\','',$cad);
		$cad=str_replace('-','',$cad);
		$cad=str_replace('\r\n','',$cad);
		$cad=str_replace("^","",$cad);
		$cad=str_replace(","," ",$cad);
		$cad=str_replace("'","",$cad);
		$cad=str_replace("#","",$cad);
		$cad=str_replace(".","",$cad);
		$cad=str_replace('"','',$cad);
		$cad=str_replace('/','',$cad);
		$cad=str_replace('     ',' ',$cad);
		$cad=str_replace('    ',' ',$cad);
		$cad=str_replace('   ',' ',$cad);
		$cad=str_replace('  ',' ',$cad);
		//echo $cad.'<br>';
		return $cad;
	}

	function getCaracteres($cad){
		$cad=strtoupper($cad);
		//echo $cad.'=';
		$cad=str_replace('CIRCLE','CIR',$cad);
		$cad=str_replace('CIRCL','CIR ',$cad);
		$cad=str_replace('CORT','CT',$cad);
		$cad=str_replace('PLACE','PL',$cad);
		$cad=str_replace('STREET','ST',$cad);
		$cad=str_replace('TERRACE','TER',$cad);
		$cad=str_replace('LINE','LN',$cad);
		$cad=str_replace('DRIVE','DR',$cad);
		$cad=str_replace('BOULEVARD','BLVD',$cad);
		$cad=str_replace('PARKWAY','PKWY',$cad);
		$cad=str_replace('ROAD','RD',$cad);
		$cad=str_replace('RUN','RN',$cad);
		$cad=str_replace('TRAIL','TRL',$cad);
		$cad=str_replace('TRACE','TRCE',$cad);
		
		if($cad=='AVENUE')$cad='AVE';
		if($cad=='AV')$cad='AVE';
		if($cad=='EAST')$cad='E';
		if($cad=='EA')$cad='E';
		if($cad=='SOUTH')$cad='S';
		if($cad=='SO')$cad='S';
		if($cad=='NORTH')$cad='N';
		if($cad=='NO')$cad='N';
		if($cad=='WEST')$cad='W';
		if($cad=='WE')$cad='W';
	 
		//echo $cad.'<br>';
		return $cad;
	}

	function findCaracteres($cad){
		$cad=strtoupper($cad);
		$busq=$cad;
		//echo $cad.'=';
		$cad=str_replace('CIRCLE','CIR',$cad);
		$cad=str_replace('CIRCL','CIR ',$cad);
		$cad=str_replace('CORT','CT',$cad);
		$cad=str_replace('PLACE','PL',$cad);
		$cad=str_replace('STREET','ST',$cad);
		$cad=str_replace('TERRACE','TER',$cad);
		$cad=str_replace('LINE','LN',$cad);
		$cad=str_replace('DRIVE','DR',$cad);
		$cad=str_replace('BOULEVARD','BLVD',$cad);
		$cad=str_replace('PARKWAY','PKWY',$cad);
		$cad=str_replace('ROAD','RD',$cad);
		$cad=str_replace('AVENUE','AVE',$cad);
		$cad=str_replace('AV','AVE',$cad);
		$cad=str_replace('RUN','RN',$cad);
		$cad=str_replace('TRAIL','TRL',$cad);
		$cad=str_replace('TRACE','TRCE',$cad);
		$cad=str_replace('EAST','E',$cad);
		$cad=str_replace('EA','E',$cad);
		$cad=str_replace('SOUTH','S',$cad);
		$cad=str_replace('SO','S',$cad);
		$cad=str_replace('NORTH','N',$cad);
		$cad=str_replace('NO','N',$cad);
		$cad=str_replace('WEST','W',$cad);
		$cad=str_replace('WE','W',$cad);
		//echo $cad.'<br>';
		return $busq==$cad ? false : true;
	}

	function getOrdinal($number)
	{
		// get first digit
		$digit = abs($number) % 10;
		$ext = 'TH';
		$ext = ((abs($number) %100 < 21 && abs($number) %100 > 4) ? 'TH' : (($digit < 4) ? ($digit < 3) ? ($digit < 2) ? ($digit < 1) ? 'TH' : 'ST' : 'ND' : 'RD' : 'TH'));
		return $number.$ext;
	}
	$modulo=$_GET['module'];
	$que='';
	$latitude=$_GET['latitude'];
	$longitude=$_GET['longitude'];
	//Actualizar data de xima system var
	if(isset($_COOKIE['datos_usr']["USERID"])){
		$query='UPDATE xima.xima_system_var SET latGps="'.(float)$latitude.'",lonGps='.(float)$longitude.' WHERE userid='.$_COOKIE['datos_usr']['USERID'];
		mysql_query($query) or die($query.mysql_error());
	}	
	switch($modulo){
		case 'countys':
			$que="	SELECT SQL_CALC_FOUND_ROWS l.idcounty,l.county
				FROM xima.lscounty l ";
		break;
		case 'location':
			$county=$_GET['county'];
			$state=$_GET['state'];
			$searchlocation=$_GET['searchlocation'];
			$search_type=$_GET['search_type'];
			/*$latitude=$_GET['latitude'];
			$longitude=$_GET['longitude'];*/
			$factor=$_GET['factor'];
			$orderby=$_GET['orderby'];
			$orderdirection=$_GET['orderdirection'];
			$limit=$_GET['limit'];
			$start=$_GET['start'];
			if($start==''){
				$start=0;
			}
			$filterwhere='';
			$leftjoinfilter='';
			$tabla_search='`properties_search`';
			$tabla_php='`properties_php`';
			$bd_search='`mantenimiento`';
			$jointable=array();
			$wherejoin='';
			$groupby='';
			$bd_search='`'.conectarPorIdCounty($county).'`';
			$q='select state from xima.lscounty where idcounty='.$county;
			$resq = mysql_query($q) or die($q.mysql_error());
			$myrow= mysql_fetch_array($resq);
			$state=$myrow['state'];
			$campos_adi=", p.address, p.city, p.zip, p.unit, p.url, p.county,p.pendes, '$state' as state";
			$wherejoin=' ';
			$campos_result= ' , `mlsresidential`.status, `mlsresidential`.beds, `mlsresidential`.bath, if(`mlsresidential`.lsqft > 0,`mlsresidential`.lsqft,`mlsresidential`.apxtotsqft) sqft, `mlsresidential`.lsqft garea, `mlsresidential`.larea, `mlsresidential`.tsqft, p.xcoded, `mlsresidential`.xcode, `mlsresidential`.lprice price, if(length(`imagenes`.url)>0 OR length(`imagenes`.urlxima)>5 OR length(`byowner_img`.urlxima)>5,"Y","N") tieneImg,`imagenes`.nphotos, concat(`imagenes`.url,`imagenes`.url2,if(`imagenes`.letra="Y",`imagenes`.mlnumber,substring(`imagenes`.mlnumber,2)),".",`imagenes`.tipo) imagen, if(`imagenes`.urlxima is null or length(`imagenes`.urlxima)<5,NULL,concat(`imagenes`.urlxima,`imagenes`.parcelid,".",`imagenes`.tipo)) imagenxima,
							if(`byowner_img`.urlxima is null or length(`byowner_img`.urlxima)<5,NULL,concat(`byowner_img`.urlxima,`byowner_img`.parcelid,".",`byowner_img`.tipo)) imagenowner, if(p.xcode="01","PS_SF","PS_CONDOS") module, p.marketvalue, p.pendes, p.latitude, p.longitude, `mlsresidential`.status, `marketvalue`.marketpctg equity, `marketvalue`.offertvalue, `marketvalue`.marketpctg, `marketvalue`.offertpctg, `marketvalue`.debttv, `mlsresidential`.mlnumber, `mlsresidential`.pool, `mlsresidential`.waterf, `mlsresidential`.yrbuilt, `mlsresidential`.dom, `marketvalue`.sold, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip ';
			$jointable[]='`mlsresidential`';
			$wherejoin=' AND `mlsresidential`.status=\'A\' ';
			$jointable[]='`marketvalue`';
			$jointable[]='`imagenes`';
			$jointable[]='`byowner_img`';
			$jointable[]='`diffvalue`';
			$qwhere=$retnum;
			$distancia=",truncate(sqrt((69.1* (p.latitude- $latitude))*(69.1*(p.latitude-$latitude))+(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))*(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))),2) as distancia";
			$query="SELECT s.parcelid $campos_adi $campos_result $distancia FROM $bd_search.$tabla_search s ";
								
			$query.="INNER JOIN $bd_search.$tabla_php p ON (s.parcelid=p.parcelid) ";
			if(count($jointable)>0){
				foreach($jointable as $k => $val)
					$query.="LEFT JOIN $bd_search.$val ON (s.parcelid=$val.parcelid) ";
			}
			
			//$factor=0.015;
			//$factor=($distanciaBuscar*0.015)/0.5;
			$latMin=$latitude-($factor);
			$latMax=$latitude+($factor);
			$lonMin=$longitude-($factor);
			$lonMax=$longitude+($factor); 
			$filterwhere.=" AND (p.latitude > $latMin AND p.latitude < $latMax AND p.longitude > $lonMin AND p.longitude < $lonMax)";
			$order="order by distancia";
			//$limit=10;
			if($search_type!=''){
				if($search_type=='nocommercial'){
					$filterwhere.=" AND (p.xcode = '01' OR p.xcode = '04')";
				}else{
					$filterwhere.=" AND p.xcode = '$search_type'";
				}	
			}
			if($orderby!=''){
				$order="order by $orderby $orderdirection, distancia";
			}else{
				$order="order by distancia";
			}
			$query.=" WHERE 1=1 ".$qwhere.$wherejoin.$filterwhere.' '.$order.' LIMIT '.$start.','.$limit;
			//echo $query; return;
			$que=$query;
		break;
		case 'properties':
			$county=$_GET['county'];
			$state=$_GET['state'];
			$search_properties=$_GET['search_properties'];
			$search_type=$_GET['search_type'];
			$search_text=$_GET['search_text'];
			$orderby=$_GET['orderby'];
			$orderdirection=$_GET['orderdirection'];
			/*$latitude=$_GET['latitude'];
			$longitude=$_GET['longitude'];*/
			$pendes=$_GET['pendes'];
			$pequity=$_GET['pequity'];
			$bed=$_GET['beds'];
			$bath=$_GET['baths'];
			$sqft=$_GET['sqft'];
			$ownerocc=$_GET['occupied'];
			$filterwhere='';
			if($search_text=='My location'){
				$search_text='';
				$factor=$_GET['factor'];
				$latMin=$latitude-($factor);
				$latMax=$latitude+($factor);
				$lonMin=$longitude-($factor);
				$lonMax=$longitude+($factor);
				$filterwhere.=" AND (p.latitude > $latMin AND p.latitude < $latMax AND p.longitude > $lonMin AND p.longitude < $lonMax)";
				$distancia=",truncate(sqrt((69.1* (p.latitude- $latitude))*(69.1*(p.latitude-$latitude))+(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))*(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))),2) as distancia";
			}else{
				$q="select avglat, avglong from xima.lscounty where idcounty=$county";
				$res2 = mysql_query($q) or die($q.mysql_error());
				$r=mysql_fetch_array($res2);
				$latitude=$r['avglat'];
				$longitude=$r['avglong'];
				$distancia=",truncate(sqrt((69.1* (p.latitude- $latitude))*(69.1*(p.latitude-$latitude))+(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))*(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))),2) as distancia";
			}
			$arrtexto= explode(' ',$search_text);
			if(count($arrtexto)==1 && $arrtexto[0]=='') $arrtexto=array();
			$limit=$_GET['limit'];
			$start=$_GET['start'];
			if($start==''){
				$start=0;
			}
			
			$leftjoinfilter='';
			$tabla_search='`properties_search`';
			$tabla_php='`properties_php`';
			$bd_search='`mantenimiento`';
			$jointable=array();
			$wherejoin='';
			$groupby='';
			$bd_search='`'.conectarPorIdCounty($county).'`';
			$q='select state from xima.lscounty where idcounty='.$county;
			$resq = mysql_query($q) or die($q.mysql_error());
			$myrow= mysql_fetch_array($resq);
			$state=$myrow['state'];
			$campos_adi=", p.address, p.city, p.zip, p.unit, p.url, p.county,p.pendes, '$state' as state";
			$wherejoin=' ';
			if($search_properties=='FS'){
				if($pequity!=-1 && ($search_type=='' || $search_type=='01' || $search_type=='04')){
					//echo 'Entro';
					$filterwhere.=" AND `marketvalue`.marketpctg $pequity";
				}
				if($bed!=-1)
					$filterwhere.=" AND `mlsresidential`.beds >= $bed";
				if($bath!=-1)
					$filterwhere.=" AND `mlsresidential`.bath >= $bath";
				if($sqft!=-1)
					$filterwhere.=" AND if(`mlsresidential`.lsqft > 0,`mlsresidential`.lsqft,`mlsresidential`.apxtotsqft) >= $sqft";
				
				$jointable[]='`mlsresidential`';
				$wherejoin=' AND `mlsresidential`.status=\'A\' ';
				$campos_result= ', `mlsresidential`.status,`mlsresidential`.beds, `mlsresidential`.bath, if(`mlsresidential`.lsqft > 0,`mlsresidential`.lsqft,`mlsresidential`.apxtotsqft) sqft, `mlsresidential`.lsqft garea, `mlsresidential`.larea, `mlsresidential`.tsqft, p.xcoded, `mlsresidential`.xcode, `mlsresidential`.lprice price, if(length(`imagenes`.url)>0 OR length(`imagenes`.urlxima)>5 OR length(`byowner_img`.urlxima)>5,"Y","N") tieneImg,`imagenes`.nphotos, concat(`imagenes`.url,`imagenes`.url2,if(`imagenes`.letra="Y",`imagenes`.mlnumber,substring(`imagenes`.mlnumber,2)),".",`imagenes`.tipo) imagen, if(`imagenes`.urlxima is null or length(`imagenes`.urlxima)<5,NULL,concat(`imagenes`.urlxima,`imagenes`.parcelid,".",`imagenes`.tipo)) imagenxima,
					if(`byowner_img`.urlxima is null or length(`byowner_img`.urlxima)<5,NULL,concat(`byowner_img`.urlxima,`byowner_img`.parcelid,".",`byowner_img`.tipo)) imagenowner, if(p.xcode="01","PS_SF","PS_CONDOS") module, p.marketvalue, p.pendes, p.latitude, p.longitude, `mlsresidential`.status, `marketvalue`.marketpctg equity, `marketvalue`.offertvalue, `marketvalue`.marketpctg, `marketvalue`.offertpctg, `marketvalue`.debttv, `mlsresidential`.mlnumber, `mlsresidential`.pool, `mlsresidential`.waterf, `mlsresidential`.yrbuilt, `mlsresidential`.dom, `marketvalue`.sold, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip ';
				$jointable[]='`marketvalue`';
				$jointable[]='`imagenes`';
				$jointable[]='`byowner_img`';
				$jointable[]='`diffvalue`';
			}else if($search_properties=='PR'){
				if($pequity!=-1 && ($search_type=='' || $search_type=='01' || $search_type=='04')){
					$filterwhere.=" AND p.equity $pequity";
				}
				if($bed!=-1)
					$filterwhere.=" AND p.beds >= $bed";
				if($bath!=-1)
					$filterwhere.=" AND p.bath >= $bath";
				if($sqft!=-1)
					$filterwhere.=" AND p.sqft >= $sqft";
				
				$jointable[]='`psummary`';
				$wherejoin=' ';
				$campos_result= ', `mlsresidential`.status,p.beds, p.bath, p.sqft, p.xcoded, p.xcode, p.price, if(length(`imagenes`.url)>0 OR length(`imagenes`.urlxima)>5 OR length(`byowner_img`.urlxima)>5,"Y","N") tieneImg,`imagenes`.nphotos, concat(`imagenes`.url,`imagenes`.url2,if(`imagenes`.letra="Y",`imagenes`.mlnumber,substring(`imagenes`.mlnumber,2)),".",`imagenes`.tipo) imagen, if(`imagenes`.urlxima is null or length(`imagenes`.urlxima)<5,NULL,concat(`imagenes`.urlxima,`imagenes`.parcelid,".",`imagenes`.tipo)) imagenxima, if(`byowner_img`.urlxima is null or length(`byowner_img`.urlxima)<5,NULL,concat(`byowner_img`.urlxima,`byowner_img`.parcelid,".",`byowner_img`.tipo)) imagenowner,if(p.xcode="01","PS_SF","PS_CONDOS") module, p.marketvalue, p.pendes, p.latitude, p.longitude, p.status, p.equity, `marketvalue`.offertvalue, `marketvalue`.marketpctg, `marketvalue`.offertpctg, `marketvalue`.debttv, `psummary`.yrbuilt, `psummary`.pool, `psummary`.waterf, `psummary`.lsqft garea, `psummary`.bheated larea, `psummary`.tsqft, `mlsresidential`.mlnumber, `mlsresidential`.dom, `marketvalue`.sold ';
				$jointable[]='`mlsresidential`';
				$jointable[]='`marketvalue`';
				$jointable[]='`imagenes`';
				$jointable[]='`byowner_img`';
			}else if($search_properties=='FR'){
				if($bed!=-1)
					$filterwhere.=" AND `rental`.beds >= $bed";
				if($bath!=-1)
					$filterwhere.=" AND `rental`.bath >= $bath";
				if($sqft!=-1)
					$filterwhere.=" AND if(`rental`.lsqft > 0,`rental`.lsqft,`rental`.apxtotsqft) >= $sqft";
				
				$jointable[]='`rental`';
				$wherejoin=' AND `rental`.status=\'A\' ';
				$campos_result= ', `mlsresidential`.status,`rental`.beds, `rental`.bath, if(`rental`.lsqft > 0,`rental`.lsqft,`rental`.apxtotsqft) sqft, `mlsresidential`.lsqft garea, `mlsresidential`.apxtotsqft larea, `mlsresidential`.tsqft, p.xcoded, p.xcode, `rental`.lprice price, if(length(`imagenes`.url)>0 OR length(`imagenes`.urlxima)>5 OR length(`byowner_img`.urlxima)>5,"Y","N") tieneImg,`imagenes`.nphotos, concat(`imagenes`.url,`imagenes`.url2,if(`imagenes`.letra="Y",`imagenes`.mlnumber,substring(`imagenes`.mlnumber,2)),".",`imagenes`.tipo) imagen, if(`imagenes`.urlxima is null or length(`imagenes`.urlxima)<5,NULL,concat(`imagenes`.urlxima,`imagenes`.parcelid,".",`imagenes`.tipo)) imagenxima, if(`byowner_img`.urlxima is null or length(`byowner_img`.urlxima)<5,NULL,concat(`byowner_img`.urlxima,`byowner_img`.parcelid,".",`byowner_img`.tipo)) imagenowner,if(p.xcode="01","PS_SF","PS_CONDOS") module, p.marketvalue, p.pendes, p.latitude, p.longitude, `rental`.status, `marketvalue`.marketpctg equity, `marketvalue`.offertvalue, `marketvalue`.marketpctg, `marketvalue`.offertpctg, `marketvalue`.debttv, `rental`.mlnumber, `rental`.pool, `rental`.waterf, `mlsresidential`.yrbuilt, `mlsresidential`.dom, `marketvalue`.sold, `diffvalue`.bath as diff_bath, `diffvalue`.beds as diff_beds, `diffvalue`.lsqft as diff_lsqft, `diffvalue`.larea as diff_larea, `diffvalue`.zip as diff_zip ';
				$jointable[]='`mlsresidential`';
				$jointable[]='`marketvalue`';
				$jointable[]='`imagenes`';
				$jointable[]='`byowner_img`';
				$jointable[]='`diffvalue`';
			}
			for($i=0;$i<count($arrtexto);$i++){
				if(is_numeric($arrtexto[$i])){
					$retnum.=" AND ((s.campo1='".getOrdinal($arrtexto[$i])."' OR s.campo2='".getOrdinal($arrtexto[$i])."' OR s.campo3='".getOrdinal($arrtexto[$i])."' OR s.campo4='".getOrdinal($arrtexto[$i])."' OR s.campo5='".getOrdinal($arrtexto[$i])."' OR s.campo6='".getOrdinal($arrtexto[$i])."' OR s.campo7='".getOrdinal($arrtexto[$i])."' OR s.campo8='".getOrdinal($arrtexto[$i])."' OR s.campo9='".getOrdinal($arrtexto[$i])."' OR s.campo10='".getOrdinal($arrtexto[$i])."' OR s.campo11='".getOrdinal($arrtexto[$i])."' OR s.campo12='".getOrdinal($arrtexto[$i])."' OR s.campo13='".getOrdinal($arrtexto[$i])."' OR s.campo14='".getOrdinal($arrtexto[$i])."' OR s.campo15='".getOrdinal($arrtexto[$i])."') OR (s.campo1='".$arrtexto[$i]."' OR s.campo2='".$arrtexto[$i]."' OR s.campo3='".$arrtexto[$i]."' OR s.campo4='".$arrtexto[$i]."' OR s.campo5='".$arrtexto[$i]."' OR s.campo6='".$arrtexto[$i]."' OR s.campo7='".$arrtexto[$i]."' OR s.campo8='".$arrtexto[$i]."' OR s.campo9='".$arrtexto[$i]."' OR s.campo10='".$arrtexto[$i]."' OR s.campo11='".$arrtexto[$i]."' OR s.campo12='".$arrtexto[$i]."' OR s.campo13='".$arrtexto[$i]."' OR s.campo14='".$arrtexto[$i]."' OR s.campo15='".$arrtexto[$i]."')) ";
				}else{
					if(findCaracteres($arrtexto[$i])){
						$retnum.=" AND (s.campo1='".$arrtexto[$i]."' OR s.campo2='".$arrtexto[$i]."' OR s.campo3='".$arrtexto[$i]."' OR s.campo4='".$arrtexto[$i]."' OR s.campo5='".$arrtexto[$i]."' OR s.campo6='".$arrtexto[$i]."' OR s.campo7='".$arrtexto[$i]."' OR s.campo8='".$arrtexto[$i]."' OR s.campo9='".$arrtexto[$i]."' OR s.campo10='".$arrtexto[$i]."' OR s.campo11='".$arrtexto[$i]."' OR s.campo12='".$arrtexto[$i]."' OR s.campo13='".$arrtexto[$i]."' OR s.campo14='".$arrtexto[$i]."' OR s.campo15='".$arrtexto[$i]."' OR s.campo1='".getCaracteres($arrtexto[$i])."' OR s.campo2='".getCaracteres($arrtexto[$i])."' OR s.campo3='".getCaracteres($arrtexto[$i])."' OR s.campo4='".getCaracteres($arrtexto[$i])."' OR s.campo5='".getCaracteres($arrtexto[$i])."' OR s.campo6='".getCaracteres($arrtexto[$i])."' OR s.campo7='".getCaracteres($arrtexto[$i])."' OR s.campo8='".getCaracteres($arrtexto[$i])."' OR s.campo9='".getCaracteres($arrtexto[$i])."' OR s.campo10='".getCaracteres($arrtexto[$i])."' OR s.campo11='".getCaracteres($arrtexto[$i])."' OR s.campo12='".getCaracteres($arrtexto[$i])."' OR s.campo13='".getCaracteres($arrtexto[$i])."' OR s.campo14='".getCaracteres($arrtexto[$i])."' OR s.campo15='".getCaracteres($arrtexto[$i])."') ";
					}else{
						$retnum.=" AND (s.campo1='".$arrtexto[$i]."' OR s.campo2='".$arrtexto[$i]."' OR s.campo3='".$arrtexto[$i]."' OR s.campo4='".$arrtexto[$i]."' OR s.campo5='".$arrtexto[$i]."' OR s.campo6='".$arrtexto[$i]."' OR s.campo7='".$arrtexto[$i]."' OR s.campo8='".$arrtexto[$i]."' OR s.campo9='".$arrtexto[$i]."' OR s.campo10='".$arrtexto[$i]."' OR s.campo11='".$arrtexto[$i]."' OR s.campo12='".$arrtexto[$i]."' OR s.campo13='".$arrtexto[$i]."' OR s.campo14='".$arrtexto[$i]."' OR s.campo15='".$arrtexto[$i]."') ";
					}
				}
			}
			$qwhere=$retnum;
			//$order="order by (abs(p.latitude)-abs($latitude)) asc, (abs(p.longitude)-abs($longitude)) asc";
			$query="SELECT s.parcelid $campos_adi $campos_result $distancia FROM $bd_search.$tabla_search s ";
								
			$query.="INNER JOIN $bd_search.$tabla_php p ON (s.parcelid=p.parcelid) ";
			if(count($jointable)>0){
				foreach($jointable as $k => $val)
					$query.="LEFT JOIN $bd_search.$val ON (s.parcelid=$val.parcelid) ";
			}
			if($search_type!=''){
				if($search_type=='nocommercial'){
					$filterwhere.=" AND (p.xcode = '01' OR p.xcode = '04')";
				}else{
					$filterwhere.=" AND p.xcode = '$search_type'";
				}	
			}
			if($pendes!=-1){
				$filterwhere.=" AND p.pendes = '$pendes'"; 
			}
			if($ownerocc!=-1){ 
				$filterwhere.=" AND `marketvalue`.ownocc = '$ownerocc'";
			}	
			if($orderby!=''){
				$order="order by $orderby $orderdirection, distancia";
			}else{
				$order="order by distancia";
			}
			//$limit=10;
			$query.="WHERE 1=1 ".$qwhere.$wherejoin.$filterwhere.$order.' limit '.$start.','.$limit;
			//echo $query; return;
			$que=$query;
		break;
	}
	$data = array();
	if($que!=''){
		$result=mysql_query ($que) or die ($que.mysql_error ());
		
		while ($row=mysql_fetch_object($result)){
			$row->imagen=getImage($row->parcelid);
			$data [] = $row;
		}
		/*$result2 = mysql_query("SELECT FOUND_ROWS();" ) or die (mysql_error());
		$num_row=mysql_fetch_row($result2);*/
		$pos=strrpos(strtolower($que),"limit");
		$que2=substr($que,0,$pos);
		$result2 = mysql_query($que2) or die ($que2.mysql_error());
		$num_row=mysql_num_rows($result2);
		echo '{"total":"'.$num_row.'","results":'.json_encode($data).'}';
	}else{
		echo '{"total":"0","results":'.json_encode($data).'}';
	}
?>