<?php
include("php/conexion.php");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>ReiFax</title>
		<link rel="stylesheet" href="/MANT/LIB/sencha-touch/resources/css/sencha-touch.css" type="text/css">
		<link rel="stylesheet" href="css/mobile-theme.css" type="text/css">
		<link rel="shortcut icon" type="image/ico" href="http://www.reifax.com/favicon.png">
		<script type="text/javascript" src="/MANT/LIB/sencha-touch/sencha-touch.js"></script>
		<script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<?php 	
			include("php/comboCounty.php");
			$lat=$_GET['lat'];
			$lon=$_GET['lon'];
			echo '<script>';
			if($lat!='' && $lon!=''){
				echo 'var latitude='.$lat.';';
				echo 'var longitude='.$lon.';';
			}else{
				echo 'var latitude=null'.';';
				echo 'var longitude=null'.';';
			}
			echo '</script>';
		?>
		<style type="text/css">
		   .labels {
			 color: black;
			 font-family: "Lucida Grande", "Arial", sans-serif;
			 font-size: 13px;
			 font-weight:bold;
			 text-align: center;
			 width: 13px;
			 white-space: nowrap;
		   }
		 </style>
		<script src="js/Ext.ux.InfiniteCarousel.js" type="text/javascript"></script>
		<script src="js/markerwithlabel.js" type="text/javascript"></script>
		<script src="js/geo.js" type="text/javascript"></script>
		<script src="js/functions.js" type="text/javascript"></script>
		<script src="js/index.js" type="text/javascript"></script>
    </head>
    <body></body>
</html>