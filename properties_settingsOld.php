<script type="text/javascript" src="includes/properties_myaccount.js"></script>

<?php
include('properties_conexion.php');
conectar();

$que="SELECT * FROM xima.ximausrs WHERE userid=".$_COOKIE['datos_usr']['USERID'];
$result=mysql_query($que) or die($que.mysql_error());
$USERCURRENT=mysql_fetch_array($result);

$que="SELECT * FROM xima.shortsale_processor_user WHERE userid=".$_COOKIE['datos_usr']['USERID'];
$result=mysql_query($que) or die($que.mysql_error());
$shortsaleEnabled = mysql_num_rows($result) > 0 ? true : false;

$que="SELECT * FROM xima.permission WHERE userid=".$_COOKIE['datos_usr']['USERID'];
$result=mysql_query($que) or die($que.mysql_error());
$permission = mysql_fetch_assoc($result);
?>


<div align="left" style="height:100%">
    <div id="body_central" style="height:100%">
        <div id="tabs3" style="padding-top:2px;"></div>
    </div>
</div>

<script>
var tabs3=null;
var ancho=640;
<?php if($USERCURRENT['idstatus']!=8 && $USERCURRENT['idstatus']!=9){ ?>
	if(user_loged) ancho=system_width; 
<?php } ?>

//----------- MY SETTINGS / MY ACCOUNT
var tb = new Ext.Toolbar(
	{
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {
?>
            {
                text: ' Personal Settings ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/personalsettings.php'});
                }
            <?php
			if($USERCURRENT['idstatus']<>2)
			{
			?>
			},{
                text: ' Defaults ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/defaultcounty.php'});
				}
            /*},{
                text: ' Banners ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/banners.php'});
                }
            },{
                text: ' Upgrade Acc. ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/upgradeaccount.php'});
                }
            },{
                text: ' Cancel Acc. ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/cancelaccount.php'});
                }
            },{
                text: ' Freeze Acc. ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/freezeaccount.php'});
                }*/
			<?php
			}
			?>
            }
<?php
    }
?>
        ],
        autoShow: true
    });


//----------- MY SETTINGS / MY DOCUMENTS
var tbdocument = new Ext.Toolbar(
	{
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {
?>
            {       
					tooltip: 'Delete Documents',
					cls:'x-btn-text-icon',
					iconAlign: 'left',
					text: ' ',
					width: 30,
					height: 30,
					scale: 'medium',
                     icon: 'http://www.reifax.com/img/toolbar/trash.png',
                toggleGroup: 'searchtoogle',
                handler: function(){

                               var seld = new Array();
                                var selpid = new Array();

                                seld= smmydocument.getSelections();

                                if (seld.length!=0)
                                {
                               for (i=0;i<seld.length;i++)
                                   {
                                       if (i>0)
                                           selpid+=",";

                                           //selpid+= seld[i].get('pid');//COMENTADO POR SMITH 21/09/2012 03:00
										   selpid+= seld[i].json.iddoc;
                                    }

                    Ext.Ajax.request({
                                waitMsg: 'Delete My Document...',
                                url: 'mysetting_tabs/mydocument_tabs/delete.php',
                                method: 'POST',
                                params: { pid: selpid,
                                          nelim: seld.length},

                                success:function(response,options)
                                {
                                    if(response.responseText== 'true' || response.responseText==true)
                                        Ext.Msg.alert('Mensaje', 'Property delete successfully to My document.');
                                    else
                                        Ext.Msg.alert('Mensaje', response.responseText);
                                },
                                failure:function(response,options)
                                {
                                    Ext.Msg.alert("Failure", response.responseText);
                                }

                                   });
                              var tab=tabs3.getActiveTab();
                               var updater = tab.getUpdater();
                               updater.update({url: 'mysetting_tabs/properties_mydocument.php'});
                               }
                                 else
                                 { alert(' my document are not selected'); }

                   }
            }
<?php
    }
?>
        ],
        autoShow: true
    });


	
var tbowner = new Ext.Toolbar({
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {
?>
            {
                 text: 'Properties For Sale ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_sale.php'});
                     }

                },{
                     text: 'Properties Share ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_share.php'});
                     }

                },{
                     text: 'Properties For Rent ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_rent.php'});
                     }
                },{
                     text: ' Inactives Sales ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_inactive.php'});
                     }

                },{
                     text: ' Inactives Rent ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_inactive2.php'});
                     }

                },{
                     text: ' Update For Sale ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_result.php'});
                     }

                },{
                     text: ' Update For Rent ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_myowner_result2.php'});
                     }
                }
<?php
    }
?>
        ],
        autoShow: true
    });

/*
 * Start of the code added by alexbariv
 * 25.03.2011
 */
//----------- MY SETTINGS / MY CONTRACTS
var tbContracts = new Ext.Toolbar({
    items : [
    <?php
    if ($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5) {
    ?>
        {
            text         : ' Contracts ',
            enableToggle : true,
            pressed      : true,
            toggleGroup  : 'searchtoogle',
            handler      : function() {
                var tab     = tabs3.getActiveTab();
                var updater = tab.getUpdater();
                updater.update({
                    url  : 'mysetting_tabs/mycontracts_tabs/mycontracts.php'
                });
            }
        },{
            text         : ' My Signatures ',
            enableToggle : true,
            pressed      : true,
            toggleGroup  : 'searchtoogle',
            handler      : function(){
                var tab     = tabs3.getActiveTab();
                var updater = tab.getUpdater();
                updater.update({
                    url   : 'mysetting_tabs/mycontracts_tabs/mysignatures.php',
                    cache : false
                });
            }
        }
    <?php
    }
    ?>
    ],
    autoShow : true
});

/*
 * End of the code added by alexbariv
 * 25.03.2011
 */
//----------- MY SETTINGS / MY LISTINGS
var tblisting = new Ext.Toolbar({
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {
?>
            {
                 text: ' View Properties ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_mylisting.php'});
                     }
                },{
                     text: ' View Properties Share ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_mylisting_share.php'});
                     }

                },{
                    text: ' Search Agent Email ',
                    enableToggle: true,
                    pressed: true,
                    toggleGroup: 'searchtoogle',
                    handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_mylisting_searchagent.php'});
                     }
                }
<?php
    }
?>
        ],
        autoShow: true
    });

var tblisting2 = new Ext.Toolbar({
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {
?>
            {         tooltip: 'Click to Delete properties to My Listing',
                      iconCls:'icon',
                     iconAlign: 'top',
                     width: 40,
                     icon: 'http://www.reifax.com/img/toolbar/trash.png',
                     scale: 'medium',
                toggleGroup: 'searchtoogle',
                handler: function(){

                                var sell = new Array();
                                var selpil = new Array();

                                sell= smmylisting.getSelections();

                                if (sell.length!=0)
                                {
                               for (i=0;i<sell.length;i++)
                                   {
                                     if (i>0)
                                           selpil+=",";

                                           selpil+= sell[i].get('pid');
                                    }

                          Ext.Ajax.request({
                                waitMsg: 'Delete My Listing...',
                                method: 'POST',
                                url: 'mysetting_tabs/mylisting_tabs/delete.php',
                                params: { pid: selpil,
                                          nelim: sell.length},

                                success:function(response,options)
                                {
                                    if(response.responseText== 'true' || response.responseText==true)
                                        Ext.Msg.alert('Mensaje', 'Property delete successfully to My Listing.');
                                    else
                                        Ext.Msg.alert('Mensaje', response.responseText);
                                },
                                failure:function(response,options)
                                {
                                    Ext.Msg.alert("Failure", response.responseText);
                                }

                                        });
                                 var tab=tabs3.getActiveTab();
                               var updater = tab.getUpdater();
                               updater.update({url:'mysetting_tabs/properties_mylisting.php'});
                                  }
                                 else
                                 { alert(' my listing are not selected'); }

                             }//
            }
<?php
    }
?>
        ],
        autoShow: true
    });


var tbweb = new Ext.Toolbar({
        items: [
<?php
    if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5)
    {      $master=$USERCURRENT['masterweb'];
?>
            {
                text: ' Profile Data ',
                enableToggle: true,
                pressed: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/profiledata.php'});
                }

            }<? if ($master==1){ ?>,{
                text: ' Profile Webmaster ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/profilewebmaster.php'});
                }
            }<? } ?>,{
                text: ' Customize ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/custom.php'});
                }

            },{
                text: ' Link ',
                enableToggle: true,
                toggleGroup: 'searchtoogle',
                handler: function(){
                    var tab=tabs3.getActiveTab();
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/link.php'});
                }
            }

<?php
    }
?>
        ],
        autoShow: true
    });
	

tabs3 = new Ext.TabPanel(
{
    renderTo: 'tabs3',
    activeTab: 0,
    width: ancho,
    height: tabs.getHeight(),
    plain:true,
    enableTabScroll:true,
    defaults:{  autoScroll: false},
    items:[
		  <?php if($USERCURRENT['idusertype']==52){?>
		  {
            /*
             * Start of the code added by Jesus ::===> 19.12.2013
             */
            title    : 'My Short Sale',
            id       : 'shortSaleTab3',
            //tbar     : tbContracts,
            autoLoad : {
                url     : 'mysetting_tabs/myshortsale_tabs/myshortsale.php',
                scripts : true
            }
            /*	
			 * End of the code added by Jesus ::===> 19.12.2013
             */
          }
		  <?php }else{?>
          {
            title: 'My Account',
            id: 'AccountTab3',
            tbar: tb,
            <?php if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5){?>
            autoLoad: {url: 'mysetting_tabs/myaccount_tabs/personalsettings.php', scripts: true}
            <?php }elseif($USERCURRENT['idstatus']==6 || $USERCURRENT['idstatus']==7){?>
            autoLoad: {url: 'mysetting_tabs/myaccount_tabs/unfreezeaccount.php', scripts: true}
            <?php }else{?>
            autoLoad: {url: 'mysetting_tabs/myaccount_tabs/activeuserinactive.php', scripts: true}
            <?php }?>
          }
          <?php if($USERCURRENT['idstatus']<>6 && $USERCURRENT['idstatus']<>7 && $USERCURRENT['idstatus']<>5){?>
          ,{
            title: 'My Archives',
            id: 'DocumentTab3',
            tbar: tbdocument,
            autoLoad: {url: 'mysetting_tabs/properties_mydocument.php', scripts: true}
          }<?php
          
		  if($permission['platinum']==1 || $permission['professional']==1 || $permission['professional_esp']==1){  ?>
          ,{
            /*
             * Start of the code added by alexbariv
             * 25.03.2011
             */
            title    : 'Documents',
            id       : 'contractsTab3',
            tbar     : tbContracts,
            autoLoad : {
                url     : 'mysetting_tabs/mycontracts_tabs/mycontracts.php',
                scripts : true
            }
            /*
             * End of the code added by alexbariv
             * 25.03.2011
             */
          } 
		  <?php }
		  if($shortsaleEnabled){?>
		  ,{
            /*
             * Start of the code added by Jesus ::===> 19.12.2013
             */
            title    : 'My Short Sale',
            id       : 'shortSaleTab3',
            //tbar     : tbContracts,
            autoLoad : {
                url     : 'mysetting_tabs/myshortsale_tabs/myshortsale.php',
                scripts : true
            }
            /*	
			 * End of the code added by Jesus ::===> 19.12.2013
             */
          } <?php }
		  if($permission['platinum']==1 || $permission['professional']==1 || $permission['professional_esp']==1){  ?>
		  ,{
				title: 'Mail Settings',
				id: 'mailsettingsTab5',
				autoLoad: {
					url: 'mysetting_tabs/mycontracts_tabs/mailsettings.php', 
					scripts: true,
					params	: {
						tabId: 'Follow'
					}
				}
			}
          <?php }
		  if($permission['professional']==1 || $permission['professional_esp']==1){  ?>
		  ,{
				title: 'Phone Settings',
				id: 'phonesettTab5',
				autoLoad: {url: 'mysetting_tabs/mycontracts_tabs/phonesettings.php', scripts: true}
			},{
				title: 'Contacts Settings',
				id: 'contactsettingsTab5',
				autoLoad: {url: 'mysetting_tabs/mycontracts_tabs/contactsettings.php', scripts: true}
			}
		   <?php } 
		   if($permission['platinum']==1 || $permission['professional']==1 || $permission['professional_esp']==1){  ?>
			,{
				title: 'Templates',
				id: 'templatesTab5',
				autoLoad: {url: 'mysetting_tabs/mycontracts_tabs/contracts_templates.php', scripts: true}
			}
		  <?php }
		  
		  }
    	} ?>
    ],
    listeners: {
        'tabchange': function(tabpanel,tab)
         {
            if(tab){
                if(tab.id=='DocumentTab3'){
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_mydocument.php', scripts: true});
                }
                if(tab.id=='listingTab3'){
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/properties_mylisting.php', scripts: true});
                }
                if(tab.id=='webTab3'){
                    var updater = tab.getUpdater();
                    updater.update({url: 'mysetting_tabs/myaccount_tabs/profiledata.php', scripts: true});
                }

             }
        }
    }
});




function buscar(){


        var simple = new Ext.FormPanel
        ({
            labelWidth: 50,
            url:'mysetting_tabs/properties_mylisting_searchagent.php',
            frame:true,
            title: 'Search Properties', // titulo de la ventana
            bodyStyle:'padding:5px 5px 0', // estilo del boton
            width: 250,
            waitMsgTarget : 'Wait...',

            items: [{
                    xtype : 'combo',
                    fieldLabel : 'County',
                    mode: 'local',
                    store: new Ext.data.JsonStore({
                            totalProperty: 'total',
                            root: 'results',
                            autoLoad: true,
                            url: 'mysetting_tabs/county_user.php',
                            fields: ['idcounty','county']
                                }),
                    valueField: 'idcounty',
                    displayField: 'county',
                    emptyText : 'Select County',
                    name: 'idcounty'

                   },{

                    xtype: 'textfield',   // tipo de campo
                    fieldLabel: 'Email',  // etiqueta
                    width: 200,
                    name: 'email',      // variable
                    vtype: 'email',  //tipo de variable

                     }],

            buttons: [{
                text: 'Search',
                             handler: function(){

                                simple.getForm().submit({

                                success: function(form, action)
                                   { win.close();},
                                failure: function(form, action)
                                  {   Ext.Msg.alert("Message", 'No encontrado.');}
                                                        });
                                                    }


                        },{
                text: 'Close',
                handler  : function(){
                        win.close();  // limpiar formulario
                              }
                     }]
        });

         ///////////////// Habilita la nueva ventana con sus propiedades

          win = new Ext.Window({

            layout      : 'fit',
            width       : 300,
            height      : 170,
            modal       : true,
            plain       : true,
            items       : simple,


                            });
        win.show(); // apertura de la ventana

    }


     </script>
