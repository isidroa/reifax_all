<?php
/**
 * properties_investor_offer.php
 *
 * Generate de Investor Report.
 * 
 * @autor   ? 				<?@?.com>                   Description
 *			Guillermo Vera	<guilleverag@gmail.com> 	Original Code
 			Alex Barrios   	<alexbariv@gmail.com> 		Review, clean, order, version control, comments
 * @version 21.04.01
 */
 
if(!isset($_POST['type']))
	include_once "../templates/properties_template.php";
	
include_once "../properties_overview_function.php";

if(is_numeric($_GET['db'])) {
	$db         = conectarPorIdCounty($_GET['db']); 
	$_GET['db'] = $db; 
} else { 
	$db         = conectarPorNameCounty($_GET['db']); 
	$db         = explode('^',$db); 
	$db         = $db[0]; 
	$_GET['db'] = $db;
}

$pid             = $_GET['pid'];
$_SERVERXIMA     = "http://www.reifax.com/";
$par_no_conexion = true;

// ----------------------

$q      = "SELECT l.latitude,l.longitude,p.xcode 
			FROM psummary p INNER JOIN latlong l ON (p.parcelid=l.parcelid) 
			WHERE p.parcelid='$pid'";
		
$result = mysql_query($q) or die($q.mysql_error());
$res    = mysql_fetch_array($result);

$lat    = $res['latitude'];
$lon    = $res['longitude'];

// ----------------------

$db_data           = $db;
$latitude          = $lat;
$longitude         = $lon;
$loged             = $print = true;
$realtor           = $block = $ocomp = false;
$realtorid         = '';
$status_pro        = '';
$xcode             = $res['xcode'];
$_GET['latitude']  = $lat;
$_GET['longitude'] = $lon;
$_GET['xcode']     = $res['xcode'];
$_GET['document']  = true;
$_POST['userweb']  = "false";

// ----------------------

$que           = "SELECT idcounty from xima.lscounty 
					WHERE bd='".str_replace('1','',$db_data)."'";
$result        = mysql_query($que) or die($que.mysql_error());
$r             = mysql_fetch_array($result);
$county        = $r[0];

// ----------------------

$sql_comparado = "Select status,lprice FROM mlsresidential Where parcelid='$pid';";	
$res           = mysql_query($sql_comparado) or die(mysql_error());
$myrow         = mysql_fetch_array($res);

$status_pro    = $myrow['status'];
$lprice_pro    = $myrow['lprice'];

// ----------------------

$sql_comparado = "Select marketvalue,offertvalue FROM marketvalue Where parcelid='$pid';";	
$res           = mysql_query($sql_comparado) or die(mysql_error());
$myrow         = mysql_fetch_array($res);

$medianMarketvalue    = $myrow['marketvalue'];
$medianActivevalue    = $myrow['offertvalue'];

// ----------------------

$que           = "SELECT profimg, profname, profemail, profphone 
					FROM xima.profile 
					WHERE userid=".$_COOKIE['datos_usr']['USERID'];
$result        = mysql_query($que) or die($que.mysql_error());
$res           = mysql_fetch_array($result);

$profimg       = $res['profimg'];
$profname      = $res['profname'];
$profemail     = $res['profemail'];
$profphone     = $res['profphone']; 

// ----------------------

?>
<script type="text/javascript">
	var SpaceNeedle = new VELatLong(<?php echo $lat.','.$lon;?>);
	
	var reifaxvalue 	= parseFloat(99999999);
	var marketvalue 	= parseFloat(99999999);
	var activevalue 	= parseFloat(99999999);
	var percentvalue 	= parseFloat(99999999);
	var finalvalue 		= parseFloat(99999999);

	var roundTo     = <?php echo $_POST['roundTo'];?>;
	percentvalue    = parseFloat(<?php echo $_POST['factor'];?>);
	<?php 
		if($status_pro=='A') 
			echo 'reifaxvalue=parseFloat('.$lprice_pro.');';  
	?>
</script>

<div id="report_content">

	<!--BANNER PERSONALIZE-->
	<br clear="all">
	<div style="margin:auto; 
    			background:url(http://www.reifax.com/img/header3.jpg) no-repeat center; 
                color:#000; width:100%; height:100px;">
                    
        <?php
        if ($profimg!='img/profile/personalnophoto.jpg') {
        ?>
        <div style=" float:left; margin:5px 10px 5px 10px; color:#000;">
            <img src="<?php echo 'http://www.reifax.com/'.$profimg;?>" 
            	 height="85px" style=" float:left; border:solid #fff 2px;">
        </div>
		<?php
        }
        ?>

	    <div style="float:left; margin:25px 10px 5px 10px; color:#fff; 
        	 		font-size:14px; text-align:left; width:250px;">
	    <?php 	
		echo '<span style="font-weight:bold;">Name:</span> '.$profname.'<br>';
		echo '<span style="font-weight:bold;">Email:</span> '.$profemail.'<br>';
		echo '<span style="font-weight:bold;">Phone:</span> '.$profphone;
    	?>
        
    </div>
    
</div>

<?php 
// ----------------------

// cabecera principal
echo '<h1 align="center" >PROPERTY INVESTOR OFFER</h1>';

// Added by alexbariv to manage the new options for low and median 20.04.2011
$investOpc = $_POST['lowMedian'] == 1 ? 'L' : 'M';

echo '<h1 align="center" >Comparables</h1><br>'; 

overviewComp($pid,$db_data,$xcode,"Rcomparable_mymap","Rcomparable_sujeto","Rcomparable_comparables","RpagingComp",
			 $print,$ocomp,$county,"PinCompR","ImageCompR",$loged,$block,$_POST['parcelids_cco'],
			 'saleprice','ASC',$investOpc);

echo '<div style="page-break-after:always"></div><br clear="all"><br clear="all">';

$_POST['no_func']=true;

echo '<h1 align="center" >Comp. Active</h1><br>'; 

overviewCompActive($pid,$db_data,$xcode,"Rcompactive_mymap","Rcompactive_sujeto","Rcompactive_comparables","RpagingActive",
				   $print,$ocomp,$county,"PinCompActiveR","ImageCompActiveR",$_POST['parcelids_cca'],
				   'lprice','ASC', $investOpc);

echo '<div style="page-break-after:always"></div><br clear="all"><br clear="all">';

$titleLM = $_POST['lowMedian']==1 ? 'LOW' : 'MEDIAN';

?>
<div style='font-family:Trebuchet MS,Arial,Helvetica,sans-serif;'>

    <table align="center" cellpadding="0" cellspacing="0"
            style="font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font-size:140%;width:600px;">
            
        <tr class="mortcelltitulo" style="height:2px">
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        
        <?php 
        if($status_pro=='A'){
        ?>
        <tr>
            <td>LISTING PRICE:</td>
            <td align="right" id="reifaxvalue">$0.00</td>
        </tr>
        <?php 
        }
        ?>
        <tr>
            <td><?=$titleLM?> MARKET VALUE:</td>
            <td align="right" id="marketvalue">$0.00</td>
        </tr>
        <tr>
            <td><?=$titleLM?> ACTIVE VALUE:</td>
            <td align="right" id="activevalue">$0.00</td>
        </tr>
        <tr class="mortcelltitulo" style="height:2px">
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        <tr>
            <td>OFFER FACTOR:</td>
            <td align="right" id="percentvalue">100.00%</td>
        </tr>
        <tr>
            <td>LOW OFFER PRICE:</td>
            <td align="right" id="selectvalue">$0.00</td>
        </tr>
        <tr>
            <td>INVESTOR OFFER VALUE 
                (LOW OFFER PRICE x OFFER FACTOR):</td>
            <td align="right" id="ioffertvalue">$0.00</td>
        </tr>
        <tr class="mortcelltitulo" style="height:2px">
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        <tr>
            <td>ROUNDED INVESTOR OFFER VALUE:</td>
            <td align="right" id="finalvalue">$0.00</td>
        </tr>
        <tr class="mortcelltitulo" style="height:2px">
            <td colspan="2" align="right">&nbsp;</td>
        </tr>
        
    </table>
    
</div> 

<script type="text/javascript">
	<?php 
		if($_POST['lowMedian']!=1){
			echo 'marketvalue='.$medianMarketvalue.';';
			echo 'activevalue='.$medianActivevalue.';';
		}
	?>
		
	
	if(document.getElementById('reifaxvalue')) document.getElementById('reifaxvalue').innerHTML = Ext.util.Format.usMoney(reifaxvalue);
	document.getElementById('marketvalue').innerHTML = Ext.util.Format.usMoney(marketvalue);
	document.getElementById('activevalue').innerHTML = Ext.util.Format.usMoney(activevalue);
	
	var selectvalue=reifaxvalue;
	if(selectvalue>marketvalue && marketvalue>0) selectvalue=marketvalue;
	if(selectvalue>activevalue && activevalue>0) selectvalue=activevalue;
	
	document.getElementById('selectvalue').innerHTML  = Ext.util.Format.usMoney(selectvalue);
	document.getElementById('percentvalue').innerHTML = percentvalue+'%';
	document.getElementById('ioffertvalue').innerHTML = Ext.util.Format.usMoney((selectvalue*(percentvalue/100)));
	
	if(roundTo==500)
		finalvalue = parseFloat((Math.floor((selectvalue*(percentvalue/100))/1000)+'500.00'));
	else if(roundTo==10)
		finalvalue = parseFloat((Math.floor((selectvalue*(percentvalue/100))/10)+'0.00')); 
	else if(roundTo==100)
		finalvalue = parseFloat((Math.floor((selectvalue*(percentvalue/100))/100)+'00.00'));
	else 
		finalvalue = parseFloat(((Math.floor((selectvalue*(percentvalue/100))/1000)+1)+'000.00'));
	
	document.getElementById('finalvalue').innerHTML=Ext.util.Format.usMoney(finalvalue);
	
	if(document.getElementById('tabs')){
		if((parseFloat(document.getElementById('report_content').offsetHeight)+1500) > tabs.getHeight()){
			tabs.setHeight(parseFloat(document.getElementById('report_content').offsetHeight)+1500);
			viewport.setHeight(parseFloat(document.getElementById('report_content').offsetHeight)+1500);
		}
	}
    if (Ext.isIE) {
		setTimeout('window.scroll(0,0);',1000);
	}
</script>