	<script>
		Ext.namespace('coor');
		Ext.namespace('pagess');
	///////////////VENTANA FALSA DE CARGANDO///////////////		
		coor.loading_win_custom=new Ext.Window({
			width:170,
			autoHeight: true,
			resizable: false,
			modal: true,
			border:false,
			closable:false,
			plain: true,
			html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="../../../../../img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
		}).show();
	</script>	
	<script>
		Ext.namespace('capas');
		var canvas;
		var canvasCtx;

		var ghostcanvass;
		var gctx;
		var HEIGHT;
		var WIDTH;
		var gctx; // fake canvas context
		var stage;
		var layer;
	
		var stagex;
		var layerx;
		capas.posx='';
		capas.posx='';
		capas.bandera=true;

		function validarSeleccionDePosicion(){
			if(capas.posx=='' || capas.posy==''){
				var msgbox = Ext.MessageBox.alert('Warning','Please: FiFFrst Select a Position over Contract PDF Page');
				return false;
			}
			return true;
		}

		function crear() {
			stagex = new Kinetic.Stage({
			container: "container",
			width: canvas.width,
			height: canvas.height
			});
				layerx = new Kinetic.Layer();
			
			layerx.getCanvas().id = 'canvasxcanvas';
			
			stagex.add(layerx);

			//////Le quito el Stylo Postion a Vacio
			document.getElementById('canvasxcanvas').style.position="";
			
			var auxJESUS = document.getElementById('canvasxcanvas');
			var otroxx = document.getElementById('container');
			auxJESUS.addEventListener("click",function(e){

//					console.debug(e);
//					console.debug(document.getElementById('body_central').scrollTop);
//					console.debug(window.pageYOffset);
//					capas.posx = e.layerX;// - otroxx.offsetLeft;
//					capas.posy = e.layerY;// - otroxx.offsetTop;
					if(BrowserDetect.browser=='Chrome'){
						capas.posx = e.layerX;
					}else{
						capas.posx = e.layerX;
					}
					//capas.posx = e.clientX - otroxx.offsetLeft - document.getElementById('principal').offsetLeft - document.getElementById('viewport').offsetLeft + 2;// - otroxx.offsetLeft;					
					capas.posy = e.clientY - otroxx.offsetTop - document.getElementById('principal').offsetTop - document.getElementById('viewport').offsetTop - 32 + window.pageYOffset;// - otroxx.offsetTop;
					var aux = Ext.getCmp('check1').pressed;
					if(aux==true){
						capas.checksbox(1);
					}
					var aux = Ext.getCmp('check2').pressed;
					if(aux==true){
						capas.checksbox(2);
					}
					var aux = Ext.getCmp('check3').pressed;
					if(aux==true){
						capas.checksbox(3);
					}
					var aux = Ext.getCmp('cmb-Tpl').getValue();
					if(aux!="" && aux!="0000"){
						var combo		= Ext.getCmp('cmb-Tpl');
						var value_id	= combo.getValue();
						var index = 0;
						combo.setValue(combo.store.getAt(index).get(combo.valueField));
						combo.selectedIndex = index;
						capas.firmas(value_id,coor.rut_imagen,coor.typ_imagen);
					}
					var aux = Ext.getCmp('idfieldcontract').getValue();
					if(aux!="" && aux!="0000"){
						var combo			= Ext.getCmp('idfieldcontract');
						var value_display	= combo.getRawValue();
						var value_id		= combo.getValue();
						var index = 0;
						combo.setValue(combo.store.getAt(index).get(combo.valueField));
						combo.selectedIndex = index;
						capas.variable(value_id,value_display);
					}
				}
			,false);
			
		};

	capas.firmas_add = function(id,imagen,type,x,y,width,height){
		capas.box = new Kinetic.Group({
			x: x,
			y: y,
			name:"grupofirmas",
			draggable: true
		});

		layerx.add(capas.box);
		stagex.add(layerx);

		// darth vader
		var img = new Image();
		img.src=imagen;
		var darthVaderImg = new Kinetic.Image({
			image:		img,
			x:			0,
			y:			0,
			idtox:		type,
			idtypex:	"signature",
			height:		height,
			width:		width,
			stroke:		"#00FF80",
			//strokeWidth:4,
			name: 		"image"
		});

		darthVaderImg.on("mouseover", function() {
			var layer = this.getLayer();
			this.setStroke("red");
			layer.draw();
		});
		darthVaderImg.on("mouseout", function() {
			var layer = this.getLayer();
			this.setStroke("#00FF80");
			layer.draw();
		});

		capas.box.add(darthVaderImg);
		addAnchor(capas.box, 0, 0, "topLeft");
		addAnchor(capas.box, darthVaderImg.attrs.width, 0, "topRight");
		addAnchor(capas.box, darthVaderImg.attrs.width, darthVaderImg.attrs.height, "bottomRight");
		addAnchor(capas.box, 0, darthVaderImg.attrs.height, "bottomLeft");

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragstart", function() {
			this.moveToTop();
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		layerx.draw();
		stagex.draw();
		//coor.updateDetalleTemp();
	}
	
	capas.firmas = function(id,imagen,type){
		capas.box = new Kinetic.Group({
			x: capas.posx,
			y: capas.posy,
			name:"grupofirmas",
			draggable: true
		});

		layerx.add(capas.box);
		stagex.add(layerx);

		// darth vader
		var img = new Image();
		var alto;
		var ancho;
		var aditional;
		if(type=='1' || type=='2' || type=='5' || type=='6'){
			alto=80;
			ancho=280;
		}else{
			alto=26;
			ancho=70;
		}
		img.src=imagen;
		var darthVaderImg = new Kinetic.Image({
			image:		img,
			x:			0,
			y:			0,
			idtox:		type,
			idtypex:	"signature",
			height:		alto,
			width:		ancho,
			name: 		"image"
		});

		darthVaderImg.on("mouseover", function() {
			var layer = this.getLayer();
			this.setStroke("red");
			layer.draw();
		});
		darthVaderImg.on("mouseout", function() {
			var layer = this.getLayer();
			this.setStroke("#00FF80");
			layer.draw();
		});

		capas.box.add(darthVaderImg);
		addAnchor(capas.box, 0, 0, "topLeft");
		addAnchor(capas.box, darthVaderImg.attrs.width, 0, "topRight");
		addAnchor(capas.box, darthVaderImg.attrs.width, darthVaderImg.attrs.height, "bottomRight");
		addAnchor(capas.box, 0, darthVaderImg.attrs.height, "bottomLeft");

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragstart", function() {
			this.moveToTop();
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		layerx.draw();
		stagex.draw();
		coor.updateDetalleTemp();
	}
	
	function update(group, activeAnchor) {
		var topLeft = group.get(".topLeft")[0];
		var topRight = group.get(".topRight")[0];
		var bottomRight = group.get(".bottomRight")[0];
		var bottomLeft = group.get(".bottomLeft")[0];
		var image = group.get(".image")[0];

		// update anchor positions
		switch (activeAnchor.getName()) {
			case "topLeft":
				topRight.attrs.y = activeAnchor.attrs.y;
				bottomLeft.attrs.x = activeAnchor.attrs.x;
				break;
			case "topRight":
				topLeft.attrs.y = activeAnchor.attrs.y;
				bottomRight.attrs.x = activeAnchor.attrs.x;
				break;
			case "bottomRight":
				bottomLeft.attrs.y = activeAnchor.attrs.y;
				topRight.attrs.x = activeAnchor.attrs.x;
				break;
			case "bottomLeft":
				bottomRight.attrs.y = activeAnchor.attrs.y;
				topLeft.attrs.x = activeAnchor.attrs.x;
				break;
		}

//		if((bottomLeft.attrs.y - topLeft.attrs.y)>10 && (topRight.attrs.x - topLeft.attrs.x)>10){
			image.setPosition(topLeft.attrs.x, topLeft.attrs.y);
			image.setSize(topRight.attrs.x - topLeft.attrs.x, bottomLeft.attrs.y - topLeft.attrs.y);
/*		}else{
			return false;
		}*/
	}
	function addAnchor(group, x, y, name) {
		var stage = group.getStage();
		var layer = group.getLayer();

		var anchor = new Kinetic.Circle({
			x: x,
			y: y,
			stroke: "red",
			fill: "#ddd",
			strokeWidth: 2,
			radius: 3,
			name: name,
			draggable: true
		});

		anchor.on("dragmove", function() {
			update(group, this);
			layer.draw();
		});
		anchor.on("mousedown", function() {
			group.draggable(false);
			this.moveToTop();
		});
		anchor.on("dragend", function() {
			group.draggable(true);
			layer.draw();
		});

		anchor.on("mouseover", function() {
			var layer = this.getLayer();
			document.body.style.cursor = "pointer";
			this.setStrokeWidth(4);
			layer.draw();
		});
		anchor.on("mouseout", function() {
			var layer = this.getLayer();
			document.body.style.cursor = "default";
			this.setStrokeWidth(2);
			layer.draw();
		});
		group.add(anchor);
	}
	
////////////////////////////VARIABLES////////////////////////////
	capas.variable = function(id,display){
		capas.box = new Kinetic.Rect({
			x: capas.posx,
			y: capas.posy-16,
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:id,
			idtypex:"variable",
			textox: "{%"+display+"%}",
			width: 130,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		
		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
		coor.updateDetalleTemp();
	}

	capas.variable_add = function(id,display,x,y){

		capas.box = new Kinetic.Rect({
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:id,
			idtypex:"variable",
			textox: display,
			width: 130,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
	}

	capas.variable_math = function(display,x,y,w){
		capas.box = new Kinetic.Rect({
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_math",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
		coor.updateDetalleTemp();
	}
	
	capas.variable_math_add = function(display,x,y,w){

		capas.box = new Kinetic.Rect({
			//x: 2 * 30 + 150,
			//y: 2 * 18 + 40,
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_math",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
	}

	capas.variable_user = function(display,x,y,w){

		capas.box = new Kinetic.Rect({
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_user",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
		coor.updateDetalleTemp();
	}

	capas.variable_user_add = function(display,x,y,w){

		capas.box = new Kinetic.Rect({
			//x: 2 * 30 + 150,
			//y: 2 * 18 + 40,
			x: parseInt(x),
			y: parseInt(y),
			fill: "#00FF80",
			stroke: "black",
			strokeWidth: 0,
			draggable: true,
			idtox:w, ///Para el Caso de estas variables voy a usar el campo idtc para almacenar el ancho de la variable
			idtypex:"variable_user",
			textox: display,
			width: w,
			height: 13
		});

		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});			
		
		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="black";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		
		layerx.add(capas.box);
		stagex.add(layerx);
	}
////////////////////////////Check's////////////////////////////
	capas.checksbox = function(id){
		switch(id){
			case 1:
				var imagen = '/img/check1_sin.png';
				Ext.getCmp('check1').toggle(false);
				break;
			case 2:
				var imagen = '/img/check2_sin.png';
				Ext.getCmp('check2').toggle(false);
				break;
			case 3:
				var imagen = '/img/check3_sin.png';
				Ext.getCmp('check3').toggle(false);
				break;
		}			
		var imageObj = new Image();
		imageObj.src = imagen;
		capas.box = new Kinetic.Image({
			image:		imageObj,
			x:			capas.posx-7,
			y:			capas.posy-8,
			width:		10,
			idtox:		id,
			idtypex:	"check",
			textox:		"",
			height:		10
		});

		capas.box.draggable(true);	
		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		
		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});

		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="white";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		layerx.add(capas.box);
		stagex.add(layerx);
		coor.updateDetalleTemp();
	}

	capas.checksbox_add = function(id,x,y){
		id=parseInt(id);
		if(layerx==undefined){
			crear();
		}		
		switch(id){
			case 1:
				var imagen = '/img/check1_sin.png';
				break;
			case 2:
				var imagen = '/img/check2_sin.png';
				break;
			case 3:
				var imagen = '/img/check3_sin.png';
				break;
		}
		var imageObj = new Image();
		imageObj.src = imagen;
		capas.box = new Kinetic.Image({
			image:		imageObj,
			x:			parseInt(x),
			y:			parseInt(y),
			width:		10,
			idtox:		id,
			idtypex:	"check",
			textox:		"",
			height:		10
		});

		capas.box.draggable(true);	
		capas.box.on("dragstart", function() {
			capas.box.moveToTop();
			layerx.draw();
		});

		capas.box.on("mouseup", function() {
			coor.updateDetalleTemp();
		});
		
		capas.box.on("dragmove", function() {
			document.body.style.cursor = "pointer";
		});
		/*
		 * dblclick to remove box for desktop app
		 * and dbltap to remove box for mobile app
		 */
		capas.box.on("dblclick dbltap", function() {
			layerx.remove(this);
			layerx.draw();
			coor.updateDetalleTemp();
		});

		capas.box.on("mouseover", function() {
			document.body.style.cursor = "pointer";
			this.attrs.stroke="red";
			this.attrs.strokeWidth=2;
			layerx.draw();			
		});
		capas.box.on("mouseout", function() {
			document.body.style.cursor = "default";
			this.attrs.stroke="white";
			this.attrs.strokeWidth=0;
			layerx.draw();
		});
		layerx.add(capas.box);
		stagex.add(layerx);
		//coor.updateDetalleTemp();
	}
	</script>

	<script>
		Ext.onReady(function(){ // Inicializamos 
				
			Ext.QuickTips.init();

			//Validacion del nombre (Permitira que solo se acepten letras)
			Ext.form.VTypes.nombreMask = /[A-Z\a-z]/;
			Ext.form.VTypes.nombre = function(v){
				return Ext.form.VTypes.nombreMask.test(v);
			};

			/////////////////Funcion para Mensajes/////////////////
			var msg = function(title, ms, ty){
				Ext.Msg.show({
					title:	title,
					msg:	ms,
					minWidth:	200,
					modal:	true,
					icon:	ty,
					buttons:	Ext.Msg.OK
				});
			};

/*			var msg_pos = function(title, msg, ty, x, y){
				var msgbox = Ext.Msg.show({
					title:	title,
					msg:	msg,
					minWidth:	200,
					modal:	true,
					icon:	ty,
					buttons:	Ext.Msg.OK
				}).getDialog();
				msgbox.setPagePosition(x,y);
			};*/
					
			coor.updateDetalleTemp = function(){
				var campos_objetos;
				campos_objetos='{"datos":[';
				var i=0;
				for(i=0;i<layerx.children.length;i++){
					if(layerx.children[i].attrs.name == "grupofirmas"){
						campos_objetos += '{"idvariable":"' + layerx.children[i].children[0].attrs.idtox + '","idtypex":"' + layerx.children[i].children[0].attrs.idtypex + '", "x":"' + layerx.children[i].attrs.x + '|' + layerx.children[i].children[0].attrs.width + '", "y":"' + layerx.children[i].attrs.y + '|' + layerx.children[i].children[0].attrs.height + '", "temp_text":"' + layerx.children[i].attrs.textox + '", "page":' + Ext.getCmp('idpagecontract').getValue() + '}';
					}else{
						campos_objetos += '{"idvariable":"' + layerx.children[i].attrs.idtox + '","idtypex":"' + layerx.children[i].attrs.idtypex + '", "x":' + layerx.children[i].attrs.x + ', "y":' + layerx.children[i].attrs.y + ', "temp_text":"' + layerx.children[i].attrs.textox + '", "page":' + Ext.getCmp('idpagecontract').getValue() + '}';
					}
					if(i!=layerx.children.length-1){
						campos_objetos += ',';
					}
				}			
				campos_objetos += ']}';
				//////////Validacion al ultimo Registro//////////
				if(layerx.children.length==0)
					var ultimo = Ext.getCmp('idpagecontract').getValue();
				else
					var ultimo = '0';
				//////////////////////Fin////////////////////////
				Ext.Ajax.request({
					waitMsg: 'Wait...',
					url: '/custom_contract/includes/php/functions.php',
					method: 'POST',
					params:	{
						idfunction:	4,
						ibjeto:		campos_objetos,
						ultimo_a_borrar:	ultimo //En realidad envio la pagina a borrar
					},
					success: function(response, request){
						var rest = Ext.util.JSON.decode(response.responseText);
						if(rest.success==false){
							msg('Warning', rest.msg, Ext.Msg.WARNING);
						}
					},
					failure: function(response, request){
						msg('Warning', 'No se puede hacer la edicion', Ext.Msg.WARNING);
					}
				});//fin ajax request
			}
			
			coor.llenarPagina = function(){			
				Ext.Ajax.request({
					waitMsg:	'Espere un momento...',
					url:	'/custom_contract/includes/php/functions.php',
					method:	'post',
					params:{
						idfunction:	5,
						page: Ext.getCmp('idpagecontract').getValue()
					},
					success: function(response){
						var rest = Ext.util.JSON.decode(response.responseText);
						//console.debug(rest);
						if(rest.length>0){
							for(i=0;i<rest.length;i++){
								switch(rest[i].camp_type){
									case "variable":
										capas.variable_add(rest[i].camp_idtc,rest[i].camp_text,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy));
										break;
									case "variable_user":
										capas.variable_user_add(rest[i].camp_text,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy),rest[i].camp_idtc);
										break;
									case "variable_math":
										capas.variable_math_add(rest[i].camp_text,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy),rest[i].camp_idtc);
										break;
									case "check":
										capas.checksbox_add(rest[i].camp_idtc,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy));
										break;
									case "signature":
										var aux = rest[i].camp_posx.split("|");
										var x = parseInt(aux[0]);
										var w = parseInt(aux[1]);
										var aux = rest[i].camp_posy.split("|");
										var y = parseInt(aux[0]);
										var h = parseInt(aux[1]);
										capas.firmas_add(rest[i].camp_idtc,rest[i].imagen,rest[i].type,x,y,w,h);
										//alert("signature");
										break;
								}
							}
						}
						if(rest.success==false){
							Ext.MessageBox.alert('Warning',rest.msg);
						}
					},
					failure: function(response){
						//var result=response.responseText;
						Ext.MessageBox.alert('Warning','No se puede hacer la edicion');
					}
				});//fin ajax request
			}

			function obtener_coordenadas(campo_id,user_id,contract_id)
			{
				var store = new Ext.data.JsonStore({
					url:	'obtener_campos.php',
					method:	'post',
					baseParams:{
						campoid:		campo_id,
						userid:		user_id,
						contractid:	contract_id
					},
					fields: [
						'canvas_x',
						'canvas_y' 
					]
				});				
				store.on("load", function(s,rs) {
					 var myArray = new Array();
						store.each(function(record) {
						Ext.getCmp('x').setValue(record.data.canvas_x);
						Ext.getCmp('y').setValue(record.data.canvas_y);
					 });
				});
				store.load();
			}

			var store_campos = new Ext.data.JsonStore({
				url:		'/custom_contract/includes/php/functions.php',
				method:		'post',
				totalProperty:	'total',
				root:		'results',
				baseParams:{
					idfunction:	2
				},
				fields: [
					{name:'id', type:'string'},
					{name:'name', type:'string'}
				],
				autoLoad: true
			});

			var store_campos_math = new Ext.data.JsonStore({
				url:		'/custom_contract/includes/php/functions.php',
				method:		'post',
				totalProperty:	'total',
				root:		'results',
				baseParams:{
					idfunction:	"camps_math"
				},
				fields: [
					{name:'id', type:'string'},
					{name:'name', type:'string'}
				],
				autoLoad: true
			});

			var store_math_operators	=	new Ext.data.SimpleStore({
				fields	:	['id','desc'],
				data	:	[
					//['00','Select Operator'],
					['+','SUM'],
					['-','SUBTRACT'],
					['*','MULTIPLY'],
					['/','DIVISION'],
					['%','PERCENT']
				]
			});
			
			var store_temple = new Ext.data.JsonStore({
				url:		'/custom_contract/includes/php/functions.php',
				method:		'post',
				totalProperty:	'total',
				root:		'results',
				baseParams:{
					idfunction:	6
				},
				fields: [
					{name:'id', type:'string'},
					{name:'name', type:'string'},
					{name:'userid', type:'string'}
				],
				autoLoad: true
			});

			var storetpl= new Ext.data.JsonStore({
				url:		'/custom_contract/includes/php/functions.php',
				root:		'data',					
				totalProperty:	'num',
				baseParams:{
					idfunction:	3
				},				
				fields: [
					{name:'id', type: 'string'},
					{name:'name', type: 'string'},
					{name:'des', type: 'string'},
					{name:'logo', type: 'string'},
					{name:'type', type: 'string'}
				]
			});
			
			var comboRemoteTpl = new Ext.form.ComboBox({
				fieldLabel:	'Signature\'s',
				name:		'cmb-Tpl',
				id:		'cmb-Tpl',
				forceSelection:	true,
				store:		storetpl,
				width:		215,
				emptyText:	'Select One...',
				valueField:	'id',
				triggerAction:	'all',
				mode:		'remote',
				itemSelector:	'div.search-item',
				tpl: 		new Ext.XTemplate('<tpl for="."><div class="search-item" style="background-image:url({logo})"><div class="name">{name}</div><div class="desc">{des}</div></div></tpl>'),
				displayField:	'name',
				listeners:{
					select:	function(obj,record,index){
						coor.rut_imagen = record.data.logo;
						coor.typ_imagen = record.data.type;
					}			
				}
			});		

			window_math_operations_aux = function(){ var window_math_operations = new Ext.Window({
					title:		'Math Operations',
					layout:		'fit',
					width:		550,
					height:		200,
					modal:		true,
					plain:		true,
					//closeAction:	'hide',
					items:[{
						xtype:		'form',
						id:		'window_math_operations_form',
						labelWidth:	120,
						items:	[{
							xtype        : 'panel',
							layout       : 'table',
							layoutConfig : { columns : 3 },
							items        : [{
								/*layout: 'form',
								labelWidth	: 43,
								items:[{*/
									xtype: 		'combo',
									id: 		'idfield3contract',
									store: 		store_campos_math,
									editable: 	false,
									displayField:	'name',
									autoLoad:		true,
									valueField:	'id',
									width:		150,
									name:		'idfield3contract',
									//fieldLabel:	'Fields',
									mode:		'local',
									allowBlank		:	false,
									triggerAction:	'all',
									emptyText:	'Select Field',
									selectOnFocus:	true
								//}]
							},{											
								xtype			:	'combo',
								id				:	'idfield4contract',
								store			:	store_math_operators,
								editable		:	false,
								displayField	:	'desc',
								autoLoad		:	true,
								valueField		:	'id',
								width			:	150,
								name			:	'idfield4contract',
								fieldLabel		:	'Fields',
								mode			:	'local',
								triggerAction	:	'all',
								allowBlank		:	false,
								emptyText		:	'Select Operator',
								selectOnFocus	:	true
							},{
								xtype			:	'numberfield',
								id				:	'window_math_operations_texto',
								colspan			:	2,
								name			:	'window_math_operations_texto',
								allowBlank		:	false,
								width			:	50
							}/*,{
								xtype:	'button',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'add_concatenate',
								listeners:{
									click:	function(me, event){
										var combo = Ext.getCmp('idfield3contract');
										if(combo.getValue()!="0000" && combo.getValue()!=""){
											var textarea = Ext.getCmp('window_math_operations_texto');
											textarea.setValue(textarea.getValue()+"{%"+combo.getRawValue()+"%}");
										}
									}
								}
							}*/],
						},{
							xtype        : 'panel',
							layout       : 'table',
							layoutConfig : { columns : 2 },
							items        : [/*{
								xtype:		'numberfield',
								id:		'window_math_operations_texto',
								colspan:		2,
								name:		'window_math_operations_texto',
								allowBlank:	false,
								width:		500
							},*/{
								xtype:	'button',
								//id:	'window_variable_users_clear',
								text:	'Clear Value',
								handler:	function(){
									Ext.getCmp('window_math_operations_texto').setValue("");
								}
							},{
								xtype:	'button',
								//id:	'window_variable_users_send',
								text:	'Send Operation',
								handler:	function(){
									if (Ext.getCmp('window_math_operations_form').getForm().isValid()) { // Validamos el formulario
										if(capas.posx=='' || capas.posy==''){
											msg('Warning', 'Please: First Select a Position over Contract PDF Page', Ext.Msg.WARNING);
										}else{
											var texto = "{%("+Ext.getCmp('idfield3contract').getRawValue()+ ") " +Ext.getCmp('idfield4contract').getValue()+ " " +Ext.getCmp('window_math_operations_texto').getValue()+"%}";
											//console.debug(texto);
											capas.variable_math(texto,capas.posx,capas.posy,(texto.length*9));
											window_math_operations.close();
										}
									}
								}
							}]
						}]
					}]
				}).show();
			}
			
			window_variable_users_aux = function(){ var window_variable_users = new Ext.Window({
					title:		'Concatenate your Text hire with or without Fields',
					layout:		'fit',
					width:		550,
					height:		200,
					modal:		true,
					plain:		true,
					//closeAction:	'hide',
					items:[{
						xtype:		'form',
						id:		'window_variable_users_form',
						labelWidth:	120,
						items:	[{
							xtype        : 'panel',
							layout       : 'table',
							layoutConfig : { columns : 2 },
							items        : [{
								layout: 'form',
								labelWidth	: 43,
								items:[{
											
									xtype: 		'combo',
									id: 		'idfield2contract',
									store: 		store_campos,
									editable: 	false,
									displayField:	'name',
									autoLoad:		true,
									valueField:	'id',
									width:		150,
									name:		'idfield2contract',
									fieldLabel:	'Fields',
									mode:		'local',
									triggerAction:	'all',
									emptyText:	'Select One...',
									selectOnFocus:	true
								}]
							},{
								xtype:	'button',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'add_concatenate',
								listeners:{
									click:	function(me, event){
										var combo = Ext.getCmp('idfield2contract');
										if(combo.getValue()!="0000" && combo.getValue()!=""){
											var textarea = Ext.getCmp('window_concatenate_users_texto');
											textarea.setValue(textarea.getValue()+"{%"+combo.getRawValue()+"%}");
										}
									}
								}
							}],
						},{
							xtype        : 'panel',
							layout       : 'table',
							layoutConfig : { columns : 2 },
							items        : [{
								xtype:		'textarea',
								id:		'window_concatenate_users_texto',
								colspan:		2,
								name:		'window_concatenate_users_texto',
								allowBlank:	false,
								width:		500
							},{
								xtype:	'button',
								id:	'window_variable_users_clear',
								text:	'Clear Text',
								handler:	function(){
									Ext.getCmp('window_concatenate_users_texto').setValue("");
								}
							},{
								xtype:	'button',
								id:	'window_variable_users_send',
								text:	'Send Text',
								handler:	function(){
									if (Ext.getCmp('window_variable_users_form').getForm().isValid()) { // Validamos el formulario
										if(capas.posx=='' || capas.posy==''){
											msg('Warning', 'Please: First Select a Position over Contract PDF Page', Ext.Msg.WARNING);
										}else{
											var texto = Ext.getCmp('window_concatenate_users_texto').getValue()
											capas.variable_user(texto,capas.posx,capas.posy,(texto.length*9));
											window_variable_users.close();
										}
									}
								}
							}]
						}]
					}]
				}).show();
			}

			window_template_aux = function(){ var window_template = new Ext.Window({
					title:		'<h2 align="center">Template\'s Manager</h2>',
					layout:		'fit',
					width:		550,
					height:		200,
					//toFront:	true,
					modal:		true,
					plain:		true,
					//closeAction:	'hide',
					items:[{
						xtype:		'form',
						id:		'window_template_form',
						labelWidth:	120,
						items:[{
							xtype:		'panel',
							layout:		'table',
							layoutConfig:	{ columns : 4 },
							items:[{
								layout:		'form',
								colspan:		4,
								labelWidth:	85,
								items:[{								
									xtype:		'combo',
									id:		'idtemplecontract',
									store:		store_temple,
									editable:		false,
									displayField:	'name',
									autoLoad:		true,
									valueField:	'id',
									width:		400,
									name:		'idtemplecontract',
									hiddenName:	'idtemplecontracts',
									fieldLabel:	'Set Templates',
									mode:		'local',
									triggerAction:	'all',
									emptyText:	'Select template to copy',
									selectOnFocus:	true,
									listeners:{
										select:{
											fn:function(combo,record){
												/////Validamos para que no sea la Ocion de Seleccionar osea que no es un contrato/////
												if(combo.getValue()=="0001"){
													Ext.getCmp('templatebuttoncreate').show();
													Ext.getCmp('layout_template_name').show();
												}else{
													Ext.getCmp('templatebuttoncreate').hide();
													Ext.getCmp('layout_template_name').hide();
												}
												if(combo.getValue()!="0001" && combo.getValue()!="0000"){
													Ext.getCmp('templatebuttonuse').show();
												}else{
													Ext.getCmp('templatebuttonuse').hide();
												}
												if(combo.getValue()!="0001" && combo.getValue()!="0000" && record.data.userid!="0"){
													Ext.getCmp('templatebuttondelete').show();
													Ext.getCmp('templatebuttonupdate').show();
												}else{
													Ext.getCmp('templatebuttondelete').hide();
													Ext.getCmp('templatebuttonupdate').hide();
												}
											}
										}
									}
								}]
							},{
								xtype:	'button',
								hidden:	true,
								colspan:	2,
								text:	'Set Template',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'templatebuttonuse',
								handler:	function(){
									Ext.MessageBox.confirm('Confirm', 'Do you really want to set this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											//coor.loading_win.show();
											Ext.Ajax.request({
												url:	'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:	'post',
												params:{
													idfunction:	7,
													template_original:	combo.getValue(),
													pages_new:	Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true"){
														msg('Notification', resp.mensaje, Ext.Msg.INFO);
														window_template.close();
														////////////////Actualizo la Pagina con el nuevo Template////////////////
														Ext.getCmp('idpagecontract').fireEvent('select',Ext.getCmp('idpagecontract'),Ext.getCmp('idpagecontract').store.getAt(Ext.getCmp('idpagecontract').selectedIndex+1));
													}
													if(resp.success=="false"){
														msg('Warning', resp.mensaje, Ext.Msg.WARNING);
													}
												}
											});
											var index = 0;
											combo.setValue(combo.store.getAt(index).get(combo.valueField));
											combo.selectedIndex = index;
											////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
											combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
										}
									});
								}
							},{
								xtype:	'button',
								hidden:	true,
								text:	'Update Template',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'templatebuttondelete',
								handler:	function(){
									Ext.MessageBox.confirm('Confirm', 'Do you really want to Update this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											Ext.Ajax.request({
												url:	'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:	'post',
												params:{
													idfunction:	10,
													template_original:	combo.getValue(),
													pages_new:	Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true")
														msg('Notification', resp.mensaje, Ext.Msg.INFO);
													if(resp.success=="false")
														msg('Warning', resp.mensaje, Ext.Msg.WARNING);
												}
											});
											var index = 0;
											combo.setValue(combo.store.getAt(index).get(combo.valueField));
											combo.selectedIndex = index;
											////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
											combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
											store_temple.load();
										}
									});
								}								
							},{
								xtype:	'button',
								hidden:	true,
								colspan:	2,
								text:	'Delete Template',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'templatebuttonupdate',
								handler:	function(){
									Ext.MessageBox.confirm('Confirm', 'Do you really want to Delete this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											Ext.Ajax.request({
												url:	'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:	'post',
												params:{
													idfunction:	9,
													template_original:	combo.getValue(),
													pages_new:	Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true"){
														store_temple.load();
														var index = 0;
														combo.setValue(combo.store.getAt(index).get(combo.valueField));
														combo.selectedIndex = index;
														////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
														combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
														msg('Notification', resp.mensaje, Ext.Msg.INFO);
													}
													if(resp.success=="false"){
														msg('Warning', resp.mensaje, Ext.Msg.WARNING);
													}
												}
											});
										}
									});
								}
							},{
								layout:		'form',
								hidden:		true,
								colspan:		3,
								id:		'layout_template_name',
								labelWidth:	90,
								items:[
									{								
										xtype:		'textfield',
										id:		'template_name',
										width:		200,
										maxLength:	50,
										fieldLabel:	'Template Name',
										allowBlank:	false
									}
								]
							},{
								xtype:	'button',
								hidden:	true,
								text:	'Create Temple',
								icon:	'/img/add.gif',
								hideLabel:true,
								id:	'templatebuttoncreate',
								handler:function(){
									if(Ext.getCmp('window_template_form').getForm().isValid()){
										Ext.Ajax.request({
											url:	'/custom_contract/includes/php/functions.php',
											waitMsg:	'Coping...',
											method:	'post',
											params:{
												idfunction:	8,
												template_name:	Ext.getCmp('template_name').getValue()
											},
											success : function(r) 
											{
												var resp   = Ext.decode(r.responseText);
												if(resp.success=="true"){
													store_temple.load();
													////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
													var combo = Ext.getCmp('idtemplecontract');
													var index = 0;
													combo.setValue(combo.store.getAt(index).get(combo.valueField));
													combo.selectedIndex = index;
													combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));													
													Ext.getCmp('window_template_form').getForm().reset();
													msg('Notification', resp.mensaje, Ext.Msg.INFO);
												}
												if(resp.success=="false"){
													msg('Warning', resp.mensaje, Ext.Msg.WARNING);
												}
											}
										});
									}
								}
							}]
						}],
						buttons:[{
							//xtype		: 'button',
							text		: 'Close',
							icon		: '/img/add.gif',
							hideLabel	: true,
							//id			: 'templatebuttonclose',
							handler		: function(){
								window_template.close();
							}
						}]
					}]
				}).show();
			}
			
			pagess.aux = new Ext.Toolbar({ 	// Creamos nuestra ventana
				renderTo:		'manu_edit_contract',
				items:[{
					xtype: 			'combo',
					id: 			'idfieldcontract',
					store: 			store_campos,
					editable: 		false,
					displayField:	'name',
					autoLoad:		true,
					valueField:		'id',
					width:			200,
					name:			'idfieldcontract',
					hiddenName:		'idfieldcontracts',
					fieldLabel:		'Fields',
					mode:			'local',
					triggerAction:	'all',
					emptyText:		'Select One...',
					selectOnFocus:	true
				},{
					xtype:		'button',
					width: 		50,
					scale:		'medium',
					tooltip:		'Preview Page',
					icon:		'/img/prev1.png',
					listeners:{
						click:	function(){
							var combo = Ext.getCmp('idpagecontract');
							var index = combo.selectedIndex - 1;
							if (index < 0) {
								index = combo.store.getCount() - 1;
							}
							combo.setValue(combo.store.getAt(index).get(combo.valueField));
							combo.selectedIndex = index;
							combo.fireEvent('select', combo, combo.store.getAt(index), index);
							//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
							layerx.removeChildren();
							//Con esto Limpiamos el Canvas a nivel Visual
							layerx.clear();
						}
					}
				},{
					xtype:		'combo',
					id: 		'idpagecontract',
					store: 		new Ext.data.SimpleStore({
								fields: ['id', 'page']
							}),
					editable: 	false,
					displayField:	'page',
					valueField:	'id',
					autoLoad:		true,
					width:		90,
					name:		'idpagecontract',
					hiddenName:	'idpagecontracts',
					mode:		'local',
					triggerAction:	'all',
					emptyText:	'Seleccione un Campo...',
					value:		1,
					selectOnFocus:	true,
					allowBlank:	false,
					listeners:{
						select:	{
							fn:function(combo,value){
								pagess.pdf_new(combo.getValue());
								//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
								if(layerx!=undefined){
									layerx.removeChildren();
									//Con esto Limpiamos el Canvas a nivel Visual
									layerx.clear();																										
								}else{
									crear();
								}
								coor.llenarPagina();
								layerx.draw();
							}
						}
					}
				},{
					xtype:		'button',
					width: 		50,
					scale:		'medium',
					tooltip:	'Next Page',
					icon:		'/img/next1.png',
					//iconAlign:	'right',
					listeners:	{
						click:	function(){
							var combo = Ext.getCmp('idpagecontract');
							var index = combo.selectedIndex + 1;
							if (index < 0) {
								index = combo.store.getCount() + 1;
							}
							if (index == combo.store.data.length){
								index = 0;
							}
							combo.setValue(combo.store.getAt(index).get(combo.valueField));
							combo.selectedIndex = index;
							combo.fireEvent('select', combo, combo.store.getAt(index), index);
							//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
							layerx.removeChildren();
							//Con esto Limpiamos el Canvas a nivel Visual
							layerx.clear();																				
						}
					}
				},{
					xtype:		'button',
					icon:		'/img/check1.png',
					toggleGroup:	'mygroup',
					tooltip:		'Add a point to the Checkbox',
					id:		'check1',
					scale:		'medium',
					enableToggle:	true
				},{
					xtype:		'button',
					icon:		'/img/check2.png',
					toggleGroup:	'mygroup',
					tooltip:		'Add an X to the Checkbox',
					id:		'check2',
					scale:		'medium',
					enableToggle: 	true
				},{
					xtype:		'button',
					icon:		'/img/check3.png',
					toggleGroup:	'mygroup',
					tooltip:		'Add a tick to the Checkbox',
					id:		'check3',
					scale:		'medium',
					enableToggle:	true
				},
				comboRemoteTpl
				,{
					xtype:	'button',
					tooltip:	'Add text',
					icon:	'/img/icons_jesus/Actions-list-add-font-24.png',
					scale:	'medium',
					listeners: {
						click:	function(){
							window_variable_users_aux();
							//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
							//layerx.removeChildren();
							//Con esto Limpiamos el Canvas a nivel Visual
							//layerx.clear();
						}
					}
				},{
					xtype:	'button',
					tooltip:	'Math Operations',
					icon:	'http://www.reifax.com/img/toolbar/sigma.jpg',
					scale:	'medium',
					listeners: {
						click:	function(){
							window_math_operations_aux();
							//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
							//layerx.removeChildren();
							//Con esto Limpiamos el Canvas a nivel Visual
							//layerx.clear();
						}
					}
				},{
					xtype:	'button',
					icon:	'/img/icons_jesus/Preview-24.png',
					scale:	'medium',
					tooltip:	'Preview Page',
					listeners: {
						click:	function(){							
							/*var cmp = Ext.getCmp('formulario'); 
							var form = cmp.getForm();
							var el = form.getEl().dom;
							 
							var target = document.createAttribute("target");*/
							var campos_objetos;

							campos_objetos='{"datos":[';
							var i=0;
							for(i=0;i<layerx.children.length;i++){
								if(layerx.children[i].attrs.name == "grupofirmas"){
									campos_objetos += '{"idvariable":"'+layerx.children[i].children[0].attrs.idtox+'","idtypex":"'+layerx.children[i].children[0].attrs.idtypex+'", "x":"'+layerx.children[i].attrs.x+'|'+layerx.children[i].children[0].attrs.width+'", "y":"'+layerx.children[i].attrs.y+'|'+layerx.children[i].children[0].attrs.height+'", "temp_text":"'+layerx.children[i].children[0].attrs.image.src+'", "page":'+Ext.getCmp('idpagecontract').getValue()+'}';
								}else{
									campos_objetos += '{"idvariable":"'+layerx.children[i].attrs.idtox+'","idtypex":"'+layerx.children[i].attrs.idtypex+'", "x":'+layerx.children[i].attrs.x+', "y":'+layerx.children[i].attrs.y+', "temp_text":"'+layerx.children[i].attrs.textox.replace(/"/g,'\\"')+'"}';
								}
								if(i!=layerx.children.length-1){
									campos_objetos += ',';
								}
							}			
							campos_objetos +=']}';
							/*Ext.getCmp('hidden_objetos').setValue(campos_objetos); 

							target.nodeValue = "_blank";
							el.action = "custom_contract/test.php"; 
							el.setAttributeNode(target);
							el.submit();*/
							Ext.Ajax.request({
								waitMsg: 'Wait...',
								url: '/custom_contract/test.php',
								method: 'POST',
								params:	{
									ibjeto:			campos_objetos,
									idpagecontracts:Ext.getCmp('idpagecontract').getValue()
								},
								success: function(response, request){
									var rest = Ext.util.JSON.decode(response.responseText);
									var Digital=new Date();
									var hours=Digital.getHours();
									var minutes=Digital.getMinutes();
									var seconds=Digital.getSeconds();
									var url = 'http://www.reifax.com/'+rest.pdf;
									window.open('http://docs.google.com/gview?url='+url+'?time='+hours+minutes+seconds); //Agregado por Jesus											
									/*var rest = Ext.util.JSON.decode(response.responseText);
									if(rest.success==false){
										msg('Warning', rest.msg, Ext.Msg.WARNING);
									}*/
								},
								failure: function(response, request){
									msg('Warning', 'Can\'t Create Preview Page', Ext.Msg.WARNING);
								}
							});//fin ajax request
							
						}
					}
				},{
					xtype:	'button',
					tooltip:	'Template\'s Manager',
					icon:	'/img/icons_jesus/Mimetype-templates-24.png',
					scale:	'medium',
					listeners: {
						click:	function(){
							window_template_aux();
						}
					}
				},new Ext.Toolbar.Fill(),{
					xtype:	'button',
					tooltip:'Close',
					icon:	'/img/cancel.png',
					scale:	'medium',
					listeners: {
						click: function(){
							var tab = tabs.getItem('idmodifycontract');
							//pagess.aux.close();
							tabs.remove(tab);
						}
					}
				}]
			});
		});
		
	</script>
	<script>
		pagess.paginas = new Array();
		
		pagess.pdf_principal = function(){
			PDFJS.getDocument(pagess.contract_name).then(function(pdf) {

				pdf.getPage(1).then(function(page) {

					var x;
					for(x=1;x<=pdf.numPages;x=x+1){
						var c = new Array(x.toString(),"Page N "+x);
						pagess.paginas.push(c);
						pagess.totalpaginas=pdf.numPages;
					}
					
					pagess.aux.show();
					Ext.getCmp('idpagecontract').store.loadData(pagess.paginas);
					Ext.getCmp('idpagecontract').selectedIndex = 0;

					var scale = 1.5;
					
					//
					// Prepare canvas using PDF page dimensions
					//
					var viewport = page.getViewport(scale);
					canvas = document.getElementById('the-canvas');
					canvasCtx = canvas.getContext('2d');

					canvas.height = viewport.height;
					canvas.width = viewport.width;

					var renderContext = {
					      canvasContext: canvasCtx,
					      viewport: viewport
					    };
					page.render(renderContext);
					
					//
					// Render PDF page into canvas canvasCtx
					//
					coor.loading_win_custom.hide();
					crear();
					coor.llenarPagina();
				});
			});
		}

		pagess.pdf_new = function(page_num){
			var page_num;
			PDFJS.getDocument(pagess.contract_name).then(function(pdf) {

				pdf.getPage(page_num).then(function(page) {
					//var scale = 1.5; By Jesus
					var scale = 1.5;
					var viewport = page.getViewport(scale);
					
					//
					// Prepare canvas using PDF page dimensions
					//
					canvas = document.getElementById('the-canvas');
					canvasCtx = canvas.getContext('2d');

					canvas.height = viewport.height;
					canvas.width = viewport.width;

					//
					// Render PDF page into canvas canvasCtx
					//
					var renderContext = {
						canvasContext: canvasCtx,
						viewport: viewport
					};
					page.render(renderContext);
				
				});
			});
		}
	Ext.onReady(function(){ // Inicializamos 
		Ext.Ajax.request({
			url	: '/custom_contract/includes/php/functions.php',
			method	: 'post',
			params	: { idfunction: 1 },
			success	: function(response, opts) {
				var obj = Ext.util.JSON.decode(response.responseText);
				pagess.contract_name = obj.filename;
				pagess.pdf_principal();
			},
			failure	: function(response, opts) {
				alert('server-side failure with status code ' + response.status);
			}
		});
	});
	</script>
<div id="manu_edit_contract"></div>
<canvas id="the-canvas" width="0" height="0" style="position:absolute;left:2%;top:50;border:1px solid black;z-index: 1;"/></canvas>
<div id="container" style="position:absolute;left:2%;top:50;z-index: 9011;border: 1px solid black;"></div>