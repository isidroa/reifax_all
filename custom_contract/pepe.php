<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');

	include($_SERVER['DOCUMENT_ROOT'].'/mant/classes/connection.class.php');
	include($_SERVER['DOCUMENT_ROOT'].'/mant/classes/globalReifax.class.php');
	$Connect=	new ReiFax(
		array(
			'downloadFiles'	=>	FALSE
		)
	);
	$conReiFax = $Connect -> mysqlByServer(array('serverNumber' => 'MyREIFAX', 'typeIp' => 'LOCAL', 'dataBase' => 'xima'));
	$_SERVER['REIFAX_DOCUMENT_ROOT']=$Connect->documentRoot;
	include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mant/classes/managerErrors.class.php');
	/*$Errors = new managerErrors(
		array(
			'downloadFiles' => FALSE
		)
	);*/

	$camptit=$Connect->getTableValues(array(
		'table'=>'xima.camptit',
		'fields'=>'`IDTC`,`Desc`',
		'where'=> 'variable=1'
	),array(
		'conReiFax'=>$conReiFax
	));
	$camptit=$Connect->getCamptitTransform(array(
		'values'=>$camptit
	));

	include($_SERVER['REIFAX_DOCUMENT_ROOT'].'/mysetting_tabs/myfollowup_tabs/followemail.class.php');

	require_once('fpdf/fpdf.php');
	require_once('fpdi/fpdi.php');
	include("../properties_conexion.php");
	include("functions.php");
	$conex_functions=conectarPorNameCounty($_POST['county']);
	//Obtain Real County Name
	$aux		=	explode("^",$conex_functions);
	$idCounty	=	$aux[1];
	$query		=	"SELECT County FROM xima.lscounty WHERE idCounty='$idCounty'";
	$result		=	mysql_query($query);
	$result		=	mysql_fetch_assoc($result);
	$countyName	=	strtoupper($result['County']);
	//End Obtain Real County Name

	//   DELETE OLD PDFS
	// -------------------------------------------
	if($_SERVER['HTTP_HOST']=='xima3.reifax.com')
		$folder_to_clean='temp/';
	else
		$folder_to_clean='temp/';

	limpiardirpdf2($folder_to_clean);
	// -------------------------------------------
	//   GET FORM DATA
	// -------------------------------------------
	//begin clear control for register types
	$_POST['emd']=0;
	$_POST['pof']=0;
	$_POST['rademdums']=0;
	//end clear control for register types

	$type       = $_POST['type'];
	$county     = $_POST['county'];
	$parcelid   = $_POST['pid'];						////Parcel id de la Propiedad
	$templateemail = $_POST['contracttemplate'];

	$query		=	"SELECT * FROM xima.systemfonts WHERE idFont='$_POST[contractFontType]'";
	$result		=	mysql_query($query);
	$result		=	mysql_fetch_assoc($result);
	$contractFontName	=	$result['usageName'];
	$contractFontSize 	=	$_POST['contractFontSize'];
	$contractColor 		=	$_POST['contractFontColor'];
	$query		=	"SELECT * FROM xima.systemfonts WHERE idFont='$_POST[addonFontType]'";
	$result		=	mysql_query($query);
	$result		=	mysql_fetch_assoc($result);
	$addonFontName	=	$result['usageName'];
	$addonFontSize 	=	$_POST['addonFontSize'];
	$addonColor 	=	$_POST['addonFontColor'];

	$contrOpc   =	$_POST['options'];
	$offer      =	$_POST['offer'];
	$dateAcc    =	$_POST['dateAcc'];
	$dateClo    =	$_POST['dateClo'];
	$rname      =	$_POST['listingagent'];
	$remail     =	$_POST['remail'];
	$mlnumber   =	$_POST['mlnaux'];
	$address    =	$_POST['addr'];
	$sendbyemail=	$_POST['sendbyemail'];
	$sendmail   =	$_POST['sendmail'];
	$addendum   =	'on';//$_POST['addendum'];
	$addendata  =	$_POST['fieldAddeum'];
	$sendme     =	$_POST['sendme'];
	$addons     =	'on';//$_POST['addons'];
	$addondata  =	$_POST['fieldAddonG'];	////Ids de los Adendum del Contrato Seleccionados
	$deposit    =	$_POST['deposit'];
	$additionalDeposit    =	empty($_POST['additionalDeposit'])	?	0	:	$_POST['additionalDeposit'];
	$inspection =	$_POST['inspection'];
	$scrow      =	$_POST['scrow'];
	$chseller   =	$_POST['csinfo'];
	$sellname   =	$_POST['sellname'];
	$chbuyer    =	$_POST['cbinfo'];
	$buyername  =	$_POST['buyername'];
	$listingagent	=	$_POST['listingagent'];
	$listingbroker  =	$_POST['listingbroker'];
	$buyeragent	=	$_POST['buyeragent'];
	$buyerbroker=	$_POST['buyerbroker'];

	$valInContract = array(
		'emd' => (string)((isset($_POST['emd']) and $_POST['emd']=='on') ? '0' : '1'),
		'pof' => (string)((isset($_POST['pof']) and $_POST['pof']=='on') ? '0' : '1'),
		'rademdums' => (string)((isset($_POST['rademdums']) and $_POST['rademdums']=='on') ? '0' : '1'),
		'offer'	=>	$offer
	);

	////////////////////////////////Recibo y Creo Variables////////////////////////////////
	$userid = $_COOKIE['datos_usr']["USERID"];		////User ID
	$contract_id = $_POST['type'];					////Contrato ID

	$query	=	"UPDATE xima.xima_system_var SET initialDeposit={$deposit},additionalDeposit={$additionalDeposit} WHERE userid={$userid}";
	mysql_query($query);
	switch($contractColor){
		case "black":
			$contractColor=explode(",","0,0,0");
		break;
		case "blue":
			$contractColor=explode(",","0,0,255");
		break;
	}
	switch($addonColor){
		case "black":
			$addonColor=explode(",","0,0,0");
		break;
		case "blue":
			$addonColor=explode(",","0,0,255");
		break;
	}
	$pdf = new FPDI();
	$pdf->AddFont('Comic','',"comic.php");
	$pdf->AddFont('Verdana','',"verdana.php");
	$pdf->AddFont('BKANT','',"BKANT.php");
	$pdf->AddFont('Tahoma','',"tahoma.php");
	$pdf->AddFont('cour','',"cour.php");
	$pdf->AddFont('ariblk','',"ariblk.php");
	$pdf->AddFont('SCRIPTBL','',"SCRIPTBL.php");
	$query = "SELECT campos, `desc`, tabla, idtc, type FROM xima.camptit WHERE variable='1' AND idtc<>733";
	$result = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
	while($rows = mysql_fetch_assoc($result)){
		$array_campos[] = $rows;
	}
	//print_r($array_campos);
	//echo "<BR><BR>";

	////////////////Extraigo la Informacion de todas las Variables estandard////////////////
	$query = "SELECT tabla FROM xima.camptit WHERE variable=1 AND idtc<>733 GROUP BY tabla";
	$result = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
	$array_tablas = array();

	/////////Recorro las Tablas/////////
	while($tablas = mysql_fetch_array($result)){
		$query = "SELECT campos FROM xima.camptit WHERE variable=1 AND tabla='$tablas[tabla]'";
		$result1 = mysql_query($query) or die("Error: D Query: <BR>$query<BR>".mysql_error());
		$total_campos = mysql_affected_rows();
		$c=1;$campos_aux="";
		/////////Agrupo los Campos de la Respectiva Tabla para luego hacer el Query/////////
		while($campos = mysql_fetch_array($result1)){
			$campos_aux .= $campos['campos'];
			if($c<$total_campos){
				$campos_aux .= ",";
			}
			$c++;
		}
		$array_tablas[] = array('tabla' => $tablas['tabla'], 'campos' => $campos_aux);
	}

	$variables_originales = array();
	foreach($array_tablas as $aux){
		if($aux['tabla']=='mlsresidential' or $aux['tabla']=='psummary' or $aux['tabla']=='pendes' or $aux['tabla']=='mortgage'){
			$query = "SELECT $aux[campos] FROM $aux[tabla] WHERE parcelid='$parcelid'";
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			while($rows = mysql_fetch_assoc($result)){
				if('pendes'==$aux['tabla']){
					if(!empty($rows['judgedate'])){
						$date = new DateTime($rows['judgedate']);
						$rows['judgedate']	=	$date->format('m-d-Y');
					}
				}
				$variables_originales[$aux['tabla']] = $rows;
			}
			if($aux['tabla']=='mlsresidential'){
				if(mysql_num_rows($result)<=0)
					//$variables_originales[$aux['tabla']] = Array('county' => obtainRealCountyNotForSale($county)); Comented By Jesus 20-06-2013
					$variables_originales[$aux['tabla']] = array('county' => str_replace('DADE','MIAMI-DADE',$countyName));
				else
					$variables_originales['mlsresidential']['county']=str_replace('DADE','MIAMI-DADE',$countyName);
			}
		}
		if($aux['tabla']=='ximausrs' or $aux['tabla']=='profile' or $aux['tabla']=='contracts_mailsettings' or $aux['tabla']=='contracts_phonesettings' or $aux['tabla']=='contracts_scrow'){
			if($aux['tabla']=='contracts_scrow'){
				$cadenaE=' and imagen="1"';
			}else{
				$cadenaE='';
			}
			$query = "SELECT $aux[campos] FROM xima.$aux[tabla] WHERE userid='$userid'".$cadenaE;
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			while($rows = mysql_fetch_assoc($result)){
				$variables_originales[$aux['tabla']] = $rows;
			}
		}
		if($aux['tabla']=='followagent'){
			$query = "SELECT a.".str_replace(',',',a.',$aux['campos'])." FROM xima.followup f LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid)
				WHERE f.userid=$userid and parcelid='$parcelid'";
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			if(mysql_num_rows($result)<=0){
				$query	= 'SELECT '.$temp="' ' ".implode(",' ' ",explode(',',$aux['campos']));
				$result	= mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			}
			while($rows = mysql_fetch_assoc($result)){
				$variables_originales[$aux['tabla']] = $rows;
			}
		}
		if($aux['tabla']=='followup'){
			$query = "SELECT f.".str_replace(',',',f.',$aux['campos'])." FROM xima.followup f LEFT JOIN xima.followagent a ON (f.agent=a.agent AND a.userid=f.userid)
				WHERE f.userid=$userid and parcelid='$parcelid'";
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			while($rows = mysql_fetch_assoc($result)){
				$variables_originales[$aux['tabla']] = $rows;
			}
		}
		if($aux['tabla']=='contracts_custom'){
			$query = "SELECT
						IF((
							SELECT tplactive FROM xima.$aux[tabla] WHERE userid='$userid' AND id='$contract_id')=1
							,'tpl1_name,tpl1_addr,tpl1_addr2,tpl1_addr3','tpl2_name as tpl1_name,tpl2_addr AS tpl1_addr,tpl2_addr2 AS tpl1_addr2,tpl2_addr3 AS tpl1_addr3')";
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			$result = mysql_fetch_array($result);
			$campos = $result[0];
			$query = "SELECT $campos FROM xima.$aux[tabla] WHERE userid='$userid' AND id='$contract_id'";
			$result = mysql_query($query) or die("Error: E Query: <BR>$query<BR>".mysql_error());
			while($rows = mysql_fetch_assoc($result)){
				$variables_originales[$aux['tabla']] = $rows;
			}

		}
	}
	//print_r($variables_originales);
	//echo "<BR><BR><BR>";

	$array_campos_y_valores_idtc = array();
	$array_campos_y_valores_desc = array();
	foreach(array_keys($variables_originales) as $tabla){
		foreach(array_keys($variables_originales[$tabla]) as $campo){
			foreach($array_campos as $aux){
				if($aux['tabla']==$tabla and $aux['campos']==$campo){
					$idtc=$aux['idtc'];
					$desc=$aux['desc'];
					break;
				}
			}
			$array_campos_y_valores_idtc[$idtc] = $variables_originales[$tabla][$campo];
			$array_campos_y_valores_desc['{%'.$desc.'%}'] = $variables_originales[$tabla][$campo];
		}
	}

	/////////////////Modifico los Campos que Vienen del formulario de Generar el Contrato/////////////////
	if(trim($_POST['buyername'])<>""){

	}

	////////////////Obtengo el Contrato a Utilizar////////////////
	if(isset($_POST['idPackage'])){
		$query	=	"
			SELECT CONCAT('../shortSale/',if(c.userid is null,'',CONCAT(c.userid,'/')),c.filename) AS filename FROM xima.shortsale_packages c
			WHERE if(c.userid is null,c.id_packages=$_POST[idPackage],c.userid=$userid AND c.id_packages=$_POST[idPackage])";
	}else{
		$query = "SELECT CONCAT('../mysetting_tabs/mycontracts_tabs/template_upload/',filename) AS filename FROM xima.contracts_custom c WHERE c.userid='$userid' AND c.id='$contract_id'";
	}
	$result = mysql_query($query) or die("Error: A ".mysql_error());
	$r = mysql_fetch_array($result);

	////////////////Obtengo el Numero de Paginas del Contrato////////////////
	////////////////   Se puede Hacer de las Dos Maneras     ////////////////
	//$pagecount = getNumPagesInPDF(array('../mysetting_tabs/mycontracts_tabs/'.$r['filename']));
	if(isset($_POST['withContract']) and $_POST['withContract']=='on'){
		$pagecount = $pdf->setSourceFile($r['filename']);

		for($i=1;$i<=$pagecount;$i++){
			//$pagecount = $pdf->setSourceFile('helloworld.pdf');
			$tplidx = $pdf->importPage($i, '/MediaBox');
			$new = $pdf->getTemplateSize($tplidx);
			$pdf->addPage('P', array($new['w'],$new['h'])); // lo dejo asi para que tome el tama�o original del documento
			$pdf->SetFont($contractFontName,'',$contractFontSize);
			$pdf->SetTextColor($contractColor[0],$contractColor[1],$contractColor[2]);

			$query = "SELECT * FROM xima.contracts_campos WHERE camp_userid='$userid' AND camp_contract_id='$contract_id' AND camp_page=$i";
			$result = mysql_query($query) or die("Error: C ".mysql_error());
			$rows = array();
			while($row=mysql_fetch_assoc($result)){
				$rows[]=$row;
			}

			$pdf->useTemplate($tplidx, 0, 0, $new['w'], $new['h']);
			foreach($rows as $b){
				if($b['camp_type']=='variable'){
					$x=($b['camp_posx']/1.5);
					$y=($b['camp_posy']/1.5)+(0.352777778*25);
					//1 point = 0.352777778 milimetro
					$x=$x*(0.352777778);
					$y=$y*(0.352777778);
					switch($b['camp_idtc']){
						case "16":	//Legal Description
							$pdf->text($x,$y,"Legal Description as Shown in Public Records");
							break;
						case "20":	//Owner Name == Seller
							if(strlen(trim($sellname))<1)
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
							else
								$pdf->text($x,$y,$sellname);
							break;
						case "95":	//Listing Price
							$pdf->text($x,$y,number_format($array_campos_y_valores_idtc[$b['camp_idtc']],2));
							break;
						case "716":	//Contac Name
							if(strlen(trim($_POST['listingagent']))<1)
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
							else
								$pdf->text($x,$y,$_POST['listingagent']);
							break;
						case "717":	//Contac Email 1
							if(strlen(trim($_POST['remail']))<1)
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
							else
								$pdf->text($x,$y,$_POST['remail']);
							break;
						case "734":	//Buyer Name
							if(strlen(trim($_POST['buyername']))<1)
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
							else
								$pdf->text($x,$y,$_POST['buyername']);
							break;
						case "735":	//Buyer Address Line 1
							$pdf->text($x,$y,$_POST['baddress1']);
							break;
						case "738":	//Buyer Address Line 2
							$pdf->text($x,$y,$_POST['baddress2']);
							break;
						case "739":	//Buyer Address Line 3
							$pdf->text($x,$y,$_POST['baddress3']);
							break;
						case "XX11":	//Seller Name
							$pdf->text($x,$y,$sellname);
							break;
						case "XX12":	//Seller Address Line 1
							$pdf->text($x,$y,$_POST['saddress1']);
							break;
						case "XX13":	//Seller Address Line 2
							$pdf->text($x,$y,$_POST['saddress2']);
							break;
						case "XX14":	//Seller Address Line 3
							$pdf->text($x,$y,$_POST['saddress3']);
							break;
						case "743":	//Contac Company
							if(strlen(trim($_POST['listingbroker']))<1)
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
							else
								$pdf->text($x,$y,$_POST['listingbroker']);
							break;
						case "XX1":	//Date Today
							$pdf->text($x,$y,date("m/d/Y"));
							break;
						case "XX2":	//Initial Deposit
							$pdf->text($x,$y,number_format($deposit,2));
							break;
						case "XX3": //Inspections Days
							$pdf->text($x,$y,$inspection);
							break;
						case "XX4":	//Acceptance Date
							$pdf->text($x,$y,date($dateAcc));
							break;
						case "XX5":	//Closing Date
							$pdf->text($x,$y,date($dateClo));
							break;
						case "XX6":	//Balance To Close
							$Balance_To_Close = (int)$offer-(int)$deposit-(int)$additionalDeposit;
							$pdf->text($x,$y,number_format($Balance_To_Close,2));
							break;
						case "XX7":	//Offer Price
							$offer_price=(int)$offer;
							$pdf->text($x,$y,number_format($offer_price,2));
							break;
						case "XX8":	//Buyer's Agent
							$pdf->text($x,$y,$buyeragent);
							break;
						case "XX9":	//Buyer's Broker
							$pdf->text($x,$y,$buyerbroker);
							break;
						case "XX10"://Additional Deposit
							$pdf->text($x,$y,number_format($additionalDeposit,2));
							break;
						default:
							$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
					}
				}
				if($b['camp_type']=='variable_user'){
					$x=($b['camp_posx']/1.5);
					$y=($b['camp_posy']/1.5)+(0.352777778*25);
					//1 point = 0.352777778 milimetro
					$x=$x*(0.352777778);
					$y=$y*(0.352777778);
					$campo_concatenado = $b['camp_text'];
					foreach(array_keys($array_campos_y_valores_desc) as $campo){
						if($campo=='{%Buyer Name%}'){
							$campo_concatenado = str_replace($campo,$_POST['buyername'],$campo_concatenado);
						}elseif($campo=='{%Buyer Address Line 1%}'){
							$campo_concatenado = str_replace($campo,$_POST['baddress1'],$campo_concatenado);
						}elseif($campo=='{%Buyer Address Line 2%}'){
							$campo_concatenado = str_replace($campo,$_POST['baddress2'],$campo_concatenado);
						}elseif($campo=='{%Buyer Address Line 3%}'){
							$campo_concatenado = str_replace($campo,$_POST['baddress3'],$campo_concatenado);
						}else if($campo=='{%Contact Company%}'){
							if(strlen(trim($_POST['listingbroker']))>0){
								$campo_concatenado = str_replace("{%Contact Company%}",$_POST['listingbroker'],$campo_concatenado);
							}else{
								$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
							}
						}elseif($campo=='{%Contact Email 1%}'){
							if(strlen(trim($_POST['remail']))>0){
								$campo_concatenado = str_replace("{%Contact Email 1%}",$_POST['remail'],$campo_concatenado);
							}else{
								$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
							}
						}elseif($campo=='{%Contact Name%}'){
							if(strlen(trim($_POST['listingagent']))>0){
								$campo_concatenado = str_replace("{%Contact Name%}",$_POST['listingagent'],$campo_concatenado);
							}else{
								$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
							}
						}else{
							$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
						}
					}
					$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
					$campo_concatenado = str_replace("{%Initial Deposit%}",number_format($deposit,2),$campo_concatenado);
					$campo_concatenado = str_replace("{%Additional Deposit%}",number_format($additionalDeposit,2),$campo_concatenado);
					$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
					$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
					$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
					$campo_concatenado = str_replace("{%Balance To Close%}",number_format((int)$offer-(int)$deposit-(int)$additionalDeposit,2),$campo_concatenado);
					$campo_concatenado = str_replace("{%Offer Price%}",number_format((int)$offer,2),$campo_concatenado);
					$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
					$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
					$campo_concatenado = str_replace("{%Seller Name%}",$sellname,$campo_concatenado);
					$campo_concatenado = str_replace("{%Seller Address Line 1%}",$_POST['saddress1'],$campo_concatenado);
					$campo_concatenado = str_replace("{%Seller Address Line 2%}",$_POST['saddress2'],$campo_concatenado);
					$campo_concatenado = str_replace("{%Seller Address Line 3%}",$_POST['saddress3'],$campo_concatenado);
					$pdf->text($x,$y,$campo_concatenado);

				}

				if($b['camp_type']=='variable_math'){
					$x=($b['camp_posx']/1.5);
					$y=($b['camp_posy']/1.5)+(0.352777778*25);
					//1 point = 0.352777778 milimetro
					$x=$x*(0.352777778);
					$y=$y*(0.352777778);
					$campo_concatenado = str_replace(")","%}",str_replace("(","{%",str_replace("%}","",str_replace("{%","",$b['camp_text']))));
					foreach(array_keys($array_campos_y_valores_desc) as $campo){
						$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
					}
					//$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
					$campo_concatenado = str_replace("{%Initial Deposit%}",number_format($deposit,2),$campo_concatenado);
					$campo_concatenado = str_replace("{%Additional Deposit%}",number_format($additionalDeposit,2),$campo_concatenado);
					//$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
					//$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
					//$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
					$campo_concatenado = str_replace("{%Balance To Close%}",((int)$offer-(int)$deposit-(int)$additionalDeposit),$campo_concatenado);
					$campo_concatenado = str_replace("{%Offer Price%}",$offer,$campo_concatenado);
					//$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
					//$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
					$aux_math	=	explode(" ",$campo_concatenado);
					switch($aux_math[1]){
						case "+":
							$campo_concatenado	=	number_format(($aux_math[0]	+	$aux_math[2]),2,".",",");
							break;
						case "-":
							$campo_concatenado	=	number_format(($aux_math[0]	-	$aux_math[2]),2,".",",");
							break;
						case "*":
							$campo_concatenado	=	number_format(($aux_math[0]	*	$aux_math[2]),2,".",",");
							break;
						case "/":
							$campo_concatenado	=	number_format(($aux_math[0]	/	$aux_math[2]),2,".",",");
							break;
						case "%":
							$campo_concatenado	=	number_format(($aux_math[0]	*	($aux_math[2]/100)),2,".",",");
							break;
					}
					$pdf->text($x,$y,$campo_concatenado);

				}

				if($b['camp_type']=='check'){
					switch((int)$b['camp_idtc']){
						case 1:
							$imagen='../img/check1.jpg';
							$y=(($b['camp_posy']-2.5)/1.5);
							break;
						case 2:
							$imagen='../img/check2.jpg';
							$y=(($b['camp_posy']-1.5)/1.5);
							break;
						case 3:
							$imagen='../img/check3.jpg';
							$y=(($b['camp_posy']-2.5)/1.5);
							break;
					}
					$x=(($b['camp_posx']-3)/1.5);
					//$y=(((int)$b['camp_posy']-1.5)/1.5);
					//1 point = 0.352777778 milimetro
					$x=$x*(0.352777778);
					$y=$y*(0.352777778);
					$pdf->Image("..".$imagen,$x,$y,3,3,'JPG');
				}
				if($b['camp_type']=='signature'){

					$query = "SELECT imagen FROM xima.contracts_signature WHERE `userid`='$userid' AND `type`=$b[camp_idtc]";
					$result = mysql_query($query) or die("Error: C ".mysql_error());
					$imagen = mysql_fetch_assoc($result);
					$aux=explode("|",$b['camp_posx']);
					$x=$aux[0];
					$width=(($aux[1]/1.5)*0.352777778)-(0.352777778*3);

					$aux=explode("|",$b['camp_posy']);
					$y=$aux[0];
					$height=(($aux[1]/1.5)*0.352777778)-(0.352777778*1);

					$x=($x / 1.5);
					$y=($y / 1.5);
					//1 point = 0.352777778 milimetro
					$x=$x*(0.352777778);
					$y=$y*(0.352777778);
					//echo "<BR> X: ".$x." Y: ".$y." Width: ".$width." Height: ".$height;
					if(mysql_affected_rows()>0){
						$pdf->Image("../mysetting_tabs/mycontracts_tabs/signatures/".$imagen['imagen'],$x,$y,$width,$height);
					}
				}
			}
			$e=false;
			if($e==true){
				print_r($new);
				echo "<br>";
				echo (int)$new['h'];
			}
			unset($rows); //limpio el array
		}//Fin del Ciclo para cargar todas las Paginas
	}
	$pdf->SetTextColor(0,0,0);
    // --------------
    // ADD NEW PAGES WITH THE ADDENDUMS IF SELECTED

    if ($addendum=='on' and trim($addendata)<>"") {

        $pdf->SetAutoPageBreak(true);
        $pdf->SetTextColor(0,0,0);

        $y_axis_initial = 25;

        $arrEsp = explode('|', $addendata);
        $carr   = count($arrEsp);

        $pdf->AddPage('P',array($new['w'],$new['h']));

        $pdf->SetY(0.5); $pdf->SetX(1.3);

        $pdf->SetFont($contractFontName, '', 16);
        //$pdf->SetFont('Helvetica', 'B', 16);
        $pdf->Cell(0, 30, 'Addendums',0,0,'C');

        $pdf->SetY(25); $pdf->SetX(15);

        $text = '';

        for ($iK = 1; $iK < $carr; $iK++) {

            $query   = "SELECT content FROM xima.contracts_addendum
                            WHERE userid = $userid AND id=".$arrEsp[$iK]."
                            ORDER BY id ASC";
            $rs      = mysql_query($query);

            if ($rs) {
                $row = mysql_fetch_array($rs);

                $text .= utf8_encode(" - ".$row[0]."\n\n");

            }
        }
		$pdf->SetFont($contractFontName, '', $contractFontSize);
        //$pdf->SetFont('Helvetica', '', 11);
        $pdf->MultiCell(185,5,$text);
    }
    // --------------
    // SCROW DEPOSIT LETTER
	$address = "$array_campos_y_valores_idtc[538], $array_campos_y_valores_idtc[10], $array_campos_y_valores_idtc[536]";	//538 Property Address, 10 Property City, 536 Property Zip
    if ($scrow=='on') {

        // Text of the letter
        $date  = date('F j, Y',mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        /*$doc   = "\n\n$date\n\n";
        $doc  .= "Re: $array_campos_y_valores_idtc[734]\n\n";	//734 Buyer Name
        $doc  .= "Property Address: $address\n\n";
        $doc  .= "To Whom It May Concern:\n\n";
        $doc  .= "In connection with the above referenced real estate transaction, ";
        $doc  .= "please note that $array_campos_y_valores_idtc[752], has received a deposit from the\n\n"; //752 Escrow Agent
        $doc  .= "Buyer: $array_campos_y_valores_idtc[734]\n\n";	//734 Buyer Name
        //$doc  .= "In the amount of $ ".number_format($deposit,2)." as earnest money towards the purchase "; Comented By Kristy Freddy Frank and Jesus por orden de frank 10-08-2013
        $doc  .= "In the amount of $ 1,000.00 as earnest money towards the purchase ";
        $doc  .= "of the above captioned property on $date.\n\n";
        $doc  .= "If you have any questions, please do not hesitate to contact the undersigned.\n\n";*/

		//New Escrow Letter with kristy 29-09-2014
		$doc   	=	"\n".$date."\n\n";
		$doc	.=	"To Whom It May Concern,\n\n";
		$doc	.=	"This letter is to confirm that ".$array_campos_y_valores_idtc[734];
		$doc	.=	' has the amount of $1,000.00 in cleared funds* held in the escrow';
		$doc	.=	' account of '.$array_campos_y_valores_idtc[752].'. These';
		$doc	.=	" funds should be verified upon acceptance for a purchase and sale contract.\n\n";
		$doc	.=	"To verify these funds email us at ".$array_campos_y_valores_idtc[754]." and a";
		$doc	.=	' representative from our office will confirm this escrow deposit and confirm';
		$doc	.=	' application of this escrow deposit to your fully executed purchase and';
		$doc	.=	" sale agreement.\n\n";
		$doc	.=	'To ensure the quickest possible response please include the name of';
		$doc	.=	' your buyer listed on the contract, the property address, a copy of the';
		$doc	.=	" executed sales contract and your complete contact information.\n\n";
		$doc	.=	'If you have any additional questions please do not hesitate to email us';
		$doc	.=	" at ".$array_campos_y_valores_idtc[754]." or call us at ".$array_campos_y_valores_idtc[755].".\n";

        // Get header and Footer
        $query   = "SELECT imagen,place FROM xima.contracts_scrow
                        WHERE userid = $userid ORDER BY place ASC";
        $rs      = mysql_query($query);
        $path   = 'http://www.reifax.com/mysetting_tabs/mycontracts_tabs/scrows/';
        $imgSc  = null;
        if ($rs) {
            while ($row = mysql_fetch_array($rs)) {
                $imgSc["s{$row['place']}"] = $path.$row[0];
            }
        }

        $pdf->SetAutoPageBreak(false);

		$pdf->SetTextColor(0,0,0);

        $pdf->AddPage('P',array($new['w'],$new['h']));

        $pdf->SetFont('Helvetica', '', 13);

        if ($imgSc['s1']!='') {
            $pdf->SetY(5); $pdf->SetX(15);
            $pdf->Image($imgSc['s1'],null,null,185);
        }

        $pdf->SetY(80); $pdf->SetX(25);
        $pdf->MultiCell(160,5,$doc,0,'J');

        if ($imgSc['s2']!='') {
            $pdf->SetY(190); $pdf->SetX(15);
            $pdf->Image($imgSc['s2'],null,null,185);
        }
    }
	// --------------
    // ADD NEW PAGES WITH THE ADDONS IF SELECTED
	if(strlen(trim($addondata))>0){
		$total_addendum=explode("|",substr($addondata,1));
		foreach($total_addendum as $other){
			$query = "SELECT CONCAT('addons/',document) AS filename,CONCAT(id,'-',userid,'-',place) AS addon_id, emd, pof, `add`  FROM xima.contracts_addonscustom c WHERE c.userid='$userid' AND c.id='$other'";
			$result = mysql_query($query) or die("Error: A ".mysql_error());
			$r = mysql_fetch_array($result);
			$_POST['emd']=$_POST['emd']+$r['emd'];
			$_POST['pof']=$_POST['pof']+$r['pof'];
			$_POST['rademdums']=$_POST['rademdums']+$r['add'];
			$addon_id=$r['addon_id'];
			$page_A_count = $pdf->setSourceFile('../mysetting_tabs/mycontracts_tabs/'.$r['filename']);

			for($j=1;$j<=$page_A_count;$j++){
				$tplidx = $pdf->importPage($j, '/MediaBox');
				$new = $pdf->getTemplateSize($tplidx);
				$pdf->addPage('P', array($new['w'],$new['h'])); // lo dejo asi para que tome el tama�o original del documento
				$pdf->SetFont($addonFontName,'',$addonFontSize);
				$pdf->SetTextColor($addonColor[0],$addonColor[1],$addonColor[2]);

				$query = "SELECT * FROM xima.contracts_campos WHERE camp_userid='$userid' AND camp_contract_id='$addon_id' AND camp_page=$j";
				$result = mysql_query($query) or die("Error: C ".mysql_error());
				$rows = array();
				while($row=mysql_fetch_assoc($result)){
					$rows[]=$row;
				}
				////////////////OJO ESTO SE COLOCA ANTES O DESPUES; DEPENDE DE DONDE QUIERE QUE SALGA EL TEXTO////////////////
				$pdf->useTemplate($tplidx, 0, 0, $new['w'], $new['h']);
				foreach($rows as $b){
					if($b['camp_type']=='variable'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						switch($b['camp_idtc']){
							case "16":	//Legal Description
								$pdf->text($x,$y,"Legal Description as Shown in Public Records");
								break;
							case "20":	//Owner Name == Seller
								if(strlen(trim($sellname))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$sellname);
								break;
							case "95":	//Listing Price
								$pdf->text($x,$y,number_format($array_campos_y_valores_idtc[$b['camp_idtc']],2));
								break;
							case "716":	//Contac Name
								if(strlen(trim($_POST['listingagent']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['listingagent']);
								break;
							case "717":	//Contac Email 1
								if(strlen(trim($_POST['remail']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['remail']);
								break;
							case "734":	//Buyer Name
								if(strlen(trim($_POST['buyername']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['buyername']);
								break;
							case "735":	//Buyer Address Line 1
								$pdf->text($x,$y,$_POST['baddress1']);
								break;
							case "738":	//Buyer Address Line 2
								$pdf->text($x,$y,$_POST['baddress2']);
								break;
							case "739":	//Buyer Address Line 3
								$pdf->text($x,$y,$_POST['baddress3']);
								break;
							case "XX11":	//Seller Name
								$pdf->text($x,$y,$sellname);
								break;
							case "XX12":	//Seller Address Line 1
								$pdf->text($x,$y,$_POST['saddress1']);
								break;
							case "XX13":	//Seller Address Line 2
								$pdf->text($x,$y,$_POST['saddress2']);
								break;
							case "XX14":	//Seller Address Line 3
								$pdf->text($x,$y,$_POST['saddress3']);
								break;
							case "743":	//Contac Company
								if(strlen(trim($_POST['listingbroker']))<1)
									$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
								else
									$pdf->text($x,$y,$_POST['listingbroker']);
								break;
							case "XX1":	//Date Today
								$pdf->text($x,$y,date("m/d/Y"));
								break;
							case "XX2":	//Initial Deposit
								$pdf->text($x,$y,number_format($deposit,2));
								break;
							case "XX3": //Inspections Days
								$pdf->text($x,$y,$inspection);
								break;
							case "XX4":	//Acceptance Date
								$pdf->text($x,$y,date($dateAcc));
								break;
							case "XX5":	//Closing Date
								$pdf->text($x,$y,date($dateClo));
								break;
							case "XX6":	//Balance To Close
								$Balance_To_Close = (int)$offer-(int)$deposit-(int)$additionalDeposit;
								$pdf->text($x,$y,number_format($Balance_To_Close,2));
								break;
							case "XX7":	//Offer Price
								$offer_price=(int)$offer;
								$pdf->text($x,$y,number_format($offer_price,2));
								break;
							case "XX8":	//Buyer's Agent
								$pdf->text($x,$y,$buyeragent);
								break;
							case "XX9":	//Buyer's Broker
								$pdf->text($x,$y,$buyerbroker);
								break;
							case "XX10"://Additional Deposit
								$pdf->text($x,$y,number_format($additionalDeposit,2));
								break;
							default:
								$pdf->text($x,$y,$array_campos_y_valores_idtc[$b['camp_idtc']]);
						}
					}
					if($b['camp_type']=='variable_user'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						$campo_concatenado = $b['camp_text'];
						foreach(array_keys($array_campos_y_valores_desc) as $campo){
							if($campo=='{%Buyer Name%}'){
								$campo_concatenado = str_replace($campo,$_POST['buyername'],$campo_concatenado);
							}elseif($campo=='{%Buyer Address Line 1%}'){
								$campo_concatenado = str_replace($campo,$_POST['baddress1'],$campo_concatenado);
							}elseif($campo=='{%Buyer Address Line 2%}'){
								$campo_concatenado = str_replace($campo,$_POST['baddress2'],$campo_concatenado);
							}elseif($campo=='{%Buyer Address Line 3%}'){
								$campo_concatenado = str_replace($campo,$_POST['baddress3'],$campo_concatenado);
							}else if($campo=='{%Contact Company%}'){
								if(strlen(trim($_POST['listingbroker']))>0){
									$campo_concatenado = str_replace("{%Contact Company%}",$_POST['listingbroker'],$campo_concatenado);
								}else{
									$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
								}
							}elseif($campo=='{%Contact Email 1%}'){
								if(strlen(trim($_POST['remail']))>0){
									$campo_concatenado = str_replace("{%Contact Email 1%}",$_POST['remail'],$campo_concatenado);
								}else{
									$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
								}
							}elseif($campo=='{%Contact Name%}'){
								if(strlen(trim($_POST['listingagent']))>0){
									$campo_concatenado = str_replace("{%Contact Name%}",$_POST['listingagent'],$campo_concatenado);
								}else{
									$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
								}
							}else{
								$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
							}
						}
						$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
						$campo_concatenado = str_replace("{%Initial Deposit%}",number_format($deposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Additional Deposit%}",number_format($additionalDeposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
						$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
						$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
						$campo_concatenado = str_replace("{%Balance To Close%}",number_format((int)$offer-(int)$deposit-(int)$additionalDeposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Offer Price%}",number_format((int)$offer,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
						$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
						$campo_concatenado = str_replace("{%Seller Name%}",$sellname,$campo_concatenado);
						$campo_concatenado = str_replace("{%Seller Address Line 1%}",$_POST['saddress1'],$campo_concatenado);
						$campo_concatenado = str_replace("{%Seller Address Line 2%}",$_POST['saddress2'],$campo_concatenado);
						$campo_concatenado = str_replace("{%Seller Address Line 3%}",$_POST['saddress3'],$campo_concatenado);
						$pdf->text($x,$y,$campo_concatenado);
					}
					if($b['camp_type']=='variable_math'){
						$x=($b['camp_posx']/1.5);
						$y=($b['camp_posy']/1.5)+(0.352777778*25);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						$campo_concatenado = str_replace(")","%}",str_replace("(","{%",str_replace("%}","",str_replace("{%","",$b['camp_text']))));
						foreach(array_keys($array_campos_y_valores_desc) as $campo){
							$campo_concatenado = str_replace($campo,$array_campos_y_valores_desc[$campo],$campo_concatenado);
						}
						//$campo_concatenado = str_replace("{%Today Date%}",date("m/d/Y"),$campo_concatenado);
						$campo_concatenado = str_replace("{%Initial Deposit%}",number_format($deposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Additional Deposit%}",number_format($additionalDeposit,2),$campo_concatenado);
						//$campo_concatenado = str_replace("{%Inspection Date%}",$inspection,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Acceptance Date%}",$dateAcc,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Closing Date%}",$dateClo,$campo_concatenado);
						$campo_concatenado = str_replace("{%Balance To Close%}",number_format((int)$offer-(int)$deposit-(int)$additionalDeposit,2),$campo_concatenado);
						$campo_concatenado = str_replace("{%Offer Price%}",$offer,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Buyer Agent%}",$buyeragent,$campo_concatenado);
						//$campo_concatenado = str_replace("{%Buyer Broker%}",$buyerbroker,$campo_concatenado);
						$aux_math	=	explode(" ",$campo_concatenado);
						switch($aux_math[1]){
							case "+":
								$campo_concatenado	=	number_format(($aux_math[0]	+	$aux_math[2]),2,".",",");
								break;
							case "-":
								$campo_concatenado	=	number_format(($aux_math[0]	-	$aux_math[2]),2,".",",");
								break;
							case "*":
								$campo_concatenado	=	number_format(($aux_math[0]	*	$aux_math[2]),2,".",",");
								break;
							case "/":
								$campo_concatenado	=	number_format(($aux_math[0]	/	$aux_math[2]),2,".",",");
								break;
							case "%":
								$campo_concatenado	=	number_format(($aux_math[0]	*	($aux_math[2]/100)),2,".",",");
								break;
						}
						$pdf->text($x,$y,$campo_concatenado);
					}
					if($b['camp_type']=='check'){
						switch((int)$b['camp_idtc']){
							case 1:
								$imagen='../img/check1.jpg';
								$y=(($b['camp_posy']-2.5)/1.5);
								break;
							case 2:
								$imagen='../img/check2.jpg';
								$y=(($b['camp_posy']-1.5)/1.5);
								break;
							case 3:
								$imagen='../img/check3.jpg';
								$y=(($b['camp_posy']-2.5)/1.5);
								break;
						}
						$x=(($b['camp_posx']-3)/1.5);
						//$y=(((int)$b['camp_posy']-1.5)/1.5);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						$pdf->Image("..".$imagen,$x,$y,3,3,'JPG');
					}
					if($b['camp_type']=='signature'){

						$query = "SELECT imagen FROM xima.contracts_signature WHERE `userid`='$userid' AND `type`=$b[camp_idtc]";
						$result = mysql_query($query) or die("Error: C ".mysql_error());
						$imagen = mysql_fetch_assoc($result);
						$aux=explode("|",$b['camp_posx']);
						$x=$aux[0];
						$width=(($aux[1]/1.5)*0.352777778)-(0.352777778*3);

						$aux=explode("|",$b['camp_posy']);
						$y=$aux[0];
						$height=(($aux[1]/1.5)*0.352777778)-(0.352777778*1);

						$x=($x / 1.5);
						$y=($y / 1.5);
						//1 point = 0.352777778 milimetro
						$x=$x*(0.352777778);
						$y=$y*(0.352777778);
						//echo "<BR> X: ".$x." Y: ".$y." Width: ".$width." Height: ".$height;
						if(mysql_affected_rows()>0){
							$pdf->Image("../mysetting_tabs/mycontracts_tabs/signatures/".$imagen['imagen'],$x,$y,$width,$height);
						}
					}
				}
				//$e=true;
				if($e==true){
					print_r($new);
					echo "<br>";
					echo (int)$new['h'];
				}
			}//Fin del Ciclo para cargar todas las Paginas
		}
	}
	/*
	$tplidx = $pdf->importPage(2, '/MediaBox');
	$pdf->addPage('P');
	$pdf->getTemplateSize($tplidx);
	$pdf->useTemplate($tplidx, 0, 0, $new['w'],$new['h']);
	*/
	$valInContract = array(
		'emd' => (string)(($_POST['emd']>0) ? '0' : '1'),
		'pof' => (string)(($_POST['pof']>0) ? '0' : '1'),
		'rademdums' => (string)(($_POST['rademdums']>0) ? '0' : '1'),
		'contract' => (string)(($_POST['withContract']=='on') ? '0' : '1'),
		'offer'	=>	$offer
	);
	//$pdf->Output('newpdf.pdf', 'I');	/////////////Abre El Documento

	//$pdf->Output('../custom_contract/temp/'.$parcelid.'.pdf', 'F');
	$pdf->Output($_SERVER['REIFAX_DOCUMENT_ROOT'].'/custom_contract/temp/'.$parcelid.'.pdf', 'F');
	$archivo=$_SERVER['REIFAX_DOCUMENT_ROOT'].'/custom_contract/temp/'.$parcelid.'.pdf';

	/************************************************************
	*	Function Send Email and Saved Document and Saved on FollowUp
	************************************************************/
	include("../overview_contract/generatecontractFunctions.php");

	unset($rows);unset($r);unset($r2); //limpio el array
	if($sendbyemail=="yes"){
		$totalMailsSend	=	controlSendMails($userid);
		//Control de contratos
		$queryControl="INSERT INTO xima.control_contratos (userid, contrato, destinatario, fecha, parcelid, typeSend)
					VALUES ($userid,'$doc','$remail',NOW(),'$parcelid','OVERVIEW')";
		mysql_query($queryControl);
	}else{
		$totalMailsSend=0;
	}
	echo "{success:true, pdf:'custom_contract/temp/$parcelid.pdf',add:'$addendum',data:'$addendata',totalSend:$totalMailsSend}";
?>
