// Last updated August 2010 by Simon Sarris
// www.simonsarris.com
// sarris@acm.org
//
// Free to use and distribute at will
// So long as you are nice to people, etc

//Box object to hold data for all drawn rects
Ext.namespace('dibujos');

function Box() {
	this.x = 0;
	this.y = 0;
	this.w = 1; // default width and height?
	this.h = 1;
	this.fill = '#444444';
}

function Circle() {
	this.x = 0;
	this.y = 0;
	this.r = 10; //radio
	this.w = 0; //Start Angle
	this.a = 2 * Math.PI; //End Angle
	this.o = false;
	this.fill = '#00FF80';
}

function addCircle(x, y) {
	var cir = new Circle;
	//boxes_names.push(Ext.getCmp('idfieldcontract').getRawValue());
	cir.x = x;
	cir.y = y;
	circles.push(cir);
	invalidate();
}

dibujos.especial = function() {
	// for this method width and height determine the starting X and Y, too.
	// so I left them as vars in case someone wanted to make them args for something and copy this code
	var width = 20;
	var height = 20;
	addCircle(width, height);
}


//Initialize a new Box, add it, and invalidate the canvas
function addRect(x, y, w, h, fill) {
	var rect = new Box;
	boxes_names.push(Ext.getCmp('idfieldcontract').getRawValue());
	rect.x = x;
	rect.y = y;
	rect.w = w
	rect.h = h;
	rect.fill = fill;
	boxes.push(rect);
	invalidate();
}

// holds all our rectangles
var boxes = []; 
var circles = []; 
var boxes_names = [];

//var canvas;
var ctx;
//var WIDTH;
//var HEIGHT;
var INTERVAL = 20;  // how often, in milliseconds, we check to see if a redraw is needed

var isDrag = false;
var mx, my; // mouse coordinates

 // when set to true, the canvas will redraw everything
 // invalidate() just sets this to false right now
 // we want to call invalidate() whenever we make a change
var canvasValid = false;

// The node (if any) being selected.
// If in the future we want to select multiple objects, this will get turned into an array
var mySel; 
var text_add; 

// The selection color and width. Right now we have a red selection with a small width
var mySelColor = '#CC0000';
var mySelWidth = 2;

// we use a fake canvas to draw individual shapes for selection testing
//var ghostcanvas;
//var gctx; // fake canvas context

// since we can drag from anywhere in a node
// instead of just its x/y corner, we need to save
// the offset of the mouse when we start dragging.
var offsetx, offsety;

// Padding and border style widths for mouse offsets
var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;

// initialize our canvas, add a ghost canvas, set draw loop
// then add everything we want to intially exist on the canvas
function init() {
  canvas = document.getElementById('variablesid');
  HEIGHT = canvas.height;
  WIDTH = canvas.width;
  ctx = canvas.getContext('2d');
  ghostcanvas = document.createElement('canvas');
  ghostcanvas.height = HEIGHT;
  ghostcanvas.width = WIDTH;
  //ghostcanvas.parentNode.insertBefore(canvas, ghostcanvas);
  gctx = ghostcanvas.getContext('2d');
  //console.debug(document.getElementById('variablesid').offsetTop);
  //fixes a problem where double clicking causes text to get selected on the canvas
  canvas.onselectstart = function () { return false; }
  
  // fixes mouse co-ordinate problems when there's a border or padding
  // see getMouse for more detail
  if (document.defaultView && document.defaultView.getComputedStyle) {
    stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
    stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
    styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
    styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  }
  
  // make draw() fire every INTERVAL milliseconds
  setInterval(draw, INTERVAL);
  
  // set our events. Up and down are for dragging,
  // double click is for making new boxes
  canvas.onmousedown = myDown;
  canvas.onmouseup = myUp;
  canvas.ondblclick = myDblClick;
  canvas.onkeydown = myErase;
  
  // add custom initialization here:
  
  // add an orange rectangle
  //addRect(200, 200, 100, 13, '#FFFFFF');
  
  // add a smaller blue rectangle
  //addRect(25, 90, 100, 13, '#FFFFFF');
}

function myErase(evt){
	evt = evt || window.event;
	var e = evt.keyCode;
	if (e==46) {
		if(mySel != null){
			var ID = mySel[hash];
			alert(ID);
			canvas.remove(canvas.mySel);
		}
	}
};

//wipes the canvas context
function clear(c) {
  c.clearRect(0, 0, WIDTH, HEIGHT);
}

// While draw is called as often as the INTERVAL variable demands,
// It only ever does something if the canvas gets invalidated by our code
function draw() {
  if (canvasValid == false) {
    clear(ctx);
    
    // Add stuff you want drawn in the background all the time here
    
    // draw all boxes
    var l = boxes.length;
    for (var i = 0; i < l; i++) {
		text_add = boxes_names[i];
        drawshape(ctx, boxes[i], boxes[i].fill, text_add);
    }
    
	var l = circles.length;
    for (var i = 0; i < l; i++) {
		//text_add = boxes_names[i];
        drawshape1(ctx, circles[i], circles[i].fill, 'Circle');
    }
    // draw selection
    // right now this is just a stroke along the edge of the selected box
    if (mySel != null) {
      ctx.strokeStyle = mySelColor;
      ctx.lineWidth = mySelWidth;
      ctx.strokeRect(mySel.x,mySel.y,mySel.w,mySel.h);
    }
    
    // Add stuff you want drawn on top all the time here
    
    
    canvasValid = true;
  }
}

function aleatorio(inferior,superior){
   numPosibilidades = superior - inferior
   aleat = Math.random() * numPosibilidades
   aleat = Math.floor(aleat)
   return parseInt(inferior) + aleat
} 
function dame_color_aleatorio(){
   hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F")
   color_aleatorio = "#";
   for (i=0;i<6;i++){
      posarray = aleatorio(0,hexadecimal.length)
      color_aleatorio += hexadecimal[posarray]
   }
   return color_aleatorio
} 
// Draws a single shape to a single context
// draw() will call this with the normal canvas
// myDown will call this with the ghost canvas
function drawshape(context, shape, fill, text_add) {
  context.fillStyle = fill;
  
  // We can skip the drawing of elements that have moved off the screen:
  if (shape.x > WIDTH || shape.y > HEIGHT) return; 
  if (shape.x + shape.w < 0 || shape.y + shape.h < 0) return;
  context.fillRect(shape.x,shape.y,shape.w,shape.h);
  ctx.fillStyle = "#444444";
  context.fillText(text_add , shape.x, shape.y+10);
}
function drawshape1(context, shape, fill, text_add) {
  context.fillStyle = fill;
  
  // We can skip the drawing of elements that have moved off the screen:
  if (shape.x > WIDTH || shape.y > HEIGHT) return; 
  if (shape.x + shape.w < 0 || shape.y + shape.h < 0) return;
  //context.fillRect(shape.x,shape.y,shape.w,shape.h);
  context.arc(shape.x,shape.y,shape.r,shape.w,shape.a,shape.o);
  ctx.stroke();
  ctx.fillStyle = '#444444';
  ctx.fill();
  context.fillText(text_add , shape.x, shape.y);
  
}

// Happens when the mouse is moving inside the canvas
function myMove(e){
  if (isDrag){
    getMouse(e);
    mySel.x = mx - offsetx;
    mySel.y = my - offsety;   
	/*Ext.getCmp('x').setValue(mySel.x);
	Ext.getCmp('y').setValue(mySel.y);    */
    // something is changing position so we better invalidate the canvas!
    invalidate();
  }
}

// Happens when the mouse is clicked in the canvas
function myDown(e){
  getMouse(e);
  clear(gctx);
  var l = boxes.length;
  for (var i = l-1; i >= 0; i--) {
    // draw shape onto ghost context
	text_add = boxes_names[i];
    drawshape(gctx, boxes[i], 'black', text_add);
    // get image data at the mouse x,y pixel
    var imageData = gctx.getImageData(mx, my, 1, 1);
    var index = (mx + my * imageData.width) * 4;

    // if the mouse pixel exists, select and break
    if (imageData.data[3] > 0) {
      mySel = boxes[i];
      offsetx = mx - mySel.x;
      offsety = my - mySel.y;
      mySel.x = mx - offsetx;
      mySel.y = my - offsety;
      isDrag = true;
      canvas.onmousemove = myMove;
      invalidate();
      clear(gctx);
      return;
    }
    
  }

  var l = circles.length;
  for (var i = l-1; i >= 0; i--) {
    // draw shape onto ghost context
    drawshape1(gctx, circles[i], 'black', 'Circle');
    // get image data at the mouse x,y pixel
    var imageData = gctx.getImageData(mx, my, 1, 1);
    var index = (mx + my * imageData.width) * 4;

    // if the mouse pixel exists, select and break
    if (imageData.data[3] > 0) {
      mySel = circles[i];
      offsetx = mx - mySel.x;
      offsety = my - mySel.y;
      mySel.x = mx - offsetx;
      mySel.y = my - offsety;
	  mySel.a = 2 * Math.random(); //End Angle
      isDrag = true;
      canvas.onmousemove = myMoveCircle;
      invalidate();
      clear(gctx);
      return;
    }
    
  }  
  // havent returned means we have selected nothing
  mySel = null;
  // clear the ghost canvas for next time
  clear(gctx);
  // invalidate because we might need the selection border to disappear
  invalidate();
}

function myUp(){
  isDrag = false;
  canvas.onmousemove = null;
}

// adds a new node
function myDblClick(e) {
	if(Ext.getCmp('idfieldcontract').getValue()!=""){
		getMouse(e);
		// for this method width and height determine the starting X and Y, too.
		// so I left them as vars in case someone wanted to make them args for something and copy this code
		var width = 20;
		var height = 20;
		addRect(mx - (width / 2), my - (height / 2), 100, 13, "#00FF80");
	}
}

function invalidate() {
  canvasValid = false;
}

// Sets mx,my to the mouse position relative to the canvas
// unfortunately this can be tricky, we have to worry about padding and borders
function getMouse(e) {
      var element = canvas, offsetX = 0, offsetY = 0;

      if (element.offsetParent) {
        do {
          offsetX += element.offsetLeft;
          offsetY += element.offsetTop;
        } while ((element = element.offsetParent));
      }

      // Add padding and border style widths to offset
      offsetX += stylePaddingLeft;
      offsetY += stylePaddingTop;

      offsetX += styleBorderLeft;
      offsetY += styleBorderTop;

      mx = e.pageX - offsetX;
      my = e.pageY - offsetY
}

// If you dont want to use <body onLoad='init()'>
// You could uncomment this init() reference and place the script reference inside the body tag
//init();