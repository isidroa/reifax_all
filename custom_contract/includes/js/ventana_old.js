Ext.namespace('coor');

Ext.onReady(function(){ // Inicializamos 
		
	Ext.QuickTips.init();

	//Validacion del nombre (Permitira que solo se acepten letras)
	Ext.form.VTypes.nombreMask = /[A-Z\a-z]/;
	Ext.form.VTypes.nombre 	= function(v){
		return Ext.form.VTypes.nombreMask.test(v);
	};

	/////////////////Funcion para Mensajes/////////////////
    var msg = function(title, msg, ty){
        Ext.Msg.show({
            title: title,
            msg: msg,
            minWidth: 200,
            modal: true,
            icon: ty,
            buttons: Ext.Msg.OK
        });
    };

    var msg_pos = function(title, msg, ty, x, y){
        var msgbox = Ext.Msg.show({
            title: title,
            msg: msg,
            minWidth: 200,
            modal: true,
            icon: ty,
            buttons: Ext.Msg.OK
        }).getDialog();
		msgbox.setPagePosition(x,y);
    };
	
	///////////////VENTANA FALSA DE CARGANDO///////////////
	coor.loading_win=new Ext.Window({
		width:170,
		autoHeight: true,
		resizable: false,
		modal: true,
		border:false,
		closable:false,
		plain: true,
		html:'<div style="background-color: #fff; color:#6593cf; font-weight:bold; font-family:Arial,Helvetica,Tahoma,Verdana,sans-serif;"><div style="float:left;"><img src="../../../../../img/loading.gif"/></div><div style="float:left; margin:0 auto; padding-top:10px">Please wait, loading!...</div></div>'
	});
	
	coor.updateDetalleTemp = function(){
		var campos_objetos;
		campos_objetos='{"datos":[';
		var i=0;
		for(i=0;i<layerx.children.length;i++){
			if(layerx.children[i].attrs.name == "grupofirmas"){
				campos_objetos += '{"idvariable":"'+layerx.children[i].children[0].attrs.idtox+'","idtypex":"'+layerx.children[i].children[0].attrs.idtypex+'", "x":"'+layerx.children[i].attrs.x+'|'+layerx.children[i].children[0].attrs.width+'", "y":"'+layerx.children[i].attrs.y+'|'+layerx.children[i].children[0].attrs.height+'", "temp_text":"'+layerx.children[i].attrs.textox+'", "page":'+Ext.getCmp('idpagecontract').getValue()+'}';
			}else{
				campos_objetos += '{"idvariable":"'+layerx.children[i].attrs.idtox+'","idtypex":"'+layerx.children[i].attrs.idtypex+'", "x":'+layerx.children[i].attrs.x+', "y":'+layerx.children[i].attrs.y+', "temp_text":"'+layerx.children[i].attrs.textox+'", "page":'+Ext.getCmp('idpagecontract').getValue()+'}';
			}
			if(i!=layerx.children.length-1){
				campos_objetos += ',';
			}
		}			
		campos_objetos +=']}';
		//////////Validacion al ultimo Registro//////////
		if(layerx.children.length==0)
			var ultimo = Ext.getCmp('idpagecontract').getValue();
		else
			var ultimo = '0';
		//////////////////////Fin////////////////////////
		Ext.Ajax.request({
			waitMsg: 'Espere un momento...',
			url: '/custom_contract/includes/php/functions.php',
			method: 'post',
			params:	{
				idfunction:			4,
				ibjeto:				campos_objetos,
				ultimo_a_borrar:	ultimo //En realidad envio la pagina a borrar
			},
			success: function(response){
				var rest = Ext.util.JSON.decode(response.responseText);
				//console.debug(rest);
				if(rest.success==true){
					//Ext.MessageBox.alert('Information',rest.msg);
				}
				if(rest.success==false){
					Ext.MessageBox.alert('Warning',rest.msg);
				}
			},
			failure: function(response){
				//var result=response.responseText;
				Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
			}
		});//fin ajax request*/
	}
	
	coor.llenarPagina = function(){			
		Ext.Ajax.request({
			waitMsg: 'Espere un momento...',
			url: '/custom_contract/includes/php/functions.php',
			method: 'post',
			params:	{
				idfunction:	5,
				page: Ext.getCmp('idpagecontract').getValue()
			},
			success: function(response){
				var rest = Ext.util.JSON.decode(response.responseText);
				//console.debug(rest);
				if(rest.length>0){
					for(i=0;i<rest.length;i++){
						switch(rest[i].camp_type){
							case "variable":
								capas.variable_add(rest[i].camp_idtc,rest[i].camp_text,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy));
								break;
							case "variable_user":
								capas.variable_user_add(rest[i].camp_text,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy),rest[i].camp_idtc);
								break;
							case "check":
								capas.checksbox_add(rest[i].camp_idtc,parseInt(rest[i].camp_posx),parseInt(rest[i].camp_posy));
								break;
							case "signature":
								var aux = rest[i].camp_posx.split("|");
								var x = parseInt(aux[0]);
								var w = parseInt(aux[1]);
								var aux = rest[i].camp_posy.split("|");
								var y = parseInt(aux[0]);
								var h = parseInt(aux[1]);
								capas.firmas_add(rest[i].camp_idtc,rest[i].imagen,rest[i].type,x,y,w,h);
								//alert("signature");
								break;
						}
					}
				}
				if(rest.success==false){
					Ext.MessageBox.alert('Warning',rest.msg);
				}
			},
			failure: function(response){
				//var result=response.responseText;
				Ext.MessageBox.alert('Warning','No se puede hacer la edici�n');
			}
		});//fin ajax request*/
	}

	function obtener_coordenadas(campo_id,user_id,contract_id)
	{
		var store = new Ext.data.JsonStore({
			url: 'obtener_campos.php',
			method: 'post',
			baseParams: {
							campoid:campo_id,
							userid:user_id,
							contractid:contract_id},
			fields: [
				//{name:'posx', type:'int'},
				'canvas_x',
				'canvas_y' 
			]
		});
		//return store;
		store.load();
		
		store.on("load", function(s,rs) {
			 var myArray = new Array();
				store.each(function(record) {
				//alert(record.data.canvas_x);
				//myArray.push(record.data.canvas_x);
				Ext.getCmp('x').setValue(record.data.canvas_x);
				Ext.getCmp('y').setValue(record.data.canvas_y);
			 });
		});
		//console.debug(store);
	}

	var store_campos = new Ext.data.JsonStore({
		url: '/custom_contract/includes/php/functions.php',
		method: 'post',
		totalProperty: 'total',
		root: 'results',
		baseParams: {
						idfunction	:2,
					},
		fields: [
					//{name:'posx', type:'int'},
					{name:'id', type:'string'},
					{name:'name', type:'string'}
				],
		autoLoad: true
	});

	var store_temple = new Ext.data.JsonStore({
		url: '/custom_contract/includes/php/functions.php',
		method: 'post',
		totalProperty: 'total',
		root: 'results',
		baseParams: {
						idfunction	:6,
					},
		fields: [
					//{name:'posx', type:'int'},
					{name:'id', type:'string'},
					{name:'name', type:'string'},
					{name:'userid', type:'string'}
				],
		autoLoad: true
	});
	///////Metoddo para Ordenar un Store///////
	//store_campos.setDefaultSort('name', 'ASC');
	/*store_campos.load({
		callback: function(){
			store_campos.sort("name", "ASC");
		}
	});		*/

	var storetpl= new Ext.data.JsonStore({
			url:'/custom_contract/includes/php/functions.php',
			root: 'data',					
			totalProperty: 'num',
			baseParams: {
							idfunction	:3,
						},				
			fields: [
				{name:'id', type: 'string'},
				{name:'name', type: 'string'},
				{name:'des', type: 'string'},
				{name:'logo', type: 'string'},
				{name:'type', type: 'string'},
			]
	});
	
	var comboRemoteTpl = new Ext.form.ComboBox({
		fieldLabel:'Signature\'s',
		name:'cmb-Tpl',
		id:'cmb-Tpl',
		forceSelection:true,
		store:storetpl,
		width:215,
		emptyText:'Select One...',
		valueField:'id',
		triggerAction: 'all',
		mode:'remote',
		itemSelector: 'div.search-item',
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="background-image:url({logo})"><div class="name">{name}</div><div class="desc">{des}</div></div></tpl>'),
		displayField:'name',
		listeners:{
					select:	function(obj,record,index){
								coor.rut_imagen = record.data.logo;
								coor.typ_imagen = record.data.type;
							}			
				}
	});		

	var window_variable_users = new Ext.Window({
		title		:'Concatenate your Text hire with or without Fields',
		layout		:'fit',
		width		:550,
		height		:300,
		y			:100,
		//closable: false,
		//modal		:true,
		plain       :true,
		closeAction	:'hide',
		//resizable	:false,
		//constrain: true, //no se para que sirver
		//height:200,
		items:	[{
					xtype:		'form',
					id:			'window_variable_users_form',
					//frame:		true,
					//border:		false,
					labelWidth:	120,
					items:	[{
								xtype        : 'panel',
								layout       : 'table',
								layoutConfig : { columns : 2 },
								items        : [{
														layout: 'form',
														labelWidth	: 43,
														items:[{
								
																xtype: 			'combo',
																id: 			'idfield2contract',
																store: 			store_campos,
																editable: 		false,
																displayField:	'name',
																autoLoad:		true,
																valueField:		'id',
																width:150,
																name:			'idfield2contract',
																fieldLabel:		'Fields',
																mode:			'local',
																triggerAction:	'all',
																emptyText:		'Select One...',
																selectOnFocus:	true,
																//allowBlank:		false,
																listeners:		{
																					select:	{
																								fn:function(combo,value){
																									//obtener_coordenadas(combo.getValue(),1,1);
																									//init();
																								}
																							}
																				}
															}]
												},{
													xtype:		'button',
													icon:		'/img/add.gif',
													hideLabel:	true,
													id:			'add_concatenate',
													listeners:	{
																	click:	function(me, event){
																		var combo = Ext.getCmp('idfield2contract');
																		var textarea = Ext.getCmp('window_concatenate_users_texto');
																		textarea.setValue(textarea.getValue()+"{%"+combo.getRawValue()+"%}");
																	}
																}
												}],
							},{
								xtype        : 'panel',
								layout       : 'table',
								layoutConfig : { columns : 2 },
								items        : [{
													xtype:		'textarea',
													id:			'window_concatenate_users_texto',
													colspan:	2,
													name:		'window_concatenate_users_texto',
													allowBlank:	false,
													width:		500
												},{
													xtype:		'button',
													id:			'window_variable_users_clear',
													text:		'Clear Text',
													handler:	function(){
																	Ext.getCmp('window_concatenate_users_texto').setValue("");
													}
												},{
													xtype:		'button',
													id:			'window_variable_users_send',
													text:		'Send Text',
													handler:	function(){
														if (Ext.getCmp('window_variable_users_form').getForm().isValid()) { // Validamos el formulario
															if(capas.posx=='' || capas.posy==''){
																msg_pos('Warning', 'Please: First Select a Position over Contract PDF Page', Ext.Msg.WARNING,300,300);
															}else{
																var texto = Ext.getCmp('window_concatenate_users_texto').getValue()
																capas.variable_user(texto,capas.posx,capas.posy,(texto.length*9));
																window_variable_users.hide();
															}
														}
													}
												}]
							}]
				}]
	});

	var window_template = new Ext.Window({
		title		: '<h1 align="center">Template\'s Manager</h1>',
		layout		: 'fit',
		width		: 550,
		height		: 200,
		y			: 100,
		//closable	: false,
		//modal		: true,
		plain       : true,
		closeAction	: 'hide',
		//resizable	: false,
		//constrain	: true, //no se para que sirver
		//height:200,
		items		: [
			{
				xtype		: 'form',
				id			: 'window_template_form',
				//frame		: true,
				//border	: false,
				labelWidth	: 120,
				items:	[
					{
						xtype        : 'panel',
						layout       : 'table',
						layoutConfig : { columns : 4 },
						items        : [
							{
								layout		: 'form',
								colspan		: 4,
								labelWidth	: 85,
								items		: [
									{								
										xtype			: 'combo',
										id				: 'idtemplecontract',
										store			: store_temple,
										editable		: false,
										displayField	: 'name',
										autoLoad		: true,
										valueField		: 'id',
										width			: 400,
										name			: 'idtemplecontract',
										hiddenName		: 'idtemplecontracts',
										fieldLabel		: 'Set Templates',
										mode			: 'local',
										triggerAction	: 'all',
										emptyText		: 'Select template to copy',
										selectOnFocus	: true,
										//allowBlank	: false,
										listeners		: {
											select:	{
												fn:function(combo,record){
													/////Validamos para que no sea la Ocion de Seleccionar osea que no es un contrato/////
													if(combo.getValue()=="0001"){
														Ext.getCmp('templatebuttoncreate').show();
														Ext.getCmp('layout_template_name').show();
													}else{
														Ext.getCmp('templatebuttoncreate').hide();
														Ext.getCmp('layout_template_name').hide();
													}
													if(combo.getValue()!="0001" && combo.getValue()!="0000"){
														Ext.getCmp('templatebuttonuse').show();
													}else{
														Ext.getCmp('templatebuttonuse').hide();
													}
													if(combo.getValue()!="0001" && combo.getValue()!="0000" && record.data.userid!="0"){
														Ext.getCmp('templatebuttondelete').show();
														Ext.getCmp('templatebuttonupdate').show();
													}else{
														Ext.getCmp('templatebuttondelete').hide();
														Ext.getCmp('templatebuttonupdate').hide();
													}
												}
											}
										}
									}
								]
							},{
								xtype		: 'button',
								hidden		: true,
								colspan		: 2,
								text		: 'Set Template',
								icon		: '/img/add.gif',
								hideLabel	: true,
								id			: 'templatebuttonuse',
								handler		: function(){
									var aux = Ext.MessageBox.confirm('Confirm', 'Do you really want to set this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											//coor.loading_win.show();
											Ext.Ajax.request({
												url:		'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:		'post',
												params:		{
													idfunction:			7,
													template_original:	combo.getValue(),
													pages_new:			Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true")
														msg_pos('Notification', resp.mensaje, Ext.Msg.INFO,300,300);
														////////////////Actualizo la Pagina con el nuevo Template////////////////
														Ext.getCmp('idpagecontract').fireEvent('select',Ext.getCmp('idpagecontract'),Ext.getCmp('idpagecontract').store.getAt(Ext.getCmp('idpagecontract').selectedIndex+1));
													if(resp.success=="false")
														msg_pos('Warning', resp.mensaje, Ext.Msg.WARNING,300,300);
												}
											});
											var index = 0;
											combo.setValue(combo.store.getAt(index).get(combo.valueField));
											combo.selectedIndex = index;
											////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
											combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
										}
									}).getDialog();
									aux.setPagePosition(300,300);
								}
							},{
								xtype		: 'button',
								hidden		: true,
								text		: 'Update Temple',
								icon		: '/img/add.gif',
								hideLabel	: true,
								id			: 'templatebuttondelete',
								handler		: function(){
									var aux = Ext.MessageBox.confirm('Confirm', 'Do you really want to Update this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											Ext.Ajax.request({
												url:		'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:		'post',
												params:		{
													idfunction:			10,
													template_original:	combo.getValue(),
													pages_new:			Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true")
														msg_pos('Notification', resp.mensaje, Ext.Msg.INFO,300,300);
													if(resp.success=="false")
														msg_pos('Warning', resp.mensaje, Ext.Msg.WARNING,300,300);
												}
											});
											var index = 0;
											combo.setValue(combo.store.getAt(index).get(combo.valueField));
											combo.selectedIndex = index;
											////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
											combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
											store_temple.load();
										}
									}).getDialog();
									aux.setPagePosition(300,300);
								}								
							},{
								xtype		: 'button',
								hidden		: true,
								colspan		: 2,
								text		: 'Delete Temple',
								icon		: '/img/add.gif',
								hideLabel	: true,
								id			: 'templatebuttonupdate',
								handler		: function(){
									var aux = Ext.MessageBox.confirm('Confirm', 'Do you really want to Delete this template?', function(opt)
									{
										if(opt == 'yes'){
											var combo = Ext.getCmp('idtemplecontract');
											Ext.Ajax.request({
												url:		'/custom_contract/includes/php/functions.php',
												waitMsg:	'Coping...',
												method:		'post',
												params:		{
													idfunction:			9,
													template_original:	combo.getValue(),
													pages_new:			Ext.getCmp('idpagecontract').store.getCount()
												},
												success : function(r) 
												{
													var resp   = Ext.decode(r.responseText);
													if(resp.success=="true")
														store_temple.load();
														var index = 0;
														combo.setValue(combo.store.getAt(index).get(combo.valueField));
														combo.selectedIndex = index;
														////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
														combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));
														msg_pos('Notification', resp.mensaje, Ext.Msg.INFO,300,300);
													if(resp.success=="false")
														msg_pos('Warning', resp.mensaje, Ext.Msg.WARNING,300,300);
												}
											});
										}
									}).getDialog();
									aux.setPagePosition(300,300);
								}
							},{
								layout		: 'form',
								hidden		: true,
								colspan		: 3,
								id			: 'layout_template_name',
								labelWidth	: 90,
								items		: [
									{								
										xtype		: 'textfield',
										id			: 'template_name',
										width		: 200,
										maxLength	: 30,
										fieldLabel	: 'Template Name',
										allowBlank	: false
									}
								]
							},{
								xtype		: 'button',
								hidden		: true,
								//colspan		: 3,
								text		: 'Create Temple',
								icon		: '/img/add.gif',
								hideLabel	: true,
								id			: 'templatebuttoncreate',
								handler		: function(){
									if(Ext.getCmp('window_template_form').getForm().isValid()){
										Ext.Ajax.request({
											url:		'/custom_contract/includes/php/functions.php',
											waitMsg:	'Coping...',
											method:		'post',
											params:		{
												idfunction:			8,
												template_name:	Ext.getCmp('template_name').getValue()
											},
											success : function(r) 
											{
												var resp   = Ext.decode(r.responseText);
												if(resp.success=="true")
													store_temple.load();
													////////////////Ejecuto el Select del combo de los contratos para que oculte lo que tenga que ocultar////////////////
													var combo = Ext.getCmp('idtemplecontract');
													var index = 0;
													combo.setValue(combo.store.getAt(index).get(combo.valueField));
													combo.selectedIndex = index;
													combo.fireEvent('select',combo,combo.store.getAt(combo.selectedIndex+1));													
													Ext.getCmp('window_template_form').getForm().reset();
													msg_pos('Notification', resp.mensaje, Ext.Msg.INFO,300,300);
												if(resp.success=="false")
													msg_pos('Warning', resp.mensaje, Ext.Msg.WARNING,300,300);
											}
										});										
									}
								}
							}
						]
					}
				],
				buttons			:[
					{
						xtype		: 'button',
						text		: 'Close',
						icon		: '/img/add.gif',
						hideLabel	: true,
						id			: 'templatebuttonclose',
						handler		: function(){
							window_template.hide();
						}
					}
				]
				
			}
		]
	});
	
	w_ejemplo = function(){ var aux = new Ext.Window({ 	// Creamos nuestra ventana
		layout : 'fit',				//Ayuda a que el formulario que insertaremos quede ajustado a la perfeccion con nuestra ventana
		//title: 'Campos que Puede Agregar al Documento',
		width: Ext.getBody().getViewSize().width,
		height: 64,
		x:0,
		y:0,
		resizable: false,
		//plain: true,
		draggable: false,
		closable: false,
		border: false,				//Elimina el borde del interior de la ventana
		//closeAction: 'hide',		//Acci�n de ocultar ventana cuando la cerremos (No la estamos destruyendo)
		items: [					//Aqu� incluimos los componentes que estaran dentro de nuestra ventana (en este caso el formulario)
				{
				xtype: 'form',		//Creamos nuestro formulario
				id:'formulario',	//Asignamos un ID al formulario, el cual nos servira para invocarlo mas adelante
				frame: true,
				border: false,
				standardSubmit: true,
				memthod: 'post',
				/*defaults: {			
					width: 350		
				},*/
				labelWidth: 70,	//Asigna el ancho de las etiquetas
				items: [					//Aqu� incluimos los componentes que estaran dentro de nuestro formulario
						{
							xtype        : 'panel',
							layout       : 'table',
							layoutConfig : { columns : 13 },
							items        : [{
												layout: 'form',
													   labelWidth	: 30,
													   items:[{
																xtype: 			'combo',
																id: 			'idfieldcontract',
																store: 			store_campos,
																editable: 		false,
																displayField:	'name',
																autoLoad:		true,
																valueField:		'id',
																width:150,
																name:			'idfieldcontract',
																hiddenName:		'idfieldcontracts',
																fieldLabel:		'Fields',
																mode:			'local',
																triggerAction:	'all',
																emptyText:		'Select One...',
																selectOnFocus:	true,
																//allowBlank:		false,
																listeners:		{
																					select:	{
																								fn:function(combo,value){
																									//obtener_coordenadas(combo.getValue(),1,1);
																									//init();
																								}
																							}
																				}
															}]
											}
/*												,{
												xtype:			'button',
												icon:			'/img/add.gif',
												toggleGroup:	'mygroup',
												enableToggle:	true,
												id:				'variable_standar',
												listeners:	{
																click:	function(me, event){
/*																				if(layerx==undefined){
																				crear();
																			}
																			var combo			= Ext.getCmp('idfieldcontract');
																			var value_display	= combo.getRawValue();
																			var value_id		= combo.getValue();
																			if(value_id==''){
																				Ext.MessageBox.alert('Warning','Select a Option');
																				me.toggle(false);
																			}else{
																				//var index 			= combo.selectedIndex - 1;
																				var a = capas.variable(value_id,value_display);
																				// Dejo Marcado el Botton
																				if(a==false){
																					me.toggle(false);
																				}else{
																					me.toggle(true);
																				}
																			}*/
/*																			}
															}
											}*/
											,{
												xtype:		'button',
												width: 		50,
												scale:		'medium',
												tooltip:	'Preview Page',
												icon:		'/img/prev1.png',
												listeners:	{
																click:	function(){
																			var combo = Ext.getCmp('idpagecontract');
																			var index = combo.selectedIndex - 1;
																			if (index < 0) {
																				index = combo.store.getCount() - 1;
																			}
																			combo.setValue(combo.store.getAt(index).get(combo.valueField));
																			combo.selectedIndex = index;
																			combo.fireEvent('select', combo, combo.store.getAt(index), index);
																			//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
																			layerx.removeChildren();
																			//Con esto Limpiamos el Canvas a nivel Visual
																			layerx.clear();
																		}
															}
											},{
												xtype: 			'combo',
												id: 			'idpagecontract',
												store: 			new Ext.data.SimpleStore({
																	fields: ['id', 'page'],
																	data : window.pagess.paginas
																}),
												editable: 		false,
												displayField:	'page',
												valueField:		'id',
												width:90,
												name:			'idpagecontract',
												hiddenName:		'idpagecontracts',
												//fieldLabel:		'Pages',
												mode:			'local',
												triggerAction:	'all',
												emptyText:		'Seleccione un Campo...',
												value:			1,
												selectOnFocus:	true,
												allowBlank:		false,
												listeners:		{
																	select:	{
																				fn:function(combo,value){
																					window.pagess.pdf_new(combo.getValue());
																					//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
																					if(layerx!=undefined){
																						layerx.removeChildren();
																						//Con esto Limpiamos el Canvas a nivel Visual
																						layerx.clear();																										
																					}else{
																						crear();
																					}
																					coor.llenarPagina();
																					layerx.draw();
																				}
																			}
																}
											},{
												xtype:		'button',
												width: 		50,
												scale:		'medium',
												tooltip:	'Next Page',
												icon:		'/img/next1.png',
												//iconAlign:	'right',
												listeners:	{
																click:	function(){
																			var combo = Ext.getCmp('idpagecontract');
																			var index = combo.selectedIndex + 1;
																			if (index < 0) {
																				index = combo.store.getCount() + 1;
																			}
																			if (index == combo.store.data.length){
																				index = 0;
																			}
																			combo.setValue(combo.store.getAt(index).get(combo.valueField));
																			combo.selectedIndex = index;
																			combo.fireEvent('select', combo, combo.store.getAt(index), index);
																			//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
																			layerx.removeChildren();
																			//Con esto Limpiamos el Canvas a nivel Visual
																			layerx.clear();																				
																		}
															}
											},{
												xtype:		'button',
												//text:		'Add',
												icon:		'/img/check1.png',
												//iconAlign:	'right', //Solo se Agrega si lleva Texto
												toggleGroup:'mygroup',
												tooltip:	'Add a point to the Checkbox',
												id:			'check1',
												scale:		'medium',
												enableToggle: true,
												listeners:	{
																click:	function(me, event){
/*																				if(layerx==undefined){
																				crear();
																			}																	
																			var a = capas.checksbox(1);
																			//window.dibujos.addCircle(10,10,13,13,"#00FF80",".");
																			// Dejo Marcado el Botton
																			if(a==false){
																				me.toggle(false);
																			}else{
																				me.toggle(true);
																			}*/
																		}
															}
											},{
												xtype:		'button',
												//text:		'Add',
												icon:		'/img/check2.png',
												//iconAlign:	'right',
												toggleGroup:'mygroup',
												tooltip:	'Add an X to the Checkbox',
												id:			'check2',
												scale:		'medium',
												enableToggle: true,
												listeners:	{
																click:	function(me, event){
/*																				if(layerx==undefined){
																				crear();
																			}																	
																			var a = capas.checksbox(2);
																			//window.dibujos.addX(10,10,13,13,"#00FF80","x");
																			// Dejo Marcado el Botton
																			if(a==false){
																				me.toggle(false);
																			}else{
																				me.toggle(true);
																			}*/
																		}
															}
											},{
												xtype:		'button',
												//text:		'Add',
												//icon:		'/img/myemail/cerrar.png',
												icon:		'/img/check3.png',
												//iconAlign:	'right',
												toggleGroup:'mygroup',
												tooltip:	'Add a tick to the Checkbox',
												id:			'check3',
												scale:		'medium',
												enableToggle: true,
												listeners:	{
																click:	function(me, event){
/*																				if(layerx==undefined){
																				crear();
																			}																	
																			var a = capas.checksbox(3);
																			//window.dibujos.addX(10,10,13,13,"#00FF80","x");
																			// Dejo Marcado el Botton
																			if(a==false){
																				me.toggle(false);
																			}else{
																				me.toggle(true);
																			}*/
																		}
															}
											},{
												layout: 'form',
														labelWidth	: 60,
														items:[
																comboRemoteTpl
															]
											}
/*												,{
												xtype:		'button',
												icon:		'/img/add.gif',
												toggleGroup:'mygroup',
												id:			'signature_initial',
												enableToggle: true,													
												listeners:	{
																click:	function(me, event){
/*																				if(layerx==undefined){
																				crear();
																			}																	
																			var combo		= Ext.getCmp('cmb-Tpl');
																			var value_id	= combo.getValue();
																			//var index = combo.selectedIndex - 1;
																			//console.debug(combo.getValue());
																			//console.debug(combo.getStore()[combo.getValue()]);
																			if(value_id==''){
																				Ext.MessageBox.alert('Warning','Select a Option');
																				me.toggle(false);
																			}else{
																				var a = capas.firmas(value_id,coor.rut_imagen,coor.typ_imagen);
																				// Dejo Marcado el Botton
																				if(a==false){
																					me.toggle(false);
																				}else{
																					me.toggle(true);
																				}
																			}*/
/*																			}
															}
											}*/
											,{
												xtype:		'button',
												//text:		'Add text',
												tooltip:	'Add text',
												//colspan:	10,
												icon:		'/img/icons_jesus/Actions-list-add-font-24.png',
												scale:		'medium',
												listeners:	{
																click:	function(){
																			window_variable_users.show(this);
																			//Con esta Funcion removemos todos los Hijos que tenga en ese momento el Canvas
																			//layerx.removeChildren();
																			//Con esto Limpiamos el Canvas a nivel Visual
																			//layerx.clear();
																		}
															}
											},{
												xtype:		'button',
												//text:		'Preview Page',
												icon:		'/img/icons_jesus/Preview-24.png',
												scale:		'medium',
												tooltip:	'Preview Page',
												handler:	function(){							
																var cmp = Ext.getCmp('formulario'); 
																var form = cmp.getForm();
																var el = form.getEl().dom;
																 
																var target = document.createAttribute("target");
																var campos_objetos;

																campos_objetos='{"datos":[';
																var i=0;
																for(i=0;i<layerx.children.length;i++){
																	if(layerx.children[i].attrs.name == "grupofirmas"){
																		campos_objetos += '{"idvariable":"'+layerx.children[i].children[0].attrs.idtox+'","idtypex":"'+layerx.children[i].children[0].attrs.idtypex+'", "x":"'+layerx.children[i].attrs.x+'|'+layerx.children[i].children[0].attrs.width+'", "y":"'+layerx.children[i].attrs.y+'|'+layerx.children[i].children[0].attrs.height+'", "temp_text":"'+layerx.children[i].children[0].image.src+'", "page":'+Ext.getCmp('idpagecontract').getValue()+'}';
																	}else{
																		campos_objetos += '{"idvariable":"'+layerx.children[i].attrs.idtox+'","idtypex":"'+layerx.children[i].attrs.idtypex+'", "x":'+layerx.children[i].attrs.x+', "y":'+layerx.children[i].attrs.y+', "temp_text":"'+layerx.children[i].attrs.textox+'"}';
																	}
																	if(i!=layerx.children.length-1){
																		campos_objetos += ',';
																	}
																}			
																campos_objetos +=']}';
																Ext.getCmp('hidden_objetos').setValue(campos_objetos); 

																target.nodeValue = "_blank";
																el.action = "test.php"; 
																el.setAttributeNode(target);
																el.submit();
																/*Ext.getCmp('formulario').getForm().getEl().dom.action = 'test.php';
																Ext.getCmp('formulario').getForm().target = '_blank';
																Ext.getCmp('formulario').getForm().submit();
																var combo		= Ext.getCmp('idpagecontract');
																var value_id	= combo.getValue();*/
																//alert(value_id);
																//updateDetalleTemp();
												}
											},{
												xtype	: 'button',
												tooltip	: 'Template\'s Manager',
												icon	: '/img/icons_jesus/Mimetype-templates-24.png',
												scale	: 'medium',
												handler	: function(){
													window_template.show();
												}
											}
/*											,{
												xtype:		'button',
												tooltip:	'Save Changes',
												scale:		'medium',
												icon:		'/img/toolbar/save.png',							
												handler:	function(){
																updateDetalleTemp();
												}
											}*/
											],
						},{
							xtype:	'hidden',
							id:		'hidden_objetos',
							name:	'hidden_objetos'
						}
					],
				}
			],
			buttons:[			// Aqui incluiremos los botones del formulario
/*						,{
						xtype: 'button',
						text: 'Preview All',
						handler: function(){
							alert(window.pagess.totalpaginas);
							//updateDetalleTemp();
						}
					},{
						xtype: 'button',
						text: 'Insert or Update',
						handler : function() {
							if (Ext.getCmp('formulario').getForm().isValid()) { // Validamos el formulario
								Ext.getCmp('formulario').getForm().submit({
									url : 'savecoordenadas.php',
									waitMsg : 'Salvando datos...',
									params: {
										userid:			"1",
										contractid:		"1"
									},
									failure: function (form, action) {
										Ext.MessageBox.show({
											title: 'Error al salvar los datos',
											msg: 'Error al salvar los datos.',
											buttons: Ext.MessageBox.OK,
											icon: Ext.MessageBox.ERROR
										});
									},
									success: function (form, request) {
										Ext.MessageBox.show({
											title: 'Datos salvados correctamente',
											msg: 'Datos salvados correctamente',
											buttons: Ext.MessageBox.OK,
											icon: Ext.MessageBox.INFO
										});
										obtener_coordenadas(Ext.getCmp('campoid').getValue(),1,1);
										/*responseData = Ext.util.JSON.decode(request.response.responseText);
										formCategories.getForm().load({
											url : 'formLoader.php',
											method: 'GET',
											params: {
												cat_id: responseData.cat_id
											},
											waitMsg : 'Espere por favor'
										});*/
/*										}
								});
							}
						}
					}*/
					]
			
		});
		aux.show();
		//Inicializo el Combo en el Primer Valor = 0
		Ext.getCmp('idpagecontract').selectedIndex = 0;
		//Ext.getCmp('idpagecontract').fireEvent('select', Ext.getCmp('idpagecontract'), Ext.getCmp('idpagecontract').store.getAt(0), 0);
	}
	//store_campos.load();
	//	w_ejemplo.show(w_ejemplo);		// Mostramos la ventana
});