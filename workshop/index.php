<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	if($_COOKIE[customerRegister])
	{
		foreach($_COOKIE[customerRegister] AS $key =>$value)
		{
			setcookie("customerRegister[$key]",false,time()-3600,'/');
		}
	unset($_COOKIE['customerRegister']);
	}
	
	$sql='SELECT * FROM workshop WHERE status=1 LIMIT 1;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$data=mysql_fetch_assoc($res);
	$sql='SELECT * FROM xima.usr_productobase where idproducto=13 AND activo=1 LIMIT 1	;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$cost=mysql_fetch_assoc($res);
	
	
	/***************
	*	PRODUCTOS, FREQUENCIA, FECHA DEL PAGO del usuario
	***************/
	$sql='SELECT p.homefinder, p.buyerspro, p.leadsgenerator, p.residential, p.platinum, p.professional, p.professional_esp,f. idfrecuency frecuency ,c.fechacobro
		FROM permission p,f_frecuency f ,usr_cobros c
		where p.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.userid=f.userid AND p.userid=c.userid limit 1';
	$respuesta=mysql_query($sql);
	if(mysql_num_rows($respuesta)>0)
	{
		$status=mysql_fetch_assoc($respuesta);
		if(($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1) && $status['frecuency']==2)
		{
			$suscritor=true;
		}
	}
	
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div	 class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Workshop</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-- >
    <div class="sidebarright">
    </div>
    <!-- Center Content-->
    <div class="">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Workshop Overview - Learn with the help from our experts</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs">
                	<h4><?php echo $data['title'] ?></h4>
                        <div>
                         <p><span class="bold bluetext">If you are subscribed to Reifax, log in into the system and then click the workshop banner in order to fill the workshop subscription automatically.</span></p>
                         <p>From the desk of <span class="bold"><?php echo $data['speaker'] ?></span></p>
                        <p>Join us on <span class="bold highlighted-text"><?php echo $data['when'] ?></span> for this LIVE training where we'll be showcasing the NEW Communications System for ReiFax Professional Subscribers.</p>
                        <p  class="bold">
                        What's NEW on the Communication System?</p>
                        <ul><li class="bold">
                        	<div class="cuote"></div>Manage your <span class="redtext">Email Messages</span> within the ReiFax Professional Follow-up System</li><li class="bold">
                        	<div class="cuote"></div>Send Follow-up <span class="redtext">SMS (Text) Messages</span> to the Contacts of the properties on your Follow-up System</li><li class="bold">
                        	<div class="cuote"></div>Keep all <span class="redtext">Voice Mail Messages</span> related to a Property organized within your Follow-up System</li><li class="bold">
                        	<div class="cuote"></div>Send <span class="redtext">Contracts by Fax</span> by Connecting the Follow-up System with your electronic fax system</li><li class="bold">
                        	<div class="cuote"></div><span class="redtext">How to use the Contract Manager</span> (Add, fill, send contracts and its documents.)</li><li class="bold">
                        	<div class="cuote"></div><span class="redtext">How to make calls from the Follow up System.</span></li><li class="bold">
                        	<div class="cuote"></div>All this added to the already existent <span class="redtext">Bulk Email Contract Delivery System</span> built-in your ReiFax Professional Follow-up System</li>
                        </ul>

                        <p><span class="bold">Who Should come to this Training?</span> All Real Estate professionals looking to automate and organize the process of making offers and following up with this offers.</p>
						<p><span class="bold">Can I come even if I am not a ReiFax Professional subscriber?</span> Yes, this is a great opportunity for you to get to see the ReiFax professional features in action, plus all ReiFax Platinum subscribers will be given an opportunity to try the ReiFax Professional subscription FREE. You should come even if you are not a ReiFax subscriber, this is a great opportunity for you to see the system in action.</p>
						<p><span class="bold">Here are the details about the Event:</span></p>
                        <table>
                        	<tr>
                            	<td align="right" valign="top">
                                	<span class="bold redtext">When:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext"><?php echo $data['when'] ?> at <?php echo $data['hour'] ?><br> <?php echo $data['whennote'] ?></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="right" valign="top">
                                	<span class="bold redtext">Where:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext"><?php echo $data['address'] ?><br> <?php echo $data['state'] ?>, <?php echo $data['city'] ?>, <?php echo $data['zipcode'] ?><br><?php echo $data['phone'] ?></span>
                                     <a href="<?php echo $data['linkMap'] ?>" target="_blank">Map & Directions</a>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="right">
                                	<span class="bold redtext">Price:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext">$<?php echo ($suscritor)?'0.00':number_format($cost['precio'],2) ?></span>
                                </td>
                            </tr>
							<!--
                        	<tr>
                            	<td align="right">
                                	<span class="bold redtext">At the door:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext">$<?php echo ($suscritor)?'0.00':'99.00' ?></span>
                                </td>
                            </tr>-->
                        </table>
                        <div class="clear">
                        </div>
                        <div class="centertext">
                        	<a href="<?php echo ($suscritor)?'../register/registerWorkshopFree.php':'../register/registerWorkshop.php' ?>" class="buttongreen bigButton">Click here to register</a>
                        </div>
     					</div> 
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-training');
</script>