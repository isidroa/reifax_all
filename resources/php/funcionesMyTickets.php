<?php
require_once("properties_conexion.php");
require("phpmailer/class.phpmailer.php");
conectar('xima');
session_start();
switch($_POST['option'])
{
	case('loadImg'):
		$aux=$_POST['picture'];
		$archivo=$_FILES[$aux]['tmp_name'];
		$name=$_FILES[$aux]['name'];
		$name=explode('.',$name);
		$destinoF='../../temfile/';
		if($_SESSION[$aux]['f'])
		{
			unlink($_SESSION[$aux]['f']);
			unset($_SESSION[$aux]);
		}
		$auxFi=(md5($_SERVER['REMOTE_ADDR'].date("Y m j h i s"))).'.'.$name[count($name)-1];
		$destinoF=$destinoF.$auxFi;
		$destinoL='/temfile/'.$auxFi;
		if (copy($archivo,$destinoF))
		{
			$_SESSION[$aux]['f']=$destinoF;
			$_SESSION[$aux]['l']=$destinoL;
			echo json_encode(array('success'=>true,'img' => $destinoL,'destino'=> $destinoF, 'imgOri' =>$name[0]));
		}
		else 
		{
			echo json_encode(array('success'=>false,'destino'=> $destinoF));
		}
	
	break;	
	case('listTickets'):
		$start = isset($_POST['start'])?$_POST['start']:0; //posición a iniciar  
		$limit = isset($_POST['limit'])?$_POST['limit']:50; //número de registros
		$user = isset($_POST['user'])?$_POST['user']:$_COOKIE['datos_usr']['USERID']; 
		$where='where c.useridclient='.$user;
		$where.= ($_POST['status']!='')?' AND c.status '.$_POST['status']:'';
		$sql='select  c.idcs,
			c.useridclient,concat( u.name ," " ,u.surname) cliente, c.useridcustomer, concat( cu.name ," " ,cu.surname) customer, c.useridprogrammer,concat(p.name ," " ,p.surname) programador,
			u.HOMETELEPHONE phone, u.MOBILETELEPHONE mobile, u.email ,
			c.id_product,pr.name as productName, c.browser, c.os, 
			(msg.id_msg) msg_cliente, (msg_sys.id_msg) msg_system, 
			c.state, co.County, c.idcounty, c.menutab, c.submenutab,c.proptype, c.errortype, c.search, c.errordescription,
			c.`status`, c.datel, c.dated, c.datef, c.datefc datec, c.errortype2, c.soldescription, c.ticket, c.source 
			
			from `xima`.`customerservices` c 
				LEFT JOIN xima.ximausrs u ON c.useridclient=u.userid 
				LEFT JOIN xima.usr_producto pr ON pr.idproducto=c.id_product 
				LEFT JOIN xima.ximausrs cu ON c.useridcustomer=cu.userid 
				LEFT JOIN xima.ximausrs p ON c.useridprogrammer=p.userid 
				LEFT JOIN xima.lscounty co ON c.idcounty=co.IdCounty 
    left JOIN (select id_ticket, status_msg_back,id_msg from xima.customerservice_msg where status_msg_back=1 group by id_ticket) msg ON msg.id_ticket=c.idcs AND msg.status_msg_back=1 
    left JOIN (select id_ticket, status_msg_system,id_msg from xima.customerservice_msg where status_msg_system=1 group by id_ticket) msg_sys ON msg_sys.id_ticket=c.idcs AND msg_sys.status_msg_system=1 
			'.$where.'
			order by c.idcs desc'
	;
		$result= mysql_query($sql);
		$data=array();
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> array_splice($data,$start,$limit),
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	case('createTicket'):
		$_POST['state']=explode('|',$_POST['state']);
		$aux=mysql_query('SELECT * FROM `xima`.`xima_system_var` where userid='.$_COOKIE['datos_usr']['USERID']);
		$backupData=mysql_fetch_assoc($aux);
		$aux=mysql_query('select (ticket+1) as ticket from customerservices order by ticket desc limit 1');
		$aux2=mysql_fetch_assoc($aux);
		$_POST['Product']=($_POST['Product']!='')?$_POST['Product']:NULL;
		$sql='INSERT INTO `xima`.`customerservices`
			(
			`useridclient`,
			`browser`,
			`os`,
			`state`,
			`idcounty`,
			`menutab`,
			`submenutab`,
			`proptype`,
			`errortype`,
			`search`,
			`errordescription`,
			`datel`,
			`errortype2`,
			`ticket`,
			`source`,
			`id_product`,
			`backup_search`,
			`status`)
			VALUES
			(
			'.$_COOKIE['datos_usr']['USERID'].',
			"'.$_POST['browser'].'",
			"'.$_POST['os'].'",
			"'.$_POST['state'][0].'",
			'.$_POST['county'].',
			"'.$_POST['data'].'",
			"'.$_POST['seccion'].'",
			"'.$_POST['propertyType'].'",
			"'.$_POST['issues'].'",
			"'.$_POST['search'].'",
			"'.mysql_real_escape_string(utf8_encode($_POST['issuesDescri'])).'",
			NOW(),
			"'.$_POST['issuesAbout'].'",
			'.$aux2['ticket'].',
			"System",
			1,
			"'.(($backupData)?mysql_real_escape_string(json_encode($backupData)):'').'",
			"Open"
			);';
	$result=mysql_query($sql);
	if(mysql_affected_rows()>0)
		{
			$ticket=mysql_insert_id ();
			for($i=1;$i<3;$i++)
			{
				if(isset($_SESSION['image'.$i]['f']) )
				{
					$auxTem=explode('.',$_SESSION['image'.$i]['f']);
					$arc=(md5($_SERVER['REMOTE_ADDR'].date("Y m j h i s").$name[0]).rand(1,100)).'.'.$auxTem[count($auxTem)-1];
					$destinoF='../../imgTickets/'.$arc;
					$destinoL='/imgTickets/'.$arc;
					if (copy($_SESSION['image'.$i]['f'],$destinoF)) 
					{
						$sql= 'INSERT INTO `xima`.`customerservices_img`
							(
							`url_archivo`,
							`uri_archivo`,
							`id_ticket`)
							VALUES
							(
								"'.$destinoL.'",
								"'.$destinoF.'",
								'.$ticket.'
							)';
						$respuesta=mysql_query($sql);
					}
				}
			}
			echo json_encode(array('success'=>true, 'ticket' => $ticket, 'error' => mysql_error()));
		}
		else
		{
			echo json_encode(array('success'=>false, 'sql' => $sql, 'error' => mysql_error()));
		}
	break;
	/*********************************
	*** ADD img to ticket
	************************************/
	case ('addImages'):
		$ticket=$_POST['ticket'];
		foreach ($_FILES as $aux)
		{
			if(isset($aux))
			{
				$archivo=$aux['tmp_name'];
				$name=explode('.',$aux['name']);
				$arc=(rand(1,100).md5($_SERVER['REMOTE_ADDR'].date("Y m j h i s").$name[0])).'.'.$name[count($name)-1];
				$destinoF=$_SERVER['DOCUMENT_ROOT'].'/imgTickets/'.$arc;
				$destinoL='/imgTickets/'.$arc;
				if (copy($archivo,$destinoF)) 
				{
					$sql= 'INSERT INTO `customerservices_img`
						(
						`url_archivo`,
						`uri_archivo`,
						`id_ticket`)
						VALUES
						(
							"'.$destinoL.'",
							"'.$destinoF.'",
							'.$ticket.'
						)';
					$respuesta=mysql_query($sql);
					if(mysql_affected_rows()<1)
					{
						$errorImg=true;	
					}
					
				}
			}
		}
		echo json_encode(array(
			'success' => true,
			'sql' => $sql,
			'img' => $img,
			'imgError' => $errorImg
		));
	break;
	/*********************************
	*** LISTAR IMAGENES
	************************************/
	case ('listarImg'):
		$sql='SELECT url_archivo img FROM `xima`.customerservices_img where id_ticket='.$_POST['id'].';';
		$result= mysql_query($sql);
		$data=array();
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> $data,
			'sql' => $sql,
			'mysql Error' => mysql_error(),
			'total' => count($data)
		));
	break;
	/*********************************
	*** LISTAR MENSAJES
	************************************/
	case ('listarMsg'):
		$sql='SELECT concat( u.name ," " ,u.surname) `user`, date_msg dateMsg,texto_msg msg FROM `xima`.`customerservice_msg` m LEFT JOIN `xima`.`ximausrs` u on u.userid=m.remitiente_msg where m.id_ticket='.$_POST['id'].';';$result= mysql_query($sql);
		$data=array();
		mysql_query('UPDATE `xima`.`customerservice_msg`
		SET
		`status_msg_system` = 2
		WHERE id_ticket=
		'.$_POST['id'].';');
		while ($aux=mysql_fetch_assoc($result))
		{
			array_push($data,$aux);
		}
		echo json_encode(array(
			'success'	=> true,
			'data'	=> $data,
			'total' => count($data)
		));
	break;	
	/*********************************
	*** AGREGAR UN NUEVO MENSAJES
	************************************/
	
	case ('newMsg'):
		session_start();
		$sql='INSERT INTO `customerservice_msg`
			(`remitiente_msg`,
			`date_msg`,
			`texto_msg`,
			`id_ticket`) VALUES 
			('.$_COOKIE['datos_usr']['USERID'].', NOW(), "'.mysql_real_escape_string($_POST['Msg']).'", '.$_POST['id'].')
		';
		mysql_query($sql);
		if(mysql_affected_rows() > 0)
		{
			$aux=mysql_query('select u.email, concat( u.name ," " ,u.surname) cliente, concat( cu.name ," " ,cu.surname) customer from
xima.customerservices c left join xima.ximausrs u on c.useridclient=u.userid LEFT JOIN xima.ximausrs cu ON cu.userid='.$_COOKIE['datos_usr']['USERID'].' where c.idcs='.$_POST['id']);
			$data=mysql_fetch_assoc($aux);
			mysql_query('UPDATE `xima`.`customerservice_msg`
			SET
			`status_msg_system` = 2
			WHERE id_ticket=
			'.$_POST['id'].';');
			echo json_encode(array(
				'success'	=> true,
				'id' => $_POST['id'],
				'sql' => $sql,
				'mysql Error' => mysql_error()
			));
		}
		else
		{
			echo json_encode(array(
				'success'	=> false,
				'id' => $_POST['id'],
				'sql' => $sql,
				'mysql Error' => mysql_error()
			));
		}
		
	break;
}
?>