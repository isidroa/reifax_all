<?php
	function camptit($campo,$tabla,$tipo='Titulos'){
		global $Camptit_array; 
		foreach($Camptit_array as $k => $val){
			if(strtolower($val[0])==strtolower($tabla) && strtolower($val[1])==strtolower($campo)){
				if($tipo=='Titulos')
					return $val[2];
				else if($tipo=='titulos_a')
					return $val[4];
				else
					return $val[3];
			}
		}
	}
	
	function overviewDetails($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude,$realtor,$loged,$block){
		global $report;
		global $print;
		conectarPorBD($db_data);
		?>
        <?php
		if(!$report)
		{
        echo '<div class="history">
        	Property(ies) and sales information from public records
        </div>';
		}
		?>
        <div class="titlePrincipal">
        	<span>
            	Public Records
            </span>
        </div>
		<?php
		$sql_comparado="Select 
		p.address,p.unit,p.ozip,p.city,p.sdname,p.folio,p.ccode,p.stories,
		p.lsqft,p.ac,p.yrbuilt,p.bheated,p.beds,p.pool,p.units,p.bath,p.waterf,p.buildingv,p.sfp,p.homstead,
		p.landv,p.taxablev,p.saledate,p.saleprice,p.owner,p.owner_a,p.owner_s,p.owner_c,p.owner_p,p.owner_z,
		p.phonename,p.phonenumber1,p.phonenumber2,p.rng,p.sec,p.twn,p.legal,p.ccoded,p.lunit_type,p.lunits,p.tsqft,
		m.closingdt,m.mlnumber
		FROM psummary p LEFT JOIN mlsresidential m ON (p.parcelid=m.parcelid)
		Where p.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_assoc($res);
		$escpecialClass=array('legal' => array ('clsPrincipal' =>'bigFila','clsTitle'=>'','clsDescribe'=>'bigDescribe', 'doulbeSpace' => true), 
			'sdname' => array ('clsPrincipal' =>'bigFila','clsTitle'=>'','clsDescribe'=>'bigDescribe', 'doulbeSpace' => true) );
		$visibles=array('address','unit','city','ozip','sdname','folio','tsqft','beds','pool','bath','waterf','lsqft','bheated','units','ccoded','buildingv','sfp','landv','taxablev','legal',	'saledate',	'saleprice');
		$myrow['buildingv']='$'.number_format($myrow['buildingv'],2,'.',',');
		$myrow['landv']='$'.number_format($myrow['landv'],2,'.',',');
		$myrow['taxablev']='$'.number_format($myrow['taxablev'],2,'.',',');
		$myrow['saleprice']='$'.number_format($myrow['saleprice'],2,'.',',');
		$myrow['sfp']='$'.number_format($myrow['sfp'],2,'.',',');
		$myrow['bheated']=number_format($myrow['bheated'],0,'.',',');
		$myrow['lsqft']=number_format($myrow['lsqft'],0,'.',',');
		$myrow['tsqft']=number_format($myrow['tsqft'],0,'.',',');
		$myrow['saledate']=formantDateNumber( $myrow['saledate']);
		
		$conta=1;
		echo '<ul>';
        foreach($visibles as $key => $valor) 
		{
			echo '<li>
					<div class="cuote"></div>
					'.camptit($valor,'psummary','titulos_a').':
					'.$myrow[$valor].'
				</li>
			';
			$conta+=($escpecialClass[$valor]['doulbeSpace'])? 1 : 0.5;
		}
		echo '</ul>';
		?>
        <div class="clear"></div>
        <div class="clear"></div>
        <div class="history">
        	
        </div>
        <div class="titlePrincipal">
        	<span>
            	Sales History
            </span>
        </div>
        <div class="clear">
        </div>
        	<table class="sales">
            	<tr>
            		<th>
                    	<label>
					 		Sales
						</label>
                    </th>
                    <th>
					 	<label>
					 		<?php echo camptit("date","sales",'desc')?>
						</label>
                    </th>
                    <th>
					 	<label>
					 		<?php echo camptit("price","sales",'desc') ?>
						</label>
                    </th>
                    <th>
					 	<label>
					 		<?php echo camptit("book","sales",'desc') ?>
						</label>
                    </th>
                    <th>
					 	<label>
					 		<?php echo camptit("page","sales",'desc')?>
						</label>
                    </th>
                </tr>
        <?php
                $sql_comparado="SELECT  
                parcelid,date,type,price,book,page,grantor
                FROM sales WHERE parcelid='$pid' 
                ORDER BY date DESC;";
            ////////////////////////Sales////////////////////////////////
                $res = mysql_query($sql_comparado) or die(mysql_error());
				$i=1;
				 while($myrow2= mysql_fetch_array($res)){
					 echo
					 (($i%2==0)?'<tr class="salesDetails par">':'<tr class="salesDetails">').'
					 	<td>
							'.$i.'
						</td>
						<td>
							<label>
								'.formantDateNumber( $myrow2['date']).'
							</label>
						</td>
						<td>
							<label>
								$'.number_format($myrow2['price'],2,'.',',').'
							</label>
						</td>
						<td>
							<label>
								'.number_format($myrow2['book'],0,'.',',').'
							</label>
						</td>
						<td>
							<label>
								'.number_format($myrow2['page'],0,'.',',').'
							</label>
						</td>
					</tr>';
					 $i++;
					 
				 }
		?>
     </table>
    <div class="clear"></div>
    <?php if($report){?>
        <div class="titlePrincipal">
        	<span>
            	Owner Information
            </span>
        </div>
        
        <?php
		$conta=1;
		$escpecialClass=array('owner' => array ('clsPrincipal' =>'bigFila','clsTitle'=>'','clsDescribe'=>'bigDescribe', 'doulbeSpace' => true) ,'owner_p' => array ('clsPrincipal' =>'bigFila','clsTitle'=>'','clsDescribe'=>'bigDescribe', 'doulbeSpace' => true) );
		$visibles=array('owner','owner_a','owner_c','owner_z','owner_s','owner_p','phonenumber1','phonenumber2');
		foreach($visibles as $key => $valor) 
		{
			echo ($conta%2==0)?'<div class="fila par '.$escpecialClass[$valor]['clsPrincipal'].'">':'<div class="fila '.$escpecialClass[$valor]['clsPrincipal'].'">';
			echo '
				<div class="title '.$escpecialClass[$valor]['clsTitle'].'">
					<div class="cuote"></div>
					'.camptit($valor,'psummary','titulos_a').':
				</div>
				<div class="describe '.$escpecialClass[$valor]['clsDescribe'].'">
					'.$myrow[$valor].'
				</div>
			</div>
			';
			$conta+=($escpecialClass[$valor]['doulbeSpace'])? 1 : 0.5;
		}
	} ?>
    <div class="clear"></div>
    
    
    
<?php
	}
	////////////////////////Comparable////////////////////////////////
	function overviewComp($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$loged,$block,$array_taken,$orderField,$orderDir,$investorLimit){
		conectarPorBD($db_data);
		global $report;	
		
		$_mainTbl	= "psummary";
		$_zip		= "$_mainTbl.ozip,";
		$_larea		= "$_mainTbl.bheated,";
		$_unit		= "$_mainTbl.unit,";
		$pricesSel	= "$_mainTbl.saleprice";
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,marketvalue.pendes,marketvalue.marketvalue,
		marketvalue.marketmedia,marketvalue.marketpctg,marketvalue.offertvalue,marketvalue.offertmedia,marketvalue.offertpctg, latlong.latitude,latlong.longitude
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (marketvalue.parcelid  = $_mainTbl.parcelid and latlong.parcelid = $_mainTbl.parcelid ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		$dt_comparado="";
		$myrow["xlat"]=$myrow["latitude"];	$myrow["xlong"]=$myrow["longitude"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		if($loged && !$block)
			$ArIDCT = getArray('Comp','comparables',true);
		else
			$ArIDCT = array(45,47,46,55,53);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		if($loged && !$block)
			$ArIDCT = getArray('Comp','comparables',false);
		else
			$ArIDCT = array(45,47,46,55,53);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
		
		if(!$report)
		{
        echo '<div class="history">
        	Property(ies) comparables information from public records
        </div>';
		}
		else
		{
			/*
			bd: "<?php echo $db_data;?>",prop: "<?php echo $xcode;?>",status:"A",reset:"true", id: "<?php echo $pid;?>", type: "nobpo",sort: orderCompField, dir: orderCompDir, array_taken: "<?php echo $array_taken;?>",report:<?php echo $loged;?>
			*/
			
			$url = 'http://www.reifax.com/properties_look4comparables.php';
			$body = 'bd='.$db_data.'&prop='.$xcode.'&status=CS,CC&reset=true&id='.$pid.'&type=nobpo&sort='.$orderField.'&dir='.$orderDir.'&array_taken=&report=true';
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $body);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			$page = curl_exec($c);
			curl_close($c);
			echo "<script>
					var myDataStoreCom=$page;
				</script>";			
		}
		?>
        <div class="titlePrincipal">
        	<span>
            	Comparables
            </span>
        </div>
		<div align="center" id="todo_comp_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
        	<?php
            	echo '<div id="'.$map.'" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>';
			?>
        <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comp_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>        <script>
		var limitComp= 50;
		var orderCompField = '<?php echo $orderField;?>';
		var orderCompDir = '<?php echo $orderDir;?>';
		
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin','<?php echo $map; ?>_circle');
		<?php if(!$print && $loged && !$block){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>,'normal',650);
			<?php echo $map; ?>.filterONOFF('COMP-MAP');
			<?php echo $map; ?>.curBoton="AVG";
			//<?php echo $map; ?>.ins_toolbar("320px","overview");
			
			<?php echo $map; ?>.addPushpinInfobox(
				'',
				<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				'<?php echo (float)$data_comparado[0]->lsqft;?>',
				'<?php echo (float)$data_comparado[0]->bheated;?>',
				'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
				'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
				'Subject',
				'',
				''
			);

			<?php echo $map; ?>.getCenterPins();
		<?php }else{?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>);
		<?php }?>
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
				return value;
		}
		
		<?php 
			/*
			if($print && $array_taken!=""){
				$_POST["comparables"]=$array_taken;
				$_POST["pid"]=$pid;
				$_POST["status"]='CC';
				$_POST['no_func']=true;
				ob_start();
				include('/properties_calculate.php');
				$content = ob_get_contents();
				ob_end_clean();
				$datos=explode("^",$content);
				
				$data_comparado[0]->marketvalue=str_replace(',','',$datos[0]);
				$data_comparado[0]->marketmedia=str_replace(',','',$datos[1]);
				$data_comparado[0]->marketpctg=str_replace(',','',$datos[2]);
				?>
				<?php
			} */
		?>
		
		var arrayDataSujetoComp = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoComp = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoComp = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			//width: 600,
			
			anchor:'100%',
			height: 65,
			store: storeSujetoComp,
			columns: [
				<?php 
					echo "{header: 'Sta.' , width:30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			]
		});

		storeSujetoComp.loadData(arrayDataSujetoComp);
		if(Ext.isIE){
			gridSujetoComp.getEl().focus();
		}
		
		
		var storeComp = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				method: 'POST',
				prettyUrls: false,
				url: '/properties_look4comparables.php' // see options parameter for Ext.Ajax.request
			}),
			reader: new Ext.data.JsonReader(),
			baseParams: {bd: "<?php echo $db_data;?>",prop: "<?php echo $xcode;?>",status:"CS,CC",reset:"true", id: "<?php echo $pid;?>", type: "nobpo",sort: orderCompField, dir: orderCompDir, array_taken: "<?php echo $array_taken;?>"<?php echo ($report)?',report :'.$report:'' ?>},
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.borrarTodoMap();
					
					<?php echo $map; ?>.addPushpinInfoboxMini(
						'', 
						<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
						'<?php echo $data_comparado[0]->address;?>',
						'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
						<?php echo ((integer)$data_comparado[0]->beds).','.((integer)$data_comparado[0]->bath);?>,
						'<?php echo (float)$data_comparado[0]->lsqft;?>',
						'Subject',
						null,
						lsHexCssPoint['SUBJECT']
					);
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							var statusText={
										'S':'Subject',
										'A-F':'Active Foreclosed',
										'A-F-S':'Active Foreclosed Sold',
										'A-P':'Active Pre-Foreclosed',
										'A-P-S':'Active Pre-Foreclosed Sold',
										'A-N':'Active',
										'CC-F':'By Owner Foreclosed',
										'CC-F-S':'By Owner Foreclosed Sold',
										'CC-P':'By Owner Pre-Foreclosed',
										'CC-P-S':'By Owner Pre-Foreclosed Sold',
										'CC-N':'By Owner',
										'CS-F':'Closed Sale Foreclosed',
										'CS-F-S':'Closed Sale Foreclosed Sold',								
										'CS-P':'Closed Sale Pre-Foreclosed',
										'CS-P-S':'Closed Sale Pre-Foreclosed Sold',
										'CS-N':'Closed Sale',
										'N-F':'Non-Active Foreclosed',
										'N-F-S':'Non-Active Foreclosed Sold',
										'N-P':'Non-Active Pre-Foreclosed',
										'N-P-S':'Non-Active Pre-Foreclosed Sold',
										'N-N':'Non-Active'
							}
							var cod=getPointColor(status,pendes,sold);
							var pointColor=lsHexCssPoint[cod];
							
							<?php echo $map; ?>.addPushpinInfoboxMini(
								ind, 
								data[k].get('pin_xlat'),
								data[k].get('pin_xlong'),
								'('+(1+parseInt(k))+') '+data[k].get('pin_address'),
								data[k].get('pin_saleprice'),
								data[k].get('pin_bed'),
								data[k].get('pin_bath'),
								data[k].get('pin_lsqft'),
								statusText[cod],
								null,
								pointColor
							);						
						}					
					}
					<?php echo $map; ?>.getCenterPins();
					<?php
					if(!$print){
						?>
					if ( Ext.fly(Ext.fly(gridComp.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckComp=true;
						gridComp.getSelectionModel().selectAll();
						selected_dataComp=new Array();
					}else{
						AllCheckComp=false;
						var sel = [];
						if(selected_dataComp.length > 0){
							for(val in selected_dataComp){
								var ind = gridComp.getStore().find('pid',selected_dataComp[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridComp.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridComp.setHeight(alto);
					<?php }?>
					/*if(document.getElementById('overviewDetailInit'))
						document.getElementById('overviewDetailInit').click();*/
				
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataComp = new Array();
		var AllCheckComp=false;
		
		var smComp = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataComp.indexOf(record.get('pid'))==-1)
						selected_dataComp.push(record.get('pid'));
					
					if(Ext.fly(gridComp.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckComp=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataComp = selected_dataComp.remove(record.get('pid'));
					AllCheckComp=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridComp = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			anchor:'100%',
			store: storeComp,
			columns: [],
			sm: smComp,
			listeners: {
				"mouseover":function (e){
					var row;
					if((row = this.getView().findRowIndex(e.getTarget())) !== false){
						var record = this.store.getAt(row);
						var ind = record.get('status').split('_')[0];
						var pin = <?php echo $map; ?>.getPushpin(ind);
						if(pin !== false) <?php echo $map; ?>.pinMouseOver(pin);
					}
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderCompField = sortInfo.field;
					orderCompDir = sortInfo.direction;
				}
			}
			<?php
			if(!$report)
			{?>
			,
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitComp,
				store: storeComp,
				width:460,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitComp=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitComp=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitComp=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitComp;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				})
				]
			})
			<?php
			}
			?>
		});
		<?php }?>
		
		storeComp.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeComp.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					//columns.push(smComp);
					<?php }?>
					Ext.each(storeComp.reader.jsonData.columns, function(column){
						columns.push(column);
					});
					gridComp.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeComp.reader.jsonData.columns;
				var data = storeComp.reader.jsonData.records;
				
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
		if(typeof(myDataStoreCom)!='undefined')
		{
		
		/**********************
		****** carga del store
		*********************/		
			<?php
				$comparado=array(
				'parcelid' => $data_comparado[0]->parcelid,
				'latitude' => $data_comparado[0]->latitude,
				'longitude' => $data_comparado[0]->longitude,
				'address' => $data_comparado[0]->address,
				'lsqft' => (float)$data_comparado[0]->lsqft,
				'bheated' => (float)$data_comparado[0]->bheated,
				'beds' => ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath),
				'price' => '$'.number_format($data_comparado[0]->saleprice,0,'.',','),
				'status' => 'Subject'
				)
			 ?>
			loadDataGrip(myDataStoreCom,'<?php echo $grid_render2; ?>','<?php echo $pin; ?>',<?php echo $map ?>,'<?php echo $db_data ?>',<?php echo json_encode($comparado) ?>);
		}
		else
		{
		/**********************
		****** carga del store
		*********************/
			storeComp.load();
		}
	
	</script>
    <?
}
	
	
	
	
	////////////////////////Comparable Active////////////////////////////////
	function overviewCompActive($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$loged,$block,$array_taken,$orderField,$orderDir){
		conectarPorBD($db_data);
		global $report;	
		
		$_mainTbl	= "psummary";
		$_zip		= "$_mainTbl.ozip,";
		$_larea		= "$_mainTbl.bheated,";
		$_unit		= "$_mainTbl.unit,";
		$pricesSel	= "$_mainTbl.saleprice";
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,marketvalue.pendes,marketvalue.marketvalue,
		marketvalue.marketmedia,marketvalue.marketpctg,marketvalue.offertvalue,marketvalue.offertmedia,marketvalue.offertpctg, latlong.latitude,latlong.longitude
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (marketvalue.parcelid  = $_mainTbl.parcelid and latlong.parcelid = $_mainTbl.parcelid ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		$dt_comparado="";
		$myrow["xlat"]=$myrow["latitude"];	$myrow["xlong"]=$myrow["longitude"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = array(45,47,46,55,53);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		if($loged && !$block)
			$ArIDCT = getArray('active','comparables',false);
		else
			$ArIDCT = array(45,47,46,9,55,53);
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
		
		if(!$report)
		{
        echo '<div class="history">
        	Property(ies) and sales information from public records
        </div>';
		}
		else
		{
			/*
			bd: "<?php echo $db_data;?>",prop: "<?php echo $xcode;?>",status:"A",reset:"true", id: "<?php echo $pid;?>", type: "nobpo",sort: orderActiveField, dir: orderActiveDir, array_taken: "<?php echo $array_taken;?>",report:<?php echo $loged;?>
			*/
			
			$url = 'http://reifax.com/properties_look4comparables.php';
			$body = 'bd='.$db_data.'&prop='.$xcode.'&status=A&reset=true&id='.$pid.'&type=nobpo&sort='.$orderField.'&dir='.$orderDir.'&array_taken=&report=true';
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $body);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			$page = curl_exec($c);
			curl_close($c);
			$respuesta=json_decode($page);
			echo "<script>
					var myDataStoreActive=$page;
				</script>";
			//print_r($respuesta);
			
		}
		?>
        <div class="titlePrincipal">
        	<span>
            	Comparables Active
            </span>
        </div>
		<div align="center" id="todo_comp_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
        	<?php
            	echo '<div id="'.$map.'" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>';
			?>
        <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comp_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>        <script>
		var limitActive= 50;
		var orderActiveField = '<?php echo $orderField;?>';
		var orderActiveDir = '<?php echo $orderDir;?>';
		
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print && $loged && !$block){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('COMP-MAP');
			<?php echo $map; ?>.curBoton="AVG";
			//<?php echo $map; ?>.ins_toolbar("320px","overview");
			
			
			<?php echo $map; ?>.addPushpinInfobox(
				'',
				<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				'<?php echo (float)$data_comparado[0]->lsqft;?>',
				'<?php echo (float)$data_comparado[0]->bheated;?>',
				'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
				'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
				'Subject',
				'',
				''
			);

			<?php echo $map; ?>.getCenterPins();
		<?php }else{?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>);
		<?php }?>
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
				return value;
		}
		
		var arrayDataSujetoActive = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoActive = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoActive = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			//width: 600,
			
			anchor:'100%',
			height: 65,
			store: storeSujetoActive,
			columns: [
				<?php 
					echo "{header: 'Sta.' , width:30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			]
		});

		storeSujetoActive.loadData(arrayDataSujetoActive);
		if(Ext.isIE){
			gridSujetoActive.getEl().focus();
		}
		
		
		var storeActive = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				method: 'POST',
				prettyUrls: false,
				url: '/properties_look4comparables.php' // see options parameter for Ext.Ajax.request
			}),
			reader: new Ext.data.JsonReader(),
			baseParams: {bd: "<?php echo $db_data;?>",prop: "<?php echo $xcode;?>",status:"A",reset:"true", id: "<?php echo $pid;?>", type: "nobpo",sort: orderActiveField, dir: orderActiveDir, array_taken: "<?php echo $array_taken;?>",report:<?php echo $loged ? 'true' : 'false';?>},
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.borrarTodoMap();
					
					<?php echo $map; ?>.addPushpinInfobox(
						'',
						<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						'<?php echo (float)$data_comparado[0]->lsqft;?>',
						'<?php echo (float)$data_comparado[0]->bheated;?>',
						'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
						'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
						'Subject',
						'',
						''
					);
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							var statusText={
										'S':'Subject',
										'A-F':'Active Foreclosed',
										'A-F-S':'Active Foreclosed Sold',
										'A-P':'Active Pre-Foreclosed',
										'A-P-S':'Active Pre-Foreclosed Sold',
										'A-N':'Active',
										'CC-F':'By Owner Foreclosed',
										'CC-F-S':'By Owner Foreclosed Sold',
										'CC-P':'By Owner Pre-Foreclosed',
										'CC-P-S':'By Owner Pre-Foreclosed Sold',
										'CC-N':'By Owner',
										'CS-F':'Closed Sale Foreclosed',
										'CS-F-S':'Closed Sale Foreclosed Sold',								
										'CS-P':'Closed Sale Pre-Foreclosed',
										'CS-P-S':'Closed Sale Pre-Foreclosed Sold',
										'CS-N':'Closed Sale',
										'N-F':'Non-Active Foreclosed',
										'N-F-S':'Non-Active Foreclosed Sold',
										'N-P':'Non-Active Pre-Foreclosed',
										'N-P-S':'Non-Active Pre-Foreclosed Sold',
										'N-N':'Non-Active'
							}
							var cod=getPointColor(status,pendes,sold);
							var pointColor=lsHexCssPoint[cod];
							
							<?php echo $map; ?>.addPushpinInfoboxMini(
								ind, 
								data[k].get('pin_xlat'),
								data[k].get('pin_xlong'),
								'('+(1+parseInt(k))+') '+data[k].get('pin_address'),
								data[k].get('pin_saleprice'),
								data[k].get('pin_bed'),
								data[k].get('pin_bath'),
								data[k].get('pin_lsqft'),
								statusText[cod],
								null,
								pointColor
							);						
						}					
					}
					<?php echo $map; ?>.getCenterPins();
					<?php
					if(!$print){
						?>
					if ( Ext.fly(Ext.fly(gridActive.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckComp=true;
						gridActive.getSelectionModel().selectAll();
						selected_dataComp=new Array();
					}else{
						AllCheckComp=false;
						var sel = [];
						if(selected_dataComp.length > 0){
							for(val in selected_dataComp){
								var ind = gridActive.getStore().find('pid',selected_dataComp[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridActive.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridActive.setHeight(alto);
					<?php }?>
					/*if(document.getElementById('overviewDetailInit'))
						document.getElementById('overviewDetailInit').click();*/
				
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataActive = new Array();
		var AllCheckActive=false;
		
		var smActive = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataActive.indexOf(record.get('pid'))==-1)
						selected_dataActive.push(record.get('pid'));
					
					if(Ext.fly(gridActive.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckActive=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataActive = selected_dataActive.remove(record.get('pid'));
					AllCheckActive=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridActive = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			anchor:'100%',
			store: storeActive,
			columns: [],
			sm: smActive,
			listeners: {
				"mouseover":function (e){
					var row;
					if((row = this.getView().findRowIndex(e.getTarget())) !== false){
						var record = this.store.getAt(row);
						var ind = record.get('status').split('_')[0];
						var pin = <?php echo $map; ?>.getPushpin(ind);
						if(pin !== false) <?php echo $map; ?>.pinMouseOver(pin);
					}
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderActiveField = sortInfo.field;
					orderActiveDir = sortInfo.direction;
				}
			}
			<?php
			if(!$report)
			{?>
			,
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitActive,
				store: storeActive,
				width:460,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitActive=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitActive=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitActive=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitActive;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				})
				]
			})
			<?php
			}
			?>
		});
		<?php }?>
		
		storeActive.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeActive.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					//columns.push(smComp);
					<?php }?>
					var Permi=new Array('Sta','Dis.','Address','Unit','GArea','Be','Ba','Lprice','Ldate','Dom')
					Ext.each(storeActive.reader.jsonData.columns, function(column){
						if(Permi.indexOf(column.header)!=-1)
							columns.push(column);
					});
					gridActive.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeActive.reader.jsonData.columns;
				var data = storeActive.reader.jsonData.records;
				
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
		if(typeof(myDataStoreActive)!='undefined')
		{
		
		/**********************
		****** carga del store
		*********************/
			<?php
				$comparado=array(
				'parcelid' => $data_comparado[0]->parcelid,
				'latitude' => $data_comparado[0]->latitude,
				'longitude' => $data_comparado[0]->longitude,
				'address' => $data_comparado[0]->address,
				'lsqft' => (float)$data_comparado[0]->lsqft,
				'bheated' => (float)$data_comparado[0]->bheated,
				'beds' => ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath),
				'price' => '$'.number_format($data_comparado[0]->saleprice,0,'.',','),
				'status' => 'Subject'
				)
			 ?>
			loadDataGrip(myDataStoreActive,'<?php echo $grid_render2; ?>','<?php echo $pin; ?>',<?php echo $map ?>,'<?php echo $db_data ?>',<?php echo json_encode($comparado) ?>);
		}
		else
		{
		/**********************
		****** carga del store
		*********************/
			storeActive.load();
		}
	
	</script>
    <?
		
	}
	////////////////////////Comparable Distress////////////////////////////////
	function overviewCompDistress($pid,$db_data,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir){
		conectarPorBD($db_data);
		global $report;
		
		
		
		$_mainTbl	= "psummary";
		$_zip		= "$_mainTbl.ozip,";
		$_larea		= "$_mainTbl.bheated,";
		$_unit		= "$_mainTbl.unit,";
		$pricesSel	= "$_mainTbl.saleprice";
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,marketvalue.pendes,marketvalue.marketvalue,
		marketvalue.marketmedia,marketvalue.marketpctg,marketvalue.offertvalue,marketvalue.offertmedia,marketvalue.offertpctg, latlong.latitude,latlong.longitude
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (marketvalue.parcelid  = $_mainTbl.parcelid and latlong.parcelid = $_mainTbl.parcelid ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		$dt_comparado="";
		$myrow["xlat"]=$myrow["latitude"];	$myrow["xlong"]=$myrow["longitude"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		
		$ArIDCT = getArray('distress','comparables',true);
		$ArIDCT = array(45,47,46,55,53);
		
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		
		$ArIDCT = getArray('distress','comparables',false);
	
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
		
		if(!$report)
		{
        echo '<div class="history">
        	Property(ies) and sales information from public records
        </div>';
		}
		else
		{
			/*
			{no_func: true, bd: '<?php echo $db_data;?>', id: '<?php echo $pid;?>' , report:true, array_taken: "<?php echo $array_taken;?>",sort: orderDistressField, dir: orderDistressDir}
			*/
			$url = 'http://reifax.com/properties_1mile_distress.php';
			$body = 'bd='.$db_data.'&no_func=true&id='.$pid.'&sort='.$orderField.'&dir='.$orderDir.'&array_taken=&report=true';
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $body);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			$page = curl_exec($c);
			curl_close($c);
			$respuesta=json_decode($page);
			echo "<script>
					var myDataStoreDistress=$page;
				</script>";			
		}
?>
        <div class="titlePrincipal">
        	<span>
            	Distress Properties
            </span>
        </div>
		<div align="center" id="todo_comp_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
        	<?php
            	echo '<div id="'.$map.'" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>';
			?>
        <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comp_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>        <script>
		var limitDistress= 50;
		var orderDistressField = '<?php echo $orderField;?>';
		var orderDistressDir = '<?php echo $orderDir;?>';
		
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print && $loged && !$block){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('COMP-MAP');
			<?php echo $map; ?>.curBoton="AVG";
			//<?php echo $map; ?>.ins_toolbar("320px","overview");		
			
			<?php echo $map; ?>.addPushpinInfobox(
				'',
				<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				'<?php echo (float)$data_comparado[0]->lsqft;?>',
				'<?php echo (float)$data_comparado[0]->bheated;?>',
				'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
				'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
				'Subject',
				'',
				''
			);

			<?php echo $map; ?>.getCenterPins();
		<?php }else{?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>);
		<?php }?>
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
				return value;
		}
		
		<?php 
			if($print && $array_taken!=""){
				$_POST["comparables"]=$array_taken;
				$_POST["pid"]=$pid;
				$_POST["status"]='CC';
				$_POST['no_func']=true;
				ob_start();
				include('/properties_calculate.php');
				$content = ob_get_contents();
				ob_end_clean();
				$datos=explode("^",$content);
				
				$data_comparado[0]->marketvalue=str_replace(',','',$datos[0]);
				$data_comparado[0]->marketmedia=str_replace(',','',$datos[1]);
				$data_comparado[0]->marketpctg=str_replace(',','',$datos[2]);
				?>
				<?php
			}
		?>
		
		var arrayDataSujetoDistress = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoDistress = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoDistress = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			//width: 600,
			
			anchor:'100%',
			height: 65,
			store: storeSujetoDistress,
			columns: [
				<?php 
					echo "{header: 'Sta.' , width:30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			],
			listeners: {
				"mouseout": function(e) {
					//VEPushpin.Hide();
				}
			}
		});

		storeSujetoDistress.loadData(arrayDataSujetoDistress);
		if(Ext.isIE){
			gridSujetoDistress.getEl().focus();
		}
		var storeDistress = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				method: 'POST',
				prettyUrls: false,
				url: '/properties_1mile_distress.php' // see options parameter for Ext.Ajax.request
			}),
			baseParams: {no_func: true, bd: '<?php echo $db_data;?>', id: '<?php echo $pid;?>' , report:true, array_taken: "<?php echo $array_taken;?>",sort: orderDistressField, dir: orderDistressDir},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.borrarTodoMap();
					
					<?php echo $map; ?>.addPushpinInfobox(
						'',
						<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						'<?php echo (float)$data_comparado[0]->lsqft;?>',
						'<?php echo (float)$data_comparado[0]->bheated;?>',
						'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
						'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
						'Subject',
						'',
						''
					);
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							getCasita(status,pendes,sold);
							
							var statusText={
										'S':'Subject',
										'A-F':'Active Foreclosed',
										'A-F-S':'Active Foreclosed Sold',
										'A-P':'Active Pre-Foreclosed',
										'A-P-S':'Active Pre-Foreclosed Sold',
										'A-N':'Active',
										'CC-F':'By Owner Foreclosed',
										'CC-F-S':'By Owner Foreclosed Sold',
										'CC-P':'By Owner Pre-Foreclosed',
										'CC-P-S':'By Owner Pre-Foreclosed Sold',
										'CC-N':'By Owner',
										'CS-F':'Closed Sale Foreclosed',
										'CS-F-S':'Closed Sale Foreclosed Sold',								
										'CS-P':'Closed Sale Pre-Foreclosed',
										'CS-P-S':'Closed Sale Pre-Foreclosed Sold',
										'CS-N':'Closed Sale',
										'N-F':'Non-Active Foreclosed',
										'N-F-S':'Non-Active Foreclosed Sold',
										'N-P':'Non-Active Pre-Foreclosed',
										'N-P-S':'Non-Active Pre-Foreclosed Sold',
										'N-N':'Non-Active'
							}
							var cod=getPointColor(status,pendes,sold);
							var pointColor=lsHexCssPoint[cod];
							
							<?php echo $map; ?>.addPushpinInfoboxMini(
								ind, 
								data[k].get('pin_xlat'),
								data[k].get('pin_xlong'),
								'('+(1+parseInt(k))+') '+(data[k].get('pin_address').replace(/\d*/,'XXXX')),
								data[k].get('pin_saleprice'),
								data[k].get('pin_bed'),
								data[k].get('pin_bath'),
								data[k].get('pin_lsqft'),
								statusText[cod],
								null,
								pointColor
							);						
						}					
					}
					<?php echo $map; ?>.getCenterPins();
					<?php
					if(!$print){
						?>
					if ( Ext.fly(Ext.fly(gridDistress.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckComp=true;
						gridDistress.getSelectionModel().selectAll();
						selected_dataComp=new Array();
					}else{
						AllCheckComp=false;
						var sel = [];
						if(selected_dataComp.length > 0){
							for(val in selected_dataComp){
								var ind = gridDistress.getStore().find('pid',selected_dataComp[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridDistress.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridDistress.setHeight(alto);
					<?php }?>
					/*if(document.getElementById('overviewDetailInit'))
						document.getElementById('overviewDetailInit').click();*/
				
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataDistress = new Array();
		var AllCheckDistress=false;
		
		var smDistress = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataDistress.indexOf(record.get('pid'))==-1)
						selected_dataDistress.push(record.get('pid'));
					
					if(Ext.fly(gridDistress.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckDistress=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataDistress = selected_dataDistress.remove(record.get('pid'));
					AllCheckDistress=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridDistress = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			anchor:'100%',
			store: storeDistress,
			columns: [],
			sm: smDistress,
			listeners: {
				"mouseover":function (e){
					var row;
					if((row = this.getView().findRowIndex(e.getTarget())) !== false){
						var record = this.store.getAt(row);
						var ind = record.get('status').split('_')[0];
						var pin = <?php echo $map; ?>.getPushpin(ind);
						if(pin !== false) <?php echo $map; ?>.pinMouseOver(pin);
					}
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderDistressField = sortInfo.field;
					orderDistressDir = sortInfo.direction;
				}
			}
			<?php
			if(!$report)
			{?>
			,
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitDistress,
				store: storeDistress,
				width:460,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitDistress=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitDistress=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitDistress=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitDistress;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				})
				]
			})
			<?php
			}
			?>
		});
		<?php }?>
		
		storeDistress.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeDistress.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					//columns.push(smComp);
					<?php }?>
					var Permi=new Array('Sta','Dis.','Address','Unit','GArea','Frclosure','File Date','Judge Date','Judge_Amt','Dom','Known Debt')
					Ext.each(storeDistress.reader.jsonData.columns, function(column){
						if(column.header=='Address'){
							column.renderer=function (val){
								return val=val.replace(/\d*/,'XXXX');
							}
						}
						if(Permi.indexOf(column.header)!=-1)
							columns.push(column);
					});
					gridDistress.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeDistress.reader.jsonData.columns;
				var data = storeDistress.reader.jsonData.records;
				
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
			<?php }?>
		});
				
		if(typeof(myDataStoreDistress)!='undefined')
		{
		
		/**********************
		****** carga del store
		*********************/
			<?php
				$comparado=array(
				'parcelid' => $data_comparado[0]->parcelid,
				'latitude' => $data_comparado[0]->latitude,
				'longitude' => $data_comparado[0]->longitude,
				'address' => $data_comparado[0]->address,
				'lsqft' => (float)$data_comparado[0]->lsqft,
				'bheated' => (float)$data_comparado[0]->bheated,
				'beds' => ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath),
				'price' => '$'.number_format($data_comparado[0]->saleprice,0,'.',','),
				'status' => 'Subject'
				)
			 ?>
			loadDataGrip(myDataStoreDistress,'<?php echo $grid_render2; ?>','<?php echo $pin; ?>',<?php echo $map ?>,'<?php echo $db_data ?>',<?php echo json_encode($comparado) ?>);
		}
		else
		{
		/**********************
		****** carga del store
		*********************/
			storeDistress.load();
		}
	
	
	</script>
    <?
		
		
		
		
		
		
		
		
		
	}
	
////////////////////////Rental Comparables ////////////////////////////////
	function overviewCompRental($pid,$db_data,$xcode,$map,$grid_render,$grid_render2,$pagin_comp_tol,$print,$ocomp,$county,$pin,$imgPin,$array_taken,$orderField,$orderDir){
		conectarPorBD($db_data);
		global $report;
		
		
		
		$_mainTbl	= "psummary";
		$_zip		= "$_mainTbl.ozip,";
		$_larea		= "$_mainTbl.bheated,";
		$_unit		= "$_mainTbl.unit,";
		$pricesSel	= "$_mainTbl.saleprice";
		$sql_comparado="Select $_mainTbl.ParcelID,$_MLNUM $_mainTbl.address,$_mainTbl.lsqft,$_zip $_larea $_unit  $_mainTbl.beds,$_mainTbl.bath,$_mainTbl.pool,$_mainTbl.waterf,
		$_mainTbl.yrbuilt,$pricesSel,'Subject' as Status,marketvalue.pendes,marketvalue.marketvalue,
		marketvalue.marketmedia,marketvalue.marketpctg,marketvalue.offertvalue,marketvalue.offertmedia,marketvalue.offertpctg, latlong.latitude,latlong.longitude
		FROM $_mainTbl LEFT JOIN (marketvalue,latlong) 
		ON (marketvalue.parcelid  = $_mainTbl.parcelid and latlong.parcelid = $_mainTbl.parcelid ) Where $_mainTbl.parcelid='".$pid."';";
		
		$res = mysql_query($sql_comparado) or die($sql_comparado.mysql_error());
		$myrow= mysql_fetch_array($res, MYSQL_ASSOC);	
		$dt_comparado="";
		$myrow["xlat"]=$myrow["latitude"];	$myrow["xlong"]=$myrow["longitude"];
		foreach($myrow as $clave=>$valor)	
		{	if($dt_comparado!="") $dt_comparado.=",";	
			$dt_comparado.='"'.strtolower($clave).'":"'.$valor.'"';
		}	
	
		$data_comparado=json_decode("[{".$dt_comparado."}]");
		
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		$ArIDCT = getArray('rental','comparables',true);
		$ArIDCT = array(45,47,46,55,53);
		$colsCompar=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$colsCompar=str_replace(  "'",'"', $colsCompar);
		$colsCompar=json_decode($colsCompar);
	
		$ArSqlCT=array('idtc','campos','tabla','titulos','type','size','Desc','numformatted','decimals','align','px_size');//Search
		$ArDfsCT=array('idtc','name','tabla','title','type','size','desc','numformatted','decimal','align','px_size');//Search
		
		$ArIDCT = getArray('rental','comparables',false);
		$hdArray=getCamptit($ArSqlCT, $ArDfsCT, $ArIDCT);
		$hdArray=str_replace(  "'",'"', $hdArray);	
		$hdArray   = json_decode($hdArray);
		
		if(!$report)
		{
        echo '<div class="history">
        	Property(ies) and sales information from public records
        </div>';
		}
		else
		{
			/*
			no_func: true,bd: '<?php echo $db_data;?>',prop: '<?php echo $xcode;?>', id: '<?php echo $pid;?>', report:true, array_taken: "<?php echo $array_taken;?>",sort: orderRentalField, dir: orderRentalDir},
			*/
			$url = 'http://reifax.com/properties_look4rental.php';
			$body = 'bd='.$db_data.'&no_func=true&id='.$pid.'&prop='. $xcode.'&sort='.$orderField.'&dir='.$orderDir.'&array_taken=&report=true';
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $body);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			$page = curl_exec($c);
			curl_close($c);
			$respuesta=json_decode($page);
			echo "<script>
					var myDataStoreRental=$page;
				</script>";			
		}
?>
        <div class="titlePrincipal">
        	<span>
            	Rental Comparables 
            </span>
        </div>
		<div align="center" id="todo_comp_panel">
        <div align="center" style="width:100%; margin:auto; margin-top:10px;">
        	<?php
            	echo '<div id="'.$map.'" style="width:99%;height:350;border: 1px solid #4E9494;position:relative;float:left; margin-right:15px;"></div></div>';
			?>
        <input type="hidden" name="<?php echo $map; ?>_mapa_search_latlong" id="<?php echo $map; ?>_mapa_search_latlong" value="-1" />
        <br clear="all" />
        <div id="comp_data_div" align="center" style="margin:auto; width:100%;">
            <div id="<?php echo $grid_render; ?>" style="margin-bottom:5px;"></div>
            <div id="<?php echo $grid_render2; ?>"></div>
        </div>
        </div>        <script>
		var limitRental= 50;
		var orderRentalField = '<?php echo $orderField;?>';
		var orderRentalDir = '<?php echo $orderDir;?>';
		
		var	<?php echo $map; ?> = new XimaMap('<?php echo $map; ?>','<?php echo $map; ?>_mapa_search_latlong','<?php echo $map; ?>_control_mapa_div','<?php echo $map; ?>_pan','<?php echo $map; ?>_draw','<?php echo $map; ?>_poly','<?php echo $map; ?>_clear','<?php echo $map; ?>_maxmin');
		<?php if(!$print && $loged && !$block){?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->xlat.','.$data_comparado[0]->xlong;?>);
			<?php echo $map; ?>.filterONOFF('COMP-MAP');
			<?php echo $map; ?>.curBoton="AVG";
			//<?php echo $map; ?>.ins_toolbar("320px","overview");			
			
			<?php echo $map; ?>.addPushpinInfobox(
				'',
				<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
				'http://www.reifax.com/img/houses/verdetotal.png',
				'<?php echo $data_comparado[0]->address;?>',
				'<?php echo (float)$data_comparado[0]->lsqft;?>',
				'<?php echo (float)$data_comparado[0]->bheated;?>',
				'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
				'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
				'Subject',
				'',
				''
			);

			<?php echo $map; ?>.getCenterPins();
		<?php }else{?>
			<?php echo $map; ?>._IniMAP(<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>);
		<?php }?>
		
		function gridgetcasita(value, metaData, record, rowIndex){
			if(value=='SUBJECT'){
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[value].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>&nbsp;</div></div>";
			}else{
				var aux=value.split('_');
				return "<div style='position:relative;text-align:center;font:bold 14px;cursor:pointer;float:left;'><img style='position:absolute;top:-1px;left:-2px;z-index:100' src='http://www.reifax.com/img/houses/"+lsImgCss[aux[1]].img+"'/><div style='position:relative;top:2px;left:3px;z-index:200;text-decoration:none!important'>"+aux[0]+"</div></div>"
			} 
		}
		
		function gridgetsold(value, metaData, record, rowIndex){
			var sold = record.get('status').split('_')[1].split('-')[2];
			
			if(sold=='S') return value+' SOLD';
			return  value;
		}
		
		function showdiffvaluewin(lsqft,larea,beds,bath,zip){
			var html='';
			if(lsqft.length>0 && lsqft>0) html+='<tr><td>GArea:</td><td><font color=#1D5AFE>' + lsqft+ '</font></td></tr>';
			if(larea.length>0 && larea>0) html+='<tr><td>LArea:</td><td><font color=#1D5AFE>' + larea+ '</font></td></tr>';
			if(beds.length>0 && beds>0) html+='<tr><td>Be:</td><td><font color=#1D5AFE>' + beds+ '</font></td></tr>';
			if(bath.length>0 && bath>0) html+='<tr><td>Ba:</td><td><font color=#1D5AFE>' + bath+ '</font></td></tr>';
			if(zip.length>0 && zip>0) html+='<tr><td>Zip:</td><td><font color=#1D5AFE>' + zip+ '</font></td></tr>';
	
			return '<table>'+html+'</table>'; 
		}
		
		function griddifflarea(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddifflsqft(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbeds(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffbath(value, metaData, record, rowIndex){
				return value;
		}
		
		function griddiffzip(value, metaData, record, rowIndex){
				return value;
		}
		
		<?php 
			if($print && $array_taken!=""){
				$_POST["comparables"]=$array_taken;
				$_POST["pid"]=$pid;
				$_POST["status"]='CC';
				$_POST['no_func']=true;
				ob_start();
				include('/properties_calculate.php');
				$content = ob_get_contents();
				ob_end_clean();
				$datos=explode("^",$content);
				
				$data_comparado[0]->marketvalue=str_replace(',','',$datos[0]);
				$data_comparado[0]->marketmedia=str_replace(',','',$datos[1]);
				$data_comparado[0]->marketpctg=str_replace(',','',$datos[2]);
				?>
				<?php
			}
		?>
		
		var arrayDataSujetoRental = [
			<?php 
				foreach($data_comparado as $x => $val){
					echo "['SUBJECT'";
					foreach($colsCompar as $k => $v){
						eval("\$xdata=\$val->".$v->name.";");
						echo ",'$xdata'";
					}
					echo ']';
				}
			?>
		];
		
		var storeSujetoRental = new Ext.data.ArrayStore({
			fields: [
			   <?php 
					echo "'status'";
					foreach($colsCompar as $k=>$val){
						echo ",'".$val->name."'";
					}
			   ?>
			]
		});
		
		var gridSujetoRental = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render; ?>',
			cls: 'grid_comparado',
			//width: 600,
			
			anchor:'100%',
			height: 65,
			store: storeSujetoRental,
			columns: [
				<?php 
					echo "{header: 'Sta.' , width:30, sortable: true, renderer: gridgetcasita, tooltip: 'Status Property.', dataIndex: 'status'}";
					foreach($colsCompar as $k=>$val){
						if($val->type=='real')
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."', xtype: 'numbercolumn'}";
						else
							echo ",{header: '".$val->title."', width: ".$val->px_size.", sortable: true, tooltip: '".$val->desc."', dataIndex: '".$val->name."'}";
					}
			   ?>
			]
		});

		storeSujetoRental.loadData(arrayDataSujetoRental);
		if(Ext.isIE){
			gridSujetoRental.getEl().focus();
		}
		var storeRental = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				method: 'POST',
				prettyUrls: false,
				url: '/properties_look4rental.php'
			}),
			baseParams: {no_func: true,bd: '<?php echo $db_data;?>',prop: '<?php echo $xcode;?>', id: '<?php echo $pid;?>', report:true, array_taken: "<?php echo $array_taken;?>",sort: orderRentalField, dir: orderRentalDir},
			reader: new Ext.data.JsonReader(),
			remoteSort: true,
			listeners: {
				'load': function(store,data,obj){
					<?php echo $map; ?>.borrarTodoMap();
					
					<?php echo $map; ?>.addPushpinInfobox(
						'',
						<?php echo $data_comparado[0]->latitude.','.$data_comparado[0]->longitude;?>,
						'http://www.reifax.com/img/houses/verdetotal.png',
						'<?php echo $data_comparado[0]->address;?>',
						'<?php echo (float)$data_comparado[0]->lsqft;?>',
						'<?php echo (float)$data_comparado[0]->bheated;?>',
						'<?php echo ((integer)$data_comparado[0]->beds).' / '.((integer)$data_comparado[0]->bath);?>',
						'<?php echo '$'.number_format($data_comparado[0]->saleprice,0,'.',',');?>',
						'Subject',
						'',
						''
					);
					
					for(k in data){
						if(Ext.isNumber(parseInt(k))){
							
							var id = data[k].get('status').split('_');
							var ind = id[0];
							var status = id[1].split('-')[0];
							var pendes = id[1].split('-')[1];
							var sold = id[1].split('-')[2];
							
							
							var statusText={
										'S':'Subject',
										'A-F':'Active Foreclosed',
										'A-F-S':'Active Foreclosed Sold',
										'A-P':'Active Pre-Foreclosed',
										'A-P-S':'Active Pre-Foreclosed Sold',
										'A-N':'Active',
										'CC-F':'By Owner Foreclosed',
										'CC-F-S':'By Owner Foreclosed Sold',
										'CC-P':'By Owner Pre-Foreclosed',
										'CC-P-S':'By Owner Pre-Foreclosed Sold',
										'CC-N':'By Owner',
										'CS-F':'Closed Sale Foreclosed',
										'CS-F-S':'Closed Sale Foreclosed Sold',								
										'CS-P':'Closed Sale Pre-Foreclosed',
										'CS-P-S':'Closed Sale Pre-Foreclosed Sold',
										'CS-N':'Closed Sale',
										'N-F':'Non-Active Foreclosed',
										'N-F-S':'Non-Active Foreclosed Sold',
										'N-P':'Non-Active Pre-Foreclosed',
										'N-P-S':'Non-Active Pre-Foreclosed Sold',
										'N-N':'Non-Active'
							}
							var cod=getPointColor(status,pendes,sold);
							var pointColor=lsHexCssPoint[cod];
							
							<?php echo $map; ?>.addPushpinInfoboxMini(
								ind, 
								data[k].get('pin_xlat'),
								data[k].get('pin_xlong'),
								'('+(1+parseInt(k))+') '+data[k].get('pin_address'),
								data[k].get('pin_saleprice'),
								data[k].get('pin_bed'),
								data[k].get('pin_bath'),
								data[k].get('pin_lsqft'),
								statusText[cod],
								null,
								pointColor
							);						
						}					
					}
					<?php echo $map; ?>.getCenterPins();
					<?php
					if(!$print){
						?>
					if ( Ext.fly(Ext.fly(gridRental.getView().getHeaderCell(0)).first()).hasClass('x-grid3-hd-checker-on')){
						AllCheckComp=true;
						gridRental.getSelectionModel().selectAll();
						selected_dataComp=new Array();
					}else{
						AllCheckComp=false;
						var sel = [];
						if(selected_dataComp.length > 0){
							for(val in selected_dataComp){
								var ind = gridRental.getStore().find('pid',selected_dataComp[val]);
								if(ind!=-1){
									sel.push(ind);
								}
							}
							if (sel.length > 0)
								gridRental.getSelectionModel().selectRows(sel);
						}
					}
					var alto = parseInt(data.length*22)+70;
					gridRental.setHeight(alto);
					<?php }?>
					/*if(document.getElementById('overviewDetailInit'))
						document.getElementById('overviewDetailInit').click();*/
				
				}
			}
			
		});
		
		<?php if(!$print && !$ocomp){?>
		var selected_dataRental = new Array();
		var AllCheckRental=false;
		
		var smRental = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true, 
			width:25,
			listeners: {
				"rowselect": function(selectionModel,index,record){
					if(selected_dataRental.indexOf(record.get('pid'))==-1)
						selected_dataRental.push(record.get('pid'));
					
					if(Ext.fly(gridRental.getView().getHeaderCell(0)).first().hasClass('x-grid3-hd-checker-on'))
						AllCheckRental=true;
				},
				"rowdeselect": function(selectionModel,index,record){
					selected_dataRental = selected_dataRental.remove(record.get('pid'));
					AllCheckRental=false;
				}
			}
		});
		<?php }?>
		
		<?php if(!$print){?>
		var gridRental = new Ext.grid.GridPanel({
			border	:false,
			renderTo: '<?php echo $grid_render2; ?>',
			cls: 'grid_comparables',
			width: 'auto',
			height: 300,
			anchor:'100%',
			store: storeRental,
			columns: [],
			sm: smRental,
			listeners: {
				"mouseover":function (e){
					var row;
					if((row = this.getView().findRowIndex(e.getTarget())) !== false){
						var record = this.store.getAt(row);
						var ind = record.get('status').split('_')[0];
						var pin = <?php echo $map; ?>.getPushpin(ind);
						if(pin !== false) <?php echo $map; ?>.pinMouseOver(pin);
					}
				},
				"sortchange": function(grid, sortInfo){
					//alert(sortInfo.field+' '+sortInfo.direction);
					orderRentalField = sortInfo.field;
					orderRentalDir = sortInfo.direction;
				}
			}
			<?php
			if(!$report)
			{?>
			,
			tbar: new Ext.PagingToolbar({
				id: '<?php echo $pagin_comp_tol;?>',
				pageSize: limitRental,
				store: storeRental,
				width:460,
				displayInfo: true,
				displayMsg: 'Total: {2} Properties',
				emptyMsg: "No properties to display",
				items: ['Show:',
				new Ext.Button({
					tooltip: 'Click to show 50 properties per page.',
					text: 50,
					handler: function(){
						limitRental=50;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					pressed: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 80 properties per page.',
					text: 80,
					handler: function(){
						limitRental=80;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				}),'-',new Ext.Button({
					tooltip: 'Click to show 100 properties per page.',
					text: 100,
					handler: function(){
						limitRental=100;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').pageSize = limitRental;
						Ext.getCmp('<?php echo $pagin_comp_tol;?>').doLoad(0);
					},
					enableToggle: true,
					toggleGroup: 'show_compComp_group'
				})
				]
			})
			<?php
			}
			?>
		});
		<?php }?>
		
		storeRental.on('metachange', function(){
			<?php if(!$print){?>
				if(typeof(storeRental.reader.jsonData.columns) === 'object') {
					var columns = [];
					<?php if(!$ocomp){?>
					//columns.push(smComp);
					<?php }?>
					var Permi=new Array('Sta','Dis.','Address','Unit','GArea','Be','Ba','Lprice','Ldate')
					Ext.each(storeRental.reader.jsonData.columns, function(column){
						if(Permi.indexOf(column.header)!=-1)
							columns.push(column);
					});
					gridRental.getColumnModel().setConfig(columns);
				}
			<?php }else{?>
				var columns = storeRental.reader.jsonData.columns;
				var data = storeRental.reader.jsonData.records;
				var html='<table cellpadding="0" cellspacing="0" style="font: 11px arial,tahoma,helvetica,sans-serif;"><tr style="line-height:15px;">';
				for(var val in columns){
					if(Ext.isNumber(parseInt(val))){
						html+='<td style="background-color:#376A95; color:#FFFFFF; width:'+columns[val].width+'; padding:4px 3px 4px 5px; border-left:1px solid #EEEEEE; border-right:1px solid #D0D0D0;">'+columns[val].header+'</td>';
					}
				}
				html+='</tr>';
				var value,td;
				for(var d in data){
					if(Ext.isNumber(parseInt(d))){
						value = data[d];
						html+='<tr style="line-height:15px;border-bottom:1px solid #EEEEEE;">';	
						for(var c in columns){
							if(Ext.isNumber(parseInt(c))){
								
								if(columns[c].dataIndex=='status')
									td= columns[c].renderer(value[columns[c].dataIndex]);
								else if(columns[c].dataIndex!='pendes')
									td= value[columns[c].dataIndex];
								else{
									td = value[columns[c].dataIndex];
									if(value['status'].split('_')[1].split('-')[2]=='S') td+=' SOLD';
								}
								td = Ext.isEmpty(td) ? '&nbsp' : td;
								//td= value[columns[c].dataIndex];
								html+='<td style="padding:4px 3px 4px 5px;border-bottom:1px solid #EEEEEE;">'+td+'</td>';	
							}
						}
						html+='</tr>';
					}
				}
	
				html+='</table>';
				document.getElementById('<?php echo $grid_render2; ?>').innerHTML = html;
				if(data.length>0)
				{
					$('#<?php echo $grid_render2; ?>').html(html);
				}
				else
				{
					$('#<?php echo $grid_render2; ?>').html('Sorry there are not Comparables for this record');	
				}
			<?php }?>
		});
		
		if(typeof(myDataStoreRental)!='undefined')
		{
		
		/**********************
		****** carga del store
		*********************/
			<?php
				$comparado=array(
				'parcelid' => $data_comparado[0]->parcelid,
				'latitude' => $data_comparado[0]->latitude,
				'longitude' => $data_comparado[0]->longitude,
				'address' => $data_comparado[0]->address,
				'lsqft' => (float)$data_comparado[0]->lsqft,
				'bheated' => (float)$data_comparado[0]->bheated,
				'beds' => ((integer)$data_comparado[0]->beds).'/'.((integer)$data_comparado[0]->bath),
				'price' => '$'.number_format($data_comparado[0]->saleprice,0,'.',','),
				'status' => 'Subject'
				)
			 ?>
			loadDataGrip(myDataStoreRental,'<?php echo $grid_render2; ?>','<?php echo $pin; ?>',<?php echo $map ?>,'<?php echo $db_data ?>',<?php echo json_encode($comparado) ?>);
		}
		else
		{
		/**********************
		****** carga del store
		*********************/
			storeRental.load();
		}
	
		
		
	</script>
<?php
	}
/*******************************************************
************ funccion de mortgage
********************************************************/
function overviewMortgage($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude){
		conectarPorBD($db_data);
		global $report;
		$sql_comparado="Select 
		parcelid,doctype,countyid,saleppsf,docdesc,usecode,
		saleprice,recdate,usedesc,grantor,buyer2,grantor_st,buyeradd2,
		buyer1,buyeradd3,buyer1_st,buyeradd4,buyeradd1,buyercity,
		buyerstate,buyercount,buyerzip,mtg_bor1,mtg_doc_ty,
		mtg_bor2,mtg_doc_dc,mtg_orbk,mtg_rattyp,mtg_orpg,mtg_amount,
		mtg_recdat,mtg_term,mtg_insdat,mtg_intrst,mtg_len_ad,
		titleco,mtg_lender
		FROM mortgage 
		Where parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$i=1;
		
		$mortgage=mysql_num_rows($res);
		if(!$report)
		{
			echo '<div class="history">
        		Property(ies) and sales information from public records
        	</div>';
		};
		?>
        <div class="titlePrincipal">
        	<span>
            	Mortgages
            </span>
        </div>
		<?php
		$conta=1;
		$i=1;
		$visibles=array('parcelid','docdesc','mtg_bor1','mtg_lender','mtg_orbk','mtg_orpg','mtg_recdat','mtg_amount');
        while($myrow= mysql_fetch_array($res))
		{
			$myrow['mtg_orbk']=number_format($myrow['mtg_orbk'],0,'.',',');
			$myrow['mtg_orpg']=number_format($myrow['mtg_orpg'],0,'.',',');
			$myrow['mtg_amount']=number_format($myrow['mtg_amount'],2,'.',',');
			$myrow['mtg_recdat']=formantDateNumber( $myrow['mtg_recdat']);
			
			echo '<br><div class="fila bigFila titleBlue centertext bold">Mortgage '.$i.'</div>';
			foreach($visibles as $key =>$valor)
			{
				echo ($conta%2==0)?'<div class="fila par '.$escpecialClass[$valor]['clsPrincipal'].'">':'<div class="fila '.$escpecialClass[$valor]['clsPrincipal'].'">';
				echo '
					<div class="title '.$escpecialClass[$valor]['clsTitle'].'">
						<div class="cuote"></div>
						'.camptit($valor,'mortgage','titulos_a').':
					</div>
					<div class="describe '.$escpecialClass[$valor]['clsDescribe'].'">
						'.$myrow[$valor].'
					</div>
				</div>
				';
				$conta+=($escpecialClass[$valor]['doulbeSpace'])? 1 : 0.5;
			}
			$i++;
		}
		?>
        <div class="clearEmpty">
        </div>
	<?php	
}

	function overviewForeclosures($pid,$db_data,$map,$map2,$status_pro,$lprice_pro,$latitude,$longitude){
		conectarPorBD($db_data);
		$sql_comparado="Select 
		p.parcelid,p.prop1samt,p.plaintiff1,p.case_numbe,p.prop1sdate,p.mtg1amt,
		p.foreclosur,p.assessedva,p.mtg1bal,p.file_date,p.loanvalrat,p.mtg1book,
		p.defowner1,p.marketvalu,p.mtg1date,p.defowner2,p.judgeamt,p.mtg1intrat,
		p.defsumaddr,p.judgedate,p.mtg1lastpa,p.defsumname,p.judgeeq,p.mtg1page,
		p.totaliens,p.judgesaler,p.mtg1paymen,p.totalpendes,p.attorney,p.mtg1positi,
		m.debttv,p.attorneyp,p.mtg1ratety,p.mtg1type,
		p.lien1amt,p.lien2amt,p.lien3amt,p.lien7amt,
		p.lien1book,p.lien2book,p.lien3book,p.lien7book,
		p.lien1holde,p.lien2holde,p.lien3holde,p.lien7holde,
		p.lien1page,p.lien2page,p.lien3page,p.lien7page,
		p.lien1type,p.lien2type,p.lien3type,p.lien7type,
		p.lien4amt,p.lien5amt,p.lien6amt,
		p.lien4book,p.lien5book,p.lien6book,
		p.lien4holde,p.lien5holde,p.lien6holde,
		p.lien4page,p.lien5page,p.lien6page,
		p.lien4type,p.lien5type,p.lien6type,
		p.entrydate, m.pendes, m.sold
		FROM pendes p LEFT JOIN marketvalue m ON (p.parcelid=m.parcelid)
		Where p.parcelid='$pid';";	
		$res = mysql_query($sql_comparado) or die(mysql_error());
		$myrow= mysql_fetch_array($res);
		$visibles=array('parcelid','case_numbe','totalpendes','debttv','defowner1','defowner2','entrydate','plaintiff1','mtg1amt','mtg1bal','mtg1book','mtg1page','mtg1paymen','judgeamt','mtg1intrat','judgedate','mtg1lastpa','attorney','mtg1positi','attorneyp','mtg1ratety','mtg1type');
		$myrow['entrydate']=formantDateNumber($myrow['entrydate']);
		$myrow['mtg1amt']=number_format($myrow['mtg1amt'],2,'.',',');
		$myrow['mtg1bal']=number_format($myrow['mtg1bal'],2,'.',',');
		$myrow['mtg1date']=formantDateNumber($myrow['mtg1date']);
		$myrow['judgeamt']=number_format($myrow['judgeamt'],2,'.',',');
		$myrow['judgedate']=formantDateNumber($myrow['judgedate']);
		$myrow['mtg1lastpa']=formantDateNumber($myrow['mtg1lastpa']);
		?>
        <div class="titlePrincipal">
        	<span>
            	Foreclosure Information
            </span>
        </div>
        
        <?php
		$conta=1;
		foreach($visibles as $key =>$valor)
			{
				echo ($conta%2==0)?'<div class="fila par '.$escpecialClass[$valor]['clsPrincipal'].'">':'<div class="fila '.$escpecialClass[$valor]['clsPrincipal'].'">';
				echo '
					<div class="title '.$escpecialClass[$valor]['clsTitle'].'">
						<div class="cuote"></div>
						'.camptit($valor,(($valor=="debttv")?"marketvalue":"pendes")).':
					</div>
					<div class="describe '.$escpecialClass[$valor]['clsDescribe'].'">
						'.$myrow[$valor].'
					</div>
				</div>
				';
				$conta+=($escpecialClass[$valor]['doulbeSpace'])? 1 : 0.5;
			}
			
			
			
			
			////////////////////////Liens////////////////////////////////
                $sql_comparado="Select 
                lien1amt,lien2amt,lien3amt,lien7amt,
                lien1book,lien2book,lien3book,lien7book,
                lien1holde,lien2holde,lien3holde,lien7holde,
                lien1page,lien2page,lien3page,lien7page,
                lien4amt,lien5amt,lien6amt,
                lien4book,lien5book,lien6book,
                lien4holde,lien5holde,lien6holde,
                lien4page,lien5page,lien6page
                FROM pendes 
                Where parcelid='$pid' and totaliens >0";	
                $res = mysql_query($sql_comparado) or die(mysql_error());
                $myrow= mysql_fetch_array($res);
                $liens=mysql_num_rows($res);
                if($liens>0){
					echo '
					<div class="titlePrincipal">
						<span>
							Liens Information
						</span>
					</div>
					';
					for($i=1;$i<8;$i++)
					{
						$amt='lien'.$i.'amt';
						$holde='lien'.$i.'holde';
						$book='lien'.$i.'book';
						$page='lien'.$i.'page';
						if($myrow[$amt]!='0.00' || $myrow[$holde]!='' || $myrow[$book]!='' || $myrow[$page]!=''){
							echo'
								<div class="title '.$escpecialClass[$amt]['clsTitle'].'">
									<div class="cuote"></div>
										'.camptit($amt,'pendes').':
									</div>
									<div class="describe '.$escpecialClass[$amt]['clsDescribe'].'">
										'.$myrow[$amt].'
									</div>
								</div>
								<div class="title '.$escpecialClass[$holde]['clsTitle'].'">
									<div class="cuote"></div>
										'.camptit($holde,'pendes').':
									</div>
									<div class="describe '.$escpecialClass[$holde]['clsDescribe'].'">
										'.$myrow[$holde].'
									</div>
								</div>
								<div class="title '.$escpecialClass[$book]['clsTitle'].'">
									<div class="cuote"></div>
										'.camptit($book,'pendes').':
									</div>
									<div class="describe '.$escpecialClass[$book]['clsDescribe'].'">
										'.$myrow[$book].'
									</div>
								</div>
								<div class="title '.$escpecialClass[$page]['clsTitle'].'">
									<div class="cuote"></div>
										'.camptit($page,'pendes').':
									</div>
									<div class="describe '.$escpecialClass[$page]['clsDescribe'].'">
										'.$myrow[$page].'
									</div>
								</div>
							';
						}
					}
					
        /*?>
        <br />
        <h1 align="left" style="color:#15428b;font-size:20px;margin-left:8px;">LIENS INFORMATION</h1>
        <table class=content id='pagtag_table' cellpadding="0" cellspacing="0" style='font-family:Verdana,Arial,Helvetica,sans-serif;font-size:1.3em;color:#15428b;width:100%;'>
            <?php 
            $i=1;
            while($i<=7){
                $amt='lien'.$i.'amt';
                $holde='lien'.$i.'holde';
                $book='lien'.$i.'book';
                $page='lien'.$i.'page';
                
                if($myrow[$amt]!='0.00' || $myrow[$holde]!='' || $myrow[$book]!='' || $myrow[$page]!=''){?>
                <tr style="background-color:#b5cedd;">
                    <td colspan="4" align="center"><span style="font-weight:bold;">LIEN <?php echo $i;?></span></td>
                </tr>
                <tr align="left">
                    <td valign=top title="<?php camptit($amt,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($amt,"pendes");?>:</span></td>
                    <td><?php echo number_format($myrow[$amt],2,'.',',');?></font></td>
                    <td valign=top title="<?php camptit($holde,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($holde,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$holde];?></font></td>
                </tr>
                <tr align="left">
                    <td valign=top title="<?php camptit($book,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($book,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$book];?></font></td>
                    <td valign=top title="<?php camptit($page,"pendes",'desc');?>"><span style="font-weight:bold;"><?php echo camptit($page,"pendes");?>:</span></td>
                    <td><?php echo $myrow[$page];?></font></td>
                </tr>
            <?php }
                $i++;
            }?>
            <tr style="background-color:#b5cedd;">
                <td colspan="4">&nbsp;</td>
            </tr>
        </table>
        <?php
            }
        ?>
        
        </div>
        </div>
        <script>
            SpaceNeedle=new VELatLong(<?php echo $latitude.','.$longitude;?>);
            var <?php echo $map; ?> = new VEMap('<?php echo $map; ?>');
            <?php echo $map; ?>.LoadMap(SpaceNeedle, 15, VEMapStyle.BirdseyeHybrid);
            <?php echo $map; ?>.HideDashboard();
            var pin = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pin.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map; ?>.AddShape(pin);
                        
            var <?php echo $map2; ?> = new VEMap('<?php echo $map2; ?>');
            <?php echo $map2; ?>.LoadMap(SpaceNeedle, 15);
            <?php echo $map2; ?>.HideDashboard();
            var pinS = new VEShape(VEShapeType.Pushpin, SpaceNeedle);
            pinS.SetCustomIcon("<div style='position:relative;text-align:center;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;font:bold 10px;cursor:pointer;width:25px;height:30px;top:-15px' >"+
                "<img style='position:absolute;top:0;left:0;z-index:100' src='http://96.31.84.77/img/houses/verdetotal.png'/></div>");
            <?php echo $map2; ?>.AddShape(pinS);
        
        </script>
			
			
			
			
			
			
			
			
		<?php */
	}
			
	}
?>