<?php
require '../../properties_conexion.php';
conectar();

function getChild($id){
	global $busqueda;
	global $busquedav;
	global $busExtruct;
	global $busVideo;
	
	$sql='SELECT a.*, v.name, v.idYoutube, m.name name_modulo FROM training_assignment_video a 
		join videos_training v on a.id_video=v.id 
		join training_modulo m on a.id_modulo=m.id where a.id_modulo='.$id.' AND v.status=5
		'.
					(($busVideo=='')?'':' AND v.id in ('.$busVideo.')')
					.'
		  order by COALESCE(a.pos, 999999999) ASC, `name` ';
	$res2=mysql_query($sql) or die (mysql_error());
	$menu=array();
	while($data2=mysql_fetch_assoc($res2)){
			$respuesta=array( 
				'text' => $data2['name'],
				'iconCls' => 'x-icon-movie',
				'isVideo'	=>	true,
				'id'=> $data2['id'].$data2['id_modulo'],
				'id_modulo'=> $data2['id_modulo'],
				'name_modulo'=> $data2['name_modulo'],
				'id_video'=>$data2['id_video'],
				'id_assing'=>$data2['id'] ,
				'leaf' => true, 'cls' => (in_array($data2['id_video'],$busquedav)?'resaltado':'')
			);
			array_push($menu, $respuesta);
	}
	$sql='SELECT m.*, id FROM training_modulo m WHERE father='.$id.' 
					'.
					(($busExtruct=='')?'':' AND id in ('.$busExtruct.')')
					.'  order by COALESCE(pos, 999999999) ASC, `name`';
	$res=mysql_query($sql) or die (mysql_error());
	while($data=mysql_fetch_assoc($res)){
			$childs=getChild($data['id']);
			$respuesta=array(
				'text' => $data['name'],
				'expanded' => (($_POST['q']!='')?true:false),
				'leaf' => false,
				'children' => $childs , 
				'id'=> $data['id'],
				'cls' => (in_array($data['id'],$busqueda)?'resaltado':'')
			);
			array_push($menu, $respuesta);
	}
	return $menu;
}
switch($_POST['action']){
	case('listMenu'):
		if($_POST['q']!=''){
			$_POST['q']=str_replace(' ','%',$_POST['q']);
			$busExtruct='';
			$busVideo='';
			$sql='SELECT id_modulo,id_video, t.name ,father
					FROM 
						videos_training t 
					join training_assignment_video a on id_video=t.id 
					join training_modulo m on m.id=a.id_modulo 
				WHERE t.typeHelp='.$_POST['typeHelp'].' AND (t.name LIKE "%'.$_POST['q'].'%" OR t.`description` LIKE "%'.$_POST['q'].'%")';
			$res=mysql_query($sql) or die ($sql.mysql_error());
			$busqueda=array();
			$busquedav=array();
			while($data=mysql_fetch_assoc($res)){
				array_push($busqueda,$data['id_modulo']);
				array_push($busquedav,$data['id_video']);
				$busExtruct.=$data['id_modulo'].','.(getFatherModule($data['father']));
				$busVideo.=(($busVideo=='')?'':',').$data['id_video'];

			}
		}
		$busExtruct=substr($busExtruct, 0, -1);
		$id=$_POST['id'];
		if($id=='id')
			$sql='SELECT m.*, id FROM training_modulo m 
				WHERE m.typeHelp='.$_POST['typeHelp'].' AND father  IS NULL
					'.
					(($busExtruct=='')?'':' AND m.id in ('.$busExtruct.')')
					.' order by COALESCE(pos, 999999999) ASC, `name`';
		else
			$sql='SELECT m.*, id FROM training_modulo m WHERE m.typeHelp='.$_POST['typeHelp'].' AND father='.$id.'
					'.
					(($busExtruct=='')?'':' AND m.id in ('.$busExtruct.')')
					.' order by COALESCE(pos, 999999999) ASC, `name`';
		//echo $sql;die();
		$res=mysql_query($sql) or die (mysql_error());
		$tree=array();
		while($data=mysql_fetch_assoc($res)){
			$childs=getChild($data['id']);
			$tem=array( 
				'text' => $data['name'],
				'expanded' => (($_POST['q']!='')?true:false), 
				'id'=> $data['id'], 
				'children' => $childs,
				'idsSearch' => $busqueda
			);
			array_push($tree,$tem);
		}
		echo json_encode($tree);
	break;
	case('getDataRelated'):
		$sql='(SELECT m.*,c.* FROM realtytask.help_related r join help_menu m on r.related=m.id_menu join help_content c on r.related=c.id_menu WHERE r.id_menu='.$_POST['id'].')
union all
(SELECT m.*,c.* FROM realtytask.help_related r join help_menu m on r.id_menu=m.id_menu join help_content c on r.id_menu=c.id_menu WHERE r.related='.$_POST['id'].');'
		;
		$res=mysql_query($sql) or die ($sql.mysql_error());
		$related=array();
		while($data=mysql_fetch_assoc($res)){
			array_push($related,$data);
		}
		echo json_encode(array('success' => true, 'related' => $related));
	break;
	case('getDataPanel'):
		$respuesta=array();
		$modules=$_POST['id'].getChilModules($_POST['id']);
		
		$secuncy="SELECT * FROM training_assignment_video a 
					join videos_training v on a.id_video=v.id WHERE a.id_modulo in ({$modules}) AND v.status=5 order by `pos`";
		$res=mysql_query($secuncy) or die ($secuncy.mysql_error());
		$respuesta['sequence']=array();
		while($data=mysql_fetch_assoc($res)){
			if($data['id_menu']==$_POST['id']){
				$data['class']='active';
			}
			array_push($respuesta['sequence'],$data);
		}
		echo json_encode(array('success' => true, 'content' => $respuesta['content'], 'sequence' => $respuesta['sequence'], 'sql' => $secuncy));
		
	break;
}
function getChilModules ($id=NULL){
	$sql="SELECT * FROM training_modulo where father={$id};";
	$result=mysql_query($sql) or die(mysql_error());
	$childConsut='';
	while($tem = mysql_fetch_assoc($result)){
		$childConsut.=', '.$tem['id'];
		$childConsut.=getChilModules($tem['id']);
	}
	$childs.=$childConsut;
	return $childs;
}
function getFatherModule ($id,$ids){
	if($id!=''){
		
		$sql="SELECT * FROM training_modulo where id={$id} limit 1;";
		$result=mysql_query($sql) or die($sql.mysql_error());
		while($tem = mysql_fetch_assoc($result)){
			$ids=$tem['id'].','.$ids;
			$ids=getFatherModule($tem['father'],$ids);
		}
		$ids=$ids;
		
	}
	return $ids;
}
?>