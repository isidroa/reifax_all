<?php
	require_once('simple_html_dom.php');
	require_once('properties_conexion.php');
	
	function getTiempo() {   
         list($usec, $sec) = explode(" ",microtime());   
         return ((float)$usec + (float)$sec);   
    }
	
	function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
       
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
    return $_SERVER['REMOTE_ADDR'];
	}

    function get_ip_info($ip = NULL) 
    { 
       if(empty($ip)) return false; 
       $ch = curl_init(); 
       curl_setopt($ch, CURLOPT_URL, 'http://www.ipaddresslocation.org/ip-address-locator.php'); 
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
       curl_setopt($ch, CURLOPT_POST, true); 
       curl_setopt($ch, CURLOPT_POSTFIELDS, array('ip' => $ip)); 
       $data = curl_exec($ch); 
       curl_close($ch);     
       preg_match_all('/<i>([a-z\s]+)\:<\/i>\s+<b>(.*)<\/b>/im', $data, $matches, PREG_SET_ORDER); 
       if(count($matches) == 0) return false;    
       $return = array(); 
       $format_labels = array(
            'IP Latitude'       => 'latitude', 
            'IP Longitude'      => 'longitude'
       ); 
       foreach($matches as $info) 
       { 
          if(isset($info[2]) && !is_null($format_labels[$info[1]])) 
          { 
             $return[$format_labels[$info[1]]] = $info[2]; 
          } 
       }
       return (count($return)) ? $return : false; 
    }

	
	
	function getCounty($latitude,$longitude)
	{
		
		conectar('xima');
		if($longitude!='' && $longitude!=''){
			$que="select county,idcounty,minlat, maxlat from lscounty where minlat<".$latitude." 
				and maxlat>".$latitude." and minlong<".$longitude." and maxlong>".$longitude;
			$result=mysql_query($que) or die ($que." ".mysql_error);
			$countyclient=0;
			$posibles='';
			$total=mysql_num_rows($result); 
			if($total==0){
				$countymin=2;
				$mindis=-1;
				$final=0;
			}else if($total==1){
				$final=1;
				$r=mysql_fetch_array($result);
				$countymin=$r['idcounty'];
				$bd_search='`'.conectarPorIdCounty($countymin).'`';
				if($bd_search=='``'){
					$bd_search='`'.$countybd[$i].'`';
				}
				$querydistancia="SELECT latitude, longitude,truncate(sqrt((69.1* (p.latitude- $latitude))*(69.1*(p.latitude-$latitude))+(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))*(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))),2) as distancia
							from $bd_search.latlong p order by distancia limit 1";
				
				
				$result1=mysql_query($querydistancia) or die ($querydistancia." ".mysql_error);
				$r=mysql_fetch_array($result1);
				$distancia=$r['distancia'];
				//echo ' '.$distancia;
				$mindis=$distancia;
			}else{
				$final=1;
				while($row=mysql_fetch_array($result)){
					$county=$row['county'];
					$idcounty=$row['idcounty'];
					$minlat=$row['minlat'];
					$maxlat=$row['maxlat'];
					$minlong=$row['minlong'];
					$maxlong=$row['maxlong'];
					$posibles.=','.$idcounty;
					//$countyclient=$idcounty;
				}
				$posibles=ltrim($posibles,',');
				$que="select idcounty,county, bd, truncate(sqrt((69.1* (avglat- $latitude))*(69.1*(avglat-$latitude))+(69.1*
						((avglong-($longitude))*cos($latitude/57.29577951)))*(69.1*((avglong-($longitude))*cos($latitude/57.29577951)))),2) as distancia 
						from lscounty where idcounty in ($posibles)
						order by distancia"; 
				$result=mysql_query($que) or die ($que." ".mysql_error);
				$ordenado=Array();
				$countybd=Array();
				while($row=mysql_fetch_array($result)){
					$ordenado[]=$row['idcounty'];
					$countybd[]=$row['bd'];	
				}
				$mindis=10000;
				$countymin=0;
				$i=0;
				foreach($ordenado as $idcounty){
					//echo '<br>'.$idcounty;
					$bd_search='`'.conectarPorIdCounty($idcounty).'`';
					if($bd_search=='``'){
						$bd_search='`'.$countybd[$i].'`';
					}
					$querydistancia="SELECT latitude, longitude,truncate(sqrt((69.1* (p.latitude- $latitude))*(69.1*(p.latitude-$latitude))+(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))*(69.1*((p.longitude-($longitude))*cos($latitude/57.29577951)))),2) as distancia
								from $bd_search.latlong p order by distancia limit 1";
					
					
					$result1=mysql_query($querydistancia) or die ($querydistancia." ".mysql_error);
					$r=mysql_fetch_array($result1);
					$distancia=$r['distancia'];
					//echo ' '.$distancia;
					if((float)$distancia<=$mindis){
						$mindis=(float)$distancia;
						$countymin=$idcounty;
					}
					$i++;
				}
			}	
			//echo "<br>El condado es ".$countymin." ".$mindis;
			$timeF = getTiempo();	
			$total = $timeF - $timeI;
			//echo '<br><br>Tiempo de ejecucion '.round($total,2);
			return array('county' => $countymin, 'final' =>$final);
			//echo '{"idcounty":"'.$countymin.'","distancia":"'.$mindis.'","msg":"'.$msg.'"}';	
		}
	}
	conectar('xima');
	$ip=getRealIP();
	$sql='SELECT * FROM ip_county WHERE ip_address="'.$ip.'" limit 1';
	$resul=mysql_query($sql) or die ($sql." ".mysql_error);
	if(mysql_num_rows($resul)!=0)
	{
		$data=mysql_fetch_array($resul);
		$ultimaFecha=explode('-',$data['date_update']);
		if($data['final']==0 && ((time()-mktime(0,0,0,$ultimaFecha[1],$ultimaFecha[2],$ultimaFecha[0]))/(60*60*24)>30))
		{
			$coorde=get_ip_info($ip);
			$county=getCounty($coorde['latitude'],$coorde['longitude']);
			$sql='update xima.ip_county set id_county="'.$county['county'].'", date_update=NOW(), final='.$county['final'].' where ip_address="'.$ip.'"';
			$county=$county['county'];
			mysql_query($sql) or die ($sql." ".mysql_error);
		}
		else
		{
			$county=$data['id_county'];
		}
	}
	else
	{
		$coorde=get_ip_info($ip);
		$county=getCounty($coorde['latitude'],$coorde['longitude']);
		$sql='insert into xima.ip_county 
			(ip_address,id_county,date_update,final,latitud,longitud,country,state,city) values
			("'.$ip.'",'.$county['county'].',NOW(),'.$county['final'].',"'.$coorde['la'].'","'.$coorde['lo'].'","'.$coorde['country'].'","'.$coorde['state'].'","'.$coorde['city'].'")';
		mysql_query($sql) or die ($sql." ".mysql_error);
		$county=$county['county'];
	}
	
	
	echo json_encode(array('success' => true, 'idcounty' =>$county ));
?>