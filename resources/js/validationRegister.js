// JavaScript Document

	/**
	validaciones de campos requeridos
	*********/
var correcto=pass=repass=email=true;
var enviar=false;
var form=false;
var validationRegister= function ()
{
	$('.zipcode').bind('keypress',solonumeros);
	
	$('.fieldRequired').each(function (){
		$(this).bind('blur',function (){
		if($(this).val())
		{
			$(this).parent().find('.noteRegister').show();
			$(this).parent().find('.errorRegister').hide();
			$(this).parent().parent().find('.noteRegister').show();
			$(this).parent().parent().find('.errorRegister').hide();
			$(this).removeClass('ErrorField');
			correcto=($('.ErrorField').length==0)?true:false;
		}
		else
		{
			$(this).parent().find('.noteRegister').hide();
			$(this).parent().find('.errorRegister').html('Required field').show();
			$(this).parent().parent().find('.noteRegister').hide();
			$(this).parent().parent().find('.errorRegister').html('Required field').show();
			$(this).addClass('ErrorField');
			correcto=false;
		}
		})
	});
	
	
	/*
	++++++++++++++++++++++++++++	validaciones del correo
	*/ 
	$('#email').bind('blur',function (){
			if($(this).attr('alt')=='ddregister')
				return;

			if(!isValidEmail($(this).val()))
			{
				$(this).addClass('ErrorField');
				$('#emailNote').hide();
				$('#emailNoteError').html("Email is incorrect").show();
				email=false;
			}
			else
			{
			$.ajax({
				url		: '../resources/php/validacionRegister.php',
				data	: 'type=email&data='+$(this).val(),
				dataType: 'json',
				success	:function (resul)
				{
					if(resul.status)
					{
						$('#email').addClass("ErrorField"); 
						$('#emailNote').hide();
						$('#emailNoteError').html(resul.msg).show();
						email=enviar=false;
					}
					else
					{
						email=true;
						if(enviar && $('.ErrorField').length<1){
							$.ajax({
								type:'POST',
								url	: "/resources/php/datarecord.php",
								data	:'action=record&statusRegister=Success&error=&'+$(form).serialize(),
								success	:function (result)
										{		
											$(form).submit();
										}
							});
						}
						
					}
				}
			})
		}
	}).bind('focus',function (){
				$('#emailNote').show();
				$('#emailNoteError').hide();
				email=true;
	});
	/*
	++++++++++++++++++++++++++++	validaciones para la comprovacion del correo
	*/ 
	$('#Re-email').bind('blur',function (){
			if($('#email').val()!=$(this).val())
				{
					$(this).addClass("ErrorField"); 
					$('#Re-emailNoteError').html("email do not match").show();
					remail=false;
				}
	}).bind('focus',function (){
			$(this).removeClass('ErrorField');
			$('#Re-emailNoteError').hide();
			remail=true;
	});
	
	/*
	++++++++++++++++++++++++++++	validaciones del password
	*/ 
	$('#password').bind('blur',function(){
			//var exp_pass = new RegExp("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$");
			//if((!exp_pass.test($(this).val())) && $(this).val()!='')
			if( $(this).val().length<5 )
			{
				$(this).addClass('ErrorField');
				$('#passwordNote').hide();
				$('#passwordNoteError').html("Must be at least 5 characters").show();//and contain a letter and a number
				pass=false;
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				$('#passwordNote').show();
				$('#passwordNoteError').hide();
				pass=true;
	});	
	/*
	++++++++++++++++++++++++++++	validaciones los numero de telefonos
	*/
	$('#txtPhoneNumber').bind('blur',function(){
			if($(this).val().length<12 )
			{
				$(this).addClass('ErrorField');
				//$('#passwordNote').hide();
				$('#numberPhoneError').html("Invalid phone!!").show();
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				//$('#passwordNote').hide();
				$('#numberPhoneError').hide();
	});	

	$('#MobileNumber').bind('blur',function(){
			if($(this).val().length>0 && $(this).val().length<12 )
			{
				$(this).addClass('ErrorField');
				//$('#passwordNote').hide();
				$('#numberPhoneError2').html("Invalid phone!!").show();
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				//$('#passwordNote').hide();
				$('#numberPhoneError2').hide();
	});	

	$('#OrganizationNumber').bind('blur',function(){
			if($(this).val().length<12 )
			{
				$(this).addClass('ErrorField');
				//$('#passwordNote').hide();
				$('#OrganizationNumberError').html("Invalid phone!!").show();
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				//$('#passwordNote').hide();
				$('#OrganizationNumberError').hide();
	});	


	$('#OrganizationFax').bind('blur',function(){
			if( $(this).val().length<12 )
			{
				$(this).addClass('ErrorField');
				//$('#passwordNote').hide();
				$('#OrganizationFaxError').html("Invalid phone!!").show();
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				//$('#passwordNote').hide();
				$('#OrganizationFaxError').hide();
	});	
	
/*	
	function phoneError (campo)
	{
		$(campo).parent().parent().find('.errorRegister').html('No spaces, dashes or punctuation and must be 7 numbers').show().find('.noteRegister').hide().parent();
		$(campo).addClass('ErrorField');
	}
	$('.numberPhone').each(function (){
		cod= new RegExp("^([0-9]{3,6})$");
		num= new RegExp("^([0-9]{7})$");
		$(this).find('input:eq(0)').bind('blur',function (){
			if($(this).is('.fieldRequired'))
			{
				if(!cod.test($(this).val()))
				{
					phoneError (this);
				}
			}
			else
			{
				if(!cod.test($(this).val()) && $(this).val()!='')
				{
					phoneError (this);
				}
			}
			}).bind('focus',function (){
				$(this).parent().parent().find('.errorRegister').hide().parent().find('.noteRegister').show();
				$(this).removeClass('ErrorField');
			})
		$(this).find('input:eq(1)').bind('blur',function (){
			if($(this).is('.fieldRequired'))
			{
				if(!num.test($(this).val()))
				{
					phoneError (this);
				}
			}
			else
			{
				if($(this).val()!='' && !num.test($(this).val()))
				{
					phoneError (this);
				}
			}
			}).bind('focus',function (){
				$(this).parent().parent().find('.errorRegister').hide().parent().find('.noteRegister').show();
				$(this).removeClass('ErrorField');
			})
		
	});
*/
	/*
	++++++++++++++++++++++++++++	validaciones para la comprovacion del password
	*/
	$('#re-password').bind('blur',function(){
			if($(this).val()!=$('#password').val())
			{
				$(this).addClass('ErrorField');
				$('#Re-passwordNoteError').html("Password do not match").show();
				repass=false;
			}
	}).bind('focus',function (){
				$(this).removeClass('ErrorField');
				$('#Re-passwordNoteError').hide();
				repass=true;
	});
	
	/*
	++++++++++++++++++++++++++++	validaciones del nickname
	*/ 
	$('#nickName').bind('blur', function (){
			if($('#nickName').val())
			{
				var exp_nickName= new RegExp("^([0-9]*)$");
				if(!exp_nickName.test($(this).val()))
				{
					$.ajax({
						url		: '../resources/php/validacionRegister.php',
						data	: 'type=nickName&data='+$(this).val(),
						success	:function (resul)
						{
							if(resul)
							{
								$('#nickName').removeClass('ErrorField');
								$('#nickNameNoteError').html('Nick Name already exists').show();
								nick=false;
							}
						}
					})
				}
				else
				{
					$(this).addClass('ErrorField');
					$('#nickNameNoteError').html('Must contain at least one alphanumeric character').show();
					nick=false;
				}
			}
	}).bind('focus',function(){
				$(this).removeClass('ErrorField');
				$('#nickNameNoteError').hide();
				nick=true;
	});
	/*
	++++++++++++++++++++++++++++	validaciones tarjeta credito
	*/ 
	$('#cardNumber').bind('blur', function (){
			if($(this).val())
			{
				var exp_nickName= new RegExp("^([0-9]{15,18})$");
				if(!exp_nickName.test($(this).val()))
				{
					$(this).addClass('ErrorField');
					$('#noteCardNumber').hide();
					$('#errorCardNumber').html('No spaces, dashes or punctuation').show();
					credicard=false;
				}
			}
	}).bind('focus',function(){
				$(this).removeClass('ErrorField');
				$('#errorCardNumber').hide()
				$('#noteCardNumber').show();
				credicard=true;
	});
	$('#csv').bind('blur', function (){
			if($(this).val())
			{
				var exp_nickName= new RegExp("^([0-9]*)$");
				if(!exp_nickName.test($(this).val()))
				{
					$(this).addClass('ErrorField');
					$(this).parent().parent().find('.noteRegister').hide();
					$(this).parent().parent().find('.errorRegister').html('Must contain at least one alphanumeric character').show();
					correcto=false;
				}
			}
	}).bind('focus',function(){
				$(this).removeClass('ErrorField');
				$(this).parent().parent().find('.noteRegister').show();
				$(this).parent().parent().find('.errorRegister').hide();
				correcto=($('.ErrorField').length==0)?true:false;
	});
/********************
	ocultar o mostrar el field para la licencia de la oficina
*******************/

$("input[name='companyRegister']").change(function(){
	if ($("input[name='companyRegister']:checked").val() == 'no')
	{
		$("#divOfficeNumber").hide('slow');
		$("#divOfficeNumber input").removeClass('fieldRequired');
	}
	else
	{
		$("#divOfficeNumber input").addClass('fieldRequired');
		$("#divOfficeNumber").show('slow');
	}
});	


	
/********************
	*eventos para los user type
*******************/
	$('#usertype').bind('change',function ()
	{
		if($(this).val()==8 || $(this).val()==3 || $(this).val()==9 || $(this).val()==20)
		{
			$('#fieldLicense').show('slow',function ()
			{
				$("input[name='companyRegister'][value=no]").attr('checked','checked');
				$("#divOfficeNumber").hide('slow');
				$(this).find('input').addClass('fieldRequired');
				$("#divOfficeNumber input").removeClass('fieldRequired');
				$('#questionCompany').show('slow');
			});
		}
		else if($(this).val()==13 || $(this).val()==21 || $(this).val()==25)
		{
			$('#questionCompany').hide('slow',function ()
			{
				$('#fieldLicense').hide('slow',function (){
						$(this).find('input').removeClass('fieldRequired');
					})
			}
			); 
			$("input[name='companyRegister'][value=yes]").attr('checked','checked');
			$("#divOfficeNumber input").addClass('fieldRequired');
			$("#divOfficeNumber").show('slow');
			
		}
		else
		{
			$("input[name='companyRegister'][value=no]").attr('checked','checked');
			$("#divOfficeNumber").hide('slow');
			$('#questionCompany').hide('slow',function ()
			{
				$('#fieldLicense').hide('slow',function (){
						$(this).find('input').removeClass('fieldRequired');
					})
			}
			);
		}
	});
	$('#fastLogin').bind('click',function (e){
		e.preventDefault();
		$('.lockScreen').fadeTo(200,0.5).bind('click',function (){
			$(this).fadeOut(200).unbind('click');
			$('.MsglockScreen').fadeOut('200').css('padding','20px');
			$('#ventanalogueo').fadeOut('fast');
		});
		if($('#ventanalogueo').length){
			$('#ventanalogueo').fadeIn('fast');
		}
		else{
			$('.MsglockScreen').html('<iframe frameborder="0" src="https://www.reifax.com/register/fastlogin.php" width="320" height="120">').css('padding','10px').fadeIn();
		}	
	});

}
// ********* 	Acciones para el boton de confirmacion 
function toConfimationPage(boton,formIns)
{	
$(boton).bind('click',function (e){
	e.preventDefault();
	form=formIns;
	enviar=true;	
	$('.fieldRequired').each(function (){
			$(this).trigger('blur');
		})
		if($('.ErrorField').length)
		{
			var error='';
			$('.ErrorField').each(function (a,b){
				error=error+', '+$(this).attr('name');
			})
			$.ajax({
				type:'POST',
				url	: "/resources/php/datarecord.php",
				data	:'action=record&statusRegister=Failure&error='+error+'&'+$(form).serialize()
			});
			$('#errorForm').stop(true,true).css({opacity:0,display:'block'}).animate({
						top			:'-65px',
						opacity		:1
					},400).delay(3000).animate({
						top		:'-55px'
					},200,function (){
							$(this).animate({
								top		:'-100px',
								opacity	:0
							},400,function (){
									$(this).css({display:'none'});
						});
				});
		}
		else{
			if(!$('#email').length){
				$(formIns).submit();
			}
		}
			
	});
	$(formIns).bind('submit',function (){
		$('#cbGrupoProductsText').attr('value',$('#product option:selected').html());
		$('input[name=price]').attr('value',$('.totalValue').html());
		$('#cbGrupoFrecuencyText').attr('value',$('#frencuency option:selected').html());
		$('#cbGrupoStateText').attr('value',$('#State option:selected').html());
		$('#cbGrupoCountyText').attr('value',$('#County option:selected').html());
		$('#usertypeText').attr('value',$('#usertype option:selected').html());
		$('#cardtypeText').attr('value',$('#cardtype option:selected').html());
	});
}
var funciones={
	proccessUpdate:function (e)
	{
		e.preventDefault();
		$('.fieldRequired').each(function (){
			$(this).trigger('blur');
			})
		if(correcto && pass && repass && email)
		{
			$('.lockScreen').fadeTo(100,0.5);
			$('.MsglockScreen').fadeIn();
			$.ajax({
				type	: 'POST',
				data 	: $(this).serialize(),
				dataType: 'json',
				url		: $(this).attr('action'),
				success	:function (respuesta)
							{
							if(respuesta.success)
							{
								$('.MsglockScreen').html('Your changes has been successfully applied <div class="clear"></div><a href="#" class="buttonblue cerrarVentana">Ok...</a>');
								$('.cerrarVentana').bind('click',function (){
									$('form').trigger('callback');
									$('.lockScreen,.MsglockScreen').fadeOut(200, function (){
												$('.MsglockScreen').css({
													 'font-size': '26px',
													 color		:'#005C83',
												}).html('<img src="../resources/img/ac.gif">  Processing...');
										});
									})
							}
							else
							{
								$('.MsglockScreen').css({
									'font-size': '16px',
									 color		:'#C60303'
								}).html((($('input[name=msgDefault]').val()==0)?respuesta.msg:'Your changes couldn\'t be processed. Please try again')+'<div class="clear"></div><a href="#" class="buttonred cerrarVentana">Return</a>');
								$('.cerrarVentana').bind('click',function (){
									$('.lockScreen,.MsglockScreen').fadeOut(200,function (){
											$('.MsglockScreen').css({
												 'font-size': '26px',
												 color		:'#005C83',
											}).html('<img src="../resources/img/ac.gif">  Processing...');
										});
									})
							}
						}
				})
			}
			return false;
		}
	}

function phonesvalidation(d,sep,rel,n)
{   
	var pat = new Array('3','3','4'); 
  	val = d.value     
	lar = val.length;
	f 	= val.charAt(lar-1);
	if (n) 
	{
		if ( isNaN(f) && f != sep )
		val = val.substr(0,lar-1);
	}
	if (f==sep)
	{
		x	= val.split(sep) 
		xi  = x.length -1;
		var teclas_especial=[45];
		if (val=teclas_especial)
		val='';
		else
			for (i=0; i < xi; i++)
			{
				for (j=0; j < pat[i] - x[i].length; j++)
				val += rel;
				val += x[i] + sep;
			}
	}		
	x= val.split(sep) 
	xi  = x.length -1;
	
	if (xi != 0)
	{
		aux = x[xi];		
	}
	else
		aux = val;
	if ( aux.length > pat[xi])
	{
		x[xi] = aux.substr(0,pat[xi]) + sep + aux.substr(pat[xi]); 
	}
	aux ='';
	for (i=0; i<= xi; i++)
	{
		aux += x[i];
		if ( xi > 0 && i < xi ) 
		{
			aux += sep;
		}
	}
	d.value = aux; 
}	
function solonumeros(e){

    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
    if (tecla==44) return true; //Coma ( En este caso para diferenciar los decimales )
    if (tecla==48) return true;
    if (tecla==49) return true;
    if (tecla==50) return true;
    if (tecla==51) return true;
    if (tecla==52) return true;
    if (tecla==53) return true;
    if (tecla==54) return true;
    if (tecla==55) return true;
    if (tecla==56) return true;
    if (tecla==57) return true;
    patron = /1/; //ver nota
    te = String.fromCharCode(tecla);
    return patron.test(te); 
}

function isValidEmail(mail)
{
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail);
}

function CurrencyFormat(number)
{
   var decimalplaces = 2;
   var decimalcharacter = ".";
   var thousandseparater = ",";
   number = parseFloat(number);
   var sign = number < 0 ? "-" : "";
   var formatted = new String(number.toFixed(decimalplaces));
   if( decimalcharacter.length && decimalcharacter != "." ) { formatted = formatted.replace(/\./,decimalcharacter); }
   var integer = "";
   var fraction = "";
   var strnumber = new String(formatted);
   var dotpos = decimalcharacter.length ? strnumber.indexOf(decimalcharacter) : -1;
   if( dotpos > -1 )
   {
      if( dotpos ) { integer = strnumber.substr(0,dotpos); }
      fraction = strnumber.substr(dotpos+1);
   }
   else { integer = strnumber; }
   if( integer ) { integer = String(Math.abs(integer)); }
   while( fraction.length < decimalplaces ) { fraction += "0"; }
   temparray = new Array();
   while( integer.length > 3 )
   {
      temparray.unshift(integer.substr(-3));
      integer = integer.substr(0,integer.length-3);
   }
   temparray.unshift(integer);
   integer = temparray.join(thousandseparater);
   return sign + integer + decimalcharacter + fraction;
}