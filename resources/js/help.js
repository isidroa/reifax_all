Ext.ns('help');

help={
	init:function(){
		$('#loading-msg').html('');
		Ext.define('helpMenu', {
			extend: 'Ext.data.Model',
			fields: [
				{name: 'text',   type: 'string'},
				{name: 'id', type:'int'},
				{name: 'is_tree', type: 'int'}
			],
			idProperty:'id'
		});
		
		
		help.interfax.menu=Ext.create('Ext.tree.Panel', {
			store: help.store.global,
			rootVisible: false,
			border:false,
			split	:true,
		    height: $(document).height()-120,
			//autoScroll :true,
			listeners:{
				itemclick:function ( obje, record, Html, index, e, eOpts ){
					//if(record.data.leaf){
						var id=record.raw.isVideo?record.raw.id_modulo:record.raw.id;
						var textTitle=record.raw.isVideo?record.raw.name_modulo:record.raw.text;
						var tabP=help.interfax.viewPor.down('#panelP');
						var tabE=tabP.down('#tabP'+id);
						if(record.raw.isVideo){
							help.nextRepro=record.raw.id_video;
						}
						if(tabE){
							tabE.show();
							if(record.raw.isVideo){
								$('[data-idVideo='+record.raw.id_video+']').trigger('click');
							}
							return true;
						}
						$.ajax({
							url:"/resources/php/funcionesHelp.php",
							data:{
								id:id,
								action	:'getDataPanel'
								},
							type:'POST',
							dataType:"json",
							success: function(res){
								$('.reproductor').each(function(index, element) {
                                   // flowplayer(this.id).pause();
                                });
								Ext.define('help.Video', {
									extend: 'Ext.data.Model',
									fields: [
										{ name:'id', type:'int' },
										{name:'id_video',type:'string'},
										{name:'idYoutube',type:'string'},
										{name:'urlImg',type:'string'},
										{ name:'name', type:'string' }
									]
								});
								
								var storeSequence=Ext.create('Ext.data.Store', {
									model: 'help.Video',
									data: res.sequence,
									listeners:{
										'load'	:	function (store, records, successful, eOpts){
											//console.debug(store, records, successful, eOpts);
										},
										beforeload: function ( store, operation, eOpts ){
											//console.debug(store, operation, eOpts);
										}
									}
								});
								
								var imageTpl = new Ext.XTemplate(
									'<tpl for=".">',
										'<div data-idVideo="{id_video}" style="margin-bottom: 10px; background: url(\'{urlImg}\') repeat scroll 0 0 / 147px auto rgba(0, 0, 0, 0)" dataidmenu="{id}",   class="thumb-wrap {class}">',
										  '',
										  '<div class="title">{name}</div></div>',
										'</div>',
									'</tpl>'
								);
								
								
								
								tabP.add({
									itemId:'tabP'+id,
									title:textTitle,
									closable : true,
									layout:'border',
									defaults: {
										collapsible: true,
										bodyStyle: 'padding:15px'
									},
									items: [{
											title: 'Videos',
											region: 'south',
											height: 170,
											autoScroll :true,
											minSize: 75,
											items:[Ext.create('Ext.view.View', {
												store: storeSequence,
												tpl: imageTpl,
												itemSelector: 'div.thumb-wrap',
												emptyText: 'No helps available',
												listeners:{
													itemclick : function ( obje, recordO, itemO, index, e, eOpts ){
														if(!$(itemO).hasClass('active')){
															$(itemO).parent().find('.active').removeClass('active');
															$(itemO).addClass('active');
															$("#player"+id+"").empty().append('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/'+recordO.data.idYoutube+'?autoplay=1" frameborder="0" allowfullscreen></iframe>');
														}
													},
													'refresh'	:	function (view,  eOpts){
															if(help.nextRepro!=null){
																$('[data-idVideo='+help.nextRepro+']').trigger('click');
																help.nextRepro=null;
															}
															else{
																$('[data-idVideo='+view.store.getAt(0).data.id_video+']').trigger('click');
															}
															
													}
												}
											})],
											maxSize: 250,
											cmargins: '5 0 0 0'
										}/*,{
											title: 'you may be interested',
											region:'east',
											margins: '5 0 0 0',
											cmargins: '5 5 0 0',
											bodyStyle: 'padding:8px',
											items:[Ext.create('Ext.view.View', {
												store: storeRelated,
												tpl: imageTpl,
												itemSelector: 'div.thumb-wrap',
												emptyText: 'No helps available',
												listeners:{
													itemclick : function ( obje, record, itemO, index, e, eOpts ){
														$('[data-recordid=menu_'+record.data.id+']').trigger('click');
													}
												}
											})],
											width: 200,
											minSize: 100,
											maxSize: 250
										}*/,{
											title: '',
											collapsible: false,
											region:'center',
											margins: '5 0 0 0',
											html:'<div class="reproductor" style="width:100%;height:95%;margin:0 auto;" id="player'+id+'"></div>'
										}
									]
								}).show();
							}
						});
						
					}
				//}
			}
		});
		help.interfax.viewPor=Ext.create('Ext.container.Viewport', {
			layout: 'border',
			items: [{
				region: 'west',
				collapsible: true,
				xtype:'panel',
				tbar: [{
						fieldLabel: 'Search',
						xtype:'textfield',
						width:200,
						emptyText:'you\'re looking for?',
						name: 'search',
						id	: 'keySearch',
						hideLabel:true,
						allowBlank: true,
						listeners:{
							resize:function (){
								
							},
							specialkey: function(field, e){
								// e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
								// e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
								if (e.getKey() == e.ENTER) {
									help.fn.search();
								}
							}
						}
					},
				  	{
						xtype	: 'button',
						text	: 'Search',
						handler	: help.fn.search
					},
				  	{
						xtype	: 'button',
						text	: 'Reset',
						handler	: help.fn.reset
					}
				],
				title: 'Reifax\'s Help',
				width: 320,
				split:true,
				layout:'fit',
				items:[help.interfax.menu]
				// could use a TreePanel or AccordionLayout for navigational items
			},/*, {
				region: 'south',
				title: 'South Panel',
				collapsible: true,
				html: 'Information goes here',
				split: true,
				height: 100,
				minHeight: 100
			}, {
				region: 'east',
				title: 'East Panel',
				collapsible: true,
				split: true,
				width: 150
			}, */{
				region: 'center',
				xtype: 'tabpanel', // TabPanel itself has no title
				activeTab: 0,
				itemId:'panelP',
				items: []
			}]
		})
		help.store.global.on('beforeload',function (store){
					var proxy=store.getProxy();
					proxy.extraParams.q=Ext.getCmp('keySearch').getValue();
				}
		)
	},
	store:{
		global:Ext.create('Ext.data.TreeStore', {
			extend	: 'Ext.data.TreeStore',
			model	: 'helpMenu',
			autoSync: true,
			proxy	: {
				type: 'rest',
				url	: '/resources/php/funcionesHelp.php',
				actionMethods	: {
					create	: 'POST',
					read	: 'POST',
					update	: 'POST',
					delete	: 'POST'
				},
				extraParams	:{
					typeHelp:typeHelp,
					action	:'listMenu'
				}
			},
			nodeParam: 'id',
			root	: {
                    id: 'id',
                    expanded: true
			}
		})
	},
	interfax:{
	},
	fn:{
		search:function (){
			help.store.global.load();
		},
		reset:function (){
			Ext.getCmp('keySearch').setValue('');
			help.store.global.load();
		}
	},
	nextRepro:null
}




Ext.onReady(help.init);


