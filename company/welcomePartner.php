<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="aboutUs.php"><span class="bluetext underline">Company</span></a> &gt; 
        	<span class="fuchsiatext">Welcome To Partner</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Welcome to <span class="greentext">REI</span>Fax Affiliate Partner Program</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>Welcome to REIFax Affiliate Partner Program</h2> 
                    <p class="bold">
                   		Congratulations!!!  You are now part of the selected group of people promoting the fastest growing info and data web site for real estate professionals.
                    </p>
                    <div class="clear">
                    </div>
                    <p>
                    	One of the most important places on REIFax is "My Settings":
                    </p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div>Click on "My Settings" button located at the upper right corner inside the Welcome box. Your main menu banner should have changed to Home, My Account and My Affiliate.
                       </li>
                    	<li>
                        	<div class="cuote"></div>If you want to modify your personal information, credit card data or your password, you may do so by clicking on the specific link in My Account.
                      	</li>
                    </ul>
                    <p>
                    	There are three important places for Affiliate Partners on REIFax:
                    </p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div><span class="bold">The first one is "Affiliate Referrals":</span> Click on "My Settings" button located at the upper right corner inside the Welcome box. Then in the drop-down menu in "My Affiliate" click on "Affiliate Referrals". This is the page where you can review and control your affiliate referrals. There are two levels: "Affiliate Level 1" is the list including customers you referred to us and "Affiliate Level 2" is the list including customers referred to us by your referrals included in the "Affiliate Level 1" list. It is worth noting that you will earn a 10% commission from every customer subscription plan payment included in "Affiliate Level 2" list, as long as the customer remains active.                      	
                       </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">The second one is "Affiliate Commission":</span> This is the page where you can review and control your affiliate commissions. Please remember: Commissions are calculated at the end of each month and paid within 21 days of the next month.                       </li>
                    	<li>
                        	<div class="cuote"></div><span class="bold">The last one is "Banners":</span> This is the page where you can find 6 different REIFax banners. Just place them in your web site. Every time a customer clicks on an REIFax affiliate link on your site will be redirected to the REIFax web site. As long as he/she remains navigating in our site our systems will recognize that customer as referred by you and will put your Promotion Code at time of registration.
                       </li>
                    </ul>
                    <p>
                    If you don't have a web site you just need to make sure that your referrals enter your Promotion Code at time of registration.
                    </p>
                    <div class="centertext">
                    <p>
                    	Your Promotion Code Is: <?php echo $_GET['proCode']; ?>
                    </p>
                    </div>
                    <p>
                    	For further information please click on <a class="bluetext underline" href="http://www.reifax.com/settings/banners.php">banners</a>.
                    </p>
                    <p>
                    	Please remember our customer service reps are ready to assist you with any question you may have on week days, from 9 am to 6 pm. Chat with them by clicking on "Live Chat" button located at the upper center box or call them dialing 1 (888) 349-5368. They will happily assist you.
                    </p>
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="../index.php" class="buttongreen bigButton">Continue</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
</script>