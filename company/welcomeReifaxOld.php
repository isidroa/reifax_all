<?php
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="aboutUs.php"><span class="bluetext underline">Company</span></a> &gt; 
        	<span class="fuchsiatext">Welcome</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">REIFax's New Website Goes Live </span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>REIFax's New Website Goes Live </h2> 
                    <div class="clear">
                    </div>
                    <p class="bold">
                   		Dear subscriber
                    </p>
                    <p>
                    	On November 16, 2011, REIFax launched a fresh, informative and easy-to-navigate website that provides our customers and other real estate professionals with the latest products, services, news and partner information from REIFax. The new website is rich in content and provides professionals in the real estate industry an ideal user experience for evaluating key products and service needs. On the site, visitors can find detailed information on property sales and foreclosure statistics, product and services, training videos and so much more.
                    </p>
                    <p>
                    	The purpose of our new site and design is to help our customers and prospects quickly and efficiently get the information they need on our products and services. We've organized the content to make it easier than ever to get in touch with us to discuss our solutions. We'll be mapping this rich content against relevant industry news that will be delivered through social media and providing insight into best practices in real estate investing. Our goal is to balance the business side of our website with our desire to be a trusted source of information in real estate market statistics, trends and the latest technology and tools to help our customers achieve their goals. Through surveys, case studies, white papers, blogs and videos, REIFax.com will become the go-to site for insight into this dynamic and profitable industry.
                    </p>
                    <p>
                    This new web site launching is only a small evidence of our firm intention to re-investing a substantial part of our income in the improvements we want to accomplish, not only to our web page, but also to all our products, and in the research and development of new features and products, in order to provide our customers with the best tools and information necessary to succeed in their real estate business.
                    </p>
                    <div class="centertext">
                    <p style="background:#FABF8F; display:compact;">
                    	We are proud to announce that we have just released two new products,<br>
REIFax Professional and Leads Generator.

                    </p>
                    </div>
                    <p>
                    	<span class="bold">REIFax Professional</span> is our top-of-the-line product which is packed with features for real estate investors, agents, short sale specialists and many other real estate professionals. It's the most advanced and complete REIFax product. It was designed for Real Estate Professionals who need the best tool to search for real estate opportunities in single family homes, condos/townhomes/villas, multi-family with 10 + or - units, commercial properties, vacant land, mobile homes and other properties. REIFax Professional includes all features included in REIFax Platinum plus the Management & Task Tools: ▪ Contact Manager, ▪ Task Manager,        ▪ Mailing Campaign Manager, ▪ Follow-up System and ▪ Short Sale Manager.                    </p>
                    <p>
                    	<span class="bold">Leads Generator</span> is a powerful tool that enables you to find clients that are ready and willing to sell their properties, with our exclusive feature Search By Lis Pendens. Leads Generator was designed with the sorting capabilities needed to get the best real estate properties selling opportunities. This product is included if you are a subscriber of REIFax Platinum or Professional.
                    </p>
                    
                    <div style="background:#E5DFEC; margin-top:10px; margin-bottom:10px;">
                    	<p style="font-style:italic">
                    		As a way to celebrate this great event, we are offering our current loyal customers a <span class="bold">15-day REIFax Professional Free Trial</span>. This opportunity will be only available until November 30, 2011. To sign up for the 15-day REIFax Professional Free Trial please <a href="#" class="bluetext underline" id="activeTrial">Click Here Now</a>
                   		</p>
                    </div>
                    <div style="background:#C6D9F1; margin-top:10px; margin-bottom:10px;">
                    	<p>
                        	We are also proud to announce that we have released our new<span class="bold"> REIFax Advertising Program</span>. We are offering our advertisers the opportunity to reach highly targeted local audience of qualified real estate professionals, with prominently placed banner ads to drive them to advertiser products and services. To learn more about this program please click on <a href="advertiser" class="bluetext underline">Advertise With Us</a>. The Ad places will sell fast, so please don't wait, purchase yours now.
                        </p>
                    </div>
                    <p>
                    	One of the most important changes in our web site is the main menu banner, along with all the sub-banners. Being this the first time you log in after the new web site launch, you are reading this announcement letter. After you click the "Continue" button at the page end and next time you log in, you will need to follow the steps below
                    </p>
                    <ul>
                    	<li><div class="cuote"></div>Click on "My Settings" button located at the upper right corner inside the Welcome box. Your main menu banner should have changed to Home, My Products and My Account.
                        </li>
                    	<li><div class="cuote"></div>If you want to modify your personal or profile information, your password, upgrade your product, freeze your account or even cancel your subscription, you may do so by clicking on the specific link in My Account.
                        </li>
                    	<li><div class="cuote"></div>"My Products" will show you all the products you have access. Click on the product link you want to access.
                        </li>
                    </ul>
                    <p>
                    	Please remember our customer service reps are ready to help you with any question you may have on week days, from 9 am to 6 pm. Chat with them by clicking on "Live Chat" button located at the upper center box or call them dialing 1 (888) 349-5368. They will happily assist you.
                    </p>
                    <p class="bold">
                    	Interested in promoting our products and make some extra cash? Click on the link below to learn how to become our Affiliate Partner and start earning commissions today:
                    </p>
                    <div class="centertext">
                    	<p>
                        	<a href="http://www.reifax.com/company/become.php" class="bluetext underline">http://www.reifax.com/company/become.php</a>
                        </p>
                    </div>
                    <p>
                    Last but not least, We highly appreciate and welcome any feedback.
                    </p>
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="../index.php" class="buttongreen bigButton">Continue</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
 $('#activeTrial').bind('click',function (e){
	 e.preventDefault();
	 $.ajax({
		 url	:'../resources/php/activeTrialProfessional.php',
		 type	:'POST',
		 dataType:'json',
		 succces	:function (resul){
			 }
		 })
	 })
</script>