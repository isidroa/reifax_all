<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
	conectar('xima');
	$sql='SELECT * FROM workshop WHERE status=1 LIMIT 1;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$data=mysql_fetch_assoc($res);
	$sql='SELECT * FROM xima.usr_productobase where idproducto=13 AND activo=1 LIMIT 1	;';
	$res=mysql_query($sql) or die ($sql.mysql_error());
	$cost=mysql_fetch_assoc($res);
	
	
	
	/***************
	*	PRODUCTOS, FREQUENCIA, FECHA DEL PAGO del usuario
	***************/
	$sql='SELECT p.homefinder, p.buyerspro, p.leadsgenerator, p.residential, p.platinum, p.professional, p.professional_esp,f. idfrecuency frecuency ,c.fechacobro
		FROM permission p,f_frecuency f ,usr_cobros c
		where p.userid='.$_COOKIE['datos_usr']['USERID'].' AND p.userid=f.userid AND p.userid=c.userid limit 1';
	$respuesta=mysql_query($sql);
	if(mysql_num_rows($respuesta)>0)
	{
		$status=mysql_fetch_assoc($respuesta);
		if(($status['professional']==1 || $status['professional_esp']==1 || $status['platinum']==1) && $status['frecuency']==2)
		{
			$suscritor=true;
		}
	}
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div	 class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Welcome Workshop</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/xima3/website/resources/img/advertise/D3.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">You are registered</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs">
                	<h4>Congratulations! You are registered...</h4>
                        <div>
                         <p>Please print this page with the location information. Make sure to bring a picture id with you so that we can validate your seat, and please arrive at least 15 minutes before the event to allow time for registration. We want to start punctual...</p><p>Here are the details about the Event:</p>
                        <table>
                        	<tr>
                            	<td align="right" valign="top">
                                	<span class="bold redtext">When:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext"><?php echo $data['when'] ?> at <?php echo $data['hour'] ?><br> <?php echo $data['whennote'] ?></span>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="right" valign="top">
                                	<span class="bold redtext">Where:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext"><?php echo $data['address'] ?><br> <?php echo $data['city'] ?>, <?php echo $data['zipcode'] ?><br><?php echo $data['phone'] ?></span>
                                     <a href="<?php echo $data['linkMap'] ?>" target="_blank">Map & Directions</a>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="right">
                                	<span class="bold redtext">Cost:</span>
                                </td>
                                <td>
                                	<span class="bold blacktext">$<?php echo ($suscritor)?'0.00':number_format($cost['precio'],2) ?></span>
                                </td>
                            </tr>
                        </table>
                        <div class="clear">
                        </div>
                        <div class="centertext">
                        	<a href="/" class="buttongreen bigButton">Go To Home</a>
                        </div>
     					</div> 
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-training');
</script>