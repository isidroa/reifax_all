<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'The leading provider of real estate tools and information','Real estate, reifax, florida, florida real estate, information, about us, reifax','REIFax was created specifically to provide the best and most accurate foreclosure and pre-foreclosure information, data and statistics available, to help Real Estate Professionals make informed investment decisions.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">About Us</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">About Us - The leading provider of real estate tools and information</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs">
                	<h2 style="padding-left:15px;">About Us</h2>
                        <div>
                         <p>REIFax was created specifically to provide the best and most accurate foreclosure and pre-foreclosure information, data and statistics available, to help Real Estate Professionals make informed investment decisions. REIFax products were initially designed mainly for Real Estate Agents, Brokers and Investors, however, they currently provide useful information to other Real Estate Professionals as, among others, Mentors, Asset Managers, Attorneys, Loss Mitigators, Mortgage brokers, Property Appraisers, Short Sale Specialists and Title Companies.</p>
                        <p>
                        REIFax provides resources and tools that Home Buyers, Investors and other Real Estate Professionals need to locate, analyze and buy properties. You can easily create comprehensive property comparison reports in specific areas, giving you a very powerful tool when making or negotiating an offer.</p>
                        <p>
                        REIFAX is the one-stop destination to search for properties with over 30% Potential Equity. All this information is collected from many different sources, but it is presented to you in one place, and in a very easy format. This will give you the ability to get Active Listing information, Public Records, Mortgage, Pre-Foreclosure and Foreclosure details; you can get comparables of active listings, closed sales and rentals. Best of all, you can customize and print any of such reports, mailing lists, and labels for easy mailings.</p>
                        <p>
                        REIFax has been chosen by the most important Real Estate Investor Associations in Florida to supply them and their members with foreclosure data and other services.</p>
                        <p>
                        REIFax is the best resource for:<br>
                        Foreclosures<br>
                        Bank-owned properties<br>
                        Short sales<br>
                        Pre-Foreclosures<br>
                        Distressed property<br>
                        Properties with 30% + potential equity<br>
                        Foreclosure listings<br>
                        Foreclosure information, foreclosure data, foreclosure statistics<br>
                        Property public records<br>
                        Active Listings<br>
                        Closed sales information<br>
                        Rental properties information<br>
                        </p>
                        <p>
                        For a complete list of product features, please click the link below:<br>
                        <a class="greentext underline" href="http://www.reifax.com/products/compare.php">http://www.reifax.com/products/compare.php</a>
                        </p>
                        <p>
                        If you wish to subscribe for free to our newsletter you may do it on the right side of this webpage. We sincerely thank you for your interest in REIFax. We are looking forward to having you as our subscriber soon.
                        </p>
     					</div> 
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
</script>