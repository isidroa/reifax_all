<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="aboutUs.php"><span class="bluetext underline">Company</span></a> &gt; 
        	<span class="fuchsiatext">Welcome to REIFax</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Welcome to <span class="greentext">REI</span>Fax</h2> 
                    <p class="bold">
</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>Welcome to REIFax</h2> 
                    <p class="bold">
                   		Congratulations!!!  You are now part of the fastest growing info and data web site for real estate professionals. We're happy you're here!
                    </p>
                    <div class="clear">
                    </div>
                    <p>
                    	One of the most important places on REIFax is "My Settings":
                    </p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div>Click on "My Settings" button located at the upper right corner inside the Welcome box. Your main menu banner should have changed to Home, My Products and My Account.
                       </li>
                    	<li>
                        	<div class="cuote"></div>If you want to modify your personal or profile information, your password, upgrade your product, freeze your account or even cancel your subscription, you may do so by clicking on the specific link in My Account.
                      	</li>
                    	<li>
                        	<div class="cuote"></div> 	"My Products" will show you all the products you have access. Click on the product link you want to access.
                      	</li>
                    </ul>
                    <p>
                    	Getting started with REIFax doesn't take a lot of time. However, you may want to view our training videos before using your product. For a list of available videos, click on the link below:
                    </p>
                    <div class="centertext">
                        <p>
                        <a href="http://www.reifax.com/training/videoTraining.php" class="bluetext underline">http://www.reifax.com/training/videoTraining.php</a>
                        </p>
                    </div>
                    <p>
                    Please remember our customer service reps are ready to help you with any question you may have on week days, from 9 am to 6 pm. Chat with them by clicking on "Live Chat" button located at the upper center box or call them dialing 1 (888) 349-5368. They will happily assist you.
                    </p>
                    <p class="bold">
                    	Interested in promoting our products and make some extra cash? Click on the link below to learn how to become our Affiliate Partner and start earning commissions today
                   </p>
                    <div class="centertext">
                    <p>
                    	<a href="http://www.reifax.com/company/become.php" class="bluetext underline">http://www.reifax.com/company/become.php</a>
                    </p>
                    </div>
                   
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="../index.php" class="buttongreen bigButton">Continue</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
</script>