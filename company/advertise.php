<?php
foreach($_COOKIE['advertiseRegister'] AS $key =>$value)
		{
			setcookie("advertiseRegister[$key]",false,time()-3600,'/');
		}
unset($_COOKIE['advertiseRegister']); 
require_once('../resources/template/template.php');
require_once('../resources/php/properties_conexion.php');
conectar('xima');
$query=mysql_query('SELECT * FROM traffic_stats order by date_rank DESC limit 1;');
$data=mysql_fetch_array($query);
$aux3=str_replace(',','.',$data['month3_traffic_rank'])-str_replace(',','.',$data['month3_change_rank']);
$aux1=str_replace(',','.',$data['month1_traffic_rank'])-str_replace(',','.',$data['month1_change_rank']);
$date=explode('-',$data['date_rank']);
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Advertise With Us - The best place to reach real estate investors','Real estate, reifax, florida real estate, Advertise With Us, Advertising, advertiser, Advertising opportunities, Advertising banner','We offer advertisers the opportunity to reach highly targeted local audience of qualified real estate professionals, with prominently placed banner ads to drive them to advertiser products and services.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Advertise</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/become.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D2.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Advertise With Us - The best place to reach real estate investors</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>REIFax Advertising Overview</h2> 
                    
                	<p>Welcome to REIFax Advertising Center. We offer advertisers the opportunity to reach highly targeted local audience of qualified real estate professionals, with prominently placed banner ads to drive them to advertiser products and services.</p>
                    <p>Our site unique and frequent visitors are mainly high-caliber real estate professionals actively engaged in the real estate investment market in need of services from:
</p>
                    
                	<ul>
                        <li>
                        	<div class="cuote"></div>Hard Money Lenders
                        </li>
                        <li>
                        	<div class="cuote"></div>Real Estate Attorneys
                        </li>
                        <li>
                        	<div class="cuote"></div>Short Sale Specialists
                        </li>
                        <li>
                        	<div class="cuote"></div>Title Companies
                        </li>
                        <li>
                        	<div class="cuote"></div>General Contractors
                        </li>
                        <li>
                        	<div class="cuote"></div>Certified Public Accountants
                        </li>
                        <li>
                        	<div class="cuote"></div>Property Appraisers
                        </li>
                        <li>
                        	<div class="cuote"></div>Property Inspectors
                        </li>
                        <li>
                        	<div class="cuote"></div>Flat Fee MLS Listing Services
                        </li>
                        <li>
                        	<div class="cuote"></div>And many other
                        </li>
                    </ul>
                	<h2>Site Statistics for <?php echo $Months[($date[1]-1)].' '.$date[2].', '.$date[0] ?></h2> 
                   
                	<p>Site Global Ranking according to Alexa.com:
                    </p>
                    <ul>
                        <li>
                        	<div class="cuote"></div>Last 3 months: <?php echo $data['month3_traffic_rank'].' (change  <img src="../resources/img/flecha01.png"> '.$data['month3_change_rank'].' 
							from '.$aux3.')'; ?>
                        </li>
                        <li>
                        	<div class="cuote"></div>Last month: <?php echo $data['month1_traffic_rank'].' (change  <img src="../resources/img/flecha01.png"> '.$data['month1_change_rank'].' 
							from '.$aux1.')'; ?>
                        </li>
                    </ul>
                    
                	<p>Site Unique Visitors: 2,452 </p>
                	<p>Site Visits: 8,560 (7,525 from Florida)</p>
                	<p>Average Time on Site: 4 minutes and 5 seconds </p>
                	<p>Site New Visits: 20.37% </p>
                    
                    <p>REIFax.com is very popular among users in South Florida where it is ranked #<?php echo $data['rank']; ?> at Alexa as of <?php echo $Months[($date[1]-1)].' '.$date[2].', '.$date[0] ?></p>
                    
                	<h2>Advertising opportunities</h2> 
                    <p>
                    We offer advertiser ad placement in our home page for a specific ad position, for a whole county and for a specific period of time (currently for a month or a quarter). When you purchase an ad position in our home page you will be the exclusive advertiser whose ad will be displayed at a specific position for a specific county and during a specific period of time (month or quarter). All REIFax.com visitors reaching the site from that specific county will see your Ad. For example, if you purchased the first Ad Position for Miami Dade county and for a month, all REIFax.com visitors reaching the site from that county will see your Ad in that position during a month.
                    </p>
                	<h2>Advertising banner specifications and placement positions</h2>
                     
                	<p>The Ads will be displayed at REIFax Home Page in the right column below the main menu banner. The following advertising position are available for placement on our home web page (Ad dimensions must be width: 300 pixels X height: 250 pixels, maximum file size 40k):</p>
                    <ul>
                        <li>
                        	<div class="cuote"></div>First Ad Position
                        </li>
                        <li>
                        	<div class="cuote"></div>Second Ad Position (right below 1st ad position).
                        </li>
                        <li>
                        	<div class="cuote"></div>Third Ad Position (right below 2nd ad position).
                        </li>
                        <li>
                        	<div class="cuote"></div>Fourth Ad Position (right below 3rd ad position).
                        </li>
                        <li>
                        	<div class="cuote"></div>Fifth Ad Position (right below 4th ad position).
                        </li>
                        <li>
                        	<div class="cuote"></div>Sixth Ad Position (right below 5th ad position).
                        </li>
                    </ul>
                    
                    <div class="clear"></div>
                	<p>Important: To ensure good reproduction and prevent pre-press problems, please follow these guidelines: REIFax accepts only standard JPG and GIF files. The ad and all images used must be RGB and optimized for the web. REIFax is not responsible for reproduction of ads that don't meet these specifications and requirements.</p>
                	<p class="bold">To purchase an Ad Position in our web page, please click on the Buy Now button below:</p>
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="http://www.reifax.com/register/advertiseRegister.php" class="buttongreen bigButton">Buy Now</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-advertise');
</script>