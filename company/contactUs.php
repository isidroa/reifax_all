<?php 
require_once('../resources/template/template.php');
require_once('../resources/php/properties_conexion.php');
if($_GET['mensaje'])
{
	$mensaje='REIFax Professional Live Training March 3rd, 2012';
}

?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'If you have questions about our products send us a message','Real estate, reifax, florida, florida real estate, contact us','If you have questions about our products, membership access, training, cancellations, billing, affiliate program, or general questions, please provide your complete contact information, add a comment about what you\'d like to talk about and we\'ll get right back to you.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        	<span class="fuchsiatext">Contact us</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Contact us - Please fill out the form below </span>
                     <div class="note">(<img src="../resources/img/ix1y64.gif">) Required Fields</div>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center register contactUs">
                	<h2>Send Us a Message</h2>
                    <div style="padding:0px 10px;">
                    If  you  have  questions  about  our  products,  membership  access,  training,  cancellations, billing, affiliate  program, or  general questions,  please provide  your complete  contact information, add a comment about what you'd like to talk about and we'll get right back to you.
                    </div>
                    <div class="clear"></div>
                    <div style="float:left;">
                    <form id="contacUs" action="../resources/php/sendEmailSupport.php">
                    <table>
                            <tr>
                                <td>
                                    <label class="required">First Name</label>
                                </td>
                                <td>
                                    <input type="text" name="firstName">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Last Name</label>
                                </td>
                                <td>
                                    <input type="text" name="lastName">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">E-mail Address</label>
                                </td>
                                <td>
                                    <input type="text" name="mailAddress">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Phone Number</label>
                                </td>
                                <td class="numberPhone">
                                    <input type="text" class="short" name="phoneNumberCod" maxlength="3" > <label>-</label> 
                                    <input type="text" name="phoneNumber" maxlength="7" > <label>Ext</label>
                                    <input type="text" class="short" name="phoneNumberExt" maxlength="3" > 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Company Name</label>
                                </td>
                                <td>
                                    <input type="text" name="companyName">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>City</label>
                                </td>
                                <td>
                                    <input type="text" name="city">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>ZIP (Postal) Code</label>
                                </td>
                                <td>
                                    <input type="text" name="ZIPCode">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="required">Your Message</label>
                                </td>
                                <td>
                                    <textarea name="message"><?php echo $mensaje; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                            	<td></td>
                                <td>
                                    <div class="centertext">
                                        <a href="#" id="send" class="buttonblue">Send</a>
                                    </div>
                                </td>
                            </tr>
                     </table>
                     
                     </form></div>
                       <div class="methodContact">
                        <p class="bold graytext">Customer Service:</p>
                     	<table border="0">
                          <tr>
                            <td>Phone:</td>
                            <td>1 (888) 349-5368</td>
                          </tr>
                          <tr>
                            <td>Fax:</td>
                            <td>1 (888) 376-9274</td>
                          </tr>
                          <tr>
                            <td>Email:</td>
                            <td>support@reifax.com</td>
                          </tr>
                          <tr>
                            <td>Chat:</td>
                            <td>
                        	<a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank" class="buttonblue">Live Chat</a></td>
                          </tr>
                        </table>
                        
                        <div class="clear"></div>
                        <p class="bold graytext">
                        	Business Hours: 		
                        </p>
                        Monday through Friday<br>
                        9am - 6pm (Eastern Time)
                        
                        <div class="clear"></div>
                        <p class="bold graytext">Mailing Address:</p>
                        Reifax<br>
                        18459 Pines Blvd, Suite 139<br>
                        Pembroke Pines, FL 33029<br>
                        
                        <div class="clear"></div>
                        <p class="bold graytext">Follow Us:</p>
                        <div id="followus"> 
                            <a href="https://www.facebook.com/REIFaxPowerfulRealEstateOnlineTool" class="facebook">&nbsp;</a>
                            <a href="https://plus.google.com/+ReifaxRealEstateFlorida" class="google">&nbsp;</a>
                            <a href="http://twitter.com/reifax" class="twitter">&nbsp;</a>
                            <a href="https://www.youtube.com/user/Reifax" class="youtube">&nbsp;</a>
                        </div>

                      </div>
                    
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>
<div class="lockScreen"></div>
<div class="MsglockScreen bold bluetext">
Processing...
</div>
</html>
<script language="javascript">
 menuClick('menu-company');
  $(document).ready(function (){
	 validationRegister();
	 $('#contacUs').bind('submit',sendToEmail);
	 
	 $('#send').bind('click',function (e){
		e.preventDefault();
		$('.fieldRequired').each(function (){
			$(this).trigger('blur');
		});
		if(email && correcto)
		{
			$('.lockScreen').fadeTo(200,0.5);
			$('.MsglockScreen').fadeIn(200);
			$('#contacUs').trigger('submit');
		}		
	})
})
function sendToEmail(e) 
{
	e.preventDefault();
	$.ajax({
		url		:$(this).attr('action'),
		type	:'POST',
		dataType:'json',
		data	:$(this).serialize(),
		success	: function (resul)
		{
			if(resul.success)
			{
				$('.MsglockScreen').html('message successfully sent');
			}
			else
			{
			}
			$('.lockScreen').delay(2000).fadeOut(500);
			$('.MsglockScreen').delay(2000).fadeOut(500,function (){$(this).html('Processing...')});
		}
	})
}
</script>