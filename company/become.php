<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead(0,'Affiliates - Join the REIFax affiliate partner program','Real estate, florida real estate, reifax affiliate partner program, become a partner, join now, subscribe, affiliates','By becoming a REIFax Affiliate Partner, you\'ll be able to provide the Real Estate industry\'s best information and tools source for your customers while focusing on what you do best.');?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt;  
        	<span class="fuchsiatext">Affiliates</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Become a Partner - Join the <span class="greentext">REI</span>Fax affiliate partner program</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>Take Advantage of a Buyer's Market</h2> 
                    
                	<p>Real Estate business has never been so rewarding as it is now. Real Estate Investors are making the best deals ever because of the current buyer's market and the amount of properties that are in Foreclosure or simple in default. It is truly a golden mine.
</p>
                    
                	<p>With REIFax, we've made it easy to get into the game. REIFax specializes in business management, marketing tools, and software for Agents, Brokers, Investors and other entrepreneurs in the real estate industry. We pride ourselves in being an all-in-one solution for Real Estate Professional, providing all the essentials our clients need to succeed in the Real Estate Investment Industry. 
</p>
                	<h2>Refer Customers for Cash</h2> 
                   
                	<p>By becoming a REIFax Affiliate Partner, you'll be able to provide the Real Estate industry's best information and tools source for your customers while focusing on what you do best. We're partnering with Real Estate Mentors to create mutually beneficial relationships and enable you to offer your customers a complete Real Estate information tool package. We offer you to become our Affiliate Partner.
</p>
                    
                	<p>Simply send us potential customers and we'll take care of the rest. We handle the billing and support, while you provide a complimentary service. Affiliate Partners receive a recurring commission stream for ongoing customer referrals. The longer referral customers maintain an active subscription plan, the more money you'll make.
</p>
                	<h2>How Does it Work</h2> 
                   	<ul><li>
                        	<div class="cuote"></div>Sign up for a REIFax Affiliate Partner Account, you'll get a Promotion Code which will identify you as a REIFax Affiliate Partner.</li><li>
                        	<div class="cuote"></div>Post our affiliate links in your web site or email.</li><li>
                        	<div class="cuote"></div>Customer clicks on an affiliate link on your site or in an email which contains embedded the Promotion Code or simply types the Promotion Code at registration.</li><li>
                        	<div class="cuote"></div>If the customer purchases a plan, the order will be registered as a sale referred by you.</li><li>
                        	<div class="cuote"></div>Once the customer orders and we successfully process the payment, your Affiliate account will be updated with the sale information.</li><li>
                        	<div class="cuote"></div>Commissions will be paid by the 15th day of the calendar month following the month in which REIFax has received payment on the Commissionable Sale.</li>
                    	<li class="bold">As long as the customers remains active, you'll continue to get paid. </li>
                    </ul>
                	<h2>Commission Payment Structure</h2> 
                	<p>REIFax offers a simple commission payment structure for each customer lead you send our way that becomes an actual sale. Commission payout amount is based on the number of REIFax products sold, as shown below:
</p>
                    <table align="center" class="bold">
                    	<tr>
                        	<th></th>
                            <th>Active Customers</th>
                            <th>Referral Commission</th>
                        </tr>
                        <tr class="par">
                        	<td>Level 1</td>
                        	<td><div class="centerNumber"><div>1</div><div>-</div><div>50</div></div></td>
                        	<td>20 %</td>
                        </tr>
                        <tr class="impar">
                        	<td>Level 2</td>
                        	<td><div class="centerNumber"><div>51</div><div>-</div><div>100</div></div></td>
                        	<td>25 %</td>
                        </tr>
                        <tr class="par">
                        	<td>Level 3</td>
                        	<td><div class="centerNumber"><div>101</div><div>-</div><div>+</div></div></td>
                        	<td>30 %</td>
                        </tr>
                    </table>
                    <div class="clear"></div>
                	<p>&nbsp;</p>
                	<p>For details about our Affiliate Program, please refer to our <a href="../resources/pdf/REIFax_Affiliate_Partner_Terms_and_Conditions.pdf" target="_blank" class="greentext underline">Affiliate Partner Terms and Conditions</a></p>
                	<p>If you have any questions or would like further information regarding the REIFax Affiliate Partner Program, please contact us at <a class="greentext underline" href="malito:support@reifax.com">support@reifax.com</a></p>
                    <p>Click on the <span class="greentext">"Join Now"</span> button to create your REIFax Affiliate Partner account and start earning money today.
</p>
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="http://www.reifax.com/register/partnerRegister.php" class="buttongreen bigButton">Join Now</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-affiliate');
</script>