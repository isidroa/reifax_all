<?php 
	require_once('../resources/template/template.php');
	require_once('../resources/php/properties_conexion.php');
?>
<!DOCTYPE HTML>
<html>

<!-- Head of ReiFax Website -->
<?php ReiFaxHead();?>

<body>

<div class="container">
    <!-- Header of ReiFax Website -->
    <?php ReiFaxHeader();?>
	
    <!--ReiFax "You are here" Text  -->
    <div id="heretext" class="bluetext">
    	<div class="cuote big">&nbsp;</div>
        You are here:
        <a href="../index.php">
           	<span class="greentext underline">REI</span><span class="bluetext underline">Fax Home</span></a> &gt; 
        <a href="aboutUs.php"><span class="bluetext underline">Company</span></a> &gt; 
        	<span class="fuchsiatext">Welcome Advertising</span>
    </div>
    <div id="contentPrincipal">
  	<!-- Advertising Right Block-->
    <div class="sidebarright">
                    <a href="http://messenger.providesupport.com/messenger/ximausa.html" target="_blank">
                    	<img src="http://www.reifax.com/resources/img/advertise/D3.png">
                    </a>
                    <a href="http://www.reifax.com/company/contactUs.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D6.png">
                    </a>
                    <a href="http://www.reifax.com/company/advertise.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D1.png">
                    </a>
                    <a href="http://www.reifax.com/training/overviewTraining.php">
                    	<img src="http://www.reifax.com/resources/img/advertise/D5.png">
                    </a>
    </div>
    <!-- Center Content-->
    <div class="content">
    	<div class="reportingframework">
        	<div class="panel">
           	 	<div class="center centertext">  
            	     <span class="title bold bluetext">Welcome to <span class="greentext">REI</span>Fax Advertising Program</span>
            	</div>
        	</div>
        </div>
       <!--Text container-->
		<div class="panel">
           	 	<div class="center informationaltext">
                	<h2>Welcome to REIFax Advertising Program</h2> 
                    <p class="bold">
                    Congratulations!!!  You have taken the right steps to reach highly targeted local audience of qualified real estate professionals. REIFax is the fastest growing info and data web site for real estate professionals.
                    </p>
                    <div class="clear">
                    </div>
                    <p>
                    	One of the most important places on REIFax is "My Settings":
                    </p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div>Click on "My Settings" button located at the upper right corner inside the Welcome box. Your main menu banner should have changed to Home, My Account and My Advertising.                      	</li>
                    	<li>
                        	<div class="cuote"></div>If you want to modify your personal information, credit card data or your password, you may do so by clicking on the specific link in My Account.
                      	</li>
                    </ul>
                    <p>
                    	To get started with your Ad placement, please follow the steps below:
                    </p>
                    <ul>
                    	<li>
                        	<div class="cuote"></div>First click on "My Settings" button located at the upper right corner inside the Welcome box. Then in the main menu click on "My Advertising". This is the page where you can add, review or change your Ad images and urls.                      	
                       </li>
                    	<li>
                        	<div class="cuote"></div>To add or change an Ad simply click on "Change Image" button and then click on "browse" button. Select the image you want to upload. When finished the image will be automatically uploaded and will be displayed on the left rectangle box. Please allow up to 48 business hours for Ad approval.
                       </li>
                    	<li>
                        	<div class="cuote"></div>Next you will need to enter the url where the Ad will be pointing. Click on the "Change Url" button, enter the url and click on "ok" button. To make sure the url is correct, click on the image, it should take you to your web page. If it doesn't take you to your web page, click on "Change Url" button and try again. If it still doesn't work call our customer service.
                       </li>
                    	<li>
                        	<div class="cuote"></div>You can change your Ad image anytime you want during the period you purchase while unexpired, however, if you do so you will have to wait up to 48 business hours for Ad approval.                      	
                       </li>
                    </ul>
                    <div class="clear"></div>
                    <div class="centertext">
                    	<a href="../index.php" class="buttongreen bigButton">Continue</a>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        
        <!--End ext container -->
        
    </div>
	    <div class="clear">&nbsp;</div>
    </div>
    <?php ReiFaxFooter();?>

</div>

</body>

</html>
<script language="javascript">
 menuClick('menu-company');
</script>